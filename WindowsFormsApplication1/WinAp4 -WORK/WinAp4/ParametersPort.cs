﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinAp4
{
    public class ParametersPort
    {
        public void SetPortParams(ref StructParamsPort structParamsPort)
         {
            string[] paramPort = iniRW.get_ParametersPort().Split(',');

            if (paramPort.Length == 9 && paramPort[0] != "" && paramPort[1] != "" && paramPort[2] != "" && paramPort[3] != "" && paramPort[4] != "" && paramPort[5] != "" && paramPort[6] != "" && paramPort[7] != "")
            {
                structParamsPort.PortName = CorrectPortParam(paramPort[0]);
                structParamsPort.BaudRate = int.Parse(paramPort[1]);
                structParamsPort.DataBits = int.Parse(paramPort[2]);

                structParamsPort.Parity = CorrectPortParam(paramPort[3]);
                structParamsPort.StopBits = CorrectPortParam(paramPort[4]);

                structParamsPort.RtsEnable = bool.Parse(paramPort[5]);
                structParamsPort.DtrEnable = bool.Parse(paramPort[6]);

                structParamsPort.ReadTimeout = int.Parse(paramPort[7]);
                structParamsPort.WriteTimeout = int.Parse(paramPort[8]);
            }
            else
                MessageBox.Show("Параметры COM-порта в ini-файле заданы неверно!", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

        }

        /// <summary>
        /// Корректировка параметров COM-порта (замена нескольких пробелов на один)
        /// </summary>
        /// <param name="strInput"></param>
        /// <returns>строка без пробелов</returns>
        private string CorrectPortParam(string strInput)
        {
            string pattern = "\\s+";
            string replacement = " ";
            Regex rgx = new Regex(pattern);

            string strResult = rgx.Replace(strInput, replacement);

            if (strResult.EndsWith(" "))
                strResult = strResult.TrimEnd(' '); // удаление пробела в конце строки

            if (strResult.StartsWith(" "))
                strResult = strResult.TrimStart(' '); // удаление пробела в начале строки

            return strResult;
        }
    }


}
