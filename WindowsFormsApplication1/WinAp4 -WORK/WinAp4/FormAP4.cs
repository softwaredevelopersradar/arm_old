﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;
using LibNMEAParser;
using LibNMEAParser.Sentence;
using DeviceLib;
using DeviceLib.DataSource;
using DeviceLib.MessageSplitting;
using DeviceLib.HeaderSplitting;
using DeviceLib.Listenning;
using System.IO.Ports;
using CNTLib;
using DLL_Compass;
using NationalInstruments.UI;
using NationalInstruments;
using System.Threading;
using ClassLibraryPeleng;

namespace WinAp4
{
    public partial class FormAP4 : Form
    {
        //-----------Для пересчета координат------------------------------------------
        // DATUM
        // ГОСТ 51794_2008
        public double dX_Coord_datum = 25;
        public double dY_Coord_datum = -141;
        public double dZ_Coord_datum = -80;

        public double deltaLatWGS84;
        public double deltaLonWGS84;

        public double dLatSK42Geo;
        public double dLonSK42Geo;

        public double dLatSK42Pr;
        public double dLonSK42Pr;
        //---------------------------------------------------****--------------------
 
        class Colors
        {
            public Color LPA1_3;
            public Color LPA2_4;
            public Color LPA;
            public Color SP;
            public Color PU;

        }
        int[] col = new int[4];
        class Mestopolozhenie
        {
            public int ARD1_azimuth;//текущий от севера
            public int ARD1_need_azimuth;// желаемый, от севера
            public int ARD1_Radant;//текущий в АПУ
            public string ARD1_comN = "Com3";
            public bool ARD1_IsOpen=false;

            public int ARD2_azimuth;
            public int ARD2_need_azimuth;
            public int ARD2_Radant;
            public string ARD2_comN = "Com2";
            public bool ARD2_IsOpen = false;

            public int ARD3_azimuth;
            public int ARD3_need_azimuth;
            public int ARD3_Radant;
            public string ARD3_comN = "Com3";
            public bool ARD3_IsOpen = false;

            public double compass1_kren;
            public int compass1_azimuth;
            public double compass1_tangazh;
            public double compass1_correction;
            public double compass1_declination;
            public int compass1_final;
            public string compas1_comN = "COM1";
            public bool compass1_IsOpen = false;

            public double compass2_kren;
            public int compass2_azimuth;
            public double compass2_tangazh;
            public double compass2_correction;
            public double compass2_declination;
            public int compass2_final;
            public string compas2_comN = "Com5";
            public bool compass2_IsOpen = false;

            public int LPA_1_3 = 270;
            public int LPA_2_4 = 270;

            public Double dLatitude;
            public Double dLongitude;
            public int sputnic_count;
            public string date_time;

        }

        #region DLL
        [DllImport("kernel32")]
        private static extern int GetPrivateProfileString(string section, string key, string def, StringBuilder retVal, int size, string filePath);
       
        #endregion
        //Thread thr1; 
        private bool isOpenned;
        public bool fBearing = false;
    
        Mestopolozhenie mestopolozhenie;
        public int hmap;
        public Graphics graph;
        public double pLat, gLat;        
        public double pLon, gLon;
        public List<PointDouble> listStations;
        public List<PointDouble> listBearing;
        Random rand;
        public MessageTranslate messageTranslate;

        Peleng peleng;
        ParametersPort parametersPort;
        StructParamsPort structParamsPort;
        SetParameters setParameters; // dll LibNMEAParser
        GetMessage getMessage; // dll LibNMEAParser
        SerialPort serial_port;
        GPSEncoder gpsEncoder;
        DeviceHolder<string> m_gps_device_holder;
        PassiveListener<string> gps_all_message_listener;
        Compass Compas1 = new Compass();
        Compass Compas2 = new Compass();
        public ComARD ARD_LPA = new ComARD();
        public ComARD ARD_PU = new ComARD();
        public ComARD ARD_SP = new ComARD();
        int Led=0;
        int TimePause = 300;
        System.Threading.Timer timer;
        System.Threading.Timer tmWriteByteARD;
        System.Threading.Timer tmReadByteARD;
        System.Threading.Timer tmWriteByteSP;
        System.Threading.Timer tmReadByteSP;
        System.Threading.Timer tmWriteBytePU;
        System.Threading.Timer tmReadBytePU;
        int TimePause_compass1=10;
        int TimePause_compass2=10;
        Colors colors=new Colors();

        public FormAP4()
        {
            InitializeComponent();
            mestopolozhenie = new Mestopolozhenie();
            if (zapolnenie() != 1) { MessageBox.Show("Не удалось загрузить сохраненные данные из INI-файла", "Ошибка"); };
            //thr1 = new Thread(led);
            cbSatSyst.SelectedIndex = 1;
            cbCoordinateSystem.SelectedIndex = 0;
            try
            {
                InitConnectionARD1();
                InitConnectionARD2();
                InitConnectionARD3();
                //ARD1.OpenPort(mestopolozhenie.ARD1_comN, 9600, Parity.None, 8, StopBits.One);
                //mestopolozhenie.ARD1_IsOpen = true;
                //bConnectLPA.BackColor = Color.Green;
                //ARDPU.OpenPort(mestopolozhenie.ARD2_comN, 9600, Parity.None, 8, StopBits.One);
                //mestopolozhenie.ARD2_IsOpen = true;
                //bConnectPU.BackColor = Color.Green;
                //ARDSP.OpenPort(mestopolozhenie.ARD3_comN, 9600, Parity.None, 8, StopBits.One);
                //mestopolozhenie.ARD3_IsOpen = true;
                //bConnectSP.BackColor = Color.Green;

                //Compas1.OpenPort(mestopolozhenie.compas1_comN, 9600, Parity.None, 8, StopBits.One, TimePause_compass1);
                //mestopolozhenie.compass1_IsOpen = true;
                //bConnectKmp1.BackColor = Color.Green;
                //Compas2.OpenPort(mestopolozhenie.compas2_comN, 9600, Parity.None, 8, StopBits.One, TimePause_compass2);
                //mestopolozhenie.compass2_IsOpen = true;
                //bConnectKmp2.BackColor = Color.Green;
            }
            catch (Exception ex) { /*MessageBox.Show("Не удалось подключиться к СОМ-порту");*/ }

            //ARD1.OnGetAngleReceived += new ComARD.GetAngleReceivedEventHandler(GetAngleReceivedLPA);
            //ARD1.OnConnectPort += new ComARD.ConnectEventHandler(ShowConnectARD);
            //ARD1.OnDisconnectPort += new ComARD.ConnectEventHandler(ShowDisconnectARD);
            //ARDPU.OnGetAngleReceived += new ComARD.GetAngleReceivedEventHandler(GetAngleReceivedPU);
            //ARDSP.OnGetAngleReceived += new ComARD.GetAngleReceivedEventHandler(GetAngleReceivedSP);
            Compas1.OnMessage += new Compass.MessageEventHandler(Compas_OnMSG1);
            Compas2.OnMessage += new Compass.MessageEventHandler(Compas_OnMSG2);
            listStations = new List<PointDouble>();
            listBearing = new List<PointDouble>();
            messageTranslate = new MessageTranslate();
            messageTranslate.MesTranslate();
            rand = new Random();
           
           
          
            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            serial_port = new SerialPort();
            getMessage = new GetMessage();
            peleng = new Peleng(null);
            parametersPort = new ParametersPort();
            structParamsPort = new StructParamsPort();
            setParameters = new SetParameters();
            parametersPort.SetPortParams(ref structParamsPort);
            gpsEncoder = new GPSEncoder();
            m_gps_device_holder = new DeviceHolder<string>(new EndLineMessageSplitter(), new EmptyGPSHeaderSplitter());       
            setParameters.Position += new SetParameters.PositionEventHandler(setParameters_Position);
            setParameters.DatetimeUTC += new SetParameters.DateTimeUTCEventHandler(setParameters_DatetimeUTC);
            setParameters.DatetimeLocal += new SetParameters.DateTimeLocalEventHandler(setParameters_DatetimeLocal);
            setParameters.SatelliteCount += new SetParameters.SatelliteCountEventHandler(setParameters_SatelliteCount);
            setParameters.HeightAntenna += new SetParameters.HeightAntennaEventHandler(setParameters_HeightAntenna);
            m_gps_device_holder.onConnectionStatusChanged += new EventHandler<bool>(M_gps_device_onConnectionStatusChanged);
            gps_all_message_listener = new PassiveListener<string> { ID = string.Empty };
            gps_all_message_listener.onDataArrived += new EventHandler<IEnumerable<byte>>(Gps_all_message_listener_onDataArrived);
            gps_all_message_listener.Start();
            m_gps_device_holder.Subscribe(gps_all_message_listener);
            //bConnectGPS.PerformClick();
            PortGPSConnect();
            calculate_angle_to_north_Paint(mestopolozhenie);
            //ShowDirection(complexPlotLPA, mestopolozhenie.ARD1_azimuth);
            //ShowDirection(complexPlotPU, mestopolozhenie.ARD2_azimuth);
            //ShowDirection(complexPlotSP, mestopolozhenie.ARD3_azimuth);
            //tbLPA.Text = mestopolozhenie.ARD1_azimuth.ToString();
            //tbPU.Text = mestopolozhenie.ARD2_azimuth.ToString();
            //tbSP.Text = mestopolozhenie.ARD3_azimuth.ToString();
            timerLedReadBytes.Enabled = true;
        }

        void ShowConnectARD()
        {
            if (bConnectLPA.InvokeRequired)
            {
                bConnectLPA.Invoke((MethodInvoker)(delegate()
                {
                    bConnectLPA.BackColor = Color.Green;
                }));
            }
            else
                bConnectLPA.BackColor = Color.Green;
        }
        void ShowConnectPU()
        {
            if (bConnectPU.InvokeRequired)
            {
                bConnectPU.Invoke((MethodInvoker)(delegate()
                {
                    bConnectPU.BackColor = Color.Green;
                }));
            }
            else
                bConnectPU.BackColor = Color.Green;
        }
        void ShowConnectSP()
        {
            if (bConnectSP.InvokeRequired)
            {
                bConnectSP.Invoke((MethodInvoker)(delegate()
                {
                    bConnectSP.BackColor = Color.Green;
                }));
            }
            else
                bConnectSP.BackColor = Color.Green;
        }
        void ShowDisconnectARD()
        {
            if (bConnectLPA.InvokeRequired)
            {
                bConnectLPA.Invoke((MethodInvoker)(delegate()
                {
                    bConnectLPA.BackColor = Color.Red;
                }));
            }
            else
                bConnectLPA.BackColor = Color.Red;
        }
        void ShowDisconnectPU()
        {
            if (bConnectPU.InvokeRequired)
            {
                bConnectPU.Invoke((MethodInvoker)(delegate()
                {
                    bConnectPU.BackColor = Color.Red;
                }));
            }
            else
                bConnectPU.BackColor = Color.Red;
        }
        void ShowDisconnectSP()
        {
            if (bConnectSP.InvokeRequired)
            {
                bConnectSP.Invoke((MethodInvoker)(delegate()
                {
                    bConnectSP.BackColor = Color.Red;
                }));
            }
            else
                bConnectSP.BackColor = Color.Red;
        }
        private void FormAP4_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (sohranenie_to_ini() != 1) { MessageBox.Show("Не удалось сохранить данные в INI-файл", "Ошибка"); };
            try
            {
                ARD_LPA.ClosePort();
                mestopolozhenie.ARD1_IsOpen = false;
                ARD_PU.ClosePort();
                mestopolozhenie.ARD2_IsOpen = false;
                ARD_SP.ClosePort();
                mestopolozhenie.ARD3_IsOpen = false;
                Compas1.ClosePort();
                mestopolozhenie.compass1_IsOpen = false;
                Compas2.ClosePort();
                mestopolozhenie.compass2_IsOpen = false;
            }
            catch (Exception ex) { /*MessageBox.Show("Не удалось закрыть СОМ-порт"); */}
        
        }
        private void Gps_all_message_listener_onDataArrived(object sender, IEnumerable<byte> e)
        {
            if (InvokeRequired)
            {
                BeginInvoke((MethodInvoker)(() => Gps_all_message_listener_onDataArrived(sender, e)));
                return;
            }

            var text = Encoding.ASCII.GetString(e.ToArray());
            try
            {
                getMessage.parseMessage(text, setParameters);
                ledReadGPS1.Value = true;
                timer = new System.Threading.Timer(TimeStepLedReadGPS, null, TimePause, 0);
            }
            catch (Exception ex) { }
        }
        private void M_gps_device_onConnectionStatusChanged(object sender, bool e)
        {
            Console.WriteLine("GPS connected: {0}", e);

            if (e)
            {
                bConnectGPS.BackColor = Color.Green;
            }
            else
            {
                bConnectGPS.BackColor = Color.Red;
            }
        }

        private void setParameters_HeightAntenna(string code, double heightAntenna)
        {
            if (InvokeRequired)
            {
                BeginInvoke((MethodInvoker)(() => setParameters_HeightAntenna(code, heightAntenna)));
                return;
            }

            switch (cbSatSyst.SelectedIndex)
            {
                case 0: // GPS
                    if (code == "GP")
                        tbAlt.Text = heightAntenna.ToString();
                    break;

                case 1: // GPS/GLONASS
                    if (code == "GN")
                        tbAlt.Text = heightAntenna.ToString();
                    break;

                case 2: // GLONASS
                    if (code == "GL")
                        tbAlt.Text = heightAntenna.ToString();
                    break;
            }
        }

        private void setParameters_SatelliteCount(string code, int satelliteCount)
        {
            if (InvokeRequired)
            {
                BeginInvoke((MethodInvoker)(() => setParameters_SatelliteCount(code, satelliteCount)));
                return;
            }

            switch (cbSatSyst.SelectedIndex)
            {
                case 0: // GPS
                    if (code == "GP")
                        tbCountSat.Text = satelliteCount.ToString();
                    break;

                case 1: // GPS/GLONASS
                    if (code == "GN")
                        tbCountSat.Text = satelliteCount.ToString();
                    break;

                case 2: // GLONASS
                    if (code == "GL")
                        tbCountSat.Text = satelliteCount.ToString();
                    break;
            }
            //tbCountSat.Text = satelliteCount.ToString();
        }
        private void setParameters_DatetimeLocal(string code, DateTime dateTime)
        {
            if (InvokeRequired)
            {
                BeginInvoke((MethodInvoker)(() => setParameters_DatetimeLocal(code, dateTime)));
                return;
            }

            string[] dt = dateTime.ToString().Split(' ');

            switch (cbSatSyst.SelectedIndex)
            {
                case 0: // GPS
                    if (code == "GP")
                        tbDate.Text = dt[0];
                    break;

                case 1: // GPS/GLONASS
                    if (code == "GN")
                        tbDate.Text = dt[0];
                    break;

                case 2: // GLONASS
                    if (code == "GL")
                        tbDate.Text = dt[0];
                    break;
            }
            
            //tbDate.Text = dt[0];

            //tbTime.Text = dt[1];
        }
        private void setParameters_DatetimeUTC(string code, DateTime dateTime)
        {
            if (InvokeRequired)
            {
                BeginInvoke((MethodInvoker)(() => setParameters_DatetimeUTC(code, dateTime)));
                return;
            }

            string[] dt = dateTime.ToString().Split(' ');

            switch (cbSatSyst.SelectedIndex)
            {
                case 0: // GPS
                    if (code == "GP")
                        tbTime.Text = dt[1];
                    break;

                case 1: // GPS/GLONASS
                    if (code == "GN")
                        tbTime.Text = dt[1];
                    break;

                case 2: // GLONASS
                    if (code == "GL")
                        tbTime.Text = dt[1];
                    break;
            }


            //tbTime.Text = dt[1];
        }

        private void CoordinateSystem(double latitude, double longitude)
        {
            if (latitude == 0.0 && longitude == 0.0)
            {
                tbLat.Text = "";
                tbLon.Text = "";
                return;
            }

            // WGS84(эллипсоид)->элл.Красовского ***************************************************
            // Перевод координат WGS84(эллипсоид) -> эллипсоид Красовского(Пулково-42)
            // WGS84(широта,долгота)из Панорамы пересчитываем в эллипсод Красовского моей функцией
            // Входные параметры -> град,km
            // Перевод в рад - внутри функции

            // dLong ..............................................................................
            // Расчет приращения по долготе при преобразованиях координат WGS84<->SK42
            // (преобразования Молоденского), угл.сек
            peleng.f_dLong
                (
                // Входные параметры (град,км)
                    latitude,   // широта
                    longitude,  // долгота
                    0,          // высота

                    // DATUM,m
                    dX_Coord_datum,
                    dY_Coord_datum,
                    dZ_Coord_datum,

                    ref deltaLonWGS84   // приращение по долготе, угл.сек
                );
            // .................................................................... dLong

            // dLat .....................................................................
            // Расчет приращения по широте при преобразованиях координат WGS84<->SK42
            // (преобразования Молоденского), угл.сек
            peleng.f_dLat
                (
                // Входные параметры (град,км)
                    latitude,   // широта
                    longitude,  // долгота
                    0,          // высота

                    // DATUM,m
                    dX_Coord_datum,
                    dY_Coord_datum,
                    dZ_Coord_datum,

                    ref deltaLatWGS84        // приращение по долготе, угл.сек
                );

            // ..................................................................... dLat

            // Lat,Long .................................................................
            // Преобразования широты и долготы при пересчете WGS84->SK42
            peleng.f_WGS84_SK42_Lat_Long
                   (
                // Входные параметры (град,км)
                       latitude,   // широта
                       longitude,  // долгота
                       0,          // высота

                       deltaLatWGS84,       // приращение по долготе, угл.сек
                       deltaLonWGS84,       // приращение по долготе, угл.сек

                       // Выходные параметры (grad)
                       ref dLatSK42Geo,   // широта
                       ref dLonSK42Geo    // долгота
                   );

            // SK42(элл.)->Крюгер *****************************************************************
            // Преобразование геодезических координат (широта, долгота, высота) 
            // эллипсоида Красовского (СК42) в плоские прямоугольные координаты в
            // проекции Гаусса-Крюгера
            // Lat_Coord_Vyx_8442, Long_Coord_Vyx_8442 -> координаты эллипсоида Красовского,
            // получили моей функцией пересчета из панарамовских координат WGS84

            // Входные параметры -> !!!grad
            peleng.f_SK42_Krug
                   (
                // Входные параметры (!!! grad)
                // !!! эллипсоид Красовского
                       dLatSK42Geo,   // широта
                       dLonSK42Geo,  // долгота

                       // Выходные параметры (km)
                       ref dLatSK42Pr,
                       ref dLonSK42Pr
                   );

            switch (cbCoordinateSystem.SelectedIndex)
            {
                case 0: // WGS 84 ( гео )
                    {
                        tbLat.Text = latitude.ToString();
                        tbLon.Text = longitude.ToString();

                        lLAt.Text = "Широта, °";
                        lLon.Text = "Долгота, °";
                    }
                    break;

                case 1: // CK 42 ( гео ) Эллипсоид Красовского
                    {
                        tbLat.Text = dLatSK42Geo.ToString("0.000000");
                        tbLon.Text = dLonSK42Geo.ToString("0.000000");

                        lLAt.Text = "Широта, °";
                        lLon.Text = "Долгота, °";
                    }
                    break;

                case 2: // CK 42 ( прям ) Гаусса - Крюгера
                    {
                        tbLat.Text = (dLatSK42Pr * 1000).ToString("0.000000");
                        tbLon.Text = (dLonSK42Pr * 1000).ToString("0.000000");

                        lLAt.Text = "X, м";
                        lLon.Text = "Y, м";
                    }
                    break;
            }
        }

        private void setParameters_Position(string code, double latitude, double longitude)
        {
            if (InvokeRequired)
            {
                BeginInvoke((MethodInvoker)(() => setParameters_Position(code, latitude, longitude)));
                return;
            }
           
            switch(cbSatSyst.SelectedIndex)
            {
                case 0: // GPS
                    if (code == "GP")
                        CoordinateSystem(latitude, longitude);
                    break;

                case 1: // GPS/GLONASS
                    if (code == "GN")
                        CoordinateSystem(latitude, longitude);
                    break;

                case 2: // GLONASS
                    if (code == "GL")
                        CoordinateSystem(latitude, longitude);
                    break;
            }
        }


        private void bTranslation_Click(object sender, EventArgs e)
        {
           
            
        }
        private void bConnectGPS_Click(object sender, EventArgs e)
        {
            if (bConnectGPS.BackColor == Color.Red && !m_gps_device_holder.IsConnected)
            {
                PortGPSConnect();
            }
            else
            {
                m_gps_device_holder.Close();
            }
        }
        public void PortGPSConnect()
        {
            Parity parity = new Parity();
            switch (structParamsPort.Parity)
            {
                case "None":
                    parity = Parity.None;
                    break;
                case "Odd":
                    parity = Parity.Odd;
                    break;
                case "Even":
                    parity = Parity.Even;
                    break;
                case "Mark":
                    parity = Parity.Mark;
                    break;
                case "Space":
                    parity = Parity.Space;
                    break;
                default:
                    break;
            }
            StopBits stopBits = new StopBits();
            switch (structParamsPort.StopBits)
            {
                case "None":
                    stopBits = StopBits.None;
                    break;
                case "One":
                    stopBits = StopBits.One;
                    break;
                case "Two":
                    stopBits = StopBits.Two;
                    break;
                case "OnePointFive":
                    stopBits = StopBits.OnePointFive;
                    break;
                default:
                    break;
            }
            //creating serial port for device
            //var serial_port = new SerialPort(structParamsPort.PortName, structParamsPort.BaudRate, parity, structParamsPort.DataBits, stopBits);
            serial_port = null;
            serial_port = new SerialPort(structParamsPort.PortName, structParamsPort.BaudRate, parity, structParamsPort.DataBits, stopBits);
            var serial_port_data_source = new SerialPortDataSource(serial_port) { ReadTimeout = 1000 };
            try
            {
                m_gps_device_holder.Open(serial_port_data_source);
                bConnectGPS.BackColor = Color.Green;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                bConnectGPS.BackColor = Color.Red;
                //Console.WriteLine(ex.Message);
            }
        }
        private void timerLedReadBytes_Tick(object sender, EventArgs e)
        {
            //if (m_gps_device_holder.IsConnected)
            //    ledReadGPS1.Value = true;
            //else ledReadGPS1.Value = false;

            try
            {
                //ledReadKmp1.Value = true;
                //mestopolozhenie.compass1_azimuth = double.Parse(Compas1._angles.head.ToString());
                //tbK1Azimuth.Text = mestopolozhenie.compass1_azimuth.ToString();
                //tbK1Kren.Text = Compas1._angles.roll.ToString();
                //tbK1Tangazh.Text = Compas1._angles.pitch.ToString();
                //Thread.Sleep(5);
                //ledReadKmp1.Value = false;
                //ledReadKmp2.Value = true;
                //mestopolozhenie.compass2_azimuth = double.Parse(Compas2._angles.head.ToString());
                //tbK2Azimuth.Text = mestopolozhenie.compass2_azimuth.ToString();
                //tbK2Kren.Text = Compas2._angles.roll.ToString();
                //tbK2Tangazh.Text = Compas2._angles.pitch.ToString();
                //Thread.Sleep(5);
                //ledReadKmp2.Value = false;
                //calculate_angle_to_north(mestopolozhenie);

                ShowDirection(complexPlotLPA, mestopolozhenie.ARD1_azimuth);
                ShowDirection(complexPlotPU, mestopolozhenie.ARD2_azimuth);
                ShowDirection(complexPlotSP, mestopolozhenie.ARD3_azimuth);
                ShowDirection(complexPlotLPA1_3, mestopolozhenie.LPA_1_3);
                ShowDirection(complexPlotLPA2_4, mestopolozhenie.LPA_2_4);
                tbLPA.Text = mestopolozhenie.ARD1_azimuth.ToString();
                tbPU.Text = mestopolozhenie.ARD2_azimuth.ToString();
                tbSP.Text = mestopolozhenie.ARD3_azimuth.ToString();
                
            }
            catch (Exception ex) { }
        }
        int zapolnenie()
        {
            try
            {
                //mestopolozhenie.ARD1_azimuth = (int)Math.Round(iniRW.read_ARD1_azimuth());
                mestopolozhenie.ARD1_Radant = (int)Math.Round(iniRW.read_ARD1_Radant ()); 
                //mestopolozhenie.ARD1_need_azimuth = (int)Math.Round(iniRW.read_ARD1_need_azimuth());
                mestopolozhenie.ARD1_comN = iniRW.read_ARD1_ComN();
                //mestopolozhenie.ARD2_azimuth = (int)Math.Round(iniRW.read_ARD2_azimuth());
                mestopolozhenie.ARD2_Radant = (int)Math.Round(iniRW.read_ARD2_Radant());
                //mestopolozhenie.ARD2_need_azimuth = (int)Math.Round(iniRW.read_ARD2_need_azimuth());
                mestopolozhenie.ARD2_comN = iniRW.read_ARD2_ComN();
                //mestopolozhenie.ARD3_azimuth = (int)Math.Round(iniRW.read_ARD3_azimuth());
                mestopolozhenie.ARD3_Radant = (int)Math.Round(iniRW.read_ARD3_Radant());
                //mestopolozhenie.ARD3_need_azimuth = (int)Math.Round(iniRW.read_ARD3_need_azimuth());
                mestopolozhenie.ARD3_comN = iniRW.read_ARD3_ComN();
                mestopolozhenie.compass1_azimuth = (int)Math.Round(iniRW.read_compass1_azimuth());
                mestopolozhenie.compass1_correction = iniRW.read_compass1_correction();
                //mestopolozhenie.compass1_declination = iniRW.read_compass1_declination();
                //mestopolozhenie.compass1_final = (int)Math.Round(iniRW.read_compass1_final());
                mestopolozhenie.compass1_kren = iniRW.read_compass1_kren();
                mestopolozhenie.compass1_tangazh = iniRW.read_compass1_tangazh();
                mestopolozhenie.compas1_comN = iniRW.read_Compass1_ComN();
                mestopolozhenie.compass2_azimuth = (int)Math.Round(iniRW.read_compass2_azimuth());
                mestopolozhenie.compass2_correction = iniRW.read_compass2_correction();
                //mestopolozhenie.compass2_declination = iniRW.read_compass2_declination();
                //mestopolozhenie.compass2_final = (int)Math.Round(iniRW.read_compass2_final());
                mestopolozhenie.compass2_kren = iniRW.read_compass2_kren();
                mestopolozhenie.compass2_tangazh = iniRW.read_compass2_tangazh();
                mestopolozhenie.compas2_comN = iniRW.read_Compass2_ComN();
                mestopolozhenie.sputnic_count = iniRW.get_amount();
                calculate_angle_to_north_Paint(mestopolozhenie);
                //tbLPA.Text = mestopolozhenie.ARD1_azimuth.ToString();
                //tbPU.Text = mestopolozhenie.ARD2_azimuth.ToString();
                //tbSP.Text = mestopolozhenie.ARD3_azimuth.ToString();
                //tbK1Azimuth.Text = mestopolozhenie.compass1_final.ToString();
                //tbK1Kren.Text = mestopolozhenie.compass1_kren.ToString();
                //tbK1Tangazh.Text = mestopolozhenie.compass1_tangazh.ToString();
                //tbK2Azimuth.Text = mestopolozhenie.compass2_final.ToString();
                //tbK2Kren.Text = mestopolozhenie.compass2_kren.ToString();
                //tbK2Tangazh.Text = mestopolozhenie.compass2_tangazh.ToString();
                col = iniRW.get_color_lpa();
                colors.LPA = Color.FromArgb(col[0], col[1], col[2], col[3]);
                col = iniRW.get_color_lpa1_3();
                colors.LPA1_3 = Color.FromArgb(col[0], col[1], col[2], col[3]);
                col = iniRW.get_color_lpa2_4();
                colors.LPA2_4 = Color.FromArgb(col[0], col[1], col[2], col[3]);
                col = iniRW.get_color_SP();
                colors.SP = Color.FromArgb(col[0], col[1], col[2], col[3]);
                col = iniRW.get_color_PU();
                colors.PU = Color.FromArgb(col[0], col[1], col[2], col[3]);
                change_colors();
                return 1;
            }
            catch (Exception ex) { return 0; }
        }
        void change_colors()
        {
            lLPA.ForeColor = colors.LPA;
            lLPA1_3.ForeColor = colors.LPA1_3;
            lLPA2_4.ForeColor = colors.LPA2_4;
            lPU.ForeColor = colors.PU;
            lSP.ForeColor = colors.SP;
            complexPlotLPA.LineColor = colors.LPA;
            complexPlotLPA1_3.LineColor = colors.LPA1_3;
            complexPlotLPA2_4.LineColor = colors.LPA2_4;
            complexPlotPU.LineColor = colors.PU;
            complexPlotSP.LineColor = colors.SP;
        }
        int sohranenie_to_ini()
        {
            try
            {
                //iniRW.write_ARD1_azimuth(mestopolozhenie.ARD1_azimuth);
                iniRW.write_ARD1_Radant(mestopolozhenie.ARD1_Radant);
                iniRW.write_ARD1_need_azimuth(mestopolozhenie.ARD1_need_azimuth);
                //iniRW.write_ARD2_azimuth(mestopolozhenie.ARD2_azimuth);
                iniRW.write_ARD2_Radant(mestopolozhenie.ARD2_Radant);
                iniRW.write_ARD2_need_azimuth(mestopolozhenie.ARD2_need_azimuth);
                //iniRW.write_ARD3_azimuth(mestopolozhenie.ARD3_azimuth);
                iniRW.write_ARD3_Radant(mestopolozhenie.ARD3_Radant);
                iniRW.write_ARD3_need_azimuth(mestopolozhenie.ARD3_need_azimuth);
                iniRW.write_compass1_azimuth(mestopolozhenie.compass1_azimuth);
                iniRW.write_compass1_correction(mestopolozhenie.compass1_correction);
                //iniRW.write_compass1_declination(mestopolozhenie.compass1_declination);
                //iniRW.write_compass1_final(mestopolozhenie.compass1_final);
                iniRW.write_compass1_kren(mestopolozhenie.compass1_kren);
                iniRW.write_compass1_tangazh(mestopolozhenie.compass1_tangazh);
                iniRW.write_compass2_azimuth(mestopolozhenie.compass2_azimuth);
                iniRW.write_compass2_correction(mestopolozhenie.compass2_correction);
                //iniRW.write_compass2_declination(mestopolozhenie.compass2_declination);
                //iniRW.write_compass2_final(mestopolozhenie.compass2_final);
                iniRW.write_compass2_kren(mestopolozhenie.compass2_kren);
                iniRW.write_compass2_tangazh(mestopolozhenie.compass2_tangazh);
                iniRW.write_Color_lpa(new int[] { colors.LPA.A, colors.LPA.R, colors.LPA.G, colors.LPA.B });
                iniRW.write_Color_lpa1_3(new int[] { colors.LPA1_3.A, colors.LPA1_3.R, colors.LPA1_3.G, colors.LPA1_3.B });
                iniRW.write_Color_lpa2_4(new int[] { colors.LPA2_4.A, colors.LPA2_4.R, colors.LPA2_4.G, colors.LPA2_4.B });
                iniRW.write_Color_SP(new int[] { colors.SP.A, colors.SP.R, colors.SP.G, colors.SP.B });
                iniRW.write_Color_PU(new int[] { colors.PU.A, colors.PU.R, colors.PU.G, colors.PU.B });
                //iniRW.write_Color_lpa1_3(colors.LPA1_3.ToString());
                //iniRW.write_Color_lpa2_4(colors.LPA2_4.ToString());
                //iniRW.write_Color_PU(colors.PU.ToString());
                //iniRW.write_Color_SP(colors.SP.ToString());
                
                return 1;
            }
            catch (Exception ex) { return 0; }
        }
        private void OnGetAngleReceivedLPA(double dAngle, double dElevation)
        {
            try
            {
                if (InvokeRequired)
                {
                    BeginInvoke((MethodInvoker)(() => OnGetAngleReceivedLPA(dAngle, dElevation)));
                    return;
                }
                mestopolozhenie.ARD1_Radant = (int)Math.Round(dAngle);
                calculate_angle_to_north_Paint(mestopolozhenie);
            }
            catch (Exception ex) {ARD_LPA.ClosePort(); }// mestopolozhenie.ARD1_IsOpen = false; bConnectLPA.BackColor = Color.Red; ledWriteLPA.Value = false; ledReadLPA.Value = false; }
        }
        private void OnGetAngleReceivedPU(double dAngle, double dElevation)
        {try
        {
            if (InvokeRequired)
            {
                BeginInvoke((MethodInvoker)(() => OnGetAngleReceivedPU(dAngle, dElevation)));
                return;
            }
            mestopolozhenie.ARD2_Radant = (int)Math.Round(dAngle);
            calculate_angle_to_north_Paint(mestopolozhenie);
        }
        catch (Exception ex) { ARD_PU.ClosePort(); }
        }
        private void OnGetAngleReceivedSP(double dAngle, double dElevation)
        {
            try
            {
            if (InvokeRequired)
            {
                BeginInvoke((MethodInvoker)(() => OnGetAngleReceivedSP(dAngle, dElevation)));
                return;
            }
            mestopolozhenie.ARD3_Radant = (int)Math.Round(dAngle);
            calculate_angle_to_north_Paint(mestopolozhenie);
            }
            catch (Exception ex) { ARD_SP.ClosePort(); }
        }
        private void SetAngleReceivedLPA(double dAngle, double dElevation)
        {
            try
            {

                if (InvokeRequired)
                {
                    BeginInvoke((MethodInvoker)(() => SetAngleReceivedLPA(dAngle, dElevation)));
                    return;
                }
                mestopolozhenie.ARD1_need_azimuth = (int)dAngle;
                ARD_LPA.SendSetAngle(dAngle, dElevation, true);

            }
            catch (Exception ex) { ARD_LPA.ClosePort(); }
        }
        private void SetAngleReceivedPU(double dAngle, double dElevation)
        {
            try
            {
                if (InvokeRequired)
                {
                    BeginInvoke((MethodInvoker)(() => SetAngleReceivedPU(dAngle, dElevation)));
                    return;
                }
                mestopolozhenie.ARD2_need_azimuth = (int)Math.Round(dAngle);
                ARD_PU.SendSetAngle(dAngle, dElevation, true);
            }
            catch (Exception ex) { ARD_PU.ClosePort(); }
        }
        private void SetAngleReceivedSP(double dAngle, double dElevation)
        {
            try
            {
                if (InvokeRequired)
                {
                    BeginInvoke((MethodInvoker)(() => SetAngleReceivedSP(dAngle, dElevation)));
                    return;
                }
                mestopolozhenie.ARD2_need_azimuth = (int)Math.Round(dAngle);
                ARD_SP.SendSetAngle(dAngle, dElevation, true);
            }
            catch (Exception ex) { ARD_SP.ClosePort(); }
        }
        void calculate_angle_to_north_Paint(Mestopolozhenie mestopolozhenie)
        {
            if (InvokeRequired)
            {
                BeginInvoke((MethodInvoker)(() => calculate_angle_to_north_Paint(mestopolozhenie)));
                return;
            }

            mestopolozhenie.compass1_final = (int)Math.Round(mestopolozhenie.compass1_azimuth - mestopolozhenie.compass1_correction);
            mestopolozhenie.compass2_final = (int)Math.Round(mestopolozhenie.compass2_azimuth - mestopolozhenie.compass2_correction);
            mestopolozhenie.ARD1_azimuth = mestopolozhenie.ARD1_Radant + mestopolozhenie.compass1_final+mestopolozhenie.ARD2_Radant;
            mestopolozhenie.ARD2_azimuth = mestopolozhenie.ARD2_Radant + mestopolozhenie.compass1_final;
            mestopolozhenie.ARD3_azimuth = mestopolozhenie.ARD3_Radant + mestopolozhenie.compass1_final;

            if (mestopolozhenie.ARD1_Radant > 359.99) { mestopolozhenie.ARD1_Radant -= 360; }
            if (mestopolozhenie.ARD2_Radant > 359.99) { mestopolozhenie.ARD2_Radant -= 360; }
            if (mestopolozhenie.ARD3_Radant > 359.99) { mestopolozhenie.ARD3_Radant -= 360; }
            if (mestopolozhenie.ARD1_azimuth > 359.99) { mestopolozhenie.ARD1_azimuth -= 360; }
            if (mestopolozhenie.ARD2_azimuth > 359.99) { mestopolozhenie.ARD2_azimuth -= 360; }
            if (mestopolozhenie.ARD3_azimuth > 359.99) { mestopolozhenie.ARD3_azimuth -= 360; }
            if (mestopolozhenie.compass1_final > 359.99) { mestopolozhenie.compass1_final -= 360; }
            if (mestopolozhenie.compass2_final > 359.99) { mestopolozhenie.compass2_final -= 360; }
            if (mestopolozhenie.ARD1_Radant < 0) { mestopolozhenie.ARD1_Radant += 360; }
            if (mestopolozhenie.ARD2_Radant < 0) { mestopolozhenie.ARD2_Radant += 360; }
            if (mestopolozhenie.ARD3_Radant < 0) { mestopolozhenie.ARD3_Radant += 360; }
            if (mestopolozhenie.ARD1_azimuth > 359.99) { mestopolozhenie.ARD1_azimuth -= 360; }
            if (mestopolozhenie.ARD2_azimuth > 359.99) { mestopolozhenie.ARD2_azimuth -= 360; }
            if (mestopolozhenie.ARD3_azimuth > 359.99) { mestopolozhenie.ARD3_azimuth -= 360; }
            if (mestopolozhenie.compass1_final < 0) { mestopolozhenie.compass1_final += 360; }
            if (mestopolozhenie.compass2_final < 0) { mestopolozhenie.compass2_final += 360; }

            ShowDirection(complexPlotLPA, mestopolozhenie.ARD1_azimuth);
            ShowDirection(complexPlotPU, mestopolozhenie.ARD2_azimuth);
            ShowDirection(complexPlotSP, mestopolozhenie.ARD3_azimuth);
            tbK1Azimuth.Text = mestopolozhenie.compass1_final.ToString();
            tbK2Azimuth.Text = mestopolozhenie.compass2_final.ToString();
            tbLPA.Text = mestopolozhenie.ARD1_azimuth.ToString();
            tbPU.Text = mestopolozhenie.ARD2_azimuth.ToString();
            tbSP.Text = mestopolozhenie.ARD3_azimuth.ToString();
        }
        private void nudLPA_ValueChanged(object sender, EventArgs e)
        {
            try
            {
            double angl;
            double.TryParse(nudLPA.Value.ToString(), out angl);
            mestopolozhenie.ARD1_need_azimuth = (int)Math.Round(angl);//- mestopolozhenie.ARD2_azimuth;//-mestopolozhenie.compass1_final
            angl = mestopolozhenie.ARD1_need_azimuth - mestopolozhenie.ARD2_azimuth;
            if (angl < 0) angl += 360;
            ARD_LPA.SendSetAngle(angl, 0, true);
            }
            catch (Exception ex) { /*MessageBox.Show("Не  могу повернуть");*/ }
        }
        private void nudPU_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                double angl;
                double.TryParse(nudPU.Value.ToString(), out angl);
                mestopolozhenie.ARD2_need_azimuth = (int)Math.Round(angl) - mestopolozhenie.compass1_final;
                if (mestopolozhenie.ARD2_need_azimuth < 0) mestopolozhenie.ARD2_need_azimuth += 360;
                ARD_PU.SendSetAngle(mestopolozhenie.ARD2_need_azimuth, 0, true);
            }
            catch (Exception ex) {  /*MessageBox.Show("Не  могу повернуть");*/  }


        }
        private void nudSP_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                // mestopolozhenie.ARD2_need_azimuth = (double)numericUpDownARD2.Value;
                SetAngleReceivedSP((double)nudSP.Value, 0.0);

            }
            catch (Exception ex) { /*MessageBox.Show("Не  могу повернуть");*/  }
            ledReadSP.Value = true;
            timer = new System.Threading.Timer(TimeStepLedReadSP, null, TimePause, 0);

        }
        private void nudLPA1_3_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                int.TryParse(nudLPA1_3.Value.ToString(), out mestopolozhenie.LPA_1_3);
                ShowDirection(complexPlotLPA1_3, mestopolozhenie.LPA_1_3);
            }
            catch (Exception ex) { }
        }
        private void nudLPA2_4_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                int.TryParse(nudLPA2_4.Value.ToString(), out mestopolozhenie.LPA_2_4);
                ShowDirection(complexPlotLPA2_4, mestopolozhenie.LPA_2_4);
            }
            catch (Exception ex) { }

        }

        private void ShowInfoTb(TextBox Tb, Double dValue)
        {
            if (Tb.InvokeRequired)
            {
                Tb.Invoke((MethodInvoker)(delegate()
                {
                    Tb.Text = dValue.ToString();

                }));

            }

            else
            {
                Tb.Text = dValue.ToString();
            }
        }
        private void ShowDirection(ComplexPlot complexPlot, double dAngle)
        {

            double dShiftAngleR = ((double)(dAngle) * Math.PI) / 180;


            complexPlot.PlotComplex(GeneratePlotData(dShiftAngleR, 4));
        }
        private static ComplexDouble[] GeneratePlotData(Double dAngle, Double dRadius)
        {
            ComplexDouble[] complexData = new ComplexDouble[2];

            complexData[0].Real = 0.3 * dRadius * Math.Sin(dAngle);
            complexData[0].Imaginary = 0.3 * dRadius * Math.Cos(dAngle);

            complexData[1].Imaginary = dRadius * Math.Cos(dAngle);
            complexData[1].Real = dRadius * Math.Sin(dAngle);

            return complexData;
        }
        // complexGraphSphere.PlotAreaImage = (rotateImage(bmp2, alpha));
        private Bitmap rotateImage(Bitmap input, float angle)
        {
            Bitmap result = new Bitmap(input.Width, input.Height);
            Graphics g = Graphics.FromImage(result);
            g.TranslateTransform((float)input.Width / 2, (float)input.Height / 2);
            g.RotateTransform(angle);
            g.TranslateTransform(-(float)input.Width / 2, -(float)input.Height / 2);
            g.DrawImage(input, new Point(0, 0));
            return result;
        }
        private void bSetLPA_Click(object sender, EventArgs e)
        {
            try
            {
                double angl;
                double.TryParse(nudLPA.Value.ToString(), out angl);
                mestopolozhenie.ARD1_need_azimuth = (int)Math.Round(angl);//- mestopolozhenie.ARD2_azimuth;//-mestopolozhenie.compass1_final
                angl = mestopolozhenie.ARD1_need_azimuth - mestopolozhenie.ARD2_azimuth;
                if (angl < 0) angl += 360;
                ARD_LPA.SendSetAngle(angl, 0, true);
            }
            catch (Exception ex) { /*MessageBox.Show("Не  могу повернуть");*/  }
        }
        private void bSetPU_Click(object sender, EventArgs e)
        {
            try
            {
                double angl;
                double.TryParse(nudPU.Value.ToString(), out angl);
                mestopolozhenie.ARD2_need_azimuth = (int)Math.Round(angl) - mestopolozhenie.compass1_final;
                if (mestopolozhenie.ARD2_need_azimuth < 0) mestopolozhenie.ARD2_need_azimuth += 360;
                ARD_PU.SendSetAngle(mestopolozhenie.ARD2_need_azimuth, 0, true);
            }
            catch (Exception ex) {  /*MessageBox.Show("Не  могу повернуть");*/  }
        }
        private void bSetSP_Click(object sender, EventArgs e)
        {
            try
            {
                double angl;
                double.TryParse(nudSP.Value.ToString(), out angl);
                mestopolozhenie.ARD3_need_azimuth = (int)Math.Round(angl) - mestopolozhenie.compass1_final;
                if (mestopolozhenie.ARD3_need_azimuth < 0) mestopolozhenie.ARD3_need_azimuth += 360;
                ARD_SP.SendSetAngle(mestopolozhenie.ARD2_need_azimuth, 0, true);
            }
            catch (Exception ex) {  /*MessageBox.Show("Не  могу повернуть");*/  }
        }
        private void bStopLPA_Click(object sender, EventArgs e)
        {
            try
            {
                ARD_LPA.SendStop();
            }
            catch (Exception ex) {}// ARD1.ClosePort(); mestopolozhenie.ARD1_IsOpen = false; bConnectLPA.BackColor = Color.Red; ledReadLPA.Value = false; ledWriteLPA.Value = false; }
            //ledWriteLPA.Value = true;
            //timer = new System.Threading.Timer(TimeStepLedWriteLPA, null, TimePause, 0);
        }
        private void bStopPU_Click(object sender, EventArgs e)
        {
            try
            {
                ARD_PU.SendStop();
            }
            catch (Exception ex) { }//ARDPU.ClosePort(); mestopolozhenie.ARD2_IsOpen = false; bConnectPU.BackColor = Color.Red; ledReadPU.Value = false; ledWritePU.Value = false; }
            //ledWritePU.Value = true;
            //timer = new System.Threading.Timer(TimeStepLedWritePU, null, TimePause, 0);
        }
        private void bStopSP_Click(object sender, EventArgs e)
        {
            try
            {
                ARD_SP.SendStop();
            }
            catch (Exception ex) {}// ARDSP.ClosePort(); mestopolozhenie.ARD3_IsOpen = false; bConnectSP.BackColor = Color.Red; ledReadSP.Value = false; ledWriteSP.Value = false; }
            //ledWriteSP.Value = true;
            //timer = new System.Threading.Timer(TimeStepLedWriteSP, null, TimePause, 0);
        }
        private void InitConnectionARD1()
        {
            if (ARD_LPA != null)
                ARD_LPA = null;
            ARD_LPA = new ComARD();
            ARD_LPA.OnGetAngleReceived += new ComARD.GetAngleReceivedEventHandler(OnGetAngleReceivedLPA);
            ARD_LPA.OnSetAngleReceived += new ComARD.SetAngleReceivedEventHandler(SetAngleReceivedLPA);
            ARD_LPA.OnConnectPort += new ComARD.ConnectEventHandler(ShowConnectARD);
            ARD_LPA.OnDisconnectPort += new ComARD.ConnectEventHandler(ShowDisconnectARD);
            ARD_LPA.OnReadByte += new ComARD.ByteEventHandler(ShowReadByteARD);
            ARD_LPA.OnWriteByte += new ComARD.ByteEventHandler(ShowWriteByteARD);
            ARD_LPA.OpenPort(mestopolozhenie.ARD1_comN, 9600, Parity.None, 8, StopBits.One);
            Thread.Sleep(100);
            ARD_LPA.SendGetAngle();
        }
        private void InitConnectionARD2()
        {
            if (ARD_PU != null)
                ARD_PU = null;
            ARD_PU = new ComARD();
            ARD_PU.OnGetAngleReceived += new ComARD.GetAngleReceivedEventHandler(OnGetAngleReceivedPU);
            ARD_PU.OnSetAngleReceived += new ComARD.SetAngleReceivedEventHandler(SetAngleReceivedPU);
            ARD_PU.OnConnectPort += new ComARD.ConnectEventHandler(ShowConnectPU);
            ARD_PU.OnDisconnectPort += new ComARD.ConnectEventHandler(ShowDisconnectPU);
            ARD_PU.OnReadByte += new ComARD.ByteEventHandler(ShowReadBytePU);
            ARD_PU.OnWriteByte += new ComARD.ByteEventHandler(ShowWriteBytePU);
            ARD_PU.OpenPort(mestopolozhenie.ARD2_comN, 9600, Parity.None, 8, StopBits.One);
            Thread.Sleep(100);
            ARD_PU.SendGetAngle();
        }
        private void InitConnectionARD3()
        {
            if (ARD_SP != null)
                ARD_SP = null;
            ARD_SP = new ComARD();
            ARD_SP.OnGetAngleReceived += new ComARD.GetAngleReceivedEventHandler(OnGetAngleReceivedSP);
            ARD_SP.OnSetAngleReceived += new ComARD.SetAngleReceivedEventHandler(SetAngleReceivedSP);
            ARD_SP.OnConnectPort += new ComARD.ConnectEventHandler(ShowConnectSP);
            ARD_SP.OnDisconnectPort += new ComARD.ConnectEventHandler(ShowDisconnectSP);
            ARD_SP.OnReadByte += new ComARD.ByteEventHandler(ShowReadByteSP);
            ARD_SP.OnWriteByte += new ComARD.ByteEventHandler(ShowWriteByteSP);
            ARD_SP.OpenPort(mestopolozhenie.ARD3_comN, 9600, Parity.None, 8, StopBits.One);
            Thread.Sleep(100);
            ARD_SP.SendGetAngle();
        }
        private void ShowReadByteARD(byte[] bData)
        {
            try
            {
                if (ledReadLPA.InvokeRequired)
                {
                    ledReadLPA.Invoke((MethodInvoker)(delegate()
                    {
                        ledReadLPA.Value = true;
                        tmReadByteARD = new System.Threading.Timer(TimeReadByteARD, null, TimePause, 0);
                    }));

                }
                else
                {
                    ledReadLPA.Value = true;
                    tmReadByteARD = new System.Threading.Timer(TimeReadByteARD, null, TimePause, 0);
                }
            }
            catch (SystemException)
            { }

        }        
        private void ShowReadByteSP(byte[] bData)
        {
            try
            {
                if (ledReadSP.InvokeRequired)
                {
                    ledReadSP.Invoke((MethodInvoker)(delegate()
                    {
                        ledReadSP.Value = true;
                        tmReadByteSP = new System.Threading.Timer(TimeReadByteSP, null, TimePause, 0);
                    }));

                }
                else
                {
                    ledReadSP.Value = true;
                    tmReadByteSP = new System.Threading.Timer(TimeReadByteSP, null, TimePause, 0);
                }
            }
            catch (SystemException)
            { }
        }
        private void ShowReadBytePU(byte[] bData)
        {
            try
            {
                if (ledReadPU.InvokeRequired)
                {
                    ledReadPU.Invoke((MethodInvoker)(delegate()
                    {
                        ledReadPU.Value = true;
                        tmReadBytePU = new System.Threading.Timer(TimeReadBytePU, null, TimePause, 0);
                    }));

                }
                else
                {
                    ledReadPU.Value = true;
                    tmReadBytePU = new System.Threading.Timer(TimeReadBytePU, null, TimePause, 0);
                }
            }
            catch (SystemException)
            { }
        }
        private void ShowWriteByteARD(byte[] bData)
        {
            if (ledWriteLPA.InvokeRequired)
            {
                ledWriteLPA.Invoke((MethodInvoker)(delegate()
                {
                    ledWriteLPA.Value = true;
                    tmWriteByteARD = new System.Threading.Timer(TimeWriteByteARD, null, TimePause, 0);
                }));

            }
            else
            {
                ledWriteLPA.Value = true;
                tmWriteByteARD = new System.Threading.Timer(TimeWriteByteARD, null, TimePause, 0);
            }
        }
        private void ShowWriteBytePU(byte[] bData)
        {
            if (ledWritePU.InvokeRequired)
            {
                ledWritePU.Invoke((MethodInvoker)(delegate()
                {
                    ledWritePU.Value = true;
                    tmWriteBytePU = new System.Threading.Timer(TimeWriteBytePU, null, TimePause, 0);
                }));

            }
            else
            {
                ledWritePU.Value = true;
                tmWriteBytePU = new System.Threading.Timer(TimeWriteBytePU, null, TimePause, 0);
            }
        }
        private void ShowWriteByteSP(byte[] bData)
        {
            if (ledWriteSP.InvokeRequired)
            {
                ledWriteSP.Invoke((MethodInvoker)(delegate()
                {
                    ledWriteSP.Value = true;
                    tmWriteByteSP = new System.Threading.Timer(TimeWriteByteSP, null, TimePause, 0);
                }));

            }
            else
            {
                ledWriteSP.Value = true;
                tmWriteByteSP = new System.Threading.Timer(TimeWriteByteSP, null, TimePause, 0);
            }
        }
        private void TimeWriteByteARD(object o)
        {
            if (ledWriteLPA.InvokeRequired)
            {
                ledWriteLPA.Invoke((MethodInvoker)(delegate()
                {
                    ledWriteLPA.Value = false;
                    tmWriteByteARD.Dispose();
                }));
            }
            else
            {
                ledWriteLPA.Value =false;
                tmWriteByteARD.Dispose();
            }
        }
        private void TimeWriteBytePU(object o)
        {
            if (ledWritePU.InvokeRequired)
            {
                ledWritePU.Invoke((MethodInvoker)(delegate()
                {
                    ledWritePU.Value = false;
                    tmWriteBytePU.Dispose();
                }));
            }
            else
            {
                ledWritePU.Value = false;
                tmWriteBytePU.Dispose();
            }
        }
        private void TimeWriteByteSP(object o)
        {
            if (ledWriteSP.InvokeRequired)
            {
                ledWriteSP.Invoke((MethodInvoker)(delegate()
                {
                    ledWriteSP.Value = false;
                    tmWriteByteSP.Dispose();
                }));
            }
            else
            {
                ledWriteSP.Value = false;
                tmWriteByteSP.Dispose();
            }
        }

        private void TimeReadByteARD(object o)
        {
            if (ledReadLPA.InvokeRequired)
            {
                ledReadLPA.Invoke((MethodInvoker)(delegate()
                {
                    ledReadLPA.Value =false;
                    tmReadByteARD.Dispose();
                }));
            }
            else
            {
                ledReadLPA.Value = false;
                tmReadByteARD.Dispose();
            }
        }
        private void TimeReadBytePU(object o)
        {
            if (ledReadPU.InvokeRequired)
            {
                ledReadPU.Invoke((MethodInvoker)(delegate()
                {
                    ledReadPU.Value = false;
                    tmReadBytePU.Dispose();
                }));
            }
            else
            {
                ledReadPU.Value = false;
                tmReadBytePU.Dispose();
            }
        }
        private void TimeReadByteSP(object o)
        {
            if (ledReadSP.InvokeRequired)
            {
                ledReadSP.Invoke((MethodInvoker)(delegate()
                {
                    ledReadSP.Value = false;
                    tmReadByteSP.Dispose();
                }));
            }
            else
            {
                ledReadSP.Value = false;
                tmReadByteSP.Dispose();
            }
        }

        private void bConnectLPA_Click(object sender, EventArgs e)
        {
            if (bConnectLPA.BackColor == Color.Red)
            {
                InitConnectionARD1();
            }
            else
                if (ARD_LPA != null)
                {
                    ARD_LPA.SendStop();
                    ARD_LPA.ClosePort();
                }
        }
        private void bConnectPU_Click(object sender, EventArgs e)
        {
            if (bConnectPU.BackColor == Color.Red)
            {
                InitConnectionARD2();
            }
            else
                if (ARD_PU != null)
                {
                    ARD_PU.SendStop();
                    ARD_PU.ClosePort();
                }
        }
        private void bConnectSP_Click(object sender, EventArgs e)
        {
            if (bConnectSP.BackColor == Color.Red)
            {
                InitConnectionARD3();
            }
            else
                if (ARD_SP != null)
                {
                    ARD_SP.SendStop();
                    ARD_SP.ClosePort();
                }
        }
        private void bConnectKmp1_Click(object sender, EventArgs e)
        {
            if (mestopolozhenie.compass1_IsOpen == false)
            {
                try
                {
                    Compas1.OpenPort(mestopolozhenie.compas1_comN, 9600, Parity.None, 8, StopBits.One, TimePause_compass1);
                    mestopolozhenie.compass1_IsOpen = true;
                    bConnectKmp1.BackColor = Color.Green;
                }
                catch (Exception ex) { bConnectKmp1.BackColor = Color.Red; ledReadKmp1.Value = false; ledWriteKmp1.Value = false; }
            }
            else
            {
                Compas1.ClosePort();
                mestopolozhenie.compass1_IsOpen = false;
                bConnectKmp1.BackColor = Color.Red; ledReadKmp1.Value = false; ledWriteKmp1.Value = false;
            }     
        }
        private void bConnectKmp2_Click(object sender, EventArgs e)
        {
            if (mestopolozhenie.compass2_IsOpen == false)
            {
                try
                {
                    Compas2.OpenPort(mestopolozhenie.compas2_comN, 9600, Parity.None, 8, StopBits.One, TimePause_compass2);
                    mestopolozhenie.compass1_IsOpen = true;
                    bConnectKmp2.BackColor = Color.Green;
                }
                catch (Exception ex) { bConnectKmp2.BackColor = Color.Red; ledReadKmp2.Value = false; ledWriteKmp2.Value = false; }
            }
            else
            {
                Compas2.ClosePort();
                mestopolozhenie.compass1_IsOpen = false;
                bConnectKmp2.BackColor = Color.Red; ledReadKmp2.Value = false; ledWriteKmp2.Value = false;
            }     
        }
        private void Compas_OnMSG1(int type)
        {           
            switch (type)
            {
                case 0: break; 
                        MessageBox.Show("Усё добра прайшло(компас 1)"); break;
                case 1: MessageBox.Show("Ошибка при открытии порта(компас 1)"); break;
                case 2: MessageBox.Show("Ошибка при закрытии порта(компас 1)"); break;
                case 3: MessageBox.Show("Ошибка записи данных(компас 1)"); break;
                case 4:
                    {
                        Compas1.ClosePort();
                        mestopolozhenie.compass1_IsOpen = false;
                        if (bConnectKmp1.InvokeRequired)
                        {
                            bConnectKmp1.Invoke((MethodInvoker)(delegate()
                            {
                                bConnectKmp1.BackColor =Color.Red; 
                            }));
                        }
                        else
                        {
                            bConnectKmp1.BackColor = Color.Red;
                        }
                        //ledReadKmp1.Value = false; ledWriteKmp1.Value = false;
                        return;
                        break;
                        MessageBox.Show("Ошибка чтения данных(компас 1)"); 
                    }
                case 5:  MessageBox.Show("Ошибка декодирования ответа(компас 1)"); break;
                case 6:
                        mestopolozhenie.compass1_azimuth = (int)Compas1._angles.head;           
                       
                        calculate_angle_to_north_Paint(mestopolozhenie);
                        break; 
                        MessageBox.Show("Получены значения углов(компас 1)"); break;
                case 7: break; MessageBox.Show("Успех при вызове функции SetDeclination(компас 1)"); break;
                case 8: MessageBox.Show("Ошибка при установке магнитного склонения(компас 1)"); break;
                case 9: break; MessageBox.Show("Получено значение отклонения компаса функцией ReadDeclination(компас 1)"); break;
                case 10: break; MessageBox.Show("Начата калибровка компаса(компас 1)"); break;
                case 11: break; MessageBox.Show("Получена точка калибровки(компас 1)"); break;
                case 12: break; MessageBox.Show("Получена последняя точка калибровки(компас 1)"); break;
                case 13: MessageBox.Show("Ошибка при старте калибровки(компас 1)"); break;
                case 14: MessageBox.Show("Неверный код при попытке остановить калибровку(компас 1)"); break;
                case 15: break; MessageBox.Show("результаты калибровки после функции StopCalibritaion(компас 1)"); break;
                case 16: MessageBox.Show("Ошибка при остановке калибровки(компас 1)"); break;
                case 17: break; MessageBox.Show("Успех при сохранении калибровки(компас 1)"); break;
                case 18: MessageBox.Show("Не удалось сохранить калибровку(компас 1)"); break;
                case 19: break; MessageBox.Show("Успех при вызове функции SetBaudRate(компас 1)"); break;
                case 20: MessageBox.Show("Не удалось установить BaudRate(компас 1)"); break;
                case 21: break; MessageBox.Show("Успех при вызове функции SetAngleOutputMode(компас 1)"); break;
                case 22: MessageBox.Show("Не удалось установить режим(компас 1)"); break;
                case 23: break; MessageBox.Show("Успех при вызове функции SetAngleMountingMode(компас 1)"); break;
                case 24: MessageBox.Show("Не удалось установить поляризацию компаса(компас 1)"); break;
                case 25: break; MessageBox.Show("Успех при вызове функции GetAngleMountingMode(компас 1)"); break;
                case 26: MessageBox.Show("Не удалось запросить поляризацию(компас 1)"); break;
                case 27: break; MessageBox.Show("Успех при вызове функции GetAngleOutputMode(компас 1)"); break;
                case 28: MessageBox.Show("Не удалось установить режим(компас 1)"); break;
                default: MessageBox.Show("Неопознанная ошибка(компас 1)"); break;
            } 
            ledReadKmp1.Value = true;
            timer = new System.Threading.Timer(TimeStepLedReadKmp1, null, TimePause, 0);
        }
        private void Compas_OnMSG2(int type)
        {
            switch (type)
            {
                case 0: break; MessageBox.Show("Усё добра прайшло(компас 2)"); break;
                case 1: MessageBox.Show("Ошибка при открытии порта(компас 2)"); break;
                case 2: MessageBox.Show("Ошибка при закрытии порта(компас 2)"); break;
                case 3: MessageBox.Show("Ошибка записи данных(компас 2)"); break;
                case 4:
                    {   Compas2.ClosePort();
                        mestopolozhenie.compass2_IsOpen = false;
                        if (bConnectKmp2.InvokeRequired)
                        {
                            bConnectKmp2.Invoke((MethodInvoker)(delegate()
                            {
                                bConnectKmp2.BackColor = Color.Red;
                            }));
                        }
                        else
                        {
                            bConnectKmp2.BackColor = Color.Red;
                        }
                        //MessageBox.Show("Ошибка чтения данных(компас 2)"); 
                        return;
                        break;
                    }
                case 5: MessageBox.Show("Ошибка декодирования ответа(компас 2)"); break;
                case 6:
                    mestopolozhenie.compass2_azimuth = (int)Compas2._angles.head;//int.Parse(Compas1._angles.head.ToString());                    
                    //if (tbK2Azimuth.InvokeRequired)
                    //{
                    //    tbK2Azimuth.Invoke((MethodInvoker)(delegate()
                    //    {
                    //        tbK2Azimuth.Text = mestopolozhenie.compass2_final.ToString();
                    //    }));
                    //}
                    //else
                    //{
                    //    tbK2Azimuth.Text = mestopolozhenie.compass2_final.ToString();
                    //}
                    calculate_angle_to_north_Paint(mestopolozhenie);
                    break; MessageBox.Show("Получены значения углов(компас 2)"); break;
                case 7: break; MessageBox.Show("Успех при вызове функции SetDeclination(компас 2)"); break;
                case 8: MessageBox.Show("Ошибка при установке магнитного склонения(компас 2)"); break;
                case 9: break; MessageBox.Show("Получено значение отклонения компаса функцией ReadDeclination(компас 2)"); break;
                case 10: break; MessageBox.Show("Начата калибровка компаса(компас 2)"); break;
                case 11: break; MessageBox.Show("Получена точка калибровки(компас 2)"); break;
                case 12: break; MessageBox.Show("Получена последняя точка калибровки(компас 2)"); break;
                case 13: MessageBox.Show("Ошибка при старте калибровки(компас 2)"); break;
                case 14: MessageBox.Show("Неверный код при попытке остановить калибровку(компас 2)"); break;
                case 15: break; MessageBox.Show("результаты калибровки после функции StopCalibritaion(компас 2)"); break;
                case 16: MessageBox.Show("Ошибка при остановке калибровки(компас 2)"); break;
                case 17: break; MessageBox.Show("Успех при сохранении калибровки(компас 2)"); break;
                case 18: MessageBox.Show("Не удалось сохранить калибровку(компас 2)"); break;
                case 19: break; MessageBox.Show("Успех при вызове функции SetBaudRate(компас 1)"); break;
                case 20: MessageBox.Show("Не удалось установить BaudRate(компас 2)"); break;
                case 21: break; MessageBox.Show("Успех при вызове функции SetAngleOutputMode(компас 2)"); break;
                case 22: MessageBox.Show("Не удалось установить режим(компас 2)"); break;
                case 23: break; MessageBox.Show("Успех при вызове функции SetAngleMountingMode(компас 2)"); break;
                case 24: MessageBox.Show("Не удалось установить поляризацию компаса(компас 2)"); break;
                case 25: break; MessageBox.Show("Успех при вызове функции GetAngleMountingMode(компас 2)"); break;
                case 26: MessageBox.Show("Не удалось запросить поляризацию(компас 2)"); break;
                case 27: break; MessageBox.Show("Успех при вызове функции GetAngleOutputMode(компас 2)"); break;
                case 28: MessageBox.Show("Не удалось установить режим(компас 2)"); break;
                default: MessageBox.Show("Неопознанная ошибка(компас 2)"); break;
            }
            ledReadKmp2.Value = true;
            timer = new System.Threading.Timer(TimeStepLedReadKmp2, null, TimePause, 0);
        }
        private void TimeStepLedReadKmp1(object o)
        {
            if (ledReadKmp1.InvokeRequired)
            {
                ledReadKmp1.Invoke((MethodInvoker)(delegate()
                {
                    ledReadKmp1.Value = false;
                    timer.Dispose();
                }));

            }
            else
            {
                ledReadKmp1.Value = false;
                timer.Dispose();
            }

        }
        private void TimeStepLedWriteKmp1(object o)
        {
            if (ledWriteKmp1.InvokeRequired)
            {
                ledWriteKmp1.Invoke((MethodInvoker)(delegate()
                {
                    ledWriteKmp1.Value = false;
                    timer.Dispose();
                }));

            }
            else
            {
                ledWriteKmp1.Value = false;
                timer.Dispose();
            }

        }
        private void TimeStepLedReadKmp2(object o)
        {
            if (ledReadKmp2.InvokeRequired)
            {
                ledReadKmp2.Invoke((MethodInvoker)(delegate()
                {
                    ledReadKmp2.Value = false;
                    timer.Dispose();
                }));

            }
            else
            {
                ledReadKmp2.Value = false;
                timer.Dispose();
            }

        }
        private void TimeStepLedWriteKmp2(object o)
        {
            if (ledWriteKmp2.InvokeRequired)
            {
                ledWriteKmp2.Invoke((MethodInvoker)(delegate()
                {
                    ledWriteKmp2.Value = false;
                    timer.Dispose();
                }));

            }
            else
            {
                ledWriteKmp2.Value = false;
                timer.Dispose();
            }
        }
        private void TimeStepLedReadLPA(object o)
        {
            if (ledReadLPA.InvokeRequired)
            {
                ledReadLPA.Invoke((MethodInvoker)(delegate()
                {
                    ledReadLPA.Value = false;
                    timer.Dispose();
                }));
            }
            else
            {
                ledReadLPA.Value = false;
                timer.Dispose();
            }
        }
        //private void TimeStepLedWriteLPA(object o)
        //{
        //    if (ledWriteLPA.InvokeRequired)
        //    {
        //        ledWriteLPA.Invoke((MethodInvoker)(delegate()
        //        {
        //            ledWriteLPA.Value = false;
        //            timer.Dispose();
        //        }));
        //    }
        //    else
        //    {
        //        ledWriteLPA.Value = false;
        //        timer.Dispose();
        //    }
        //}
        private void TimeStepLedReadSP(object o)
        {
            if (ledReadSP.InvokeRequired)
            {
                ledReadSP.Invoke((MethodInvoker)(delegate()
                {
                    ledReadSP.Value = false;
                    timer.Dispose();
                }));
            }
            else
            {
                ledReadSP.Value = false;
                timer.Dispose();
            }
        }
        private void TimeStepLedWriteSP(object o)
        {
            if (ledWriteSP.InvokeRequired)
            {
                ledWriteSP.Invoke((MethodInvoker)(delegate()
                {
                    ledWriteSP.Value = false;
                    timer.Dispose();
                }));
            }
            else
            {
                ledWriteSP.Value = false;
                timer.Dispose();
            }
        }
        private void TimeStepLedReadPU(object o)
        {
            if (ledReadPU.InvokeRequired)
            {
                ledReadPU.Invoke((MethodInvoker)(delegate()
                {
                    ledReadPU.Value = false;
                    timer.Dispose();
                }));
            }
            else
            {
                ledReadPU.Value = false;
                timer.Dispose();
            }
        }
        private void TimeStepLedWritePU(object o)
        {
            if (ledWritePU.InvokeRequired)
            {
                ledWritePU.Invoke((MethodInvoker)(delegate()
                {
                    ledWritePU.Value = false;
                    timer.Dispose();
                }));
            }
            else
            {
                ledWritePU.Value = false;
                timer.Dispose();
            }
        }
        private void TimeStepLedReadGPS(object o)
        {
            if (ledReadGPS1.InvokeRequired)
            {
                ledReadGPS1.Invoke((MethodInvoker)(delegate()
                {
                    ledReadGPS1.Value = false;
                    timer.Dispose();
                }));
            }
            else
            {
                ledReadGPS1.Value = false;
                timer.Dispose();
            }
        }
        private void TimeStepLedWriteGPS(object o)
        {
            if (ledWriteGPS.InvokeRequired)
            {
                ledWriteGPS.Invoke((MethodInvoker)(delegate()
                {
                    ledWriteGPS.Value = false;
                    timer.Dispose();
                }));
            }
            else
            {
                ledWriteGPS.Value = false;
                timer.Dispose();
            }
        }
        private void lLPA_DoubleClick(object sender, EventArgs e)
        {
            if (colorDialog1.ShowDialog() == DialogResult.OK)
            {
             colors.LPA = colorDialog1.Color;
             change_colors();
            }
        }
        private void lPU_DoubleClick(object sender, EventArgs e)
        {
            if (colorDialog1.ShowDialog() == DialogResult.OK)
            {
                colors.PU = colorDialog1.Color;
                change_colors();
            }
        }
        private void lSP_DoubleClick(object sender, EventArgs e)
        {
            if (colorDialog1.ShowDialog() == DialogResult.OK)
            {
                colors.SP = colorDialog1.Color;
                change_colors();
            }
        }
        private void lLPA1_3_DoubleClick(object sender, EventArgs e)
        {
            if (colorDialog1.ShowDialog() == DialogResult.OK)
            {
                colors.LPA1_3 = colorDialog1.Color;
                change_colors();
            }
        }
        private void lLPA2_4_DoubleClick(object sender, EventArgs e)
        {
            if (colorDialog1.ShowDialog() == DialogResult.OK)
            {
                colors.LPA2_4 = colorDialog1.Color;
                change_colors();
            }
        }

        private void cbCoordinateSystem_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void cbSatSyst_SelectedIndexChanged(object sender, EventArgs e)
        {
            tbDate.Text = "";
            tbTime.Text = "";
            tbLat.Text = "";
            tbLon.Text = "";
            tbAlt.Text = "";
            tbCountSat.Text = "";
        }
    }    
}

