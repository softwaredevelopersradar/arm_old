﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinAp4
{
    public struct PointDouble
    {
        public byte TypeStation;
        public bool Accessory;
        public double X;
        public double Y;
        public double Frequency;
        public int Bearing;

        public PointDouble(byte typeStation, bool accessory, double x, double y, double frequency, int bearing)
        {
            this.TypeStation = typeStation;
            this.Accessory = accessory;
            this.X = x;
            this.Y = y;
            this.Bearing = bearing;
            this.Frequency = frequency;
        }

        public void setBearing(double frequency, int bearing)
        {
            Bearing = bearing;
            Frequency = frequency;
        }

        public void set(double x, double y, double frequency, int bearing)
        {
            X = x;
            Y = y;
            Bearing = bearing;
            Frequency = frequency;
        }

        public void toZero()
        {
            X = 0;
            Y = 0;
        }
    }

    public struct MessageTranslate
    {
        public string StatusStripLatLon;
        public string strIniFind;
        public string strError;

        public void MesTranslate()
        {
            this.StatusStripLatLon = "Широта = {0} | Долгота = {1}";
            this.strIniFind = "Файл Init.ini не найден!";
            this.strError = "Ошибка!";
        }

    }

    #region Параметры COM-порта
    /// <summary>
    /// Параметры COM-порта
    /// </summary>
    public class StructParamsPort
    {
        string portName;
        int baudRate;
        int dataBits;
        string parity;
        string stopBits;
        bool rtsEnable;
        bool dtrEnable;
        int readTimeout;
        int writeTimeout;

        public string PortName
        {
            get { return portName; }
            set { portName = value; }
        }

        public int BaudRate
        {
            get { return baudRate; }
            set { baudRate = value; }
        }

        public int DataBits
        {
            get { return dataBits; }
            set { dataBits = value; }
        }

        public string Parity
        {
            get { return parity; }
            set { parity = value; }
        }

        public string StopBits
        {
            get { return stopBits; }
            set { stopBits = value; }
        }

        public bool RtsEnable
        {
            get { return rtsEnable; }
            set { rtsEnable = value; }
        }

        public bool DtrEnable
        {
            get { return dtrEnable; }
            set { dtrEnable = value; }
        }

        public int ReadTimeout
        {
            get { return readTimeout; }
            set { readTimeout = value; }
        }

        public int WriteTimeout
        {
            get { return writeTimeout; }
            set { writeTimeout = value; }
        }
    }
    #endregion

}
