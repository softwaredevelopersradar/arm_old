﻿namespace WinAp4
{
    partial class FormAP4
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormAP4));
            this.mapContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.addStation = new System.Windows.Forms.ToolStripMenuItem();
            this.nudLPA2_4 = new System.Windows.Forms.NumericUpDown();
            this.nudLPA1_3 = new System.Windows.Forms.NumericUpDown();
            this.lLPA2_4 = new System.Windows.Forms.Label();
            this.lLPA1_3 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.tbAlt = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.cbCoordinateSystem = new System.Windows.Forms.ComboBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.cbSatSyst = new System.Windows.Forms.ComboBox();
            this.tbDate = new System.Windows.Forms.TextBox();
            this.lDate = new System.Windows.Forms.Label();
            this.tbTime = new System.Windows.Forms.TextBox();
            this.lTime = new System.Windows.Forms.Label();
            this.tbCountSat = new System.Windows.Forms.TextBox();
            this.tbLon = new System.Windows.Forms.TextBox();
            this.tbLat = new System.Windows.Forms.TextBox();
            this.lCountSat = new System.Windows.Forms.Label();
            this.lLon = new System.Windows.Forms.Label();
            this.lLAt = new System.Windows.Forms.Label();
            this.ledWriteKmp2 = new NationalInstruments.UI.WindowsForms.Led();
            this.ledReadKmp1 = new NationalInstruments.UI.WindowsForms.Led();
            this.ledWriteKmp1 = new NationalInstruments.UI.WindowsForms.Led();
            this.panel6 = new System.Windows.Forms.Panel();
            this.bConnectKmp1 = new System.Windows.Forms.Button();
            this.label18 = new System.Windows.Forms.Label();
            this.ledReadSP = new NationalInstruments.UI.WindowsForms.Led();
            this.ledWriteSP = new NationalInstruments.UI.WindowsForms.Led();
            this.panel5 = new System.Windows.Forms.Panel();
            this.bConnectSP = new System.Windows.Forms.Button();
            this.label17 = new System.Windows.Forms.Label();
            this.ledReadPU = new NationalInstruments.UI.WindowsForms.Led();
            this.ledWritePU = new NationalInstruments.UI.WindowsForms.Led();
            this.panel4 = new System.Windows.Forms.Panel();
            this.bConnectPU = new System.Windows.Forms.Button();
            this.label16 = new System.Windows.Forms.Label();
            this.ledReadLPA = new NationalInstruments.UI.WindowsForms.Led();
            this.ledWriteLPA = new NationalInstruments.UI.WindowsForms.Led();
            this.panel2 = new System.Windows.Forms.Panel();
            this.bConnectLPA = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.ledReadGPS1 = new NationalInstruments.UI.WindowsForms.Led();
            this.ledWriteGPS = new NationalInstruments.UI.WindowsForms.Led();
            this.panel3 = new System.Windows.Forms.Panel();
            this.bConnectGPS = new System.Windows.Forms.Button();
            this.lGPS = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.bStopSP = new System.Windows.Forms.Button();
            this.bStopPU = new System.Windows.Forms.Button();
            this.bSetSP = new System.Windows.Forms.Button();
            this.bSetPU = new System.Windows.Forms.Button();
            this.bStopLPA = new System.Windows.Forms.Button();
            this.bSetLPA = new System.Windows.Forms.Button();
            this.nudPU = new System.Windows.Forms.NumericUpDown();
            this.nudLPA = new System.Windows.Forms.NumericUpDown();
            this.tbPU = new System.Windows.Forms.TextBox();
            this.nudSP = new System.Windows.Forms.NumericUpDown();
            this.tbLPA = new System.Windows.Forms.TextBox();
            this.lPU = new System.Windows.Forms.Label();
            this.lLPA = new System.Windows.Forms.Label();
            this.lSP = new System.Windows.Forms.Label();
            this.tbSP = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tbK2Azimuth = new System.Windows.Forms.TextBox();
            this.lK2Azimuth = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tbK1Azimuth = new System.Windows.Forms.TextBox();
            this.lK1Azimuth = new System.Windows.Forms.Label();
            this.complexGraph = new NationalInstruments.UI.WindowsForms.ComplexGraph();
            this.complexPlotLPA = new NationalInstruments.UI.ComplexPlot();
            this.complexXAxis1 = new NationalInstruments.UI.ComplexXAxis();
            this.complexYAxis1 = new NationalInstruments.UI.ComplexYAxis();
            this.complexPlotPU = new NationalInstruments.UI.ComplexPlot();
            this.complexPlotSP = new NationalInstruments.UI.ComplexPlot();
            this.complexPlotLPA1_3 = new NationalInstruments.UI.ComplexPlot();
            this.complexPlotLPA2_4 = new NationalInstruments.UI.ComplexPlot();
            this.imageList2 = new System.Windows.Forms.ImageList(this.components);
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.ledReadKmp2 = new NationalInstruments.UI.WindowsForms.Led();
            this.bConnectKmp2 = new System.Windows.Forms.Button();
            this.timerLedReadBytes = new System.Windows.Forms.Timer(this.components);
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.label19 = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.pState = new System.Windows.Forms.Panel();
            this.pKmpPA = new System.Windows.Forms.Panel();
            this.pPU = new System.Windows.Forms.Panel();
            this.panel14 = new System.Windows.Forms.Panel();
            this.panel11 = new System.Windows.Forms.Panel();
            this.pKmpRR = new System.Windows.Forms.Panel();
            this.panel16 = new System.Windows.Forms.Panel();
            this.panel13 = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.panel10 = new System.Windows.Forms.Panel();
            this.pLPA = new System.Windows.Forms.Panel();
            this.pGPS = new System.Windows.Forms.Panel();
            this.pSP = new System.Windows.Forms.Panel();
            this.mapContextMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudLPA2_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudLPA1_3)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ledWriteKmp2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ledReadKmp1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ledWriteKmp1)).BeginInit();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ledReadSP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ledWriteSP)).BeginInit();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ledReadPU)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ledWritePU)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ledReadLPA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ledWriteLPA)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ledReadGPS1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ledWriteGPS)).BeginInit();
            this.panel3.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudPU)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudLPA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSP)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.complexGraph)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ledReadKmp2)).BeginInit();
            this.panel7.SuspendLayout();
            this.pState.SuspendLayout();
            this.pKmpPA.SuspendLayout();
            this.pPU.SuspendLayout();
            this.pKmpRR.SuspendLayout();
            this.pLPA.SuspendLayout();
            this.pGPS.SuspendLayout();
            this.pSP.SuspendLayout();
            this.SuspendLayout();
            // 
            // mapContextMenu
            // 
            this.mapContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem2,
            this.addStation});
            this.mapContextMenu.Name = "contextMenuStrip1";
            this.mapContextMenu.ShowCheckMargin = true;
            this.mapContextMenu.ShowImageMargin = false;
            resources.ApplyResources(this.mapContextMenu, "mapContextMenu");
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Checked = true;
            this.toolStripMenuItem2.CheckState = System.Windows.Forms.CheckState.Checked;
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            resources.ApplyResources(this.toolStripMenuItem2, "toolStripMenuItem2");
            // 
            // addStation
            // 
            this.addStation.Name = "addStation";
            resources.ApplyResources(this.addStation, "addStation");
            // 
            // nudLPA2_4
            // 
            this.nudLPA2_4.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.nudLPA2_4, "nudLPA2_4");
            this.nudLPA2_4.Maximum = new decimal(new int[] {
            330,
            0,
            0,
            0});
            this.nudLPA2_4.Minimum = new decimal(new int[] {
            210,
            0,
            0,
            0});
            this.nudLPA2_4.Name = "nudLPA2_4";
            this.nudLPA2_4.Value = new decimal(new int[] {
            270,
            0,
            0,
            0});
            this.nudLPA2_4.ValueChanged += new System.EventHandler(this.nudLPA2_4_ValueChanged);
            // 
            // nudLPA1_3
            // 
            this.nudLPA1_3.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.nudLPA1_3, "nudLPA1_3");
            this.nudLPA1_3.Maximum = new decimal(new int[] {
            330,
            0,
            0,
            0});
            this.nudLPA1_3.Minimum = new decimal(new int[] {
            210,
            0,
            0,
            0});
            this.nudLPA1_3.Name = "nudLPA1_3";
            this.nudLPA1_3.Value = new decimal(new int[] {
            270,
            0,
            0,
            0});
            this.nudLPA1_3.ValueChanged += new System.EventHandler(this.nudLPA1_3_ValueChanged);
            // 
            // lLPA2_4
            // 
            resources.ApplyResources(this.lLPA2_4, "lLPA2_4");
            this.lLPA2_4.ForeColor = System.Drawing.Color.Olive;
            this.lLPA2_4.Name = "lLPA2_4";
            this.lLPA2_4.DoubleClick += new System.EventHandler(this.lLPA2_4_DoubleClick);
            // 
            // lLPA1_3
            // 
            resources.ApplyResources(this.lLPA1_3, "lLPA1_3");
            this.lLPA1_3.ForeColor = System.Drawing.Color.Blue;
            this.lLPA1_3.Name = "lLPA1_3";
            this.lLPA1_3.DoubleClick += new System.EventHandler(this.lLPA1_3_DoubleClick);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.tbAlt);
            this.groupBox4.Controls.Add(this.label1);
            this.groupBox4.Controls.Add(this.groupBox6);
            this.groupBox4.Controls.Add(this.groupBox5);
            this.groupBox4.Controls.Add(this.tbDate);
            this.groupBox4.Controls.Add(this.lDate);
            this.groupBox4.Controls.Add(this.tbTime);
            this.groupBox4.Controls.Add(this.lTime);
            this.groupBox4.Controls.Add(this.tbCountSat);
            this.groupBox4.Controls.Add(this.tbLon);
            this.groupBox4.Controls.Add(this.tbLat);
            this.groupBox4.Controls.Add(this.lCountSat);
            this.groupBox4.Controls.Add(this.lLon);
            this.groupBox4.Controls.Add(this.lLAt);
            this.groupBox4.ForeColor = System.Drawing.Color.Blue;
            resources.ApplyResources(this.groupBox4, "groupBox4");
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.TabStop = false;
            // 
            // tbAlt
            // 
            resources.ApplyResources(this.tbAlt, "tbAlt");
            this.tbAlt.Name = "tbAlt";
            this.tbAlt.ReadOnly = true;
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Name = "label1";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.cbCoordinateSystem);
            resources.ApplyResources(this.groupBox6, "groupBox6");
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.TabStop = false;
            // 
            // cbCoordinateSystem
            // 
            this.cbCoordinateSystem.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbCoordinateSystem.FormattingEnabled = true;
            this.cbCoordinateSystem.Items.AddRange(new object[] {
            resources.GetString("cbCoordinateSystem.Items"),
            resources.GetString("cbCoordinateSystem.Items1"),
            resources.GetString("cbCoordinateSystem.Items2")});
            resources.ApplyResources(this.cbCoordinateSystem, "cbCoordinateSystem");
            this.cbCoordinateSystem.Name = "cbCoordinateSystem";
            this.cbCoordinateSystem.SelectedIndexChanged += new System.EventHandler(this.cbCoordinateSystem_SelectedIndexChanged);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.cbSatSyst);
            resources.ApplyResources(this.groupBox5, "groupBox5");
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.TabStop = false;
            // 
            // cbSatSyst
            // 
            this.cbSatSyst.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbSatSyst.FormattingEnabled = true;
            this.cbSatSyst.Items.AddRange(new object[] {
            resources.GetString("cbSatSyst.Items"),
            resources.GetString("cbSatSyst.Items1"),
            resources.GetString("cbSatSyst.Items2")});
            resources.ApplyResources(this.cbSatSyst, "cbSatSyst");
            this.cbSatSyst.Name = "cbSatSyst";
            this.cbSatSyst.SelectedIndexChanged += new System.EventHandler(this.cbSatSyst_SelectedIndexChanged);
            // 
            // tbDate
            // 
            resources.ApplyResources(this.tbDate, "tbDate");
            this.tbDate.Name = "tbDate";
            this.tbDate.ReadOnly = true;
            // 
            // lDate
            // 
            resources.ApplyResources(this.lDate, "lDate");
            this.lDate.ForeColor = System.Drawing.Color.Black;
            this.lDate.Name = "lDate";
            // 
            // tbTime
            // 
            resources.ApplyResources(this.tbTime, "tbTime");
            this.tbTime.Name = "tbTime";
            this.tbTime.ReadOnly = true;
            // 
            // lTime
            // 
            resources.ApplyResources(this.lTime, "lTime");
            this.lTime.ForeColor = System.Drawing.Color.Black;
            this.lTime.Name = "lTime";
            // 
            // tbCountSat
            // 
            resources.ApplyResources(this.tbCountSat, "tbCountSat");
            this.tbCountSat.Name = "tbCountSat";
            this.tbCountSat.ReadOnly = true;
            // 
            // tbLon
            // 
            resources.ApplyResources(this.tbLon, "tbLon");
            this.tbLon.Name = "tbLon";
            this.tbLon.ReadOnly = true;
            // 
            // tbLat
            // 
            resources.ApplyResources(this.tbLat, "tbLat");
            this.tbLat.Name = "tbLat";
            this.tbLat.ReadOnly = true;
            // 
            // lCountSat
            // 
            resources.ApplyResources(this.lCountSat, "lCountSat");
            this.lCountSat.ForeColor = System.Drawing.Color.Black;
            this.lCountSat.Name = "lCountSat";
            // 
            // lLon
            // 
            resources.ApplyResources(this.lLon, "lLon");
            this.lLon.ForeColor = System.Drawing.Color.Black;
            this.lLon.Name = "lLon";
            // 
            // lLAt
            // 
            resources.ApplyResources(this.lLAt, "lLAt");
            this.lLAt.ForeColor = System.Drawing.Color.Black;
            this.lLAt.Name = "lLAt";
            // 
            // ledWriteKmp2
            // 
            this.ledWriteKmp2.LedStyle = NationalInstruments.UI.LedStyle.Round3D;
            resources.ApplyResources(this.ledWriteKmp2, "ledWriteKmp2");
            this.ledWriteKmp2.Name = "ledWriteKmp2";
            this.ledWriteKmp2.OffColor = System.Drawing.Color.DarkGray;
            this.ledWriteKmp2.OnColor = System.Drawing.Color.Green;
            this.toolTip.SetToolTip(this.ledWriteKmp2, resources.GetString("ledWriteKmp2.ToolTip"));
            // 
            // ledReadKmp1
            // 
            this.ledReadKmp1.LedStyle = NationalInstruments.UI.LedStyle.Round3D;
            resources.ApplyResources(this.ledReadKmp1, "ledReadKmp1");
            this.ledReadKmp1.Name = "ledReadKmp1";
            this.ledReadKmp1.OffColor = System.Drawing.Color.DarkGray;
            this.ledReadKmp1.OnColor = System.Drawing.Color.Green;
            this.toolTip.SetToolTip(this.ledReadKmp1, resources.GetString("ledReadKmp1.ToolTip"));
            // 
            // ledWriteKmp1
            // 
            this.ledWriteKmp1.LedStyle = NationalInstruments.UI.LedStyle.Round3D;
            resources.ApplyResources(this.ledWriteKmp1, "ledWriteKmp1");
            this.ledWriteKmp1.Name = "ledWriteKmp1";
            this.ledWriteKmp1.OffColor = System.Drawing.Color.DarkGray;
            this.ledWriteKmp1.OnColor = System.Drawing.Color.Green;
            this.toolTip.SetToolTip(this.ledWriteKmp1, resources.GetString("ledWriteKmp1.ToolTip"));
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.bConnectKmp1);
            this.panel6.Controls.Add(this.label18);
            resources.ApplyResources(this.panel6, "panel6");
            this.panel6.Name = "panel6";
            // 
            // bConnectKmp1
            // 
            this.bConnectKmp1.BackColor = System.Drawing.Color.Red;
            resources.ApplyResources(this.bConnectKmp1, "bConnectKmp1");
            this.bConnectKmp1.Name = "bConnectKmp1";
            this.toolTip.SetToolTip(this.bConnectKmp1, resources.GetString("bConnectKmp1.ToolTip"));
            this.bConnectKmp1.UseVisualStyleBackColor = false;
            this.bConnectKmp1.Click += new System.EventHandler(this.bConnectKmp1_Click);
            // 
            // label18
            // 
            resources.ApplyResources(this.label18, "label18");
            this.label18.Name = "label18";
            // 
            // ledReadSP
            // 
            this.ledReadSP.LedStyle = NationalInstruments.UI.LedStyle.Round3D;
            resources.ApplyResources(this.ledReadSP, "ledReadSP");
            this.ledReadSP.Name = "ledReadSP";
            this.ledReadSP.OffColor = System.Drawing.Color.DarkGray;
            this.ledReadSP.OnColor = System.Drawing.Color.Green;
            this.toolTip.SetToolTip(this.ledReadSP, resources.GetString("ledReadSP.ToolTip"));
            // 
            // ledWriteSP
            // 
            this.ledWriteSP.LedStyle = NationalInstruments.UI.LedStyle.Round3D;
            resources.ApplyResources(this.ledWriteSP, "ledWriteSP");
            this.ledWriteSP.Name = "ledWriteSP";
            this.ledWriteSP.OffColor = System.Drawing.Color.DarkGray;
            this.ledWriteSP.OnColor = System.Drawing.Color.Red;
            this.toolTip.SetToolTip(this.ledWriteSP, resources.GetString("ledWriteSP.ToolTip"));
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.bConnectSP);
            this.panel5.Controls.Add(this.label17);
            resources.ApplyResources(this.panel5, "panel5");
            this.panel5.Name = "panel5";
            // 
            // bConnectSP
            // 
            this.bConnectSP.BackColor = System.Drawing.Color.Red;
            resources.ApplyResources(this.bConnectSP, "bConnectSP");
            this.bConnectSP.Name = "bConnectSP";
            this.toolTip.SetToolTip(this.bConnectSP, resources.GetString("bConnectSP.ToolTip"));
            this.bConnectSP.UseVisualStyleBackColor = false;
            this.bConnectSP.Click += new System.EventHandler(this.bConnectSP_Click);
            // 
            // label17
            // 
            resources.ApplyResources(this.label17, "label17");
            this.label17.Name = "label17";
            // 
            // ledReadPU
            // 
            this.ledReadPU.LedStyle = NationalInstruments.UI.LedStyle.Round3D;
            resources.ApplyResources(this.ledReadPU, "ledReadPU");
            this.ledReadPU.Name = "ledReadPU";
            this.ledReadPU.OffColor = System.Drawing.Color.DarkGray;
            this.ledReadPU.OnColor = System.Drawing.Color.Green;
            this.toolTip.SetToolTip(this.ledReadPU, resources.GetString("ledReadPU.ToolTip"));
            // 
            // ledWritePU
            // 
            this.ledWritePU.LedStyle = NationalInstruments.UI.LedStyle.Round3D;
            resources.ApplyResources(this.ledWritePU, "ledWritePU");
            this.ledWritePU.Name = "ledWritePU";
            this.ledWritePU.OffColor = System.Drawing.Color.DarkGray;
            this.ledWritePU.OnColor = System.Drawing.Color.Red;
            this.toolTip.SetToolTip(this.ledWritePU, resources.GetString("ledWritePU.ToolTip"));
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.bConnectPU);
            this.panel4.Controls.Add(this.label16);
            resources.ApplyResources(this.panel4, "panel4");
            this.panel4.Name = "panel4";
            // 
            // bConnectPU
            // 
            this.bConnectPU.BackColor = System.Drawing.Color.Red;
            resources.ApplyResources(this.bConnectPU, "bConnectPU");
            this.bConnectPU.Name = "bConnectPU";
            this.toolTip.SetToolTip(this.bConnectPU, resources.GetString("bConnectPU.ToolTip"));
            this.bConnectPU.UseVisualStyleBackColor = false;
            this.bConnectPU.Click += new System.EventHandler(this.bConnectPU_Click);
            // 
            // label16
            // 
            resources.ApplyResources(this.label16, "label16");
            this.label16.Name = "label16";
            // 
            // ledReadLPA
            // 
            this.ledReadLPA.LedStyle = NationalInstruments.UI.LedStyle.Round3D;
            resources.ApplyResources(this.ledReadLPA, "ledReadLPA");
            this.ledReadLPA.Name = "ledReadLPA";
            this.ledReadLPA.OffColor = System.Drawing.Color.DarkGray;
            this.ledReadLPA.OnColor = System.Drawing.Color.Green;
            this.toolTip.SetToolTip(this.ledReadLPA, resources.GetString("ledReadLPA.ToolTip"));
            // 
            // ledWriteLPA
            // 
            this.ledWriteLPA.LedStyle = NationalInstruments.UI.LedStyle.Round3D;
            resources.ApplyResources(this.ledWriteLPA, "ledWriteLPA");
            this.ledWriteLPA.Name = "ledWriteLPA";
            this.ledWriteLPA.OffColor = System.Drawing.Color.DarkGray;
            this.ledWriteLPA.OnColor = System.Drawing.Color.Red;
            this.toolTip.SetToolTip(this.ledWriteLPA, resources.GetString("ledWriteLPA.ToolTip"));
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.bConnectLPA);
            this.panel2.Controls.Add(this.label11);
            resources.ApplyResources(this.panel2, "panel2");
            this.panel2.Name = "panel2";
            // 
            // bConnectLPA
            // 
            this.bConnectLPA.BackColor = System.Drawing.Color.Red;
            resources.ApplyResources(this.bConnectLPA, "bConnectLPA");
            this.bConnectLPA.Name = "bConnectLPA";
            this.toolTip.SetToolTip(this.bConnectLPA, resources.GetString("bConnectLPA.ToolTip"));
            this.bConnectLPA.UseVisualStyleBackColor = false;
            this.bConnectLPA.Click += new System.EventHandler(this.bConnectLPA_Click);
            // 
            // label11
            // 
            resources.ApplyResources(this.label11, "label11");
            this.label11.Name = "label11";
            // 
            // ledReadGPS1
            // 
            this.ledReadGPS1.LedStyle = NationalInstruments.UI.LedStyle.Round3D;
            resources.ApplyResources(this.ledReadGPS1, "ledReadGPS1");
            this.ledReadGPS1.Name = "ledReadGPS1";
            this.ledReadGPS1.OffColor = System.Drawing.Color.DarkGray;
            this.ledReadGPS1.OnColor = System.Drawing.Color.Green;
            this.toolTip.SetToolTip(this.ledReadGPS1, resources.GetString("ledReadGPS1.ToolTip"));
            // 
            // ledWriteGPS
            // 
            this.ledWriteGPS.LedStyle = NationalInstruments.UI.LedStyle.Round3D;
            resources.ApplyResources(this.ledWriteGPS, "ledWriteGPS");
            this.ledWriteGPS.Name = "ledWriteGPS";
            this.ledWriteGPS.OffColor = System.Drawing.Color.DarkGray;
            this.ledWriteGPS.OnColor = System.Drawing.Color.Green;
            this.toolTip.SetToolTip(this.ledWriteGPS, resources.GetString("ledWriteGPS.ToolTip"));
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.bConnectGPS);
            this.panel3.Controls.Add(this.lGPS);
            resources.ApplyResources(this.panel3, "panel3");
            this.panel3.Name = "panel3";
            // 
            // bConnectGPS
            // 
            this.bConnectGPS.BackColor = System.Drawing.Color.Red;
            resources.ApplyResources(this.bConnectGPS, "bConnectGPS");
            this.bConnectGPS.Name = "bConnectGPS";
            this.toolTip.SetToolTip(this.bConnectGPS, resources.GetString("bConnectGPS.ToolTip"));
            this.bConnectGPS.UseVisualStyleBackColor = false;
            this.bConnectGPS.Click += new System.EventHandler(this.bConnectGPS_Click);
            // 
            // lGPS
            // 
            resources.ApplyResources(this.lGPS, "lGPS");
            this.lGPS.Name = "lGPS";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.bStopSP);
            this.groupBox3.Controls.Add(this.bStopPU);
            this.groupBox3.Controls.Add(this.bSetSP);
            this.groupBox3.Controls.Add(this.bSetPU);
            this.groupBox3.Controls.Add(this.bStopLPA);
            this.groupBox3.Controls.Add(this.bSetLPA);
            this.groupBox3.Controls.Add(this.nudPU);
            this.groupBox3.Controls.Add(this.nudLPA);
            this.groupBox3.Controls.Add(this.tbPU);
            this.groupBox3.Controls.Add(this.nudSP);
            this.groupBox3.Controls.Add(this.tbLPA);
            this.groupBox3.Controls.Add(this.lPU);
            this.groupBox3.Controls.Add(this.lLPA);
            this.groupBox3.Controls.Add(this.lSP);
            this.groupBox3.Controls.Add(this.tbSP);
            this.groupBox3.ForeColor = System.Drawing.Color.Blue;
            resources.ApplyResources(this.groupBox3, "groupBox3");
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.TabStop = false;
            // 
            // bStopSP
            // 
            this.bStopSP.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.bStopSP, "bStopSP");
            this.bStopSP.Name = "bStopSP";
            this.bStopSP.UseVisualStyleBackColor = true;
            this.bStopSP.Click += new System.EventHandler(this.bStopSP_Click);
            // 
            // bStopPU
            // 
            this.bStopPU.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.bStopPU, "bStopPU");
            this.bStopPU.Name = "bStopPU";
            this.bStopPU.UseVisualStyleBackColor = true;
            this.bStopPU.Click += new System.EventHandler(this.bStopPU_Click);
            // 
            // bSetSP
            // 
            this.bSetSP.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.bSetSP, "bSetSP");
            this.bSetSP.Name = "bSetSP";
            this.bSetSP.UseVisualStyleBackColor = true;
            this.bSetSP.Click += new System.EventHandler(this.bSetSP_Click);
            // 
            // bSetPU
            // 
            this.bSetPU.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.bSetPU, "bSetPU");
            this.bSetPU.Name = "bSetPU";
            this.bSetPU.UseVisualStyleBackColor = true;
            this.bSetPU.Click += new System.EventHandler(this.bSetPU_Click);
            // 
            // bStopLPA
            // 
            this.bStopLPA.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.bStopLPA, "bStopLPA");
            this.bStopLPA.Name = "bStopLPA";
            this.bStopLPA.UseVisualStyleBackColor = true;
            this.bStopLPA.Click += new System.EventHandler(this.bStopLPA_Click);
            // 
            // bSetLPA
            // 
            this.bSetLPA.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.bSetLPA, "bSetLPA");
            this.bSetLPA.Name = "bSetLPA";
            this.bSetLPA.UseVisualStyleBackColor = true;
            this.bSetLPA.Click += new System.EventHandler(this.bSetLPA_Click);
            // 
            // nudPU
            // 
            this.nudPU.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.nudPU, "nudPU");
            this.nudPU.Maximum = new decimal(new int[] {
            359,
            0,
            0,
            0});
            this.nudPU.Name = "nudPU";
            this.nudPU.ValueChanged += new System.EventHandler(this.nudPU_ValueChanged);
            // 
            // nudLPA
            // 
            this.nudLPA.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.nudLPA, "nudLPA");
            this.nudLPA.Maximum = new decimal(new int[] {
            359,
            0,
            0,
            0});
            this.nudLPA.Name = "nudLPA";
            this.nudLPA.ValueChanged += new System.EventHandler(this.nudLPA_ValueChanged);
            // 
            // tbPU
            // 
            resources.ApplyResources(this.tbPU, "tbPU");
            this.tbPU.ForeColor = System.Drawing.Color.Black;
            this.tbPU.Name = "tbPU";
            this.tbPU.ReadOnly = true;
            // 
            // nudSP
            // 
            this.nudSP.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.nudSP, "nudSP");
            this.nudSP.Maximum = new decimal(new int[] {
            359,
            0,
            0,
            0});
            this.nudSP.Name = "nudSP";
            this.nudSP.ValueChanged += new System.EventHandler(this.nudSP_ValueChanged);
            // 
            // tbLPA
            // 
            this.tbLPA.BackColor = System.Drawing.SystemColors.Control;
            resources.ApplyResources(this.tbLPA, "tbLPA");
            this.tbLPA.ForeColor = System.Drawing.Color.Black;
            this.tbLPA.Name = "tbLPA";
            this.tbLPA.ReadOnly = true;
            // 
            // lPU
            // 
            resources.ApplyResources(this.lPU, "lPU");
            this.lPU.ForeColor = System.Drawing.Color.Cyan;
            this.lPU.Name = "lPU";
            this.lPU.DoubleClick += new System.EventHandler(this.lPU_DoubleClick);
            // 
            // lLPA
            // 
            resources.ApplyResources(this.lLPA, "lLPA");
            this.lLPA.ForeColor = System.Drawing.Color.Red;
            this.lLPA.Name = "lLPA";
            this.lLPA.DoubleClick += new System.EventHandler(this.lLPA_DoubleClick);
            // 
            // lSP
            // 
            resources.ApplyResources(this.lSP, "lSP");
            this.lSP.ForeColor = System.Drawing.Color.Green;
            this.lSP.Name = "lSP";
            this.lSP.DoubleClick += new System.EventHandler(this.lSP_DoubleClick);
            // 
            // tbSP
            // 
            resources.ApplyResources(this.tbSP, "tbSP");
            this.tbSP.ForeColor = System.Drawing.Color.Black;
            this.tbSP.Name = "tbSP";
            this.tbSP.ReadOnly = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.tbK2Azimuth);
            this.groupBox2.Controls.Add(this.lK2Azimuth);
            this.groupBox2.ForeColor = System.Drawing.Color.Blue;
            resources.ApplyResources(this.groupBox2, "groupBox2");
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.TabStop = false;
            // 
            // tbK2Azimuth
            // 
            resources.ApplyResources(this.tbK2Azimuth, "tbK2Azimuth");
            this.tbK2Azimuth.Name = "tbK2Azimuth";
            this.tbK2Azimuth.ReadOnly = true;
            // 
            // lK2Azimuth
            // 
            resources.ApplyResources(this.lK2Azimuth, "lK2Azimuth");
            this.lK2Azimuth.ForeColor = System.Drawing.Color.Black;
            this.lK2Azimuth.Name = "lK2Azimuth";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tbK1Azimuth);
            this.groupBox1.Controls.Add(this.lK1Azimuth);
            this.groupBox1.ForeColor = System.Drawing.Color.Blue;
            resources.ApplyResources(this.groupBox1, "groupBox1");
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.TabStop = false;
            // 
            // tbK1Azimuth
            // 
            resources.ApplyResources(this.tbK1Azimuth, "tbK1Azimuth");
            this.tbK1Azimuth.Name = "tbK1Azimuth";
            this.tbK1Azimuth.ReadOnly = true;
            // 
            // lK1Azimuth
            // 
            resources.ApplyResources(this.lK1Azimuth, "lK1Azimuth");
            this.lK1Azimuth.ForeColor = System.Drawing.Color.Black;
            this.lK1Azimuth.Name = "lK1Azimuth";
            // 
            // complexGraph
            // 
            this.complexGraph.BackgroundImage = global::WinAp4.Properties.Resources.Compass21;
            this.complexGraph.Border = NationalInstruments.UI.Border.None;
            resources.ApplyResources(this.complexGraph, "complexGraph");
            this.complexGraph.Name = "complexGraph";
            this.complexGraph.PlotAreaBorder = NationalInstruments.UI.Border.None;
            this.complexGraph.PlotAreaColor = System.Drawing.Color.Transparent;
            this.complexGraph.PlotAreaImageAlignment = NationalInstruments.UI.ImageAlignment.Center;
            this.complexGraph.Plots.AddRange(new NationalInstruments.UI.ComplexPlot[] {
            this.complexPlotLPA,
            this.complexPlotPU,
            this.complexPlotSP,
            this.complexPlotLPA1_3,
            this.complexPlotLPA2_4});
            this.complexGraph.UseColorGenerator = true;
            this.complexGraph.XAxes.AddRange(new NationalInstruments.UI.ComplexXAxis[] {
            this.complexXAxis1});
            this.complexGraph.YAxes.AddRange(new NationalInstruments.UI.ComplexYAxis[] {
            this.complexYAxis1});
            // 
            // complexPlotLPA
            // 
            this.complexPlotLPA.CanScaleXAxis = false;
            this.complexPlotLPA.CanScaleYAxis = false;
            this.complexPlotLPA.LineColor = System.Drawing.Color.Red;
            this.complexPlotLPA.LineColorPrecedence = NationalInstruments.UI.ColorPrecedence.UserDefinedColor;
            this.complexPlotLPA.LineWidth = 2F;
            this.complexPlotLPA.XAxis = this.complexXAxis1;
            this.complexPlotLPA.YAxis = this.complexYAxis1;
            // 
            // complexXAxis1
            // 
            this.complexXAxis1.Mode = NationalInstruments.UI.AxisMode.Fixed;
            this.complexXAxis1.OriginLineVisible = false;
            this.complexXAxis1.Visible = false;
            // 
            // complexYAxis1
            // 
            this.complexYAxis1.OriginLineVisible = false;
            this.complexYAxis1.Visible = false;
            // 
            // complexPlotPU
            // 
            this.complexPlotPU.CanScaleXAxis = false;
            this.complexPlotPU.CanScaleYAxis = false;
            this.complexPlotPU.LineColor = System.Drawing.Color.Cyan;
            this.complexPlotPU.LineColorPrecedence = NationalInstruments.UI.ColorPrecedence.UserDefinedColor;
            this.complexPlotPU.LineWidth = 2F;
            this.complexPlotPU.XAxis = this.complexXAxis1;
            this.complexPlotPU.YAxis = this.complexYAxis1;
            // 
            // complexPlotSP
            // 
            this.complexPlotSP.CanScaleXAxis = false;
            this.complexPlotSP.CanScaleYAxis = false;
            this.complexPlotSP.LineColor = System.Drawing.Color.Green;
            this.complexPlotSP.LineColorPrecedence = NationalInstruments.UI.ColorPrecedence.UserDefinedColor;
            this.complexPlotSP.LineWidth = 2F;
            this.complexPlotSP.XAxis = this.complexXAxis1;
            this.complexPlotSP.YAxis = this.complexYAxis1;
            // 
            // complexPlotLPA1_3
            // 
            this.complexPlotLPA1_3.CanScaleXAxis = false;
            this.complexPlotLPA1_3.CanScaleYAxis = false;
            this.complexPlotLPA1_3.LineColor = System.Drawing.Color.Blue;
            this.complexPlotLPA1_3.LineColorPrecedence = NationalInstruments.UI.ColorPrecedence.UserDefinedColor;
            this.complexPlotLPA1_3.LineWidth = 2F;
            this.complexPlotLPA1_3.XAxis = this.complexXAxis1;
            this.complexPlotLPA1_3.YAxis = this.complexYAxis1;
            // 
            // complexPlotLPA2_4
            // 
            this.complexPlotLPA2_4.CanScaleXAxis = false;
            this.complexPlotLPA2_4.CanScaleYAxis = false;
            this.complexPlotLPA2_4.LineColor = System.Drawing.Color.Olive;
            this.complexPlotLPA2_4.LineColorPrecedence = NationalInstruments.UI.ColorPrecedence.UserDefinedColor;
            this.complexPlotLPA2_4.LineWidth = 2F;
            this.complexPlotLPA2_4.XAxis = this.complexXAxis1;
            this.complexPlotLPA2_4.YAxis = this.complexYAxis1;
            // 
            // imageList2
            // 
            this.imageList2.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList2.ImageStream")));
            this.imageList2.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList2.Images.SetKeyName(0, "plus26x26.png");
            this.imageList2.Images.SetKeyName(1, "minus26x26.png");
            this.imageList2.Images.SetKeyName(2, "clear26x26.png");
            this.imageList2.Images.SetKeyName(3, "open26x26.png");
            this.imageList2.Images.SetKeyName(4, "stop26x26.png");
            this.imageList2.Images.SetKeyName(5, "start26x26.png");
            this.imageList2.Images.SetKeyName(6, "back26x26.png");
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "classic-arrows_move_simple-green-gradient_48x48.png");
            this.imageList1.Images.SetKeyName(1, "classic-arrows_redirect-left-thick_simple-green-gradient_48x48.png");
            this.imageList1.Images.SetKeyName(2, "alphanum_number-3_simple-green_32x32.png");
            this.imageList1.Images.SetKeyName(3, "alphanum_number-3_simple-ios-neon-green-gradient_48x48.png");
            // 
            // ledReadKmp2
            // 
            this.ledReadKmp2.LedStyle = NationalInstruments.UI.LedStyle.Round3D;
            resources.ApplyResources(this.ledReadKmp2, "ledReadKmp2");
            this.ledReadKmp2.Name = "ledReadKmp2";
            this.ledReadKmp2.OffColor = System.Drawing.Color.DarkGray;
            this.ledReadKmp2.OnColor = System.Drawing.Color.Green;
            this.toolTip.SetToolTip(this.ledReadKmp2, resources.GetString("ledReadKmp2.ToolTip"));
            // 
            // bConnectKmp2
            // 
            this.bConnectKmp2.BackColor = System.Drawing.Color.Red;
            resources.ApplyResources(this.bConnectKmp2, "bConnectKmp2");
            this.bConnectKmp2.Name = "bConnectKmp2";
            this.toolTip.SetToolTip(this.bConnectKmp2, resources.GetString("bConnectKmp2.ToolTip"));
            this.bConnectKmp2.UseVisualStyleBackColor = false;
            this.bConnectKmp2.Click += new System.EventHandler(this.bConnectKmp2_Click);
            // 
            // timerLedReadBytes
            // 
            this.timerLedReadBytes.Interval = 500;
            this.timerLedReadBytes.Tick += new System.EventHandler(this.timerLedReadBytes_Tick);
            // 
            // label19
            // 
            resources.ApplyResources(this.label19, "label19");
            this.label19.Name = "label19";
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.bConnectKmp2);
            this.panel7.Controls.Add(this.label19);
            resources.ApplyResources(this.panel7, "panel7");
            this.panel7.Name = "panel7";
            // 
            // pState
            // 
            this.pState.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pState.Controls.Add(this.pKmpPA);
            this.pState.Controls.Add(this.pPU);
            this.pState.Controls.Add(this.panel14);
            this.pState.Controls.Add(this.panel11);
            this.pState.Controls.Add(this.pKmpRR);
            this.pState.Controls.Add(this.panel16);
            this.pState.Controls.Add(this.panel13);
            this.pState.Controls.Add(this.panel8);
            this.pState.Controls.Add(this.panel10);
            this.pState.Controls.Add(this.pLPA);
            resources.ApplyResources(this.pState, "pState");
            this.pState.Name = "pState";
            // 
            // pKmpPA
            // 
            this.pKmpPA.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pKmpPA.Controls.Add(this.ledReadKmp2);
            this.pKmpPA.Controls.Add(this.panel7);
            this.pKmpPA.Controls.Add(this.ledWriteKmp2);
            resources.ApplyResources(this.pKmpPA, "pKmpPA");
            this.pKmpPA.Name = "pKmpPA";
            // 
            // pPU
            // 
            this.pPU.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pPU.Controls.Add(this.panel4);
            this.pPU.Controls.Add(this.ledWritePU);
            this.pPU.Controls.Add(this.ledReadPU);
            resources.ApplyResources(this.pPU, "pPU");
            this.pPU.Name = "pPU";
            // 
            // panel14
            // 
            this.panel14.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            resources.ApplyResources(this.panel14, "panel14");
            this.panel14.Name = "panel14";
            // 
            // panel11
            // 
            this.panel11.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            resources.ApplyResources(this.panel11, "panel11");
            this.panel11.Name = "panel11";
            // 
            // pKmpRR
            // 
            this.pKmpRR.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pKmpRR.Controls.Add(this.panel6);
            this.pKmpRR.Controls.Add(this.ledWriteKmp1);
            this.pKmpRR.Controls.Add(this.ledReadKmp1);
            resources.ApplyResources(this.pKmpRR, "pKmpRR");
            this.pKmpRR.Name = "pKmpRR";
            // 
            // panel16
            // 
            this.panel16.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            resources.ApplyResources(this.panel16, "panel16");
            this.panel16.Name = "panel16";
            // 
            // panel13
            // 
            this.panel13.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            resources.ApplyResources(this.panel13, "panel13");
            this.panel13.Name = "panel13";
            // 
            // panel8
            // 
            this.panel8.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            resources.ApplyResources(this.panel8, "panel8");
            this.panel8.Name = "panel8";
            // 
            // panel10
            // 
            this.panel10.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            resources.ApplyResources(this.panel10, "panel10");
            this.panel10.Name = "panel10";
            // 
            // pLPA
            // 
            this.pLPA.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pLPA.Controls.Add(this.panel2);
            this.pLPA.Controls.Add(this.ledWriteLPA);
            this.pLPA.Controls.Add(this.ledReadLPA);
            resources.ApplyResources(this.pLPA, "pLPA");
            this.pLPA.Name = "pLPA";
            // 
            // pGPS
            // 
            this.pGPS.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pGPS.Controls.Add(this.panel3);
            this.pGPS.Controls.Add(this.ledWriteGPS);
            this.pGPS.Controls.Add(this.ledReadGPS1);
            resources.ApplyResources(this.pGPS, "pGPS");
            this.pGPS.Name = "pGPS";
            // 
            // pSP
            // 
            this.pSP.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pSP.Controls.Add(this.panel5);
            this.pSP.Controls.Add(this.ledWriteSP);
            this.pSP.Controls.Add(this.ledReadSP);
            resources.ApplyResources(this.pSP, "pSP");
            this.pSP.Name = "pSP";
            // 
            // FormAP4
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ContextMenuStrip = this.mapContextMenu;
            this.Controls.Add(this.pSP);
            this.Controls.Add(this.pGPS);
            this.Controls.Add(this.pState);
            this.Controls.Add(this.nudLPA2_4);
            this.Controls.Add(this.nudLPA1_3);
            this.Controls.Add(this.complexGraph);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.lLPA1_3);
            this.Controls.Add(this.lLPA2_4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormAP4";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormAP4_FormClosing);
            this.mapContextMenu.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.nudLPA2_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudLPA1_3)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ledWriteKmp2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ledReadKmp1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ledWriteKmp1)).EndInit();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ledReadSP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ledWriteSP)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ledReadPU)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ledWritePU)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ledReadLPA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ledWriteLPA)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ledReadGPS1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ledWriteGPS)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudPU)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudLPA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSP)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.complexGraph)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ledReadKmp2)).EndInit();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.pState.ResumeLayout(false);
            this.pKmpPA.ResumeLayout(false);
            this.pPU.ResumeLayout(false);
            this.pKmpRR.ResumeLayout(false);
            this.pLPA.ResumeLayout(false);
            this.pGPS.ResumeLayout(false);
            this.pSP.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ImageList imageList1;
        public System.Windows.Forms.ContextMenuStrip mapContextMenu;
        public System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.ToolStripMenuItem addStation;
        private System.Windows.Forms.ImageList imageList2;
        private NationalInstruments.UI.WindowsForms.ComplexGraph complexGraph;
        private NationalInstruments.UI.ComplexPlot complexPlotLPA;
        private NationalInstruments.UI.ComplexYAxis complexYAxis1;
        private NationalInstruments.UI.ComplexPlot complexPlotPU;
        private NationalInstruments.UI.ComplexPlot complexPlotSP;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button bStopSP;
        private System.Windows.Forms.Button bSetSP;
        private System.Windows.Forms.Button bStopPU;
        private System.Windows.Forms.Button bSetPU;
        private System.Windows.Forms.Button bStopLPA;
        private System.Windows.Forms.Button bSetLPA;
        private System.Windows.Forms.NumericUpDown nudSP;
        private System.Windows.Forms.NumericUpDown nudPU;
        private System.Windows.Forms.NumericUpDown nudLPA;
        private System.Windows.Forms.TextBox tbSP;
        private System.Windows.Forms.TextBox tbPU;
        private System.Windows.Forms.TextBox tbLPA;
        private System.Windows.Forms.Label lSP;
        private System.Windows.Forms.Label lPU;
        private System.Windows.Forms.Label lLPA;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label lK2Azimuth;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lK1Azimuth;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label lTime;
        private System.Windows.Forms.Label lCountSat;
        private System.Windows.Forms.Label lLon;
        private System.Windows.Forms.Label lLAt;
        private NationalInstruments.UI.ComplexXAxis complexXAxis1;
        private System.Windows.Forms.TextBox tbTime;
        private System.Windows.Forms.TextBox tbCountSat;
        private System.Windows.Forms.TextBox tbLon;
        private System.Windows.Forms.TextBox tbLat;
        private System.Windows.Forms.TextBox tbK2Azimuth;
        private System.Windows.Forms.TextBox tbK1Azimuth;
        private System.Windows.Forms.TextBox tbDate;
        private System.Windows.Forms.Label lDate;
        private NationalInstruments.UI.WindowsForms.Led ledWriteKmp2;
        private NationalInstruments.UI.WindowsForms.Led ledReadKmp1;
        private NationalInstruments.UI.WindowsForms.Led ledWriteKmp1;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Button bConnectKmp1;
        private System.Windows.Forms.Label label18;
        private NationalInstruments.UI.WindowsForms.Led ledReadSP;
        private NationalInstruments.UI.WindowsForms.Led ledWriteSP;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Button bConnectSP;
        private System.Windows.Forms.Label label17;
        private NationalInstruments.UI.WindowsForms.Led ledReadPU;
        private NationalInstruments.UI.WindowsForms.Led ledWritePU;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button bConnectPU;
        private System.Windows.Forms.Label label16;
        private NationalInstruments.UI.WindowsForms.Led ledReadLPA;
        private NationalInstruments.UI.WindowsForms.Led ledWriteLPA;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button bConnectLPA;
        private System.Windows.Forms.Label label11;
        private NationalInstruments.UI.WindowsForms.Led ledReadGPS1;
        private NationalInstruments.UI.WindowsForms.Led ledWriteGPS;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button bConnectGPS;
        private System.Windows.Forms.Label lGPS;
        private System.Windows.Forms.Timer timerLedReadBytes;
        private System.Windows.Forms.Label lLPA2_4;
        private System.Windows.Forms.Label lLPA1_3;
        private NationalInstruments.UI.ComplexPlot complexPlotLPA1_3;
        private NationalInstruments.UI.ComplexPlot complexPlotLPA2_4;
        private System.Windows.Forms.NumericUpDown nudLPA1_3;
        private System.Windows.Forms.NumericUpDown nudLPA2_4;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.ComboBox cbCoordinateSystem;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.ComboBox cbSatSyst;
        private System.Windows.Forms.ColorDialog colorDialog1;
        private NationalInstruments.UI.WindowsForms.Led ledReadKmp2;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Button bConnectKmp2;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Panel pState;
        private System.Windows.Forms.Panel pGPS;
        private System.Windows.Forms.TextBox tbAlt;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel pKmpPA;
        private System.Windows.Forms.Panel pPU;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Panel pKmpRR;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Panel pLPA;
        private System.Windows.Forms.Panel pSP;

    }
}

