﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinAp4
{
    class GPSEncoder
    {
        public enum TypeRestart { COLD_START, WARM_START };

        private byte[] GetBytesForQuery(string message)
        {
            return Encoding.UTF8.GetBytes(message);
        }

        public byte[] GPSRestart(TypeRestart type)
        {
            byte[] bQuery = new byte[]{};

            if (type == TypeRestart.COLD_START)
                bQuery = GetBytesForQuery("$PNVGRST,F*30\r\n");

            if (type == TypeRestart.WARM_START)
                bQuery = GetBytesForQuery("$PNVGRST,W*21\r\n");

            return bQuery;
        }

       
    }
}
