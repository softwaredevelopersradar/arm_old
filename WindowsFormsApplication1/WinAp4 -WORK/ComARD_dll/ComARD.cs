﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO.Ports;
using System.Threading;
using System.Globalization;

namespace CNTLib
{
    public struct TDecodeCmd
    {
        public String strCode;
        public String strData;
    }
        
    public class ComARD
    {
        static Int32 TIME_QUERY = 1000;
        private SerialPort _port;

        private Thread thrRead;
        private Thread thrQuery;

        String strRead = "";

        
        bool blChangingAngle = false;
        bool blSendQuery = false;

        double dSetAngle = 0;
        double dGetAngle = 0;

        double dGetAngleLast = 0;
        byte bCountSameAngle = 0;

        String strSendGetAngle = "Y\r";
        String strSendStop = "S\r";
        String strSendSetAngle = "Q\r";

        String strReceiveError = "ERR<CR>\r";
        String strReceiveAck = "ACK<CR>\r";
        String strReceiveOk = "OK<CR>\r";



        #region Delegates

        public delegate void ConnectEventHandler();
        public delegate void ByteEventHandler(byte[] bByte);
        public delegate void CmdEventHandler(object obj);


        public delegate void GetAngleReceivedEventHandler(double dAngle, double dElevation);
        public delegate void SetAngleReceivedEventHandler(double dAngle, double dElevation);
        public delegate void ERRReceivedEventHandler();
        public delegate void ACKReceivedEventHandler();
        public delegate void STPReceivedEventHandler();
    
        #endregion

        #region Events

        public event GetAngleReceivedEventHandler OnGetAngleReceived;
        public event SetAngleReceivedEventHandler OnSetAngleReceived;
        public event ERRReceivedEventHandler OnERRReceived;
        public event ACKReceivedEventHandler OnACKReceived;
        public event STPReceivedEventHandler OnSTPReceived;
        public event ConnectEventHandler OnConnectPort;
        public event ConnectEventHandler OnDisconnectPort;
        public event ByteEventHandler OnReadByte;
        public event ByteEventHandler OnWriteByte;
        public event CmdEventHandler OnReceiveCmd;
        public event CmdEventHandler OnSendCmd;
        public event CmdEventHandler OnReceive_ERROR;
        #endregion


        #region Events


        // open port
        protected virtual void ConnectPort()
        {
            if (OnConnectPort != null)
            {
                OnConnectPort();//Raise the event
            }
        }

        // close port
        protected virtual void DisconnectPort()
        {
            if (OnDisconnectPort != null)
            {
                OnDisconnectPort();//Raise the event
            }
        }


        // receive error
        protected virtual void Receive_ERROR(object obj)
        {
            if (OnReceive_ERROR != null)
            {
                OnReceive_ERROR(obj);//Raise the event
            }
        }

        // read byte array
        protected virtual void ReadByte(byte[] bByte)
        {
            if (OnReadByte != null)
            {
                OnReadByte(bByte);//Raise the event
            }
        }

        // write byte array
        protected virtual void WriteByte(byte[] bByte)
        {
            if (OnWriteByte != null)
            {
                OnWriteByte(bByte);//Raise the event
            }
        }


        // receive cmd
        protected virtual void ReceiveCmd(object obj)
        {
            if (OnReceiveCmd != null)
            {
                OnReceiveCmd(obj);//Raise the event
            }
        }

        // send cmd
        protected virtual void SendCmd(object obj)
        {
            if (OnSendCmd != null)
            {
                OnSendCmd(obj);//Raise the event
            }
        }

        // send cmd
        protected virtual void GetAngleReceived(double dAngle, double dElevation)
        {
            if (OnGetAngleReceived != null)
            {
                OnGetAngleReceived(dAngle, dElevation);//Raise the event
            }
        }

        // send cmd
        protected virtual void SetAngleReceived(double dAngle, double dElevation)
        {
            if (OnSetAngleReceived != null)
            {
                OnSetAngleReceived(dAngle, dElevation);//Raise the event
            }
        }


        // send cmd
        protected virtual void ERRReceived()
        {
            if (OnERRReceived != null)
            {
                OnERRReceived();//Raise the event
            }
        }


        // send cmd
        protected virtual void ACKReceived()
        {
            if (OnACKReceived != null)
            {
                OnACKReceived();//Raise the event
            }
        }

        // send cmd
        protected virtual void STPReceived()
        {
            if (OnSTPReceived != null)
            {
                OnSTPReceived();//Raise the event
            }
        }



        #endregion

        public void OpenPort(string portName, Int32 baudRate, System.IO.Ports.Parity parity, Int32 dataBits, System.IO.Ports.StopBits stopBits)
        {
            // Open COM port

            if (_port == null)
                _port = new SerialPort();

            // if port is open
            if (_port.IsOpen)

                // close it
                ClosePort();

            // try to open 
            try
            {
                // set parameters of port
                _port.PortName = portName;
                _port.BaudRate = baudRate;

                _port.Parity = parity;
                _port.DataBits = dataBits;
                _port.StopBits = stopBits;

                _port.RtsEnable = true;
                _port.DtrEnable = true;

                //_port.ReadTimeout = 1000;
                //_port.WriteTimeout = 1000;

                _port.ReceivedBytesThreshold = 1000;
                // set parameters of port
                /*  _port.PortName = portName;
                  _port.BaudRate = baudRate;

                  _port.Parity = System.IO.Ports.Parity.None;
                  _port.DataBits = 8;
                  _port.StopBits = System.IO.Ports.StopBits.One;*/


                // open it
                _port.Open();

                // create the thread for reading data from the port
                if (thrRead != null)
                {
                    thrRead.Abort();
                    thrRead.Join(500);
                    thrRead = null;
                }

                // load function of the thread for reading data from the port
                try
                {
                    thrRead = new Thread(new ThreadStart(ReadData));
                    thrRead.IsBackground = true;
                    thrRead.Start();
                }
                catch (System.Exception ex)
                {

                }

                // raise event
                // ConnectPort();

                // return true;

                ConnectPort();
            }
            catch (System.Exception ex)
            {
                DisconnectPort();

                //return false;
            }
        }


        // Close COM port
        public void ClosePort()
        {
            // clear in buffer
            try
            {
                _port.DiscardInBuffer();
            }
            catch (System.Exception ex)
            {


            }

            // clear in buffer
            try
            {
                _port.DiscardOutBuffer();
            }
            catch (System.Exception ex)
            {

            }

            try
            {
                // close port
                _port.Close();

                // destroy thread of reading
                if (thrRead != null)
                {
                    thrRead.Abort();
                    thrRead.Join(500);
                    thrRead = null;
                }

                // raise event
                DisconnectPort();
            }

            catch (System.Exception ex)
            {

            }

        }



        // Read data (byte) from port (thread)
        private void ReadData()
        {
            // buffer for reading bytes
            byte[] bBufRead;
            bBufRead = new byte[256];

            // buffer to transfer and save reading bytes
            byte[] bBufSave = new byte[2000];
            byte[] bBufSaveS;
            bBufSaveS = new byte[2000];

            // length of reading bytes
            int iReadByte = -1;

            // while port open
            while (true)
            {
                try
                {
                    bBufRead = new byte[256];

                    // clear burre of reading
                    Array.Clear(bBufRead, 0, bBufRead.Length);

                    iReadByte = 0;

                    // read data
                    iReadByte = _port.Read(bBufRead, 0, bBufRead.Length);

                    // if bytes was read
                    if (iReadByte > 0)
                    {

                        Array.Resize(ref bBufRead, iReadByte);
                        ReadByte(bBufRead);

                        strRead += Encoding.ASCII.GetString(bBufRead);

                        TDecodeCmd tDecodeCmd = FindCmd(ref strRead);


                        while (tDecodeCmd.strCode != null)
                        {
                            switch (tDecodeCmd.strCode)
                            {
                                case "OK":
                                    try 
                                    {
                                        //double dGetAngle = Convert.ToDouble((tDecodeCmd.strData.Substring(0,5)));//.Replace('.',','));

                                        /*Char separator = System.Globalization.CultureInfo.CurrentCulture.NumberFormat.CurrencyDecimalSeparator[0];
                                        String Source = (tDecodeCmd.strData.Substring(0, 5)).Replace(',', separator);
                                        Double dGetAngle = Convert.ToDouble(Source);*/

                                        //Double dGetAngle = Convert.ToDouble(tDecodeCmd.strData.Substring(0, 5), IFormatProvider provider)


                                        Double dGetAngle = Convert.ToDouble(tDecodeCmd.strData.Substring(0, 5), System.Globalization.NumberFormatInfo.InvariantInfo);

                                        

                                        GetAngleReceived(dGetAngle, 0);

                                        if (dGetAngle == dSetAngle)
                                        {
                                            STPReceived();

                                            //SendSetAngle(dSetAngle + (double)5, 0, false);
                                            StopThreadChangingAngle();

                                        }

                                        else
                                        {
                                            if (dGetAngleLast == dGetAngle)
                                            {
                                                bCountSameAngle++;
                                            }

                                            dGetAngleLast = dGetAngle;

                                            if (bCountSameAngle == 3)
                                            {
                                                STPReceived();

                                                //SendSetAngle(dSetAngle + (double)5, 0, false);
                                                StopThreadChangingAngle();

                                            }
                                        }
                                        
                                        
                                    }
                                    catch(SystemException)
                                    {

                                    }
                                    
                                    break;

                                case "ACK":

                                    ACKReceived();
                                    StartThreadChangingAngle();
                                    
                                    break;

                                case "ERR":
                                    ERRReceived();
                                    break;

                                default:
                                    break;
                            }

                            tDecodeCmd = FindCmd(ref strRead);
                            Thread.Sleep(5);
                        }

                    }

                    else
                        DisconnectPort();
                }

                catch (System.Exception ex)
                {

                    ClosePort();

                }

                Thread.Sleep(5);
            }
        }

        public TDecodeCmd FindCmd(ref String strReceive)
        {
            TDecodeCmd tDecodeCmd = new TDecodeCmd();
            
            String[] strCmd = { "OK", "ERR", "ACK" };

            Int32[] iIndexCmd = { -1, -1, -1 };
            Int32[] iLengthCmd = { 14, 4, 4 };

           
            try
            {
                for (int i = 0; i < strCmd.Length; i++)
                {
                    iIndexCmd[i] = strReceive.IndexOf(strCmd[i]);

                    if (iIndexCmd[i] == -1)
                        iIndexCmd[i] = 32000;
                }

                int iMinValue = iIndexCmd.Min(); //1
                int indexMin = Array.IndexOf(iIndexCmd, iMinValue);

                if (indexMin > -1)
                {
                    tDecodeCmd.strCode = strReceive.Substring(iIndexCmd[indexMin], iLengthCmd[indexMin]);

                    tDecodeCmd.strCode = strCmd[indexMin];
                    try
                    {
                        tDecodeCmd.strData = strReceive.Substring(strCmd[indexMin].Length, iLengthCmd[indexMin] - strCmd[indexMin].Length - 2);
                    }
                    catch(SystemException)
                    {}

                    strReceive = strReceive.Remove(0, iIndexCmd[indexMin]+iLengthCmd[indexMin]);

                }
                
            }

            
            catch(SystemException)
            {
                
            }

            return tDecodeCmd;
        }

        private bool WriteToARD(String strCmd)
        {
            
                try
                {
                    _port.WriteLine(strCmd);

                    WriteByte(Encoding.ASCII.GetBytes(strCmd));

                    return true;
                }
                catch (System.Exception ex)
                {
                    return false;
                }

        }

        private bool WriteToARD(byte[] bSend)
        {

            try
            {

                _port.Write(bSend, 0, bSend.Length);

                WriteByte(bSend);

                return true;
            }
            catch (System.Exception ex)
            {
                return false;
            }

        }

        // Represents the EN-US culture, used for numers in NMEA sentences
        public static CultureInfo NmeaCultureInfo = new CultureInfo("en-US");
        // Used to convert knots into miles per hour
        public static double MPHPerKnot = double.Parse("1.150779",
          NmeaCultureInfo);


        



      
      
        public void SendSetAngle(double dAngle, double dElevation, bool blAutoQuery)
        {
            bCountSameAngle = 0;
           
            String strSendSetAngleIns = dAngle.ToString("000.0") + " " + dElevation.ToString("000.0");

            byte[] bData = Encoding.ASCII.GetBytes(strSendSetAngleIns);


            byte[] bSend = new byte[3 + bData.Length];

            bSend[0] = 81;

            Array.Copy(bData, 0, bSend, 1, bData.Length);
            
            bSend[12] = 10;
            bSend[13] = 13;

            bSend[4] = 46;
            bSend[6] = 32;
            bSend[10] = 46;


            if (WriteToARD(bSend) && blAutoQuery)
            {
                dSetAngle = dAngle;
            }
        }

        private void StartThreadChangingAngle()
        {
            if (thrQuery != null)
                {
                    thrQuery.Abort();
                    thrQuery.Join(500);
                    thrQuery = null;
                }

                // load function of the thread for reading data from the port
                try
                {
                    thrQuery = new Thread(new ThreadStart(QueryAngle));
                    thrQuery.IsBackground = true;
                    thrQuery.Start();
                }
                catch (System.Exception ex)
                {

                }
        }

        private void StopThreadChangingAngle()
        {
            if (thrQuery != null)
            {
                thrQuery.Abort();
                thrQuery.Join(500);
                thrQuery = null;
            }

            
        }

        public void SendStop()
        {
            StopThreadChangingAngle();
            
            byte[] bSend = new byte[3];
            bSend[0] = 83;
            bSend[1] = 10;
            bSend[2] = 13;

            WriteToARD(bSend);
         
        }

        public void SendGetAngle()
        {
            byte[] bSend = new byte[3];
            bSend[0] = 89;
            bSend[1] = 10;
            bSend[2] = 13;
            WriteToARD(bSend);
        }

        private void QueryAngle()
        {
            while (true)
            {
                if (blSendQuery == false)
                {
                    SendGetAngle();
                    Thread.Sleep(TIME_QUERY);
                    //blSendQuery = true;
                }
                
            }

        }
    
    }
}
