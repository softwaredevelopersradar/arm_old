﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO.Ports;
using System.Threading;
using System.Globalization;

namespace CNTLib
{
    public class ComARD
    {
        private SerialPort _port;

        private Thread thrRead;

        String strRead = "";

        String strSendGetAngle = "Y<CR>";
        String strSendStop = "S<CR>";
        String strSendSetAngle = "Q<CR>";

        String strReceiveError = "ERR<CR>";
        String strReceiveAck = "ACK<CR>";
        String strReceiveOk = "OK<CR>";



        #region Delegates

        public delegate void ConnectEventHandler();
        public delegate void ByteEventHandler(byte[] bByte);
        public delegate void CmdEventHandler(object obj);


        public delegate void CMPReceivedEventHandler(double value);
        public delegate void GRSCPReceivedEventHandler(double valueX, double valueY, double valueZ);
        public delegate void BRReceivedEventHandler(double value);
        public delegate void ACMPReceivedEventHandler(double valueX, double valueY, double valueZ);
        #endregion

        #region Events
        
        public event CMPReceivedEventHandler CMPReceived;
        public event GRSCPReceivedEventHandler GRSCPReceived;
        public event BRReceivedEventHandler BRReceived;
        public event ACMPReceivedEventHandler ACMPReceived;

        public event ConnectEventHandler OnConnectPort;
        public event ConnectEventHandler OnDisconnectPort;

        public event ByteEventHandler OnReadByte;
        public event ByteEventHandler OnWriteByte;

        public event CmdEventHandler OnReceiveCmd;
        public event CmdEventHandler OnSendCmd;
        public event CmdEventHandler OnReceive_ERROR;
        #endregion


        #region Events
        // open port
        protected virtual void ConnectPort()
        {
            if (OnConnectPort != null)
            {
                OnConnectPort();//Raise the event
            }
        }

        // close port
        protected virtual void DisconnectPort()
        {
            if (OnDisconnectPort != null)
            {
                OnDisconnectPort();//Raise the event
            }
        }


        // receive error
        protected virtual void Receive_ERROR(object obj)
        {
            if (OnReceive_ERROR != null)
            {
                OnReceive_ERROR(obj);//Raise the event
            }
        }

        // read byte array
        protected virtual void ReadByte(byte[] bByte)
        {
            if (OnReadByte != null)
            {
                OnReadByte(bByte);//Raise the event
            }
        }

        // write byte array
        protected virtual void WriteByte(byte[] bByte)
        {
            if (OnWriteByte != null)
            {
                OnWriteByte(bByte);//Raise the event
            }
        }


        // receive cmd
        protected virtual void ReceiveCmd(object obj)
        {
            if (OnReceiveCmd != null)
            {
                OnReceiveCmd(obj);//Raise the event
            }
        }

        // send cmd
        protected virtual void SendCmd(object obj)
        {
            if (OnSendCmd != null)
            {
                OnSendCmd(obj);//Raise the event
            }
        }
        #endregion

        public void OpenPort(string portName, Int32 baudRate, System.IO.Ports.Parity parity, Int32 dataBits, System.IO.Ports.StopBits stopBits)
        {
            // Open COM port

            if (_port == null)
                _port = new SerialPort();

            // if port is open
            if (_port.IsOpen)

                // close it
                ClosePort();

            // try to open 
            try
            {
                // set parameters of port
                _port.PortName = portName;
                _port.BaudRate = baudRate;

                _port.Parity = parity;
                _port.DataBits = dataBits;
                _port.StopBits = stopBits;

                // set parameters of port
                /*  _port.PortName = portName;
                  _port.BaudRate = baudRate;

                  _port.Parity = System.IO.Ports.Parity.None;
                  _port.DataBits = 8;
                  _port.StopBits = System.IO.Ports.StopBits.One;*/


                // open it
                _port.Open();

                // create the thread for reading data from the port
                if (thrRead != null)
                {
                    thrRead.Abort();
                    thrRead.Join(500);
                    thrRead = null;
                }

                // load function of the thread for reading data from the port
                try
                {
                    thrRead = new Thread(new ThreadStart(ReadData));
                    thrRead.IsBackground = true;
                    thrRead.Start();
                }
                catch (System.Exception ex)
                {

                }

                // raise event
                // ConnectPort();

                // return true;

                ConnectPort();
            }
            catch (System.Exception ex)
            {
                DisconnectPort();

                //return false;
            }
        }


        // Close COM port
        public void ClosePort()
        {
            // clear in buffer
            try
            {
                _port.DiscardInBuffer();
            }
            catch (System.Exception ex)
            {


            }

            // clear in buffer
            try
            {
                _port.DiscardOutBuffer();
            }
            catch (System.Exception ex)
            {

            }

            try
            {
                // close port
                _port.Close();

                // destroy thread of reading
                if (thrRead != null)
                {
                    thrRead.Abort();
                    thrRead.Join(500);
                    thrRead = null;
                }

                // raise event
                DisconnectPort();
            }

            catch (System.Exception ex)
            {

            }

        }



        // Read data (byte) from port (thread)
        private void ReadData()
        {
            // buffer for reading bytes
            byte[] bBufRead;
            bBufRead = new byte[256];

            // buffer to transfer and save reading bytes
            byte[] bBufSave = new byte[2000];
            byte[] bBufSaveS;
            bBufSaveS = new byte[2000];

            // length of reading bytes
            int iReadByte = -1;

            // while port open
            while (true)
            {
                try
                {
                    bBufRead = new byte[256];

                    // clear burre of reading
                    Array.Clear(bBufRead, 0, bBufRead.Length);

                    iReadByte = 0;

                    // read data
                    iReadByte = _port.Read(bBufRead, 0, bBufRead.Length);

                    // if bytes was read
                    if (iReadByte > 0)
                    {

                        Array.Resize(ref bBufRead, iReadByte);
                        ReadByte(bBufRead);

                        strRead += Encoding.ASCII.GetString(bBufRead);

                        int iPosStart = strRead.IndexOf('$');
                        int iPosStop = strRead.IndexOf("\r\n");


                        while (iPosStart > -1 && iPosStop > -1 && iPosStop > iPosStart)
                        {
                            String strReply = strRead.Substring(iPosStart, iPosStop - iPosStart);

                            String strCode = strReply.Substring(1, 5);


                            switch (strCode)
                            {
                                case "GPRMC":
                                    ReceiveCmd(strReply);
                                    ParseGPRMC(strReply);
                                    break;

                                case "GPGGA":
                                    ReceiveCmd(strReply);
                                    break;

                                case "PAAG,":
                                    ParsePAAG(strReply);
                                    ReceiveCmd(strReply);
                                    break;

                                default:
                                    break;
                            }

                            strRead = strRead.Substring(iPosStop + 2, strRead.Length - iPosStop - 2);

                            iPosStart = strRead.IndexOf('$');
                            iPosStop = strRead.IndexOf("\r\n");

                        }

                    }

                    else
                        DisconnectPort();
                }

                catch (System.Exception ex)
                {

                    //ClosePort();

                }
            }
        }



        private void WriteToGPS(String strCmd)
        {
            if (_port != null)
            {
                try
                {
                    _port.WriteLine(strCmd);

                    WriteByte(Encoding.ASCII.GetBytes(strCmd));
                }
                catch (System.Exception ex)
                {

                }

                
            }


        }

        // Represents the EN-US culture, used for numers in NMEA sentences
        public static CultureInfo NmeaCultureInfo = new CultureInfo("en-US");
        // Used to convert knots into miles per hour
        public static double MPHPerKnot = double.Parse("1.150779",
          NmeaCultureInfo);


        // Processes information from the GPS receiver
        public bool Parse(string sentence)
        {
            // Discard the sentence if its checksum does not match our
            // calculated checksum
            if (!IsValid(sentence)) return false;
            // Look at the first word to decide where to go next
            switch (GetWords(sentence)[0])
            {
                case "$GPRMC":
                    // A "Recommended Minimum" sentence was found!
                    return ParseGPRMC(sentence);
                case "$GPGSV":
                    // A "Satellites in View" sentence was received
                    return ParseGPGSV(sentence);
                case "$GPGSA":
                    return ParseGPGSA(sentence);
                default:
                    // Indicate that the sentence was not recognized
                    return false;
            }
        }

        // Divides a sentence into individual words
        public string[] GetWords(string sentence)
        {
            return sentence.Split(',');
        }

        // Interprets a $GPRMC message
        public bool ParseGPRMC(string sentence)
        {
            // Divide the sentence into words
            string[] Words = GetWords(sentence);
            // Do we have enough values to describe our location?
            if (Words[3] != "" && Words[4] != "" &&

              Words[5] != "" && Words[6] != "")
            {
                // Yes. Extract latitude and longitude
                // Append hours
                string Latitude = Words[3].Substring(0, 2) + "°";
                // Append minutes
                Latitude = Latitude + Words[3].Substring(2) + "\"";
                // Append hours
                Latitude = Latitude + Words[4]; // Append the hemisphere


                string Longitude = Words[5].Substring(0, 3) + "°";
                // Append minutes
                Longitude = Longitude + Words[5].Substring(3) + "\"";
                // Append the hemisphere
                Longitude = Longitude + Words[6];


                //************* in Degrees Latitude**************** //
                double dLatD = Convert.ToDouble(Words[3].Substring(0, 2));
                String strLatM = Words[3].Substring(2);
                strLatM = strLatM.Replace('.', ',');
                double dLatM = Convert.ToDouble(strLatM);
                dLatD = dLatD + dLatM / 60;

                Latitude = dLatD.ToString();

                //*************************************************** //

                //************* in Degrees Longitude**************** //
                double dLongD = Convert.ToDouble(Words[3].Substring(0, 2));
                String strLongM = Words[3].Substring(2);
                strLongM = strLongM.Replace('.', ',');
                double dLongM = Convert.ToDouble(strLongM);
                dLongD = dLongD + dLongM / 60;

                Longitude = dLongD.ToString();

                //************* in Degrees **************** //
               
                // Notify the calling application of the change
                if (PositionReceived != null)
                    PositionReceived(Latitude, Longitude);
            }
            // Do we have enough values to parse satellite-derived time?
            if (Words[1] != "")
            {
                // Yes. Extract hours, minutes, seconds and milliseconds
                int UtcHours = Convert.ToInt32(Words[1].Substring(0, 2));
                int UtcMinutes = Convert.ToInt32(Words[1].Substring(2, 2));
                int UtcSeconds = Convert.ToInt32(Words[1].Substring(4, 2));
                int UtcMilliseconds = 0;
                // Extract milliseconds if it is available
                if (Words[1].Length > 7)
                {
                    UtcMilliseconds = Convert.ToInt32(
                        float.Parse(Words[1].Substring(6), NmeaCultureInfo) * 1000);
                }
                // Now build a DateTime object with all values
                System.DateTime Today = System.DateTime.Now.ToUniversalTime();
                System.DateTime SatelliteTime = new System.DateTime(Today.Year,
                  Today.Month, Today.Day, UtcHours, UtcMinutes, UtcSeconds,
                  UtcMilliseconds);
                // Notify of the new time, adjusted to the local time zone
                if (DateTimeChanged != null)
                    DateTimeChanged(SatelliteTime.ToLocalTime());
            }
            // Do we have enough information to extract the current speed?
            if (Words[7] != "")
            {
                // Yes.  Parse the speed and convert it to MPH
                double Speed = double.Parse(Words[7], NmeaCultureInfo) *
                  MPHPerKnot;
                // Notify of the new speed
                if (SpeedReceived != null)
                    SpeedReceived(Speed);
                // Are we over the highway speed limit?
                if (Speed > 55)
                    if (SpeedLimitReached != null)
                        SpeedLimitReached();
            }
            // Do we have enough information to extract bearing?
            if (Words[8] != "")
            {
                // Indicate that the sentence was recognized
                double Bearing = double.Parse(Words[8], NmeaCultureInfo);
                if (BearingReceived != null)
                    BearingReceived(Bearing);
            }
            // Does the device currently have a satellite fix?
            if (Words[2] != "")
            {
                switch (Words[2])
                {
                    case "A":
                        if (FixObtained != null)
                            FixObtained();
                        break;
                    case "V":
                        if (FixLost != null)
                            FixLost();
                        break;
                }
            }
            // Indicate that the sentence was recognized
            return true;
        }


        private bool ParsePAAG(string sentence)
        {
            double dGyroscopeX = 0;
            double dGyroscopeY = 0;
            double dGyroscopeZ = 0;

            double dHeadingDegree = 0;

            double dResultX = 0;
            double dResultY = 0;
            double dResultZ = 0;

            double dBarometer = 0;

            // Divide the sentence into words
            string[] Words = GetWords(sentence);

            switch (Words[2])
            {

                case "G":

                    try
                    {
                        dGyroscopeX = Convert.ToDouble(Words[4]) / 14.375;
                        dGyroscopeY = Convert.ToDouble(Words[5]) / 14.375;
                        dGyroscopeZ = Convert.ToDouble(Words[6]) / 14.375;
                    }
                    catch (System.Exception ex)
                    { }

                    if (GRSCPReceived != null)
                        GRSCPReceived(dGyroscopeX, dGyroscopeY, dGyroscopeZ);

                    break;

                case "C":

                    try
                    {
                        double dCompassX = Convert.ToDouble(Words[4]) / 1090;
                        double dCompassY = Convert.ToDouble(Words[5]) / 1090;
                        double dCompassZ = Convert.ToDouble(Words[6]) / 1090;

                        double dX = 0;
                        double dY = 0;
                        try
                        {
                            dY = Convert.ToDouble(Words[5]);
                            dX = Convert.ToDouble(Words[4]);
                        }

                        catch (System.Exception ex)
                        { }

                        double dHeadingRadian = Math.Atan(dY/ dX) +2 * Math.PI;

                        //if (dHeadingRadian > 2 * Math.PI)
                          //  dHeadingRadian -= 2 * Math.PI;
                        dHeadingDegree = dHeadingRadian * 180 / Math.PI;

                        if (dX > 0 && dY > 0)
                            dHeadingDegree += 180;
                        if (dX > 0 && dY < 0)
                            dHeadingDegree = 180 - dHeadingDegree;
                        if (dX < 0 && dY > 0)
                            dHeadingDegree = 360 - dHeadingDegree;


                    }
                    catch (System.Exception ex)
                    { }


                    if (CMPReceived != null)
                        CMPReceived(dHeadingDegree);
                    break;

                case "B":
                    Words[4] = Words[4].Replace('.', ',');
                    dBarometer = Convert.ToDouble(Words[4]);

                    if (BRReceived != null)
                        BRReceived(dBarometer);
                    break;

                case "T":
                    try
                    {
                        double dAccelerometerX = Convert.ToDouble(Words[4]) / 8192;
                        double dAccelerometerY = Convert.ToDouble(Words[5]) / 8192;
                        double dAccelerometerZ = Convert.ToDouble(Words[6]) / 8192;

                        double d_pi = 180.0 / Math.PI;


                        dResultX = -1 * Math.Atan2(dAccelerometerY, Math.Sqrt((dAccelerometerX * dAccelerometerX) + (dAccelerometerZ * dAccelerometerZ))) * d_pi;
                        dResultY = Math.Atan2(-1 * dAccelerometerX, (dAccelerometerZ < 0 ? -1 : 1) * Math.Sqrt((dAccelerometerY * dAccelerometerY) + (dAccelerometerZ * dAccelerometerZ))) * d_pi;
                        dResultZ = 0;
                    }

                    catch (System.Exception ex)
                    { }


                    if (ACMPReceived != null)
                        ACMPReceived(dResultX, dResultY, dResultZ);

                    break;

                default:
                    break;


            }

            return true;
        }




        // Interprets a "Satellites in View" NMEA sentence
        public bool ParseGPGSV(string sentence)
        {
            int PseudoRandomCode = 0;
            int Azimuth = 0;
            int Elevation = 0;
            int SignalToNoiseRatio = 0;
            // Divide the sentence into words
            string[] Words = GetWords(sentence);
            // Each sentence contains four blocks of satellite information.
            // Read each block and report each satellite's information
            int Count = 0;
            for (Count = 1; Count <= 4; Count++)
            {
                // Does the sentence have enough words to analyze?
                if ((Words.Length - 1) >= (Count * 4 + 3))
                {
                    // Yes.  Proceed with analyzing the block.
                    // Does it contain any information?
                    if (Words[Count * 4] != "" && Words[Count * 4 + 1] != ""

                       && Words[Count * 4 + 2] != "" && Words[Count * 4 + 3] != "")
                    {
                        // Yes. Extract satellite information and report it
                        PseudoRandomCode = System.Convert.ToInt32(Words[Count * 4]);
                        Elevation = Convert.ToInt32(Words[Count * 4 + 1]);
                        Azimuth = Convert.ToInt32(Words[Count * 4 + 2]);
                        SignalToNoiseRatio = Convert.ToInt32(Words[Count * 4 + 3]);
                        // Notify of this satellite's information
                        if (SatelliteReceived != null)
                            SatelliteReceived(PseudoRandomCode, Azimuth,
                            Elevation, SignalToNoiseRatio);
                    }
                }
            }
            // Indicate that the sentence was recognized
            return true;
        }

        

        // Returns True if a sentence's checksum matches the
        // calculated checksum
        public bool IsValid(string sentence)
        {
            // Compare the characters after the asterisk to the calculation
            return sentence.Substring(sentence.IndexOf("*") + 1) ==
              GetChecksum(sentence);
        }

        public void SendStart()
        {
            WriteToGPS(strSendStart);
        }

        public void SendStop()
        {
            WriteToGPS(strSendStop);
        }

        public void SendGetOne()
        {
            WriteToGPS(strSendGetOne);
        }

        // Calculates the checksum for a sentence
        public string GetChecksum(string sentence)
        {
            // Loop through all chars to get a checksum
            int Checksum = 0;
            foreach (char Character in sentence)
            {
                if (Character == '$')
                {
                    // Ignore the dollar sign
                }
                else if (Character == '*')
                {
                    // Stop processing before the asterisk
                    break;
                }
                else
                {
                    // Is this the first value for the checksum?
                    if (Checksum == 0)
                    {
                        // Yes. Set the checksum to the value
                        Checksum = Convert.ToByte(Character);
                    }
                    else
                    {
                        // No. XOR the checksum with this character's value
                        Checksum = Checksum ^ Convert.ToByte(Character);
                    }
                }
            }
            // Return the checksum formatted as a two-character hexadecimal
            return Checksum.ToString("X2");
        }
    
    }
}
