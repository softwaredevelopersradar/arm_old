﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using USR_DLL;

namespace VariableStatic
{
    public class VariableConnection
    {
        string strNameFile = "Connection.ini";
        static CIni cIni;
        static string strExePath = "";
        

        # region  NameSection
        string strSectionDirectionFinder = "DirectionFinder";
        string strSectionJammerLinked = "JammerLinked";
        string strSectionPostControl = "PostControl";
        string strSectionFHS = "FHS";
        string strSectionReceiverAOR = "ReceiverAOR";
        string strSectionCompassStatic = "CompassStatic";
        string strSectionCompassDynamic = "CompassDynamic";
        string strSectionRotateDevicePC = "RotateDevicePC";
        string strSectionRotateDeviceJL = "RotateDeviceJL";
        string strSectionRotateDeviceLPA = "RotateDeviceLPA";
        string strSectionGNSS = "GNSS";
        string strSectionADS = "ADS";
        string strSectionDSP = "DSP";
        string strSectionReceiverMain = "ReceiverMain";
        string strSectionSimulatorIntell = "SimulatorIntell";
        string strSectionHS = "HS";//hardware station
        string strSectionASP = "ASP";
        # endregion


        # region  DirectionFinder
        static private string strDirectionFinderAddress = "";
        static private int iDirectionFinderPort = 0;
        static private int iPanoramaSpectrumRequest = 100;
        # endregion

        # region  PostControl
        private static byte bPostControlType = 0;
        
        private static string strPcIpAddressRRS = "";
        private static int iPcPortRRS = 0;

        private static string strPcIpAddress3G = "";
        private static int iPcPort3G = 0;

        private static string strPcComRRs = "";
        private static string strPcComModem = "";
        private static int iPostControlBaudRateRRS = 0;
        private static int iPostControlBaudRateModem = 0;


        private static byte bPostControlDataBits = 0;
        private static string bPostControlStopBits = "";
        private static string strPostControlParity = "";
        # endregion
              
        # region StructPcCom
        public struct StructPc
        {
            public string Connect;

            public string Address;
            public int Port;

            public string Com;
            public int BaudRate;
            public byte DataBits;
            public string StopBits;
            public string Parity;
        }
        public static StructPc structPc;
        # endregion 
        
        # region StructHS
        public struct StructHS
        {
            public string Addres;
            public int Port;
        }
        public static StructHS structHS;
        # endregion
        # region StructASP
        public struct StructASP
        {
            public string Address;
            public int Port;

            public string Com;
            public int BaudRate;
            public byte DataBits;
            public string StopBits;
            public string Parity;
        }
        public static StructASP structASP;
        # endregion


        # region  ReceiverAOR
        static private byte bReceiverAORCom = 0;
        static private int iReceiverAORBaudRate = 0;
        static private byte bReceiverAORDataBits = 0;
        static private byte bReceiverAORStopBits = 0;
        static private string strReceiverAORParity = "";
        # endregion

        # region  CompassStatic
        static private byte bCompassStaticCom = 0;
        static private int iCompassStaticBaudRate = 0;
        static private byte bCompassStaticDataBits = 0;
        static private byte bCompassStaticStopBits = 0;
        static private string strCompassStaticParity = "";
        # endregion

        # region  CompassDynamic
        static private byte bCompassDynamicCom = 0;
        static private int iCompassDynamicBaudRate = 0;
        static private byte bCompassDynamicDataBits = 0;
        static private byte bCompassDynamicStopBits = 0;
        static private string strCompassDynamicParity = "";
        # endregion

        # region  RotateDevicePC
        static private byte bRotateDevicePCCom = 0;
        static private int iRotateDevicePCBaudRate = 0;
        static private byte bRotateDevicePCDataBits = 0;
        static private byte bRotateDevicePCStopBits = 0;
        static private string strRotateDevicePCParity = "";
        # endregion

        # region  RotateDeviceJL
        static private byte bRotateDeviceJLCom = 0;
        static private int iRotateDeviceJLBaudRate = 0;
        static private byte bRotateDeviceJLDataBits = 0;
        static private byte bRotateDeviceJLStopBits = 0;
        static private string strRotateDeviceJLParity = "";
        # endregion

        # region  RotateDeviceLPA
        static private byte bRotateDeviceLPACom = 0;
        static private int iRotateDeviceLPABaudRate = 0;
        static private byte bRotateDeviceLPADataBits = 0;
        static private byte bRotateDeviceLPAStopBits = 0;
        static private string strRotateDeviceLPAParity = "";
        # endregion

        # region  GNSS
        static private byte bGNSSCom = 0;
        static private int iGNSSBaudRate = 0;
        static private byte bGNSSDataBits = 0;
        static private byte bGNSSStopBits = 0;
        static private string strGNSSParity = "";
        # endregion

        # region  ADS
        static private string strADSAddress = "";
        static private int iADSPort = 0;      
        # endregion

        # region  DSP
        static private string strDSPAddress = "";
        static private int iDSPPort = 0;
        # endregion
        # region ASP
        static private byte bAspType = 0;
        static private string strAspAddressRRS = "";
        static private int iAspPortRRS = 0;
        static private string strAspAddress3G = "";
        static private int iAspPort3G = 0;
        private static int iAspBaudRateModem = 0;
        private static byte bAspDataBits = 0;
        private static string bAspStopBits = "";
        private static string strAspParity = "";
        private static string strAspComModem = "";
        static private string strAspAddressOF = "";
        static private int iAspPortOF = 0;
        # endregion

        # region HS
        static private byte bHSType = 0;
        static private string strHSAddressRRS = "";
        static private int iHSPortRRS = 0;
        static private string strHSAddress3G = "";
        static private int iHSPort3G = 0;
        # endregion

        # region  ReceiverMain
        static private string strReceiverMainAddress = "";
        static private int iReceiverMainPort = 0;
        # endregion

        # region  SimulatorIntell
        static private string strSimulatorIntellAddress = "";
        static private int iSimulatorIntellPort = 0;
        # endregion

        # region  FHS
        static private string strAddressARM = "";
        static private string strAddressFHS = "";
        static private int iPortARM = 0;
        static private int iPortFHS = 0;
        static private int iCountErrBytes = 0;
        static private int iLengthInfoPart = 0;      
        # endregion
        #region Event
        public delegate void ChangeConnectionEventHandler();

        public static ChangeConnectionEventHandler OnChangeTypeHS;
        public static ChangeConnectionEventHandler OnChangeTypePostControl;
        public static ChangeConnectionEventHandler OnChangeTypeASP;
        #endregion

        public VariableConnection()
        {
            if (strExePath == "")
            {
                strExePath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
                cIni = new CIni(strExePath + "\\INI\\" + strNameFile);

                InitVariable();
            }
        }


        private void InitVariable()
        {          
            InitVariableDirectionFinder(strSectionDirectionFinder);
            InitVariablePostControl(strSectionPostControl);
            InitVariableFHS(strSectionFHS);
            InitVariableReceiverAOR(strSectionReceiverAOR);
            InitVariableCompassStatic(strSectionCompassStatic);
            InitVariableCompassDynamic(strSectionCompassDynamic);
            InitVariableRotateDevicePC(strSectionRotateDevicePC);
            InitVariableRotateDeviceJL(strSectionRotateDeviceJL);
            InitVariableRotateDeviceLPA(strSectionRotateDeviceLPA);
            InitVariableGNSS(strSectionGNSS);
            InitVariableADS(strSectionADS);
            InitVariableDSP(strSectionDSP);
            InitVariableReceiverMain(strSectionReceiverMain);
            InitVariableSimulatorIntell(strSectionSimulatorIntell);
            InitVariableHS(strSectionHS);
            InitVariableASP(strSectionASP);
        }

        private void InitVariableDirectionFinder(string strSection)
        {
            strDirectionFinderAddress = cIni.ReadString(strSection, "Address ");
            iDirectionFinderPort = cIni.ReadInt(strSection, "Port   ", 1);
            iPanoramaSpectrumRequest = cIni.ReadInt(strSection, "PanoramaSpectrumRequest ", 100);
        }


        private void InitVariablePostControl(string strSection)
        {
            bPostControlType = cIni.ReadByte(strSection, "Type  ", 0);
            strPcIpAddressRRS = cIni.ReadString(strSection, "IPAddressRRS ");
            iPcPortRRS = cIni.ReadInt(strSection, "PortRRs ", 1);
            strPcIpAddress3G = cIni.ReadString(strSection, "IPAddress3G ");
            iPcPort3G = cIni.ReadInt(strSection, "Port3G ", 1);
            strPcComRRs = cIni.ReadString(strSection, "ComRRS ");
            strPcComModem = cIni.ReadString(strSection, "ComModem ");
            iPostControlBaudRateRRS = cIni.ReadInt(strSection, "BaudRateRRS    ", 1);
            iPostControlBaudRateModem = cIni.ReadInt(strSection, "BaudRateModem    ", 1);
            bPostControlDataBits = cIni.ReadByte(strSection, "DataBits  ", 111);
            bPostControlStopBits = cIni.ReadString(strSection, "StopBits  ");
            strPostControlParity = cIni.ReadString(strSection, "Parity  ");

        }


        private void InitVariableReceiverAOR(string strSection)
        {           
            bReceiverAORCom = cIni.ReadByte(strSection, "Com ", 111);
            iReceiverAORBaudRate = cIni.ReadInt(strSection, "BaudRate    ", 1);
            bReceiverAORDataBits = cIni.ReadByte(strSection, "DataBits  ", 111);
            bReceiverAORStopBits = cIni.ReadByte(strSection, "StopBits  ", 111);
            strReceiverAORParity = cIni.ReadString(strSection, "Parity  ");

        }

        private void InitVariableCompassStatic(string strSection)
        {
            bCompassStaticCom = cIni.ReadByte(strSection, "Com ", 111);
            iCompassStaticBaudRate = cIni.ReadInt(strSection, "BaudRate    ", 1);
            bCompassStaticDataBits = cIni.ReadByte(strSection, "DataBits  ", 111);
            bCompassStaticStopBits = cIni.ReadByte(strSection, "StopBits  ", 111);
            strCompassStaticParity = cIni.ReadString(strSection, "Parity  ");

        }

        private void InitVariableCompassDynamic(string strSection)
        {
            bCompassDynamicCom = cIni.ReadByte(strSection, "Com ", 111);
            iCompassDynamicBaudRate = cIni.ReadInt(strSection, "BaudRate    ", 1);
            bCompassDynamicDataBits = cIni.ReadByte(strSection, "DataBits  ", 111);
            bCompassDynamicStopBits = cIni.ReadByte(strSection, "StopBits  ", 111);
            strCompassDynamicParity = cIni.ReadString(strSection, "Parity  ");

        }

        private void InitVariableRotateDevicePC(string strSection)
        {
            bRotateDevicePCCom = cIni.ReadByte(strSection, "Com ", 111);
            iRotateDevicePCBaudRate = cIni.ReadInt(strSection, "BaudRate    ", 1);
            bRotateDevicePCDataBits = cIni.ReadByte(strSection, "DataBits  ", 111);
            bRotateDevicePCStopBits = cIni.ReadByte(strSection, "StopBits  ", 111);
            strRotateDevicePCParity = cIni.ReadString(strSection, "Parity  ");
        }

        private void InitVariableRotateDeviceJL(string strSection)
        {
            bRotateDeviceJLCom = cIni.ReadByte(strSection, "Com ", 111);
            iRotateDeviceJLBaudRate = cIni.ReadInt(strSection, "BaudRate    ", 1);
            bRotateDeviceJLDataBits = cIni.ReadByte(strSection, "DataBits  ", 111);
            bRotateDeviceJLStopBits = cIni.ReadByte(strSection, "StopBits  ", 111);
            strRotateDeviceJLParity = cIni.ReadString(strSection, "Parity  ");
        }

        private void InitVariableRotateDeviceLPA(string strSection)
        {
            bRotateDeviceLPACom = cIni.ReadByte(strSection, "Com ", 111);
            iRotateDeviceLPABaudRate = cIni.ReadInt(strSection, "BaudRate    ", 1);
            bRotateDeviceLPADataBits = cIni.ReadByte(strSection, "DataBits  ", 111);
            bRotateDeviceLPAStopBits = cIni.ReadByte(strSection, "StopBits  ", 111);
            strRotateDeviceLPAParity = cIni.ReadString(strSection, "Parity  ");
        }

        private void InitVariableGNSS(string strSection)
        {
            bGNSSCom = cIni.ReadByte(strSection, "Com ", 111);
            iGNSSBaudRate = cIni.ReadInt(strSection, "BaudRate    ", 1);
            bGNSSDataBits = cIni.ReadByte(strSection, "DataBits  ", 111);
            bGNSSStopBits = cIni.ReadByte(strSection, "StopBits  ", 111);
            strGNSSParity = cIni.ReadString(strSection, "Parity  ");
        }

        private void InitVariableADS(string strSection)
        {                    
            strADSAddress = cIni.ReadString(strSection, "Address ");
            iADSPort = cIni.ReadInt(strSection, "Port   ", 1);
        }

        private void InitVariableDSP(string strSection)
        {
            strDSPAddress = cIni.ReadString(strSection, "Address ");
            iDSPPort = cIni.ReadInt(strSection, "Port   ", 1);
        }

        private void InitVariableReceiverMain(string strSection)
        {
            strReceiverMainAddress = cIni.ReadString(strSection, "Address ");
            iReceiverMainPort = cIni.ReadInt(strSection, "Port   ", 1);
        }

        private void InitVariableSimulatorIntell(string strSection)
        {
            strSimulatorIntellAddress = cIni.ReadString(strSection, "Address ");
            iSimulatorIntellPort = cIni.ReadInt(strSection, "Port   ", 1);
        }
        private void InitVariableHS(string strSection)
        {
            bHSType = cIni.ReadByte(strSection, "Type", 0);
            strHSAddressRRS = cIni.ReadString(strSection, "RRSAddress");
            iHSPortRRS = cIni.ReadInt(strSection, "RRSPort", 1);
            strHSAddress3G = cIni.ReadString(strSection, "3GAddress");
            iHSPort3G = cIni.ReadInt(strSection, "3GPort", 1);
        }
        private void InitVariableFHS(string strSection)
        {
            strAddressARM = cIni.ReadString(strSection, "AddressARM");
            strAddressFHS = cIni.ReadString(strSection, "AddressFHS");
            iPortARM = cIni.ReadInt(strSection, "PortARM", 0);
            iPortFHS = cIni.ReadInt(strSection, "PortFHS", 0);
            iCountErrBytes = cIni.ReadInt(strSection, "CountErrBytes", 0);
            iLengthInfoPart = cIni.ReadInt(strSection, "LengthInfoPart", 0);
        }
        private void InitVariableASP(string strSection)
        {
            bAspType = cIni.ReadByte(strSection, "Type", 0);
            strAspAddressRRS = cIni.ReadString(strSection, "RRSAddress");
            iAspPortRRS = cIni.ReadInt(strSection, "RRSPort", 1);
            strAspAddress3G = cIni.ReadString(strSection, "3GAddress");
            iAspPort3G = cIni.ReadInt(strSection, "3GPort", 1);
            strAspAddressOF = cIni.ReadString(strSection, "OpticAddress");
            iAspPortOF = cIni.ReadInt(strSection, "OpticPort", 1);

            strAspComModem = cIni.ReadString(strSection, "ComModem ");
            iAspBaudRateModem = cIni.ReadInt(strSection, "BaudRateModem    ", 1);
            bAspDataBits = cIni.ReadByte(strSection, "DataBits  ", 111);
            bAspStopBits = cIni.ReadString(strSection, "StopBits  ");
            strAspParity = cIni.ReadString(strSection, "Parity  ");
        }

        # region  PropertyDirectionFinder

        public string DirectionFinderAddress
        {
            get { return strDirectionFinderAddress; }
            set { strDirectionFinderAddress = value; }
        }

        public int DirectionFinderPort
        {
            get { return iDirectionFinderPort; }
            set { iDirectionFinderPort = value; }
        }

        public int PanoramaSpecrumRequest
        {
            get { return iPanoramaSpectrumRequest; }
            set { iPanoramaSpectrumRequest = value; }
        }

        # endregion


        # region  PropertyPostControl

        public byte PostControlType
        {
            get { return bPostControlType; }
            set
            {
                if (bPostControlType != value)
                {
                    bPostControlType = value;
                    cIni.WriteByte(strSectionPostControl, "Type  ", bPostControlType);
                    if (OnChangeTypePostControl != null)
                        OnChangeTypePostControl();
                }
            }
        }
        public byte HSType
        {
            get { return bHSType; }
            set 
            {
                if (bHSType != value)
                {
                    bHSType = value;
                    cIni.WriteByte(strSectionHS, "Type ", bHSType);
                    if (OnChangeTypeHS != null)
                        OnChangeTypeHS();
                }
            }
        }
        public byte ASPType
        {
            get { return bAspType; }
            set
            {
                if (bAspType != value)
                {
                    bAspType = value;
                    cIni.WriteByte(strSectionASP, "Type ", bAspType);

                    if (OnChangeTypeASP != null)
                    {
                        OnChangeTypeASP();
                    }

                }
            }
        }
        public StructHS HSStruct
        {
            get
            {
                switch (bHSType)
                {
                    case 1: //RRS
                        structHS.Addres = strHSAddressRRS;
                        structHS.Port = iHSPortRRS;
                        break;
                    case 2://3G
                        structHS.Addres = strHSAddress3G;
                        structHS.Port = iHSPort3G;
                        break;
                }
                return structHS;
            }
        }

        public StructPc PostControlStruct
        {
            get
            {
                switch (bPostControlType)
                {
                    case 1:
                        structPc.Connect = "RS";
                        structPc.Com = strPcComRRs; 
                        structPc.Address = null;
                        structPc.Port = 255;
                        structPc.Parity = strPostControlParity;
                        structPc.StopBits = bPostControlStopBits;
                        structPc.DataBits = bPostControlDataBits;
                        structPc.BaudRate = iPostControlBaudRateRRS;                       
                        break;
                    case 2:
                        structPc.Connect = "RS";
                        structPc.Com = strPcComModem;
                        structPc.Address = null;
                        structPc.Port = 255;
                        structPc.Parity = strPostControlParity;
                        structPc.StopBits = bPostControlStopBits;
                        structPc.DataBits = bPostControlDataBits;
                        structPc.BaudRate = iPostControlBaudRateModem;
                        break;
                    case 3:
                        structPc.Connect = "Eth";
                        structPc.Address = strPcIpAddress3G;
                        structPc.Port = iPcPort3G;
                        structPc.Parity = "";
                        structPc.StopBits = null;
                        structPc.DataBits = 255;
                        structPc.Com = null;
                        structPc.BaudRate = 255;
                        break;
                    case 4:
                        structPc.Connect = "Eth";
                        structPc.Address = strPcIpAddressRRS;
                        structPc.Port = iPcPortRRS;
                        structPc.Parity = "";
                        structPc.StopBits = null;
                        structPc.DataBits = 255;
                        structPc.Com = null;
                        structPc.BaudRate = 255;
                        break;
                }
                return structPc;
            }
        }
        public StructASP ASPStruct
        {
            get
            {
                switch (bAspType)
                {
                    case 1://RRS (Eth)
                        structASP.Address = strAspAddressRRS + ":" + iAspPortRRS.ToString();
                        structASP.Port = iAspPortRRS;
                        structASP.Parity = "";
                        structASP.StopBits = null;
                        structASP.DataBits = 255;
                        structASP.Com = null;
                        structASP.BaudRate = 255;
                        break;
                    case 2: //3G (Eth)
                        structASP.Address = strAspAddress3G + ":" + iAspPort3G.ToString();
                        structASP.Port = iAspPort3G;
                        structASP.Parity = "";
                        structASP.StopBits = null;
                        structASP.DataBits = 255;
                        structASP.Com = null;
                        structASP.BaudRate = 255;
                        break;
                    case 3: //Modem (RS232)
                        structASP.Com = strAspComModem;
                        structASP.BaudRate = iAspBaudRateModem;
                        structASP.Address = strAspComModem;
                        structASP.Port = 255;
                        structASP.Parity = strAspParity;
                        structASP.StopBits = bAspStopBits;
                        structASP.DataBits = bAspDataBits;
                        break;
                    case 4: //Optical fibre (Eth)
                        structASP.Address = strAspAddressOF + ":" + iAspPortOF.ToString();
                        structASP.Port = iAspPortOF;
                        structASP.Parity = "";
                        structASP.StopBits = null;
                        structASP.DataBits = 255;
                        structASP.Com = null;
                        structASP.BaudRate = 255;
                        break;
                }
                return structASP;
            }
        }
        /*public string PostControlAddress
        {
            get { return strPostControlAddress; }
            set { strPostControlAddress = value; }
        }

        public int PostControlPort
        {
            get { return iPostControlPort; }
            set { iPostControlPort = value; }
        }

        public byte PostControlCom
        {
            get { return bPostControlCom; }
            set { bPostControlCom = value; }
        }
        */
        /*public int PostControlBaudRate
        {
            get { return iPostControlBaudRate; }
            set 
            { 
                iPostControlBaudRate = value;
                structPc.BaudRate = iPostControlBaudRate;
            }
        }*/

        public byte PostControlDataBits
        {
            get { return bPostControlDataBits; }
            set 
            {
                bPostControlDataBits = value;
                structPc.DataBits = bPostControlDataBits;
            }
        }

        public string PostControlStopBits
        {
            get { return bPostControlStopBits; }
            set
            {
                bPostControlStopBits = value;
                structPc.StopBits = bPostControlStopBits;
            }
        }

        public string PostControlParity
        {
            get { return strPostControlParity; }
            set 
            { 
                strPostControlParity = value;
                structPc.Parity = strPostControlParity;
            }
        }

        # endregion

        # region  PropertyFHS

        public string AddressFPS
        {
            get { return strAddressFHS; }
            set { strAddressFHS = value; }
        }
        public string AddressARM
        {
            get { return strAddressARM; }
            set { strAddressARM = value; }
        }

        public int PortARM
        {
            get { return iPortARM; }
            set { iPortARM = value; }
        }

        public int PortFPS
        {
            get { return iPortFHS; }
            set { iPortFHS = value; }
        }

        public int CountErrBytes
        {
            get { return iCountErrBytes; }
            set { iCountErrBytes = value; }
        }

        public int LengthInfoPart
        {
            get { return iLengthInfoPart; }
            set { iLengthInfoPart = value; }
        }
        # endregion

        # region  PropertyReceiverAOR

        

        public byte ReceiverAORCom
        {
            get { return bReceiverAORCom; }
            set { bReceiverAORCom = value; }
        }

        public int ReceiverAORBaudRate
        {
            get { return iReceiverAORBaudRate; }
            set { iReceiverAORBaudRate = value; }
        }

        public byte ReceiverAORDataBits
        {
            get { return bReceiverAORDataBits; }
            set { bReceiverAORDataBits = value; }
        }

        public byte ReceiverAORStopBits
        {
            get { return bReceiverAORStopBits; }
            set { bReceiverAORStopBits = value; }
        }

        public string ReceiverAORParity
        {
            get { return strReceiverAORParity; }
            set { strReceiverAORParity = value; }
        }

        # endregion

        # region  PropertyCompassStatic



        public byte CompassStaticCom
        {
            get { return bCompassStaticCom; }
            set { bCompassStaticCom = value; }
        }

        public int CompassStaticBaudRate
        {
            get { return iCompassStaticBaudRate; }
            set { iCompassStaticBaudRate = value; }
        }

        public byte CompassStaticDataBits
        {
            get { return bCompassStaticDataBits; }
            set { bCompassStaticDataBits = value; }
        }

        public byte CompassStaticStopBits
        {
            get { return bCompassStaticStopBits; }
            set { bCompassStaticStopBits = value; }
        }

        public string CompassStaticParity
        {
            get { return strCompassStaticParity; }
            set { strCompassStaticParity = value; }
        }

        # endregion

        # region  PropertyCompassDynamic



        public byte CompassDynamicCom
        {
            get { return bCompassDynamicCom; }
            set { bCompassDynamicCom = value; }
        }

        public int CompassDynamicBaudRate
        {
            get { return iCompassDynamicBaudRate; }
            set { iCompassDynamicBaudRate = value; }
        }

        public byte CompassDynamicDataBits
        {
            get { return bCompassDynamicDataBits; }
            set { bCompassDynamicDataBits = value; }
        }

        public byte CompassDynamicStopBits
        {
            get { return bCompassDynamicStopBits; }
            set { bCompassDynamicStopBits = value; }
        }

        public string CompassDynamicParity
        {
            get { return strCompassDynamicParity; }
            set { strCompassDynamicParity = value; }
        }

        # endregion

        # region  PropertyRotateDevicePC



        public byte RotateDevicePCCom
        {
            get { return bRotateDevicePCCom; }
            set { bRotateDevicePCCom = value; }
        }

        public int RotateDevicePCBaudRate
        {
            get { return iRotateDevicePCBaudRate; }
            set { iRotateDevicePCBaudRate = value; }
        }

        public byte RotateDevicePCDataBits
        {
            get { return bRotateDevicePCDataBits; }
            set { bRotateDevicePCDataBits = value; }
        }

        public byte RotateDevicePCStopBits
        {
            get { return bRotateDevicePCStopBits; }
            set { bRotateDevicePCStopBits = value; }
        }

        public string RotateDevicePCParity
        {
            get { return strRotateDevicePCParity; }
            set { strRotateDevicePCParity = value; }
        }

        # endregion

        # region  PropertyRotateDeviceJL



        public byte RotateDeviceJLCom
        {
            get { return bRotateDeviceJLCom; }
            set { bRotateDeviceJLCom = value; }
        }

        public int RotateDeviceJLBaudRate
        {
            get { return iRotateDeviceJLBaudRate; }
            set { iRotateDeviceJLBaudRate = value; }
        }

        public byte RotateDeviceJLDataBits
        {
            get { return bRotateDeviceJLDataBits; }
            set { bRotateDeviceJLDataBits = value; }
        }

        public byte RotateDeviceJLStopBits
        {
            get { return bRotateDeviceJLStopBits; }
            set { bRotateDeviceJLStopBits = value; }
        }

        public string RotateDeviceJLParity
        {
            get { return strRotateDeviceJLParity; }
            set { strRotateDeviceJLParity = value; }
        }

        # endregion

        # region  PropertyRotateDeviceLPA



        public byte RotateDeviceLPACom
        {
            get { return bRotateDeviceLPACom; }
            set { bRotateDeviceLPACom = value; }
        }

        public int RotateDeviceLPABaudRate
        {
            get { return iRotateDeviceLPABaudRate; }
            set { iRotateDeviceLPABaudRate = value; }
        }

        public byte RotateDeviceLPADataBits
        {
            get { return bRotateDeviceLPADataBits; }
            set { bRotateDeviceLPADataBits = value; }
        }

        public byte RotateDeviceLPAStopBits
        {
            get { return bRotateDeviceLPAStopBits; }
            set { bRotateDeviceLPAStopBits = value; }
        }

        public string RotateDeviceLPAParity
        {
            get { return strRotateDeviceLPAParity; }
            set { strRotateDeviceLPAParity = value; }
        }

        # endregion

        # region  PropertyGNSS



        public byte GNSSCom
        {
            get { return bGNSSCom; }
            set { bGNSSCom = value; }
        }

        public int GNSSBaudRate
        {
            get { return iGNSSBaudRate; }
            set { iGNSSBaudRate = value; }
        }

        public byte GNSSDataBits
        {
            get { return bGNSSDataBits; }
            set { bGNSSDataBits = value; }
        }

        public byte GNSSStopBits
        {
            get { return bGNSSStopBits; }
            set { bGNSSStopBits = value; }
        }

        public string GNSSParity
        {
            get { return strGNSSParity; }
            set { strGNSSParity = value; }
        }

        # endregion

        # region  PropertyADS
        public string ADSAddress
        {
            get { return strADSAddress; }
            set { strADSAddress = value; }
        }

        public int ADSPort
        {
            get { return iADSPort; }
            set { iADSPort = value; }
        }
        # endregion

        # region  PropertyDSP
        public string DSPAddress
        {
            get { return strDSPAddress; }
            set { strDSPAddress = value; }
        }

        public int DSPPort
        {
            get { return iDSPPort; }
            set { iDSPPort = value; }
        }
        # endregion

        # region  PropertyReceiverMain
        public string ReceiverMainAddress
        {
            get { return strReceiverMainAddress; }
            set { strReceiverMainAddress = value; }
        }

        public int ReceiverMainPort
        {
            get { return iReceiverMainPort; }
            set { iReceiverMainPort = value; }
        }
        # endregion

        # region  PropertySimulatorIntell
        public string SimulatorIntellAddress
        {
            get { return strSimulatorIntellAddress; }
            set { strSimulatorIntellAddress = value; }
        }

        public int SimulatorIntellPort
        {
            get { return iSimulatorIntellPort; }
            set { iSimulatorIntellPort = value; }
        }
        # endregion



    }
}
