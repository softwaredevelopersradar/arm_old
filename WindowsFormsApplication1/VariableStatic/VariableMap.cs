﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using USR_DLL;

namespace VariableStatic
{
    class VariableMap
    {
        string strNameFile = "Map.ini";
        static CIni cIni;
        static string strExePath = "";

        # region  NameSection
        string strSectionCommon = "Common";      
        # endregion

        # region  Common
        private string strMapPath = "";
        private int iScale = 0;
        private byte bSystemCoord = 0;
        private int iCenterCoordX = 0;
        private int iCenterCoordY = 0;        
        # endregion

       
        public VariableMap()
        {
            if (strExePath == "")
            {
                strExePath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
                cIni = new CIni(strExePath + "\\INI\\" + strNameFile);

                InitVariable();
            }
        }

        private void InitVariable()
        {
            InitVariableCommon(strSectionCommon);            
        }

        private void InitVariableCommon(string strSection)
        {
            strMapPath = cIni.ReadString(strSection, "MapPath ");
            iScale = cIni.ReadInt(strSection, "Scale    ", 1);
            bSystemCoord = cIni.ReadByte(strSection, "SystemCoord  ", 0);
            iCenterCoordX = cIni.ReadInt(strSection, "CenterCoordX    ", 1);
            iCenterCoordY = cIni.ReadInt(strSection, "CenterCoordY    ", 1);
            
        }

        # region  PropertyQueryJammerLinked

        public string MapPath
        {
            get { return strMapPath; }
            set 
            {
                strMapPath = value;
                cIni.WriteString(strSectionCommon, "MapPath ", strMapPath);                
            }
        }

        public int Scale
        {
            get { return iScale; }
            set 
            { 
                iScale = value;
                cIni.WriteInt(strSectionCommon, "Scale ", iScale);
            }
        }

        public byte SystemCoord
        {
            get { return bSystemCoord; }
            set 
            {
                bSystemCoord = value;
                cIni.WriteByte(strSectionCommon, "SystemCoord  ", bSystemCoord);
            }

        }

        public int CenterCoordX
        {
            get { return iCenterCoordX; }
            set
            { 
                iCenterCoordX = value;
                cIni.WriteInt(strSectionCommon, "CenterCoordX  ", iCenterCoordX);
            }
        }

        public int CenterCoordY
        {
            get { return iCenterCoordY; }
            set 
            { 
                iCenterCoordY = value;
                cIni.WriteInt(strSectionCommon, "CenterCoordY   ", iCenterCoordY);
            }
        }

        
        # endregion
    }
}
