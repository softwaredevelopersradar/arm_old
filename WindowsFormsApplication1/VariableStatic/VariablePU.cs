﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using USR_DLL;


namespace VariableStatic
{
    public class VariablePU
    {
        string strNameFile = "PU.ini";
        CIni cIni;

        # region  NameSection
        string strSectionCommon = "Common";
        string strSectionTypeStation = "TypeStation";
        # endregion
        #region TypeStation
        static string sBel = "";
        static string sAzer = "";
        #endregion
        public VariablePU()
        {
            string strExePath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            cIni = new CIni(strExePath + "\\INI\\" + strNameFile);
            InitVariablePU(strSectionTypeStation);
        }
        public int NumPacket
        {
            get
            {
                return cIni.ReadInt(strSectionCommon, "NumPacket ", 50);
            }
        }
        public int TimePeriod
        {
            get
            {
                return cIni.ReadInt(strSectionCommon, "TimePeriod ", 500);
            }
        }
        void InitVariablePU(string strSection)
        {
            sBel = cIni.ReadString(strSection, "Bel");
            sAzer = cIni.ReadString(strSection, "Azer");
        }
        public string Bel
        {
            get { return sBel; }           
        }
        public string Azer
        {
            get { return sAzer; }
        }

    }
}
