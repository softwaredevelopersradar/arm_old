﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using USR_DLL;


namespace VariableStatic
{
    public class VariableColor
    {
        string strNameFile = "Color.ini";
        static CIni cIni;
        static string strExePath = "";

        # region  NameSection
        string strSectionPanorama = "Panorama";
        string strSectionObject = "Object";      
        # endregion

        # region  Panorama
        private static string strBackGround = "";
        private static string strThreshold = "";
        private static string strCross = "";
        private static string strGrid = "";
        private static string strLabel = "";
        private static string strGlobalBackground = "";
        private static string strGraphFA = "";
        private static string strGraphFD = "";
        private static string strGraphFT = "";

        private static string strGraphFT0 = "";
        private static string strGraphFT1 = "";
        # endregion

        # region  Object
        private static string strRange30_88 = "";
        private static string strRange88_108 = "";
        private static string strRange108_170 = "";
        private static string strRange170_220 = "";
        private static string strRange220_400 = "";
        private static string strRange400_512 = "";
        private static string strRange512_860 = "";
        private static string strRange860_1215 = "";
        private static string strRange1215_1575 = "";
        private static string strRange1575_3000 = "";
        # endregion


        #region Event
        public delegate void ChangeColorEventHandler();

        public static event ChangeColorEventHandler OnChangeColorPanorama;
        public static event ChangeColorEventHandler OnChangeColorObject;
        #endregion



        public VariableColor()
        {
            if (strExePath == "")
            {
                strExePath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
                cIni = new CIni(strExePath + "\\INI\\" + strNameFile);

                InitVariable();
            }
        }


        private void InitVariable()
        {
            InitVariablePanorama(strSectionPanorama);
            InitVariableObject(strSectionObject);            
        }


        private void InitVariablePanorama(string strSection)
        {
           
            strBackGround = cIni.ReadString(strSection, "BackGround");
            strThreshold = cIni.ReadString(strSection, "Threshold ");
            strCross = cIni.ReadString(strSection, "Cross");
            strGrid = cIni.ReadString(strSection, "Grid ");
            strLabel = cIni.ReadString(strSection, "Label");
            strGlobalBackground = cIni.ReadString(strSection, "GlobalBackground");
            strGraphFA = cIni.ReadString(strSection, "GraphFA");
            strGraphFD = cIni.ReadString(strSection, "GraphFD");
            strGraphFT = cIni.ReadString(strSection, "GraphFT");
            strGraphFT0 = cIni.ReadString(strSection, "GraphFT0");
            strGraphFT1 = cIni.ReadString(strSection, "GraphFT1");  
        }

        private void InitVariableObject(string strSection)
        {
            strRange30_88 = cIni.ReadString(strSection, "Range30-88");
            strRange88_108 = cIni.ReadString(strSection, "Range88-108 ");
            strRange108_170 = cIni.ReadString(strSection, "Range108-170");
            strRange170_220 = cIni.ReadString(strSection, "Range170-220 ");
            strRange220_400 = cIni.ReadString(strSection, "Range220-400");
            strRange400_512 = cIni.ReadString(strSection, "Range400-512");
            strRange512_860 = cIni.ReadString(strSection, "Range512-860");
            strRange860_1215 = cIni.ReadString(strSection, "Range860-1215");
            strRange1215_1575 = cIni.ReadString(strSection, "Range1215-1575");
            strRange1575_3000 = cIni.ReadString(strSection, "Range1575-3000");
        }


        # region  PropertyPanorama


        public string BackGround
        {
            get { return strBackGround; }
            set 
            {
                if (strBackGround != value)
                {
                    strBackGround = value;
                    cIni.WriteString(strSectionPanorama, "BackGround", strBackGround);
                    if (OnChangeColorPanorama != null)
                        OnChangeColorPanorama();
                }
            }
        }

        public string Threshold
        {
            get { return strThreshold; }
            set 
            {
                if (strThreshold != value)
                {
                    strThreshold = value;
                    cIni.WriteString(strSectionPanorama, "Threshold", strThreshold);
                    if (OnChangeColorPanorama != null)
                        OnChangeColorPanorama();
                }
            }
        }

        public string Cross
        {
            get { return strCross; }
            set 
            {
                if (strCross != value)
                {
                    strCross = value;
                    cIni.WriteString(strSectionPanorama, "Cross", strCross);
                    if (OnChangeColorPanorama != null)
                        OnChangeColorPanorama();
                }
            }
        }

        public string Grid
        {
            get { return strGrid; }
            set 
            {
                if (strGrid != value)
                {
                    strGrid = value;
                    cIni.WriteString(strSectionPanorama, "Grid ", strGrid);
                    if (OnChangeColorPanorama != null)
                        OnChangeColorPanorama();
                }
            }
        }

        public string Label
        {
            get { return strLabel; }
            set 
            {
                if (strLabel != value)
                {
                    strLabel = value;
                    cIni.WriteString(strSectionPanorama, "Label ", strLabel);
                    if (OnChangeColorPanorama != null)
                        OnChangeColorPanorama();
                }
            }
        }

        public string GlobalBackGround
        {
            get { return strGlobalBackground; }
            set 
            {
                if (strGlobalBackground != value)
                {
                    strGlobalBackground = value;
                    cIni.WriteString(strSectionPanorama, "GlobalBackground ", strGlobalBackground);
                    if (OnChangeColorPanorama != null)
                        OnChangeColorPanorama();
                }
            }
        }

        public string GraphFA
        {
            get { return strGraphFA; }
            set 
            {
                if (strGraphFA != value)
                {
                    strGraphFA = value;
                    cIni.WriteString(strSectionPanorama, "GraphFA  ", strGraphFA);
                    if (OnChangeColorPanorama != null)
                        OnChangeColorPanorama();
                }
            }
        }

        public string GraphFD
        {
            get { return strGraphFD; }
            set
            {
                if (strGraphFD != value)
                {
                    strGraphFD = value;
                    cIni.WriteString(strSectionPanorama, "GraphFD   ", strGraphFD);
                    if (OnChangeColorPanorama != null)
                        OnChangeColorPanorama();
                }
            }
        }

        public string GraphFT
        {
            get { return strGraphFT; }
            set 
            {
                if (strGraphFT != value)
                {
                    strGraphFT = value;
                    cIni.WriteString(strSectionPanorama, "GraphFT    ", strGraphFT);
                    if (OnChangeColorPanorama != null)
                        OnChangeColorPanorama();
                }
            }
        }

        public string GraphFT0
        {
            get { return strGraphFT0; }
            set
            {
                if (strGraphFT0 != value)
                {
                    strGraphFT0 = value;
                    cIni.WriteString(strSectionPanorama, "GraphFT0    ", strGraphFT0);
                    if (OnChangeColorPanorama != null)
                        OnChangeColorPanorama();
                }
            }
        }

        public string GraphFT1
        {
            get { return strGraphFT1; }
            set
            {
                if (strGraphFT1 != value)
                {
                    strGraphFT1 = value;
                    cIni.WriteString(strSectionPanorama, "GraphFT1    ", strGraphFT1);
                    if (OnChangeColorPanorama != null)
                        OnChangeColorPanorama();
                }
            }
        }

        # endregion


        # region  PropertyObject


        public string Range30_88
        {
            get { return strRange30_88; }
            set
            {
                if (strRange30_88 != value)
                {
                    strRange30_88 = value;
                    cIni.WriteString(strSectionObject, "Range30-88", strRange30_88);
                    if (OnChangeColorObject != null)
                        OnChangeColorObject();
                }
            }
        }

        public string Range88_108
        {
            get { return strRange88_108; }
            set 
            {
                if (strRange88_108 != value)
                {
                    strRange88_108 = value;
                    cIni.WriteString(strSectionObject, "Range88-108", strRange88_108);
                    if (OnChangeColorObject != null)
                        OnChangeColorObject();
                }
            }
        }

        public string Range108_170
        {
            get { return strRange108_170; }
            set 
            {
                if (strRange108_170 != value)
                {
                    strRange108_170 = value;
                    cIni.WriteString(strSectionObject, "Range108-170", strRange108_170);
                    if (OnChangeColorObject != null)
                        OnChangeColorObject();
                }
            }
        }

        public string Range170_220
        {
            get { return strRange170_220; }
            set 
            {
                if (strRange170_220 != value)
                {
                    strRange170_220 = value;
                    cIni.WriteString(strSectionObject, "Range170-220", strRange170_220);
                    if (OnChangeColorObject != null)
                        OnChangeColorObject();
                }
            }
        }

        public string Range220_400
        {
            get { return strRange220_400; }
            set 
            {
                if (strRange220_400 != value)
                {
                    strRange220_400 = value;
                    cIni.WriteString(strSectionObject, "Range220-400", strRange220_400);
                    if (OnChangeColorObject != null)
                        OnChangeColorObject();
                }
            }
        }

        public string Range400_512
        {
            get { return strRange400_512; }
            set 
            {
                if (strRange400_512 != value)
                {
                    strRange400_512 = value;
                    cIni.WriteString(strSectionObject, "Range400-512", strRange400_512);
                    if (OnChangeColorObject != null)
                        OnChangeColorObject();
                }
            }
        }

        public string Range512_860
        {
            get { return strRange512_860; }
            set 
            {
                if (strRange512_860 != value)
                {
                    strRange512_860 = value;
                    cIni.WriteString(strSectionObject, "Range512-860", strRange512_860);
                    if (OnChangeColorObject != null)
                        OnChangeColorObject();
                }
            }
        }

        public string Range860_1215
        {
            get { return strRange860_1215; }
            set
            {
                if (strRange860_1215 != value)
                {
                    strRange860_1215 = value;
                    cIni.WriteString(strSectionObject, "Range860-1215", strRange860_1215);
                    if (OnChangeColorObject != null)
                        OnChangeColorObject();
                }
            }
        }

        public string Range1215_1575
        {
            get { return strRange1215_1575; }
            set 
            {
                if (strRange1215_1575 != value)
                {
                    strRange1215_1575 = value;
                    cIni.WriteString(strSectionObject, "Range1215-1575", strRange1215_1575);
                    if (OnChangeColorObject != null)
                        OnChangeColorObject();
                }
            }
        }

        public string Range1575_3000
        {
            get { return strRange1575_3000; }
            set 
            {
                if (strRange1575_3000 != value)
                {
                    strRange1575_3000 = value;
                    cIni.WriteString(strSectionObject, "Range1575-3000", strRange1575_3000);
                    if (OnChangeColorObject != null)
                        OnChangeColorObject();
                }
            }
        }

        # endregion
    }
}
