﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using USR_DLL;

namespace VariableStatic
{
    public class VariableIntellegence
    {
        string strNameFile = "Intellegence.ini";
        static CIni cIni;
        static string strExePath = "";

        # region  NameSection
        string strSectionCalibration = "Calibration";
        string strSectionADSB = "ADSB";
        string strSectionCommon = "Common";
        # endregion

        # region  Calibration
        static private string strPathRadioTract = "";
        static private int iScan = 0;
        static private int iStep = 0;
        static private int iTimeLearn = 0;
        # endregion

        # region  ADSB
        static private int iTimeMonitor = 0;
        # endregion

        # region  Common
        static private byte bBearing = 0;
        static private int iAveraging = 0;
        static private byte bDetectionPPRCH = 0;
        static private int iZeroVibrAngle = 90;
        static private int iRelativeBearing = 0;
        static private byte bAutoRelativeBearing = 0;
        # endregion


        #region Event
        public delegate void ChangeIntellegenceEventHandler();
        public static event ChangeIntellegenceEventHandler OnChangeBearing;
        public static event ChangeIntellegenceEventHandler OnChangeAveraging;
        public static event ChangeIntellegenceEventHandler OnChangeDetectionPPRCH;
        public static event ChangeIntellegenceEventHandler OnChangeZeroVibrAngle;
        public static event ChangeIntellegenceEventHandler OnChangeRelativeBearing;
        public static event ChangeIntellegenceEventHandler OnChangeAutoRelativeBearing;
        # endregion

        public VariableIntellegence()
        {
            if (strExePath == "")
            {
                strExePath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
                cIni = new CIni(strExePath + "\\INI\\" + strNameFile);

                InitVariable();
            }
        }

        private void InitVariable()
        {
            InitVariableCalibration(strSectionCalibration);
            InitVariableADSB(strSectionADSB);
            InitVariableCommon(strSectionCommon);
        }

        private void InitVariableCalibration(string strSection)
        {
            strPathRadioTract = cIni.ReadString(strSection, "PathRadioTract  ");
            iScan = cIni.ReadInt(strSection, "Scan ", 1);
            iStep = cIni.ReadInt(strSection, "Step ", 1);
            iTimeLearn = cIni.ReadInt(strSection, "TimeLearn ", 1);

        }

        private void InitVariableADSB(string strSection)
        {
            iTimeMonitor = cIni.ReadInt(strSection, "TimeMonitor", 100);            
        }

        private void InitVariableCommon(string strSection)
        {
            bBearing = cIni.ReadByte(strSection, "Bearing", 0);
            iAveraging = cIni.ReadInt(strSection, "Averaging", 0);
            bDetectionPPRCH = cIni.ReadByte(strSection, "DetectionPPRCH", 0);
            iZeroVibrAngle = cIni.ReadInt(strSection, "ZeroVibrAngle", 90);
            iRelativeBearing = cIni.ReadInt(strSection, "RelativeBearing", 0);
            bAutoRelativeBearing = cIni.ReadByte(strSection, "AutoRelativeBearing", 0);
        }


        # region  PropertyCalibration

        public string PathRadioTract
        {
            get { return strPathRadioTract; }
            set
            {
                strPathRadioTract = value;

            }
        }

        public int Scan
        {
            get { return iScan; }
            set
            {
                iScan = value;
                cIni.WriteInt(strSectionCalibration, "Scan", iScan);
            }
        }

        public int Step
        {
            get { return iStep; }
            set
            {
                iStep = value;
                cIni.WriteInt(strSectionCalibration, "Step ", iStep);
            }
        }

        public int TimeLearn
        {
            get { return iTimeLearn; }
            set
            {
                iTimeLearn = value;
                cIni.WriteInt(strSectionCalibration, "TimeLearn", iTimeLearn);
            }
        }

        # endregion

        # region  PropertyADSB


        public int TimeMonitor
        {
            get { return iTimeMonitor; }
            set
            {
                iTimeMonitor = value;
                cIni.WriteInt(strSectionADSB, "TimeMonitor", iTimeMonitor);
            }
        }

        public byte Bearing
        {
            get { return bBearing; }
            set
            {
                if (bBearing != value)
                {
                    bBearing = value;
                    cIni.WriteByte(strSectionCommon, "Bearing", bBearing);
                    if (OnChangeBearing != null)
                    {
                        OnChangeBearing();
                    }
                }
            }
        }

        public int Averaging
        {
            get { return iAveraging; }
            set
            {
                if (iAveraging != value)
                {
                    iAveraging = value;
                    cIni.WriteInt(strSectionCommon, "Averaging", iAveraging);
                    if (OnChangeAveraging != null)
                    {
                        OnChangeAveraging();
                    }
                }
            }
        }
        public byte DetectionPPRCH
        {
            get { return bDetectionPPRCH; }
            set
            {
                if (bDetectionPPRCH != value)
                {
                    bDetectionPPRCH = value;
                    cIni.WriteByte(strSectionCommon, "DetectionPPRCH", bDetectionPPRCH);
                    if (OnChangeDetectionPPRCH != null)
                    {
                        OnChangeDetectionPPRCH();
                    }
                }
            }
        }

        public int ZeroVibrAngle
        {
            get { return iZeroVibrAngle; }
            set
            {
                if (iZeroVibrAngle != value)
                {
                    iZeroVibrAngle = value;
                    cIni.WriteInt(strSectionCommon, "ZeroVibrAngle", iZeroVibrAngle);
                    if (OnChangeZeroVibrAngle != null)
                    {
                        OnChangeZeroVibrAngle();
                    }
                }
            }
        }

        public int RelativeBearing
        {
            get { return iRelativeBearing; }
            set
            {
                if (iRelativeBearing != value)
                {
                    iRelativeBearing = value;
                    cIni.WriteInt(strSectionCommon, "RelativeBearing", iRelativeBearing);
                    if (OnChangeRelativeBearing != null)
                    {
                        OnChangeRelativeBearing();
                    }
                }
            }
        }
        public byte AutoRelativeBearing
        {
            get { return bAutoRelativeBearing; }
            set
            {
                if (bAutoRelativeBearing != value)
                {
                    bAutoRelativeBearing = value;
                    cIni.WriteByte(strSectionCommon, "AutoRelativeBearing", bAutoRelativeBearing);
                    if (OnChangeAutoRelativeBearing != null)
                    {
                        OnChangeAutoRelativeBearing();
                    }
                }
            }
        }
        # endregion
    }
}
