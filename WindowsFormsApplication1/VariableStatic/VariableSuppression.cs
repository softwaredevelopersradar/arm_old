﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using USR_DLL;

namespace VariableStatic
{
    public class VariableSuppression
    {
        string strNameFile = "Suppression.ini";
        static CIni cIni;
        static string strExePath = "";

        # region  NameSection
        string strSectionDistribution = "Distribution";
        string strSectionOwnLetter = "OwnLetter";
        string strSectionJammerLinkedLetter = "JammerLinkedLetter";
        string strSectionSuppressionFWS = "SuppressionFWS";
        string strSectionSuppressionFHSS = "SuppressionFHSS";
        string strSectionAmplifier = "Amplifier";
        string strAPRCH = "APRCH";
        # endregion

        # region  Distribution
        private static byte bCountOwnLetter = 0;
        private static byte bCountJammerLinkedLetter = 0;
        private static byte bCheckSubrangeSector = 0;
        private static byte bSourceOrder = 0;
        private static byte bDeleteSourceSuppr = 0;
        private static byte bCountSourceLetter = 0;
        private static byte bAutoClearTable = 0;
        # endregion

        # region  OwnLetter
        private static byte bOwnLetter1 = 0;
        private static byte bOwnLetter2 = 0;
        private static byte bOwnLetter3 = 0;
        private static byte bOwnLetter4 = 0;
        private static byte bOwnLetter5 = 0;
        private static byte bOwnLetter6 = 0;
        private static byte bOwnLetter7 = 0;
        private static byte bOwnLetter8 = 0;
        private static byte bOwnLetter9 = 0;
        private static byte[] bOwnLetters = new byte[9];
        # endregion

        # region  JammerLinkedLetter
        private static byte bJammerLinkedLetter1 = 0;
        private static byte bJammerLinkedLetter2 = 0;
        private static byte bJammerLinkedLetter3 = 0;
        private static byte bJammerLinkedLetter4 = 0;
        private static byte bJammerLinkedLetter5 = 0;
        private static byte bJammerLinkedLetter6 = 0;
        private static byte bJammerLinkedLetter7 = 0;
        private static byte bJammerLinkedLetter8 = 0;
        private static byte bJammerLinkedLetter9 = 0;
        private static byte[] bLinkedLetters = new byte[9];
        # endregion

        # region  SuppressionFWS
        private static byte bRegimeRadioSuppr = 0;
        private static short sTimeRadiatFWS = 0;
        private static byte bChannelUse = 0;
        private static short sThresholdDefault = 0;
        private static byte bCircle = 0;
        private static byte bPriorHandle = 0;
        private static byte bUP = 0;
        # endregion

        # region  SuppressionFHSS
        private static short sTimeRadiatFHSS = 0;
        private static byte bCodeFFT = 0;        
        # endregion

        # region  Amplifier
        private static short sPowerMin = 0;
        private static short sPowerMax = 0;
        private static short sVoltageMin = 0;
        private static short sVoltageMax = 0;
        private static short sTempMin = 0;
        private static short sTempMax = 0;
        private static short sCurrentMin = 0;
        private static short sCurrentMax = 0;
        # endregion

        #region APRCH
        private static int iSearchArc = 0;
        private static int iSearchTime = 0;
        private static int iSearchBand = 0;
        #endregion

        #region Event
        public delegate void ChangeSuppressionEventHandler();

        public static event ChangeSuppressionEventHandler OnChangeSuppressionLetterOwn;
        public static event ChangeSuppressionEventHandler OnChangeSuppressionLetterJammerLinked;
        public static event ChangeSuppressionEventHandler OnChangeTimeRadiatFWS;
        public static event ChangeSuppressionEventHandler OnChangeRegimeRadioSuppr;
        public static event ChangeSuppressionEventHandler OnChangeSearchArc;
        public static event ChangeSuppressionEventHandler OnChangeSearchTime;
        public static event ChangeSuppressionEventHandler OnChangeSearchBand;
        #endregion


        public VariableSuppression()
        {
            if (strExePath == "")
            {
                strExePath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
                cIni = new CIni(strExePath + "\\INI\\" + strNameFile);

                InitVariable();
            }
        }


        private void InitVariable()
        {
            InitVariableDistribution(strSectionDistribution);
            InitVariableOwnLetter(strSectionOwnLetter);
            InitVariableJammerLinkedLetter(strSectionJammerLinkedLetter);
            InitVariableSuppressionFWS(strSectionSuppressionFWS);
            InitVariableSuppressionFHSS(strSectionSuppressionFHSS);
            InitVariableAmplifier(strSectionAmplifier);
            InitVariableAPRCH(strAPRCH);
        }

        private void InitVariableDistribution(string strSection)
        {
            bCountOwnLetter = cIni.ReadByte(strSection, "CountOwnLetter  ", 5);
            bCountJammerLinkedLetter = cIni.ReadByte(strSection, "CountJammerLinkedLetter  ", 5);
            bCheckSubrangeSector = cIni.ReadByte(strSection, "CheckSubrangeSector  ", 0);
            bSourceOrder = cIni.ReadByte(strSection, "SourceOrder  ", 0);
            bDeleteSourceSuppr = cIni.ReadByte(strSection, "DeleteSourceSuppr  ", 0);
            bCountSourceLetter = cIni.ReadByte(strSection, "CountSourceLetter  ", 10);
            bAutoClearTable = cIni.ReadByte(strSection, "AutoClearTable", 0);
        }

        private void InitVariableOwnLetter(string strSection)
        {
            bOwnLetter1 = cIni.ReadByte(strSection, "Letter1   ", 0);
            bOwnLetter2 = cIni.ReadByte(strSection, "Letter2   ", 0);
            bOwnLetter3 = cIni.ReadByte(strSection, "Letter3   ", 0);
            bOwnLetter4 = cIni.ReadByte(strSection, "Letter4   ", 0);
            bOwnLetter5 = cIni.ReadByte(strSection, "Letter5   ", 0);
            bOwnLetter6 = cIni.ReadByte(strSection, "Letter6   ", 0);
            bOwnLetter7 = cIni.ReadByte(strSection, "Letter7   ", 0);
            bOwnLetter8 = cIni.ReadByte(strSection, "Letter8   ", 0);
            bOwnLetter9 = cIni.ReadByte(strSection, "Letter9   ", 0);
        }

        private void InitVariableJammerLinkedLetter(string strSection)
        {
            bJammerLinkedLetter1 = cIni.ReadByte(strSection, "Letter1   ", 0);
            bJammerLinkedLetter2 = cIni.ReadByte(strSection, "Letter2   ", 0);
            bJammerLinkedLetter3 = cIni.ReadByte(strSection, "Letter3   ", 0);
            bJammerLinkedLetter4 = cIni.ReadByte(strSection, "Letter4   ", 0);
            bJammerLinkedLetter5 = cIni.ReadByte(strSection, "Letter5   ", 0);
            bJammerLinkedLetter6 = cIni.ReadByte(strSection, "Letter6   ", 0);
            bJammerLinkedLetter7 = cIni.ReadByte(strSection, "Letter7   ", 0);
            bJammerLinkedLetter8 = cIni.ReadByte(strSection, "Letter8   ", 0);
            bJammerLinkedLetter9 = cIni.ReadByte(strSection, "Letter9   ", 0);
        }

        private void InitVariableSuppressionFWS(string strSection)
        {
            bRegimeRadioSuppr = cIni.ReadByte(strSection, "RegimeRadioSuppr", 0);
            sTimeRadiatFWS = Convert.ToInt16(cIni.ReadInt(strSection, "TimeRadiat ", 800));
            bChannelUse = cIni.ReadByte(strSection, "ChannelUse  ", 4);
            sThresholdDefault = Convert.ToInt16(cIni.ReadInt(strSection, "ThresholdDefault ", 90));
            bCircle = cIni.ReadByte(strSection, "Circle  ", 10);
            bPriorHandle = cIni.ReadByte(strSection, "PriorHandle", 1);
            bUP = cIni.ReadByte(strSection, "UP", 0);
            
        }

        private void InitVariableSuppressionFHSS(string strSection)
        {
            sTimeRadiatFHSS = Convert.ToInt16(cIni.ReadInt(strSection, "TimeRadiat  ", 111));
            bCodeFFT = cIni.ReadByte(strSection, "CodeFFT   ", 0);
           
        }

        private void InitVariableAmplifier(string strSection)
        {
            sPowerMin = Convert.ToInt16(cIni.ReadInt(strSection, "PowerMin ", 111));
            sPowerMax = Convert.ToInt16(cIni.ReadInt(strSection, "PowerMax  ", 1));
            sVoltageMin = Convert.ToInt16(cIni.ReadInt(strSection, "VoltageMin  ", 111));
            sVoltageMax = Convert.ToInt16(cIni.ReadInt(strSection, "VoltageMax   ", 1));
            sTempMin = Convert.ToInt16(cIni.ReadInt(strSection, "TempMin ", 111));
            sTempMax = Convert.ToInt16(cIni.ReadInt(strSection, "TempMax  ", 1));
            sCurrentMin = Convert.ToInt16(cIni.ReadInt(strSection, "CurrentMin ", 111));
            sCurrentMax = Convert.ToInt16(cIni.ReadInt(strSection, "CurrentMax  ", 1));
            
        }

        private void InitVariableAPRCH(string strSection)
        {
            iSearchArc = cIni.ReadInt(strSection, "SearchArc", 100);
            if (iSearchArc > 180 || iSearchArc < 1)
                SearchArc = 100;
            iSearchTime = cIni.ReadInt(strSection, "SearchTime", 1);
            if (iSearchTime > 255 || iSearchTime < 0)
                SearchTime = 1;
            iSearchBand = cIni.ReadInt(strSection, "SearchBand", 1);
            if (iSearchBand > 30 || iSearchBand < 0)
                SearchBand = 1;
        }

        # region  PropertyDistribution


        public byte CountOwnLetter
        {
            get { return bCountOwnLetter; }
            set
            { 
                bCountOwnLetter = value;
                cIni.WriteByte(strSectionDistribution, "CountOwnLetter ", bCountOwnLetter);
            }
        }

        public byte CountJammerLinkedLetter
        {
            get { return bCountJammerLinkedLetter; }
            set 
            { 
                bCountJammerLinkedLetter = value;
                cIni.WriteByte(strSectionDistribution, "CountJammerLinkedLetter ", bCountJammerLinkedLetter);
            }
        }

        public byte CheckSubrangeSector
        {
            get { return bCheckSubrangeSector; }
            set 
            { 
                bCheckSubrangeSector = value;
                cIni.WriteByte(strSectionDistribution, "CheckSubrangeSector ", bCheckSubrangeSector);
            }
        }

        public byte SourceOrder
        {
            get { return bSourceOrder; }
            set { bSourceOrder = value; }
        }

        public byte DeleteSourceSuppr
        {
            get { return bDeleteSourceSuppr; }
            set 
            { 
                bDeleteSourceSuppr = value;
                cIni.WriteByte(strSectionDistribution, "DeleteSourceSuppr  ", bDeleteSourceSuppr);
            }
        }

        public byte CountSourceLetter
        {
            get { return bCountSourceLetter; }
            set
            {
                bCountSourceLetter = value;
                cIni.WriteByte(strSectionDistribution, "CountSourceLetter  ", bCountSourceLetter);
            }
        }
        public byte AutoClearTable
        {
            get { return bAutoClearTable; }
            set 
            {
                if (bAutoClearTable != value)
                {
                    bAutoClearTable = value;
                    cIni.WriteByte(strSectionDistribution, "AutoClearTable", bAutoClearTable);
                }
            }
        }
        # endregion

        # region  PropertyOwnLetter
        public byte[] OwnLetters
        {
            get 
            {
                bOwnLetters[0] = bOwnLetter1;
                bOwnLetters[1] = bOwnLetter2;
                bOwnLetters[2] = bOwnLetter3;
                bOwnLetters[3] = bOwnLetter4;
                bOwnLetters[4] = bOwnLetter5;
                bOwnLetters[5] = bOwnLetter6;
                bOwnLetters[6] = bOwnLetter7;
                bOwnLetters[7] = bOwnLetter8;
                bOwnLetters[8] = bOwnLetter9;
                return bOwnLetters;
            }
        }

        public byte OwnLetter1
        {
            get { return bOwnLetter1; }
            set 
            {
                if (bOwnLetter1 != value)
                {
                    bOwnLetter1 = value;
                    cIni.WriteByte(strSectionOwnLetter, "Letter1   ", bOwnLetter1);
                    if (OnChangeSuppressionLetterOwn != null)
                        OnChangeSuppressionLetterOwn();
                }
            }
        }

        public byte OwnLetter2
        {
            get { return bOwnLetter2; }
            set 
            {
                if (bOwnLetter2 != value)
                {
                    bOwnLetter2 = value;
                    cIni.WriteByte(strSectionOwnLetter, "Letter2   ", bOwnLetter2);
                    if (OnChangeSuppressionLetterOwn != null)
                        OnChangeSuppressionLetterOwn();
                }
            }
        }

        public byte OwnLetter3
        {
            get { return bOwnLetter3; }
            set 
            {
                if (bOwnLetter3 != value)
                {
                    bOwnLetter3 = value;
                    cIni.WriteByte(strSectionOwnLetter, "Letter3  ", bOwnLetter3);
                    if (OnChangeSuppressionLetterOwn != null)
                        OnChangeSuppressionLetterOwn();
                }
            }
        }

        public byte OwnLetter4
        {
            get { return bOwnLetter4; }
            set 
            {
                if (bOwnLetter4 != value)
                {
                    bOwnLetter4 = value;
                    cIni.WriteByte(strSectionOwnLetter, "Letter4  ", bOwnLetter4);
                    if (OnChangeSuppressionLetterOwn != null)
                        OnChangeSuppressionLetterOwn();
                }
            }
        }

        public byte OwnLetter5
        {
            get { return bOwnLetter5; }
            set 
            {
                if (bOwnLetter5 != value)
                {
                    bOwnLetter5 = value;
                    cIni.WriteByte(strSectionOwnLetter, "Letter5  ", bOwnLetter5);
                    if (OnChangeSuppressionLetterOwn != null)
                        OnChangeSuppressionLetterOwn();
                }
            }
        }

        public byte OwnLetter6
        {
            get { return bOwnLetter6; }
            set 
            {
                if (bOwnLetter6 != value)
                {
                    bOwnLetter6 = value;
                    cIni.WriteByte(strSectionOwnLetter, "Letter6  ", bOwnLetter6);
                    if (OnChangeSuppressionLetterOwn != null)
                        OnChangeSuppressionLetterOwn();
                }
            }
        }

        public byte OwnLetter7
        {
            get { return bOwnLetter7; }
            set 
            {
                if (bOwnLetter7 != value)
                {
                    bOwnLetter7 = value;
                    cIni.WriteByte(strSectionOwnLetter, "Letter7  ", bOwnLetter7);
                    if (OnChangeSuppressionLetterOwn != null)
                        OnChangeSuppressionLetterOwn();
                }
            }
        }

        public byte OwnLetter8
        {
            get { return bOwnLetter8; }
            set 
            {
                if (bOwnLetter8 != value)
                {
                    bOwnLetter8 = value;
                    cIni.WriteByte(strSectionOwnLetter, "Letter8  ", bOwnLetter8);
                    if (OnChangeSuppressionLetterOwn != null)
                        OnChangeSuppressionLetterOwn();
                }
            }
        }

        public byte OwnLetter9
        {
            get { return bOwnLetter9; }
            set 
            {
                if (bOwnLetter9 != value)
                {
                    bOwnLetter9 = value;
                    cIni.WriteByte(strSectionOwnLetter, "Letter9  ", bOwnLetter9);
                    if (OnChangeSuppressionLetterOwn != null)
                        OnChangeSuppressionLetterOwn();
                }
            }
        }

        
        # endregion

        # region  PropertyJammerLinkedLetter

        public byte[] LinkedLetters
        {
            get
            {
                bLinkedLetters[0] = bJammerLinkedLetter1;
                bLinkedLetters[1] = bJammerLinkedLetter2;
                bLinkedLetters[2] = bJammerLinkedLetter3;
                bLinkedLetters[3] = bJammerLinkedLetter4;
                bLinkedLetters[4] = bJammerLinkedLetter5;
                bLinkedLetters[5] = bJammerLinkedLetter6;
                bLinkedLetters[6] = bJammerLinkedLetter7;
                bLinkedLetters[7] = bJammerLinkedLetter8;
                bLinkedLetters[8] = bJammerLinkedLetter9;
                return bLinkedLetters;
            }
        }

        public byte JammerLinkedLetter1
        {
            get { return bJammerLinkedLetter1; }
            set 
            {
                if (bJammerLinkedLetter1 != value)
                {
                    bJammerLinkedLetter1 = value;
                    cIni.WriteByte(strSectionJammerLinkedLetter, "Letter1  ", bJammerLinkedLetter1);
                    if (OnChangeSuppressionLetterJammerLinked != null)
                        OnChangeSuppressionLetterJammerLinked();
                }
            }
        }

        public byte JammerLinkedLetter2
        {
            get { return bJammerLinkedLetter2; }
            set 
            {
                if (bJammerLinkedLetter2 != value)
                {
                    bJammerLinkedLetter2 = value;
                    cIni.WriteByte(strSectionJammerLinkedLetter, "Letter2  ", bJammerLinkedLetter2);
                    if (OnChangeSuppressionLetterJammerLinked != null)
                        OnChangeSuppressionLetterJammerLinked();
                }
            }
        }

        public byte JammerLinkedLetter3
        {
            get { return bJammerLinkedLetter3; }
            set
            {
                if (bJammerLinkedLetter3 != value)
                {
                    bJammerLinkedLetter3 = value;
                    cIni.WriteByte(strSectionJammerLinkedLetter, "Letter3  ", bJammerLinkedLetter3);
                    if (OnChangeSuppressionLetterJammerLinked != null)
                        OnChangeSuppressionLetterJammerLinked();
                }
            }
        }

        public byte JammerLinkedLetter4
        {
            get { return bJammerLinkedLetter4; }
            set
            {
                if (bJammerLinkedLetter4 != value)
                {
                    bJammerLinkedLetter4 = value;
                    cIni.WriteByte(strSectionJammerLinkedLetter, "Letter4  ", bJammerLinkedLetter4);
                    if (OnChangeSuppressionLetterJammerLinked != null)
                        OnChangeSuppressionLetterJammerLinked();
                }
            }
        }

        public byte JammerLinkedLetter5
        {
            get { return bJammerLinkedLetter5; }
            set
            {
                if (bJammerLinkedLetter5 != value)
                {
                    bJammerLinkedLetter5 = value;
                    cIni.WriteByte(strSectionJammerLinkedLetter, "Letter5  ", bJammerLinkedLetter5);
                    if (OnChangeSuppressionLetterJammerLinked != null)
                        OnChangeSuppressionLetterJammerLinked();
                }
            }
        }

        public byte JammerLinkedLetter6
        {
            get { return bJammerLinkedLetter6; }
            set
            {
                if (bJammerLinkedLetter6 != value)
                {
                    bJammerLinkedLetter6 = value;
                    cIni.WriteByte(strSectionJammerLinkedLetter, "Letter6  ", bJammerLinkedLetter6);
                    if (OnChangeSuppressionLetterJammerLinked != null)
                        OnChangeSuppressionLetterJammerLinked();
                }
            }
        }

        public byte JammerLinkedLetter7
        {
            get { return bJammerLinkedLetter7; }
            set
            {
                if (bJammerLinkedLetter7 != value)
                {
                    bJammerLinkedLetter7 = value;
                    cIni.WriteByte(strSectionJammerLinkedLetter, "Letter7  ", bJammerLinkedLetter7);
                    if (OnChangeSuppressionLetterJammerLinked != null)
                        OnChangeSuppressionLetterJammerLinked();
                }
            }
        }

        public byte JammerLinkedLetter8
        {
            get { return bJammerLinkedLetter8; }
            set
            {
                if (bJammerLinkedLetter8 != value)
                {
                    bJammerLinkedLetter8 = value;
                    cIni.WriteByte(strSectionJammerLinkedLetter, "Letter8  ", bJammerLinkedLetter8);
                    if (OnChangeSuppressionLetterJammerLinked != null)
                        OnChangeSuppressionLetterJammerLinked();
                }
            }
        }

        public byte JammerLinkedLetter9
        {
            get { return bJammerLinkedLetter9; }
            set
            {
                if (bJammerLinkedLetter9 != value)
                {
                    bJammerLinkedLetter9 = value;
                    cIni.WriteByte(strSectionJammerLinkedLetter, "Letter9  ", bJammerLinkedLetter9);
                    if (OnChangeSuppressionLetterJammerLinked != null)
                        OnChangeSuppressionLetterJammerLinked();
                }
            }
        }


        # endregion

        # region  PropertySuppressionFWS

        public byte RegimeRadioSuppr
        {
            get { return bRegimeRadioSuppr; }
            set
            {
                if (bRegimeRadioSuppr != value)
                {
                    bRegimeRadioSuppr = value;
                    cIni.WriteByte(strSectionSuppressionFWS, "RegimeRadioSuppr", bRegimeRadioSuppr);
                    if (OnChangeRegimeRadioSuppr != null)
                    {
                        OnChangeRegimeRadioSuppr();
                    }
                }
            }
        }

        public short TimeRadiatFWS
        {
            get { return sTimeRadiatFWS; }
            set 
            {
                int MinVal = 100;
                int MaxVal = 10000;
                if (sTimeRadiatFWS != value && value >= MinVal && value <= MaxVal)
                {
                    sTimeRadiatFWS = value;
                    cIni.WriteInt(strSectionSuppressionFWS, "TimeRadiat   ", (int)sTimeRadiatFWS);
                    if (OnChangeTimeRadiatFWS != null)
                    {
                        OnChangeTimeRadiatFWS();
                    }
                }
            }
        }

        public byte ChannelUse
        {
            get { return bChannelUse; }
            set 
            { 
                bChannelUse = value;
                cIni.WriteByte(strSectionSuppressionFWS, "ChannelUse   ", bChannelUse);
            }
        }

        public short ThresholdDefault
        {
            get { return sThresholdDefault; }
            set 
            { 
                sThresholdDefault = value;
                cIni.WriteInt(strSectionSuppressionFWS, "ThresholdDefault    ", (int)sThresholdDefault);
            }
        }

        public byte Circle
        {
            get { return bCircle; }
            set 
            { 
                bCircle = value;
                cIni.WriteByte(strSectionSuppressionFWS, "Circle    ", bCircle);
            }
        }
        public byte PriorHandle
        {
            get { return bPriorHandle; }
            set
            {
                bPriorHandle = value;
                cIni.WriteByte(strSectionSuppressionFWS, "PriorHandle", bPriorHandle);
            }
        }
        public byte UP
        {
            get { return bUP; }
            set 
            {
                bUP = value;
                cIni.WriteByte(strSectionSuppressionFWS, "UP", bUP);
            }
        }
        
        #endregion

        # region  PropertySuppressionFHSS

        public short TimeRadiatFHSS
        {
            get { return sTimeRadiatFHSS; }
            set 
            {
                sTimeRadiatFHSS = value;
                cIni.WriteInt(strSectionSuppressionFHSS, "TimeRadiat    ", (int)sTimeRadiatFHSS);
            }
        }

        public byte CodeFFT
        {
            get { return bCodeFFT; }
            set 
            { 
                bCodeFFT = value;
                cIni.WriteByte(strSectionSuppressionFHSS, "CodeFFT     ", bCodeFFT);
            }
        }
      
        #endregion


        # region  PropertyAmplifier

        public short PowerMin
        {
            get { return sPowerMin; }
            set { sPowerMin = value; }
        }

        public short PowerMax
        {
            get { return sPowerMax; }
            set { sPowerMax = value; }
        }

        public short VoltageMin
        {
            get { return sVoltageMin; }
            set { sVoltageMin = value; }
        }

        public short VoltageMax
        {
            get { return sVoltageMax; }
            set { sVoltageMax = value; }
        }


        public short TempMin
        {
            get { return sTempMin; }
            set { sTempMin = value; }
        }

        public short TempMax
        {
            get { return sTempMax; }
            set { sTempMax = value; }
        }

        public short CurrentMin
        {
            get { return sCurrentMin; }
            set { sCurrentMin = value; }
        }

        public short CurrentMax
        {
            get { return sCurrentMax; }
            set { sCurrentMax = value; }
        }
       
        #endregion

        #region APRCH
        public int SearchArc
        {
            get { return iSearchArc; }
            set 
            {
                if (value != iSearchArc)
                {
                    iSearchArc = value;
                    cIni.WriteInt(strAPRCH, "SearchArc", iSearchArc);
                    if (OnChangeSearchArc != null)
                        OnChangeSearchArc();
                }

            }
        }
        public int SearchTime
        {
            get { return iSearchTime; }
            set
            {
                if (value != iSearchTime)
                {
                    iSearchTime = value;
                    cIni.WriteInt(strAPRCH, "SearchTime", iSearchTime);
                    if (OnChangeSearchTime != null)
                        OnChangeSearchTime();
                }
            }
        }
        public int SearchBand
        {
            get { return iSearchBand; }
            set
            {
                if (value != iSearchBand)
                {
                    iSearchBand = value;
                    cIni.WriteInt(strAPRCH, "SearchBand", iSearchBand);
                    if (OnChangeSearchBand != null)
                        OnChangeSearchBand();
                }
            }
        }
        #endregion

    }
}
