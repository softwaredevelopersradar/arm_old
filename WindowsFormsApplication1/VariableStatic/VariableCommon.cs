﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using USR_DLL;
using System.Reflection;
using System.IO;


namespace VariableStatic
{
    public class VariableCommon
    {
        string strNameFile = "Common.ini";
        static CIni cIni;

        # region  NameSection
        string strSectionInfo = "Info";
        string strSectionCallSign = "CallSign";
        string strSectionLocation = "Location";
        string strSectionAntennaDirection = "AntennaDirection";
        string strSectionQueryJammerLinked = "QueryJammerLinked";
        string strSectionWork = "Work";
        string strSectionAddressCmd = "AddressCmd";
        # endregion


        # region  Info
        private static short sOwnNumber = 0;
        private static short sOwnAddressPC = 0;
        private static short sJammerLinkedNumber = 0;
        private static short sJammerLinkedAddressPC = 0;
        private static byte bRole = 0;
        private static byte bOperator = 0;
        private static byte bLanguage = 0;
        private static byte bTypeStation = 0; //0 - 3 ГГц; 1 - 6 ГГц; 2 - 6 ГЦЦ без Тумана
        # endregion

        # region  CallSign
        private static string strOwnCallSign = "";
        private static string strJammerLinkedCallSign = "";
        private static string strPostControlCallSign = "";
        # endregion

        # region  Location
        private static byte bUseGNSS = 0;
        private static string strDefLatitude = "";
        private static string strDefLongitude = "";
        private static string strDefHeight = "";
        private static string strOwnLatitude = "";
        private static string strOwnLongitude = "";
        private static string strOwnHeight = "";
        private static string strJammerLinkedLatitude = "";
        private static string strJammerLinkedLongitude = "";
        private static string strJammerLinkedHeight = "";
        private static string strPCLatitude = "";
        private static string strPCLongitude = "";
        private static string strPCHeight = "";
        # endregion

        # region  AntennaDirection
        private static short sARD1 = 0;
        private static short sARD2 = 0;
        private static short sARD3 = 0;
        private static short sCompass = 0;
        private static short sLPA1_3 = 0;
        private static short sLPA2_4 = 0;

        private static short sAntennaPostControl = 0;
        private static short sAntennaJammerLinked = 0;
        private static short sAntennaLogPeriodicL13 = 0;
        private static short sAntennaLogPeriodicL24 = 0;
        private static short sAntennaLogPeriodicL567 = 0;
        # endregion

        # region  QueryJammerLinked
        private static byte bQueryCoord = 0;
        private static int iQueryCoordInterval = 0;
        private static byte bQueryFWS = 0;
        private static int iQueryFWSInterval = 0;
        private static byte bQueryFHSS = 0;
        private static int iQueryFHSSInterval = 0;
        # endregion

        # region  Work
        private static byte bSimulator = 0;
        # endregion

        # region  AddressCmd
        private static byte bAdrAWP1 = 0;
        private static byte bAdrAWP2 = 0;
        private static byte bAdrFHS = 0;
        private static byte bAdrDirectFinder = 0;
        private static byte bAdrJammerLinked = 0;
        private static byte bAdrPostControl = 0;
        # endregion


        #region Event
        public delegate void ChangeCommonEventHandler();

        public static event ChangeCommonEventHandler OnChangeCommonOwnNumber;
        public static event ChangeCommonEventHandler OnChangeCommonJammerLinkedNumber;
        public static event ChangeCommonEventHandler OnChangeCommonOwnAddressPC;
        public static event ChangeCommonEventHandler OnChangeCommonJammerLinkedAddressPC;
        public static event ChangeCommonEventHandler OnChangeCommonRole;
        public static event ChangeCommonEventHandler OnChangeCommonOperator;
        public static event ChangeCommonEventHandler OnChangeCommonLanguage;
        public static event ChangeCommonEventHandler OnChangeCommonTypeStation;
        public static event ChangeCommonEventHandler OnChangeCommonUseGNSS;

        public event ChangeCommonEventHandler OnChangeCommonCallSign;        
        #endregion



        static int Numinstans = 0;
        static string strExePath = "";
        public VariableCommon()
        {
            Numinstans++;
            if (strExePath == "")
            {
                strExePath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
                cIni = new CIni(strExePath + "\\INI\\" + strNameFile);

                InitVariable();
            }
        }


        private void InitVariable()
        {
            InitVariableInfo(strSectionInfo);
            InitVariableCallSign(strSectionCallSign);
            InitVariableLocation(strSectionLocation);
            InitVariableAntennaDirection(strSectionAntennaDirection);
            InitVariableQueryJammerLinked(strSectionQueryJammerLinked);
            InitVariableWork(strSectionWork);
            InitVariableAddressCmd(strSectionAddressCmd);
        }


        private void InitVariableInfo(string strSection)
        {
            sOwnNumber = Convert.ToInt16(cIni.ReadInt(strSection, "OwnNumber", 111));
            sOwnAddressPC = Convert.ToInt16(cIni.ReadInt(strSection, "OwnAddressPC ", 1));
            sJammerLinkedNumber = Convert.ToInt16(cIni.ReadInt(strSection, "JammerLinkedNumber ", 112));
            sJammerLinkedAddressPC = Convert.ToInt16(cIni.ReadInt(strSection, "JammerLinkedAddressPC ", 6));
            bRole = cIni.ReadByte(strSection, "Role ", 0);
            if (bRole > 2 || bOperator < 0)
                Role = 0;
            if (OnChangeCommonRole != null)
                OnChangeCommonRole();
            bOperator = cIni.ReadByte(strSection, "Operator ", 0);
            if (bOperator > 1 || bOperator < 0)
                Operator = 0;
            if (OnChangeCommonOperator != null)
                OnChangeCommonOperator();
            bLanguage = cIni.ReadByte(strSection, "Language ", 0);
            bTypeStation = cIni.ReadByte(strSection, "TypeStation", 0);
            if (bTypeStation > 2)
            {
                bTypeStation = 2;
                cIni.WriteByte(strSection, "TypeStation", bTypeStation);
            }
        }

        private void InitVariableCallSign(string strSection)
        {
            strOwnCallSign = cIni.ReadString(strSection, "Own");
            strJammerLinkedCallSign = cIni.ReadString(strSection, "JammerLinked");
            strPostControlCallSign = cIni.ReadString(strSection, "PostControl");
        }

        private void InitVariableLocation(string strSection)
        {
            bUseGNSS = cIni.ReadByte(strSection, "UseGNSS", 0);

            strDefLatitude = cIni.ReadString(strSection, "DefLatitude");
            strDefLongitude = cIni.ReadString(strSection, "DefLongitude");
            strDefHeight = cIni.ReadString(strSection, "DefHeight");

            strOwnLatitude = cIni.ReadString(strSection, "OwnLatitude");
            strOwnLongitude = cIni.ReadString(strSection, "OwnLongitude");
            strOwnHeight = cIni.ReadString(strSection, "OwnHeight");

            strJammerLinkedLatitude = cIni.ReadString(strSection, "JammerLinkedLatitude");
            strJammerLinkedLongitude = cIni.ReadString(strSection, "JammerLinkedLongitude");
            strJammerLinkedHeight = cIni.ReadString(strSection, "JammerLinkedHeight");
            
            strPCLatitude = cIni.ReadString(strSection, "PCLatitude");
            strPCLongitude = cIni.ReadString(strSection, "PCLongitude");
            strPCHeight = cIni.ReadString(strSection, "PCHeight");
        }

        private void InitVariableAntennaDirection(string strSection)
        {

            sARD1 = Convert.ToInt16(cIni.ReadInt(strSection, "ARD1 ", -1));
            sARD2 = Convert.ToInt16(cIni.ReadInt(strSection, "ARD2 ", -1));
            sARD3 = Convert.ToInt16(cIni.ReadInt(strSection, "ARD3 ", -1));
            sCompass = Convert.ToInt16(cIni.ReadInt(strSection, "Compass ", -1));
            sLPA1_3 = Convert.ToInt16(cIni.ReadInt(strSection, "LPA1_3 ", -1));
            sLPA2_4 = Convert.ToInt16(cIni.ReadInt(strSection, "LPA2_4 ", -1));

            sAntennaPostControl = Convert.ToInt16(cIni.ReadInt(strSection, "PostControl ", 111));
            sAntennaJammerLinked = Convert.ToInt16(cIni.ReadInt(strSection, "JammerLinked  ", 1));
            sAntennaLogPeriodicL13 = Convert.ToInt16(cIni.ReadInt(strSection, "LogPeriodicL13  ", 112));
            sAntennaLogPeriodicL24 = Convert.ToInt16(cIni.ReadInt(strSection, "LogPeriodicL24  ", 6));
            sAntennaLogPeriodicL567 = Convert.ToInt16(cIni.ReadInt(strSection, "LogPeriodicL567  ", 6));

        }

        private void InitVariableQueryJammerLinked(string strSection)
        {
            bQueryCoord = cIni.ReadByte(strSection, "Coord ", 111);
            iQueryCoordInterval = cIni.ReadInt(strSection, "InetrvalCoord   ", 1);
            bQueryFWS = cIni.ReadByte(strSection, "FWS ", 111);
            iQueryFWSInterval = cIni.ReadInt(strSection, "IntervalFWS  ", 1);
            bQueryFHSS = cIni.ReadByte(strSection, "FHSS ", 111);
            iQueryFHSSInterval = cIni.ReadInt(strSection, "IntervalFHSS  ", 1);


        }

        private void InitVariableWork(string strSection)
        {
            bSimulator = cIni.ReadByte(strSection, "SimulatorIntell  ", 0);
        }

        private void InitVariableAddressCmd(string strSection)
        {
            bAdrAWP1 = cIni.ReadByte(strSection, "AWP1", 0);
            bAdrAWP2 = cIni.ReadByte(strSection, "AWP2", 0);
            bAdrFHS = cIni.ReadByte(strSection, "FHS", 0);
            bAdrDirectFinder = cIni.ReadByte(strSection, "DirectFinder", 0);
            bAdrJammerLinked = cIni.ReadByte(strSection, "JammerLinked", 0);
            bAdrPostControl = cIni.ReadByte(strSection, "PostControl", 0);
        }


        # region  PropertyInfo

        public short OwnNumber
        {
            get { return sOwnNumber; }
            set
            {
                if (sOwnNumber != value)
                {
                    sOwnNumber = value;
                    cIni.WriteInt(strSectionInfo, "OwnNumber ", (int)sOwnNumber);
                    if (OnChangeCommonOwnNumber != null)
                        OnChangeCommonOwnNumber();
                }
            }
        }

        public short OwnAddressPC
        {
            get { return sOwnAddressPC; }
            set 
            {
                if (sOwnAddressPC != value)
                {
                    sOwnAddressPC = value;
                    cIni.WriteInt(strSectionInfo, "OwnAddressPC  ", (int)sOwnAddressPC);
                    if (OnChangeCommonOwnAddressPC != null)
                        OnChangeCommonOwnAddressPC();
                }
            }
        }

        public short JammerLinkedNumber
        {
            get { return sJammerLinkedNumber; }
            set 
            {
                if (sJammerLinkedNumber != value)
                {
                    sJammerLinkedNumber = value;
                    cIni.WriteInt(strSectionInfo, "JammerLinkedNumber  ", (int)sJammerLinkedNumber);
                    if (OnChangeCommonJammerLinkedNumber != null)
                        OnChangeCommonJammerLinkedNumber();
                }
            }
        }

        public short JammerLinkedAddressPC
        {
            get { return sJammerLinkedAddressPC; }
            set 
            {
                if (sJammerLinkedAddressPC != value)
                {
                    sJammerLinkedAddressPC = value;
                    cIni.WriteInt(strSectionInfo, "JammerLinkedAddressPC  ", (int)sJammerLinkedAddressPC);
                    if (OnChangeCommonJammerLinkedAddressPC != null)
                        OnChangeCommonJammerLinkedAddressPC();
                }
            }
        }

        public byte Role
        {
            get { return bRole; }
            set 
            {
                if (bRole != value)
                {
                    bRole = value;
                    cIni.WriteByte(strSectionInfo, "Role", bRole);
                    if (OnChangeCommonRole != null)
                        OnChangeCommonRole();
                }
            }
        }

        public byte Operator
        {
            get { return bOperator; }
            set 
            {
                if (bOperator != value)
                {
                    bOperator = value;
                    cIni.WriteByte(strSectionInfo, "Operator ", bOperator);
                    if (OnChangeCommonOperator != null)
                        OnChangeCommonOperator();
                }
            }
        }

        public byte Language
        {
            get { return bLanguage; }
            set
            {
                if (bLanguage == value || bLanguage == value + 1)
                {
                    return;
                }
                bLanguage = (byte)(value == 1 ? 2 : 0);
                cIni.WriteByte(strSectionInfo, "Language ", bLanguage);
                if (OnChangeCommonLanguage != null)
                    OnChangeCommonLanguage();
            }
        }
        public byte TypeStation
        {
            get { return bTypeStation; }
            set
            {
                if (bTypeStation != value)
                {
                    bTypeStation = value;
                    cIni.WriteByte(strSectionInfo, "TypeStation", bTypeStation);
                    if (OnChangeCommonTypeStation != null)
                        OnChangeCommonTypeStation();
                }
            }
        }
        # endregion

        # region  PropertyCallSign

        public string OwnCallSign
        {
            get { return strOwnCallSign; }
            set 
            {
                if (strOwnCallSign != value)
                {
                    strOwnCallSign = value;
                    cIni.WriteString(strSectionCallSign, "Own", strOwnCallSign);
                    if (OnChangeCommonCallSign != null)
                        OnChangeCommonCallSign();
                }
            }
        }

        public string JammerLinkedCallSign
        {
            get { return strJammerLinkedCallSign; }
            set 
            {
                if (strJammerLinkedCallSign != value)
                {
                    strJammerLinkedCallSign = value;
                    cIni.WriteString(strSectionCallSign, "JammerLinked ", strJammerLinkedCallSign);
                    if (OnChangeCommonCallSign != null)
                        OnChangeCommonCallSign();
                }
            }
        }

        public string PostControlCallSign
        {
            get { return strPostControlCallSign; }
            set 
            {
                if (strPostControlCallSign != value)
                {
                    strPostControlCallSign = value;
                    cIni.WriteString(strSectionCallSign, "PostControl  ", strPostControlCallSign);
                    if (OnChangeCommonCallSign != null)
                        OnChangeCommonCallSign();
                }
            }
        }

        # endregion

        # region  PropertyLocation
        public byte UseGNSS
        {
            get { return bUseGNSS; }
            set
            {
                if (bUseGNSS != value)
                {
                    bUseGNSS = Convert.ToByte(value);
                    cIni.WriteByte(strSectionLocation, "UseGNSS", bUseGNSS);
                    if (OnChangeCommonUseGNSS != null)
                        OnChangeCommonUseGNSS();
                }
            }
        }

        public string DefLatitude
        {
            get { return strDefLatitude; }
            set 
            { 
                strDefLatitude = value;
                cIni.WriteString(strSectionLocation, "DefLatitude ", strDefLatitude);
            }
        }

        public string DefLongitude
        {
            get { return strDefLongitude; }
            set 
            { 
                strDefLongitude = value;
                cIni.WriteString(strSectionLocation, "DefLongitude  ", strDefLongitude);
            }
        }

        public string DefHeight
        {
            get { return strDefHeight; }
            set 
            { 
                strDefHeight = value;
                cIni.WriteString(strSectionLocation, "DefHeight ", strDefHeight);
            }
        }

        public string OwnLatitude
        {
            get { return strOwnLatitude; }
            set 
            {
                strOwnLatitude = value;
                cIni.WriteString(strSectionLocation, "OwnLatitude ", strOwnLatitude);
            }
        }

        public string OwnLongitude
        {
            get { return strOwnLongitude; }
            set 
            {
                strOwnLongitude = value;
                cIni.WriteString(strSectionLocation, "OwnLongitude  ", strOwnLongitude);
            }
        }

        public string OwnHeight
        {
            get { return strOwnHeight; }
            set 
            {
                strOwnHeight = value;
                cIni.WriteString(strSectionLocation, "OwnHeight ", strOwnHeight);
            }
        }

        public string JammerLinkedLatitude
        {
            get { return strJammerLinkedLatitude; }
            set 
            {
                strJammerLinkedLatitude = value;
                cIni.WriteString(strSectionLocation, "JammerLinkedLatitude ", strJammerLinkedLatitude);
            }
        }

        public string JammerLinkedLongitude
        {
            get { return strJammerLinkedLongitude; }
            set
            {
                strJammerLinkedLongitude = value;
                cIni.WriteString(strSectionLocation, "JammerLinkedLongitude ", strJammerLinkedLongitude);
            }
        }

        public string JammerLinkedHeight
        {
            get { return strJammerLinkedHeight; }
            set 
            {
                strJammerLinkedHeight = value;
                cIni.WriteString(strSectionLocation, "JammerLinkedHeight ", strJammerLinkedHeight);
            }
        }

        public string PCLatitude
        {
            get { return strPCLatitude; }
            set 
            {
                strPCLatitude = value;
                cIni.WriteString(strSectionLocation, "PCLatitude", strPCLatitude);                
            }
        }

        public string PCLongitude
        {
            get { return strPCLongitude; }
            set
            {
                strPCLongitude = value;
                cIni.WriteString(strSectionLocation, "PCLongitude", strPCLongitude);                
            }
        }
        public string PCHeight
        {
            get { return strPCHeight; }
            set
            {
                strPCHeight = value;
                cIni.WriteString(strSectionLocation, "PCHeight", strPCHeight);
            }
        }
        # endregion

        # region  PropertyAntennaDirection

        public short ARD1
        {
            get { return sARD1; }
            set
            {
                sARD1 = value;
                cIni.WriteInt(strSectionAntennaDirection, "ARD1 ", (int)sARD1);
            }
        }
        public short ARD2
        {
            get { return sARD2; }
            set
            {
                sARD2 = value;
                cIni.WriteInt(strSectionAntennaDirection, "ARD2 ", (int)sARD2);
            }
        }
        public short ARD3
        {
            get { return sARD3; }
            set
            {
                sARD3 = value;
                cIni.WriteInt(strSectionAntennaDirection, "ARD3 ", (int)sARD3);
            }
        }
        public short Compass
        {
            get { return sCompass; }
            set
            {
                sCompass = value;
                cIni.WriteInt(strSectionAntennaDirection, "Compass  ", (int)sCompass);
            }
        }
        public short LPA1_3
        {
            get { return sLPA1_3; }
            set
            {
                sLPA1_3 = value;
                cIni.WriteInt(strSectionAntennaDirection, "LPA1_3 ", (int)sLPA1_3);
            }
        }
        public short LPA2_4
        {
            get { return sLPA2_4; }
            set
            {
                sLPA2_4 = value;
                cIni.WriteInt(strSectionAntennaDirection, "LPA2_4 ", (int)sLPA2_4);
            }
        }




        public short AntennaPostControl
        {
            get { return sAntennaPostControl; }
            set 
            { 
                sAntennaPostControl = value;
                cIni.WriteInt(strSectionAntennaDirection, "PostControl  ", (int)sAntennaPostControl);
            }
        }

        public short AntennaJammerLinked
        {
            get { return sAntennaJammerLinked; }
            set 
            { 
                sAntennaJammerLinked = value;
                cIni.WriteInt(strSectionAntennaDirection, "JammerLinked   ", (int)sAntennaJammerLinked);
            }
        }

        public short AntennaLogPeriodicL13
        {
            get { return sAntennaLogPeriodicL13; }
            set
            { 
                sAntennaLogPeriodicL13 = value;
                cIni.WriteInt(strSectionAntennaDirection, "LogPeriodicL13 ", (int)sAntennaLogPeriodicL13);
            }
        }

        public short AntennaLogPeriodicL24
        {
            get { return sAntennaLogPeriodicL24; }
            set 
            { 
                sAntennaLogPeriodicL24 = value;
                cIni.WriteInt(strSectionAntennaDirection, "LogPeriodicL24", (int)sAntennaLogPeriodicL24);
            }
        }

        public short AntennaLogPeriodicL567
        {
            get { return sAntennaLogPeriodicL567; }
            set 
            { 
                sAntennaLogPeriodicL567 = value;
                cIni.WriteInt(strSectionAntennaDirection, "LogPeriodicL567 ", (int)sAntennaLogPeriodicL567);
            }
        }

        # endregion

        # region  PropertyQueryJammerLinked

        public byte QueryCoord
        {
            get { return bQueryCoord; }
            set 
            { 
                bQueryCoord = value;
                cIni.WriteByte(strSectionQueryJammerLinked, "Coord  ", bQueryCoord);
            }
        }

        public int QueryCoordInterval
        {
            get { return iQueryCoordInterval; }
            set
            { 
                iQueryCoordInterval = value;
                cIni.WriteInt(strSectionQueryJammerLinked, "InetrvalCoord ", iQueryCoordInterval);
            }
        }

        public byte QueryFWS
        {
            get { return bQueryFWS; }
            set
            { 
                bQueryFWS = value;
                cIni.WriteByte(strSectionQueryJammerLinked, "FWS   ", bQueryFWS);
            }
        }

        public int QueryFWSInterval
        {
            get { return iQueryFWSInterval; }
            set 
            { 
                iQueryFWSInterval = value;
                cIni.WriteInt(strSectionQueryJammerLinked, "IntervalFWS  ", iQueryFWSInterval);
            }
        }

        public byte QueryFHSS
        {
            get { return bQueryFHSS; }
            set 
            { 
                bQueryFHSS = value;
                cIni.WriteByte(strSectionQueryJammerLinked, "FHSS    ", bQueryFHSS);
            }
        }

        public int QueryFHSSInterval
        {
            get { return iQueryFHSSInterval; }
            set 
            { 
                iQueryFHSSInterval = value;
                cIni.WriteInt(strSectionQueryJammerLinked, "IntervalFHSS ", iQueryFHSSInterval);
            }
        }

        # endregion

        # region  PropertyWork

        public byte Simulator
        {
            get { return bSimulator; }
            set
            { 
                bSimulator = value;
                cIni.WriteByte(strSectionWork, "SimulatorIntell  ", bSimulator);
            }
        }

        #endregion

        # region  PropertyAddressCmd

        public byte AdrAWP1
        {
            get { return bAdrAWP1; }
            set 
            { 
                bAdrAWP1 = value; 
            }
        }

        public byte AdrAWP2
        {
            get { return bAdrAWP2; }
            set { bAdrAWP2 = value; }
        }

        public byte AdrFHS
        {
            get { return bAdrFHS; }
            set { bAdrFHS = value; }
        }

        public byte AdrDirectFinder
        {
            get { return bAdrDirectFinder; }
            set { bAdrDirectFinder = value; }
        }

        public byte AdrJammerLinked
        {
            get { return bAdrJammerLinked; }
            set { bAdrJammerLinked = value; }
        }

        public byte AdrPostControl
        {
            get { return bAdrPostControl; }
            set { bAdrPostControl = value; }
        }

        #endregion


        #region ARM
        public Boolean butCompass
        {
            get
            {
                switch (bOperator)
                {
                    case 0:
                        return Convert.ToBoolean(cIni.ReadByte("ARM1", "butCompass", 0));
                    case 1:
                        return Convert.ToBoolean(cIni.ReadByte("ARM2", "butCompass", 0));

                    default:
                        return false;
                }
            }
        }

        public Boolean butFPS
        {
            get
            {
                switch (bOperator)
                {
                    case 0:
                        return Convert.ToBoolean(cIni.ReadByte("ARM1", "butFPS", 0));
                    case 1:
                        return Convert.ToBoolean(cIni.ReadByte("ARM2", "butFPS", 0));

                    default:
                        return false;
                }
            }
        }
       
        public Boolean bModeS
        {
            get
            {
                switch (bOperator)
                {
                    case 0:
                        return Convert.ToBoolean(cIni.ReadByte("ARM1", "bModeS", 0));
                    case 1:
                        return Convert.ToBoolean(cIni.ReadByte("ARM2", "bModeS", 0));
                    default:
                        return false;
                }

            }
        }

        public Boolean pADSB
        {
            get 
            {
                switch (bOperator)
                {
                    case 0:
                        return Convert.ToBoolean(cIni.ReadByte("ARM1", "pADSB", 0));
                    case 1:
                        return Convert.ToBoolean(cIni.ReadByte("ARM2", "pADSB", 0));
                    default:
                        return false;
                }
            }
        }
        public Boolean Map
        {
            get
            {
                switch (bOperator)
                {
                    case 0:
                        return Convert.ToBoolean(cIni.ReadByte("ARM1", "Map", 0));
                    case 1:
                        return Convert.ToBoolean(cIni.ReadByte("ARM2", "Map", 0));
                    default:
                        return false;
                }
            }
        }

        public Boolean EnabledAsp
        {
            get 
            {
                switch (bOperator)
                {
                    case 0:
                        return true;
                    case 1:
                        return false;
                    default:
                        return false;
                }
            }
        }
        public Boolean FogNut
        {
            get 
            {
                if (bTypeStation == 1)
                    return false;
                switch (bOperator)
                {
                    case 0:
                        return Convert.ToBoolean(cIni.ReadByte("ARM1", "FogNut", 0));
                    case 1:
                        return Convert.ToBoolean(cIni.ReadByte("ARM2", "FogNut", 0));
                    default:
                        return false;
                }
            }
        }
        #endregion

        #region Role

        public Boolean bRegimeStop
        {
            get
            {
                if (bOperator == 1) { return false; }
                switch (bRole)
                {
                    case 0:
                        return Convert.ToBoolean(cIni.ReadByte("Independent", "bRegimeStop", 0));
                    case 1:
                        return Convert.ToBoolean(cIni.ReadByte("Leading", "bRegimeStop", 0));
                    case 2:
                        return Convert.ToBoolean(cIni.ReadByte("Slave", "bRegimeStop", 0));
                    default:
                        return false;
                }
            }
        }

        public Boolean bRegimeRecon
        {
            get
            {
                if (bOperator == 1) { return false; }
                switch (bRole)
                {
                    case 0:
                        return Convert.ToBoolean(cIni.ReadByte("Independent", "bRegimeRecon", 0));
                    case 1:
                        return Convert.ToBoolean(cIni.ReadByte("Leading", "bRegimeRecon", 0));
                    case 2:
                        return Convert.ToBoolean(cIni.ReadByte("Slave", "bRegimeRecon", 0));
                    default:
                        return false;
                }
            }
        }
        public Boolean bRegimeSuppr
        {
            get
            {
                if (bOperator == 1) { return false; }
                switch (bRole)
                {
                    case 0:
                        return Convert.ToBoolean(cIni.ReadByte("Independent", "bRegimeSuppr", 0));
                    case 1:
                        return Convert.ToBoolean(cIni.ReadByte("Leading", "bRegimeSuppr", 0));
                    case 2:
                        return Convert.ToBoolean(cIni.ReadByte("Slave", "bRegimeSuppr", 0));
                    default:
                        return false;
                }
            }
        }
        public Boolean butClientAsp
        {
            get
            {
                switch (bRole)
                {
                    case 0:
                        return Convert.ToBoolean(cIni.ReadByte("Independent", "butClientAsp", 0));
                    case 1:
                        return Convert.ToBoolean(cIni.ReadByte("Leading", "butClientAsp", 0));
                    case 2:
                        return Convert.ToBoolean(cIni.ReadByte("Slave", "butClientAsp", 0));
                    default:
                        return false;
                }
            }
        }

        public Boolean pASP
        {
            get
            {
                switch (bRole)
                {
                    case 0:
                        return Convert.ToBoolean(cIni.ReadByte("Independent", "pASP", 0));
                    case 1:
                        return Convert.ToBoolean(cIni.ReadByte("Leading", "pASP", 0));
                    case 2:
                        return Convert.ToBoolean(cIni.ReadByte("Slave", "pASP", 0));
                    default:
                        return false;
                }
            }
        }
        public Boolean pPU
        {
            get
            {
                if (bOperator == 1)
                    return false;
                switch (bRole)
                {
                    case 0:
                        return Convert.ToBoolean(cIni.ReadByte("Independent", "pPU", 0));
                    case 1:
                        return Convert.ToBoolean(cIni.ReadByte("Leading", "pPU", 0));
                    case 2:
                        return Convert.ToBoolean(cIni.ReadByte("Slave", "pPU", 0));
                    default:
                        return false; 
                }
            }
        }

        public Boolean VisGNSS
        {
            get 
            {
                switch (bOperator)
                {
                    case 0:
                        return true;
                    case 1:
                        return false;
                    default:
                        return false;
                }
            }
        }
        #endregion

    }
}
