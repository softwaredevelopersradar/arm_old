﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using USR_DLL;

namespace Contract
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    /*public class Service1 : IService1
    {
        public string GetData(int value)
        {
            return string.Format("You entered: {0}", value);
        }

        public CompositeType GetDataUsingDataContract(CompositeType composite)
        {
            if (composite == null)
            {
                throw new ArgumentNullException("composite");
            }
            if (composite.BoolValue)
            {
                composite.StringValue += "Suffix";
            }
            return composite;
        }
    }*/


    [ServiceContract]
    public interface IService
    {
        //[OperationContract]
        //void Hello(string name);

        //--------------------------------------------------------------------------------
        // APM -> Map
        //--------------------------------------------------------------------------------
        [OperationContract]
        void AirPlane(TDataADSBReceiver[] tDadaADSBReceiver);

        /// <summary>
        /// Обновление текущего выбранного пеленга
        /// </summary>
        /// <param name="direction">Пеленг в градусах от 0 до 360</param>
        [OperationContract]
        void CurrentDirection(TDirections tDirections);

        [OperationContract]
        void CurrentCoords(USR_DLL.TCoordsGNSS[] tCoordsGNSS);

        [OperationContract]
        void CheckGNSS(byte bCheck);

        [OperationContract]
        void CoordsIRI(TCoordsIRI[] tCoordsIRI);

        [OperationContract]
        void DirectionAntennas(TDirectionAntennas tDirectionAntennas);

        //[OperationContract]
        //void CoordsIRI_PPRCh(TCoordsIRI_PPRCh[] tCoordsIRI_PPRCh);

        [OperationContract]
        void CoordsIRI_PPRCh(List<TCoordsIRI_PPRCh> tCoordsIRI_PPRCh);

        [OperationContract]
        void PelengsIRI_PPRCh(TCoordsIRI_PPRCh[] tPelengsIRI_PPRCh);

        //--------------------------------------------------------------------------------
        // Map -> APM
        //--------------------------------------------------------------------------------
        [OperationContract]
        void MapCurrentCoords(USR_DLL.TCoordsGNSS[] tCoordsGNSS);

        [OperationContract]
        void MapIsOpen(bool mapIsOpen);

    }
}
