﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using USR_DLL;

namespace Contract
{
    /// <summary>
    /// Реализация сервиса
    /// </summary>
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    public class ServiceImplementation : Contract.IService
    {
        //--------------------------------------------------------------------------------
        // АРМ -> Карта
        //--------------------------------------------------------------------------------

        public event EventHandler<string> HelloReceived = delegate { };

        public event EventHandler<TDataADSBReceiver[]> AirPlaneReceived = delegate { };

        public event EventHandler<TDirections> CurrentDirectionUpdated = delegate { };

        public event EventHandler<USR_DLL.TCoordsGNSS[]> CurrentCoordsUpdated = delegate { };

        public event EventHandler<byte> CheckGNSSUpdated = delegate { };

        public event EventHandler<TCoordsIRI[]> CoordsIRIUpdated = delegate { };

        public event EventHandler<TDirectionAntennas> DirectionAntennasUpdated = delegate { };

        //public event EventHandler<TCoordsIRI_PPRCh[]> CoordsIRI_PPRChUpdated = delegate { };
        public event EventHandler<List<TCoordsIRI_PPRCh>> CoordsIRI_PPRChUpdated = delegate { };
        public event EventHandler<TCoordsIRI_PPRCh[]> PelengsIRI_PPRChUpdated = delegate { };

        public void Hello(string name)
        {
            //вызываем событие HelloReceived
            HelloReceived(this, name);
        }

        public void AirPlane(TDataADSBReceiver[] tDadaADSBReceiver)
        {
            //вызываем событие HelloReceived
            AirPlaneReceived(this, tDadaADSBReceiver);
        }

        public void CurrentDirection(TDirections tDirections)
        {
            CurrentDirectionUpdated(this, tDirections);
        }

        public void CurrentCoords(USR_DLL.TCoordsGNSS[] tCoordsGNSS)
        {
            CurrentCoordsUpdated(this, tCoordsGNSS);
        }

        public void CoordsIRI(TCoordsIRI[] tCoordsIRI)
        {
            CoordsIRIUpdated(this, tCoordsIRI);
        }

        public void DirectionAntennas(TDirectionAntennas tDirectionAntennas)
        {
            DirectionAntennasUpdated(this, tDirectionAntennas);
        }

        //public void CoordsIRI_PPRCh(TCoordsIRI_PPRCh[] tCoordsIRI_PPRCh)
        //{
        //    CoordsIRI_PPRChUpdated(this, tCoordsIRI_PPRCh);
        //}

        public void CoordsIRI_PPRCh(List<TCoordsIRI_PPRCh> tCoordsIRI_PPRCh)
        {
            CoordsIRI_PPRChUpdated(this, tCoordsIRI_PPRCh);
        }

        public void PelengsIRI_PPRCh(TCoordsIRI_PPRCh[] tPelengsIRI_PPRCh)
        {
            PelengsIRI_PPRChUpdated(this, tPelengsIRI_PPRCh);
        }

        public void CheckGNSS(byte bCheck)
        {
            CheckGNSSUpdated(this, bCheck);
        }


        //--------------------------------------------------------------------------------
        // Карта -> АРМ
        //--------------------------------------------------------------------------------

        public event EventHandler<USR_DLL.TCoordsGNSS[]> MapCurrentCoordsUpdated = delegate { };

        public event EventHandler<bool> MapIsOpenUpdated = delegate { };



        public void MapCurrentCoords(USR_DLL.TCoordsGNSS[] tCoordsGNSS)
        {
            MapCurrentCoordsUpdated(this, tCoordsGNSS);
        }

        public void MapIsOpen(bool mapIsOpen)
        {
            MapIsOpenUpdated(this, mapIsOpen);
        }


    }





    /*
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IService1
    {

        [OperationContract]
        string GetData(int value);

        [OperationContract]
        CompositeType GetDataUsingDataContract(CompositeType composite);

        // TODO: Add your service operations here
    }


    // Use a data contract as illustrated in the sample below to add composite types to service operations.
    [DataContract]
    public class CompositeType
    {
        bool boolValue = true;
        string stringValue = "Hello ";

        [DataMember]
        public bool BoolValue
        {
            get { return boolValue; }
            set { boolValue = value; }
        }

        [DataMember]
        public string StringValue
        {
            get { return stringValue; }
            set { stringValue = value; }
        }
    }*/
}
