﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using USR_DLL;


namespace VariableDynamic
{
    public class VariableWork
    {
        # region  Info
        private static byte bRegime = 0;

        private static Int64 iFrequency = 0;

        private static int iCheckIridiumInmarsat = 0;

        private static int iChangeTabControlTables = 0;

        private static int iID_FRCh_CR = 0;

        static Protocols.RangeSector[] tRangeSectorReconOwn = null;
        static Protocols.RangeSector[] tRangeSectorReconLinked = null;

        static Protocols.RangeSector[] tRangeSectorSupprOwn = null;
        static Protocols.RangeSector[] tRangeSectorSupprLinked = null;

        static Protocols.FrequencyRange[] tFrequencyRangeForbiddenOwn = null;
        static Protocols.FrequencyRange[] tFrequencyRangeForbiddenLinked = null;

        static Protocols.FrequencyRange[] tFrequencyRangeKnownOwn = null;
        static Protocols.FrequencyRange[] tFrequencyRangeKnownLinked = null;

        static Protocols.FrequencyRange[] tFrequencyRangeImportantOwn = null;
        static Protocols.FrequencyRange[] tFrequencyRangeImportantLinked = null;

        private static TSupprFWS[] tSupprFWS_Own = null;
        private static TSupprFWS[] tSupprFWS_Linked = null;

        private static TDataADSBReceiver[] tDataADSBReceiver = null;

        private static TCoordsGNSS[] tCoordsGNSS = new TCoordsGNSS[3];

        private static TDirectionAntennas tDirectionAntennas;

        private static TCoordsIRI[] tCoordsIRI = null;
        //private static TCoordsIRI_PPRCh[] tCoordsIRI_PPRCh = null;
        private static List<TCoordsIRI_PPRCh> tCoordsIRI_PPRCh = null;
        private static TCoordsIRI_PPRCh[] tPelengsIRI_PPRCh = null;

        private static TDistribFWS[] tDistribFWS = null;

        private static TDistribFHSS[] tDistribFHSS = null;
        private static TDistribFHSS_RP[] tDistribFHSS_RPOwn = null;
        private static TDistribFHSS_RP[] tDistribFHSS_RPLinked = null;
        private static TDistribFHSS_RPExclude[] tDistribFHSS_RPExcludeOwn = null;
        private static TDistribFHSS_RPExclude[] tDistribFHSS_RPExcludeLinked = null;

        private static TFreqImportantKRPU[] tFreqImportantKRPU = null;

        public static AWPtoBearingDSPprotocolNew aWPtoBearingDSPprotocolNew = new AWPtoBearingDSPprotocolNew();

   
        # endregion

        #region Event
        public delegate void ChangeRegimeEventHandler();

        public delegate void ChangeFrequencyEventHandler(Int64 iFreq);

        public delegate void ChangeCheckIridiumInmarsatEventHandler(int iCheckIridiumInmarsat);

        public delegate void ChangeTabControlTablesEventHandler(int index);

        public delegate void ChangeVariableEventHandler();

        public delegate void ChangeVariableADSBEventHandler(TDataADSBReceiver[] tDadaADSBReceiver);

        public delegate void ChangeVariableCoordsGNSSEventHandler(TCoordsGNSS[] coordsGNSS);

        public delegate void ChangeVariableDirectionAntennasEventHandler(TDirectionAntennas directionAntennas);

        public delegate void ChangeVariableCoordsIRIEventHandler(TCoordsIRI[] coordsIRI);
        //public delegate void ChangeVariableCoordsIRI_PPRChEventHandler(TCoordsIRI_PPRCh[] coordsIRI_PPRCh);
        public delegate void ChangeVariableCoordsIRI_PPRChEventHandler(List<TCoordsIRI_PPRCh> coordsIRI_PPRCh);
        public delegate void ChangeVariablePelengsIRI_PPRChEventHandler(TCoordsIRI_PPRCh[] pelengsIRI_PPRCh);

        public delegate void AddFreqImportantKRPUEventHandler(TFreqImportantKRPU[] freqImportantKRPU);

        public delegate void UpdateAllRangeSectorEventHandler();

        public delegate void UpdateAllSpecFreqEventHandler();

        public delegate void AddRecSpecFreqEventHandler(int FreqMinkHzx10, int FreqMaxkHzx10, byte TypeTable, byte bTable);

        //public static event UpdateAllRangeSectorEventHandler OnChangeDBAllRangeRROwn;
        //public static event UpdateAllRangeSectorEventHandler OnChangeDBAllRangeRRLinked;
        //public static event UpdateAllRangeSectorEventHandler OnChangeDBAllRangeRPOwn;
        //public static event UpdateAllRangeSectorEventHandler OnChangeDBAllRangeRPLinked;

        public delegate void DelFRCh_CREventHandler(int iID_FRCh_CR);
        public static event DelFRCh_CREventHandler OnDelFRCh_CR;

        public static event UpdateAllSpecFreqEventHandler OnChangeDBAllSpecForbiddenOwn;
        public static event UpdateAllSpecFreqEventHandler OnChangeDBAllSpecForbiddenLinked;
        public static event UpdateAllSpecFreqEventHandler OnChangeDBAllSpecKnownOwn;
        public static event UpdateAllSpecFreqEventHandler OnChangeDBAllSpecKnownLinked;
        public static event UpdateAllSpecFreqEventHandler OnChangeDBAllSpecImportantOwn;


        public static event UpdateAllSpecFreqEventHandler OnChangeDBAllSpecImportantLinked;

        public static event ChangeRegimeEventHandler OnChangeRegime;

        public static event ChangeRegimeEventHandler OnChangeRangeSectorReconOwn;
        public static event ChangeRegimeEventHandler OnChangeRangeSectorReconLinked;

        public static event ChangeRegimeEventHandler OnChangeRangeSectorSupprOwn;
        public static event ChangeRegimeEventHandler OnChangeRangeSectorSupprLinked;

        public static event ChangeRegimeEventHandler OnChangeFrequencyRangeForbiddenOwn;
        public static event ChangeRegimeEventHandler OnChangeFrequencyRangeForbiddenLinked;

        public static event ChangeRegimeEventHandler OnChangeFrequencyRangeKnownOwn;
        public static event ChangeRegimeEventHandler OnChangeFrequencyRangeKnownLinked;

        public static event ChangeRegimeEventHandler OnChangeFrequencyRangeImportantOwn;
        public static event ChangeRegimeEventHandler OnChangeFrequencyRangeImportantLinked;

        public static event ChangeVariableEventHandler OnChangeSupprFWS_Own;
        public static event ChangeVariableEventHandler OnChangeSupprFWS_Linked;

        public static event ChangeVariableADSBEventHandler OnChangeDataADSBReceiver;

        public static event ChangeVariableEventHandler OnChangeDistribFWS;

        public static event ChangeVariableEventHandler OnChangeDistribFHSS;

        public static event ChangeVariableEventHandler OnChangeDistribFHSS_RPOwn;
        public static event ChangeVariableEventHandler OnChangeDistribFHSS_RPLinked;

        public static event ChangeVariableEventHandler OnChangeDistribFHSS_RPExcludeOwn;
        public static event ChangeVariableEventHandler OnChangeDistribFHSS_RPExcludeLinked;

        public static event AddFreqImportantKRPUEventHandler OnAddFreqImportantKRPU;

        public static event ChangeVariableCoordsGNSSEventHandler OnChangeCoordsGNSS;

        public static event ChangeVariableDirectionAntennasEventHandler OnChangeDirectionAntennas;

        public static event ChangeVariableCoordsIRIEventHandler OnChangeCoordsIRI;
        //public static event ChangeVariableCoordsIRI_PPRChEventHandler OnChangeCoordsIRI_PPRCh;
        public static event ChangeVariableCoordsIRI_PPRChEventHandler OnChangeCoordsIRI_PPRCh;
        public static event ChangeVariablePelengsIRI_PPRChEventHandler OnChangePelengsIRI_PPRCh;

        public static event ChangeFrequencyEventHandler OnChangeFrequency;

        public static event ChangeCheckIridiumInmarsatEventHandler OnChangeCheckIridiumInmarsat;
        public static event ChangeTabControlTablesEventHandler OnChangeTabControlTables;

        public static event AddRecSpecFreqEventHandler OnAddRecSpecFreq;


        public delegate void RecBRSEventHandler(double frequency);
        public static event RecBRSEventHandler RecBRSEvent;

        public delegate void RecFtoRJEventHandler(double frequency);
        public static event RecFtoRJEventHandler RecFtoRJEvent;

        #endregion

        public void RecBRSsend(double frequency)
        {
            if (RecBRSEvent != null)
                RecBRSEvent(frequency);
        }

        public void RecFtoRJSsend(double frequency)
        {
            if (RecFtoRJEvent != null)
                RecFtoRJEvent(frequency);
        }

        public Protocols.RangeSector[] RangeSectorReconOwn
        {
            get { return tRangeSectorReconOwn; }
            set
            {
                tRangeSectorReconOwn = value;

                if (OnChangeRangeSectorReconOwn != null)
                    OnChangeRangeSectorReconOwn();
            }
        }

        public Protocols.RangeSector[] RangeSectorReconLinked
        {
            get { return tRangeSectorReconLinked; }
            set
            {
                tRangeSectorReconLinked = value;

                if (OnChangeRangeSectorReconLinked != null)
                    OnChangeRangeSectorReconLinked();
            }
        }

        public Protocols.RangeSector[] RangeSectorSupprOwn
        {
            get { return tRangeSectorSupprOwn; }
            set
            {
                tRangeSectorSupprOwn = value;


                if (OnChangeRangeSectorSupprOwn != null)
                    OnChangeRangeSectorSupprOwn();
            }
        }

        public Protocols.RangeSector[] RangeSectorSupprLinked
        {
            get { return tRangeSectorSupprLinked; }
            set
            {
                tRangeSectorSupprLinked = value;


                if (OnChangeRangeSectorSupprLinked != null)
                    OnChangeRangeSectorSupprLinked();
            }
        }

        public void UpdateAllRangeSectorReconOwn(Protocols.RangeSector[] newRangeSectors)
        {
            RangeSectorReconOwn = newRangeSectors;

            if (OnChangeRangeSectorReconOwn != null)
            {
                OnChangeRangeSectorReconOwn();
            }
        }

        public void UpdateAllRangeSectorReconLinked(Protocols.RangeSector[] newRangeSectors)
        {
            RangeSectorReconLinked = newRangeSectors;

            if (OnChangeRangeSectorReconLinked != null)
            {
                OnChangeRangeSectorReconLinked();
            }
        }

        public void UpdateAllRangeSectorSupprOwn(Protocols.RangeSector[] newRangeSectors)
        {
            RangeSectorSupprOwn = newRangeSectors;

            if (OnChangeRangeSectorSupprOwn != null)
            {
                OnChangeRangeSectorSupprOwn();
            }
        }

        public void UpdateAllRangeSectorSupprLinked(Protocols.RangeSector[] newRangeSectors)
        {
            RangeSectorSupprLinked = newRangeSectors;

            if (OnChangeRangeSectorSupprLinked != null)
            {
                OnChangeRangeSectorSupprLinked();
            }
        }

        public Protocols.FrequencyRange[] FrequencyRangeForbiddenOwn
        {
            get { return tFrequencyRangeForbiddenOwn; }
            set
            {
                tFrequencyRangeForbiddenOwn = value;

                if (OnChangeFrequencyRangeForbiddenOwn != null)
                    OnChangeFrequencyRangeForbiddenOwn();
            }
        }

        public Protocols.FrequencyRange[] FrequencyRangeForbiddenLinked
        {
            get { return tFrequencyRangeForbiddenLinked; }
            set
            {
                tFrequencyRangeForbiddenLinked = value;

                if (OnChangeFrequencyRangeForbiddenLinked != null)
                    OnChangeFrequencyRangeForbiddenLinked();
            }
        }

        public Protocols.FrequencyRange[] FrequencyRangeKnownOwn
        {
            get { return tFrequencyRangeKnownOwn; }
            set
            {
                tFrequencyRangeKnownOwn = value;

                if (OnChangeFrequencyRangeKnownOwn != null)
                    OnChangeFrequencyRangeKnownOwn();
            }
        }

        public Protocols.FrequencyRange[] FrequencyRangeKnownLinked
        {
            get { return tFrequencyRangeKnownLinked; }
            set
            {
                tFrequencyRangeKnownLinked = value;

                if (OnChangeFrequencyRangeKnownLinked != null)
                    OnChangeFrequencyRangeKnownLinked();
            }
        }

        public Protocols.FrequencyRange[] FrequencyRangeImportantOwn
        {
            get { return tFrequencyRangeImportantOwn; }
            set
            {
                tFrequencyRangeImportantOwn = value; 

                if (OnChangeFrequencyRangeImportantOwn != null)
                    OnChangeFrequencyRangeImportantOwn();
            }
        }

        public Protocols.FrequencyRange[] FrequencyRangeImportantLinked
        {
            get { return tFrequencyRangeImportantLinked; }
            set
            {
                tFrequencyRangeImportantLinked = value;

                if (OnChangeFrequencyRangeImportantLinked != null)
                    OnChangeFrequencyRangeImportantLinked();
            }
        }

        public void UpdateAllSpecForbiddenOwn(Protocols.FrequencyRange[] newSpecFreq)
        {
            FrequencyRangeForbiddenOwn = newSpecFreq;

            if (OnChangeDBAllSpecForbiddenOwn != null)
            {
                OnChangeDBAllSpecForbiddenOwn();
            }
        }

        public void UpdateAllSpecForbiddenLinked(Protocols.FrequencyRange[] newSpecFreq)
        {
            FrequencyRangeForbiddenLinked = newSpecFreq;

            if (OnChangeDBAllSpecForbiddenLinked != null)
            {
                OnChangeDBAllSpecForbiddenLinked();
            }
        }

        public void UpdateAllSpecKnownOwn(Protocols.FrequencyRange[] newSpecFreq)
        {
            FrequencyRangeKnownOwn = newSpecFreq;

            if (OnChangeDBAllSpecKnownOwn != null)
            {
                OnChangeDBAllSpecKnownOwn();
            }
        }

        public void UpdateAllSpecKnownLinked(Protocols.FrequencyRange[] newSpecFreq)
        {
            FrequencyRangeKnownLinked = newSpecFreq;

            if (OnChangeDBAllSpecKnownLinked != null)
            {
                OnChangeDBAllSpecKnownLinked();
            }
        }

        public void UpdateAllSpecImportantOwn(Protocols.FrequencyRange[] newSpecFreq)
        {
            FrequencyRangeImportantOwn = newSpecFreq;

            if (OnChangeDBAllSpecImportantOwn != null)
            {
                OnChangeDBAllSpecImportantOwn();
            }
        }

        public void UpdateAllSpecImportantLinked(Protocols.FrequencyRange[] newSpecFreq)
        {
            FrequencyRangeImportantLinked = newSpecFreq;

            if (OnChangeDBAllSpecImportantLinked != null)
            {
                OnChangeDBAllSpecImportantLinked();
            }
        }

        public TSupprFWS[] SupprFWS_Own
        {
            get { return tSupprFWS_Own; }
            set
            {
                tSupprFWS_Own = value;

                if (OnChangeSupprFWS_Own != null)
                    OnChangeSupprFWS_Own();
            }
        }

        public TSupprFWS[] SupprFWS_Linked
        {
            get { return tSupprFWS_Linked; }
            set
            {
                tSupprFWS_Linked = value;

                if (OnChangeSupprFWS_Linked != null)
                    OnChangeSupprFWS_Linked();
            }
        }

        public TDataADSBReceiver[] DataADSBReceiver
        {
            get { return tDataADSBReceiver; }
            set
            {
                tDataADSBReceiver = value;

                if (OnChangeDataADSBReceiver != null)
                {
                    OnChangeDataADSBReceiver(tDataADSBReceiver);
                }
            }
        }

        public TDistribFWS[] DistribFWS
        {
            get { return tDistribFWS; }
            set
            {
                tDistribFWS = value;

                if (OnChangeDistribFWS != null)
                    OnChangeDistribFWS();
            }
        }

        public TDistribFHSS[] DistribFHSS
        {
            get { return tDistribFHSS; }
            set
            {
                tDistribFHSS = value;

                if (OnChangeDistribFHSS != null)
                    OnChangeDistribFHSS();
            }
        }

        public TDistribFHSS_RP[] DistribFHSS_RPOwn
        {
            get { return tDistribFHSS_RPOwn; }
            set
            {
                tDistribFHSS_RPOwn = value;

                if (OnChangeDistribFHSS_RPOwn != null)
                    OnChangeDistribFHSS_RPOwn();
            }
        }

        public TDistribFHSS_RP[] DistribFHSS_RPLinked
        {
            get { return tDistribFHSS_RPLinked; }
            set
            {
                tDistribFHSS_RPLinked = value;

                if (OnChangeDistribFHSS_RPLinked != null)
                    OnChangeDistribFHSS_RPLinked();
            }
        }

        public TDistribFHSS_RPExclude[] DistribFHSS_RPExcludeOwn
        {
            get { return tDistribFHSS_RPExcludeOwn; }
            set
            {
                tDistribFHSS_RPExcludeOwn = value;

                if (OnChangeDistribFHSS_RPExcludeOwn != null)
                    OnChangeDistribFHSS_RPExcludeOwn();
            }
        }

        public TDistribFHSS_RPExclude[] DistribFHSS_RPExcludeLinked
        {
            get { return tDistribFHSS_RPExcludeLinked; }
            set
            {
                tDistribFHSS_RPExcludeLinked = value;

                if (OnChangeDistribFHSS_RPExcludeLinked != null)
                    OnChangeDistribFHSS_RPExcludeLinked();
            }
        }

        public TCoordsGNSS[] CoordsGNSS
        {
            get { return tCoordsGNSS; }
            set
            {
                tCoordsGNSS = value;

                if (OnChangeCoordsGNSS != null)
                {
                    OnChangeCoordsGNSS(tCoordsGNSS);
                }
            }
        }

        public TDirectionAntennas DirectionAntennas
        {
            get { return tDirectionAntennas; }
            set
            {
                tDirectionAntennas = value;

                if (OnChangeDirectionAntennas != null)
                {
                    OnChangeDirectionAntennas(tDirectionAntennas);
                }
            }
        }

        public TCoordsIRI[] CoordsIRI
        {
            get { return tCoordsIRI; }
            set
            {
                tCoordsIRI = value;

                if (OnChangeCoordsIRI != null)
                {
                    OnChangeCoordsIRI(tCoordsIRI);
                }
            }
        }

        //public TCoordsIRI_PPRCh[] CoordsIRI_PPRCh
        //{
        //    get { return tCoordsIRI_PPRCh; }
        //    set
        //    {
        //        tCoordsIRI_PPRCh = value;

        //        if (OnChangeCoordsIRI_PPRCh != null)
        //        {
        //            OnChangeCoordsIRI_PPRCh(tCoordsIRI_PPRCh);
        //        }
        //    }
        //}

        public List<TCoordsIRI_PPRCh> CoordsIRI_PPRCh
        {
            get { return tCoordsIRI_PPRCh; }
            set
            {
                tCoordsIRI_PPRCh = value;

                if (OnChangeCoordsIRI_PPRCh != null)
                {
                    OnChangeCoordsIRI_PPRCh(tCoordsIRI_PPRCh);
                }
            }
        }

        public TCoordsIRI_PPRCh[] PelengsIRI_PPRCh
        {
            get { return tPelengsIRI_PPRCh; }
            set
            {
                tPelengsIRI_PPRCh = value;

                if (OnChangePelengsIRI_PPRCh != null)
                {
                    OnChangePelengsIRI_PPRCh(tPelengsIRI_PPRCh);
                }
            }
        }

        public TFreqImportantKRPU[] FreqImportantKRPU
        {
            get { return tFreqImportantKRPU; }
            set
            {
                tFreqImportantKRPU = value;

                if (OnAddFreqImportantKRPU != null)
                {
                    OnAddFreqImportantKRPU(tFreqImportantKRPU);
                }
            }
        }

        public byte Regime
        {
            get { return bRegime; }
            set
            {
                bRegime = value;

                if (OnChangeRegime != null)
                    OnChangeRegime();
            }
        }

        public Int64 Frequency
        {
            get { return iFrequency; }
            set
            {
                iFrequency = value;

                if (OnChangeFrequency != null)
                    OnChangeFrequency(iFrequency);
            }
        }

        public int CheckIridiumInmarsat
        {
            get { return iCheckIridiumInmarsat; }
            set
            {
                iCheckIridiumInmarsat = value;

                if (OnChangeCheckIridiumInmarsat != null)
                    OnChangeCheckIridiumInmarsat(iCheckIridiumInmarsat);
            }
        }

        public int ChangeTabControlTables
        {
            get { return iChangeTabControlTables; }
            set
            {
                iChangeTabControlTables = value;

                if (OnChangeTabControlTables != null)
                    OnChangeTabControlTables(iChangeTabControlTables);
            }
        }

        public void SendSpecFreq(int FreqMinkHzx10, int FreqMaxkHzx10, byte TypeTable, byte bTable)
        {
            if (OnAddRecSpecFreq != null)
                OnAddRecSpecFreq(FreqMinkHzx10, FreqMaxkHzx10, TypeTable, bTable);
        }

        public int ID_FRCh_CR
        {
            get { return iID_FRCh_CR; }
            set
            {
                iID_FRCh_CR = value;

                if (OnDelFRCh_CR != null)
                    OnDelFRCh_CR(iID_FRCh_CR);
            }
        }

      
    }
}
