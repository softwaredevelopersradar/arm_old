﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TypeStruct
{
    public class TypeUser
    {
        public struct TRangeSector
        {
            public int iFreqMin;
            public int iFreqMax;
            public int iAngleMin;
            public int iAngleMax;
        }

        public struct TRange
        {
            public int iFreqMin;
            public int iFreqMax;           
        }
    }
}
