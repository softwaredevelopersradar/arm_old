﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AxaxGisToolKit;
using axGisToolKit;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace GrozaMap
{
    public partial class FormPeleng : Form
    {

        [DllImport("gisacces.dll")]
        public static extern int mapPlaneToGeoWGS84(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        public static extern int mapPlaneToPicture(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        public static extern int mapPictureToPlane(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        public static extern int mapPlaneToGeo(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        static extern int mapGeoToPlane(int hmap, ref double Bx, ref double Ly);

        //private MapForm objMapForm3;
        private AxaxcMapScreen axaxcMapScreen;

        // Переменные VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR 
        public double dchislo1;
        public long ichislo1;

        // ......................................................................
        // Пеленг

        //public double[] mas_Otl_Pel = new double[10000];
        //Координаты фиктивных точек
        //public double[] mas_Pel = new double[10000];     // R,Широта,долгота
        //public double[] mas_Pel_XYZ = new double[10000]; // XYZ в опорной геоцентрической СК

        // Otl -> для отладки
        public uint flagF2_Pel;
        public uint flagF3_Pel;
        public uint Ind_Otl_Pel;
        public uint i1;

        // Число фиктивных точек в плоскости пеленгования пеленгатора 
        public uint NumbFikt_Pel;

        // Индексы в массивах (0, ...)
        public uint IndFikt_Pel;
        public uint IndFikt_XYZ_Pel;

        // Текущее время моделирования
        public double time_tek_Pel;
        // Общее время моделирования
        public double T_Pel;
        // Дискрет времени
        public double dt_Pel;

        // Пеленг(рад)
        public double Theta_Pel;
        // Max дальность отображения пеленга
        public double Mmax_Pel;

        // Широта и долгота стояния пеленгатора
        public double LatP_Pel;
        public double LongP_Pel;

        // Координаты пеленгатора в опорной геоцентрической СК
        public double XP_Pel;
        public double YP_Pel;
        public double ZP_Pel;

        // Координаты N-й фиктивной точки в опорной геоцентрической СК
        public double XFN_Pel;
        public double YFN_Pel;
        public double ZFN_Pel;
        // ......................................................................


        // VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR Переменные


        // Конструктор *********************************************************** 

        public FormPeleng(ref AxaxcMapScreen axaxcMapScreen1)
        {
            InitializeComponent();

            axaxcMapScreen = axaxcMapScreen1;

            dchislo1 = 0;
            ichislo1 = 0;

            // ......................................................................
            // Пеленг

            // Дискрет времени моделирования
            dt_Pel = 0.1;

            // Индексы в массивах (0, ...)
            IndFikt_Pel = 0;
            IndFikt_XYZ_Pel = 0;

            //Координаты фиктивных точек
            //Array.Clear(mas_Pel, 0, 10000);
            //Array.Clear(mas_Pel_XYZ, 0, 10000);

            // Текущее время моделирования
            time_tek_Pel = 0;
            // Общее время моделирования
            T_Pel = 0;

            flagF2_Pel = 0;
            flagF3_Pel = 0;

            // Координаты пеленгатора в опорной геоцентрической СК
            XP_Pel = 0;
            YP_Pel = 0;
            ZP_Pel = 0;

            // Координаты N-й фиктивной точки в опорной геоцентрической СК
            XFN_Pel = 0;
            YFN_Pel = 0;
            ZFN_Pel = 0;

            // ......................................................................


        } // Конструктор
        // ***********************************************************  Конструктор


        // *****************************************************************************************
        // Обработчик кнопки Button1: пеленг
        // *****************************************************************************************

        private void button1_Click(object sender, EventArgs e)
        {
/*
            // ......................................................................
            GlobalVarLn.fl_Peleng_stat = 1;
            // ......................................................................
            //Координаты фиктивных точек

            Array.Clear(GlobalVarLn.mas_Pel, 0, 10000);
            Array.Clear(GlobalVarLn.mas_Pel_XYZ, 0, 10000);
            // ......................................................................
            // Number of points
            NumbFikt_Pel = Convert.ToUInt32(textBox5.Text);
            GlobalVarLn.NumbFikt_Pel_stat = NumbFikt_Pel;

            // град->перевод в рад - внутри функции
            LatP_Pel = Convert.ToDouble(textBox2.Text);
            LongP_Pel = Convert.ToDouble(textBox1.Text);
            Theta_Pel = Convert.ToDouble(textBox3.Text);
            GlobalVarLn.LatP_Pel_stat = LatP_Pel;
            GlobalVarLn.LongP_Pel_stat = LongP_Pel;

            // км->перевод в m - внутри функции
            Mmax_Pel = Convert.ToDouble(textBox4.Text);
            // ......................................................................

            ClassMap objClassMap5 = new ClassMap();
            // ......................................................................

            objClassMap5.f_Peleng(

                         // Пеленг(град)
                         Theta_Pel,
                         // Max дальность отображения пеленга(км)
                         Mmax_Pel,
                         // Количество фиктивных точек в плоскости пеленгования пеленгатора
                         NumbFikt_Pel,
                         // Широта и долгота стояния пеленгатора(град)
                         LatP_Pel,
                         LongP_Pel,

                         // Координаты пеленгатора в опорной геоцентрической СК(км)
                         ref XP_Pel,
                         ref YP_Pel,
                         ref ZP_Pel,

                         // Координаты N-й фиктивной точки в опорной геоцентрической СК(км)
                         ref XFN_Pel,
                         ref YFN_Pel,
                         ref ZFN_Pel,

                         // Координаты фиктивных точек
                         ref GlobalVarLn.mas_Pel,    // R(км),Lat(град),Long(град)
                         ref GlobalVarLn.mas_Pel_XYZ // км

                         );
 
            // ......................................................................
            f_Map_Pol_LatLong(
                                 LatP_Pel,
                                 LongP_Pel
                                 );


            while (IndFikt_Pel < (NumbFikt_Pel * 3))
            {
                // .......................................................................
                // Широта, долгота, grad

                f_Map_Rect_LatLong2(
                                     GlobalVarLn.mas_Pel[IndFikt_Pel + 1],
                                     GlobalVarLn.mas_Pel[IndFikt_Pel + 2]
                                     );


                // otladka
                //f_Map_Rect_LatLong2(
                //                     LatP_Pel,
                //                     LongP_Pel
                //                     );



                IndFikt_Pel += 3;
                IndFikt_XYZ_Pel += 3;
                // .......................................................................

            }; // WHILE

            // .......................................................................
            IndFikt_Pel = 0;
            IndFikt_XYZ_Pel = 0;
            GlobalVarLn.IndFikt_Pel_stat = 0;
            // .......................................................................

*/
        } // //  Button1: пеленг
        // *****************************************************************************************

        // *****************************************************************************************

        private void FormPeleng_Load(object sender, EventArgs e)
        {
            // ......................................................................
            //Peleng

            textBox2.Text = "54,63";      // Lat,grad
            textBox1.Text = "27,24";      // Long, grad
            textBox3.Text = "270";     // Theta, grad
            textBox4.Text = "12";    //Mmax,km
            textBox5.Text = "1000";    // Число фикт.точек
            // ......................................................................

        } //  Load Form
        // *****************************************************************************************


        // FUNCTIONS ********************************************************************************

        // ******************************************************************************************
        // Нарисовать метку по координатам (красный кружок для линии)
        //
        // Входные параметры:
        // Lat - широта, град (GEO)
        // Long - долгота, град (GEO)
        // ******************************************************************************************
        public void f_Map_Rect_LatLong2(

                                         double Lat,
                                         double Long
                                        )
        {

            double LatDX, LongDY;

            // -------------------------------------------------------------------------------------
            Graphics graph = axaxcMapScreen.CreateGraphics();

            Pen penRed1 = new Pen(Color.Red, 2);
            Brush brushRed1 = new SolidBrush(Color.Red);

            // -------------------------------------------------------------------------------------

            // grad->rad
            LatDX = (Lat * Math.PI) / 180;
            LongDY = (Long * Math.PI) / 180;

            // Подаем градусы, получаем там же расстояние на карте в км
            mapGeoToPlane(GlobalVarLn.hmapl, ref LatDX, ref LongDY);

            // Расстояние в км на карте -> пикселы на изображении
            mapPlaneToPicture(GlobalVarLn.hmapl, ref LatDX, ref LongDY);
            // -------------------------------------------------------------------------------------

            if (graph != null)
            {
                //objMapForm3.graph.FillRectangle(brushRed1, (int)LatDX - objMapForm3.axaxcMapScreen1.MapLeft, (int)LongDY - objMapForm3.axaxcMapScreen1.MapTop, 7, 7);
                graph.FillEllipse(brushRed1, (int)LatDX - axaxcMapScreen.MapLeft, (int)LongDY - axaxcMapScreen.MapTop, 5, 5);



                // Надпись
                //TitleObject(typeStation, lat, lon);
            }

            // -------------------------------------------------------------------------------------

            //objMapForm3.graph.Dispose();


        } // P/P f_Map_Rect_LatLong2
        // *************************************************************************************

        // ******************************************************************************************
        // Нарисовать метку по координатам (красный треугольник)
        //
        // Входные параметры:
        // Lat - широта, град (GEO)
        // Long - долгота, град (GEO)
        // ******************************************************************************************
        public void f_Map_Pol_LatLong(

                                         double Lat,
                                         double Long
                                        )
        {

            double LatDX, LongDY;

            // -------------------------------------------------------------------------------------
            Graphics graph = axaxcMapScreen.CreateGraphics();

            Pen penRed1 = new Pen(Color.Red, 2);
            Brush brushRed1 = new SolidBrush(Color.Red);

            // -------------------------------------------------------------------------------------

            // grad->rad
            LatDX = (Lat * Math.PI) / 180;
            LongDY = (Long * Math.PI) / 180;

            // Подаем rad, получаем там же расстояние на карте в м
            mapGeoToPlane(GlobalVarLn.hmapl, ref LatDX, ref LongDY);

            // Расстояние в м на карте -> пикселы на изображении
            mapPlaneToPicture(GlobalVarLn.hmapl, ref LatDX, ref LongDY);
            // -------------------------------------------------------------------------------------

            if (graph != null)
            {
                //objMapForm3.graph.FillEllipse(brushRed1, (int)LatDX - objMapForm3.axaxcMapScreen1.MapLeft, (int)LongDY - objMapForm3.axaxcMapScreen1.MapTop, 5, 5);


                Point[] toDraw = new Point[3];
                toDraw[0].X = (int)LatDX - (axaxcMapScreen.MapLeft + 7);
                toDraw[0].Y = (int)LongDY - (axaxcMapScreen.MapTop - 7);
                toDraw[1].X = (int)LatDX - axaxcMapScreen.MapLeft;
                toDraw[1].Y = (int)LongDY - (axaxcMapScreen.MapTop + 7);
                toDraw[2].X = (int)LatDX - (axaxcMapScreen.MapLeft - 7);
                toDraw[2].Y = (int)LongDY - (axaxcMapScreen.MapTop - 7);
                //Brush brush3 = new SolidBrush(color);
                graph.FillPolygon(brushRed1, toDraw);

            }

            // -------------------------------------------------------------------------------------

            //objMapForm3.graph.Dispose();


        }

        private void FormPeleng_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            Hide();

        } // P/P f_Map_Pol_LatLong
        // *************************************************************************************


        // ********************************************************************************* FUNCTIONS


    } // Class
} // Namespace
