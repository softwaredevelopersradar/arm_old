﻿using System;
using System.Drawing;
using AxaxGisToolKit;
using axGisToolKit;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Linq;

using System.IO;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;
using System.ComponentModel;



namespace GrozaMap
{
    public partial class FormSuppression : Form
    {
        [DllImport("gisacces.dll")]
        public static extern int mapPlaneToGeoWGS84(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        public static extern int mapPlaneToPicture(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        public static extern int mapPictureToPlane(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        public static extern int mapPlaneToGeo(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        static extern int mapGeoToPlane(int hmap, ref double Bx, ref double Ly);

        private AxaxcMapScreen axaxcMapScreen;

        // Переменные VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR 
        private double dchislo;
        private long ichislo;
        private double LAMBDA;

        // .....................................................................
        // Координаты СП,ys

        //private uint flCoordSP_comm; // =1-> Выбрали СП
        //private uint flCoordYS1_comm; // =1-> Выбрали YS1
        //private uint flCoordYS2_comm; // =1-> Выбрали YS2

/*
        // Координаты СП на местности в м
        private double XSP_comm;
        private double YSP_comm;
        private double XYS1_comm;
        private double YYS1_comm;
        private double XYS2_comm;
        private double YYS2_comm;

        // DATUM
        private double dXdat_comm;
        private double dYdat_comm;
        private double dZdat_comm;

        private double dLat_comm;
        private double dLong_comm;

        // Эллипсоид Красовского, град
        private double LatKrG_comm;
        private double LongKrG_comm;
        // Эллипсоид Красовского, rad
        private double LatKrR_comm;
        private double LongKrR_comm;
        // Эллипсоид Красовского, град,мин,сек
        private int Lat_Grad_comm;
        private int Lat_Min_comm;
        private double Lat_Sec_comm;
        private int Long_Grad_comm;
        private int Long_Min_comm;
        private double Long_Sec_comm;
        // Гаусс-крюгер(СК42) м
        private double XSP42_comm;
        private double YSP42_comm;

        // Эллипсоид Красовского, град
        private double LatKrG_YS1_comm;
        private double LongKrG_YS1_comm;
        // Эллипсоид Красовского, rad
        private double LatKrR_YS1_comm;
        private double LongKrR_YS1_comm;
        // Эллипсоид Красовского, град,мин,сек
        private int Lat_Grad_YS1_comm;
        private int Lat_Min_YS1_comm;
        private double Lat_Sec_YS1_comm;
        private int Long_Grad_YS1_comm;
        private int Long_Min_YS1_comm;
        private double Long_Sec_YS1_comm;
        // Гаусс-крюгер(СК42) м
        private double XYS142_comm;
        private double YYS142_comm;

        // Эллипсоид Красовского, град
        private double LatKrG_YS2_comm;
        private double LongKrG_YS2_comm;
        // Эллипсоид Красовского, rad
        private double LatKrR_YS2_comm;
        private double LongKrR_YS2_comm;
        // Эллипсоид Красовского, град,мин,сек
        private int Lat_Grad_YS2_comm;
        private int Lat_Min_YS2_comm;
        private double Lat_Sec_YS2_comm;
        private int Long_Grad_YS2_comm;
        private int Long_Min_YS2_comm;
        private double Long_Sec_YS2_comm;
        // Гаусс-крюгер(СК42) м
        private double XYS242_comm;
        private double YYS242_comm;
*/
        // ......................................................................
        // Основные параметры

        private double OwnHeight_comm;
        private double Point1Height_comm;
        private double Point2Height_comm;
        //private double HeightOwnObject_comm;
        private double PowerOwn_comm;
        private double CoeffOwn_comm;
        private double CoeffOwnPod_comm;
        //private double RadiusZone_comm;
        //private double MaxDist_comm;

        private int i_HeightOwnObject_comm;
        private int i_Pt1HeightOwnObject_comm;
        private int i_Cap1_comm;
        private int i_WidthHindrance_comm;
        private int i_Surface_comm;
        private double Cap1_comm;
        private double WidthHindrance_comm;
        //private double Surface_comm;

        // Высота средства подавления
        private double HeightAntennOwn_comm;
        private double HeightTotalOwn_comm;
        private double Pt1HeightTotalOwn_comm;
        private double Pt1HeightTotalOwn1_comm;


        // Для подавляемой линии
        private double Freq_comm;
        private double PowerOpponent_comm;
        private double CoeffTransmitOpponent_comm;
        private double CoeffReceiverOpponent_comm;
        private double RangeComm_comm;
        private double WidthSignal_comm;
        private double HeightTransmitOpponent_comm;
        private double HeightTransmitOpponent1_comm;
        private double HeightReceiverOpponent_comm;
        private double CoeffSupOpponent_comm;
        private int i_PolarOpponent_comm;
        private int i_CoeffSupOpponent_comm;
        private int i_TypeCommOpponent_comm;

        // ......................................................................
        // Зона

        private double dCoeffQ_comm;
        private double dCoeffHE_comm;
        private int iCorrectHeightOwn_comm;
        private int iResultHeightOwn_comm;
        private int iMiddleHeight_comm;
        private int iMinHeight_comm;
        private int iCorrectHeightOpponent_comm;
        private int iResultHeightOpponent_comm;
        private long iMaxDistance_comm;
        private double dGamma_comm;
        //private long liRadiusZone_comm;
        private double dDistanceObject;
        private double dRadiusZoneSup;
        private double dDelta1;
        private double dDelta2;

        private int iGamma;
        private double dKp1;
        private double dKp2;
        private double dCoeffA1;
        private double dCoeffA2;
        private double dRadiusZone1;
        private double dRadiusZone2;
        private double iXmin;
        private double iXmax;

        // ......................................................................
        private int iDistJammerComm1; // расстояние от УС1 до средства подаления
        private int iDistJammerComm2; // расстояние от УС2 до средства подаления
        private int iDistBetweenComm;
        private bool blResultSupress;


        // VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR Переменные

        // Конструктор *********************************************************** 
        ComponentResourceManager resourcesForTest = new ComponentResourceManager(typeof(FormSuppression));
        private int NumberOfLanguage;



        public FormSuppression(ref AxaxcMapScreen axaxcMapScreen1)
        {
            InitializeComponent();

           

            axaxcMapScreen = axaxcMapScreen1;

            dchislo = 0;
            ichislo = 0;

            LAMBDA = 300000;

            //flCoordSP_comm = 0; // =1-> Выбрали СП
            //flCoordYS1_comm = 0; // =1-> Выбрали YS1
            //flCoordYS2_comm = 0; // =1-> Выбрали YS2

            // .....................................................................
            // Координаты СП

/*
            // Координаты СП на местности в м
            XSP_comm = 0;
            YSP_comm = 0;
            XYS1_comm = 0;
            YYS1_comm = 0;
            XYS2_comm = 0;
            YYS2_comm = 0;

            // DATUM
            // ГОСТ 51794_2008
            dXdat_comm = 25;
            dYdat_comm = -141;
            dZdat_comm = -80;

            dLat_comm = 0;
            dLong_comm = 0;

            // Эллипсоид Красовского, град
            LatKrG_comm = 0;
            LongKrG_comm = 0;
            // Эллипсоид Красовского, rad
            LatKrR_comm = 0;
            LongKrR_comm = 0;
            // Эллипсоид Красовского, град,мин,сек
            Lat_Grad_comm = 0;
            Lat_Min_comm = 0;
            Lat_Sec_comm = 0;
            Long_Grad_comm = 0;
            Long_Min_comm = 0;
            Long_Sec_comm = 0;
            // Гаусс-крюгер(СК42) м
            XSP42_comm = 0;
            YSP42_comm = 0;

            // Эллипсоид Красовского, град
            LatKrG_YS1_comm = 0;
            LongKrG_YS1_comm = 0;
            // Эллипсоид Красовского, rad
            LatKrR_YS1_comm = 0;
            LongKrR_YS1_comm = 0;
            // Эллипсоид Красовского, град,мин,сек
            Lat_Grad_YS1_comm = 0;
            Lat_Min_YS1_comm = 0;
            Lat_Sec_YS1_comm = 0;
            Long_Grad_YS1_comm = 0;
            Long_Min_YS1_comm = 0;
            Long_Sec_YS1_comm = 0;
            // Гаусс-крюгер(СК42) м
            XYS142_comm = 0;
            YYS142_comm = 0;

            // Эллипсоид Красовского, град
            LatKrG_YS2_comm = 0;
            LongKrG_YS2_comm = 0;
            // Эллипсоид Красовского, rad
            LatKrR_YS2_comm = 0;
            LongKrR_YS2_comm = 0;
            // Эллипсоид Красовского, град,мин,сек
            Lat_Grad_YS2_comm = 0;
            Lat_Min_YS2_comm = 0;
            Lat_Sec_YS2_comm = 0;
            Long_Grad_YS2_comm = 0;
            Long_Min_YS2_comm = 0;
            Long_Sec_YS2_comm = 0;
            // Гаусс-крюгер(СК42) м
            XYS242_comm = 0;
            YYS242_comm = 0;
*/
            // ......................................................................
            // Основные параметры

            OwnHeight_comm = 0;
            Point1Height_comm = 0;
            Point2Height_comm = 0;
            //HeightOwnObject_comm = 0;
            PowerOwn_comm = 0;
            CoeffOwn_comm = 0;
            CoeffOwnPod_comm = 0;
            //RadiusZone_comm = 0;
            //MaxDist_comm = 0;

            i_HeightOwnObject_comm = 0;
            i_Pt1HeightOwnObject_comm = 0;
            i_Cap1_comm = 0;
            i_WidthHindrance_comm = 0;
            i_Surface_comm = 0;
            Cap1_comm = 0;
            WidthHindrance_comm = 0;
            //Surface_comm = 0;

            // Высота средства подавления
            // ??????????????????????
            HeightAntennOwn_comm = 0;
            HeightTotalOwn_comm = 0;
            Pt1HeightTotalOwn_comm = 0;
            Pt1HeightTotalOwn1_comm = 0;

            // Для подавляемой линии
            Freq_comm = 0;
            PowerOpponent_comm = 0;
            CoeffTransmitOpponent_comm = 0;
            CoeffReceiverOpponent_comm = 0;
            RangeComm_comm = 0;
            WidthSignal_comm = 0;
            HeightTransmitOpponent_comm = 0;
            HeightTransmitOpponent1_comm = 0;
            HeightReceiverOpponent_comm = 0;
            CoeffSupOpponent_comm = 0;
            i_PolarOpponent_comm = 0;
            i_CoeffSupOpponent_comm = 0;
            i_TypeCommOpponent_comm = 0;

            // ......................................................................
            // Зона

            dCoeffQ_comm = 0;
            dCoeffHE_comm = 0;
            iCorrectHeightOwn_comm = 0;
            iResultHeightOwn_comm = 0;
            iMiddleHeight_comm = 0;
            iMinHeight_comm = 0;
            iCorrectHeightOpponent_comm = 0;
            iResultHeightOpponent_comm = 0;
            iMaxDistance_comm = 0;
            dGamma_comm = 0;
            //liRadiusZone_comm = 0;
            dDistanceObject = 0;
            dRadiusZoneSup = 0;

            iGamma = 0;
            dKp1 = 0;
            dKp2 = 0;
            dCoeffA1 = 0;
            dCoeffA2 = 0;
            dRadiusZone1 = 0;
            dRadiusZone2 = 0;
            dDelta1 = 0;
            dDelta2 = 0;

            iXmin = 0;
            iXmax = 0;

            // ......................................................................
            iDistJammerComm1 = 0; // расстояние от УС1 до средства подаления
            iDistJammerComm2 = 0; // расстояние от УС2 до средства подаления
            iDistBetweenComm = 0;
            blResultSupress = false;

        } // Конструктор
        // ***********************************************************  Конструктор

        // ************************************************************************
        // Загрузка формы
        // ************************************************************************
        private void FormSuppression_Load(object sender, EventArgs e)
        {

            string strExePath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

            string PathFolder1 = Path.GetDirectoryName(strExePath);

            string strPathLatest = PathFolder1 + "\\INI\\Common.ini";
            foreach (string line in File.ReadLines(strPathLatest))
            {
                if (line.Contains("Language "))
                {
                    string LanguageAsNumber = line.Split(new string[] { "=" }, StringSplitOptions.None).Last();
                    NumberOfLanguage = Int32.Parse(LanguageAsNumber);
                    break;
                }
            }
            LanguageChooser();

            // ----------------------------------------------------------------------
            gbOwnRect.Visible = true;
            gbOwnRect.Location = new Point(6, 12);

            gbOwnRect42.Visible = false;
            gbOwnRad.Visible = false;
            gbOwnDegMin.Visible = false;
            gbOwnDegMinSec.Visible = false;

            cbChooseSC.SelectedIndex = 0;
            cbCenterLSR.SelectedIndex = 0;
            // ----------------------------------------------------------------------
            gbPt1Rect.Visible = true;
            gbPt1Rect.Location = new Point(6, 12);

            gbPt1Rect42.Visible = false;
            gbPt1Rad.Visible = false;
            gbPt1DegMin.Visible = false;
            gbPt1DegMinSec.Visible = false;
            // ----------------------------------------------------------------------
            // Переменные

            GlobalVarLn.fl_Suppression = 0; // Отрисовка зоны
            GlobalVarLn.fl_ZoneSuppression = 0; // Отрисовка зоны
            GlobalVarLn.flCoordSP_sup = 0; // =1-> Выбрали СП
            GlobalVarLn.flCoordOP = 0;
            GlobalVarLn.flA_sup = 0;
            // ----------------------------------------------------------------------
        }
        // ************************************************************************

        // ************************************************************************
        // Очистка
        // ************************************************************************
        private void bClear_Click(object sender, EventArgs e)
        {

            ClassMap.ClearSuppression();

        } // Clear
        // ************************************************************************

        // ************************************************************************
        // Обработчик ComboBox : Выбор СК 
        // ************************************************************************
        private void cbChooseSC_SelectedIndexChanged(object sender, EventArgs e)
        {
            ChooseSystemCoordSPOP_sup(cbChooseSC.SelectedIndex);
        } // Выбор СК
        // ************************************************************************

        // ************************************************************************

        // ************************************************************************
        // Обработчик ComboBox : Выбор СК OP
        // ************************************************************************
        private void cbCommChooseSC_SelectedIndexChanged(object sender, EventArgs e)
        {
            //ChooseSystemCoordOP_sup(cbCommChooseSC.SelectedIndex);
            ;

        }
        // ************************************************************************

        // ************************************************************************
        // Обработчик ComboBox "cbOwnObject": Выбор SP
        // ************************************************************************
        private void cbOwnObject_SelectedIndexChanged(object sender, EventArgs e)
        {
/*
            if (cbOwnObject.SelectedIndex == 0)
                GlobalVarLn.NumbSP_sup = "";
            else
                GlobalVarLn.NumbSP_sup = Convert.ToString(cbOwnObject.Items[cbOwnObject.SelectedIndex]);
*/
        }
        // ************************************************************************

        // ************************************************************************
        // Обработчик Button1 : Выбор CenterZone
        // ************************************************************************
        private void button1_Click(object sender, EventArgs e)
        {

            double xtmp_ed, ytmp_ed, htmp_ed;
            double xtmp1_ed, ytmp1_ed;
            int ind1 = 0;
            int ind2 = 0;

            xtmp_ed = 0;
            ytmp_ed = 0;
            htmp_ed = 0;
            xtmp1_ed = 0;
            ytmp1_ed = 0;
            // ......................................................................
            ClassMap objClassMap1_ed = new ClassMap();
            ClassMap objClassMap2_ed = new ClassMap();
            ClassMap objClassMap3_ed = new ClassMap();
            // ......................................................................
            ind1=GlobalVarLn.objFormSuppressionG.cbChooseSC.SelectedIndex;
            ind2=GlobalVarLn.objFormSuppressionG.cbCenterLSR.SelectedIndex;

            ClassMap.ClearSuppression();

            GlobalVarLn.objFormSuppressionG.cbChooseSC.SelectedIndex = ind1;
            GlobalVarLn.objFormSuppressionG.cbCenterLSR.SelectedIndex = ind2;

            // ----------------------------------------------------------------------
            // Enter CenterZone

            switch (cbCenterLSR.SelectedIndex)
            {
                case 0: // Мышь на карте

                    // !!! реальные координаты на местности карты в м (Plane)
                    GlobalVarLn.XCenter_sup = GlobalVarLn.MapX1;
                    GlobalVarLn.YCenter_sup = GlobalVarLn.MapY1;

                    if ((GlobalVarLn.XCenter_sup == 0) || (GlobalVarLn.YCenter_sup == 0))
                    {
                        if (GlobalVarLn.fl_Azb == 0)
                        {
                            MessageBox.Show("Не выбран центр зоны");
                        }
                        else
                        {
                            //Azb???
                            MessageBox.Show("zonanın merkezi  seçilmeyib");
                        }

                        return;
                    }

                    break;

                case 1: // Выбор из списка СП: АСП

                    //if (System.IO.File.Exists(Application.StartupPath + "\\Settings.ini"))
                    //{
                    //    GlobalVarLn.XCenter_sup = (double)iniRW.get_X_ASP();
                    //    GlobalVarLn.YCenter_sup = (double)iniRW.get_Y_ASP();
                    // }
                    // else
                    // {
                    //    MessageBox.Show("Невозможно открыть INI файл");
                    //     return;
                    // }

                    //777
                    GlobalVarLn.XCenter_sup = GlobalVarLn.XCenter_Sost;
                    GlobalVarLn.YCenter_sup = GlobalVarLn.YCenter_Sost;

                    if ((GlobalVarLn.XCenter_sup == 0) || (GlobalVarLn.YCenter_sup == 0))
                    {
                        if (GlobalVarLn.fl_Azb == 0)
                        {
                            MessageBox.Show("Нет координат АСП");
                        }
                        else
                        {
                            //Azb???
                            MessageBox.Show("No mənşəyi (MS)");
                        }

                        return;
                    }

                    break;

                case 2: // Выбор из списка СП: АСПсопр.

                    //if (System.IO.File.Exists(Application.StartupPath + "\\Settings.ini"))
                    //{
                    //    GlobalVarLn.XCenter_sup = (double)iniRW.get_X_ASPS();
                    //    GlobalVarLn.YCenter_sup = (double)iniRW.get_Y_ASPS();
                    //}
                    //else
                    //{
                    //    MessageBox.Show("Невозможно открыть INI файл");
                    //    return;
                    //}
                    //777
                    GlobalVarLn.XCenter_sup = GlobalVarLn.XPoint1_Sost;
                    GlobalVarLn.YCenter_sup = GlobalVarLn.YPoint1_Sost;

                    if ((GlobalVarLn.XCenter_sup == 0) || (GlobalVarLn.YCenter_sup == 0))
                    {
                        if (GlobalVarLn.fl_Azb == 0)
                        {
                            MessageBox.Show("Нет координат АСП сопряженной");
                        }
                        else
                        {
                            //Azb???
                            MessageBox.Show("No mənşəyi (Beraber işleyen manee stansiyası)");
                        }

                        return;
                    }

                    break;

                case 3: // Выбор из списка СП: PU

                    //if (System.IO.File.Exists(Application.StartupPath + "\\Settings.ini"))
                    //{
                    //    GlobalVarLn.XCenter_sup = (double)iniRW.get_X_PU();
                    //    GlobalVarLn.YCenter_sup = (double)iniRW.get_Y_PU();
                    //}
                    //else
                    //{
                    //    MessageBox.Show("Невозможно открыть INI файл");
                    //    return;
                    //}
                    //777
                    GlobalVarLn.XCenter_sup = GlobalVarLn.XPoint2_Sost;
                    GlobalVarLn.YCenter_sup = GlobalVarLn.YPoint2_Sost;

                    if ((GlobalVarLn.XCenter_sup == 0) || (GlobalVarLn.YCenter_sup == 0))
                    {
                        if (GlobalVarLn.fl_Azb == 0)
                        {
                            MessageBox.Show("Нет координат ПУ");
                        }
                        else
                        {
                            //Azb???
                            MessageBox.Show("No mənşəyi (idareetme pultu)");
                        }

                        return;
                    }

                    break;


            } // SWITCH
            // ----------------------------------------------------------------------

            // ......................................................................
            xtmp_ed = GlobalVarLn.XCenter_sup;
            ytmp_ed = GlobalVarLn.YCenter_sup;
            // ......................................................................
            // H

            GlobalVarLn.axMapPointGlobalAdd.SetPoint(xtmp_ed, ytmp_ed);
            GlobalVarLn.HCenter_sup = (int)GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);
            htmp_ed = GlobalVarLn.HCenter_sup;
            tbOwnHeight.Text = Convert.ToString(htmp_ed);
             // ......................................................................
            GlobalVarLn.fl_Suppression = 1;
            GlobalVarLn.flCoordSP_sup = 1;        // Центр выбран
            // ......................................................................
            // SP на карте

            // Убрать с карты
            //GlobalVarLn.axMapScreenGlobal.Repaint();

            // SP
            ClassMap.f_DrawSPXY(
                          GlobalVarLn.XCenter_sup,  // m на местности
                          GlobalVarLn.YCenter_sup,
                              ""
                         );
            // ......................................................................
            // Реальные координаты карты в м -> в долготу и широту WGS84 с помощью функций Панорамы
            // !!! Выход функции(rad) идет на место входных переменных

            mapPlaneToGeoWGS84(GlobalVarLn.hmapl, ref xtmp_ed, ref ytmp_ed);

            // rad(WGS84)->grad(WGS84)
            xtmp1_ed = (xtmp_ed * 180) / Math.PI;
            ytmp1_ed = (ytmp_ed * 180) / Math.PI;

            //333
            GlobalVarLn.BWGS84_1_sup = xtmp1_ed;
            GlobalVarLn.LWGS84_1_sup = ytmp1_ed;
            // .......................................................................

            // WGS84(эллипсоид)->элл.Красовского *************************************
            // Перевод координат WGS84(эллипсоид) -> эллипсоид Красовского(Пулково-42)
            // WGS84(широта,долгота)из Панорамы пересчитываем в эллипсод Красовского моей функцией
            // Входные параметры -> град,km
            // Перевод в рад - внутри функции

            // dLong ..................................................................
            // Расчет приращения по долготе при преобразованиях координат WGS84<->SK42
            // (преобразования Молоденского), угл.сек

            objClassMap1_ed.f_dLong
                (
                // Входные параметры (град,км)
                    xtmp1_ed,   // широта
                    ytmp1_ed,  // долгота
                    0,     // высота

                    // DATUM,m
                    GlobalVarLn.dXdat_comm,
                    GlobalVarLn.dYdat_comm,
                    GlobalVarLn.dZdat_comm,

                    ref GlobalVarLn.dLong_sup   // приращение по долготе, угл.сек
                );
            // ................................................................ dLong

            // dLat .................................................................
            // Расчет приращения по широте при преобразованиях координат WGS84<->SK42
            // (преобразования Молоденского), угл.сек

            objClassMap1_ed.f_dLat
                (
                // Входные параметры (град,км)
                    xtmp1_ed,   // широта
                    ytmp1_ed,  // долгота
                    0,     // высота

                    // DATUM,m
                    GlobalVarLn.dXdat_comm,
                    GlobalVarLn.dYdat_comm,
                    GlobalVarLn.dZdat_comm,

                    ref GlobalVarLn.dLat_sup        // приращение по долготе, угл.сек
                );
            // ................................................................. dLat

            // Lat,Long .............................................................
            // Преобразования широты и долготы при пересчете WGS84->SK42

            objClassMap1_ed.f_WGS84_SK42_Lat_Long
                   (
                // Входные параметры (град,км)
                       xtmp1_ed,   // широта
                       ytmp1_ed,  // долгота
                       0,     // высота
                       GlobalVarLn.dLat_sup,       // приращение по долготе, угл.сек
                       GlobalVarLn.dLong_sup,      // приращение по долготе, угл.сек

                       // Выходные параметры (grad)
                       ref GlobalVarLn.LatKrG_sup,   // широта
                       ref GlobalVarLn.LongKrG_sup   // долгота
                   );
            // ............................................................ Lat,Long


            // *********************************** WGS84(эллипсоид)->элл.Красовского

            // .......................................................................
            // Эллипсоид Красовского, радианы

            GlobalVarLn.LatKrR_sup = (GlobalVarLn.LatKrG_sup * Math.PI) / 180;
            GlobalVarLn.LongKrR_sup = (GlobalVarLn.LongKrG_sup * Math.PI) / 180;
            // .......................................................................
            // Эллипсоид Красовского, grad,min,sec
            // dd.ddddd -> DD MM SS

            // Широта
            objClassMap3_ed.f_Grad_GMS
              (
                // Входные параметры (grad)
                GlobalVarLn.LatKrG_sup,

                // Выходные параметры 
                ref GlobalVarLn.Lat_Grad_sup,
                ref GlobalVarLn.Lat_Min_sup,
                ref GlobalVarLn.Lat_Sec_sup
              );

            // Долгота
            objClassMap3_ed.f_Grad_GMS
              (
                // Входные параметры (grad)
                GlobalVarLn.LongKrG_sup,

                // Выходные параметры 
                ref GlobalVarLn.Long_Grad_sup,
                ref GlobalVarLn.Long_Min_sup,
                ref GlobalVarLn.Long_Sec_sup
              );
            // .......................................................................

            // SK42(элл.)->Крюгер ****************************************************
            // Преобразование геодезических координат (широта, долгота, высота) 
            // эллипсоида Красовского (СК42) в плоские прямоугольные координаты в
            // проекции Гаусса-Крюгера
            // Входные параметры -> !!!grad

            objClassMap3_ed.f_SK42_Krug
                   (
                // Входные параметры (!!! grad)
                // !!! эллипсоид Красовского
                       GlobalVarLn.LatKrG_sup,   // широта
                       GlobalVarLn.LongKrG_sup,  // долгота

                       // Выходные параметры (km)
                       ref GlobalVarLn.XSP42_sup,
                       ref GlobalVarLn.YSP42_sup
                   );

            // km->m
            GlobalVarLn.XSP42_sup = GlobalVarLn.XSP42_sup * 1000;
            GlobalVarLn.YSP42_sup = GlobalVarLn.YSP42_sup * 1000;
            // **************************************************** SK42(элл.)->Крюгер

            // .......................................................................
            // Отобразить координаты в окошках

            ClassMap.OtobrSP_Supression();
            // .......................................................................


        } // SP
        // ************************************************************************

        // ************************************************************************
        // Обработчик Button2 : Выбор OP
        // ************************************************************************

        private void button2_Click(object sender, EventArgs e)
        {

            double xtmp_ed, ytmp_ed, htmp_ed;
            double xtmp1_ed, ytmp1_ed;

            xtmp_ed = 0;
            ytmp_ed = 0;
            htmp_ed = 0;
            xtmp1_ed = 0;
            ytmp1_ed = 0;
            // ......................................................................
            ClassMap objClassMap1_ed = new ClassMap();
            ClassMap objClassMap2_ed = new ClassMap();
            ClassMap objClassMap3_ed = new ClassMap();
            // ......................................................................

            // ----------------------------------------------------------------------
            // Enter OP

                   // Мышь на карте

                    // !!! реальные координаты на местности карты в м (Plane)
                    GlobalVarLn.XPoint1_sup = GlobalVarLn.MapX1;
                    GlobalVarLn.YPoint1_sup = GlobalVarLn.MapY1;

                    if ((GlobalVarLn.XPoint1_sup == 0) || (GlobalVarLn.YPoint1_sup == 0))
                    {
                        if (GlobalVarLn.fl_Azb == 0)
                        {
                            MessageBox.Show("Не выбран ОП");
                        }
                        else
                        {
                            //Azb???
                            MessageBox.Show("Hedef işarelenmiyib");
                        }

                        return;
                    }

                    if (GlobalVarLn.flCoordSP_sup==0)
                    {
                        if (GlobalVarLn.fl_Azb == 0)
                        {
                            MessageBox.Show("Не выбран центр зоны");
                        }
                        else
                        {
                            //Azb???
                            MessageBox.Show("zonanın merkezi seçilmeyib");
                        }

                        return;
                    }

                    if (GlobalVarLn.flCoordOP == 1)
                    {
                        if (GlobalVarLn.fl_Azb == 0)
                        {
                            MessageBox.Show("ОП уже выбран. Очистите данные");
                        }
                        else
                        {
                            //Azb???
                            MessageBox.Show("Obyekt artıq seçilmiş. Melumatları silmek");
                        }

                        return;
                    }

            // ----------------------------------------------------------------------

            // ......................................................................
            xtmp_ed = GlobalVarLn.XPoint1_sup;
            ytmp_ed = GlobalVarLn.YPoint1_sup;
            // ......................................................................
            // H

            GlobalVarLn.axMapPointGlobalAdd.SetPoint(xtmp_ed, ytmp_ed);
            GlobalVarLn.HPoint1_sup = (int)GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);
            htmp_ed = GlobalVarLn.HPoint1_sup;
            tbPt1Height.Text = Convert.ToString(htmp_ed);
            // ......................................................................
            GlobalVarLn.flCoordOP = 1;        // OP выбран
            // ......................................................................
            // SP на карте

            // Убрать с карты
            //GlobalVarLn.axMapScreenGlobal.Repaint();

            // OP
            ClassMap.f_Map_Pol_XY_stat(
                          GlobalVarLn.XPoint1_sup,  // m
                         GlobalVarLn.YPoint1_sup,
                          2,
                          //1,
                          ""
                         );

            // ......................................................................
            // Реальные координаты карты в м -> в долготу и широту WGS84 с помощью функций Панорамы
            // !!! Выход функции(rad) идет на место входных переменных

            mapPlaneToGeoWGS84(GlobalVarLn.hmapl, ref xtmp_ed, ref ytmp_ed);

            // rad(WGS84)->grad(WGS84)
            xtmp1_ed = (xtmp_ed * 180) / Math.PI;
            ytmp1_ed = (ytmp_ed * 180) / Math.PI;

            //333
            GlobalVarLn.BWGS84_2_sup = xtmp1_ed;
            GlobalVarLn.LWGS84_2_sup = ytmp1_ed;
            // .......................................................................

            // WGS84(эллипсоид)->элл.Красовского *************************************
            // Перевод координат WGS84(эллипсоид) -> эллипсоид Красовского(Пулково-42)
            // WGS84(широта,долгота)из Панорамы пересчитываем в эллипсод Красовского моей функцией
            // Входные параметры -> град,km
            // Перевод в рад - внутри функции

            // dLong ..................................................................
            // Расчет приращения по долготе при преобразованиях координат WGS84<->SK42
            // (преобразования Молоденского), угл.сек

            objClassMap1_ed.f_dLong
                (
                // Входные параметры (град,км)
                    xtmp1_ed,   // широта
                    ytmp1_ed,  // долгота
                    0,     // высота

                    // DATUM,m
                    GlobalVarLn.dXdat_comm,
                    GlobalVarLn.dYdat_comm,
                    GlobalVarLn.dZdat_comm,

                    ref GlobalVarLn.dLong_sup   // приращение по долготе, угл.сек
                );
            // ................................................................ dLong

            // dLat .................................................................
            // Расчет приращения по широте при преобразованиях координат WGS84<->SK42
            // (преобразования Молоденского), угл.сек

            objClassMap1_ed.f_dLat
                (
                // Входные параметры (град,км)
                    xtmp1_ed,   // широта
                    ytmp1_ed,  // долгота
                    0,     // высота

                    // DATUM,m
                    GlobalVarLn.dXdat_comm,
                    GlobalVarLn.dYdat_comm,
                    GlobalVarLn.dZdat_comm,

                    ref GlobalVarLn.dLat_sup        // приращение по долготе, угл.сек
                );
            // ................................................................. dLat

            // Lat,Long .............................................................
            // Преобразования широты и долготы при пересчете WGS84->SK42

            objClassMap1_ed.f_WGS84_SK42_Lat_Long
                   (
                // Входные параметры (град,км)
                       xtmp1_ed,   // широта
                       ytmp1_ed,  // долгота
                       0,     // высота
                       GlobalVarLn.dLat_sup,       // приращение по долготе, угл.сек
                       GlobalVarLn.dLong_sup,      // приращение по долготе, угл.сек

                       // Выходные параметры (grad)
                       ref GlobalVarLn.LatKrG_YS1_sup,   // широта
                       ref GlobalVarLn.LongKrG_YS1_sup   // долгота
                   );
            // ............................................................ Lat,Long


            // *********************************** WGS84(эллипсоид)->элл.Красовского

            // .......................................................................
            // Эллипсоид Красовского, радианы

            GlobalVarLn.LatKrR_YS1_sup = (GlobalVarLn.LatKrG_YS1_sup * Math.PI) / 180;
            GlobalVarLn.LongKrR_YS1_sup = (GlobalVarLn.LongKrG_YS1_sup * Math.PI) / 180;
            // .......................................................................
            // Эллипсоид Красовского, grad,min,sec
            // dd.ddddd -> DD MM SS

            // Широта
            objClassMap3_ed.f_Grad_GMS
              (
                // Входные параметры (grad)
                GlobalVarLn.LatKrG_YS1_sup,

                // Выходные параметры 
                ref GlobalVarLn.Lat_Grad_YS1_sup,
                ref GlobalVarLn.Lat_Min_YS1_sup,
                ref GlobalVarLn.Lat_Sec_YS1_sup
              );

            // Долгота
            objClassMap3_ed.f_Grad_GMS
              (
                // Входные параметры (grad)
                GlobalVarLn.LongKrG_YS1_sup,

                // Выходные параметры 
                ref GlobalVarLn.Long_Grad_YS1_sup,
                ref GlobalVarLn.Long_Min_YS1_sup,
                ref GlobalVarLn.Long_Sec_YS1_sup
              );
            // .......................................................................

            // SK42(элл.)->Крюгер ****************************************************
            // Преобразование геодезических координат (широта, долгота, высота) 
            // эллипсоида Красовского (СК42) в плоские прямоугольные координаты в
            // проекции Гаусса-Крюгера
            // Входные параметры -> !!!grad

            objClassMap3_ed.f_SK42_Krug
                   (
                // Входные параметры (!!! grad)
                // !!! эллипсоид Красовского
                       GlobalVarLn.LatKrG_YS1_sup,   // широта
                       GlobalVarLn.LongKrG_YS1_sup,  // долгота

                       // Выходные параметры (km)
                       ref GlobalVarLn.XYS142_sup,
                       ref GlobalVarLn.YYS142_sup
                   );

            // km->m
            GlobalVarLn.XYS142_sup = GlobalVarLn.XYS142_sup * 1000;
            GlobalVarLn.YYS142_sup = GlobalVarLn.YYS142_sup * 1000;
            // **************************************************** SK42(элл.)->Крюгер

            // .......................................................................
            // Отобразить координаты в окошках

            ClassMap.OtobrOP_Suppression();
            // .......................................................................


        } // OP
        // ************************************************************************


        // ************************************************************************
        // Button "Принять" 
        // ************************************************************************

        // Расчет зоны MAIN MAIN MAIN MAIN MAIN MAIN MAIN MAIN MAIN MAIN MAIN MAIN 
        // Button "Принять"

        private void bAccept_Click(object sender, EventArgs e)
        {
            // ----------------------------------------------------------------------
            if (GlobalVarLn.flCoordSP_sup == 0)
            {
                if (GlobalVarLn.fl_Azb == 0)
                {
                    MessageBox.Show("Центр зоны не выбран");
                }
                else
                {
                    //Azb???
                    MessageBox.Show("zonanın merkezi seçilmeyib");
                }

                return;
            }
            if (GlobalVarLn.flCoordOP == 0)
            {
                if (GlobalVarLn.fl_Azb == 0)
                {
                    MessageBox.Show("ОП не выбран");
                }
                else
                {
                    //Azb???
                    MessageBox.Show("Hedef işarelenmiyib");
                }

                return;
            }
            // ----------------------------------------------------------------------

            // Центр ЗПВ **************************************************************
            // Координаты на местности в м

            GlobalVarLn.tpOwnCoordRect_sup.X = (int)GlobalVarLn.XCenter_sup;
            GlobalVarLn.tpOwnCoordRect_sup.Y = (int)GlobalVarLn.YCenter_sup;

            //if ((tbXRect.Text == "") || (tbYRect.Text == ""))
            //{
            //    MessageBox.Show("Некорректные координаты центра зоны (метры на местности)");
            //    return;
           // }
            // ************************************************************** Центр ЗПВ

            // OP **********************************************************************
            // Координаты на местности в м

            GlobalVarLn.tpPoint1Rect_sup.X = (int)GlobalVarLn.XPoint1_sup;
            GlobalVarLn.tpPoint1Rect_sup.Y = (int)GlobalVarLn.YPoint1_sup;

            //if ((tbPt1XRect.Text == "") || (tbPt1YRect.Text == ""))
            //{
            //    MessageBox.Show("Некорректные координаты ОП (метры на местности)");
            //    return;
            //}
            // ********************************************************************** OP

            // !!! Высоты *************************************************************
            // !!! GlobalVarLn.HCenter_ZPV введена по кнопке ЦентрЗоны

            // .........................................................................
            // Средняя высота местности

            iMiddleHeight_comm = DefineMiddleHeight_Comm(GlobalVarLn.tpOwnCoordRect_sup, GlobalVarLn.axMapPointGlobalAdd,
                                                         GlobalVarLn.axMapScreenGlobal);
            tbMidH.Text = iMiddleHeight_comm.ToString();
            // .........................................................................
            // ЦЕНТР ЗПВ

            // Высота антенны
            //HeightAntennOwn_comm = Convert.ToDouble(tbHAnt.Text);
            String ss = "";
            ss = tbHAnt.Text;
            try
            {
                HeightAntennOwn_comm = Convert.ToDouble(ss);
            }
            catch (SystemException)
            {
                try
                {
                    if (ss.IndexOf(",") > -1) ss = ss.Replace(',', '.');
                    else ss = ss.Replace('.', ',');
                    HeightAntennOwn_comm = Convert.ToDouble(ss);
                }
                catch
                {
                    if (GlobalVarLn.fl_Azb == 0)
                    {
                        MessageBox.Show("Недопустимая высота антенны");
                    }
                    else
                    {
                        //Azb?
                        MessageBox.Show("Yolverilməz Antenanın hündürlüyü göstəriciləri");
                    }
                    return;
                }
            }

            // Рельеф
            OwnHeight_comm = GlobalVarLn.HCenter_sup;
            GlobalVarLn.H_sup = OwnHeight_comm;

            // Общая высота СП
            HeightTotalOwn_comm = HeightAntennOwn_comm + OwnHeight_comm;
            // отобразить значение высоты
            ichislo = (long)(HeightTotalOwn_comm);
            tbHeightOwnObject.Text = Convert.ToString(ichislo);
            // .........................................................................
            // ОП

            // Антенна ОП
            //HeightTransmitOpponent_comm = Convert.ToDouble(tbOpponentAntenna.Text);   // transmitter 
            ss = tbOpponentAntenna.Text;
            try
            {
                HeightTransmitOpponent_comm = Convert.ToDouble(ss);
            }
            catch (SystemException)
            {
                try
                {
                    if (ss.IndexOf(",") > -1) ss = ss.Replace(',', '.');
                    else ss = ss.Replace('.', ',');
                    HeightTransmitOpponent_comm = Convert.ToDouble(ss);
                }
                catch
                {
                    if (GlobalVarLn.fl_Azb == 0)
                    {
                        MessageBox.Show("Недопустимая высота антенны");
                    }
                    else
                    {
                        //Azb?
                        MessageBox.Show("Yolverilməz Antenanın hündürlüyü göstəriciləri");
                    }
                    return;
                }
            }

            //HeightTransmitOpponent1_comm = Convert.ToDouble(tbOpponentAntenna1.Text); // receiver
            ss = tbOpponentAntenna1.Text;
            try
            {
                HeightTransmitOpponent1_comm = Convert.ToDouble(ss);
            }
            catch (SystemException)
            {
                try
                {
                    if (ss.IndexOf(",") > -1) ss = ss.Replace(',', '.');
                    else ss = ss.Replace('.', ',');
                    HeightTransmitOpponent1_comm = Convert.ToDouble(ss);
                }
                catch
                {
                    if (GlobalVarLn.fl_Azb == 0)
                    {
                        MessageBox.Show("Недопустимая высота антенны");
                    }
                    else
                    {
                        //Azb?
                        MessageBox.Show("Yolverilməz Antenanın hündürlüyü göstəriciləri");
                    }
                    return;
                }
            }

            // Рельеф
            Point1Height_comm = GlobalVarLn.HPoint1_sup;

            // Общая высота ОП
            Pt1HeightTotalOwn_comm = HeightTransmitOpponent_comm + Point1Height_comm;    // transmitter
            Pt1HeightTotalOwn1_comm = iMiddleHeight_comm + HeightTransmitOpponent1_comm; // receiver
            // .........................................................................

            // ************************************************************* !!! Высоты

            // Ввод параметров ********************************************************
            // !!! Координаты SP,OP уже расчитаны и введены по кнопке 'SP','OP'


            String s1 = "";

            // ------------------------------------------------------------------
            // SP

            // Мощность 
            s1 = tbPowerOwn.Text;
            try
            {
                //if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                PowerOwn_comm = Convert.ToDouble(s1);
            }
            catch (SystemException)
            {
                try
                {
                    if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                    else s1 = s1.Replace('.', ',');
                    PowerOwn_comm = Convert.ToDouble(s1);
                }
                catch
                {
                    if (GlobalVarLn.fl_Azb == 0)
                    {
                        MessageBox.Show("Недопустимое значение мощности");
                    }
                    else
                    {
                        //Azb???
                        MessageBox.Show("Yolverilməz parametr göstəriciləri (Güc)");
                    }

                    return;
                }
            }
            if ((PowerOwn_comm < 10) || (PowerOwn_comm > 300))
            {
                if (GlobalVarLn.fl_Azb == 0)
                {
                    MessageBox.Show("Параметр 'мощность' выходит за пределы 10Вт - 300Вт ");
                }
                else
                {
                    //Azb
                    MessageBox.Show("Susdurulacaq vasitənin gücü susdurma dairəsində deyil 10Vt - 300Vt ");
                }
                return;
            }

            // Коэффициент усиления
            s1 = tbCoeffOwn.Text;
            try
            {
                //if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                CoeffOwn_comm = Convert.ToDouble(s1);
            }
            catch (SystemException)
            {
                try
                {
                    if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                    else s1 = s1.Replace('.', ',');
                    CoeffOwn_comm = Convert.ToDouble(s1);
                }
                catch
                {
                    if (GlobalVarLn.fl_Azb == 0)
                    {
                        MessageBox.Show("Недопустимое значение Кусиления");
                    }
                    else
                    {
                        //Azb???
                        MessageBox.Show("Yolverilməz parametr göstəriciləri (Gücləndirmə əmsalı)");

                    }
                    return;
                }
            }
            if ((CoeffOwn_comm < 4) || (CoeffOwn_comm > 8))
            {
                if (GlobalVarLn.fl_Azb == 0)
                {
                    MessageBox.Show("Параметр 'Кусиления' выходит за пределы 4-8");
                }
                else
                {
                    //Azb
                    MessageBox.Show("Susdurulacaq vasitənin gücləndirmə əmsalının göstəriciləri susdurma dairəsindən uzaqdadır 4-8");
                }
                return;
            }

            // Коэффициент подавления
            s1 = tbCoeffSupOwn.Text;
            try
            {
                //if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                CoeffOwnPod_comm = Convert.ToDouble(s1);
            }
            catch (SystemException)
            {
                try
                {
                    if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                    else s1 = s1.Replace('.', ',');
                    CoeffOwnPod_comm = Convert.ToDouble(s1);
                }
                catch
                {
                    if (GlobalVarLn.fl_Azb == 0)
                    {
                        MessageBox.Show("Недопустимое значение Кподавления");
                    }
                    else
                    {
                        //Azb???
                        MessageBox.Show("Yolverilməz parametr göstəriciləri (Susdurma əmsalı)");
                    }

                    return;
                }
            }
            if ((CoeffOwnPod_comm < 1) || (CoeffOwnPod_comm > 100))
            {
                if (GlobalVarLn.fl_Azb == 0)
                {
                    MessageBox.Show("Параметр 'Кподавления' выходит за пределы 1-100");
                }
                else
                {
                    //Azb???
                    MessageBox.Show("Yolverilməz parametr göstəriciləri (Susdurma əmsalı 1-100)");
                }
                return;
            }

            // ------------------------------------------------------------------
            // OP

            // Мощность 
            s1 = textBox2.Text;
            try
            {
                //if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                PowerOpponent_comm = Convert.ToDouble(s1);
            }
            catch (SystemException)
            {
                try
                {
                    if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                    else s1 = s1.Replace('.', ',');
                    PowerOpponent_comm = Convert.ToDouble(s1);
                }
                catch
                {
                    if (GlobalVarLn.fl_Azb == 0)
                    {
                        MessageBox.Show("Недопустимое значение мощности");
                    }
                    else
                    {
                        //Azb???
                        MessageBox.Show("Yolverilməz parametr göstəriciləri (Güc)");
                    }

                    return;
                }

            }
            if ((PowerOpponent_comm < 1) || (PowerOpponent_comm > 100))
            {
                if (GlobalVarLn.fl_Azb == 0)
                {
                    MessageBox.Show("Параметр 'мощность' выходит за пределы 1Вт - 100Вт");
                }
                else
                {
                    //Azb
                    MessageBox.Show("Susdurulacaq hədəfin gücü susdurma dairəsində deyil 1Vt - 100Vt");
                }
                return;
            }

            // коэффициент усиления 
            s1 = textBox3.Text;
            try
            {
                //if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                CoeffTransmitOpponent_comm = Convert.ToDouble(s1);
            }
            catch (SystemException)
            {
                try
                {
                    if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                    else s1 = s1.Replace('.', ',');
                    CoeffTransmitOpponent_comm = Convert.ToDouble(s1);
                }
                catch
                {
                    if (GlobalVarLn.fl_Azb == 0)
                    {
                        MessageBox.Show("Недопустимое значение Кусиления");
                    }
                    else
                    {
                        //Azb???
                        MessageBox.Show("Yolverilməz parametr göstəriciləri (Gücləndirmə əmsalı)");
                    }

                    return;
                }
            }
            if ((CoeffTransmitOpponent_comm < 1) || (CoeffTransmitOpponent_comm > 10))
            {
                if (GlobalVarLn.fl_Azb == 0)
                {
                    MessageBox.Show("Параметр 'Кусиления' выходит за пределы 1-10");
                }
                else
                {
                    //Azb
                    MessageBox.Show("Susdurulacaq vasitənin gücləndirmə əmsalının göstəriciləri susdurma dairəsindən uzaqdadır 1-10");
                }
                return;
            }
            // ------------------------------------------------------------------

            // ******************************************************** Ввод параметров


            // Расчет зоны ************************************************************
            GlobalVarLn.fl_ZoneSuppression = 0;

            dDistanceObject = DefineDistanceObject(GlobalVarLn.tpOwnCoordRect_sup, GlobalVarLn.tpPoint1Rect_sup);

            // ЗОНА ПОДАВЛЕНИЯ
            // !!! Receiver
            dRadiusZoneSup = DefineDistanceSup((int)OwnHeight_comm, (int)HeightAntennOwn_comm, (int)Pt1HeightTotalOwn1_comm, iMiddleHeight_comm);
            GlobalVarLn.RZ_sup = dRadiusZoneSup;
            tbRadiusZone.Text = Convert.ToString((int)dRadiusZoneSup);

            iGamma = 1;
            dKp1 = CoeffOwnPod_comm; // К подавления СП

            dCoeffA1 = DefineCoeffA(
                                   (int)PowerOwn_comm,         // Psp
                                   CoeffOwn_comm,              // К усиления СП 
                                   iGamma,                     // =1
                                   (int)PowerOpponent_comm,    // Pop
                                   CoeffTransmitOpponent_comm, // К усиления ОП
                                   dKp1,                       // Kpod
                                   HeightAntennOwn_comm,       // ANTsp
                                   HeightTransmitOpponent_comm,// ANTprd
                                   HeightTransmitOpponent1_comm // ANTprm
                                   );

            //dCoeffA1 = 0.1;

            // Зона неподавления
            dRadiusZone1 = DefineZoneNotSup(dDistanceObject, ref dCoeffA1);
            GlobalVarLn.RZ1_sup = dRadiusZone1;
            tbRadiusZone1.Text = Convert.ToString((int)dRadiusZone1);

            dDelta1 = DefineDelta(dDistanceObject, dCoeffA1);

            GlobalVarLn.tpPointCentre1_sup.X = 0;
            GlobalVarLn.tpPointCentre1_sup.Y = 0;
            if (GlobalVarLn.flA_sup == 2)
                GlobalVarLn.tpPointCentre1_sup = DefineCentreRNS(GlobalVarLn.tpOwnCoordRect_sup, GlobalVarLn.tpPoint1Rect_sup, dDelta1);
            else if (GlobalVarLn.flA_sup == 3)
                GlobalVarLn.tpPointCentre1_sup = DefineCentreRNS(GlobalVarLn.tpPoint1Rect_sup, GlobalVarLn.tpOwnCoordRect_sup, dDelta1);

            // ????? Dlja chego?
/*
            iXmin = 0;
            iXmin = DefineXmin(GlobalVarLn.tpOwnCoordRect_sup.X, dRadiusZoneSup);
            iXmax = 0;
            iXmax = DefineXmax(GlobalVarLn.tpOwnCoordRect_sup.X, dRadiusZoneSup);
            GlobalVarLn.tpPointMiddle_sup = DefineCoordMiddleDistance(GlobalVarLn.tpOwnCoordRect_sup, GlobalVarLn.tpPoint1Rect_sup);
*/
            // ************************************************************ Расчет зоны


            // DRAW *******************************************************************
            // ------------------------------------------------------------------------ 
            // Убрать с карты
            GlobalVarLn.axMapScreenGlobal.Repaint();
            // ------------------------------------------------------------------------
            ClassMap.f_Map_Zon_Suppression(
                                           GlobalVarLn.tpOwnCoordRect_sup,
                                           1,
                                           (long)dRadiusZoneSup
                                          ); 
            ClassMap.DrawPolygon(GlobalVarLn.listControlJammingZone, Color.Red);

            if (GlobalVarLn.flA_sup == 2)
            {
                ClassMap.f_Map_Zon_Suppression(
                                               GlobalVarLn.tpPointCentre1_sup,
                                               2,
                                               (long)dRadiusZone1
                                              );

            }

            else if (GlobalVarLn.flA_sup == 3) // SP
            {
                ClassMap.f_Map_Zon_Suppression(
                                               //GlobalVarLn.tpOwnCoordRect_sup,
                                               GlobalVarLn.tpPointCentre1_sup,
                                               2,
                                               (long)dRadiusZone1
                                              );

            }

            else
            {
                MessageBox.Show("A=1");
            }

            GlobalVarLn.fl_ZoneSuppression = 1;

            // ******************************************************************* DRAW


        } // Zona (ACCEPT)
        // ************************************************************************


        // FUNCTIONS_ZONE ********************************************************

        // ************************************************************************
        // Функция определения средней высота местности
        // ************************************************************************

        private int DefineMiddleHeight_Comm(Point tpReferencePoint, axMapPoint axMapPointTemp, AxaxcMapScreen AxaxcMapScreenTemp)
        {
            int iRadius = 30000;

            int iStep = 100;
            int iCount = 0;
            double dMiddleHeightStep = 0;
            int iMiddleHeight = 0;

            if ((tpReferencePoint.X > 0) & (tpReferencePoint.Y > 0))
            {
                int iMinX = 0;
                int iMinY = 0;
                int iMaxX = 0;
                int iMaxY = 0;

                iMinX = tpReferencePoint.X - iRadius;
                iMinY = tpReferencePoint.Y - iRadius;
                iMaxX = tpReferencePoint.X + iRadius;
                iMaxY = tpReferencePoint.Y + iRadius;

                // пройти по координатам карты с шагом Shag
                for (int i = iMinX; i < iMaxX; i = i + iStep)
                {
                    for (int j = iMinY; j < iMaxY; j = j + iStep)
                    {
                        double dSetX = 0;
                        double dSetY = 0;
                        dSetX = i;
                        dSetY = j;

                        axMapPointTemp.SetPoint(dSetX, dSetY);
                        dMiddleHeightStep = AxaxcMapScreenTemp.PointHeight_get(axMapPointTemp);

                        // 6_9_18
                        if (dMiddleHeightStep < 0)
                        {
                            dMiddleHeightStep = 0;
                        }

                        // увеличить счетчик на 1
                        iCount++;

                        // суммировать высоты
                        iMiddleHeight = (int)((double)iMiddleHeight + dMiddleHeightStep);

                    }
                }

                // средняя высота = сумма всех полученных высот/на кол-во пройденных точек     
                iMiddleHeight = (int)((double)iMiddleHeight / (double)iCount);

                if (iMiddleHeight < 0)
                    iMiddleHeight = 0;

            } // IF

            return iMiddleHeight;

        } // MiddleHeight
        // ************************************************************************

       // *************************************************************************
        // функция определения расстояния между средством и объектом подавления
        // ************************************************************************

        private double DefineDistanceObject(Point tpPointOwn, Point tpPointOpponent)
        {
            double dDistObject = 0;
            double x1 = 0;
            double y1 = 0;
            double x2 = 0;
            double y2 = 0;

            x1 = (double)tpPointOwn.X;
            y1 = (double)tpPointOwn.Y;
            x2 = (double)tpPointOpponent.X;
            y2 = (double)tpPointOpponent.Y;

            dDistObject = Math.Sqrt((x1 - x2) * (x1 - x2) +
                (y1 - y2) * (y1 - y2));

            return dDistObject;

        } // DefineDistanceObject
        // ************************************************************************

        // ************************************************************************
        // Радиус зоны подавления
        // ************************************************************************

        private double DefineDistanceSup(
                                         int iHeightCoordOwn,  // рельеф СП
                                         int iHeightAntenOwn,  // антенна СП
                                         int iHeightPlaneOpponent, // Hобщая ОП(Нсредн+Антенна(ПРМ))
                                         int mdl
                                         )
        {
            double dDistSup = 0;
            double l_p = 0;
            double l_pr = 0;

            /*
                        l_p = iHeightCoordOwn + iHeightAntenOwn - mdl;
                        if (l_p < 0)
                        {
                            l_p = 0;
                        }

                        l_pr = iHeightPlaneOpponent - mdl;

                        if (l_pr < 0)
                        {
                            l_pr = 0;
                        }
             */


            // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            double mdl1 = Math.Min(iHeightCoordOwn, mdl);

            l_p = iHeightCoordOwn + iHeightAntenOwn - mdl1;
            l_pr = iHeightPlaneOpponent - mdl1;
            // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

            dDistSup = 4120 * (Math.Sqrt(l_p) + Math.Sqrt(l_pr));

            return dDistSup;
        } // Rzone_pod
        // ************************************************************************

        // ************************************************************************
        // функция определения коэффициента A
        // ************************************************************************

        private double DefineCoeffA(
                                   int iPowerOwn,           // Psp
                                   double dCoeffOwn,        // Kyssp
                                   int iGamma,              // =1
                                   int iPowerOpponent,      // Pop
                                   double dCoeffOpponent,   // Kysop
                                   double dKp,              // Kpod
                                   double HASP,             // ANTsp
                                   double HAOP,             // ANTprdop
                                   double HAOP1             // ANTprmop
                                  )
        {
            double dCoeffA = 0;
            double A1 = 0;

            if ((HAOP > 50) || (HAOP1 > 50))
                dCoeffA = Math.Sqrt((iPowerOwn * dCoeffOwn * iGamma) / (iPowerOpponent * dCoeffOpponent * dKp));
            else
            {
                A1 = ((iPowerOwn * dCoeffOwn * iGamma) / (iPowerOpponent * dCoeffOpponent * dKp)) *
                    ((HASP * HASP) / (HAOP * HAOP));

                dCoeffA = Math.Pow(A1, 0.25);

            }
            return dCoeffA;
        } // A
        // ************************************************************************

        // ************************************************************************
        // функция определения зоны неподавления
        // ************************************************************************
        //777

        private double DefineZoneNotSup(double dDistObj, ref double dCoeffA)
        {
            double dRadZone = 0;
            double A2 = 0;

/*
            if (Math.Abs(dCoeffA - 1) < 0.001)
            {
                A2 = 1.001;
            }
            if (dCoeffA > 1 - 0.001)
            {
                GlobalVarLn.flA_sup = 2; // Зона2-вокруг ОП
                A2 = dCoeffA;
            }
            else
            {
                GlobalVarLn.flA_sup = 3; // Зона2-вокругSP
                A2 = 1 / dCoeffA;
            }
*/

            // IF1
            if (Math.Abs(dCoeffA - 1) <= 0.001)
            {
                GlobalVarLn.flA_sup = 2; // Зона2-вокруг ОП
                A2 = 1.001;
            }

            else  // On IF1
            {
                if (dCoeffA > (1 + 0.001))
                {
                    GlobalVarLn.flA_sup = 2; // Зона2-вокруг ОП
                    A2 = dCoeffA;
                }
                else
                {
                    GlobalVarLn.flA_sup = 3; // Зона2-вокругSP
                    A2 = 1 / dCoeffA;
                }
            } // else1

            dRadZone = (dDistObj * A2) / (A2 * A2 - 1);
            return dRadZone;

        } // Rzone_NOsup
        // ************************************************************************

        // ************************************************************************
        // функция определения дельта
        // ************************************************************************
        //777

        private double DefineDelta(double dDistObj, double dCoeffA)
        {
            double dDelta = 0;
            double A1 = 0;

/*
            if (dCoeffA > 1)
            {
                A1 = dCoeffA;
            }
            else
            {
                A1 = 1 / dCoeffA;
            }
*/
            // IF1
            if (Math.Abs(dCoeffA - 1) <= 0.001)
            {
                A1 = 1.001;
            }

            else  // On IF1
            {
                // ***
                if (dCoeffA > 1 + 0.001)
                {
                    A1 = dCoeffA;
                }
                else
                {
                    A1 = 1 / dCoeffA;
                }
            } // else1


            dDelta = dDistObj / (A1 * A1 - 1);
            return dDelta;

        } // Delta
        // ************************************************************************

        // ************************************************************************
        // Определение центра зоны неподавления
        // ************************************************************************

        private Point DefineCentreRNS(Point tpPointOwn, Point tpPointOpponent, double dDelta)

        {
        
            Point tpPointCentre = new Point();

            double dHypotenuse = 0;
            double dLegTriangle1 = 0;
            double dLegTriangle2 = 0;
            double dLegTriangle3 = 0;

            double dAlfaRad = 0;

            double x1 = 0;
            double y1 = 0;
            double x2 = 0;
            double y2 = 0;

            x1 = (double)tpPointOwn.X;
            y1 = (double)tpPointOwn.Y;
            x2 = (double)tpPointOpponent.X;
            y2 = (double)tpPointOpponent.Y;

            if (dDelta == 0)
            {
                tpPointCentre.X = 0;
                tpPointCentre.Y = 0;
                return tpPointCentre;
            }

            tpPointCentre.X = 0;
            tpPointCentre.Y = 0;

            if (tpPointOwn.X == tpPointOpponent.X)
                tpPointOpponent.X = tpPointOpponent.X + 1;

            if (tpPointOwn.Y == tpPointOpponent.Y)
                tpPointOpponent.Y = tpPointOpponent.Y + 1;

            dHypotenuse = Math.Sqrt((x1 - x2) * (x1 - x2) +
                (y1 - y2) * (y1 - y2));

            // если попала точка в первую четверть
            if ((tpPointOpponent.X > tpPointOwn.X) & (tpPointOpponent.Y > tpPointOwn.Y))
            {
                dLegTriangle1 = 0;
                dLegTriangle1 = tpPointOpponent.X - tpPointOwn.X;

                dAlfaRad = Math.Asin(dLegTriangle1 / dHypotenuse);


                dLegTriangle2 = 0;
                dLegTriangle2 = dDelta * Math.Sin(dAlfaRad);

                tpPointCentre.X = (int)(tpPointOpponent.X + dLegTriangle2);

                dLegTriangle3 = 0;
                dLegTriangle3 = dDelta * Math.Cos(dAlfaRad);


                tpPointCentre.Y = (int)(tpPointOpponent.Y + dLegTriangle3);

            }

            // если попала точка во вторую четверть
            if ((tpPointOpponent.X > tpPointOwn.X) & (tpPointOpponent.Y < tpPointOwn.Y))
            {
                dLegTriangle1 = 0;
                dLegTriangle1 = tpPointOpponent.X - tpPointOwn.X;

                dAlfaRad = Math.Acos(dLegTriangle1 / dHypotenuse);


                dLegTriangle2 = 0;
                dLegTriangle2 = dDelta * Math.Cos(dAlfaRad);

                tpPointCentre.X = (int)(tpPointOpponent.X + dLegTriangle2);

                dLegTriangle3 = 0;
                dLegTriangle3 = dDelta * Math.Sin(dAlfaRad);


                tpPointCentre.Y = (int)(tpPointOpponent.Y - dLegTriangle3);
            }

            // если попала точка в третюю четверть
            if ((tpPointOpponent.X < tpPointOwn.X) & (tpPointOpponent.Y < tpPointOwn.Y))
            {
                dLegTriangle1 = 0;
                dLegTriangle1 = tpPointOwn.X - tpPointOpponent.X;

                dAlfaRad = Math.Asin(dLegTriangle1 / dHypotenuse);


                dLegTriangle2 = 0;
                dLegTriangle2 = dDelta * Math.Sin(dAlfaRad);

                tpPointCentre.X = (int)(tpPointOpponent.X - dLegTriangle2);

                dLegTriangle3 = 0;
                dLegTriangle3 = dDelta * Math.Cos(dAlfaRad);


                tpPointCentre.Y = (int)(tpPointOpponent.Y - dLegTriangle3);
            }

            // если попала точка в третюю четверть
            if ((tpPointOpponent.X < tpPointOwn.X) & (tpPointOpponent.Y > tpPointOwn.Y))
            {
                dLegTriangle1 = 0;
                dLegTriangle1 = tpPointOwn.X - tpPointOpponent.X;

                dAlfaRad = Math.Acos(dLegTriangle1 / dHypotenuse);


                dLegTriangle2 = 0;
                dLegTriangle2 = dDelta * Math.Cos(dAlfaRad);

                tpPointCentre.X = (int)(tpPointOpponent.X - dLegTriangle2);

                dLegTriangle3 = 0;
                dLegTriangle3 = dDelta * Math.Sin(dAlfaRad);


                tpPointCentre.Y = (int)(tpPointOpponent.Y + dLegTriangle3);
            }

            return tpPointCentre;

        } // Определение центра зоны неподавления
        // ************************************************************************


        // ******************************************************** FUNCTIONS_ZONE


        // ************************************************************************
        // функция расчета ДПВ

        public int CountDSR(Point p)
        {
            var iOpponAntenComm = HeightTransmitOpponent1_comm;
            var heightAntennOwnComm = HeightAntennOwn_comm;
            var heightTotalOwnComm = HeightTotalOwn_comm;
            var iMiddleHeightComm = iMiddleHeight_comm;
            var OwnHeightComm = OwnHeight_comm;
            var HeightOpponentComm = Pt1HeightTotalOwn1_comm;

            return CountDSR((int)heightTotalOwnComm, (int)OwnHeightComm, (int)iMiddleHeightComm, (int)HeightOpponentComm);

        }

        private int CountDSR(int iHeightTotalOwn, int iHeightOwnObj, int iHeightMiddle, int iHeightOpponentObj)
        {
            int iDSR = 0;
            int iHeightMin = 0;
            int h1 = 0;
            int h2 = 0;

            // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            if (iHeightMiddle < iHeightOwnObj)
            {
                h1 = iHeightTotalOwn - iHeightMiddle;
                h2 = iHeightOpponentObj - iHeightMiddle; // Hop=Hsredn+Hant

            } // iHeightMiddle < iHeightOwnObj

            else // iHeightMiddle > iHeightOwnObj
            {
                h1 = iHeightTotalOwn - iHeightOwnObj;
                h2 = iHeightOpponentObj - iHeightOwnObj; // Hop=Hsredn+Hant

            } // iHeightMiddle > iHeightOwnObj

            // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

            iDSR = (int)(4.12 * (Math.Pow(h1, 0.5) + Math.Pow(h2, 0.5)));

            // рассчитать ДПВ по формуле
            iDSR = iDSR * 1000;

            return iDSR;
        }

        // ************************************************************************

        // ************************************************************************
        // функция расчета точек ЗПВ

        void CountPointLSR(Point tpCenterLSR,
                           int iHeightCenterLSR,     // HeightTotalOwn_comm
                           int iHeightAnten,         //HeightAntennOwn_comm
                           double dDSR,
                           int iHeightAntenOpponent) // HeightTransmitOpponent1_comm
        {
            var points = CreateLineSightPolygon(tpCenterLSR);
            GlobalVarLn.listControlJammingZone.AddRange(points);
        }
        // --------------------------------------------------------------------------

        public List<Point> CreateLineSightPolygon(Point tpCenterLSR, int maxRadius = int.MaxValue)
        {
            var dsr = CountDSR(tpCenterLSR);

            var heightAntennOwnComm = HeightAntennOwn_comm;
            var heightTotalOwnComm = HeightTotalOwn_comm;
            var iOpponAntenComm = HeightTransmitOpponent1_comm;


            var points = CreateLineSightPolygon(tpCenterLSR, (int)heightTotalOwnComm, dsr, (int)iOpponAntenComm);

            for (int i = 0; i < points.Count; i++)
            {
                var p = points[i];

                double dx = p.X - tpCenterLSR.X;
                double dy = p.Y - tpCenterLSR.Y;

                var distance = Math.Sqrt(dx * dx + dy * dy);
                distance = Math.Min(distance, maxRadius);

                var x = tpCenterLSR.X + Math.Sin(i * Math.PI / 180) * distance;
                var y = tpCenterLSR.Y + Math.Cos(i * Math.PI / 180) * distance;

                points[i] = new Point((int)x, (int)y);
            }

            return points;
        }
        // --------------------------------------------------------------------------

        public List<Point> CreateLineSightPolygon(Point tpCenterLSR,
                                                 int iHeightCenterLSR, // HeightTotalOwn_comm
                                                 double dDSR,
                                                 int iHeightAntenOpponent) // iOpponAnten_comm
        {
            var listKoordReal = new List<Point>();

            double h_Dob;
            double dH;
            double alfa;
            double dL;
            double VarL;
            double H_Line;
            int angleFi;

            // otl***
            //String strFileName;
            //strFileName = "RLF.txt";
            //StreamWriter srFile;
            //srFile = new StreamWriter(strFileName);

            // otl***
            //srFile.WriteLine("DSR =" + Convert.ToString(dDSR));

            // FORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFO
            // пройти в цикле по всем углам   (против часовой стрелки, 1 град)

            for (angleFi = 0; angleFi < 361; angleFi = angleFi + GlobalVarLn.iStepAngleInput_ZPV)
            {

                // otl***
                //srFile.WriteLine("Fi =" + Convert.ToString(angleFi));

                // .................................................................................
                // найти координаты точки реальной ДПВ (от центра на расстоянии ДПВ)

                KoordThree koord2;

                koord2.y = tpCenterLSR.Y + dDSR * Math.Cos((angleFi * Math.PI) / 180);
                koord2.x = tpCenterLSR.X + dDSR * Math.Sin((angleFi * Math.PI) / 180);

                var dSetX = koord2.x;
                var dSetY = koord2.y;

                GlobalVarLn.axMapPointGlobalAdd.SetPoint(dSetX, dSetY);
                koord2.h = GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);

                //GlobalVarLn.axMapPointGlobalAdd.SetPoint(dSetY, dSetX);
                // koord2.h = GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);

                if (koord2.h < 0)
                    koord2.h = 0;

                // антенна ОП
                koord2.h += iHeightAntenOpponent;
                // .................................................................................
                // otl***
                //srFile.WriteLine("(1)" + Convert.ToString(iHeightCenterLSR));
                //srFile.WriteLine("HC =" + Convert.ToString(iHeightCenterLSR));
                //srFile.WriteLine("H2 =" + Convert.ToString(koord2.h));
                // .................................................................................
                // изначально координаты Koord4 равны координатам точки реальной ДПВ

                KoordThree koord4;
                koord4.x = koord2.x;
                koord4.y = koord2.y;
                koord4.h = koord2.h;
                // .................................................................................

                // если значение высоты СП совпадает со значением высоты
                // точки реальной ДПВ
                if (Math.Abs(iHeightCenterLSR - koord2.h) < 1e-3)
                {
                    // увеличить первую на 2 м.
                    iHeightCenterLSR = iHeightCenterLSR + 2;
                }
                // .................................................................................

                // Hsp>Hdpv IF1********************************************************************     
                // если высота СП больше высоты точки реальной ДПВ

                // IF1
                KoordThree koord3;
                KoordThree koordPrev;

                // 27_09_2018
                KoordThree koordPrev1;

                if (iHeightCenterLSR > koord2.h)
                {
                    // -----------------------------------------------------------------------------
                    // otl***
                    //srFile.WriteLine("(2) FIRST");

                    // разница высот (см. рис)
                    dH = iHeightCenterLSR - koord2.h;

                    // угол альфа (см. рис)
                    //alfa = Math.Asin(dH / dDSR);
                    alfa = Math.Atan(dH / dDSR);

                    // координаты текущей точки, удаленной от СП на STEP_LENGTH метров
                    // и отклоненной на angle_fi угол от 0
                    koord3.y = tpCenterLSR.Y + GlobalVarLn.iStepLengthInput_ZPV * Math.Cos((angleFi * Math.PI) / 180);
                    koord3.x = tpCenterLSR.X + GlobalVarLn.iStepLengthInput_ZPV * Math.Sin((angleFi * Math.PI) / 180);
                    dSetX = koord3.x;
                    dSetY = koord3.y;
                    GlobalVarLn.axMapPointGlobalAdd.SetPoint(dSetX, dSetY);
                    koord3.h = GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);
                    if (koord3.h < 0)
                        koord3.h = 0;

                    // расстояние от СП до текущей точки (см. рис)
                    dL = Math.Sqrt((tpCenterLSR.X - koord3.x) * (tpCenterLSR.X - koord3.x) +
                                   (tpCenterLSR.Y - koord3.y) * (tpCenterLSR.Y - koord3.y));

                    // otl***
                    //srFile.WriteLine("(3)");
                    //srFile.WriteLine("dL =" + Convert.ToString(dL));
                    //srFile.WriteLine("A =" + Convert.ToString(alfa));
                    //srFile.WriteLine("dH =" + Convert.ToString(dH));
                    //srFile.WriteLine("H3 =" + Convert.ToString(koord3.h));
                    // -----------------------------------------------------------------------------

                    // WHILE1 ----------------------------------------------------------------------
                    // пока не достигнута реальная ДПВ     

                    // WHILE1
                    while (dL < dDSR)
                    {
                        // (см. рис)
                        VarL = dDSR - dL;

                        // высота воображаемой линии в точки с координатами Koord3
                        //H_Line = VarL * Math.Sin(alfa) + koord2.h;
                        H_Line = VarL * Math.Tan(alfa) + koord2.h;

                        h_Dob = dL * (dDSR - dL) / (2 * GlobalVarLn.RADIUS_EARTH);
                        koord3.h = koord3.h + h_Dob;

                        // 27_09_2018
                        koordPrev1 = koord3;

                        // otl***
                        //srFile.WriteLine("(4)");
                        //srFile.WriteLine("HLine =" + Convert.ToString(H_Line));
                        //srFile.WriteLine("A =" + Convert.ToString(alfa));
                        //srFile.WriteLine("HDob =" + Convert.ToString(h_Dob));
                        //srFile.WriteLine("H3 =" + Convert.ToString(koord3.h));

                        // IF2 .....................................................................
                        // если значение высоты воображаемой линии меньше поверхности земли в этой точке

                        // IF2
                        if (koord3.h > H_Line)
                        {
                            koordPrev.x = 0;
                            koordPrev.y = 0;
                            koordPrev.h = 0;

                            // Словили выход рельефа над линией видимости
                            koordPrev = koord3;

                            // увеличить рсстояние от СП на STEP_LENGTH метров
                            dL = dL + GlobalVarLn.iStepLengthInput_ZPV;

                            // otl***
                            //srFile.WriteLine("(5)");
                            //srFile.WriteLine("dL =" + Convert.ToString(dL));
                            //srFile.WriteLine("HPrev =" + Convert.ToString(koordPrev.h));

                            // WHILE2 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

                            // WHILE2
                            while (dL < dDSR)
                            {
                                // (см. рис)
                                VarL = dDSR - dL;

                                // расссчитать координаты в этой точке
                                koord3.y = tpCenterLSR.Y + dL * Math.Cos((angleFi * Math.PI) / 180);
                                koord3.x = tpCenterLSR.X + dL * Math.Sin((angleFi * Math.PI) / 180);
                                dSetX = koord3.x;
                                dSetY = koord3.y;
                                GlobalVarLn.axMapPointGlobalAdd.SetPoint(dSetX, dSetY);
                                koord3.h = GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);
                                if (koord3.h < 0)
                                    koord3.h = 0;

                                h_Dob = dL * (dDSR - dL) / (2 * GlobalVarLn.RADIUS_EARTH);
                                koord3.h = koord3.h + h_Dob;

                                // 27_09_2018
                                //koordPrev1 = koord3;

                                // otl***
                                //srFile.WriteLine("6");
                                //srFile.WriteLine("dL=" + Convert.ToString(dL));
                                //srFile.WriteLine("H3=" + Convert.ToString(koord3.h));
                                //srFile.WriteLine("HDob=" + Convert.ToString(h_Dob));
                                //srFile.WriteLine("HPrev=" + Convert.ToString(koordPrev.h));

                                // Еще идем вверх
                                if (koord3.h > koordPrev.h)
                                {
                                    koordPrev = koord3;
                                    dL = dL + GlobalVarLn.iStepLengthInput_ZPV;

                                    // otl***
                                    //srFile.WriteLine("Dalshe1");

                                    // 27_09_2018
                                    koordPrev1 = koord3;
                                }
                                else // Пошли вниз
                                {
                                    // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                                    // 1109

                                    // Уже не видим ОП
                                    if ((koordPrev.h - koord3.h) > iHeightAntenOpponent)
                                    {
                                        // 27_09_2018
                                        //koord3 = koordPrev;
                                        koord3 = koordPrev1;

                                        // otl***
                                        //srFile.WriteLine("Exit1");
                                        // выйти из цикла while2
                                        dL = dDSR + 1;
                                    }
                                    else // Еще видим ОП
                                    {
                                        // otl***
                                        //srFile.WriteLine("Exit1");
                                        // выйти из цикла while
                                        //dL = dDSR + 1;
                                        //koordPrev = koord3; // &&&&&&&&&&&

                                        dL = dL + GlobalVarLn.iStepLengthInput_ZPV;
                                        // otl***
                                        // srFile.WriteLine("Dalshe1_1");

                                        // 27_09_2018
                                        koordPrev1 = koord3;
                                    }
                                    // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

                                } // ELSE

                            } // WHILE2
                            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> WHILE2

                            // присвоить Koord4 значения текущих координат
                            koord4.x = koord3.x;
                            koord4.y = koord3.y;
                            koord4.h = koord3.h;

                            // выйти из цикла while1
                            dL = dDSR + 1;

                        } // IF2 (высота рельефа больше воображаемой линии)
                        //  ..................................................................... IF2

                        // ELSE по IF2 ..............................................................
                        // если значение высоты воображаемой линии больше или равно
                        // поверхности земли в этой точке

                        else
                        {
                            // увеличить рсстояние от СП на STEP_LENGTH метров
                            dL = dL + GlobalVarLn.iStepLengthInput_ZPV;

                            // расссчитать координаты в этой точке
                            koord3.y = tpCenterLSR.Y + dL * Math.Cos((angleFi * Math.PI) / 180);
                            koord3.x = tpCenterLSR.X + dL * Math.Sin((angleFi * Math.PI) / 180);

                            dSetX = koord3.x;
                            dSetY = koord3.y;

                            GlobalVarLn.axMapPointGlobalAdd.SetPoint(dSetX, dSetY);
                            koord3.h = GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);

                            if (koord3.h < 0)
                                koord3.h = 0;

                            // otl***
                            //rFile.WriteLine("(5_1)");
                            //srFile.WriteLine("dL =" + Convert.ToString(dL));
                            //srFile.WriteLine("H3 =" + Convert.ToString(koord3.h));

                        } // ELSE po IF2 (высота воображаемой линии больше или равна рельефу в этой точке)
                        // .............................................................. ELSE по IF2

                    } // конец WHILE1 (dL<dDSR)
                    // ---------------------------------------------------------------------- WHILE1

                    // otl***
                    //srFile.WriteLine("END WHILE: dL<DSR");

                    // записать конечный результат расчета координат
                    // с углом angle_fi
                    listKoordReal.Add(new Point((int)koord4.x, (int)koord4.y));

                    // otl***
                    //srFile.WriteLine("(6)");
                    //srFile.WriteLine("X3 =" + Convert.ToString(koord3.x));
                    //rFile.WriteLine("Y3 =" + Convert.ToString(koord3.y));
                    //srFile.WriteLine("H3 =" + Convert.ToString(koord3.h));
                    //srFile.WriteLine("X4 =" + Convert.ToString(koord4.x));
                    //srFile.WriteLine("Y4 =" + Convert.ToString(koord4.y));
                    //srFile.WriteLine("H4 =" + Convert.ToString(koord4.h));

                } // IF1 
                // ******************************************************************* Hsp>Hdpv IF1

                // Hsp<Hdpv ELSE po IF1 ***********************************************************
                // если высота СП меньше высоты точки реальной ДПВ   

                else
                {
                    // разница высот (см. рис)
                    dH = koord2.h - iHeightCenterLSR;

                    // угол альфа (см. рис)
                    //alfa = Math.Asin(dH / dDSR);
                    alfa = Math.Atan(dH / dDSR);

                    // координаты текущей точки, удаленной от СП на len метров
                    // и отклоненной на angle_fi угол от 0
                    koord3.y = tpCenterLSR.Y + GlobalVarLn.iStepLengthInput_ZPV * Math.Cos((angleFi * Math.PI) / 180);
                    koord3.x = tpCenterLSR.X + GlobalVarLn.iStepLengthInput_ZPV * Math.Sin((angleFi * Math.PI) / 180);
                    dSetX = koord3.x;
                    dSetY = koord3.y;
                    GlobalVarLn.axMapPointGlobalAdd.SetPoint(dSetX, dSetY);
                    koord3.h = GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);
                    if (koord3.h < 0)
                        koord3.h = 0;

                    // расстояние от СП до текущей точки (см. рис)
                    dL = Math.Sqrt((tpCenterLSR.X - koord3.x) * (tpCenterLSR.X - koord3.x) +
                                   (tpCenterLSR.Y - koord3.y) * (tpCenterLSR.Y - koord3.y));

                    // otl***
                    //srFile.WriteLine("(7)");
                    //srFile.WriteLine("dH =" + Convert.ToString(dH));
                    //srFile.WriteLine("A =" + Convert.ToString(alfa));
                    //srFile.WriteLine("H3 =" + Convert.ToString(koord3.h));
                    //srFile.WriteLine("dL =" + Convert.ToString(dL));

                    // WHILE3 ----------------------------------------------------------------------
                    // пока не достигнута реальная ДПВ     

                    // WHILE3
                    while (dL < dDSR)
                    {
                        // (см. рис)
                        VarL = dL;

                        // высота воображаемой линии в точки с координатами Koord3
                        //H_Line = VarL * Math.Sin(alfa) + koord2.h;
                        //H_Line = VarL * Math.Tan(alfa) + koord2.h;
                        H_Line = VarL * Math.Tan(alfa) + iHeightCenterLSR;

                        h_Dob = dL * (dDSR - dL) / (2 * GlobalVarLn.RADIUS_EARTH);
                        koord3.h = koord3.h + h_Dob;

                        // 27_09_2018
                        koordPrev1 = koord3;

                        // otl***
                        //srFile.WriteLine("(8)");
                        //srFile.WriteLine("HLine =" + Convert.ToString(H_Line));
                        //srFile.WriteLine("HDob =" + Convert.ToString(h_Dob));
                        //srFile.WriteLine("H3 =" + Convert.ToString(koord3.h));

                        // IF3 .....................................................................
                        // если значение высоты воображаемой линии меньше поверхности земли в этой точке

                        // IF3
                        // Словили выход рельефа над линией видимости
                        if (koord3.h > H_Line)
                        {

                            koordPrev.x = 0;
                            koordPrev.y = 0;
                            koordPrev.h = 0;

                            koordPrev = koord3;

                            // увеличить рсстояние от СП на STEP_LENGTH метров
                            dL = dL + GlobalVarLn.iStepLengthInput_ZPV;

                            // otl***
                            //srFile.WriteLine("(9)");
                            //srFile.WriteLine("HPrev =" + Convert.ToString(koordPrev.h));
                            //srFile.WriteLine("dL =" + Convert.ToString(dL));

                            // WHILE4 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

                            // WHILE4
                            while (dL < dDSR)
                            {
                                VarL = dDSR - dL;  // ???????????????????

                                // расссчитать координаты в этой точке
                                koord3.y = tpCenterLSR.Y + dL * Math.Cos((angleFi * Math.PI) / 180);
                                koord3.x = tpCenterLSR.X + dL * Math.Sin((angleFi * Math.PI) / 180);
                                dSetX = koord3.x;
                                dSetY = koord3.y;
                                GlobalVarLn.axMapPointGlobalAdd.SetPoint(dSetX, dSetY);
                                koord3.h = GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);
                                if (koord3.h < 0)
                                    koord3.h = 0;
                                h_Dob = dL * (dDSR - dL) / (2 * GlobalVarLn.RADIUS_EARTH);
                                koord3.h = koord3.h + h_Dob;

                                // 27_09_2018
                                //koordPrev1 = koord3;

                                // otl***
                                //srFile.WriteLine("(10)");
                                //srFile.WriteLine("HDob =" + Convert.ToString(h_Dob));
                                // srFile.WriteLine("H3 =" + Convert.ToString(koord3.h));

                                // Еще идем вверх
                                if (koord3.h > koordPrev.h)
                                {
                                    koordPrev = koord3;
                                    dL = dL + GlobalVarLn.iStepLengthInput_ZPV;

                                    // 27_09_2018
                                    koordPrev1 = koord3;

                                    // otl***
                                    //srFile.WriteLine("dalse2");
                                }
                                else  // Пошли вниз
                                {
                                    // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                                    // 1109

                                    // Уже не видим ОП
                                    if ((koordPrev.h - koord3.h) > iHeightAntenOpponent)
                                    {
                                        // 27_09_2018
                                        //koord3 = koordPrev;
                                        koord3 = koordPrev1;

                                        // otl***
                                        //srFile.WriteLine("Exit2");
                                        // выйти из цикла while4
                                        dL = dDSR + 1;

                                    }
                                    else // Еще видим ОП
                                    {
                                        // otl***
                                        //srFile.WriteLine("Exit1");
                                        // выйти из цикла while
                                        //dL = dDSR + 1;

                                        dL = dL + GlobalVarLn.iStepLengthInput_ZPV;

                                        // 27_09_2018
                                        koordPrev1 = koord3;

                                        // otl***
                                        //srFile.WriteLine("Dalshe2");
                                    }
                                    // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

                                } // else

                            } // WHILE4 
                            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> WHILE4

                            // выйти из цикла while3
                            dL = dDSR + 1;

                            // присвоить Koord4 значения текущих координат
                            koord4.x = koord3.x;
                            koord4.y = koord3.y;
                            koord4.h = koord3.h;

                            // otl***
                            //srFile.WriteLine("(11)");
                            //srFile.WriteLine("dL =" + Convert.ToString(dL));
                            //srFile.WriteLine("H3 =" + Convert.ToString(koord3.h));

                        } // IF3 если значение высоты воображаемой линии меньше поверхности земли в этой точке
                        // ..................................................................... IF3

                        // ELSE po IF3 .............................................................
                        // если значение высоты воображаемой линии блольше или равно
                        // поверхности земли в этой точке   
                        else
                        {
                            // увеличить рсстояние от СП на STEP_LENGTH метров
                            dL = dL + GlobalVarLn.iStepLengthInput_ZPV;

                            // расссчитать координаты в этой точке
                            koord3.y = tpCenterLSR.Y + dL * Math.Cos((angleFi * Math.PI) / 180);
                            koord3.x = tpCenterLSR.X + dL * Math.Sin((angleFi * Math.PI) / 180);
                            dSetX = koord3.x;
                            dSetY = koord3.y;
                            GlobalVarLn.axMapPointGlobalAdd.SetPoint(dSetX, dSetY);
                            koord3.h = GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);
                            if (koord3.h < 0)
                                koord3.h = 0;

                            // otl***
                            // srFile.WriteLine("(12)");
                            // srFile.WriteLine("dL =" + Convert.ToString(dL));
                            // srFile.WriteLine("H3 =" + Convert.ToString(koord3.h));

                        } // Else po IF3
                        // ............................................................. ELSE po IF3

                    } // конец while3 (dL<dDSR)
                    // ---------------------------------------------------------------------- WHILE3

                    // записать конечный результат расчета координат
                    // с углом angle_fi
                    listKoordReal.Add(new Point((int)koord4.x, (int)koord4.y));

                } // ELSE po IF1 (Hsp<Hdpv)
                // *********************************************************** Hsp<Hdpv ELSE po IF1

            } // конец  for (angle_fi=0; angle_fi<361; angle_fi = angle_fi+STEP_ANGLE,j++ )   
            // FORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFO

            //srFile.Close();

            return listKoordReal;
        }
        // ******************************************************************************************************

        // FUNCTIONS_MY  **********************************************************

        // ************************************************************************
        // функция выбора системы координат SP1,SP2,PU
        // ************************************************************************

        private void ChooseSystemCoordSPOP_sup(int iSystemCoord)
        {
            gbOwnRect.Visible = false;
            gbOwnRect42.Visible = false;
            gbOwnRad.Visible = false;
            gbOwnDegMin.Visible = false;
            gbOwnDegMinSec.Visible = false;
            gbPt1Rect.Visible = false;
            gbPt1Rect42.Visible = false;
            gbPt1Rad.Visible = false;
            gbPt1DegMin.Visible = false;
            gbPt1DegMinSec.Visible = false;


            switch (iSystemCoord)
            {
                case 0: // Метры на местности

                    // SP
                    gbOwnRect.Visible = true;
                    gbOwnRect.Location = new Point(10, 35);
                    // OP
                    gbPt1Rect.Visible = true;
                    gbPt1Rect.Location = new Point(10, 35);

                    // SP
                    if (GlobalVarLn.flCoordSP_sup == 1)
                    {
                        //333
                        //ichislo = (long)(GlobalVarLn.XCenter_sup);
                        //tbXRect.Text = Convert.ToString(ichislo);
                        //ichislo = (long)(GlobalVarLn.YCenter_sup);
                        //tbYRect.Text = Convert.ToString(ichislo);

                        ichislo = (long)(GlobalVarLn.BWGS84_1_sup * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        tbXRect.Text = Convert.ToString(dchislo);
                        ichislo = (long)(GlobalVarLn.LWGS84_1_sup * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        tbYRect.Text = Convert.ToString(dchislo);


                    } // IF

                    // OP
                    if (GlobalVarLn.flCoordOP == 1)
                    {
                        //333
                        //ichislo = (long)(GlobalVarLn.XPoint1_sup);
                        //tbPt1XRect.Text = Convert.ToString(ichislo);
                        //ichislo = (long)(GlobalVarLn.YPoint1_sup);
                        //tbPt1YRect.Text = Convert.ToString(ichislo);

                        ichislo = (long)(GlobalVarLn.BWGS84_2_sup * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        tbPt1XRect.Text = Convert.ToString(dchislo);
                        ichislo = (long)(GlobalVarLn.LWGS84_2_sup * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        tbPt1YRect.Text = Convert.ToString(dchislo);

                    }

                    break;

                case 1: // Метры 1942 года

                    // SP
                    gbOwnRect42.Visible = true;
                    gbOwnRect42.Location = new Point(10, 35);
                    // OP
                    gbPt1Rect42.Visible = true;
                    gbPt1Rect42.Location = new Point(10, 35);

                    // SP
                    if (GlobalVarLn.flCoordSP_sup == 1)
                    {
                        ichislo = (long)(GlobalVarLn.XSP42_sup);
                        tbXRect42.Text = Convert.ToString(ichislo);
                        ichislo = (long)(GlobalVarLn.YSP42_sup);
                        tbYRect42.Text = Convert.ToString(ichislo);

                    } // IF

                    // OP
                    if (GlobalVarLn.flCoordOP == 1)
                    {
                        ichislo = (long)(GlobalVarLn.XYS142_sup);
                        tbPt1XRect42.Text = Convert.ToString(ichislo);
                        ichislo = (long)(GlobalVarLn.YYS142_sup);
                        tbPt1YRect42.Text = Convert.ToString(ichislo);

                    }

                    break;

                case 2: // Радианы (Красовский)

                    // SP
                    gbOwnRad.Visible = true;
                    gbOwnRad.Location = new Point(10, 35);
                    // OP
                    gbPt1Rad.Visible = true;
                    gbPt1Rad.Location = new Point(10, 35);

                    // SP
                    if (GlobalVarLn.flCoordSP_sup == 1)
                    {
                        ichislo = (long)(GlobalVarLn.LatKrR_sup * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        tbBRad.Text = Convert.ToString(dchislo);
                        ichislo = (long)(GlobalVarLn.LongKrR_sup * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        tbLRad.Text = Convert.ToString(dchislo);


                    } // IF
                    // OP
                    if (GlobalVarLn.flCoordOP == 1)
                    {
                        ichislo = (long)(GlobalVarLn.LatKrR_YS1_sup * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        tbPt1BRad.Text = Convert.ToString(dchislo);
                        ichislo = (long)(GlobalVarLn.LongKrR_YS1_sup * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        tbPt1LRad.Text = Convert.ToString(dchislo);

                    }
                    break;

                case 3: // Градусы (Красовский)

                    // SP
                    gbOwnDegMin.Visible = true;
                    gbOwnDegMin.Location = new Point(10, 35);
                    // OP
                    gbPt1DegMin.Visible = true;
                    gbPt1DegMin.Location = new Point(10, 35);

                    // SP
                    if (GlobalVarLn.flCoordSP_sup == 1)
                    {
                        ichislo = (long)(GlobalVarLn.LatKrG_sup * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        tbBMin1.Text = Convert.ToString(dchislo);
                        ichislo = (long)(GlobalVarLn.LongKrG_sup * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        tbLMin1.Text = Convert.ToString(dchislo);


                    } // IF
                    // OP
                    if (GlobalVarLn.flCoordOP == 1)
                    {
                        ichislo = (long)(GlobalVarLn.LatKrG_YS1_sup * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        tbPt1BMin1.Text = Convert.ToString(dchislo);
                        ichislo = (long)(GlobalVarLn.LongKrG_YS1_sup * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        tbPt1LMin1.Text = Convert.ToString(dchislo);

                    }

                    break;

                case 4: // Градусы,мин,сек (Красовский)

                    // SP
                    gbOwnDegMinSec.Visible = true;
                    gbOwnDegMinSec.Location = new Point(10, 35);
                    // OP
                    gbPt1DegMinSec.Visible = true;
                    gbPt1DegMinSec.Location = new Point(10, 35);

                    // SP
                    if (GlobalVarLn.flCoordSP_sup == 1)
                    {
                        tbBDeg2.Text = Convert.ToString(GlobalVarLn.Lat_Grad_sup);
                        tbBMin2.Text = Convert.ToString(GlobalVarLn.Lat_Min_sup);
                        ichislo = (long)(GlobalVarLn.Lat_Sec_sup);
                        tbBSec.Text = Convert.ToString(ichislo);
                        tbLDeg2.Text = Convert.ToString(GlobalVarLn.Long_Grad_sup);
                        tbLMin2.Text = Convert.ToString(GlobalVarLn.Long_Min_sup);
                        ichislo = (long)(GlobalVarLn.Long_Sec_sup);
                        tbLSec.Text = Convert.ToString(ichislo);


                    } // IF
                    // OP
                    if (GlobalVarLn.flCoordOP == 1)
                    {
                        tbPt1BDeg2.Text = Convert.ToString(GlobalVarLn.Lat_Grad_YS1_sup);
                        tbPt1BMin2.Text = Convert.ToString(GlobalVarLn.Lat_Min_YS1_sup);
                        ichislo = (long)(GlobalVarLn.Lat_Sec_YS1_sup);
                        tbPt1BSec.Text = Convert.ToString(ichislo);
                        tbPt1LDeg2.Text = Convert.ToString(GlobalVarLn.Long_Grad_YS1_sup);
                        tbPt1LMin2.Text = Convert.ToString(GlobalVarLn.Long_Min_YS1_sup);
                        ichislo = (long)(GlobalVarLn.Long_Sec_YS1_sup);
                        tbPt1LSec.Text = Convert.ToString(ichislo);

                    }

                    break;

                default:
                    break;

            } // SWITCH

        } // ChooseSystemCoordSPOP_sup
        // ************************************************************************


        //  ********************************************************** FUNCTIONS_MY


        // ****************************************************************************************
        // Закрыть форму
        // ****************************************************************************************
        private void FormSuppression_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            Hide();

            GlobalVarLn.f_Open_objFormSuppression = 0;

        } // Closing
        // ****************************************************************************************


        private void label20_Click(object sender, EventArgs e)
        {
            ;
        }

        private void tbRadiusZone1_TextChanged(object sender, EventArgs e)
        {

        }
        // ****************************************************************************************
        // Activated
        // ****************************************************************************************
        private void FormSuppression_Activated(object sender, EventArgs e)
        {
            GlobalVarLn.f_Open_objFormSuppression = 1;

        } // Activated
        // ****************************************************************************************

        private void tbHAnt_TextChanged(object sender, EventArgs e)
        {

        }

        private void cbCenterLSR_SelectedIndexChanged(object sender, EventArgs e)
        {

        }


        private void LanguageChooser()
        {
            var cont = this.Controls;

            if (NumberOfLanguage.Equals(0))
            {
                ChangeLanguageToRu(cont);
                this.Text = "Расчет энергодоступности по зонам";
            }
            if (NumberOfLanguage.Equals(1))
            {
                //ChangeLanguageToEng(cont);
            }
            if (NumberOfLanguage.Equals(2))
            {
                ChangeLanguageToAzer(cont);
                this.Text = "Zonalar üzrə qidalandırılmanın hesabatı";
            }
        }

        private void ChangeLanguageToRu(System.Windows.Forms.Control.ControlCollection cont)
        {
            string lang = "ru-RU";
            foreach (System.Windows.Forms.Control cc in cont)
            {
                if (cc is ComboBox)
                {

                    for (int i = 0; i < (cc as ComboBox).Items.Count; i++)
                    {
                        if (i == 0)
                        {
                            string a = resourcesForTest.GetString(cc.Name + ".Items", new CultureInfo(lang));
                            (cc as ComboBox).Items[i] = a;
                        }
                        else
                        {
                            string a = resourcesForTest.GetString(cc.Name + ".Items" + i.ToString(), new CultureInfo(lang));
                            (cc as ComboBox).Items[i] = a;
                        }
                    }
                }
                //}
                if (cc is Label || cc is Button || cc is RadioButton || cc is TabControl || cc is TabPage)
                {
                    resourcesForTest.ApplyResources(cc, cc.Name, new CultureInfo(lang));
                    //toolTip.SetToolTip(cc, resources.GetString(cc.Name + ".ToolTip", new CultureInfo(lang)));
                }
                if (cc is Panel || cc is GroupBox || cc is ListBox || cc is TabControl || cc is TabPage)
                {
                    ChangeLanguageToRu(cc.Controls);
                }
            }
        }

        private void ChangeLanguageToAzer(System.Windows.Forms.Control.ControlCollection cont)
        {
            string lang = "az-Latn";

            foreach (System.Windows.Forms.Control cc in cont)
            {
                if (cc is ComboBox)
                {

                    for (int i = 0; i < (cc as ComboBox).Items.Count; i++)
                    {
                        if (i == 0)
                        {
                            string a = resourcesForTest.GetString(cc.Name + ".Items", new CultureInfo(lang));
                            (cc as ComboBox).Items[i] = a;
                        }
                        else
                        {
                            string a = resourcesForTest.GetString(cc.Name + ".Items" + i.ToString(), new CultureInfo(lang));
                            (cc as ComboBox).Items[i] = a;
                        }
                    }
                }
                //}
                if (cc is Label || cc is Button || cc is RadioButton || cc is TabControl || cc is TabPage || cc is GroupBox)
                {
                    resourcesForTest.ApplyResources(cc, cc.Name, new CultureInfo(lang));
                    //toolTip.SetToolTip(cc, resources.GetString(cc.Name + ".ToolTip", new CultureInfo(lang)));
                }
                if (cc is Panel || cc is GroupBox || cc is ListBox || cc is TabControl || cc is DataGridView || cc is TabPage)
                {
                    ChangeLanguageToAzer(cc.Controls);
                }

            }
        }

    } // Class
} // Namespace
