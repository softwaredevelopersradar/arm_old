﻿namespace GrozaMap
{
    partial class FormBearing
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbOwnRect = new System.Windows.Forms.GroupBox();
            this.tbYRect = new System.Windows.Forms.TextBox();
            this.lYRect = new System.Windows.Forms.Label();
            this.tbXRect = new System.Windows.Forms.TextBox();
            this.lXRect = new System.Windows.Forms.Label();
            this.tbOwnHeight = new System.Windows.Forms.TextBox();
            this.gbOwnDegMin = new System.Windows.Forms.GroupBox();
            this.tbLMin1 = new System.Windows.Forms.TextBox();
            this.tbBMin1 = new System.Windows.Forms.TextBox();
            this.lLDegMin = new System.Windows.Forms.Label();
            this.lBDegMin = new System.Windows.Forms.Label();
            this.gbOwnDegMinSec = new System.Windows.Forms.GroupBox();
            this.tbLSec = new System.Windows.Forms.TextBox();
            this.tbBSec = new System.Windows.Forms.TextBox();
            this.lMin4 = new System.Windows.Forms.Label();
            this.lMin3 = new System.Windows.Forms.Label();
            this.tbLMin2 = new System.Windows.Forms.TextBox();
            this.tbBMin2 = new System.Windows.Forms.TextBox();
            this.lSec2 = new System.Windows.Forms.Label();
            this.lSec1 = new System.Windows.Forms.Label();
            this.lDeg4 = new System.Windows.Forms.Label();
            this.lDeg3 = new System.Windows.Forms.Label();
            this.tbLDeg2 = new System.Windows.Forms.TextBox();
            this.lLDegMinSec = new System.Windows.Forms.Label();
            this.tbBDeg2 = new System.Windows.Forms.TextBox();
            this.lBDegMinSec = new System.Windows.Forms.Label();
            this.gbOwnRect42 = new System.Windows.Forms.GroupBox();
            this.tbYRect42 = new System.Windows.Forms.TextBox();
            this.lYRect42 = new System.Windows.Forms.Label();
            this.tbXRect42 = new System.Windows.Forms.TextBox();
            this.lXRect42 = new System.Windows.Forms.Label();
            this.lOwnHeight = new System.Windows.Forms.Label();
            this.gbOwnRad = new System.Windows.Forms.GroupBox();
            this.tbLRad = new System.Windows.Forms.TextBox();
            this.lLRad = new System.Windows.Forms.Label();
            this.tbBRad = new System.Windows.Forms.TextBox();
            this.lBRad = new System.Windows.Forms.Label();
            this.lChooseSC = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.tbPt1Height = new System.Windows.Forms.TextBox();
            this.lPt1Height = new System.Windows.Forms.Label();
            this.gbPt1DegMinSec = new System.Windows.Forms.GroupBox();
            this.tbPt1LSec = new System.Windows.Forms.TextBox();
            this.tbPt1BSec = new System.Windows.Forms.TextBox();
            this.lPt1Min4 = new System.Windows.Forms.Label();
            this.lPt1Min3 = new System.Windows.Forms.Label();
            this.tbPt1LMin2 = new System.Windows.Forms.TextBox();
            this.tbPt1BMin2 = new System.Windows.Forms.TextBox();
            this.lPt1Sec2 = new System.Windows.Forms.Label();
            this.lPt1Sec1 = new System.Windows.Forms.Label();
            this.lPt1Deg4 = new System.Windows.Forms.Label();
            this.lPt1Deg3 = new System.Windows.Forms.Label();
            this.tbPt1LDeg2 = new System.Windows.Forms.TextBox();
            this.lPt1LDegMinSec = new System.Windows.Forms.Label();
            this.tbPt1BDeg2 = new System.Windows.Forms.TextBox();
            this.lPt1BDegMinSec = new System.Windows.Forms.Label();
            this.gbPt1Rad = new System.Windows.Forms.GroupBox();
            this.tbPt1LRad = new System.Windows.Forms.TextBox();
            this.lPt1LRad = new System.Windows.Forms.Label();
            this.tbPt1BRad = new System.Windows.Forms.TextBox();
            this.lPt1BRad = new System.Windows.Forms.Label();
            this.gbPt1Rect = new System.Windows.Forms.GroupBox();
            this.tbPt1YRect = new System.Windows.Forms.TextBox();
            this.lPt1YRect = new System.Windows.Forms.Label();
            this.tbPt1XRect = new System.Windows.Forms.TextBox();
            this.lPt1XRect = new System.Windows.Forms.Label();
            this.gbPt1DegMin = new System.Windows.Forms.GroupBox();
            this.tbPt1LMin1 = new System.Windows.Forms.TextBox();
            this.tbPt1BMin1 = new System.Windows.Forms.TextBox();
            this.lPt1LDegMin = new System.Windows.Forms.Label();
            this.lPt1BDegMin = new System.Windows.Forms.Label();
            this.gbPt1Rect42 = new System.Windows.Forms.GroupBox();
            this.tbPt1YRect42 = new System.Windows.Forms.TextBox();
            this.lPt1YRect42 = new System.Windows.Forms.Label();
            this.tbPt1XRect42 = new System.Windows.Forms.TextBox();
            this.lPt1XRect42 = new System.Windows.Forms.Label();
            this.cbChooseSC = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.chbXY1 = new System.Windows.Forms.CheckBox();
            this.label16 = new System.Windows.Forms.Label();
            this.bAccept = new System.Windows.Forms.Button();
            this.bClear = new System.Windows.Forms.Button();
            this.tb1 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tb2 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tb3 = new System.Windows.Forms.TextBox();
            this.button5 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.tbProzr = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.tbR1 = new System.Windows.Forms.TextBox();
            this.tbR2 = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.tbR4 = new System.Windows.Forms.TextBox();
            this.tbR3 = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.tbR8 = new System.Windows.Forms.TextBox();
            this.tbR7 = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.tbR6 = new System.Windows.Forms.TextBox();
            this.tbR5 = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.tbR16 = new System.Windows.Forms.TextBox();
            this.tbR15 = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.tbR14 = new System.Windows.Forms.TextBox();
            this.tbR13 = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.tbR12 = new System.Windows.Forms.TextBox();
            this.tbR11 = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.tbR10 = new System.Windows.Forms.TextBox();
            this.tbR9 = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.tbR20 = new System.Windows.Forms.TextBox();
            this.tbR19 = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.tbR18 = new System.Windows.Forms.TextBox();
            this.tbR17 = new System.Windows.Forms.TextBox();
            this.cbStep = new System.Windows.Forms.ComboBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tb200 = new System.Windows.Forms.TextBox();
            this.gbOwnRect.SuspendLayout();
            this.gbOwnDegMin.SuspendLayout();
            this.gbOwnDegMinSec.SuspendLayout();
            this.gbOwnRect42.SuspendLayout();
            this.gbOwnRad.SuspendLayout();
            this.gbPt1DegMinSec.SuspendLayout();
            this.gbPt1Rad.SuspendLayout();
            this.gbPt1Rect.SuspendLayout();
            this.gbPt1DegMin.SuspendLayout();
            this.gbPt1Rect42.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbOwnRect
            // 
            this.gbOwnRect.Controls.Add(this.tbYRect);
            this.gbOwnRect.Controls.Add(this.lYRect);
            this.gbOwnRect.Controls.Add(this.tbXRect);
            this.gbOwnRect.Controls.Add(this.lXRect);
            this.gbOwnRect.Location = new System.Drawing.Point(10, 18);
            this.gbOwnRect.Name = "gbOwnRect";
            this.gbOwnRect.Size = new System.Drawing.Size(158, 63);
            this.gbOwnRect.TabIndex = 67;
            this.gbOwnRect.TabStop = false;
            this.gbOwnRect.Visible = false;
            // 
            // tbYRect
            // 
            this.tbYRect.Location = new System.Drawing.Point(78, 36);
            this.tbYRect.MaxLength = 7;
            this.tbYRect.Name = "tbYRect";
            this.tbYRect.Size = new System.Drawing.Size(75, 20);
            this.tbYRect.TabIndex = 5;
            // 
            // lYRect
            // 
            this.lYRect.AutoSize = true;
            this.lYRect.Location = new System.Drawing.Point(6, 39);
            this.lYRect.Name = "lYRect";
            this.lYRect.Size = new System.Drawing.Size(55, 13);
            this.lYRect.TabIndex = 6;
            this.lYRect.Text = "L..............";
            // 
            // tbXRect
            // 
            this.tbXRect.Location = new System.Drawing.Point(78, 13);
            this.tbXRect.MaxLength = 7;
            this.tbXRect.Name = "tbXRect";
            this.tbXRect.Size = new System.Drawing.Size(75, 20);
            this.tbXRect.TabIndex = 4;
            // 
            // lXRect
            // 
            this.lXRect.AutoSize = true;
            this.lXRect.Location = new System.Drawing.Point(4, 20);
            this.lXRect.Name = "lXRect";
            this.lXRect.Size = new System.Drawing.Size(59, 13);
            this.lXRect.TabIndex = 3;
            this.lXRect.Text = "B...............";
            // 
            // tbOwnHeight
            // 
            this.tbOwnHeight.BackColor = System.Drawing.SystemColors.Menu;
            this.tbOwnHeight.Location = new System.Drawing.Point(115, 0);
            this.tbOwnHeight.Name = "tbOwnHeight";
            this.tbOwnHeight.Size = new System.Drawing.Size(48, 20);
            this.tbOwnHeight.TabIndex = 64;
            // 
            // gbOwnDegMin
            // 
            this.gbOwnDegMin.Controls.Add(this.tbLMin1);
            this.gbOwnDegMin.Controls.Add(this.tbBMin1);
            this.gbOwnDegMin.Controls.Add(this.lLDegMin);
            this.gbOwnDegMin.Controls.Add(this.lBDegMin);
            this.gbOwnDegMin.Location = new System.Drawing.Point(10, 17);
            this.gbOwnDegMin.Name = "gbOwnDegMin";
            this.gbOwnDegMin.Size = new System.Drawing.Size(158, 63);
            this.gbOwnDegMin.TabIndex = 29;
            this.gbOwnDegMin.TabStop = false;
            this.gbOwnDegMin.Visible = false;
            // 
            // tbLMin1
            // 
            this.tbLMin1.Location = new System.Drawing.Point(75, 36);
            this.tbLMin1.MaxLength = 8;
            this.tbLMin1.Name = "tbLMin1";
            this.tbLMin1.Size = new System.Drawing.Size(76, 20);
            this.tbLMin1.TabIndex = 15;
            // 
            // tbBMin1
            // 
            this.tbBMin1.Location = new System.Drawing.Point(75, 13);
            this.tbBMin1.MaxLength = 8;
            this.tbBMin1.Name = "tbBMin1";
            this.tbBMin1.Size = new System.Drawing.Size(76, 20);
            this.tbBMin1.TabIndex = 13;
            // 
            // lLDegMin
            // 
            this.lLDegMin.AutoSize = true;
            this.lLDegMin.Location = new System.Drawing.Point(11, 39);
            this.lLDegMin.Name = "lLDegMin";
            this.lLDegMin.Size = new System.Drawing.Size(61, 13);
            this.lLDegMin.TabIndex = 12;
            this.lLDegMin.Text = "L................";
            // 
            // lBDegMin
            // 
            this.lBDegMin.AutoSize = true;
            this.lBDegMin.Location = new System.Drawing.Point(11, 16);
            this.lBDegMin.Name = "lBDegMin";
            this.lBDegMin.Size = new System.Drawing.Size(62, 13);
            this.lBDegMin.TabIndex = 10;
            this.lBDegMin.Text = "B................";
            // 
            // gbOwnDegMinSec
            // 
            this.gbOwnDegMinSec.Controls.Add(this.tbLSec);
            this.gbOwnDegMinSec.Controls.Add(this.tbBSec);
            this.gbOwnDegMinSec.Controls.Add(this.lMin4);
            this.gbOwnDegMinSec.Controls.Add(this.lMin3);
            this.gbOwnDegMinSec.Controls.Add(this.tbLMin2);
            this.gbOwnDegMinSec.Controls.Add(this.tbBMin2);
            this.gbOwnDegMinSec.Controls.Add(this.lSec2);
            this.gbOwnDegMinSec.Controls.Add(this.lSec1);
            this.gbOwnDegMinSec.Controls.Add(this.lDeg4);
            this.gbOwnDegMinSec.Controls.Add(this.lDeg3);
            this.gbOwnDegMinSec.Controls.Add(this.tbLDeg2);
            this.gbOwnDegMinSec.Controls.Add(this.lLDegMinSec);
            this.gbOwnDegMinSec.Controls.Add(this.tbBDeg2);
            this.gbOwnDegMinSec.Controls.Add(this.lBDegMinSec);
            this.gbOwnDegMinSec.Location = new System.Drawing.Point(10, 17);
            this.gbOwnDegMinSec.Name = "gbOwnDegMinSec";
            this.gbOwnDegMinSec.Size = new System.Drawing.Size(158, 60);
            this.gbOwnDegMinSec.TabIndex = 30;
            this.gbOwnDegMinSec.TabStop = false;
            this.gbOwnDegMinSec.Visible = false;
            // 
            // tbLSec
            // 
            this.tbLSec.Location = new System.Drawing.Point(102, 36);
            this.tbLSec.MaxLength = 3;
            this.tbLSec.Name = "tbLSec";
            this.tbLSec.Size = new System.Drawing.Size(25, 20);
            this.tbLSec.TabIndex = 23;
            // 
            // tbBSec
            // 
            this.tbBSec.Location = new System.Drawing.Point(103, 12);
            this.tbBSec.MaxLength = 3;
            this.tbBSec.Name = "tbBSec";
            this.tbBSec.Size = new System.Drawing.Size(25, 20);
            this.tbBSec.TabIndex = 18;
            // 
            // lMin4
            // 
            this.lMin4.AutoSize = true;
            this.lMin4.Location = new System.Drawing.Point(91, 35);
            this.lMin4.Name = "lMin4";
            this.lMin4.Size = new System.Drawing.Size(9, 13);
            this.lMin4.TabIndex = 27;
            this.lMin4.Text = "\'";
            // 
            // lMin3
            // 
            this.lMin3.AutoSize = true;
            this.lMin3.Location = new System.Drawing.Point(92, 12);
            this.lMin3.Name = "lMin3";
            this.lMin3.Size = new System.Drawing.Size(9, 13);
            this.lMin3.TabIndex = 26;
            this.lMin3.Text = "\'";
            // 
            // tbLMin2
            // 
            this.tbLMin2.Location = new System.Drawing.Point(67, 35);
            this.tbLMin2.MaxLength = 2;
            this.tbLMin2.Name = "tbLMin2";
            this.tbLMin2.Size = new System.Drawing.Size(25, 20);
            this.tbLMin2.TabIndex = 22;
            // 
            // tbBMin2
            // 
            this.tbBMin2.Location = new System.Drawing.Point(67, 12);
            this.tbBMin2.MaxLength = 2;
            this.tbBMin2.Name = "tbBMin2";
            this.tbBMin2.Size = new System.Drawing.Size(25, 20);
            this.tbBMin2.TabIndex = 16;
            // 
            // lSec2
            // 
            this.lSec2.AutoSize = true;
            this.lSec2.Location = new System.Drawing.Point(132, 36);
            this.lSec2.Name = "lSec2";
            this.lSec2.Size = new System.Drawing.Size(9, 13);
            this.lSec2.TabIndex = 25;
            this.lSec2.Text = "\'";
            // 
            // lSec1
            // 
            this.lSec1.AutoSize = true;
            this.lSec1.Location = new System.Drawing.Point(132, 12);
            this.lSec1.Name = "lSec1";
            this.lSec1.Size = new System.Drawing.Size(9, 13);
            this.lSec1.TabIndex = 24;
            this.lSec1.Text = "\'";
            // 
            // lDeg4
            // 
            this.lDeg4.AutoSize = true;
            this.lDeg4.Location = new System.Drawing.Point(51, 38);
            this.lDeg4.Name = "lDeg4";
            this.lDeg4.Size = new System.Drawing.Size(13, 13);
            this.lDeg4.TabIndex = 21;
            this.lDeg4.Text = "^";
            // 
            // lDeg3
            // 
            this.lDeg3.AutoSize = true;
            this.lDeg3.Location = new System.Drawing.Point(51, 13);
            this.lDeg3.Name = "lDeg3";
            this.lDeg3.Size = new System.Drawing.Size(13, 13);
            this.lDeg3.TabIndex = 20;
            this.lDeg3.Text = "^";
            // 
            // tbLDeg2
            // 
            this.tbLDeg2.Location = new System.Drawing.Point(23, 36);
            this.tbLDeg2.MaxLength = 2;
            this.tbLDeg2.Name = "tbLDeg2";
            this.tbLDeg2.Size = new System.Drawing.Size(25, 20);
            this.tbLDeg2.TabIndex = 19;
            // 
            // lLDegMinSec
            // 
            this.lLDegMinSec.AutoSize = true;
            this.lLDegMinSec.Location = new System.Drawing.Point(6, 36);
            this.lLDegMinSec.Name = "lLDegMinSec";
            this.lLDegMinSec.Size = new System.Drawing.Size(13, 13);
            this.lLDegMinSec.TabIndex = 17;
            this.lLDegMinSec.Text = "L";
            // 
            // tbBDeg2
            // 
            this.tbBDeg2.Location = new System.Drawing.Point(24, 10);
            this.tbBDeg2.MaxLength = 2;
            this.tbBDeg2.Name = "tbBDeg2";
            this.tbBDeg2.Size = new System.Drawing.Size(25, 20);
            this.tbBDeg2.TabIndex = 15;
            // 
            // lBDegMinSec
            // 
            this.lBDegMinSec.AutoSize = true;
            this.lBDegMinSec.Location = new System.Drawing.Point(5, 14);
            this.lBDegMinSec.Name = "lBDegMinSec";
            this.lBDegMinSec.Size = new System.Drawing.Size(14, 13);
            this.lBDegMinSec.TabIndex = 14;
            this.lBDegMinSec.Text = "B";
            // 
            // gbOwnRect42
            // 
            this.gbOwnRect42.Controls.Add(this.tbYRect42);
            this.gbOwnRect42.Controls.Add(this.lYRect42);
            this.gbOwnRect42.Controls.Add(this.tbXRect42);
            this.gbOwnRect42.Controls.Add(this.lXRect42);
            this.gbOwnRect42.Location = new System.Drawing.Point(10, 17);
            this.gbOwnRect42.Name = "gbOwnRect42";
            this.gbOwnRect42.Size = new System.Drawing.Size(158, 63);
            this.gbOwnRect42.TabIndex = 27;
            this.gbOwnRect42.TabStop = false;
            // 
            // tbYRect42
            // 
            this.tbYRect42.Location = new System.Drawing.Point(78, 36);
            this.tbYRect42.MaxLength = 7;
            this.tbYRect42.Name = "tbYRect42";
            this.tbYRect42.Size = new System.Drawing.Size(75, 20);
            this.tbYRect42.TabIndex = 5;
            // 
            // lYRect42
            // 
            this.lYRect42.AutoSize = true;
            this.lYRect42.Location = new System.Drawing.Point(4, 39);
            this.lYRect42.Name = "lYRect42";
            this.lYRect42.Size = new System.Drawing.Size(73, 13);
            this.lYRect42.TabIndex = 6;
            this.lYRect42.Text = "Y, м...............";
            // 
            // tbXRect42
            // 
            this.tbXRect42.Location = new System.Drawing.Point(78, 13);
            this.tbXRect42.MaxLength = 7;
            this.tbXRect42.Name = "tbXRect42";
            this.tbXRect42.Size = new System.Drawing.Size(75, 20);
            this.tbXRect42.TabIndex = 4;
            // 
            // lXRect42
            // 
            this.lXRect42.AutoSize = true;
            this.lXRect42.Location = new System.Drawing.Point(4, 20);
            this.lXRect42.Name = "lXRect42";
            this.lXRect42.Size = new System.Drawing.Size(73, 13);
            this.lXRect42.TabIndex = 3;
            this.lXRect42.Text = "X, м...............";
            // 
            // lOwnHeight
            // 
            this.lOwnHeight.AutoSize = true;
            this.lOwnHeight.Location = new System.Drawing.Point(13, 3);
            this.lOwnHeight.Name = "lOwnHeight";
            this.lOwnHeight.Size = new System.Drawing.Size(98, 13);
            this.lOwnHeight.TabIndex = 63;
            this.lOwnHeight.Text = "H, м.......................";
            // 
            // gbOwnRad
            // 
            this.gbOwnRad.Controls.Add(this.tbLRad);
            this.gbOwnRad.Controls.Add(this.lLRad);
            this.gbOwnRad.Controls.Add(this.tbBRad);
            this.gbOwnRad.Controls.Add(this.lBRad);
            this.gbOwnRad.Location = new System.Drawing.Point(10, 17);
            this.gbOwnRad.Name = "gbOwnRad";
            this.gbOwnRad.Size = new System.Drawing.Size(158, 63);
            this.gbOwnRad.TabIndex = 28;
            this.gbOwnRad.TabStop = false;
            this.gbOwnRad.Visible = false;
            // 
            // tbLRad
            // 
            this.tbLRad.Location = new System.Drawing.Point(78, 36);
            this.tbLRad.MaxLength = 10;
            this.tbLRad.Name = "tbLRad";
            this.tbLRad.Size = new System.Drawing.Size(75, 20);
            this.tbLRad.TabIndex = 5;
            // 
            // lLRad
            // 
            this.lLRad.AutoSize = true;
            this.lLRad.Location = new System.Drawing.Point(4, 41);
            this.lLRad.Name = "lLRad";
            this.lLRad.Size = new System.Drawing.Size(73, 13);
            this.lLRad.TabIndex = 6;
            this.lLRad.Text = "L, рад............";
            // 
            // tbBRad
            // 
            this.tbBRad.Location = new System.Drawing.Point(78, 13);
            this.tbBRad.MaxLength = 10;
            this.tbBRad.Name = "tbBRad";
            this.tbBRad.Size = new System.Drawing.Size(75, 20);
            this.tbBRad.TabIndex = 4;
            // 
            // lBRad
            // 
            this.lBRad.AutoSize = true;
            this.lBRad.Location = new System.Drawing.Point(4, 18);
            this.lBRad.Name = "lBRad";
            this.lBRad.Size = new System.Drawing.Size(74, 13);
            this.lBRad.TabIndex = 3;
            this.lBRad.Text = "B, рад............";
            // 
            // lChooseSC
            // 
            this.lChooseSC.AutoSize = true;
            this.lChooseSC.Location = new System.Drawing.Point(16, 498);
            this.lChooseSC.Name = "lChooseSC";
            this.lChooseSC.Size = new System.Drawing.Size(73, 13);
            this.lChooseSC.TabIndex = 82;
            this.lChooseSC.Text = "Пеленгатор1";
            this.lChooseSC.Visible = false;
            this.lChooseSC.Click += new System.EventHandler(this.lChooseSC_Click);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(12, 526);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(73, 13);
            this.label18.TabIndex = 83;
            this.label18.Text = "Пеленгатор2";
            this.label18.Visible = false;
            // 
            // tbPt1Height
            // 
            this.tbPt1Height.BackColor = System.Drawing.SystemColors.Menu;
            this.tbPt1Height.Location = new System.Drawing.Point(291, 0);
            this.tbPt1Height.Name = "tbPt1Height";
            this.tbPt1Height.Size = new System.Drawing.Size(48, 20);
            this.tbPt1Height.TabIndex = 37;
            // 
            // lPt1Height
            // 
            this.lPt1Height.AutoSize = true;
            this.lPt1Height.Location = new System.Drawing.Point(183, 3);
            this.lPt1Height.Name = "lPt1Height";
            this.lPt1Height.Size = new System.Drawing.Size(95, 13);
            this.lPt1Height.TabIndex = 36;
            this.lPt1Height.Text = "H, м......................";
            this.lPt1Height.Click += new System.EventHandler(this.lPt1Height_Click);
            // 
            // gbPt1DegMinSec
            // 
            this.gbPt1DegMinSec.Controls.Add(this.tbPt1LSec);
            this.gbPt1DegMinSec.Controls.Add(this.tbPt1BSec);
            this.gbPt1DegMinSec.Controls.Add(this.lPt1Min4);
            this.gbPt1DegMinSec.Controls.Add(this.lPt1Min3);
            this.gbPt1DegMinSec.Controls.Add(this.tbPt1LMin2);
            this.gbPt1DegMinSec.Controls.Add(this.tbPt1BMin2);
            this.gbPt1DegMinSec.Controls.Add(this.lPt1Sec2);
            this.gbPt1DegMinSec.Controls.Add(this.lPt1Sec1);
            this.gbPt1DegMinSec.Controls.Add(this.lPt1Deg4);
            this.gbPt1DegMinSec.Controls.Add(this.lPt1Deg3);
            this.gbPt1DegMinSec.Controls.Add(this.tbPt1LDeg2);
            this.gbPt1DegMinSec.Controls.Add(this.lPt1LDegMinSec);
            this.gbPt1DegMinSec.Controls.Add(this.tbPt1BDeg2);
            this.gbPt1DegMinSec.Controls.Add(this.lPt1BDegMinSec);
            this.gbPt1DegMinSec.Location = new System.Drawing.Point(181, 17);
            this.gbPt1DegMinSec.Name = "gbPt1DegMinSec";
            this.gbPt1DegMinSec.Size = new System.Drawing.Size(158, 63);
            this.gbPt1DegMinSec.TabIndex = 35;
            this.gbPt1DegMinSec.TabStop = false;
            this.gbPt1DegMinSec.Visible = false;
            // 
            // tbPt1LSec
            // 
            this.tbPt1LSec.Location = new System.Drawing.Point(93, 38);
            this.tbPt1LSec.MaxLength = 3;
            this.tbPt1LSec.Name = "tbPt1LSec";
            this.tbPt1LSec.Size = new System.Drawing.Size(25, 20);
            this.tbPt1LSec.TabIndex = 23;
            // 
            // tbPt1BSec
            // 
            this.tbPt1BSec.Location = new System.Drawing.Point(93, 12);
            this.tbPt1BSec.MaxLength = 3;
            this.tbPt1BSec.Name = "tbPt1BSec";
            this.tbPt1BSec.Size = new System.Drawing.Size(25, 20);
            this.tbPt1BSec.TabIndex = 18;
            // 
            // lPt1Min4
            // 
            this.lPt1Min4.AutoSize = true;
            this.lPt1Min4.Location = new System.Drawing.Point(84, 41);
            this.lPt1Min4.Name = "lPt1Min4";
            this.lPt1Min4.Size = new System.Drawing.Size(9, 13);
            this.lPt1Min4.TabIndex = 27;
            this.lPt1Min4.Text = "\'";
            // 
            // lPt1Min3
            // 
            this.lPt1Min3.AutoSize = true;
            this.lPt1Min3.Location = new System.Drawing.Point(84, 15);
            this.lPt1Min3.Name = "lPt1Min3";
            this.lPt1Min3.Size = new System.Drawing.Size(9, 13);
            this.lPt1Min3.TabIndex = 26;
            this.lPt1Min3.Text = "\'";
            // 
            // tbPt1LMin2
            // 
            this.tbPt1LMin2.Location = new System.Drawing.Point(59, 38);
            this.tbPt1LMin2.MaxLength = 2;
            this.tbPt1LMin2.Name = "tbPt1LMin2";
            this.tbPt1LMin2.Size = new System.Drawing.Size(25, 20);
            this.tbPt1LMin2.TabIndex = 22;
            // 
            // tbPt1BMin2
            // 
            this.tbPt1BMin2.Location = new System.Drawing.Point(59, 14);
            this.tbPt1BMin2.MaxLength = 2;
            this.tbPt1BMin2.Name = "tbPt1BMin2";
            this.tbPt1BMin2.Size = new System.Drawing.Size(25, 20);
            this.tbPt1BMin2.TabIndex = 16;
            // 
            // lPt1Sec2
            // 
            this.lPt1Sec2.AutoSize = true;
            this.lPt1Sec2.Location = new System.Drawing.Point(120, 37);
            this.lPt1Sec2.Name = "lPt1Sec2";
            this.lPt1Sec2.Size = new System.Drawing.Size(9, 13);
            this.lPt1Sec2.TabIndex = 25;
            this.lPt1Sec2.Text = "\'";
            // 
            // lPt1Sec1
            // 
            this.lPt1Sec1.AutoSize = true;
            this.lPt1Sec1.Location = new System.Drawing.Point(117, 13);
            this.lPt1Sec1.Name = "lPt1Sec1";
            this.lPt1Sec1.Size = new System.Drawing.Size(9, 13);
            this.lPt1Sec1.TabIndex = 24;
            this.lPt1Sec1.Text = "\'";
            // 
            // lPt1Deg4
            // 
            this.lPt1Deg4.AutoSize = true;
            this.lPt1Deg4.Location = new System.Drawing.Point(46, 36);
            this.lPt1Deg4.Name = "lPt1Deg4";
            this.lPt1Deg4.Size = new System.Drawing.Size(13, 13);
            this.lPt1Deg4.TabIndex = 21;
            this.lPt1Deg4.Text = "^";
            // 
            // lPt1Deg3
            // 
            this.lPt1Deg3.AutoSize = true;
            this.lPt1Deg3.Location = new System.Drawing.Point(46, 13);
            this.lPt1Deg3.Name = "lPt1Deg3";
            this.lPt1Deg3.Size = new System.Drawing.Size(13, 13);
            this.lPt1Deg3.TabIndex = 20;
            this.lPt1Deg3.Text = "^";
            // 
            // tbPt1LDeg2
            // 
            this.tbPt1LDeg2.Location = new System.Drawing.Point(19, 37);
            this.tbPt1LDeg2.MaxLength = 2;
            this.tbPt1LDeg2.Name = "tbPt1LDeg2";
            this.tbPt1LDeg2.Size = new System.Drawing.Size(25, 20);
            this.tbPt1LDeg2.TabIndex = 19;
            // 
            // lPt1LDegMinSec
            // 
            this.lPt1LDegMinSec.AutoSize = true;
            this.lPt1LDegMinSec.Location = new System.Drawing.Point(4, 41);
            this.lPt1LDegMinSec.Name = "lPt1LDegMinSec";
            this.lPt1LDegMinSec.Size = new System.Drawing.Size(13, 13);
            this.lPt1LDegMinSec.TabIndex = 17;
            this.lPt1LDegMinSec.Text = "L";
            // 
            // tbPt1BDeg2
            // 
            this.tbPt1BDeg2.Location = new System.Drawing.Point(19, 13);
            this.tbPt1BDeg2.MaxLength = 2;
            this.tbPt1BDeg2.Name = "tbPt1BDeg2";
            this.tbPt1BDeg2.Size = new System.Drawing.Size(25, 20);
            this.tbPt1BDeg2.TabIndex = 15;
            // 
            // lPt1BDegMinSec
            // 
            this.lPt1BDegMinSec.AutoSize = true;
            this.lPt1BDegMinSec.Location = new System.Drawing.Point(4, 18);
            this.lPt1BDegMinSec.Name = "lPt1BDegMinSec";
            this.lPt1BDegMinSec.Size = new System.Drawing.Size(14, 13);
            this.lPt1BDegMinSec.TabIndex = 14;
            this.lPt1BDegMinSec.Text = "B";
            // 
            // gbPt1Rad
            // 
            this.gbPt1Rad.Controls.Add(this.tbPt1LRad);
            this.gbPt1Rad.Controls.Add(this.lPt1LRad);
            this.gbPt1Rad.Controls.Add(this.tbPt1BRad);
            this.gbPt1Rad.Controls.Add(this.lPt1BRad);
            this.gbPt1Rad.Location = new System.Drawing.Point(181, 17);
            this.gbPt1Rad.Name = "gbPt1Rad";
            this.gbPt1Rad.Size = new System.Drawing.Size(158, 63);
            this.gbPt1Rad.TabIndex = 31;
            this.gbPt1Rad.TabStop = false;
            this.gbPt1Rad.Visible = false;
            // 
            // tbPt1LRad
            // 
            this.tbPt1LRad.Location = new System.Drawing.Point(78, 36);
            this.tbPt1LRad.MaxLength = 10;
            this.tbPt1LRad.Name = "tbPt1LRad";
            this.tbPt1LRad.Size = new System.Drawing.Size(75, 20);
            this.tbPt1LRad.TabIndex = 5;
            // 
            // lPt1LRad
            // 
            this.lPt1LRad.AutoSize = true;
            this.lPt1LRad.Location = new System.Drawing.Point(4, 40);
            this.lPt1LRad.Name = "lPt1LRad";
            this.lPt1LRad.Size = new System.Drawing.Size(67, 13);
            this.lPt1LRad.TabIndex = 6;
            this.lPt1LRad.Text = "L, рад..........";
            // 
            // tbPt1BRad
            // 
            this.tbPt1BRad.Location = new System.Drawing.Point(78, 13);
            this.tbPt1BRad.MaxLength = 10;
            this.tbPt1BRad.Name = "tbPt1BRad";
            this.tbPt1BRad.Size = new System.Drawing.Size(75, 20);
            this.tbPt1BRad.TabIndex = 4;
            // 
            // lPt1BRad
            // 
            this.lPt1BRad.AutoSize = true;
            this.lPt1BRad.Location = new System.Drawing.Point(4, 17);
            this.lPt1BRad.Name = "lPt1BRad";
            this.lPt1BRad.Size = new System.Drawing.Size(68, 13);
            this.lPt1BRad.TabIndex = 3;
            this.lPt1BRad.Text = "B, рад..........";
            // 
            // gbPt1Rect
            // 
            this.gbPt1Rect.Controls.Add(this.tbPt1YRect);
            this.gbPt1Rect.Controls.Add(this.lPt1YRect);
            this.gbPt1Rect.Controls.Add(this.tbPt1XRect);
            this.gbPt1Rect.Controls.Add(this.lPt1XRect);
            this.gbPt1Rect.Location = new System.Drawing.Point(181, 17);
            this.gbPt1Rect.Name = "gbPt1Rect";
            this.gbPt1Rect.Size = new System.Drawing.Size(158, 63);
            this.gbPt1Rect.TabIndex = 29;
            this.gbPt1Rect.TabStop = false;
            this.gbPt1Rect.Visible = false;
            // 
            // tbPt1YRect
            // 
            this.tbPt1YRect.Location = new System.Drawing.Point(78, 36);
            this.tbPt1YRect.MaxLength = 7;
            this.tbPt1YRect.Name = "tbPt1YRect";
            this.tbPt1YRect.Size = new System.Drawing.Size(75, 20);
            this.tbPt1YRect.TabIndex = 5;
            // 
            // lPt1YRect
            // 
            this.lPt1YRect.AutoSize = true;
            this.lPt1YRect.Location = new System.Drawing.Point(4, 41);
            this.lPt1YRect.Name = "lPt1YRect";
            this.lPt1YRect.Size = new System.Drawing.Size(52, 13);
            this.lPt1YRect.TabIndex = 6;
            this.lPt1YRect.Text = "L.............";
            // 
            // tbPt1XRect
            // 
            this.tbPt1XRect.Location = new System.Drawing.Point(78, 13);
            this.tbPt1XRect.MaxLength = 7;
            this.tbPt1XRect.Name = "tbPt1XRect";
            this.tbPt1XRect.Size = new System.Drawing.Size(75, 20);
            this.tbPt1XRect.TabIndex = 4;
            // 
            // lPt1XRect
            // 
            this.lPt1XRect.AutoSize = true;
            this.lPt1XRect.Location = new System.Drawing.Point(4, 19);
            this.lPt1XRect.Name = "lPt1XRect";
            this.lPt1XRect.Size = new System.Drawing.Size(53, 13);
            this.lPt1XRect.TabIndex = 3;
            this.lPt1XRect.Text = "B.............";
            // 
            // gbPt1DegMin
            // 
            this.gbPt1DegMin.Controls.Add(this.tbPt1LMin1);
            this.gbPt1DegMin.Controls.Add(this.tbPt1BMin1);
            this.gbPt1DegMin.Controls.Add(this.lPt1LDegMin);
            this.gbPt1DegMin.Controls.Add(this.lPt1BDegMin);
            this.gbPt1DegMin.Location = new System.Drawing.Point(181, 17);
            this.gbPt1DegMin.Name = "gbPt1DegMin";
            this.gbPt1DegMin.Size = new System.Drawing.Size(158, 63);
            this.gbPt1DegMin.TabIndex = 34;
            this.gbPt1DegMin.TabStop = false;
            this.gbPt1DegMin.Visible = false;
            // 
            // tbPt1LMin1
            // 
            this.tbPt1LMin1.Location = new System.Drawing.Point(75, 38);
            this.tbPt1LMin1.MaxLength = 8;
            this.tbPt1LMin1.Name = "tbPt1LMin1";
            this.tbPt1LMin1.Size = new System.Drawing.Size(75, 20);
            this.tbPt1LMin1.TabIndex = 15;
            // 
            // tbPt1BMin1
            // 
            this.tbPt1BMin1.Location = new System.Drawing.Point(75, 14);
            this.tbPt1BMin1.MaxLength = 8;
            this.tbPt1BMin1.Name = "tbPt1BMin1";
            this.tbPt1BMin1.Size = new System.Drawing.Size(75, 20);
            this.tbPt1BMin1.TabIndex = 13;
            // 
            // lPt1LDegMin
            // 
            this.lPt1LDegMin.AutoSize = true;
            this.lPt1LDegMin.Location = new System.Drawing.Point(4, 41);
            this.lPt1LDegMin.Name = "lPt1LDegMin";
            this.lPt1LDegMin.Size = new System.Drawing.Size(61, 13);
            this.lPt1LDegMin.TabIndex = 12;
            this.lPt1LDegMin.Text = "L................";
            // 
            // lPt1BDegMin
            // 
            this.lPt1BDegMin.AutoSize = true;
            this.lPt1BDegMin.Location = new System.Drawing.Point(4, 18);
            this.lPt1BDegMin.Name = "lPt1BDegMin";
            this.lPt1BDegMin.Size = new System.Drawing.Size(59, 13);
            this.lPt1BDegMin.TabIndex = 10;
            this.lPt1BDegMin.Text = "B...............";
            // 
            // gbPt1Rect42
            // 
            this.gbPt1Rect42.Controls.Add(this.tbPt1YRect42);
            this.gbPt1Rect42.Controls.Add(this.lPt1YRect42);
            this.gbPt1Rect42.Controls.Add(this.tbPt1XRect42);
            this.gbPt1Rect42.Controls.Add(this.lPt1XRect42);
            this.gbPt1Rect42.Location = new System.Drawing.Point(181, 17);
            this.gbPt1Rect42.Name = "gbPt1Rect42";
            this.gbPt1Rect42.Size = new System.Drawing.Size(158, 63);
            this.gbPt1Rect42.TabIndex = 30;
            this.gbPt1Rect42.TabStop = false;
            // 
            // tbPt1YRect42
            // 
            this.tbPt1YRect42.Location = new System.Drawing.Point(78, 36);
            this.tbPt1YRect42.MaxLength = 7;
            this.tbPt1YRect42.Name = "tbPt1YRect42";
            this.tbPt1YRect42.Size = new System.Drawing.Size(75, 20);
            this.tbPt1YRect42.TabIndex = 5;
            // 
            // lPt1YRect42
            // 
            this.lPt1YRect42.AutoSize = true;
            this.lPt1YRect42.Location = new System.Drawing.Point(10, 39);
            this.lPt1YRect42.Name = "lPt1YRect42";
            this.lPt1YRect42.Size = new System.Drawing.Size(67, 13);
            this.lPt1YRect42.TabIndex = 6;
            this.lPt1YRect42.Text = "Y, м.............";
            // 
            // tbPt1XRect42
            // 
            this.tbPt1XRect42.Location = new System.Drawing.Point(78, 13);
            this.tbPt1XRect42.MaxLength = 7;
            this.tbPt1XRect42.Name = "tbPt1XRect42";
            this.tbPt1XRect42.Size = new System.Drawing.Size(75, 20);
            this.tbPt1XRect42.TabIndex = 4;
            // 
            // lPt1XRect42
            // 
            this.lPt1XRect42.AutoSize = true;
            this.lPt1XRect42.Location = new System.Drawing.Point(10, 16);
            this.lPt1XRect42.Name = "lPt1XRect42";
            this.lPt1XRect42.Size = new System.Drawing.Size(67, 13);
            this.lPt1XRect42.TabIndex = 3;
            this.lPt1XRect42.Text = "X, м.............";
            // 
            // cbChooseSC
            // 
            this.cbChooseSC.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbChooseSC.DropDownWidth = 150;
            this.cbChooseSC.FormattingEnabled = true;
            this.cbChooseSC.Items.AddRange(new object[] {
            "Градусы (WGS84)",
            "Метры (Гаусс-Крюгер)",
            "Радианы (СК42)",
            "Градусы (СК42)",
            "Град,мин,сек(СК42)"});
            this.cbChooseSC.Location = new System.Drawing.Point(22, 90);
            this.cbChooseSC.Name = "cbChooseSC";
            this.cbChooseSC.Size = new System.Drawing.Size(141, 21);
            this.cbChooseSC.TabIndex = 88;
            this.cbChooseSC.SelectedIndexChanged += new System.EventHandler(this.cbChooseSC_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(-2, 94);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(21, 13);
            this.label1.TabIndex = 89;
            this.label1.Text = "СК";
            // 
            // chbXY1
            // 
            this.chbXY1.AutoSize = true;
            this.chbXY1.Location = new System.Drawing.Point(274, 522);
            this.chbXY1.Name = "chbXY1";
            this.chbXY1.Size = new System.Drawing.Size(15, 14);
            this.chbXY1.TabIndex = 108;
            this.chbXY1.UseVisualStyleBackColor = true;
            this.chbXY1.Visible = false;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(107, 498);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(133, 13);
            this.label16.TabIndex = 109;
            this.label16.Text = "Ручной выбор координат";
            this.label16.Visible = false;
            // 
            // bAccept
            // 
            this.bAccept.Location = new System.Drawing.Point(4, 162);
            this.bAccept.Name = "bAccept";
            this.bAccept.Size = new System.Drawing.Size(81, 23);
            this.bAccept.TabIndex = 110;
            this.bAccept.Text = "Принять";
            this.bAccept.UseVisualStyleBackColor = true;
            this.bAccept.Click += new System.EventHandler(this.bAccept_Click);
            // 
            // bClear
            // 
            this.bClear.Location = new System.Drawing.Point(89, 162);
            this.bClear.Name = "bClear";
            this.bClear.Size = new System.Drawing.Size(81, 23);
            this.bClear.TabIndex = 111;
            this.bClear.Text = "Очистить";
            this.bClear.UseVisualStyleBackColor = true;
            this.bClear.Click += new System.EventHandler(this.bClear_Click);
            // 
            // tb1
            // 
            this.tb1.Location = new System.Drawing.Point(277, 85);
            this.tb1.MaxLength = 7;
            this.tb1.Name = "tb1";
            this.tb1.Size = new System.Drawing.Size(55, 20);
            this.tb1.TabIndex = 112;
            this.tb1.Text = "30000";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(171, 89);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(104, 13);
            this.label2.TabIndex = 113;
            this.label2.Text = "Макс. дальность,м";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(171, 113);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 13);
            this.label3.TabIndex = 115;
            this.label3.Text = "Шаг, м";
            // 
            // tb2
            // 
            this.tb2.Location = new System.Drawing.Point(34, 308);
            this.tb2.MaxLength = 5;
            this.tb2.Name = "tb2";
            this.tb2.Size = new System.Drawing.Size(55, 20);
            this.tb2.TabIndex = 114;
            this.tb2.Visible = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(172, 134);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(85, 13);
            this.label4.TabIndex = 118;
            this.label4.Text = "СКО, град (1-5) ";
            // 
            // tb3
            // 
            this.tb3.Location = new System.Drawing.Point(277, 131);
            this.tb3.MaxLength = 3;
            this.tb3.Name = "tb3";
            this.tb3.Size = new System.Drawing.Size(55, 20);
            this.tb3.TabIndex = 117;
            this.tb3.Text = "2";
            // 
            // button5
            // 
            this.button5.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.button5.Location = new System.Drawing.Point(4, 136);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(82, 23);
            this.button5.TabIndex = 217;
            this.button5.Text = "Пеленгатор1";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button1
            // 
            this.button1.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.button1.Location = new System.Drawing.Point(89, 136);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(82, 23);
            this.button1.TabIndex = 218;
            this.button1.Text = "Пеленгатор2";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.Red;
            this.button2.Location = new System.Drawing.Point(343, 5);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(19, 19);
            this.button2.TabIndex = 221;
            this.button2.UseVisualStyleBackColor = false;
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.Red;
            this.button3.Location = new System.Drawing.Point(343, 26);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(19, 19);
            this.button3.TabIndex = 222;
            this.button3.UseVisualStyleBackColor = false;
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.Red;
            this.button4.Location = new System.Drawing.Point(343, 68);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(19, 19);
            this.button4.TabIndex = 224;
            this.button4.UseVisualStyleBackColor = false;
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.Color.Red;
            this.button6.Location = new System.Drawing.Point(343, 47);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(19, 19);
            this.button6.TabIndex = 223;
            this.button6.UseVisualStyleBackColor = false;
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.Color.Red;
            this.button7.Location = new System.Drawing.Point(343, 89);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(19, 19);
            this.button7.TabIndex = 225;
            this.button7.UseVisualStyleBackColor = false;
            // 
            // button8
            // 
            this.button8.BackColor = System.Drawing.Color.Red;
            this.button8.Location = new System.Drawing.Point(343, 194);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(19, 19);
            this.button8.TabIndex = 230;
            this.button8.UseVisualStyleBackColor = false;
            // 
            // button9
            // 
            this.button9.BackColor = System.Drawing.Color.Red;
            this.button9.Location = new System.Drawing.Point(343, 173);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(19, 19);
            this.button9.TabIndex = 229;
            this.button9.UseVisualStyleBackColor = false;
            // 
            // button10
            // 
            this.button10.BackColor = System.Drawing.Color.Red;
            this.button10.Location = new System.Drawing.Point(343, 152);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(19, 19);
            this.button10.TabIndex = 228;
            this.button10.UseVisualStyleBackColor = false;
            // 
            // button11
            // 
            this.button11.BackColor = System.Drawing.Color.Red;
            this.button11.Location = new System.Drawing.Point(343, 131);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(19, 19);
            this.button11.TabIndex = 227;
            this.button11.UseVisualStyleBackColor = false;
            // 
            // button12
            // 
            this.button12.BackColor = System.Drawing.Color.Red;
            this.button12.Location = new System.Drawing.Point(343, 110);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(19, 19);
            this.button12.TabIndex = 226;
            this.button12.UseVisualStyleBackColor = false;
            // 
            // tbProzr
            // 
            this.tbProzr.Location = new System.Drawing.Point(276, 180);
            this.tbProzr.MaxLength = 7;
            this.tbProzr.Name = "tbProzr";
            this.tbProzr.Size = new System.Drawing.Size(54, 20);
            this.tbProzr.TabIndex = 241;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(173, 183);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(49, 13);
            this.label17.TabIndex = 242;
            this.label17.Text = "База, м ";
            // 
            // tbR1
            // 
            this.tbR1.BackColor = System.Drawing.SystemColors.Menu;
            this.tbR1.Location = new System.Drawing.Point(367, 3);
            this.tbR1.MaxLength = 9;
            this.tbR1.Name = "tbR1";
            this.tbR1.Size = new System.Drawing.Size(45, 20);
            this.tbR1.TabIndex = 243;
            this.tbR1.Text = "Rмин";
            // 
            // tbR2
            // 
            this.tbR2.BackColor = System.Drawing.SystemColors.Menu;
            this.tbR2.Location = new System.Drawing.Point(426, 3);
            this.tbR2.MaxLength = 9;
            this.tbR2.Name = "tbR2";
            this.tbR2.Size = new System.Drawing.Size(45, 20);
            this.tbR2.TabIndex = 244;
            this.tbR2.Text = "2Rмин";
            this.tbR2.TextChanged += new System.EventHandler(this.tbR2_TextChanged);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(411, 6);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(16, 13);
            this.label19.TabIndex = 245;
            this.label19.Text = "...";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(411, 27);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(16, 13);
            this.label20.TabIndex = 248;
            this.label20.Text = "...";
            // 
            // tbR4
            // 
            this.tbR4.BackColor = System.Drawing.SystemColors.Menu;
            this.tbR4.Location = new System.Drawing.Point(426, 23);
            this.tbR4.MaxLength = 9;
            this.tbR4.Name = "tbR4";
            this.tbR4.Size = new System.Drawing.Size(45, 20);
            this.tbR4.TabIndex = 247;
            this.tbR4.Text = "3Rмин";
            // 
            // tbR3
            // 
            this.tbR3.BackColor = System.Drawing.SystemColors.Menu;
            this.tbR3.Location = new System.Drawing.Point(367, 23);
            this.tbR3.MaxLength = 9;
            this.tbR3.Name = "tbR3";
            this.tbR3.Size = new System.Drawing.Size(45, 20);
            this.tbR3.TabIndex = 246;
            this.tbR3.Text = "2Rмин";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(411, 68);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(16, 13);
            this.label21.TabIndex = 254;
            this.label21.Text = "...";
            // 
            // tbR8
            // 
            this.tbR8.BackColor = System.Drawing.SystemColors.Menu;
            this.tbR8.Location = new System.Drawing.Point(426, 67);
            this.tbR8.MaxLength = 9;
            this.tbR8.Name = "tbR8";
            this.tbR8.Size = new System.Drawing.Size(45, 20);
            this.tbR8.TabIndex = 253;
            this.tbR8.Text = "5Rмин";
            // 
            // tbR7
            // 
            this.tbR7.BackColor = System.Drawing.SystemColors.Menu;
            this.tbR7.Location = new System.Drawing.Point(367, 67);
            this.tbR7.MaxLength = 9;
            this.tbR7.Name = "tbR7";
            this.tbR7.Size = new System.Drawing.Size(45, 20);
            this.tbR7.TabIndex = 252;
            this.tbR7.Text = "4Rмин";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(411, 48);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(16, 13);
            this.label22.TabIndex = 251;
            this.label22.Text = "...";
            // 
            // tbR6
            // 
            this.tbR6.BackColor = System.Drawing.SystemColors.Menu;
            this.tbR6.Location = new System.Drawing.Point(426, 45);
            this.tbR6.MaxLength = 9;
            this.tbR6.Name = "tbR6";
            this.tbR6.Size = new System.Drawing.Size(45, 20);
            this.tbR6.TabIndex = 250;
            this.tbR6.Text = "4Rмин";
            // 
            // tbR5
            // 
            this.tbR5.BackColor = System.Drawing.SystemColors.Menu;
            this.tbR5.Location = new System.Drawing.Point(367, 45);
            this.tbR5.MaxLength = 9;
            this.tbR5.Name = "tbR5";
            this.tbR5.Size = new System.Drawing.Size(45, 20);
            this.tbR5.TabIndex = 249;
            this.tbR5.Text = "3Rмин";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(411, 153);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(16, 13);
            this.label23.TabIndex = 266;
            this.label23.Text = "...";
            // 
            // tbR16
            // 
            this.tbR16.BackColor = System.Drawing.SystemColors.Menu;
            this.tbR16.Location = new System.Drawing.Point(426, 151);
            this.tbR16.MaxLength = 9;
            this.tbR16.Name = "tbR16";
            this.tbR16.Size = new System.Drawing.Size(45, 20);
            this.tbR16.TabIndex = 265;
            this.tbR16.Text = "9Rмин";
            // 
            // tbR15
            // 
            this.tbR15.BackColor = System.Drawing.SystemColors.Menu;
            this.tbR15.Location = new System.Drawing.Point(367, 151);
            this.tbR15.MaxLength = 9;
            this.tbR15.Name = "tbR15";
            this.tbR15.Size = new System.Drawing.Size(45, 20);
            this.tbR15.TabIndex = 264;
            this.tbR15.Text = "8Rмин";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(411, 133);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(16, 13);
            this.label24.TabIndex = 263;
            this.label24.Text = "...";
            // 
            // tbR14
            // 
            this.tbR14.BackColor = System.Drawing.SystemColors.Menu;
            this.tbR14.Location = new System.Drawing.Point(426, 131);
            this.tbR14.MaxLength = 9;
            this.tbR14.Name = "tbR14";
            this.tbR14.Size = new System.Drawing.Size(45, 20);
            this.tbR14.TabIndex = 262;
            this.tbR14.Text = "8Rмин";
            // 
            // tbR13
            // 
            this.tbR13.BackColor = System.Drawing.SystemColors.Menu;
            this.tbR13.Location = new System.Drawing.Point(367, 131);
            this.tbR13.MaxLength = 9;
            this.tbR13.Name = "tbR13";
            this.tbR13.Size = new System.Drawing.Size(45, 20);
            this.tbR13.TabIndex = 261;
            this.tbR13.Text = "7Rмин";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(411, 112);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(16, 13);
            this.label25.TabIndex = 260;
            this.label25.Text = "...";
            // 
            // tbR12
            // 
            this.tbR12.BackColor = System.Drawing.SystemColors.Menu;
            this.tbR12.Location = new System.Drawing.Point(426, 109);
            this.tbR12.MaxLength = 9;
            this.tbR12.Name = "tbR12";
            this.tbR12.Size = new System.Drawing.Size(45, 20);
            this.tbR12.TabIndex = 259;
            this.tbR12.Text = "7Rмин";
            // 
            // tbR11
            // 
            this.tbR11.BackColor = System.Drawing.SystemColors.Menu;
            this.tbR11.Location = new System.Drawing.Point(367, 109);
            this.tbR11.MaxLength = 9;
            this.tbR11.Name = "tbR11";
            this.tbR11.Size = new System.Drawing.Size(45, 20);
            this.tbR11.TabIndex = 258;
            this.tbR11.Text = "6Rмин";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(411, 177);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(16, 13);
            this.label26.TabIndex = 257;
            this.label26.Text = "...";
            // 
            // tbR10
            // 
            this.tbR10.BackColor = System.Drawing.SystemColors.Menu;
            this.tbR10.Location = new System.Drawing.Point(426, 88);
            this.tbR10.MaxLength = 9;
            this.tbR10.Name = "tbR10";
            this.tbR10.Size = new System.Drawing.Size(45, 20);
            this.tbR10.TabIndex = 256;
            this.tbR10.Text = "6Rмин";
            // 
            // tbR9
            // 
            this.tbR9.BackColor = System.Drawing.SystemColors.Menu;
            this.tbR9.Location = new System.Drawing.Point(367, 88);
            this.tbR9.MaxLength = 9;
            this.tbR9.Name = "tbR9";
            this.tbR9.Size = new System.Drawing.Size(45, 20);
            this.tbR9.TabIndex = 255;
            this.tbR9.Text = "5Rмин";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(411, 195);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(16, 13);
            this.label27.TabIndex = 272;
            this.label27.Text = "...";
            // 
            // tbR20
            // 
            this.tbR20.BackColor = System.Drawing.SystemColors.Menu;
            this.tbR20.Location = new System.Drawing.Point(426, 194);
            this.tbR20.MaxLength = 9;
            this.tbR20.Name = "tbR20";
            this.tbR20.Size = new System.Drawing.Size(45, 20);
            this.tbR20.TabIndex = 271;
            this.tbR20.Text = "11Rмин";
            // 
            // tbR19
            // 
            this.tbR19.BackColor = System.Drawing.SystemColors.Menu;
            this.tbR19.Location = new System.Drawing.Point(367, 194);
            this.tbR19.MaxLength = 9;
            this.tbR19.Name = "tbR19";
            this.tbR19.Size = new System.Drawing.Size(45, 20);
            this.tbR19.TabIndex = 270;
            this.tbR19.Text = "10Rмин";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(411, 89);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(16, 13);
            this.label28.TabIndex = 269;
            this.label28.Text = "...";
            // 
            // tbR18
            // 
            this.tbR18.BackColor = System.Drawing.SystemColors.Menu;
            this.tbR18.Location = new System.Drawing.Point(426, 173);
            this.tbR18.MaxLength = 9;
            this.tbR18.Name = "tbR18";
            this.tbR18.Size = new System.Drawing.Size(45, 20);
            this.tbR18.TabIndex = 268;
            this.tbR18.Text = "10Rмин";
            // 
            // tbR17
            // 
            this.tbR17.BackColor = System.Drawing.SystemColors.Menu;
            this.tbR17.Location = new System.Drawing.Point(367, 173);
            this.tbR17.MaxLength = 9;
            this.tbR17.Name = "tbR17";
            this.tbR17.Size = new System.Drawing.Size(45, 20);
            this.tbR17.TabIndex = 267;
            this.tbR17.Text = "9Rмин";
            // 
            // cbStep
            // 
            this.cbStep.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbStep.FormattingEnabled = true;
            this.cbStep.Items.AddRange(new object[] {
            "100",
            "200",
            "300",
            "400",
            "500"});
            this.cbStep.Location = new System.Drawing.Point(277, 108);
            this.cbStep.Name = "cbStep";
            this.cbStep.Size = new System.Drawing.Size(54, 21);
            this.cbStep.TabIndex = 273;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(276, 156);
            this.textBox2.MaxLength = 7;
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(54, 20);
            this.textBox2.TabIndex = 274;
            this.textBox2.Text = "70";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(172, 158);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(96, 13);
            this.label6.TabIndex = 275;
            this.label6.Text = "Прозрачность (%)";
            // 
            // tb200
            // 
            this.tb200.Location = new System.Drawing.Point(116, 308);
            this.tb200.MaxLength = 5;
            this.tb200.Name = "tb200";
            this.tb200.Size = new System.Drawing.Size(55, 20);
            this.tb200.TabIndex = 276;
            this.tb200.Visible = false;
            // 
            // FormBearing
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(475, 218);
            this.Controls.Add(this.tbPt1Height);
            this.Controls.Add(this.tbOwnHeight);
            this.Controls.Add(this.tb200);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.cbStep);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.tbR20);
            this.Controls.Add(this.tbR19);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.tbR18);
            this.Controls.Add(this.tbR17);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.tbR16);
            this.Controls.Add(this.tbR15);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.tbR14);
            this.Controls.Add(this.tbR13);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.tbR12);
            this.Controls.Add(this.tbR11);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.tbR10);
            this.Controls.Add(this.tbR9);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.tbR8);
            this.Controls.Add(this.tbR7);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.tbR6);
            this.Controls.Add(this.tbR5);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.tbR4);
            this.Controls.Add(this.tbR3);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.tbR2);
            this.Controls.Add(this.tbR1);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.tbProzr);
            this.Controls.Add(this.gbPt1DegMinSec);
            this.Controls.Add(this.gbPt1Rect42);
            this.Controls.Add(this.gbPt1Rad);
            this.Controls.Add(this.gbPt1DegMin);
            this.Controls.Add(this.gbPt1Rect);
            this.Controls.Add(this.gbOwnRect);
            this.Controls.Add(this.gbOwnRect42);
            this.Controls.Add(this.gbOwnRad);
            this.Controls.Add(this.gbOwnDegMinSec);
            this.Controls.Add(this.gbOwnDegMin);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.button10);
            this.Controls.Add(this.button11);
            this.Controls.Add(this.button12);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.tb3);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tb2);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tb1);
            this.Controls.Add(this.bClear);
            this.Controls.Add(this.bAccept);
            this.Controls.Add(this.chbXY1);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbChooseSC);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.lChooseSC);
            this.Controls.Add(this.lOwnHeight);
            this.Controls.Add(this.lPt1Height);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Location = new System.Drawing.Point(500, 100);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormBearing";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Зона ошибок местоопределения";
            this.TopMost = true;
            this.Activated += new System.EventHandler(this.FormBearing_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormBearing_FormClosing);
            this.Load += new System.EventHandler(this.FormBearing_Load);
            this.gbOwnRect.ResumeLayout(false);
            this.gbOwnRect.PerformLayout();
            this.gbOwnDegMin.ResumeLayout(false);
            this.gbOwnDegMin.PerformLayout();
            this.gbOwnDegMinSec.ResumeLayout(false);
            this.gbOwnDegMinSec.PerformLayout();
            this.gbOwnRect42.ResumeLayout(false);
            this.gbOwnRect42.PerformLayout();
            this.gbOwnRad.ResumeLayout(false);
            this.gbOwnRad.PerformLayout();
            this.gbPt1DegMinSec.ResumeLayout(false);
            this.gbPt1DegMinSec.PerformLayout();
            this.gbPt1Rad.ResumeLayout(false);
            this.gbPt1Rad.PerformLayout();
            this.gbPt1Rect.ResumeLayout(false);
            this.gbPt1Rect.PerformLayout();
            this.gbPt1DegMin.ResumeLayout(false);
            this.gbPt1DegMin.PerformLayout();
            this.gbPt1Rect42.ResumeLayout(false);
            this.gbPt1Rect42.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox gbOwnRect;
        public System.Windows.Forms.TextBox tbYRect;
        private System.Windows.Forms.Label lYRect;
        public System.Windows.Forms.TextBox tbXRect;
        private System.Windows.Forms.Label lXRect;
        private System.Windows.Forms.GroupBox gbOwnDegMin;
        public System.Windows.Forms.TextBox tbLMin1;
        public System.Windows.Forms.TextBox tbBMin1;
        private System.Windows.Forms.Label lLDegMin;
        private System.Windows.Forms.Label lBDegMin;
        private System.Windows.Forms.GroupBox gbOwnDegMinSec;
        public System.Windows.Forms.TextBox tbLSec;
        public System.Windows.Forms.TextBox tbBSec;
        private System.Windows.Forms.Label lMin4;
        private System.Windows.Forms.Label lMin3;
        public System.Windows.Forms.TextBox tbLMin2;
        public System.Windows.Forms.TextBox tbBMin2;
        private System.Windows.Forms.Label lSec2;
        private System.Windows.Forms.Label lSec1;
        private System.Windows.Forms.Label lDeg4;
        private System.Windows.Forms.Label lDeg3;
        public System.Windows.Forms.TextBox tbLDeg2;
        private System.Windows.Forms.Label lLDegMinSec;
        public System.Windows.Forms.TextBox tbBDeg2;
        private System.Windows.Forms.Label lBDegMinSec;
        private System.Windows.Forms.GroupBox gbOwnRect42;
        public System.Windows.Forms.TextBox tbYRect42;
        private System.Windows.Forms.Label lYRect42;
        public System.Windows.Forms.TextBox tbXRect42;
        private System.Windows.Forms.Label lXRect42;
        private System.Windows.Forms.Label lOwnHeight;
        private System.Windows.Forms.GroupBox gbOwnRad;
        public System.Windows.Forms.TextBox tbLRad;
        private System.Windows.Forms.Label lLRad;
        public System.Windows.Forms.TextBox tbBRad;
        private System.Windows.Forms.Label lBRad;
        private System.Windows.Forms.Label lChooseSC;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label lPt1Height;
        private System.Windows.Forms.GroupBox gbPt1DegMinSec;
        public System.Windows.Forms.TextBox tbPt1LSec;
        public System.Windows.Forms.TextBox tbPt1BSec;
        private System.Windows.Forms.Label lPt1Min4;
        private System.Windows.Forms.Label lPt1Min3;
        public System.Windows.Forms.TextBox tbPt1LMin2;
        public System.Windows.Forms.TextBox tbPt1BMin2;
        private System.Windows.Forms.Label lPt1Sec2;
        private System.Windows.Forms.Label lPt1Sec1;
        private System.Windows.Forms.Label lPt1Deg4;
        private System.Windows.Forms.Label lPt1Deg3;
        public System.Windows.Forms.TextBox tbPt1LDeg2;
        private System.Windows.Forms.Label lPt1LDegMinSec;
        public System.Windows.Forms.TextBox tbPt1BDeg2;
        private System.Windows.Forms.Label lPt1BDegMinSec;
        private System.Windows.Forms.GroupBox gbPt1Rad;
        public System.Windows.Forms.TextBox tbPt1LRad;
        private System.Windows.Forms.Label lPt1LRad;
        public System.Windows.Forms.TextBox tbPt1BRad;
        private System.Windows.Forms.Label lPt1BRad;
        private System.Windows.Forms.GroupBox gbPt1Rect;
        public System.Windows.Forms.TextBox tbPt1YRect;
        private System.Windows.Forms.Label lPt1YRect;
        public System.Windows.Forms.TextBox tbPt1XRect;
        private System.Windows.Forms.Label lPt1XRect;
        private System.Windows.Forms.GroupBox gbPt1DegMin;
        public System.Windows.Forms.TextBox tbPt1LMin1;
        public System.Windows.Forms.TextBox tbPt1BMin1;
        private System.Windows.Forms.Label lPt1LDegMin;
        private System.Windows.Forms.Label lPt1BDegMin;
        private System.Windows.Forms.GroupBox gbPt1Rect42;
        public System.Windows.Forms.TextBox tbPt1YRect42;
        private System.Windows.Forms.Label lPt1YRect42;
        public System.Windows.Forms.TextBox tbPt1XRect42;
        private System.Windows.Forms.Label lPt1XRect42;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox chbXY1;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Button bAccept;
        private System.Windows.Forms.Button bClear;
        private System.Windows.Forms.TextBox tb1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tb3;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button12;
        public System.Windows.Forms.TextBox tbOwnHeight;
        public System.Windows.Forms.TextBox tbPt1Height;
        public System.Windows.Forms.ComboBox cbChooseSC;
        private System.Windows.Forms.Label label17;
        public System.Windows.Forms.TextBox tbProzr;
        public System.Windows.Forms.TextBox tbR1;
        public System.Windows.Forms.TextBox tbR2;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        public System.Windows.Forms.TextBox tbR4;
        public System.Windows.Forms.TextBox tbR3;
        private System.Windows.Forms.Label label21;
        public System.Windows.Forms.TextBox tbR8;
        public System.Windows.Forms.TextBox tbR7;
        private System.Windows.Forms.Label label22;
        public System.Windows.Forms.TextBox tbR6;
        public System.Windows.Forms.TextBox tbR5;
        private System.Windows.Forms.Label label23;
        public System.Windows.Forms.TextBox tbR16;
        public System.Windows.Forms.TextBox tbR15;
        private System.Windows.Forms.Label label24;
        public System.Windows.Forms.TextBox tbR14;
        public System.Windows.Forms.TextBox tbR13;
        private System.Windows.Forms.Label label25;
        public System.Windows.Forms.TextBox tbR12;
        public System.Windows.Forms.TextBox tbR11;
        private System.Windows.Forms.Label label26;
        public System.Windows.Forms.TextBox tbR10;
        public System.Windows.Forms.TextBox tbR9;
        private System.Windows.Forms.Label label27;
        public System.Windows.Forms.TextBox tbR20;
        public System.Windows.Forms.TextBox tbR19;
        private System.Windows.Forms.Label label28;
        public System.Windows.Forms.TextBox tbR18;
        public System.Windows.Forms.TextBox tbR17;
        private System.Windows.Forms.ComboBox cbStep;
        public System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label6;
        public System.Windows.Forms.TextBox tb200;
        public System.Windows.Forms.TextBox tb2;

    }
}