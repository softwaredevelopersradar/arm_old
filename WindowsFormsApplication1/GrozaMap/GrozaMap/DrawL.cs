﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AxaxGisToolKit;
using axGisToolKit;
using System.Runtime.InteropServices;
using System.Drawing;
using System.Drawing.Drawing2D;

using System.Windows.Forms;
using System.ComponentModel;
using System.Data;


namespace GrozaMap
{
    class DrawL
    {

        [DllImport("gisacces.dll")]
        static extern int mapGeoToPlane(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        static extern int mapPlaneToPicture(int hmap, ref double Bx, ref double Ly);


        // VARS *******************************************************************************

        //private MapForm objMapForm;

        // Для отрисовки точки на карте
       // private double LatDX;
       // private double LongDY;
        private static double LatDX=0;
        private static double LongDY=0;

        public static Graphics graphl;

        // ******************************************************************************* VARS


        // Конструктор *************************************************************************

        //public DrawL(MapForm obj1MapForm)
        public DrawL()

        {

          //objMapForm = obj1MapForm;

        // Для отрисовки точки на карте
        //LatDX=0;
        //LongDY=0;

        } // Конструктор
        // ************************************************************************* Конструктор

        // *************************************************************************************
        // Нарисовать метку по координатам (красный прямоугольник)
        //
        // Входные параметры:
        // Lat - широта, град (GEO)
        // Long - долгота, град (GEO)
        // *************************************************************************************
        //public void f_Map_Rect_LatLong(
        public static void f_Map_Rect_LatLong(

                                         double Lat,
                                         double Long
                                        )
        {

            //MapForm objMapForm;
        // -------------------------------------------------------------------------------------
           // graphl = MapForm.axaxcMapScreen1.CreateGraphics();
            //graphl = axaxcMapScreen1.CreateGraphics();

            Pen penRed = new Pen(Color.Red, 2);
            Brush brushRed = new SolidBrush(Color.Red);

        // -------------------------------------------------------------------------------------

            // grad->rad
            LatDX = (Lat * Math.PI) / 180;
            LongDY = (Long * Math.PI) / 180;

            // Подаем градусы, получаем там же расстояние на карте в км
            mapGeoToPlane(GlobalVarLn.hmapl, ref LatDX, ref LongDY);

            // Расстояние в км на карте -> пикселы на изображении
            mapPlaneToPicture(GlobalVarLn.hmapl, ref LatDX, ref LongDY);
        // -------------------------------------------------------------------------------------


               if (graphl != null)
                {
                    //graphl.FillRectangle(brushRed, (int)lat - formAP4.axaxcMapScreen.MapLeft, (int)lon - formAP4.axaxcMapScreen.MapTop, 7, 7);
                    //graphl.FillRectangle(brushRed, (int)Lat, (int)Long, 7, 7);
                    //graphl.FillRectangle(brushRed, (int)Lat - objMapForm.axaxcMapScreen1.MapLeft, (int)Long, 7, 7);

                    graphl.FillRectangle(brushRed, (int)LatDX, (int)LongDY, 7, 7);
                    //graphl.FillRectangle(brushRed, (int)LatDX - objMapForm.panel1.axaxcMapScreen1.MapLeft, (int)LongDY, 7, 7);



                    // Надпись
                    //TitleObject(typeStation, lat, lon);
                }


        // -------------------------------------------------------------------------------------

            //graphl.Dispose();


        } // P/P f_Map_Rect_LatLong
        // *************************************************************************************













    } // Class DrawL
} // namespace GrazaMap
