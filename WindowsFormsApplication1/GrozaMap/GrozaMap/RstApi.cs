﻿using System.Runtime.InteropServices;
using axGisToolKit;

namespace Berezina.Maps
{
    public class RstApi
    {
        public const string GisLibrary1 = "gisacces.dll";


        [DllImport(GisLibrary1, CharSet = CharSet.Ansi)]
        public static extern int mapOpenMtr(string rstname, int mode = 0);

        [DllImport(GisLibrary1, CharSet = CharSet.Ansi)]
        public static extern double mapGetHeightValue(int hmap, double x, double y);



        [DllImport(GisLibrary1, CharSet = CharSet.Ansi)]
        public static extern int mapCreatePaintControl(int hmap);

        [DllImport(GisLibrary1, CharSet = CharSet.Ansi)]
        public static extern double mapGetPrecisionHeightTriangleEx(int hmap, double x, double y, int hpaint);

        [DllImport(GisLibrary1, CharSet = CharSet.Ansi)]
        public static extern double mapFreePaintControl(int hpaint);

        // Закрыть растровые данные и удалить файл
        // hMap   - идентификатор открытых данных
        // number - номер растрового файла в цепочке
        // Если number равен 0, закрываются и удалаются все растровые данные
        // При ошибке возвращает ноль
        [DllImport(GisLibrary1, CharSet = CharSet.Ansi)]
        public static extern int mapDeleteRst(int hpaint, int number);


        // Запросить номер растра в цепочке по имени файла 
        // hMap    - идентификатор открытых данных
        // name        - имя файла растра
        // В цепочке номера растров начинаются с 1.
        // При ошибке возвращает ноль
        [DllImport(GisLibrary1, CharSet = CharSet.Ansi)]
        public static extern int mapGetRstNumberByName(int hpaint, string name);

        /*
      // Создание файла растрового изображения
      // rstname    - имя создаваемого файла
      // width      - ширина растрового изображения в элементах
      // height     - высота растрового изображения в элементах
      // nbits      - размер элемента (бит на пиксел)
      // palette    - адрес устанавливаемой палитры
      // colorcount - число элементов в новой палитре
      // scale      - масштаб
      // precision  - разрешение растра
      // При успешном завершении функция создает файл rstname с заполненным
      // паспортом и палитрой растра.
      //  При ошибке возвращает 0
 
        _MAPIMP HMAP _MAPAPI mapCreateRst(const char *rstname, long int width, long int height,
                                  long int nbits, COLORREF* palette, long int colorcount,
                                  double scale = 0, double precision = 0);
             */

        [DllImport(GisLibrary1, CharSet = CharSet.Ansi)]
        public static extern int mapCreateRst(string rstname, int width, int height,
                                  int nbits, TuColorRef palette, int colorcount, double scale = 0, double precision = 0);

        [DllImport(GisLibrary1, CharSet = CharSet.Ansi)]
        public static extern int mapOpenRst(string rstname, int mode=0);

        [DllImport(GisLibrary1, CharSet = CharSet.Ansi)]
        public static extern int mapPutRstPoint(int hMap, int number, int value, int row, int column);

        [DllImport(GisLibrary1, CharSet = CharSet.Ansi)]
        public static extern int mapGetRstPoint(int hMap, int number, ref int value, int row, int column);

        /*
         // Создание файла растровой карты
  // name            - полное имя растра
  // width           - ширина изображения в пикселях
  // height          - высота изображения в пикселях
  // nbits           - количество бит на пиксель (1,4,8,24)
  // palette         - указатель на палитру растра (справедливо для 1,4,8 бит на пиксель)
  // scale           - мастаб растра
  // precisionMet    - разрешения растра (точек на метр)
  // meterInElementX - размер пикселя растра в метрах на местности по оси X (по вертикали)
  // meterInElementY - размер пикселя растра в метрах на местности по оси Y (по горизонтали)
  //                   meterInElementX и meterInElementY могут иметь разные значения
  // location        - координаты юго-западного угла растра в метрах, соответствующие СК в mapregister
  // mapregister     - проекции исходного материала
  // Важно!!!
  // Для корректного открытия растров в 10-ой и более ранних версиях необходимо выполнить условие:
  //                            meterInElementX = scale/precisionMet;
  // Иначе масштаб и разрешение будут пересчитаны!!!
  // При ошибке возвращает 0
 
_MAPIMP HMAP _MAPAPI mapCreateRstEx(char *rstname, long int width, long int height, long int nbits,
                                    COLORREF* palette, long int colorcount, double scale, double precisionMet,
                                    double meterInElementX, double meterInElementY,
                                    DOUBLEPOINT *location, MAPREGISTEREX *mapregister);
         */

        [DllImport(GisLibrary1, CharSet = CharSet.Ansi)]
        public static extern int mapCreateRstEx(string rstname, int width, int height, int nbits,
                                    int palette, int colorcount, double scale, double precisionMet,
                                    double meterInElementX, double meterInElementY,
                                    ref DOUBLEPOINT location, ref MAPREGISTEREX mapregister);


        [DllImport(GisLibrary1, CharSet = CharSet.Ansi)]
        public static extern int mapRegisterFromMapType(int maptype, ref MAPREGISTEREX mapreg);


        /*
         // Запись элемента  по его прямоугольным координатам
  // (в метрах) в буфер
  // hMap       - идентификатор открытой векторной карты
  // number     - номер файла в цепочке
  // x,y        - координаты элемента
  // value      - значение элемента
  // При ошибке возвращает ноль
 
 _MAPIMP long int _MAPAPI mapPutRstPlanePoint(HMAP hMap, long int number, long int value,
                                             double x, double y);
 
         */

        [DllImport(GisLibrary1, CharSet = CharSet.Ansi)]
        public static extern int mapPutRstPlanePoint(int hMap, int number, int value, double x, double y);

        /*
         // Установить прозрачный цвет растра (для 16-,24-,32-битных растров) 
  //  hMap   - идентификатор открытой векторной карты
  //  number - номер файла в цепочке
  //  color  - значение прозрачного цвета в формате RGB (от 0 до 0x00FFFFFF)
  // При установке IMGC_TRANSPARENT (0xFFFFFFFF) прозрачный цвет не используется
  // При ошибке возвращает IMGC_TRANSPARENT
 
_MAPIMP COLORREF _MAPAPI mapSetRstTransparentColor(HMAP hMap, long int number, COLORREF color);
         
         */

        [DllImport(GisLibrary1, CharSet = CharSet.Ansi)]
        public static extern int mapSetRstTransparentColor(int hMap, int number, int palette);

        /*
         // Запросить ширину растра в элементах
  // hMap       - идентификатор открытой векторной карты
  // number - номер файла в цепочке
  // При ошибке возвращает ноль
 
  _MAPIMP long int _MAPAPI mapGetRstWidth(HMAP hMap,long int number);
 
  // Запросить высоту растра в элементах
  // hMap       - идентификатор открытой векторной карты
  // number - номер файла в цепочке
  // При ошибке возвращает ноль
 
  _MAPIMP long int _MAPAPI mapGetRstHeight(HMAP hMap,long int number);
 */

                [DllImport(GisLibrary1, CharSet = CharSet.Ansi)]
        public static extern int mapGetRstWidth(int hMap, int number);

                [DllImport(GisLibrary1, CharSet = CharSet.Ansi)]
        public static extern int mapGetRstHeight(int hMap, int number);


        [DllImport(GisLibrary1, CharSet = CharSet.Ansi)]
        public static extern int mapGetBackColor(int hMap);

        /*
         *mapGetRstPalette(HMAP hMap, COLORREF *palette,
                                          long int count, long int number);
 
  // Установить описание палитры растра
  // hMap    - идентификатор открытой векторной карты
  // palette - адрес устанавливаемой палитры
  // count   - число элементов в новой палитре
  // number  - номер файла в цепочке
  // При ошибке возвращает ноль
         
         */

        [DllImport(GisLibrary1, CharSet = CharSet.Ansi)]
        public static extern int mapGetRstPalette(int hMap, ref TxColorRef palette, int count, int number);



    }
}
