﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AxaxGisToolKit;
using axGisToolKit;
using System.Runtime.InteropServices;
using System.Windows.Forms;

using System.IO;
using System.Reflection;
using System.Globalization;


namespace GrozaMap
{
    public partial class FormBearing : Form
    {
        [DllImport("gisacces.dll")]
        public static extern int mapPlaneToGeoWGS84(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        public static extern int mapPlaneToPicture(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        public static extern int mapPictureToPlane(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        public static extern int mapPlaneToGeo(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        static extern int mapGeoToPlane(int hmap, ref double Bx, ref double Ly);

        private AxaxcMapScreen axaxcMapScreen;

        // Переменные VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR 
        private double dchislo;
        private long ichislo;
        private double LAMBDA;

        // .....................................................................
        // Координаты СП,ys

        //private uint flCoordSP_comm; // =1-> Выбрали СП
        //private uint flCoordYS1_comm; // =1-> Выбрали YS1
        //private uint flCoordYS2_comm; // =1-> Выбрали YS2

        // Координаты СП на местности в м
        private double XSP_comm;
        private double YSP_comm;
        private double XYS1_comm;
        private double YYS1_comm;
        private double XYS2_comm;
        private double YYS2_comm;

        // DATUM
        private double dXdat_comm;
        private double dYdat_comm;
        private double dZdat_comm;

        private double dLat_comm;
        private double dLong_comm;

        // Эллипсоид Красовского, град
        private double LatKrG_comm;
        private double LongKrG_comm;
        // Эллипсоид Красовского, rad
        private double LatKrR_comm;
        private double LongKrR_comm;
        // Эллипсоид Красовского, град,мин,сек
        private int Lat_Grad_comm;
        private int Lat_Min_comm;
        private double Lat_Sec_comm;
        private int Long_Grad_comm;
        private int Long_Min_comm;
        private double Long_Sec_comm;
        // Гаусс-крюгер(СК42) м
        private double XSP42_comm;
        private double YSP42_comm;

        // Эллипсоид Красовского, град
        private double LatKrG_YS1_comm;
        private double LongKrG_YS1_comm;
        // Эллипсоид Красовского, rad
        private double LatKrR_YS1_comm;
        private double LongKrR_YS1_comm;
        // Эллипсоид Красовского, град,мин,сек
        private int Lat_Grad_YS1_comm;
        private int Lat_Min_YS1_comm;
        private double Lat_Sec_YS1_comm;
        private int Long_Grad_YS1_comm;
        private int Long_Min_YS1_comm;
        private double Long_Sec_YS1_comm;
        // Гаусс-крюгер(СК42) м
        private double XYS142_comm;
        private double YYS142_comm;

        // Эллипсоид Красовского, град
        private double LatKrG_YS2_comm;
        private double LongKrG_YS2_comm;
        // Эллипсоид Красовского, rad
        private double LatKrR_YS2_comm;
        private double LongKrR_YS2_comm;
        // Эллипсоид Красовского, град,мин,сек
        private int Lat_Grad_YS2_comm;
        private int Lat_Min_YS2_comm;
        private double Lat_Sec_YS2_comm;
        private int Long_Grad_YS2_comm;
        private int Long_Min_YS2_comm;
        private double Long_Sec_YS2_comm;
        // Гаусс-крюгер(СК42) м
        private double XYS242_comm;
        private double YYS242_comm;

        // ......................................................................
        // Основные параметры

        private double OwnHeight_comm;
        private double Point1Height_comm;
        private double Point2Height_comm;
        //private double HeightOwnObject_comm;
        private double PowerOwn_comm;
        private double CoeffOwn_comm;
        //private double RadiusZone_comm;
        //private double MaxDist_comm;

        private int i_HeightOwnObject_comm;
        private int i_Cap1_comm;
        private int i_WidthHindrance_comm;
        private int i_Surface_comm;
        private double Cap1_comm;
        private double WidthHindrance_comm;
        //private double Surface_comm;

        // Высота средства подавления
        private double HeightAntennOwn_comm;
        private double HeightTotalOwn_comm;

        // Для подавляемой линии
        private double Freq_comm;
        private double PowerOpponent_comm;
        private double CoeffTransmitOpponent_comm;
        private double CoeffReceiverOpponent_comm;
        private double RangeComm_comm;
        private double WidthSignal_comm;
        private double HeightTransmitOpponent_comm;
        private double HeightReceiverOpponent_comm;
        private double CoeffSupOpponent_comm;
        private int i_PolarOpponent_comm;
        private int i_CoeffSupOpponent_comm;
        private int i_TypeCommOpponent_comm;

        // ......................................................................
        // Зона

        private double dCoeffQ_comm;
        private double dCoeffHE_comm;
        private int iCorrectHeightOwn_comm;
        private int iResultHeightOwn_comm;
        private int iMiddleHeight_comm;
        private int iMinHeight_comm;
        private int iCorrectHeightOpponent_comm;
        private int iResultHeightOpponent_comm;
        private long iMaxDistance_comm;
        private double dGamma_comm;
        //private long liRadiusZone_comm;

        // ......................................................................
        private int iDistJammerComm1; // расстояние от УС1 до средства подаления
        private int iDistJammerComm2; // расстояние от УС2 до средства подаления
        private int iDistBetweenComm;
        private bool blResultSupress;

       // ----------------------------------------------------------------------
       // Zone

        private double Dmax_PelMain;
        private double dD_PelMain;
        private double CKO_PelMain;
        private double dTheta_PelMain;
        private double R_PelMain;
        private double Baza_PelMain;
        private double Gamma_PelMain;
        private double R1_PelMain;
        private double R2_PelMain;

        // VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR Переменные

        // Конструктор *********************************************************** 
        ComponentResourceManager resources = new ComponentResourceManager(typeof(FormBearing));
        private int NumberOfLanguage;


        public FormBearing(ref AxaxcMapScreen axaxcMapScreen1)
        {
            InitializeComponent();



            axaxcMapScreen = axaxcMapScreen1;

            dchislo = 0;
            ichislo = 0;

            LAMBDA = 300000;

            //flCoordSP_comm = 0; // =1-> Выбрали СП
            //flCoordYS1_comm = 0; // =1-> Выбрали YS1
            //flCoordYS2_comm = 0; // =1-> Выбрали YS2

            // .....................................................................
            // Координаты СП

            // Координаты СП на местности в м
            XSP_comm = 0;
            YSP_comm = 0;
            XYS1_comm = 0;
            YYS1_comm = 0;
            XYS2_comm = 0;
            YYS2_comm = 0;

            // DATUM
            // ГОСТ 51794_2008
            dXdat_comm = 25;
            dYdat_comm = -141;
            dZdat_comm = -80;

            dLat_comm = 0;
            dLong_comm = 0;

            // Эллипсоид Красовского, град
            LatKrG_comm = 0;
            LongKrG_comm = 0;
            // Эллипсоид Красовского, rad
            LatKrR_comm = 0;
            LongKrR_comm = 0;
            // Эллипсоид Красовского, град,мин,сек
            Lat_Grad_comm = 0;
            Lat_Min_comm = 0;
            Lat_Sec_comm = 0;
            Long_Grad_comm = 0;
            Long_Min_comm = 0;
            Long_Sec_comm = 0;
            // Гаусс-крюгер(СК42) м
            XSP42_comm = 0;
            YSP42_comm = 0;

            // Эллипсоид Красовского, град
            LatKrG_YS1_comm = 0;
            LongKrG_YS1_comm = 0;
            // Эллипсоид Красовского, rad
            LatKrR_YS1_comm = 0;
            LongKrR_YS1_comm = 0;
            // Эллипсоид Красовского, град,мин,сек
            Lat_Grad_YS1_comm = 0;
            Lat_Min_YS1_comm = 0;
            Lat_Sec_YS1_comm = 0;
            Long_Grad_YS1_comm = 0;
            Long_Min_YS1_comm = 0;
            Long_Sec_YS1_comm = 0;
            // Гаусс-крюгер(СК42) м
            XYS142_comm = 0;
            YYS142_comm = 0;

            // Эллипсоид Красовского, град
            LatKrG_YS2_comm = 0;
            LongKrG_YS2_comm = 0;
            // Эллипсоид Красовского, rad
            LatKrR_YS2_comm = 0;
            LongKrR_YS2_comm = 0;
            // Эллипсоид Красовского, град,мин,сек
            Lat_Grad_YS2_comm = 0;
            Lat_Min_YS2_comm = 0;
            Lat_Sec_YS2_comm = 0;
            Long_Grad_YS2_comm = 0;
            Long_Min_YS2_comm = 0;
            Long_Sec_YS2_comm = 0;
            // Гаусс-крюгер(СК42) м
            XYS242_comm = 0;
            YYS242_comm = 0;

            // ......................................................................
            // Основные параметры

            OwnHeight_comm = 0;
            Point1Height_comm = 0;
            Point2Height_comm = 0;
            //HeightOwnObject_comm = 0;
            PowerOwn_comm = 0;
            CoeffOwn_comm = 0;
            //RadiusZone_comm = 0;
            //MaxDist_comm = 0;

            i_HeightOwnObject_comm = 0;
            i_Cap1_comm = 0;
            i_WidthHindrance_comm = 0;
            i_Surface_comm = 0;
            Cap1_comm = 0;
            WidthHindrance_comm = 0;
            //Surface_comm = 0;

            // Высота средства подавления
            // ??????????????????????
            HeightAntennOwn_comm = 0;
            HeightTotalOwn_comm = 0;

            // Для подавляемой линии
            Freq_comm = 0;
            PowerOpponent_comm = 0;
            CoeffTransmitOpponent_comm = 0;
            CoeffReceiverOpponent_comm = 0;
            RangeComm_comm = 0;
            WidthSignal_comm = 0;
            HeightTransmitOpponent_comm = 0;
            HeightReceiverOpponent_comm = 0;
            CoeffSupOpponent_comm = 0;
            i_PolarOpponent_comm = 0;
            i_CoeffSupOpponent_comm = 0;
            i_TypeCommOpponent_comm = 0;

            // ......................................................................
            // Зона

            dCoeffQ_comm = 0;
            dCoeffHE_comm = 0;
            iCorrectHeightOwn_comm = 0;
            iResultHeightOwn_comm = 0;
            iMiddleHeight_comm = 0;
            iMinHeight_comm = 0;
            iCorrectHeightOpponent_comm = 0;
            iResultHeightOpponent_comm = 0;
            iMaxDistance_comm = 0;
            dGamma_comm = 0;
            //liRadiusZone_comm = 0;

            // ......................................................................
            iDistJammerComm1 = 0; // расстояние от УС1 до средства подаления
            iDistJammerComm2 = 0; // расстояние от УС2 до средства подаления
            iDistBetweenComm = 0;
            blResultSupress = false;

       // ----------------------------------------------------------------------
       // Zone

        Dmax_PelMain=0;
        dD_PelMain=0;
        CKO_PelMain=0;
        dTheta_PelMain=0;
        R_PelMain=0;
        Baza_PelMain=0;
        Gamma_PelMain=0;
        R1_PelMain=0;
        R2_PelMain=0;


        } // Конструктор
        // ***********************************************************  Конструктор

        // ************************************************************************
        // Загрузка формы
        // ************************************************************************
        private void FormBearing_Load(object sender, EventArgs e)
        {
            string strExePath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

            string PathFolder1 = Path.GetDirectoryName(strExePath);

            string strPathLatest = PathFolder1 + "\\INI\\Common.ini";
            foreach (string line in File.ReadLines(strPathLatest))
            {
                if (line.Contains("Language "))
                {
                    string LanguageAsNumber = line.Split(new string[] { "=" }, StringSplitOptions.None).Last();
                    NumberOfLanguage = Int32.Parse(LanguageAsNumber);
                    break;
                }
            }
            LanguageChooser();

            // ----------------------------------------------------------------------
            gbOwnRect.Visible = true;
            gbOwnRect.Location = new Point(10, 17);

            gbOwnRect42.Visible = false;
            gbOwnRad.Visible = false;
            gbOwnDegMin.Visible = false;
            gbOwnDegMinSec.Visible = false;

            cbChooseSC.SelectedIndex = 0;
            // ----------------------------------------------------------------------
            gbPt1Rect.Visible = true;
            gbPt1Rect.Location = new Point(181, 17);

            gbPt1Rect42.Visible = false;
            gbPt1Rad.Visible = false;
            gbPt1DegMin.Visible = false;
            gbPt1DegMinSec.Visible = false;
            // ----------------------------------------------------------------------

            cbStep.SelectedIndex = 0;

            //chbXY1.Checked = false;
            // ----------------------------------------------------------------------
            // Переменные

            GlobalVarLn.fl_PelMain = 0; // Отрисовка зоны
            GlobalVarLn.flCoordSP_PelMain = 0; // =1-> Выбрали СП
            GlobalVarLn.flCoordYS1_PelMain = 0;
            GlobalVarLn.list1_PelMain.Clear();
            GlobalVarLn.list2_PelMain.Clear();

            // ----------------------------------------------------------------------
            button2.BackColor = Color.FromArgb(255, 0, GlobalVarLn.grn1, 0);
            button3.BackColor = Color.FromArgb(255, 0, GlobalVarLn.grn2, 0);
            button6.BackColor = Color.FromArgb(255, 0, GlobalVarLn.grn3, 0);
            button4.BackColor = Color.FromArgb(255, 0, GlobalVarLn.grn4, 0);
            button7.BackColor = Color.FromArgb(255, 0, GlobalVarLn.grn5, 0);
            button12.BackColor = Color.FromArgb(255, GlobalVarLn.rd1, 0, 0);
            button11.BackColor = Color.FromArgb(255, GlobalVarLn.rd2, 0, 0);
            button10.BackColor = Color.FromArgb(255, GlobalVarLn.rd3, 0, 0);
            button9.BackColor = Color.FromArgb(255, GlobalVarLn.rd4, 0, 0);
            button8.BackColor = Color.FromArgb(255, GlobalVarLn.rd5, 0, 0);
            // ----------------------------------------------------------------------


        } // Load
        // ************************************************************************

        // ************************************************************************
        // Очистка
        // ************************************************************************        
        private void bClear_Click(object sender, EventArgs e)
        {

            ClassMap.ClearBearing();


        } // Clear
        // ************************************************************************        

        // ************************************************************************
        // Обработчик ComboBox : Выбор СК 
        // ************************************************************************
        private void cbChooseSC_SelectedIndexChanged(object sender, EventArgs e)
        {
            ChooseSystemCoord_PelMain(cbChooseSC.SelectedIndex);

        } // SK
        // ************************************************************************        

        // ************************************************************************
        // Выбор Pelengator1
        // ************************************************************************
        private void button5_Click(object sender, EventArgs e)
        {

            double xtmp_ed, ytmp_ed, htmp_ed;
            double xtmp1_ed, ytmp1_ed;

            xtmp_ed = 0;
            ytmp_ed = 0;
            htmp_ed = 0;
            xtmp1_ed = 0;
            ytmp1_ed = 0;
            // ......................................................................
            ClassMap objClassMap1_ed = new ClassMap();
            ClassMap objClassMap2_ed = new ClassMap();
            ClassMap objClassMap3_ed = new ClassMap();
            // ......................................................................

            // ----------------------------------------------------------------------
            // Enter Pelengator1
            // Мышь на карте

            // !!! реальные координаты на местности карты в м (Plane)
            GlobalVarLn.XCenter_PelMain = GlobalVarLn.MapX1;
            GlobalVarLn.YCenter_PelMain = GlobalVarLn.MapY1;

            if ((GlobalVarLn.XCenter_PelMain == 0) || (GlobalVarLn.YCenter_PelMain == 0))
             {
                 if (GlobalVarLn.fl_Azb == 0)
                 {
                     MessageBox.Show("Не выбран пеленгатор1");
                 }
                 else
                 {
                     //Azb
                     MessageBox.Show("Pelenqator1 seçilməyib");
                 }

               return;
             }
            // ----------------------------------------------------------------------

            // ......................................................................
            xtmp_ed = GlobalVarLn.XCenter_PelMain;
            ytmp_ed = GlobalVarLn.YCenter_PelMain;
            // ......................................................................
            // H

            GlobalVarLn.axMapPointGlobalAdd.SetPoint(xtmp_ed, ytmp_ed);
            GlobalVarLn.HCenter_PelMain = (int)GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);
            htmp_ed = GlobalVarLn.HCenter_PelMain;
            tbOwnHeight.Text = Convert.ToString(htmp_ed);
            // ......................................................................
            GlobalVarLn.fl_PelMain = 1;
            GlobalVarLn.flCoordSP_PelMain = 1; // Pelengator1 выбран
            // ......................................................................
            // SP(Pelengator1) на карте

            //2106
            // Убрать с карты
            //GlobalVarLn.axMapScreenGlobal.Repaint();

            // SP(Pel1)
            ClassMap.f_DrawSPXY(
                          GlobalVarLn.XCenter_PelMain,  // m на местности
                          GlobalVarLn.YCenter_PelMain,
                              ""
                         );
            // ......................................................................
            // Реальные координаты карты в м -> в долготу и широту WGS84 с помощью функций Панорамы
            // !!! Выход функции(rad) идет на место входных переменных

            mapPlaneToGeoWGS84(GlobalVarLn.hmapl, ref xtmp_ed, ref ytmp_ed);

            // rad(WGS84)->grad(WGS84)
            xtmp1_ed = (xtmp_ed * 180) / Math.PI;
            ytmp1_ed = (ytmp_ed * 180) / Math.PI;

            //333
            GlobalVarLn.BWGS84_1_bear = xtmp1_ed;
            GlobalVarLn.LWGS84_1_bear = ytmp1_ed;
            // .......................................................................

            // WGS84(эллипсоид)->элл.Красовского *************************************
            // Перевод координат WGS84(эллипсоид) -> эллипсоид Красовского(Пулково-42)
            // WGS84(широта,долгота)из Панорамы пересчитываем в эллипсод Красовского моей функцией
            // Входные параметры -> град,km
            // Перевод в рад - внутри функции

            // dLong ..................................................................
            // Расчет приращения по долготе при преобразованиях координат WGS84<->SK42
            // (преобразования Молоденского), угл.сек

            objClassMap1_ed.f_dLong
                (
                // Входные параметры (град,км)
                    xtmp1_ed,   // широта
                    ytmp1_ed,  // долгота
                    0,     // высота

                    // DATUM,m
                    GlobalVarLn.dXdat_comm,
                    GlobalVarLn.dYdat_comm,
                    GlobalVarLn.dZdat_comm,

                    ref GlobalVarLn.dLong_br   // приращение по долготе, угл.сек
                );
            // ................................................................ dLong

            // dLat .................................................................
            // Расчет приращения по широте при преобразованиях координат WGS84<->SK42
            // (преобразования Молоденского), угл.сек

            objClassMap1_ed.f_dLat
                (
                // Входные параметры (град,км)
                    xtmp1_ed,   // широта
                    ytmp1_ed,  // долгота
                    0,     // высота

                    // DATUM,m
                    GlobalVarLn.dXdat_comm,
                    GlobalVarLn.dYdat_comm,
                    GlobalVarLn.dZdat_comm,

                    ref GlobalVarLn.dLat_br        // приращение по долготе, угл.сек
                );
            // ................................................................. dLat

            // Lat,Long .............................................................
            // Преобразования широты и долготы при пересчете WGS84->SK42

            objClassMap1_ed.f_WGS84_SK42_Lat_Long
                   (
                // Входные параметры (град,км)
                       xtmp1_ed,   // широта
                       ytmp1_ed,  // долгота
                       0,     // высота
                       GlobalVarLn.dLat_br,       // приращение по долготе, угл.сек
                       GlobalVarLn.dLong_br,      // приращение по долготе, угл.сек

                       // Выходные параметры (grad)
                       ref GlobalVarLn.LatKrG_br,   // широта
                       ref GlobalVarLn.LongKrG_br   // долгота
                   );
            // ............................................................ Lat,Long


            // *********************************** WGS84(эллипсоид)->элл.Красовского

            // .......................................................................
            // Эллипсоид Красовского, радианы

            GlobalVarLn.LatKrR_br = (GlobalVarLn.LatKrG_br * Math.PI) / 180;
            GlobalVarLn.LongKrR_br = (GlobalVarLn.LongKrG_br * Math.PI) / 180;
            // .......................................................................
            // Эллипсоид Красовского, grad,min,sec
            // dd.ddddd -> DD MM SS

            // Широта
            objClassMap3_ed.f_Grad_GMS
              (
                // Входные параметры (grad)
                GlobalVarLn.LatKrG_br,

                // Выходные параметры 
                ref GlobalVarLn.Lat_Grad_br,
                ref GlobalVarLn.Lat_Min_br,
                ref GlobalVarLn.Lat_Sec_br
              );

            // Долгота
            objClassMap3_ed.f_Grad_GMS
              (
                // Входные параметры (grad)
                GlobalVarLn.LongKrG_br,

                // Выходные параметры 
                ref GlobalVarLn.Long_Grad_br,
                ref GlobalVarLn.Long_Min_br,
                ref GlobalVarLn.Long_Sec_br
              );
            // .......................................................................

            // SK42(элл.)->Крюгер ****************************************************
            // Преобразование геодезических координат (широта, долгота, высота) 
            // эллипсоида Красовского (СК42) в плоские прямоугольные координаты в
            // проекции Гаусса-Крюгера
            // Входные параметры -> !!!grad

            objClassMap3_ed.f_SK42_Krug
                   (
                // Входные параметры (!!! grad)
                // !!! эллипсоид Красовского
                       GlobalVarLn.LatKrG_br,   // широта
                       GlobalVarLn.LongKrG_br,  // долгота

                       // Выходные параметры (km)
                       ref GlobalVarLn.XSP42_br,
                       ref GlobalVarLn.YSP42_br
                   );

            // km->m
            GlobalVarLn.XSP42_br = GlobalVarLn.XSP42_br * 1000;
            GlobalVarLn.YSP42_br = GlobalVarLn.YSP42_br * 1000;
            // **************************************************** SK42(элл.)->Крюгер

            // .......................................................................
            // Отобразить координаты в окошках

            ClassMap.OtobrPel1_Bearing();
            // .......................................................................


/*
            double xtmp_ed, ytmp_ed;
            double xtmp1_ed, ytmp1_ed;
            xtmp_ed = 0;
            ytmp_ed = 0;
            xtmp1_ed = 0;
            ytmp1_ed = 0;
            // ......................................................................
            ClassMap objClassMap1_ed = new ClassMap();
            ClassMap objClassMap2_ed = new ClassMap();
            ClassMap objClassMap3_ed = new ClassMap();

            LF objLF = new LF();
            // ......................................................................
            // !!! XSP_ed,YSP_ed -> реальные координаты на местности карты в м (Plane)

            XSP_comm = GlobalVarLn.MapX1;
            YSP_comm = GlobalVarLn.MapY1;
            // ......................................................................
            // Ручной ввод

            if (chbXY1.Checked == true)
            {

                if ((tbXRect.Text == "") || (tbYRect.Text == ""))
                {
                    MessageBox.Show("Недопустимые координаты СП");
                    return;
                }

                XSP_comm = Convert.ToDouble(tbXRect.Text);
                YSP_comm = Convert.ToDouble(tbYRect.Text);
            }
            // ......................................................................
            // Треугольник на карте

            xtmp_ed = XSP_comm;
            ytmp_ed = YSP_comm;

            GlobalVarLn.XCenter_PelMain = XSP_comm;
            GlobalVarLn.YCenter_PelMain = YSP_comm;

            objLF.X_m = XSP_comm;
            objLF.Y_m = YSP_comm;
            // SP
            GlobalVarLn.axMapPointGlobalAdd.SetPoint(GlobalVarLn.XCenter_PelMain, GlobalVarLn.YCenter_PelMain);
            OwnHeight_comm = (int)GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);
            tbOwnHeight.Text = Convert.ToString(OwnHeight_comm);

            objLF.H_m = OwnHeight_comm;
            objLF.indzn = 0;
            GlobalVarLn.list1_PelMain.Add(objLF);

            // SP1
            ClassMap.f_DrawSPXY11(
                          GlobalVarLn.XCenter_PelMain,  // m на местности
                          GlobalVarLn.YCenter_PelMain,
                   (Bitmap)GlobalVarLn.objFormSostG.imageList1.Images[GlobalVarLn.list1_PelMain[0].indzn]
                         );


            // ......................................................................
            GlobalVarLn.fl_PelMain = 1;
            GlobalVarLn.flCoordSP_PelMain = 1; // СП1 выбрана
            // ......................................................................
            // Реальные координаты карты в м -> в долготу и широту WGS84 с помощью функций Панорамы
            // !!! Выход функции(rad) идет на место входных переменных

            xtmp_ed = XSP_comm;
            ytmp_ed = YSP_comm;

            mapPlaneToGeoWGS84(GlobalVarLn.hmapl, ref xtmp_ed, ref ytmp_ed);

            // rad(WGS84)->grad(WGS84)
            xtmp1_ed = (xtmp_ed * 180) / Math.PI;
            ytmp1_ed = (ytmp_ed * 180) / Math.PI;
            // .......................................................................


            // WGS84(эллипсоид)->элл.Красовского *************************************
            // Перевод координат WGS84(эллипсоид) -> эллипсоид Красовского(Пулково-42)
            // WGS84(широта,долгота)из Панорамы пересчитываем в эллипсод Красовского моей функцией
            // Входные параметры -> град,km
            // Перевод в рад - внутри функции

            // dLong ..................................................................
            // Расчет приращения по долготе при преобразованиях координат WGS84<->SK42
            // (преобразования Молоденского), угл.сек

            objClassMap1_ed.f_dLong
                (

                    // Входные параметры (град,км)
                    xtmp1_ed,   // широта
                    ytmp1_ed,  // долгота
                    0,     // высота

                    // DATUM,m
                    dXdat_comm,
                    dYdat_comm,
                    dZdat_comm,

                    ref dLong_comm   // приращение по долготе, угл.сек

                );

            // ................................................................ dLong

            // dLat .................................................................
            // Расчет приращения по широте при преобразованиях координат WGS84<->SK42
            // (преобразования Молоденского), угл.сек

            objClassMap1_ed.f_dLat
                (

                    // Входные параметры (град,км)
                    xtmp1_ed,   // широта
                    ytmp1_ed,  // долгота
                    0,     // высота

                    // DATUM,m
                    dXdat_comm,
                    dYdat_comm,
                    dZdat_comm,

                    ref dLat_comm        // приращение по долготе, угл.сек

                );

            // ................................................................. dLat

            // Lat,Long .............................................................
            // Преобразования широты и долготы при пересчете WGS84->SK42

            objClassMap1_ed.f_WGS84_SK42_Lat_Long
                   (

                       // Входные параметры (град,км)
                       xtmp1_ed,   // широта
                       ytmp1_ed,  // долгота
                       0,     // высота
                       dLat_comm,       // приращение по долготе, угл.сек
                       dLong_comm,      // приращение по долготе, угл.сек

                       // Выходные параметры (grad)
                       ref LatKrG_comm,   // широта
                       ref LongKrG_comm   // долгота

                   );

            // ............................................................ Lat,Long


            // *********************************** WGS84(эллипсоид)->элл.Красовского

            // .......................................................................
            // Эллипсоид Красовского, радианы

            LatKrR_comm = (LatKrG_comm * Math.PI) / 180;
            LongKrR_comm = (LongKrG_comm * Math.PI) / 180;
            // .......................................................................
            // Эллипсоид Красовского, grad,min,sec
            // dd.ddddd -> DD MM SS

            // Широта
            objClassMap3_ed.f_Grad_GMS
              (
                // Входные параметры (grad)
                LatKrG_comm,

                // Выходные параметры 
                ref Lat_Grad_comm,
                ref Lat_Min_comm,
                ref Lat_Sec_comm

              );

            // Долгота
            objClassMap3_ed.f_Grad_GMS
              (
                // Входные параметры (grad)
                LongKrG_comm,

                // Выходные параметры 
                ref Long_Grad_comm,
                ref Long_Min_comm,
                ref Long_Sec_comm

              );

            // .......................................................................


            // SK42(элл.)->Крюгер ****************************************************
            // Преобразование геодезических координат (широта, долгота, высота) 
            // эллипсоида Красовского (СК42) в плоские прямоугольные координаты в
            // проекции Гаусса-Крюгера
            // Входные параметры -> !!!grad

            objClassMap3_ed.f_SK42_Krug
                   (
                // Входные параметры (!!! grad)
                // !!! эллипсоид Красовского
                       LatKrG_comm,   // широта
                       LongKrG_comm,  // долгота

                       // Выходные параметры (km)
                       ref XSP42_comm,
                       ref YSP42_comm

                   );

            // km->m
            XSP42_comm = XSP42_comm * 1000;
            YSP42_comm = YSP42_comm * 1000;
            // **************************************************** SK42(элл.)->Крюгер

            // .......................................................................
            // Отображение СП в выбранной СК

            OtobrSP_PelMain();
            // .......................................................................
 */

        } // Pelengator1
        // ************************************************************************

        // ************************************************************************
        // Выбор Pelengator2
        // ************************************************************************
        private void button1_Click(object sender, EventArgs e)
        {

            double xtmp_ed, ytmp_ed, htmp_ed;
            double xtmp1_ed, ytmp1_ed;

            xtmp_ed = 0;
            ytmp_ed = 0;
            htmp_ed = 0;
            xtmp1_ed = 0;
            ytmp1_ed = 0;
            // ......................................................................
            ClassMap objClassMap1_ed = new ClassMap();
            ClassMap objClassMap2_ed = new ClassMap();
            ClassMap objClassMap3_ed = new ClassMap();
            // ......................................................................

            if(GlobalVarLn.flCoordSP_PelMain==0)
            {
                if (GlobalVarLn.fl_Azb == 0)
                {
                    MessageBox.Show("Не выбран пеленгатор1");
                }
                else
                {
                    //Azb
                    MessageBox.Show("Pelenqator1 seçilməyib");
                }

                return;
            }

            if (GlobalVarLn.flCoordYS1_PelMain==1)
            {
                if (GlobalVarLn.fl_Azb == 0)
                {
                    MessageBox.Show("Пеленгатор2 уже выбран. Очистите данные");
                }
                else
                {
                    //Azb???
                    MessageBox.Show("Pelenqator2 seçilmiş. Melumatları silmek");

                }
                return;
            }
            // ----------------------------------------------------------------------
            // Enter Pelengator2
            // Мышь на карте

            // !!! реальные координаты на местности карты в м (Plane)
            GlobalVarLn.XPoint1_PelMain = GlobalVarLn.MapX1;
            GlobalVarLn.YPoint1_PelMain = GlobalVarLn.MapY1;

            if ((GlobalVarLn.XPoint1_PelMain == 0) || (GlobalVarLn.YPoint1_PelMain == 0))
            {
                if (GlobalVarLn.fl_Azb == 0)
                {
                    MessageBox.Show("Не выбран пеленгатор2");
                }
                else
                {
                    //Azb
                    MessageBox.Show("Pelenqator2 seçilməyib");
                }

                return;
            }
            // ----------------------------------------------------------------------

            // ......................................................................
            xtmp_ed = GlobalVarLn.XPoint1_PelMain;
            ytmp_ed = GlobalVarLn.YPoint1_PelMain;
            // ......................................................................
            // H

            GlobalVarLn.axMapPointGlobalAdd.SetPoint(xtmp_ed, ytmp_ed);
            GlobalVarLn.HPoint1_PelMain = (int)GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);
            htmp_ed = GlobalVarLn.HPoint1_PelMain;
            tbPt1Height.Text = Convert.ToString(htmp_ed);
            // ......................................................................
            GlobalVarLn.flCoordYS1_PelMain = 1;        // Pel2 выбран
            // ......................................................................
            // SP(Pelengator2) на карте

            // Убрать с карты
            //GlobalVarLn.axMapScreenGlobal.Repaint();

            // SP(Pel2)
            ClassMap.f_DrawSPXY(
                          GlobalVarLn.XPoint1_PelMain,  // m на местности
                          GlobalVarLn.YPoint1_PelMain,
                              ""
                         );
            // ......................................................................

            // ......................................................................
            // Реальные координаты карты в м -> в долготу и широту WGS84 с помощью функций Панорамы
            // !!! Выход функции(rad) идет на место входных переменных

            mapPlaneToGeoWGS84(GlobalVarLn.hmapl, ref xtmp_ed, ref ytmp_ed);

            // rad(WGS84)->grad(WGS84)
            xtmp1_ed = (xtmp_ed * 180) / Math.PI;
            ytmp1_ed = (ytmp_ed * 180) / Math.PI;

            //333
            GlobalVarLn.BWGS84_2_bear = xtmp1_ed;
            GlobalVarLn.LWGS84_2_bear = ytmp1_ed;
            // .......................................................................

            // WGS84(эллипсоид)->элл.Красовского *************************************
            // Перевод координат WGS84(эллипсоид) -> эллипсоид Красовского(Пулково-42)
            // WGS84(широта,долгота)из Панорамы пересчитываем в эллипсод Красовского моей функцией
            // Входные параметры -> град,km
            // Перевод в рад - внутри функции

            // dLong ..................................................................
            // Расчет приращения по долготе при преобразованиях координат WGS84<->SK42
            // (преобразования Молоденского), угл.сек

            objClassMap1_ed.f_dLong
                (
                // Входные параметры (град,км)
                    xtmp1_ed,   // широта
                    ytmp1_ed,  // долгота
                    0,     // высота

                    // DATUM,m
                    GlobalVarLn.dXdat_comm,
                    GlobalVarLn.dYdat_comm,
                    GlobalVarLn.dZdat_comm,

                    ref GlobalVarLn.dLong_br   // приращение по долготе, угл.сек
                );
            // ................................................................ dLong

            // dLat .................................................................
            // Расчет приращения по широте при преобразованиях координат WGS84<->SK42
            // (преобразования Молоденского), угл.сек

            objClassMap1_ed.f_dLat
                (
                // Входные параметры (град,км)
                    xtmp1_ed,   // широта
                    ytmp1_ed,  // долгота
                    0,     // высота

                    // DATUM,m
                    GlobalVarLn.dXdat_comm,
                    GlobalVarLn.dYdat_comm,
                    GlobalVarLn.dZdat_comm,

                    ref GlobalVarLn.dLat_br        // приращение по долготе, угл.сек
                );
            // ................................................................. dLat

            // Lat,Long .............................................................
            // Преобразования широты и долготы при пересчете WGS84->SK42

            objClassMap1_ed.f_WGS84_SK42_Lat_Long
                   (
                // Входные параметры (град,км)
                       xtmp1_ed,   // широта
                       ytmp1_ed,  // долгота
                       0,     // высота
                       GlobalVarLn.dLat_br,       // приращение по долготе, угл.сек
                       GlobalVarLn.dLong_br,      // приращение по долготе, угл.сек

                       // Выходные параметры (grad)
                       ref GlobalVarLn.LatKrG_YS1_br,   // широта
                       ref GlobalVarLn.LongKrG_YS1_br   // долгота
                   );
            // ............................................................ Lat,Long


            // *********************************** WGS84(эллипсоид)->элл.Красовского

            // .......................................................................
            // Эллипсоид Красовского, радианы

            GlobalVarLn.LatKrR_YS1_br = (GlobalVarLn.LatKrG_YS1_br * Math.PI) / 180;
            GlobalVarLn.LongKrR_YS1_br = (GlobalVarLn.LongKrG_YS1_br * Math.PI) / 180;
            // .......................................................................
            // Эллипсоид Красовского, grad,min,sec
            // dd.ddddd -> DD MM SS

            // Широта
            objClassMap3_ed.f_Grad_GMS
              (
                // Входные параметры (grad)
                GlobalVarLn.LatKrG_YS1_br,

                // Выходные параметры 
                ref GlobalVarLn.Lat_Grad_YS1_br,
                ref GlobalVarLn.Lat_Min_YS1_br,
                ref GlobalVarLn.Lat_Sec_YS1_br
              );

            // Долгота
            objClassMap3_ed.f_Grad_GMS
              (
                // Входные параметры (grad)
                GlobalVarLn.LongKrG_YS1_br,

                // Выходные параметры 
                ref GlobalVarLn.Long_Grad_YS1_br,
                ref GlobalVarLn.Long_Min_YS1_br,
                ref GlobalVarLn.Long_Sec_YS1_br
              );
            // .......................................................................

            // SK42(элл.)->Крюгер ****************************************************
            // Преобразование геодезических координат (широта, долгота, высота) 
            // эллипсоида Красовского (СК42) в плоские прямоугольные координаты в
            // проекции Гаусса-Крюгера
            // Входные параметры -> !!!grad

            objClassMap3_ed.f_SK42_Krug
                   (
                // Входные параметры (!!! grad)
                // !!! эллипсоид Красовского
                       GlobalVarLn.LatKrG_YS1_br,   // широта
                       GlobalVarLn.LongKrG_YS1_br,  // долгота

                       // Выходные параметры (km)
                       ref GlobalVarLn.XYS142_br,
                       ref GlobalVarLn.YYS142_br
                   );

            // km->m
            GlobalVarLn.XYS142_br = GlobalVarLn.XYS142_br * 1000;
            GlobalVarLn.YYS142_br = GlobalVarLn.YYS142_br * 1000;
            // **************************************************** SK42(элл.)->Крюгер

            // .......................................................................
            // Отобразить координаты в окошках

            ClassMap.OtobrPel2_Bearing();
            // .......................................................................



/*
            double xtmp_ed, ytmp_ed;
            double xtmp1_ed, ytmp1_ed;

            xtmp_ed = 0;
            ytmp_ed = 0;
            xtmp1_ed = 0;
            ytmp1_ed = 0;
            // ......................................................................
            ClassMap objClassMap4_ed = new ClassMap();
            ClassMap objClassMap5_ed = new ClassMap();
            ClassMap objClassMap6_ed = new ClassMap();

            LF objLF = new LF();
            // ......................................................................
            // !!! реальные координаты на местности карты в м (Plane)

            XYS1_comm = GlobalVarLn.MapX1;
            YYS1_comm = GlobalVarLn.MapY1;
            // ......................................................................
            // Ручной ввод

            if (chbXY1.Checked == true)
            {

                if ((tbPt1XRect.Text == "") || (tbPt1YRect.Text == ""))
                {
                    MessageBox.Show("Недопустимые координаты УС1");
                    return;
                }

                XYS1_comm = Convert.ToDouble(tbPt1XRect.Text);
                YYS1_comm = Convert.ToDouble(tbPt1YRect.Text);

            }
            // ......................................................................

            xtmp_ed = XYS1_comm;
            ytmp_ed = YYS1_comm;

            GlobalVarLn.XPoint1_PelMain = XYS1_comm;
            GlobalVarLn.YPoint1_PelMain = YYS1_comm;

            objLF.X_m = XYS1_comm;
            objLF.Y_m = YYS1_comm;
            // SP2
            GlobalVarLn.axMapPointGlobalAdd.SetPoint(GlobalVarLn.XPoint1_PelMain, GlobalVarLn.YPoint1_PelMain);
            Point1Height_comm = (int)GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);
            tbPt1Height.Text = Convert.ToString(Point1Height_comm);

            objLF.H_m = Point1Height_comm;
            objLF.indzn = 0;
            GlobalVarLn.list1_PelMain.Add(objLF);

            // SP2
            ClassMap.f_DrawSPXY11(
                          GlobalVarLn.XPoint1_PelMain,  // m на местности
                          GlobalVarLn.YPoint1_PelMain,
                   (Bitmap)GlobalVarLn.objFormSostG.imageList1.Images[GlobalVarLn.list1_PelMain[1].indzn]
                         );
            // ......................................................................
            GlobalVarLn.flCoordYS1_PelMain = 1; // SP2 выбран
            // ......................................................................
            // Реальные координаты карты в м -> в долготу и широту WGS84 с помощью функций Панорамы
            // !!! Выход функции(rad) идет на место входных переменных

            xtmp_ed = XYS1_comm;
            ytmp_ed = YYS1_comm;

            mapPlaneToGeoWGS84(GlobalVarLn.hmapl, ref xtmp_ed, ref ytmp_ed);

            // rad(WGS84)->grad(WGS84)
            xtmp1_ed = (xtmp_ed * 180) / Math.PI;
            ytmp1_ed = (ytmp_ed * 180) / Math.PI;
            // .......................................................................


            // WGS84(эллипсоид)->элл.Красовского *************************************
            // Перевод координат WGS84(эллипсоид) -> эллипсоид Красовского(Пулково-42)
            // WGS84(широта,долгота)из Панорамы пересчитываем в эллипсод Красовского моей функцией
            // Входные параметры -> град,km
            // Перевод в рад - внутри функции

            // dLong ..................................................................
            // Расчет приращения по долготе при преобразованиях координат WGS84<->SK42
            // (преобразования Молоденского), угл.сек

            objClassMap4_ed.f_dLong
                (

                    // Входные параметры (град,км)
                    xtmp1_ed,   // широта
                    ytmp1_ed,  // долгота
                    0,     // высота

                    // DATUM,m
                    dXdat_comm,
                    dYdat_comm,
                    dZdat_comm,

                    ref dLong_comm   // приращение по долготе, угл.сек

                );

            // ................................................................ dLong

            // dLat .................................................................
            // Расчет приращения по широте при преобразованиях координат WGS84<->SK42
            // (преобразования Молоденского), угл.сек

            objClassMap4_ed.f_dLat
                (

                    // Входные параметры (град,км)
                    xtmp1_ed,   // широта
                    ytmp1_ed,  // долгота
                    0,     // высота

                    // DATUM,m
                    dXdat_comm,
                    dYdat_comm,
                    dZdat_comm,

                    ref dLat_comm        // приращение по долготе, угл.сек

                );

            // ................................................................. dLat

            // Lat,Long .............................................................
            // Преобразования широты и долготы при пересчете WGS84->SK42

            objClassMap4_ed.f_WGS84_SK42_Lat_Long
                   (

                       // Входные параметры (град,км)
                       xtmp1_ed,   // широта
                       ytmp1_ed,  // долгота
                       0,     // высота
                       dLat_comm,       // приращение по долготе, угл.сек
                       dLong_comm,      // приращение по долготе, угл.сек

                       // Выходные параметры (grad)
                       ref LatKrG_YS1_comm,   // широта
                       ref LongKrG_YS1_comm   // долгота

                   );

            // ............................................................ Lat,Long


            // *********************************** WGS84(эллипсоид)->элл.Красовского

            // .......................................................................
            // Эллипсоид Красовского, радианы

            LatKrR_YS1_comm = (LatKrG_YS1_comm * Math.PI) / 180;
            LongKrR_YS1_comm = (LongKrG_YS1_comm * Math.PI) / 180;
            // .......................................................................
            // Эллипсоид Красовского, grad,min,sec
            // dd.ddddd -> DD MM SS

            // Широта
            objClassMap5_ed.f_Grad_GMS
              (
                // Входные параметры (grad)
                LatKrG_YS1_comm,

                // Выходные параметры 
                ref Lat_Grad_YS1_comm,
                ref Lat_Min_YS1_comm,
                ref Lat_Sec_YS1_comm

              );

            // Долгота
            objClassMap5_ed.f_Grad_GMS
              (
                // Входные параметры (grad)
                LongKrG_YS1_comm,

                // Выходные параметры 
                ref Long_Grad_YS1_comm,
                ref Long_Min_YS1_comm,
                ref Long_Sec_YS1_comm

              );
            // .......................................................................

            // SK42(элл.)->Крюгер ****************************************************
            // Преобразование геодезических координат (широта, долгота, высота) 
            // эллипсоида Красовского (СК42) в плоские прямоугольные координаты в
            // проекции Гаусса-Крюгера
            // Входные параметры -> !!!grad

            objClassMap6_ed.f_SK42_Krug
                   (
                // Входные параметры (!!! grad)
                // !!! эллипсоид Красовского
                       LatKrG_YS1_comm,   // широта
                       LongKrG_YS1_comm,  // долгота

                       // Выходные параметры (km)
                       ref XYS142_comm,
                       ref YYS142_comm

                   );

            // km->m
            XYS142_comm = XYS142_comm * 1000;
            YYS142_comm = YYS142_comm * 1000;
            // **************************************************** SK42(элл.)->Крюгер

            // .......................................................................
            // Отображение СП в выбранной СК

            OtobrYS1_PelMain();
            // .......................................................................
*/

        } // Pelengator2
        // ***************************************************************************

        // Расчет зоны MAIN MAIN MAIN MAIN MAIN MAIN MAIN MAIN MAIN MAIN MAIN MAIN 

        private void bAccept_Click(object sender, EventArgs e)
        {
            // ----------------------------------------------------------------------
            if (GlobalVarLn.flCoordSP_PelMain == 0)
            {
                if (GlobalVarLn.fl_Azb == 0)
                {
                    MessageBox.Show("Не выбран пеленгатор1");
                }
                else
                {
                    //Azb
                    MessageBox.Show("Pelenqator1 seçilməyib");
                }

                return;
            }

            if (GlobalVarLn.flCoordYS1_PelMain == 0)
            {
                if (GlobalVarLn.fl_Azb == 0)
                {
                    MessageBox.Show("Не выбран пеленгатор2");
                }
                else
                {
                    //Azb
                    MessageBox.Show("Pelenqator2 seçilməyib");
                }

                return;
            }
            // ----------------------------------------------------------------------

            // Pel1Pel2 **************************************************************
            // Координаты на местности в м

            GlobalVarLn.tpOwnCoordRect_PelMain.X = (int)GlobalVarLn.XCenter_PelMain;
            GlobalVarLn.tpOwnCoordRect_PelMain.Y = (int)GlobalVarLn.YCenter_PelMain;
            GlobalVarLn.tpPoint1Rect_PelMain.X = (int)GlobalVarLn.XPoint1_PelMain;
            GlobalVarLn.tpPoint1Rect_PelMain.Y = (int)GlobalVarLn.YPoint1_PelMain;

            //if ((tbXRect.Text == "") || (tbYRect.Text == "") || (tbPt1XRect.Text == "") ||
            //    (tbPt1YRect.Text == ""))
            //{
            //    MessageBox.Show("Недопустимое значение координат");
            //    return;
            //}
            // ************************************************************** Pel1Pel2

            // !!! Высоты *************************************************************

            // Рельеф
            OwnHeight_comm = (int)GlobalVarLn.HCenter_PelMain; // Pel1
            Point1Height_comm = (int)GlobalVarLn.HPoint1_PelMain; // Pel2
            // ************************************************************* !!! Высоты

            // Ввод параметров ********************************************************
            // !!! Координаты СП уже расчитаны и введены по кнопке СП


            String s1 = "";

            // Максимальная дальность
            //Dmax_PelMain = Convert.ToDouble(tb1.Text);
            s1 = tb1.Text;   
            try
            {
                //if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                Dmax_PelMain = Convert.ToDouble(s1);
            }
            catch (SystemException)
            {
                try
                {
                    if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                    else s1 = s1.Replace('.', ',');
                    Dmax_PelMain = Convert.ToDouble(s1);
                }
                catch
                {
                    if (GlobalVarLn.fl_Azb == 0)
                    {
                        MessageBox.Show("Недопустимое значение максимальной дальности");
                    }
                    else
                    {
                        //Azb?
                        MessageBox.Show("Yanlış maksimum uzaqlığı dəyəri ");
                    }
                    return;
                }
            }
            if ((Dmax_PelMain < 10000) || (Dmax_PelMain > 100000))
            {
                if (GlobalVarLn.fl_Azb == 0)
                {
                    MessageBox.Show("Параметр 'максимальная дальность' выходит за пределы 10000 м - 20000 м");
                }
                else
                {
                    //Azb
                    MessageBox.Show("Yolverilməz parametr göstəriciləri (Uzaqlıq 10000 m - 20000 m)");
                }
                return;
            }

            // Step
            s1 = cbStep.Text; 
            try
            {
                //if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                dD_PelMain = Convert.ToDouble(s1);
            }
            catch (SystemException)
            {
                try
                {
                    if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                    else s1 = s1.Replace('.', ',');
                    dD_PelMain = Convert.ToDouble(s1);
                }
                catch
                {
                    //Azb
                    if (GlobalVarLn.fl_Azb == 0)
                    {
                        MessageBox.Show("Параметр шаг имеет недлпустимое значение");
                    }
                    else
                    {
                        MessageBox.Show("Yolverilməz  parametr göstəriciləri (addımı)");
                    }
                    return;
                }
            }
            GlobalVarLn.dD_PelMain1 = dD_PelMain;
            if ((dD_PelMain < 100) || (dD_PelMain > 500))
            {
                //Azb
                if (GlobalVarLn.fl_Azb == 0)
                {
                    MessageBox.Show("Параметр 'шаг' выходит за пределы 100 м - 500 м");
                }
                else
                {
                    MessageBox.Show("Yolverilməz  parametr göstəriciləri (addımı 100 m-500 m)");

                }
                return;
            }

            // CKO
            //CKO_PelMain = Convert.ToDouble(tb3.Text);
            s1 = tb3.Text;
            try
            {
                //if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                CKO_PelMain = Convert.ToDouble(s1);
            }
            catch (SystemException)
            {
                try
                {
                    if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                    else s1 = s1.Replace('.', ',');
                    CKO_PelMain = Convert.ToDouble(s1);
                }
                catch
                {
                    if (GlobalVarLn.fl_Azb == 0)
                    {
                        MessageBox.Show("Недопустимое значение СКО");
                    }
                    else
                    {
                        //Azb?
                        MessageBox.Show("Yanlış OKS dəyəri ");
                    }

                    return;
                }
            }
            if ((CKO_PelMain < 1) || (CKO_PelMain > 5))
            {
                //Azb
                if (GlobalVarLn.fl_Azb == 0)
                {
                    MessageBox.Show("Параметр 'СКО' выходит за пределы 1 град - 5 град");
                }
                else
                {
                    MessageBox.Show("Yolverilməz parametr göstəriciləri (OKS 1 dərəcə - 5 dərəcə)");
                }
                return;
            }

            // Prozrachnost
            s1 = textBox2.Text;
            try
            {
                //if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                GlobalVarLn.prozrd = (int)Convert.ToDouble(s1);
            }
            catch (SystemException)
            {
                try
                {
                    if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                    else s1 = s1.Replace('.', ',');
                    GlobalVarLn.prozrd = (int)Convert.ToDouble(s1);
                }
                catch
                {
                    if (GlobalVarLn.fl_Azb == 0)
                    {
                        MessageBox.Show("Недопустимое значение прозрачности");
                    }
                    else
                    {
                        //Azb?
                        MessageBox.Show("Yanlış şəffaflıq dəyəri ");
                    }
                    return;
                }

            }
            if ((GlobalVarLn.prozrd < 0) || (GlobalVarLn.prozrd > 100))
            {
                //Azb
                if (GlobalVarLn.fl_Azb == 0)
                {
                    MessageBox.Show("Параметр 'Прозрачность' выходит за пределы 0 - 100 %");
                }
                else
                {
                    MessageBox.Show("Yanlış şəffaflıq dəyəri (0-100%)");
                }
                return;
            }

            GlobalVarLn.prozrd = (GlobalVarLn.prozrd*255) / 100;
            GlobalVarLn.prozr = (int)GlobalVarLn.prozrd;
            // ******************************************************** Ввод параметров

            // Убрать с карты
            //GlobalVarLn.axMapScreenGlobal.Repaint();

            // Расчет зоны ************************************************************
            // Raschet and Draw 

            // Убрать с карты
            //GlobalVarLn.axMapScreenGlobal.Repaint();

            f_ZoneBearing();

            // ************************************************************ Расчет зоны


        } //Zone
        // MAIN MAIN MAIN MAIN MAIN MAIN MAIN MAIN MAIN MAIN MAIN MAIN  Расчет зоны


        // FUNCTIONS_MY  **********************************************************

        // ************************************************************************
        // функция выбора системы координат Pel1, Pel2
        // ************************************************************************

        private void ChooseSystemCoord_PelMain(int iSystemCoord)
        {
            gbOwnRect.Visible = false;
            gbOwnRect42.Visible = false;
            gbOwnRad.Visible = false;
            gbOwnDegMin.Visible = false;
            gbOwnDegMinSec.Visible = false;
            gbPt1Rect.Visible = false;
            gbPt1Rect42.Visible = false;
            gbPt1Rad.Visible = false;
            gbPt1DegMin.Visible = false;
            gbPt1DegMinSec.Visible = false;

            switch (iSystemCoord)
            {
                case 0: // Метры на местности

                    // Pel1
                    gbOwnRect.Visible = true;
                    gbOwnRect.Location = new Point(10, 17);
                    // Pel2
                    gbPt1Rect.Visible = true;
                    gbPt1Rect.Location = new Point(181, 17);

                    // Pel1
                    if (GlobalVarLn.flCoordSP_PelMain == 1)
                    {
                        //333
                        //ichislo = (long)(GlobalVarLn.XCenter_PelMain);
                        //tbXRect.Text = Convert.ToString(ichislo);
                        //ichislo = (long)(GlobalVarLn.YCenter_PelMain);
                        //tbYRect.Text = Convert.ToString(ichislo);

                        ichislo = (long)(GlobalVarLn.BWGS84_1_bear * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        tbXRect.Text = Convert.ToString(dchislo);
                        ichislo = (long)(GlobalVarLn.LWGS84_1_bear * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        tbYRect.Text = Convert.ToString(dchislo);

                    } // IF

                    // Pel2
                    if (GlobalVarLn.flCoordYS1_PelMain == 1)
                    {
                        //333
                        //ichislo = (long)(GlobalVarLn.XPoint1_PelMain);
                        //tbPt1XRect.Text = Convert.ToString(ichislo);
                        //ichislo = (long)(GlobalVarLn.YPoint1_PelMain);
                        //tbPt1YRect.Text = Convert.ToString(ichislo);

                        ichislo = (long)(GlobalVarLn.BWGS84_2_bear * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        tbPt1XRect.Text = Convert.ToString(dchislo);
                        ichislo = (long)(GlobalVarLn.LWGS84_2_bear * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        tbPt1YRect.Text = Convert.ToString(dchislo);

                    }

                    break;

                
                case 1: // Метры 1942 года

                    // Pel1
                    gbOwnRect42.Visible = true;
                    gbOwnRect42.Location = new Point(10, 17);
                    // Pel2
                    gbPt1Rect42.Visible = true;
                    gbPt1Rect42.Location = new Point(181, 17);

                    // Pel1
                    if (GlobalVarLn.flCoordSP_PelMain == 1)
                    {
                        ichislo = (long)(GlobalVarLn.XSP42_br);
                        tbXRect42.Text = Convert.ToString(ichislo);
                        ichislo = (long)(GlobalVarLn.YSP42_br);
                        tbYRect42.Text = Convert.ToString(ichislo);

                    } // IF

                    // Pel2
                    if (GlobalVarLn.flCoordYS1_PelMain == 1)
                    {
                        ichislo = (long)(GlobalVarLn.XYS142_br);
                        tbPt1XRect42.Text = Convert.ToString(ichislo);
                        ichislo = (long)(GlobalVarLn.YYS142_br);
                        tbPt1YRect42.Text = Convert.ToString(ichislo);

                    }

                    break;

                case 2: // Радианы (Красовский)

                    // Pel1
                    gbOwnRad.Visible = true;
                    gbOwnRad.Location = new Point(10, 17);
                    // Pel2
                    gbPt1Rad.Visible = true;
                    gbPt1Rad.Location = new Point(181, 17);

                    // Pel1
                    if (GlobalVarLn.flCoordSP_PelMain == 1)
                    {
                        ichislo = (long)(GlobalVarLn.LatKrR_br * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        tbBRad.Text = Convert.ToString(dchislo);
                        ichislo = (long)(GlobalVarLn.LongKrR_br * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        tbLRad.Text = Convert.ToString(dchislo);


                    } // IF
                    // Pel2
                    if (GlobalVarLn.flCoordYS1_PelMain==1)
                    {
                        ichislo = (long)(GlobalVarLn.LatKrR_YS1_br * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        tbPt1BRad.Text = Convert.ToString(dchislo);
                        ichislo = (long)(GlobalVarLn.LongKrR_YS1_br * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        tbPt1LRad.Text = Convert.ToString(dchislo);

                    }
                    break;

                case 3: // Градусы (Красовский)

                    // Pel1
                    gbOwnDegMin.Visible = true;
                    gbOwnDegMin.Location = new Point(10, 17);
                    // Pel2
                    gbPt1DegMin.Visible = true;
                    gbPt1DegMin.Location = new Point(181, 17);

                    // Pel1
                    if (GlobalVarLn.flCoordSP_PelMain == 1)
                    {
                        ichislo = (long)(GlobalVarLn.LatKrG_br * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        tbBMin1.Text = Convert.ToString(dchislo);
                        ichislo = (long)(GlobalVarLn.LongKrG_br * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        tbLMin1.Text = Convert.ToString(dchislo);


                    } // IF
                    // Pel2
                    if (GlobalVarLn.flCoordYS1_PelMain == 1)
                    {
                        ichislo = (long)(GlobalVarLn.LatKrG_YS1_br * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        tbPt1BMin1.Text = Convert.ToString(dchislo);
                        ichislo = (long)(GlobalVarLn.LongKrG_YS1_br * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        tbPt1LMin1.Text = Convert.ToString(dchislo);

                    }

                    break;

                case 4: // Градусы,мин,сек (Красовский)

                    // Pel1
                    gbOwnDegMinSec.Visible = true;
                    gbOwnDegMinSec.Location = new Point(10, 17);
                    // Pel2
                    gbPt1DegMinSec.Visible = true;
                    gbPt1DegMinSec.Location = new Point(181, 17);

                    // Pel1
                    if (GlobalVarLn.flCoordSP_PelMain == 1)
                    {
                        tbBDeg2.Text = Convert.ToString(GlobalVarLn.Lat_Grad_br);
                        tbBMin2.Text = Convert.ToString(GlobalVarLn.Lat_Min_br);
                        ichislo = (long)(GlobalVarLn.Lat_Sec_br);
                        tbBSec.Text = Convert.ToString(ichislo);
                        tbLDeg2.Text = Convert.ToString(GlobalVarLn.Long_Grad_br);
                        tbLMin2.Text = Convert.ToString(GlobalVarLn.Long_Min_br);
                        ichislo = (long)(GlobalVarLn.Long_Sec_br);
                        tbLSec.Text = Convert.ToString(ichislo);


                    } // IF
                    // Pel2
                    if (GlobalVarLn.flCoordYS1_PelMain == 1)
                    {
                        tbPt1BDeg2.Text = Convert.ToString(GlobalVarLn.Lat_Grad_YS1_br);
                        tbPt1BMin2.Text = Convert.ToString(GlobalVarLn.Lat_Min_YS1_br);
                        ichislo = (long)(GlobalVarLn.Lat_Sec_YS1_br);
                        tbPt1BSec.Text = Convert.ToString(ichislo);
                        tbPt1LDeg2.Text = Convert.ToString(GlobalVarLn.Long_Grad_YS1_br);
                        tbPt1LMin2.Text = Convert.ToString(GlobalVarLn.Long_Min_YS1_br);
                        ichislo = (long)(GlobalVarLn.Long_Sec_YS1_br);
                        tbPt1LSec.Text = Convert.ToString(ichislo);

                    }

                    break;

                default:
                    break;

            } // SWITCH

        } // ChooseSystemCoordSPOP_sup
        // ************************************************************************

        // ***************************************************************************       
        // f_ZoneBearing
        // ***************************************************************************       

        private void f_ZoneBearing()
        {
            double x1 = 0;
            double y1 = 0;
            double x2 = 0;
            double y2 = 0;
            double x3 = 0;
            double y3 = 0;

            double dx = 0;
            double dy = 0;
            double ds = 0;
            double dh = 0;
            double xi = 0;
            double yj = 0;
            double x0 = 0;
            double y0 = 0;
            double l = 0;
            double xmin = 0;
            double ymin = 0;
            double xmax = 0;
            double ymax = 0;
            double dth = 0;
            double rmin1 = 0;
            double rmax1 = 0;


            int i_PelMain = 0;
            int j_PelMain = 0;
            int ni_PelMain = 0;
            int nj_PelMain = 0;
            int ii = 0;
            int intens = 0;
            int color = 0;

            double a = 0;
            double b = 0;

            LF1 objLF1 = new LF1();
            // --------------------------------------------------------------------------

            x1 = GlobalVarLn.XCenter_PelMain;
            y1 = GlobalVarLn.YCenter_PelMain;
            x2 = GlobalVarLn.XPoint1_PelMain;
            y2 = GlobalVarLn.YPoint1_PelMain;
            l = Dmax_PelMain;
            dh = dD_PelMain;
            ds = dD_PelMain;

            dth = (CKO_PelMain * Math.PI) / 180;
            // --------------------------------------------------------------------------
            if (x1 >= x2)
            {
                dx = x1 - x2;
                x0 = x1 - dx / 2;
            }
            else
            {
                dx = x2 - x1;
                x0 = x1 + dx / 2;
            }

            dy = y2 - y1;
            y0 = y1 + dy / 2;
            // -------------------------------------------------------------------------
            xmin = x0 - l;
            xmax = x0 + l;
            ymin = y0 - l;
            ymax = y0 + l;
            // --------------------------------------------------------------------------
            // Число квадратов

            ni_PelMain = (int)(2 * l / ds) + 1;
            nj_PelMain = (int)(2 * l / dh) + 1;
            // --------------------------------------------------------------------------
            GlobalVarLn.list2_PelMain.Clear();
            // --------------------------------------------------------------------------
            // База

            Baza_PelMain = Math.Sqrt(dx * dx + dy * dy);
            tbProzr.Text = Convert.ToString((int)Baza_PelMain);
            // --------------------------------------------------------------------------
            // Rmin,Rmax

            rmin1 = (double)3 / (double)4;
            rmin1 = rmin1 * (Math.Sqrt((double)3 / (double)2));
            rmin1 = rmin1 * Baza_PelMain;
            rmin1 = rmin1 * dth;
            //rmin = (3 / 4) * (Math.Sqrt(3 / 2)) * Baza_PelMain * dth;

            GlobalVarLn.objFormBearingG.tbR1.Text = Convert.ToString((int)rmin1);
            GlobalVarLn.objFormBearingG.tbR2.Text = Convert.ToString((int)(2*rmin1));
            GlobalVarLn.objFormBearingG.tbR3.Text = Convert.ToString((int)(2*rmin1));
            GlobalVarLn.objFormBearingG.tbR4.Text = Convert.ToString((int)(3 * rmin1));
            GlobalVarLn.objFormBearingG.tbR5.Text = Convert.ToString((int)(3 * rmin1));
            GlobalVarLn.objFormBearingG.tbR6.Text = Convert.ToString((int)(4 * rmin1));
            GlobalVarLn.objFormBearingG.tbR7.Text = Convert.ToString((int)(4 * rmin1));
            GlobalVarLn.objFormBearingG.tbR8.Text = Convert.ToString((int)(5 * rmin1));
            GlobalVarLn.objFormBearingG.tbR9.Text = Convert.ToString((int)(5 * rmin1));
            GlobalVarLn.objFormBearingG.tbR10.Text = Convert.ToString((int)(6 * rmin1));
            GlobalVarLn.objFormBearingG.tbR11.Text = Convert.ToString((int)(6 * rmin1));
            GlobalVarLn.objFormBearingG.tbR12.Text = Convert.ToString((int)(7 * rmin1));
            GlobalVarLn.objFormBearingG.tbR13.Text = Convert.ToString((int)(7 * rmin1));
            GlobalVarLn.objFormBearingG.tbR14.Text = Convert.ToString((int)(8 * rmin1));
            GlobalVarLn.objFormBearingG.tbR15.Text = Convert.ToString((int)(8 * rmin1));
            GlobalVarLn.objFormBearingG.tbR16.Text = Convert.ToString((int)(9 * rmin1));
            GlobalVarLn.objFormBearingG.tbR17.Text = Convert.ToString((int)(9 * rmin1));
            GlobalVarLn.objFormBearingG.tbR18.Text = Convert.ToString((int)(10 * rmin1));
            GlobalVarLn.objFormBearingG.tbR19.Text = Convert.ToString((int)(10 * rmin1));
            GlobalVarLn.objFormBearingG.tbR20.Text = Convert.ToString((int)(11 * rmin1));

            // -------------------------------------------------------------------------

            for (ii = 0; ii < 4; ii++)
            {

                if (ii == 0)
                {
                    // 1-й угол+
                    xi = x0 + l;
                    yj = y0 - l;
                    R1_PelMain = Math.Sqrt((xi - x1) * (xi - x1) + (yj - y1) * (yj - y1));
                    R2_PelMain = Math.Sqrt((xi - x2) * (xi - x2) + (yj - y2) * (yj - y2));

                    a = R1_PelMain * R1_PelMain + R2_PelMain * R2_PelMain - Baza_PelMain * Baza_PelMain;
                    b = a / (2 * R1_PelMain * R2_PelMain);
                    Gamma_PelMain = Math.Acos((R1_PelMain * R1_PelMain + R2_PelMain * R2_PelMain - Baza_PelMain * Baza_PelMain) / (2 * R1_PelMain * R2_PelMain));
                    R_PelMain = (Math.Sqrt(R1_PelMain * R1_PelMain + R2_PelMain * R2_PelMain) * dth) / Math.Sin(Gamma_PelMain);
                    rmax1 = R_PelMain;
                }
                else if (ii == 1)
                {
                    // 2-й угол
                    xi = x0 + l;
                    yj = y0 + l;
                    R1_PelMain = Math.Sqrt((xi - x1) * (xi - x1) + (yj - y1) * (yj - y1));
                    R2_PelMain = Math.Sqrt((xi - x2) * (xi - x2) + (yj - y2) * (yj - y2));
                    Gamma_PelMain = Math.Acos((R1_PelMain * R1_PelMain + R2_PelMain * R2_PelMain - Baza_PelMain * Baza_PelMain) / (2 * R1_PelMain * R2_PelMain));
                    R_PelMain = (Math.Sqrt(R1_PelMain * R1_PelMain + R2_PelMain * R2_PelMain) * dth) / Math.Sin(Gamma_PelMain);
                    if (R_PelMain > rmax1)
                        rmax1 = R_PelMain;
                }
                else if (ii == 2)
                {
                    // 3-й угол
                    xi = x0 - l;
                    yj = y0 + l;
                    R1_PelMain = Math.Sqrt((xi - x1) * (xi - x1) + (yj - y1) * (yj - y1));
                    R2_PelMain = Math.Sqrt((xi - x2) * (xi - x2) + (yj - y2) * (yj - y2));
                    Gamma_PelMain = Math.Acos((R1_PelMain * R1_PelMain + R2_PelMain * R2_PelMain - Baza_PelMain * Baza_PelMain) / (2 * R1_PelMain * R2_PelMain));
                    R_PelMain = (Math.Sqrt(R1_PelMain * R1_PelMain + R2_PelMain * R2_PelMain) * dth) / Math.Sin(Gamma_PelMain);
                    if (R_PelMain > rmax1)
                        rmax1 = R_PelMain;
                }
                else
                {
                    // 4-й угол
                    xi = x0 - l;
                    yj = y0 - l;
                    R1_PelMain = Math.Sqrt((xi - x1) * (xi - x1) + (yj - y1) * (yj - y1));
                    R2_PelMain = Math.Sqrt((xi - x2) * (xi - x2) + (yj - y2) * (yj - y2));
                    Gamma_PelMain = Math.Acos((R1_PelMain * R1_PelMain + R2_PelMain * R2_PelMain - Baza_PelMain * Baza_PelMain) / (2 * R1_PelMain * R2_PelMain));
                    R_PelMain = (Math.Sqrt(R1_PelMain * R1_PelMain + R2_PelMain * R2_PelMain) * dth) / Math.Sin(Gamma_PelMain);
                    if (R_PelMain > rmax1)
                        rmax1 = R_PelMain;
                }
            }
            // -------------------------------------------------------------------------

            xi = xmax - dh / 2;

            // FOR1FOR1FOR1FOR1FOR1FOR1FOR1FOR1FOR1FOR1FOR1FOR1FOR1FOR1FOR1FOR1FOR1FOR1FOR1
            for (i_PelMain = 0; i_PelMain < ni_PelMain; i_PelMain++)
            {

                yj = ymin + ds / 2;

                // FOR2FOR2FOR2FOR2FOR2FOR2FOR2FOR2FOR2FOR2FOR2FOR2FOR2FOR2FOR2FOR2FOR2
                for (j_PelMain = 0; j_PelMain < nj_PelMain; j_PelMain++)
                {

                    R1_PelMain = Math.Sqrt((xi - x1) * (xi - x1) + (yj - y1) * (yj - y1));
                    R2_PelMain = Math.Sqrt((xi - x2) * (xi - x2) + (yj - y2) * (yj - y2));

                    Gamma_PelMain = Math.Acos((R1_PelMain * R1_PelMain + R2_PelMain * R2_PelMain - Baza_PelMain * Baza_PelMain) / (2 * R1_PelMain * R2_PelMain));

                    R_PelMain = (Math.Sqrt(R1_PelMain * R1_PelMain + R2_PelMain * R2_PelMain) * dth) / Math.Sin(Gamma_PelMain);

                    intens = (int)(100 * ((R_PelMain - rmin1) / (rmax1 - rmin1)));

                    objLF1.X_m = xi;
                    objLF1.Y_m = yj;
                    objLF1.R = R_PelMain;
                    objLF1.Intensity = intens;
                    objLF1.Rmin = rmin1;
                    objLF1.Gamma = Gamma_PelMain;
                    objLF1.ds = ds;
                    objLF1.dh = dh;

                    // 1610
                    if(
                        (R_PelMain>=rmin1)&&
                        (R_PelMain<=(11*rmin1))&&
                        (Gamma_PelMain>=0.1)&&
                        (Gamma_PelMain <= 3.1)

                      )
                    {
                      GlobalVarLn.list2_PelMain.Add(objLF1);
                    }

                    //ClassMap.f_Map_Bearing(xi, yj, ds, dh, R_PelMain, rmin1, intens, Gamma_PelMain);


                    yj = yj + ds;
                } // FOR2(Y)
                // FOR2FOR2FOR2FOR2FOR2FOR2FOR2FOR2FOR2FOR2FOR2FOR2FOR2FOR2FOR2FOR2FOR2

                xi = xi - dh;

            } // FOR1(X)            
            // FOR1FOR1FOR1FOR1FOR1FOR1FOR1FOR1FOR1FOR1FOR1FOR1FOR1FOR1FOR1FOR1FOR1FOR1FOR1


            // ZONE *************************************************************

/*
            if (GlobalVarLn.axMapScreenGlobal.ViewScale == 500000)
            {
                GlobalVarLn.prozr1_bear = GlobalVarLn.prozr;
            }
            else if ((GlobalVarLn.axMapScreenGlobal.ViewScale > 500000) && (GlobalVarLn.axMapScreenGlobal.ViewScale <= 1000000))
            {
                if (GlobalVarLn.dD_PelMain1 == 100)
                    //GlobalVarLn.prozr1_bear = 100;
                    GlobalVarLn.prozr1_bear = GlobalVarLn.prozr-130;
                else if (GlobalVarLn.dD_PelMain1 == 200)
                    //GlobalVarLn.prozr1_bear = 200;
                    GlobalVarLn.prozr1_bear = GlobalVarLn.prozr-30;
               else if (GlobalVarLn.dD_PelMain1 == 300)
                    //GlobalVarLn.prozr1_bear = 200;
                    GlobalVarLn.prozr1_bear = GlobalVarLn.prozr-30;
                else if (GlobalVarLn.dD_PelMain1 == 400)
                    //GlobalVarLn.prozr1_bear = 200;
                    GlobalVarLn.prozr1_bear = GlobalVarLn.prozr-30;
                else if (GlobalVarLn.dD_PelMain1 == 500)
                    //GlobalVarLn.prozr1_bear = 200;
                    GlobalVarLn.prozr1_bear = GlobalVarLn.prozr - 30;

                if (GlobalVarLn.prozr1_bear <= 30)
                    GlobalVarLn.prozr1_bear = 30;

            }
            else if ((GlobalVarLn.axMapScreenGlobal.ViewScale > 1000000) && (GlobalVarLn.axMapScreenGlobal.ViewScale <= 2000000))
            //prozr1 = 100;
            {

                if (GlobalVarLn.dD_PelMain1 == 100)
                    //GlobalVarLn.prozr1_bear = 100;
                    GlobalVarLn.prozr1_bear = GlobalVarLn.prozr - 130;
                else if (GlobalVarLn.dD_PelMain1 == 200)
                    //GlobalVarLn.prozr1_bear = 100;
                    GlobalVarLn.prozr1_bear = GlobalVarLn.prozr - 130;
                else if (GlobalVarLn.dD_PelMain1 == 300)
                    //GlobalVarLn.prozr1_bear = 200;
                    GlobalVarLn.prozr1_bear = GlobalVarLn.prozr - 30;
                else if (GlobalVarLn.dD_PelMain1 == 400)
                    //GlobalVarLn.prozr1_bear = 200;
                    GlobalVarLn.prozr1_bear = GlobalVarLn.prozr - 30;
                else if (GlobalVarLn.dD_PelMain1 == 500)
                    //GlobalVarLn.prozr1_bear = 200;
                    GlobalVarLn.prozr1_bear = GlobalVarLn.prozr - 30;
                if (GlobalVarLn.prozr1_bear <= 30)
                    GlobalVarLn.prozr1_bear = 30;


            }
            else if ((GlobalVarLn.axMapScreenGlobal.ViewScale > 2000000) && (GlobalVarLn.axMapScreenGlobal.ViewScale <= 3000000))
            //prozr1 = 100;
            {

                if (GlobalVarLn.dD_PelMain1 == 100)
                    //GlobalVarLn.prozr1_bear = 100;
                    GlobalVarLn.prozr1_bear = GlobalVarLn.prozr - 130;
                else if (GlobalVarLn.dD_PelMain1 == 200)
                    //GlobalVarLn.prozr1_bear = 100;
                    GlobalVarLn.prozr1_bear = GlobalVarLn.prozr - 130;
                else if (GlobalVarLn.dD_PelMain1 == 300)
                    //GlobalVarLn.prozr1_bear = 200;
                    GlobalVarLn.prozr1_bear = GlobalVarLn.prozr - 30;
                else if (GlobalVarLn.dD_PelMain1 == 400)
                    //GlobalVarLn.prozr1_bear = 200;
                    GlobalVarLn.prozr1_bear = GlobalVarLn.prozr - 30;
                else if (GlobalVarLn.dD_PelMain1 == 500)
                    //GlobalVarLn.prozr1_bear = 200;
                    GlobalVarLn.prozr1_bear = GlobalVarLn.prozr - 30;
                if (GlobalVarLn.prozr1_bear <= 30)
                    GlobalVarLn.prozr1_bear = 30;


            }

            else if ((GlobalVarLn.axMapScreenGlobal.ViewScale > 3000000) && (GlobalVarLn.axMapScreenGlobal.ViewScale <= 4000000))
            //prozr1 = 100;
            {

                if (GlobalVarLn.dD_PelMain1 == 100)
                    //GlobalVarLn.prozr1_bear = 100;
                    GlobalVarLn.prozr1_bear = GlobalVarLn.prozr - 130;
                else if (GlobalVarLn.dD_PelMain1 == 200)
                    //GlobalVarLn.prozr1_bear = 100;
                    GlobalVarLn.prozr1_bear = GlobalVarLn.prozr - 130;
                else if (GlobalVarLn.dD_PelMain1 == 300)
                    //GlobalVarLn.prozr1_bear = 200;
                    GlobalVarLn.prozr1_bear = GlobalVarLn.prozr - 30;
                else if (GlobalVarLn.dD_PelMain1 == 400)
                    //GlobalVarLn.prozr1_bear = 200;
                    GlobalVarLn.prozr1_bear = GlobalVarLn.prozr - 30;
                else if (GlobalVarLn.dD_PelMain1 == 500)
                    //GlobalVarLn.prozr1_bear = 200;
                    GlobalVarLn.prozr1_bear = GlobalVarLn.prozr - 30;
                if (GlobalVarLn.prozr1_bear <= 30)
                    GlobalVarLn.prozr1_bear = 30;

            }

            // .................................................................
            else if ((GlobalVarLn.axMapScreenGlobal.ViewScale < 500000) && (GlobalVarLn.axMapScreenGlobal.ViewScale >= 250000))
            //prozr1 = 240;
            {

                if (GlobalVarLn.dD_PelMain1 == 100)
                    //GlobalVarLn.prozr1_bear = 240;
                    GlobalVarLn.prozr1_bear = GlobalVarLn.prozr + 10;
                else if (GlobalVarLn.dD_PelMain1 == 200)
                    //GlobalVarLn.prozr1_bear = 240;
                    GlobalVarLn.prozr1_bear = GlobalVarLn.prozr + 10;
                else if (GlobalVarLn.dD_PelMain1 == 300)
                    //GlobalVarLn.prozr1_bear = 240;
                    GlobalVarLn.prozr1_bear = GlobalVarLn.prozr + 10;
                else if (GlobalVarLn.dD_PelMain1 == 400)
                    //GlobalVarLn.prozr1_bear = 240;
                    GlobalVarLn.prozr1_bear = GlobalVarLn.prozr + 10;
                else if (GlobalVarLn.dD_PelMain1 == 500)
                    //GlobalVarLn.prozr1_bear = 240;
                    GlobalVarLn.prozr1_bear = GlobalVarLn.prozr + 10;
                if (GlobalVarLn.prozr1_bear >= 255)
                    GlobalVarLn.prozr1_bear = 255;

            }
            else if ((GlobalVarLn.axMapScreenGlobal.ViewScale < 250000) && (GlobalVarLn.axMapScreenGlobal.ViewScale >= 125000))
            //prozr1 = 235;
            {

                if (GlobalVarLn.dD_PelMain1 == 100)
                    //GlobalVarLn.prozr1_bear = 235;
                    GlobalVarLn.prozr1_bear = GlobalVarLn.prozr + 5;
                else if (GlobalVarLn.dD_PelMain1 == 200)
                    //GlobalVarLn.prozr1_bear = 235;
                    GlobalVarLn.prozr1_bear = GlobalVarLn.prozr + 5;
                else if (GlobalVarLn.dD_PelMain1 == 300)
                    //GlobalVarLn.prozr1_bear = 235;
                    GlobalVarLn.prozr1_bear = GlobalVarLn.prozr + 5;
                else if (GlobalVarLn.dD_PelMain1 == 400)
                    //GlobalVarLn.prozr1_bear = 235;
                    GlobalVarLn.prozr1_bear = GlobalVarLn.prozr + 5;
                else if (GlobalVarLn.dD_PelMain1 == 500)
                    //GlobalVarLn.prozr1_bear = 235;
                    GlobalVarLn.prozr1_bear = GlobalVarLn.prozr + 5;
                if (GlobalVarLn.prozr1_bear >= 255)
                    GlobalVarLn.prozr1_bear = 255;

            }
            else if ((GlobalVarLn.axMapScreenGlobal.ViewScale < 125000) && (GlobalVarLn.axMapScreenGlobal.ViewScale >= 62500))
            //prozr1 = 230;
            {

                if (GlobalVarLn.dD_PelMain1 == 100)
                    //GlobalVarLn.prozr1_bear = 230;
                    GlobalVarLn.prozr1_bear = GlobalVarLn.prozr;
                else if (GlobalVarLn.dD_PelMain1 == 200)
                    //GlobalVarLn.prozr1_bear = 230;
                    GlobalVarLn.prozr1_bear = GlobalVarLn.prozr;
                else if (GlobalVarLn.dD_PelMain1 == 300)
                    //GlobalVarLn.prozr1_bear = 230;
                    GlobalVarLn.prozr1_bear = GlobalVarLn.prozr;
                else if (GlobalVarLn.dD_PelMain1 == 400)
                    //GlobalVarLn.prozr1_bear = 230;
                    GlobalVarLn.prozr1_bear = GlobalVarLn.prozr;
                else if (GlobalVarLn.dD_PelMain1 == 500)
                    //GlobalVarLn.prozr1_bear = 230;
                    GlobalVarLn.prozr1_bear = GlobalVarLn.prozr;
                if (GlobalVarLn.prozr1_bear >= 255)
                    GlobalVarLn.prozr1_bear = 255;

            }
            else if ((GlobalVarLn.axMapScreenGlobal.ViewScale < 60000) && (GlobalVarLn.axMapScreenGlobal.ViewScale >= 31250))
            //prozr1 = 230;
            {
                if (GlobalVarLn.dD_PelMain1 == 100)
                    //GlobalVarLn.prozr1_bear = 230;
                    GlobalVarLn.prozr1_bear = GlobalVarLn.prozr;
                else if (GlobalVarLn.dD_PelMain1 == 200)
                    //GlobalVarLn.prozr1_bear = 230;
                    GlobalVarLn.prozr1_bear = GlobalVarLn.prozr;
                else if (GlobalVarLn.dD_PelMain1 == 300)
                    //GlobalVarLn.prozr1_bear = 230;
                    GlobalVarLn.prozr1_bear = GlobalVarLn.prozr;
                else if (GlobalVarLn.dD_PelMain1 == 400)
                    //GlobalVarLn.prozr1_bear = 230;
                    GlobalVarLn.prozr1_bear = GlobalVarLn.prozr;
                else if (GlobalVarLn.dD_PelMain1 == 500)
                    //GlobalVarLn.prozr1_bear = 230;
                    GlobalVarLn.prozr1_bear = GlobalVarLn.prozr;
                if (GlobalVarLn.prozr1_bear >= 255)
                    GlobalVarLn.prozr1_bear = 255;
            }
            else if ((GlobalVarLn.axMapScreenGlobal.ViewScale < 30000) && (GlobalVarLn.axMapScreenGlobal.ViewScale >= 15625))
            //prozr1 = 230;
            {
                if (GlobalVarLn.dD_PelMain1 == 100)
                    //GlobalVarLn.prozr1_bear = 230;
                    GlobalVarLn.prozr1_bear = GlobalVarLn.prozr;
                else if (GlobalVarLn.dD_PelMain1 == 200)
                    //GlobalVarLn.prozr1_bear = 230;
                    GlobalVarLn.prozr1_bear = GlobalVarLn.prozr;
                else if (GlobalVarLn.dD_PelMain1 == 300)
                    //GlobalVarLn.prozr1_bear = 230;
                    GlobalVarLn.prozr1_bear = GlobalVarLn.prozr;
                else if (GlobalVarLn.dD_PelMain1 == 400)
                    //GlobalVarLn.prozr1_bear = 230;
                    GlobalVarLn.prozr1_bear = GlobalVarLn.prozr;
                else if (GlobalVarLn.dD_PelMain1 == 500)
                    //GlobalVarLn.prozr1_bear = 230;
                    GlobalVarLn.prozr1_bear = GlobalVarLn.prozr;
                if (GlobalVarLn.prozr1_bear >= 255)
                    GlobalVarLn.prozr1_bear = 255;
            }
            else if ((GlobalVarLn.axMapScreenGlobal.ViewScale < 15625) && (GlobalVarLn.axMapScreenGlobal.ViewScale >= 10000))
            //prozr1 = 230;
            {
                if (GlobalVarLn.dD_PelMain1 == 100)
                    //GlobalVarLn.prozr1_bear = 230;
                    GlobalVarLn.prozr1_bear = GlobalVarLn.prozr;
                else if (GlobalVarLn.dD_PelMain1 == 200)
                    //GlobalVarLn.prozr1_bear = 230;
                    GlobalVarLn.prozr1_bear = GlobalVarLn.prozr;
                else if (GlobalVarLn.dD_PelMain1 == 300)
                    //GlobalVarLn.prozr1_bear = 230;
                    GlobalVarLn.prozr1_bear = GlobalVarLn.prozr;
                else if (GlobalVarLn.dD_PelMain1 == 400)
                    //GlobalVarLn.prozr1_bear = 230;
                    GlobalVarLn.prozr1_bear = GlobalVarLn.prozr;
                else if (GlobalVarLn.dD_PelMain1 == 500)
                    //GlobalVarLn.prozr1_bear = 230;
                    GlobalVarLn.prozr1_bear = GlobalVarLn.prozr;
                if (GlobalVarLn.prozr1_bear >= 255)
                    GlobalVarLn.prozr1_bear = 255;
            }
            else
                GlobalVarLn.prozr1_bear = GlobalVarLn.prozr;
            // -----------------------------------------------------------------
            // Rmin...2Rmin
              GlobalVarLn.brush1_1bear = new SolidBrush(Color.FromArgb(GlobalVarLn.prozr1_bear, 0, GlobalVarLn.grn1, 0)); // ближняя

            // 2Rmin...3Rmin
              GlobalVarLn.brush1_2bear = new SolidBrush(Color.FromArgb(GlobalVarLn.prozr1_bear, 0, GlobalVarLn.grn2, 0)); 

            // 3Rmin...4Rmin
              GlobalVarLn.brush1_3bear = new SolidBrush(Color.FromArgb(GlobalVarLn.prozr1_bear, 0, GlobalVarLn.grn3, 0)); 

            // 4Rmin...5Rmin
              GlobalVarLn.brush1_4bear = new SolidBrush(Color.FromArgb(GlobalVarLn.prozr1_bear, 0, GlobalVarLn.grn4, 0)); 

            // 5Rmin...6Rmin
              GlobalVarLn.brush1_5bear = new SolidBrush(Color.FromArgb(GlobalVarLn.prozr1_bear, 0, GlobalVarLn.grn5, 0)); 

            // 6Rmin...7Rmin
              GlobalVarLn.brush1_6bear = new SolidBrush(Color.FromArgb(GlobalVarLn.prozr1_bear, GlobalVarLn.rd1, 0, 0)); 

            // 7Rmin...8Rmin
              GlobalVarLn.brush1_7bear = new SolidBrush(Color.FromArgb(GlobalVarLn.prozr1_bear, GlobalVarLn.rd2, 0, 0)); 

            // 8Rmin...9Rmin
              GlobalVarLn.brush1_8bear = new SolidBrush(Color.FromArgb(GlobalVarLn.prozr1_bear, GlobalVarLn.rd3, 0, 0)); 

            // 9Rmin...10Rmin
              GlobalVarLn.brush1_9bear = new SolidBrush(Color.FromArgb(GlobalVarLn.prozr1_bear, GlobalVarLn.rd4, 0, 0)); 

            // 10Rmin...11Rmin
              GlobalVarLn.brush1_10bear = new SolidBrush(Color.FromArgb(GlobalVarLn.prozr1_bear, GlobalVarLn.rd5, 0, 0)); 

            // -----------------------------------------------------------------

*/
            // ************************************************************* ZONE

            //2106
            int fff = 0;
            if (GlobalVarLn.flsect == 1)
            {
                fff = 1;
                GlobalVarLn.flsect = 0;
            }
            // Перерисовка
            GlobalVarLn.axMapScreenGlobal.Repaint();

            //ClassMap.f_ReDraw_Map_Bearing1();

            //2106
            //Sect
            if (fff == 1)
            {
                GlobalVarLn.flsect = 1;
                ClassMap.Sector((double)GlobalVarLn.XCenter_Sost, (double)GlobalVarLn.YCenter_Sost);
            }


        } // f_ZoneBearing
        // ***************************************************************************       


        //  ********************************************************** FUNCTIONS_MY


        // ***********************************************************************
        // Closing, Activated
        // ***********************************************************************
        private void FormBearing_FormClosing(object sender, FormClosingEventArgs e)
        {
            GlobalVarLn.f_Open_objFormBearing = 0;

            e.Cancel = true;
            Hide();
        } // Closing

        private void FormBearing_Activated(object sender, EventArgs e)
        {
            GlobalVarLn.f_Open_objFormBearing = 1;

        }

        private void lPt1Height_Click(object sender, EventArgs e)
        {

        }

        private void lChooseSC_Click(object sender, EventArgs e)
        {

        }

        private void tbR2_TextChanged(object sender, EventArgs e)
        {

        } // Activated
        // ***********************************************************************


        private void LanguageChooser()
        {
            var cont = this.Controls;

            if (NumberOfLanguage.Equals(0))
            {
                ChangeLanguageToRu(cont);
                this.Text = "Зона ошибок местоопределения";
            }
            if (NumberOfLanguage.Equals(1))
            {
                //ChangeLanguageToEng(cont);
            }
            if (NumberOfLanguage.Equals(2))
            {
                ChangeLanguageToAzer(cont);
                this.Text = "Yerin müəyyənləşdirilməsində səhv sahəsi";
            }
        }

        private void ChangeLanguageToRu(System.Windows.Forms.Control.ControlCollection cont)
        {
            string lang = "ru-RU";
            foreach (System.Windows.Forms.Control cc in cont)
            {
                if (cc is ComboBox)
                {

                    for (int i = 0; i < (cc as ComboBox).Items.Count; i++)
                    {
                        if (i == 0)
                        {
                            string a = resources.GetString(cc.Name + ".Items", new CultureInfo(lang));
                            (cc as ComboBox).Items[i] = a;
                        }
                        else
                        {
                            string a = resources.GetString(cc.Name + ".Items" + i.ToString(), new CultureInfo(lang));
                            (cc as ComboBox).Items[i] = a;
                        }
                    }
                }
                //}
                if (cc is Label || cc is Button || cc is RadioButton || cc is TabControl || cc is TabPage || cc is TextBox)
                {
                    resources.ApplyResources(cc, cc.Name, new CultureInfo(lang));
                    //toolTip.SetToolTip(cc, resources.GetString(cc.Name + ".ToolTip", new CultureInfo(lang)));
                }
                if (cc is Panel || cc is GroupBox || cc is ListBox || cc is TabControl || cc is TabPage)
                {
                    ChangeLanguageToRu(cc.Controls);
                }
            }
        }

        private void ChangeLanguageToAzer(System.Windows.Forms.Control.ControlCollection cont)
        {
            string lang = "az-Latn";

            foreach (System.Windows.Forms.Control cc in cont)
            {
                if (cc is ComboBox)
                {

                    for (int i = 0; i < (cc as ComboBox).Items.Count; i++)
                    {
                        if (i == 0)
                        {
                            string a = resources.GetString(cc.Name + ".Items", new CultureInfo(lang));
                            (cc as ComboBox).Items[i] = a;
                        }
                        else
                        {
                            string a = resources.GetString(cc.Name + ".Items" + i.ToString(), new CultureInfo(lang));
                            (cc as ComboBox).Items[i] = a;
                        }
                    }
                }
                //}
                if (cc is Label || cc is Button || cc is RadioButton || cc is TabControl || cc is TabPage || cc is TextBox)
                {
                    resources.ApplyResources(cc, cc.Name, new CultureInfo(lang));
                    //toolTip.SetToolTip(cc, resources.GetString(cc.Name + ".ToolTip", new CultureInfo(lang)));
                }
                if (cc is Panel || cc is GroupBox || cc is ListBox || cc is TabControl || cc is DataGridView || cc is TabPage)
                {
                    ChangeLanguageToAzer(cc.Controls);
                }

            }
        }



    } // Class
} // Namespace
