﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AxaxGisToolKit;
using axGisToolKit;
using System.Runtime.InteropServices;
using System.Windows.Forms;

using System.IO;
using System.Reflection;
using System.Globalization;

namespace GrozaMap
{
    public partial class FormWay : Form
    {

        [DllImport("gisacces.dll")]
        public static extern int mapPlaneToGeoWGS84(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        public static extern int mapPlaneToPicture(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        public static extern int mapPictureToPlane(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        public static extern int mapPlaneToGeo(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        static extern int mapGeoToPlane(int hmap, ref double Bx, ref double Ly);

        private AxaxcMapScreen axaxcMapScreen;

        // Переменные VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR 
        public string flname_W;
        public string flname_R;

        public double dchisloW;
        public long ichisloW;
        public double X_Coordl5;
        public double Y_Coordl5;

        public double DXX_W;
        public double DYY_W;

        // !!! Флаг окончаня маршрута (устанавливается по кнопке ЗАГРУЗИТЬ)
        public static uint flEndWay = 0;
        //Координаты 
        public static double[] mas_LW = new double[10000];
        public static double[] mas_XW = new double[10000];
        public static double[] mas_YW = new double[10000];

        public static uint iW=0;
        public static double LW=0;
        public static double X_StartW=0;
        public static double Y_StartW=0;
        public static double X1_W=0;
        public static double Y1_W=0;
        public static double X2_W=0;
        public static double Y2_W=0;

        // ......................................................................

        // VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR Переменные

        // Конструктор *********************************************************** 
        ComponentResourceManager resources = new ComponentResourceManager(typeof(FormWay));
        private int NumberOfLanguage;



        public FormWay(ref AxaxcMapScreen axaxcMapScreen1)
        {
            InitializeComponent();

           
            axaxcMapScreen = axaxcMapScreen1;

            //iW = 0;

            dchisloW=0;
            ichisloW=0;
            X_Coordl5 = 0;
            Y_Coordl5 = 0;

            DXX_W=0;
            DYY_W=0;
            // ......................................................................


        } // Конструктор
        // ***********************************************************  Конструктор


        // *************************************************************************************
        // Загрузка формы
        // *************************************************************************************

        private void FormWay_Load(object sender, EventArgs e)
        {
            string strExePath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

            string PathFolder1 = Path.GetDirectoryName(strExePath);

            string strPathLatest = PathFolder1 + "\\INI\\Common.ini";
            foreach (string line in File.ReadLines(strPathLatest))
            {
                if (line.Contains("Language "))
                {
                    string LanguageAsNumber = line.Split(new string[] { "=" }, StringSplitOptions.None).Last();
                    NumberOfLanguage = Int32.Parse(LanguageAsNumber);
                    break;
                }
            }
            LanguageChooser();

            // SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
            // Для маршрута
            // 1-я загрузка формы

            chbWay.Checked = true;
            // .....................................................................................
            // Очистка dataGridView

            dataGridView1.ClearSelection();
            for (int i = 0; i < GlobalVarLn.sizeDatWay_stat; i++)
            {
                dataGridView1.Rows.Add("", "", "");
            }
            // .....................................................................................
            if (chbWay.Checked == true)
            {
                GlobalVarLn.blWay_stat = true;
                GlobalVarLn.flEndWay_stat = 1;
            }

            // way
            //Array.Clear(GlobalVarLn.mas_LW_stat, 0, 10000);
            //Array.Clear(GlobalVarLn.mas_XW_stat, 0, 10000);
            //Array.Clear(GlobalVarLn.mas_YW_stat, 0, 10000);
            GlobalVarLn.list_way.Clear();

            GlobalVarLn.iW_stat = 0;
            GlobalVarLn.X_StartW_stat = 0;
            GlobalVarLn.Y_StartW_stat = 0;
            GlobalVarLn.LW_stat = 0;


            if (GlobalVarLn.fl_Azb == 0)
            {

                dataGridView1.Columns[0].HeaderText = "X, м";
                dataGridView1.Columns[1].HeaderText = "Y, м";
                dataGridView1.Columns[2].HeaderText = "L, м";
            }
            else
            {

                dataGridView1.Columns[0].HeaderText = "X, m";
                dataGridView1.Columns[1].HeaderText = "Y, m";
                dataGridView1.Columns[2].HeaderText = "L, m";
            }


            // .....................................................................................

            // SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS

        } // Load
        // *************************************************************************************

        // *****************************************************************************************
        // Обработчик кнопки Button1: Начало обработки маршрута маршрута
        // *****************************************************************************************

        private void button1_Click(object sender, EventArgs e)
        {
/*
            // ......................................................................
            //MessageBox.Show("Выберите пункты с помощью левой кнопки  мыши и нажмите кнопку ЗАГРУЗИТЬ");
            // ......................................................................
            // Очистка

            if (GlobalVarLn.blWay_stat == false)
            {
                flEndWay = 1;
                GlobalVarLn.blWay_stat = true;

                Array.Clear(mas_LW, 0, 10000);
                Array.Clear(mas_XW, 0, 10000);
                Array.Clear(mas_YW, 0, 10000);

                iW = 0;
                X_StartW = 0;
                Y_StartW = 0;
                LW = 0;
            }
            else
            {
                MessageBox.Show("Маршрут уже запущен");
            }
            // ......................................................................
*/
            ;
        } // Button1
        // *****************************************************************************************

        // *****************************************************************************************
        // Обработчик кнопки Button2:ЗАГРУЗИТЬ (Конец обработки маршрута маршрута)
        // *****************************************************************************************

        private void button2_Click(object sender, EventArgs e)
        {
/*
            // !!! Флаг окончаня маршрута (устанавливается по кнопке ЗАГРУЗИТЬ)
            flEndWay = 0;

            ichisloW = (long)((LW / 1000) * 10000000);
            dchisloW = ((double)ichisloW) / 10000000;
            textBox2.Text = Convert.ToString(dchisloW);   // L

            textBox1.Text = Convert.ToString(iW);         // N
*/
            ;
        } // Button2
        // *****************************************************************************************

        // *****************************************************************************************
        // Обработчик кнопки Button3: очистить
        // *****************************************************************************************

        private void button3_Click(object sender, EventArgs e)
        {
            // .....................................................................................
            // Очистка dataGridView

            while (dataGridView1.Rows.Count != 0)
                dataGridView1.Rows.Remove(dataGridView1.Rows[dataGridView1.Rows.Count - 1]);

            dataGridView1.ClearSelection();
            for (int i = 0; i < GlobalVarLn.sizeDatWay_stat; i++)
            {
                dataGridView1.Rows.Add("", "", "");
            }
            // .....................................................................................
            if (chbWay.Checked == false)
            {
                GlobalVarLn.blWay_stat = false;
                GlobalVarLn.flEndWay_stat = 0;
            }
            else
            {
                GlobalVarLn.blWay_stat = true;
                GlobalVarLn.flEndWay_stat = 1;
            }

            // way
            //Array.Clear(GlobalVarLn.mas_LW_stat, 0, 10000);
            //Array.Clear(GlobalVarLn.mas_XW_stat, 0, 10000);
            //Array.Clear(GlobalVarLn.mas_YW_stat, 0, 10000);
            GlobalVarLn.list_way.Clear();

            GlobalVarLn.iW_stat = 0;
            GlobalVarLn.X_StartW_stat = 0;
            GlobalVarLn.Y_StartW_stat = 0;
            GlobalVarLn.LW_stat = 0;
            // .....................................................................................
            textBox2.Text = "";   // L
            textBox1.Text = "";   // N
            // .....................................................................................
            // Убрать с карты

            GlobalVarLn.axMapScreenGlobal.Repaint();
            // .....................................................................................

        } // Button3
        // *****************************************************************************************

        // *****************************************************************************************
        // Обработчик кнопки Button4: сохранить
        // *****************************************************************************************
        //777

        private void button4_Click(object sender, EventArgs e)
        {
            int i_tmp=0;


            if (
                (saveFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK) &&
                (saveFileDialog1.FileName.Length > 0)
                )

            {
                // -----------------------------------------------------------------------------------------

                GlobalVarLn.flname_W_stat = saveFileDialog1.FileName;

                if (GlobalVarLn.flname_W_stat != "")
                {
                    StreamWriter srFile = new StreamWriter(GlobalVarLn.flname_W_stat);

                    //srFile.WriteLine("N =" + Convert.ToString(GlobalVarLn.iW_stat));
                    srFile.WriteLine("N =" + Convert.ToString(GlobalVarLn.list_way.Count - 1));

                    //for (i_tmp = 1; i_tmp <= GlobalVarLn.iW_stat; i_tmp++)
                    for (i_tmp = 1; i_tmp <= (GlobalVarLn.list_way.Count - 1); i_tmp++)
                    {
                        srFile.WriteLine("X" + Convert.ToString(i_tmp) + " =" + Convert.ToString((int)GlobalVarLn.list_way[i_tmp].X_m));
                        srFile.WriteLine("Y" + Convert.ToString(i_tmp) + " =" + Convert.ToString((int)GlobalVarLn.list_way[i_tmp].Y_m));
                    }

                    srFile.Close();

                }  // filename!=""
                // -----------------------------------------------------------------------------------------
            }  // OK


        } // Button4
        // *****************************************************************************************

        // *****************************************************************************************
        // Обработчик кнопки Button5: чтение
        // *****************************************************************************************
        //777

        private void button5_Click(object sender, EventArgs e)
        {
            // ......................................................................
            String strLine = "";

            String strLine1 = "";
            //String strLine2 = "";

            double number1=0;
            int number2=0;

            char symb1 = 'N';
            char symb2 = 'X';
            char symb3 = 'Y';
            char symb4 = '=';

            int indStart=0;
            int indStop=0;
            int iLength=0;

            int IndZap=0;
            int TekPoz=0;

            double x1 = 0;
            double y1 = 0;
            double x2 = 0;
            double y2 = 0;
            double dx = 0;
            double dy = 0;
            double li = 0;
            int fi=0;


            //if (
            //   openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.Cancel
            //   )
            //    return;
            //if (
            //   openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.No
            //   )
            //    return;
            //if (
            //   openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.None
            //   )
            //    return;
            //if (
            //   openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.Abort
            //   )
            //    return;
            //if (
            //   openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.Retry
            //   )
            //    return;



            if (
                (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK) &&
                (openFileDialog1.FileName.Length > 0)
               )
            {

                // Очистка ---------------------------------------------------------------------------
                // .....................................................................................
                // Очистка dataGridView

                while (dataGridView1.Rows.Count != 0)
                    dataGridView1.Rows.Remove(dataGridView1.Rows[dataGridView1.Rows.Count - 1]);

                dataGridView1.ClearSelection();
                for (int i = 0; i < GlobalVarLn.sizeDatWay_stat; i++)
                {
                    dataGridView1.Rows.Add("", "", "");
                }
                // .....................................................................................
                GlobalVarLn.blWay_stat = true;
                GlobalVarLn.flEndWay_stat = 1;

                GlobalVarLn.list_way.Clear();

                GlobalVarLn.iW_stat = 0;
                GlobalVarLn.X_StartW_stat = 0;
                GlobalVarLn.Y_StartW_stat = 0;
                GlobalVarLn.LW_stat = 0;
                // .....................................................................................
                textBox2.Text = "";   // L
                textBox1.Text = "";   // N
                // .....................................................................................
                // Убрать с карты

                GlobalVarLn.axMapScreenGlobal.Repaint();
                // .....................................................................................

                // --------------------------------------------------------------------------- Очистка

                // Чтение файла ---------------------------------------------------------
                // ......................................................................

                /*
                            if (
                               (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK) &&
                               (openFileDialog1.FileName.Length > 0)
                              )
                            GlobalVarLn.flname_R_stat = openFileDialog1.FileName;
                */

                GlobalVarLn.flname_R_stat = openFileDialog1.FileName;

                // ......................................................................

                if (GlobalVarLn.flname_R_stat != "")
                {
                    StreamReader srFile1 = new StreamReader(GlobalVarLn.flname_R_stat);
                    // ......................................................................

                    // ----------------------------------------------------------------------
                    // Чтение
                    // N =...
                    // X1 =...
                    // Y1 =...
                    // X2 =...
                    // Y2 =...
                    // ...
                    try
                    {
                        TekPoz = 0;
                        IndZap = 0;

                        // way
                        // 1-й List пустой
                        Way objWay = new Way();
                        objWay.X_m = 0;
                        objWay.Y_m = 0;
                        objWay.L_m = 0;
                        GlobalVarLn.list_way.Add(objWay);

                        // .......................................................
                        // N =...
                        // 1-я строка

                        strLine = srFile1.ReadLine();

                        if (strLine == null)
                        {
                            if (GlobalVarLn.fl_Azb == 0)
                            {
                                MessageBox.Show("Нет информации");
                            }
                            else
                            {
                                //Azb???
                                MessageBox.Show("melumat yoxdur");
                            }

                            srFile1.Close();
                            return;
                        }

                        indStart = strLine.IndexOf(symb1, TekPoz); // N

                        if (indStart == -1)
                        {
                            if (GlobalVarLn.fl_Azb == 0)
                            {
                                MessageBox.Show("Нет информации");
                            }
                            else
                            {
                                //Azb???
                                MessageBox.Show("melumat yoxdur");
                            }

                            srFile1.Close();
                            return;
                        }

                        indStop = strLine.IndexOf(symb4, TekPoz);  //=

                        if ((indStop == -1) || (indStop < indStart))
                        {
                            if (GlobalVarLn.fl_Azb == 0)
                            {
                                MessageBox.Show("Нет информации");
                            }
                            else
                            {
                                //Azb???
                                MessageBox.Show("melumat yoxdur");
                            }

                            srFile1.Close();
                            return;
                        }

                        iLength = indStop - indStart + 1;
                        // Убираем 'N ='
                        strLine1 = strLine.Remove(indStart, iLength);

                        if (strLine1 == "")
                        {
                            if (GlobalVarLn.fl_Azb == 0)
                            {
                                MessageBox.Show("Нет информации");
                            }
                            else
                            {
                                //Azb???
                                MessageBox.Show("melumat yoxdur");
                            }

                            srFile1.Close();
                            return;
                        }

                        // Количество пунктов
                        number2 = Convert.ToInt32(strLine1);
                        GlobalVarLn.iW_stat = (uint)number2;
                        // .......................................................
                        // 1-я координата X
                        // X1 =...

                        IndZap = 1;

                        strLine = srFile1.ReadLine(); // 2-я строка
                        if (strLine == null)
                        {
                            GlobalVarLn.iW_stat = 0;

                            if (GlobalVarLn.fl_Azb == 0)
                            {
                                MessageBox.Show("Нет информации");
                            }
                            else
                            {
                                //Azb???
                                MessageBox.Show("melumat yoxdur");
                            }

                            srFile1.Close();
                            return;
                        }

                        indStart = strLine.IndexOf(symb2, TekPoz); // X
                        if (indStart == -1)
                        {
                            GlobalVarLn.iW_stat = 0;

                            if (GlobalVarLn.fl_Azb == 0)
                            {
                                MessageBox.Show("Нет информации");
                            }
                            else
                            {
                                //Azb???
                                MessageBox.Show("melumat yoxdur");
                            }

                            srFile1.Close();
                            return;
                        }

                        indStop = strLine.IndexOf(symb4, TekPoz);  // =
                        if ((indStop == -1) || (indStop < indStart))
                        {
                            GlobalVarLn.iW_stat = 0;

                            if (GlobalVarLn.fl_Azb == 0)
                            {
                                MessageBox.Show("Нет информации");
                            }
                            else
                            {
                                //Azb???
                                MessageBox.Show("melumat yoxdur");
                            }

                            srFile1.Close();
                            return;
                        }

                        iLength = indStop - indStart + 1;
                        strLine1 = strLine.Remove(indStart, iLength);
                        if (strLine1 == "")
                        {
                            GlobalVarLn.iW_stat = 0;

                            if (GlobalVarLn.fl_Azb == 0)
                            {
                                MessageBox.Show("Нет информации");
                            }
                            else
                            {
                                //Azb???
                                MessageBox.Show("melumat yoxdur");
                            }

                            srFile1.Close();
                            return;
                        }

                        number1 = Convert.ToDouble(strLine1);

                        objWay.X_m = number1;

                        x1 = number1;
                        // .......................................................
                        // 1-я координата Y
                        // Y1 =...

                        strLine = srFile1.ReadLine(); // 3-я строка
                        if (strLine == null)
                        {
                            GlobalVarLn.iW_stat = 0;

                            if (GlobalVarLn.fl_Azb == 0)
                            {
                                MessageBox.Show("Нет информации");
                            }
                            else
                            {
                                //Azb???
                                MessageBox.Show("melumat yoxdur");
                            }

                            srFile1.Close();
                            return;
                        }

                        indStart = strLine.IndexOf(symb3, TekPoz); // Y
                        if (indStart == -1)
                        {
                            GlobalVarLn.iW_stat = 0;

                            if (GlobalVarLn.fl_Azb == 0)
                            {
                                MessageBox.Show("Нет информации");
                            }
                            else
                            {
                                //Azb???
                                MessageBox.Show("melumat yoxdur");
                            }

                            srFile1.Close();
                            return;
                        }

                        indStop = strLine.IndexOf(symb4, TekPoz);  // =
                        if ((indStop == -1) || (indStop < indStart))
                        {
                            GlobalVarLn.iW_stat = 0;

                            if (GlobalVarLn.fl_Azb == 0)
                            {
                                MessageBox.Show("Нет информации");
                            }
                            else
                            {
                                //Azb???
                                MessageBox.Show("melumat yoxdur");
                            }

                            srFile1.Close();
                            return;
                        }

                        iLength = indStop - indStart + 1;
                        strLine1 = strLine.Remove(indStart, iLength);
                        if (strLine1 == "")
                        {
                            GlobalVarLn.iW_stat = 0;

                            if (GlobalVarLn.fl_Azb == 0)
                            {
                                MessageBox.Show("Нет информации");
                            }
                            else
                            {
                                //Azb???
                                MessageBox.Show("melumat yoxdur");
                            }

                            srFile1.Close();
                            return;
                        }

                        number1 = Convert.ToDouble(strLine1);

                        objWay.Y_m = number1;

                        y1 = number1;
                        // ......................................................
                        // Занести в List

                        objWay.L_m = 0;
                        GlobalVarLn.list_way.Add(objWay);
                        // .......................................................
                        // Занести в таблицу

                        //dataGridView1.Rows[(int)(IndZap - 1)].Cells[0].Value = (int)GlobalVarLn.list_way[IndZap].X_m; // X
                        //dataGridView1.Rows[(int)(IndZap - 1)].Cells[1].Value = (int)GlobalVarLn.list_way[IndZap].Y_m; // Y

                        // SK42 ..................................................................

                        double mrelX = 0;
                        double mrelY = 0;
                        double mrelX42 = 0;
                        double mrelY42 = 0;

                        // m rel
                        mrelX = GlobalVarLn.list_way[IndZap].X_m;
                        mrelY = GlobalVarLn.list_way[IndZap].Y_m;

                        // rad
                        MapCore.mapPlaneToGeo((int)GlobalVarLn.hmapl, ref mrelX, ref mrelY);
                        // grad
                        mrelX = (mrelX * 180) / Math.PI;
                        mrelY = (mrelY * 180) / Math.PI;

                        // SK42(элл.)->Крюгер 
                        ClassMap objClassMap_s = new ClassMap();
                        objClassMap_s.f_SK42_Krug
                               (
                            // Входные параметры (!!! grad)
                            // !!! эллипсоид Красовского
                                   mrelX,   // широта
                                   mrelY,  // долгота

                                   // Выходные параметры (km)
                                   ref mrelX42,
                                   ref mrelY42
                               );

                        // km->m
                        mrelX42 = mrelX42 * 1000;
                        mrelY42 = mrelY42 * 1000;

                        // Добавить строку
                        dataGridView1.Rows[(int)(IndZap - 1)].Cells[0].Value = (int)mrelX42; // X
                        dataGridView1.Rows[(int)(IndZap - 1)].Cells[1].Value = (int)mrelY42; // Y
                        // .................................................................. SK42

                        // .......................................................

                        fi = 0;
                        strLine = srFile1.ReadLine(); // читаем далее (X2)
                        if ((strLine == "") || (strLine == null))
                            fi = 1;
                        // .......................................................

                        // WHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWH
                        while ((strLine != "") && (strLine != null))
                        {
                            IndZap += 1;
                            // ...................................................................
                            // X

                            indStart = strLine.IndexOf(symb2, TekPoz); // X
                            if (indStart == -1)
                                fi = 1;

                            indStop = strLine.IndexOf(symb4, TekPoz);  // =
                            if ((indStop == -1) || (indStop < indStart))
                                fi = 1;

                            iLength = indStop - indStart + 1;
                            strLine1 = strLine.Remove(indStart, iLength);
                            if (strLine1 == "")
                                fi = 1;

                            number1 = Convert.ToDouble(strLine1);

                            if (fi == 0)
                                objWay.X_m = number1;
                            else
                                objWay.X_m = 0;

                            x2 = number1;

                            fi = 0;
                            strLine = srFile1.ReadLine(); // читаем далее (Y)
                            if ((strLine == "") || (strLine == null))
                                fi = 1;
                            // ...................................................................
                            // Y

                            indStart = strLine.IndexOf(symb3, TekPoz); // Y
                            if (indStart == -1)
                                fi = 1;

                            indStop = strLine.IndexOf(symb4, TekPoz);  // =
                            if ((indStop == -1) || (indStop < indStart))
                                fi = 1;

                            iLength = indStop - indStart + 1;
                            strLine1 = strLine.Remove(indStart, iLength);
                            if (strLine1 == "")
                                fi = 1;

                            number1 = Convert.ToDouble(strLine1);

                            if (fi == 0)
                                objWay.Y_m = number1;
                            else
                                objWay.Y_m = 0;

                            y2 = number1;
                            // ...................................................................
                            // L

                            dx = x2 - x1;
                            dy = y2 - y1;

                            li = Math.Sqrt(dx * dx + dy * dy);

                            objWay.L_m = li;

                            GlobalVarLn.LW_stat += li;

                            x1 = x2;
                            y1 = y2;
                            // .......................................................
                            // Занести в List

                            GlobalVarLn.list_way.Add(objWay);
                            // .......................................................

                            // ...................................................................
                            // Занести в таблицу

                            //dataGridView1.Rows[(int)(IndZap - 1)].Cells[0].Value = (int)GlobalVarLn.list_way[IndZap].X_m; // X
                            //dataGridView1.Rows[(int)(IndZap - 1)].Cells[1].Value = (int)GlobalVarLn.list_way[IndZap].Y_m; // Y
                            dataGridView1.Rows[(int)(IndZap - 1)].Cells[2].Value = (int)GlobalVarLn.list_way[IndZap].L_m; // L

                            // SK42 ..................................................................

                            // m rel
                            mrelX = GlobalVarLn.list_way[IndZap].X_m;
                            mrelY = GlobalVarLn.list_way[IndZap].Y_m;

                            // rad
                            MapCore.mapPlaneToGeo((int)GlobalVarLn.hmapl, ref mrelX, ref mrelY);
                            // grad
                            mrelX = (mrelX * 180) / Math.PI;
                            mrelY = (mrelY * 180) / Math.PI;

                            // SK42(элл.)->Крюгер 
                            objClassMap_s.f_SK42_Krug
                                   (
                                // Входные параметры (!!! grad)
                                // !!! эллипсоид Красовского
                                       mrelX,   // широта
                                       mrelY,  // долгота

                                       // Выходные параметры (km)
                                       ref mrelX42,
                                       ref mrelY42
                                   );

                            // km->m
                            mrelX42 = mrelX42 * 1000;
                            mrelY42 = mrelY42 * 1000;

                            // Добавить строку
                            dataGridView1.Rows[(int)(IndZap - 1)].Cells[0].Value = (int)mrelX42; // X
                            dataGridView1.Rows[(int)(IndZap - 1)].Cells[1].Value = (int)mrelY42; // Y
                            // .................................................................. SK42

                            // ...................................................................

                            fi = 0;
                            strLine = srFile1.ReadLine(); // читаем далее (X)
                            if ((strLine == "") || (strLine == null))
                                fi = 1;

                            // ...................................................................


                        } // WHILE
                        // WHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWH

                    }
                    catch
                    {
                    }
                    // ----------------------------------------------------------------------
                    srFile1.Close();
                    // --------------------------------------------------------- Чтение файла

                    //GlobalVarLn.ichisloW_stat = (long)((GlobalVarLn.LW_stat / 1000) * 10000000);
                    //GlobalVarLn.dchisloW_stat = ((double)GlobalVarLn.ichisloW_stat) / 10000000;
                    //textBox2.Text = Convert.ToString(GlobalVarLn.dchisloW_stat);   // L
                    textBox2.Text = Convert.ToString((int)GlobalVarLn.LW_stat);   // L


                    //textBox1.Text = Convert.ToString(GlobalVarLn.iW_stat);         // N
                    textBox1.Text = Convert.ToString(GlobalVarLn.list_way.Count - 1);

                    // Перерисовка
                    f_WayReDraw();

                }// Filename!=""

            }// OK



        } // Button5 (Read from file)
        // *****************************************************************************************

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            ;
        }

        private void label2_Click(object sender, EventArgs e)
        {
            ;
        }

        // FUNCTIONS ********************************************************************************

        // ******************************************************************************************
        // Нарисовать метку по координатам (красный прямоугольник)
        //
        // Входные параметры(на карте):
        // X - X, m
        // Y - Y, m
        // ******************************************************************************************
        public void f_Map_Rect_XY2(

                                         double X,
                                         double Y
                                        )
        {

            // -------------------------------------------------------------------------------------
            //objMapForm10.graph = objMapForm10.axaxcMapScreen1.CreateGraphics();
            Graphics graph = axaxcMapScreen.CreateGraphics();

            Pen penRed2 = new Pen(Color.Red, 2);
            Brush brushRed2 = new SolidBrush(Color.Red);

            // -------------------------------------------------------------------------------------

            // Расстояние в м на карте -> пикселы на изображении
            mapPlaneToPicture(GlobalVarLn.hmapl, ref X, ref Y);
            // -------------------------------------------------------------------------------------

            //if (objMapForm10.graph != null)
            if (graph != null)

            {
                //objMapForm10.graph.FillRectangle(brushRed2, (int)X - objMapForm10.axaxcMapScreen1.MapLeft, (int)Y - objMapForm10.axaxcMapScreen1.MapTop, 7, 7);
                graph.FillRectangle(brushRed2, (int)X - axaxcMapScreen.MapLeft, (int)Y - axaxcMapScreen.MapTop, 7, 7);

                // Надпись
                //TitleObject(typeStation, lat, lon);
            }
            // -------------------------------------------------------------------------------------

            //objMapForm10.graph.Dispose();


        } // P/P f_Map_Rect_XY1


        // *************************************************************************************

        // ******************************************************************************************
        // Обработка нажатия левой кнопки мыши при отрисовке маршрута
        //
        // Входные параметры:
        // X - X, m на местности
        // Y - Y, m
        // ******************************************************************************************
        public void f_Way(
                          double X,
                          double Y
                         )
        {

            // ......................................................................
            GlobalVarLn.iW_stat += 1; // Номер пункта

            // way
            Way objWay = new Way();
            // ......................................................................
            // way

            if (GlobalVarLn.iW_stat == 1)
            {
                // 1-й List пустой
                objWay.X_m = 0;
                objWay.Y_m = 0;
                objWay.L_m = 0;
                GlobalVarLn.list_way.Add(objWay);
            }
            // ......................................................................

            GlobalVarLn.X1_W_stat = GlobalVarLn.X_StartW_stat;
            GlobalVarLn.Y1_W_stat = GlobalVarLn.Y_StartW_stat;
            GlobalVarLn.X2_W_stat = GlobalVarLn.MapX1;
            GlobalVarLn.Y2_W_stat = GlobalVarLn.MapY1;
            GlobalVarLn.X_StartW_stat = GlobalVarLn.X2_W_stat;
            GlobalVarLn.Y_StartW_stat = GlobalVarLn.Y2_W_stat;

            objWay.X_m = GlobalVarLn.MapX1;
            objWay.Y_m = GlobalVarLn.MapY1;

            // ......................................................................
            // Отображение координат карты, полученных кликом мыши на карте
            // !!! MapX1,MapY1 -> реальные координаты на местности карты в м (Plane)

            ClassMap.f_Map_Rect_XYS_stat(
                          GlobalVarLn.MapX1,
                          GlobalVarLn.MapY1,
                          //Convert.ToString(GlobalVarLn.iW_stat)
                          ""
                         );
            // ......................................................................
            // Добавить строку

            //dataGridView1.Rows[(int)(GlobalVarLn.iW_stat - 1)].Cells[0].Value = (int)GlobalVarLn.X_StartW_stat; // X
            //dataGridView1.Rows[(int)(GlobalVarLn.iW_stat - 1)].Cells[1].Value = (int)GlobalVarLn.Y_StartW_stat; // Y

            // SK42 ..................................................................

            double mrelX = 0;
            double mrelY = 0;
            double mrelX42 = 0;
            double mrelY42 = 0;

            // m rel
            mrelX = GlobalVarLn.X_StartW_stat;
            mrelY = GlobalVarLn.Y_StartW_stat;

            // rad
            MapCore.mapPlaneToGeo((int)GlobalVarLn.hmapl, ref mrelX, ref mrelY);
            // grad
            mrelX = (mrelX * 180) / Math.PI;
            mrelY = (mrelY * 180) / Math.PI;

            // SK42(элл.)->Крюгер 
            ClassMap objClassMap_s = new ClassMap();
            objClassMap_s.f_SK42_Krug
                   (
                // Входные параметры (!!! grad)
                // !!! эллипсоид Красовского
                       mrelX,   // широта
                       mrelY,  // долгота

                       // Выходные параметры (km)
                       ref mrelX42,
                       ref mrelY42
                   );

            // km->m
            mrelX42 = mrelX42 * 1000;
            mrelY42 = mrelY42 * 1000;

            // Добавить строку
            dataGridView1.Rows[(int)(GlobalVarLn.iW_stat - 1)].Cells[0].Value = (int)mrelX42; // X
            dataGridView1.Rows[(int)(GlobalVarLn.iW_stat - 1)].Cells[1].Value = (int)mrelY42; // Y
            // .................................................................. SK42

            // ......................................................................

            // IF2
            if (GlobalVarLn.iW_stat > 1)
            {
                // ......................................................................
                //m
                GlobalVarLn.DXX_W_stat = GlobalVarLn.X2_W_stat - GlobalVarLn.X1_W_stat;
                GlobalVarLn.DYY_W_stat = GlobalVarLn.Y2_W_stat - GlobalVarLn.Y1_W_stat;

                objWay.L_m = Math.Sqrt(GlobalVarLn.DXX_W_stat * GlobalVarLn.DXX_W_stat +
                                       GlobalVarLn.DYY_W_stat * GlobalVarLn.DYY_W_stat);
                // ......................................................................
                GlobalVarLn.LW_stat += objWay.L_m;
                // ......................................................................
                //GlobalVarLn.ichisloW_stat = (long)((objWay.L_m / 1000) * 10000000);
                //GlobalVarLn.dchisloW_stat = ((double)GlobalVarLn.ichisloW_stat) / 10000000;
                //dataGridView1.Rows[(int)(GlobalVarLn.iW_stat - 1)].Cells[2].Value = GlobalVarLn.dchisloW_stat; // L
                dataGridView1.Rows[(int)(GlobalVarLn.iW_stat - 1)].Cells[2].Value = (int)objWay.L_m; // L
                // ......................................................................
                // Соединить линией

                ClassMap.f_Map_Line_XY_stat(
                                      GlobalVarLn.X1_W_stat,
                                      GlobalVarLn.Y1_W_stat,
                                      GlobalVarLn.X2_W_stat,
                                      GlobalVarLn.Y2_W_stat
                                            );
                // ......................................................................
                //GlobalVarLn.ichisloW_stat = (long)((GlobalVarLn.LW_stat / 1000) * 10000000);
                //GlobalVarLn.dchisloW_stat = ((double)GlobalVarLn.ichisloW_stat) / 10000000;
                //textBox2.Text = Convert.ToString(GlobalVarLn.dchisloW_stat);   // L
                textBox2.Text = Convert.ToString((int)GlobalVarLn.LW_stat);   // L

                textBox1.Text = Convert.ToString(GlobalVarLn.iW_stat);         // N
                // ......................................................................
                // way
                // Добавить в List

                GlobalVarLn.list_way.Add(objWay);
                // ......................................................................


            } // IF2

            // way
            else
            {
                objWay.L_m = 0;
                // ......................................................................
                // way
                // Добавить в List

                GlobalVarLn.list_way.Add(objWay);
                // ......................................................................
            }

        } // P/P f_Way
        // *************************************************************************************

        // *************************************************************************************
        // Перерисовка маршрута
        // way
        // *************************************************************************************
        public void f_WayReDraw()
        {
          int i = 0;
          double x1 = 0;
          double y1 = 0;
          double x2 = 0;
          double y2 = 0;

        //777
       // for (i = 1; i <= GlobalVarLn.iW_stat;i++ )
       for (i = 1; i <= (GlobalVarLn.list_way.Count-1); i++)

          {
           // ...............................................................................
           // 1-й элемент

              // IF1
              if (i == 1)
              {
                  ClassMap.f_Map_Rect_XYS_stat(
                                GlobalVarLn.list_way[i].X_m,
                                GlobalVarLn.list_way[i].Y_m,
                                //Convert.ToString(i)
                                ""
                               );
                  x1 = GlobalVarLn.list_way[i].X_m;
                  y1 = GlobalVarLn.list_way[i].Y_m;

              } // IF1
              // ...............................................................................
              //  НЕ 1-й элемент

              else
              {
                  ClassMap.f_Map_Rect_XYS_stat(
                                GlobalVarLn.list_way[i].X_m,
                                GlobalVarLn.list_way[i].Y_m,
                                //Convert.ToString(i)
                                ""
                               );
                  x2 = GlobalVarLn.list_way[i].X_m;
                  y2 = GlobalVarLn.list_way[i].Y_m;


                  ClassMap.f_Map_Line_XY_stat(
                                           x1,
                                           y1,
                                           x2,
                                           y2
                                              );
                  x1 = GlobalVarLn.list_way[i].X_m;
                  y1 = GlobalVarLn.list_way[i].Y_m;

              } // else
              // ...............................................................................


          } // FOR
            
        } // P/P f_WayReDraw
        // *************************************************************************************

        // ********************************************************************************* FUNCTIONS

        // *************************************************************************************
        // Изменение состояния cgbWay
        // *************************************************************************************

        private void chbWay_CheckedChanged(object sender, EventArgs e)
        {

            // SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
            // Для маршрута 

            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            if (chbWay.Checked == false)
            {
                // .....................................................................................
                // Очистка dataGridView

                while (dataGridView1.Rows.Count != 0)
                    dataGridView1.Rows.Remove(dataGridView1.Rows[dataGridView1.Rows.Count - 1]);

                dataGridView1.ClearSelection();
                for (int i = 0; i < GlobalVarLn.sizeDatWay_stat; i++)
                {
                    dataGridView1.Rows.Add("", "", "");
                }
                // .....................................................................................
                GlobalVarLn.blWay_stat = false;
                GlobalVarLn.flEndWay_stat = 0;

                // way
                //Array.Clear(GlobalVarLn.mas_LW_stat, 0, 10000);
                //Array.Clear(GlobalVarLn.mas_XW_stat, 0, 10000);
                //Array.Clear(GlobalVarLn.mas_YW_stat, 0, 10000);
                GlobalVarLn.list_way.Clear();

                GlobalVarLn.iW_stat = 0;
                GlobalVarLn.X_StartW_stat = 0;
                GlobalVarLn.Y_StartW_stat = 0;
                GlobalVarLn.LW_stat = 0;
                // .....................................................................................
                textBox2.Text = "";   // L
                textBox1.Text = "";   // N
                // .....................................................................................
                // Убрать с карты

                GlobalVarLn.axMapScreenGlobal.Repaint();
                // .....................................................................................

            }
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

            else
            {
                GlobalVarLn.blWay_stat = true;
                GlobalVarLn.flEndWay_stat = 1;

            }
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

            // SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS

        } // Изменение состояния cgbWay
        // *************************************************************************************

        // *************************************************************************************
        // Закрыть форму

        private void FormWay_FormClosing(object sender, FormClosingEventArgs e)
        {
            GlobalVarLn.f_Open_objFormWay = 0;

            e.Cancel = true;
            Hide();

            // SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
            // Для маршрута (приостановить обработку, если идет)

            GlobalVarLn.blWay_stat = false;
            // SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS

        } // Closing
        // *************************************************************************************
        // Активизировать форму

        private void FormWay_Activated(object sender, EventArgs e)
        {
            GlobalVarLn.f_Open_objFormWay = 1;

            // SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
            // Для маршрута (возобновить обработку, если идет)

            if (chbWay.Checked == true)
            {
                GlobalVarLn.blWay_stat = true;
                GlobalVarLn.flEndWay_stat = 1;
            }
            // SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS

        }

        private void saveFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            ;
        } // Activated
        // *************************************************************************************

        private void LanguageChooser()
        {
            var cont = this.Controls;

            if (NumberOfLanguage.Equals(0))
            {
                ChangeLanguageToRu(cont);
                this.Text = "Маршрут";
            }
            if (NumberOfLanguage.Equals(1))
            {
                //ChangeLanguageToEng(cont);
            }
            if (NumberOfLanguage.Equals(2))
            {
                ChangeLanguageToAzer(cont);
                this.Text = "Marşrut";
            }
        }

        private void ChangeLanguageToRu(System.Windows.Forms.Control.ControlCollection cont)
        {
            string lang = "ru-RU";
            foreach (System.Windows.Forms.Control cc in cont)
            {
                if (cc is ComboBox)
                {

                    for (int i = 0; i < (cc as ComboBox).Items.Count; i++)
                    {
                        if (i == 0)
                        {
                            string a = resources.GetString(cc.Name + ".Items", new CultureInfo(lang));
                            (cc as ComboBox).Items[i] = a;
                        }
                        else
                        {
                            string a = resources.GetString(cc.Name + ".Items" + i.ToString(), new CultureInfo(lang));
                            (cc as ComboBox).Items[i] = a;
                        }
                    }
                }
                //}
                if (cc is Label || cc is Button || cc is RadioButton || cc is TabControl || cc is TabPage)
                {
                    resources.ApplyResources(cc, cc.Name, new CultureInfo(lang));
                    //toolTip.SetToolTip(cc, resources.GetString(cc.Name + ".ToolTip", new CultureInfo(lang)));
                }
                if (cc is Panel || cc is GroupBox || cc is ListBox || cc is TabControl || cc is TabPage)
                {
                    ChangeLanguageToRu(cc.Controls);
                }
            }
        }

        private void ChangeLanguageToAzer(System.Windows.Forms.Control.ControlCollection cont)
        {
            string lang = "az-Latn";

            foreach (System.Windows.Forms.Control cc in cont)
            {
                if (cc is ComboBox)
                {

                    for (int i = 0; i < (cc as ComboBox).Items.Count; i++)
                    {
                        if (i == 0)
                        {
                            string a = resources.GetString(cc.Name + ".Items", new CultureInfo(lang));
                            (cc as ComboBox).Items[i] = a;
                        }
                        else
                        {
                            string a = resources.GetString(cc.Name + ".Items" + i.ToString(), new CultureInfo(lang));
                            (cc as ComboBox).Items[i] = a;
                        }
                    }
                }
                //}
                if (cc is Label || cc is Button || cc is RadioButton || cc is TabControl || cc is TabPage)
                {
                    resources.ApplyResources(cc, cc.Name, new CultureInfo(lang));
                    //toolTip.SetToolTip(cc, resources.GetString(cc.Name + ".ToolTip", new CultureInfo(lang)));
                }
                if (cc is Panel || cc is GroupBox || cc is ListBox || cc is TabControl || cc is DataGridView || cc is TabPage)
                {
                    ChangeLanguageToAzer(cc.Controls);
                }

            }
        }


    } // Class
} // namespace
