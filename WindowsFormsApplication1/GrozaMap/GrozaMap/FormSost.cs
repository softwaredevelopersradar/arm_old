﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AxaxGisToolKit;
using axGisToolKit;
using System.Runtime.InteropServices;
using System.Windows.Forms;

using System.IO;

using System.Globalization;

using VariableDynamic;
using System.Reflection;

namespace GrozaMap
{
    public partial class FormSost : Form
    {
        [DllImport("gisacces.dll")]
        public static extern int mapPlaneToGeoWGS84(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        public static extern int mapPlaneToPicture(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        public static extern int mapPictureToPlane(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        public static extern int mapPlaneToGeo(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        static extern int mapGeoToPlane(int hmap, ref double Bx, ref double Ly);

        public delegate void ClickGetLocationEventHandler(double Lat, double Lon, double Alt);
        public event ClickGetLocationEventHandler ClickGetGNSS;

        public event ClickGetLocationEventHandler ClickGetGNSS2;

        private AxaxcMapScreen axaxcMapScreen;

        // Переменные VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR 
        private double dchislo;
        private long ichislo;
        private double LAMBDA;

        // .....................................................................
        // Координаты СП,ys

        //private uint flCoordSP_comm; // =1-> Выбрали СП
        //private uint flCoordYS1_comm; // =1-> Выбрали YS1
        //private uint flCoordYS2_comm; // =1-> Выбрали YS2

        // Координаты СП на местности в м
        private double XSP_comm;
        private double YSP_comm;
        private double XYS1_comm;
        private double YYS1_comm;
        private double XYS2_comm;
        private double YYS2_comm;

        // DATUM
        private double dXdat_comm;
        private double dYdat_comm;
        private double dZdat_comm;

        private double dLat_comm;
        private double dLong_comm;

        // Эллипсоид Красовского, град
        private double LatKrG_comm;
        private double LongKrG_comm;
        // Эллипсоид Красовского, rad
        private double LatKrR_comm;
        private double LongKrR_comm;
        // Эллипсоид Красовского, град,мин,сек
        private int Lat_Grad_comm;
        private int Lat_Min_comm;
        private double Lat_Sec_comm;
        private int Long_Grad_comm;
        private int Long_Min_comm;
        private double Long_Sec_comm;
        // Гаусс-крюгер(СК42) м
        private double XSP42_comm;
        private double YSP42_comm;

        // Эллипсоид Красовского, град
        private double LatKrG_YS1_comm;
        private double LongKrG_YS1_comm;
        // Эллипсоид Красовского, rad
        private double LatKrR_YS1_comm;
        private double LongKrR_YS1_comm;
        // Эллипсоид Красовского, град,мин,сек
        private int Lat_Grad_YS1_comm;
        private int Lat_Min_YS1_comm;
        private double Lat_Sec_YS1_comm;
        private int Long_Grad_YS1_comm;
        private int Long_Min_YS1_comm;
        private double Long_Sec_YS1_comm;
        // Гаусс-крюгер(СК42) м
        private double XYS142_comm;
        private double YYS142_comm;

        // Эллипсоид Красовского, град
        private double LatKrG_YS2_comm;
        private double LongKrG_YS2_comm;
        // Эллипсоид Красовского, rad
        private double LatKrR_YS2_comm;
        private double LongKrR_YS2_comm;
        // Эллипсоид Красовского, град,мин,сек
        private int Lat_Grad_YS2_comm;
        private int Lat_Min_YS2_comm;
        private double Lat_Sec_YS2_comm;
        private int Long_Grad_YS2_comm;
        private int Long_Min_YS2_comm;
        private double Long_Sec_YS2_comm;
        // Гаусс-крюгер(СК42) м
        private double XYS242_comm;
        private double YYS242_comm;

        // ......................................................................
        // Основные параметры

        private double OwnHeight_comm;
        private double Point1Height_comm;
        private double Point2Height_comm;
        //private double HeightOwnObject_comm;

/*
        private double PowerOwn_comm;
        private double CoeffOwn_comm;
        //private double RadiusZone_comm;
        //private double MaxDist_comm;

        private int i_HeightOwnObject_comm;
        private int i_Cap1_comm;
        private int i_WidthHindrance_comm;
        private int i_Surface_comm;
        private double Cap1_comm;
        private double WidthHindrance_comm;
        //private double Surface_comm;

        // Высота средства подавления
        private double HeightAntennOwn_comm;
        private double HeightTotalOwn_comm;

        // Для подавляемой линии
        private double Freq_comm;
        private double PowerOpponent_comm;
        private double CoeffTransmitOpponent_comm;
        private double CoeffReceiverOpponent_comm;
        private double RangeComm_comm;
        private double WidthSignal_comm;
        private double HeightTransmitOpponent_comm;
        private double HeightReceiverOpponent_comm;
        private double CoeffSupOpponent_comm;
        private int i_PolarOpponent_comm;
        private int i_CoeffSupOpponent_comm;
        private int i_TypeCommOpponent_comm;

        // ......................................................................
        // Зона

        private double dCoeffQ_comm;
        private double dCoeffHE_comm;
        private int iCorrectHeightOwn_comm;
        private int iResultHeightOwn_comm;
        private int iMiddleHeight_comm;
        private int iMinHeight_comm;
        private int iCorrectHeightOpponent_comm;
        private int iResultHeightOpponent_comm;
        private long iMaxDistance_comm;
        private double dGamma_comm;
        //private long liRadiusZone_comm;

        // ......................................................................
        private int iDistJammerComm1; // расстояние от УС1 до средства подаления
        private int iDistJammerComm2; // расстояние от УС2 до средства подаления
        private int iDistBetweenComm;
        private bool blResultSupress;
*/

        // VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR Переменные

        // Конструктор *********************************************************** 
        ComponentResourceManager resources = new ComponentResourceManager(typeof(FormSost));
        private int NumberOfLanguage;



        
        public FormSost(ref AxaxcMapScreen axaxcMapScreen1)
        {
            InitializeComponent();



            axaxcMapScreen = axaxcMapScreen1;

            dchislo = 0;
            ichislo = 0;

            LAMBDA = 300000;

            //flCoordSP_comm = 0; // =1-> Выбрали СП
            //flCoordYS1_comm = 0; // =1-> Выбрали YS1
            //flCoordYS2_comm = 0; // =1-> Выбрали YS2

            // .....................................................................
            // Координаты СП

            // Координаты СП на местности в м
            XSP_comm = 0;
            YSP_comm = 0;
            XYS1_comm = 0;
            YYS1_comm = 0;
            XYS2_comm = 0;
            YYS2_comm = 0;

            // DATUM
            // ГОСТ 51794_2008
            dXdat_comm = 25;
            dYdat_comm = -141;
            dZdat_comm = -80;

            dLat_comm = 0;
            dLong_comm = 0;

            // Эллипсоид Красовского, град
            LatKrG_comm = 0;
            LongKrG_comm = 0;
            // Эллипсоид Красовского, rad
            LatKrR_comm = 0;
            LongKrR_comm = 0;
            // Эллипсоид Красовского, град,мин,сек
            Lat_Grad_comm = 0;
            Lat_Min_comm = 0;
            Lat_Sec_comm = 0;
            Long_Grad_comm = 0;
            Long_Min_comm = 0;
            Long_Sec_comm = 0;
            // Гаусс-крюгер(СК42) м
            XSP42_comm = 0;
            YSP42_comm = 0;

            // Эллипсоид Красовского, град
            LatKrG_YS1_comm = 0;
            LongKrG_YS1_comm = 0;
            // Эллипсоид Красовского, rad
            LatKrR_YS1_comm = 0;
            LongKrR_YS1_comm = 0;
            // Эллипсоид Красовского, град,мин,сек
            Lat_Grad_YS1_comm = 0;
            Lat_Min_YS1_comm = 0;
            Lat_Sec_YS1_comm = 0;
            Long_Grad_YS1_comm = 0;
            Long_Min_YS1_comm = 0;
            Long_Sec_YS1_comm = 0;
            // Гаусс-крюгер(СК42) м
            XYS142_comm = 0;
            YYS142_comm = 0;

            // Эллипсоид Красовского, град
            LatKrG_YS2_comm = 0;
            LongKrG_YS2_comm = 0;
            // Эллипсоид Красовского, rad
            LatKrR_YS2_comm = 0;
            LongKrR_YS2_comm = 0;
            // Эллипсоид Красовского, град,мин,сек
            Lat_Grad_YS2_comm = 0;
            Lat_Min_YS2_comm = 0;
            Lat_Sec_YS2_comm = 0;
            Long_Grad_YS2_comm = 0;
            Long_Min_YS2_comm = 0;
            Long_Sec_YS2_comm = 0;
            // Гаусс-крюгер(СК42) м
            XYS242_comm = 0;
            YYS242_comm = 0;

            // ......................................................................
            // Основные параметры

            OwnHeight_comm = 0;
            Point1Height_comm = 0;
            Point2Height_comm = 0;
            //HeightOwnObject_comm = 0;
/*
            PowerOwn_comm = 0;
            CoeffOwn_comm = 0;
            //RadiusZone_comm = 0;
            //MaxDist_comm = 0;

            i_HeightOwnObject_comm = 0;
            i_Cap1_comm = 0;
            i_WidthHindrance_comm = 0;
            i_Surface_comm = 0;
            Cap1_comm = 0;
            WidthHindrance_comm = 0;
            //Surface_comm = 0;

            // Высота средства подавления
            // ??????????????????????
            HeightAntennOwn_comm = 0;
            HeightTotalOwn_comm = 0;

            // Для подавляемой линии
            Freq_comm = 0;
            PowerOpponent_comm = 0;
            CoeffTransmitOpponent_comm = 0;
            CoeffReceiverOpponent_comm = 0;
            RangeComm_comm = 0;
            WidthSignal_comm = 0;
            HeightTransmitOpponent_comm = 0;
            HeightReceiverOpponent_comm = 0;
            CoeffSupOpponent_comm = 0;
            i_PolarOpponent_comm = 0;
            i_CoeffSupOpponent_comm = 0;
            i_TypeCommOpponent_comm = 0;

            // ......................................................................
            // Зона

            dCoeffQ_comm = 0;
            dCoeffHE_comm = 0;
            iCorrectHeightOwn_comm = 0;
            iResultHeightOwn_comm = 0;
            iMiddleHeight_comm = 0;
            iMinHeight_comm = 0;
            iCorrectHeightOpponent_comm = 0;
            iResultHeightOpponent_comm = 0;
            iMaxDistance_comm = 0;
            dGamma_comm = 0;
            //liRadiusZone_comm = 0;

            // ......................................................................
            iDistJammerComm1 = 0; // расстояние от УС1 до средства подаления
            iDistJammerComm2 = 0; // расстояние от УС2 до средства подаления
            iDistBetweenComm = 0;
            blResultSupress = false;
*/

        } // Конструктор
        // ***********************************************************  Конструктор

        // ************************************************************************
        // Загрузка формы
        // ************************************************************************
        private void FormSost_Load(object sender, EventArgs e)
        {
            string strExePath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

            string PathFolder1 = Path.GetDirectoryName(strExePath);

            string strPathLatest = PathFolder1 + "\\INI\\Common.ini";
            foreach (string line in File.ReadLines(strPathLatest))
            {
                if (line.Contains("Language "))
                {
                    string LanguageAsNumber = line.Split(new string[] { "=" }, StringSplitOptions.None).Last();
                    NumberOfLanguage = Int32.Parse(LanguageAsNumber);
                    break;
                }
            }
            LanguageChooser();

            // ----------------------------------------------------------------------
            gbOwnRect.Visible = true;
            gbOwnRect.Location = new Point(10, 34);

            gbOwnRect42.Visible = false;
            gbOwnRad.Visible = false;
            gbOwnDegMin.Visible = false;
            gbOwnDegMinSec.Visible = false;

            cbChooseSC.SelectedIndex = 0;
            // ----------------------------------------------------------------------
            gbPt1Rect.Visible = true;
            //gbPt1Rect.Location = new Point(6, 11);
            gbPt1Rect.Location = new Point(10, 34);
            gbPt2Rect.Visible = true;
            //gbPt2Rect.Location = new Point(6, 11);
            gbPt2Rect.Location = new Point(10, 34);
            gbPt1Rect42.Visible = false;
            gbPt1Rad.Visible = false;
            gbPt1DegMin.Visible = false;
            gbPt1DegMinSec.Visible = false;

            gbPt2Rect42.Visible = false;
            gbPt2Rad.Visible = false;
            gbPt2DegMin.Visible = false;
            gbPt2DegMinSec.Visible = false;

            // ----------------------------------------------------------------------
            chbXY1.Checked = false;
            chbGPS.Checked = false;


            //3101
            // SP1
            gbOwnRect.Enabled = false;
            gbOwnRect42.Enabled = false;
            gbOwnRad.Enabled = false;
            gbOwnDegMin.Enabled = false;
            gbOwnDegMinSec.Enabled = false;
            // SP2
            gbPt1Rect.Enabled = false;
            gbPt1Rect42.Enabled = false;
            gbPt1Rad.Enabled = false;
            gbPt1DegMin.Enabled = false;
            gbPt1DegMinSec.Enabled = false;
            // PU
            gbPt2Rect.Enabled = false;
            gbPt2Rect42.Enabled = false;
            gbPt2Rad.Enabled = false;
            gbPt2DegMin.Enabled = false;
            gbPt2DegMinSec.Enabled = false;

            // ----------------------------------------------------------------------
            // Переменные

            //GlobalVarLn.fl_Sost = 0; // Отрисовка зоны
            //GlobalVarLn.flCoordSP_Sost = 0; // =1-> Выбрали СП
            //GlobalVarLn.flCoordYS1_Sost = 0;
            //GlobalVarLn.flCoordYS2_Sost = 0;
            //GlobalVarLn.list1_Sost.Clear();

/*
            GlobalVarLn.indz1_Sost = 0;
            GlobalVarLn.indz2_Sost = 0;
            GlobalVarLn.indz3_Sost = 0;
            pbSP.Image = imageList1.Images[GlobalVarLn.indz1_Sost];
            pictureBox1.Image = imageList2.Images[GlobalVarLn.indz2_Sost];
            pictureBox2.Image = imageList3.Images[GlobalVarLn.indz3_Sost];
*/

            // 13_09_2018
            // !!! Значки читаем с INI файла
            GlobalVarLn.indz1_Sost = iniRW.get_ZnakSP1();
            GlobalVarLn.indz2_Sost = iniRW.get_ZnakSP2();
            GlobalVarLn.indz3_Sost = iniRW.get_ZnakPU();
            GlobalVarLn.objFormSostG.pbSP.Image = GlobalVarLn.objFormSostG.imageList1.Images[GlobalVarLn.indz1_Sost];
            GlobalVarLn.objFormSostG.pictureBox1.Image = GlobalVarLn.objFormSostG.imageList2.Images[GlobalVarLn.indz2_Sost];
            GlobalVarLn.objFormSostG.pictureBox2.Image = GlobalVarLn.objFormSostG.imageList3.Images[GlobalVarLn.indz3_Sost];


            //GlobalVarLn.fl_First_GPS = 0;
            // ----------------------------------------------------------------------
            //LF objLF = new LF();
            //for (int i = 0; i < GlobalVarLn.Number_Sost; i++)
            //{
            //    objLF.X_m = 0;
            //    objLF.Y_m = 0;
            //    objLF.H_m = 0;
            //    objLF.indzn = 0;
            //    GlobalVarLn.list1_Sost.Add(objLF);
            //}
            // ----------------------------------------------------------------------
            // !!! Первоначальная загрузка составы комплекса с INI файла
            // GPSSPPU

            //ClassMap.f_LoadSostIni();
            // ----------------------------------------------------------------------

        } // Load
        // ************************************************************************

        // ************************************************************************
        // Очистка
        // ************************************************************************        
        private void button3_Click(object sender, EventArgs e)
        {

            // --------------------------------------------------------------------------------------
            // 14_09_2018
            // Для пеленгов
            
                            // Нажата птичка для отрисовки пеленгов
                            if (GlobalVarLn.flPelMain==1) 
                            {
                                     // FRCH
                                    GlobalVarLn.PrevP1 = -1;
                                    GlobalVarLn.PrevP2 = -1;
                                    GlobalVarLn.flPelMain2 = 0;   // 2-й пеленг
                                    GlobalVarLn.fl_PelIRI_1 = 0; // 1-й пеленг
                                    GlobalVarLn.flag_eq_draw1 = 0;
                                    GlobalVarLn.flag_eq_draw2 = 0;

                                    // 28_09_2018
                                    GlobalVarLn.PELENGG_1 = -1;
                                    GlobalVarLn.PELENGG_2 = -1;

                                    //------------------------------------------------------------------------
                                    for (int iii = (GlobalVarLn.list_PelIRI.Count - 1); iii >= 0; iii--)
                                    {


                                        PelIRI[] mass = GlobalVarLn.list_PelIRI.ToArray();
                                        mass[iii].Pel1 = -1;
                                        mass[iii].Pel2 = -1;
                                        GlobalVarLn.list_PelIRI = mass.ToList();

                                        if (
                                            (GlobalVarLn.list_PelIRI[iii].Pel1 == -1) &&
                                            (GlobalVarLn.list_PelIRI[iii].Pel2 == -1) &&
                                            (GlobalVarLn.list_PelIRI[iii].Lat == -1) &&
                                            (GlobalVarLn.list_PelIRI[iii].Long == -1)
                                           )
                                        {
                                            GlobalVarLn.list_PelIRI.Remove(GlobalVarLn.list_PelIRI[iii]);
                                        }
                                    }
                                    //------------------------------------------------------------------------

                                    // Убрать с карты
                                    //GlobalVarLn.axMapScreenGlobal.Repaint();

                            } // Нажата птичка для отрисовки пеленгов
            // --------------------------------------------------------------------------------------

            ClassMap.ClearSost(); // Есть Repaint

            // 10_10_2018
            ClickGetGNSS(-1, -1, -1);
            ClickGetGNSS2(-1, -1, -1);


        } // Clear
        // ************************************************************************        

        // ************************************************************************
        // Обработчик ComboBox "cbChooseSC": Выбор СК SP
        // ************************************************************************
        private void cbChooseSC_SelectedIndexChanged(object sender, EventArgs e)
        {
            ChooseSystemCoord_Sost(cbChooseSC.SelectedIndex);

        } // SK
        // ************************************************************************

        // ************************************************************************
        // Выбор SP1
        // ************************************************************************
        private void button5_Click(object sender, EventArgs e)
        {
            double xtmp_ed, ytmp_ed, htmp_ed;
            double xtmp1_ed, ytmp1_ed;

            xtmp_ed = 0;
            ytmp_ed = 0;
            htmp_ed = 0;
            xtmp1_ed = 0;
            ytmp1_ed = 0;
            // ......................................................................
            ClassMap objClassMap1_ed = new ClassMap();
            ClassMap objClassMap2_ed = new ClassMap();
            ClassMap objClassMap3_ed = new ClassMap();

            LF objLF = new LF();
            // ......................................................................


            // --------------------------------------------------------------------------------------
            // 19_09_2018
            // Для пеленгов

            // 23_10_2018
            // Нажата птичка для отрисовки пеленгов

/*
            if (GlobalVarLn.flPelMain == 1)
            {
                // FRCH
                GlobalVarLn.PrevP1 = -1;
                GlobalVarLn.fl_PelIRI_1 = 0; // 1-й пеленг
                GlobalVarLn.flag_eq_draw1 = 0;

                //------------------------------------------------------------------------
                //PPRCH
                for (int iii = (GlobalVarLn.list_PelIRI.Count - 1); iii >= 0; iii--)
                {


                    PelIRI[] mass = GlobalVarLn.list_PelIRI.ToArray();
                    mass[iii].Pel1 = -1;
                    GlobalVarLn.list_PelIRI = mass.ToList();

                    if (
                        (GlobalVarLn.list_PelIRI[iii].Pel1 == -1) &&
                        (GlobalVarLn.list_PelIRI[iii].Pel2 == -1) &&
                        (GlobalVarLn.list_PelIRI[iii].Lat == -1) &&
                        (GlobalVarLn.list_PelIRI[iii].Long == -1)
                       )
                    {
                        GlobalVarLn.list_PelIRI.Remove(GlobalVarLn.list_PelIRI[iii]);
                    }
                }
                //------------------------------------------------------------------------

            } // Нажата птичка для отрисовки пеленгов
 */ 


            // --------------------------------------------------------------------------------------

            // ......................................................................
            // !!! реальные координаты на местности карты в м (Plane)

            GlobalVarLn.XCenter_Sost = GlobalVarLn.MapX1;
            GlobalVarLn.YCenter_Sost = GlobalVarLn.MapY1;
            // ......................................................................

            // RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
            // Ручной ввод

            if (chbXY1.Checked == true)
            {
                String s1 = "";

                // ----------------------------------------------------------------------
                // CK42

                GlobalVarLn.f42SP_sost = 0;
                GlobalVarLn.f42SP_G_sost = 0;
                GlobalVarLn.f84SP_G_sost = 0;

                if (gbOwnRect42.Visible == true)
                {

                    // .............................................................................
                    if ((tbXRect42.Text == "") || (tbYRect42.Text == ""))
                    {
                        if (GlobalVarLn.fl_Azb == 0)
                        {
                            MessageBox.Show("Недопустимые координаты АСП");
                        }
                        else
                        {
                            //Azb
                            MessageBox.Show("Yolverilməz koordinat göstəriciləri MS");
                        }
                        return;
                    }
                    //GlobalVarLn.XCenter_Sost = Convert.ToDouble(tbXRect.Text,GlobalVarLn.formatG);
                    //GlobalVarLn.YCenter_Sost = Convert.ToDouble(tbYRect.Text, GlobalVarLn.formatG);

                    GlobalVarLn.f42SP_sost = 1;
                    // .............................................................................
                    // Гаусс_Крюгер

                    s1 = tbXRect42.Text;   // X
                    try
                    {
                        //if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                        GlobalVarLn.XSP42_comm = Convert.ToDouble(s1);
                    }
                    catch (SystemException)
                    {
                        try
                        {
                            if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                            else s1 = s1.Replace('.', ',');
                            GlobalVarLn.XSP42_comm = Convert.ToDouble(s1);
                        }
                        catch
                        {
                            if (GlobalVarLn.fl_Azb == 0)
                            {
                                MessageBox.Show("Недопустимые координаты АСП");
                            }
                            else
                            {
                                //Azb
                                MessageBox.Show("Yolverilməz koordinat göstəriciləri MS");
                            }
                            return;
                        }

                    }

                    s1 = tbYRect42.Text;   // Y
                    try
                    {
                        //if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                        GlobalVarLn.YSP42_comm = Convert.ToDouble(s1);
                    }
                    catch (SystemException)
                    {
                        try
                        {
                            if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                            else s1 = s1.Replace('.', ',');
                            GlobalVarLn.YSP42_comm = Convert.ToDouble(s1);
                        }
                        catch
                        {
                            if (GlobalVarLn.fl_Azb == 0)
                            {
                                MessageBox.Show("Недопустимые координаты АСП");
                            }
                            else
                            {
                                //Azb
                                MessageBox.Show("Yolverilməz koordinat göstəriciləri MS");
                            }
                            return;
                        }

                    }
                    // .............................................................................
                    // Эллипсоид Красовского (grad)

                    objClassMap3_ed.f_Krug_SK42
                    (
                        // Входные параметры (км)
                     GlobalVarLn.XSP42_comm / 1000,
                     GlobalVarLn.YSP42_comm / 1000,

                     // Выходные параметры (grad)
                     ref GlobalVarLn.LatKrG_comm,   // широта
                     ref GlobalVarLn.LongKrG_comm   // долгота
                     );
                    // .............................................................................
                    // м на местности (ГИС)

                    GlobalVarLn.XCenterGRAD_Sost = GlobalVarLn.LatKrG_comm;
                    GlobalVarLn.YCenterGRAD_Sost = GlobalVarLn.LongKrG_comm;

                    // grad->rad
                    GlobalVarLn.XCenterGRAD_Sost = (GlobalVarLn.XCenterGRAD_Sost * Math.PI) / 180;
                    GlobalVarLn.YCenterGRAD_Sost = (GlobalVarLn.YCenterGRAD_Sost * Math.PI) / 180;

                    // Подаем rad, получаем там же расстояние на карте в м
                    mapGeoToPlane(GlobalVarLn.hmapl, ref GlobalVarLn.XCenterGRAD_Sost, ref GlobalVarLn.YCenterGRAD_Sost);

                    GlobalVarLn.XCenter_Sost = GlobalVarLn.XCenterGRAD_Sost;
                    GlobalVarLn.YCenter_Sost = GlobalVarLn.YCenterGRAD_Sost;
                    // .............................................................................

                } // gbOwnRect42.Visible==true

            // ----------------------------------------------------------------------
                // Градусы

                else if (gbOwnDegMin.Visible == true)
                {

                    if ((tbBMin1.Text == "") || (tbLMin1.Text == ""))
                    {
                        if (GlobalVarLn.fl_Azb == 0)
                        {
                            MessageBox.Show("Недопустимые координаты АСП");
                        }
                        else
                        {
                            //Azb
                            MessageBox.Show("Yolverilməz koordinat göstəriciləri MS");
                        }
                        return;
                    }

                    GlobalVarLn.f42SP_G_sost = 1;

                    s1 = tbBMin1.Text;   //Lat
                    try
                    {
                        //if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                        GlobalVarLn.XCenterGRAD_Sost = Convert.ToDouble(s1);
                    }
                    catch (SystemException)
                    {
                        try
                        {
                            if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                            else s1 = s1.Replace('.', ',');
                            GlobalVarLn.XCenterGRAD_Sost = Convert.ToDouble(s1);
                        }
                        catch
                        {
                            if (GlobalVarLn.fl_Azb == 0)
                            {
                                MessageBox.Show("Недопустимые координаты АСП");
                            }
                            else
                            {
                                //Azb
                                MessageBox.Show("Yolverilməz koordinat göstəriciləri MS");
                            }
                            return;
                        }

                    }

                    s1 = tbLMin1.Text;   // Long
                    try
                    {
                        //if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                        GlobalVarLn.YCenterGRAD_Sost = Convert.ToDouble(s1);
                    }
                    catch (SystemException)
                    {
                        try
                        {
                            if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                            else s1 = s1.Replace('.', ',');
                            GlobalVarLn.YCenterGRAD_Sost = Convert.ToDouble(s1);
                        }
                        catch
                        {
                            if (GlobalVarLn.fl_Azb == 0)
                            {
                                MessageBox.Show("Недопустимые координаты АСП");
                            }
                            else
                            {
                                //Azb
                                MessageBox.Show("Yolverilməz koordinat göstəriciləri MS");
                            }
                            return;
                        }

                    }

                    // Krasovsky (grad)
                    GlobalVarLn.LatKrG_comm = GlobalVarLn.XCenterGRAD_Sost;   // широта
                    GlobalVarLn.LongKrG_comm = GlobalVarLn.YCenterGRAD_Sost;   // долгота

                    // grad->rad
                    GlobalVarLn.XCenterGRAD_Sost = (GlobalVarLn.XCenterGRAD_Sost * Math.PI) / 180;
                    GlobalVarLn.YCenterGRAD_Sost = (GlobalVarLn.YCenterGRAD_Sost * Math.PI) / 180;

                    // Подаем rad, получаем там же расстояние на карте в м
                    mapGeoToPlane(GlobalVarLn.hmapl, ref GlobalVarLn.XCenterGRAD_Sost, ref GlobalVarLn.YCenterGRAD_Sost);

                    GlobalVarLn.XCenter_Sost = GlobalVarLn.XCenterGRAD_Sost;
                    GlobalVarLn.YCenter_Sost = GlobalVarLn.YCenterGRAD_Sost;


                } // gbOwnDegMinSec.Visible == true
                // ----------------------------------------------------------------------
                //999
                // Градусы WGS84

                else if (gbOwnRect.Visible == true)
                {

                    if ((tbXRect.Text == "") || (tbYRect.Text == ""))
                    {
                        if (GlobalVarLn.fl_Azb == 0)
                        {
                            MessageBox.Show("Недопустимые координаты АСП");
                        }
                        else
                        {
                            //Azb
                            MessageBox.Show("Yolverilməz koordinat göstəriciləri MS");
                        }
                        return;
                    }

                    GlobalVarLn.f84SP_G_sost = 1;

                    s1 = tbXRect.Text;   //Lat
                    try
                    {
                        //if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                        xtmp1_ed = Convert.ToDouble(s1);
                    }
                    catch (SystemException)
                    {
                        try
                        {
                            if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                            else s1 = s1.Replace('.', ',');
                            xtmp1_ed = Convert.ToDouble(s1);
                        }
                        catch
                        {
                            if (GlobalVarLn.fl_Azb == 0)
                            {
                                MessageBox.Show("Недопустимые координаты АСП");
                            }
                            else
                            {
                                //Azb
                                MessageBox.Show("Yolverilməz koordinat göstəriciləri MS");
                            }
                            return;
                        }
                    }

                    s1 = tbYRect.Text;   // Long
                    try
                    {
                        //if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                        ytmp1_ed = Convert.ToDouble(s1);
                    }
                    catch (SystemException)
                    {
                        try
                        {
                            if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                            else s1 = s1.Replace('.', ',');
                            ytmp1_ed = Convert.ToDouble(s1);
                        }
                        catch
                        {
                            if (GlobalVarLn.fl_Azb == 0)
                            {
                                MessageBox.Show("Недопустимые координаты АСП");
                            }
                            else
                            {
                                //Azb
                                MessageBox.Show("Yolverilməz koordinat göstəriciləri MS");
                            }
                            return;
                        }

                    }

                    // WGS84_GRAD
                    GlobalVarLn.BWGS84_1_sost = xtmp1_ed;
                    GlobalVarLn.LWGS84_1_sost = ytmp1_ed;

                    // WGS84(эллипсоид)->элл.Красовского *************************************
                    // Перевод координат WGS84(эллипсоид) -> эллипсоид Красовского(Пулково-42)
                    // WGS84(широта,долгота)из Панорамы пересчитываем в эллипсод Красовского моей функцией
                    // Входные параметры -> град,km
                    // Перевод в рад - внутри функции


                    // dLong ..................................................................
                    // Расчет приращения по долготе при преобразованиях координат WGS84<->SK42
                    // (преобразования Молоденского), угл.сек

                    objClassMap1_ed.f_dLong
                        (
                        // Входные параметры (град,км)
                            xtmp1_ed,   // широта
                            ytmp1_ed,  // долгота
                            0,     // высота

                            // DATUM,m
                            GlobalVarLn.dXdat_comm,
                            GlobalVarLn.dYdat_comm,
                            GlobalVarLn.dZdat_comm,

                            ref GlobalVarLn.dLong_comm   // приращение по долготе, угл.сек
                        );
                    // ................................................................ dLong

                    // dLat .................................................................
                    // Расчет приращения по широте при преобразованиях координат WGS84<->SK42
                    // (преобразования Молоденского), угл.сек

                    objClassMap1_ed.f_dLat
                        (
                        // Входные параметры (град,км)
                            xtmp1_ed,   // широта
                            ytmp1_ed,  // долгота
                            0,     // высота

                            // DATUM,m
                            GlobalVarLn.dXdat_comm,
                            GlobalVarLn.dYdat_comm,
                            GlobalVarLn.dZdat_comm,

                            ref GlobalVarLn.dLat_comm        // приращение по долготе, угл.сек
                        );
                    // ................................................................. dLat

                    // Lat,Long .............................................................
                    // Преобразования широты и долготы при пересчете WGS84->SK42

                    objClassMap1_ed.f_WGS84_SK42_Lat_Long
                           (
                        // Входные параметры (град,км)
                               xtmp1_ed,   // широта
                               ytmp1_ed,  // долгота
                               0,     // высота
                               GlobalVarLn.dLat_comm,       // приращение по долготе, угл.сек
                               GlobalVarLn.dLong_comm,      // приращение по долготе, угл.сек

                               // Выходные параметры (grad)
                               ref GlobalVarLn.LatKrG_comm,   // широта
                               ref GlobalVarLn.LongKrG_comm   // долгота
                           );
                    // ............................................................ Lat,Long

                    // *********************************** WGS84(эллипсоид)->элл.Красовского

                    double lt1 = 0;
                    double lng1 = 0;

                    // grad->rad
                    lt1 = (GlobalVarLn.LatKrG_comm * Math.PI) / 180;
                    lng1 = (GlobalVarLn.LongKrG_comm * Math.PI) / 180;

                    // Подаем rad, получаем там же расстояние на карте в м
                    mapGeoToPlane(GlobalVarLn.hmapl, ref lt1, ref lng1);

                    GlobalVarLn.XCenter_Sost = lt1;
                    GlobalVarLn.YCenter_Sost = lng1;

                } // WGS84
                // ----------------------------------------------------------------------

            } // Ручной ввод

            else
            {
                GlobalVarLn.f42SP_sost = 0;
                GlobalVarLn.f42SP_G_sost = 0;
                GlobalVarLn.f84SP_G_sost = 0;
            }
            // RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR

            // ......................................................................
            xtmp_ed = GlobalVarLn.XCenter_Sost;
            ytmp_ed = GlobalVarLn.YCenter_Sost;

            objLF.X_m = xtmp_ed;
            objLF.Y_m = ytmp_ed;
            // ......................................................................
            // H

            GlobalVarLn.axMapPointGlobalAdd.SetPoint(xtmp_ed, ytmp_ed);
            GlobalVarLn.HCenter_Sost = (int)GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);
            htmp_ed = GlobalVarLn.HCenter_Sost;
            tbOwnHeight.Text = Convert.ToString(htmp_ed);
            objLF.H_m = htmp_ed;
            // ......................................................................
            // Sign

            objLF.indzn = GlobalVarLn.indz1_Sost;
            // ......................................................................
            GlobalVarLn.fl_Sost = 1;
            GlobalVarLn.flCoordSP_Sost = 1; // СП1 выбрана

            // 14_09_2018
            // Peleng, FRCH
            if (GlobalVarLn.flPelMain==1) // нажата птичка отрисовки пеленга
                GlobalVarLn.flag_eq_draw1 = 1;

            // ......................................................................
            // List

            GlobalVarLn.list1_Sost[0] = objLF;
            // ......................................................................
            // Draw SP1

            // Убрать с карты
            GlobalVarLn.axMapScreenGlobal.Repaint();
            // Redraw
            ClassMap.f_Map_Redraw_Sost();

            // ......................................................................
            // Реальные координаты карты в м -> в долготу и широту WGS84 с помощью функций Панорамы
            // !!! Выход функции(rad) идет на место входных переменных

            //999
            if(GlobalVarLn.f84SP_G_sost==0) // не ручной ввод
            {
            mapPlaneToGeoWGS84(GlobalVarLn.hmapl, ref xtmp_ed, ref ytmp_ed);

            // rad(WGS84)->grad(WGS84)
            xtmp1_ed = (xtmp_ed * 180) / Math.PI;
            ytmp1_ed = (ytmp_ed * 180) / Math.PI;

            //333
            GlobalVarLn.BWGS84_1_sost = xtmp1_ed;
            GlobalVarLn.LWGS84_1_sost = ytmp1_ed;

            //777
            GlobalVarLn.XCenterGRAD_Dubl_Sost = xtmp1_ed;
            GlobalVarLn.YCenterGRAD_Dubl_Sost = ytmp1_ed;
  
            }
            // .......................................................................


            // WGS84(эллипсоид)->элл.Красовского *************************************
            // Перевод координат WGS84(эллипсоид) -> эллипсоид Красовского(Пулково-42)
            // WGS84(широта,долгота)из Панорамы пересчитываем в эллипсод Красовского моей функцией
            // Входные параметры -> град,km
            // Перевод в рад - внутри функции

            //999
            if ((GlobalVarLn.f42SP_sost == 0) && (GlobalVarLn.f42SP_G_sost == 0) && (GlobalVarLn.f84SP_G_sost == 0))
            {
                // dLong ..................................................................
                // Расчет приращения по долготе при преобразованиях координат WGS84<->SK42
                // (преобразования Молоденского), угл.сек

                objClassMap1_ed.f_dLong
                    (
                    // Входные параметры (град,км)
                        xtmp1_ed,   // широта
                        ytmp1_ed,  // долгота
                        0,     // высота

                        // DATUM,m
                        GlobalVarLn.dXdat_comm,
                        GlobalVarLn.dYdat_comm,
                        GlobalVarLn.dZdat_comm,

                        ref GlobalVarLn.dLong_comm   // приращение по долготе, угл.сек
                    );
                // ................................................................ dLong

                // dLat .................................................................
                // Расчет приращения по широте при преобразованиях координат WGS84<->SK42
                // (преобразования Молоденского), угл.сек

                objClassMap1_ed.f_dLat
                    (
                    // Входные параметры (град,км)
                        xtmp1_ed,   // широта
                        ytmp1_ed,  // долгота
                        0,     // высота

                        // DATUM,m
                        GlobalVarLn.dXdat_comm,
                        GlobalVarLn.dYdat_comm,
                        GlobalVarLn.dZdat_comm,

                        ref GlobalVarLn.dLat_comm        // приращение по долготе, угл.сек
                    );
                // ................................................................. dLat

                // Lat,Long .............................................................
                // Преобразования широты и долготы при пересчете WGS84->SK42

                objClassMap1_ed.f_WGS84_SK42_Lat_Long
                       (
                    // Входные параметры (град,км)
                           xtmp1_ed,   // широта
                           ytmp1_ed,  // долгота
                           0,     // высота
                           GlobalVarLn.dLat_comm,       // приращение по долготе, угл.сек
                           GlobalVarLn.dLong_comm,      // приращение по долготе, угл.сек

                           // Выходные параметры (grad)
                           ref GlobalVarLn.LatKrG_comm,   // широта
                           ref GlobalVarLn.LongKrG_comm   // долгота
                       );
                // ............................................................ Lat,Long

            } // (GlobalVarLn.f42SP_sost == 0) && (GlobalVarLn.f42SP_G_sost == 0)77(GlobalVarLn.f84SP_G_sost==0)
            // *********************************** WGS84(эллипсоид)->элл.Красовского

            // .......................................................................
            // Эллипсоид Красовского, радианы

            GlobalVarLn.LatKrR_comm = (GlobalVarLn.LatKrG_comm * Math.PI) / 180;
            GlobalVarLn.LongKrR_comm = (GlobalVarLn.LongKrG_comm * Math.PI) / 180;
            // .......................................................................
            // Эллипсоид Красовского, grad,min,sec
            // dd.ddddd -> DD MM SS

            // Широта
            objClassMap3_ed.f_Grad_GMS
              (
                // Входные параметры (grad)
                GlobalVarLn.LatKrG_comm,

                // Выходные параметры 
                ref GlobalVarLn.Lat_Grad_comm,
                ref GlobalVarLn.Lat_Min_comm,
                ref GlobalVarLn.Lat_Sec_comm
              );

            // Долгота
            objClassMap3_ed.f_Grad_GMS
              (
                // Входные параметры (grad)
                GlobalVarLn.LongKrG_comm,

                // Выходные параметры 
                ref GlobalVarLn.Long_Grad_comm,
                ref GlobalVarLn.Long_Min_comm,
                ref GlobalVarLn.Long_Sec_comm
              );
            // .......................................................................

            // SK42(элл.)->Крюгер ****************************************************
            // Преобразование геодезических координат (широта, долгота, высота) 
            // эллипсоида Красовского (СК42) в плоские прямоугольные координаты в
            // проекции Гаусса-Крюгера
            // Входные параметры -> !!!grad

            if (GlobalVarLn.f42SP_sost == 0)
            {
                objClassMap3_ed.f_SK42_Krug
                       (
                    // Входные параметры (!!! grad)
                    // !!! эллипсоид Красовского
                           GlobalVarLn.LatKrG_comm,   // широта
                           GlobalVarLn.LongKrG_comm,  // долгота

                           // Выходные параметры (km)
                           ref GlobalVarLn.XSP42_comm,
                           ref GlobalVarLn.YSP42_comm
                       );

                // km->m
                GlobalVarLn.XSP42_comm = GlobalVarLn.XSP42_comm * 1000;
                GlobalVarLn.YSP42_comm = GlobalVarLn.YSP42_comm * 1000;

            } // GlobalVarLn.f42SP_sost == 0
            // **************************************************** SK42(элл.)->Крюгер

            // .......................................................................
            // Отобразить координаты в окошках

            ClassMap.OtobrSP1();
            // .......................................................................

            // 13_09_2018
            if (ClickGetGNSS != null)
            {
                if (GlobalVarLn.HCenter_Sost == -111111)
                    GlobalVarLn.HCenter_Sost = -11111;
                ClickGetGNSS(GlobalVarLn.BWGS84_1_sost, GlobalVarLn.LWGS84_1_sost, GlobalVarLn.HCenter_Sost);
            }

        }  // SP1
        // ************************************************************************

        // ************************************************************************
        // ВыборSP2
        // ************************************************************************
        private void button6_Click(object sender, EventArgs e)
        {
            double xtmp_ed, ytmp_ed, htmp_ed;
            double xtmp1_ed, ytmp1_ed;

            xtmp_ed = 0;
            ytmp_ed = 0;
            htmp_ed = 0;
            xtmp1_ed = 0;
            ytmp1_ed = 0;
            // ......................................................................
            ClassMap objClassMap4_ed = new ClassMap();
            ClassMap objClassMap5_ed = new ClassMap();
            ClassMap objClassMap6_ed = new ClassMap();

            LF objLF = new LF();
            // ......................................................................

            // --------------------------------------------------------------------------------------
            // 19_09_2018
            // Для пеленгов

            // 23_10_2018
/*
            // Нажата птичка для отрисовки пеленгов
            if (GlobalVarLn.flPelMain == 1)
            {
                // FRCH
                GlobalVarLn.PrevP2 = -1;
                GlobalVarLn.flPelMain2 = 0;   // 2-й пеленг
                GlobalVarLn.flag_eq_draw2 = 0;
                //------------------------------------------------------------------------
                // PPRCH
                for (int iii = (GlobalVarLn.list_PelIRI.Count - 1); iii >= 0; iii--)
                {


                    PelIRI[] mass = GlobalVarLn.list_PelIRI.ToArray();
                    mass[iii].Pel2 = -1;
                    GlobalVarLn.list_PelIRI = mass.ToList();

                    if (
                        (GlobalVarLn.list_PelIRI[iii].Pel1 == -1) &&
                        (GlobalVarLn.list_PelIRI[iii].Pel2 == -1) &&
                        (GlobalVarLn.list_PelIRI[iii].Lat == -1) &&
                        (GlobalVarLn.list_PelIRI[iii].Long == -1)
                       )
                    {
                        GlobalVarLn.list_PelIRI.Remove(GlobalVarLn.list_PelIRI[iii]);
                    }
                }
                //------------------------------------------------------------------------

            } // Нажата птичка для отрисовки пеленгов
 */ 
            // --------------------------------------------------------------------------------------

            // ......................................................................
            // !!! реальные координаты на местности карты в м (Plane)

            GlobalVarLn.XPoint1_Sost = GlobalVarLn.MapX1;
            GlobalVarLn.YPoint1_Sost = GlobalVarLn.MapY1;
            // ......................................................................

            // Ручной ввод

            // RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
            // Ручной ввод

            if (chbXY1.Checked == true)
            {
                String s1 = "";
                // ----------------------------------------------------------------------
                // CK42

                GlobalVarLn.f42SP1_sost = 0;
                GlobalVarLn.f42SP1_G_sost = 0;

                if (gbPt1Rect42.Visible == true)
                {

                    // .............................................................................
                    if ((tbPt1XRect42.Text == "") || (tbPt1YRect42.Text == ""))
                    {
                        if (GlobalVarLn.fl_Azb == 0)
                        {
                            MessageBox.Show("Недопустимые координаты сопряженной АСП");
                        }
                        else
                        {
                            //Azb???
                            MessageBox.Show("No mənşəyi (Beraber işleyen manee stansiyası");
                        }
                        return;
                    }

                    GlobalVarLn.f42SP1_sost = 1;
                    // .............................................................................
                    // Гаусс_Крюгер

                    s1 = tbPt1XRect42.Text;   // X
                    try
                    {
                        //if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                        GlobalVarLn.XYS142_comm = Convert.ToDouble(s1);
                    }
                    catch (SystemException)
                    {
                        try
                        {
                            if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                            else s1 = s1.Replace('.', ',');
                            GlobalVarLn.XYS142_comm = Convert.ToDouble(s1);
                        }
                        catch
                        {
                            if (GlobalVarLn.fl_Azb == 0)
                            {
                                MessageBox.Show("Недопустимые координаты сопряженной АСП");
                            }
                            else
                            {
                                //Azb???
                                MessageBox.Show("No mənşəyi (Beraber işleyen manee stansiyası");
                            }
                            return;
                        }

                    }

                    s1 = tbPt1YRect42.Text;   // Y
                    try
                    {
                        //if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                        GlobalVarLn.YYS142_comm = Convert.ToDouble(s1);
                    }
                    catch (SystemException)
                    {
                        try
                        {
                            if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                            else s1 = s1.Replace('.', ',');
                            GlobalVarLn.YYS142_comm = Convert.ToDouble(s1);
                        }
                        catch
                        {
                            if (GlobalVarLn.fl_Azb == 0)
                            {
                                MessageBox.Show("Недопустимые координаты сопряженной АСП");
                            }
                            else
                            {
                                //Azb???
                                MessageBox.Show("No mənşəyi (Beraber işleyen manee stansiyası");
                            }
                            return;
                        }

                    }
                    // .............................................................................
                    // Эллипсоид Красовского (grad)

                    objClassMap6_ed.f_Krug_SK42
                    (
                        // Входные параметры (км)
                     GlobalVarLn.XYS142_comm / 1000,
                     GlobalVarLn.YYS142_comm / 1000,

                     // Выходные параметры (grad)
                     ref GlobalVarLn.LatKrG_YS1_comm,   // широта
                     ref GlobalVarLn.LongKrG_YS1_comm   // долгота
                     );
                    // .............................................................................
                    // м на местности (ГИС)

                    GlobalVarLn.XPoint1GRAD_Sost = GlobalVarLn.LatKrG_YS1_comm;
                    GlobalVarLn.YPoint1GRAD_Sost = GlobalVarLn.LongKrG_YS1_comm;

                    // grad->rad
                    GlobalVarLn.XPoint1GRAD_Sost = (GlobalVarLn.XPoint1GRAD_Sost * Math.PI) / 180;
                    GlobalVarLn.YPoint1GRAD_Sost = (GlobalVarLn.YPoint1GRAD_Sost * Math.PI) / 180;

                    // Подаем rad, получаем там же расстояние на карте в м
                    mapGeoToPlane(GlobalVarLn.hmapl, ref GlobalVarLn.XPoint1GRAD_Sost, ref GlobalVarLn.YPoint1GRAD_Sost);

                    GlobalVarLn.XPoint1_Sost = GlobalVarLn.XPoint1GRAD_Sost;
                    GlobalVarLn.YPoint1_Sost = GlobalVarLn.YPoint1GRAD_Sost;
                    // .............................................................................

                } // gbPt1Rect42.Visible==true

                // ----------------------------------------------------------------------
                // Градусы

                else if (gbPt1DegMin.Visible == true)
                {

                    if ((tbPt1BMin1.Text == "") || (tbPt1LMin1.Text == ""))
                    {
                        if (GlobalVarLn.fl_Azb == 0)
                        {
                            MessageBox.Show("Недопустимые координаты сопряженной АСП");
                        }
                        else
                        {
                            //Azb???
                            MessageBox.Show("No mənşəyi (Beraber işleyen manee stansiyası");
                        }
                        return;
                    }

                    GlobalVarLn.f42SP1_G_sost = 1;

                    s1 = tbPt1BMin1.Text;   //Lat
                    try
                    {
                        //if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                        GlobalVarLn.XPoint1GRAD_Sost = Convert.ToDouble(s1);
                    }
                    catch (SystemException)
                    {
                        try
                        {
                            if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                            else s1 = s1.Replace('.', ',');
                            GlobalVarLn.XPoint1GRAD_Sost = Convert.ToDouble(s1);
                        }
                        catch
                        {
                            if (GlobalVarLn.fl_Azb == 0)
                            {
                                MessageBox.Show("Недопустимые координаты сопряженной АСП");
                            }
                            else
                            {
                                //Azb???
                                MessageBox.Show("No mənşəyi (Beraber işleyen manee stansiyası");
                            }
                            return;
                        }

                    }

                    s1 = tbPt1LMin1.Text;   // Long
                    try
                    {
                        //if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                        GlobalVarLn.YPoint1GRAD_Sost = Convert.ToDouble(s1);
                    }
                    catch (SystemException)
                    {
                        try
                        {
                            if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                            else s1 = s1.Replace('.', ',');
                            GlobalVarLn.YPoint1GRAD_Sost = Convert.ToDouble(s1);
                        }
                        catch
                        {
                            if (GlobalVarLn.fl_Azb == 0)
                            {
                                MessageBox.Show("Недопустимые координаты сопряженной АСП");
                            }
                            else
                            {
                                //Azb???
                                MessageBox.Show("No mənşəyi (Beraber işleyen manee stansiyası");
                            }
                            return;
                        }

                    }

                    // Krasovsky (grad)
                    GlobalVarLn.LatKrG_YS1_comm = GlobalVarLn.XPoint1GRAD_Sost;   // широта
                    GlobalVarLn.LongKrG_YS1_comm = GlobalVarLn.YPoint1GRAD_Sost;   // долгота

                    // grad->rad
                    GlobalVarLn.XPoint1GRAD_Sost = (GlobalVarLn.XPoint1GRAD_Sost * Math.PI) / 180;
                    GlobalVarLn.YPoint1GRAD_Sost = (GlobalVarLn.YPoint1GRAD_Sost * Math.PI) / 180;

                    // Подаем rad, получаем там же расстояние на карте в м
                    mapGeoToPlane(GlobalVarLn.hmapl, ref GlobalVarLn.XPoint1GRAD_Sost, ref GlobalVarLn.YPoint1GRAD_Sost);

                    GlobalVarLn.XPoint1_Sost = GlobalVarLn.XPoint1GRAD_Sost;
                    GlobalVarLn.YPoint1_Sost = GlobalVarLn.YPoint1GRAD_Sost;

                } // gbOwnDegMinSec.Visible == true
                // ----------------------------------------------------------------------
                //999
                // Градусы WGS84

                else if (gbPt1Rect.Visible == true)
                {

                    if ((tbPt1XRect.Text == "") || (tbPt1YRect.Text == ""))
                    {
                        if (GlobalVarLn.fl_Azb == 0)
                        {
                            MessageBox.Show("Недопустимые координаты сопряженной АСП ");
                        }
                        else
                        {
                            //Azb???
                            MessageBox.Show("No mənşəyi (Beraber işleyen manee stansiyası");
                        }
                        return;
                    }

                    GlobalVarLn.f84SP1_G_sost = 1;

                    s1 = tbPt1XRect.Text;   //Lat
                    try
                    {
                        //if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                        xtmp1_ed = Convert.ToDouble(s1);
                    }
                    catch (SystemException)
                    {
                        try
                        {
                            if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                            else s1 = s1.Replace('.', ',');
                            xtmp1_ed = Convert.ToDouble(s1);
                        }
                        catch
                        {
                            if (GlobalVarLn.fl_Azb == 0)
                            {
                                MessageBox.Show("Недопустимые координаты сопряженной АСП");
                            }
                            else
                            {
                                //Azb???
                                MessageBox.Show("No mənşəyi (Beraber işleyen manee stansiyası");
                            }
                            return;
                        }

                    }

                    s1 = tbPt1YRect.Text;   // Long
                    try
                    {
                        //if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                        ytmp1_ed = Convert.ToDouble(s1);
                    }
                    catch (SystemException)
                    {
                        try
                        {
                            if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                            else s1 = s1.Replace('.', ',');
                            ytmp1_ed = Convert.ToDouble(s1);
                        }
                        catch
                        {
                            if (GlobalVarLn.fl_Azb == 0)
                            {
                                MessageBox.Show("Недопустимые координаты сопряженной АСП");
                            }
                            else
                            {
                                //Azb???
                                MessageBox.Show("No mənşəyi (Beraber işleyen manee stansiyası");
                            }
                            return;
                        }

                    }

                    // WGS84_GRAD
                    GlobalVarLn.BWGS84_2_sost = xtmp1_ed;
                    GlobalVarLn.LWGS84_2_sost = ytmp1_ed;

                    // WGS84(эллипсоид)->элл.Красовского *************************************
                    // Перевод координат WGS84(эллипсоид) -> эллипсоид Красовского(Пулково-42)
                    // WGS84(широта,долгота)из Панорамы пересчитываем в эллипсод Красовского моей функцией
                    // Входные параметры -> град,km
                    // Перевод в рад - внутри функции


                    // dLong ..................................................................
                    // Расчет приращения по долготе при преобразованиях координат WGS84<->SK42
                    // (преобразования Молоденского), угл.сек

                    objClassMap4_ed.f_dLong
                        (
                        // Входные параметры (град,км)
                            xtmp1_ed,   // широта
                            ytmp1_ed,  // долгота
                            0,     // высота

                            // DATUM,m
                            GlobalVarLn.dXdat_comm,
                            GlobalVarLn.dYdat_comm,
                            GlobalVarLn.dZdat_comm,

                            ref GlobalVarLn.dLong_comm   // приращение по долготе, угл.сек
                        );
                    // ................................................................ dLong

                    // dLat .................................................................
                    // Расчет приращения по широте при преобразованиях координат WGS84<->SK42
                    // (преобразования Молоденского), угл.сек

                    objClassMap4_ed.f_dLat
                        (
                        // Входные параметры (град,км)
                            xtmp1_ed,   // широта
                            ytmp1_ed,  // долгота
                            0,     // высота

                            // DATUM,m
                            GlobalVarLn.dXdat_comm,
                            GlobalVarLn.dYdat_comm,
                            GlobalVarLn.dZdat_comm,

                            ref GlobalVarLn.dLat_comm        // приращение по долготе, угл.сек
                        );
                    // ................................................................. dLat

                    // Lat,Long .............................................................
                    // Преобразования широты и долготы при пересчете WGS84->SK42

                    objClassMap4_ed.f_WGS84_SK42_Lat_Long
                           (
                        // Входные параметры (град,км)
                               xtmp1_ed,   // широта
                               ytmp1_ed,  // долгота
                               0,     // высота
                               GlobalVarLn.dLat_comm,       // приращение по долготе, угл.сек
                               GlobalVarLn.dLong_comm,      // приращение по долготе, угл.сек

                               // Выходные параметры (grad)
                               ref GlobalVarLn.LatKrG_YS1_comm,   // широта
                               ref GlobalVarLn.LongKrG_YS1_comm   // долгота
                           );
                    // ............................................................ Lat,Long

                    // *********************************** WGS84(эллипсоид)->элл.Красовского

                    double lt1 = 0;
                    double lng1 = 0;

                    // grad->rad
                    lt1 = (GlobalVarLn.LatKrG_YS1_comm * Math.PI) / 180;
                    lng1 = (GlobalVarLn.LongKrG_YS1_comm * Math.PI) / 180;

                    // Подаем rad, получаем там же расстояние на карте в м
                    mapGeoToPlane(GlobalVarLn.hmapl, ref lt1, ref lng1);

                    GlobalVarLn.XPoint1_Sost = lt1;
                    GlobalVarLn.YPoint1_Sost = lng1;

                }  // WGS84
                // ----------------------------------------------------------------------


            } // Ручной ввод

            else
            {
                GlobalVarLn.f42SP1_sost = 0;
                GlobalVarLn.f42SP1_G_sost = 0;
                GlobalVarLn.f84SP1_G_sost = 0;
            }
            // RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR


            // ......................................................................
            xtmp_ed = GlobalVarLn.XPoint1_Sost;
            ytmp_ed = GlobalVarLn.YPoint1_Sost;

            objLF.X_m = xtmp_ed;
            objLF.Y_m = ytmp_ed;
            // ......................................................................
            // H

            GlobalVarLn.axMapPointGlobalAdd.SetPoint(xtmp_ed, ytmp_ed);
            GlobalVarLn.HPoint1_Sost = (int)GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);
            htmp_ed = GlobalVarLn.HPoint1_Sost;
            tbPt1Height.Text = Convert.ToString(htmp_ed);
            objLF.H_m = htmp_ed;
            // ......................................................................
            // Sign

            objLF.indzn = GlobalVarLn.indz2_Sost;
            // ......................................................................
            // List

            GlobalVarLn.list1_Sost[1] = objLF;
            // ......................................................................
            GlobalVarLn.flCoordYS1_Sost = 1; // SP2 выбрана

            // 13_09_2018
            GlobalVarLn.fl_Sost = 1;

            // 14_09_2018
            // Peleng, FRCH
            if (GlobalVarLn.flPelMain == 1) // нажата птичка отрисовки пеленга
                GlobalVarLn.flag_eq_draw2 = 1;
            // ......................................................................
            // Draw SP2

            // Убрать с карты
            GlobalVarLn.axMapScreenGlobal.Repaint();
            // Redraw
            ClassMap.f_Map_Redraw_Sost();
            // ......................................................................
            //GlobalVarLn.flCoordYS1_Sost = 1; // YS1 выбран
            // ......................................................................
            // Реальные координаты карты в м -> в долготу и широту WGS84 с помощью функций Панорамы
            // !!! Выход функции(rad) идет на место входных переменных

            //999
            if (GlobalVarLn.f84SP1_G_sost == 0) // не ручной ввод
            {
                mapPlaneToGeoWGS84(GlobalVarLn.hmapl, ref xtmp_ed, ref ytmp_ed);

                // rad(WGS84)->grad(WGS84)
                xtmp1_ed = (xtmp_ed * 180) / Math.PI;
                ytmp1_ed = (ytmp_ed * 180) / Math.PI;

                //333
                GlobalVarLn.BWGS84_2_sost = xtmp1_ed;
                GlobalVarLn.LWGS84_2_sost = ytmp1_ed;

                //777
                GlobalVarLn.XPoint1GRAD_Dubl_Sost = xtmp1_ed;
                GlobalVarLn.YPoint1GRAD_Dubl_Sost = ytmp1_ed;

            }
            // .......................................................................

            // WGS84(эллипсоид)->элл.Красовского *************************************
            // Перевод координат WGS84(эллипсоид) -> эллипсоид Красовского(Пулково-42)
            // WGS84(широта,долгота)из Панорамы пересчитываем в эллипсод Красовского моей функцией
            // Входные параметры -> град,km
            // Перевод в рад - внутри функции

            //999
            if ((GlobalVarLn.f42SP1_sost == 0) && (GlobalVarLn.f42SP1_G_sost == 0) && (GlobalVarLn.f84SP1_G_sost == 0))
            {

                // dLong ..................................................................
                // Расчет приращения по долготе при преобразованиях координат WGS84<->SK42
                // (преобразования Молоденского), угл.сек

                objClassMap4_ed.f_dLong
                    (
                    // Входные параметры (град,км)
                        xtmp1_ed,   // широта
                        ytmp1_ed,  // долгота
                        0,     // высота

                        // DATUM,m
                        GlobalVarLn.dXdat_comm,
                        GlobalVarLn.dYdat_comm,
                        GlobalVarLn.dZdat_comm,

                        ref GlobalVarLn.dLong_comm   // приращение по долготе, угл.сек
                    );
                // ................................................................ dLong

                // dLat .................................................................
                // Расчет приращения по широте при преобразованиях координат WGS84<->SK42
                // (преобразования Молоденского), угл.сек

                objClassMap4_ed.f_dLat
                    (
                    // Входные параметры (град,км)
                        xtmp1_ed,   // широта
                        ytmp1_ed,  // долгота
                        0,     // высота

                        // DATUM,m
                        GlobalVarLn.dXdat_comm,
                        GlobalVarLn.dYdat_comm,
                        GlobalVarLn.dZdat_comm,

                        ref GlobalVarLn.dLat_comm        // приращение по долготе, угл.сек
                    );
                // ................................................................. dLat

                // Lat,Long .............................................................
                // Преобразования широты и долготы при пересчете WGS84->SK42

                objClassMap4_ed.f_WGS84_SK42_Lat_Long
                       (
                    // Входные параметры (град,км)
                           xtmp1_ed,   // широта
                           ytmp1_ed,  // долгота
                           0,     // высота
                           GlobalVarLn.dLat_comm,       // приращение по долготе, угл.сек
                           GlobalVarLn.dLong_comm,      // приращение по долготе, угл.сек

                           // Выходные параметры (grad)
                           ref GlobalVarLn.LatKrG_YS1_comm,   // широта
                           ref GlobalVarLn.LongKrG_YS1_comm   // долгота
                       );
                // ............................................................ Lat,Long

            } // (GlobalVarLn.f42SP1_sost == 0) && (GlobalVarLn.f42SP1_G_sost == 0)
            // *********************************** WGS84(эллипсоид)->элл.Красовского

            // .......................................................................
            // Эллипсоид Красовского, радианы

            //LatKrR_YS1_comm = (LatKrG_YS1_comm * Math.PI) / 180;
            //LongKrR_YS1_comm = (LongKrG_YS1_comm * Math.PI) / 180;

            GlobalVarLn.LatKrR_YS1_comm = (GlobalVarLn.LatKrG_YS1_comm * Math.PI) / 180;
            GlobalVarLn.LongKrR_YS1_comm = (GlobalVarLn.LongKrG_YS1_comm * Math.PI) / 180;
            // .......................................................................
            // Эллипсоид Красовского, grad,min,sec
            // dd.ddddd -> DD MM SS

            // Широта
            objClassMap5_ed.f_Grad_GMS
              (
                // Входные параметры (grad)
                GlobalVarLn.LatKrG_YS1_comm,

                // Выходные параметры 
                ref GlobalVarLn.Lat_Grad_YS1_comm,
                ref GlobalVarLn.Lat_Min_YS1_comm,
                ref GlobalVarLn.Lat_Sec_YS1_comm
              );

            // Долгота
            objClassMap5_ed.f_Grad_GMS
              (
                // Входные параметры (grad)
                GlobalVarLn.LongKrG_YS1_comm,

                // Выходные параметры 
                ref GlobalVarLn.Long_Grad_YS1_comm,
                ref GlobalVarLn.Long_Min_YS1_comm,
                ref GlobalVarLn.Long_Sec_YS1_comm
              );
            // .......................................................................

            // SK42(элл.)->Крюгер ****************************************************
            // Преобразование геодезических координат (широта, долгота, высота) 
            // эллипсоида Красовского (СК42) в плоские прямоугольные координаты в
            // проекции Гаусса-Крюгера
            // Входные параметры -> !!!grad

            if (GlobalVarLn.f42SP1_sost == 0)
            {

                objClassMap6_ed.f_SK42_Krug
                       (
                    // Входные параметры (!!! grad)
                    // !!! эллипсоид Красовского
                           GlobalVarLn.LatKrG_YS1_comm,   // широта
                           GlobalVarLn.LongKrG_YS1_comm,  // долгота

                           // Выходные параметры (km)
                           ref GlobalVarLn.XYS142_comm,
                           ref GlobalVarLn.YYS142_comm
                       );

                // km->m
                GlobalVarLn.XYS142_comm = GlobalVarLn.XYS142_comm * 1000;
                GlobalVarLn.YYS142_comm = GlobalVarLn.YYS142_comm * 1000;

            } // GlobalVarLn.f42SP1_sost == 0
            // **************************************************** SK42(элл.)->Крюгер

            // .......................................................................
            // Отобразить координаты в окошках

            //OtobrYS1_Sost();
            ClassMap.OtobrSP2();
            // .......................................................................

            // 13_09_2018
            if (ClickGetGNSS2 != null)
            {
                if (GlobalVarLn.HPoint1_Sost == -111111)
                    GlobalVarLn.HPoint1_Sost = -11111;

                ClickGetGNSS2(GlobalVarLn.BWGS84_2_sost, GlobalVarLn.LWGS84_2_sost, GlobalVarLn.HPoint1_Sost);
            }

        } // SP2
        // ************************************************************************

        // ************************************************************************
        // PU
        // ************************************************************************

        private void button7_Click(object sender, EventArgs e)
        {
            double xtmp_ed, ytmp_ed, htmp_ed;
            double xtmp1_ed, ytmp1_ed;

            xtmp_ed = 0;
            ytmp_ed = 0;
            htmp_ed = 0;
            xtmp1_ed = 0;
            ytmp1_ed = 0;
            // ......................................................................
            ClassMap objClassMap7_ed = new ClassMap();
            ClassMap objClassMap8_ed = new ClassMap();
            ClassMap objClassMap9_ed = new ClassMap();

            LF objLF = new LF();
            // ......................................................................
            // !!! реальные координаты на местности карты в м (Plane)

            GlobalVarLn.XPoint2_Sost = GlobalVarLn.MapX1;
            GlobalVarLn.YPoint2_Sost = GlobalVarLn.MapY1;
            // ......................................................................

            // RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
            // Ручной ввод

            if (chbXY1.Checked == true)
            {
                String s1 = "";

                // ----------------------------------------------------------------------
                // CK42

                GlobalVarLn.f42PU_sost = 0;
                GlobalVarLn.f42PU_G_sost = 0;

                if (gbPt2Rect42.Visible == true)
                {

                    // .............................................................................
                    if ((tbPt2XRect42.Text == "") || (tbPt2YRect42.Text == ""))
                    {
                        if (GlobalVarLn.fl_Azb == 0)
                        {
                            MessageBox.Show("Недопустимые координаты ПУ");
                        }
                        else
                        {
                            //Azb???
                            MessageBox.Show("No mənşəyi (idareetme pultu)");
                        }
                        return;
                    }

                    GlobalVarLn.f42PU_sost = 1;
                    // .............................................................................
                    // Гаусс_Крюгер

                    s1 = tbPt2XRect42.Text;   // X
                    try
                    {
                        //if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                        GlobalVarLn.XYS242_comm = Convert.ToDouble(s1);
                    }
                    catch (SystemException)
                    {
                        try
                        {
                            if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                            else s1 = s1.Replace('.', ',');
                            GlobalVarLn.XYS242_comm = Convert.ToDouble(s1);
                        }
                        catch
                        {
                            if (GlobalVarLn.fl_Azb == 0)
                            {
                                MessageBox.Show("Недопустимые координаты ПУ");
                            }
                            else
                            {
                                //Azb???
                                MessageBox.Show("No mənşəyi (idareetme pultu)");
                            }
                            return;
                        }

                    }

                    s1 = tbPt2YRect42.Text;   // Y
                    try
                    {
                        //if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                        GlobalVarLn.YYS242_comm = Convert.ToDouble(s1);
                    }
                    catch (SystemException)
                    {
                        try
                        {
                            if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                            else s1 = s1.Replace('.', ',');
                            GlobalVarLn.YYS242_comm = Convert.ToDouble(s1);
                        }
                        catch
                        {
                            if (GlobalVarLn.fl_Azb == 0)
                            {
                                MessageBox.Show("Недопустимые координаты ПУ");
                            }
                            else
                            {
                                //Azb???
                                MessageBox.Show("No mənşəyi (idareetme pultu)");
                            }
                            return;
                        }

                    }
                    // .............................................................................
                    // Эллипсоид Красовского (grad)

                    objClassMap9_ed.f_Krug_SK42
                    (
                        // Входные параметры (км)
                     GlobalVarLn.XYS242_comm / 1000,
                     GlobalVarLn.YYS242_comm / 1000,

                     // Выходные параметры (grad)
                     ref GlobalVarLn.LatKrG_YS2_comm,   // широта
                     ref GlobalVarLn.LongKrG_YS2_comm   // долгота
                     );
                    // .............................................................................
                    // м на местности (ГИС)

                    GlobalVarLn.XPoint2GRAD_Sost = GlobalVarLn.LatKrG_YS2_comm;
                    GlobalVarLn.YPoint2GRAD_Sost = GlobalVarLn.LongKrG_YS2_comm;

                    // grad->rad
                    GlobalVarLn.XPoint2GRAD_Sost = (GlobalVarLn.XPoint2GRAD_Sost * Math.PI) / 180;
                    GlobalVarLn.YPoint2GRAD_Sost = (GlobalVarLn.YPoint2GRAD_Sost * Math.PI) / 180;

                    // Подаем rad, получаем там же расстояние на карте в м
                    mapGeoToPlane(GlobalVarLn.hmapl, ref GlobalVarLn.XPoint2GRAD_Sost, ref GlobalVarLn.YPoint2GRAD_Sost);

                    GlobalVarLn.XPoint2_Sost = GlobalVarLn.XPoint2GRAD_Sost;
                    GlobalVarLn.YPoint2_Sost = GlobalVarLn.YPoint2GRAD_Sost;
                    // .............................................................................

                } // gbPt2Rect42.Visible==true

                // ----------------------------------------------------------------------
                // Градусы

                else if (gbPt2DegMin.Visible == true)
                {

                    if ((tbPt2BMin1.Text == "") || (tbPt2LMin1.Text == ""))
                    {
                        if (GlobalVarLn.fl_Azb == 0)
                        {
                            MessageBox.Show("Недопустимые координаты ПУ");
                        }
                        else
                        {
                            //Azb???
                            MessageBox.Show("No mənşəyi (idareetme pultu)");
                        }
                        return;
                    }

                    GlobalVarLn.f42PU_G_sost = 1;

                    s1 = tbPt2BMin1.Text;   //Lat
                    try
                    {
                        //if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                        GlobalVarLn.XPoint2GRAD_Sost = Convert.ToDouble(s1);
                    }
                    catch (SystemException)
                    {
                        try
                        {
                            if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                            else s1 = s1.Replace('.', ',');
                            GlobalVarLn.XPoint2GRAD_Sost = Convert.ToDouble(s1);
                        }
                        catch
                        {
                            if (GlobalVarLn.fl_Azb == 0)
                            {
                                MessageBox.Show("Недопустимые координаты ПУ");
                            }
                            else
                            {
                                //Azb???
                                MessageBox.Show("No mənşəyi (idareetme pultu)");
                            }
                            return;
                        }

                    }

                    s1 = tbPt2LMin1.Text;   // Long
                    try
                    {
                        //if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                        GlobalVarLn.YPoint2GRAD_Sost = Convert.ToDouble(s1);
                    }
                    catch (SystemException)
                    {
                        try
                        {
                            if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                            else s1 = s1.Replace('.', ',');
                            GlobalVarLn.YPoint2GRAD_Sost = Convert.ToDouble(s1);
                        }
                        catch
                        {
                            if (GlobalVarLn.fl_Azb == 0)
                            {
                                MessageBox.Show("Недопустимые координаты ПУ");
                            }
                            else
                            {
                                //Azb???
                                MessageBox.Show("No mənşəyi (idareetme pultu)");
                            }
                            return;
                        }

                    }

                    // Krasovsky (grad)
                    GlobalVarLn.LatKrG_YS2_comm = GlobalVarLn.XPoint2GRAD_Sost;   // широта
                    GlobalVarLn.LongKrG_YS2_comm = GlobalVarLn.YPoint2GRAD_Sost;   // долгота

                    // grad->rad
                    GlobalVarLn.XPoint2GRAD_Sost = (GlobalVarLn.XPoint2GRAD_Sost * Math.PI) / 180;
                    GlobalVarLn.YPoint2GRAD_Sost = (GlobalVarLn.YPoint2GRAD_Sost * Math.PI) / 180;

                    // Подаем rad, получаем там же расстояние на карте в м
                    mapGeoToPlane(GlobalVarLn.hmapl, ref GlobalVarLn.XPoint2GRAD_Sost, ref GlobalVarLn.YPoint2GRAD_Sost);

                    GlobalVarLn.XPoint2_Sost = GlobalVarLn.XPoint2GRAD_Sost;
                    GlobalVarLn.YPoint2_Sost = GlobalVarLn.YPoint2GRAD_Sost;

                } // gbOwnDegMinSec.Visible == true
                // ----------------------------------------------------------------------
                //999
                // Градусы WGS84

                else if (gbPt2Rect.Visible == true)
                {

                    if ((tbPt2XRect.Text == "") || (tbPt2YRect.Text == ""))
                    {
                        if (GlobalVarLn.fl_Azb == 0)
                        {
                            MessageBox.Show("Недопустимые координаты ПУ ");
                        }
                        else
                        {
                            //Azb???
                            MessageBox.Show("No mənşəyi (idareetme pultu)");
                        }
                        return;
                    }

                    GlobalVarLn.f84PU_G_sost = 1;

                    s1 = tbPt2XRect.Text;   //Lat
                    try
                    {
                        //if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                        xtmp1_ed = Convert.ToDouble(s1);
                    }
                    catch (SystemException)
                    {
                        try
                        {
                            if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                            else s1 = s1.Replace('.', ',');
                            xtmp1_ed = Convert.ToDouble(s1);
                        }
                        catch
                        {
                            if (GlobalVarLn.fl_Azb == 0)
                            {
                                MessageBox.Show("Недопустимые координаты ПУ");
                            }
                            else
                            {
                                //Azb???
                                MessageBox.Show("No mənşəyi (idareetme pultu)");
                            }
                            return;
                        }

                    }

                    s1 = tbPt2YRect.Text;   // Long
                    try
                    {
                        //if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                        ytmp1_ed = Convert.ToDouble(s1);
                    }
                    catch (SystemException)
                    {
                        try
                        {
                            if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                            else s1 = s1.Replace('.', ',');
                            ytmp1_ed = Convert.ToDouble(s1);
                        }
                        catch
                        {
                            if (GlobalVarLn.fl_Azb == 0)
                            {
                                MessageBox.Show("Недопустимые координаты ПУ");
                            }
                            else
                            {
                                //Azb???
                                MessageBox.Show("No mənşəyi (idareetme pultu)");
                            }
                            return;
                        }

                    }

                    // WGS84_GRAD
                    GlobalVarLn.BWGS84_3_sost = xtmp1_ed;
                    GlobalVarLn.LWGS84_3_sost = ytmp1_ed;

                    // WGS84(эллипсоид)->элл.Красовского *************************************
                    // Перевод координат WGS84(эллипсоид) -> эллипсоид Красовского(Пулково-42)
                    // WGS84(широта,долгота)из Панорамы пересчитываем в эллипсод Красовского моей функцией
                    // Входные параметры -> град,km
                    // Перевод в рад - внутри функции


                    // dLong ..................................................................
                    // Расчет приращения по долготе при преобразованиях координат WGS84<->SK42
                    // (преобразования Молоденского), угл.сек

                    objClassMap7_ed.f_dLong
                        (
                        // Входные параметры (град,км)
                            xtmp1_ed,   // широта
                            ytmp1_ed,  // долгота
                            0,     // высота

                            // DATUM,m
                            GlobalVarLn.dXdat_comm,
                            GlobalVarLn.dYdat_comm,
                            GlobalVarLn.dZdat_comm,

                            ref GlobalVarLn.dLong_comm   // приращение по долготе, угл.сек
                        );
                    // ................................................................ dLong

                    // dLat .................................................................
                    // Расчет приращения по широте при преобразованиях координат WGS84<->SK42
                    // (преобразования Молоденского), угл.сек

                    objClassMap7_ed.f_dLat
                        (
                        // Входные параметры (град,км)
                            xtmp1_ed,   // широта
                            ytmp1_ed,  // долгота
                            0,     // высота

                            // DATUM,m
                            GlobalVarLn.dXdat_comm,
                            GlobalVarLn.dYdat_comm,
                            GlobalVarLn.dZdat_comm,

                            ref GlobalVarLn.dLat_comm        // приращение по долготе, угл.сек
                        );
                    // ................................................................. dLat

                    // Lat,Long .............................................................
                    // Преобразования широты и долготы при пересчете WGS84->SK42

                    objClassMap7_ed.f_WGS84_SK42_Lat_Long
                           (
                        // Входные параметры (град,км)
                               xtmp1_ed,   // широта
                               ytmp1_ed,  // долгота
                               0,     // высота
                               GlobalVarLn.dLat_comm,       // приращение по долготе, угл.сек
                               GlobalVarLn.dLong_comm,      // приращение по долготе, угл.сек

                               // Выходные параметры (grad)
                               ref GlobalVarLn.LatKrG_YS2_comm,   // широта
                               ref GlobalVarLn.LongKrG_YS2_comm   // долгота
                           );
                    // ............................................................ Lat,Long

                    // *********************************** WGS84(эллипсоид)->элл.Красовского

                    double lt1 = 0;
                    double lng1 = 0;

                    // grad->rad
                    lt1 = (GlobalVarLn.LatKrG_YS2_comm * Math.PI) / 180;
                    lng1 = (GlobalVarLn.LongKrG_YS2_comm * Math.PI) / 180;

                    // Подаем rad, получаем там же расстояние на карте в м
                    mapGeoToPlane(GlobalVarLn.hmapl, ref lt1, ref lng1);

                    GlobalVarLn.XPoint2_Sost = lt1;
                    GlobalVarLn.YPoint2_Sost = lng1;

                }  // WGS84
                // ----------------------------------------------------------------------


            } // Ручной ввод

            else
            {
                GlobalVarLn.f42PU_sost = 0;
                GlobalVarLn.f42PU_G_sost = 0;
                GlobalVarLn.f84PU_G_sost = 0;
            }

            // RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR

            // ......................................................................
            xtmp_ed = GlobalVarLn.XPoint2_Sost;
            ytmp_ed = GlobalVarLn.YPoint2_Sost;

            objLF.X_m = xtmp_ed;
            objLF.Y_m = ytmp_ed;
            // ......................................................................
            // H

            GlobalVarLn.axMapPointGlobalAdd.SetPoint(xtmp_ed, ytmp_ed);
            GlobalVarLn.HPoint2_Sost = (int)GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);
            htmp_ed = GlobalVarLn.HPoint2_Sost;
            tbPt2Height.Text = Convert.ToString(htmp_ed);
            objLF.H_m = htmp_ed;
            // ......................................................................
            // Sign

            objLF.indzn = GlobalVarLn.indz3_Sost;
            // ......................................................................
            // List

            GlobalVarLn.list1_Sost[2] = objLF;
            // ......................................................................
            GlobalVarLn.flCoordYS2_Sost = 1; // PU выбран

            // 13_09_2018
            GlobalVarLn.fl_Sost = 1;
            // ......................................................................
            // Draw PU


            // Убрать с карты
            GlobalVarLn.axMapScreenGlobal.Repaint();
            // Redraw
            ClassMap.f_Map_Redraw_Sost();
            // ......................................................................
            //GlobalVarLn.flCoordYS2_Sost = 1; // PU выбран
            // ......................................................................
            // Реальные координаты карты в м -> в долготу и широту WGS84 с помощью функций Панорамы
            // !!! Выход функции(rad) идет на место входных переменных

            //999
            if (GlobalVarLn.f84PU_G_sost == 0) // не ручной ввод
            {
                mapPlaneToGeoWGS84(GlobalVarLn.hmapl, ref xtmp_ed, ref ytmp_ed);

                // rad(WGS84)->grad(WGS84)
                xtmp1_ed = (xtmp_ed * 180) / Math.PI;
                ytmp1_ed = (ytmp_ed * 180) / Math.PI;

                //333
                GlobalVarLn.BWGS84_3_sost = xtmp1_ed;
                GlobalVarLn.LWGS84_3_sost = ytmp1_ed;

                //777
                GlobalVarLn.XPoint2GRAD_Dubl_Sost = xtmp1_ed;
                GlobalVarLn.YPoint2GRAD_Dubl_Sost = ytmp1_ed;

            }
            // .......................................................................


            // WGS84(эллипсоид)->элл.Красовского *************************************
            // Перевод координат WGS84(эллипсоид) -> эллипсоид Красовского(Пулково-42)
            // WGS84(широта,долгота)из Панорамы пересчитываем в эллипсод Красовского моей функцией
            // Входные параметры -> град,km
            // Перевод в рад - внутри функции

            //999
            if ((GlobalVarLn.f42PU_sost == 0) && (GlobalVarLn.f42PU_G_sost == 0) && (GlobalVarLn.f84PU_G_sost == 0))
            {

                // dLong ..................................................................
                // Расчет приращения по долготе при преобразованиях координат WGS84<->SK42
                // (преобразования Молоденского), угл.сек

                objClassMap7_ed.f_dLong
                    (
                    // Входные параметры (град,км)
                        xtmp1_ed,   // широта
                        ytmp1_ed,  // долгота
                        0,     // высота

                        // DATUM,m
                        GlobalVarLn.dXdat_comm,
                        GlobalVarLn.dYdat_comm,
                        GlobalVarLn.dZdat_comm,

                        ref GlobalVarLn.dLong_comm   // приращение по долготе, угл.сек
                    );
                // ................................................................ dLong

                // dLat .................................................................
                // Расчет приращения по широте при преобразованиях координат WGS84<->SK42
                // (преобразования Молоденского), угл.сек

                objClassMap7_ed.f_dLat
                    (
                    // Входные параметры (град,км)
                        xtmp1_ed,   // широта
                        ytmp1_ed,  // долгота
                        0,     // высота

                        // DATUM,m
                        GlobalVarLn.dXdat_comm,
                        GlobalVarLn.dYdat_comm,
                        GlobalVarLn.dZdat_comm,

                        ref GlobalVarLn.dLat_comm        // приращение по долготе, угл.сек
                    );
                // ................................................................. dLat

                // Lat,Long .............................................................
                // Преобразования широты и долготы при пересчете WGS84->SK42


                objClassMap7_ed.f_WGS84_SK42_Lat_Long
                       (
                    // Входные параметры (град,км)
                           xtmp1_ed,   // широта
                           ytmp1_ed,  // долгота
                           0,     // высота
                           GlobalVarLn.dLat_comm,       // приращение по долготе, угл.сек
                           GlobalVarLn.dLong_comm,      // приращение по долготе, угл.сек

                           // Выходные параметры (grad)
                           ref GlobalVarLn.LatKrG_YS2_comm,   // широта
                           ref GlobalVarLn.LongKrG_YS2_comm   // долгота
                       );
                // ............................................................ Lat,Long

            } // (GlobalVarLn.f42PU_sost == 0) && (GlobalVarLn.f42PU_G_sost == 0)
            // *********************************** WGS84(эллипсоид)->элл.Красовского

            // .......................................................................
            // Эллипсоид Красовского, радианы

            GlobalVarLn.LatKrR_YS2_comm = (GlobalVarLn.LatKrG_YS2_comm * Math.PI) / 180;
            GlobalVarLn.LongKrR_YS2_comm = (GlobalVarLn.LongKrG_YS2_comm * Math.PI) / 180;
            // .......................................................................
            // Эллипсоид Красовского, grad,min,sec
            // dd.ddddd -> DD MM SS

            // Широта
            objClassMap8_ed.f_Grad_GMS
              (
                // Входные параметры (grad)
                GlobalVarLn.LatKrG_YS2_comm,

                // Выходные параметры 
                ref GlobalVarLn.Lat_Grad_YS2_comm,
                ref GlobalVarLn.Lat_Min_YS2_comm,
                ref GlobalVarLn.Lat_Sec_YS2_comm
              );

            // Долгота
            objClassMap8_ed.f_Grad_GMS
              (
                // Входные параметры (grad)
                GlobalVarLn.LongKrG_YS2_comm,

                // Выходные параметры 
                ref GlobalVarLn.Long_Grad_YS2_comm,
                ref GlobalVarLn.Long_Min_YS2_comm,
                ref GlobalVarLn.Long_Sec_YS2_comm
              );
            // .......................................................................

            // SK42(элл.)->Крюгер ****************************************************
            // Преобразование геодезических координат (широта, долгота, высота) 
            // эллипсоида Красовского (СК42) в плоские прямоугольные координаты в
            // проекции Гаусса-Крюгера
            // Входные параметры -> !!!grad

            if (GlobalVarLn.f42PU_sost == 0)
            {

                objClassMap9_ed.f_SK42_Krug
                       (
                    // Входные параметры (!!! grad)
                    // !!! эллипсоид Красовского
                           GlobalVarLn.LatKrG_YS2_comm,   // широта
                           GlobalVarLn.LongKrG_YS2_comm,  // долгота

                           // Выходные параметры (km)
                           ref GlobalVarLn.XYS242_comm,
                           ref GlobalVarLn.YYS242_comm
                       );

                // km->m
                GlobalVarLn.XYS242_comm = GlobalVarLn.XYS242_comm * 1000;
                GlobalVarLn.YYS242_comm = GlobalVarLn.YYS242_comm * 1000;

            } // GlobalVarLn.f42PU_sost == 0
            // **************************************************** SK42(элл.)->Крюгер

            // .......................................................................
            // Отобразить координаты в окошках

            ClassMap.OtobrPU();
            // .......................................................................


            // INI *************************************************************************************
            if (System.IO.File.Exists(Application.StartupPath + "\\Settings.ini"))
            {
                GlobalVarLn.XCenter_Sost = (double)iniRW.get_X_ASP();
                GlobalVarLn.YCenter_Sost = (double)iniRW.get_Y_ASP();

                // SP1
                //iniRW.write_XY_ASP((int)GlobalVarLn.list1_Sost[0].X_m, (int)GlobalVarLn.list1_Sost[0].Y_m, (int)GlobalVarLn.list1_Sost[0].H_m, (int)GlobalVarLn.list1_Sost[0].indzn);
                // SP2
                //iniRW.write_XY_ASPS((int)GlobalVarLn.list1_Sost[1].X_m, (int)GlobalVarLn.list1_Sost[1].Y_m, (int)GlobalVarLn.list1_Sost[1].H_m, (int)GlobalVarLn.list1_Sost[1].indzn);
                // PU
                iniRW.write_XY_PU((int)GlobalVarLn.list1_Sost[2].X_m, (int)GlobalVarLn.list1_Sost[2].Y_m, (int)GlobalVarLn.list1_Sost[2].H_m, (int)GlobalVarLn.list1_Sost[2].indzn);

            }
            else
            {
                MessageBox.Show("Невозможно открыть INI файл");
                return;
            }

            // ************************************************************************************* INI


        } // PU
        // ************************************************************************

        // ************************************************************************
        // Для чтения из файла

/*
        private void f_PU_Sost()
        {
            double xtmp_ed, ytmp_ed;
            double xtmp1_ed, ytmp1_ed;


            xtmp_ed = 0;
            ytmp_ed = 0;
            xtmp1_ed = 0;
            ytmp1_ed = 0;

            // ......................................................................
            ClassMap objClassMap7_ed = new ClassMap();
            ClassMap objClassMap8_ed = new ClassMap();
            ClassMap objClassMap9_ed = new ClassMap();

            // ......................................................................
            // !!! реальные координаты на местности карты в м (Plane)

            XYS2_comm = GlobalVarLn.list1_Sost[2].X_m;
            YYS2_comm = GlobalVarLn.list1_Sost[2].Y_m;
            // ......................................................................

            xtmp_ed = XYS2_comm;
            ytmp_ed = YYS2_comm;

            GlobalVarLn.XPoint2_Sost = XYS2_comm;
            GlobalVarLn.YPoint2_Sost = YYS2_comm;
            Point2Height_comm = GlobalVarLn.list1_Sost[2].H_m;
            tbPt2Height.Text = Convert.ToString(Point2Height_comm);

            // ......................................................................
            GlobalVarLn.flCoordYS2_Sost = 1; // YS2 выбран
            // ......................................................................
            // Реальные координаты карты в м -> в долготу и широту WGS84 с помощью функций Панорамы
            // !!! Выход функции(rad) идет на место входных переменных

            xtmp_ed = XYS2_comm;
            ytmp_ed = YYS2_comm;

            mapPlaneToGeoWGS84(GlobalVarLn.hmapl, ref xtmp_ed, ref ytmp_ed);

            // rad(WGS84)->grad(WGS84)
            xtmp1_ed = (xtmp_ed * 180) / Math.PI;
            ytmp1_ed = (ytmp_ed * 180) / Math.PI;
            // .......................................................................


            // WGS84(эллипсоид)->элл.Красовского *************************************
            // Перевод координат WGS84(эллипсоид) -> эллипсоид Красовского(Пулково-42)
            // WGS84(широта,долгота)из Панорамы пересчитываем в эллипсод Красовского моей функцией
            // Входные параметры -> град,km
            // Перевод в рад - внутри функции

            // dLong ..................................................................
            // Расчет приращения по долготе при преобразованиях координат WGS84<->SK42
            // (преобразования Молоденского), угл.сек

            objClassMap7_ed.f_dLong
                (

                    // Входные параметры (град,км)
                    xtmp1_ed,   // широта
                    ytmp1_ed,  // долгота
                    0,     // высота

                    // DATUM,m
                    dXdat_comm,
                    dYdat_comm,
                    dZdat_comm,

                    ref dLong_comm   // приращение по долготе, угл.сек

                );

            // ................................................................ dLong

            // dLat .................................................................
            // Расчет приращения по широте при преобразованиях координат WGS84<->SK42
            // (преобразования Молоденского), угл.сек

            objClassMap7_ed.f_dLat
                (

                    // Входные параметры (град,км)
                    xtmp1_ed,   // широта
                    ytmp1_ed,  // долгота
                    0,     // высота

                    // DATUM,m
                    dXdat_comm,
                    dYdat_comm,
                    dZdat_comm,

                    ref dLat_comm        // приращение по долготе, угл.сек

                );

            // ................................................................. dLat

            // Lat,Long .............................................................
            // Преобразования широты и долготы при пересчете WGS84->SK42

            objClassMap7_ed.f_WGS84_SK42_Lat_Long
                   (

                       // Входные параметры (град,км)
                       xtmp1_ed,   // широта
                       ytmp1_ed,  // долгота
                       0,     // высота
                       dLat_comm,       // приращение по долготе, угл.сек
                       dLong_comm,      // приращение по долготе, угл.сек

                       // Выходные параметры (grad)
                       ref LatKrG_YS2_comm,   // широта
                       ref LongKrG_YS2_comm   // долгота

                   );

            // ............................................................ Lat,Long


            // *********************************** WGS84(эллипсоид)->элл.Красовского

            // .......................................................................
            // Эллипсоид Красовского, радианы

            LatKrR_YS2_comm = (LatKrG_YS2_comm * Math.PI) / 180;
            LongKrR_YS2_comm = (LongKrG_YS2_comm * Math.PI) / 180;
            // .......................................................................
            // Эллипсоид Красовского, grad,min,sec
            // dd.ddddd -> DD MM SS

            // Широта
            objClassMap8_ed.f_Grad_GMS
              (
                // Входные параметры (grad)
                LatKrG_YS2_comm,

                // Выходные параметры 
                ref Lat_Grad_YS2_comm,
                ref Lat_Min_YS2_comm,
                ref Lat_Sec_YS2_comm

              );

            // Долгота
            objClassMap8_ed.f_Grad_GMS
              (
                // Входные параметры (grad)
                LongKrG_YS2_comm,

                // Выходные параметры 
                ref Long_Grad_YS2_comm,
                ref Long_Min_YS2_comm,
                ref Long_Sec_YS2_comm

              );

            // .......................................................................


            // SK42(элл.)->Крюгер ****************************************************
            // Преобразование геодезических координат (широта, долгота, высота) 
            // эллипсоида Красовского (СК42) в плоские прямоугольные координаты в
            // проекции Гаусса-Крюгера
            // Входные параметры -> !!!grad

            objClassMap9_ed.f_SK42_Krug
                   (
                // Входные параметры (!!! grad)
                // !!! эллипсоид Красовского
                       LatKrG_YS2_comm,   // широта
                       LongKrG_YS2_comm,  // долгота

                       // Выходные параметры (km)
                       ref XYS242_comm,
                       ref YYS242_comm

                   );

            // km->m
            XYS242_comm = XYS242_comm * 1000;
            YYS242_comm = YYS242_comm * 1000;
            // **************************************************** SK42(элл.)->Крюгер

            // .......................................................................
            // Отображение YS2 в выбранной СК

            OtobrYS2_Sost();
            // .......................................................................

        } // f_PU_Sost
 */
        // ************************************************************************

// FUNCTIONS ***********************************************************************

        // ************************************************************************
        // функция выбора системы координат SP1,SP2,PU
        // ************************************************************************

        private void ChooseSystemCoord_Sost(int iSystemCoord)
        {
            gbOwnRect.Visible = false;
            gbOwnRect42.Visible = false;
            gbOwnRad.Visible = false;
            gbOwnDegMin.Visible = false;
            gbOwnDegMinSec.Visible = false;
            gbPt1Rect.Visible = false;
            gbPt1Rect42.Visible = false;
            gbPt1Rad.Visible = false;
            gbPt1DegMin.Visible = false;
            gbPt1DegMinSec.Visible = false;
            gbPt2Rect.Visible = false;
            gbPt2Rect42.Visible = false;
            gbPt2Rad.Visible = false;
            gbPt2DegMin.Visible = false;
            gbPt2DegMinSec.Visible = false;


            switch (iSystemCoord)
            {
                case 0: // Метры на местности

                    // SP1
                    gbOwnRect.Visible = true;
                    gbOwnRect.Location = new Point(10, 34);
                    // SP2
                    gbPt1Rect.Visible = true;
                    //gbPt1Rect.Location = new Point(6, 11);
                    gbPt1Rect.Location = new Point(10, 34);

                    // PU
                    gbPt2Rect.Visible = true;
                    //gbPt2Rect.Location = new Point(6, 11);
                    gbPt2Rect.Location = new Point(10, 34);

                    // SP1
                    if (GlobalVarLn.flCoordSP_Sost == 1)
                    {
                        //333
                        //ichislo = (long)(GlobalVarLn.XCenter_Sost);
                        //tbXRect.Text = Convert.ToString(ichislo);
                        //ichislo = (long)(GlobalVarLn.YCenter_Sost);
                        //tbYRect.Text = Convert.ToString(ichislo);

                        ichislo = (long)(GlobalVarLn.BWGS84_1_sost * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        tbXRect.Text = Convert.ToString(dchislo);
                        ichislo = (long)(GlobalVarLn.LWGS84_1_sost * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        tbYRect.Text = Convert.ToString(dchislo);

                    } // IF

                    // SP2
                    if (GlobalVarLn.flCoordYS1_Sost == 1)
                    {
                        //333
                        //ichislo = (long)(GlobalVarLn.XPoint1_Sost);
                        //tbPt1XRect.Text = Convert.ToString(ichislo);
                        //ichislo = (long)(GlobalVarLn.YPoint1_Sost);
                        //tbPt1YRect.Text = Convert.ToString(ichislo);

                        ichislo = (long)(GlobalVarLn.BWGS84_2_sost * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        tbPt1XRect.Text = Convert.ToString(dchislo);
                        ichislo = (long)(GlobalVarLn.LWGS84_2_sost * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        tbPt1YRect.Text = Convert.ToString(dchislo);

                    }
                    // PU
                    if (GlobalVarLn.flCoordYS2_Sost == 1)
                    {
                        //333
                        //ichislo = (long)(GlobalVarLn.XPoint2_Sost);
                        //tbPt2XRect.Text = Convert.ToString(ichislo);
                        //ichislo = (long)(GlobalVarLn.YPoint2_Sost);
                        //tbPt2YRect.Text = Convert.ToString(ichislo);

                        ichislo = (long)(GlobalVarLn.BWGS84_3_sost * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        tbPt2XRect.Text = Convert.ToString(dchislo);
                        ichislo = (long)(GlobalVarLn.LWGS84_3_sost * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        tbPt2YRect.Text = Convert.ToString(dchislo);

                    }

                    break;

                case 1: // Метры 1942 года

                    // SP1
                    gbOwnRect42.Visible = true;
                    gbOwnRect42.Location = new Point(10, 34);
                    // SP2
                    gbPt1Rect42.Visible = true;
                    //gbPt1Rect42.Location = new Point(6, 11);
                    gbPt1Rect42.Location = new Point(10, 34);
                    // PU
                    gbPt2Rect42.Visible = true;
                    //gbPt2Rect42.Location = new Point(6, 11);
                    gbPt2Rect42.Location = new Point(10, 34);

                    // SP1
                    if (GlobalVarLn.flCoordSP_Sost == 1)
                    {
                        ichislo = (long)(GlobalVarLn.XSP42_comm);
                        tbXRect42.Text = Convert.ToString(ichislo);
                        ichislo = (long)(GlobalVarLn.YSP42_comm);
                        tbYRect42.Text = Convert.ToString(ichislo);

                    } // IF
                    // SP2
                    if (GlobalVarLn.flCoordYS1_Sost == 1)
                    {
                        ichislo = (long)(GlobalVarLn.XYS142_comm);
                        tbPt1XRect42.Text = Convert.ToString(ichislo);
                        ichislo = (long)(GlobalVarLn.YYS142_comm);
                        tbPt1YRect42.Text = Convert.ToString(ichislo);

                    }
                    // PU
                    if (GlobalVarLn.flCoordYS2_Sost == 1)
                    {
                        ichislo = (long)(GlobalVarLn.XYS242_comm);
                        tbPt2XRect42.Text = Convert.ToString(ichislo);
                        ichislo = (long)(GlobalVarLn.YYS242_comm);
                        tbPt2YRect42.Text = Convert.ToString(ichislo);

                    }

                    break;

                case 2: // Радианы (Красовский)

                    // SP1
                    gbOwnRad.Visible = true;
                    gbOwnRad.Location = new Point(10, 34);
                    // SP2
                    gbPt1Rad.Visible = true;
                    //gbPt1Rad.Location = new Point(6, 11);
                    gbPt1Rad.Location = new Point(10, 34);
                    // PU
                    gbPt2Rad.Visible = true;
                    //gbPt2Rad.Location = new Point(6, 11);
                    gbPt2Rad.Location = new Point(10, 34);

                    // SP1
                    if (GlobalVarLn.flCoordSP_Sost == 1)
                    {

                        ichislo = (long)(GlobalVarLn.LatKrR_comm * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        tbBRad.Text = Convert.ToString(dchislo);
                        ichislo = (long)(GlobalVarLn.LongKrR_comm * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        tbLRad.Text = Convert.ToString(dchislo);


                    } // IF
                    // SP2
                    if (GlobalVarLn.flCoordYS1_Sost == 1)
                    {
                        ichislo = (long)(GlobalVarLn.LatKrR_YS1_comm * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        tbPt1BRad.Text = Convert.ToString(dchislo);
                        ichislo = (long)(GlobalVarLn.LongKrR_YS1_comm * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        tbPt1LRad.Text = Convert.ToString(dchislo);

                    }
                    // PU
                    if (GlobalVarLn.flCoordYS2_Sost == 1)
                    {
                        ichislo = (long)(GlobalVarLn.LatKrR_YS2_comm * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        tbPt2BRad.Text = Convert.ToString(dchislo);
                        ichislo = (long)(GlobalVarLn.LongKrR_YS2_comm * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        tbPt2LRad.Text = Convert.ToString(dchislo);

                    }

                    break;

                case 3: // Градусы (Красовский)

                    // SP1
                    gbOwnDegMin.Visible = true;
                    gbOwnDegMin.Location = new Point(10, 34);
                    // SP2
                    gbPt1DegMin.Visible = true;
                    //gbPt1DegMin.Location = new Point(6, 11);
                    gbPt1DegMin.Location = new Point(10, 34);
                    // PU
                    gbPt2DegMin.Visible = true;
                    //gbPt2DegMin.Location = new Point(6, 11);
                    gbPt2DegMin.Location = new Point(10, 34);

                    // SP1
                    if (GlobalVarLn.flCoordSP_Sost == 1)
                    {
                        ichislo = (long)(GlobalVarLn.LatKrG_comm * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        tbBMin1.Text = Convert.ToString(dchislo);
                        ichislo = (long)(GlobalVarLn.LongKrG_comm * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        tbLMin1.Text = Convert.ToString(dchislo);


                    } // IF
                    // SP2
                    if (GlobalVarLn.flCoordYS1_Sost == 1)
                    {
                        ichislo = (long)(GlobalVarLn.LatKrG_YS1_comm * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        tbPt1BMin1.Text = Convert.ToString(dchislo);
                        ichislo = (long)(GlobalVarLn.LongKrG_YS1_comm * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        tbPt1LMin1.Text = Convert.ToString(dchislo);

                    }
                    // PU
                    if (GlobalVarLn.flCoordYS2_Sost == 1)
                    {
                        ichislo = (long)(GlobalVarLn.LatKrG_YS2_comm * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        tbPt2BMin1.Text = Convert.ToString(dchislo);
                        ichislo = (long)(GlobalVarLn.LongKrG_YS2_comm * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        tbPt2LMin1.Text = Convert.ToString(dchislo);

                    }

                    break;

                case 4: // Градусы,мин,сек (Красовский)

                    // SP1
                    gbOwnDegMinSec.Visible = true;
                    gbOwnDegMinSec.Location = new Point(10, 34);
                    // SP2
                    gbPt1DegMinSec.Visible = true;
                    //gbPt1DegMinSec.Location = new Point(6, 11);
                    gbPt1DegMinSec.Location = new Point(10, 34);
                    // PU
                    gbPt2DegMinSec.Visible = true;
                    //gbPt2DegMinSec.Location = new Point(6, 11);
                    gbPt2DegMinSec.Location = new Point(10, 34);

                    // SP1
                    if (GlobalVarLn.flCoordSP_Sost == 1)
                    {
                        tbBDeg2.Text = Convert.ToString(GlobalVarLn.Lat_Grad_comm);
                        tbBMin2.Text = Convert.ToString(GlobalVarLn.Lat_Min_comm);
                        ichislo = (long)(GlobalVarLn.Lat_Sec_comm);
                        tbBSec.Text = Convert.ToString(ichislo);
                        tbLDeg2.Text = Convert.ToString(GlobalVarLn.Long_Grad_comm);
                        tbLMin2.Text = Convert.ToString(GlobalVarLn.Long_Min_comm);
                        ichislo = (long)(GlobalVarLn.Long_Sec_comm);
                        tbLSec.Text = Convert.ToString(ichislo);


                    } // IF
                    // SP2
                    if (GlobalVarLn.flCoordYS1_Sost == 1)
                    {
                        tbPt1BDeg2.Text = Convert.ToString(GlobalVarLn.Lat_Grad_YS1_comm);
                        tbPt1BMin2.Text = Convert.ToString(GlobalVarLn.Lat_Min_YS1_comm);
                        ichislo = (long)(GlobalVarLn.Lat_Sec_YS1_comm);
                        tbPt1BSec.Text = Convert.ToString(ichislo);
                        tbPt1LDeg2.Text = Convert.ToString(GlobalVarLn.Long_Grad_YS1_comm);
                        tbPt1LMin2.Text = Convert.ToString(GlobalVarLn.Long_Min_YS1_comm);
                        ichislo = (long)(GlobalVarLn.Long_Sec_YS1_comm);
                        tbPt1LSec.Text = Convert.ToString(ichislo);

                    }
                    // PU
                    if (GlobalVarLn.flCoordYS2_Sost == 1)
                    {
                        tbPt2BDeg2.Text = Convert.ToString(GlobalVarLn.Lat_Grad_YS2_comm);
                        tbPt2BMin2.Text = Convert.ToString(GlobalVarLn.Lat_Min_YS2_comm);
                        ichislo = (long)(GlobalVarLn.Lat_Sec_YS2_comm);
                        tbPt2BSec.Text = Convert.ToString(ichislo);
                        tbPt2LDeg2.Text = Convert.ToString(GlobalVarLn.Long_Grad_YS2_comm);
                        tbPt2LMin2.Text = Convert.ToString(GlobalVarLn.Long_Min_YS2_comm);
                        ichislo = (long)(GlobalVarLn.Long_Sec_YS2_comm);
                        tbPt2LSec.Text = Convert.ToString(ichislo);

                    }

                    break;

                default:
                    break;

            } // SWITCH

        } // ChooseSystemCoord_Comm
        // ************************************************************************

        // ************************************************************************
        // функция отображения координат SP1
        // ************************************************************************

        private void OtobrSP_Sost()
        {

            // Метры на местности
            //if (gbOwnRect.Visible == true)
            {
                //333
                //ichislo = (long)(XSP_comm);
                //tbXRect.Text = Convert.ToString(ichislo);
                //ichislo = (long)(YSP_comm);
                //tbYRect.Text = Convert.ToString(ichislo);

                ichislo = (long)(GlobalVarLn.BWGS84_1_sost * 1000000);
                dchislo = ((double)ichislo) / 1000000;
                tbXRect.Text = Convert.ToString(dchislo);
                ichislo = (long)(GlobalVarLn.LWGS84_1_sost * 1000000);
                dchislo = ((double)ichislo) / 1000000;
                tbYRect.Text = Convert.ToString(dchislo);


            } // IF


            // Метры 1942 года
            //else if (gbOwnRect42.Visible == true)
            {
                ichislo = (long)(XSP42_comm);
                tbXRect42.Text = Convert.ToString(ichislo);

                ichislo = (long)(YSP42_comm);
                tbYRect42.Text = Convert.ToString(ichislo);

            } // IF


            // Радианы (Красовский)
            //else if (gbOwnRad.Visible == true)
            {
                ichislo = (long)(LatKrR_comm * 1000000);
                dchislo = ((double)ichislo) / 1000000;
                tbBRad.Text = Convert.ToString(dchislo);   // X, карта

                ichislo = (long)(LongKrR_comm * 1000000);
                dchislo = ((double)ichislo) / 1000000;
                tbLRad.Text = Convert.ToString(dchislo);   // X, карта

            } // IF


            // Градусы (Красовский)
            //else if (gbOwnDegMin.Visible == true)
            {

                ichislo = (long)(LatKrG_comm * 1000000);
                dchislo = ((double)ichislo) / 1000000;
                tbBMin1.Text = Convert.ToString(dchislo);

                ichislo = (long)(LongKrG_comm * 1000000);
                dchislo = ((double)ichislo) / 1000000;
                tbLMin1.Text = Convert.ToString(dchislo);

            } // IF


            // Градусы,мин,сек (Красовский)
            //else if (gbOwnDegMinSec.Visible == true)
            {

                tbBDeg2.Text = Convert.ToString(Lat_Grad_comm);
                tbBMin2.Text = Convert.ToString(Lat_Min_comm);
                ichislo = (long)(Lat_Sec_comm);
                tbBSec.Text = Convert.ToString(ichislo);

                tbLDeg2.Text = Convert.ToString(Long_Grad_comm);
                tbLMin2.Text = Convert.ToString(Long_Min_comm);
                ichislo = (long)(Long_Sec_comm);
                tbLSec.Text = Convert.ToString(ichislo);

            } // IF


        } // OtobrSP_Sost
        // ************************************************************************

        // ************************************************************************
        // функция отображения координат SP2
        // ************************************************************************

        private void OtobrYS1_Sost()
        {

            // Метры на местности
            //if (gbPt1Rect.Visible == true)
            {
                //333
                //ichislo = (long)(XYS1_comm);
                //tbPt1XRect.Text = Convert.ToString(ichislo);
                //ichislo = (long)(YYS1_comm);
                //tbPt1YRect.Text = Convert.ToString(ichislo);

                ichislo = (long)(GlobalVarLn.BWGS84_2_sost * 1000000);
                dchislo = ((double)ichislo) / 1000000;
                tbPt1XRect.Text = Convert.ToString(dchislo);
                ichislo = (long)(GlobalVarLn.LWGS84_2_sost * 1000000);
                dchislo = ((double)ichislo) / 1000000;
                tbPt1YRect.Text = Convert.ToString(dchislo);

            } // IF


            // Метры 1942 года
            //else if (gbPt1Rect42.Visible == true)
            {
                ichislo = (long)(XYS142_comm);
                tbPt1XRect42.Text = Convert.ToString(ichislo);

                ichislo = (long)(YYS142_comm);
                tbPt1YRect42.Text = Convert.ToString(ichislo);

            } // IF


            // Радианы (Красовский)
            //else if (gbPt1Rad.Visible == true)
            {
                ichislo = (long)(LatKrR_YS1_comm * 1000000);
                dchislo = ((double)ichislo) / 1000000;
                tbPt1BRad.Text = Convert.ToString(dchislo);   // X, карта

                ichislo = (long)(LongKrR_YS1_comm * 1000000);
                dchislo = ((double)ichislo) / 1000000;
                tbPt1LRad.Text = Convert.ToString(dchislo);   // X, карта

            } // IF


            // Градусы (Красовский)
            //else if (gbPt1DegMin.Visible == true)
            {

                ichislo = (long)(LatKrG_YS1_comm * 1000000);
                dchislo = ((double)ichislo) / 1000000;
                tbPt1BMin1.Text = Convert.ToString(dchislo);

                ichislo = (long)(LongKrG_YS1_comm * 1000000);
                dchislo = ((double)ichislo) / 1000000;
                tbPt1LMin1.Text = Convert.ToString(dchislo);

            } // IF


            // Градусы,мин,сек (Красовский)
            //else if (gbPt1DegMinSec.Visible == true)
            {

                tbPt1BDeg2.Text = Convert.ToString(Lat_Grad_YS1_comm);
                tbPt1BMin2.Text = Convert.ToString(Lat_Min_YS1_comm);
                ichislo = (long)(Lat_Sec_YS1_comm);
                tbPt1BSec.Text = Convert.ToString(ichislo);

                tbPt1LDeg2.Text = Convert.ToString(Long_Grad_YS1_comm);
                tbPt1LMin2.Text = Convert.ToString(Long_Min_YS1_comm);
                ichislo = (long)(Long_Sec_YS1_comm);
                tbPt1LSec.Text = Convert.ToString(ichislo);

            } // IF


        } // OtobrYS1_Sost
        // ************************************************************************

        // ************************************************************************
        // функция отображения координат PU
        // ************************************************************************

        private void OtobrYS2_Sost()
        {

            // Метры на местности
            //if (gbPt2Rect.Visible == true)
            {
                //333
                //ichislo = (long)(XYS2_comm);
                //tbPt2XRect.Text = Convert.ToString(ichislo);
                //ichislo = (long)(YYS2_comm);
                //tbPt2YRect.Text = Convert.ToString(ichislo);

                ichislo = (long)(GlobalVarLn.BWGS84_3_sost * 1000000);
                dchislo = ((double)ichislo) / 1000000;
                tbPt2XRect.Text = Convert.ToString(dchislo);
                ichislo = (long)(GlobalVarLn.LWGS84_3_sost * 1000000);
                dchislo = ((double)ichislo) / 1000000;
                tbPt2YRect.Text = Convert.ToString(dchislo);

            } // IF


            // Метры 1942 года
            //else if (gbPt2Rect42.Visible == true)
            {
                ichislo = (long)(XYS242_comm);
                tbPt2XRect42.Text = Convert.ToString(ichislo);

                ichislo = (long)(YYS242_comm);
                tbPt2YRect42.Text = Convert.ToString(ichislo);

            } // IF


            // Радианы (Красовский)
            //else if (gbPt2Rad.Visible == true)
            {
                ichislo = (long)(LatKrR_YS2_comm * 1000000);
                dchislo = ((double)ichislo) / 1000000;
                tbPt2BRad.Text = Convert.ToString(dchislo);   // X, карта

                ichislo = (long)(LongKrR_YS2_comm * 1000000);
                dchislo = ((double)ichislo) / 1000000;
                tbPt2LRad.Text = Convert.ToString(dchislo);   // X, карта

            } // IF


            // Градусы (Красовский)
            //else if (gbPt2DegMin.Visible == true)
            {

                ichislo = (long)(LatKrG_YS2_comm * 1000000);
                dchislo = ((double)ichislo) / 1000000;
                tbPt2BMin1.Text = Convert.ToString(dchislo);

                ichislo = (long)(LongKrG_YS2_comm * 1000000);
                dchislo = ((double)ichislo) / 1000000;
                tbPt2LMin1.Text = Convert.ToString(dchislo);

            } // IF


            // Градусы,мин,сек (Красовский)
            //else if (gbPt2DegMinSec.Visible == true)
            {

                tbPt2BDeg2.Text = Convert.ToString(Lat_Grad_YS2_comm);
                tbPt2BMin2.Text = Convert.ToString(Lat_Min_YS2_comm);
                ichislo = (long)(Lat_Sec_YS2_comm);
                tbPt2BSec.Text = Convert.ToString(ichislo);

                tbPt2LDeg2.Text = Convert.ToString(Long_Grad_YS2_comm);
                tbPt2LMin2.Text = Convert.ToString(Long_Min_YS2_comm);
                ichislo = (long)(Long_Sec_YS2_comm);
                tbPt2LSec.Text = Convert.ToString(ichislo);

            } // IF


        } // OtobrYS2_Sost
        // ************************************************************************


// *********************************************************************** FUNCTIONS






// **********************************************************************************
// ZnachokSP1
// **********************************************************************************
private void buttonZSP_Click(object sender, EventArgs e)
{
    GlobalVarLn.indz1_Sost += 1;
    if (GlobalVarLn.indz1_Sost == imageList1.Images.Count)
        GlobalVarLn.indz1_Sost = 0;
    pbSP.Image = imageList1.Images[GlobalVarLn.indz1_Sost];

    iniRW.write_ZnakSP1(GlobalVarLn.indz1_Sost);

} // zn1
// **********************************************************************************

// **********************************************************************************
// ZnachokSP2
// **********************************************************************************
private void button1_Click(object sender, EventArgs e)
{
    GlobalVarLn.indz2_Sost += 1;
    if (GlobalVarLn.indz2_Sost == imageList2.Images.Count)
        GlobalVarLn.indz2_Sost = 0;
    pictureBox1.Image = imageList2.Images[GlobalVarLn.indz2_Sost];

    iniRW.write_ZnakSP2(GlobalVarLn.indz2_Sost);

} // zn2
// **********************************************************************************

// **********************************************************************************
// ZnachokPU
// **********************************************************************************
private void button2_Click(object sender, EventArgs e)
{
    GlobalVarLn.indz3_Sost += 1;
    if (GlobalVarLn.indz3_Sost == imageList3.Images.Count)
        GlobalVarLn.indz3_Sost = 0;
    pictureBox2.Image = imageList3.Images[GlobalVarLn.indz3_Sost];

    iniRW.write_ZnakPU(GlobalVarLn.indz3_Sost);

} // zn3
// **********************************************************************************

// ************************************************************************
// Обработчик кнопки : Save in file
// ************************************************************************

private void bAccept_Click(object sender, EventArgs e)
{
    int i_tmp = 0;

    // TXT *************************************************************************************

    // -----------------------------------------------------------------------------------------
    String strFileName;
    strFileName = "ComplexCompound.txt";
    StreamWriter srFile;

    try
    {
        srFile = new StreamWriter(strFileName);
    }
    catch
    {
        MessageBox.Show("Невозможно сохранить в файл");
        return;
    }

   // LF objLF = new LF();
    // -----------------------------------------------------------------------------------------

    for (i_tmp = 0; i_tmp < GlobalVarLn.list1_Sost.Count; i_tmp++)
    {

        if (i_tmp == 0)
        {
            srFile.WriteLine("SP1 =");
        }
        else if(i_tmp==1)
        {
            srFile.WriteLine("SP2 =");
        }
        else
        {
            srFile.WriteLine("PU =");
        }

            srFile.WriteLine("X =" + Convert.ToString((int)GlobalVarLn.list1_Sost[i_tmp].X_m));
            srFile.WriteLine("Y =" + Convert.ToString((int)GlobalVarLn.list1_Sost[i_tmp].Y_m));
            srFile.WriteLine("H =" + Convert.ToString((int)GlobalVarLn.list1_Sost[i_tmp].H_m));
            srFile.WriteLine("indzn =" + Convert.ToString(GlobalVarLn.list1_Sost[i_tmp].indzn));
        
    } // FOR
    // -------------------------------------------------------------------------------------

    srFile.Close();
    // ------------------------------------------------------------------------------------

    // ************************************************************************************* TXT

    // INI *************************************************************************************
    if (System.IO.File.Exists(Application.StartupPath + "\\Settings.ini"))
    {
        GlobalVarLn.XCenter_Sost = (double)iniRW.get_X_ASP();
        GlobalVarLn.YCenter_Sost = (double)iniRW.get_Y_ASP();

        // SP1
        iniRW.write_XY_ASP((int)GlobalVarLn.list1_Sost[0].X_m, (int)GlobalVarLn.list1_Sost[0].Y_m, (int)GlobalVarLn.list1_Sost[0].H_m,(int)GlobalVarLn.list1_Sost[0].indzn);
        // SP2
        iniRW.write_XY_ASPS((int)GlobalVarLn.list1_Sost[1].X_m, (int)GlobalVarLn.list1_Sost[1].Y_m, (int)GlobalVarLn.list1_Sost[1].H_m, (int)GlobalVarLn.list1_Sost[1].indzn);
        // PU
        iniRW.write_XY_PU((int)GlobalVarLn.list1_Sost[2].X_m, (int)GlobalVarLn.list1_Sost[2].Y_m, (int)GlobalVarLn.list1_Sost[2].H_m, (int)GlobalVarLn.list1_Sost[2].indzn);

    }
    else
    {
        MessageBox.Show("Невозможно открыть INI файл");
        return;
    }

    // ************************************************************************************* INI


} // Save in file
// **********************************************************************************

// **********************************************************************************
// Read from file
// **********************************************************************************
private void button4_Click(object sender, EventArgs e)
{
    String strLine = "";
    String strLine1 = "";

    double number1 = 0;
    int number2 = 0;

    char symb1 = 'S';
    char symb2 = 'X';
    char symb3 = 'Y';
    char symb4 = '=';
    char symb5 = 'H';
    //char symb6 = 'R';
    //char symb7 = 'A';
    char symb8 = 'P';
    char symb9 = 'i';

    int indStart = 0;
    int indStop = 0;
    int iLength = 0;

    int IndZap = 0;
    int TekPoz = 0;

    int fi = 0;

// Clear ************************************************************************

/*
    // SP1
    tbXRect.Text = "";
    tbYRect.Text = "";
    tbXRect42.Text = "";
    tbYRect42.Text = "";
    tbBRad.Text = "";
    tbLRad.Text = "";
    tbBMin1.Text = "";
    tbLMin1.Text = "";
    tbBDeg2.Text = "";
    tbBMin2.Text = "";
    tbBSec.Text = "";
    tbLDeg2.Text = "";
    tbLMin2.Text = "";
    tbLSec.Text = "";

    // SP2
    tbPt1XRect.Text = "";
    tbPt1YRect.Text = "";
    tbPt1XRect42.Text = "";
    tbPt1YRect42.Text = "";
    tbPt1BRad.Text = "";
    tbPt1LRad.Text = "";
    tbPt1BMin1.Text = "";
    tbPt1LMin1.Text = "";
    tbPt1BDeg2.Text = "";
    tbPt1BMin2.Text = "";
    tbPt1BSec.Text = "";
    tbPt1LDeg2.Text = "";
    tbPt1LMin2.Text = "";
    tbPt1LSec.Text = "";

    // PU
    tbPt2XRect.Text = "";
    tbPt2YRect.Text = "";
    tbPt2XRect42.Text = "";
    tbPt2YRect42.Text = "";
    tbPt2BRad.Text = "";
    tbPt2LRad.Text = "";
    tbPt2BMin1.Text = "";
    tbPt2LMin1.Text = "";
    tbPt2BDeg2.Text = "";
    tbPt2BMin2.Text = "";
    tbPt2BSec.Text = "";
    tbPt2LDeg2.Text = "";
    tbPt2LMin2.Text = "";
    tbPt2LSec.Text = "";

    tbOwnHeight.Text = "";

    tbPt1Height.Text = "";
    tbPt2Height.Text = "";

    // ...................................................................
    // переменные

    GlobalVarLn.flCoordSP_Sost = 0; // =1-> Выбрали СП
    GlobalVarLn.flCoordYS1_Sost = 0;
    GlobalVarLn.flCoordYS2_Sost = 0;
    GlobalVarLn.fl_Sost = 0; // Отрисовка зоны
    GlobalVarLn.list1_Sost.Clear();
    // ...................................................................
    chbXY1.Checked = false;

    GlobalVarLn.indz1_Sost = 0;
    GlobalVarLn.indz2_Sost = 0;
    GlobalVarLn.indz3_Sost = 0;
    pbSP.Image = imageList1.Images[GlobalVarLn.indz1_Sost];
    pictureBox1.Image = imageList2.Images[GlobalVarLn.indz2_Sost];
    pictureBox2.Image = imageList3.Images[GlobalVarLn.indz3_Sost];

    // Убрать с карты
    GlobalVarLn.axMapScreenGlobal.Repaint();
    // ---------------------------------------------------------------------
*/

    ClassMap.ClearSost();

// ************************************************************************ Clear



/*
// Open file *******************************************************************
// Чтение
// SP1 =...
// X =...
// Y =...
// H =...
// indzn =...
// SP2 =...
// X =...
// Y =...
// H =...
// indzn =...
// PU =...
// X =...
// Y =...
// H =...
// indzn =...


    String strFileName;
    strFileName = "ComplexCompound.txt";

    StreamReader srFile;
    try
    {
        srFile = new StreamReader(strFileName);
    }
    catch
    {
        MessageBox.Show("Can’t open file");
        return;

    }

// ******************************************************************* Open file

try
{

LF objLF = new LF();
TekPoz = 0;
IndZap = 0;

// ------------------------------------------------------------------------------
// String1->SP1

strLine = srFile.ReadLine();

if (strLine == null)
{
    MessageBox.Show("No information");
    return;
}

indStart = strLine.IndexOf(symb1, TekPoz); // S

if (indStart == -1)
{
    MessageBox.Show("No information");
    return;
}

indStop = strLine.IndexOf(symb4, TekPoz);  //=

if ((indStop == -1) || (indStop < indStart))
{
    MessageBox.Show("No information");
    return;
}

// .......................................................

fi = 0;
strLine = srFile.ReadLine(); // читаем далее (X)
if ((strLine == "") || (strLine == null))
{
    fi = 1;
}

// WHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHWHILE
while ((strLine != "") && (strLine != null))
{
    IndZap += 1;

    // .......................................................
    // X =...

    indStart = strLine.IndexOf(symb2, TekPoz); // X
    indStop = strLine.IndexOf(symb4, TekPoz);  // =
    iLength = indStop - indStart + 1;
    strLine1 = strLine.Remove(indStart, iLength);
    number1 = Convert.ToInt32(strLine1);

    objLF.X_m = number1;
    // .......................................................
    // Y =...

    strLine = srFile.ReadLine();

    indStart = strLine.IndexOf(symb3, TekPoz); // Y
    indStop = strLine.IndexOf(symb4, TekPoz);  // =
    iLength = indStop - indStart + 1;
    strLine1 = strLine.Remove(indStart, iLength);
    number1 = Convert.ToInt32(strLine1);

    objLF.Y_m = number1;
    // .......................................................
    // H =...

    strLine = srFile.ReadLine();

    indStart = strLine.IndexOf(symb5, TekPoz); // H
    indStop = strLine.IndexOf(symb4, TekPoz);  // =
    iLength = indStop - indStart + 1;
    strLine1 = strLine.Remove(indStart, iLength);
    number1 = Convert.ToInt32(strLine1);

    objLF.H_m = number1;
    // .......................................................
    // indzn =...

    strLine = srFile.ReadLine();

    indStart = strLine.IndexOf(symb9, TekPoz); // i
    indStop = strLine.IndexOf(symb4, TekPoz);  // =
    iLength = indStop - indStart + 1;
    strLine1 = strLine.Remove(indStart, iLength);
    number1 = Convert.ToInt32(strLine1);

    objLF.indzn = (int)number1;
    // .......................................................
    // Занести в List

    GlobalVarLn.list1_Sost.Add(objLF);
    // .......................................................
    fi = 0;
    strLine = srFile.ReadLine(); // читаем далее (SP2/PU)
    if ((strLine == "") || (strLine == null))
    {
        fi = 1;
        srFile.Close();
        f_SP1_Sost();
        f_SP2_Sost();
        f_PU_Sost();
        ClassMap.f_Map_Redraw_Sost();
        return;
    }
    strLine = srFile.ReadLine(); // читаем далее (Xi)
    // .......................................................


} // WHILE
// WHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHWHILE


} // try

catch
{
    srFile.Close();
    MessageBox.Show("Can't open file");
    return;

} // catch


srFile.Close();
ClassMap.f_Map_Redraw_Sost();

// -------------------------------------------------------------------------------------
*/

    // GPSSPPU
    //ClassMap.f_LoadSostIni();


} // Read from file

// **********************************************************************************

// **********************************************************************************
// Closing, activated
// **********************************************************************************
private void FormSost_FormClosing(object sender, FormClosingEventArgs e)
{
    GlobalVarLn.f_Open_objFormSost = 0;



    e.Cancel = true;
    Hide();

    // ................................................................................
    //Sect  АСП
/*
    int fff = 0;
    //if ((GlobalVarLn.flsect == 1) && (MapForm.chbSect.Checked == true))
    if (GlobalVarLn.flsect == 1)
    {
        fff = 1;
        GlobalVarLn.flsect = 0;
    }

    axaxcMapScreen.Repaint();

    //Sect
    if (fff == 1)
    {
        GlobalVarLn.flsect = 1;
        ClassMap.Sector((double)GlobalVarLn.XCenter_Sost, (double)GlobalVarLn.YCenter_Sost);
    }
    // ................................................................................
*/

} // Closing

private void FormSost_Activated(object sender, EventArgs e)
{
    GlobalVarLn.f_Open_objFormSost = 1;

    // ................................................................................
    //Sect  АСП
/*
    int fff = 0;
    //if ((GlobalVarLn.flsect == 1) && (MapForm.chbSect.Checked == true))
    if (GlobalVarLn.flsect == 1)

    {
        fff = 1;
        GlobalVarLn.flsect = 0;
    }

    axaxcMapScreen.Repaint();

    //Sect
    if (fff == 1)
    {
        GlobalVarLn.flsect = 1;
        ClassMap.Sector((double)GlobalVarLn.XCenter_Sost, (double)GlobalVarLn.YCenter_Sost);
    }
 */
    // ................................................................................


}// Activated
// **********************************************************************************

private void FormSost_MouseUp(object sender, MouseEventArgs e)
{
    ;
}

private void FormSost_Move(object sender, EventArgs e)
{

/*
    // ................................................................................
    //Sect  АСП

    int fff = 0;
    //if ((GlobalVarLn.flsect == 1) && (MapForm.chbSect.Checked == true))
    if (GlobalVarLn.flsect == 1)
    {
        fff = 1;
        GlobalVarLn.flsect = 0;
    }

    axaxcMapScreen.Repaint();

    //Sect
    if (fff == 1)
    {
        GlobalVarLn.flsect = 1;
        ClassMap.Sector((double)GlobalVarLn.XCenter_Sost, (double)GlobalVarLn.YCenter_Sost);
    }
    // ................................................................................
 */ 
}

// **********************************************************************************
// Переход к ручному вводу координат
// **********************************************************************************

private void chbXY1_CheckedChanged(object sender, EventArgs e)
{

// -------------------------------------------------------------------------------
// Ручной ввод

if (chbXY1.Checked == true)
 {

  // SP1
  gbOwnRect42.Visible = false;
  gbOwnRad.Visible = false;
  //gbOwnDegMin.Visible = true;
  //gbOwnDegMin.Location = new Point(10, 34);
  gbOwnDegMinSec.Visible = false;
  //gbOwnRect.Visible = false;
  gbOwnRect.Visible = true;
  gbOwnRect.Location = new Point(10, 34);
  gbOwnDegMin.Visible = false;

  // SP2
  gbPt1Rect42.Visible = false;
  gbPt1Rad.Visible = false;
  //gbPt1DegMin.Visible = true;
  //gbPt1DegMin.Location = new Point(10, 34);
  gbPt1DegMinSec.Visible = false;
  //gbPt1Rect.Visible = false;
  gbPt1Rect.Visible = true;
  gbPt1Rect.Location = new Point(10, 34);
  gbPt1DegMin.Visible = false;

  // PU
  gbPt2Rect42.Visible = false;
  gbPt2Rad.Visible = false;
  ///gbPt2DegMin.Visible = true;
  //gbPt2DegMin.Location = new Point(10, 34);
  gbPt2DegMinSec.Visible = false;
  //gbPt2Rect.Visible = false;
  gbPt2Rect.Visible = true;
  gbPt2Rect.Location = new Point(10, 34);
  gbPt2DegMin.Visible = false;

  // ....................................

  // SP1
  gbOwnRect42.Enabled = true;
  gbOwnRect.Enabled = true;
  gbOwnRad.Enabled = false;
  gbOwnDegMin.Enabled = true;
  gbOwnDegMinSec.Enabled = false;

  // SP2
  gbPt1Rect42.Enabled = true;
  gbPt1Rect.Enabled = true;
  gbPt1Rad.Enabled = false;
  gbPt1DegMin.Enabled = true;
  gbPt1DegMinSec.Enabled = false;

  // PU
  gbPt2Rect42.Enabled = true;
  gbPt2Rect.Enabled = true;
  gbPt2Rad.Enabled = false;
  gbPt2DegMin.Enabled = true;
  gbPt2DegMinSec.Enabled = false;
 
  cbChooseSC.SelectedIndex = 0;

 }
// -------------------------------------------------------------------------------
 else
 {
/*
     // SP1
     gbOwnRect.Enabled = true;
     gbOwnRect42.Enabled = true;
     gbOwnRad.Enabled = true;
     gbOwnDegMin.Enabled = true;
     gbOwnDegMinSec.Enabled = true;
     // SP2
     gbPt1Rect.Enabled = true;
     gbPt1Rect42.Enabled = true;
     gbPt1Rad.Enabled = true;
     gbPt1DegMin.Enabled = true;
     gbPt1DegMinSec.Enabled = true;
     // PU
     gbPt2Rect.Enabled = true;
     gbPt2Rect42.Enabled = true;
     gbPt2Rad.Enabled = true;
     gbPt2DegMin.Enabled = true;
     gbP
 */

    //3101
     // SP1
     gbOwnRect.Enabled = false;
     gbOwnRect42.Enabled = false;
     gbOwnRad.Enabled = false;
     gbOwnDegMin.Enabled = false;
     gbOwnDegMinSec.Enabled = false;
     // SP2
     gbPt1Rect.Enabled = false;
     gbPt1Rect42.Enabled = false;
     gbPt1Rad.Enabled = false;
     gbPt1DegMin.Enabled = false;
     gbPt1DegMinSec.Enabled = false;
     // PU
     gbPt2Rect.Enabled = false;
     gbPt2Rect42.Enabled = false;
     gbPt2Rad.Enabled = false;
     gbPt2DegMin.Enabled = false;
     gbPt2DegMinSec.Enabled = false;


 }
// -------------------------------------------------------------------------------

} // Переход к ручному вводу координат
// **********************************************************************************

private void lPt1Height_Click(object sender, EventArgs e)
{
    ;
}

private void lChooseSC_Click(object sender, EventArgs e)
{
    ;
}

// **********************************************************************************
// Прием координат от GPS
// **********************************************************************************

private void button8_Click(object sender, EventArgs e)
{

// !!! Get to (WGS84)
//        GlobalVarLn.XCenterGRAD_Sost = ;
//        GlobalVarLn.YCenterGRAD_Sost = ;
//        GlobalVarLn.XPoint1GRAD_Sost = ;
//        GlobalVarLn.YPoint1GRAD_Sost = ;
//        GlobalVarLn.XPoint2GRAD_Sost = ;
//        GlobalVarLn.YPoint2GRAD_Sost = ;

    //GlobalVarLn.XCenterGRAD_Sost = 53.881239;
    //GlobalVarLn.YCenterGRAD_Sost = 27.606449;

    //ClassMap.f_GetGPS();

} // GPS

// **********************************************************************************

// **********************************************************************************

private void chbGPS_CheckedChanged(object sender, EventArgs e)
{
    //777->Убрали птичку, сейчас на арм она будет
/*
    if (chbGPS.Checked == false)
    {
        //GlobalVarLn.fl_First_GPS = 0;
        timerGPS.Stop();
        GlobalVarLn.flTmrGPS = 0;
    }

    else
    {
        //GlobalVarLn.fl_First_GPS = 0;
        timerGPS.Start();
        GlobalVarLn.flTmrGPS = 1;

    }
 */ 
}

// **********************************************************************************

// **********************************************************************************
// Timer

private void timerGPS_Tick(object sender, EventArgs e)
{
    //777
    //999
    //GlobalVarLn.flTmrGPS = 1;


}
// **********************************************************************************


private void LanguageChooser()
{
    var cont = this.Controls;

    if (NumberOfLanguage.Equals(0))
    {
        ChangeLanguageToRu(cont);
        this.Text = "Состав комплекса";
    }
    if (NumberOfLanguage.Equals(1))
    {
        //ChangeLanguageToEng(cont);
    }
    if (NumberOfLanguage.Equals(2))
    {
        ChangeLanguageToAzer(cont);
        this.Text = "Kompleksin tərkibi ";
    }
}

private void ChangeLanguageToRu(System.Windows.Forms.Control.ControlCollection cont)
{
    string lang = "ru-RU";
    foreach (System.Windows.Forms.Control cc in cont)
    {
        if (cc is ComboBox)
        {

            for (int i = 0; i < (cc as ComboBox).Items.Count; i++)
            {
                if (i == 0)
                {
                    string a = resources.GetString(cc.Name + ".Items", new CultureInfo(lang));
                    (cc as ComboBox).Items[i] = a;
                }
                else
                {
                    string a = resources.GetString(cc.Name + ".Items" + i.ToString(), new CultureInfo(lang));
                    (cc as ComboBox).Items[i] = a;
                }
            }
        }
        //}
        if (cc is Label || cc is Button || cc is RadioButton || cc is TabControl || cc is TabPage)
        {
            resources.ApplyResources(cc, cc.Name, new CultureInfo(lang));
            //toolTip.SetToolTip(cc, resources.GetString(cc.Name + ".ToolTip", new CultureInfo(lang)));
        }
        if (cc is Panel || cc is GroupBox || cc is ListBox || cc is TabControl || cc is TabPage)
        {
            ChangeLanguageToRu(cc.Controls);
        }
    }
}

private void ChangeLanguageToAzer(System.Windows.Forms.Control.ControlCollection cont)
{
    string lang = "az-Latn";

    foreach (System.Windows.Forms.Control cc in cont)
    {
        if (cc is ComboBox)
        {

            for (int i = 0; i < (cc as ComboBox).Items.Count; i++)
            {
                if (i == 0)
                {
                    string a = resources.GetString(cc.Name + ".Items", new CultureInfo(lang));
                    (cc as ComboBox).Items[i] = a;
                }
                else
                {
                    string a = resources.GetString(cc.Name + ".Items" + i.ToString(), new CultureInfo(lang));
                    (cc as ComboBox).Items[i] = a;
                }
            }
        }
        //}
        if (cc is Label || cc is Button || cc is RadioButton || cc is TabControl || cc is TabPage)
        {
            resources.ApplyResources(cc, cc.Name, new CultureInfo(lang));
            //toolTip.SetToolTip(cc, resources.GetString(cc.Name + ".ToolTip", new CultureInfo(lang)));
        }
        if (cc is Panel || cc is GroupBox || cc is ListBox || cc is TabControl || cc is DataGridView || cc is TabPage)
        {
            ChangeLanguageToAzer(cc.Controls);
        }

    }
}

         // Delete SP1/SP2/PU
        // 13_09_2018
         private void button9_Click(object sender, EventArgs e)
        {

            // --------------------------------------------------------------------------------------

            // Есть Repaint
            if (tcParam.SelectedIndex == 0)
            {

                // --------------------------------------------------------------------------------------
                // 14_09_2018
                // Для пеленгов

                // Нажата птичка для отрисовки пеленгов
                if (GlobalVarLn.flPelMain == 1)
                {
                    // FRCH
                    GlobalVarLn.PrevP1 = -1;
                    GlobalVarLn.fl_PelIRI_1 = 0; // 1-й пеленг
                    GlobalVarLn.flag_eq_draw1 = 0;

                    // 28_09_2018
                    GlobalVarLn.PELENGG_1 = -1;
                    //------------------------------------------------------------------------
                    //PPRCH
                    for (int iii = (GlobalVarLn.list_PelIRI.Count - 1); iii >= 0; iii--)
                    {


                        PelIRI[] mass = GlobalVarLn.list_PelIRI.ToArray();
                        mass[iii].Pel1 = -1;
                        GlobalVarLn.list_PelIRI = mass.ToList();

                        if (
                            (GlobalVarLn.list_PelIRI[iii].Pel1 == -1) &&
                            (GlobalVarLn.list_PelIRI[iii].Pel2 == -1) &&
                            (GlobalVarLn.list_PelIRI[iii].Lat == -1) &&
                            (GlobalVarLn.list_PelIRI[iii].Long == -1)
                           )
                        {
                            GlobalVarLn.list_PelIRI.Remove(GlobalVarLn.list_PelIRI[iii]);
                        }
                    }
                    //------------------------------------------------------------------------

                } // Нажата птичка для отрисовки пеленгов
                // --------------------------------------------------------------------------------------

                ClassMap.ClearSostSP1();

             // 10_10_2018
             ClickGetGNSS(-1, -1, -1);



            }

            else if (tcParam.SelectedIndex == 1)
            {

                // --------------------------------------------------------------------------------------
                // 14_09_2018
                // Для пеленгов

                // Нажата птичка для отрисовки пеленгов
                if (GlobalVarLn.flPelMain == 1)
                {
                    // FRCH
                    GlobalVarLn.PrevP2 = -1;
                    GlobalVarLn.flPelMain2 = 0;   // 2-й пеленг
                    GlobalVarLn.flag_eq_draw2 = 0;

                    // 28_09_2018
                    GlobalVarLn.PELENGG_2 = -1;
                    //------------------------------------------------------------------------
                    // PPRCH
                    for (int iii = (GlobalVarLn.list_PelIRI.Count - 1); iii >= 0; iii--)
                    {


                        PelIRI[] mass = GlobalVarLn.list_PelIRI.ToArray();
                        mass[iii].Pel2 = -1;
                        GlobalVarLn.list_PelIRI = mass.ToList();

                        if (
                            (GlobalVarLn.list_PelIRI[iii].Pel1 == -1) &&
                            (GlobalVarLn.list_PelIRI[iii].Pel2 == -1) &&
                            (GlobalVarLn.list_PelIRI[iii].Lat == -1) &&
                            (GlobalVarLn.list_PelIRI[iii].Long == -1)
                           )
                        {
                            GlobalVarLn.list_PelIRI.Remove(GlobalVarLn.list_PelIRI[iii]);
                        }
                    }
                    //------------------------------------------------------------------------

                    // Убрать с карты
                    //GlobalVarLn.axMapScreenGlobal.Repaint();

                } // Нажата птичка для отрисовки пеленгов
                // --------------------------------------------------------------------------------------

                ClassMap.ClearSostSP2();

                // 10_10_2018
               ClickGetGNSS2(-1, -1, -1);

            }

            else
                ClassMap.ClearSostPU();

        }



    } // Class
} // Namespace
