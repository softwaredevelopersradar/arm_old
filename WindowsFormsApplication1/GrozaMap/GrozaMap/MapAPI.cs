﻿using System;
using System.Runtime.InteropServices;
using System.Text;

namespace Berezina.Maps
{
    [StructLayout(LayoutKind.Sequential, CharSet = System.Runtime.InteropServices.CharSet.Ansi)]

    public struct IMGTEXT           // (142) ШРИФТ (size = 32)
    {
        public long Color;   // Цвет текста
        public long BkgndColor;   // Цвет фона (IMGC_TRANSPARENT - прозрачный)
        public long ShadowColor;   // Цвет тени (IMGC_TRANSPARENT - прозрачный)
        public long Height;   // Высота (При растягивании по метрике по умолчанию 1800 мкм)
        public short Weight;   // Вес шрифта, толщина контура (FW_THIN, ...)
        public char Outline;   // Признак вывода тени в виде контура (0-не выводить, 1-тонкий контур, 2-толстый)
        public char Reserve;   // Резерв
        public short Align;   // Выравнивание (FA_LEFT|FA_BASELINE, ...)
        public short Service;   // Равно 0
        public char Wide;   // Относительная ширина символа (UNIW_NORMAL, ...)
        public char Horizontal;   // Признак горизонтального расположения (0/1)
        public char Italic;   // Признак наклона символов (0/1)
        public char Underline;   // Признак подчеркивания    (0/1)
        public char StrikeOut;   // Признак перечеркивания   (0/1)
        public char Type;   // Тип шрифта (номер в таблице: 0,1,2,3...)
        public char CharSet;   // Кодовая страница (RUSSIAN_CHARSET, ...)
        public char Flag;   // Флаги для текста : 1 - Признак растягивания по метрике
    }
    



    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
    public struct TASKPARMEX  // Параметры вызова диалогов 
    {
        public int Language; // Код языка диалогов (1 - ENGLISH, 
        // 2 - RUSSIAN, ...) 
        public int Resource;// Должен быть 0! 
        public IntPtr HelpName;// Полное имя файла ".hlp" 
        public IntPtr IniName;// Полное имя файла ".ini" приложения 
        public IntPtr PathShell;// Каталог приложения (exe,dll,...) 
        public IntPtr ApplicationName;// Имя приложения 
        public int Handle;// Идентификатор главного окна приложения 
        public int DocHandle;

        public void Init()
        {
            Language = 1;
            Resource = 0;
            HelpName = IntPtr.Zero;
            IniName = IntPtr.Zero;
            PathShell = IntPtr.Zero;
            ApplicationName = IntPtr.Zero;
            Handle = 0;
            DocHandle = 0;
        }
    }
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
    public struct ARRAYNAME
    {
        public int Code; // Код семантики для поиска
        public int Title; // Признак учета подписей
        public int Count;//Количество установленных названий семантик
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4096)]
        public char[,] semname;
        public void Init()
        {
            Code = 0;
            Title = 0;
            Count = 0;
            semname = new char[16, 256];
        }
    }
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
    public struct CHOICEOBJECTPARM
    {
        int hSelect;            // Фильтр (список доступных локализаций объекта)

        int MapSelect;              // Фильтр для работы со списком карт
        //  = 0  - список по всем картам документа
        //  = 1  - список из одной карты (карта будет
        //         запрошена из идентификатора объекта)

        int MapEditSelect;          // Фильтр для работы со списком карт (по признаку редактирования)
        //  = 0  - список по всем картам документа
        //  = 1  - список из карт, подлежащим редактированию

        public void Init()
        {
            hSelect = 0;
            MapSelect = 0;
            MapEditSelect = 0;
        }

    }
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
    public struct MEDRSCPARM
    {
        int Regime;                  // Режим создания объекта одним из способов:
        // от MC_POLYLINE до MC_THREEPOINT
        // == -1 - режимы создания недоступны в диалоге!
        // Если установлен режим, не соответствующий локализации,
        // будет установлен MC_POLYLINE

        int Repeat;                  // Повторяемость семантики
        // == 1 - повторяемая
        // == 0 - неповторяемая
        // == -1 - недоступна в диалоге

        int FlagKey;                 // Флаг установки режимов создания
        // если == 0, то доступность режимов
        // устанавливается автоматически,
        // иначе - в соответствии с Кеу

        int Rezerv;                  // Резерв
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 128)]
        public char[] NameDlg; // Название диалога
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 32)]
        public char[] Key;  // Уникальный идентификатор вида объекта в классификаторе
        public void Init()
        {
            Regime = -1;
            Repeat = -1;
            FlagKey = 0;
            NameDlg = new char[128];
            Key = new char[32];
        }

    }
    public struct SELECTCOLOR
    {
        public axGisToolKit.TxColorRef aColor;
        public void Init()
        {//задаем цвет выделения объектов в виде RGB
            aColor = new axGisToolKit.TxColorRef();
            aColor.Red = 243;
            aColor.Green = 0;
            aColor.Blue = 97;
        }
    }
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
    public struct DOUBLEPOINT                     // КООРДИНАТЫ ТОЧКИ
    {
        public double X;
        public double Y;
        public void Point(double x, double y)
        {
            X = x; Y = y;
        }
    }

    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
    public struct PLACEDATA
    {
        public DOUBLEPOINT[] Points;      // Списки координат отдельных контуров
        public int PolyCounts;      // Списки количества точек контуров
        public int Count;      // Количество контуров или структур TEXTRECORD'
        public void Init()
        {

            Count = 1;
            PolyCounts = 0;

        }
    }
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
    public struct DFrame
    {
        public double X1;
        public double Y1;
        public double X2;
        public double Y2;
        public bool Empty
        {
            get { return (X1.Equals(0.0) && X2.Equals(0.0) && Y1.Equals(0.0) && Y2.Equals(0.0)); }
        }

    }
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
    public struct MAPREGISTEREX             // ФОРМУЛЯР РАЙОНА РАБОТ
    {
        public int Length;              // Размер данной структуры
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)]
        public string Name;                // Имя района
        public int Scale;               // Знаменатель масштаба
        public int ProjectionFlag;      // Флаг соответствия проекции
        public int EllipsoideKind;      // Вид эллипсоида
        public int HeightSystem;        // Система высот
        public int MaterialProjection;  // Проекция исх. материала
        public int CoordinateSystem;    // Система координат
        public int PlaneUnit;           // Единица измерения в плане
        public int HeightUnit;          // Единица измерения по высоте
        public int FrameKind;           // Вид рамки
        public int MapType;             // Обобщенный тип карты
        public int DeviceCapability;    // Разрешающая способность прибора
        // Обычно равна 20 000
        // Для карт повышенной точности:
        // -1 - максимальная точность
        // -2 - хранить координаты в сантиметрах
        // -3 - хранить координаты в миллиметрах
        public int DataProjection;      // Наличие данных о проекции
        public int OrderViewSheetFlag;  // Порядок отображения листов
        public int FlagRealPlace;       // Признак реальных координат
        public int ZoneNumber;          // Заполняется системой при запросе
        // формуляра - номер зоны топокарты

        // В радианах:
        public double FirstMainParallel;   // Первая главная параллель
        public double SecondMainParallel;  // Вторая главная параллель
        public double AxisMeridian;        // Осевой меридиан
        public double MainPointParallel;   // Параллель главной точки
        public double PoleLatitude;        // Широта полюса проекции
        public double PoleLongitude;       // Долгота полюса проекции
        public double FalseEasting;             // Резерв = 0
        public double FalseNorthing;             // Резерв = 0
        public double ScaleFactor;             // Резерв = 0
        public double TurnAngle;             // Резерв = 0
        public double Reserv5;             // Резерв = 0
        public double Reserv6;             // Резерв = 0
        public double Reserv7;             // Резерв = 0
        public double Reserv8;             // Резерв = 0
    }
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
    public struct DATUMPARAM
    {
        double DX;           // Сдвиги по осям в метрах
        double DY;
        double DZ;
        double RX;           // Угловые поправки в секундах
        double RY;
        double RZ;
        double M;            // Поправка масштаба
        int Count;        // 3 или 7  (14 - признак пересчета через ПЗ-90.02 для СК42\95)
        int Reserve;      // Равно 0
    }
    public struct ELLIPSOIDPARAM
    {
        double SemiMajorAxis;          // Длина большой полуоси эллипсоида
        double InverseFlattening;      // Полярное сжатие эллипсоида
    }
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
    public struct TASKPARM              // Параметры вызова диалогов
    {
        public int Language;            // Код языка диалогов (1 - ENGLISH,
        // 2 - RUSSIAN, ...)
        public int Resource;            // Должен быть 0!
        public IntPtr HelpName;         // Полное имя файла ".hlp"
        public IntPtr IniName;          // Полное имя файла ".ini" приложения
        public IntPtr PathShell;        // Каталог приложения (exe,dll,...)
        public IntPtr ApplicationName;  // Имя приложения
        public int Handle;              // Идентификатор главного окна приложения
        public void Init()
        {
            Language = 1;
            Resource = 0;
            HelpName = IntPtr.Zero;
            IniName = IntPtr.Zero;
            PathShell = IntPtr.Zero;
            ApplicationName = IntPtr.Zero;
            Handle = 0;
        }
    }

    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
    public struct CREATETIFPARM
    {
        public int BandCount;    // количество каналов
        public int BitInBand;    // бит на канал
        public int PixelType;    // тип пикселя (PT_BYTE и т.д.)
        public int Compress;     // тип компрессии (см. NOTCOMPRESS и т.д.)
                                 // JPEG - возможен только для 3 и 1 канальных 8, 16 битных растров
                                 //      - для 3 канальных допустим ColorSpace = CS_RGB или CS_YCBCR (лучшее сжатие, но медленнее)
                                 //      - при CS_YCBCR в буфер тайла записываются RGB
        public int IsPredictor;  // для LZW и DEFLATE компрессии - признак сжатия расхождений
                                 // 0 - сжимаются сами цвета
                                 // 1 - сжимаются расхождения между соседними пикселями в строке
        public int JPEGQuality;  // качество JPEG сжатия (1-100) (по умолчанию 75)
        public int ColorSpace;   // интерпретация каналов для 3 канальных растров (CMYK для 4 канальных) см. CS_RGB и т.д.
        public int DeflateLevel; // уровень DEFLATE сжатия (1-9) (по умолчанию 6)
        public int TileWidth;    // ширина тайла в пикселях
        public int TileHeight;   // высота тайла в пикселях
        public int BigTIFF;      // условия создания BIGTIFF (см. BT_NO и т.д.)
        public int IsPlane;      // признак хранения изображения по плоскостям (0 - RGB RGB ..., 1 - RR...BB...GG...)
                                 // при JPEG сжатии с цветовой схемой YCBCR должно быть = 0
        public int IsAlpha;      // признак использования альфа канала
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 256)]
        public RGBQUAD[] Palette; // палитра 1,4,8 битных растров
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 64)]
        public int[] Reserved;
        public void Init()
        {
            BandCount = 3;
            BitInBand = 16;
            PixelType = 2;
            Compress = 1;
            ColorSpace = 2;
            TileWidth = 128;
            TileHeight = 128;
            BigTIFF = 0;
            IsPlane = 0;
            IsAlpha = 0;
            RGBQUAD[] Palette = new RGBQUAD[256];
            Reserved = new int[64];

        }
    }
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
    public struct RGBQUAD
    {
        public int rgbRed;
        public int rgbGreen;
        public int rgbBlue;
        public int rgbReserved;
    }

    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
    public struct CREATESITE                // СОЗДАНИЕ ПОЛЬЗОВАТЕЛЬСКОЙ КАРТЫ
    {
        public int Length;              // Длина записи структуры
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)]
        public string MapName;            // Имя района
        public int MapType;            // Обобщенный тип карты
        public int MaterialProjection; // Проекция исх. материала
        public int Scale;              // Знаменатель масштаба карты
                                       // В радианах:
        public double FirstMainParallel;  // Первая главная параллель
        public double SecondMainParallel; // Вторая главная параллель
        public double AxisMeridian;       // Осевой меридиан
        public double MainPointParallel;  // Параллель главной точки
    }

    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
    public struct BUILDSURFACE  // 15/06/11
    {

        public uint StructSize; // Размер данной структуры : sizeof (BUILDSURFACE) = 320 байт
        public int FileMtw;    // Флаг расширения создаваемого файла :
                               //   0 - создание файла матрицы качеств (*.mtq)
                               //   1 - создание файла матрицы высот (*.mtw)

        public double BeginX;    // Прямоугольные координаты начала
        public double BeginY;    // (юго-западного угла) матрицы в метрах

        public double Width;     // Ширина матрицы в метрах
        public double Height;    // Высота матрицы в метрах

        public double MinValue;  // Диапазон значений характеристики качества создаваемой матрицы,
        public double MaxValue;  // если MinValue >= MaxValue в матрицу заносится фактический диапазон значений

        public double ElemSizeMeters;  // Размер стороны элементарного участка
                                       // в метрах на местности (дискрет матрицы)

        public int UserType;           // Произвольное число, связываемое с создаваемой матрицей
                                       //  (тип, характеристика матрицы)

        public int SearchSectorCount;  // Количество секторов для поиска соседних точек (для Method = 9,11,12) // 09/06/11
                                       // 1 - по кругу
                                       // 4 - по 4 секторам
                                       // 8 - по 8 секторам

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 32)]
        public char[] UserName;      // Условное имя матрицы (название характеристики качества)

        public int Border;  // Идентификатор замкнутого объекта карты, ограничивающего область
                            // заполняемых элементов матрицы (если равен нулю, то не используется)
                            // Если объект расположен вне габаритов матрицы, определяемых значениями
                            // полей BeginX, BeginY, Width, Height данной структуры, то не используется

        public int Handle;  // Идентификатор окна диалога, которому посылается
                            // сообщение 0x0581 о проценте выполненных работ (в WPARAM),
                            // если процесс должен быть принудительно завершен, в ответ
                            // должно вернуться значение 0x0581.
                            // Если Handle равно нулю - сообщения не посылаются.

        public IntPtr Palette;      // Адрес палитры создаваемой матрицы качеств(*.mtq),
                                    // если равен нулю - используется палитра по умолчанию
        public int PaletteCount;       // Kоличество цветов в палитре (от 1 до 256)

        public int Method;             // Метод построения поверхности :
                                       //  8 - Линейная интерполяция по сетке треугольников
                                       //      ( используются точки массива PointArray )
                                       //  9 - Логарифмическая интерполяция по локальной поверхности
                                       //       вокруг определяемого элемента
                                       //      ( используются точки метрики объектов карты )
                                       // 10 - Линейная интерполяция по сетке треугольников
                                       //      ( используются точки метрики объектов карты )
                                       // 11 - Кригинг
                                       // 12 - Кокригинг // 19/11/12

        public int PointArray;  // Адрес массива значений характеристики качества (для Method = 8)
        public int PointCount;    // Число точек в массиве PointArray (для Method = 8)

        public int SemanticCode;  // Код семантики моделируемой характеристики
                                  // если = 0, то не используется 

        public int LocalSurfacePointCount;  // Kоличество точек для построения локальной поверхности
                                            // вокруг определяемого элемента (для Method = 9,11,12)
                                            // если SearchSectorCount = 4, то LocalSurfacePointCount должно быть кратно 4
                                            // если SearchSectorCount = 8, то LocalSurfacePointCount должно быть кратно 8

        public int LocalSurfaceRebuildPointCount;  // Kоличество обновленных точек при переходе
                                                   // к очередному определяемому элементу,
                                                   // при котором перестраивается локальная
                                                   // поверхность (для Method = 9)
        public double MaxMetricCutLength;  // Шаг добавления точек по контуру линейных и площадных объектов (для Method = 9,11,12)
                                           // Используется если IsMetricCutLength = 1

        public int Use3DMetric;            // Флаг использования высот из трехмерной метрики объектов :  // 06/11/07
                                           //  0 - высоты из трехмерной метрики не используются
                                           //  1 - высоты из трехмерной метрики используются


        public int SemanticCode2;          // Код семантики дополнительной характеристики для кокригинга (для Method = 12) // 13/02/13

        public int FillBorderType;         // Тип заполнения матрицы внутри ограничивающего объекта Border
                                           // 0 - заполняется весь объект
                                           // 1 - заполняется объект без подобъектов
                                           // 2 - заполняются объект и подобъекты
                                           // 3 - заполняются только подобъекты

        public int IsMetricCutLength; // Добавлять точки по контуру линейных и площадных объектов (для Method = 9,11,12) // 09/06/11
                                      // Точки добавляются с шагом MaxMetricCutLength

        public int IsAddPointsInEmptyRegion; // Добавлять точки в пустых областях (для Method = 9,11,12) // 09/06/11

        public int IsLimitHeight;      // Если включено, то высота элемента матрицы не может быть больше или меньше высот (для Method = 9,11,12) // 09/06/11
                                       // соседних точек, по которым строится поверхность, на LimitOffset

        public double DistBeforePointsInEmptyRegion; // Расстояние между точками в пустых областях (для Method = 9,11,12) // 09/06/11
                                                     // Значение высоты (характеристики) в этих
                                                     // точках определяется методом интерполяции по 8 направлениям.
                                                     // Эти точки добавляются для того, чтобы в пустых областях избавится от появления
                                                     // гор и ям в случаях когда на границе пустых областей есть значительные градиенты.
        public double LimitOffset; // Если включено IsLimitHeight то разрешенное отклонение вычисленного  (для Method = 9,11,12) // 09/06/11
                                   // значения матрицы от минимального, максимального значения среди соседних точек
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 128)]
        public char[] Reserve;     // Должны быть нули

        public void Init()
        {

            StructSize = 0;
            FileMtw = 0;
            BeginX = 0;
            BeginY = 0;
            Width = 0;
            Height = 0;
            MinValue = 0;
            MaxValue = 0;
            ElemSizeMeters = 0;
            UserType = 0;
            SearchSectorCount = 0;
            UserName = new char[32];
            Border = 0;
            Handle = 0;
            //Palette = 0;
            PaletteCount = 0;
            Method = 0;
            PointArray = 0;
            PointCount = 0;
            SemanticCode = 0;
            FillBorderType = 0;
            IsMetricCutLength = 0;
            IsAddPointsInEmptyRegion = 0;
            IsLimitHeight = 0;
            DistBeforePointsInEmptyRegion = 0;
            LimitOffset = 0;
            Reserve = new char[128];



        }
    }
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
    public struct BUILDMTW
    {
        public uint StructSize;          // Размер данной структуры : sizeof (BUILDMTW)

        public int NotCheckDiskFreeSpace;  // Флаг - не проверять наличие свободного места на диске  17/04/13
                                           // при создании файла матрицы :
                                           //   0 - проверять наличие свободного места
                                           //   1 - не проверять наличие свободного места

        public double BeginX;         // Прямоугольные координаты начала
        public double BeginY;         // (юго-западного угла) матрицы в метрах

        public double Width;          // Ширина матрицы в метрах
        public double Height;         // Высота матрицы в метрах

        public double ElemSizeMeters; // Размер стороны элементарного участка
                                      // в метрах на местности

        public int ElemSizeBytes;       // Pазмер элемента матрицы в байтах
                                        // (допустимые значения : 1,2,4,8)
                                        // При создании матрицы высот MTW по векторной карте функцией
                                        // mapBuildMtw значение данного поля должно быть равно 4
                                        // Значение 1 соответствует типу "unsigned char" (код отсутствия данных = 0)
                                        // Значение 2 соответствует типу "short int" (код отсутствия данных = -32767)
                                        // Значение 4 соответствует типу "long int" (код отсутствия данных = -32767000)
                                        // Значение 8 соответствует типу "double" (код отсутствия данных = -32767000)

        public int Unit;                // Eдиница измерения высоты матрицы высот MTW
                                        // (0 - метры, 1 - дециметры,
                                        //  2 - сантиметры, 3 - миллиметры )

        public int ReliefType;          // Тип матрицы высот MTW (0 - абсолютные высоты,
                                        //  1 - абсолютные + относительные,
                                        //  2 - относительные)

        public int UserType;            // Произвольное число, связываемое
                                        // с создаваемой матрицей

        public int Scale;               // Знаменатель масштаба создаваемой
                                        // матричной карты (при создании матрицы высот MTW
                                        // по векторной карте функцией mapBuildMtw значение
                                        // данного поля будет заменено на знаменатель
                                        // масштаба векторной карты)

        public int HeightSuper;         // Флаг занесения высоты в элемент при создании матрицы
                                        // высот MTW по векторной карте функцией mapBuildMtw и при попадании
                                        // в элемент более одного объекта с абсолютной высотой :
                                        //  0 - заносить среднюю высоту
                                        //  1 - заносить максимальную высоту
                                        //  2 - заносить минимальную высоту

        public int FastBuilding;        // УСТАРЕВШЕЕ ПОЛЕ
                                        // Режим создания матрицы (при Method = 0) :
                                        //  0 - средневзвешенная интерполяция по 16 направлениям,
                                        //    без формирования локальных экстремумов
                                        //  1 - средневзвешенная интерполяция по 8 направлениям,
                                        //    без формирования локальных экстремумов
                                        //  2 - средневзвешенная интерполяция по 16 направлениям,
                                        //    с формированием локальных экстремумов
                                        //  3 - средневзвешенная интерполяция по 8 направлениям,
                                        //    с формированием локальных экстремумов

        public int Method;          // Метод построения поверхности при создании матрицы высот MTW
                                    // по векторной карте функцией mapBuildMtw :
                                    //  0 - метод определяется режимом FastBuilding
                                    //  1 - средневзвешенная интерполяция по 16 направлениям
                                    //  2 - средневзвешенная интерполяция по 8 направлениям
                                    //  3 - средневзвешенная интерполяция по 16 направлениям,
                                    //      сглаживание поверхности
                                    //  8 - линейная интерполяция по сетке треугольников
                                    //     (в методе используются только точечные объекты)
                                    //  16 - плоская поверхность с нулевой высотой
                                    //       для ReliefType = 2

        public int Extremum;       // Флаг формирования локальных экстремумов
                                   //
                                   // функцией mapBuildMtw (при Method = 1,2,3) :
                                   //  0 - локальные экстремумы не формируются
                                   //  1 - локальные экстремумы формируются

        public int Border;        // Идентификатор замкнутого объекта при создании
                                  // матрицы высот MTW по векторной карте функцией mapBuildMtw.
                                  // По данному объекту устанавливается рамка создаваемой матрицы
                                  // (если объект расположен вне габаритов матрицы,
                                  // определяемых значениями полей BeginX, BeginY, Width, Height
                                  // данной структуры, то рамка не устанавливается)

        public int LimitMatrixFrame;
        // Флаг ограничения габаритов при создании матрицы MTW
        // по векторной карте функцией mapBuildMtw при Method = 1,2,3,8
        // (габариты матрицы определяются значениями полей
        // BeginX, BeginY, Width, Height данной структуры) :
        //  0 - ограничение не выполняется
        //  1 - габариты матрицы ограничиваются
        //      габаритами района
        //  2 - габариты матрицы ограничиваются
        //      габаритами области расположения
        //      объектов с абсолютной высотой


        public int NotUse3DMetric;
        // Флаг - не использовать трехмерную метрику объектов
        // при создании матрицы MTW по векторной карте функцией mapBuildMtw:
        //  0 - трехмерная метрика используется
        //  1 - трехмерная метрика не используется

        public int SurfaceSquare3DObject;
        // Флаг при создании матрицы MTW по векторной карте функцией mapBuildMtw -
        // строить поверхность внутри площадного
        // объекта по его трехмерной метрике :
        //  0 - не строить поверхность
        //  1 - строить поверхность
        // Если параметр NotUse3DMetric равен 1,
        // то поверхность не строится

        public int AltitudeMarksNet;
        // Флаг дополнительной обработки высотных точек при
        // создании матрицы MTW по векторной карте функцией mapBuildMtw для
        // средневзвешенной интерполяции (Method = 1,2,3) :
        //  0 - для каждой высотной точки выполняется построение и
        //      занесение в матрицу лучей влияния высоты (луч влияния
        //      высоты - 3D-отрезок,выходящий из точки по одному из
        //      16 радиальных направлений; высоты элементов отрезка
        //      определяются высотой точки и высотой, найденной при
        //      сканировании матрицы из точки по данному направлению.
        //      Лучи влияния высоты компенсируют промахи мимо точки
        //      с абсолютной высотой при поиске значащих высот в процессе
        //      вычисления незаполненых элементов матрицы.
        //  1 - по набору высотных точек создается триангуляция,
        //      ребра триангуляции (или части ребер) заносятся в матрицу
        //      в виде 3D-отрезков (если ребро не пересекает объекты
        //      с абсолютной высотой, то оно выводится целиком,
        //      если пересекает, то выводится часть ребра, ограниченная
        //      его вершиной и точкой пересечения с объектом);
        //  2 - дополнительная обработка высотных точек не выполняется,
        //      высота точки заносится в один элемент матрицы,
        //      обычно применяется при построении поверхности
        //      по набору отметок высот

        public int LimitMatrixByFramesOfSheets;
        // Флаг ограничения матрицы рамками листов при создании
        // матрицы MTW по векторной карте функцией mapBuildMtw (для Method = 1,2,3,8):
        //  0 - ограничение матрицы не выполняется,
        //      информативные элементы могут располагаться
        //      вне рамок листов
        //  1 - ограничение матрицы выполняется,
        //      информативные элементы располагаются
        //      только внутри габаритов рамок листов

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 20)]
        public char[] Reserve;  // Должны быть нули
    }
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
    public struct MTRPROJECTIONDATA  //  ПАРАМЕТРЫ СОЗДАНИЯ МАТРИЦЫ ВЫСОТ
    {                                 //   (ДАННЫЕ О ПРОЕКЦИИ)
        public int StructSize;              // Размер данной структуры : 128 байт
                                            //  sizeof (MTRPROJECTIONDATA)

        public int Free;                    // Должен быть ноль

        public int MapType;                 // Тип карты (задавать в соответствии
                                            //   с MAPTYPE, файл MAPCREAT.H)

        public int ProjectionType;          // Тип проекции (задавать в соответствии
                                            //   с MAPPROJECTION, файл MAPCREAT.H)
                                            // long  MaterialProjection; // Проекция исх. материала

        // В радианах
        public double FirstMainParallel;  // Первая главная параллель
        public double SecondMainParallel; // Вторая главная параллель
        public double AxisMeridian;       // Осевой меридиан
        public double MainPointParallel;  // Параллель главной точки
        public double PoleLatitude;       // Широта полюса проекции                 // 27/06/05
        public double PoleLongitude;      // Долгота полюса проекции                // 27/06/05

        public int EllipsoideKind;     // Вид эллипсоида                         // 01/07/05
        public int HeightSystem;       // Система высот                          // 01/07/05
        public int CoordinateSystem;   // Система координат                      // 01/07/05
        public int ZoneNumber;         // Номер зоны топокарты                   // 28/11/07

        public double FalseEasting;    // Смещение координат по оси Y            // 05/07/10
        public double FalseNorthing;   // Смещение координат по оси X            // 05/07/10
        public double ScaleFactor;     // Масштабный коэффициент                 // 05/07/10
        public double TurnAngle;       // Угол разворота осей для локальных систем (МСК)   // 05/07/10
        public int ZoneIdent;       // Идентификатор района (для МСК 63: A-X или 0)     // 26/04/11

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 12)]
        public char[] Reserve;     // Должны быть нули

    }
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
    public struct BUILDVISIBLE
    {
        public double Radius;   // предельный радиус видимости
        public double ElemSize; // размер элемента создаваемой матрицы в метрах
        public int Color;    // цвет, которым показывается элементы, с которых видны точки
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 63)]
        public int[] Reserved;
    }


    //-----------------------------------------------------------------------------------------------------
    public class Semantic_DLG
    {
        public const string GisLibrary1 = "C:\\Windows\\SysWOW64\\gisforms.dll";
        [DllImport(GisLibrary1, CharSet = CharSet.Ansi)]
        public static extern int selSearchName(int hmap,
                                               int select,
                                               ref ARRAYNAME arname,
                                               ref TASKPARMEX parm);
    }
    public class PASPAPI
    {

        public const string GisLibrary3 = "C:\\Windows\\SysWOW64\\gisdlgs.dll";


        //public const string GisLibrary2 = "C:\\Windows\\SysWOW64\\gispasp.dll";
        [DllImport(GisLibrary2, CharSet = CharSet.Ansi)]
        public static extern int paspCreateMap(int hmap,
                                               StringBuilder mapname,
                                               int size,
                                               ref TASKPARMEX parm);

        public const string GisLibrary2 = "C:\\Windows\\SysWOW64\\gispasp.dll";
        [DllImport(GisLibrary2, CharSet = CharSet.Ansi)]
        public static extern int paspCreatePlan(StringBuilder mapname,
                                                int size,
                                                ref TASKPARMEX parm);

        [DllImport(GisLibrary3, CharSet = CharSet.Ansi)]
        public static extern int scnChoiceNewObject(int hmap, ref TASKPARMEX parm, int hobj, ref MEDRSCPARM medparm, ref CHOICEOBJECTPARM choiceparm,
                                    int hselect);


        [DllImport(GisLibrary2, CharSet = CharSet.Ansi)]
        public static extern int paspViewPasp(int hmap, int hsite, ref TASKPARMEX parm);

        public const string GisLibrary = "C:\\Windows\\System32\\gisacces.dll";

        [DllImport(GisLibrary, CharSet = CharSet.Ansi)]
        public static extern int mapGetActiveSite(int hMap);

        public const string GisLibrary4 = "C:\\Windows\\System32\\gisvectr.dll";

        [DllImport(GisLibrary4, CharSet = CharSet.Ansi)]
        public static extern int MapSort(int hmap, ref TASKPARM parm);

    }
    public class EditMapObj
    {
        //   public const string GisLibrary1 = "C:\\Windows\\SysWOW64\\gisacces.dll";   
        //удалить подобъект по номеру
        [DllImport(GisLibrary1, CharSet = CharSet.Ansi)]
        public static extern int mapDeleteSubject(int info, int number);

        // удалить объект по порядковому номеру
        [DllImport(GisLibrary1, CharSet = CharSet.Ansi)]
        public static extern int mapDeleteObjectByNumber(int hMap,
                                                         int list, int number);
        // получить порядковый номер объекта
        [DllImport(GisLibrary1, CharSet = CharSet.Ansi)]
        public static extern int mapGetObjectNumber(int info);


        [DllImport(GisLibrary1, CharSet = CharSet.Ansi)]
        public static extern int mapGetObjectCenter(int hmap, int info, ref double x, ref double y);
        [DllImport(GisLibrary1, CharSet = CharSet.Ansi)]
        public static extern double mapSideDirection(int info, int number, int subject);

        [DllImport(GisLibrary1, CharSet = CharSet.Ansi)]
        public static extern IntPtr mapSetRscObjectKey(int hRsc, int incode, StringBuilder key);
        public static string mapSetRscObjectKey1(int hRsc, int incode, StringBuilder key)
        {
            IntPtr ptr = mapSetRscObjectKey(hRsc, incode, key);
            if (ptr == IntPtr.Zero) return string.Empty;
            return Marshal.PtrToStringAnsi(ptr);
        }


        [DllImport(GisLibrary1, CharSet = CharSet.Ansi)]
        public static extern int mapRegisterObject(int info, int excode, int local);

        [DllImport(GisLibrary1, CharSet = CharSet.Ansi)]
        public static extern int mapSetObjectKey(int info, int number);

        [DllImport(GisLibrary1, CharSet = CharSet.Ansi)]
        public static extern int mapSetRscObjectClass(IntPtr hRsc, int incode, int ident);
        //public static extern int scnGetObjectFromRsc(int hmap, ref TASKPARMEX parm, int hobj, OBJFROMRSC* objparm, char *title);

        [DllImport(GisLibrary1, CharSet = CharSet.Ansi)]
        public static extern int mapRelocateObject(int hmap, int info, ref DOUBLEPOINT delta, int place);

        [DllImport(GisLibrary1, CharSet = CharSet.Ansi)]
        public static extern int mapGetRscObjectIdent(IntPtr hRsc, int incode);

        [DllImport(GisLibrary1, CharSet = CharSet.Ansi)]
        public static extern int mapGetRscIdent(int hmap, int hsite);

        public const string GisLibrary1 = "C:\\Windows\\SysWOW64\\gisacces.dll";
        //создать копию объекта 
        [DllImport(GisLibrary1, CharSet = CharSet.Ansi)]
        public static extern int mapCreateCopyObject(int hMap, int info);

        [DllImport(GisLibrary1, CharSet = CharSet.Ansi)]
        public static extern int mapRelocateObjectPlane(int info, ref DOUBLEPOINT delta);

        [DllImport(GisLibrary1, CharSet = CharSet.Ansi)]
        public static extern int mapCommitObject(int info);

        [DllImport(GisLibrary1, CharSet = CharSet.Ansi)]
        public static extern int mapObjectKey(int info);

        [DllImport(GisLibrary1, CharSet = CharSet.Ansi)]
        public static extern int mapCommitObjectAsNew(int info);

        //[DllImport(GisLibrary1, CharSet = CharSet.Ansi)]
        //public static extern int mapViewMapObject(int hmap, int hwnd, ref DOUBLEPOINT point, ref PAINTPARM image, int info, int place);

        [DllImport(GisLibrary1, CharSet = CharSet.Ansi)]
        public static extern int mapCreateObjectCutByLine(int info1, int info2, int method, double precision);

        //рассечение объекта по линии
        [DllImport(GisLibrary1, CharSet = CharSet.Ansi)]
        public static extern int mapGetNextCut(int hCross, int info);
        //получение результирующих объектов после рассечения 
        [DllImport(GisLibrary1, CharSet = CharSet.Ansi)]
        public static extern int mapFreeObjectsCut(int hCross);
    }
    public class RstAPI
    {
        public const string GisLibrary1 = "C:\\Windows\\SysWOW64\\gisrswg.dll";
        public const string GisLibrary2 = "C:\\Windows\\SysWOW64\\gisforms.dll";

        [DllImport(GisLibrary1, CharSet = CharSet.Ansi)]
        public static extern int RswCuttingBySelectedObjects(int hmap, ref TASKPARMEX parm, int rswNumber);

        [DllImport(GisLibrary2, CharSet = CharSet.Ansi)]
        public static extern int LoadImageToRstEx(int hmap, StringBuilder lpszsource, StringBuilder lpsztarget, ref TASKPARMEX param);

        //диалог загрузка графического изображения в формат RSW
        [DllImport(GisLibrary1, CharSet = CharSet.Ansi)]
        public static extern int DialogLoadImageToRsw(int hmap, StringBuilder lpszsource,
                                                      int sizesource, StringBuilder lpsztarget,
                                         int sizetarget, int fileLocType, ref TASKPARMEX parm);


        [DllImport(GisLibrary3, CharSet = CharSet.Ansi)]
        public static extern IntPtr mapGetRstName(int hMap, int number);


        [DllImport(GisLibrary3, CharSet = CharSet.Ansi)]
        public static extern int mapSetRstLocation(int hMap, int number, ref DOUBLEPOINT location);

        [DllImport(GisLibrary3, CharSet = CharSet.Ansi)]
        public static extern int mapSetRstBorder(int hMap, int number, int info);


        [DllImport(GisLibrary3, CharSet = CharSet.Ansi)]
        public static extern int mapGetActualRstFrame(int hMap, ref DFrame frame, int number);


        [DllImport(GisLibrary3, CharSet = CharSet.Ansi)]
        public static extern IntPtr mapGetMapName(int hmap);



        [DllImport(GisLibrary3, CharSet = CharSet.Ansi)]
        public static extern int mapAppendData(int hMap, string name, int mode);

        public const string GisLibrary3 = "C:\\Windows\\SysWOW64\\gisacces.dll";
        //создать временную пользовательскую карту 
        [DllImport(GisLibrary3, CharSet = CharSet.Ansi)]
        public static extern int mapCreateAndAppendTempSite(int hmap, IntPtr rscname);

        [DllImport(GisLibrary3, CharSet = CharSet.Ansi)]
        public static extern int mapDeleteRstBorder(int hMap, int number);

        [DllImport(GisLibrary3, CharSet = CharSet.Ansi)]
        public static extern int mapSetDocProjection(int hMap, ref MAPREGISTEREX map, ref DATUMPARAM datum, ref ELLIPSOIDPARAM ellparm);

        public const string GisLibrary4 = "gispicex.dll";
        //привязка растра с масштабированием по двум точкам
        [DllImport(GisLibrary4, CharSet = CharSet.Ansi)]
        public static extern int AttachRswWithScalingEx(int hMap, IntPtr rswName,
                                ref DOUBLEPOINT pointMet1, ref DOUBLEPOINT pointMetNew1,
                                ref DOUBLEPOINT pointMet2, ref DOUBLEPOINT pointMetNew2,
                                                                            int message);


        [DllImport(GisLibrary4, CharSet = CharSet.Ansi)]
        public static extern int AttachRswWithScaling(int hMap, IntPtr rswName, ref DOUBLEPOINT pointMet1, ref DOUBLEPOINT pointMetNew1,
                                                             ref DOUBLEPOINT pointMet2, ref DOUBLEPOINT pointMetNew2, int message);

        [DllImport(GisLibrary4, CharSet = CharSet.Ansi)]
        public static extern int AttachRswWithScalingAndRotation(int hmap, int handle, IntPtr rswName, ref DOUBLEPOINT pointMet1, ref DOUBLEPOINT pointMetNew1,
                                                             ref DOUBLEPOINT pointMet2, ref DOUBLEPOINT pointMetNew2, int message);

        //диалог "Оптимизация растра карты"
        [DllImport(GisLibrary1, CharSet = CharSet.Ansi)]
        public static extern int LoadRstCompressDialog(int hmap, int handle,
                         StringBuilder name, ref TASKPARM parm, int reDraw);

        [DllImport(GisLibrary3, CharSet = CharSet.Ansi)]
        public static extern int mapGetTotalBorder(int hmap, ref DFrame dframe, int place);

        //загрузить курсор из файла
        [DllImport("User32.dll", SetLastError = true)]
        public static extern IntPtr LoadCursorFromFile(String str);

        //сохранить карту в формат BMP, TIFF, RSW
        [DllImport(GisLibrary4, CharSet = CharSet.Ansi)]
        public static extern int LoadMapToPicture(int map, int handle, ref DFrame dframe,
                                                  int bitcount, int scale, int resolution,
                                                  IntPtr filename, int handleMainWin);
        //Запросить масштаб растра
        [DllImport(GisLibrary3, CharSet = CharSet.Ansi)]
        public static extern int mapGetRstScale(int hMap, int number, ref double scale);

        [DllImport(GisLibrary3, CharSet = CharSet.Ansi)]
        public static extern int mapSaveRst(int hMap, int number);
    }

    public class MultiSpecAPI
    {
        public const string GisLibrary = "C:\\Windows\\SysWOW64\\gisacces.dll";

        // Запросить тип растра
        [DllImport(GisLibrary, CharSet = CharSet.Ansi)]
        public static extern int mapRstIsAccessTiff(int hmap, int number);


        //Запросить количество каналов TIFF-растра с номером  number
        [DllImport(GisLibrary, CharSet = CharSet.Ansi)]
        public static extern int mapGetRstBandCount_Tiff(int hMap, int number);
        //Возвращает яркость пиксела изображения на канал bandnum
        [DllImport(GisLibrary, CharSet = CharSet.Ansi)]
        public static extern int mapGetRstBandPixel_Tiff(int hMap, int number,
                                      int x, int y, int bandnum, ref int color);
        //Размер элемента растра в метрах по оси X
        [DllImport(GisLibrary, CharSet = CharSet.Ansi)]
        public static extern int mapGetRstMeterInElementX(int hMap, int number,
                                                         ref double metinelemX);
        //Размер элемента растра в метрах по оси Y
        [DllImport(GisLibrary, CharSet = CharSet.Ansi)]
        public static extern int mapGetRstMeterInElementY(int hMap, int number,
                                                         ref double metinelemY);


        [DllImport(GisLibrary, CharSet = CharSet.Ansi)]
        public static extern int mapCopyRstFile(IntPtr oldname, IntPtr newname, int exist);

        [DllImport(GisLibrary, CharSet = CharSet.Ansi)]
        public static extern int mapGetRstPoint(int hMap, int number, ref int value, int row, int column);

        [DllImport(GisLibrary, CharSet = CharSet.Ansi)]
        public static extern int mapCreateTiff(IntPtr filename, int width, int height, ref CREATETIFPARM parm);



        //Создаем файл растрового изображения
        [DllImport(GisLibrary, CharSet = CharSet.Ansi)]
        public static extern int mapCreateAndAppendRstEx(int hMap, IntPtr rstname,
                                                           int width, int height,
                                                   int nbits, ref int[] palette,
                                  int colorcount, double scale, double precision,
                                  double meterInElementX, double meterInElementY,
                         ref DOUBLEPOINT location, ref MAPREGISTEREX mapregister);
        //Получаем данные о проекции карты
        [DllImport(GisLibrary, CharSet = CharSet.Ansi)]
        public static extern int mapGetRstProjectionData(int hMap, int number,
                                                 ref MAPREGISTEREX mapregister);
        //Получаем резрешение растра
        [DllImport(GisLibrary, CharSet = CharSet.Ansi)]
        public static extern int mapGetRstPrecision(int hMap, int number,
                                                         ref double precision);



        [DllImport(GisLibrary, CharSet = CharSet.Ansi)]
        public static extern int mapSetRstContrast(int hMap, int contrast, int number);
        [DllImport(GisLibrary, CharSet = CharSet.Ansi)]
        public static extern int mapSetRstBright(int hMap, int bright, int number);

        [DllImport(GisLibrary, CharSet = CharSet.Ansi)]
        public static extern int mapWriteTiffBlock(int tiff, int bandnum, int col, int row, [In, Out] int[,] image);

        [DllImport(GisLibrary, CharSet = CharSet.Ansi)]
        public static extern int mapGetPlanePoint(int info, ref DOUBLEPOINT point, int number, int subject);

        // Возвращает таблицу преобразования цвета для отображения панхроматических,
        // RGB и мультиспектральных растров с глубиной цвета 8 или 16 бит
        [DllImport(GisLibrary, CharSet = CharSet.Ansi)]
        public static extern int mapGetRstLookupTable_Tiff(int hMap, int number,
                                                                    int bandnum,
                                          [In, Out] byte[] table, int tablesize);

        [DllImport(GisLibrary, CharSet = CharSet.Ansi)]
        public static extern int mapCheckInsidePoint(int info, int subject, ref DOUBLEPOINT point);

        [DllImport(GisLibrary, CharSet = CharSet.Ansi)]
        public static extern int mapPutRstPoint(int hMap, int number, int value, int w, int h);

        [DllImport(GisLibrary, CharSet = CharSet.Ansi)]
        public static extern int mapObjectFrame(int info, ref DFrame dframe);


        [DllImport(GisLibrary, CharSet = CharSet.Ansi)]
        public static extern int mapGetRstElementSize(int hMap, int number);

    }
    public class ForSvgAPT
    {

        public const string GisLibrary = "C:\\Windows\\SysWOW64\\gisacces.dll";

        [DllImport(GisLibrary, CharSet = CharSet.Ansi)]
        public static extern int mapGetMapPlanePoint(int info, ref DOUBLEPOINT point, int number, int subject);

        [DllImport(GisLibrary, CharSet = CharSet.Ansi)]
        public static extern int mapPointCount(int info, int subject);

        // Запросить номер функции отображения объекта по внутреннему  коду
        [DllImport(GisLibrary, CharSet = CharSet.Ansi)]
        public static extern int mapGetRscObjectFunction(int hRsc, int incode);

        // Запросить длину параметров отображения объекта по внутреннему  коду
        [DllImport(GisLibrary, CharSet = CharSet.Ansi)]
        public static extern int mapGetRscObjectParametersSize(int hRsc, int incode);

        // Запросить параметры отображения объекта по внутреннему  коду
        [DllImport(GisLibrary, CharSet = CharSet.Ansi)]
        public static extern IntPtr mapGetRscObjectParameters(int hRsc, int incode);
        public static string mapGetRscObjectParameters1(int hRsc, int incode)
        {
            IntPtr ptr = mapGetRscObjectParameters(hRsc, incode);
            if (ptr == IntPtr.Zero) return string.Empty;
            return Marshal.PtrToStringAnsi(ptr);
        }
        // Запросить количество примитивов в параметрах отображения объекта по
        // внутреннему коду (порядковому номеру) объекта (с 1) и виду отображения
        // viewtype: 0 - экранный, 1 - принтерный
        [DllImport(GisLibrary, CharSet = CharSet.Ansi)]
        public static extern int mapGetRscPrimitiveCount(int hRsc, int incode, int viewtype = 0);

        // Запросить номер функции отображения примитива по порядковому
        // номеру примитива в параметрах отображения объекта ,
        // внутреннему коду (порядковому номеру) объекта (с 1) и виду отображения
        // viewtype: 0 - экранный, 1 - принтерный
        [DllImport(GisLibrary, CharSet = CharSet.Ansi)]
        public static extern int mapGetRscPrimitiveFunction(int hRsc, int incode, int number, int viewtype = 0);
        // Запросить длину параметров примитива по порядковому
        // номеру примитива в параметрах отображения объекта ,
        // внутреннему коду (порядковому номеру) объекта (с 1) и виду отображения
        // viewtype: 0 - экранный, 1 - принтерный
        [DllImport(GisLibrary, CharSet = CharSet.Ansi)]
        public static extern int mapGetRscPrimitiveLength(int hRsc, int incode, int number, int viewtype = 0);

        // Запросить адрес параметров примитива по порядковому
        // номеру примитива в параметрах отображения объекта ,
        // внутреннему коду (порядковому номеру) объекта (с 1) и виду отображения
        // viewtype: 0 - экранный, 1 - принтерный
        [DllImport(GisLibrary, CharSet = CharSet.Ansi)]
        public static extern IntPtr mapGetRscPrimitiveParameters(int hRsc, int incode, int number,
                                         int viewtype = 0);


        [DllImport(GisLibrary, CharSet = CharSet.Ansi)]
        public static extern int mapGetRscColor(int hRsc, int index, int number = 1);

    }
    public class DbfAPI
    {

        public const string GisLibrary = "C:\\Windows\\SysWOW64\\gisacces.dll";
        //открыть таблицу
        [DllImport(GisLibrary, CharSet = CharSet.Ansi)]
        public static extern int dbOpenTable([MarshalAs(UnmanagedType.LPStr)] string table,
                                                                                    int mode);
        //получить количество полей в записи 
        [DllImport(GisLibrary, CharSet = CharSet.Ansi)]
        public static extern int dbGetFieldCount(int hdbf);
        //получить название и тип поля
        [DllImport(GisLibrary, CharSet = CharSet.Ansi)]
        public static extern int dbGetNameAndTypeByNumber(int hdbf, int number,
                                                          StringBuilder name, int size);

        [DllImport(GisLibrary, CharSet = CharSet.Ansi)]
        public static extern int dbGetValueAsDouble(int hdbf, IntPtr name, ref double value);

        //перейти на первую запись в таблице
        [DllImport(GisLibrary, CharSet = CharSet.Ansi)]
        public static extern int dbGotoFirst(int hdbf);
        //перейти на следующую запись в таблице
        [DllImport(GisLibrary, CharSet = CharSet.Ansi)]
        public static extern int dbGotoNext(int hdbf);
        //получить количество записей в таблице
        [DllImport(GisLibrary, CharSet = CharSet.Ansi)]
        public static extern int dbGetRecordCount(int hdbf);
        //считать данные в виде символьной строки
        [DllImport(GisLibrary, CharSet = CharSet.Ansi)]
        public static extern int dbGetValueAsChar(int hdbf, IntPtr name,
                                          StringBuilder value, int size);


        [DllImport(GisLibrary, CharSet = CharSet.Ansi)]
        public static extern int dbGetValueAsInt(int hdbf, StringBuilder name, ref int value);



        [DllImport(GisLibrary, CharSet = CharSet.Ansi)]
        public static extern int mapCopySite(int hMap, int hSite, IntPtr newname);

        //перенести объект на другую карту
        [DllImport(GisLibrary, CharSet = CharSet.Ansi)]
        public static extern int mapSetObjectMap(int info, int hSite);
        //создать пользовательскую карту по уже открытой карте
        [DllImport(GisLibrary, CharSet = CharSet.Ansi)]
        public static extern int mapCreateAndAppendSiteForMap(int hmap, string mapname,
                                                                       string rscname);


        [DllImport(GisLibrary, CharSet = CharSet.Ansi)]
        public static extern int mapChangeObjectMap(int hObj, int hMap, int hSite);

        [DllImport(GisLibrary, CharSet = CharSet.Ansi)]
        public static extern int mapClearSite(int hMap, int hSite);

        [DllImport(GisLibrary, CharSet = CharSet.Ansi)]
        public static extern int mapGetDocProjection(int hMap, ref MAPREGISTEREX map, ref DATUMPARAM datum, ref ELLIPSOIDPARAM ellparm);

        [DllImport(GisLibrary, CharSet = CharSet.Ansi)]
        public static extern int mapCreateAndAppendSite(int hMap, string mapname, string rscname, ref CREATESITE createsite);



        [DllImport(GisLibrary, CharSet = CharSet.Ansi)]
        public static extern int mapCommit(int info);

        [DllImport(GisLibrary, CharSet = CharSet.Ansi)]
        public static extern int mapCreateCopyObject(int hMap, int hObj);

        [DllImport(GisLibrary, CharSet = CharSet.Ansi)]
        public static extern void mapFreeObject(int hObj);

        [DllImport(GisLibrary, CharSet = CharSet.Ansi)]
        public static extern int mapCommitObjectAsNew(int info);

        [DllImport(GisLibrary, CharSet = CharSet.Ansi)]
        public static extern int mapTotalSeekObjectCount(int hMap);

        [DllImport(GisLibrary, CharSet = CharSet.Ansi)]
        public static extern int mapSetActiveSite(int hMap, int hSite);

        [DllImport(GisLibrary, CharSet = CharSet.Ansi)]
        public static extern int mapSetSiteEditFlag(int hMap, int hSite, int flag);

        [DllImport(GisLibrary, CharSet = CharSet.Ansi)]
        public static extern int mapSetSiteInquiryFlag(int hMap, int hSite, int flag);

        [DllImport(GisLibrary, CharSet = CharSet.Ansi)]
        public static extern int mapCloseSiteForMapByName(int hMap, string name);

        [DllImport(GisLibrary, CharSet = CharSet.Ansi)]
        public static extern int mapCloseSiteForMap(int hMap, int hSite);

        [DllImport(GisLibrary, CharSet = CharSet.Ansi)]
        public static extern int mapSetSiteViewFlag(int hMap, int hSite, int flag);

        [DllImport(GisLibrary, CharSet = CharSet.Ansi)]
        public static extern int mapAppendSemanticLong(int info, int code, int value);

        [DllImport(GisLibrary, CharSet = CharSet.Ansi)]
        public static extern int mapSemanticAmount(int info);

        [DllImport(GisLibrary, CharSet = CharSet.Ansi)]
        private static extern int mapSemanticValue(int info, int number, StringBuilder sb, int bufsize);
        public static string netSemanticValue(int hobj, int number)
        {
            StringBuilder sb = new StringBuilder(256);
            if (mapSemanticValue(hobj, number, sb, sb.Capacity) == 0) return "";
            return sb.ToString();
        }

        [DllImport(GisLibrary, CharSet = CharSet.Ansi)]
        public static extern int mapSetTotalSeekSample(int hMap, string listname, int key);

        [DllImport(GisLibrary, CharSet = CharSet.Ansi)]
        public static extern int dbCloseTable(int hdbf);


    }
    public class GISServAPI
    {



        [DllImport(GisLibrary, CharSet = CharSet.Ansi)]
        public static extern int svGetUserData(ref TASKPARM parm);

        [DllImport(GisLibrary, CharSet = CharSet.Ansi)]
        public static extern int svGetConnectParameters(ref TASKPARM parm);

        //Диалог выбора доступных пользователю данных на ГИС Сервере
        public const string GisLibrary = "C:\\Windows\\SysWOW64\\gisdlgs.dll";
        [DllImport(GisLibrary, CharSet = CharSet.Ansi)]
        public static extern int svOpenData(ref TASKPARM parm);


    }
    public class GEOPort
    {
        public const string GisLibrary = "C:\\Windows\\System32\\gisacces.dll";


        [DllImport(GisLibrary, CharSet = CharSet.Ansi, SetLastError = true)]
        public static extern int mapPortalMenu(int hmap, ref TASKPARMEX parm, int left, int top);

        [DllImport(GisLibrary, CharSet = CharSet.Ansi)]
        public static extern int mapEditWms(ref TASKPARMEX parm, ref string wmsstring, int size, int flag);
        [DllImport(GisLibrary, CharSet = CharSet.Ansi)]
        public static extern int mapOpenData(string name, int mode);

        [DllImport(GisLibrary, CharSet = CharSet.Ansi)]
        public static extern int mapPlaneToGeoWGS84(int hmap, ref double Bx, ref double Ly);

        [DllImport(GisLibrary, CharSet = CharSet.Ansi)]
        public static extern int mapGeoWGS84ToUserPlane(int huser, ref double Bx, ref double Ly);

        [DllImport(GisLibrary, CharSet = CharSet.Ansi)]
        public static extern int mapGeoWGS84ToPlane3D(int hmap, ref double Bx, ref double Ly, ref double H);




    }
    public class ConvertCoordinates
    {

        public const string GisLibrary = "C:\\Windows\\System32\\gisacces.dll";

        [DllImport(GisLibrary, CharSet = CharSet.Ansi)]
        public static extern int mapIsGeoSupported(int hmap);

        [DllImport(GisLibrary, CharSet = CharSet.Ansi)]
        public static extern int mapPlaneToMap(int hmap, ref double x, ref double y);

        [DllImport(GisLibrary, CharSet = CharSet.Ansi)]
        public static extern int mapPlaneToPicture(int hmap, ref double x, ref double y);

        [DllImport(GisLibrary, CharSet = CharSet.Ansi)]
        public static extern int mapPlaneToGeo42(int hmap, ref double Bx, ref double Ly);

        [DllImport(GisLibrary, CharSet = CharSet.Ansi)]
        public static extern int mapPlaneToGeoWGS84(int hmap, ref double Bx, ref double Ly);

        [DllImport(GisLibrary, CharSet = CharSet.Ansi)]
        public static extern int mapPlaneToGeoEP903D(int hmap, ref double Bx, ref double Ly,
                                                                              ref double H);
    }
    public class CreateMTQF
    {


        public const string GisLibrary1 = "C:\\Windows\\SysWOW64\\gisacces.dll";

        //[DllImport("C:\\Windows\\SysWOW64\\gisacces.dll", CharSet = CharSet.Ansi)]
        //public static extern int mapBuildMatrixSurface(int hmap, string mtrname,
        //                                 ref BUILDSURFACE mtrparm);

        public const string GisLibrary = "C:\\Windows\\SysWOW64\\gismtrex.dll";
        [DllImport(GisLibrary, CharSet = CharSet.Ansi)]
        public static extern int mtrBuildMatrixSurface(IntPtr hmap, IntPtr mtrname,
                                                         ref BUILDSURFACE mtrparm);

        [DllImport(GisLibrary1, CharSet = CharSet.Ansi)]
        public static extern int mapCreateMtq(IntPtr name, ref BUILDMTW parm, ref MTRPROJECTIONDATA projectiondata,
                                                 IntPtr palette, int countpalette);

        [DllImport(GisLibrary1, CharSet = CharSet.Ansi)]
        public static extern int mapPutMtqValue(int hMap, int number, double x, double y, double h);

        [DllImport(GisLibrary1, CharSet = CharSet.Ansi)]
        public static extern int mapSetMtqShowRange(int hMap, int number, double minvalue, double maxvalue);

        [DllImport(GisLibrary1, CharSet = CharSet.Ansi)]
        public static extern int mapCloseData(int hMap);

        [DllImport(GisLibrary, CharSet = CharSet.Ansi)]
        public static extern int mtrCalcCharacteristic(int hMap, ref DOUBLEPOINT point, int semanticCode, int flagSelect,
                                         ref double value);


        [DllImport(GisLibrary1, CharSet = CharSet.Ansi)]
        public static extern int mapGetMtqValue(int hMap, int number, double x, double y);

        [DllImport(GisLibrary1, CharSet = CharSet.Ansi)]
        public static extern int mapGetMtqMeasure(int hMap, int number);




        [DllImport(GisLibrary1, CharSet = CharSet.Ansi)]
        public static extern int mapGetMtqHeightInElement(int hMap, int number);

        [DllImport(GisLibrary1, CharSet = CharSet.Ansi)]
        public static extern int mapGetMtqWidthInElement(int hMap, int number);

        [DllImport(GisLibrary1, CharSet = CharSet.Ansi)]
        public static extern int mapGetMtqPoint(int hMap, int number, ref double value, int row, int column);

        [DllImport(GisLibrary1, CharSet = CharSet.Ansi)]
        public static extern int mapSetMtqPalette(int hMap, IntPtr palette, int count, int number);

        [DllImport(GisLibrary1, CharSet = CharSet.Ansi)]
        public static extern int mapSetMtqPaletteDiapason(int hMap, IntPtr diapason, int count, int number);

        [DllImport(GisLibrary1, CharSet = CharSet.Ansi)]
        public static extern int mapSaveMtq(int hMap, int number);

        [DllImport(GisLibrary1, CharSet = CharSet.Ansi)]
        public static extern int mapOpenMtr(string mtrname, int mode);

        [DllImport(GisLibrary1, CharSet = CharSet.Ansi)]
        public static extern int mapCloseMtr(int hMap, int number);

        [DllImport(GisLibrary1, CharSet = CharSet.Ansi)]
        public static extern int mapCloseMtrForMap(int hMap, int number);

        [DllImport(GisLibrary1, CharSet = CharSet.Ansi)]
        public static extern int mapMakeMtqPalette(ref uint[] skeletPalette, ref uint[] resultPalette, int resultColorCount, int smoothColorModification);

        [DllImport(GisLibrary1, CharSet = CharSet.Ansi)]
        public static extern int mapBuildMtw(int hMap, string mtrname, int filtername, ref BUILDMTW mtrparm, int handle);

        [DllImport(GisLibrary, CharSet = CharSet.Unicode)]
        public static extern int mtrBuildVisibleMtq(int hmap, int hsit, string mtqname, int hselect, ref BUILDVISIBLE parm, int hwnd);

        [DllImport(GisLibrary1, CharSet = CharSet.Ansi)]
        public static extern int mapGetMtqCount(int hMap);

        [DllImport(GisLibrary1, CharSet = CharSet.Ansi)]
        public static extern int mapGetMtrCount(int hMap);


        [DllImport(GisLibrary1, CharSet = CharSet.Ansi)]
        public static extern string mapGetMtqName(int hMap, int number);

        [DllImport(GisLibrary1, CharSet = CharSet.Ansi)]
        public static extern string mapGetMtrName(int hMap, int number);

        [DllImport(GisLibrary1, CharSet = CharSet.Ansi)]
        public static extern int mapGetActualMtqFrame(int hMap, ref DFrame frame, int number);

        [DllImport(GisLibrary1, CharSet = CharSet.Ansi)]
        public static extern int mapGetMtqAccuracy(int hMap, int number);

        [DllImport(GisLibrary1, CharSet = CharSet.Ansi)]
        public static extern int mapGetSiteCount(IntPtr hmap);


    }

    public class MatrixH
    {
        public const string GisLibrary = "gisacces.dll";

        [DllImport(GisLibrary, CharSet = CharSet.Ansi)]
        public static extern double mapGetHeightValue(int hmap, double x, double y);
    }

    public class Matrix
    {
        public const double Ae = 8919.4;

        public const string GisLibrary = "gisacces.dll";

        [DllImport(GisLibrary, CharSet = CharSet.Ansi)]
        public static extern double mapGetHeightValue(int hmap, double x, double y);

        [DllImport(GisLibrary, CharSet = CharSet.Ansi)]
        public static extern double mapOpenMtqForMap(int hmap, string mtqname, long mode);

        [DllImport(GisLibrary, CharSet = CharSet.Ansi)]
        public static extern double mapCreateAndAppendTempSite(int hmap, string rscname);

        [DllImport(GisLibrary, CharSet = CharSet.Ansi)]
        public static extern double mapCreateAndAppendSite(int hmap, string sitefilename, string rscname);

        [DllImport(GisLibrary, CharSet = CharSet.Ansi)]
        public static extern double mapGetRscFileName(int hmap, string sitefilename, string rscname);
    }
}
