﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AxaxGisToolKit;
using axGisToolKit;
using System.Runtime.InteropServices;
using System.Windows.Forms;

using System.IO;
using System.Reflection;
using USR_DLL;
using VariableStatic;

using System.ServiceModel;
using Contract;

using System.Diagnostics;
using System.Threading;

using System.Globalization;

namespace GrozaMap
{


    public partial class MapForm : Form
    {

        [DllImport("gisacces.dll")]
        public static extern int mapPlaneToGeoWGS84(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        public static extern int mapPlaneToPicture(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        public static extern int mapPictureToPlane(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        public static extern int mapPlaneToGeo(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        static extern int mapGeoToPlane(int hmap, ref double Bx, ref double Ly);

        private bool MapIsOpenned = false;
        private bool HeightMatrixIsOpenned = false;

        // Переменные VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR 
        // Lena

        Form1 form1;
        Form2 ObjCommPowerAvail;
        FormAirPlane ObjFormAirPlane;
        FormAz objFormAz;
        FormAz1 objFormAz1;
        FormLineSightRange ObjFormLineSightRange;
        FormPeleng objFormPeleng;
        FormS objFormS;
        //FormWay objFormWay;
        public FormWay objFormWay;
        public ZonePowerAvail objZonePowerAvail;
        public FormBearing objFormBearing;
        public FormSost objFormSost;
        public FormSuppression objFormSuppression;

        public static int hmapl1;

        public static bool blAirObject = false;
        //public static System.Windows.Forms.CheckBox chbair1;

        //private double dchislo;
        //private long ichislo;

        // Otl
        //TDataADSBReceiver[] tDadaADSBReceiver = new TDataADSBReceiver[10];
        TDataADSBReceiver[] tDadaADSBReceiver;
        // VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR Переменные

        int iCounter = 0;
        ComponentResourceManager resources = new ComponentResourceManager(typeof(MapForm));
        private int NumberOfLanguage;

        // Конструктор ***********************************************************
        public MapForm()
        {
            InitializeComponent();

            ServiceAPMCreate(); // создать клиент

            form1 = new Form1(ref axaxcMapScreen1);
            ObjCommPowerAvail = new Form2(ref axaxcMapScreen1);
            ObjFormAirPlane = new FormAirPlane(ref axaxcMapScreen1);
            objFormAz = new FormAz(ref axaxcMapScreen1);
            objFormAz1 = new FormAz1(ref axaxcMapScreen1);
            ObjFormLineSightRange = new FormLineSightRange(ref axaxcMapScreen1);
            objFormPeleng = new FormPeleng(ref axaxcMapScreen1);
            objFormS = new FormS(ref axaxcMapScreen1);
            objFormWay = new FormWay(ref axaxcMapScreen1);
            objZonePowerAvail = new ZonePowerAvail(ref axaxcMapScreen1);
            objFormBearing = new FormBearing(ref axaxcMapScreen1);
            objFormSost = new FormSost(ref axaxcMapScreen1);
            objFormSuppression = new FormSuppression(ref axaxcMapScreen1);

            objFormSost.ClickGetGNSS +=objFormSost_ClickGetGNSS;

            objFormSost.ClickGetGNSS2 += objFormSost_ClickGetGNSS2;

            // .................................................................................................
            // WCF
            //создаем сервис
            var service = new ServiceImplementation();
            //подписываемся на событие HelloReceived
            service.AirPlaneReceived += Service_AirPlaneReceived;
            //service.CurrentDirectionUpdated += ServiceCurrentDirectionUpdated;
            service.CurrentCoordsUpdated += service_CurrentCoordsUpdated; // service_CurrentCoordsUpdated;
            service.CoordsIRIUpdated += service_CoordsIRIUpdated;

            //service.CoordsIRI_PPRChUpdated += service_CoordsIRI_PPRChUpdated;
            service.CoordsIRI_PPRChUpdated += service_CoordsIRI_PPRChUpdated;
            service.PelengsIRI_PPRChUpdated += service_PelengsIRI_PPRChUpdated; 

            service.DirectionAntennasUpdated += service_DirectionAntennasUpdated;

            service.CheckGNSSUpdated += new EventHandler<byte>(service_CheckGNSSUpdated);


            //стартуем сервер
            var svh = new ServiceHost(service);
            svh.AddServiceEndpoint(typeof(Contract.IService), new NetTcpBinding(), "net.tcp://localhost:8000");
            svh.Open();
            // .................................................................................................

             // Otl
             tDadaADSBReceiver = new TDataADSBReceiver[10];

            ///Чтение файла
            ///

             //String pathMap = iniRW.get_map_path();

            /////////////////////////////////////////////////////////////////////////////////////////////////
            //30.01.2018 в Init.ini хранятся имя папки и файла матр. высот
             MapCore.openMapFunc(ref MapIsOpenned, ref axaxcMapScreen1, Application.StartupPath + iniRW.get_map_path(), "\\Init.ini");

            SendMapIsOpenToAPM(MapIsOpenned); //обратный контракт
            MapCore.openMatrixFunc(ref HeightMatrixIsOpenned, ref axaxcMapScreen1, Application.StartupPath + iniRW.get_map_mtw());

            ////////////////////////////////////////////////////////////////////////////////////////////////////////
            MouseWheel += MapForm_MouseWheel;
           
            axaxMapSelDlg.cMapView = axaxcMapScreen1.C_CONTAINER;

        }

        // Peleng_IRI_PPRCH *******************************************************
        // INTERRUPT
        // !!! Теперь это только пеленги
        // 23_10_2018

        private object threadLock_2 = new object();
        void service_PelengsIRI_PPRChUpdated(object sender, TCoordsIRI_PPRCh[] coordsIRI)
        {

            // 19_10_2018 ****************************************************************
            int countPel = 0;
            double a1 = 0;
            double b1 = 0;
            double c1 = 0;
            double d1 = 0;
            double f1 = 0;
            double g1 = 0;
            double a2 = 0;
            double b2 = 0;
            double c2 = 0;
            double d2 = 0;
            double f2 = 0;
            double g2 = 0;
            double x184 = 0;
            double y184 = 0;
            double x284 = 0;
            double y284 = 0;

            double[] arr_Pel_XYZ1 = new double[GlobalVarLn.numberofdots * 3];
            double[] arr_Pel_XYZ2 = new double[GlobalVarLn.numberofdots * 3];


            double azgr1 = -1;
            double azgr2 = -1;
            PelIRI objPelIRI = new PelIRI();
            PelIRI objPelIRI2 = new PelIRI();  // For IRI

            int flg = 0;
            int ind = 0;

            int AdDl = 0;
            //Color clr = Color.Red;
            // ------------------------------------------------------------------------

            // ------------------------------------------------------------------------
            lock (threadLock_2)
            {

                // !!! Идут только пеленги (флпаг 3)/только ИРИ(флаг 0/1)/ удалить все (флаг 2)
                AdDl = (int)coordsIRI[0].bDelIRI;

                // Delete All ************************************************************
                // Delete Peleng+IRI

                if (AdDl == 2)
                {

                    // Del Peleng
                    for (int iii20 = (GlobalVarLn.list_PelIRI.Count - 1); iii20 >= 0; iii20--)
                    {
                        GlobalVarLn.list_PelIRI.Remove(GlobalVarLn.list_PelIRI[iii20]);
                    }
                    // DelIRI
                    for (int iii21 = (GlobalVarLn.list_PelIRI2.Count - 1); iii21 >= 0; iii21--)
                    {
                        GlobalVarLn.list_PelIRI2.Remove(GlobalVarLn.list_PelIRI2[iii21]);
                    }

                    axaxcMapScreen1.Repaint();
                    return;

                }  // AdDl=2
                // ************************************************************ Delete All

                // Peleng ****************************************************************
                // Del(Q1=Q2=-1)/Add Peleng
                // !!! Old Peleng delete

                if (
                    (AdDl == 3) &&
                    (chbPeleng.Checked == true)
                    )
                {
                    // ........................................................................
                    // Все старые пеленги удалить

                    for (int iii22 = (GlobalVarLn.list_PelIRI.Count - 1); iii22 >= 0; iii22--)
                    {
                        GlobalVarLn.list_PelIRI.Remove(GlobalVarLn.list_PelIRI[iii22]);
                    }
                    // ........................................................................

                    // Add_Struct_ID ..........................................................
                    // Потом добавляем в лист массив пришедших структур

                    // FOR1_1
                    for (countPel = 0; countPel < coordsIRI.Length; countPel++)
                    {
                        if (
                             (coordsIRI[countPel].iQ1 != -1) ||
                             (coordsIRI[countPel].iQ2 != -1)
                           )
                        {
                            objPelIRI.ID = coordsIRI[countPel].iID;
                            objPelIRI.XGPS_IRI = -1;
                            objPelIRI.YGPS_IRI = -1;
                            objPelIRI.flPelMain2 = 0;
                            objPelIRI.arr_Pel1 = new double[10000];     // R,Широта,долгота
                            objPelIRI.arr_Pel2 = new double[10000];     // R,Широта,долгота
                            objPelIRI.Lat = -1;
                            objPelIRI.Long = -1;
                            objPelIRI.AddDel = (int)coordsIRI[countPel].bDelIRI; // 3 fore Peleng

                            if ((coordsIRI[countPel].iQ1 != -1) && (chbPeleng.Checked == true))
                                objPelIRI.Pel1 = (double)(coordsIRI[countPel].iQ1 / 10d);
                            else
                                objPelIRI.Pel1 = -1;

                            if ((coordsIRI[countPel].iQ2 != -1) && (chbPeleng.Checked == true))
                            {
                                objPelIRI.Pel2 = (double)(coordsIRI[countPel].iQ2 / 10d);
                                objPelIRI.flPelMain2 = 1;
                            }
                            else
                            {
                                objPelIRI.Pel2 = -1;
                                objPelIRI.flPelMain2 = 0;
                            }

                            GlobalVarLn.list_PelIRI.Add(objPelIRI);

                        } //IF(есть пеленг) 

                    } //FOR1_1
                    // .......................................................... Add_Struct_ID

                    // Calculate  .............................................................

                    GlobalVarLn.X1_PelMain = (int)GlobalVarLn.list1_Sost[0].X_m;  // SP1
                    GlobalVarLn.Y1_PelMain = (int)GlobalVarLn.list1_Sost[0].Y_m;
                    GlobalVarLn.X1_1_PelMain = (int)GlobalVarLn.list1_Sost[1].X_m; // SP2
                    GlobalVarLn.Y1_1_PelMain = (int)GlobalVarLn.list1_Sost[1].Y_m;

                    // FOR33 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                    for (countPel = 0; countPel < coordsIRI.Length; countPel++)
                    {

                        if ((coordsIRI[countPel].iQ1 != -1) || (coordsIRI[countPel].iQ2 != -1))
                        {
                            if (coordsIRI[countPel].iQ1 != -1)
                            {
                                azgr1 = (double)(coordsIRI[countPel].iQ1 / 10d); // Peleng1, grad
                            }
                            if (coordsIRI[countPel].iQ2 != -1)
                            {
                                azgr2 = (double)(coordsIRI[countPel].iQ2 / 10d); // Peleng2
                            }

                            x184 = GlobalVarLn.X1_PelMain;
                            y184 = GlobalVarLn.Y1_PelMain;
                            x284 = GlobalVarLn.X1_1_PelMain;
                            y284 = GlobalVarLn.Y1_1_PelMain;

                            mapPlaneToGeo(GlobalVarLn.hmapl, ref x184, ref y184);
                            mapPlaneToGeo(GlobalVarLn.hmapl, ref x284, ref y284);

                            // rad->grad
                            x184 = (x184 * 180) / Math.PI;
                            y184 = (y184 * 180) / Math.PI;
                            x284 = (x284 * 180) / Math.PI;
                            y284 = (y284 * 180) / Math.PI;

                            ClassMap classMap = new ClassMap();
                            ClassMap classMap1 = new ClassMap();

                            // Peleng1
                            if (coordsIRI[countPel].iQ1 != -1)
                            {
                                PelIRI[] mass1 = GlobalVarLn.list_PelIRI.ToArray();
                                classMap.f_Peleng(azgr1, GlobalVarLn.distance, GlobalVarLn.numberofdots, x184, y184,
                                                  ref a1, ref b1, ref c1, ref d1, ref f1, ref g1,
                                                  ref mass1[countPel].arr_Pel1, ref arr_Pel_XYZ1);
                                GlobalVarLn.list_PelIRI = mass1.ToList();
                            } // Peleng1

                            // Peleng2
                            if (GlobalVarLn.list_PelIRI[countPel].flPelMain2 == 1)
                            {
                                PelIRI[] mass2 = GlobalVarLn.list_PelIRI.ToArray();
                                classMap1.f_Peleng(azgr2, GlobalVarLn.distance, GlobalVarLn.numberofdots, x284, y284,
                                                   ref a2, ref b2, ref c2, ref d2, ref f2, ref g2,
                                                   ref mass2[countPel].arr_Pel2, ref arr_Pel_XYZ2);
                                GlobalVarLn.list_PelIRI = mass2.ToList();

                            } // Peleng2


                        } // if ((coordsIRI[countPel].iQ1 != -1) || (coordsIRI[countPel].iQ2 != -1))

                    } // FOR33
                    //  ............................................................. Calculate

                    axaxcMapScreen1.Repaint();
                    return;

                }  // AdDl=3chbPeleng.Checked == true
                // **************************************************************** Peleng

            } // threadLock_2
            // **************************************************************** 19_10_2018

        } // INTERRUPT
        // ******************************************************* Peleng_IRI_PPRCH

        // IRI_PPRCH ***********************************************************************
        // INTERRUPT
        // !!! Теперь это только IRI PPRCH
        // Variant2

        // 22_10_2018
        private object threadLock_5 = new object();
        //void service_CoordsIRI_PPRChUpdated(object sender, TCoordsIRI_PPRCh[] coordsIRI)
        void service_CoordsIRI_PPRChUpdated(object sender, List<TCoordsIRI_PPRCh> listCoordsIRI_PPRCh)
        {
            // 2504
            double dds = 250; // m

            int AdDl = 0;
            int countPel = 0;
            int countPel1 = 0;

            lock (threadLock_5)
            {
                // !!! Идут только пеленги (флпаг 3)/только ИРИ(флаг 0/1)/ удалить все (флаг 2)
                AdDl = (int)listCoordsIRI_PPRCh[0].bDelIRI;

                PelIRI objPelIRI2 = new PelIRI();  // For IRI

                // Delete All ************************************************************
                // Delete Peleng+IRI

                if (AdDl == 2)
                {

                    // Del Peleng
                    for (int iii20 = (GlobalVarLn.list_PelIRI.Count - 1); iii20 >= 0; iii20--)
                    {
                        GlobalVarLn.list_PelIRI.Remove(GlobalVarLn.list_PelIRI[iii20]);
                    }
                    // DelIRI
                    for (int iii21 = (GlobalVarLn.list_PelIRI2.Count - 1); iii21 >= 0; iii21--)
                    {
                        GlobalVarLn.list_PelIRI2.Remove(GlobalVarLn.list_PelIRI2[iii21]);
                    }

                    axaxcMapScreen1.Repaint();
                    return;

                }  // AdDl=2
                // ************************************************************ Delete All

                // Delete ID *************************************************************
                // Удалить конкретный ID

                if (AdDl == 1)
                {
                    // Del_Struct_ID ..........................................................
                    // удаляем все структуры с таким IDi 
                    // Придет в 

                    // FOR111
                    for (countPel = 0; countPel < listCoordsIRI_PPRCh.Count; countPel++)
                    {
                        int ffg1 = (int)listCoordsIRI_PPRCh[countPel].bDelIRI;

                        if (ffg1 == 1) // Del
                        {
                            // For22
                            for (int iii33 = (GlobalVarLn.list_PelIRI2.Count - 1); iii33 >= 0; iii33--)
                            {
                                if (GlobalVarLn.list_PelIRI2[iii33].ID == listCoordsIRI_PPRCh[countPel].iID)
                                    GlobalVarLn.list_PelIRI2.Remove(GlobalVarLn.list_PelIRI2[iii33]);

                            } //for22

                        } // IF

                    } // FOR111

                    axaxcMapScreen1.Repaint();
                    return;

                } // AdDl==1 Del
                // ************************************************************* Delete ID

                // Add IRI ****************************************************************

                if (AdDl == 0)
                {

/*

                    // Del_Struct .............................................................
                    // Сначала удаляем все структуры 

                    // DelIRI
                    for (int iii99 = (GlobalVarLn.list_PelIRI2.Count - 1); iii99 >= 0; iii99--)
                    {
                        GlobalVarLn.list_PelIRI2.Remove(GlobalVarLn.list_PelIRI2[iii99]);
                    }
                    // ............................................................. Del_Struct

                    // Add_Struct_ID ..........................................................
                    // Потом добавляем в лист массив пришедших структур

                    // FOR1_5
                    for (countPel = 0; countPel < listCoordsIRI_PPRCh.Count; countPel++)
                    {
                        int ffg = (int)listCoordsIRI_PPRCh[countPel].bDelIRI;

                        if (
                             (listCoordsIRI_PPRCh[countPel].dLatitude != -1) &&
                             (listCoordsIRI_PPRCh[countPel].dLongitude != -1) &&
                             (ffg == 0)
                           )
                        {
                            objPelIRI2.ID = listCoordsIRI_PPRCh[countPel].iID;
                            objPelIRI2.XGPS_IRI = -1;
                            objPelIRI2.YGPS_IRI = -1;
                            objPelIRI2.flPelMain2 = 0;
                            objPelIRI2.arr_Pel1 = new double[10000];     // R,Широта,долгота
                            objPelIRI2.arr_Pel2 = new double[10000];     // R,Широта,долгота
                            objPelIRI2.Pel1 = -1;
                            objPelIRI2.Pel2 = -1;

                            if ((listCoordsIRI_PPRCh[countPel].dLatitude != -1) && (listCoordsIRI_PPRCh[countPel].dLongitude != -1))
                            {
                                objPelIRI2.Lat = listCoordsIRI_PPRCh[countPel].dLatitude;
                                objPelIRI2.Long = listCoordsIRI_PPRCh[countPel].dLongitude;
                                objPelIRI2.color = ColorFromSharpString(listCoordsIRI_PPRCh[countPel].sColorIRI);
                                objPelIRI2.AddDel = (int)listCoordsIRI_PPRCh[countPel].bDelIRI; // 0 - add, 1 - delete;
                            }
                            else
                            {
                                objPelIRI2.Lat = -1;
                                objPelIRI2.Long = -1;
                                objPelIRI2.XGPS_IRI = -1;
                                objPelIRI2.YGPS_IRI = -1;
                                objPelIRI2.AddDel = (int)listCoordsIRI_PPRCh[countPel].bDelIRI; // 0 - add, 1 - delete;

                            }

                            GlobalVarLn.list_PelIRI2.Add(objPelIRI2);

                            ClassMap.f_GetGPS_IRI_2(GlobalVarLn.list_PelIRI2.Count - 1);

                        } //IF(есть координаты) 

                    } //FOR1_5
                    // .......................................................... Add_Struct_ID

                    axaxcMapScreen1.Repaint();
*/

                // ------------------------------------------------------------------------
                // 2504

                // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                bool SignRepaint = false;
                int i1 = 0;
                int i2 = 0;
                int i3 = 0;
                int i4 = 0;
                bool flexist = false;
                int ffg = 0;
                // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                // Убрать в старом те, которых нет в новых

                for (i1 = (GlobalVarLn.list_PelIRI2.Count - 1); i1 >= 0; i1--) // old
                {
                    flexist = false;
                    for (i2 = 0; i2 < listCoordsIRI_PPRCh.Count; i2++) // new
                    {
                        ffg = (int)listCoordsIRI_PPRCh[i2].bDelIRI;

                        if (
                            (GlobalVarLn.list_PelIRI2[i1].ID == listCoordsIRI_PPRCh[i2].iID) &&
                            (listCoordsIRI_PPRCh[i2].dLatitude != -1) &&
                            (listCoordsIRI_PPRCh[i2].dLongitude != -1) &&
                            (ffg == 0)
                           )
                        {
                            flexist = true;
                        }

                    } // i2

                    if (flexist == false)
                    {
                        GlobalVarLn.list_PelIRI2.Remove(GlobalVarLn.list_PelIRI2[i1]);
                        SignRepaint = true;
                        //i1 = -1;
                    }

                } // i1 old

                // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                // 

                    // FOR1_5 вперед по новому
                    for (countPel = 0; countPel < listCoordsIRI_PPRCh.Count; countPel++) // new
                    {

                        ffg = (int)listCoordsIRI_PPRCh[countPel].bDelIRI;

                        if (
                             (listCoordsIRI_PPRCh[countPel].dLatitude != -1) &&
                             (listCoordsIRI_PPRCh[countPel].dLongitude != -1) &&
                             (ffg == 0)
                           )
                        {

                            flexist = false;
                            for (int iii99 = (GlobalVarLn.list_PelIRI2.Count - 1); iii99 >= 0; iii99--)
                            {

                                if (GlobalVarLn.list_PelIRI2[iii99].ID == listCoordsIRI_PPRCh[countPel].iID)
                                {
                                    // есть с таким ID
                                    flexist = true;
                                    double sss=0;
                                    sss = ClassMap.f_D_2Points(GlobalVarLn.list_PelIRI2[iii99].Lat, GlobalVarLn.list_PelIRI2[iii99].Long, listCoordsIRI_PPRCh[countPel].dLatitude, listCoordsIRI_PPRCh[countPel].dLongitude,1);
                                    if (sss > dds)
                                    {
                                        SignRepaint = true;
                                        // Убираем старый
                                        GlobalVarLn.list_PelIRI2.Remove(GlobalVarLn.list_PelIRI2[iii99]);

                                        // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                                        // добавляем новый

                                        objPelIRI2.ID = listCoordsIRI_PPRCh[countPel].iID;
                                        objPelIRI2.XGPS_IRI = -1;
                                        objPelIRI2.YGPS_IRI = -1;
                                        objPelIRI2.flPelMain2 = 0;
                                        objPelIRI2.arr_Pel1 = new double[10000];     // R,Широта,долгота
                                        objPelIRI2.arr_Pel2 = new double[10000];     // R,Широта,долгота
                                        objPelIRI2.Pel1 = -1;
                                        objPelIRI2.Pel2 = -1;

                                        if ((listCoordsIRI_PPRCh[countPel].dLatitude != -1) && (listCoordsIRI_PPRCh[countPel].dLongitude != -1))
                                        {
                                            objPelIRI2.Lat = listCoordsIRI_PPRCh[countPel].dLatitude;
                                            objPelIRI2.Long = listCoordsIRI_PPRCh[countPel].dLongitude;
                                            objPelIRI2.color = ColorFromSharpString(listCoordsIRI_PPRCh[countPel].sColorIRI);
                                            objPelIRI2.AddDel = (int)listCoordsIRI_PPRCh[countPel].bDelIRI; // 0 - add, 1 - delete;
                                        }
                                        else
                                        {
                                            objPelIRI2.Lat = -1;
                                            objPelIRI2.Long = -1;
                                            objPelIRI2.XGPS_IRI = -1;
                                            objPelIRI2.YGPS_IRI = -1;
                                            objPelIRI2.AddDel = (int)listCoordsIRI_PPRCh[countPel].bDelIRI; // 0 - add, 1 - delete;

                                        }

                                        GlobalVarLn.list_PelIRI2.Add(objPelIRI2);

                                        ClassMap.f_GetGPS_IRI_2(GlobalVarLn.list_PelIRI2.Count - 1);

                                        // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                                      
                                    } // S>ds

                                }  // ID==

                            }  // for old назад

                            if (flexist == false) // это новый ID -> добавляем его
                            {
                                SignRepaint = true;

                                // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                                // добавляем новый

                                objPelIRI2.ID = listCoordsIRI_PPRCh[countPel].iID;
                                objPelIRI2.XGPS_IRI = -1;
                                objPelIRI2.YGPS_IRI = -1;
                                objPelIRI2.flPelMain2 = 0;
                                objPelIRI2.arr_Pel1 = new double[10000];     // R,Широта,долгота
                                objPelIRI2.arr_Pel2 = new double[10000];     // R,Широта,долгота
                                objPelIRI2.Pel1 = -1;
                                objPelIRI2.Pel2 = -1;

                                if ((listCoordsIRI_PPRCh[countPel].dLatitude != -1) && (listCoordsIRI_PPRCh[countPel].dLongitude != -1))
                                {
                                    objPelIRI2.Lat = listCoordsIRI_PPRCh[countPel].dLatitude;
                                    objPelIRI2.Long = listCoordsIRI_PPRCh[countPel].dLongitude;
                                    objPelIRI2.color = ColorFromSharpString(listCoordsIRI_PPRCh[countPel].sColorIRI);
                                    objPelIRI2.AddDel = (int)listCoordsIRI_PPRCh[countPel].bDelIRI; // 0 - add, 1 - delete;
                                }
                                else
                                {
                                    objPelIRI2.Lat = -1;
                                    objPelIRI2.Long = -1;
                                    objPelIRI2.XGPS_IRI = -1;
                                    objPelIRI2.YGPS_IRI = -1;
                                    objPelIRI2.AddDel = (int)listCoordsIRI_PPRCh[countPel].bDelIRI; // 0 - add, 1 - delete;

                                }

                                GlobalVarLn.list_PelIRI2.Add(objPelIRI2);

                                ClassMap.f_GetGPS_IRI_2(GlobalVarLn.list_PelIRI2.Count - 1);

                                // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

                            } // // это новый ID

                        } //IF(есть координаты) 

                    } //FOR1_5 new
                    // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

                    if (SignRepaint == true)
                        axaxcMapScreen1.Repaint();


                } // AdDl==0
                // **************************************************************** Add IRI

            } // threadLock_5

        } // INTERRUPT
        // *********************************************************************** IRI_PPRCH
   
        // SECTOR ****************************************************************
        //Sect ПРИНЯТЬ

        /// <summary>
        /// Направление антенн
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void service_DirectionAntennasUpdated(object sender, TDirectionAntennas e)
        {

            // -------------------------------------------------------------------
            // Направление секторов

            GlobalVarLn.luch1 = (double)e.LPA13; // grad
            GlobalVarLn.luch2 = (double)e.LPA24;
            GlobalVarLn.luch3 = (double)e.ARD1;
            GlobalVarLn.luch4 = (double)e.ARD2;
            GlobalVarLn.luch5 = (double)e.ARD3;
            // -------------------------------------------------------------------
            if (GlobalVarLn.luch1 != -1)
                GlobalVarLn.fllSect1=1;
            else
                GlobalVarLn.fllSect1 = 0;

            if(GlobalVarLn.luch2!=-1)
                GlobalVarLn.fllSect2=1;
            else
                GlobalVarLn.fllSect2 = 0;

            if(GlobalVarLn.luch3!=-1)
                GlobalVarLn.fllSect3=1;
            else
                GlobalVarLn.fllSect3 = 0;

            if(GlobalVarLn.luch4!=-1)
                GlobalVarLn.fllSect4=1;
            else
                GlobalVarLn.fllSect4 = 0;

            if(GlobalVarLn.luch5!=-1)
                GlobalVarLn.fllSect5=1;
            else
                GlobalVarLn.fllSect5 = 0;
            // -------------------------------------------------------------------

            // IF***
            if(
                ((GlobalVarLn.fllSect1==1)||
                 (GlobalVarLn.fllSect2==1)||
                 (GlobalVarLn.fllSect3==1)||
                 (GlobalVarLn.fllSect4==1)||
                 (GlobalVarLn.fllSect5==1)) &&
                (chbSect.Checked==true)
               )
            {
              // .................................................................
                if (GlobalVarLn.f_luch == 0)
                {
                    GlobalVarLn.f_luch = 1;
                    GlobalVarLn.luch1_dubl = GlobalVarLn.luch1;
                    GlobalVarLn.luch2_dubl = GlobalVarLn.luch2;
                    GlobalVarLn.luch3_dubl = GlobalVarLn.luch3;
                    GlobalVarLn.luch4_dubl = GlobalVarLn.luch4;
                    GlobalVarLn.luch5_dubl = GlobalVarLn.luch5;

                    GlobalVarLn.flsect = 0;
                    // Убрать с карты
                    GlobalVarLn.axMapScreenGlobal.Repaint();

                    GlobalVarLn.flsect = 1;

                    ClassMap.Sector(
                                    (double)GlobalVarLn.XCenter_Sost,
                                    (double)GlobalVarLn.YCenter_Sost
                                  );
                } // f_luch==0
                // .................................................................
                // f_luch=1

                else
                {
                    // Те же значения
                    if (
                        (GlobalVarLn.luch1 == GlobalVarLn.luch1_dubl) &&
                        (GlobalVarLn.luch2 == GlobalVarLn.luch2_dubl) &&
                        (GlobalVarLn.luch3 == GlobalVarLn.luch3_dubl) &&
                        (GlobalVarLn.luch4 == GlobalVarLn.luch4_dubl) &&
                        (GlobalVarLn.luch5 == GlobalVarLn.luch5_dubl)
                      )
                    {
                        return;
                    } // Те же значения

                    // Новые
                    else
                    {
                        GlobalVarLn.luch1_dubl = GlobalVarLn.luch1;
                        GlobalVarLn.luch2_dubl = GlobalVarLn.luch2;
                        GlobalVarLn.luch3_dubl = GlobalVarLn.luch3;
                        GlobalVarLn.luch4_dubl = GlobalVarLn.luch4;
                        GlobalVarLn.luch5_dubl = GlobalVarLn.luch5;

                        GlobalVarLn.flsect = 0;
                        // Убрать с карты
                        GlobalVarLn.axMapScreenGlobal.Repaint();

                        GlobalVarLn.flsect = 1;

                        ClassMap.Sector(
                                        (double)GlobalVarLn.XCenter_Sost,
                                        (double)GlobalVarLn.YCenter_Sost
                                      );

                    } // Новые

                } // f_luch=1
                // .................................................................

            } // IF***
            // -------------------------------------------------------------------

        } // P/P

        void service_CheckGNSSUpdated(object sender, byte e)
        {
            GlobalVarLn.bCheckGNSS = Convert.ToBoolean(e);
        }

        // chbSect
        private void chbSect_CheckedChanged(object sender, EventArgs e)
        {
          // ----------------------------------------------------------
            if (chbSect.Checked == true)
            {
                // ....................................................

                if (
                    (GlobalVarLn.fllSect1 == 1) ||
                     (GlobalVarLn.fllSect2 == 1) ||
                     (GlobalVarLn.fllSect3 == 1) ||
                     (GlobalVarLn.fllSect4 == 1) ||
                     (GlobalVarLn.fllSect5 == 1) 
                   )
                {
                    GlobalVarLn.flsect = 1;

                    ClassMap.Sector(
                                   (double)GlobalVarLn.XCenter_Sost,
                                   (double)GlobalVarLn.YCenter_Sost
                                  );
                }

            } // chbSect.Checked == true
            // ----------------------------------------------------------
            else
            {
                ClassMap.Sector_Clear();


            } // chbSect.Checked == false
          // ----------------------------------------------------------

        }

        // **************************************************************** SECTOR




        // PELENG_OTLADKA *********************************************************
        private void button9_Click(object sender, EventArgs e)
        {

            double azgr1 = 17;
            double azgr2 = 249;

            GlobalVarLn.X1_PelMain = (int)GlobalVarLn.list1_Sost[0].X_m;
            GlobalVarLn.Y1_PelMain = (int)GlobalVarLn.list1_Sost[0].Y_m;
            GlobalVarLn.X1_1_PelMain = (int)GlobalVarLn.list1_Sost[1].X_m;
            GlobalVarLn.Y1_1_PelMain = (int)GlobalVarLn.list1_Sost[1].Y_m;

            // ................................................................
            double a1 = 0;
            double b1 = 0;
            double c1 = 0;
            double d1 = 0;
            double f1 = 0;
            double g1 = 0;
            double a2 = 0;
            double b2 = 0;
            double c2 = 0;
            double d2 = 0;
            double f2 = 0;
            double g2 = 0;
            double x184 = 0;
            double y184 = 0;
            double x284 = 0;
            double y284 = 0;

            double[] arr_Pel_XYZ1 = new double[GlobalVarLn.numberofdots * 3];
            double[] arr_Pel_XYZ2 = new double[GlobalVarLn.numberofdots * 3];
            ClassMap classMap = new ClassMap();
            ClassMap classMap1 = new ClassMap();

            x184 = GlobalVarLn.X1_PelMain;
            y184 = GlobalVarLn.Y1_PelMain;
            x284 = GlobalVarLn.X1_1_PelMain;
            y284 = GlobalVarLn.Y1_1_PelMain;

            //mapPlaneToGeoWGS84(GlobalVarLn.hmapl, ref x184, ref y184);
            //mapPlaneToGeoWGS84(GlobalVarLn.hmapl, ref x284, ref y284);
            mapPlaneToGeo(GlobalVarLn.hmapl, ref x184, ref y184);
            mapPlaneToGeo(GlobalVarLn.hmapl, ref x284, ref y284);

            // rad->grad
            x184 = (x184 * 180) / Math.PI;
            y184 = (y184 * 180) / Math.PI;
            x284 = (x284 * 180) / Math.PI;
            y284 = (y284 * 180) / Math.PI;


            classMap.f_Peleng(azgr1, GlobalVarLn.distance, GlobalVarLn.numberofdots, x184, y184,
                              ref a1, ref b1, ref c1, ref d1, ref f1, ref g1,
                              ref GlobalVarLn.arr_Pel1, ref arr_Pel_XYZ1);
            classMap1.f_Peleng(azgr2, GlobalVarLn.distance, GlobalVarLn.numberofdots, x284, y284,
                               ref a2, ref b2, ref c2, ref d2, ref f2, ref g2,
                               ref GlobalVarLn.arr_Pel2, ref arr_Pel_XYZ2);

            /*
                            int ind2 = 0;
                            for (int i2 = 0; i2 < GlobalVarLn.numberofdots; i2++)
                            {
                                // grad->rad
                                GlobalVarLn.arr_Pel1[ind2 + 1] = (GlobalVarLn.arr_Pel1[ind2 + 1] * Math.PI) / 180;
                                GlobalVarLn.arr_Pel1[ind2 + 2] = (GlobalVarLn.arr_Pel1[ind2 + 2] * Math.PI) / 180;
                                GlobalVarLn.arr_Pel2[ind2 + 1] = (GlobalVarLn.arr_Pel2[ind2 + 1] * Math.PI) / 180;
                                GlobalVarLn.arr_Pel2[ind2 + 2] = (GlobalVarLn.arr_Pel2[ind2 + 2] * Math.PI) / 180;
                                ind2 += 3;
                            }

                            int ind = 0;
                            for (int i = 0; i < GlobalVarLn.numberofdots; i++)
                            {
                                mapGeoToPlane(GlobalVarLn.hmapl, ref GlobalVarLn.arr_Pel1[ind + 1], ref GlobalVarLn.arr_Pel1[ind + 2]);
                                mapPlaneToPicture(GlobalVarLn.hmapl, ref GlobalVarLn.arr_Pel1[ind + 1], ref GlobalVarLn.arr_Pel1[ind + 2]);
                                mapGeoToPlane(GlobalVarLn.hmapl, ref GlobalVarLn.arr_Pel2[ind + 1], ref GlobalVarLn.arr_Pel2[ind + 2]);
                                mapPlaneToPicture(GlobalVarLn.hmapl, ref GlobalVarLn.arr_Pel2[ind + 1], ref GlobalVarLn.arr_Pel2[ind + 2]);
                                ind += 3;
                            }
            */
            // gggggggggggggggggggggggggggggggggggggggggggggggggggggg

            /*
                            Graphics graph = axaxcMapScreen1.CreateGraphics();
                            Brush brushRed2 = new SolidBrush(Color.Red);

                            int ind1 = 0;
                            for (int i1 = 0; i1 < GlobalVarLn.numberofdots; i1++)
                            {
                                graph.FillEllipse(
                                                  brushRed2,
                                                  (int)GlobalVarLn.arr_Pel1[ind1 + 1] - GlobalVarLn.axMapScreenGlobal.MapLeft,
                                                  (int)GlobalVarLn.arr_Pel1[ind1 + 2] - GlobalVarLn.axMapScreenGlobal.MapTop,
                                                  5,
                                                  5
                                                  );
                                graph.FillEllipse(
                                                  brushRed2,
                                                  (int)GlobalVarLn.arr_Pel2[ind1 + 1] - GlobalVarLn.axMapScreenGlobal.MapLeft,
                                                  (int)GlobalVarLn.arr_Pel2[ind1 + 2] - GlobalVarLn.axMapScreenGlobal.MapTop,
                                                  5,
                                                  5
                                                  );
                                ind1 += 3;

                            }
             */

            /*
                            Point pnt1_1=new Point();
                            Point pnt2_1=new Point();
                            Point pnt1_2=new Point();
                            Point pnt2_2=new Point();
                            Graphics graph = GlobalVarLn.axMapScreenGlobal.CreateGraphics();
                            Pen pen1 = new Pen(Color.Red, 2.0f);
                            int ind3 = 0;
                            for (int i3 = 0; i3 < GlobalVarLn.numberofdots; i3++)
                            {
                                if (i3 == 0)
                                {
                                    pnt1_1.X = (int)GlobalVarLn.arr_Pel1[ind3 + 1] - GlobalVarLn.axMapScreenGlobal.MapLeft;
                                    pnt1_1.Y = (int)GlobalVarLn.arr_Pel1[ind3 + 2] - GlobalVarLn.axMapScreenGlobal.MapTop;
                                    pnt1_2.X = (int)GlobalVarLn.arr_Pel2[ind3 + 1] - GlobalVarLn.axMapScreenGlobal.MapLeft;
                                    pnt1_2.Y = (int)GlobalVarLn.arr_Pel2[ind3 + 2] - GlobalVarLn.axMapScreenGlobal.MapTop;

                                }
                                else
                                {
                                    pnt2_1.X = (int)GlobalVarLn.arr_Pel1[ind3 + 1] - GlobalVarLn.axMapScreenGlobal.MapLeft;
                                    pnt2_1.Y = (int)GlobalVarLn.arr_Pel1[ind3 + 2] - GlobalVarLn.axMapScreenGlobal.MapTop;
                                    pnt2_2.X = (int)GlobalVarLn.arr_Pel2[ind3 + 1] - GlobalVarLn.axMapScreenGlobal.MapLeft;
                                    pnt2_2.Y = (int)GlobalVarLn.arr_Pel2[ind3 + 2] - GlobalVarLn.axMapScreenGlobal.MapTop;
                                    graph.DrawLine(pen1, pnt1_1, pnt2_1);
                                    graph.DrawLine(pen1, pnt1_2, pnt2_2);
                                    pnt1_1.X = pnt2_1.X;
                                    pnt1_1.Y = pnt2_1.Y;
                                    pnt1_2.X = pnt2_2.X;
                                    pnt1_2.Y = pnt2_2.Y;

                                }

                                ind3 += 3;

                            } // FOR
            */
            GlobalVarLn.flPelMain2 = 1;
            axaxcMapScreen1.Repaint();
            ClassMap.f_ReDrawPeleng();


            /*
                            Graphics graph = GlobalVarLn.axMapScreenGlobal.CreateGraphics();
                            Pen pen1 = new Pen(Color.Red, 2.0f);
                            if (graph != null)
                            {
                                graph.DrawPolygon(pen1, lst1);
                                graph.DrawPolygon(pen1, lst2);
                            }
            */

            // -------------------------------------------------------------------------------------

            // ...............................................................................................

            // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            double dLat1 = 0;
            double dLong1 = 0;
            // Эллипсоид Красовского, град
            double LatKrG1 = 0;
            double LongKrG1 = 0;
            // Гаусс-крюгер(СК42) м
            double XSP421 = 0;
            double YSP421 = 0;

            double dLat2 = 0;
            double dLong2 = 0;
            // Эллипсоид Красовского, град
            double LatKrG2 = 0;
            double LongKrG2 = 0;
            // Гаусс-крюгер(СК42) м
            double XSP422 = 0;
            double YSP422 = 0;

            double X_IRI = 0;
            double Y_IRI = 0;

            ClassMap objClassMap1_ed = new ClassMap();
            ClassMap objClassMap2_ed = new ClassMap();
            ClassMap objClassMap3_ed = new ClassMap();
            ClassMap objClassMap4_ed = new ClassMap();
            ClassMap objClassMap5_ed = new ClassMap();
            ClassMap objClassMap6_ed = new ClassMap();
            ClassMap objClassMap7_ed = new ClassMap();
            ClassMap objClassMap8_ed = new ClassMap();
            ClassMap objClassMap9_ed = new ClassMap();
            ClassMap objClassMap10_ed = new ClassMap();

            // WGS84,grad
            double xtmp1_ed = 53.881244;
            double ytmp1_ed = 27.606453;
            double xtmp2_ed = 53.9216687;
            double ytmp2_ed = 27.6799367;

            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            // WGS84(эллипсоид)->элл.Красовского
            objClassMap1_ed.f_dLong
                (
                // Входные параметры (град,км)
                    xtmp1_ed,   // широта
                    ytmp1_ed,  // долгота
                    0,     // высота

                    // DATUM,m
                    GlobalVarLn.dXdat_comm,
                    GlobalVarLn.dYdat_comm,
                    GlobalVarLn.dZdat_comm,

                    ref dLong1   // приращение по долготе, угл.сек
                );
            objClassMap1_ed.f_dLat
                (
                // Входные параметры (град,км)
                    xtmp1_ed,   // широта
                    ytmp1_ed,  // долгота
                    0,     // высота

                    // DATUM,m
                    GlobalVarLn.dXdat_comm,
                    GlobalVarLn.dYdat_comm,
                    GlobalVarLn.dZdat_comm,

                    ref dLat1        // приращение по долготе, угл.сек
                );
            objClassMap1_ed.f_WGS84_SK42_Lat_Long
                   (
                // Входные параметры (град,км)
                       xtmp1_ed,   // широта
                       ytmp1_ed,  // долгота
                       0,     // высота
                       dLat1,       // приращение по долготе, угл.сек
                       dLong1,      // приращение по долготе, угл.сек

                       // Выходные параметры (grad)
                       ref LatKrG1,   // широта
                       ref LongKrG1   // долгота
                   );
            // SK42(элл.)->Крюгер 
            objClassMap3_ed.f_SK42_Krug
                   (
                // Входные параметры (!!! grad)
                // !!! эллипсоид Красовского
                       LatKrG1,   // широта
                       LongKrG1,  // долгота

                       // Выходные параметры (km)
                       ref XSP421,
                       ref YSP421
                   );

            // km->m
            //XSP421 = XSP421 * 1000;
            //YSP421 = YSP421 * 1000;
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            // WGS84(эллипсоид)->элл.Красовского
            objClassMap4_ed.f_dLong
                (
                // Входные параметры (град,км)
                    xtmp2_ed,   // широта
                    ytmp2_ed,  // долгота
                    0,     // высота

                    // DATUM,m
                    GlobalVarLn.dXdat_comm,
                    GlobalVarLn.dYdat_comm,
                    GlobalVarLn.dZdat_comm,

                    ref dLong2   // приращение по долготе, угл.сек
                );
            objClassMap4_ed.f_dLat
                (
                // Входные параметры (град,км)
                    xtmp2_ed,   // широта
                    ytmp2_ed,  // долгота
                    0,     // высота

                    // DATUM,m
                    GlobalVarLn.dXdat_comm,
                    GlobalVarLn.dYdat_comm,
                    GlobalVarLn.dZdat_comm,

                    ref dLat2        // приращение по долготе, угл.сек
                );
            objClassMap4_ed.f_WGS84_SK42_Lat_Long
                   (
                // Входные параметры (град,км)
                       xtmp2_ed,   // широта
                       ytmp2_ed,  // долгота
                       0,     // высота
                       dLat2,       // приращение по долготе, угл.сек
                       dLong2,      // приращение по долготе, угл.сек

                       // Выходные параметры (grad)
                       ref LatKrG2,   // широта
                       ref LongKrG2   // долгота
                   );
            // SK42(элл.)->Крюгер 
            objClassMap6_ed.f_SK42_Krug
                   (
                // Входные параметры (!!! grad)
                // !!! эллипсоид Красовского
                       LatKrG2,   // широта
                       LongKrG2,  // долгота

                       // Выходные параметры (km)
                       ref XSP422,
                       ref YSP422
                   );

            // km->m
            //XSP422 = XSP422 * 1000;
            //YSP422 = YSP422 * 1000;

            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            objClassMap7_ed.f_Triang2(
                // km
                           XSP421,
                           YSP421,
                           XSP422,
                           YSP422,
                // Пеленг(град)
                           azgr1,
                           azgr2,
                           30,
                // Координаты искомого ИРИ (км) CK42
                           ref X_IRI,
                           ref Y_IRI
                                    );
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            // Эллипсоид Красовского (grad)
            objClassMap8_ed.f_Krug_SK42
            (
                // Входные параметры (км)
             X_IRI,
             Y_IRI,

             // Выходные параметры (grad)
             ref LatKrG1,   // широта
             ref LongKrG1   // долгота
             );
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            // элл.Красовского->WGS84(эллипсоид)
            objClassMap9_ed.f_dLong
                (
                // Входные параметры (град,км)
                    LatKrG1,   // широта
                    LongKrG1,  // долгота
                    0,     // высота

                    // DATUM,m
                    GlobalVarLn.dXdat_comm,
                    GlobalVarLn.dYdat_comm,
                    GlobalVarLn.dZdat_comm,

                    ref dLong1   // приращение по долготе, угл.сек
                );
            objClassMap9_ed.f_dLat
                (
                // Входные параметры (град,км)
                    LatKrG1,   // широта
                    LongKrG1,  // долгота
                    0,     // высота

                    // DATUM,m
                    GlobalVarLn.dXdat_comm,
                    GlobalVarLn.dYdat_comm,
                    GlobalVarLn.dZdat_comm,

                    ref dLat1        // приращение по долготе, угл.сек
                );
            objClassMap9_ed.f_SK42_WGS84_Lat_Long
                   (
                // Входные параметры (град,км)
                       LatKrG1,   // широта
                       LongKrG1,  // долгота
                       0,     // высота
                       dLat1,       // приращение по долготе, угл.сек
                       dLong1,      // приращение по долготе, угл.сек

                       // Выходные параметры (grad) WGS84
                       ref xtmp1_ed,   // широта
                       ref ytmp1_ed   // долгота
                   );

            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

            // WGS84(эллипсоид)->элл.Красовского 
            objClassMap10_ed.f_dLong
                (
                // Входные параметры (град,км)
                    xtmp1_ed,   // широта
                    ytmp1_ed,  // долгота
                    0,     // высота

                    // DATUM,m
                    GlobalVarLn.dXdat_comm,
                    GlobalVarLn.dYdat_comm,
                    GlobalVarLn.dZdat_comm,

                    ref dLong1   // приращение по долготе, угл.сек
                );
            objClassMap10_ed.f_dLat
                (
                // Входные параметры (град,км)
                    xtmp1_ed,   // широта
                    ytmp1_ed,  // долгота
                    0,     // высота

                    // DATUM,m
                    GlobalVarLn.dXdat_comm,
                    GlobalVarLn.dYdat_comm,
                    GlobalVarLn.dZdat_comm,

                    ref dLat1        // приращение по долготе, угл.сек
                );
            objClassMap10_ed.f_WGS84_SK42_Lat_Long
                   (
                // Входные параметры (град,км)
                       xtmp1_ed,   // широта
                       ytmp1_ed,  // долгота
                       0,     // высота
                       dLat1,       // приращение по долготе, угл.сек
                       dLong1,      // приращение по долготе, угл.сек

                       // Выходные параметры (grad)
                       ref LatKrG1,   // широта
                       ref LongKrG1   // долгота
                   );
            // ellipsKras -> M real
            xtmp1_ed = LatKrG1;
            ytmp1_ed = LongKrG1;
            // Grad->rad
            xtmp1_ed = (xtmp1_ed * Math.PI) / 180;
            ytmp1_ed = (ytmp1_ed * Math.PI) / 180;
            mapGeoToPlane(GlobalVarLn.hmapl, ref xtmp1_ed, ref ytmp1_ed);

            // Redraw
            ClassMap.f_Pol_GPS_IRI(
                                       xtmp1_ed,
                                       ytmp1_ed,
                                       Color.Blue,
                                       ""
                                      );

            // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

        } //  Button9

        // ********************************************************* PELENG_OTLADKA



        // Peleng_IRI_FRCH ********************************************************

        // Color ------------------------------------------------------------------
        /// <summary>
        /// Convert HEX to color
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        private static System.Drawing.Color ConvertToColor(String s)
        {
            //s = s.Remove(0, 1);
            string str = s.Remove(0, 1);
            int R;
            int G;
            int B;
            System.Drawing.Color rtn = System.Drawing.Color.Empty;
            try
            {
                R = Convert.ToByte(str.Substring(0, 2), 16);
                G = Convert.ToByte(str.Substring(2, 2), 16);
                B = Convert.ToByte(str.Substring(4, 2), 16);
                rtn = Color.FromArgb(R, G, B);
            }
            catch (Exception)
            {
                //doing nothing
            }

            return rtn;
        }
        // ------------------------------------------------------------------ Color

        // Interrupt_Peleng_IRI ---------------------------------------------------
        // INTERRUPT
        // !!! Для ФРЧ

        private double prevPel1 = -1;
        private double prevPel2 = -1;

        private object threadLock = new object();
        void service_CoordsIRIUpdated(object sender, TCoordsIRI[] coordsIRI)
        {
         lock (threadLock)
         {

             // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
             //  01_10_2018
             // Delete all

             byte bDel = coordsIRI[0].bDelIRI; // 0 - add IRI, 1 - delete IRI 2- del all 3- IRI Не трогать (только пеленги)

             // При удалении всего придет одна структура
             if (bDel == 2)
             {
                 // Del Peleng1,2
                 if (chbPeleng.Checked == true)
                 {
                     // FRCH
                     GlobalVarLn.PrevP1 = -1;
                     GlobalVarLn.PrevP2 = -1;
                     GlobalVarLn.flPelMain2 = 0;   // 2-й пеленг
                     GlobalVarLn.fl_PelIRI_1 = 0; // 1-й пеленг
                     GlobalVarLn.flag_eq_draw1 = 0;
                     GlobalVarLn.flag_eq_draw2 = 0;
                     GlobalVarLn.PELENGG_1 = -1;
                     GlobalVarLn.PELENGG_2 = -1;

                 }

                 // IRI, есть Repaint
                 ClassMap.f_DelGPS_IRI(0, 2);

                 return;

             } // bDel=2
             // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

        // PELENG ..................................................................

        if ((GlobalVarLn.list1_Sost.Count != 0)&&(bDel==3))
        {
          if (chbPeleng.Checked == true)
          {
                        double azgr1 = -1;
                        double azgr2 = -1;

                        if (coordsIRI[0].tDirections.iQ1 != -1)
                            azgr1 = (double)(coordsIRI[0].tDirections.iQ1 / 10d); // Peleng1, grad
                        if (coordsIRI[0].tDirections.iQ2 != -1)
                            azgr2 = (double)(coordsIRI[0].tDirections.iQ2 / 10d); // Peleng2

                        if ((coordsIRI[0].tDirections.iQ1 != -1) && (GlobalVarLn.list1_Sost[0].X_m != 0) && (GlobalVarLn.list1_Sost[0].Y_m != 0))
                                GlobalVarLn.fl_PelIRI_1 = 1;
                        else
                            GlobalVarLn.fl_PelIRI_1 = 0;

                        if ((coordsIRI[0].tDirections.iQ2 != -1) && (GlobalVarLn.list1_Sost[1].X_m != 0) && (GlobalVarLn.list1_Sost[1].Y_m != 0))
                            GlobalVarLn.flPelMain2 = 1;
                        else
                            GlobalVarLn.flPelMain2 = 0;

                        // 28_09_2018
                        GlobalVarLn.PELENGG_1 = azgr1;
                        GlobalVarLn.PELENGG_2 = azgr2;

                        // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                        // 01_10_2018
                        // Удаление 2-ух пеленгов + all IRI

                        //if (coordsIRI.bDelIRI == 2)
                        //{
                        //    // FRCH
                        //    GlobalVarLn.PrevP1 = -1;
                        //    GlobalVarLn.PrevP2 = -1;
                        //    GlobalVarLn.flPelMain2 = 0;   // 2-й пеленг
                        //    GlobalVarLn.fl_PelIRI_1 = 0; // 1-й пеленг
                        //    GlobalVarLn.flag_eq_draw1 = 0;
                        //    GlobalVarLn.flag_eq_draw2 = 0;
                        //    GlobalVarLn.PELENGG_1 = -1;
                        //    GlobalVarLn.PELENGG_2 = -1;
                        //    azgr1 = -1;
                        //    azgr2 = -1;

                        //} // Удаление 2-ух пеленгов
                        // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>



                        //if (
                        //    (azgr1 != GlobalVarLn.PrevP1) ||
                        //    (azgr2 != GlobalVarLn.PrevP2) ||
                        //    (GlobalVarLn.flag_eq_draw1 == 1)||
                        //    (GlobalVarLn.flag_eq_draw2 == 1)
                        //   )
                        // 28_09_2018
                        if (
                            (
                            //(azgr1 != GlobalVarLn.PrevP1) ||
                            //(azgr2 != GlobalVarLn.PrevP2) ||
                            (Math.Abs(azgr1 - GlobalVarLn.PrevP1)) > 1 ||
                            (Math.Abs(azgr2 - GlobalVarLn.PrevP2)) > 1 ||
                            (GlobalVarLn.flag_eq_draw1 == 1) ||
                            (GlobalVarLn.flag_eq_draw2 == 1)
                            ) &&
                            ((GlobalVarLn.fl_PelIRI_1 == 1) || (GlobalVarLn.flPelMain2 == 1))
                           )
                        {
                            GlobalVarLn.flag_eq_draw1 = 0;
                            GlobalVarLn.flag_eq_draw2 = 0;

                            GlobalVarLn.X1_PelMain = (int)GlobalVarLn.list1_Sost[0].X_m;  // SP1
                            GlobalVarLn.Y1_PelMain = (int)GlobalVarLn.list1_Sost[0].Y_m;
                            GlobalVarLn.X1_1_PelMain = (int)GlobalVarLn.list1_Sost[1].X_m; // SP2
                            GlobalVarLn.Y1_1_PelMain = (int)GlobalVarLn.list1_Sost[1].Y_m;

                            double a1 = 0;
                            double b1 = 0;
                            double c1 = 0;
                            double d1 = 0;
                            double f1 = 0;
                            double g1 = 0;
                            double a2 = 0;
                            double b2 = 0;
                            double c2 = 0;
                            double d2 = 0;
                            double f2 = 0;
                            double g2 = 0;
                            double x184 = 0;
                            double y184 = 0;
                            double x284 = 0;
                            double y284 = 0;

                            double[] arr_Pel_XYZ1 = new double[GlobalVarLn.numberofdots * 3];
                            double[] arr_Pel_XYZ2 = new double[GlobalVarLn.numberofdots * 3];
                            ClassMap classMap = new ClassMap();
                            ClassMap classMap1 = new ClassMap();

                            x184 = GlobalVarLn.X1_PelMain;
                            y184 = GlobalVarLn.Y1_PelMain;
                            x284 = GlobalVarLn.X1_1_PelMain;
                            y284 = GlobalVarLn.Y1_1_PelMain;

                            mapPlaneToGeo(GlobalVarLn.hmapl, ref x184, ref y184);
                            mapPlaneToGeo(GlobalVarLn.hmapl, ref x284, ref y284);

                            // rad->grad
                            x184 = (x184 * 180) / Math.PI;
                            y184 = (y184 * 180) / Math.PI;
                            x284 = (x284 * 180) / Math.PI;
                            y284 = (y284 * 180) / Math.PI;

                            // Peleng1
                            if (GlobalVarLn.fl_PelIRI_1==1)

                            {
                            classMap.f_Peleng(azgr1, GlobalVarLn.distance, GlobalVarLn.numberofdots, x184, y184,
                                              ref a1, ref b1, ref c1, ref d1, ref f1, ref g1,
                                              ref GlobalVarLn.arr_Pel1, ref arr_Pel_XYZ1);
                            }
                            // Peleng2
                            if (GlobalVarLn.flPelMain2 == 1)
                            {
                                classMap1.f_Peleng(azgr2, GlobalVarLn.distance, GlobalVarLn.numberofdots, x284, y284,
                                                   ref a2, ref b2, ref c2, ref d2, ref f2, ref g2,
                                                   ref GlobalVarLn.arr_Pel2, ref arr_Pel_XYZ2);
                            } 

                            axaxcMapScreen1.Repaint();

                            //ClassMap.f_ReDrawPeleng();
                            //GlobalVarLn.PrevP1 = azgr1;
                            //GlobalVarLn.PrevP2 = azgr2;


                            // 28_09_2018
                            //if (
                            //    (
                            //    (azgr1 != GlobalVarLn.PrevP1) ||
                            //    (azgr2 != GlobalVarLn.PrevP2) ||
                            //    (GlobalVarLn.flag_eq_draw1 == 1)||
                            //    (GlobalVarLn.flag_eq_draw2 == 1)
                            //    )&&
                            //    ((GlobalVarLn.fl_PelIRI_1 == 1) || (GlobalVarLn.flPelMain2==1))
                            //   )
                        } // !!! См. IF указанное выше - это конец этого IF



                        // 02_10_2018
                        if (
                            ((azgr1 != GlobalVarLn.PrevP1) && (azgr1 == -1)) ||
                            ((azgr2 != GlobalVarLn.PrevP2) && (azgr2 == -1))
                           )
                        {
                            if (azgr1 == -1)
                            {
                                GlobalVarLn.fl_PelIRI_1 = 0; // 1-й пеленг
                                GlobalVarLn.flag_eq_draw1 = 0;
                                GlobalVarLn.PELENGG_1 = -1;
                            }

                            if (azgr2 == -1)
                            {
                                GlobalVarLn.flPelMain2 = 0;   // 2-й пеленг
                                GlobalVarLn.flag_eq_draw2 = 0;
                                GlobalVarLn.PELENGG_2 = -1;
                            }
                            // Убрать с карты
                            GlobalVarLn.axMapScreenGlobal.Repaint();
                        }

                        GlobalVarLn.PrevP1 = azgr1;
                        GlobalVarLn.PrevP2 = azgr2;

                   } // Chacked==true

                } // GlobalVarLn.list1_Sost.Count != 0 && bDel==3
                // .................................................................. PELENG

               // IRI ......................................................................
               // IRI
               // 02_10_2018

                   //  01_10_2018
                   //byte bDel = coordsIRI.bDelIRI; // 0 - add, 1 - delete 2- del all IRI
                   //if (GlobalVarLn.AdDlGPS_IRI == 2)
                   //    ClassMap.f_DelGPS_IRI(0, 2);

                if (bDel == 3)
                    return;
                    
                // FOR1
                for (int yjy = 0; yjy < coordsIRI.Length; yjy++)
                {

                    if ((coordsIRI[yjy].dLat != -1) && (coordsIRI[yjy].dLon != -1))
                    {
                        GlobalVarLn.flGPS_IRI = 1;

                        byte bDel1 = coordsIRI[yjy].bDelIRI; // 0 - add, 1 - delete 

                        GlobalVarLn.AdDlGPS_IRI = (int)bDel;
                        GlobalVarLn.LatGPS_IRI = coordsIRI[yjy].dLat;
                        GlobalVarLn.LongGPS_IRI = coordsIRI[yjy].dLon;
                        GlobalVarLn.IDGPS_IRI = coordsIRI[yjy].iID;

                        double freqd = coordsIRI[yjy].iFreq / 10d;

                        GlobalVarLn.sGPS_IRI = Convert.ToString(freqd);

                        GlobalVarLn.ColGPS_IRI = ColorFromSharpString(coordsIRI[yjy].sColorIRI);

                        if (GlobalVarLn.AdDlGPS_IRI == 0)
                        {
                            ClassMap.f_GetGPS_IRI(GlobalVarLn.IDGPS_IRI);
                        }
                        else
                        {
                            ClassMap.f_DelGPS_IRI(GlobalVarLn.IDGPS_IRI, bDel);
                        }

                    } // Lat,Long!=-1 

                } // FOR1
             GlobalVarLn.axMapScreenGlobal.Repaint();
             // Redraw
             ClassMap.f_Map_Redraw_GPS_IRI();
               // ...................................................................... IRI


        } // lock (threadLock)
        } // INTERRUPT
        // --------------------------------------------------- Interrupt_Peleng_IRI

        // Color ------------------------------------------------------------------

        private Color ColorFromSharpString(string sharpString)
        {
            try
            {
                string hexColor = sharpString.Replace("#", "FF");
                int hexValue = int.Parse(hexColor, System.Globalization.NumberStyles.HexNumber);
                return Color.FromArgb(hexValue);
            }
            catch { return Color.FromArgb(0,0,0,0); }
        }
        // ------------------------------------------------------------------ Color

        // ******************************************************** Peleng_IRI_FRCH

        // Peleng_IRI_PPRCH *******************************************************
        // INTERRUPT

        //private object threadLock_2 = new object();
        //void service_CoordsIRI_PPRChUpdated(object sender, TCoordsIRI_PPRCh[] coordsIRI)
        //{


/*
         // ------------------------------------------------------------------------
            int countPel = 0;
            double a1 = 0;
            double b1 = 0;
            double c1 = 0;
            double d1 = 0;
            double f1 = 0;
            double g1 = 0;
            double a2 = 0;
            double b2 = 0;
            double c2 = 0;
            double d2 = 0;
            double f2 = 0;
            double g2 = 0;
            double x184 = 0;
            double y184 = 0;
            double x284 = 0;
            double y284 = 0;

            double[] arr_Pel_XYZ1 = new double[GlobalVarLn.numberofdots * 3];
            double[] arr_Pel_XYZ2 = new double[GlobalVarLn.numberofdots * 3];

            double azgr1 = -1;
            double azgr2 = -1;
            PelIRI objPelIRI = new PelIRI();

            int flg = 0;
            int ind = 0;

            int AdDl = 0;
            //Color clr = Color.Red;
            // ------------------------------------------------------------------------

            // ------------------------------------------------------------------------
            lock (threadLock_2)
            {

             AdDl = (int)coordsIRI[countPel].bDelIRI; // 0 - add, 1 - delete;

            // Delete All .............................................................

             if (AdDl == 2)
             {

              for (int iii20 = (GlobalVarLn.list_PelIRI.Count - 1); iii20 >= 0; iii20--)
              {
               GlobalVarLn.list_PelIRI.Remove(GlobalVarLn.list_PelIRI[iii20]);
              }

              axaxcMapScreen1.Repaint();
              return;

             } 
             // ............................................................. Delete All

             // Delete ID ..............................................................

             if (AdDl == 1)
             {

                 for (int iii21 = (GlobalVarLn.list_PelIRI.Count - 1); iii21 >= 0; iii21--)
                 {
                     if (GlobalVarLn.list_PelIRI[iii21].ID == coordsIRI[countPel].iID)
                       GlobalVarLn.list_PelIRI.Remove(GlobalVarLn.list_PelIRI[iii21]);
                 }

                 axaxcMapScreen1.Repaint();
                 return;

             }
             // .............................................................. Delete ID

             // Del_Struct_ID ..........................................................
            // Сначала удаляем все структуры с таким IDi (со всеми ID из входного массива структур)

            // FOR1
            for (countPel = 0; countPel < coordsIRI.Length; countPel++ )
            {
                // for2
                for (int iii = (GlobalVarLn.list_PelIRI.Count - 1); iii >= 0; iii--)
                {
                    if (GlobalVarLn.list_PelIRI[iii].ID == coordsIRI[countPel].iID)
                         GlobalVarLn.list_PelIRI.Remove(GlobalVarLn.list_PelIRI[iii]);

                } //for2

            } //FOR1
            // .......................................................... Del_Struct_ID

            // Add_Struct_ID ..........................................................
            // Потом добавляем в лист массив пришедших структур

            // FOR1_1
            for (countPel = 0; countPel < coordsIRI.Length; countPel++)
            {

                if (
                     (coordsIRI[countPel].iQ1 != -1) ||
                     ((coordsIRI[countPel].dLatitude != -1) && (coordsIRI[countPel].dLongitude != -1))
                   )
                {
                    objPelIRI.ID = coordsIRI[countPel].iID;
                    objPelIRI.XGPS_IRI = -1;
                    objPelIRI.YGPS_IRI = -1;
                    objPelIRI.flPelMain2 = 0;
                    objPelIRI.arr_Pel1 = new double[10000];     // R,Широта,долгота
                    objPelIRI.arr_Pel2 = new double[10000];     // R,Широта,долгота

                    if ((coordsIRI[countPel].iQ1 != -1) && (chbPeleng.Checked == true))
                        objPelIRI.Pel1 = (double)(coordsIRI[countPel].iQ1/10d);
                    else
                        objPelIRI.Pel1 = -1;

                    if ((coordsIRI[countPel].iQ2 != -1) && (chbPeleng.Checked == true))
                    {
                        objPelIRI.Pel2 = (double)(coordsIRI[countPel].iQ2 / 10d);
                        objPelIRI.flPelMain2 = 1;
                    }
                    else
                    {
                        objPelIRI.Pel2 = -1;
                        objPelIRI.flPelMain2 = 0;
                    }

                    if ((coordsIRI[countPel].dLatitude != -1) && (coordsIRI[countPel].dLongitude != -1))
                    {
                        objPelIRI.Lat = coordsIRI[countPel].dLatitude;
                        objPelIRI.Long = coordsIRI[countPel].dLongitude;
                        objPelIRI.color = ColorFromSharpString(coordsIRI[countPel].sColorIRI);
                        //objPelIRI.color = clr;

                        objPelIRI.AddDel = (int)coordsIRI[countPel].bDelIRI; // 0 - add, 1 - delete;
                    }
                    else
                    {
                        objPelIRI.Lat = -1;
                        objPelIRI.Long = -1;
                        objPelIRI.XGPS_IRI = -1;
                        objPelIRI.YGPS_IRI = -1;
                        objPelIRI.AddDel = (int)coordsIRI[countPel].bDelIRI; // 0 - add, 1 - delete;

                    }

                    GlobalVarLn.list_PelIRI.Add(objPelIRI);

                } //IF(есть пеленг или координаты) 

            } //FOR1_1
            // .......................................................... Add_Struct_ID

            // Remove from List .......................................................
            // Если в элементе List стали все -1

            // for5
            for (int iii7 = (GlobalVarLn.list_PelIRI.Count - 1); iii7 >=0; iii7--)
            {
                if (
                    (GlobalVarLn.list_PelIRI[iii7].Pel1 == -1)&&
                    (GlobalVarLn.list_PelIRI[iii7].Pel2 == -1)&&
                    (GlobalVarLn.list_PelIRI[iii7].Lat == -1)&&
                    (GlobalVarLn.list_PelIRI[iii7].Long == -1)
                   )
                {
                    GlobalVarLn.list_PelIRI.Remove(GlobalVarLn.list_PelIRI[iii7]);
                }

            } //for5
            // ....................................................... Remove from List

            // PELENG &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
            // PELENG 

            if (GlobalVarLn.list1_Sost.Count != 0)
            {
             GlobalVarLn.X1_PelMain = (int)GlobalVarLn.list1_Sost[0].X_m;  // SP1
             GlobalVarLn.Y1_PelMain = (int)GlobalVarLn.list1_Sost[0].Y_m;
             GlobalVarLn.X1_1_PelMain = (int)GlobalVarLn.list1_Sost[1].X_m; // SP2
             GlobalVarLn.Y1_1_PelMain = (int)GlobalVarLn.list1_Sost[1].Y_m;

             if (chbPeleng.Checked == true)
             {

              // FOR33 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
              for (countPel = 0; countPel < coordsIRI.Length; countPel++ )
              {
               // 28_09_2018
               if ((coordsIRI[countPel].iQ1 != -1) || (coordsIRI[countPel].iQ2 != -1))
               {
                  // for55
                  flg = 0;
                  for (int iii1 = 0; iii1 < GlobalVarLn.list_PelIRI.Count; iii1++)
                  {
                    // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
                    // Нашли этот элемент в List
                    // 28_09_2018

                    double nbm = (double)(coordsIRI[countPel].iQ1 / 10d);
                    double nbm2 = (double)(coordsIRI[countPel].iQ2 / 10d);

                    if (
                        (GlobalVarLn.list_PelIRI[iii1].ID == coordsIRI[countPel].iID) &&
                        ((GlobalVarLn.list_PelIRI[iii1].Pel1 == nbm) || (GlobalVarLn.list_PelIRI[iii1].Pel1 == -1 && coordsIRI[countPel].iQ1 == -1)) &&
                        ((GlobalVarLn.list_PelIRI[iii1].Pel2 == nbm2) || (GlobalVarLn.list_PelIRI[iii1].Pel2 == -1 && coordsIRI[countPel].iQ2 == -1))
                       )
                    // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
                    // Нашли этот элемент в List
                    //if (
                    //    (GlobalVarLn.list_PelIRI[iii1].ID == coordsIRI[countPel].iID)&&
                    //    (GlobalVarLn.list_PelIRI[iii1].Pel1 == (coordsIRI[countPel].iQ1/10d))&&
                    //    (GlobalVarLn.list_PelIRI[iii1].Pel2 == (coordsIRI[countPel].iQ2/10d))
                    //   )
                        {
                         ind = iii1;
                         flg = 1;
                         }
                    } //for55

                    // Элемент нашли в листе ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
                    if (flg == 1)
                    {
                                  if (coordsIRI[countPel].iQ1 != -1)
                                  {
                                      azgr1 = (double)(coordsIRI[countPel].iQ1 / 10d); // Peleng1, grad
                                  }
                                  if (coordsIRI[countPel].iQ2 != -1)
                                  {
                                      azgr2 = (double)(coordsIRI[countPel].iQ2 / 10d); // Peleng2
                                  }

                                      x184 = GlobalVarLn.X1_PelMain;
                                      y184 = GlobalVarLn.Y1_PelMain;
                                      x284 = GlobalVarLn.X1_1_PelMain;
                                      y284 = GlobalVarLn.Y1_1_PelMain;

                                      mapPlaneToGeo(GlobalVarLn.hmapl, ref x184, ref y184);
                                      mapPlaneToGeo(GlobalVarLn.hmapl, ref x284, ref y284);

                                      // rad->grad
                                      x184 = (x184 * 180) / Math.PI;
                                      y184 = (y184 * 180) / Math.PI;
                                      x284 = (x284 * 180) / Math.PI;
                                      y284 = (y284 * 180) / Math.PI;

                                      ClassMap classMap = new ClassMap();
                                      ClassMap classMap1 = new ClassMap();

                                      // 28_09_2018
                                      // Peleng1
                                      if (coordsIRI[countPel].iQ1 != -1)
                                      {
                                          PelIRI[] mass1 = GlobalVarLn.list_PelIRI.ToArray();
                                          classMap.f_Peleng(azgr1, GlobalVarLn.distance, GlobalVarLn.numberofdots, x184, y184,
                                                            ref a1, ref b1, ref c1, ref d1, ref f1, ref g1,
                                                            ref mass1[ind].arr_Pel1, ref arr_Pel_XYZ1);
                                          GlobalVarLn.list_PelIRI = mass1.ToList();
                                      } // Peleng1

                                      // Peleng2
                                      if (GlobalVarLn.list_PelIRI[ind].flPelMain2 == 1)
                                      {
                                          PelIRI[] mass2 = GlobalVarLn.list_PelIRI.ToArray();
                                          classMap1.f_Peleng(azgr2, GlobalVarLn.distance, GlobalVarLn.numberofdots, x284, y284,
                                                             ref a2, ref b2, ref c2, ref d2, ref f2, ref g2,
                                                             ref mass2[ind].arr_Pel2, ref arr_Pel_XYZ2);
                                          GlobalVarLn.list_PelIRI = mass2.ToList();

                                      } // Peleng2

                   } //IF(flg==1)
                   // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,, Элемент нашли в листе

                 } // if ((coordsIRI[countPel].iQ1 != -1) || (coordsIRI[countPel].iQ2 != -1))
                 } // FOR33
                 // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> FOR33


                } // Chacked==true
                } // GlobalVarLn.list1_Sost.Count != 0
                // &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& PELENG

                // IRI &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
                // IRI 

                // FOR99
               for (countPel = 0; countPel < coordsIRI.Length; countPel++ )
               {

               if ((coordsIRI[countPel].dLatitude != -1) && (coordsIRI[countPel].dLongitude != -1))
               {
                   // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
                  // for66
                  flg = 0;
                  for (int iii2 = 0; iii2 < GlobalVarLn.list_PelIRI.Count; iii2++)
                  {
                      if (
                          (GlobalVarLn.list_PelIRI[iii2].ID == coordsIRI[countPel].iID)&&
                          (GlobalVarLn.list_PelIRI[iii2].Lat==coordsIRI[countPel].dLatitude)&&
                          (GlobalVarLn.list_PelIRI[iii2].Long == coordsIRI[countPel].dLongitude)
                         )
                      {
                          ind = iii2;
                          flg = 1;
                      }

                  } //for66
                  // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,

                  // Элемент нашли в листе ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,

                  if (flg == 1)
                  {
                          if (GlobalVarLn.list_PelIRI[ind].AddDel == 0)
                          {
                              ClassMap.f_GetGPS_IRI_2(ind);
                          }

                          else
                          {
                              //ClassMap.f_DelGPS_IRI_2(GlobalVarLn.list_PelIRI[ind].ID);
                          }

                   } //IF(flg==1)
                   // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,, Элемент нашли в листе

              } // Lat,Long!=-1 
              } // FOR99
               // &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& IRI

              //?????????????????
              axaxcMapScreen1.Repaint();

          } // lock (threadLock_2)
            // ------------------------------------------------------------------------

*/
        // 19_10_2018

/*
            // 19_10_2018 ****************************************************************
            int countPel = 0;
            double a1 = 0;
            double b1 = 0;
            double c1 = 0;
            double d1 = 0;
            double f1 = 0;
            double g1 = 0;
            double a2 = 0;
            double b2 = 0;
            double c2 = 0;
            double d2 = 0;
            double f2 = 0;
            double g2 = 0;
            double x184 = 0;
            double y184 = 0;
            double x284 = 0;
            double y284 = 0;

            double[] arr_Pel_XYZ1 = new double[GlobalVarLn.numberofdots * 3];
            double[] arr_Pel_XYZ2 = new double[GlobalVarLn.numberofdots * 3];

            double azgr1 = -1;
            double azgr2 = -1;
            PelIRI objPelIRI = new PelIRI();
            PelIRI objPelIRI2 = new PelIRI();  // For IRI

            int flg = 0;
            int ind = 0;

            int AdDl = 0;
            //Color clr = Color.Red;
            // ------------------------------------------------------------------------

            // ------------------------------------------------------------------------
            lock (threadLock_2)
            {

                // !!! Идут только пеленги (флпаг 3)/только ИРИ(флаг 0/1)/ удалить все (флаг 2)
                AdDl = (int)coordsIRI[0].bDelIRI; 

                // Delete All ************************************************************
                // Delete Peleng+IRI

                if (AdDl == 2)
                {

                    // Del Peleng
                    for (int iii20 = (GlobalVarLn.list_PelIRI.Count - 1); iii20 >= 0; iii20--)
                    {
                        GlobalVarLn.list_PelIRI.Remove(GlobalVarLn.list_PelIRI[iii20]);
                    }
                    // DelIRI
                    for (int iii21 = (GlobalVarLn.list_PelIRI2.Count - 1); iii21 >= 0; iii21--)
                    {
                        GlobalVarLn.list_PelIRI2.Remove(GlobalVarLn.list_PelIRI2[iii21]);
                    }

                    axaxcMapScreen1.Repaint();
                    return;

                }  // AdDl=2
                // ************************************************************ Delete All

                // Peleng ****************************************************************
                // Del(Q1=Q2=-1)/Add Peleng
                // !!! Old Peleng delete

                if (
                    (AdDl == 3)&&
                    (chbPeleng.Checked == true)
                    )
                {
                    // ........................................................................
                    // Все старые пеленги удалить

                    for (int iii22 = (GlobalVarLn.list_PelIRI.Count - 1); iii22 >= 0; iii22--)
                    {
                        GlobalVarLn.list_PelIRI.Remove(GlobalVarLn.list_PelIRI[iii22]);
                    }
                    // ........................................................................

                    // Add_Struct_ID ..........................................................
                    // Потом добавляем в лист массив пришедших структур

                    // FOR1_1
                    for (countPel = 0; countPel < coordsIRI.Length; countPel++)
                    {
                        if (
                             (coordsIRI[countPel].iQ1 != -1) ||
                             (coordsIRI[countPel].iQ2 != -1)
                           )
                        {
                            objPelIRI.ID = coordsIRI[countPel].iID;
                            objPelIRI.XGPS_IRI = -1;
                            objPelIRI.YGPS_IRI = -1;
                            objPelIRI.flPelMain2 = 0;
                            objPelIRI.arr_Pel1 = new double[10000];     // R,Широта,долгота
                            objPelIRI.arr_Pel2 = new double[10000];     // R,Широта,долгота
                            objPelIRI.Lat = -1;
                            objPelIRI.Long = -1;
                            objPelIRI.AddDel = (int)coordsIRI[countPel].bDelIRI; // 3 fore Peleng

                            if ((coordsIRI[countPel].iQ1 != -1) && (chbPeleng.Checked == true))
                                objPelIRI.Pel1 = (double)(coordsIRI[countPel].iQ1 / 10d);
                            else
                                objPelIRI.Pel1 = -1;

                            if ((coordsIRI[countPel].iQ2 != -1) && (chbPeleng.Checked == true))
                            {
                                objPelIRI.Pel2 = (double)(coordsIRI[countPel].iQ2 / 10d);
                                objPelIRI.flPelMain2 = 1;
                            }
                            else
                            {
                                objPelIRI.Pel2 = -1;
                                objPelIRI.flPelMain2 = 0;
                            }

                            GlobalVarLn.list_PelIRI.Add(objPelIRI);

                        } //IF(есть пеленг) 

                    } //FOR1_1
                    // .......................................................... Add_Struct_ID

                    // Calculate  .............................................................

                    GlobalVarLn.X1_PelMain = (int)GlobalVarLn.list1_Sost[0].X_m;  // SP1
                    GlobalVarLn.Y1_PelMain = (int)GlobalVarLn.list1_Sost[0].Y_m;
                    GlobalVarLn.X1_1_PelMain = (int)GlobalVarLn.list1_Sost[1].X_m; // SP2
                    GlobalVarLn.Y1_1_PelMain = (int)GlobalVarLn.list1_Sost[1].Y_m;

                    // FOR33 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                    for (countPel = 0; countPel < coordsIRI.Length; countPel++)
                    {

                        if ((coordsIRI[countPel].iQ1 != -1) || (coordsIRI[countPel].iQ2 != -1))
                        {
                            if (coordsIRI[countPel].iQ1 != -1)
                            {
                                azgr1 = (double)(coordsIRI[countPel].iQ1 / 10d); // Peleng1, grad
                            }
                            if (coordsIRI[countPel].iQ2 != -1)
                            {
                                azgr2 = (double)(coordsIRI[countPel].iQ2 / 10d); // Peleng2
                            }

                            x184 = GlobalVarLn.X1_PelMain;
                            y184 = GlobalVarLn.Y1_PelMain;
                            x284 = GlobalVarLn.X1_1_PelMain;
                            y284 = GlobalVarLn.Y1_1_PelMain;

                            mapPlaneToGeo(GlobalVarLn.hmapl, ref x184, ref y184);
                            mapPlaneToGeo(GlobalVarLn.hmapl, ref x284, ref y284);

                            // rad->grad
                            x184 = (x184 * 180) / Math.PI;
                            y184 = (y184 * 180) / Math.PI;
                            x284 = (x284 * 180) / Math.PI;
                            y284 = (y284 * 180) / Math.PI;

                            ClassMap classMap = new ClassMap();
                            ClassMap classMap1 = new ClassMap();

                            // Peleng1
                            if (coordsIRI[countPel].iQ1 != -1)
                            {
                                PelIRI[] mass1 = GlobalVarLn.list_PelIRI.ToArray();
                                classMap.f_Peleng(azgr1, GlobalVarLn.distance, GlobalVarLn.numberofdots, x184, y184,
                                                  ref a1, ref b1, ref c1, ref d1, ref f1, ref g1,
                                                  ref mass1[countPel].arr_Pel1, ref arr_Pel_XYZ1);
                                GlobalVarLn.list_PelIRI = mass1.ToList();
                            } // Peleng1

                            // Peleng2
                            if (GlobalVarLn.list_PelIRI[countPel].flPelMain2 == 1)
                            {
                                PelIRI[] mass2 = GlobalVarLn.list_PelIRI.ToArray();
                                classMap1.f_Peleng(azgr2, GlobalVarLn.distance, GlobalVarLn.numberofdots, x284, y284,
                                                   ref a2, ref b2, ref c2, ref d2, ref f2, ref g2,
                                                   ref mass2[countPel].arr_Pel2, ref arr_Pel_XYZ2);
                                GlobalVarLn.list_PelIRI = mass2.ToList();

                            } // Peleng2


                        } // if ((coordsIRI[countPel].iQ1 != -1) || (coordsIRI[countPel].iQ2 != -1))

                    } // FOR33
                    //  ............................................................. Calculate

                    axaxcMapScreen1.Repaint();
                    return;

                }  // AdDl=3chbPeleng.Checked == true
                // **************************************************************** Peleng

               // IRI ********************************************************************

                // Del_Struct_ID ..........................................................
                // Сначала удаляем все структуры с таким IDi (со всеми ID из входного массива структур)

                // FOR1
                for (countPel = 0; countPel < coordsIRI.Length; countPel++)
                {
                    // for2
                    for (int iii = (GlobalVarLn.list_PelIRI2.Count - 1); iii >= 0; iii--)
                    {
                        if (GlobalVarLn.list_PelIRI2[iii].ID == coordsIRI[countPel].iID)
                            GlobalVarLn.list_PelIRI2.Remove(GlobalVarLn.list_PelIRI2[iii]);

                    } //for2

                } //FOR1
                // .......................................................... Del_Struct_ID

                // Add_Struct_ID ..........................................................
                // Потом добавляем в лист массив пришедших структур

                // FOR1_5
                for (countPel = 0; countPel < coordsIRI.Length; countPel++)
                {
                    int ffg=(int)coordsIRI[countPel].bDelIRI;

                    if (
                         (coordsIRI[countPel].dLatitude != -1) &&
                         (coordsIRI[countPel].dLongitude != -1)&&
                         (ffg==0)
                       )
                    {
                        objPelIRI2.ID = coordsIRI[countPel].iID;
                        objPelIRI2.XGPS_IRI = -1;
                        objPelIRI2.YGPS_IRI = -1;
                        objPelIRI2.flPelMain2 = 0;
                        objPelIRI2.arr_Pel1 = new double[10000];     // R,Широта,долгота
                        objPelIRI2.arr_Pel2 = new double[10000];     // R,Широта,долгота
                        objPelIRI2.Pel1 = -1;
                        objPelIRI2.Pel2 = -1;

                        if ((coordsIRI[countPel].dLatitude != -1) && (coordsIRI[countPel].dLongitude != -1))
                        {
                            objPelIRI2.Lat = coordsIRI[countPel].dLatitude;
                            objPelIRI2.Long = coordsIRI[countPel].dLongitude;
                            objPelIRI2.color = ColorFromSharpString(coordsIRI[countPel].sColorIRI);
                            objPelIRI2.AddDel = (int)coordsIRI[countPel].bDelIRI; // 0 - add, 1 - delete;
                        }
                        else
                        {
                            objPelIRI2.Lat = -1;
                            objPelIRI2.Long = -1;
                            objPelIRI2.XGPS_IRI = -1;
                            objPelIRI2.YGPS_IRI = -1;
                            objPelIRI2.AddDel = (int)coordsIRI[countPel].bDelIRI; // 0 - add, 1 - delete;

                        }

                        GlobalVarLn.list_PelIRI2.Add(objPelIRI2);

                        ClassMap.f_GetGPS_IRI_2(GlobalVarLn.list_PelIRI2.Count-1);

                    } //IF(есть координаты) 

                } //FOR1_5
                // .......................................................... Add_Struct_ID

                axaxcMapScreen1.Repaint();

                // ******************************************************************** IRI

            } // threadLock_2
            // **************************************************************** 19_10_2018
  */      
        
    //    } // INTERRUPT
        // ******************************************************* Peleng_IRI_PPRCH


        // *****************************************************************************************
        // Обработчик кнопки Button11
        // *****************************************************************************************
        //2501otl
        private void button11_Click(object sender, EventArgs e)
        {
        // 2504

            if (GlobalVarLn.shtmr1 == 0)
            {
                timer1.Start();
                GlobalVarLn.shtmr1 += 1;
                return;
            }
            else
            {
                timer1.Stop();
                GlobalVarLn.shtmr1 -= 1;
                return;
            }







            //TCoordsIRI_PPRCh[] coordsIRI = new TCoordsIRI_PPRCh[4];
            TCoordsIRI_PPRCh coordsIRI = new TCoordsIRI_PPRCh();
            List<TCoordsIRI_PPRCh> listCoordsIRI_PPRCh = new List<TCoordsIRI_PPRCh>();
            Color clr = Color.Red;

            if (GlobalVarLn.shPelIRI == 0)
            {
            coordsIRI.bDelIRI = 0;
            coordsIRI.iID = 1;
            coordsIRI.dLatitude = 56.649194;
            coordsIRI.dLongitude = 24.388778;
            listCoordsIRI_PPRCh.Add(coordsIRI);

            coordsIRI.bDelIRI = 0;
            coordsIRI.iID = 2;
            coordsIRI.dLatitude = 56.650417;
            coordsIRI.dLongitude = 24.454536;
            listCoordsIRI_PPRCh.Add(coordsIRI);

            coordsIRI.bDelIRI = 0;
            coordsIRI.iID = 3;
            coordsIRI.dLatitude = 56.613391;
            coordsIRI.dLongitude = 24.39187;
            listCoordsIRI_PPRCh.Add(coordsIRI);

            coordsIRI.bDelIRI = 0;
            coordsIRI.iID = 7;
            coordsIRI.dLatitude = 56.614923;
            coordsIRI.dLongitude = 24.457059;
            listCoordsIRI_PPRCh.Add(coordsIRI);

            GlobalVarLn.shPelIRI += 1;
            }

            else
            {
                    for (int i = (listCoordsIRI_PPRCh.Count - 1); i >= 0; i--)
                    {
                        listCoordsIRI_PPRCh.Remove(listCoordsIRI_PPRCh[i]);
                    }

            
            coordsIRI.bDelIRI = 0;
            coordsIRI.iID = 4;
            coordsIRI.dLatitude = 56.652315;
            coordsIRI.dLongitude = 24.518753;
            listCoordsIRI_PPRCh.Add(coordsIRI);

            coordsIRI.bDelIRI = 0;
            coordsIRI.iID = 2;
            coordsIRI.dLatitude = 56.653399;
            coordsIRI.dLongitude = 24.58377;
            listCoordsIRI_PPRCh.Add(coordsIRI);

            coordsIRI.bDelIRI = 0;
            coordsIRI.iID = 5;
            coordsIRI.dLatitude = 56.617228;
            coordsIRI.dLongitude = 24.587116;
            listCoordsIRI_PPRCh.Add(coordsIRI);

             GlobalVarLn.shPelIRI -= 1;
            }


            // 2504
            double dds = 250; // m

            int AdDl = 0;
            int countPel = 0;
            int countPel1 = 0;

                // !!! Идут только пеленги (флпаг 3)/только ИРИ(флаг 0/1)/ удалить все (флаг 2)
                AdDl = (int)listCoordsIRI_PPRCh[0].bDelIRI;

                PelIRI objPelIRI2 = new PelIRI();  // For IRI

                // Delete All ************************************************************
                // Delete Peleng+IRI

                if (AdDl == 2)
                {

                    // Del Peleng
                    for (int iii20 = (GlobalVarLn.list_PelIRI.Count - 1); iii20 >= 0; iii20--)
                    {
                        GlobalVarLn.list_PelIRI.Remove(GlobalVarLn.list_PelIRI[iii20]);
                    }
                    // DelIRI
                    for (int iii21 = (GlobalVarLn.list_PelIRI2.Count - 1); iii21 >= 0; iii21--)
                    {
                        GlobalVarLn.list_PelIRI2.Remove(GlobalVarLn.list_PelIRI2[iii21]);
                    }

                    axaxcMapScreen1.Repaint();
                    return;

                }  // AdDl=2
                // ************************************************************ Delete All

                // Delete ID *************************************************************
                // Удалить конкретный ID

                if (AdDl == 1)
                {
                    // Del_Struct_ID ..........................................................
                    // удаляем все структуры с таким IDi 
                    // Придет в 

                    // FOR111
                    for (countPel = 0; countPel < listCoordsIRI_PPRCh.Count; countPel++)
                    {
                        int ffg1 = (int)listCoordsIRI_PPRCh[countPel].bDelIRI;

                        if (ffg1 == 1) // Del
                        {
                            // For22
                            for (int iii33 = (GlobalVarLn.list_PelIRI2.Count - 1); iii33 >= 0; iii33--)
                            {
                                if (GlobalVarLn.list_PelIRI2[iii33].ID == listCoordsIRI_PPRCh[countPel].iID)
                                    GlobalVarLn.list_PelIRI2.Remove(GlobalVarLn.list_PelIRI2[iii33]);

                            } //for22

                        } // IF

                    } // FOR111

                    axaxcMapScreen1.Repaint();
                    return;

                } // AdDl==1 Del
                // ************************************************************* Delete ID

                // Add IRI ****************************************************************

                if (AdDl == 0)
                {

                    // ------------------------------------------------------------------------
                    // 2504

                    // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                    bool SignRepaint = false;
                    int i1 = 0;
                    int i2 = 0;
                    int i3 = 0;
                    int i4 = 0;
                    bool flexist = false;
                    int ffg = 0;
                    // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                    // Убрать в старом те, которых нет в новых

                    for (i1 = (GlobalVarLn.list_PelIRI2.Count - 1); i1 >= 0; i1--) // old
                    {
                        flexist = false;
                        for (i2 = 0; i2 < listCoordsIRI_PPRCh.Count; i2++) // new
                        {
                            ffg = (int)listCoordsIRI_PPRCh[i2].bDelIRI;

                            if (
                                (GlobalVarLn.list_PelIRI2[i1].ID == listCoordsIRI_PPRCh[i2].iID) &&
                                (listCoordsIRI_PPRCh[i2].dLatitude != -1) &&
                                (listCoordsIRI_PPRCh[i2].dLongitude != -1) &&
                                (ffg == 0)
                               )
                            {
                                flexist = true;
                            }

                        } // i2

                        if (flexist == false)
                        {
                            GlobalVarLn.list_PelIRI2.Remove(GlobalVarLn.list_PelIRI2[i1]);
                            SignRepaint = true;
                            //i1 = -1;
                        }

                    } // i1 old

                    // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                    // 

                    // FOR1_5 вперед по новому
                    for (countPel = 0; countPel < listCoordsIRI_PPRCh.Count; countPel++) // new
                    {

                        ffg = (int)listCoordsIRI_PPRCh[countPel].bDelIRI;

                        if (
                             (listCoordsIRI_PPRCh[countPel].dLatitude != -1) &&
                             (listCoordsIRI_PPRCh[countPel].dLongitude != -1) &&
                             (ffg == 0)
                           )
                        {

                            flexist = false;
                            for (int iii99 = (GlobalVarLn.list_PelIRI2.Count - 1); iii99 >= 0; iii99--)
                            {

                                if (GlobalVarLn.list_PelIRI2[iii99].ID == listCoordsIRI_PPRCh[countPel].iID)
                                {
                                    // есть с таким ID
                                    flexist = true;
                                    double sss = 0;
                                    sss = ClassMap.f_D_2Points(GlobalVarLn.list_PelIRI2[iii99].Lat, GlobalVarLn.list_PelIRI2[iii99].Long, listCoordsIRI_PPRCh[countPel].dLatitude, listCoordsIRI_PPRCh[countPel].dLongitude, 1);
                                    if (sss > dds)
                                    {
                                        SignRepaint = true;
                                        // Убираем старый
                                        GlobalVarLn.list_PelIRI2.Remove(GlobalVarLn.list_PelIRI2[iii99]);

                                        // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                                        // добавляем новый

                                        objPelIRI2.ID = listCoordsIRI_PPRCh[countPel].iID;
                                        objPelIRI2.XGPS_IRI = -1;
                                        objPelIRI2.YGPS_IRI = -1;
                                        objPelIRI2.flPelMain2 = 0;
                                        objPelIRI2.arr_Pel1 = new double[10000];     // R,Широта,долгота
                                        objPelIRI2.arr_Pel2 = new double[10000];     // R,Широта,долгота
                                        objPelIRI2.Pel1 = -1;
                                        objPelIRI2.Pel2 = -1;

                                        if ((listCoordsIRI_PPRCh[countPel].dLatitude != -1) && (listCoordsIRI_PPRCh[countPel].dLongitude != -1))
                                        {
                                            objPelIRI2.Lat = listCoordsIRI_PPRCh[countPel].dLatitude;
                                            objPelIRI2.Long = listCoordsIRI_PPRCh[countPel].dLongitude;
                                            objPelIRI2.color = clr;
                                            objPelIRI2.AddDel = (int)listCoordsIRI_PPRCh[countPel].bDelIRI; // 0 - add, 1 - delete;
                                        }
                                        else
                                        {
                                            objPelIRI2.Lat = -1;
                                            objPelIRI2.Long = -1;
                                            objPelIRI2.XGPS_IRI = -1;
                                            objPelIRI2.YGPS_IRI = -1;
                                            objPelIRI2.AddDel = (int)listCoordsIRI_PPRCh[countPel].bDelIRI; // 0 - add, 1 - delete;

                                        }

                                        GlobalVarLn.list_PelIRI2.Add(objPelIRI2);

                                        ClassMap.f_GetGPS_IRI_2(GlobalVarLn.list_PelIRI2.Count - 1);

                                        // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

                                    } // S>ds

                                }  // ID==

                            }  // for old назад

                            if (flexist == false) // это новый ID -> добавляем его
                            {
                                SignRepaint = true;

                                // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                                // добавляем новый

                                objPelIRI2.ID = listCoordsIRI_PPRCh[countPel].iID;
                                objPelIRI2.XGPS_IRI = -1;
                                objPelIRI2.YGPS_IRI = -1;
                                objPelIRI2.flPelMain2 = 0;
                                objPelIRI2.arr_Pel1 = new double[10000];     // R,Широта,долгота
                                objPelIRI2.arr_Pel2 = new double[10000];     // R,Широта,долгота
                                objPelIRI2.Pel1 = -1;
                                objPelIRI2.Pel2 = -1;

                                if ((listCoordsIRI_PPRCh[countPel].dLatitude != -1) && (listCoordsIRI_PPRCh[countPel].dLongitude != -1))
                                {
                                    objPelIRI2.Lat = listCoordsIRI_PPRCh[countPel].dLatitude;
                                    objPelIRI2.Long = listCoordsIRI_PPRCh[countPel].dLongitude;
                                    objPelIRI2.color = clr;
                                    objPelIRI2.AddDel = (int)listCoordsIRI_PPRCh[countPel].bDelIRI; // 0 - add, 1 - delete;
                                }
                                else
                                {
                                    objPelIRI2.Lat = -1;
                                    objPelIRI2.Long = -1;
                                    objPelIRI2.XGPS_IRI = -1;
                                    objPelIRI2.YGPS_IRI = -1;
                                    objPelIRI2.AddDel = (int)listCoordsIRI_PPRCh[countPel].bDelIRI; // 0 - add, 1 - delete;

                                }

                                GlobalVarLn.list_PelIRI2.Add(objPelIRI2);

                                ClassMap.f_GetGPS_IRI_2(GlobalVarLn.list_PelIRI2.Count - 1);

                                // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

                            } // // это новый ID

                        } //IF(есть координаты) 

                    } //FOR1_5 new
                    // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

                    if (SignRepaint == true)
                        axaxcMapScreen1.Repaint();


                } // AdDl==0
                // **************************************************************** Add IRI


                int yyy = 0;
                yyy = yyy;





         // PELENG_IRI_2_OTLADKA *******************************************************************

/*
            // ...............................................................
            int countPel = 0;
            double a1 = 0;
            double b1 = 0;
            double c1 = 0;
            double d1 = 0;
            double f1 = 0;
            double g1 = 0;
            double a2 = 0;
            double b2 = 0;
            double c2 = 0;
            double d2 = 0;
            double f2 = 0;
            double g2 = 0;
            double x184 = 0;
            double y184 = 0;
            double x284 = 0;
            double y284 = 0;

            double[] arr_Pel_XYZ1 = new double[GlobalVarLn.numberofdots * 3];
            double[] arr_Pel_XYZ2 = new double[GlobalVarLn.numberofdots * 3];
            //ClassMap classMap = new ClassMap();
            //ClassMap classMap1 = new ClassMap();

            double azgr1 = -1;
            double azgr2 = -1;
            PelIRI objPelIRI = new PelIRI();

            int flg = 0;
            int ind = 0;
            Color clr=Color.Red;
 */
            // ...............................................................

            //TCoordsIRI_PPRCh[] coordsIRI = new TCoordsIRI_PPRCh[4];

/*
            if (GlobalVarLn.shPelIRI == 0)
            {
                coordsIRI[0].iQ1 = 100;
                coordsIRI[0].iQ2 = -1;
                coordsIRI[0].dLatitude = 54.414;
                coordsIRI[0].dLongitude = 26.95;
                coordsIRI[0].iID = 1;
                coordsIRI[0].bDelIRI = 0;

                coordsIRI[1].iQ1 = 900;
                coordsIRI[1].iQ2 = 450;
                coordsIRI[1].dLatitude = 54.378;
                coordsIRI[1].dLongitude = 26.968;
                coordsIRI[1].iID = 2;
                coordsIRI[1].bDelIRI = 0;

                coordsIRI[2].iQ1 = -1;
                coordsIRI[2].iQ2 = -1;
                coordsIRI[2].dLatitude = 54.342;
                coordsIRI[2].dLongitude = 27.06;
                coordsIRI[2].iID = 3;
                coordsIRI[2].bDelIRI = 0;

                coordsIRI[3].iQ1 = 2900;
                coordsIRI[3].iQ2 = -1;
                coordsIRI[3].dLatitude = 54.377;
                coordsIRI[3].dLongitude = 26.877;
                coordsIRI[3].iID = 4;
                coordsIRI[3].bDelIRI = 0;

                GlobalVarLn.shPelIRI += 1;
            }
            else
                {
                    coordsIRI[0].iQ1 = 3600;
                    coordsIRI[0].iQ2 = -1;
                    coordsIRI[0].dLatitude = 54.414;
                    coordsIRI[0].dLongitude = 26.937;
                    coordsIRI[0].iID = 5;
                    coordsIRI[0].bDelIRI = 0;

                    coordsIRI[1].iQ1 = 450;
                    coordsIRI[1].iQ2 = -1;
                    coordsIRI[1].dLatitude = 54.412;
                    coordsIRI[1].dLongitude = 26.996;
                    coordsIRI[1].iID = 1;
                    coordsIRI[1].bDelIRI = 0;

                    coordsIRI[2].iQ1 = -1;
                    coordsIRI[2].iQ2 = -1;
                    coordsIRI[2].dLatitude = -1;
                    coordsIRI[2].dLongitude = -1;
                    coordsIRI[2].iID = 3;
                    coordsIRI[2].bDelIRI = 0;

                    coordsIRI[3].iQ1 = -1;
                    coordsIRI[3].iQ2 = -1;
                    coordsIRI[3].dLatitude = 54.377;
                    coordsIRI[3].dLongitude = 26.877;
                    coordsIRI[3].iID = 4;
                    coordsIRI[3].bDelIRI = 0;

                    GlobalVarLn.shPelIRI += 1;
                }
*/

/*

            if (GlobalVarLn.shPelIRI == 0)
            {
                coordsIRI[0].iQ1 = 100;
                coordsIRI[0].iQ2 = -1;
                coordsIRI[0].dLatitude = 54.414;
                coordsIRI[0].dLongitude = 26.95;
                coordsIRI[0].iID = 1;
                coordsIRI[0].bDelIRI = 0;

                coordsIRI[1].iQ1 = 900;
                coordsIRI[1].iQ2 = 450;
                coordsIRI[1].dLatitude = 54.378;
                coordsIRI[1].dLongitude = 26.968;
                coordsIRI[1].iID = 1;
                coordsIRI[1].bDelIRI = 0;

                coordsIRI[2].iQ1 = -1;
                coordsIRI[2].iQ2 = -1;
                coordsIRI[2].dLatitude = 54.342;
                coordsIRI[2].dLongitude = 27.06;
                coordsIRI[2].iID = 1;
                coordsIRI[2].bDelIRI = 0;

                coordsIRI[3].iQ1 = 2900;
                coordsIRI[3].iQ2 = -1;
                coordsIRI[3].dLatitude = 54.377;
                coordsIRI[3].dLongitude = 26.877;
                coordsIRI[3].iID = 1;
                coordsIRI[3].bDelIRI = 0;

                GlobalVarLn.shPelIRI += 1;
            }
            else
            {
                coordsIRI[0].iQ1 = 3600;
                coordsIRI[0].iQ2 = -1;
                coordsIRI[0].dLatitude = 54.414;
                coordsIRI[0].dLongitude = 26.937;
                coordsIRI[0].iID = 5;
                coordsIRI[0].bDelIRI = 0;

                coordsIRI[1].iQ1 = 3200;
                coordsIRI[1].iQ2 = -1;
                coordsIRI[1].dLatitude = 54.412;
                coordsIRI[1].dLongitude = 26.996;
                coordsIRI[1].iID = 1;
                coordsIRI[1].bDelIRI = 0;

                coordsIRI[2].iQ1 = -1;
                coordsIRI[2].iQ2 = -1;
                coordsIRI[2].dLatitude = -1;
                coordsIRI[2].dLongitude = -1;
                coordsIRI[2].iID = 1;
                coordsIRI[2].bDelIRI = 0;

                coordsIRI[3].iQ1 = -1;
                coordsIRI[3].iQ2 = -1;
                coordsIRI[3].dLatitude = -1;
                coordsIRI[3].dLongitude = -1;
                coordsIRI[3].iID = 1;
                coordsIRI[3].bDelIRI = 0;

                GlobalVarLn.shPelIRI += 1;
            }
*/

/*
            int AdDl = 0;

            AdDl = (int)coordsIRI[countPel].bDelIRI; // 0 - add, 1 - delete;

            // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
            // Delete All

            if (AdDl == 2)
            {

                for (int iii20 = (GlobalVarLn.list_PelIRI.Count - 1); iii20 >= 0; iii20--)
                {
                    GlobalVarLn.list_PelIRI.Remove(GlobalVarLn.list_PelIRI[iii20]);
                }

                axaxcMapScreen1.Repaint();
                return;

            }
            // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
            // Delete ID

            if (AdDl == 1)
            {

                for (int iii21 = (GlobalVarLn.list_PelIRI.Count - 1); iii21 >= 0; iii21--)
                {
                    if (GlobalVarLn.list_PelIRI[iii21].ID == coordsIRI[countPel].iID)
                        GlobalVarLn.list_PelIRI.Remove(GlobalVarLn.list_PelIRI[iii21]);
                }

                axaxcMapScreen1.Repaint();
                return;

            }
            // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,

                // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
                // EУдаляем все структуры с таким ID

                // FOR1
                for (countPel = 0; countPel < coordsIRI.Length; countPel++)
                {
                    // for2
                    for (int iii = (GlobalVarLn.list_PelIRI.Count - 1); iii >= 0; iii--)
                    {
                        if (GlobalVarLn.list_PelIRI[iii].ID == coordsIRI[countPel].iID)
                            GlobalVarLn.list_PelIRI.Remove(GlobalVarLn.list_PelIRI[iii]);

                    } //for2

                } //FOR1
                // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,

                // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
                // Добавляем новые

                // FOR1_1
                for (countPel = 0; countPel < coordsIRI.Length; countPel++)
                {

                    if (
                         (coordsIRI[countPel].iQ1 != -1) ||
                         ((coordsIRI[countPel].dLatitude != -1) && (coordsIRI[countPel].dLongitude != -1))
                       )
                    {
                        objPelIRI.ID = coordsIRI[countPel].iID;
                        objPelIRI.XGPS_IRI = -1;
                        objPelIRI.YGPS_IRI = -1;
                        objPelIRI.flPelMain2 = 0;
                        objPelIRI.arr_Pel1 = new double[10000];     // R,Широта,долгота
                        objPelIRI.arr_Pel2 = new double[10000];     // R,Широта,долгота

                        if (coordsIRI[countPel].iQ1 != -1)
                            objPelIRI.Pel1 = coordsIRI[countPel].iQ1;
                        else
                            objPelIRI.Pel1 = -1;

                        if (coordsIRI[countPel].iQ2 != -1)
                        {
                            objPelIRI.Pel2 = coordsIRI[countPel].iQ2;
                            objPelIRI.flPelMain2 = 1;
                        }
                        else
                        {
                            objPelIRI.Pel2 = -1;
                            objPelIRI.flPelMain2 = 0;
                        }

                        if ((coordsIRI[countPel].dLatitude != -1) && (coordsIRI[countPel].dLongitude != -1))
                        {
                            objPelIRI.Lat = coordsIRI[countPel].dLatitude;
                            objPelIRI.Long = coordsIRI[countPel].dLongitude;

                            //2501otl
                            //objPelIRI.color = ColorFromSharpString(coordsIRI[countPel].sColorIRI);
                            objPelIRI.color = clr;

                            objPelIRI.AddDel = (int)coordsIRI[countPel].bDelIRI; // 0 - add, 1 - delete;

                        }
                        else
                        {
                            objPelIRI.Lat = -1;
                            objPelIRI.Long = -1;
                            objPelIRI.XGPS_IRI = -1;
                            objPelIRI.YGPS_IRI = -1;
                            objPelIRI.AddDel = (int)coordsIRI[countPel].bDelIRI; // 0 - add, 1 - delete;

                        }

                        GlobalVarLn.list_PelIRI.Add(objPelIRI);

                    } //IF(есть пеленг или координаты) 


                } //FOR1_1
                // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,


                // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,

                for (int iii7 = (GlobalVarLn.list_PelIRI.Count - 1); iii7 >= 0; iii7--)
                {
                    if (
                        (GlobalVarLn.list_PelIRI[iii7].Pel1 == -1) &&
                        (GlobalVarLn.list_PelIRI[iii7].Pel2 == -1) &&
                        (GlobalVarLn.list_PelIRI[iii7].Lat == -1) &&
                        (GlobalVarLn.list_PelIRI[iii7].Long == -1)
                       )
                    {
                        GlobalVarLn.list_PelIRI.Remove(GlobalVarLn.list_PelIRI[iii7]);
                    }
                } //for5
                // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,


                // PELENG ***************************************************************
                if (GlobalVarLn.list1_Sost.Count != 0)
                {
                    GlobalVarLn.X1_PelMain = (int)GlobalVarLn.list1_Sost[0].X_m;  // SP1
                    GlobalVarLn.Y1_PelMain = (int)GlobalVarLn.list1_Sost[0].Y_m;
                    GlobalVarLn.X1_1_PelMain = (int)GlobalVarLn.list1_Sost[1].X_m; // SP2
                    GlobalVarLn.Y1_1_PelMain = (int)GlobalVarLn.list1_Sost[1].Y_m;

                    if (chbPeleng.Checked == true)
                    {

                        // FORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFOR
                        for (countPel = 0; countPel < coordsIRI.Length; countPel++)
                        {
                            if (coordsIRI[countPel].iQ1 != -1)
                            {
                                // for5
                                flg = 0;
                                for (int iii1 = 0; iii1 < GlobalVarLn.list_PelIRI.Count; iii1++)
                                {
                                    // Нашли этот элемент в List
                                    if (
                                        (GlobalVarLn.list_PelIRI[iii1].ID == coordsIRI[countPel].iID) &&
                                        (GlobalVarLn.list_PelIRI[iii1].Pel1 == (coordsIRI[countPel].iQ1)) &&
                                        (GlobalVarLn.list_PelIRI[iii1].Pel2 == (coordsIRI[countPel].iQ2))
                                       )
                                    {
                                        ind = iii1;
                                        flg = 1;
                                    }
                                } //for5


                                if (flg == 1)
                                {

                                    if (coordsIRI[countPel].iQ1 != -1)
                                    {
                                        azgr1 = coordsIRI[countPel].iQ1 / 10d; // Peleng1, grad
                                    }
                                    if (coordsIRI[countPel].iQ2 != -1)
                                    {
                                        azgr2 = coordsIRI[countPel].iQ2 / 10d; // Peleng2
                                    }


                                    x184 = GlobalVarLn.X1_PelMain;
                                    y184 = GlobalVarLn.Y1_PelMain;
                                    x284 = GlobalVarLn.X1_1_PelMain;
                                    y284 = GlobalVarLn.Y1_1_PelMain;

                                    mapPlaneToGeo(GlobalVarLn.hmapl, ref x184, ref y184);
                                    mapPlaneToGeo(GlobalVarLn.hmapl, ref x284, ref y284);

                                    // rad->grad
                                    x184 = (x184 * 180) / Math.PI;
                                    y184 = (y184 * 180) / Math.PI;
                                    x284 = (x284 * 180) / Math.PI;
                                    y284 = (y284 * 180) / Math.PI;


                                    ClassMap classMap = new ClassMap();
                                    ClassMap classMap1 = new ClassMap();

                                    PelIRI[] mass1 = GlobalVarLn.list_PelIRI.ToArray();
                                    // Peleng1
                                    classMap.f_Peleng(azgr1, GlobalVarLn.distance, GlobalVarLn.numberofdots, x184, y184,
                                                      ref a1, ref b1, ref c1, ref d1, ref f1, ref g1,
                                                      ref mass1[ind].arr_Pel1, ref arr_Pel_XYZ1);
                                    GlobalVarLn.list_PelIRI = mass1.ToList();


                                    if (GlobalVarLn.list_PelIRI[ind].flPelMain2 == 1)
                                    {
                                        PelIRI[] mass2 = GlobalVarLn.list_PelIRI.ToArray();
                                        classMap1.f_Peleng(azgr2, GlobalVarLn.distance, GlobalVarLn.numberofdots, x284, y284,
                                                           ref a2, ref b2, ref c2, ref d2, ref f2, ref g2,
                                                           ref mass2[ind].arr_Pel2, ref arr_Pel_XYZ2);
                                        GlobalVarLn.list_PelIRI = mass2.ToList();

                                    } // Peleng2



                                } //IF(flg==1)
                            } // IF(q1!=-1)
                        } // FOR
                        // FORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFOR


                    } // Chacked==true
                } // GlobalVarLn.list1_Sost.Count != 0

                // *************************************************************** PELENG

                // IRI *******************************************************************

                // FORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFOR
                for (countPel = 0; countPel < coordsIRI.Length; countPel++)
                {

                    if ((coordsIRI[countPel].dLatitude != -1) && (coordsIRI[countPel].dLongitude != -1))
                    {

                        // for6
                        flg = 0;
                        for (int iii2 = 0; iii2 < GlobalVarLn.list_PelIRI.Count; iii2++)
                        {
                            // Нашли этот элемент в List
                            if (
                                (GlobalVarLn.list_PelIRI[iii2].ID == coordsIRI[countPel].iID) &&
                                (GlobalVarLn.list_PelIRI[iii2].Lat == coordsIRI[countPel].dLatitude) &&
                                (GlobalVarLn.list_PelIRI[iii2].Long == coordsIRI[countPel].dLongitude)
                               )
                            {
                                ind = iii2;
                                flg = 1;
                            }
                        } //for6

                        if (flg == 1)
                        {
                            if (GlobalVarLn.list_PelIRI[ind].AddDel == 0)
                            {
                                ClassMap.f_GetGPS_IRI_2(ind);
                            }

                            else
                            {
                                //ClassMap.f_DelGPS_IRI_2(GlobalVarLn.list_PelIRI[ind].ID);
                            }


                        } //IF(flg==1)

                    } // Lat,Long!=-1 
                } // FOR
                // FORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFOR

                // ******************************************************************* IRI


                //?????????????????
                axaxcMapScreen1.Repaint();


         // ******************************************************************* PELENG_IRI_2_OTLADKA

*/
   


            /*
                        // Otl
                        // Ручная отладка удаления самолетов
 
                        int ii = 0;
                        int ii1 = 0;
                        int fi = 0;
                        String s1 = "";


                        //TDataADSBReceiver[] tDadaADSBReceiver=new TDataADSBReceiver[10];
                        // ---------------------------------------------------------------------------------------
                        // tDadaADSBReceiver!=null

                        if (tDadaADSBReceiver != null)
                        {

                            // ....................................................................................
                            // 1-й сеанс получения данных

                            if (GlobalVarLn.fl_AirPlane == 0)
                            {

                                tDadaADSBReceiver[0].sICAO = "111111";
                                tDadaADSBReceiver[0].sLatitude = "51,4";
                                tDadaADSBReceiver[0].sLongitude = "21,1";
                                tDadaADSBReceiver[1].sICAO = "222222";
                                tDadaADSBReceiver[1].sLatitude = "52";
                                tDadaADSBReceiver[1].sLongitude = "21,2";
                                tDadaADSBReceiver[2].sICAO = "333333";
                                tDadaADSBReceiver[2].sLatitude = "51,5";
                                tDadaADSBReceiver[2].sLongitude = "22,9";
                                tDadaADSBReceiver[3].sICAO = "111111";
                                tDadaADSBReceiver[3].sLatitude = "51,9";
                                tDadaADSBReceiver[3].sLongitude = "22,2";
                                tDadaADSBReceiver[4].sICAO = "444444";
                                tDadaADSBReceiver[4].sLatitude = "52";
                                tDadaADSBReceiver[4].sLongitude = "21,9";
                                tDadaADSBReceiver[5].sICAO = "111111";
                                tDadaADSBReceiver[5].sLatitude = "52,6";
                                tDadaADSBReceiver[5].sLongitude = "21,9";
                                tDadaADSBReceiver[6].sICAO = "555555";
                                tDadaADSBReceiver[6].sLatitude = "52";
                                tDadaADSBReceiver[6].sLongitude = "24";
                                tDadaADSBReceiver[7].sICAO = "555555";
                                tDadaADSBReceiver[7].sLatitude = "52,2";
                                tDadaADSBReceiver[7].sLongitude = "24,2";







                                GlobalVarLn.fl_AirPlane = 1;

                                if (chbair.Checked == true)
                                    GlobalVarLn.fl_AirPlaneVisible = 1;

                                //for (ii = 0; ii < tDadaADSBReceiver.Length; ii++)
                                for (ii = 0; ii < 8; ii++)

                                {
                                    // Добавляем 1-й цикл структур в лист

                                    s1 = String.Copy(tDadaADSBReceiver[ii].sLatitude);
                                    try
                                    {
                                        GlobalVarLn.Lat_air = Convert.ToDouble(s1);
                                    }
                                    catch (SystemException)
                                    {
                                        if (s1.IndexOf(",") > -1)
                                            s1 = s1.Replace(',', '.');
                                        else
                                            s1 = s1.Replace('.', ',');

                                        GlobalVarLn.Lat_air = Convert.ToDouble(s1);
                                    }

                                    s1 = String.Copy(tDadaADSBReceiver[ii].sLongitude);
                                    try
                                    {
                                        GlobalVarLn.Long_air = Convert.ToDouble(s1);
                                    }
                                    catch (SystemException)
                                    {
                                        if (s1.IndexOf(",") > -1)
                                            s1 = s1.Replace(',', '.');
                                        else
                                            s1 = s1.Replace('.', ',');
                                        GlobalVarLn.Long_air = Convert.ToDouble(s1);

                                    }

                                    GlobalVarLn.sNum_air = String.Copy(tDadaADSBReceiver[ii].sICAO);

                                    // 1-й самолет
                                    if (GlobalVarLn.list_air.Count == 0)
                                    {
                                        ClassMap.f_AddAirPlane1(
                                                               GlobalVarLn.Lat_air,
                                                               GlobalVarLn.Long_air,
                                                               GlobalVarLn.sNum_air,
                                                               0
                                                              );
                                    }
                                    else
                                    {
                                        ClassMap.f_AddAirPlane1(
                                                               GlobalVarLn.Lat_air,
                                                               GlobalVarLn.Long_air,
                                                               GlobalVarLn.sNum_air,
                                                               1
                                                              );
                                    }

                                } // FOR

                                double g=GlobalVarLn.list_air[0].Angle;


                                if (chbair.Checked == true)
                                {
                                    // Перерисовка самолетов
                                    ClassMap.f_ReDrawAirPlane1();
                                }

                            } // fl_AirPlane == 0
                            // ....................................................................................
                            // НЕ 1-й сеанс получения данных

                            else
                            {

                                tDadaADSBReceiver[0].sICAO = "222222";
                                tDadaADSBReceiver[0].sLatitude = "52";
                                tDadaADSBReceiver[0].sLongitude = "22,2";
                                tDadaADSBReceiver[1].sICAO = "333333";
                                tDadaADSBReceiver[1].sLatitude = "52,3";
                                tDadaADSBReceiver[1].sLongitude = "22,8";
                                tDadaADSBReceiver[2].sICAO = "444444";
                                tDadaADSBReceiver[2].sLatitude = "52,6";
                                tDadaADSBReceiver[2].sLongitude = "22";
                                tDadaADSBReceiver[3].sICAO = "555555";
                                tDadaADSBReceiver[3].sLatitude = "52,6";
                                tDadaADSBReceiver[3].sLongitude = "23,9";


                                // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                                // Проверить на исчезновение самолетов (Если в текущей партии какого-то самолета
                                // нет, значит он исчез)

                                // FOR1 (Свой List с конца)
                                for (ii = (GlobalVarLn.Number_air - 1); ii >= 0; ii--)
                                {

                                    fi = 0;
                                    // FOR2
                                    //for (ii1 = 0; ii1 < tDadaADSBReceiver.Length; ii1++)
                                    for (ii1 = 0; ii1 < 4; ii1++)

                                    {
                                        s1 = String.Copy(tDadaADSBReceiver[ii1].sICAO);
                                        if (String.Compare(GlobalVarLn.list_air[ii].sNum, s1) == 0)
                                            fi = 1;
                                    } // FOR2

                                    if (fi == 0)  // Этого самолета уже нет
                                        ClassMap.f_DelAirPlane(GlobalVarLn.list_air[ii].sNum);


                                } // FOR1
                                // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

                                //for (ii = 0; ii < tDadaADSBReceiver.Length; ii++)
                                for (ii = 0; ii < 4; ii++)

                                {
                                    // Добавляем цикл структур в лист
                                    s1 = String.Copy(tDadaADSBReceiver[ii].sLatitude);
                                    try
                                    {
                                        GlobalVarLn.Lat_air = Convert.ToDouble(s1);
                                    }
                                    catch (SystemException)
                                    {
                                        if (s1.IndexOf(",") > -1)
                                            s1 = s1.Replace(',', '.');
                                        else
                                            s1 = s1.Replace('.', ',');
                                        GlobalVarLn.Lat_air = Convert.ToDouble(s1);
                                    }

                                    s1 = String.Copy(tDadaADSBReceiver[ii].sLongitude);
                                    try
                                    {
                                        GlobalVarLn.Long_air = Convert.ToDouble(s1);
                                    }
                                    catch (SystemException)
                                    {
                                        if (s1.IndexOf(",") > -1)
                                            s1 = s1.Replace(',', '.');
                                        else
                                            s1 = s1.Replace('.', ',');
                                        GlobalVarLn.Long_air = Convert.ToDouble(s1);

                                    }

                                    GlobalVarLn.sNum_air = String.Copy(tDadaADSBReceiver[ii].sICAO);


                                    ClassMap.f_AddAirPlane1(
                                                           GlobalVarLn.Lat_air,
                                                           GlobalVarLn.Long_air,
                                                           GlobalVarLn.sNum_air,
                                                           1
                                                          );

                                } // FOR

                                if (chbair.Checked == true)
                                {
                                    // Перерисовка самолетов
                                    ClassMap.f_ReDrawAirPlane1();
                                }

                                tDadaADSBReceiver = null;

                            } // fl_AirPlane == 1
                            // ....................................................................................


                        } // tDadaADSBReceiver!=null
                        // ---------------------------------------------------------------------------------------
                        // tDadaADSBReceiver==null -> убрать все

                        else
                        {
                            ClassMap.f_DelAirPlaneAll();
                        }
                        // ---------------------------------------------------------------------------------------
            */
            ;

        } // Button11
        // *****************************************************************************************






        // GPS_SOST ***************************************************************
        // GPSSPPU
        // !!! Get to (WGS84)
        //777Main

        //void service_CurrentCoordsUpdated(object sender, TCoordsGNSS[] coordsGNSS, fl)
        public void service_CurrentCoordsUpdated(object sender, TCoordsGNSS[] coordsGNSS)
        {

            // NEW *******************************************************************

            // -----------------------------------------------------------------------
            // Нажата ли птичка на АРМ (посылка координат от GPS)

            TCoordsGNSS[] coordsGNSS_1 = new TCoordsGNSS[3];
                coordsGNSS_1[0].Lat = coordsGNSS[0].Lat;
                coordsGNSS_1[0].Lon = coordsGNSS[0].Lon;
            if (coordsGNSS.Length == 2)
            {
                coordsGNSS_1[1].Lat = coordsGNSS[1].Lat;
                coordsGNSS_1[1].Lon = coordsGNSS[1].Lon;
            }
            else if (coordsGNSS.Length == 3)
            {
                coordsGNSS_1[1].Lat = coordsGNSS[1].Lat;
                coordsGNSS_1[1].Lon = coordsGNSS[1].Lon;
                coordsGNSS_1[2].Lat = coordsGNSS[2].Lat;
                coordsGNSS_1[2].Lon = coordsGNSS[2].Lon;
            }

            // -----------------------------------------------------------------------
            // Пошли координаты -1

            LF objLF = new LF();
            int fsp1_1 = 0;
            int fsp2_1 = 0;
            int fpu_1 = 0;

            // ..................................................................
            // SP1 -> идут -1

            if (
               //(fl == true) &&
               (GlobalVarLn.bCheckGNSS) &&
               //777Otl
               ((coordsGNSS_1[0].Lat == -1) || (coordsGNSS_1[0].Lon == -1))
              )
            {
                // раньше были координаты (свои/GPS)
                if (
                   (GlobalVarLn.XCenterGRAD_Dubl_Sost != -1) &&
                   (GlobalVarLn.YCenterGRAD_Dubl_Sost != -1)
                   )
                {
                    GlobalVarLn.XCenterGRAD_Sost = -1;
                    GlobalVarLn.YCenterGRAD_Sost = -1;
                    GlobalVarLn.XCenterGRAD_Dubl_Sost = -1;
                    GlobalVarLn.YCenterGRAD_Dubl_Sost = -1;
                    GlobalVarLn.XCenter_Sost = 0;
                    GlobalVarLn.YCenter_Sost = 0;
                    GlobalVarLn.HCenter_Sost = 0;

                    objLF.X_m = 0;
                    objLF.Y_m = 0;
                    objLF.H_m = 0;

                    objLF.indzn = iniRW.get_ZnakSP1();
                    GlobalVarLn.indz1_Sost = objLF.indzn;
                    GlobalVarLn.objFormSostG.pbSP.Image = GlobalVarLn.objFormSostG.imageList1.Images[GlobalVarLn.indz1_Sost];

                    GlobalVarLn.list1_Sost[0] = objLF;

                    GlobalVarLn.flCoordSP_Sost = 0;
                    fsp1_1 = 1;

                    GlobalVarLn.objFormSostG.tbXRect.Text = "";
                    GlobalVarLn.objFormSostG.tbYRect.Text = "";
                    GlobalVarLn.objFormSostG.tbXRect42.Text = "";
                    GlobalVarLn.objFormSostG.tbYRect42.Text = "";
                    GlobalVarLn.objFormSostG.tbBRad.Text = "";
                    GlobalVarLn.objFormSostG.tbLRad.Text = "";
                    GlobalVarLn.objFormSostG.tbBMin1.Text = "";
                    GlobalVarLn.objFormSostG.tbLMin1.Text = "";
                    GlobalVarLn.objFormSostG.tbBDeg2.Text = "";
                    GlobalVarLn.objFormSostG.tbBMin2.Text = "";
                    GlobalVarLn.objFormSostG.tbBSec.Text = "";
                    GlobalVarLn.objFormSostG.tbLDeg2.Text = "";
                    GlobalVarLn.objFormSostG.tbLMin2.Text = "";
                    GlobalVarLn.objFormSostG.tbLSec.Text = "";

                    GlobalVarLn.objFormSostG.tbOwnHeight.Text = "";

                // --------------------------------------------------------------------------------------
                // 14_09_2018
                // Для пеленгов

                // Нажата птичка для отрисовки пеленгов
                    if (GlobalVarLn.flPelMain == 1)
                    {
                        // FRCH
                        GlobalVarLn.PrevP1 = -1;
                        GlobalVarLn.fl_PelIRI_1 = 0; // 1-й пеленг
                        GlobalVarLn.flag_eq_draw1 = 0;

                        //------------------------------------------------------------------------
                        //PPRCH
                        for (int iii = (GlobalVarLn.list_PelIRI.Count - 1); iii >= 0; iii--)
                        {
                            PelIRI[] mass = GlobalVarLn.list_PelIRI.ToArray();
                            mass[iii].Pel1 = -1;
                            GlobalVarLn.list_PelIRI = mass.ToList();

                            if (
                                (GlobalVarLn.list_PelIRI[iii].Pel1 == -1) &&
                                (GlobalVarLn.list_PelIRI[iii].Pel2 == -1) &&
                                (GlobalVarLn.list_PelIRI[iii].Lat == -1) &&
                                (GlobalVarLn.list_PelIRI[iii].Long == -1)
                               )
                            {
                                GlobalVarLn.list_PelIRI.Remove(GlobalVarLn.list_PelIRI[iii]);
                            }
                        }

                    }
                    //------------------------------------------------------------------------
                }

            }  // -1 SP1
            // ..................................................................
            // SP2

            if (
               (GlobalVarLn.bCheckGNSS) &&
               //777Otl
                ((coordsGNSS_1[1].Lat == -1) || (coordsGNSS_1[1].Lon == -1))
              )
            {
                // раньше были координаты (свои/GPS)
                if (
                   (GlobalVarLn.XPoint1GRAD_Dubl_Sost != -1) &&
                   (GlobalVarLn.YPoint1GRAD_Dubl_Sost != -1)
                   )
                {
                    GlobalVarLn.XPoint1GRAD_Sost = -1;
                    GlobalVarLn.YPoint1GRAD_Sost = -1;
                    GlobalVarLn.XPoint1GRAD_Dubl_Sost = -1;
                    GlobalVarLn.YPoint1GRAD_Dubl_Sost = -1;
                    GlobalVarLn.XPoint1_Sost = 0;
                    GlobalVarLn.YPoint1_Sost = 0;
                    GlobalVarLn.HPoint1_Sost = 0;

                    objLF.X_m = 0;
                    objLF.Y_m = 0;
                    objLF.H_m = 0;

                    // 13_09_2018
                    //objLF.indzn = 0;
                    GlobalVarLn.indz2_Sost = iniRW.get_ZnakSP2();
                    GlobalVarLn.objFormSostG.pictureBox1.Image = GlobalVarLn.objFormSostG.imageList2.Images[GlobalVarLn.indz2_Sost];
                    objLF.indzn = GlobalVarLn.indz2_Sost;

                    GlobalVarLn.list1_Sost[1] = objLF;

                    GlobalVarLn.flCoordYS1_Sost = 0;
                    fsp2_1 = 1;

                    GlobalVarLn.objFormSostG.tbPt1XRect.Text = "";
                    GlobalVarLn.objFormSostG.tbPt1YRect.Text = "";
                    GlobalVarLn.objFormSostG.tbPt1XRect42.Text = "";
                    GlobalVarLn.objFormSostG.tbPt1YRect42.Text = "";
                    GlobalVarLn.objFormSostG.tbPt1BRad.Text = "";
                    GlobalVarLn.objFormSostG.tbPt1LRad.Text = "";
                    GlobalVarLn.objFormSostG.tbPt1BMin1.Text = "";
                    GlobalVarLn.objFormSostG.tbPt1LMin1.Text = "";
                    GlobalVarLn.objFormSostG.tbPt1BDeg2.Text = "";
                    GlobalVarLn.objFormSostG.tbPt1BMin2.Text = "";
                    GlobalVarLn.objFormSostG.tbPt1BSec.Text = "";
                    GlobalVarLn.objFormSostG.tbPt1LDeg2.Text = "";
                    GlobalVarLn.objFormSostG.tbPt1LMin2.Text = "";
                    GlobalVarLn.objFormSostG.tbPt1LSec.Text = "";

                    GlobalVarLn.objFormSostG.tbPt1Height.Text = "";

                // --------------------------------------------------------------------------------------
                // 14_09_2018
                // Для пеленгов

                // Нажата птичка для отрисовки пеленгов
                if (GlobalVarLn.flPelMain == 1)
                {
                    // FRCH
                    GlobalVarLn.PrevP2 = -1;
                    GlobalVarLn.flPelMain2 = 0;   // 2-й пеленг
                    GlobalVarLn.flag_eq_draw2 = 0;
                    //------------------------------------------------------------------------
                    // PPRCH
                    for (int iii = (GlobalVarLn.list_PelIRI.Count - 1); iii >= 0; iii--)
                    {


                        PelIRI[] mass = GlobalVarLn.list_PelIRI.ToArray();
                        mass[iii].Pel2 = -1;
                        GlobalVarLn.list_PelIRI = mass.ToList();

                        if (
                            (GlobalVarLn.list_PelIRI[iii].Pel1 == -1) &&
                            (GlobalVarLn.list_PelIRI[iii].Pel2 == -1) &&
                            (GlobalVarLn.list_PelIRI[iii].Lat == -1) &&
                            (GlobalVarLn.list_PelIRI[iii].Long == -1)
                           )
                        {
                            GlobalVarLn.list_PelIRI.Remove(GlobalVarLn.list_PelIRI[iii]);
                        }
                    }
                    //------------------------------------------------------------------------

                    // Убрать с карты
                    //GlobalVarLn.axMapScreenGlobal.Repaint();

                } // Нажата птичка для отрисовки пеленгов
                // --------------------------------------------------------------------------------------


                }

            }  // -1 SP2
            // ..................................................................
            // PU


            if (
               //(fl == true) &&
               (GlobalVarLn.bCheckGNSS) &&
               //777Otl
                ((coordsGNSS_1[2].Lat == -1) || (coordsGNSS_1[2].Lon == -1))
              )
            {
                // раньше были координаты (свои/GPS)
                if (
                   (GlobalVarLn.XPoint2GRAD_Dubl_Sost != -1) &&
                   (GlobalVarLn.YPoint2GRAD_Dubl_Sost != -1)
                   )
                {
                    GlobalVarLn.XPoint2GRAD_Sost = -1;
                    GlobalVarLn.YPoint2GRAD_Sost = -1;
                    GlobalVarLn.XPoint2GRAD_Dubl_Sost = -1;
                    GlobalVarLn.YPoint2GRAD_Dubl_Sost = -1;
                    GlobalVarLn.XPoint2_Sost = 0;
                    GlobalVarLn.YPoint2_Sost = 0;
                    GlobalVarLn.HPoint2_Sost = 0;

                    objLF.X_m = 0;
                    objLF.Y_m = 0;
                    objLF.H_m = 0;

                    // 13_09_2018
                    //objLF.indzn = 0;
                    // !!! Значки читаем с INI файла
                    GlobalVarLn.indz3_Sost = iniRW.get_ZnakPU();
                    GlobalVarLn.objFormSostG.pictureBox2.Image = GlobalVarLn.objFormSostG.imageList3.Images[GlobalVarLn.indz3_Sost];
                    objLF.indzn = GlobalVarLn.indz3_Sost;

                    GlobalVarLn.list1_Sost[2] = objLF;

                    GlobalVarLn.flCoordYS2_Sost = 0;

                    fpu_1 = 1;
                    GlobalVarLn.objFormSostG.tbPt2XRect.Text = "";
                    GlobalVarLn.objFormSostG.tbPt2YRect.Text = "";
                    GlobalVarLn.objFormSostG.tbPt2XRect42.Text = "";
                    GlobalVarLn.objFormSostG.tbPt2YRect42.Text = "";
                    GlobalVarLn.objFormSostG.tbPt2BRad.Text = "";
                    GlobalVarLn.objFormSostG.tbPt2LRad.Text = "";
                    GlobalVarLn.objFormSostG.tbPt2BMin1.Text = "";
                    GlobalVarLn.objFormSostG.tbPt2LMin1.Text = "";
                    GlobalVarLn.objFormSostG.tbPt2BDeg2.Text = "";
                    GlobalVarLn.objFormSostG.tbPt2BMin2.Text = "";
                    GlobalVarLn.objFormSostG.tbPt2BSec.Text = "";
                    GlobalVarLn.objFormSostG.tbPt2LDeg2.Text = "";
                    GlobalVarLn.objFormSostG.tbPt2LMin2.Text = "";
                    GlobalVarLn.objFormSostG.tbPt2LSec.Text = "";
                    //GlobalVarLn.objFormSostG.tbOwnHeight.Text = "";
                    //GlobalVarLn.objFormSostG.tbPt1Height.Text = "";
                    GlobalVarLn.objFormSostG.tbPt2Height.Text = "";

                }

            }  // -1 PU
            // ..................................................................

            if ((fsp1_1 == 1) || (fsp2_1 == 1) || (fpu_1 == 1))
            {
                // Убрать с карты
                GlobalVarLn.axMapScreenGlobal.Repaint();
                // Redraw
                ClassMap.f_Map_Redraw_Sost();

            }
            // ..................................................................

            // ------------------------------------------------------------------------
            // Checked==true

            if (
                (GlobalVarLn.bCheckGNSS) &&
                (
                 //777Otl
                ((coordsGNSS_1[0].Lat != -1) && (coordsGNSS_1[0].Lon != -1))||
                ((coordsGNSS_1[1].Lat != -1) && (coordsGNSS_1[1].Lon != -1))||
                ((coordsGNSS_1[2].Lat != -1) && (coordsGNSS_1[2].Lon != -1))

                )
                )
            {
                // .......................................................................
                // 1-й раз

                if ((GlobalVarLn.fl_First_GPS == 0))
                {

                    GlobalVarLn.fl_First_GPS = 1;

                    // SP1
                    //777Otl
                    if ((coordsGNSS_1[0].Lat != -1) && (coordsGNSS_1[0].Lon != -1))

                    {
                        //777Otl
                        GlobalVarLn.XCenterGRAD_Sost = coordsGNSS_1[0].Lat;
                        GlobalVarLn.YCenterGRAD_Sost = coordsGNSS_1[0].Lon;

                        GlobalVarLn.XCenterGRAD_Dubl_Sost = GlobalVarLn.XCenterGRAD_Sost;
                        GlobalVarLn.YCenterGRAD_Dubl_Sost = GlobalVarLn.YCenterGRAD_Sost;
                    }

                    // SP2
                    //777Otl
                    //if ((GlobalVarLn.lll[1].Lat != -1) && (GlobalVarLn.lll[1].Long != -1))
                    if ((coordsGNSS_1[1].Lat != -1) && (coordsGNSS_1[1].Lon != -1))
                    {
                        GlobalVarLn.XPoint1GRAD_Sost = coordsGNSS_1[1].Lat;
                        GlobalVarLn.YPoint1GRAD_Sost = coordsGNSS_1[1].Lon;

                        GlobalVarLn.XPoint1GRAD_Dubl_Sost = GlobalVarLn.XPoint1GRAD_Sost;
                        GlobalVarLn.YPoint1GRAD_Dubl_Sost = GlobalVarLn.YPoint1GRAD_Sost;
                    }

                    // PU
                    //777Otl
                    if ((coordsGNSS_1[2].Lat != -1) && (coordsGNSS_1[2].Lon != -1))
                    {
                        //777Otl
                        GlobalVarLn.XPoint2GRAD_Sost = coordsGNSS_1[2].Lat;
                        GlobalVarLn.YPoint2GRAD_Sost = coordsGNSS_1[2].Lon;

                        GlobalVarLn.XPoint2GRAD_Dubl_Sost = GlobalVarLn.XPoint2GRAD_Sost;
                        GlobalVarLn.YPoint2GRAD_Dubl_Sost = GlobalVarLn.YPoint2GRAD_Sost;
                    }

                    ClassMap.f_GetGPS();

                } // 1-й раз
                // .......................................................................
                // НЕ 1-й раз
                // 2504

                else
                {

                    int fsp1 = 0;
                    int fsp2 = 0;
                    int fpu = 0;
                    double dds = 30; // m
                    double sss = 0;

                    // ''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    // SP1
                    if ((coordsGNSS_1[0].Lat != -1) && (coordsGNSS_1[0].Lon != -1))
                    {
                        sss = ClassMap.f_D_2Points(GlobalVarLn.XCenterGRAD_Dubl_Sost, GlobalVarLn.YCenterGRAD_Dubl_Sost, GlobalVarLn.XCenterGRAD_Sost, GlobalVarLn.YCenterGRAD_Sost, 1);

                        //Положение SP1 изменилось
                        if (
                            ((coordsGNSS_1[0].Lat != GlobalVarLn.XCenterGRAD_Dubl_Sost) ||
                            (coordsGNSS_1[0].Lon != GlobalVarLn.YCenterGRAD_Dubl_Sost))&&
                            (sss>dds)
                           ) 
                        //   //(GlobalVarLn.flTmrGPS == 1)
                        //  )
                        {
                            fsp1 = 1;

                            GlobalVarLn.XCenterGRAD_Sost = coordsGNSS_1[0].Lat;
                            GlobalVarLn.YCenterGRAD_Sost = coordsGNSS_1[0].Lon;

                            GlobalVarLn.XCenterGRAD_Dubl_Sost = GlobalVarLn.XCenterGRAD_Sost;
                            GlobalVarLn.YCenterGRAD_Dubl_Sost = GlobalVarLn.YCenterGRAD_Sost;

                        } // Положение SP1 изменилось

                    } // SP1
                    // ''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    // SP2

                    if ((coordsGNSS_1[1].Lat != -1) && (coordsGNSS_1[1].Lon != -1))

                    {
                        sss = ClassMap.f_D_2Points(GlobalVarLn.XPoint1GRAD_Dubl_Sost, GlobalVarLn.YPoint1GRAD_Dubl_Sost, GlobalVarLn.XPoint1GRAD_Sost, GlobalVarLn.YPoint1GRAD_Sost, 1);

                        //Положение SP2 изменилось
                        if (
                            ((coordsGNSS_1[1].Lat != GlobalVarLn.XPoint1GRAD_Dubl_Sost) ||
                            (coordsGNSS_1[1].Lon != GlobalVarLn.YPoint1GRAD_Dubl_Sost))&&
                            (sss>dds)
                           ) 
                       //    //(GlobalVarLn.flTmrGPS == 1)
                       //   )
                         
                        {
                            fsp2 = 1;

                            GlobalVarLn.XPoint1GRAD_Sost = coordsGNSS_1[1].Lat;
                            GlobalVarLn.YPoint1GRAD_Sost = coordsGNSS_1[1].Lon;

                            GlobalVarLn.XPoint1GRAD_Dubl_Sost = GlobalVarLn.XPoint1GRAD_Sost;
                            GlobalVarLn.YPoint1GRAD_Dubl_Sost = GlobalVarLn.YPoint1GRAD_Sost;

                        } // Положение SP2 изменилось

                    } // SP2
                    // ''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    // PU

                    if ((coordsGNSS_1[2].Lat != -1) && (coordsGNSS_1[2].Lon != -1))

                    {
                        sss = ClassMap.f_D_2Points(GlobalVarLn.XPoint2GRAD_Dubl_Sost, GlobalVarLn.YPoint2GRAD_Dubl_Sost, GlobalVarLn.XPoint2GRAD_Sost, GlobalVarLn.YPoint2GRAD_Sost, 1);

                        //Положение PU изменилось
                        if (
                            ((coordsGNSS_1[2].Lat != GlobalVarLn.XPoint2GRAD_Dubl_Sost) ||
                            (coordsGNSS_1[2].Lon != GlobalVarLn.YPoint2GRAD_Dubl_Sost))&&
                            (sss>dds) 
                           ) 
                        //   //(GlobalVarLn.flTmrGPS == 1)
                        //  )
                        {
                            fpu = 1;
                            GlobalVarLn.XPoint2GRAD_Sost = coordsGNSS_1[2].Lat;
                            GlobalVarLn.YPoint2GRAD_Sost = coordsGNSS_1[2].Lon;

                            GlobalVarLn.XPoint2GRAD_Dubl_Sost = GlobalVarLn.XPoint2GRAD_Sost;
                            GlobalVarLn.YPoint2GRAD_Dubl_Sost = GlobalVarLn.YPoint2GRAD_Sost;

                        } // Положение PU изменилось

                    } // PU
                    // ''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                    if ((fsp1 == 1) || (fsp2 == 1) || (fpu == 1))
                    {
                        ClassMap.f_GetGPS();
                    }


                } // НЕ 1-й раз
                // .......................................................................

            } // if (Checked == true&& Coord!=-1)
            // ------------------------------------------------------------------------

            // ******************************************************************* NEW



            ClassMap.f_LoadSostIni();



        }  // GetSostGPS

        //public void service_CurrentCoordsUpdated(object sender, TCoordsGNSS coordsGNSS)
        //{
        //    GlobalVarLn.XCenterGRAD_Sost = coordsGNSS.Lat;
        //    GlobalVarLn.YCenterGRAD_Sost = coordsGNSS.Lon;
        //    GlobalVarLn.XCenterGRAD_Dubl_Sost = GlobalVarLn.XCenterGRAD_Sost;
        //    GlobalVarLn.YCenterGRAD_Dubl_Sost = GlobalVarLn.YCenterGRAD_Sost;

        //    if (GlobalVarLn.flTmrGPS == 1)
        //        ClassMap.f_GetGPS();
        //} // GetSostGPS

        private void timer1_Tick(object sender, EventArgs e)
        {
            //777Otl -> !!!No delete->Otladka GPS_Sostav
/*
            AirPlane objs = new AirPlane();
            objs.Lat = GlobalVarLn.lll[0].Lat+0.0001; // SP1
            objs.Long = GlobalVarLn.lll[0].Long+0.0001;
            GlobalVarLn.lll[0] = objs;
 */
            Stopwatch sw = new Stopwatch();
            sw.Start();

            TCoordsIRI_PPRCh coordsIRI = new TCoordsIRI_PPRCh();
            List<TCoordsIRI_PPRCh> listCoordsIRI_PPRCh = new List<TCoordsIRI_PPRCh>();
            Color clr = Color.Red;

            if (GlobalVarLn.shPelIRI == 0)
            {
                coordsIRI.bDelIRI = 0;
                coordsIRI.iID = 1;
                coordsIRI.dLatitude = 56.649194;
                coordsIRI.dLongitude = 24.388778;
                listCoordsIRI_PPRCh.Add(coordsIRI);

                coordsIRI.bDelIRI = 0;
                coordsIRI.iID = 2;
                coordsIRI.dLatitude = 56.650417;
                coordsIRI.dLongitude = 24.454536;
                listCoordsIRI_PPRCh.Add(coordsIRI);

                coordsIRI.bDelIRI = 0;
                coordsIRI.iID = 3;
                coordsIRI.dLatitude = 56.613391;
                coordsIRI.dLongitude = 24.39187;
                listCoordsIRI_PPRCh.Add(coordsIRI);

                coordsIRI.bDelIRI = 0;
                coordsIRI.iID = 7;
                coordsIRI.dLatitude = 56.614923;
                coordsIRI.dLongitude = 24.457059;
                listCoordsIRI_PPRCh.Add(coordsIRI);

                GlobalVarLn.shPelIRI += 1;
            }

            else
            {
                for (int i = (listCoordsIRI_PPRCh.Count - 1); i >= 0; i--)
                {
                    listCoordsIRI_PPRCh.Remove(listCoordsIRI_PPRCh[i]);
                }


                coordsIRI.bDelIRI = 0;
                coordsIRI.iID = 4;
                coordsIRI.dLatitude = 56.652315;
                coordsIRI.dLongitude = 24.518753;
                listCoordsIRI_PPRCh.Add(coordsIRI);

                coordsIRI.bDelIRI = 0;
                coordsIRI.iID = 2;
                coordsIRI.dLatitude = 56.653399;
                coordsIRI.dLongitude = 24.58377;
                listCoordsIRI_PPRCh.Add(coordsIRI);

                coordsIRI.bDelIRI = 0;
                coordsIRI.iID = 5;
                coordsIRI.dLatitude = 56.617228;
                coordsIRI.dLongitude = 24.587116;
                listCoordsIRI_PPRCh.Add(coordsIRI);

                GlobalVarLn.shPelIRI -= 1;
            }


            // 2504
            double dds = 250; // m

            int AdDl = 0;
            int countPel = 0;
            int countPel1 = 0;

            // !!! Идут только пеленги (флпаг 3)/только ИРИ(флаг 0/1)/ удалить все (флаг 2)
            AdDl = (int)listCoordsIRI_PPRCh[0].bDelIRI;

            PelIRI objPelIRI2 = new PelIRI();  // For IRI

            // Delete All ************************************************************
            // Delete Peleng+IRI

            if (AdDl == 2)
            {

                // Del Peleng
                for (int iii20 = (GlobalVarLn.list_PelIRI.Count - 1); iii20 >= 0; iii20--)
                {
                    GlobalVarLn.list_PelIRI.Remove(GlobalVarLn.list_PelIRI[iii20]);
                }
                // DelIRI
                for (int iii21 = (GlobalVarLn.list_PelIRI2.Count - 1); iii21 >= 0; iii21--)
                {
                    GlobalVarLn.list_PelIRI2.Remove(GlobalVarLn.list_PelIRI2[iii21]);
                }

                axaxcMapScreen1.Repaint();
                return;

            }  // AdDl=2
            // ************************************************************ Delete All

            // Delete ID *************************************************************
            // Удалить конкретный ID

            if (AdDl == 1)
            {
                // Del_Struct_ID ..........................................................
                // удаляем все структуры с таким IDi 
                // Придет в 

                // FOR111
                for (countPel = 0; countPel < listCoordsIRI_PPRCh.Count; countPel++)
                {
                    int ffg1 = (int)listCoordsIRI_PPRCh[countPel].bDelIRI;

                    if (ffg1 == 1) // Del
                    {
                        // For22
                        for (int iii33 = (GlobalVarLn.list_PelIRI2.Count - 1); iii33 >= 0; iii33--)
                        {
                            if (GlobalVarLn.list_PelIRI2[iii33].ID == listCoordsIRI_PPRCh[countPel].iID)
                                GlobalVarLn.list_PelIRI2.Remove(GlobalVarLn.list_PelIRI2[iii33]);

                        } //for22

                    } // IF

                } // FOR111

                axaxcMapScreen1.Repaint();
                return;

            } // AdDl==1 Del
            // ************************************************************* Delete ID

            // Add IRI ****************************************************************

            if (AdDl == 0)
            {

                // ------------------------------------------------------------------------
                // 2504

                // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                bool SignRepaint = false;
                int i1 = 0;
                int i2 = 0;
                int i3 = 0;
                int i4 = 0;
                bool flexist = false;
                int ffg = 0;
                // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                // Убрать в старом те, которых нет в новых

                for (i1 = (GlobalVarLn.list_PelIRI2.Count - 1); i1 >= 0; i1--) // old
                {
                    flexist = false;
                    for (i2 = 0; i2 < listCoordsIRI_PPRCh.Count; i2++) // new
                    {
                        ffg = (int)listCoordsIRI_PPRCh[i2].bDelIRI;

                        if (
                            (GlobalVarLn.list_PelIRI2[i1].ID == listCoordsIRI_PPRCh[i2].iID) &&
                            (listCoordsIRI_PPRCh[i2].dLatitude != -1) &&
                            (listCoordsIRI_PPRCh[i2].dLongitude != -1) &&
                            (ffg == 0)
                           )
                        {
                            flexist = true;
                        }

                    } // i2

                    if (flexist == false)
                    {
                        GlobalVarLn.list_PelIRI2.Remove(GlobalVarLn.list_PelIRI2[i1]);
                        SignRepaint = true;
                        //i1 = -1;
                    }

                } // i1 old

                // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                // 

                // FOR1_5 вперед по новому
                for (countPel = 0; countPel < listCoordsIRI_PPRCh.Count; countPel++) // new
                {

                    ffg = (int)listCoordsIRI_PPRCh[countPel].bDelIRI;

                    if (
                         (listCoordsIRI_PPRCh[countPel].dLatitude != -1) &&
                         (listCoordsIRI_PPRCh[countPel].dLongitude != -1) &&
                         (ffg == 0)
                       )
                    {

                        flexist = false;
                        for (int iii99 = (GlobalVarLn.list_PelIRI2.Count - 1); iii99 >= 0; iii99--)
                        {

                            if (GlobalVarLn.list_PelIRI2[iii99].ID == listCoordsIRI_PPRCh[countPel].iID)
                            {
                                // есть с таким ID
                                flexist = true;
                                double sss = 0;
                                sss = ClassMap.f_D_2Points(GlobalVarLn.list_PelIRI2[iii99].Lat, GlobalVarLn.list_PelIRI2[iii99].Long, listCoordsIRI_PPRCh[countPel].dLatitude, listCoordsIRI_PPRCh[countPel].dLongitude, 1);
                                if (sss > dds)
                                {
                                    SignRepaint = true;
                                    // Убираем старый
                                    GlobalVarLn.list_PelIRI2.Remove(GlobalVarLn.list_PelIRI2[iii99]);

                                    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                                    // добавляем новый

                                    objPelIRI2.ID = listCoordsIRI_PPRCh[countPel].iID;
                                    objPelIRI2.XGPS_IRI = -1;
                                    objPelIRI2.YGPS_IRI = -1;
                                    objPelIRI2.flPelMain2 = 0;
                                    objPelIRI2.arr_Pel1 = new double[10000];     // R,Широта,долгота
                                    objPelIRI2.arr_Pel2 = new double[10000];     // R,Широта,долгота
                                    objPelIRI2.Pel1 = -1;
                                    objPelIRI2.Pel2 = -1;

                                    if ((listCoordsIRI_PPRCh[countPel].dLatitude != -1) && (listCoordsIRI_PPRCh[countPel].dLongitude != -1))
                                    {
                                        objPelIRI2.Lat = listCoordsIRI_PPRCh[countPel].dLatitude;
                                        objPelIRI2.Long = listCoordsIRI_PPRCh[countPel].dLongitude;
                                        objPelIRI2.color = clr;
                                        objPelIRI2.AddDel = (int)listCoordsIRI_PPRCh[countPel].bDelIRI; // 0 - add, 1 - delete;
                                    }
                                    else
                                    {
                                        objPelIRI2.Lat = -1;
                                        objPelIRI2.Long = -1;
                                        objPelIRI2.XGPS_IRI = -1;
                                        objPelIRI2.YGPS_IRI = -1;
                                        objPelIRI2.AddDel = (int)listCoordsIRI_PPRCh[countPel].bDelIRI; // 0 - add, 1 - delete;

                                    }

                                    GlobalVarLn.list_PelIRI2.Add(objPelIRI2);

                                    ClassMap.f_GetGPS_IRI_2(GlobalVarLn.list_PelIRI2.Count - 1);

                                    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

                                } // S>ds

                            }  // ID==

                        }  // for old назад

                        if (flexist == false) // это новый ID -> добавляем его
                        {
                            SignRepaint = true;

                            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                            // добавляем новый

                            objPelIRI2.ID = listCoordsIRI_PPRCh[countPel].iID;
                            objPelIRI2.XGPS_IRI = -1;
                            objPelIRI2.YGPS_IRI = -1;
                            objPelIRI2.flPelMain2 = 0;
                            objPelIRI2.arr_Pel1 = new double[10000];     // R,Широта,долгота
                            objPelIRI2.arr_Pel2 = new double[10000];     // R,Широта,долгота
                            objPelIRI2.Pel1 = -1;
                            objPelIRI2.Pel2 = -1;

                            if ((listCoordsIRI_PPRCh[countPel].dLatitude != -1) && (listCoordsIRI_PPRCh[countPel].dLongitude != -1))
                            {
                                objPelIRI2.Lat = listCoordsIRI_PPRCh[countPel].dLatitude;
                                objPelIRI2.Long = listCoordsIRI_PPRCh[countPel].dLongitude;
                                objPelIRI2.color = clr;
                                objPelIRI2.AddDel = (int)listCoordsIRI_PPRCh[countPel].bDelIRI; // 0 - add, 1 - delete;
                            }
                            else
                            {
                                objPelIRI2.Lat = -1;
                                objPelIRI2.Long = -1;
                                objPelIRI2.XGPS_IRI = -1;
                                objPelIRI2.YGPS_IRI = -1;
                                objPelIRI2.AddDel = (int)listCoordsIRI_PPRCh[countPel].bDelIRI; // 0 - add, 1 - delete;

                            }

                            GlobalVarLn.list_PelIRI2.Add(objPelIRI2);

                            ClassMap.f_GetGPS_IRI_2(GlobalVarLn.list_PelIRI2.Count - 1);

                            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

                        } // // это новый ID

                    } //IF(есть координаты) 

                } //FOR1_5 new
                // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

                if (SignRepaint == true)
                    axaxcMapScreen1.Repaint();


            } // AdDl==0
            // **************************************************************** Add IRI


            int yyy = 0;
            yyy = yyy;


            sw.Stop();
            // Get the elapsed time as a TimeSpan value.
            TimeSpan ts = sw.Elapsed;

            // Format and display the TimeSpan value.
            string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:000}",
                ts.Hours, ts.Minutes, ts.Seconds, ts.Milliseconds);
            //MessageBox.Show("RunTime " + elapsedTime);
            //Console.WriteLine("RunTime " + elapsedTime);

        }

        // *************************************************************** GPS_SOST

        // GPS_PELENG *************************************************************

        private void fpell(double az1, int x11, int y11, ref int x21, ref int y21)
        {
            double x1=0;
            double y1 = 0;
            double x2 = 0;
            double y2 = 0;

            x1 = (double)x11;
            y1 = (double)y11;
            x2 = (double)x21;
            y2 = (double)y21;

            double az = 0;
            // l = Дальность отрисовки пеленга в метрах
            //double l = 15000;
            double l = 1000000;

            az=(az1*Math.PI)/180;

            if ((az >= 0) && (az <= Math.PI/2))
            {
                //x2 = x1 - l * Math.Cos(az);
                x2 = x1 + l * Math.Cos(az);
                y2 = y1 + l * Math.Sin(az);
            }
            else if ((az > Math.PI/2) && (az <= Math.PI))
            {
                //x2 = x1 + l * Math.Sin(az-Math.PI/2);
                x2 = x1 - l * Math.Sin(az - Math.PI / 2);
                y2 = y1 + l * Math.Cos(az-Math.PI/2);
            }
            else if ((az > Math.PI) && (az <= (3*Math.PI/2)))
            {
                //x2 = x1 + l * Math.Cos(az - Math.PI );
                x2 = x1 - l * Math.Cos(az - Math.PI);
                y2 = y1 - l * Math.Sin(az - Math.PI);
            }
            else if ((az > (3 * Math.PI / 2)) && (az <= (2 * Math.PI)))
            {
                //x2 = x1 - l * Math.Sin(az - (3 * Math.PI / 2));
                x2 = x1 + l * Math.Sin(az - (3 * Math.PI / 2));
                y2 = y1 - l * Math.Cos(az - (3 * Math.PI / 2));
            }
            else
            {   //Azb
                if (GlobalVarLn.fl_Azb == 0)
                {
                    MessageBox.Show("Недопустимое значение азимута");
                }
                else
                {
                    MessageBox.Show("Yolverilməz azimut göstəriciləri");
                }
                return;
            }
            
            y21 = (int)y2;
            x21 = (int)x2;
       

        }// fpel

        // ******************************************************************************************
        private void f_Map_Line_Pl(
                                  double X111,
                                  double Y111,
                                  double X222,
                                  double Y222

                                 )
        {


            // -------------------------------------------------------------------------------------
            Graphics graph = axaxcMapScreen1.CreateGraphics();

            Pen penRed2 = new Pen(Color.Red, 2);
            Brush brushRed2 = new SolidBrush(Color.Red);
            // -------------------------------------------------------------------------------------
            // Расстояние в м на карте -> пикселы на изображении
            mapPlaneToPicture(GlobalVarLn.hmapl, ref X111, ref Y111);
            mapPlaneToPicture(GlobalVarLn.hmapl, ref X222, ref Y222);
            // -------------------------------------------------------------------------------------
            if (graph != null)
            {

                Point point1 = new Point((int)X111 - axaxcMapScreen1.MapLeft, (int)Y111 - axaxcMapScreen1.MapTop);
                Point point2 = new Point((int)X222 - axaxcMapScreen1.MapLeft, (int)Y222 - axaxcMapScreen1.MapTop);

                graph.DrawLine(penRed2, point1, point2);
            }
            // -------------------------------------------------------------------------------------

        }

        private void f_RedrawPelMain()
        {
            double xx1 = 0;
            double yy1 = 0;
            double xx2 = 0;
            double yy2 = 0;

            xx1 = (double)GlobalVarLn.X1_PelMain;
            yy1 = (double)GlobalVarLn.Y1_PelMain;
            xx2 = (double)GlobalVarLn.X2_PelMain;
            yy2 = (double)GlobalVarLn.Y2_PelMain;
            f_Map_Line_Pl(xx1, yy1, xx2, yy2);

            if (GlobalVarLn.flPelMain2 == 1)
            {
                xx1 = (double)GlobalVarLn.X1_1_PelMain;
                yy1 = (double)GlobalVarLn.Y1_1_PelMain;
                xx2 = (double)GlobalVarLn.X2_1_PelMain;
                yy2 = (double)GlobalVarLn.Y2_1_PelMain;
                f_Map_Line_Pl(xx1, yy1, xx2, yy2);
            }
        }


        // ************************************************************* GPS_PELENG


        // AirPlane ***************************************************************
        // Lena 

        private void Service_AirPlaneReceived(object sender, TDataADSBReceiver[] tDadaADSBReceiver)
        {
          int ii=0;
          int ii1 = 0;
          int fi = 0;
          String s1 = "";

        // ---------------------------------------------------------------------------------------
          // tDadaADSBReceiver!=null

          if (tDadaADSBReceiver != null)
          {

              // ....................................................................................
              // 1-й сеанс получения данных

              if (GlobalVarLn.fl_AirPlane == 0)
              {
                  GlobalVarLn.fl_AirPlane = 1;

                  if (chbair.Checked == true)
                      GlobalVarLn.fl_AirPlaneVisible = 1;

                  for (ii = 0; ii < tDadaADSBReceiver.Length; ii++)
                  {
                      // Добавляем 1-й цикл структур в лист

                      s1 = String.Copy(tDadaADSBReceiver[ii].sLatitude);
                      try
                      {
                          GlobalVarLn.Lat_air = Convert.ToDouble(s1);
                      }
                      catch (SystemException)
                      {
                          if (s1.IndexOf(",") > -1)
                              s1 = s1.Replace(',', '.');
                          else
                              s1 = s1.Replace('.', ',');

                          GlobalVarLn.Lat_air = Convert.ToDouble(s1);
                      }

                      s1 = String.Copy(tDadaADSBReceiver[ii].sLongitude);
                      try
                      {
                          GlobalVarLn.Long_air = Convert.ToDouble(s1);
                      }
                      catch (SystemException)
                      {
                          if (s1.IndexOf(",") > -1)
                              s1 = s1.Replace(',', '.');
                          else
                              s1 = s1.Replace('.', ',');
                          GlobalVarLn.Long_air = Convert.ToDouble(s1);

                      }

                      GlobalVarLn.sNum_air = String.Copy(tDadaADSBReceiver[ii].sICAO);

                      ClassMap.f_AddAirPlane1(
                                             GlobalVarLn.Lat_air,
                                             GlobalVarLn.Long_air,
                                             GlobalVarLn.sNum_air,
                                             0
                                            );

                  } // FOR

                  if (chbair.Checked == true)
                  {
                      // Перерисовка самолетов
                      ClassMap.f_ReDrawAirPlane1();
                  }

              } // fl_AirPlane == 0
              // ....................................................................................
              // НЕ 1-й сеанс получения данных

              else
              {
                  // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                  // Проверить на исчезновение самолетов (Если в текущей партии какого-то самолета
                  // нет, значит он исчез)

                  // FOR1 (Свой List с конца)
                  for (ii = (GlobalVarLn.Number_air - 1); ii >= 0; ii--)
                  {

                      fi = 0;
                      // FOR2
                      for (ii1 = 0; ii1 < tDadaADSBReceiver.Length; ii1++)
                      {
                         s1 = String.Copy(tDadaADSBReceiver[ii1].sICAO);
                         if (String.Compare(GlobalVarLn.list_air[ii].sNum, s1) == 0)
                             fi = 1;  
                      } // FOR2

                      if (fi == 0)  // Этого самолета уже нет
                          ClassMap.f_DelAirPlane(GlobalVarLn.list_air[ii].sNum);


                  } // FOR1
                  // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>


                  for (ii = 0; ii < tDadaADSBReceiver.Length; ii++)
                  {
                      // Добавляем цикл структур в лист
                      s1 = String.Copy(tDadaADSBReceiver[ii].sLatitude);
                      try
                      {
                          GlobalVarLn.Lat_air = Convert.ToDouble(s1);
                      }
                      catch (SystemException)
                      {
                          if (s1.IndexOf(",") > -1)
                              s1 = s1.Replace(',', '.');
                          else
                              s1 = s1.Replace('.', ',');
                          GlobalVarLn.Lat_air = Convert.ToDouble(s1);
                      }

                      s1 = String.Copy(tDadaADSBReceiver[ii].sLongitude);
                      try
                      {
                          GlobalVarLn.Long_air = Convert.ToDouble(s1);
                      }
                      catch (SystemException)
                      {
                          if (s1.IndexOf(",") > -1)
                              s1 = s1.Replace(',', '.');
                          else
                              s1 = s1.Replace('.', ',');
                          GlobalVarLn.Long_air = Convert.ToDouble(s1);

                      }

                      GlobalVarLn.sNum_air = String.Copy(tDadaADSBReceiver[ii].sICAO);


                      ClassMap.f_AddAirPlane1(
                                             GlobalVarLn.Lat_air,
                                             GlobalVarLn.Long_air,
                                             GlobalVarLn.sNum_air,
                                             1
                                            );

                  } // FOR

                  if (chbair.Checked == true)
                  {
                      // Перерисовка самолетов
                      ClassMap.f_ReDrawAirPlane1();
                  }

              } // fl_AirPlane == 1
              // ....................................................................................


          } // tDadaADSBReceiver!=null
          // ---------------------------------------------------------------------------------------
          // tDadaADSBReceiver==null -> убрать все

          else
          {
            ClassMap.f_DelAirPlaneAll();
          }
       // ---------------------------------------------------------------------------------------

        } // AirPlane
        // ************************************************************************************ AirPlane

        // MOUSE_WHEEL ******************************************************************
        //Обработка колёсика
        // ******************************************************************************
        void MapForm_MouseWheel(object sender, MouseEventArgs e)
        {
            if (MapIsOpenned == true)
            {

                Rectangle rp = axaxcMapScreen1.Bounds;
                Rectangle rp2 = panel2.Bounds;

                if ((e.X >= rp.Left && e.X <= rp.Right && e.Y >= rp.Top && e.Y <= rp.Bottom) && (e.X >= rp2.Left && e.X <= rp2.Right && e.Y >= rp2.Top && e.Y <= rp2.Bottom))
                {
                    if (e.Delta != 0)
                    {

                        //Sect
                       int fff = 0;

                        if (e.Delta > 0)
                        {
                        // ........................................................
                        //Sect

                        if ((GlobalVarLn.flsect == 1) && (chbSect.Checked == true))
                        {
                          fff = 1;
                          GlobalVarLn.flsect = 0;
                         }
                        // ........................................................

                            MapCore.ZoomInFunc(ref axaxcMapScreen1);
                        }
                        if (e.Delta < 0)
                        {
                        // ........................................................
                        //Sect

                        if ((GlobalVarLn.flsect == 1) && (chbSect.Checked == true))
                        {
                          fff = 1;
                          GlobalVarLn.flsect = 0;
                         }
                        // ........................................................

                            MapCore.ZoomOutFunc(ref axaxcMapScreen1);
                        }

                       // ........................................................
                       //Sect 

                       if (fff == 1)
                       {
                        GlobalVarLn.flsect = 1;
                        ClassMap.Sector((double)GlobalVarLn.XCenter_Sost, (double)GlobalVarLn.YCenter_Sost);
                       }
                      // ..........................................................

                    }
                }

            } // MapIsOpenned == true
        }

        // ****************************************************************** MOUSE_WHEEL

        // ******************************************************************************
        //Закрытие формы
        // ******************************************************************************

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MapIsOpenned == true)
            {
                MapCore.closeMatrixFunc(ref HeightMatrixIsOpenned, ref axaxcMapScreen1);
                MapCore.closeMapFunc(ref MapIsOpenned, ref axaxcMapScreen1, Application.StartupPath + "\\Init.ini");
                SendMapIsOpenToAPM(MapIsOpenned); //обратный контракт
            }
        } // Close
        // ******************************************************************************

        // ******************************************************************************
        // Menu: открыть карту
        // ******************************************************************************

        private void openMapToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MapIsOpenned == false)
            {
                //MapCore.openMapFunc(ref MapIsOpenned, ref axaxcMapScreen1, ref axaxOpenMapDialog1, Application.StartupPath + "\\Init.ini");
                MapCore.openMapFunc(ref MapIsOpenned, ref axaxcMapScreen1, ref openFileDialog1, Application.StartupPath + "\\Init.ini");

                // Lena
                GlobalVarLn.hmapl = (int)axaxcMapScreen1.MapHandle;
                hmapl1 = (int)axaxcMapScreen1.MapHandle;

                SendMapIsOpenToAPM(MapIsOpenned); //обратный контракт
            }
            //Azb
            else
            {
                if (GlobalVarLn.fl_Azb == 0)
                {
                    MessageBox.Show("Карта уже открыта");
                }
                else
                {
                    MessageBox.Show("Xəritə açıqdır");
                }
            }

        } // Menu: открыть карту
        // ******************************************************************************

        // ******************************************************************************
        // Menu: закрыть карту
        // ******************************************************************************

        private void closeMapToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MapIsOpenned == true)
            {
                MapCore.closeMapFunc(ref MapIsOpenned, ref axaxcMapScreen1, Application.StartupPath + "\\Init.ini");
                SendMapIsOpenToAPM(MapIsOpenned); //обратный контракт
            }
        } // Menu: close map
        // ******************************************************************************

        // ******************************************************************************
        // Menu: открыть матрицу высот
        // ******************************************************************************

        private void openHeightMatrixToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MapIsOpenned == true)
            {
                if (HeightMatrixIsOpenned == false)
                {
                    MapCore.openMatrixFunc(ref HeightMatrixIsOpenned, ref axaxcMapScreen1, ref openFileDialog1);
                }
                else
                {
                    if (GlobalVarLn.fl_Azb == 0)
                    {
                        //Azb
                        MessageBox.Show("Матрица высот уже открыта");
                    }
                    else
                    {
                        MessageBox.Show("Hündürlük matrisası açıqdır");
                    }
                }
            }
            else
            {   
                //Azb
                if (GlobalVarLn.fl_Azb == 0)
                {
                    MessageBox.Show("Карта не открыта");
                }
                else
                {
                    MessageBox.Show("Xəritə açıq deyil");
                }

            }
        }
        // ******************************************************************************

        // ******************************************************************************
        // Menu:закрыть матрицу высот
        // ******************************************************************************

        private void closeHeightMatrixToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MapCore.closeMatrixFunc(ref HeightMatrixIsOpenned, ref axaxcMapScreen1);
        }
        // ******************************************************************************
        // увеличить масштаб
        // ******************************************************************************

        private void ZoomIn_Click(object sender, EventArgs e)
        {
            // ..........................................................................
            //Sect увеличить масштаб

            int fff = 0;
            if ((GlobalVarLn.flsect == 1) && (chbSect.Checked == true))
            {
                fff = 1;
                GlobalVarLn.flsect = 0;
            }
            // ..........................................................................

            MapCore.ZoomInFunc(ref axaxcMapScreen1);
            label5.Text = string.Format("М 1: {0}", Convert.ToString((int)axaxcMapScreen1.ViewScale));

            // ..........................................................................
            //Sect увеличить масштаб

            if (fff == 1)
            {
                GlobalVarLn.flsect = 1;
                ClassMap.Sector((double)GlobalVarLn.XCenter_Sost, (double)GlobalVarLn.YCenter_Sost);
            }
            // ..........................................................................

        }
        // ******************************************************************************

        // ******************************************************************************
        // уменьшить масштаб
        // ******************************************************************************

        private void ZoomOut_Click(object sender, EventArgs e)
        {
            // ..........................................................................
            //Sect уменьшить масштаб

            int fff = 0;
            if ((GlobalVarLn.flsect == 1) && (chbSect.Checked == true))
            {
                fff = 1;
                GlobalVarLn.flsect = 0;
            }
            // ..........................................................................

            MapCore.ZoomOutFunc(ref axaxcMapScreen1);
            label5.Text = string.Format("М 1: {0}", Convert.ToString((int)axaxcMapScreen1.ViewScale));

            // ..........................................................................
            //Sect уменьшить масштаб

            if (fff == 1)
            {
                GlobalVarLn.flsect = 1;
                ClassMap.Sector((double)GlobalVarLn.XCenter_Sost, (double)GlobalVarLn.YCenter_Sost);
            }
            // ..........................................................................

        }
        // ******************************************************************************
        // исходный масштаб
        // ******************************************************************************

        private void ZoomInitial_Click(object sender, EventArgs e)
        {
            // ............................................................................
            //Sect исходный масштаб

            int fff = 0;
            if ((GlobalVarLn.flsect == 1) && (chbSect.Checked == true))
            {
                fff = 1;
                GlobalVarLn.flsect = 0;
                if (axaxcMapScreen1.ViewScale == 500000)
                    axaxcMapScreen1.Repaint();

            }
            // ............................................................................

            //MapCore.ZoomInitialFunc(ref axaxcMapScreen1);

            axaxcMapScreen1.ViewScale = 500000;
            label5.Text = string.Format("М 1: {0}", Convert.ToString((int)axaxcMapScreen1.ViewScale));

            // ............................................................................
            //Sect исходный масштаб

            if (fff == 1)
            {
                GlobalVarLn.flsect = 1;
                ClassMap.Sector((double)GlobalVarLn.XCenter_Sost, (double)GlobalVarLn.YCenter_Sost);
            }
            // ............................................................................

        }
        // ******************************************************************************

        // ******************************************************************************
        //Sect
        // Otladka

        //Форма по кнопке 1
        public CheckPointForm checkPointForm;
        private void button1_Click(object sender, EventArgs e)
        {

/*
            if (checkPointForm == null || checkPointForm.IsDisposed)
            {
                checkPointForm = new CheckPointForm(this, axaxcMapScreen1);
                checkPointForm.Show();
            }
*/

            if (chbSect.Checked == true)
            {
                 GlobalVarLn.luch1 = 0; // grad
                 GlobalVarLn.luch2 = 10;
                 GlobalVarLn.luch3 = 90;
                 GlobalVarLn.luch4 = 180;
                 GlobalVarLn.luch5 = 300;

                GlobalVarLn.flsect = 1;
                GlobalVarLn.fllSect1 = 1;
                GlobalVarLn.fllSect2 = 1;
                GlobalVarLn.fllSect3 = 1;
                GlobalVarLn.fllSect4 = 1;
                GlobalVarLn.fllSect5 = 1;

                // .....................................................................
                if (GlobalVarLn.f_luch == 0)
                {
                    GlobalVarLn.f_luch = 1;
                    GlobalVarLn.luch1_dubl = GlobalVarLn.luch1;
                    GlobalVarLn.luch2_dubl = GlobalVarLn.luch2;
                    GlobalVarLn.luch3_dubl = GlobalVarLn.luch3;
                    GlobalVarLn.luch4_dubl = GlobalVarLn.luch4;
                    GlobalVarLn.luch5_dubl = GlobalVarLn.luch5;

                    GlobalVarLn.flsect = 0;
                    // Убрать с карты
                    GlobalVarLn.axMapScreenGlobal.Repaint();

                    GlobalVarLn.flsect = 1;

                    ClassMap.Sector(
                                    (double)GlobalVarLn.XCenter_Sost,
                                    (double)GlobalVarLn.YCenter_Sost
                                  );
                }
                // .....................................................................
                else
                {
                    if (
                        (GlobalVarLn.luch1 == GlobalVarLn.luch1_dubl) &&
                        (GlobalVarLn.luch2 == GlobalVarLn.luch2_dubl) &&
                        (GlobalVarLn.luch3 == GlobalVarLn.luch3_dubl) &&
                        (GlobalVarLn.luch4 == GlobalVarLn.luch4_dubl) &&
                        (GlobalVarLn.luch5 == GlobalVarLn.luch5_dubl)
                      )
                    {
                        return;
                    }

                    else
                    {
                        GlobalVarLn.luch1_dubl = GlobalVarLn.luch1;
                        GlobalVarLn.luch2_dubl = GlobalVarLn.luch2;
                        GlobalVarLn.luch3_dubl = GlobalVarLn.luch3;
                        GlobalVarLn.luch4_dubl = GlobalVarLn.luch4;
                        GlobalVarLn.luch5_dubl = GlobalVarLn.luch5;

                        GlobalVarLn.flsect = 0;
                        // Убрать с карты
                        GlobalVarLn.axMapScreenGlobal.Repaint();

                        GlobalVarLn.flsect = 1;

                        ClassMap.Sector(
                                        (double)GlobalVarLn.XCenter_Sost,
                                        (double)GlobalVarLn.YCenter_Sost
                                      );

                    }

                }
                // .....................................................................


            } // chbSect.Checked == true


        } // P/P
        // ******************************************************************************

        //Клик правой кнопкой мыши
        private double righteX;
        private double righteY;

        //Клик левой кнопкой мыши и премещение
        private bool waspressleft = false;
        private double startlefteX;
        private double startlefteY;
        private double movelefteX;
        private double movelefteY;


// MOUSE_DOWN **************************************************************************************************
//Обработка мыши на карте

        private void axaxcMapScreen1_OnMapMouseDown(object sender, AxaxGisToolKit.IaxMapScreenEvents_OnMapMouseDownEvent e)
        {

            // 1210
            GlobalVarLn.MousePress = true;

            // SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
            // Для маршрута

            // -----------------------------------------------------------------------------------
            // Lena

            ClassMap.f_XYMap(e.x, e.y);

            // Маршрут
            if (
                (GlobalVarLn.blWay_stat == true)&&
                (GlobalVarLn.f_Open_objFormWay==1)
               )
            {
                objFormWay.f_Way(
                               GlobalVarLn.MapX1,
                               GlobalVarLn.MapY1
                              );
            }
            // -----------------------------------------------------------------------------------

            // SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS

// Sostav *****************************************************************************************
/*
            if (GlobalVarLn.blWay_stat == true)
            {
                objFormWay.f_Way(
                               GlobalVarLn.MapX1,
                               GlobalVarLn.MapY1
                              );
            }
            // -----------------------------------------------------------------------------------
*/

// ***************************************************************************************** Sostav

            if (e.button == 0)
            {
                waspressleft = true;
                startlefteX = e.x;
                startlefteY = e.y;

                MapCore.mapPlaneToPicture((int)axaxcMapScreen1.MapHandle, ref startlefteX, ref startlefteY);

            }
            if (e.button == 1)
            {
                righteX = e.x;
                righteY = e.y;
            }

        }
// ************************************************************************************************** MOUSE_DOWN

        // MOUSE_MOVE *************************************************************************************************
        // MouseMove

        private void axaxcMapScreen1_OnMapMouseMove(object sender, AxaxGisToolKit.IaxMapScreenEvents_OnMapMouseMoveEvent e)
        {
            if (waspressleft == true)
            {

                movelefteX = e.x;
                movelefteY = e.y;

                MapCore.mapPlaneToPicture((int)axaxcMapScreen1.MapHandle, ref movelefteX, ref movelefteY);

                this.Cursor = Cursors.SizeAll;
                axaxcMapScreen1.MapLeft -= (int)(movelefteX - startlefteX);
                axaxcMapScreen1.MapTop -= (int)(movelefteY - startlefteY);


            }


            // Координаты внизу********************************************************************
            if (MapIsOpenned == true)
            {

                double moveX1 = 0;
                double moveY1 = 0;
                double moveHH = 0;

                moveX1 = e.x;
                moveY1 = e.y;
                //label2.Text = string.Format("X, m = {0}", Convert.ToString((int)moveX1));
                //label3.Text = string.Format("Y, m = {0}", Convert.ToString((int)moveY1));

                // m rel
                GlobalVarLn.X_Down_mrel = moveX1;
                GlobalVarLn.Y_Down_mrel = moveY1;

                // rad
                MapCore.mapPlaneToGeo((int)axaxcMapScreen1.MapHandle, ref GlobalVarLn.X_Down_mrel, ref GlobalVarLn.Y_Down_mrel);
                // grad
                GlobalVarLn.X_Down_mrel = (GlobalVarLn.X_Down_mrel * 180) / Math.PI;
                GlobalVarLn.Y_Down_mrel = (GlobalVarLn.Y_Down_mrel * 180) / Math.PI;

                // SK42(элл.)->Крюгер ****************************************************
                // Преобразование геодезических координат (широта, долгота, высота) 
                // эллипсоида Красовского (СК42) в плоские прямоугольные координаты в
                // проекции Гаусса-Крюгера
                // Входные параметры -> !!!grad

                ClassMap objClassMap_glob = new ClassMap();

                objClassMap_glob.f_SK42_Krug
                       (
                    // Входные параметры (!!! grad)
                    // !!! эллипсоид Красовского
                           GlobalVarLn.X_Down_mrel,   // широта
                           GlobalVarLn.Y_Down_mrel,  // долгота

                           // Выходные параметры (km)
                           ref GlobalVarLn.X42_mrel,
                           ref GlobalVarLn.Y42_mrel
                       );

                // km->m
                GlobalVarLn.X42_mrel = GlobalVarLn.X42_mrel * 1000;
                GlobalVarLn.Y42_mrel = GlobalVarLn.Y42_mrel * 1000;

                label2.Text = string.Format("X, м = {0}", Convert.ToString((int)GlobalVarLn.X42_mrel));
                label3.Text = string.Format("Y, м = {0}", Convert.ToString((int)GlobalVarLn.Y42_mrel));

                // **************************************************** SK42(элл.)->Крюгер

                // .......................................................................
                // H

                GlobalVarLn.axMapPointGlobalAdd.SetPoint(moveX1, moveY1);
                moveHH = GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);
                label4.Text = string.Format("H, м = {0}", Convert.ToString((int)moveHH));

                MapCore.mapPlaneToGeo((int)axaxcMapScreen1.MapHandle, ref moveX1, ref moveY1);
                moveX1=(moveX1*180)/Math.PI;
                moveY1 = (moveY1 * 180) / Math.PI;

                if (GlobalVarLn.fl_Azb == 0)
                {
                    lLat.Text = string.Format("Широта,° = {0}", moveX1.ToString("0.000"));
                    label1.Text = string.Format("Долгота,° = {0}", moveY1.ToString("0.000"));
                }
                else
                {
                    //Azb ???
                    lLat.Text = string.Format("Enlik,° = {0}", moveX1.ToString("0.000"));
                    label1.Text = string.Format("Uzunluq,° = {0}", moveY1.ToString("0.000"));
                }
                // ******************************************************************** Координаты внизу

                label5.Text = string.Format("М 1: {0}", Convert.ToString((int)axaxcMapScreen1.ViewScale));

            } // MapIsOpened

        } // MouseMove
        // ************************************************************************************************* MOUSE_MOVE

        // MOUSE_UP ****************************************************************************************************
        private void axaxcMapScreen1_OnMapMouseUp(object sender, AxaxGisToolKit.IaxMapScreenEvents_OnMapMouseUpEvent e)
        {
            if (e.button == 0)
            {
                // 1210
                GlobalVarLn.MousePress = false;

                // .....................................................................................................
                int fff = 0;
                // Sect Mouse_up
                if (
                    // !!! Это было до сектора                    
                    //((GlobalVarLn.fl_PelMain == 1) && (GlobalVarLn.f_Open_objFormBearing == 1)) ||
                    ((GlobalVarLn.fl_PelMain == 1)) ||

                    //((GlobalVarLn.fl_LineSightRange == 1) && (GlobalVarLn.f_Open_ObjFormLineSightRange==1)) ||
                    (GlobalVarLn.fl_LineSightRange == 1) ||
                    ((GlobalVarLn.flCoordSP_sup == 1) && (GlobalVarLn.f_Open_objFormSuppression == 1)) ||

                    ((GlobalVarLn.flsect == 1) && (chbSect.Checked == true))
                   )
                {
                    if ((GlobalVarLn.flsect == 1) && (chbSect.Checked == true))
                    {
                        fff = 1;
                        GlobalVarLn.flsect = 0;
                    }

                    // !!! Это было до сектора
                    // Убрать с карты
                    GlobalVarLn.axMapScreenGlobal.Repaint();

                    if (fff == 1)
                    {
                        GlobalVarLn.flsect = 1;
                        ClassMap.Sector((double)GlobalVarLn.XCenter_Sost, (double)GlobalVarLn.YCenter_Sost);
                    }


                }
                // .....................................................................................................

                waspressleft = false;
            }

            this.Cursor = Cursors.Default;
        }
        // **************************************************************************************************** MOUSE_UP

        //Флаги перерисовки
        bool paintRadioSources = true;
        bool paintStations = true;
        bool paintArcPoints = true;
        bool paintPeleng = true;
        bool infopaint = true;


// ПЕРЕРИСОВКА КАРТЫ ***********************************************************************************************

        //Перисовка карты
        private void axaxcMapScreen1_OnMapPaint(object sender, AxaxGisToolKit.IaxMapScreenEvents_OnMapPaintEvent e)
        {
            
            // LENA **********************************************************************************
            // Lena (перерисовка)

            // ------------------------------------------------------------------------------------
            // Пеленг

            if (GlobalVarLn.fl_Peleng_stat == 1)
            {
                //ClassMap.f_ReDrawPeleng();
            }


            // 14_09_2018 ?????????????? Выяснить почему две функции вызываются
            if (chbPeleng.Checked == true)
            {
                // 14_09_2018
                //if(GlobalVarLn.fl_PelIRI_1==1)
                  // Для ФРЧ
                  ClassMap.f_ReDrawPeleng();

                 // Для ППРЧ
                 ClassMap.f_ReDrawPeleng_2();
            }
            // -----------------------------------------------------------------------------------
            // !!!IRI_2 -> всегда

            ClassMap.f_RedrawIRI_2();
            // -----------------------------------------------------------------------------------
            // S

            if (GlobalVarLn.fl_S1_stat == 1)
            {
                ClassMap.f_ReDrawS_stat();
            }
            // -----------------------------------------------------------------------------------
            // Маршрут

            if (GlobalVarLn.flEndWay_stat == 1)
            {
                objFormWay.f_WayReDraw();

            }
            // -----------------------------------------------------------------------------------
            // Самолеты

            if (GlobalVarLn.fl_AirPlaneVisible == 1)
            {

                // Перерисовка самолетов
                ClassMap.f_ReDrawAirPlane1();
            }
            // -------------------------------------------------------------------------------------
            // Энергодоступность по УС

            if (GlobalVarLn.fl_CommPowerAvail == 1)
            {
                ClassMap.f_Map_Redraw_CommPowerAvail1();
            }
            // -------------------------------------------------------------------------------------
            //2106

/*
            int fff1 = 0;
            int fff2 = 0;
            int fff3 = 0;

            if (GlobalVarLn.fl_RepSect == 0)
            {
                if (
                    (GlobalVarLn.fl_LineSightRange == 1) &&
                    (GlobalVarLn.MousePress == false)
                   )
                {
                    fff1 = 1;
                    GlobalVarLn.fl_LineSightRange = 0;
                }
                if (
                    (GlobalVarLn.fl_PelMain == 1) &&
                    (GlobalVarLn.MousePress == false)
                    )
                {
                    fff2 = 1;
                    GlobalVarLn.fl_PelMain = 0;
                }

                if (
                    (GlobalVarLn.flsect == 1) &&
                    (GlobalVarLn.MousePress == false) &&
                    (chbSect.Checked == true)
                   )
                {
                    fff3 = 1;
                    GlobalVarLn.flsect = 0;
                }

                if ((fff1 == 1) || (fff2 == 1) || (fff3 == 1))
                {
                    GlobalVarLn.fl_RepSect = 1;
                    GlobalVarLn.axMapScreenGlobal.Repaint();
                    GlobalVarLn.fl_RepSect = 0;
                }

                if (fff1 == 1)
                {
                    fff1 = 0;
                    GlobalVarLn.fl_LineSightRange = 1;
                }
                if (fff2 == 1)
                {
                    fff2 = 0;
                    GlobalVarLn.fl_PelMain = 1;
                }
                if (fff3 == 1)
                {
                    fff3 = 0;
                    GlobalVarLn.flsect = 1;
                }

            }
 */
            // -------------------------------------------------------------------------------------
            // ЗПВ

            if (
                (GlobalVarLn.fl_LineSightRange == 1)&&
                (GlobalVarLn.MousePress == false)
               )
            {
                // SP
                ClassMap.f_DrawSPXY(
                              GlobalVarLn.XCenter_ZPV,  // m на местности
                              GlobalVarLn.YCenter_ZPV,
                                  ""
                             );


                ClassMap.DrawLSR();
            }

            if (
                (GlobalVarLn.flCoordParaZPV == 1) &&
                (GlobalVarLn.MousePress == false)
               )
            {
                ClassMap.DrawLSR_Z1();
                ClassMap.DrawLSR_Z2();

            }
            // -------------------------------------------------------------------------------------
            // Состав комплекса

            // GPSSPPU
            //if (GlobalVarLn.fl_Sost == 1)
            if(
               (GlobalVarLn.flCoordSP_Sost==1)||
               (GlobalVarLn.flCoordYS1_Sost==1)||
               (GlobalVarLn.flCoordYS2_Sost == 1)
               )
            {
                ClassMap.f_Map_Redraw_Sost();
            }
            // -------------------------------------------------------------------------------------


            //2106
            //if (GlobalVarLn.flscrolsect == 1)
            //{
            //    GlobalVarLn.flscrolsect = 0;
            //    return;
            //}



            // Bearing

            if (
                (GlobalVarLn.fl_PelMain == 1)&&
                (GlobalVarLn.MousePress==false)&&
                (GlobalVarLn.xxxY==0)&&
                (GlobalVarLn.xxxY1==0)

               )
            {

                if (GlobalVarLn.ftmr == 0)
                {
                    ClassMap.f_ReDraw_Map_Bearing1();
                    GlobalVarLn.ftmr = 1;
                    GlobalVarLn.stopWatch.Start();
                }
                else
                {

                    GlobalVarLn.ftmr = 0;
                    GlobalVarLn.stopWatch.Stop();
                    GlobalVarLn.ts = GlobalVarLn.stopWatch.Elapsed;


                    if (
                        (GlobalVarLn.ts.Hours == 0) &&
                        (GlobalVarLn.ts.Minutes == 0) &&
                        (GlobalVarLn.ts.Seconds <= 0.1)
                        )
                    {
                        ;
                    }
                    else
                    {
                        //ZZZ
                        ClassMap.f_ReDraw_Map_Bearing1();

                    }

                } // GlobalVarLn.ftmr == 1

            } // GlobalVarLn.fl_PelMain == 1

            // ------------------------------------------------------------------------------------
            // Az1

            if (GlobalVarLn.flCoord_Az1 == 1)
            {
                ClassMap.f_Rect_Az1(GlobalVarLn.XCenter_Az1, GlobalVarLn.YCenter_Az1);
            }
            // -------------------------------------------------------------------------------------
            // Зона подавления

            if (
                (GlobalVarLn.flCoordSP_sup == 1)&&
                (GlobalVarLn.MousePress == false)
               )
            {
                ClassMap.f_Map_Redraw_Zon_Suppression();
            }
            // -------------------------------------------------------------------------------------
            //GPSIRI

            if (GlobalVarLn.flGPS_IRI == 1)
            {
                ClassMap.f_Map_Redraw_GPS_IRI();
            }

            // -------------------------------------------------------------------------------------
            //Sect перерисовка

            if (
                (GlobalVarLn.flsect == 1) &&
                (GlobalVarLn.MousePress == false) &&
                (chbSect.Checked == true)

               )
            {
                // .........................................................................

                if (GlobalVarLn.ftmr_Sect == 0)
                {

                    if (GlobalVarLn.flscrolsect == 1)
                    {
                        GlobalVarLn.flscrolsect = 0;
                        GlobalVarLn.ftmr_Sect = 1;
                        GlobalVarLn.stopWatch2.Start();
                        return;
                    }

                    ClassMap.Sector(
                                      (double)GlobalVarLn.XCenter_Sost,
                                      (double)GlobalVarLn.YCenter_Sost
                                    );

                    GlobalVarLn.ftmr_Sect = 1;
                    GlobalVarLn.stopWatch2.Start();
                }
                else
                {

                    GlobalVarLn.ftmr_Sect = 0;
                    GlobalVarLn.stopWatch2.Stop();
                    GlobalVarLn.ts_Sect = GlobalVarLn.stopWatch2.Elapsed;


                    if (
                        (GlobalVarLn.ts_Sect.Hours == 0) &&
                        (GlobalVarLn.ts_Sect.Minutes == 0) &&
                        (GlobalVarLn.ts_Sect.Seconds <= 0.1)
                        )
                    {
                        int ii = 0;
                        ii = ii;
                    }
                    else
                    {
                        //2106
                        //GlobalVarLn.flsect = 0;
                        // Убрать с карты
                        //GlobalVarLn.axMapScreenGlobal.Repaint();
                        //GlobalVarLn.flsect = 1;

                        ClassMap.Sector(
                                          (double)GlobalVarLn.XCenter_Sost,
                                          (double)GlobalVarLn.YCenter_Sost
                                        );

                    }

                } // GlobalVarLn.ftmr_Sect == 1
  
                // .........................................................................

            } // Sector
            // ********************************************************************************** LENA



        } // Перерисовка карты
        // *********************************************************************************************** ПЕРЕРИСОВКА КАРТЫ


        //Радиоисточники
        private List<MapCore.RadioSource> radioSources = new List<MapCore.RadioSource>();

        //Станции
        private List<MapCore.Station> stations = new List<MapCore.Station>();

        //Точки кривой
        private List<PointF> ArcPoints = new List<PointF>();

        // Снять координаты
        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (MapIsOpenned == true)
            {
                if (checkPointForm != null && !checkPointForm.IsDisposed)
                {
                    checkPointForm.FillTextBoxes((int)axaxcMapScreen1.MapHandle, righteX, righteY);

                }
            }
        }

        // добавить ИРИ
        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            if (MapIsOpenned == true)
            {
                MapCore.RadioSource temp = new MapCore.RadioSource(righteX, righteY);
                MapCore.DrawAndSaveTriangle(ref temp, ref axaxcMapScreen1);
                radioSources.Add(temp);
            }
        }

        // удалить ИРИ
        private void toolStripMenuItem3_Click(object sender, EventArgs e)
        {
            if (MapIsOpenned == true)
            {
                MapCore.DeleteTriangle(righteX, righteY, ref radioSources, ref axaxcMapScreen1);
            }
        }

        // добавить точку для дуги
        private void toolStripMenuItem4_Click(object sender, EventArgs e)
        {
            if (MapIsOpenned == true)
            {
                ArcPoints.Add(new PointF((float)righteX, (float)righteY));
            }
        }

        //нарисовать дугу
        private void toolStripMenuItem5_Click(object sender, EventArgs e)
        {
            if (MapIsOpenned == true)
            {
                if (ArcPoints.Count >= 2)
                    MapCore.DrawArc(ArcPoints.ToArray(), ref axaxcMapScreen1);
            }
        }

        //очистить точки для дуги
        private void toolStripMenuItem6_Click(object sender, EventArgs e)
        {
            if (MapIsOpenned == true)
            {
                ArcPoints.Clear();
                axaxcMapScreen1.Repaint();
            }
        }

        //Добавить станцию
        private void toolStripMenuItem7_Click(object sender, EventArgs e)
        {
            if (MapIsOpenned == true)
            {
                MapCore.Station temp = new MapCore.Station(righteX, righteY);
                MapCore.DrawAndSaveStation(ref temp, ref axaxcMapScreen1, imageList1);
                stations.Add(temp);
            }
        }

        //Удалить станцию
        private void toolStripMenuItem8_Click(object sender, EventArgs e)
        {
            if (MapIsOpenned == true)
            {
                MapCore.DeleteStation(righteX, righteY, ref stations, ref axaxcMapScreen1);
            }
        }

        //Отрисовать пеленг
        private void toolStripMenuItem9_Click(object sender, EventArgs e)
        {
            double a = 0;
            double b = 0;
            double c = 0;
            double d = 0;
            double f = 0;
            double g = 0;

            var currRSIndex = MapCore.GetRadioSourceIndex(righteX, righteY, ref radioSources, ref axaxcMapScreen1);

            if (currRSIndex != -1)
            {
/*
                double peleng = 255;
                double distance = 200;
                uint numberofdots = 1000;

                double[] arr_Pel = new double[numberofdots * 3];
                double[] arr_Pel_XYZ = new double[numberofdots * 3];

                ClassMap classMap = new ClassMap();
                classMap.f_Peleng(peleng, distance, numberofdots, radioSources[currRSIndex].geoX, radioSources[currRSIndex].geoY, ref a, ref b, ref c, ref d, ref f, ref g, ref arr_Pel, ref arr_Pel_XYZ);

                double[] latitude = new double[numberofdots];
                double[] longitude = new double[numberofdots];

                MapCore.One3NumArrtoTwo1NumArrs(arr_Pel, ref latitude, ref longitude);

                var temp = radioSources[currRSIndex];
                temp.RadioSourceSetLatLon(latitude, longitude);
                radioSources[currRSIndex] = temp;

                MapCore.DrawPeleng(radioSources[currRSIndex].latitude, radioSources[currRSIndex].longitude, ref axaxcMapScreen1);
 */
            }
        }

        //Отобразить/скрыть текст
        private void toolStripMenuItem10_Click(object sender, EventArgs e)
        {
            MapCore.SetInfoPaint(righteX, righteY, ref radioSources, ref axaxcMapScreen1);
        }


        // LENA ************************************************************************************
        // Lena

        // *****************************************************************************************
        // Загрузка головной формы
        // *****************************************************************************************

        private void MapForm_Load(object sender, EventArgs e)
        {
            string pathToMap ="";
            string pathToMTW = "";

            string strExePath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

            string PathFolder1 = Path.GetDirectoryName(strExePath);

            string strPathLatest = PathFolder1 + "\\INI\\Common.ini";
            foreach (string line in File.ReadLines(strPathLatest))
            {
                if (line.Contains("Language "))
                {
                    string LanguageAsNumber = line.Split(new string[] { "=" }, StringSplitOptions.None).Last();
                    NumberOfLanguage = Int32.Parse(LanguageAsNumber);
                    break;
                }

            }
            LanguageChooser();

           

            GlobalVarLn.stopWatch.Stop();


            GlobalVarLn.flPelMain = 0;
            GlobalVarLn.flPelMain2 = 0;
            chbPeleng.Checked = false;

            // --------------------------------------------------
            GlobalVarLn.objFormBearingG = objFormBearing;
            GlobalVarLn.objFormSostG = objFormSost;
            GlobalVarLn.objFormWayG = objFormWay;
            GlobalVarLn.objFormAzG = objFormAz;
            GlobalVarLn.objFormAz1G = objFormAz1;
            GlobalVarLn.ObjFormLineSightRangeG = ObjFormLineSightRange;
            GlobalVarLn.objZonePowerAvailG = objZonePowerAvail;
            GlobalVarLn.ObjCommPowerAvailG = ObjCommPowerAvail;
            GlobalVarLn.objFormSuppressionG = objFormSuppression;
            // --------------------------------------------------

            GlobalVarLn.axMapScreenGlobal = axaxcMapScreen1;

            // Привязка Point к Screen
            GlobalVarLn.axMapPointGlobalAdd = new axGisToolKit.axMapPoint();
            GlobalVarLn.axMapPointGlobalAdd.cMapView = axaxcMapScreen1.C_CONTAINER;
            GlobalVarLn.axMapPointGlobalAdd.PlaceInp = axGisToolKit.TxPPLACE.PP_PLANE;
            GlobalVarLn.axMapPointGlobalAdd.PlaceOut = axGisToolKit.TxPPLACE.PP_PLANE;

            GlobalVarLn.hmapl = (int)axaxcMapScreen1.MapHandle;
            hmapl1 = (int)axaxcMapScreen1.MapHandle;


            // *********************************************************************************************
            // Значки
            String nn;
            String nn1;
            String s;
            long iz;
            // ..............................................................................................
            // SP1
            GlobalVarLn.objFormSostG.imageList1.Images.Clear();
            try
            {
                s = Application.StartupPath + "\\Images\\Jammer\\";
                DirectoryInfo di = new DirectoryInfo(@s);

                iz = 0;
                foreach (var fi in di.GetFiles("*.png"))
                {

                    nn = fi.Name;
                    nn1 = fi.DirectoryName + "\\" + fi.Name;
                    GlobalVarLn.objFormSostG.imageList1.Images.Add(Image.FromFile(nn1));
                    iz += 1;

                }
                foreach (var fi_1 in di.GetFiles("*.bmp"))
                {

                    nn = fi_1.Name;
                    nn1 = fi_1.DirectoryName + "\\" + fi_1.Name;
                    GlobalVarLn.objFormSostG.imageList1.Images.Add(Image.FromFile(nn1));
                    iz += 1;

                }
                foreach (var fi_1 in di.GetFiles("*.jpeg"))
                {

                    nn = fi_1.Name;
                    nn1 = fi_1.DirectoryName + "\\" + fi_1.Name;
                    GlobalVarLn.objFormSostG.imageList1.Images.Add(Image.FromFile(nn1));
                    iz += 1;

                }

                //Azb
                if (GlobalVarLn.fl_Azb == 0)
                {
                    if (iz == 0) MessageBox.Show("Нет списка значков");
                }
                else
                {
                    if (iz == 0) MessageBox.Show("Nişan yoxdur");
                }

            }
            catch
            {
                //Azb
                if (GlobalVarLn.fl_Azb == 0)
                {
                    MessageBox.Show("Нет списка значков");
                }
                else
                {
                    MessageBox.Show("Nişan yoxdur");
                }

            }
            // ..............................................................................................
            // SP2
            GlobalVarLn.objFormSostG.imageList2.Images.Clear();
            try
            {
                s = Application.StartupPath + "\\Images\\JammerSopr\\";
                DirectoryInfo di1 = new DirectoryInfo(@s);

                iz = 0;
                foreach (var fi1 in di1.GetFiles("*.png"))
                {

                    nn = fi1.Name;
                    nn1 = fi1.DirectoryName + "\\" + fi1.Name;
                    GlobalVarLn.objFormSostG.imageList2.Images.Add(Image.FromFile(nn1));
                    iz += 1;

                }
                foreach (var fi1_1 in di1.GetFiles("*.bmp"))
                {

                    nn = fi1_1.Name;
                    nn1 = fi1_1.DirectoryName + "\\" + fi1_1.Name;
                    GlobalVarLn.objFormSostG.imageList2.Images.Add(Image.FromFile(nn1));
                    iz += 1;

                }
                foreach (var fi1_1 in di1.GetFiles("*.jpeg"))
                {

                    nn = fi1_1.Name;
                    nn1 = fi1_1.DirectoryName + "\\" + fi1_1.Name;
                    GlobalVarLn.objFormSostG.imageList2.Images.Add(Image.FromFile(nn1));
                    iz += 1;

                }

                //Azb
                if (GlobalVarLn.fl_Azb == 0)
                {
                    if (iz == 0) MessageBox.Show("Нет списка значков");
                }
                else
                {
                    if (iz == 0) MessageBox.Show("Nişan yoxdur");
                }

            }
            catch
            {
                if (GlobalVarLn.fl_Azb == 0)
                {
                    //Azb
                    MessageBox.Show("Нет списка значков");
                }
                else
                {
                    MessageBox.Show("Nişan yoxdur");
                }

            }
            //...............................................................................................
            // PU

            GlobalVarLn.objFormSostG.imageList3.Images.Clear();
            try
            {
                s = Application.StartupPath + "\\Images\\PU\\";
                DirectoryInfo di2 = new DirectoryInfo(@s);

                iz = 0;
                foreach (var fi2 in di2.GetFiles("*.png"))
                {

                    nn = fi2.Name;
                    nn1 = fi2.DirectoryName + "\\" + fi2.Name;
                    GlobalVarLn.objFormSostG.imageList3.Images.Add(Image.FromFile(nn1));
                    iz += 1;

                }
                foreach (var fi2_1 in di2.GetFiles("*.bmp"))
                {

                    nn = fi2_1.Name;
                    nn1 = fi2_1.DirectoryName + "\\" + fi2_1.Name;
                    GlobalVarLn.objFormSostG.imageList3.Images.Add(Image.FromFile(nn1));
                    iz += 1;

                }
                foreach (var fi2_1 in di2.GetFiles("*.jpeg"))
                {

                    nn = fi2_1.Name;
                    nn1 = fi2_1.DirectoryName + "\\" + fi2_1.Name;
                    GlobalVarLn.objFormSostG.imageList3.Images.Add(Image.FromFile(nn1));
                    iz += 1;

                }
                if (GlobalVarLn.fl_Azb == 0)
                {
                    //Azb
                    if (iz == 0) MessageBox.Show("Нет списка значков");
                }
                else
                {
                    if (iz == 0) MessageBox.Show("Nişan yoxdur");
                }
            }
            catch
            {
                //Azb
                if (GlobalVarLn.fl_Azb == 0)
                {
                    MessageBox.Show("Нет списка значков");
                }
                else
                {
                     MessageBox.Show("Nişan yoxdur");
                }
            }
            //...............................................................................................

            label5.Text = string.Format("М 1: {0}", Convert.ToString((int)axaxcMapScreen1.ViewScale));

            // ----------------------------------------------------------------------
            // !!! Первоначальная загрузка составы комплекса с INI файла

            // 19-11_2018
            // GPSSPPU
            //ClassMap.f_LoadSostIni();
            ClassMap.ClearSost();
            ClassMap.f_LoadSostIni();


            // 13_09_2018
            // !!! Значки читаем с INI файла
            GlobalVarLn.indz1_Sost = iniRW.get_ZnakSP1();
            GlobalVarLn.indz2_Sost = iniRW.get_ZnakSP2();
            GlobalVarLn.indz3_Sost = iniRW.get_ZnakPU();
            GlobalVarLn.objFormSostG.pbSP.Image = GlobalVarLn.objFormSostG.imageList1.Images[GlobalVarLn.indz1_Sost];
            GlobalVarLn.objFormSostG.pictureBox1.Image = GlobalVarLn.objFormSostG.imageList2.Images[GlobalVarLn.indz2_Sost];
            GlobalVarLn.objFormSostG.pictureBox2.Image = GlobalVarLn.objFormSostG.imageList3.Images[GlobalVarLn.indz3_Sost];

            // ----------------------------------------------------------------------
            // Флаг языка

            if (System.IO.File.Exists(Application.StartupPath + "\\Init.ini"))
            {
                GlobalVarLn.fl_Azb = (int)iniRW.get_fl_Azb();
            }
            else
            {
                //Azb
                if (GlobalVarLn.fl_Azb == 0)
                {
                    MessageBox.Show("Невозможно открыть INI файл");
                }
                else
                {
                    MessageBox.Show("fayl açıq deyil");
                }
                return;
            }

            // ---------------------------------------------------------------------
            //2501
            GlobalVarLn.list_PelIRI.Clear();
            // ---------------------------------------------------------------------
            // Инициализация секторов
            //Sect

            ClassMap.Sector_INI();
            // ---------------------------------------------------------------------



            // OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
            //777Otl-> NO delete->Otladka GPS_Sostav

/*
            AirPlane objs = new AirPlane();
            objs.Lat = 54.226731; // SP1
            objs.Long = 28.50241;
            GlobalVarLn.lll.Add(objs);
            objs.Lat = 54.236027; // SP2
            objs.Long = 28.526574;
            //objs.Lat = -1; // SP2
            //objs.Long = -1;
            GlobalVarLn.lll.Add(objs);
            objs.Lat = 54.221148; // PU
            objs.Long = 28.5427;
            GlobalVarLn.lll.Add(objs);
*/
            //timer1.Start();
            // OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO


        } // Загрузка головной формы
        // *****************************************************************************************

        // *****************************************************************************************
        // Обработчик кнопки Button2: открыть окно отображения координат
        // *****************************************************************************************
        private void button2_Click(object sender, EventArgs e)
        {

            //ToolTip.("Координаты");

            if (form1 == null || form1.IsDisposed)
            {
                form1 = new Form1(ref axaxcMapScreen1);
                form1.Show();
            }
            else
            {
                form1.Show();
            }

            //// Окно отображения координат
            //Form1 form1 = new Form1(ref axaxcMapScreen1);
            //form1.Show();

        } // Button2

        // *****************************************************************************************
        // Обработчик кнопки Button3: открыть окно отображения пеленга
        // *****************************************************************************************
        private void button3_Click(object sender, EventArgs e)
        {



/*
            // Peleng
            if (objFormPeleng == null || objFormPeleng.IsDisposed)
            {
                objFormPeleng = new FormPeleng(ref axaxcMapScreen1);
                objFormPeleng.Show();
            }
            else
            {
                objFormPeleng.Show();
            }


            //FormPeleng objFormPeleng = new FormPeleng(ref axaxcMapScreen1);
            //objFormPeleng.Show();
*/


        } // Button3

        // *****************************************************************************************
        // Обработчик кнопки Button4: расстояние между двумя пунктами
        // *****************************************************************************************
        // GPSSPPU
        // !!! Otladka Sostav-> NO delete

        //777
        private void button4_Click(object sender, EventArgs e)

        {



/*



            // OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
            //777Otl

            GlobalVarLn.ssaa += 1;
            AirPlane objs = new AirPlane();

            if (GlobalVarLn.ssaa <= 5)
            {
                objs.Lat = 54.226731; // SP1
                objs.Long = 28.50241;
                GlobalVarLn.lll[0]=objs;
                objs.Lat = 54.236027; // SP2
                objs.Long = 28.526574;
                GlobalVarLn.lll[1] = objs;
                objs.Lat = 54.221148; // PU
                objs.Long = 28.5427;
                GlobalVarLn.lll[2] = objs;
            }

            else if ((GlobalVarLn.ssaa > 5) && (GlobalVarLn.ssaa <=10))
            {
                objs.Lat = -1; // SP1
                objs.Long = -1;
                GlobalVarLn.lll[0]=objs;
                objs.Lat = -1; // SP2
                objs.Long = -1;
                GlobalVarLn.lll[1] = objs;
                objs.Lat = -1; // PU
                objs.Long = -1;
                GlobalVarLn.lll[2] = objs;
            }
            else
            {
                objs.Lat = 54.226731; // SP1
                objs.Long = 28.50241;
                GlobalVarLn.lll[0] = objs;
                objs.Lat = 54.236027; // SP2
                objs.Long = 28.526574;
                GlobalVarLn.lll[1] = objs;
                objs.Lat = 54.221148; // PU
                objs.Long = 28.5427;
                GlobalVarLn.lll[2] = objs;
            }

            // OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO


            // -----------------------------------------------------------------------
            // Otl
            bool fl = true; // птичка на АРМ-е
            // -----------------------------------------------------------------------
            // Нажата ли птичка на АРМ (посылка координат от GPS)

            if (fl == true) // Checked==true
            {
                // Раньше была не нажата
                if (GlobalVarLn.flTmpGPS_VKL == false)
                {
                    GlobalVarLn.flTmpGPS_VKL = true;
                    GlobalVarLn.flTmrGPS = 1;
                    GlobalVarLn.objFormSostG.timerGPS.Start();

                }
            }

            else // Checked==false
            {
                // Раньше была нажата
                if (GlobalVarLn.flTmpGPS_VKL == true)
                {
                    GlobalVarLn.flTmpGPS_VKL = false;
                    GlobalVarLn.flTmrGPS = 0;
                    GlobalVarLn.objFormSostG.timerGPS.Stop();
                    GlobalVarLn.fl_First_GPS = 0;
                }
            }
            // ------------------------------------------------------------------------
            // Пошли координаты -1

            LF objLF = new LF();
            int fsp1_1=0;
            int fsp2_1 = 0;
            int fpu_1 = 0;

            // ..................................................................
            // SP1 -> идут -1

            if (
               (fl == true) &&
               ((GlobalVarLn.lll[0].Lat == -1) || (GlobalVarLn.lll[0].Long == -1))
              )
            {
                // раньше были координаты (свои/GPS)
                if(
                   (GlobalVarLn.XCenterGRAD_Dubl_Sost!=-1)&&
                   (GlobalVarLn.YCenterGRAD_Dubl_Sost != -1)
                   )
                {
                GlobalVarLn.XCenterGRAD_Sost = -1;
                GlobalVarLn.YCenterGRAD_Sost = -1;
                GlobalVarLn.XCenterGRAD_Dubl_Sost = -1;
                GlobalVarLn.YCenterGRAD_Dubl_Sost = -1;
                GlobalVarLn.XCenter_Sost = 0;
                GlobalVarLn.YCenter_Sost = 0;
                GlobalVarLn.HCenter_Sost = 0;

                objLF.X_m = 0;
                objLF.Y_m = 0;
                objLF.H_m = 0;
                objLF.indzn = 0;
                GlobalVarLn.list1_Sost[0]=objLF;

                GlobalVarLn.flCoordSP_Sost = 0;
                fsp1_1 = 1;

                GlobalVarLn.objFormSostG.tbXRect.Text = "";
                GlobalVarLn.objFormSostG.tbYRect.Text = "";
                GlobalVarLn.objFormSostG.tbXRect42.Text = "";
                GlobalVarLn.objFormSostG.tbYRect42.Text = "";
                GlobalVarLn.objFormSostG.tbBRad.Text = "";
                GlobalVarLn.objFormSostG.tbLRad.Text = "";
                GlobalVarLn.objFormSostG.tbBMin1.Text = "";
                GlobalVarLn.objFormSostG.tbLMin1.Text = "";
                GlobalVarLn.objFormSostG.tbBDeg2.Text = "";
                GlobalVarLn.objFormSostG.tbBMin2.Text = "";
                GlobalVarLn.objFormSostG.tbBSec.Text = "";
                GlobalVarLn.objFormSostG.tbLDeg2.Text = "";
                GlobalVarLn.objFormSostG.tbLMin2.Text = "";
                GlobalVarLn.objFormSostG.tbLSec.Text = "";

                }
              
            }  // -1 SP1
            // ..................................................................
            // SP2

            if (
               (fl == true) &&
               ((GlobalVarLn.lll[1].Lat == -1) || (GlobalVarLn.lll[1].Long == -1))
              )
            {
                // раньше были координаты (свои/GPS)
                if (
                   (GlobalVarLn.XPoint1GRAD_Dubl_Sost != -1) &&
                   (GlobalVarLn.YPoint1GRAD_Dubl_Sost != -1)
                   )
                {
                    GlobalVarLn.XPoint1GRAD_Sost = -1;
                    GlobalVarLn.YPoint1GRAD_Sost = -1;
                    GlobalVarLn.XPoint1GRAD_Dubl_Sost = -1;
                    GlobalVarLn.YPoint1GRAD_Dubl_Sost = -1;
                    GlobalVarLn.XPoint1_Sost = 0;
                    GlobalVarLn.YPoint1_Sost = 0;
                    GlobalVarLn.HPoint1_Sost = 0;

                    objLF.X_m = 0;
                    objLF.Y_m = 0;
                    objLF.H_m = 0;
                    objLF.indzn = 0;
                    GlobalVarLn.list1_Sost[1] = objLF;

                    GlobalVarLn.flCoordYS1_Sost = 0;
                    fsp2_1 = 1;

                    GlobalVarLn.objFormSostG.tbPt1XRect.Text = "";
                    GlobalVarLn.objFormSostG.tbPt1YRect.Text = "";
                    GlobalVarLn.objFormSostG.tbPt1XRect42.Text = "";
                    GlobalVarLn.objFormSostG.tbPt1YRect42.Text = "";
                    GlobalVarLn.objFormSostG.tbPt1BRad.Text = "";
                    GlobalVarLn.objFormSostG.tbPt1LRad.Text = "";
                    GlobalVarLn.objFormSostG.tbPt1BMin1.Text = "";
                    GlobalVarLn.objFormSostG.tbPt1LMin1.Text = "";
                    GlobalVarLn.objFormSostG.tbPt1BDeg2.Text = "";
                    GlobalVarLn.objFormSostG.tbPt1BMin2.Text = "";
                    GlobalVarLn.objFormSostG.tbPt1BSec.Text = "";
                    GlobalVarLn.objFormSostG.tbPt1LDeg2.Text = "";
                    GlobalVarLn.objFormSostG.tbPt1LMin2.Text = "";
                    GlobalVarLn.objFormSostG.tbPt1LSec.Text = "";

                }

            }  // -1 SP2
            // ..................................................................
            // PU

            if (
               (fl == true) &&
               ((GlobalVarLn.lll[2].Lat == -1) || (GlobalVarLn.lll[2].Long == -1))
              )
            {
                // раньше были координаты (свои/GPS)
                if (
                   (GlobalVarLn.XPoint2GRAD_Dubl_Sost != -1) &&
                   (GlobalVarLn.YPoint2GRAD_Dubl_Sost != -1)
                   )
                {
                    GlobalVarLn.XPoint2GRAD_Sost = -1;
                    GlobalVarLn.YPoint2GRAD_Sost = -1;
                    GlobalVarLn.XPoint2GRAD_Dubl_Sost = -1;
                    GlobalVarLn.YPoint2GRAD_Dubl_Sost = -1;
                    GlobalVarLn.XPoint2_Sost = 0;
                    GlobalVarLn.YPoint2_Sost = 0;
                    GlobalVarLn.HPoint2_Sost = 0;

                    objLF.X_m = 0;
                    objLF.Y_m = 0;
                    objLF.H_m = 0;
                    objLF.indzn = 0;
                    GlobalVarLn.list1_Sost[2] = objLF;

                    GlobalVarLn.flCoordYS2_Sost = 0;

                    fpu_1 = 1;
                    GlobalVarLn.objFormSostG.tbPt2XRect.Text = "";
                    GlobalVarLn.objFormSostG.tbPt2YRect.Text = "";
                    GlobalVarLn.objFormSostG.tbPt2XRect42.Text = "";
                    GlobalVarLn.objFormSostG.tbPt2YRect42.Text = "";
                    GlobalVarLn.objFormSostG.tbPt2BRad.Text = "";
                    GlobalVarLn.objFormSostG.tbPt2LRad.Text = "";
                    GlobalVarLn.objFormSostG.tbPt2BMin1.Text = "";
                    GlobalVarLn.objFormSostG.tbPt2LMin1.Text = "";
                    GlobalVarLn.objFormSostG.tbPt2BDeg2.Text = "";
                    GlobalVarLn.objFormSostG.tbPt2BMin2.Text = "";
                    GlobalVarLn.objFormSostG.tbPt2BSec.Text = "";
                    GlobalVarLn.objFormSostG.tbPt2LDeg2.Text = "";
                    GlobalVarLn.objFormSostG.tbPt2LMin2.Text = "";
                    GlobalVarLn.objFormSostG.tbPt2LSec.Text = "";
                    GlobalVarLn.objFormSostG.tbOwnHeight.Text = "";
                    GlobalVarLn.objFormSostG.tbPt1Height.Text = "";
                    GlobalVarLn.objFormSostG.tbPt2Height.Text = "";

                }

            }  // -1 PU
            // ..................................................................

            if ((fsp1_1 == 1) || (fsp2_1 == 1) || (fpu_1 == 1))
            {
                // Убрать с карты
                GlobalVarLn.axMapScreenGlobal.Repaint();
                // Redraw
                ClassMap.f_Map_Redraw_Sost();

            }
            // ..................................................................

            // -----------------------------------------------------------------------
            // TIMER 1s

            // ------------------------------------------------------------------------
            // Checked==true

            if (
                //(GlobalVarLn.objFormSostG.chbGPS.Checked == true) &&
                (fl == true) &&
                (
                 ((GlobalVarLn.lll[0].Lat != -1) && (GlobalVarLn.lll[0].Long != -1)) ||
                 ((GlobalVarLn.lll[1].Lat != -1) && (GlobalVarLn.lll[1].Long != -1)) ||
                 ((GlobalVarLn.lll[2].Lat != -1) && (GlobalVarLn.lll[2].Long != -1))
                )
                )
            {
                // .......................................................................
                // 1-й раз

                if ((GlobalVarLn.fl_First_GPS == 0))
                {

                    GlobalVarLn.fl_First_GPS = 1;

                    // SP1
                    if ((GlobalVarLn.lll[0].Lat != -1) && (GlobalVarLn.lll[0].Long != -1))
                    {
                        GlobalVarLn.XCenterGRAD_Sost = GlobalVarLn.lll[0].Lat;
                        GlobalVarLn.YCenterGRAD_Sost = GlobalVarLn.lll[0].Long;
                        GlobalVarLn.XCenterGRAD_Dubl_Sost = GlobalVarLn.XCenterGRAD_Sost;
                        GlobalVarLn.YCenterGRAD_Dubl_Sost = GlobalVarLn.YCenterGRAD_Sost;
                    }

                    // SP2
                    if ((GlobalVarLn.lll[1].Lat != -1) && (GlobalVarLn.lll[1].Long != -1))
                    {
                        GlobalVarLn.XPoint1GRAD_Sost = GlobalVarLn.lll[1].Lat;
                        GlobalVarLn.YPoint1GRAD_Sost = GlobalVarLn.lll[1].Long;
                        GlobalVarLn.XPoint1GRAD_Dubl_Sost = GlobalVarLn.XPoint1GRAD_Sost;
                        GlobalVarLn.YPoint1GRAD_Dubl_Sost = GlobalVarLn.YPoint1GRAD_Sost;
                    }

                    // PU
                    if ((GlobalVarLn.lll[2].Lat != -1) && (GlobalVarLn.lll[2].Long != -1))
                    {
                        GlobalVarLn.XPoint2GRAD_Sost = GlobalVarLn.lll[2].Lat;
                        GlobalVarLn.YPoint2GRAD_Sost = GlobalVarLn.lll[2].Long;
                        GlobalVarLn.XPoint2GRAD_Dubl_Sost = GlobalVarLn.XPoint2GRAD_Sost;
                        GlobalVarLn.YPoint2GRAD_Dubl_Sost = GlobalVarLn.YPoint2GRAD_Sost;
                    }

                    ClassMap.f_GetGPS();

                } // 1-й раз
                // .......................................................................
                // НЕ 1-й раз

                else
                {

                    int fsp1 = 0;
                    int fsp2 = 0;
                    int fpu = 0;

                    // ''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    // SP1

                    if ((GlobalVarLn.lll[0].Lat != -1) && (GlobalVarLn.lll[0].Long != -1))
                    {
                        //Положение SP1 изменилось
                        if (
                           (
                            (GlobalVarLn.lll[0].Lat != GlobalVarLn.XCenterGRAD_Dubl_Sost) ||
                            (GlobalVarLn.lll[0].Long != GlobalVarLn.YCenterGRAD_Dubl_Sost)
                           ) &&
                           (GlobalVarLn.flTmrGPS == 1)
                          )
                        {
                            fsp1 = 1;
                            GlobalVarLn.XCenterGRAD_Sost = GlobalVarLn.lll[0].Lat;
                            GlobalVarLn.YCenterGRAD_Sost = GlobalVarLn.lll[0].Long;
                            GlobalVarLn.XCenterGRAD_Dubl_Sost = GlobalVarLn.XCenterGRAD_Sost;
                            GlobalVarLn.YCenterGRAD_Dubl_Sost = GlobalVarLn.YCenterGRAD_Sost;

                        } // Положение SP1 изменилось

                    } // SP1
                    // ''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    // SP2

                    if ((GlobalVarLn.lll[1].Lat != -1) && (GlobalVarLn.lll[1].Long != -1))
                    {
                        //Положение SP2 изменилось
                        if (
                           (
                            (GlobalVarLn.lll[1].Lat != GlobalVarLn.XPoint1GRAD_Dubl_Sost) ||
                            (GlobalVarLn.lll[1].Long != GlobalVarLn.YPoint1GRAD_Dubl_Sost)
                           ) &&
                           (GlobalVarLn.flTmrGPS == 1)
                          )
                        {
                            fsp2 = 1;
                            GlobalVarLn.XPoint1GRAD_Sost = GlobalVarLn.lll[1].Lat;
                            GlobalVarLn.YPoint1GRAD_Sost = GlobalVarLn.lll[1].Long;
                            GlobalVarLn.XPoint1GRAD_Dubl_Sost = GlobalVarLn.XPoint1GRAD_Sost;
                            GlobalVarLn.YPoint1GRAD_Dubl_Sost = GlobalVarLn.YPoint1GRAD_Sost;

                        } // Положение SP2 изменилось

                    } // SP2
                    // ''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    // PU

                    if ((GlobalVarLn.lll[2].Lat != -1) && (GlobalVarLn.lll[2].Long != -1))
                    {
                        //Положение PU изменилось
                        if (
                           (
                            (GlobalVarLn.lll[2].Lat != GlobalVarLn.XPoint2GRAD_Dubl_Sost) ||
                            (GlobalVarLn.lll[2].Long != GlobalVarLn.YPoint2GRAD_Dubl_Sost)
                           ) &&
                           (GlobalVarLn.flTmrGPS == 1)
                          )
                        {
                            fpu = 1;
                            GlobalVarLn.XPoint2GRAD_Sost = GlobalVarLn.lll[2].Lat;
                            GlobalVarLn.YPoint2GRAD_Sost = GlobalVarLn.lll[2].Long;
                            GlobalVarLn.XPoint2GRAD_Dubl_Sost = GlobalVarLn.XPoint2GRAD_Sost;
                            GlobalVarLn.YPoint2GRAD_Dubl_Sost = GlobalVarLn.YPoint2GRAD_Sost;

                        } // Положение PU изменилось

                    } // PU
                    // ''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                    if ((fsp1 == 1) || (fsp2 == 1) || (fpu == 1))
                    {
                        GlobalVarLn.flTmrGPS = 0;
                        ClassMap.f_GetGPS();
                    }


                } // НЕ 1-й раз
                // .......................................................................

            } // if (Checked == true&& Coord!=-1)
            // ------------------------------------------------------------------------
*/

//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


            // NEW *******************************************************************

            // -----------------------------------------------------------------------
            // Нажата ли птичка на АРМ (посылка координат от GPS)

            TCoordsGNSS[] coordsGNSS_1 = new TCoordsGNSS[3];

            GlobalVarLn.ssaa += 1;

            if (GlobalVarLn.ssaa == 1)
            {
                coordsGNSS_1[0].Lat = 54.051;
                coordsGNSS_1[0].Lon = 26.084;
            }
            if (GlobalVarLn.ssaa == 2)
            {
                coordsGNSS_1[0].Lat = 54.037822;
                coordsGNSS_1[0].Lon = 25.981732;
            }


            GlobalVarLn.bCheckGNSS = true;
            // -----------------------------------------------------------------------
            // Пошли координаты -1

            LF objLF = new LF();
            int fsp1_1 = 0;
            int fsp2_1 = 0;
            int fpu_1 = 0;

            // ..................................................................
            // SP1 -> идут -1

            if (
                //(fl == true) &&
               (GlobalVarLn.bCheckGNSS) &&
                //777Otl
               ((coordsGNSS_1[0].Lat == -1) || (coordsGNSS_1[0].Lon == -1))
              )
            {
                // раньше были координаты (свои/GPS)
                if (
                   (GlobalVarLn.XCenterGRAD_Dubl_Sost != -1) &&
                   (GlobalVarLn.YCenterGRAD_Dubl_Sost != -1)
                   )
                {
                    GlobalVarLn.XCenterGRAD_Sost = -1;
                    GlobalVarLn.YCenterGRAD_Sost = -1;
                    GlobalVarLn.XCenterGRAD_Dubl_Sost = -1;
                    GlobalVarLn.YCenterGRAD_Dubl_Sost = -1;
                    GlobalVarLn.XCenter_Sost = 0;
                    GlobalVarLn.YCenter_Sost = 0;
                    GlobalVarLn.HCenter_Sost = 0;

                    objLF.X_m = 0;
                    objLF.Y_m = 0;
                    objLF.H_m = 0;

                    objLF.indzn = iniRW.get_ZnakSP1();
                    GlobalVarLn.indz1_Sost = objLF.indzn;
                    GlobalVarLn.objFormSostG.pbSP.Image = GlobalVarLn.objFormSostG.imageList1.Images[GlobalVarLn.indz1_Sost];

                    GlobalVarLn.list1_Sost[0] = objLF;

                    GlobalVarLn.flCoordSP_Sost = 0;
                    fsp1_1 = 1;

                    GlobalVarLn.objFormSostG.tbXRect.Text = "";
                    GlobalVarLn.objFormSostG.tbYRect.Text = "";
                    GlobalVarLn.objFormSostG.tbXRect42.Text = "";
                    GlobalVarLn.objFormSostG.tbYRect42.Text = "";
                    GlobalVarLn.objFormSostG.tbBRad.Text = "";
                    GlobalVarLn.objFormSostG.tbLRad.Text = "";
                    GlobalVarLn.objFormSostG.tbBMin1.Text = "";
                    GlobalVarLn.objFormSostG.tbLMin1.Text = "";
                    GlobalVarLn.objFormSostG.tbBDeg2.Text = "";
                    GlobalVarLn.objFormSostG.tbBMin2.Text = "";
                    GlobalVarLn.objFormSostG.tbBSec.Text = "";
                    GlobalVarLn.objFormSostG.tbLDeg2.Text = "";
                    GlobalVarLn.objFormSostG.tbLMin2.Text = "";
                    GlobalVarLn.objFormSostG.tbLSec.Text = "";

                    GlobalVarLn.objFormSostG.tbOwnHeight.Text = "";

                    // --------------------------------------------------------------------------------------
                    // 14_09_2018
                    // Для пеленгов

                    // Нажата птичка для отрисовки пеленгов
                    if (GlobalVarLn.flPelMain == 1)
                    {
                        // FRCH
                        GlobalVarLn.PrevP1 = -1;
                        GlobalVarLn.fl_PelIRI_1 = 0; // 1-й пеленг
                        GlobalVarLn.flag_eq_draw1 = 0;

                        //------------------------------------------------------------------------
                        //PPRCH
                        for (int iii = (GlobalVarLn.list_PelIRI.Count - 1); iii >= 0; iii--)
                        {
                            PelIRI[] mass = GlobalVarLn.list_PelIRI.ToArray();
                            mass[iii].Pel1 = -1;
                            GlobalVarLn.list_PelIRI = mass.ToList();

                            if (
                                (GlobalVarLn.list_PelIRI[iii].Pel1 == -1) &&
                                (GlobalVarLn.list_PelIRI[iii].Pel2 == -1) &&
                                (GlobalVarLn.list_PelIRI[iii].Lat == -1) &&
                                (GlobalVarLn.list_PelIRI[iii].Long == -1)
                               )
                            {
                                GlobalVarLn.list_PelIRI.Remove(GlobalVarLn.list_PelIRI[iii]);
                            }
                        }

                    }
                    //------------------------------------------------------------------------
                }

            }  // -1 SP1
            // ..................................................................
            // SP2

            if (
               (GlobalVarLn.bCheckGNSS) &&
                //777Otl
                ((coordsGNSS_1[1].Lat == -1) || (coordsGNSS_1[1].Lon == -1))
              )
            {
                // раньше были координаты (свои/GPS)
                if (
                   (GlobalVarLn.XPoint1GRAD_Dubl_Sost != -1) &&
                   (GlobalVarLn.YPoint1GRAD_Dubl_Sost != -1)
                   )
                {
                    GlobalVarLn.XPoint1GRAD_Sost = -1;
                    GlobalVarLn.YPoint1GRAD_Sost = -1;
                    GlobalVarLn.XPoint1GRAD_Dubl_Sost = -1;
                    GlobalVarLn.YPoint1GRAD_Dubl_Sost = -1;
                    GlobalVarLn.XPoint1_Sost = 0;
                    GlobalVarLn.YPoint1_Sost = 0;
                    GlobalVarLn.HPoint1_Sost = 0;

                    objLF.X_m = 0;
                    objLF.Y_m = 0;
                    objLF.H_m = 0;

                    // 13_09_2018
                    //objLF.indzn = 0;
                    GlobalVarLn.indz2_Sost = iniRW.get_ZnakSP2();
                    GlobalVarLn.objFormSostG.pictureBox1.Image = GlobalVarLn.objFormSostG.imageList2.Images[GlobalVarLn.indz2_Sost];
                    objLF.indzn = GlobalVarLn.indz2_Sost;

                    GlobalVarLn.list1_Sost[1] = objLF;

                    GlobalVarLn.flCoordYS1_Sost = 0;
                    fsp2_1 = 1;

                    GlobalVarLn.objFormSostG.tbPt1XRect.Text = "";
                    GlobalVarLn.objFormSostG.tbPt1YRect.Text = "";
                    GlobalVarLn.objFormSostG.tbPt1XRect42.Text = "";
                    GlobalVarLn.objFormSostG.tbPt1YRect42.Text = "";
                    GlobalVarLn.objFormSostG.tbPt1BRad.Text = "";
                    GlobalVarLn.objFormSostG.tbPt1LRad.Text = "";
                    GlobalVarLn.objFormSostG.tbPt1BMin1.Text = "";
                    GlobalVarLn.objFormSostG.tbPt1LMin1.Text = "";
                    GlobalVarLn.objFormSostG.tbPt1BDeg2.Text = "";
                    GlobalVarLn.objFormSostG.tbPt1BMin2.Text = "";
                    GlobalVarLn.objFormSostG.tbPt1BSec.Text = "";
                    GlobalVarLn.objFormSostG.tbPt1LDeg2.Text = "";
                    GlobalVarLn.objFormSostG.tbPt1LMin2.Text = "";
                    GlobalVarLn.objFormSostG.tbPt1LSec.Text = "";

                    GlobalVarLn.objFormSostG.tbPt1Height.Text = "";

                    // --------------------------------------------------------------------------------------
                    // 14_09_2018
                    // Для пеленгов

                    // Нажата птичка для отрисовки пеленгов
                    if (GlobalVarLn.flPelMain == 1)
                    {
                        // FRCH
                        GlobalVarLn.PrevP2 = -1;
                        GlobalVarLn.flPelMain2 = 0;   // 2-й пеленг
                        GlobalVarLn.flag_eq_draw2 = 0;
                        //------------------------------------------------------------------------
                        // PPRCH
                        for (int iii = (GlobalVarLn.list_PelIRI.Count - 1); iii >= 0; iii--)
                        {


                            PelIRI[] mass = GlobalVarLn.list_PelIRI.ToArray();
                            mass[iii].Pel2 = -1;
                            GlobalVarLn.list_PelIRI = mass.ToList();

                            if (
                                (GlobalVarLn.list_PelIRI[iii].Pel1 == -1) &&
                                (GlobalVarLn.list_PelIRI[iii].Pel2 == -1) &&
                                (GlobalVarLn.list_PelIRI[iii].Lat == -1) &&
                                (GlobalVarLn.list_PelIRI[iii].Long == -1)
                               )
                            {
                                GlobalVarLn.list_PelIRI.Remove(GlobalVarLn.list_PelIRI[iii]);
                            }
                        }
                        //------------------------------------------------------------------------

                        // Убрать с карты
                        //GlobalVarLn.axMapScreenGlobal.Repaint();

                    } // Нажата птичка для отрисовки пеленгов
                    // --------------------------------------------------------------------------------------


                }

            }  // -1 SP2
            // ..................................................................
            // PU

            if (
                //(fl == true) &&
               (GlobalVarLn.bCheckGNSS) &&
                //777Otl
                ((coordsGNSS_1[2].Lat == -1) || (coordsGNSS_1[2].Lon == -1))
              )
            {
                // раньше были координаты (свои/GPS)
                if (
                   (GlobalVarLn.XPoint2GRAD_Dubl_Sost != -1) &&
                   (GlobalVarLn.YPoint2GRAD_Dubl_Sost != -1)
                   )
                {
                    GlobalVarLn.XPoint2GRAD_Sost = -1;
                    GlobalVarLn.YPoint2GRAD_Sost = -1;
                    GlobalVarLn.XPoint2GRAD_Dubl_Sost = -1;
                    GlobalVarLn.YPoint2GRAD_Dubl_Sost = -1;
                    GlobalVarLn.XPoint2_Sost = 0;
                    GlobalVarLn.YPoint2_Sost = 0;
                    GlobalVarLn.HPoint2_Sost = 0;

                    objLF.X_m = 0;
                    objLF.Y_m = 0;
                    objLF.H_m = 0;

                    // 13_09_2018
                    //objLF.indzn = 0;
                    // !!! Значки читаем с INI файла
                    GlobalVarLn.indz3_Sost = iniRW.get_ZnakPU();
                    GlobalVarLn.objFormSostG.pictureBox2.Image = GlobalVarLn.objFormSostG.imageList3.Images[GlobalVarLn.indz3_Sost];
                    objLF.indzn = GlobalVarLn.indz3_Sost;

                    GlobalVarLn.list1_Sost[2] = objLF;

                    GlobalVarLn.flCoordYS2_Sost = 0;

                    fpu_1 = 1;
                    GlobalVarLn.objFormSostG.tbPt2XRect.Text = "";
                    GlobalVarLn.objFormSostG.tbPt2YRect.Text = "";
                    GlobalVarLn.objFormSostG.tbPt2XRect42.Text = "";
                    GlobalVarLn.objFormSostG.tbPt2YRect42.Text = "";
                    GlobalVarLn.objFormSostG.tbPt2BRad.Text = "";
                    GlobalVarLn.objFormSostG.tbPt2LRad.Text = "";
                    GlobalVarLn.objFormSostG.tbPt2BMin1.Text = "";
                    GlobalVarLn.objFormSostG.tbPt2LMin1.Text = "";
                    GlobalVarLn.objFormSostG.tbPt2BDeg2.Text = "";
                    GlobalVarLn.objFormSostG.tbPt2BMin2.Text = "";
                    GlobalVarLn.objFormSostG.tbPt2BSec.Text = "";
                    GlobalVarLn.objFormSostG.tbPt2LDeg2.Text = "";
                    GlobalVarLn.objFormSostG.tbPt2LMin2.Text = "";
                    GlobalVarLn.objFormSostG.tbPt2LSec.Text = "";
                    //GlobalVarLn.objFormSostG.tbOwnHeight.Text = "";
                    //GlobalVarLn.objFormSostG.tbPt1Height.Text = "";
                    GlobalVarLn.objFormSostG.tbPt2Height.Text = "";

                }

            }  // -1 PU
            // ..................................................................

            if ((fsp1_1 == 1) || (fsp2_1 == 1) || (fpu_1 == 1))
            {
                // Убрать с карты
                GlobalVarLn.axMapScreenGlobal.Repaint();
                // Redraw
                ClassMap.f_Map_Redraw_Sost();

            }
            // ..................................................................

            // ------------------------------------------------------------------------
            // Checked==true

            if (
                (GlobalVarLn.bCheckGNSS) &&
                (
                //777Otl
                ((coordsGNSS_1[0].Lat != -1) && (coordsGNSS_1[0].Lon != -1)) ||
                ((coordsGNSS_1[1].Lat != -1) && (coordsGNSS_1[1].Lon != -1)) ||
                ((coordsGNSS_1[2].Lat != -1) && (coordsGNSS_1[2].Lon != -1))

                )
                )
            {
                // .......................................................................
                // 1-й раз

                if ((GlobalVarLn.fl_First_GPS == 0))
                {

                    GlobalVarLn.fl_First_GPS = 1;

                    // SP1
                    //777Otl
                    if ((coordsGNSS_1[0].Lat != -1) && (coordsGNSS_1[0].Lon != -1))
                    {
                        //777Otl
                        GlobalVarLn.XCenterGRAD_Sost = coordsGNSS_1[0].Lat;
                        GlobalVarLn.YCenterGRAD_Sost = coordsGNSS_1[0].Lon;

                        GlobalVarLn.XCenterGRAD_Dubl_Sost = GlobalVarLn.XCenterGRAD_Sost;
                        GlobalVarLn.YCenterGRAD_Dubl_Sost = GlobalVarLn.YCenterGRAD_Sost;
                    }

                    // SP2
                    //777Otl
                    //if ((GlobalVarLn.lll[1].Lat != -1) && (GlobalVarLn.lll[1].Long != -1))
                    if ((coordsGNSS_1[1].Lat != -1) && (coordsGNSS_1[1].Lon != -1))
                    {
                        GlobalVarLn.XPoint1GRAD_Sost = coordsGNSS_1[1].Lat;
                        GlobalVarLn.YPoint1GRAD_Sost = coordsGNSS_1[1].Lon;

                        GlobalVarLn.XPoint1GRAD_Dubl_Sost = GlobalVarLn.XPoint1GRAD_Sost;
                        GlobalVarLn.YPoint1GRAD_Dubl_Sost = GlobalVarLn.YPoint1GRAD_Sost;
                    }

                    // PU
                    //777Otl
                    if ((coordsGNSS_1[2].Lat != -1) && (coordsGNSS_1[2].Lon != -1))
                    {
                        //777Otl
                        GlobalVarLn.XPoint2GRAD_Sost = coordsGNSS_1[2].Lat;
                        GlobalVarLn.YPoint2GRAD_Sost = coordsGNSS_1[2].Lon;

                        GlobalVarLn.XPoint2GRAD_Dubl_Sost = GlobalVarLn.XPoint2GRAD_Sost;
                        GlobalVarLn.YPoint2GRAD_Dubl_Sost = GlobalVarLn.YPoint2GRAD_Sost;
                    }

                    ClassMap.f_GetGPS();

                } // 1-й раз
                // .......................................................................
                // НЕ 1-й раз

                else
                {

                    int fsp1 = 0;
                    int fsp2 = 0;
                    int fpu = 0;

                    // ''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    // SP1
                    //777Otl
                    if ((coordsGNSS_1[0].Lat != -1) && (coordsGNSS_1[0].Lon != -1))
                    {
                        //Положение SP1 изменилось
                        //777Otl
                        //if (
                        //   (
                        //    (coordsGNSS_1[0].Lat != GlobalVarLn.XCenterGRAD_Dubl_Sost) ||
                        //    (coordsGNSS_1[0].Lon != GlobalVarLn.YCenterGRAD_Dubl_Sost)

                        //   ) 
                        //   //(GlobalVarLn.flTmrGPS == 1)
                        //  )
                        {
                            fsp1 = 1;
                            //777Otl
                            GlobalVarLn.XCenterGRAD_Sost = coordsGNSS_1[0].Lat;
                            GlobalVarLn.YCenterGRAD_Sost = coordsGNSS_1[0].Lon;

                            GlobalVarLn.XCenterGRAD_Dubl_Sost = GlobalVarLn.XCenterGRAD_Sost;
                            GlobalVarLn.YCenterGRAD_Dubl_Sost = GlobalVarLn.YCenterGRAD_Sost;

                        } // Положение SP1 изменилось

                    } // SP1
                    // ''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    // SP2

                    //777Otl
                    if ((coordsGNSS_1[1].Lat != -1) && (coordsGNSS_1[1].Lon != -1))
                    {
                        //Положение SP2 изменилось
                        /*
                        if (
                           (
                            //777Otl
                            (coordsGNSS_1[1].Lat != GlobalVarLn.XPoint1GRAD_Dubl_Sost) ||
                            (coordsGNSS_1[1].Lon != GlobalVarLn.YPoint1GRAD_Dubl_Sost)

                           ) 
                           //(GlobalVarLn.flTmrGPS == 1)
                          )
                         */
                        {
                            fsp2 = 1;
                            //777Otl
                            GlobalVarLn.XPoint1GRAD_Sost = coordsGNSS_1[1].Lat;
                            GlobalVarLn.YPoint1GRAD_Sost = coordsGNSS_1[1].Lon;

                            GlobalVarLn.XPoint1GRAD_Dubl_Sost = GlobalVarLn.XPoint1GRAD_Sost;
                            GlobalVarLn.YPoint1GRAD_Dubl_Sost = GlobalVarLn.YPoint1GRAD_Sost;

                        } // Положение SP2 изменилось

                    } // SP2
                    // ''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    // PU

                    //777Otl
                    if ((coordsGNSS_1[2].Lat != -1) && (coordsGNSS_1[2].Lon != -1))
                    {
                        //Положение PU изменилось
                        //if (
                        //   (
                        //    //777Otl
                        //    (coordsGNSS_1[2].Lat != GlobalVarLn.XPoint2GRAD_Dubl_Sost) ||
                        //    (coordsGNSS_1[2].Lon != GlobalVarLn.YPoint2GRAD_Dubl_Sost)

                        //   ) 
                        //   //(GlobalVarLn.flTmrGPS == 1)
                        //  )
                        {
                            fpu = 1;
                            //777Otl
                            GlobalVarLn.XPoint2GRAD_Sost = coordsGNSS_1[2].Lat;
                            GlobalVarLn.YPoint2GRAD_Sost = coordsGNSS_1[2].Lon;

                            GlobalVarLn.XPoint2GRAD_Dubl_Sost = GlobalVarLn.XPoint2GRAD_Sost;
                            GlobalVarLn.YPoint2GRAD_Dubl_Sost = GlobalVarLn.YPoint2GRAD_Sost;

                        } // Положение PU изменилось

                    } // PU
                    // ''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                    if ((fsp1 == 1) || (fsp2 == 1) || (fpu == 1))
                    {
                        ClassMap.f_GetGPS();
                    }


                } // НЕ 1-й раз
                // .......................................................................

            } // if (Checked == true&& Coord!=-1)
            // ------------------------------------------------------------------------

            // ******************************************************************* NEW




        } // Button4

        // ************************************************************************************ LENA
        // Обработчик кнопки Button5: Azimuth
        // *****************************************************************************************
        private void button5_Click(object sender, EventArgs e)
        {
            // -------------------------------------------------------------------------------------
            //GlobalVarLn.f_Open_objFormAz = 1;
            //ClassMap.f_RemoveFrm(3);

            GlobalVarLn.f_Open_objFormAz1 = 1;
            ClassMap.f_RemoveFrm(8);
            // -------------------------------------------------------------------------------------

            if (objFormAz1 == null || objFormAz1.IsDisposed)
            {
                objFormAz1 = new FormAz1(ref axaxcMapScreen1);
                objFormAz1.Show();
            }
            else
            {
                objFormAz1.Show();
            }


        } // Button5
        // *****************************************************************************************

        // *****************************************************************************************
        // Обработчик кнопки Button6: Маршрут
        // *****************************************************************************************

        private void button6_Click(object sender, EventArgs e)
        {

            // -------------------------------------------------------------------------------------
            GlobalVarLn.f_Open_objFormWay = 1;
            ClassMap.f_RemoveFrm(2);
            // -------------------------------------------------------------------------------------

            if (objFormWay == null || objFormWay.IsDisposed)
            {
                objFormWay = new FormWay(ref axaxcMapScreen1);
                objFormWay.Show();

            // SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
            // Для маршрута
            // 1-я загрузка формы

            // .....................................................................................
            // Очистка dataGridView

                objFormWay.dataGridView1.ClearSelection();
                for (int i = 0; i < GlobalVarLn.sizeDatWay_stat; i++)
                {
                    objFormWay.dataGridView1.Rows.Add("", "", "");
                }
           // .....................................................................................
                if (objFormWay.chbWay.Checked == true)
                {
                    GlobalVarLn.blWay_stat = true;
                    GlobalVarLn.flEndWay_stat = 1;
                }

                // way
                //Array.Clear(GlobalVarLn.mas_LW_stat, 0, 10000);
                //Array.Clear(GlobalVarLn.mas_XW_stat, 0, 10000);
                //Array.Clear(GlobalVarLn.mas_YW_stat, 0, 10000);
                GlobalVarLn.list_way.Clear();

                GlobalVarLn.iW_stat = 0;
                GlobalVarLn.X_StartW_stat = 0;
                GlobalVarLn.Y_StartW_stat = 0;
                GlobalVarLn.LW_stat = 0;
            // .....................................................................................

            // SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS

            }
           // -------------------------------------------------------------------------------------
            else
            {
                objFormWay.Show();

                // SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
                // Для маршрута
                // НЕ 1-я загрузка формы

                if (objFormWay.chbWay.Checked == true)
                {
                    GlobalVarLn.blWay_stat = true;
                    GlobalVarLn.flEndWay_stat = 1;
                }
                // SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS

            }
            // -------------------------------------------------------------------------------------


        } // Button6
        // *****************************************************************************************

        // *****************************************************************************************
        // Обработчик кнопки Button7: Расчет энергодоступности по зонам 
        // *****************************************************************************************

        private void button7_Click(object sender, EventArgs e)
        {

/*
            // -------------------------------------------------------------------------------------
            GlobalVarLn.f_Open_objZonePowerAvail = 1;
            ClassMap.f_RemoveFrm(5);
            // -------------------------------------------------------------------------------------

            if (objZonePowerAvail == null || objZonePowerAvail.IsDisposed)
            {
                objZonePowerAvail = new ZonePowerAvail(ref axaxcMapScreen1);
                objZonePowerAvail.Show();
            }
            else
            {
                objZonePowerAvail.Show();
            }
*/

            // -------------------------------------------------------------------------------------
            GlobalVarLn.f_Open_objFormSuppression = 1;
            ClassMap.f_RemoveFrm(9);
            // -------------------------------------------------------------------------------------

            if (objFormSuppression == null || objFormSuppression.IsDisposed)
            {
                objFormSuppression = new FormSuppression(ref axaxcMapScreen1);
                objFormSuppression.Show();
            }
            else
            {
                objFormSuppression.Show();
            }
            // -------------------------------------------------------------------------------------

        } // Button7
        // *****************************************************************************************

        // *****************************************************************************************
        // Обработчик кнопки Button8: Расчет энергодоступности по зонам для УС
        // *****************************************************************************************

        private void button8_Click(object sender, EventArgs e)
        {
            // -------------------------------------------------------------------------------------
            GlobalVarLn.f_Open_ObjCommPowerAvail = 1;
            ClassMap.f_RemoveFrm(6);
            // -------------------------------------------------------------------------------------

            if (ObjCommPowerAvail == null || ObjCommPowerAvail.IsDisposed)
            {
                ObjCommPowerAvail = new Form2(ref axaxcMapScreen1);
                ObjCommPowerAvail.Show();
            }
            else
            {
                ObjCommPowerAvail.Show();
            }

            //Form2 ObjCommPowerAvail = new Form2(ref axaxcMapScreen1);
            //ObjCommPowerAvail.Show();


        } // Button8
        // *****************************************************************************************

        // *****************************************************************************************
        // Обработчик кнопки Button9: Самолеты
        // *****************************************************************************************

//        private void button9_Click(object sender, EventArgs e)
//        {




/*
            if (ObjFormAirPlane == null || ObjFormAirPlane.IsDisposed)
            {
                ObjFormAirPlane = new FormAirPlane(ref axaxcMapScreen1);
                ObjFormAirPlane.Show();
            }
            else
            {
                ObjFormAirPlane.Show();
            }
*/
            //FormAirPlane ObjFormAirPlane = new FormAirPlane(ref axaxcMapScreen1);
            //ObjFormAirPlane.Show();

//        } // Button9
        // *****************************************************************************************

        // *****************************************************************************************
        // Обработчик кнопки Button10: ЗПВ
        // *****************************************************************************************

        private void button10_Click(object sender, EventArgs e)
        {
            // -------------------------------------------------------------------------------------
            GlobalVarLn.f_Open_ObjFormLineSightRange = 1;
            ClassMap.f_RemoveFrm(4);
            // -------------------------------------------------------------------------------------

            if (ObjFormLineSightRange == null || ObjFormLineSightRange.IsDisposed)
            {
                ObjFormLineSightRange = new FormLineSightRange(ref axaxcMapScreen1);
                ObjFormLineSightRange.Show();
            }
            else
            {
                ObjFormLineSightRange.Show();
            }


        } // Button10
        // *****************************************************************************************
        // Воздушная обстановка
        // *****************************************************************************************

        private void chbair_CheckedChanged(object sender, EventArgs e)
        {
            blAirObject = chbair.Checked;

            if (blAirObject == false)
            {
                GlobalVarLn.fl_AirPlaneVisible = 0;
                // Убрать с карты
                GlobalVarLn.axMapScreenGlobal.Repaint();
            }

            else if (blAirObject == true)
            {
                GlobalVarLn.fl_AirPlaneVisible = 1;
            }

            //chbair1.Checked = blAirObject;

        }
        // *****************************************************************************************

        //2501
        private void chbPeleng_CheckedChanged(object sender, EventArgs e)
        {
            if (chbPeleng.Checked == false)
            {
                // 14_09_2018
                GlobalVarLn.PrevP1 = -1;
                GlobalVarLn.PrevP2 = -1;

                GlobalVarLn.flPelMain = 0;
                GlobalVarLn.flPelMain2 = 0;

                // 14_09_2018
                GlobalVarLn.flag_eq_draw1 = 0;
                GlobalVarLn.flag_eq_draw2 = 0;

                // 14_09_2018
                GlobalVarLn.fl_PelIRI_1 = -1;
                //------------------------------------------------------------------------
                for (int iii = (GlobalVarLn.list_PelIRI.Count - 1); iii >= 0; iii--)
                {


                    PelIRI[] mass = GlobalVarLn.list_PelIRI.ToArray();
                    mass[iii].Pel1 = -1;
                    mass[iii].Pel2 = -1;
                    GlobalVarLn.list_PelIRI = mass.ToList();

                    if (
                        (GlobalVarLn.list_PelIRI[iii].Pel1 == -1) &&
                        (GlobalVarLn.list_PelIRI[iii].Pel2 == -1) &&
                        (GlobalVarLn.list_PelIRI[iii].Lat == -1) &&
                        (GlobalVarLn.list_PelIRI[iii].Long == -1)
                       )
                    {
                        GlobalVarLn.list_PelIRI.Remove(GlobalVarLn.list_PelIRI[iii]);
                    }
                }
                //------------------------------------------------------------------------

                // Убрать с карты
                GlobalVarLn.axMapScreenGlobal.Repaint();
            }

            else 
            {
                GlobalVarLn.flPelMain = 1;
                // 14_09_2018
                GlobalVarLn.flag_eq_draw1 = 1;
                GlobalVarLn.flag_eq_draw2 = 1;

            }

        } // ZPel




        // *****************************************************************************************
        // Слои
        // *****************************************************************************************

        private void составКартыToolStripMenuItem_Click(object sender, EventArgs e)
        {

            if (axaxMapSelDlg.Execute(axaxcMapScreen1.ViewSelect, false) == true)
                axaxcMapScreen1.Invalidate();
        } // Слои

        // *****************************************************************************************

        // *****************************************************************************************
        // Ввод состава комплекса
        // *****************************************************************************************
        private void button12_Click(object sender, EventArgs e)
        {
            GlobalVarLn.f_Open_objFormSost = 1;
            ClassMap.f_RemoveFrm(1);

            if (objFormSost == null || objFormSost.IsDisposed)
            {
                objFormSost = new FormSost(ref axaxcMapScreen1);
                objFormSost.Show();
            }
            else
            {
                objFormSost.Show();
            }


            //ClassMap.f_LoadSostIni();

        } // Sostav
        // *****************************************************************************************

        // *****************************************************************************************
        // Зоны пеленгования
        // *****************************************************************************************
        private void bZoneDirectFind_Click(object sender, EventArgs e)
        {
            // -------------------------------------------------------------------------------------
            GlobalVarLn.f_Open_objFormBearing = 1;
            ClassMap.f_RemoveFrm(7);
            // -------------------------------------------------------------------------------------

            if (objFormBearing == null || objFormBearing.IsDisposed)
            {
                objFormBearing = new FormBearing(ref axaxcMapScreen1);
                objFormBearing.Show();
            }
            else
            {
                objFormBearing.Show();
            }
        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            ;
        }

        private void toolStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

       // **************************************************************************************       
       // навестись на АСП
       // **************************************************************************************       

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            double lt = 0;
            double ln = 0;

            lt = GlobalVarLn.XCenter_Sost;
            ln = GlobalVarLn.YCenter_Sost;


            if (
                ((GlobalVarLn.XCenter_Sost == 0) || (GlobalVarLn.YCenter_Sost == 0))&&
                ((GlobalVarLn.XPoint1_Sost == 0) || (GlobalVarLn.YPoint1_Sost == 0))&&
                ((GlobalVarLn.XPoint1_Sost == 0) || (GlobalVarLn.YPoint1_Sost == 0))
               )
            {
                if (GlobalVarLn.fl_Azb == 0)
                {
                    MessageBox.Show("Нет координат");
                }
                else
                {
                    //Azb?
                    MessageBox.Show("Koordinatlar yoxdur");
                }
                return;
            }
            
            // Расстояние в м на карте -> пикселы на изображении
            mapPlaneToPicture(GlobalVarLn.hmapl, ref lt, ref ln);

            axaxcMapScreen1.MapLeft = (int)(lt - axaxcMapScreen1.Width / 2);
            axaxcMapScreen1.MapTop = (int)(ln - axaxcMapScreen1.Height / 2);

            // ................................................................................
            //Sect  АСП

            int fff = 0;
            if ((GlobalVarLn.flsect == 1) && (chbSect.Checked == true))
            {
                fff = 1;
                GlobalVarLn.flsect = 0;
            }

            axaxcMapScreen1.Repaint();

            //Sect
            if (fff == 1)
            {
                GlobalVarLn.flsect = 1;
                ClassMap.Sector((double)GlobalVarLn.XCenter_Sost, (double)GlobalVarLn.YCenter_Sost);
            }
            // ................................................................................

        } // Point_ASP
        // **************************************************************************************       



        //
        private void MapForm_Scroll(object sender, ScrollEventArgs e)
        {
            ;
        }

        // ***************************************************************************************
        // ScrollBar
        // ??? Приходит два раза подряд 
        // e.wPARAM=0(Up)/1(Down)/8(2-й раз)???


        // ---------------------------------------------------------------------------------------
        private void axaxcMapScreen1_OnVScroll(object sender, IaxMapScreenEvents_OnVScrollEvent e)
        {
            //int i = 0;
            //GlobalVarLn.shyyy += 1;
            //e.lPARAM = e.lPARAM;
            //e.wPARAM = e.wPARAM;
            //e.msgResult = e.msgResult;
            //e.message = e.message;
            //if(e.wPARAM==8)
            //  i = 10;
            //if (GlobalVarLn.shyyy >= 10)
            //    i = 20;

            
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
/*
            GlobalVarLn.xxxY++;

            if (GlobalVarLn.xxxY == 1)
            {
                GlobalVarLn.fl_scrb = 1;
                //GlobalVarLn.axMapScreenGlobal.Repaint();
            }


            if (GlobalVarLn.xxxY == 2)
            {
                GlobalVarLn.xxxY = 0;
                GlobalVarLn.axMapScreenGlobal.Repaint();
            }
 */
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            //Sect VScroll

            //int fff = 0;
            //if ((GlobalVarLn.flsect == 1) && (chbSect.Checked == true))
            //{
            //    fff = 1;
            //    GlobalVarLn.flsect = 0;
            //}

            //axaxcMapScreen1.Repaint();

            //Sect
            //if (fff == 1)
            //{
            //    GlobalVarLn.flsect = 1;
            //    ClassMap.Sector((double)GlobalVarLn.XCenter_Sost, (double)GlobalVarLn.YCenter_Sost);
            //}
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

            if ((chbSect.Checked == true) && (GlobalVarLn.fl_PelMain == 1))
                GlobalVarLn.flscrolsect = 1;

            if ((chbSect.Checked == true) && (GlobalVarLn.fl_PelMain == 0))
            {
                axaxcMapScreen1.Repaint();
            }

        }
        // ---------------------------------------------------------------------------------------

        private void axaxcMapScreen1_OnHScroll(object sender, IaxMapScreenEvents_OnHScrollEvent e)
        {
            //int i = 0;
            //GlobalVarLn.shyyy += 1;
            //e.lPARAM = e.lPARAM;
            //e.wPARAM = e.wPARAM;
            //e.masResult = e.masResult;
            //e.message = e.message;
            //if(e.wPARAM!=0)
            //i = 10;
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
/*
            GlobalVarLn.xxxY1++;
            if (GlobalVarLn.xxxY1 == 1)
            {
                GlobalVarLn.fl_scrb = 1;
            }
            if (GlobalVarLn.xxxY1 == 2)
            {
                GlobalVarLn.xxxY1 = 0;
                GlobalVarLn.axMapScreenGlobal.Repaint();
            }
 */
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            //Sect HScroll

            //int fff = 0;
            //if ((GlobalVarLn.flsect == 1) && (chbSect.Checked == true))
            //{
             //   fff = 1;
             //   GlobalVarLn.flsect = 0;
            //}

            //axaxcMapScreen1.Repaint();

            //Sect
            //if (fff == 1)
            //{
            //    GlobalVarLn.flsect = 1;
            //    ClassMap.Sector((double)GlobalVarLn.XCenter_Sost, (double)GlobalVarLn.YCenter_Sost);
            //}
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

            if ((chbSect.Checked == true) && (GlobalVarLn.fl_PelMain == 1))
                GlobalVarLn.flscrolsect = 1;

            if ((chbSect.Checked == true)&&(GlobalVarLn.fl_PelMain==0))
            {
               axaxcMapScreen1.Repaint();
            }
        }
        // ---------------------------------------------------------------------------------------

        // ***************************************************************************************

        private void MapForm_MouseDown(object sender, MouseEventArgs e)
        {
            ;
        }

        // **************************************************************************************       


        // ************************************************************************************ LENA

        // LinaPasha************************************************************************************ 
        Contract.IService service;

        private void ServiceAPMCreate()
        {
            var scf = new ChannelFactory<Contract.IService>(new NetTcpBinding(), "net.tcp://localhost:8001");
            service = scf.CreateChannel();
        }

        private void SendMapIsOpenToAPM(bool isOpen)
        {
            //if (MapIsOpenned)
                TrySendToAPM(() => service.MapIsOpen(isOpen));
        }

        void objFormSost_ClickGetGNSS(double Lat, double Lon, double Alt)
        {
            USR_DLL.TCoordsGNSS[] tCoordsGNSS = new USR_DLL.TCoordsGNSS[1];
            tCoordsGNSS[0].Lat = Lat;
            tCoordsGNSS[0].Lon = Lon;
            tCoordsGNSS[0].Alt = Alt;
            SendCoordsGNSSToAPM(tCoordsGNSS);
        }

        void objFormSost_ClickGetGNSS2(double Lat, double Lon, double Alt)
        {
            USR_DLL.TCoordsGNSS[] tCoordsGNSS = new USR_DLL.TCoordsGNSS[2];
            tCoordsGNSS[1].Lat = Lat;
            tCoordsGNSS[1].Lon = Lon;
            tCoordsGNSS[1].Alt = Alt;
            SendCoordsGNSSToAPM(tCoordsGNSS);
        }

        private void SendCoordsGNSSToAPM(USR_DLL.TCoordsGNSS[] tCoordsGNSS)
        {
            //if (MapIsOpenned)
            TrySendToAPM(() => service.MapCurrentCoords(tCoordsGNSS));
        }

        private void TrySendToAPM(Action action)
        {
            Task.Run(() =>
            {
                try
                {
                    action();
                }
                catch
                {
                    //создаем клиент
                    var scf = new ChannelFactory<Contract.IService>(new NetTcpBinding(), "net.tcp://localhost:8001");
                    service = scf.CreateChannel();

                    try
                    {
                        action();
                    }
                    catch
                    {
                    }
                }
            });
        }
        // ************************************************************************************LinaPasha 

        private void LanguageChooser()
        {
            var cont = this.Controls;

            if (NumberOfLanguage.Equals(0))
            {
                ChangeLanguageToRu(cont);
                this.Text = "Карта";
            }
            if (NumberOfLanguage.Equals(1))
            {
                //ChangeLanguageToEng(cont);
            }
            if (NumberOfLanguage.Equals(2))
            {
                ChangeLanguageToAzer(cont);
                this.Text = "Xəritə";
            }
        }

        private void ChangeLanguageToRu(System.Windows.Forms.Control.ControlCollection cont)
        {
            button1.Text = "Check";
            toolTip1.SetToolTip(button10, " Зона прямой видимости (ЗПВ)");
            button11.Text = "Слои";
            toolTip1.SetToolTip(button12, "Состав комплекса");
            button2.Text = "XYZ";
            toolTip1.SetToolTip(button2, "Координаты");
            button3.Text = "Пел";
            button4.Text = "S";
            toolTip1.SetToolTip(button5, "Азимут");
            //button6.Text = "";
            toolTip1.SetToolTip(button6, "Маршрут");
            toolTip1.SetToolTip(button7, "Расчет энергодоступности по зонам");
            toolTip1.SetToolTip(button8, "Расчет энергодоступности по узлам связи");
            toolTip1.SetToolTip(bZoneDirectFind, "Зона ошибок местоопределения");
            chbair.Text = "Воздушная обстановка";
            chbPeleng.Text = "Пеленг";
            closeHeightMatrixToolStripMenuItem.Text = "Закрыть матрицу высот";
            closeMapToolStripMenuItem.Text = "Закрыть карту";
            fileToolStripMenuItem.Text = "Карта";
            menuStrip1.Text = "menuStrip1";
            openHeightMatrixToolStripMenuItem.Text = "Открыть матрицу высот";
            openMapToolStripMenuItem.Text = "Открыть карту";
            statusStrip1.Text = "statusStrip1";
            toolStripButton1.Text = "АСП";
            ZoomIn.Text = "Увеличить масштаб";
            ZoomInitial.Text = "Исходный масштаб";
            ZoomOut.Text = "Уменьшить масштаб";
            настройкиToolStripMenuItem.Text = "Настройки";
            составКартыToolStripMenuItem.Text = "Состав карты";
            chbSect.Text = "Направления антенн";
        }

        private void ChangeLanguageToAzer(System.Windows.Forms.Control.ControlCollection cont)
        {

            button1.Text = "Check";
            toolTip1.SetToolTip(button10, "Birbaşa görünüş zonası");
            button11.Text = "Laylar";
            toolTip1.SetToolTip(button12, "Kompleksin tərkibi ");
            button2.Text = "XYZ";
            toolTip1.SetToolTip(button2, "Koordinatlar");
            button3.Text = "Pelenq";
            button4.Text = "S";
            toolTip1.SetToolTip(button5, "Azimut ");
            //button6.Text = "";
            toolTip1.SetToolTip(button6, "Marşrut");
            toolTip1.SetToolTip(button7, "Zonalar üzrə qidalandırılmanın hesabatı");
            toolTip1.SetToolTip(button8, "Rabitə üzrə elektrik gücünün bölüşdürülməs hesabı");
            toolTip1.SetToolTip(bZoneDirectFind, "Yerin müəyyənləşdirilməsində səhv sahəsi");

            chbair.Text = "Hava vəziyyəti";
            chbPeleng.Text = "Pelenq";
            closeHeightMatrixToolStripMenuItem.Text = "Hündürlük matrisasını bağla";
            closeMapToolStripMenuItem.Text = "Xəritəni bağla";
            fileToolStripMenuItem.Text = "Xəritə";
            menuStrip1.Text = "menuStrip1";
            openHeightMatrixToolStripMenuItem.Text = "Hündürlük matrisalarını aç ";
            openMapToolStripMenuItem.Text = "Xəritəni aç";
            statusStrip1.Text = "statusStrip1";
            toolStripButton1.Text = "AMS";
            ZoomIn.Text = "Miqyası böyüt";
            ZoomInitial.Text = "Ilkin vəziyyət";
            ZoomOut.Text = "Miqyası azalt";

            настройкиToolStripMenuItem.Text = "Tənzimləmə";
            составКартыToolStripMenuItem.Text = "Xəritənin tərkibi";
            chbSect.Text = "anten istiqaməti";
        }

        // SECTOR_EVENTS ********************************************************************
        //Sect Events

        private void MapForm_AutoSizeChanged(object sender, EventArgs e)
        {
            int i = 0;
            i = i;
        }

        // --------------------------------------------------------------------------------------
        // Нажатие в верхнем правом углу формы квадратика "Растянуть на весь экран" (WindowsState=Maximized)
        // Нажатие в верхнем правом углу формы квадратикa "Стянуть" (WindowsState=Normal)
        // Нажатие в верхнем правом углу формы черточки "Стянуть вниз экрана" (WindowsState=Minimized)
        // Вернуть форму с низа экрана на место (WindowsState=Normal/Maximized)
        // --------------------------------------------------------------------------------------

        private void MapForm_Resize(object sender, EventArgs e)
        {

            // ................................................................................
            //Sect 

            //if ((GlobalVarLn.flsect == 1) && (chbSect.Checked == true))
            if (chbSect.Checked == true)
            {
                // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

                if (this.WindowState == FormWindowState.Maximized)
                {
                    // Нажатие в верхнем правом углу формы квадратика "Растянуть на весь экран"
                    if ((GlobalVarLn.ffstate == 0) || (GlobalVarLn.ffstate == 3))
                    {
                        GlobalVarLn.flsect = 0;
                        axaxcMapScreen1.Repaint();
                        GlobalVarLn.flsect = 1;
                        ClassMap.Sector((double)GlobalVarLn.XCenter_Sost, (double)GlobalVarLn.YCenter_Sost);

                        GlobalVarLn.ffstate = 1;
                        return;
                    }

                    // Вернуть форму с низа экрана на место
                    if (GlobalVarLn.ffstate == 2)
                    {

                        GlobalVarLn.flsect = 0;
                        axaxcMapScreen1.Repaint();
                        GlobalVarLn.flsect = 1;
                        ClassMap.Sector((double)GlobalVarLn.XCenter_Sost, (double)GlobalVarLn.YCenter_Sost);

                        GlobalVarLn.ffstate = 1;
                        return;
                    }


                } // Max

                // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                else if (this.WindowState == FormWindowState.Normal)
                {
                    // Нажатие в верхнем правом углу формы квадратика "Стянуть экран обратно"
                    if (GlobalVarLn.ffstate == 1)
                    {
                        GlobalVarLn.flsect = 0;
                        axaxcMapScreen1.Repaint();
                        GlobalVarLn.flsect = 1;
                        ClassMap.Sector((double)GlobalVarLn.XCenter_Sost, (double)GlobalVarLn.YCenter_Sost);

                        GlobalVarLn.ffstate = 3;
                        return;
                    }

                    // Вернуть форму с низа экрана на место
                    if (GlobalVarLn.ffstate == 2)
                    {
                        GlobalVarLn.flsect = 0;
                        axaxcMapScreen1.Repaint();
                        GlobalVarLn.flsect = 1;
                        ClassMap.Sector((double)GlobalVarLn.XCenter_Sost, (double)GlobalVarLn.YCenter_Sost);

                        GlobalVarLn.ffstate = 3;
                        return;
                    }

                } // Norm
                // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                else if (this.WindowState == FormWindowState.Minimized)
                {
                    //GlobalVarLn.flsect = 0;
                    //axaxcMapScreen1.Repaint();
                    //GlobalVarLn.flsect = 1;
                    //ClassMap.Sector((double)GlobalVarLn.XCenter_Sost, (double)GlobalVarLn.YCenter_Sost);

                    // Нажатие в верхнем правом углу формы черточки "Отправить экран вниз"
                    GlobalVarLn.ffstate = 2;
                    return;

                } // Norm
                // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

            }
            // ................................................................................

        } // Resize

        // --------------------------------------------------------------------------------------
        // Растягивание(стягивание) формы стрелкой
        // --------------------------------------------------------------------------------------
        //Sect
        // Начало действия
        private void MapForm_ResizeBegin(object sender, EventArgs e)
        {
/*
            if ((GlobalVarLn.flsect == 1) && (chbSect.Checked == true))
            {
                GlobalVarLn.flsect = 0;
                axaxcMapScreen1.Repaint();
                GlobalVarLn.flsect = 1;
                ClassMap.Sector((double)GlobalVarLn.XCenter_Sost, (double)GlobalVarLn.YCenter_Sost);
            }
*/
        }
        // Конец действия
        private void MapForm_ResizeEnd(object sender, EventArgs e)
        {
            if ((GlobalVarLn.flsect == 1) && (chbSect.Checked == true))
            {
                    GlobalVarLn.flsect = 0;
                    axaxcMapScreen1.Repaint();
                    GlobalVarLn.flsect = 1;
                    ClassMap.Sector((double)GlobalVarLn.XCenter_Sost, (double)GlobalVarLn.YCenter_Sost);

            }

        }
        // --------------------------------------------------------------------------------------



        private void MapForm_MouseHover(object sender, EventArgs e)
        {
            ;
        }

        private void panel1_MouseHover(object sender, EventArgs e)
        {
            ;
        }

        private void button12_MouseHover(object sender, EventArgs e)
        {
            ;

        }

        private void button12_MouseEnter(object sender, EventArgs e)
        {
            ;
        }

        private void button12_MouseLeave(object sender, EventArgs e)
        {
            if ((GlobalVarLn.flsect == 1) && (chbSect.Checked == true))
            {
               //GlobalVarLn.flsect = 0;
                //axaxcMapScreen1.Repaint();
                //GlobalVarLn.flsect = 1;
                //ClassMap.Sector((double)GlobalVarLn.XCenter_Sost, (double)GlobalVarLn.YCenter_Sost);
            }

        }

        private void button12_MouseMove(object sender, MouseEventArgs e)
        {
            ;
        }

        private void panel1_MouseLeave(object sender, EventArgs e)
        {
            ;
        }

        private void panel3_MouseEnter(object sender, EventArgs e)
        {
            ;
        }

        private void MapForm_MouseEnter(object sender, EventArgs e)
        {
            if ((GlobalVarLn.flsect == 1) && (chbSect.Checked == true))
            {
                //GlobalVarLn.flsect = 0;
                //axaxcMapScreen1.Repaint();
                //GlobalVarLn.flsect = 1;
                //ClassMap.Sector((double)GlobalVarLn.XCenter_Sost, (double)GlobalVarLn.YCenter_Sost);
            }

        }

        private void View_Click(object sender, EventArgs e)
        {
            //Console.WriteLine(axaxcMapScreen1.ViewSelect.Layers_get(0));
            //Console.WriteLine(axaxcMapScreen1.ViewSelect.Layers_get(1));
            //Console.WriteLine(axaxcMapScreen1.ViewSelect.Layers_get(2));
            //Console.WriteLine(axaxcMapScreen1.ViewSelect.Layers_get(3));
            //Console.WriteLine(axaxcMapScreen1.ViewSelect.Layers_get(4));
            //Console.WriteLine(axaxcMapScreen1.ViewSelect.Layers_get(5));
            //Console.WriteLine(axaxcMapScreen1.ViewSelect.Layers_get(6));
            //Console.WriteLine(axaxcMapScreen1.ViewSelect.Layers_get(7));
            //Console.WriteLine(axaxcMapScreen1.ViewSelect.Layers_get(8));
            //Console.WriteLine(axaxcMapScreen1.ViewSelect.Layers_get(9));
            //Console.WriteLine(axaxcMapScreen1.ViewSelect.Layers_get(10));
            //Console.WriteLine(axaxcMapScreen1.ViewSelect.Layers_get(11));
            //Console.WriteLine(axaxcMapScreen1.ViewSelect.Layers_get(12));
            //Console.WriteLine(axaxcMapScreen1.ViewSelect.Layers_get(13));
            //Console.WriteLine(axaxcMapScreen1.ViewSelect.Layers_get(14));
            //Console.WriteLine(axaxcMapScreen1.ViewSelect.Layers_get(15));
            //Console.WriteLine(axaxcMapScreen1.ViewSelect.Layers_get(16));
            //Console.WriteLine(axaxcMapScreen1.ViewSelect.Layers_get(17));
            //Console.WriteLine(axaxcMapScreen1.ViewSelect.Layers_get(18));
            //Console.WriteLine(axaxcMapScreen1.ViewSelect.Layers_get(19));
            SimpleView(View.Checked);
        }

        private void SimpleView(bool value)
        {
            axaxcMapScreen1.Selecting = true;

            axaxcMapScreen1.ViewSelect.Layers_set(0, !value);
            axaxcMapScreen1.ViewSelect.Layers_set(1, !value);
            axaxcMapScreen1.ViewSelect.Layers_set(2, !value);
            axaxcMapScreen1.ViewSelect.Layers_set(3, !value);
            axaxcMapScreen1.ViewSelect.Layers_set(4, !value);
            axaxcMapScreen1.ViewSelect.Layers_set(5, !value);
            axaxcMapScreen1.ViewSelect.Layers_set(6, !value);
            axaxcMapScreen1.ViewSelect.Layers_set(7, !value);
            axaxcMapScreen1.ViewSelect.Layers_set(8, true);
            axaxcMapScreen1.ViewSelect.Layers_set(9, !value);
            axaxcMapScreen1.ViewSelect.Layers_set(10, !value);
            axaxcMapScreen1.ViewSelect.Layers_set(11, true);
            axaxcMapScreen1.ViewSelect.Layers_set(12, !value);
            axaxcMapScreen1.ViewSelect.Layers_set(13, !value);
            axaxcMapScreen1.ViewSelect.Layers_set(14, !value);
            axaxcMapScreen1.ViewSelect.Layers_set(15, true);
            axaxcMapScreen1.ViewSelect.Layers_set(16, !value);
            axaxcMapScreen1.ViewSelect.Layers_set(17, true);
            axaxcMapScreen1.ViewSelect.Layers_set(18, !value);
            axaxcMapScreen1.ViewSelect.Layers_set(19, !value);

            // Отключение подписей рек
            axaxcMapScreen1.ViewSelect.set_KeyObject("T0091082000", !value);
            axaxcMapScreen1.ViewSelect.set_KeyObject("T00910820001", !value);
            axaxcMapScreen1.ViewSelect.set_KeyObject("T00910820002", !value);
            axaxcMapScreen1.ViewSelect.set_KeyObject("T00910820003", !value);
            axaxcMapScreen1.ViewSelect.set_KeyObject("T00910820004", !value);
            axaxcMapScreen1.ViewSelect.set_KeyObject("T00910820005", !value);
            axaxcMapScreen1.ViewSelect.set_KeyObject("T00910820006", !value);
            axaxcMapScreen1.ViewSelect.set_KeyObject("T00910820007", !value);
            axaxcMapScreen1.ViewSelect.set_KeyObject("T00910820008", !value);
            axaxcMapScreen1.ViewSelect.set_KeyObject("T00910820009", !value);
            axaxcMapScreen1.ViewSelect.set_KeyObject("T009108200010", !value);
            axaxcMapScreen1.ViewSelect.set_KeyObject("T009108200011", !value);
            axaxcMapScreen1.ViewSelect.set_KeyObject("T009108200012", !value);
            axaxcMapScreen1.ViewSelect.set_KeyObject("T009108200013", !value);
            axaxcMapScreen1.ViewSelect.set_KeyObject("T009108200014", !value);
            axaxcMapScreen1.ViewSelect.set_KeyObject("T009108200015", !value);
            axaxcMapScreen1.ViewSelect.set_KeyObject("T009108200016", !value);
            axaxcMapScreen1.ViewSelect.set_KeyObject("T009108200017", !value);

            //Отключение рек и озёр
            axaxcMapScreen1.ViewSelect.set_KeyObject("L0031410000", !value);
            axaxcMapScreen1.ViewSelect.set_KeyObject("L00314100001", !value);
            axaxcMapScreen1.ViewSelect.set_KeyObject("L0031431110", !value);
            axaxcMapScreen1.ViewSelect.set_KeyObject("L0031431120", !value);
            axaxcMapScreen1.ViewSelect.set_KeyObject("L0031431300", !value);

            axaxcMapScreen1.ViewSelect.set_KeyObject("S0031120000", !value);
            axaxcMapScreen1.ViewSelect.set_KeyObject("S00311200001", !value);
            axaxcMapScreen1.ViewSelect.set_KeyObject("S0031131000", !value);
            axaxcMapScreen1.ViewSelect.set_KeyObject("S00311310001", !value);
            axaxcMapScreen1.ViewSelect.set_KeyObject("S0031410000", !value);
            axaxcMapScreen1.ViewSelect.set_KeyObject("S00314100001", !value);
            axaxcMapScreen1.ViewSelect.set_KeyObject("T0092172000", !value);

            // Отключение мелких городов
            //axaxcMapScreen1.ViewSelect.set_KeyObject("P00410000006", !value);

            //Отключение подписей гор
            axaxcMapScreen1.ViewSelect.set_KeyObject("T0091050000", !value);
            axaxcMapScreen1.ViewSelect.set_KeyObject("T00910500001", !value);
            axaxcMapScreen1.ViewSelect.set_KeyObject("T00910500002", !value);
            axaxcMapScreen1.ViewSelect.set_KeyObject("T00910500003", !value);
            axaxcMapScreen1.ViewSelect.set_KeyObject("T00910500004", !value);
            axaxcMapScreen1.ViewSelect.set_KeyObject("T00910500005", !value);
            axaxcMapScreen1.ViewSelect.set_KeyObject("T00910500006", !value);
            axaxcMapScreen1.ViewSelect.set_KeyObject("T00910500007", !value);
            axaxcMapScreen1.ViewSelect.set_KeyObject("T00910500008", !value);
            axaxcMapScreen1.ViewSelect.set_KeyObject("T00910500009", !value);
            axaxcMapScreen1.ViewSelect.set_KeyObject("T009105000010", !value);
            axaxcMapScreen1.ViewSelect.set_KeyObject("T00921700001", !value);
            axaxcMapScreen1.ViewSelect.set_KeyObject("T0092170000", !value);
            axaxcMapScreen1.ViewSelect.set_KeyObject("T00910600009", !value);

            // Отключение подписей мелких городов
            axaxcMapScreen1.ViewSelect.set_KeyObject("T0091040000", !value);
            axaxcMapScreen1.ViewSelect.set_KeyObject("T00910400001", !value);
            //axaxcMapScreen1.ViewSelect.set_KeyObject("T00910400002", !value);
            //axaxcMapScreen1.ViewSelect.set_KeyObject("T00910400003", !value);
            axaxcMapScreen1.ViewSelect.set_KeyObject("T00910400004", !value);
            axaxcMapScreen1.ViewSelect.set_KeyObject("T00910400005", !value);
            axaxcMapScreen1.ViewSelect.set_KeyObject("T00910400006", !value);
            axaxcMapScreen1.ViewSelect.set_KeyObject("T00910400007", !value);
            axaxcMapScreen1.ViewSelect.set_KeyObject("T00910400008", !value);
            axaxcMapScreen1.ViewSelect.set_KeyObject("T00910400009", !value);
            axaxcMapScreen1.ViewSelect.set_KeyObject("T0091200000", !value);
            //axaxcMapScreen1.ViewSelect.set_KeyObject("T0091120000", !value);
            //axaxcMapScreen1.ViewSelect.set_KeyObject("T0091170000", !value);

            // Отключение подписей чего-то
            axaxcMapScreen1.ViewSelect.set_KeyObject("T0091130000", !value);

            // Отключение подписей островов
            axaxcMapScreen1.ViewSelect.set_KeyObject("T0093140000", !value);
            axaxcMapScreen1.ViewSelect.set_KeyObject("T00931400004", !value);

            //axaxcMapScreen1.ScreenRepaint
            axaxcMapScreen1.Repaint();
        }

        // --------------------------------------------------------------------------------------











        // ******************************************************************** SECTOR_EVENTS



    } // Class
} // namespace
