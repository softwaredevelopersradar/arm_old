﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GrozaMap
{
    class iniRW
    {
        [DllImport("kernel32")]
        private static extern long WritePrivateProfileString(string section, string key, string val, string filePath);
        [DllImport("kernel32")]
        private static extern long WritePrivateProfileSection(string section, string val, string filePath);
        [DllImport("kernel32")]
        private static extern int GetPrivateProfileString(string section, string key, string def, StringBuilder retVal, int size, string filePath);

        public static string get_IPaddress()
        {
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("Connect", "IPaddress", "0", temp, 255, Application.StartupPath + "\\Setting.ini");
            return temp.ToString();
        }

        public static int get_Port()
        {
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("Connect", "Port", "0", temp, 255, Application.StartupPath + "\\Setting.ini");
            return int.Parse(temp.ToString());
        }



        // otl33
        public static int get_AdressOwn()
        {
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("Common", "AdressOwn", "0", temp, 255, Application.StartupPath + "\\Setting.ini");
            return int.Parse(temp.ToString());
        }
        public static int get_MLT()
        {
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("Position", "Lat", "0", temp, 255, Application.StartupPath + "\\InitPoz.ini");
            return int.Parse(temp.ToString());
        }
        public static int get_MLN()
        {
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("Position", "Long", "0", temp, 255, Application.StartupPath + "\\InitPoz.ini");
            return int.Parse(temp.ToString());
        }



        public static int get_AdressLinked()
        {
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("Common", "AdressLinked", "0", temp, 255, Application.StartupPath + "\\Setting.ini");
            return int.Parse(temp.ToString());
        }


        public static void write_map_inf(int mapLeft, int mapTop, int scale, string path)
        {
            WritePrivateProfileString("Position", "MapLeft", mapLeft.ToString(), Application.StartupPath + "\\Setting.ini");
            WritePrivateProfileString("Position", "MapTop", mapTop.ToString(), Application.StartupPath + "\\Setting.ini");
            WritePrivateProfileString("Position", "Scale", scale.ToString(), Application.StartupPath + "\\Setting.ini");
            WritePrivateProfileString("Map", "Path", path, Application.StartupPath + "\\Setting.ini");
        }

        public static void write_map_inf(int mapLeft, int mapTop, int scale)
        {
            WritePrivateProfileString("Position", "MapLeft", mapLeft.ToString(), Application.StartupPath + "\\Setting.ini");
            WritePrivateProfileString("Position", "MapTop", mapTop.ToString(), Application.StartupPath + "\\Setting.ini");
            WritePrivateProfileString("Position", "Scale", scale.ToString(), Application.StartupPath + "\\Setting.ini");
        }
/*
        public static string get_map_path()
        {
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("Map", "Path", "", temp, 255, Application.StartupPath + "\\Setting.ini");
            return temp.ToString();
        }
*/
        public static string get_matrix_path()
        {
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("Map", "PathMatrix", "", temp, 255, Application.StartupPath + "\\Setting.ini");
            return temp.ToString();
        }

// NEW ************************************************************************************************************************

        public static int get_X_ASP()
        {
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("CoordinatesASP", "X", "0", temp, 255,Application.StartupPath + "\\Settings.ini");
            return int.Parse(temp.ToString());
        }

        public static int get_Y_ASP()
        {
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("CoordinatesASP", "Y", "0", temp, 255, Application.StartupPath + "\\Settings.ini");
            return int.Parse(temp.ToString());
        }

        public static int get_H_ASP()
        {
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("CoordinatesASP", "H", "0", temp, 255, Application.StartupPath + "\\Settings.ini");
            return int.Parse(temp.ToString());
        }

        public static int get_Sign_ASP()
        {
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("CoordinatesASP", "Sign", "0", temp, 255, Application.StartupPath + "\\Settings.ini");
            return int.Parse(temp.ToString());
        }

        public static int get_X_ASPS()
        {
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("CoordinatesASPS", "X", "0", temp, 255, Application.StartupPath + "\\Settings.ini");
            return int.Parse(temp.ToString());
        }

        public static int get_Y_ASPS()
        {
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("CoordinatesASPS", "Y", "0", temp, 255, Application.StartupPath + "\\Settings.ini");
            return int.Parse(temp.ToString());
        }

        public static int get_H_ASPS()
        {
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("CoordinatesASPS", "H", "0", temp, 255, Application.StartupPath + "\\Settings.ini");
            return int.Parse(temp.ToString());
        }

        public static int get_Sign_ASPS()
        {
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("CoordinatesASPS", "Sign", "0", temp, 255, Application.StartupPath + "\\Settings.ini");
            return int.Parse(temp.ToString());
        }

        // 19_11_2018
        public static int get_X_PU()
        {
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("CoordinatesPU", "X", "0", temp, 255, Application.StartupPath + "\\Settings.ini");
            return int.Parse(temp.ToString());
        }

        public static int get_Y_PU()
        {
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("CoordinatesPU", "Y", "0", temp, 255, Application.StartupPath + "\\Settings.ini");
            return int.Parse(temp.ToString());
        }

        public static int get_H_PU()
        {
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("CoordinatesPU", "H", "0", temp, 255, Application.StartupPath + "\\Settings.ini");
            return int.Parse(temp.ToString());
        }

        public static int get_Sign_PU()
        {
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("CoordinatesPU", "Sign", "0", temp, 255, Application.StartupPath + "\\Settings.ini");
            return int.Parse(temp.ToString());
        }

        public static void write_XY_ASP(int X, int Y, int H, int Sign)
        {
            WritePrivateProfileString("CoordinatesASP", "X", X.ToString(), Application.StartupPath + "\\Settings.ini");
            WritePrivateProfileString("CoordinatesASP", "Y", Y.ToString(), Application.StartupPath + "\\Settings.ini");
            WritePrivateProfileString("CoordinatesASP", "H", H.ToString(), Application.StartupPath + "\\Settings.ini");
            WritePrivateProfileString("CoordinatesASP", "Sign", Sign.ToString(), Application.StartupPath + "\\Settings.ini");
        }

        public static void write_XY_ASPS(int X, int Y, int H, int Sign)
        {
            WritePrivateProfileString("CoordinatesASPS", "X", X.ToString(), Application.StartupPath + "\\Settings.ini");
            WritePrivateProfileString("CoordinatesASPS", "Y", Y.ToString(), Application.StartupPath + "\\Settings.ini");
            WritePrivateProfileString("CoordinatesASPS", "H", H.ToString(), Application.StartupPath + "\\Settings.ini");
            WritePrivateProfileString("CoordinatesASPS", "Sign", Sign.ToString(), Application.StartupPath + "\\Settings.ini");
        }

        public static void write_XY_PU(int X, int Y, int H, int Sign)
        {
            WritePrivateProfileString("CoordinatesPU", "X", X.ToString(), Application.StartupPath + "\\Settings.ini");
            WritePrivateProfileString("CoordinatesPU", "Y", Y.ToString(), Application.StartupPath + "\\Settings.ini");
            WritePrivateProfileString("CoordinatesPU", "H", H.ToString(), Application.StartupPath + "\\Settings.ini");
            WritePrivateProfileString("CoordinatesPU", "Sign", Sign.ToString(), Application.StartupPath + "\\Settings.ini");
        }


        public static int get_fl_Azb()
        {
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("Fl_Azb", "fl_Azb", "0", temp, 255, Application.StartupPath + "\\Init.ini");
            return int.Parse(temp.ToString());
        }

        public static string get_map_path()
        {
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("Map", "Path", "", temp, 255, Application.StartupPath + "\\Init.ini");
            return temp.ToString();
        }

        public static string get_map_mtw()
        {
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("Map", "MTW", "", temp, 255, Application.StartupPath + "\\Init.ini");
            return temp.ToString();
        }

// ************************************************************************************************************************ NEW


        // SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
        //Sect

        // ------------------------------------------------------------------------------------------------------
        // Цвета сектора

        public static int get_Col1_1()
        {
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("Antenna", "Col1_1", "0", temp, 255, Application.StartupPath + "\\Settings.ini");
            return int.Parse(temp.ToString());
        }
        public static int get_Col1_2()
        {
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("Antenna", "Col1_2", "0", temp, 255, Application.StartupPath + "\\Settings.ini");
            return int.Parse(temp.ToString());
        }
        public static int get_Col1_3()
        {
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("Antenna", "Col1_3", "0", temp, 255, Application.StartupPath + "\\Settings.ini");
            return int.Parse(temp.ToString());
        }
        public static int get_Col2_1()
        {
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("Antenna", "Col2_1", "0", temp, 255, Application.StartupPath + "\\Settings.ini");
            return int.Parse(temp.ToString());
        }
        public static int get_Col2_2()
        {
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("Antenna", "Col2_2", "0", temp, 255, Application.StartupPath + "\\Settings.ini");
            return int.Parse(temp.ToString());
        }
        public static int get_Col2_3()
        {
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("Antenna", "Col2_3", "0", temp, 255, Application.StartupPath + "\\Settings.ini");
            return int.Parse(temp.ToString());
        }
        public static int get_Col3_1()
        {
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("Antenna", "Col3_1", "0", temp, 255, Application.StartupPath + "\\Settings.ini");
            return int.Parse(temp.ToString());
        }
        public static int get_Col3_2()
        {
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("Antenna", "Col3_2", "0", temp, 255, Application.StartupPath + "\\Settings.ini");
            return int.Parse(temp.ToString());
        }
        public static int get_Col3_3()
        {
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("Antenna", "Col3_3", "0", temp, 255, Application.StartupPath + "\\Settings.ini");
            return int.Parse(temp.ToString());
        }
        public static int get_Col4_1()
        {
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("Antenna", "Col4_1", "0", temp, 255, Application.StartupPath + "\\Settings.ini");
            return int.Parse(temp.ToString());
        }
        public static int get_Col4_2()
        {
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("Antenna", "Col4_2", "0", temp, 255, Application.StartupPath + "\\Settings.ini");
            return int.Parse(temp.ToString());
        }
        public static int get_Col4_3()
        {
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("Antenna", "Col4_3", "0", temp, 255, Application.StartupPath + "\\Settings.ini");
            return int.Parse(temp.ToString());
        }
        public static int get_Col5_1()
        {
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("Antenna", "Col5_1", "0", temp, 255, Application.StartupPath + "\\Settings.ini");
            return int.Parse(temp.ToString());
        }
        public static int get_Col5_2()
        {
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("Antenna", "Col5_2", "0", temp, 255, Application.StartupPath + "\\Settings.ini");
            return int.Parse(temp.ToString());
        }
        public static int get_Col5_3()
        {
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("Antenna", "Col5_3", "0", temp, 255, Application.StartupPath + "\\Settings.ini");
            return int.Parse(temp.ToString());
        }

        // ------------------------------------------------------------------------------------------------------
        // Угловая величина сектора (град)

        public static int get_Sect1()
        {
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("Antenna", "Sect1", "0", temp, 255, Application.StartupPath + "\\Settings.ini");
            return int.Parse(temp.ToString());
        }

        public static int get_Sect2()
        {
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("Antenna", "Sect2", "0", temp, 255, Application.StartupPath + "\\Settings.ini");
            return int.Parse(temp.ToString());
        }

        public static int get_Sect3()
        {
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("Antenna", "Sect3", "0", temp, 255, Application.StartupPath + "\\Settings.ini");
            return int.Parse(temp.ToString());
        }

        public static int get_Sect4()
        {
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("Antenna", "Sect4", "0", temp, 255, Application.StartupPath + "\\Settings.ini");
            return int.Parse(temp.ToString());
        }

        public static int get_Sect5()
        {
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("Antenna", "Sect5", "0", temp, 255, Application.StartupPath + "\\Settings.ini");
            return int.Parse(temp.ToString());
        }
        // ------------------------------------------------------------------------------------------------------
        // Радиус сектора

        public static int get_RSect()
        {
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("Antenna", "R", "0", temp, 255, Application.StartupPath + "\\Settings.ini");
            return int.Parse(temp.ToString());
        }
        // ------------------------------------------------------------------------------------------------------
/*
        public static void write_flSect(int f1, int f2, int f3, int f4, int f5)
        {
            WritePrivateProfileString("Antenna", "FS1", f1.ToString(), Application.StartupPath + "\\Settings.ini");
            WritePrivateProfileString("Antenna", "FS2", f2.ToString(), Application.StartupPath + "\\Settings.ini");
            WritePrivateProfileString("Antenna", "FS3", f3.ToString(), Application.StartupPath + "\\Settings.ini");
            WritePrivateProfileString("Antenna", "FS4", f4.ToString(), Application.StartupPath + "\\Settings.ini");
            WritePrivateProfileString("Antenna", "FS5", f5.ToString(), Application.StartupPath + "\\Settings.ini");

        }
*/

        // Знак SP1
        public static int get_ZnakSP1()
        {
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("ZNAK", "ind1", "0", temp, 255, Application.StartupPath + "\\Settings.ini");
            return int.Parse(temp.ToString());
        }
        // Знак SP2
        public static int get_ZnakSP2()
        {
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("ZNAK", "ind2", "0", temp, 255, Application.StartupPath + "\\Settings.ini");
            return int.Parse(temp.ToString());
        }
        // Знак PU
        public static int get_ZnakPU()
        {
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("ZNAK", "ind3", "0", temp, 255, Application.StartupPath + "\\Settings.ini");
            return int.Parse(temp.ToString());
        }
        public static void write_ZnakSP1(int Sign)
        {
            WritePrivateProfileString("ZNAK", "ind1", Sign.ToString(), Application.StartupPath + "\\Settings.ini");
        }
        public static void write_ZnakSP2(int Sign)
        {
            WritePrivateProfileString("ZNAK", "ind2", Sign.ToString(), Application.StartupPath + "\\Settings.ini");
        }
        public static void write_ZnakPU(int Sign)
        {
            WritePrivateProfileString("ZNAK", "ind3", Sign.ToString(), Application.StartupPath + "\\Settings.ini");
        }
        // SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS


    }
}
