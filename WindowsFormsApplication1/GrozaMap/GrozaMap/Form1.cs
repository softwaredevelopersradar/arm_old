﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AxaxGisToolKit;
using axGisToolKit;
using System.Runtime.InteropServices;
using System.Windows.Forms;


namespace GrozaMap
{
    public partial class Form1 : Form
    {


        [DllImport("gisacces.dll")]
        public static extern int mapPlaneToGeoWGS84(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        public static extern int mapPlaneToPicture(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        public static extern int mapPictureToPlane(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        public static extern int mapPlaneToGeo(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        static extern int mapGeoToPlane(int hmap, ref double Bx, ref double Ly);


        //private MapForm objMapForm1;
        private AxaxcMapScreen axaxcMapScreen;


        // Переменные VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR 
        public double dchislo;
        public long ichislo;

        // ......................................................................
        // Координаты

        public double X_Coordl;
        public double Y_Coordl;
        public double Lat_Coordl;
        public double Long_Coordl;

        public double Grad_Dbl_Coordl;
        public int Grad_I_Coordl;
        public int Min_Coordl;
        public double Sec_Coordl;

        // DATUM
        public double dX_Coord_datum;
        public double dY_Coord_datum;
        public double dZ_Coord_datum;

        // пересчет Панорамы, град
        public double Lat_WGS84_Pan;
        public double Long_WGS84_Pan;
        public double Lat_GEO_Pan;
        public double Long_GEO_Pan;

        // Приращения по долготе,широтепри пересчете координат
        public double dLong_Coord;
        public double dLat_Coord;

        // Перевод координат WGS84(эллипсоид) -> эллипсоид Красовского(Пулково-42) 
        public double Lat_Coord_Vyx_8442;   // широта
        public double Long_Coord_Vyx_8442;  // долгота

        // Преобразование геодезических координат (широта, долгота, высота) 
        // эллипсоида Красовского (СК42) в плоские прямоугольные координаты в
        // проекции Гаусса-Крюгера и обратно
        public double X_Coord_Kr;
        public double Y_Coord_Kr;
        // .......................................................................
        // Для отрисовки точки на карте

        public double Lat_Draw;
        public double Long_Draw;
        // ......................................................................
        // ......................................................................


        // VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR Переменные

        // Конструктор *********************************************************** 

        public Form1(ref AxaxcMapScreen axaxcMapScreen1)
        {
          InitializeComponent();

          axaxcMapScreen = axaxcMapScreen1;
          

          dchislo=0;
          ichislo=0;
        // ......................................................................
        // Координаты

        X_Coordl=0;
        Y_Coordl=0;
        Lat_Coordl=0;
        Long_Coordl=0;

        Grad_Dbl_Coordl=0;
        Grad_I_Coordl=0;
        Min_Coordl=0;
        Sec_Coordl=0;

        // DATUM
        // ГОСТ 51794_2008
        dX_Coord_datum = 25;
        dY_Coord_datum = -141;
        dZ_Coord_datum = -80;

        // пересчет Панорамы, град
        Lat_WGS84_Pan=0;
        Long_WGS84_Pan=0;
        Lat_GEO_Pan=0;
        Long_GEO_Pan=0;

        // Приращения по долготе,широтепри пересчете координат
        dLong_Coord=0;
        dLat_Coord=0;

        // Перевод координат WGS84(эллипсоид) -> эллипсоид Красовского(Пулково-42) 
        Lat_Coord_Vyx_8442=0;   // широта
        Long_Coord_Vyx_8442=0;  // долгота

        // Преобразование геодезических координат (широта, долгота, высота) 
        // эллипсоида Красовского (СК42) в плоские прямоугольные координаты в
        // проекции Гаусса-Крюгера и обратно
        X_Coord_Kr=0;
        Y_Coord_Kr=0;

        // ......................................................................
        // Для отрисовки точки на карте

        Lat_Draw=0;
        Long_Draw=0;

        //LatDX=0;
        //LongDY=0;

        // ......................................................................


        } // Конструктор
        // ***********************************************************  Конструктор


        // *****************************************************************************************
        // Обработчик кнопки Button19: отобразить координаты
        // *****************************************************************************************

        private void button19_Click(object sender, EventArgs e)
        {

            // -------------------------------------------------------------------------------------
              ClassMap objClassMap1 = new ClassMap();
              ClassMap objClassMap2 = new ClassMap();
              ClassMap objClassMap3 = new ClassMap();

            // -------------------------------------------------------------------------------------
            // DATUM
            // m (ГОСТ 51794-2008)

             dX_Coord_datum = Convert.ToDouble(textBox31.Text);
             dY_Coord_datum = Convert.ToDouble(textBox30.Text);
             dZ_Coord_datum = Convert.ToDouble(textBox29.Text);
            // -------------------------------------------------------------------------------------

            // PLANE *******************************************************************************
            // Отображение координат карты, полученных кликом мыши на карте
            // !!! MapX1,MapY1 -> реальные координаты на местности карты в м (Plane)

            // m->km
             X_Coordl = GlobalVarLn.MapX1 / 1000;
             Y_Coordl = GlobalVarLn.MapY1 / 1000;

            ichislo = (long)(X_Coordl * 10000000);
            dchislo = ((double)ichislo) / 10000000;
            textBox2.Text = Convert.ToString(dchislo);   // X, карта

            ichislo = (long)(Y_Coordl * 10000000);
            dchislo = ((double)ichislo) / 10000000;
            textBox1.Text = Convert.ToString(dchislo);   // Y, карта

            // ******************************************************************************* PLANE

            // mapPlaneToGeoWGS84 *****************************************************************
            // Реальные координаты карты в м -> в долготу и широту с помощью функций Панорамы
            // !!! Выход функции(rad) идет на место входных переменных

            X_Coordl = GlobalVarLn.MapX1;
            Y_Coordl = GlobalVarLn.MapY1;

            mapPlaneToGeoWGS84(GlobalVarLn.hmapl, ref X_Coordl, ref Y_Coordl);

            // rad->grad
            Lat_Coordl = (X_Coordl * 180) / Math.PI;
            Long_Coordl = (Y_Coordl * 180) / Math.PI;
            Lat_WGS84_Pan = Lat_Coordl;
            Long_WGS84_Pan = Long_Coordl;

            ichislo = (long)(Lat_Coordl * 10000000);
            dchislo = ((double)ichislo) / 10000000;
            textBox3.Text = Convert.ToString(dchislo);   // Широта, град

            ichislo = (long)(Long_Coordl * 10000000);
            dchislo = ((double)ichislo) / 10000000;
            textBox4.Text = Convert.ToString(dchislo);   // Долгота, град
            // .....................................................................................
            // dd.ddddd -> DD MM SS

            // Широта
            objClassMap1.f_Grad_GMS
              (
                // Входные параметры (grad)
                Lat_Coordl,

                // Выходные параметры 
                ref Grad_I_Coordl,
                ref Min_Coordl,
                ref Sec_Coordl

              );

            textBox11.Text = Convert.ToString(Grad_I_Coordl);
            textBox12.Text = Convert.ToString(Min_Coordl);

            ichislo = (long)(Sec_Coordl * 1000);
            dchislo = ((double)ichislo) / 1000;
            textBox13.Text = Convert.ToString(dchislo);   // ss

            // Долгота
            objClassMap1.f_Grad_GMS
              (
                // Входные параметры (grad)
                Long_Coordl,

                // Выходные параметры 
                ref Grad_I_Coordl,
                ref Min_Coordl,
                ref Sec_Coordl

              );

            textBox14.Text = Convert.ToString(Grad_I_Coordl);
            textBox15.Text = Convert.ToString(Min_Coordl);

            ichislo = (long)(Sec_Coordl * 1000);
            dchislo = ((double)ichislo) / 1000;
            textBox16.Text = Convert.ToString(dchislo);   // ss
            // ....................................................................................

            // ***************************************************************** mapPlaneToGeoWGS84

            // mapPlaneToGeo **********************************************************************

            X_Coordl = GlobalVarLn.MapX1;
            Y_Coordl = GlobalVarLn.MapY1;

            mapPlaneToGeo(GlobalVarLn.hmapl, ref X_Coordl, ref Y_Coordl);

            // rad->grad
            Lat_Coordl = (X_Coordl * 180) / Math.PI;
            Long_Coordl = (Y_Coordl * 180) / Math.PI;
            Lat_GEO_Pan = Lat_Coordl;
            Long_GEO_Pan = Long_Coordl;

            ichislo = (long)(Lat_Coordl * 10000000);
            dchislo = ((double)ichislo) / 10000000;
            textBox5.Text = Convert.ToString(dchislo);   // Широта, град

            ichislo = (long)(Long_Coordl * 10000000);
            dchislo = ((double)ichislo) / 10000000;
            textBox6.Text = Convert.ToString(dchislo);   // Долгота, град
            // .....................................................................................
            // dd.ddddd -> DD MM SS

            // Широта
            objClassMap1.f_Grad_GMS
              (
                // Входные параметры (grad)
                Lat_Coordl,

                // Выходные параметры 
                ref Grad_I_Coordl,
                ref Min_Coordl,
                ref Sec_Coordl

              );

            textBox28.Text = Convert.ToString(Grad_I_Coordl);
            textBox27.Text = Convert.ToString(Min_Coordl);

            ichislo = (long)(Sec_Coordl * 1000);
            dchislo = ((double)ichislo) / 1000;
            textBox26.Text = Convert.ToString(dchislo);   // ss

            // Долгота
            objClassMap1.f_Grad_GMS
              (
                // Входные параметры (grad)
                Long_Coordl,

                // Выходные параметры 
                ref Grad_I_Coordl,
                ref Min_Coordl,
                ref Sec_Coordl

              );

            textBox25.Text = Convert.ToString(Grad_I_Coordl);
            textBox24.Text = Convert.ToString(Min_Coordl);

            ichislo = (long)(Sec_Coordl * 1000);
            dchislo = ((double)ichislo) / 1000;
            textBox23.Text = Convert.ToString(dchislo);   // ss
            // .....................................................................................

            // ********************************************************************** mapPlaneToGeo

            // WGS84(эллипсоид)->элл.Красовского ***************************************************
            // Перевод координат WGS84(эллипсоид) -> эллипсоид Красовского(Пулково-42)
            // WGS84(широта,долгота)из Панорамы пересчитываем в эллипсод Красовского моей функцией
            // Входные параметры -> град,km
            // Перевод в рад - внутри функции

            // dLong ..............................................................................
            // Расчет приращения по долготе при преобразованиях координат WGS84<->SK42
            // (преобразования Молоденского), угл.сек

            objClassMap2.f_dLong
                (

                    // Входные параметры (град,км)
                    Lat_WGS84_Pan,   // широта
                    Long_WGS84_Pan,  // долгота
                    0,     // высота

                    // DATUM,m
                    dX_Coord_datum,
                    dY_Coord_datum,
                    dZ_Coord_datum,

                    ref dLong_Coord   // приращение по долготе, угл.сек

                );

            // .................................................................... dLong

            // dLat .....................................................................
            // Расчет приращения по широте при преобразованиях координат WGS84<->SK42
            // (преобразования Молоденского), угл.сек

            objClassMap2.f_dLat
                (

                    // Входные параметры (град,км)
                    Lat_WGS84_Pan,   // широта
                    Long_WGS84_Pan,  // долгота
                    0,     // высота

                    // DATUM,m
                    dX_Coord_datum,
                    dY_Coord_datum,
                    dZ_Coord_datum,

                    ref dLat_Coord        // приращение по долготе, угл.сек

                );

            // ..................................................................... dLat

            // Lat,Long .................................................................
            // Преобразования широты и долготы при пересчете WGS84->SK42

            objClassMap2.f_WGS84_SK42_Lat_Long
                   (

                       // Входные параметры (град,км)
                       Lat_WGS84_Pan,   // широта
                       Long_WGS84_Pan,  // долгота
                       0,     // высота
                       dLat_Coord,       // приращение по долготе, угл.сек
                       dLong_Coord,      // приращение по долготе, угл.сек

                       // Выходные параметры (grad)
                       ref Lat_Coord_Vyx_8442,   // широта
                       ref Long_Coord_Vyx_8442   // долгота

                   );

            // ................................................................ Lat,Long

            // Отображение .............................................................

            // dd.ddddd
            ichislo = (long)(Lat_Coord_Vyx_8442 * 10000000);
            dchislo = ((double)ichislo) / 10000000;
            textBox7.Text = Convert.ToString(dchislo);   // Широта, град
            ichislo = (long)(Long_Coord_Vyx_8442 * 10000000);
            dchislo = ((double)ichislo) / 10000000;
            textBox8.Text = Convert.ToString(dchislo);   // Долгота, град
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            // dd.ddddd -> DD MM SS

            // Широта
            objClassMap2.f_Grad_GMS
              (
                // Входные параметры (grad)
                Lat_Coord_Vyx_8442,

                // Выходные параметры 
                ref Grad_I_Coordl,
                ref Min_Coordl,
                ref Sec_Coordl

              );

            textBox17.Text = Convert.ToString(Grad_I_Coordl);
            textBox18.Text = Convert.ToString(Min_Coordl);

            ichislo = (long)(Sec_Coordl * 1000);
            dchislo = ((double)ichislo) / 1000;
            textBox19.Text = Convert.ToString(dchislo);   // ss

            // Долгота
            objClassMap1.f_Grad_GMS
              (
                // Входные параметры (grad)
                Long_Coord_Vyx_8442,

                // Выходные параметры 
                ref Grad_I_Coordl,
                ref Min_Coordl,
                ref Sec_Coordl

              );

            textBox20.Text = Convert.ToString(Grad_I_Coordl);
            textBox21.Text = Convert.ToString(Min_Coordl);

            ichislo = (long)(Sec_Coordl * 1000);
            dchislo = ((double)ichislo) / 1000;
            textBox22.Text = Convert.ToString(dchislo);   // ss

            // ....................................................................... Отображение

            // *************************************************** WGS84(эллипсоид)->элл.Красовского

            // SK42(элл.)->Крюгер *****************************************************************
            // Преобразование геодезических координат (широта, долгота, высота) 
            // эллипсоида Красовского (СК42) в плоские прямоугольные координаты в
            // проекции Гаусса-Крюгера
            // Lat_Coord_Vyx_8442, Long_Coord_Vyx_8442 -> координаты эллипсоида Красовского,
            // получили моей функцией пересчета из панарамовских координат WGS84

            // Входные параметры -> !!!grad

            objClassMap3.f_SK42_Krug
                   (
                       // Входные параметры (!!! grad)
                       // !!! эллипсоид Красовского
                       Lat_Coord_Vyx_8442,   // широта
                       Long_Coord_Vyx_8442,  // долгота

                       // Выходные параметры (km)
                       ref X_Coord_Kr,
                       ref Y_Coord_Kr

                   );

            // Отображение ........................................................................

            ichislo = (long)(X_Coord_Kr * 10000000);
            dchislo = ((double)ichislo) / 10000000;
            textBox9.Text = Convert.ToString(dchislo);   // X

            ichislo = (long)(Y_Coord_Kr * 10000000);
            dchislo = ((double)ichislo) / 10000000;
            textBox10.Text = Convert.ToString(dchislo);   // Y

            // ........................................................................ Отображение

            // ***************************************************************** SK42(элл.)->Крюгер


        } // Button19

        // *****************************************************************************************

        private void Form1_Load(object sender, EventArgs e)
        {
            // DATUM
            // ГОСТ 51794_2008
            textBox31.Text = "25";       // dX,m
            textBox30.Text = "-141";     // dY,m
            textBox29.Text = "-80";      // dZ,m

            // Координаты для отображения (град)
            // Кривичи
            textBox32.Text = "54,63";       // Lat
            textBox33.Text = "27,24";       // Long

        }
        // *****************************************************************************************

        // Button1: отобразить *********************************************************************
        private void button1_Click(object sender, EventArgs e)
        {

            GlobalVarLn.fl_DrawCoord = 1;
            // -------------------------------------------------------------------------------------
            // долгота и широта (GEO) в град для отрисовки точки на карте

            Lat_Draw  = Convert.ToDouble(textBox32.Text);
            Long_Draw = Convert.ToDouble(textBox33.Text);
            GlobalVarLn.Lat_CRD = Lat_Draw;
            GlobalVarLn.Long_CRD = Long_Draw;
            // -------------------------------------------------------------------------------------

            f_Map_Rect_LatLong1(
                                 Lat_Draw,
                                 Long_Draw
                                 );
  
                                 //ref objMapForm.axaxcMapScreen1);

            //GrozaMap.MapCore.RadioSource radioSource = new MapCore.RadioSource();
            //radioSource.planeX = 0;
            //radioSource.planeY = 0;
            // MapCore.DrawAndSaveTriangle(ref radioSource, ref objMapForm1.axaxcMapScreen1);

        } // Button1
        //  ********************************************************************* Button1: отобразить


        // FUNCTIONS ********************************************************************************

        // ******************************************************************************************
        // Нарисовать метку по координатам (красный прямоугольник)
        //
        // Входные параметры:
        // Lat - широта, град (GEO)
        // Long - долгота, град (GEO)
        // ******************************************************************************************
        public void f_Map_Rect_LatLong1(

                                         double Lat,
                                         double Long
                                        )
        {

            double LatDX, LongDY;

            // -------------------------------------------------------------------------------------
            Graphics graph = axaxcMapScreen.CreateGraphics();

            Pen penRed = new Pen(Color.Red, 2);
            Brush brushRed = new SolidBrush(Color.Red);

            // -------------------------------------------------------------------------------------

            // grad->rad
            LatDX = (Lat * Math.PI) / 180;
            LongDY = (Long * Math.PI) / 180;

            // Подаем rad, получаем там же расстояние на карте в м
            mapGeoToPlane(GlobalVarLn.hmapl, ref LatDX, ref LongDY);

            // Расстояние в м на карте -> пикселы на изображении
            mapPlaneToPicture(GlobalVarLn.hmapl, ref LatDX, ref LongDY);
            // -------------------------------------------------------------------------------------

            if (graph != null)
            {
                graph.FillRectangle(brushRed, (int)LatDX - axaxcMapScreen.MapLeft, (int)LongDY - axaxcMapScreen.MapTop, 7, 7);

                // Надпись
                //TitleObject(typeStation, lat, lon);
            }

            // -------------------------------------------------------------------------------------

            //objMapForm1.graph.Dispose();


        } // P/P f_Map_Rect_LatLong
        // *************************************************************************************

        // *************************************************************************************
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            Hide();

        } 

        // *************************************************************************************


        // ********************************************************************************* FUNCTIONS


    } // Class Form1
} // Namespace
