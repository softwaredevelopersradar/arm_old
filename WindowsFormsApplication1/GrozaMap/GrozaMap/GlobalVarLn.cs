﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AxaxGisToolKit;
using axGisToolKit;
using System.Runtime.InteropServices;
using System.Windows.Forms;

using System.IO;

using System.Diagnostics;
using System.Threading;

using System.Globalization;

namespace GrozaMap
{
    // Структуры ************************************************************


    // ---------------------------------------------------------------------
    //2501

    public struct PelIRI
    {
        public int ID;
        public int flPelMain2;

        // 14_09_2018
        public int fl_PelIRI_1;
        public int flag_eq_draw1; // 0- не рисовать
        public int flag_eq_draw2; // 0- не рисовать

        public double Pel1;
        public double Pel2;
        //public double prevPel1;
        //public double prevPel2;
        //public double prevLat;
        //public double prevLong;

        public double[] arr_Pel1;     // R,Широта,долгота
        public double[] arr_Pel2;     // R,Широта,долгота
        public double Lat;
        public double Long;
        public int AddDel;
        public Color color;

        //public int flGPS_IRI;

        public double XGPS_IRI;
        public double YGPS_IRI;

    }


    // ---------------------------------------------------------------------
    // Структура LF

    public struct LF
    {
        // m на местности
        public double X_m;
        public double Y_m;
        public double H_m;
        public int indzn;
    }
    // ---------------------------------------------------------------------
    // Структура LF

    public struct LF1
    {
        // m на местности
        public double X_m;
        public double Y_m;
        public double R;
        public double Rmin;
        public double Gamma;
        public int Color;
        public int Intensity;
        public double ds;
        public double dh;

    }
    // ---------------------------------------------------------------------

    // Структура маршрута

    public struct Way
    {
        public double X_m;
        public double Y_m;

        public double L_m;

    }
    // ---------------------------------------------------------------------
    // Структура Самолета

    public struct AirPlane
    {
        public double Lat;
        public double Long;
        public double X_m;
        public double Y_m;

        public double Angle;

        public int Num;
        public String sNum;

        public int sh;
        public int fl_sh;

        public Color cl;


    }
    // ---------------------------------------------------------------------
    // Для ЗПВ

    public struct KoordThree
    {
        public double x;
        public double y;
        public double h;
    }
    public struct KoordTwo
    {
        public double x;
        public double y;
    }

    // ************************************************************ Структуры

    class GlobalVarLn
    {

        public static NumberFormatInfo formatG;

        public static Stopwatch stopWatch = new Stopwatch();
        public static Stopwatch stopWatch1 = new Stopwatch();
        public static Stopwatch stopWatch2 = new Stopwatch();

        public static FormBearing objFormBearingG;
        public static FormSost objFormSostG;
        public static FormWay objFormWayG;
        public static FormAz objFormAzG;
        public static FormAz1 objFormAz1G;
        public static FormLineSightRange ObjFormLineSightRangeG;
        public static ZonePowerAvail objZonePowerAvailG;
        public static Form2 ObjCommPowerAvailG;
        public static FormSuppression objFormSuppressionG;
        // ----------------------------------------------------------------------------------------
        public static axGisToolKit.axMapPoint axMapPointGlobalAdd;	   // объект MapPoint для доступа всем классам	
        //public static AxaxGisToolKit.AxaxMapPoint axMapPointGlobal;		// объект MapPoint для доступа всем классам	
        public static AxaxGisToolKit.AxaxcMapScreen axMapScreenGlobal;  // объект MapScreen для доступа всем классам
        // ----------------------------------------------------------------------------------------
        public static double MapX1 = 0;
        public static double MapY1 = 0;
        public static int hmapl = 0;

        public static int ftmr = 0;
        public static TimeSpan ts;
        public static int ftmr1 = 0;
        public static TimeSpan ts1;


        //public static int f_hmapl = 0;
        //public static int hmapl;

        // 1210
        public static bool MousePress = false;
        // ----------------------------------------------------------------------------------------
        // Для высветки снизу

        public static double X_Down_mrel;
        public static double Y_Down_mrel;
        public static double X42_mrel;
        public static double Y42_mrel;

        // ----------------------------------------------------------------------------------------
        // ?????????????????
        // Глубина подавления
        public static int iSupDepth=1500;
        public static int RADIUS_EARTH = 8500000;

        public static int width_SP = 24;
        public static int height_SP = 24;
        // ----------------------------------------------------------------------------------------
        // Координаты (Form1)

        public static int fl_DrawCoord = 0;
        public static double Lat_CRD=0;
        public static double Long_CRD = 0;
        // ----------------------------------------------------------------------------------------
        // Пеленг

        //public static double[] mas_Pel_XYZ = new double[10000]; // XYZ в опорной геоцентрической СК

        public static uint IndFikt_Pel_stat=0;
        public static uint NumbFikt_Pel_stat=0;
        public static int fl_Peleng_stat = 0;
        public static double LatP_Pel_stat=0;
        public static double LongP_Pel_stat=0;

        // Peleng_NEW
        //GPS_PELENG
        public static int flPelMain = 0;
        public static int flPelMain2 = 0;

        public static int X1_PelMain = 0;
        public static int Y1_PelMain = 0;
        public static int X2_PelMain = 0;
        public static int Y2_PelMain = 0;

        public static int X1_1_PelMain = 0;
        public static int Y1_1_PelMain = 0;
        public static int X2_1_PelMain = 0;
        public static int Y2_1_PelMain = 0;

        public static uint numberofdots = 1000;
        public static double distance = 200;
        public static double[] arr_Pel1 = new double[10000];     // R,Широта,долгота
        public static double[] arr_Pel2 = new double[10000];     // R,Широта,долгота


        public static int fl_PelIRI_1 = -1;


        public static double PrevP1 = -1;
        public static double PrevP2 = -1;

        // 28_09_2018
        public static double PELENGG_1 = -1;
        public static double PELENGG_2 = -1;


        // 14_09_2018
        // когда пеленг(ФРЧ) равен предыдущему, но его надо нарисоваьть
        // (станцию убрали, а потом нарисовали, во время работы пришли новые
        //  к-ты станции)
        public static int flag_eq_draw1=0; // 0- не рисовать
        public static int flag_eq_draw2 = 0; // 0- не рисовать

        // .......................................................................................
        // Peleng2
        //2501


        public static List<PelIRI> list_PelIRI = new List<PelIRI>();
        public static List<PelIRI> list_PelIRI2 = new List<PelIRI>();

        // ----------------------------------------------------------------------------------------
        // S

        public static int fl_S1_stat = 0;
        public static int fl_S2_stat = 0;

        // m на местности
        public static double X_Coordl1_stat=0;
        public static double Y_Coordl1_stat=0;
        public static double X_Coordl2_stat=0;
        public static double Y_Coordl2_stat=0;

        // ----------------------------------------------------------------------------------------  
        // Маршрут

        public static int f_Open_objFormWay = 0;

        // Флаг активации маршрута
        public static bool blWay_stat = false;

        // Размер dataGridView
        public static int sizeDatWay_stat = 1000;

        public static string flname_W_stat="";
        public static string flname_R_stat="";

        public static double dchisloW_stat=0;
        public static long ichisloW_stat=0;
        public static double X_Coordl5_stat=0;
        public static double Y_Coordl5_stat=0;

        public static double DXX_W_stat=0;
        public static double DYY_W_stat=0;

        // !!! Флаг окончаня маршрута (устанавливается по кнопке ЗАГРУЗИТЬ)
        public static uint flEndWay_stat = 0;
        //Координаты 
        //public static double[] mas_LW_stat = new double[10000];
        //public static double[] mas_XW_stat = new double[10000];
        //public static double[] mas_YW_stat = new double[10000];

        public static uint iW_stat = 0;
        public static double LW_stat = 0;
        public static double X_StartW_stat = 0;
        public static double Y_StartW_stat = 0;
        public static double X1_W_stat = 0;
        public static double Y1_W_stat = 0;
        public static double X2_W_stat = 0;
        public static double Y2_W_stat = 0;

        public static List<Way> list_way = new List<Way>();
        // ----------------------------------------------------------------------------------------
        // Азимут

        public static int f_Open_objFormAz = 0;

        // ----------------------------------------------------------------------------------------
        
        // Самолеты

        public static int fl_AirPlane = 0;
        public static int fl_AirPlaneVisible = 0;

        public static double Lat_air;
        public static double Long_air;
        public static double Angle_air;
        public static int Num_air = 0;
        public static String sNum_air = "";
        public static int Number_air = 0;
        public static int width_air = 24;
        public static int height_air = 24;

        //public static int ndraw_air = 10;
        public static int ndraw_air = 1;

        public static AirPlane[] mass_stAirPlane = new AirPlane[1000];
        public static List<AirPlane> list_air = new List<AirPlane>();
        // --------------------------------------------------------------------------------------------
        // Зона энергодоступности

        public static int f_Open_objZonePowerAvail = 0;

        public static int fl_ZonePowerAvail = 0;
        public static uint flCoord_ed = 0; // =1-> Выбрали центр ЗПВ

        public static Point tpOwnCoordRect;
        public static long liRadiusZone_ed = 0;
        public static double XCenter_ed = 0;
        public static double YCenter_ed = 0;
        public static double HCenter_ed = 0;

        public static double dLat_comm_ed = 0;
        public static double dLong_comm_ed = 0;
        // Эллипсоид Красовского, град
        public static double LatKrG_comm_ed = 0;
        public static double LongKrG_comm_ed = 0;
        // Эллипсоид Красовского, rad
        public static double LatKrR_comm_ed = 0;
        public static double LongKrR_comm_ed = 0;
        // Эллипсоид Красовского, град,мин,сек
        public static int Lat_Grad_comm_ed = 0;
        public static int Lat_Min_comm_ed = 0;
        public static double Lat_Sec_comm_ed = 0;
        public static int Long_Grad_comm_ed = 0;
        public static int Long_Min_comm_ed = 0;
        public static double Long_Sec_comm_ed = 0;
        // Гаусс-крюгер(СК42) м
        public static double XSP42_comm_ed = 0;
        public static double YSP42_comm_ed = 0;
        // --------------------------------------------------------------------------------------------
        // Энергодоступность по узлам

        public static int f_Open_ObjCommPowerAvail = 0;

        public static int fl_CommPowerAvail = 0;

        public static int fl_NoSup1 = 0;
        public static int fl_NoSup2 = 0;
        public static int fl_ZSupYS = 0;

        public static double R_CommPA=0;

        public static uint flCoordSP_comm = 0; // =1-> Выбрали СП
        public static uint flCoordYS1_comm = 0; // =1-> Выбрали YS1
        public static uint flCoordYS2_comm = 0; // =1-> Выбрали YS2

        public static int Numb_CommPowerAvail = 0;
        public static Point tpOwnCoordRect_comm;
        public static Point tpPoint1Rect;
        public static Point tpPoint2Rect;
        public static double XCenter_comm = 0;
        public static double YCenter_comm = 0;
        public static double HCenter_comm = 0;
        public static double XPoint1_comm = 0;
        public static double YPoint1_comm = 0;
        public static double HPoint1_comm = 0;
        public static double XPoint2_comm = 0;
        public static double YPoint2_comm = 0;
        public static double HPoint2_comm = 0;

        public static double dLat_comm1 = 0;
        public static double dLong_comm1 = 0;
        // Эллипсоид Красовского, град
        public static double LatKrG_comm1 = 0;
        public static double LongKrG_comm1 = 0;
        // Эллипсоид Красовского, rad
        public static double LatKrR_comm1 = 0;
        public static double LongKrR_comm1 = 0;
        // Эллипсоид Красовского, град,мин,сек
        public static int Lat_Grad_comm1 = 0;
        public static int Lat_Min_comm1 = 0;
        public static double Lat_Sec_comm1 = 0;
        public static int Long_Grad_comm1 = 0;
        public static int Long_Min_comm1 = 0;
        public static double Long_Sec_comm1 = 0;
        // Гаусс-крюгер(СК42) м
        public static double XSP42_comm1 = 0;
        public static double YSP42_comm1 = 0;

        // Эллипсоид Красовского, град
        public static double LatKrG_YS1_comm1 = 0;
        public static double LongKrG_YS1_comm1 = 0;
        // Эллипсоид Красовского, rad
        public static double LatKrR_YS1_comm1 = 0;
        public static double LongKrR_YS1_comm1 = 0;
        // Эллипсоид Красовского, град,мин,сек
        public static int Lat_Grad_YS1_comm1 = 0;
        public static int Lat_Min_YS1_comm1 = 0;
        public static double Lat_Sec_YS1_comm1 = 0;
        public static int Long_Grad_YS1_comm1 = 0;
        public static int Long_Min_YS1_comm1 = 0;
        public static double Long_Sec_YS1_comm1 = 0;
        // Гаусс-крюгер(СК42) м
        public static double XYS142_comm1 = 0;
        public static double YYS142_comm1 = 0;

        // Эллипсоид Красовского, град
        public static double LatKrG_YS2_comm1 = 0;
        public static double LongKrG_YS2_comm1 = 0;
        // Эллипсоид Красовского, rad
        public static double LatKrR_YS2_comm1 = 0;
        public static double LongKrR_YS2_comm1 = 0;
        // Эллипсоид Красовского, град,мин,сек
        public static int Lat_Grad_YS2_comm1 = 0;
        public static int Lat_Min_YS2_comm1 = 0;
        public static double Lat_Sec_YS2_comm1 = 0;
        public static int Long_Grad_YS2_comm1 = 0;
        public static int Long_Min_YS2_comm1 = 0;
        public static double Long_Sec_YS2_comm1 = 0;
        // Гаусс-крюгер(СК42) м
        public static double XYS242_comm1 = 0;
        public static double YYS242_comm1 = 0;

        //333
        // WGS84,grad
        public static double BWGS84_1_comm2 = 0;
        public static double LWGS84_1_comm2 = 0;
        public static double BWGS84_2_comm2 = 0;
        public static double LWGS84_2_comm2 = 0;
        public static double BWGS84_3_comm2 = 0;
        public static double LWGS84_3_comm2 = 0;

        // --------------------------------------------------------------------------------------------
        // ЗПВ

        //333
        // WGS84,grad
        public static double BWGS84_ZPV = 0;
        public static double LWGS84_ZPV = 0;

        public static int f_Open_ObjFormLineSightRange = 0;

        public static int fl_LineSightRange = 0;
        public static uint flCoordZPV = 0; // =1-> Выбрали центр ЗПВ
        public static double XCenter_ZPV = 0;
        public static double YCenter_ZPV = 0;
        public static double HCenter_ZPV = 0;
        public static Point tpOwnCoordRect_ZPV;

        public static int iStepAngleInput_ZPV = 1;
        public static int iStepLengthInput_ZPV = 100;

        public static List<Point> listPointDSR = new List<Point>();

        public static Point [] listPointPictDSR;

        public static double dLat_comm_ZPV = 0;
        public static double dLong_comm_ZPV = 0;
        // Эллипсоид Красовского, град
        public static double LatKrG_comm_ZPV = 0;
        public static double LongKrG_comm_ZPV = 0;
        // Эллипсоид Красовского, rad
        public static double LatKrR_comm_ZPV = 0;
        public static double LongKrR_comm_ZPV = 0;
        // Эллипсоид Красовского, град,мин,сек
        public static int Lat_Grad_comm_ZPV = 0;
        public static int Lat_Min_comm_ZPV = 0;
        public static double Lat_Sec_comm_ZPV = 0;
        public static int Long_Grad_comm_ZPV = 0;
        public static int Long_Min_comm_ZPV = 0;
        public static double Long_Sec_comm_ZPV = 0;
        // Гаусс-крюгер(СК42) м
        public static double XSP42_comm_ZPV = 0;
        public static double YSP42_comm_ZPV = 0;

        // Сопряженная пара
        //777*
        public static double XCenter1_ZPV = 0;
        public static double YCenter1_ZPV = 0;
        public static double HCenter1_ZPV = 0;
        public static double XCenter2_ZPV = 0;
        public static double YCenter2_ZPV = 0;
        public static double HCenter2_ZPV = 0;
        public static uint flCoordParaZPV = 0; // =1-> Выбрали центр ЗПВ
        public static List<Point> listPointDSR1 = new List<Point>();
        public static Point[] listPointPictDSR1;
        public static List<Point> listPointDSR2 = new List<Point>();
        public static Point[] listPointPictDSR2;
        public static Point tpOwnCoordRect1_ZPV;
        public static Point tpOwnCoordRect2_ZPV;

        // ---------------------------------------------------------------------------------------
        // Состав

        public static int f_Open_objFormSost = 0;

        public static int fl_Sost = 0;

        public static uint flCoordSP_Sost = 0; // =1-> Выбрали СП
        public static uint flCoordYS1_Sost = 0; // =1-> Выбрали YS1
        public static uint flCoordYS2_Sost = 0; // =1-> Выбрали YS2

        public static int Number_Sost = 3;

        //public static int Numb_CommPowerAvail = 0;
        //public static Point tpOwnCoordRect_Sost;
        //public static Point tpPoint1Rect_Sost;
        //public static Point tpPoint2Rect_Sost;
        public static double XCenter_Sost = 0;
        public static double YCenter_Sost = 0;
        public static double HCenter_Sost = 0;
        public static double XPoint1_Sost = 0;
        public static double YPoint1_Sost = 0;
        public static double HPoint1_Sost = 0;
        public static double XPoint2_Sost = 0;
        public static double YPoint2_Sost = 0;
        public static double HPoint2_Sost = 0;

        // ......................................
        // GPSSPPU
        public static int fl_First_GPS = 0;
        public static int flASP_GPS = 0;
        public static int flASPS_GPS = 0;
        public static int flPU_GPS = 0;
        public static double XCenterGRAD_Sost = -1;
        public static double YCenterGRAD_Sost = -1;
        public static double XPoint1GRAD_Sost = -1;
        public static double YPoint1GRAD_Sost = -1;
        public static double XPoint2GRAD_Sost = -1;
        public static double YPoint2GRAD_Sost = -1;

        public static double XCenterGRAD_Dubl_Sost = -1;
        public static double YCenterGRAD_Dubl_Sost = -1;
        public static double XPoint1GRAD_Dubl_Sost = -1;
        public static double YPoint1GRAD_Dubl_Sost = -1;
        public static double XPoint2GRAD_Dubl_Sost = -1;
        public static double YPoint2GRAD_Dubl_Sost = -1;

        public static bool flTmpGPS_VKL = false;
        // ......................................

        //public static int width_Sost = 24;
        //public static int height_Sost = 24;
        public static int width_Sost = 35;
        public static int height_Sost = 35;

        public static List<LF> list1_Sost = new List<LF>();

        public static int indz1_Sost=0;
        public static int indz2_Sost = 0;
        public static int indz3_Sost = 0;

        // DATUM
        // ГОСТ 51794_2008
        public static double dXdat_comm = 25;
        public static double dYdat_comm = -141;
        public static double dZdat_comm = -80;

        public static double dLat_comm=0;
        public static double dLong_comm=0;
        // Эллипсоид Красовского, град
        public static double LatKrG_comm=0;
        public static double LongKrG_comm=0;
        // Эллипсоид Красовского, rad
        public static double LatKrR_comm=0;
        public static double LongKrR_comm=0;
        // Эллипсоид Красовского, град,мин,сек
        public static int Lat_Grad_comm=0;
        public static int Lat_Min_comm=0;
        public static double Lat_Sec_comm=0;
        public static int Long_Grad_comm=0;
        public static int Long_Min_comm=0;
        public static double Long_Sec_comm=0;
        // Гаусс-крюгер(СК42) м
        public static double XSP42_comm=0;
        public static double YSP42_comm=0;

        // Эллипсоид Красовского, град
        public static double LatKrG_YS1_comm=0;
        public static double LongKrG_YS1_comm=0;
        // Эллипсоид Красовского, rad
        public static double LatKrR_YS1_comm=0;
        public static double LongKrR_YS1_comm=0;
        // Эллипсоид Красовского, град,мин,сек
        public static int Lat_Grad_YS1_comm=0;
        public static int Lat_Min_YS1_comm=0;
        public static double Lat_Sec_YS1_comm=0;
        public static int Long_Grad_YS1_comm=0;
        public static int Long_Min_YS1_comm=0;
        public static double Long_Sec_YS1_comm=0;
        // Гаусс-крюгер(СК42) м
        public static double XYS142_comm=0;
        public static double YYS142_comm=0;

        // WGS84,grad
        public static double BWGS84_1_sost = 0;
        public static double LWGS84_1_sost = 0;
        public static double BWGS84_2_sost = 0;
        public static double LWGS84_2_sost = 0;
        public static double BWGS84_3_sost = 0;
        public static double LWGS84_3_sost = 0;


        // Эллипсоид Красовского, град
        public static double LatKrG_YS2_comm=0;
        public static double LongKrG_YS2_comm=0;
        // Эллипсоид Красовского, rad
        public static double LatKrR_YS2_comm=0;
        public static double LongKrR_YS2_comm=0;
        // Эллипсоид Красовского, град,мин,сек
        public static int Lat_Grad_YS2_comm=0;
        public static int Lat_Min_YS2_comm=0;
        public static double Lat_Sec_YS2_comm=0;
        public static int Long_Grad_YS2_comm=0;
        public static int Long_Min_YS2_comm=0;
        public static double Long_Sec_YS2_comm=0;
        // Гаусс-крюгер(СК42) м
        public static double XYS242_comm=0;
        public static double YYS242_comm=0;

        // Для ручного ввода CK42(m)
        public static int f42SP_sost = 0;
        public static int f42SP1_sost = 0;
        public static int f42PU_sost = 0;

        // Для ручного ввода CK42(grad)
        public static int f42SP_G_sost = 0;
        public static int f42SP1_G_sost = 0;
        public static int f42PU_G_sost = 0;

        // Для ручного ввода WGS84(grad)
        //999
        public static int f84SP_G_sost = 0;
        public static int f84SP1_G_sost = 0;
        public static int f84PU_G_sost = 0;


        // --------------------------------------------------------------------------------------
        // Bearing

        public static int xxxY = 0;
        public static int xxxY1 = 0;

        public static int f_Open_objFormBearing = 0;

        public static int fl_PelMain = 0;

        public static uint flCoordSP_PelMain = 0; // =1-> Выбрали Пеленгатор1
        public static uint flCoordYS1_PelMain = 0; // =1-> Выбрали Пеленгатор2

        public static Point tpOwnCoordRect_PelMain;
        public static Point tpPoint1Rect_PelMain;
        public static double XCenter_PelMain = 0;
        public static double YCenter_PelMain = 0;
        public static double HCenter_PelMain = 0;
        public static double XPoint1_PelMain = 0;
        public static double YPoint1_PelMain = 0;
        public static double HPoint1_PelMain = 0;
        public static int width_PelMain = 24;
        public static int height_PelMain = 24;
        public static List<LF> list1_PelMain = new List<LF>();
        public static List<LF1> list2_PelMain = new List<LF1>();

        public static double dLat_br = 0;
        public static double dLong_br = 0;
        // Эллипсоид Красовского, град
        public static double LatKrG_br = 0;
        public static double LongKrG_br = 0;
        // Эллипсоид Красовского, rad
        public static double LatKrR_br = 0;
        public static double LongKrR_br = 0;
        // Эллипсоид Красовского, град,мин,сек
        public static int Lat_Grad_br = 0;
        public static int Lat_Min_br = 0;
        public static double Lat_Sec_br = 0;
        public static int Long_Grad_br = 0;
        public static int Long_Min_br = 0;
        public static double Long_Sec_br = 0;
        // Гаусс-крюгер(СК42) м
        public static double XSP42_br = 0;
        public static double YSP42_br = 0;

        // Эллипсоид Красовского, град
        public static double LatKrG_YS1_br = 0;
        public static double LongKrG_YS1_br = 0;
        // Эллипсоид Красовского, rad
        public static double LatKrR_YS1_br = 0;
        public static double LongKrR_YS1_br = 0;
        // Эллипсоид Красовского, град,мин,сек
        public static int Lat_Grad_YS1_br = 0;
        public static int Lat_Min_YS1_br = 0;
        public static double Lat_Sec_YS1_br = 0;
        public static int Long_Grad_YS1_br = 0;
        public static int Long_Min_YS1_br = 0;
        public static double Long_Sec_YS1_br = 0;
        // Гаусс-крюгер(СК42) м
        public static double XYS142_br = 0;
        public static double YYS142_br = 0;

        public static double dD_PelMain1=0;

        // 0 - полностью прозрачно 255- непрозрачно
        public static double prozrd = 0;
        public static int prozr = 0;
        //public static int prozr = 255;
        //public static int prozr = 200;

        public static int grn1 = 255;  //1..2Rmin яркий
        public static int grn2 = 210;
        public static int grn3 = 170;
        public static int grn4 = 120;
        public static int grn5 = 80;   // dark

        public static int rd1 = 255;
        public static int rd2 = 210;
        public static int rd3 = 170;
        public static int rd4 = 120;
        public static int rd5 = 80;


        //2210
        public static Rectangle[] rects;

        //2310
        public static List<Rectangle> list_rects1 = new List<Rectangle>();
        public static List<Rectangle> list_rects2 = new List<Rectangle>();
        public static List<Rectangle> list_rects3 = new List<Rectangle>();
        public static List<Rectangle> list_rects4 = new List<Rectangle>();
        public static List<Rectangle> list_rects5 = new List<Rectangle>();
        public static List<Rectangle> list_rects6 = new List<Rectangle>();
        public static List<Rectangle> list_rects7 = new List<Rectangle>();
        public static List<Rectangle> list_rects8 = new List<Rectangle>();
        public static List<Rectangle> list_rects9 = new List<Rectangle>();
        public static List<Rectangle> list_rects10 = new List<Rectangle>();
 
        public static Brush brush1_bear;
        public static Brush brush1_1bear;
        public static Brush brush1_2bear;
        public static Brush brush1_3bear;
        public static Brush brush1_4bear;
        public static Brush brush1_5bear;
        public static Brush brush1_6bear;
        public static Brush brush1_7bear;
        public static Brush brush1_8bear;
        public static Brush brush1_9bear;
        public static Brush brush1_10bear;

        public static int fl1_bear = 0;
        public static int fl2_bear = 0;
        public static int fl3_bear = 0;
        public static int clr1_bear = 0;
        public static int prozr1_bear = 0;

        public static int fl_scrb = 0;

        //333
        // WGS84,grad
        public static double BWGS84_1_bear = 0;
        public static double LWGS84_1_bear = 0;
        public static double BWGS84_2_bear = 0;
        public static double LWGS84_2_bear = 0;

        // --------------------------------------------------------------------------------------
        // Az1

        public static int f_Open_objFormAz1 = 0;
        public static int flCoord_Az1 = 0;
        public static int NumbRow_Az1=3;

        public static double XCenter_Az1 = 0;
        public static double YCenter_Az1 = 0;
        public static double HCenter_Az1 = 0;

        public static double dLat_comm_Az1 = 0;
        public static double dLong_comm_Az1 = 0;
        // Эллипсоид Красовского, град
        public static double LatKrG_comm_Az1 = 0;
        public static double LongKrG_comm_Az1 = 0;
        // Эллипсоид Красовского, rad
        public static double LatKrR_comm_Az1 = 0;
        public static double LongKrR_comm_Az1 = 0;
        // Эллипсоид Красовского, град,мин,сек
        public static int Lat_Grad_comm_Az1 = 0;
        public static int Lat_Min_comm_Az1 = 0;
        public static double Lat_Sec_comm_Az1 = 0;
        public static int Long_Grad_comm_Az1 = 0;
        public static int Long_Min_comm_Az1 = 0;
        public static double Long_Sec_comm_Az1 = 0;
        // Гаусс-крюгер(СК42) м
        public static double XSP42_comm_Az1 = 0;
        public static double YSP42_comm_Az1 = 0;

        public static double XSP1_Az1 = 0;
        public static double YSP1_Az1 = 0;
        public static double XSP2_Az1 = 0;
        public static double YSP2_Az1 = 0;
        public static double XPU_Az1 = 0;
        public static double YPU_Az1 = 0;

        //333
        // WGS84,grad
        public static double BWGS84_Az1 = 0;
        public static double LWGS84_Az1 = 0;

        // --------------------------------------------------------------------------------------
        // Зона подавления (!!! Энергодоступность по зонам)

        public static int f_Open_objFormSuppression = 0;

        //public static int fFSuppr = 0;
        public static int fl_Suppression = 0;
        public static int fl_ZoneSuppression = 0;

        public static uint flCoordSP_sup = 0; // =1-> Выбрали СП
        public static uint flCoordOP = 0;     // =1-> Выбрали OП

        //333
        // WGS84,grad
        public static double BWGS84_1_sup = 0;
        public static double LWGS84_1_sup = 0;
        public static double BWGS84_2_sup = 0;
        public static double LWGS84_2_sup = 0;

        public static Point tpOwnCoordRect_sup;
        public static Point tpPoint1Rect_sup;
        public static Point tpPointCentre1_sup;
        public static Point tpPointCentre2_sup;
        public static Point tpPointMiddle_sup;
        public static double XCenter_sup = 0;
        public static double YCenter_sup = 0;
        public static double HCenter_sup = 0;
        public static double XPoint1_sup = 0;
        public static double YPoint1_sup = 0;
        public static double HPoint1_sup = 0;
        public static double RZ_sup = 0;
        public static List<Point> listControlJammingZone = new List<Point>();
        public static double RZ1_sup = 0;
        public static double RZ2_sup = 0;

        public static int flA_sup = 0;
        public static double s_sup = 30000;
        public static Point Luch_sup3;
        public static Point Luch_sup4;

        public static double H_sup = 0;

        public static List<Point> listPointDSR_sup = new List<Point>();

        public static double dLat_sup = 0;
        public static double dLong_sup = 0;
        // Эллипсоид Красовского, град
        public static double LatKrG_sup = 0;
        public static double LongKrG_sup = 0;
        // Эллипсоид Красовского, rad
        public static double LatKrR_sup = 0;
        public static double LongKrR_sup = 0;
        // Эллипсоид Красовского, град,мин,сек
        public static int Lat_Grad_sup = 0;
        public static int Lat_Min_sup = 0;
        public static double Lat_Sec_sup = 0;
        public static int Long_Grad_sup = 0;
        public static int Long_Min_sup = 0;
        public static double Long_Sec_sup = 0;
        // Гаусс-крюгер(СК42) м
        public static double XSP42_sup = 0;
        public static double YSP42_sup = 0;

        // Эллипсоид Красовского, град
        public static double LatKrG_YS1_sup = 0;
        public static double LongKrG_YS1_sup = 0;
        // Эллипсоид Красовского, rad
        public static double LatKrR_YS1_sup = 0;
        public static double LongKrR_YS1_sup = 0;
        // Эллипсоид Красовского, град,мин,сек
        public static int Lat_Grad_YS1_sup = 0;
        public static int Lat_Min_YS1_sup = 0;
        public static double Lat_Sec_YS1_sup = 0;
        public static int Long_Grad_YS1_sup = 0;
        public static int Long_Min_YS1_sup = 0;
        public static double Long_Sec_YS1_sup = 0;
        // Гаусс-крюгер(СК42) м
        public static double XYS142_sup = 0;
        public static double YYS142_sup = 0;

        // -----------------------------------------------------------------------------------------
        //GPSIRI

        public static double LatGPS_IRI = 0;
        public static double LongGPS_IRI = 0;
        public static double XGPS_IRI = 0;
        public static double YGPS_IRI = 0;
        public static String sGPS_IRI = "";
        public static int IDGPS_IRI=0;
        //public static int ColGPS_IRI = 0;
        public static Color ColGPS_IRI;
        public static int flGPS_IRI = 0;
        public static int AdDlGPS_IRI = 0;
        public static int flDublGPS_IRI = -1;
        public static double DublLatGPS_IRI = 0;
        public static double DublLongGPS_IRI = 0;

        public static double dLat_GPS = 0;
        public static double dLong_GPS = 0;
        // Эллипсоид Красовского, град
        public static double LatKrG_GPS = 0;
        public static double LongKrG_GPS = 0;
        // Эллипсоид Красовского, rad
        public static double LatKrR_GPS = 0;
        public static double LongKrR_GPS = 0;
        // Эллипсоид Красовского, град,мин,сек
        public static int Lat_Grad_GPS = 0;
        public static int Lat_Min_GPS = 0;
        public static double Lat_Sec_GPS = 0;
        public static int Long_Grad_GPS = 0;
        public static int Long_Min_GPS = 0;
        public static double Long_Sec_GPS = 0;
        // Гаусс-крюгер(СК42) м
        public static double XSP42_GPS = 0;
        public static double YSP42_GPS = 0;

        public static int flTmrGPS = 0;

        public static bool bCheckGNSS = false; 

        public static List<AirPlane> listGPS_IRI = new List<AirPlane>();
        // -----------------------------------------------------------------------------------------
        // Sectors of antenn
        //Sect

        public static int flsect = 0;

        public static int fllSect1 = 0;
        public static int fllSect2 = 0;
        public static int fllSect3 = 0;
        public static int fllSect4 = 0;
        public static int fllSect5 = 0;

        public static double ValSect1 = 0; // grad
        public static double ValSect2 = 0;
        public static double ValSect3 = 0;
        public static double ValSect4 = 0;
        public static double ValSect5 = 0;

        public static int f_Open_objFormSect = 0;

        public static double luch1 = -1;
        public static double luch2 = -1;
        public static double luch3 = -1;
        public static double luch4 = -1;
        public static double luch5 = -1;

        public static double luch1_dubl = -1;
        public static double luch2_dubl = -1;
        public static double luch3_dubl = -1;
        public static double luch4_dubl = -1;
        public static double luch5_dubl = -1;
        public static int f_luch = 0;

        // Радиус сектора
        public static int RSect = 0; // m

        public static double dAlpha1 = 0;
        public static double dAlpha2 = 0;
        public static double dAlpha3 = 0;
        public static double dAlpha4 = 0;
        public static double dAlpha5 = 0;

        public static double Alpha1 = 0;
        public static double Alpha2 = 0;

        public static float startA = 0;
        public static float sweepA = 0;


        //public static int f1s_1 = 0;
        //public static int f1s_2 = 0;
        //public static int f2s_1 = 0;
        //public static int f2s_2 = 0;
        //public static int f3s_1 = 0;
        //public static int f3s_2 = 0;
        //public static int f4s_1 = 0;
        //public static int f4s_2 = 0;
        //public static int f5s_1 = 0;
        //public static int f5s_2 = 0;

        // Цвета сектора
        public static int Col1_1=0;
        public static int Col1_2=0;
        public static int Col1_3=0;
        public static int Col2_1=0;
        public static int Col2_2=0;
        public static int Col2_3=0;
        public static int Col3_1=0;
        public static int Col3_2=0;
        public static int Col3_3=0;
        public static int Col4_1=0;
        public static int Col4_2=0;
        public static int Col4_3=0;
        public static int Col5_1=0;
        public static int Col5_2=0;
        public static int Col5_3 = 0;

        // =1 state=Maximized
        // =2 state=Minimized
        // =3 state=Normal
        public static int ffstate = 0;


        public static int ftmr_Sect = 0;
        public static TimeSpan ts_Sect;

        public static int flscrolsect = 0;

        public static int fl_RepSect = 0;

        // -----------------------------------------------------------------------------------------



        public static int fl_Azb=0;


        // OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
        //777Otl
        // Otladka

        public static List<AirPlane> lll = new List<AirPlane>();
        public static int ssaa = 0;
        public static int shPelIRI = 0;

        public static int shyyy = 0;


        public static int shtmr1 = 0;
        // OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO



    } // Class
} // NameSpace
