﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Collections.Generic; // для обобщенных структур, в частности hashset

namespace NoiseShaper
{
    // тип нагрузки на литеру
    public struct TTypeLoad
    {
        public byte bLetter;           // литера
        public byte bType;             // тип: 1 - , 0 - 
    }
    // структура одного ИРИ ФРЧ на РП
    public struct TOneSourceSupress
    {
        public uint uiFreq;         // частота
        public byte bModulation;    // вид модуляции
        public byte bDeviation;     // вид девиации
        public byte bManipulation;  // вид манипуляции           
        public byte bDuration;      // длительность     
    }
    // структура Параметры РП + длительность для отправки в ФП
    public struct TDurationParamFWS
    {
        public int iDuration;           // длительность
        public byte bCount;
        public TOneSourceSupress[] SourceSupress;
    }
    // структура ППРЧ на РП для отправки в ФП
    public struct TDurationParamFHSS
    {
        public int iFreqMin;        // частота
        public int iDuration;       // длительность импульса излучения
        public byte bCodeFFT;       // код БПФ
        public byte bModulation;    // вид модуляции
        public byte bDeviation;     // вид девиации
        public byte bManipulation;  // вид манипуляции           
    }
    //след 2 структуры для 18 шифра
    public struct TParamFHSS//исправить
    {
        public int iFreq;        // частота
        public byte bCodeFFT;       // код БПФ
        public byte bModulation;    // вид модуляции
        public byte bDeviation;     // вид девиации
        public byte bManipulation;  // вид манипуляции           
    }
    public struct TDurationParamFHSSForMoreOneNetworks
    {
        public int iDuration;           // длительность
        public byte bCount;
        public TParamFHSS[] AllParamFHSS;
    }
    public struct DurationMeasurement // Для отправки запроса на измерение длительности излучения
    {
        public int iFreqMin;        // частота
        public int iRadiationDuration;           // длительность излучения
        public int iCycleDuration;           // длительность цикла
    }
    public struct TCodeErrorInform//изменено
    {
        public byte[] bCodeError;//для новых ошибок
        public byte[] bInform;
    }
    public class ModuleSHS
    {
        HashSet<byte> CmdCodes = new HashSet<byte>(new byte[16] { 1, 2, 5, 6, 7, 8, 9, 10, 11, 12, 15, 16, 17, 18, 13, 14 });// хэштаблица для быстрого определения наличия
        public enum CodeogramCiphers : byte // перечисление для шифров кодограм
        {
            REQUEST_STATE = 1,
            TYPE_LOAD,
            DURAT_PARAM_FWS = 5,
            REQUEST_VOLTAGE,
            REQUEST_POWER,
            REQUEST_CURRENT,
            REQUEST_TEMP,
            RADIAT_OFF,
            PARAM_FHSS,
            STOP_FHSS,//Выход из режима РП-ППРЧ
            REQUEST_FPS_STATE,
            SET_FHSS_PARAMS_DURAT,// установить параметры ППРЧ для измерения длительности излучения
            RESET,
            ReadinessReceptionSLI,// speech-like interference
            ParamsSLI,
            PARAM_FHSSForMoreOneNetwork
        }
        private byte bLastStateQuery;            // номер литеры при запросе состояния//public
        private byte bLastPowerQuery;            // номер литеры при запросе мощности//public
        private byte bLastVoltageQuery;          // номер литеры при запросе напряжения//public
        private byte bLastCurrentQuery;          // номер литеры при запросе тока//public
        private byte bLastTempQuery;             // номер литеры при запросе температуры//public
        //
        UdpClient udpSIS;              // udp клиент для ФП
        IPEndPoint localIPEndPoint;    // точка для udp   ;IPEndPoint Представляет сетевую конечную точка в виде IP-адреса и номера порта.     
        IPEndPoint remoteIPEndPoint;   // точка для udp ; remote - удаленный       
        Thread thrRead;                // поток приема кодограмм от ФП
        private byte bAdressSIS;//public
        private byte bAdressClient;//public
        int iAmauntOfErrorBytes;
        public int iDelayTime {get; set;}
        TCodeErrorInform tCodeErrorReadInform;
        //для счетчика кодограм
        private Semaphore smphForCount;
        byte bCountSendCmd;
        byte bCountReceiveCmd;
        byte bCountChiperSendCmd;
        //
        String NameFolder;
        String NameFile;
        int AmountOfRecordPart;
        int NumberOfRecordPart;
        byte[] ArrayForFile;
        int AmountRecordsByte;
        int SizeOfPart = 1024;
        int SizeOfNumber = 2;
        int ToTheEnd;
        int SendingPart = 0;
        bool bBeingSendRecord;
        int iSizeOfPartForOnline = 1200;//сделать ф-ию изменения
        bool bOnlineContinuing = false;
        bool bLastPartOfRecordForOnline = false;
        // СОБЫТИЯ 
        // наличие (отсутствие) подключения
        public delegate void WithoutParamsEventHandler();//делегат обработка события подключения
        public event WithoutParamsEventHandler ConnectNet;// событие подключния, связанное с делегатом ConnectEventHandler
        public event WithoutParamsEventHandler DisconnectNet;// событие отключения
        public event WithoutParamsEventHandler LostAnyCmd;//Утеряна кодограмма
        protected void OnWithoutParamsEvent(WithoutParamsEventHandler some_event)
        {
            if (some_event != null)
                some_event();//Raise the event
        }
        public delegate void ReceiveOrSendEventHandler(byte bCode, object obj);//делегат обработки события получения кодограммы
        public event ReceiveOrSendEventHandler ReceiveCmd;// событие получения команды
        public event ReceiveOrSendEventHandler SendCmd; // отправлена кодограмма
        protected virtual void OnReceiveOrSendCmd(ReceiveOrSendEventHandler some_ev, byte bCode, object obj)
        {
            if (some_ev != null)
                some_ev(bCode, obj);//Raise the event
        }
        public delegate void ByteArrayEventHandler(byte[] bByte);
        public event ByteArrayEventHandler ReceiveByte;
        public event ByteArrayEventHandler SendByte;// отпрвлен массив байт
        protected virtual void OnByteArray(ByteArrayEventHandler some_ev, byte[] bByte)
        {
            if (some_ev != null)
                some_ev(bByte);//Raise the event
        }
        public delegate void UpdateEventHandler(byte bLetter, TCodeErrorInform tCodeErrorInform);
        public event UpdateEventHandler UpdateState;
        public event UpdateEventHandler UpdatePower;
        public event UpdateEventHandler UpdateVoltage;
        public event UpdateEventHandler UpdateCurrent;
        public event UpdateEventHandler UpdateTemp;
        protected void OnUpdateSmth(UpdateEventHandler some_ev, byte bLetter, TCodeErrorInform tCodeErrorInform)
        {
            if (some_ev != null)
                some_ev(bLetter, tCodeErrorInform);//Raise the event
        }
        //для перессылки аудиозаписи
        public delegate void BooleanParamEventHandler(bool CorrectSending);
        public event BooleanParamEventHandler SendRecord;
        public event BooleanParamEventHandler AgreementForOnlineRecordSending; //для отправки аудиозаписи онлайн
        public event BooleanParamEventHandler SendRecordPartForOnline;
        protected void OnBooleanParamEvent(BooleanParamEventHandler some_ev, bool CorrectSending)
        {
            if (some_ev != null)
                some_ev(CorrectSending);//Raise the event
        }
        //
        public delegate void SendRecorPartdEventHandler(int count, int number);
        public event SendRecorPartdEventHandler SendRecordPart;
        protected void OnSendRecordPart(int count, int number)
        {
            if (SendRecordPart != null)
                SendRecordPart(count, number);//Raise the event
        }
        delegate void FillMainParams(ref byte[] ArrForSend);//мой делегат для общей части
        public ModuleSHS(int AmauntOfErrorBytes)
        {
            smphForCount = new Semaphore(0, 1);
            iAmauntOfErrorBytes = AmauntOfErrorBytes;
            bCountSendCmd = bCountReceiveCmd = bCountChiperSendCmd = 0;
            iDelayTime = 1000;
        }
        public void InitConst(byte BAdressSIS1, byte BAdressClient1)//инициализируем адреса
        {
            bAdressSIS = BAdressSIS1;
            bAdressClient = BAdressClient1;
        }
        // поток приема кодограмм от ФП
        public void ChangingAfterCatchLoss()
        {
            bCountReceiveCmd = 0;
            bCountSendCmd = 0;
        }
        private void CatchesLosses(byte CountOfRecisevedCMD)//менять !!!
        {
            smphForCount.Release();
            bCountReceiveCmd = Convert.ToByte((bCountReceiveCmd == 255 ? 0 : bCountReceiveCmd + 1));
            if (bCountReceiveCmd != bCountSendCmd && CountOfRecisevedCMD == bCountChiperSendCmd)
            {
                OnWithoutParamsEvent(LostAnyCmd);
                ChangingAfterCatchLoss();
            }
            smphForCount.WaitOne();
        }
        void FillingForTCodeErrorInform(byte NumberOfCMD,byte[] bRead)//сделать под оба протокола
        {
            tCodeErrorReadInform.bInform = (bRead.Length > (5 + iAmauntOfErrorBytes) ? new byte[bRead.Length - (5 + iAmauntOfErrorBytes)] : null);// поменяла
            if (iAmauntOfErrorBytes == 1)//так сделано, потому что для исходного протокола есть некоторые различия в кодах ошибок для разных кодограмм 
                tCodeErrorReadInform.bCodeError = new byte[2] { bRead[2], bRead[5] };
            else if (iAmauntOfErrorBytes == 3)
            {
                tCodeErrorReadInform.bCodeError = new byte[iAmauntOfErrorBytes];// код ошибки
                Array.Copy(bRead, 5, tCodeErrorReadInform.bCodeError, 0, iAmauntOfErrorBytes);
            }
            if (bRead.Length > 5 + iAmauntOfErrorBytes)
                Array.Copy(bRead, 5 + iAmauntOfErrorBytes, tCodeErrorReadInform.bInform, 0, bRead.Length - (5 + iAmauntOfErrorBytes));
            OnReceiveOrSendCmd(ReceiveCmd, NumberOfCMD, tCodeErrorReadInform);// передать параметры в событие ReceiveCMD// подтверждение принятия !!!
            CatchesLosses(bRead[3]);//передется счетчик кодограм
        }
        private void ReceiveData()// получение данных
        {
            while (true)// работает постоянно
            {
                try
                {
                    byte[] bRead = udpSIS.Receive(ref localIPEndPoint);//Receive Возвращает UDP-датаграмму, которая была послана удаленным узлом.

                    //byte[] bRead = new byte[9] { 0x04, 0x02, 0x0d, 0xad, 0x04, 0x80, 0x00, 0x00, 0x04};// Ira test duration event
                    if (bRead.Length >= 5)//не должно быть короче 5
                    {
                        byte bCode = bRead[2];// шифр кодограммы
                        //поменять по звуку
                        if (CmdCodes.Contains(bCode))//если есть такая кодограмма
                        {
                            FillingForTCodeErrorInform(bCode, bRead);
                            OnByteArray(ReceiveByte, bRead);
                            //сделать tuple
                            if (bCode == (byte)CodeogramCiphers.REQUEST_STATE)
                                OnUpdateSmth(UpdateState, bLastStateQuery, tCodeErrorReadInform);
                            else if (bCode == (byte)CodeogramCiphers.REQUEST_VOLTAGE)
                                OnUpdateSmth(UpdateVoltage, bLastVoltageQuery, tCodeErrorReadInform);
                            else if (bCode == (byte)CodeogramCiphers.REQUEST_POWER)
                                OnUpdateSmth(UpdatePower, bLastPowerQuery, tCodeErrorReadInform);
                            else if (bCode == (byte)CodeogramCiphers.REQUEST_CURRENT)
                                OnUpdateSmth(UpdateCurrent, bLastCurrentQuery, tCodeErrorReadInform);
                            else if (bCode == (byte)CodeogramCiphers.REQUEST_TEMP)
                                OnUpdateSmth(UpdateTemp, bLastTempQuery, tCodeErrorReadInform);
                        }
                    }
                }
                catch (System.Exception)
                {
                    //MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.ServiceNotification);
                }
            }
        }
        public bool ConnectClient(string strIP_SIS, string strIP_Client, int iPortSIS, int iPortClient)// соединение с клиентом
        {//
            if (udpSIS != null)// если существующее подключение не пусто
            {
                udpSIS.Close();//Close Закрывает UDP-подключения. 
                udpSIS = null;
            }
            if (thrRead != null)// если поток не пуст
            {
                thrRead.Abort();//прерывание потока
                thrRead.Join(500);//завершит поток через 500 млс
                thrRead = null;
            }
            if (remoteIPEndPoint != null)
                remoteIPEndPoint = null;
            try
            {
                localIPEndPoint = new IPEndPoint(IPAddress.Parse(strIP_Client), iPortClient);// присваиваем айпишник и номер порта сокету
                remoteIPEndPoint = new IPEndPoint(IPAddress.Parse(strIP_SIS), iPortSIS);
                udpSIS = new UdpClient(localIPEndPoint);
                thrRead = new Thread(new ThreadStart(ReceiveData));
                thrRead.IsBackground = true;// задаем поток фоновым
                thrRead.Start();// запускаем поток
                OnWithoutParamsEvent(ConnectNet);// коннектимся проверить!
                return true;
            }
            catch (System.Exception)
            {
                OnWithoutParamsEvent(DisconnectNet);// разрываем соединение проверить!
                return false;
            }
        }
        public void DisconnectClient()// разъединение с клиентом
        {
            if (udpSIS != null)
            {
                udpSIS.Close();
                udpSIS = null;
            }
            if (thrRead != null)
            {
                thrRead.Abort();
                thrRead.Join(500);
                thrRead = null;
            }
            OnWithoutParamsEvent(DisconnectNet);// разъединение проверить!
        }
        //
        bool CommanPartForCodograms(int CodogramaLenght, byte bCipher, FillMainParams some_del, Object for_some_del)
        {
            //общая часть: заполнение служебной части кодограммы
            byte[] bArreyForSend = new byte[CodogramaLenght];
            bArreyForSend[0] = bAdressClient;
            bArreyForSend[1] = bAdressSIS;
            bArreyForSend[2] = bCipher;
            smphForCount.Release();
            bCountChiperSendCmd = Convert.ToByte((bCountChiperSendCmd == 255 ? 0 : bCountChiperSendCmd + 1));
            bArreyForSend[3] = bCountChiperSendCmd;
            smphForCount.WaitOne();
            bArreyForSend[4] = Convert.ToByte(bArreyForSend.Length - 5);
            //место для делегата, который заполнит или нет (если null) информационную часть, различную практически для всех кодограмм
            if(some_del != null)
                some_del.Invoke(ref bArreyForSend);
            //снова общая часть
            if (SendData(bArreyForSend))
            {
                OnReceiveOrSendCmd(SendCmd, bCipher, for_some_del);//отследить вторую часть
                OnByteArray(SendByte, bArreyForSend);
                smphForCount.Release();
                bCountSendCmd = Convert.ToByte((bCountSendCmd == 255 ? 0 : bCountSendCmd + 1));
                smphForCount.WaitOne();
                return true;
            }
            else
                return false;
        }
        public bool SendRadiatOff()
        {
            return CommanPartForCodograms(5, (byte)CodeogramCiphers.RADIAT_OFF, null, null);
        }
        public bool SendRequestState(byte tRequestParam)//состояние одной литеры
        {
            return CommanPartForCodograms(6, (byte)CodeogramCiphers.REQUEST_STATE,
                delegate(ref byte[] ForSend)
                {
                    ForSend[5] = tRequestParam;
                    bLastStateQuery = tRequestParam;
                },
                tRequestParam);
        }
        public bool SendDurationParamFWS(TDurationParamFWS tDurationParamFWS)// Установить параметры РП для ИРИ ФРЧ и включить излучение заданной длительности
        {
            return CommanPartForCodograms(8 + tDurationParamFWS.bCount * 7, (byte)CodeogramCiphers.DURAT_PARAM_FWS,
                delegate(ref byte[] ForSend)
                {
                    Array.Copy(BitConverter.GetBytes(tDurationParamFWS.iDuration * 10), 0, ForSend, 5, 3);
                    for (int i = 0; i < tDurationParamFWS.bCount; i++)
                    {
                        Array.Copy(BitConverter.GetBytes(tDurationParamFWS.SourceSupress[i].uiFreq / 10), 0, ForSend, i * 7 + 8, 3);//??
                        ForSend[i * 7 + 11] = tDurationParamFWS.SourceSupress[i].bModulation;
                        ForSend[i * 7 + 12] = tDurationParamFWS.SourceSupress[i].bDeviation;
                        ForSend[i * 7 + 13] = tDurationParamFWS.SourceSupress[i].bManipulation;
                        ForSend[i * 7 + 14] = tDurationParamFWS.SourceSupress[i].bDuration;
                    }
                },
                tDurationParamFWS);
        }
        public bool SendParamFHSS(TDurationParamFHSS tDurationParamFHSS)//запрос "Установить параметры РП для ИРИ ППРЧ"
        {
            return CommanPartForCodograms(15, (byte)CodeogramCiphers.PARAM_FHSS,
                delegate(ref byte[] ForSend)
                {
                    Array.Copy(BitConverter.GetBytes(tDurationParamFHSS.iFreqMin), 0, ForSend, 5, 3);// копирует BitConverter.GetBytes(tDurationParamFHSS.iFreqMin) с 0-ого лемента в массив bSend с 5 номера в размере 3 элементов
                    Array.Copy(BitConverter.GetBytes(tDurationParamFHSS.iDuration * 10), 0, ForSend, 8, 3);
                    ForSend[11] = tDurationParamFHSS.bCodeFFT;
                    ForSend[12] = tDurationParamFHSS.bModulation;
                    ForSend[13] = tDurationParamFHSS.bDeviation;
                    ForSend[14] = tDurationParamFHSS.bManipulation;
                },
                tDurationParamFHSS);
        }
        public bool SendParamFHSSForMoreOneNetwork(TDurationParamFHSSForMoreOneNetworks tDurationParamFHSSForMoreOneNetworks)//проверить
        {
            return CommanPartForCodograms(8 + tDurationParamFHSSForMoreOneNetworks.bCount * 7, (byte)CodeogramCiphers.PARAM_FHSSForMoreOneNetwork,
                delegate(ref byte[] ForSend)
                {
                    Array.Copy(BitConverter.GetBytes(tDurationParamFHSSForMoreOneNetworks.iDuration * 10), 0, ForSend, 5, 3);// копирует BitConverter.GetBytes(tDurationParamFHSS.iFreqMin) с 0-ого лемента в массив bSend с 5 номера в размере 3 элементов
                    for (int i = 0; i < tDurationParamFHSSForMoreOneNetworks.bCount; i++)// проверить насчет длины
                    {
                        Array.Copy(BitConverter.GetBytes(tDurationParamFHSSForMoreOneNetworks.AllParamFHSS[i].iFreq), 0, ForSend, 7 * i + 8, 3);
                        ForSend[7 * i + 11] = tDurationParamFHSSForMoreOneNetworks.AllParamFHSS[i].bCodeFFT;
                        ForSend[7 * i + 12] = tDurationParamFHSSForMoreOneNetworks.AllParamFHSS[i].bModulation;
                        ForSend[7 * i + 13] = tDurationParamFHSSForMoreOneNetworks.AllParamFHSS[i].bDeviation;
                        ForSend[7 * i + 14] = tDurationParamFHSSForMoreOneNetworks.AllParamFHSS[i].bManipulation;
                    }
                },
                tDurationParamFHSSForMoreOneNetworks); 
        }
        public bool SendRequestVoltage(byte tRequestParam)// Запрос "	Параметры усилителя мощности для литеры"
        {
            return CommanPartForCodograms(6, (byte)CodeogramCiphers.REQUEST_VOLTAGE,
                delegate(ref byte[] ForSend)
                {
                    ForSend[5] = tRequestParam;
                    bLastVoltageQuery = tRequestParam;
                },
                tRequestParam); 
        }
        public bool SendRequestCurrent(byte tRequestParam)
        {
            return CommanPartForCodograms(6, (byte)CodeogramCiphers.REQUEST_CURRENT,
                delegate(ref byte[] ForSend)
                {
                    ForSend[5] = tRequestParam;
                    bLastCurrentQuery = tRequestParam;
                },
                tRequestParam); 
        }
        public bool SendRequestTemp(byte tRequestParam)
        {
            return CommanPartForCodograms(6, (byte)CodeogramCiphers.REQUEST_TEMP,
                delegate(ref byte[] ForSend)
                {
                    ForSend[5] = tRequestParam;
                    bLastTempQuery = tRequestParam;
                },
                tRequestParam); 
        }
        public bool SendRequestPower(byte tRequestParam)
        {
            return CommanPartForCodograms(6, (byte)CodeogramCiphers.REQUEST_POWER,
                delegate(ref byte[] ForSend)
                {
                    ForSend[5] = tRequestParam;
                    bLastPowerQuery = tRequestParam;
                },
                tRequestParam); 
        }
        public bool SendTypeLoad(TTypeLoad tTypeLoad)// Запрос "Установить тип нагрузки"
        {
            return CommanPartForCodograms(7, (byte)CodeogramCiphers.TYPE_LOAD,
                delegate(ref byte[] ForSend)
                {
                    ForSend[5] = tTypeLoad.bLetter;
                    ForSend[6] = tTypeLoad.bType;
                },
                tTypeLoad); 
        }
        public bool SendReset()// отправить перезагрузку
        {
            return CommanPartForCodograms(5, (byte)CodeogramCiphers.RESET, null, null); 
        }
        public bool SendStopFHSS()//отправить стоп ФПРЧ
        {
            return CommanPartForCodograms(5, (byte)CodeogramCiphers.STOP_FHSS, null, null); 
        }
        public bool SendFPSState()//запрос выключить излучение
        {
            return CommanPartForCodograms(5, (byte)CodeogramCiphers.REQUEST_FPS_STATE, null, null); 
        }
        public bool SendFHSSParamDur(DurationMeasurement durationMeasurement)//установить параметры ППРЧ для измерения длительности излучения
        {
            return CommanPartForCodograms(14, (byte)CodeogramCiphers.SET_FHSS_PARAMS_DURAT, 
                delegate(ref byte[] ForSend)
                {
                    Array.Copy(BitConverter.GetBytes(durationMeasurement.iFreqMin), 0, ForSend, 5, 3);// проверить
                    Array.Copy(BitConverter.GetBytes(durationMeasurement.iRadiationDuration), 0, ForSend, 8, 3);// проверить
                    Array.Copy(BitConverter.GetBytes(durationMeasurement.iCycleDuration), 0, ForSend, 11, 3);// проверить
                }, 
                durationMeasurement);
        }
        public bool SendConfirmation()//запрос на разрешение передачи звуковой помехи
        {
            return CommanPartForCodograms(5, (byte)CodeogramCiphers.ReadinessReceptionSLI, null, null);//проверить
        }
        public bool SendPartOfRecord(int NumberOfPart, byte[] RecordForSending)//менять
        {
            return CommanPartForCodograms(9 + RecordForSending.Length, (byte)CodeogramCiphers.ParamsSLI,
                delegate(ref byte[] ForSend)//проверять на правильную передачу параметров
                {
                    Array.Copy(BitConverter.GetBytes(RecordForSending.Length), 0, ForSend, 5, 2);
                    Array.Copy(BitConverter.GetBytes(NumberOfPart), 0, ForSend, 7, 2);//проверить
                    Array.Copy(RecordForSending, 0, ForSend, 9, RecordForSending.Length);
                },
                RecordForSending);
        }
        //
        private bool SendData(byte[] bSend)
        {
            try
            {
                udpSIS.Send(bSend, bSend.Length, remoteIPEndPoint); //Отправляет UDP-датаграмму в узел в указанной удаленн. конечн. т-ке: что отправить, какая длина этого, куда
                return true;
            }
            catch (System.Exception)
            {
                return false;
            }
        }
    }
}

