﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NAudio;
using NAudio.Wave;
using NAudio.FileFormats;
using NAudio.CoreAudioApi;
using System.IO;
using System.IO.Ports;
using MathNet.Numerics.IntegralTransforms;
using MathNet.Numerics.Signals;
using System.Threading;

public class ARDV1_DLL
{
    public struct AOR
    {
        public string s;
        public int frequency;
        public int Mode;
        public int Bandwidth;
        public int AGC;
        public int SquelchSelect;
        public int LevelSquelch;
        public int NoiseSquelch;
        public int ManualGain;
        public int Smeter;
        public int SmeterSQLStatus;
        public int AudioGain;
    }

    public AOR ArOne = new AOR();
    private SerialPort _port;
    private Thread thrRead;
    double ArOne_frequency;

    public delegate void ByteEventHandler();
    public delegate void ByteEventHandler1(string data);
    public delegate void ConnectEventHandler();

    public event ByteEventHandler OnReadByte;
    public event ByteEventHandler OnWriteByte;
    public event ConnectEventHandler OnConnectPort;
    public event ConnectEventHandler OnDisconnectPort;
    public event ByteEventHandler1 OnDecodedError;
    public event ByteEventHandler OnDecodedFrq;
    public event ByteEventHandler OnDecodedBW;
    public event ByteEventHandler OnDecodedMode;
    public event ByteEventHandler OnDecodedAGC;
    public event ByteEventHandler OnDecodedLevelSquelch;
    public event ByteEventHandler OnDecodedNoiseSquelch;
    public event ByteEventHandler OnDecodedSmeter;
    public event ByteEventHandler OnDecodedManualGain;
    public event ByteEventHandler OnDecodedAudioGain;
    public event ByteEventHandler OnDecodedSquelchSelect;
    

    public bool SendToAOR(string message)
    {
        try
        {
            message += "\x0D\x0A";
            _port.WriteLine(message);
            return true;
        }
        catch (Exception) { return false; }
    }
    public bool TurnON()
    {
        try
        {
            string message = "ZP\x0D\x0A";
            _port.WriteLine(message);
            Thread.Sleep(30);
            return true;
        }
        catch (Exception) { return false; }
    }
    public bool TurnOFF()
    {
        try
        {
            string message = "QP\x0D\x0A";
            _port.WriteLine(message);
            Thread.Sleep(40);
            return true;
        }
        catch (Exception) { return false; }
    }
    //public bool AutoModeSet(double AuM)
    //{
    //    try
    //    {
    //        if ((AuM > 1) || (AuM < 0)) return false;
    //        string message = "AU" + AuM + "\x0D\x0A";
    //        _port.WriteLine(message);
    //        Thread.Sleep(40);
    //        return true;
    //    }
    //    catch (Exception) { return false; }
    ////}
    //public bool AutoModeGet()
    //{
    //    try
    //    {
    //        _port.WriteLine("AU\x0D\x0A");
    //        Thread.Sleep(20);
    //        return true;
    //    }
    //    catch (Exception) { return false; }
    //}
    public bool FrequencySet(double FrqMhz)
    {
        try
        {
            if ((FrqMhz < 0.1) || (FrqMhz > 1300)) return false;


            Int64 FrqHz = (Int64)((FrqMhz * 1000000) / 1);
            //string frq = FrqHz.ToString().PadLeft(10, '0');
            string frq1 = (FrqHz / 1000000).ToString().PadLeft(4, '0');
            string frq2 = ((FrqHz/10) % 100000).ToString();
            frq2 = frq2.PadRight(5, '0');

            string message = "RF" + frq1 + "." + frq2 + "\x0D\x0A";
            _port.WriteLine(message);
            Thread.Sleep(40);
            return true;
        }
        catch (Exception) { return false; }
    }
    public bool FrequencyGet()
    {
        try
        {
            _port.WriteLine("RF\x0D\x0A");
            Thread.Sleep(20);
            return true;
        }
        catch (Exception) { return false; }
    }
    //public bool FrequencySet(string freq_MHz)//MHz
    //{
    //    try
    //    {
    //        long frql;
    //        double frqd;
    //        if (!double.TryParse(freq_MHz, out frqd)) { frqd = 0; return false; }
    //        frqd = 1000000 * frqd;
    //        if ((frqd > 3300000000) || (frqd < 10000)) { return false; }
    //        frql = (long)frqd;
    //        freq_MHz = frql.ToString();
    //        freq_MHz = freq_MHz.PadLeft(10, '0');
    //        string message = "RF" + freq_MHz + "\x0D\x0A";
    //        _port.WriteLine(message);
    //        Thread.Sleep(40);
    //        return true;
    //    }
    //    catch (Exception) { return false; }
    //}
    public bool ModeSet(int DigitalDecodedMode, int DigitalModeSetting, int AnalogReceiveMode)
    {
        string message;
        try
        {
            if ((DigitalDecodedMode > 7) || (DigitalDecodedMode < 0)) return false;
            if ((DigitalModeSetting > 8) && (DigitalModeSetting < 0)) return false;
            if ((AnalogReceiveMode > 8) && (AnalogReceiveMode < 0)) return false;
            if (AnalogReceiveMode != 8) message = "MD" + DigitalDecodedMode.ToString() + DigitalModeSetting.ToString() + AnalogReceiveMode.ToString() + "\x0D\x0A";
            else message = "MD" + DigitalDecodedMode.ToString() + DigitalModeSetting.ToString() + "F\x0D\x0A";
            _port.WriteLine(message);
            Thread.Sleep(20);
            return true;
        }
        catch (Exception) { return false; }
    }
    public bool ModeSet(int mod)
    {
        string message;
        if ((mod > 23) || (mod < 0)) return false;
        try
        {
            switch (mod)
            {
                case 0: message = "MD000\x0D\x0A";
                    _port.WriteLine(message);
                    Thread.Sleep(5);
                    break;
                case 1: message = "MD010\x0D\x0A";
                    _port.WriteLine(message);
                    Thread.Sleep(5);
                    break;
                case 2: message = "MD020\x0D\x0A";
                    _port.WriteLine(message);
                    Thread.Sleep(5);
                    break;
                case 3:
                    message = "MD030\x0D\x0A";
                    _port.WriteLine(message);
                    Thread.Sleep(5);
                    break;
                case 4:
                    message = "MD040\x0D\x0A";
                    _port.WriteLine(message);
                    Thread.Sleep(5);
                    break;
                case 5:
                    message = "MD050\x0D\x0A";
                    _port.WriteLine(message);
                    Thread.Sleep(5);
                    break;
                case 6:
                    message = "MD060\x0D\x0A";
                    _port.WriteLine(message);
                    Thread.Sleep(5);
                    break;
                case 7:
                    message = "MD070\x0D\x0A";
                    _port.WriteLine(message);
                    Thread.Sleep(5);
                    break;
                case 8:
                    message = "MD0F0\x0D\x0A";
                    _port.WriteLine(message);
                    Thread.Sleep(5);
                    break; ;
                case 9:
                    message = "MD0F1\x0D\x0A";
                    _port.WriteLine(message);
                    Thread.Sleep(5);
                    break;
                case 10:
                    message = "MD0F2\x0D\x0A";
                    _port.WriteLine(message);
                    Thread.Sleep(5);
                    break;
                case 11:
                    message = "MD0F3\x0D\x0A";
                    _port.WriteLine(message);
                    Thread.Sleep(5);
                    break;
                case 12:
                    message = "MD0F4\x0D\x0A";
                    _port.WriteLine(message);
                    Thread.Sleep(5);
                    break;
                case 13:
                    message = "MD0F5\x0D\x0A";
                    _port.WriteLine(message);
                    Thread.Sleep(5);
                    break;
                case 14:
                    message = "MD0F6\x0D\x0A";
                    _port.WriteLine(message);
                    Thread.Sleep(5);
                    break;
                case 15:
                    message = "MD0F7\x0D\x0A";
                    _port.WriteLine(message);
                    Thread.Sleep(5);
                    break;
            //switch (mod)
            //{
            //    case 0: message = "MD0F0\x0D\x0A";
            //        _port.WriteLine(message);
            //        Thread.Sleep(5);
            //        break;
            //    case 1: message = "MD1F0\x0D\x0A";
            //        _port.WriteLine(message);
            //        Thread.Sleep(5);
            //        break;
            //    case 2: message = "MD2F0\x0D\x0A";
            //        _port.WriteLine(message);
            //        Thread.Sleep(5);
            //        break;
            //    case 3: message = "MD3F0\x0D\x0A";
            //        _port.WriteLine(message);
            //        Thread.Sleep(5);
            //        break;
            //    case 4: message = "MD4F0\x0D\x0A";
            //        _port.WriteLine(message);
            //        Thread.Sleep(5);
            //        break;
            //    case 5: message = "MD5F0\x0D\x0A";
            //        _port.WriteLine(message);
            //        Thread.Sleep(5);
            //        break;
            //    case 6: message = "MD6F0\x0D\x0A";
            //        _port.WriteLine(message);
            //        Thread.Sleep(5);
            //        break;
            //    case 7: message = "MD7F0\x0D\x0A";
            //        _port.WriteLine(message);
            //        Thread.Sleep(5);
            //        break;
            //    case 8: message = "MD000\x0D\x0A";
            //        _port.WriteLine(message);
            //        Thread.Sleep(5);
            //        break;
            //    case 9: message = "MD010\x0D\x0A";
            //        _port.WriteLine(message);
            //        Thread.Sleep(5);
            //        break;
            //    case 10: message = "MD020\x0D\x0A";
            //        _port.WriteLine(message);
            //        Thread.Sleep(5);
            //        break;
            //    case 11:
            //        message = "MD030\x0D\x0A";
            //        _port.WriteLine(message);
            //        Thread.Sleep(5);
            //        break;
            //    case 12:
            //        message = "MD040\x0D\x0A";
            //        _port.WriteLine(message);
            //        Thread.Sleep(5);
            //        break;
            //    case 13:
            //        message = "MD050\x0D\x0A";
            //        _port.WriteLine(message);
            //        Thread.Sleep(5);
            //        break;
            //    case 14:
            //        message = "MD060\x0D\x0A";
            //        _port.WriteLine(message);
            //        Thread.Sleep(5);
            //        break;
            //    case 15:
            //        message = "MD070\x0D\x0A";
            //        _port.WriteLine(message);
            //        Thread.Sleep(5);
            //        break;
            //    case 16:
            //        message = "MD0F0\x0D\x0A";
            //        _port.WriteLine(message);
            //        Thread.Sleep(5);
            //        break; ;
            //    case 17:
            //        message = "MD0F0\x0D\x0A";
            //        _port.WriteLine(message);
            //        Thread.Sleep(5);
            //        break;
            //    case 18:
            //        message = "MD0F1\x0D\x0A";
            //        _port.WriteLine(message);
            //        Thread.Sleep(5);
            //        break;
            //    case 19:
            //        message = "MD0F2\x0D\x0A";
            //        _port.WriteLine(message);
            //        Thread.Sleep(5);
            //        break;
            //    case 20:
            //        message = "MD0F3\x0D\x0A";
            //        _port.WriteLine(message);
            //        Thread.Sleep(5);
            //        break;
            //    case 21:
            //        message = "MD0F4\x0D\x0A";
            //        _port.WriteLine(message);
            //        Thread.Sleep(5);
            //        break;
            //    case 22:
            //        message = "MD0F5\x0D\x0A";
            //        _port.WriteLine(message);
            //        Thread.Sleep(5);
            //        break;
            //    case 23:
            //        message = "MD0F6\x0D\x0A";
            //        _port.WriteLine(message);
            //        Thread.Sleep(5);
            //        break;
            }            
            return true;
        }
        catch (Exception) { return false; }
    }
    public bool ModeGet()
    {
        try
        {
            _port.WriteLine("MD\x0D\x0A");
            Thread.Sleep(20);
            return true;
        }
        catch (Exception) { return false; }
    }
    public bool BandWidthSet(int BW)
    {
        try
        {
            if ((BW > 4) || (BW < 0)) return false;
            //switch (ArOne.Mode.ToString().Substring(2, 1))
            //{
            //    case "0": if ((BW > 4) || (BW < 0)) return false; break;
            //    case "1": if ((BW > 3) || (BW < 0)) return false; break;
            //    case "2": if ((BW > 1) || (BW < 0)) return false; break;
            //    case "3": if ((BW > 1) || (BW < 0)) return false; break;
            //    case "4": if ((BW > 1) || (BW < 0)) return false; break;
            //    case "5": if ((BW > 1) || (BW < 0)) return false; break;
            //    case "6": if ((BW > 1) || (BW < 0)) return false; break;
            //}
            string message = "IF" + BW.ToString() + "\x0D\x0A";
            _port.WriteLine(message);
            Thread.Sleep(20);
            return true;
        }
        catch (Exception) { return false; }
    }
    public bool BandwidthGet()
    {
        try
        {
            _port.WriteLine("IF\x0D\x0A");
            Thread.Sleep(20);
            return true;
        }
        catch (Exception) { return false; }
    }
    public bool AutomaticGainControlSet(int AGC)
    {
        string message;
        try
        {

            //if ((ArOne.Mode.ToString().Substring(2, 1) != "1") || (ArOne.Mode.ToString().Substring(2, 1) != "4") || (ArOne.Mode.ToString().Substring(2, 1) != "5") || (ArOne.Mode.ToString().Substring(2, 1) != "6")) return false;
            if ((ArOne.Mode != 18) && (ArOne.Mode != 21) && (ArOne.Mode != 22) && (ArOne.Mode != 23)) return false;// only in AM SSB CW
            if ((AGC.ToString() != "0") && (AGC.ToString() != "1") && (AGC.ToString() != "2") && (AGC.ToString() != "3")) { return false; }

            //if (AGC == 3) 
            //    message = "ACF" + "\x0D\x0A";
            //else
            
            message = "AC" + AGC.ToString() + "\x0D\x0A";
            _port.WriteLine(message);
            Thread.Sleep(20);
            return true;
        }
        catch (Exception) { return false; }
    }
    public bool AutomaticGainControlGet()
    {
        try
        {
            _port.WriteLine("AC\x0D\x0A");
            Thread.Sleep(20);
            return true;
        }
        catch (Exception) { return false; }
    }
    public bool LevelSQuelchSet(int LSQ)
    {
        try
        {
            if ((LSQ > 99) || (LSQ < 0)) return false;
            string message = "LQ" + LSQ.ToString().PadLeft(2, '0') + "\x0D\x0A";
            _port.WriteLine(message);
            Thread.Sleep(20);
            return true;
        }
        catch (Exception) { return false; }
    }
    public bool LevelSQuelchGet()
    {
        try
        {
            _port.WriteLine("LQ\x0D\x0A");
            Thread.Sleep(20);
            return true;
        }
        catch (Exception) { return false; }
    }
    public bool NoiseSQuelchSet(int NSQ)
    {
        try
        {
            if ((NSQ > 99) || (NSQ < 0)) return false;
            string message = "NQ" + NSQ.ToString().PadLeft(2, '0') + "\x0D\x0A";
            _port.WriteLine(message);
            Thread.Sleep(20);
            return true;
        }
        catch (Exception) { return false; }
    }
    public bool NioseSQGet()
    {
        try
        {
            _port.WriteLine("NQ\x0D\x0A");
            Thread.Sleep(20);
            return true;
        }
        catch (Exception) { return false; }
    }
    public bool ManualGainSet(int RFG)
    {
        try
        {
            if ((RFG > 110) || (RFG < 0)) return false;
            string message = "RG" + RFG.ToString().PadLeft(3, '0') + "\x0D\x0A";
            _port.WriteLine(message);
            Thread.Sleep(20);
            return true;
        }
        catch (Exception) { return false; }
    }
    public bool ManualGainGet()
    {
        try
        {
            _port.WriteLine("RG\x0D\x0A");
            Thread.Sleep(20);
            return true;
        }
        catch (Exception) { return false; }
    }
    public bool AudioGainSet(int AG)
    {
        try
        {
            if ((AG > 99) || (AG < 0)) return false;
            string message = "AG" + AG.ToString().PadLeft(2, '0') + "\x0D\x0A";
            _port.WriteLine(message);
            Thread.Sleep(20);
            return true;
        }
        catch (Exception) { return false; }
    }
    public bool AudioGainGet()
    {
        try
        {
            _port.WriteLine("AG\x0D\x0A");
            Thread.Sleep(20);
            return true;
        }
        catch (Exception) { return false; }
    }

    public bool SquelchSelectSet(int SS)
    {
        try
        {
            if ((SS != 0) && (SS != 1) && (SS != 2)) return false;
            string message = "SQ" + SS.ToString() + "\x0D\x0A";
            _port.WriteLine(message);
            Thread.Sleep(20);
            return true;
        }
        catch (Exception) { return false; }
    }
    public bool SquelchSelectGet()
    {
        try
        {
            _port.WriteLine("SQ\x0D\x0A");
            Thread.Sleep(20);
            return true;
        }
        catch (Exception) { return false; }
    }
    public bool SMeterGet()
    {
        try
        {
            _port.WriteLine("LM\x0D\x0A");
            Thread.Sleep(20);
            return true;
        }
        catch (Exception) { return false; }
    }
    public long FrqStrMhzToLongHz(string FreqMHz)
    {
        try
        {
            long frql;
            double frqd;
            if (!double.TryParse(FreqMHz, out frqd)) { return 0; }//MessageBox.Show("Введите частоту в диапазоне \nот 0,01 до 3 300  МГц"); }
            frqd = 1000000 * frqd;
            frql = (long)frqd;
            return frql;
        }
        catch (Exception) { return -1; }
    }
    public void OpenPort(string portName)
    {
        if (_port == null)
            _port = new SerialPort();
        if (_port.IsOpen)
            ClosePort();
        try
        {
            _port.PortName = portName;
            _port.BaudRate = 115200;
            _port.Parity = Parity.None;
            _port.DataBits = 8;
            _port.StopBits = StopBits.One;
            _port.RtsEnable = true;
            _port.DtrEnable = true;
            _port.ReceivedBytesThreshold = 1000;
            _port.Open();
            if (thrRead != null)
            {
                thrRead.Abort();
                thrRead.Join(500);
                thrRead = null;
            }
            try
            {
                thrRead = new Thread(new ThreadStart(ReadExistingComPort));
                thrRead.IsBackground = true;
                thrRead.Start();
            }
            catch (System.Exception) { }
            ConnectPort();
        }
        catch (System.Exception)
        {
            DisconnectPort();
        }
    }
    protected virtual void ConnectPort()
    {
        if (OnConnectPort != null)
        {
            OnConnectPort();//Raise the event
        }
    }
    protected virtual void DisconnectPort()
    {
        if (OnDisconnectPort != null)
        {
            OnDisconnectPort();//Raise the event
        }
    }
    public void ClosePort()
    {
        try
        {
            _port.DiscardInBuffer();
        }
        catch (System.Exception) { }
        try
        {
            _port.DiscardOutBuffer();
        }
        catch (System.Exception) { }
        try
        {

            _port.Close();
            if (thrRead != null)
            {
                thrRead.Abort();
                thrRead.Join(500);
                thrRead = null;
            }
            DisconnectPort();
        }
        catch (System.Exception) { }
    }
    protected virtual void ReadByte(byte[] bByte)
    {
        if (OnReadByte != null)
        {
            OnReadByte();//Raise the event
        }
    }
    protected virtual void WriteByte(byte[] bByte)
    {
        if (OnWriteByte != null)
        {
            OnWriteByte();//Raise the event
        }
    }
    private void ReadExistingComPort()
    {
        string comand = null;
        while (true)
        {
            try
            {
                comand = null;
                //string s = _port.ReadExisting();
                string s = _port.ReadLine();
                if (s.Length > 2)
                {

                    comand = s.Substring(2, 2);
                    if (s.Substring(2, 2) == "RF")
                    {
                        string ss = s.Substring(4, 4);
                        ss = s.Substring(9, 5);
                        int.TryParse((s.Substring(4, 4) + s.Substring(9, 5)), out ArOne.frequency); ArOne.frequency *= 10;
                        if (OnDecodedFrq != null)
                        {
                            OnDecodedFrq();
                        }
                    } try
                    {
                        switch (comand)
                        {
                            case "MD":
                                ArOne.Mode = 0;                                
                                if (s.Substring(4, 1) != "0") ArOne.Mode= int.Parse(s.Substring(4, 1));
                                if (s.Substring(5, 1) != "F") ArOne.Mode= int.Parse(s.Substring(5, 1));
                                if (s.Substring(6, 1) != "0") ArOne.Mode= int.Parse(s.Substring(6, 1))+8;
                                if (OnDecodedMode != null)
                                {
                                    OnDecodedMode();
                                }
                                break;
                            case "IF":
                                int.TryParse(s.Substring(4, 1), out ArOne.Bandwidth);
                                if (OnDecodedBW != null)
                                {
                                    OnDecodedBW();
                                }
                                break;
                            case "AC":
                                string bbc = s.Substring(4, 1);
                                if (bbc == "F") { ArOne.AGC = 3; }
                                else
                                {
                                    int.TryParse(s.Substring(2, 1), out ArOne.AGC);
                                }
                                bbc = "";
                                if (OnDecodedAGC != null)
                                {
                                    OnDecodedAGC();
                                }
                                break;
                            case "LM":
                                int.TryParse(s.Substring(4, 3), out ArOne.Smeter);
                                int.TryParse(s.Substring(7, 1), out ArOne.SmeterSQLStatus);
                                if (OnDecodedSmeter != null)
                                {
                                    OnDecodedSmeter();
                                }
                                break;
                            case "NQ":
                                int.TryParse(s.Substring(4, 2), out ArOne.NoiseSquelch);
                                if (OnDecodedNoiseSquelch != null)
                                {
                                    OnDecodedNoiseSquelch();
                                }
                                break;
                            case "LQ":
                                int.TryParse(s.Substring(4, 2), out ArOne.LevelSquelch);
                                if (OnDecodedLevelSquelch != null)
                                {
                                    OnDecodedLevelSquelch();
                                }
                                break;
                            case "AG":
                                int.TryParse(s.Substring(4, 2), out ArOne.AudioGain);
                                if (OnDecodedAudioGain != null)
                                {
                                    OnDecodedAudioGain();
                                }
                                break;
                            case "RG":          //agc must be manual
                                int.TryParse(s.Substring(4, 3), out ArOne.ManualGain);
                                if (OnDecodedManualGain != null)
                                {
                                    OnDecodedManualGain();
                                }
                                break;
                            case "SQ":
                                int.TryParse(s.Substring(4, 1), out ArOne.SquelchSelect);
                                if (OnDecodedSquelchSelect != null)
                                {
                                    OnDecodedSquelchSelect();
                                }
                                break;
                        }
                    }
                    catch { }
                }
            }

            catch (Exception)
            {
                if (OnDecodedError != null)
                {
                    OnDecodedError(ArOne.s);//Raise the event
                }
            }
            //if (OnDecodedByte != null)
            //{
            //    OnDecodedByte(1);//Raise the event
            //}
        }
    }
}