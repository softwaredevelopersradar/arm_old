﻿using MathNet.Numerics.IntegralTransforms;
using NAudio.Wave;
using System.Threading;
using System.Numerics;
using System.IO.Ports;
using System.Globalization;
using System.Runtime.InteropServices;
using System.IO;
using System.Diagnostics;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MyDataGridView;
using System.Data.SQLite;
using System.Reflection;


namespace Control_AR_DV1
{
    public struct Struct_ARDV1
    {
        public byte bPriority;
        public int iAmpl;
        public int iAttenuator;
        public int iBW;
        public Int64 iFreq;
        public int iHPF;
        public int iID;
        public int iLPF;
        public int iMode;
        public int iOnOff;
        public int iPause;
        public int iU;
        public string Note;
        public string sTimeFirst;
    }
     public partial class UserControl1: UserControl
    {
        System.Diagnostics.Process ProcessWavPlayer;
        FunctionsDB functionsDB;


        ComponentResourceManager resources = new ComponentResourceManager(typeof(UserControl1));
        private int NumberOfLanguage;

        public UserControl1()
        {
            InitializeComponent();
            functionsDB = new FunctionsDB();
            ProcessWavPlayer = new Process();
            //ProcessWavPlayer.StartInfo.FileName = Application.StartupPath + "\\Player\\Player.exe";
            InitLabelParamArOne();
            InitConnectionArOne();
            cbAGC.SelectedIndex = 0;
            cbSQL.SelectedIndex = 0;
            comboBoxBW.SelectedIndex = 0;
            comboBoxMode.SelectedIndex = 0;
            cbTablePriority.SelectedIndex = 0;
            cbFrqStep.SelectedIndex = 0;

            /*
            foreach (string line in File.ReadLines(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\INI\\Common.ini"))
            {
                if (line.Contains("Language "))
                {
                    string LanguageAsNumber = line.Split(new string[] { "=" }, StringSplitOptions.None).Last();
                    NumberOfLanguage = Int32.Parse(LanguageAsNumber);
                    break;
                }
            }
            LanguageChooser(NumberOfLanguage);
            */

            MuteArone(false);
            radio_ARDV1.AudioGainSet(60);// (tbAudioGain.Value);

            VariableDynamic.VariableWork.OnAddFreqImportantKRPU += new VariableDynamic.VariableWork.AddFreqImportantKRPUEventHandler(VariableWork_OnAddFreqImportantKRPU);
        }


        void VariableWork_OnAddFreqImportantKRPU(USR_DLL.TFreqImportantKRPU[] freqImportantKRPU)
        {
            VariableStatic.VariableCommon vc = new VariableStatic.VariableCommon();

            if (vc.Operator == 1)
            {
                try
                {
                    for (int i = 0; i < freqImportantKRPU.Length; i++)
                    {
                        if (freqImportantKRPU[i].iFreqMin == freqImportantKRPU[i].iFreqMax)
                            AddFrqToTablARDV1(freqImportantKRPU[i].iFreqMin, 1);
                    }
                }
                catch { Exception ex; }
            }
        }
        public bool BW_obrabatyvaetsa = false;
        public ARDV1_DLL radio_ARDV1;
        string Com_ArOne = "COM3";
        string FrqToAOR = "";
        bool zapolneno = false;
        int spektorYMinPorog = -110;
        int spektorYMaxPorog = -40;
        //int TimePause_Baraban = 300;
        double taimer_zapisi_max = 100;
        double taimer_zapisi = 0;
        NumberFormatInfo nfi = new CultureInfo("en-US", false).NumberFormat;
        bool DotInFrq = false;
        int countafterdot = 0;
        static double Fs = 44100;               // Частота дискретизвции 
        static double T = 1.0 / Fs;             // Шаг дискретизации
        static double Fn = Fs / 2;              // Частота Найквиста
        static int N = (int)Fs / 5;                           //Длина сигнала (точек)
        WaveIn waveIn;// WaveIn - поток для записи
        WaveFileWriter writer;
        Complex[] sig1;
        int waterflow_count = 0;
        LinkedList<string> link = new LinkedList<string>();
        LinkedListNode<string> node;
        Complex max = 2400;                         //для корректировки уровня спектра
        int iAverage = 0;
        int val = 0;                            //для заолнения таблицы
        string sval = "";                            //для заолнения таблицы
        double[] SignalX;           //для точек графика БПФ 
        double[] SignalY;           //для точек графика БПФ 
        double[] PorogObnar;           //для точек порог обнаружения
        double[] SignalYAverage;           //для точек графика БПФ
        double[] SignalYMinPorog;   //для мин порога графика БПФ
        double[] SignalYMaxPorog;   //для макс порога графика БПФ
        double[] SignalYResized;
        int iNumDataMx = 0;
        int iNumDataSigma = 0;
        int count_baund = 0;
        int WaterflowTime = 50;             //коэффициент высота водопада
        double[,] intensity;
        public int resolutionX = 1000;
        public int resolutionY = 1000;
        public int delitel;
        double qwe = 0;                             //точка сигнала на графике
        double sig_sum = 0;                         //среднее за 5 отсчетов
        double K = N / 4;
        Task ThrIntensityGraphPain;
        public bool closing = false;
        bool bMute = false;
        Task TaskIntensityPaint;
        int ibaraban = 0;
        int ThrIntensityGraphN;
        int ThrIntensityGraphNeedCount = 3;
        int Nwaterflow_count = 3;               // коэффициент пропуски водопада
        Struct_ARDV1 StrARDV1 = new Struct_ARDV1();
        PropertyDGV propertyDGV;
        SQLiteConnection sqLiteConnect;
        SQLiteCommand sqLiteCommand;
        SQLiteDataReader sqLiteRead;
        int[] IDBaraban0;       //обычный
        int[] IDBaraban1;       //важный
        int IDBaraban0Count = 0;
        int IDBaraban1Count = 0;
        int NBaraban = 0;
        bool BarabanPriorityWorking = false;
        int Baraban1Num = 0;
        int Baraban0Num = 0;
        bool auto_save_wav = false;
        int kol_previshenij_dlia_zapisi = 10;       //колво превышений за 1 набор БПФ
        int kol_previshenij_dlia_zapisi_time_count = 5; //колво превышений подряд
        int kol_previshenij_dlia_zapisi_time_count_i = 0;
        int kol_buf_ostanovka_zapisi = 300;                  //задержка конца записи
        int PorogZapisi = -120;

        System.Threading.Timer timer;
        System.Threading.Timer timerRead;
        int TimePause = 100;
        public float step_frq_MHz = 10f / 1000000f;
        [DllImport("kernel32")]
        private static extern long WritePrivateProfileString(string section, string key, string val, string filePath);
        [DllImport("kernel32")]
        private static extern int GetPrivateProfileString(string section, string key, string def, StringBuilder retVal, int size, string filePath);

        public delegate void ByteEventHandler();
        public event ByteEventHandler OnBAORRed;
        public event ByteEventHandler OnBAORGreen;

        const string kHz = "кГц";
        const string dBm = "дБм";
        const string dB = "дБ";
        const string UsilVKL = "Усил ВКЛ";
        const string Attt = "Атт";
        const string Avto = "Авто";
        const string Vykl = "ВЫКЛ";
        const string Gc = "Гц";
        const string Obychnyj = "Обычный";
        const string Vazhnyj = "Важный";
        const string OshibkaChtenijaDannyhTablicy = "Ошибка чтения данных таблицы";
        const string Oshibka = "Ошибка";
        //const string aroneVolumeChannelName = "Лин. вход (2- Realtek High Definition Audio)";
        //const string aroneVolumeChannelName2 = "Микрофон (Устройство с поддержкой High Definition Audio)";
        const string aroneVolumeChannelName2 = "Микрофон (Cirrus Logic CDB4207)";

        public static void write_ARONE(string COMPort)
        {
            WritePrivateProfileString("AORARDV1", "ComN", COMPort, Application.StartupPath + "\\INI\\Connection.ini");
        }
        public static string read_ARONE_ComN()
        {
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("AORARDV1", "ComN", "COM14", temp, 255, Application.StartupPath + "\\INI\\Connection.ini");
            return temp.ToString();
        }
        public void FreqSetToAOR(Int64 f)
        {
            radio_ARDV1.FrequencySet(f / 1000000d);

            if (lFRQFromAOR.InvokeRequired)
            {
                lFRQFromAOR.Invoke((MethodInvoker)(delegate()
                {
                    string FreqText = f.ToString();
                    FreqText = FreqText.Insert(6, ",");
                    if (NumberOfLanguage == 0)
                        FreqText = FreqText + "кГц";
                    if (NumberOfLanguage == 2)
                        FreqText = FreqText + "kHs";
                    lFRQFromAOR.Text = FreqText;

                }));

            }
            else
            {
                string FreqText = f.ToString();
                FreqText = FreqText.Insert(6, ",");
                if (NumberOfLanguage == 0)
                    FreqText = FreqText + "кГц";
                if (NumberOfLanguage == 2)
                    FreqText = FreqText + "kHs";
                lFRQFromAOR.Text = FreqText;
            }

        }
        private void InitConnectionArOne()
        {
            try
            
            {
                cbACP.Image = ControlARDV1.Properties.Resources.stop;
                cbAverage.Image = ControlARDV1.Properties.Resources.average_gray;
                cbRec.Image = ControlARDV1.Properties.Resources.rec_grey;
                cbARec.Image = ControlARDV1.Properties.Resources.a_rec_grey;
                cbIntensityPaint.Image = ControlARDV1.Properties.Resources.play;
                pictureBox18.Image = ControlARDV1.Properties.Resources.wavPlayerGray;
                waveIn = new WaveIn();
                if (cbACP.Checked) cbACP.Checked = false;
                bAOR.BackColor = Color.Red;
                if (OnBAORRed != null)
                    OnBAORRed();
                cbACP.Enabled = false;
                cbAverage.Enabled = false;
                cbRec.Enabled = false;
                cbARec.Enabled = false;
                cbIntensityPaint.Enabled = false;
                b1.Enabled = false;
                b2.Enabled = false;
                b3.Enabled = false;
                b4.Enabled = false;
                b5.Enabled = false;
                b6.Enabled = false;
                b7.Enabled = false;
                b8.Enabled = false;
                b9.Enabled = false;
                b0.Enabled = false;
                bENT.Enabled = false;
                bESC.Enabled = false;
                groupBox1.Enabled = false;
                groupBox2.Enabled = false;
                groupBox4.Enabled = false;
                cbSQL.Enabled = false;
                bIspPel.Enabled = false;
                bFRSonRS.Enabled = false;
                bFrqMinus.Enabled = false;
                bFrqPlus.Enabled = false;
                cbFrqStep.Enabled = false;
                this.comboBoxBW.SelectedIndexChanged -= new System.EventHandler(this.comboBoxBW_SelectedIndexChanged);
                this.comboBoxMode.SelectedIndexChanged -= new System.EventHandler(this.comboBoxMode_SelectedIndexChanged);
                this.cbAGC.SelectedIndexChanged -= new System.EventHandler(this.cbAGC_SelectedIndexChanged);
                this.cbSQL.SelectedIndexChanged -= new System.EventHandler(this.cbSQL_SelectedIndexChanged);
                this.tbManualGain.Scroll -= new EventHandler(this.trackBarManualGain_Scroll);
                this.tbAudioGain.Scroll -= new EventHandler(this.tbAudioGain_Scroll);
                this.tbSQL.Scroll -= new EventHandler(this.tbSQL_Scroll);

                cbAGC.SelectedIndex = 0;
                cbSQL.SelectedIndex = 0;
                comboBoxBW.SelectedIndex = 0;
                comboBoxMode.SelectedIndex = 0;
                cbTablePriority.SelectedIndex = 0;
                comboBoxBW.SelectedIndex = 0;
                comboBoxMode.SelectedIndex = 0;
                cbFrqStep.SelectedIndex = 0;
                this.tmBaraban.Tick -= new System.EventHandler(this.tmBaraban_Tick);
            }
            catch { }
            try
            {
                if (radio_ARDV1 != null) radio_ARDV1 = null;
                if (radio_ARDV1 != null)
                    radio_ARDV1 = null;
                radio_ARDV1 = new ARDV1_DLL();
                nfi.NumberGroupSeparator = " ";
                radio_ARDV1.OnConnectPort += new ARDV1_DLL.ConnectEventHandler(ConnectPort);
                radio_ARDV1.OnDisconnectPort += new ARDV1_DLL.ConnectEventHandler(DisconnextPort);
                radio_ARDV1.OnDecodedFrq += new ARDV1_DLL.ByteEventHandler(DecodedFrq);
                radio_ARDV1.OnDecodedBW += new ARDV1_DLL.ByteEventHandler(DecodedBW);
                radio_ARDV1.OnDecodedMode += new ARDV1_DLL.ByteEventHandler(DecodedMode);
                radio_ARDV1.OnDecodedAGC += new ARDV1_DLL.ByteEventHandler(DecodedAGC);
                radio_ARDV1.OnDecodedManualGain += new ARDV1_DLL.ByteEventHandler(DecodedManualGain);
                radio_ARDV1.OnDecodedSmeter += new ARDV1_DLL.ByteEventHandler(DecodedSMeter);
                radio_ARDV1.OnDecodedSquelchSelect += new ARDV1_DLL.ByteEventHandler(DecodedSquelchSelect);
                radio_ARDV1.OnDecodedLevelSquelch += new ARDV1_DLL.ByteEventHandler(DecodedLevelSquelch);
                radio_ARDV1.OnDecodedNoiseSquelch += new ARDV1_DLL.ByteEventHandler(DecodedNoiseSquelch);
                radio_ARDV1.OnDecodedAudioGain += new ARDV1_DLL.ByteEventHandler(DecodedAudioGain);
                Com_ArOne = read_ARONE_ComN();
                //radio_ARDV1.OpenPort(Com_ArOne);
                //Thread.Sleep(300);
                //FlashLedWriteARONE();
                //radio_ARDV1.TurnON();
                //Ask_Arone();
                //int mod = radio_ARDV1.ArOne.Mode;
                sig1 = new Complex[44100];
                SignalX = new double[2205];
                SignalY = new double[2205];
                PorogObnar = new double[2205];
                SignalYAverage = new double[2205];
                SignalYMinPorog = new double[2205];
                SignalYMaxPorog = new double[2205];
                ConnectAudio();
                zapolneno = true;
                intensityGraph1Resize();
                ThrIntensityGraphPain.Start();
                intensityPlot1.Plot(intensity);
            }
            catch { }
            propertyDGV = new PropertyDGV();
            propertyDGV.SetPropertyDGV(myDataGridView1);
            //zagruzka_tablFromBD();
            int IDNum = 0;
            //nudTableFRQ.Value = decimal.Parse(dgv.Rows[IDNum].Cells[2].Value.ToString());
            //nudTablePorogObnar.Value = int.Parse(dgv.Rows[IDNum].Cells[4].Value.ToString());
            //cbTablePriority.Text = dgv.Rows[IDNum].Cells[12].Value.ToString();
            //tbNote.Text = dgv.Rows[IDNum].Cells[13].Value.ToString();
            //nudPause.Value = int.Parse(dgv.Rows[IDNum].Cells[3].Value.ToString());
            //comboBoxMode.Text = dgv.Rows[IDNum].Cells[8].Value.ToString();
            //comboBoxBW.Text = dgv.Rows[IDNum].Cells[9].Value.ToString();
            this.comboBoxBW.SelectedIndexChanged += new System.EventHandler(this.comboBoxBW_SelectedIndexChanged);
            this.comboBoxMode.SelectedIndexChanged += new System.EventHandler(this.comboBoxMode_SelectedIndexChanged);
            this.cbAGC.SelectedIndexChanged += new System.EventHandler(this.cbAGC_SelectedIndexChanged);
            this.cbSQL.SelectedIndexChanged += new System.EventHandler(this.cbSQL_SelectedIndexChanged);
            this.tbManualGain.Scroll += new EventHandler(this.trackBarManualGain_Scroll);
            this.tbSQL.Scroll += new EventHandler(this.tbSQL_Scroll);
            this.tbAudioGain.Scroll += new EventHandler(this.tbAudioGain_Scroll);
            this.tmBaraban.Tick += new System.EventHandler(this.tmBaraban_Tick);
            var TaskIntensityPaint = Task.Run(() => intensityPlot1.Plot(intensity));
        }
        
        private void DecodedNoiseSquelch()
        {
            FlashLedReadARONE();
            if (lSQLevel.InvokeRequired)
            {
                lSQLevel.Invoke((MethodInvoker)(delegate()
                {
                    lSQLevel.Text = radio_ARDV1.ArOne.NoiseSquelch.ToString() + dBm; //"dBm";
                }));
            }
            else
            {
                lSQLevel.Text = radio_ARDV1.ArOne.NoiseSquelch.ToString() + dBm; // "dBm";
            }
        }
        private void DecodedLevelSquelch()
        {
            FlashLedReadARONE();
            if (lSQLevel.InvokeRequired)
            {
                lSQLevel.Invoke((MethodInvoker)(delegate()
                {
                    lSQLevel.Text = radio_ARDV1.ArOne.LevelSquelch.ToString() + dBm; //"dBm";
                }));
            }
            else
            {
                lSQLevel.Text = radio_ARDV1.ArOne.LevelSquelch.ToString() + dBm; //"dBm";
            }
        }
        /*
        if (xxx.InvokeRequired)
        {
            xxx.Invoke((MethodInvoker)(delegate()
            {
                    
            }));
        }
        else
        {
               

        }
         */
        private void DecodedSMeter()
        {
            FlashLedReadARONE();
            lLevel.Text = radio_ARDV1.ArOne.Smeter.ToString() + dBm; //"dBm";
            switch (radio_ARDV1.ArOne.SmeterSQLStatus)
            {
                case 0: lSQ.Text = "SQL Close"; break;
                case 1: lSQ.Text = "Noise SQL"; break;
                case 2: lSQ.Text = "Tone SQL"; break;
                case 3: lSQ.Text = "Digital SQL"; break;
            }
        }
        private void ConnectAudio()
        {
            try
            {
                waveIn.DeviceNumber = 0;//Дефолтное устройство для записи (если оно имеется)
                // waveIn.RecordingStopped += waveIn_RecordingStopped;//Прикрепляем обработчик завершения записи
                //waveIn.DataAvailable += waveIn_DataAvailable;
                waveIn.WaveFormat = new WaveFormat((int)Fs, 1); //Формат wav-файла - принимает параметры - частоту дискретизации и количество каналов(здесь mono)
                waveIn.StartRecording();
                //string outputFilename = String.Format("{0}{1}-{2}-{3}{4}", "C:\\ARM1\\Анализатор\\wav\\", textBox1.Text, DateTime.Now.ToString("dd.MM.yyyy"), DateTime.Now.ToString("hh.mm.ss"), ".wav");
                //"./" + textBox1.Text + "_" + DateTime.Now.ToString("yyyy:MM:dd-HH:mm") + "demo.wav";// 
                //writer = new WaveFileWriter(outputFilename, waveIn.WaveFormat);
                //WaveOut Waveout = new WaveOut();
            }
            catch (Exception ex)
            { MessageBox.Show(ex.Message); }
        }
        void Ask_Arone()
        {
            if (bAOR.BackColor == Color.Red) return;
            try
            {
                FlashLedWriteARONE();
                radio_ARDV1.AutomaticGainControlGet();
                Thread.Sleep(5);
                FlashLedWriteARONE();
                FlashLedWriteARONE();
                radio_ARDV1.FrequencyGet();
                Thread.Sleep(5);
                FlashLedWriteARONE();
                radio_ARDV1.ManualGainGet();
                Thread.Sleep(5);
                FlashLedWriteARONE();
                radio_ARDV1.ModeGet();
                Thread.Sleep(5);
                FlashLedWriteARONE();
                UpdateBW();
                radio_ARDV1.LevelSQuelchGet();
                Thread.Sleep(5);
                FlashLedWriteARONE();
                radio_ARDV1.NioseSQGet();
                Thread.Sleep(5);
                FlashLedWriteARONE();
                radio_ARDV1.BandwidthGet();
                Thread.Sleep(5);
                radio_ARDV1.SMeterGet();
                Thread.Sleep(5);
                FlashLedWriteARONE();
                radio_ARDV1.SquelchSelectGet();
                Thread.Sleep(5);
                FlashLedWriteARONE();
                radio_ARDV1.AudioGainGet();
                Thread.Sleep(5);
            }
            catch { }
        }
        private void InitLabelParamArOne()
        {
            lFrq.BackColor = pScreen.BackColor;
            lFrq.ForeColor = lFRQFromAOR.ForeColor;
        }
        private void DecodedSquelchSelect()
        {
            FlashLedReadARONE();
            if (cbSQL.InvokeRequired)
            {
                cbSQL.Invoke((MethodInvoker)(delegate()
                {
                    this.cbSQL.SelectedIndexChanged -= new EventHandler(this.cbSQL_SelectedIndexChanged);
                    if (!zapolneno) cbSQL.SelectedIndex = radio_ARDV1.ArOne.SquelchSelect;
                    lSQ.Text = cbSQL.Text;
                    this.cbSQL.SelectedIndexChanged += new EventHandler(this.cbSQL_SelectedIndexChanged);
                }));
            }
            else
            {
                this.cbSQL.SelectedIndexChanged -= new EventHandler(this.cbSQL_SelectedIndexChanged);
                if (!zapolneno) cbSQL.SelectedIndex = radio_ARDV1.ArOne.SquelchSelect;
                this.cbSQL.SelectedIndexChanged += new EventHandler(this.cbSQL_SelectedIndexChanged);

            }
            if (lSQ.InvokeRequired)
            {
                lSQ.Invoke((MethodInvoker)(delegate()
                {
                    switch (radio_ARDV1.ArOne.SquelchSelect)
                    {
                        case 0: lSQ.Text = "Auto"; break;
                        case 1: lSQ.Text = "NSq"; break;
                        case 2: lSQ.Text = "LSq"; break;
                    }
                }));
            }
            else
            {
                switch (radio_ARDV1.ArOne.SquelchSelect)
                {
                    case 0: lSQ.Text = "Auto"; break;
                    case 1: lSQ.Text = "NSq"; break;
                    case 2: lSQ.Text = "LSq"; break;
                }
            }
            if (tbSQL.InvokeRequired)
            {
                tbSQL.Invoke((MethodInvoker)(delegate()
                {
                    switch (cbSQL.SelectedIndex)
                    {
                        case 0: tbSQL.Enabled = false; break;
                        case 1: radio_ARDV1.NioseSQGet(); tbSQL.Enabled = true; break;
                        case 2: radio_ARDV1.LevelSQuelchGet(); tbSQL.Enabled = true; break;
                    }
                }));
            }
            else
            {
                switch (cbSQL.SelectedIndex)
                {
                    case 0: tbSQL.Enabled = false; break;
                    case 1: radio_ARDV1.NioseSQGet(); tbSQL.Enabled = true; break;
                    case 2: radio_ARDV1.LevelSQuelchGet(); tbSQL.Enabled = true; break;
                }
            }
        }
        private void DecodedAudioGain()
        {
            FlashLedReadARONE();
            if (!zapolneno) tbAudioGain.Value = radio_ARDV1.ArOne.AudioGain;
        }
        private void DecodedSignalLevelUnit_dBmV()
        {
            FlashLedReadARONE();
        }
        private void DecodedFrq()
        {
            FlashLedReadARONE();
            if (lFRQFromAOR.InvokeRequired)
            {
                lFRQFromAOR.Invoke((MethodInvoker)(delegate()
                {
                    float qwwe = radio_ARDV1.ArOne.frequency / 1000f;
                    //lFRQFromAOR.Text = qwwe.ToString("0.000"/*"N3", nfi*/) + " кГц";// ("{0:#,###,###.###}");             //ToString("F3");
                    lFRQFromAOR.Text = (radio_ARDV1.ArOne.frequency / 1000).ToString() + "," + (radio_ARDV1.ArOne.frequency % 1000).ToString("000") + kHz;// " кГц";// ("{0:#,###,###.###}");             //ToString("F3");
                }));

            }
            else
            {
                lFRQFromAOR.Text = (radio_ARDV1.ArOne.frequency / 1000).ToString() + "," + (radio_ARDV1.ArOne.frequency % 1000).ToString("000") + kHz;//" кГц";// ("{0:#,###,###.###}");             //ToString("F3");
            }
        }
        private void DecodedMode()
        {
            FlashLedReadARONE();
            if (lMode.InvokeRequired)
            {
                lMode.Invoke((MethodInvoker)(delegate()
                {
                    switch (radio_ARDV1.ArOne.Mode)
                    {

                        case 0: lMode.Text = "AUTO(digital)"; break;
                        case 1: lMode.Text = "D-STAR"; break;
                        case 2: lMode.Text = "YAESU"; break;
                        case 3: lMode.Text = "ALINCO"; break;
                        case 4: lMode.Text = "D-CR/NXDN"; break;
                        case 5: lMode.Text = "P-25"; break;
                        case 6: lMode.Text = "dPMR"; break;
                        case 7: lMode.Text = "DMR"; break;
                        case 8: lMode.Text = "FM"; break;
                        case 9: lMode.Text = "AM"; break;
                        case 10: lMode.Text = "SAH"; break;
                        case 11: lMode.Text = "SAL"; break;
                        case 12: lMode.Text = "USB"; break;
                        case 13: lMode.Text = "LSB"; break;
                        case 14: lMode.Text = "CW"; break;
                    }
                    if (zapolneno == false) comboBoxMode.SelectedIndex = radio_ARDV1.ArOne.Mode;
                }));

            }
            else
            {
                switch (radio_ARDV1.ArOne.Mode)
                {
                    case 0: lMode.Text = "AUTO(digital)"; break;
                    case 1: lMode.Text = "D-STAR"; break;
                    case 2: lMode.Text = "YAESU"; break;
                    case 3: lMode.Text = "ALINCO"; break;
                    case 4: lMode.Text = "D-CR/NXDN"; break;
                    case 5: lMode.Text = "P-25"; break;
                    case 6: lMode.Text = "dPMR"; break;
                    case 7: lMode.Text = "DMR"; break;
                    case 8: lMode.Text = "FM"; break;
                    case 9: lMode.Text = "AM"; break;
                    case 10: lMode.Text = "SAH"; break;
                    case 11: lMode.Text = "SAL"; break;
                    case 12: lMode.Text = "USB"; break;
                    case 13: lMode.Text = "LSB"; break;
                    case 14: lMode.Text = "CW"; break;
                }
                if (zapolneno == false) comboBoxMode.SelectedIndex = radio_ARDV1.ArOne.Mode;
            }
            
        }
        private void DecodedManualGain()
        {
            FlashLedReadARONE();
            if (tbManualGain.InvokeRequired)
            {
                tbManualGain.Invoke((MethodInvoker)(delegate()
                {
                    tbManualGain.Value = radio_ARDV1.ArOne.ManualGain;
                }));

            }
            else
            {
                tbManualGain.Value = radio_ARDV1.ArOne.ManualGain;
            }
        }
        private void DecodedLevelSquelchTreshold()
        {
            FlashLedReadARONE();
        }
        private void DecodedIFGain()
        {
            FlashLedReadARONE();
        }
        private void DecodedErr(string data)
        {
            FlashLedReadARONE();
        }
        private void DecodedAGC()
        {
            FlashLedReadARONE();
            if (cbAGC.InvokeRequired)
            {
                cbAGC.Invoke((MethodInvoker)(delegate()
                {
                    if (zapolneno == false) cbAGC.SelectedIndex =radio_ARDV1.ArOne.AGC;
                }));

            }
            else
            {
                if (zapolneno == false) cbAGC.SelectedIndex = radio_ARDV1.ArOne.AGC;
            }
        }
        private void DecodedBW()
        {
            BW_obrabatyvaetsa = true;
            FlashLedReadARONE();
            if (lBw.InvokeRequired)
            {
                lBw.Invoke((MethodInvoker)(delegate()
                {
                    this.comboBoxBW.SelectedIndexChanged -= new System.EventHandler(this.comboBoxBW_SelectedIndexChanged);
                    switch (radio_ARDV1.ArOne.Mode)
                    {
                        case 0://fm
                        case 1://fm
                        case 2://fm
                        case 3://fm
                        case 4://fm
                        case 5://fm
                        case 6://fm
                        case 7://fm
                        case 8://fm


                            
                            if (radio_ARDV1.ArOne.Bandwidth == 0)
                                lBw.Text = "200 " + kHz;//кГц";
                            if (radio_ARDV1.ArOne.Bandwidth == 1)
                                lBw.Text = "100 " + kHz;//кГц";
                            if (radio_ARDV1.ArOne.Bandwidth == 2)
                                lBw.Text = "30 " + kHz;//кГц";
                            if (radio_ARDV1.ArOne.Bandwidth == 3)
                                lBw.Text = "15 " + kHz;//кГц";
                            if (radio_ARDV1.ArOne.Bandwidth == 4)
                                lBw.Text = "6 " + kHz;//кГц";
                            break;

                        case 9://fm
                            if (radio_ARDV1.ArOne.Bandwidth == 0)
                                lBw.Text = "15 " + kHz;//кГц";
                            if (radio_ARDV1.ArOne.Bandwidth == 1)
                                lBw.Text = "8 " + kHz;//кГц";
                            if (radio_ARDV1.ArOne.Bandwidth == 2)
                                lBw.Text = "5.5 " + kHz;//кГц";
                            if (radio_ARDV1.ArOne.Bandwidth == 3)
                                lBw.Text = "3.8 " + kHz;//кГц";
                            break;
                        case 10:
                        case 11:
                            if (radio_ARDV1.ArOne.Bandwidth == 0)
                                lBw.Text = "5.5 " + kHz;//кГц";
                            if (radio_ARDV1.ArOne.Bandwidth == 1)
                                lBw.Text = "3.8 " + kHz;//кГц";
                            break;
                        case 12:
                        case 13:
                            if (radio_ARDV1.ArOne.Bandwidth == 0)
                                lBw.Text = "2.6 " + kHz;//кГц";
                            if (radio_ARDV1.ArOne.Bandwidth == 1)
                                lBw.Text = "1.8 " + kHz;//кГц";
                            break;
                        case 14:
                            if (radio_ARDV1.ArOne.Bandwidth == 0)
                                lBw.Text = "0.5 " + kHz;//кГц";
                            if (radio_ARDV1.ArOne.Bandwidth == 1)
                                lBw.Text = "0.2 " + kHz;//кГц";
                            break;
                    }
                    this.comboBoxBW.SelectedIndexChanged += new System.EventHandler(this.comboBoxBW_SelectedIndexChanged);
                }));

            }
            else
            {

                this.comboBoxBW.SelectedIndexChanged -= new System.EventHandler(this.comboBoxBW_SelectedIndexChanged);
                switch (radio_ARDV1.ArOne.Mode)
                {
                    case 0://fm
                    case 1://fm
                    case 2://fm
                    case 3://fm
                    case 4://fm
                    case 5://fm
                    case 6://fm
                    case 7://fm
                    case 8://fm
                        if (radio_ARDV1.ArOne.Bandwidth == 0)
                            lBw.Text = "200 " + kHz;//кГц";
                        if (radio_ARDV1.ArOne.Bandwidth == 1)
                            lBw.Text = "100 " + kHz;//кГц";
                        if (radio_ARDV1.ArOne.Bandwidth == 2)
                            lBw.Text = "30 " + kHz;//кГц";
                        if (radio_ARDV1.ArOne.Bandwidth == 3)
                            lBw.Text = "15 " + kHz;//кГц";
                        if (radio_ARDV1.ArOne.Bandwidth == 4)
                            lBw.Text = "6 " + kHz;//кГц";
                        break;

                    case 9://fm
                        if (radio_ARDV1.ArOne.Bandwidth == 0)
                            lBw.Text = "15 " + kHz;//кГц";
                        if (radio_ARDV1.ArOne.Bandwidth == 1)
                            lBw.Text = "8 " + kHz;//кГц";
                        if (radio_ARDV1.ArOne.Bandwidth == 2)
                            lBw.Text = "5.5 " + kHz;//кГц";
                        if (radio_ARDV1.ArOne.Bandwidth == 3)
                            lBw.Text = "3.8 " + kHz;//кГц";
                        break;
                    case 10:
                    case 11:
                        if (radio_ARDV1.ArOne.Bandwidth == 0)
                            lBw.Text = "5.5 " + kHz;//кГц";
                        if (radio_ARDV1.ArOne.Bandwidth == 1)
                            lBw.Text = "3.8 " + kHz;//кГц";
                        break;
                    case 12:
                    case 13:
                        if (radio_ARDV1.ArOne.Bandwidth == 0)
                            lBw.Text = "2.6 " + kHz;//кГц";
                        if (radio_ARDV1.ArOne.Bandwidth == 1)
                            lBw.Text = "1.8 " + kHz;//кГц";
                        break;
                    case 14:
                        if (radio_ARDV1.ArOne.Bandwidth == 0)
                            lBw.Text = "0.5 " + kHz;//кГц";
                        if (radio_ARDV1.ArOne.Bandwidth == 1)
                            lBw.Text = "0.2 " + kHz;//кГц";
                        break;
                }
                this.comboBoxBW.SelectedIndexChanged += new System.EventHandler(this.comboBoxBW_SelectedIndexChanged);
            }



            {
                if (comboBoxBW.InvokeRequired)
                {
                    comboBoxBW.Invoke((MethodInvoker)(delegate()
                    {
                        this.comboBoxBW.SelectedIndexChanged -= new System.EventHandler(this.comboBoxBW_SelectedIndexChanged);
                        switch (radio_ARDV1.ArOne.Mode)
                        {
                            case 0://fm
                            case 1://fm
                            case 2://fm
                            case 3://fm
                            case 4://fm
                            case 5://fm
                            case 6://fm
                            case 7://fm
                            case 8://fm
                                if (radio_ARDV1.ArOne.Bandwidth == 0)
                                    lBw.Text = "200 " + kHz;//кГц";
                                if (radio_ARDV1.ArOne.Bandwidth == 1)
                                    lBw.Text = "100 " + kHz;//кГц";
                                if (radio_ARDV1.ArOne.Bandwidth == 2)
                                    lBw.Text = "30 " + kHz;//кГц";
                                if (radio_ARDV1.ArOne.Bandwidth == 3)
                                    lBw.Text = "15 " + kHz;//кГц";
                                if (radio_ARDV1.ArOne.Bandwidth == 4)
                                    lBw.Text = "6 " + kHz;//кГц";
                                break;

                            case 9://fm
                                if (radio_ARDV1.ArOne.Bandwidth == 0)
                                    lBw.Text = "15 " + kHz;//кГц";
                                if (radio_ARDV1.ArOne.Bandwidth == 1)
                                    lBw.Text = "8 " + kHz;//кГц";
                                if (radio_ARDV1.ArOne.Bandwidth == 2)
                                    lBw.Text = "5.5 " + kHz;//кГц";
                                if (radio_ARDV1.ArOne.Bandwidth == 3)
                                    lBw.Text = "3.8 " + kHz;//кГц";
                                break;
                            case 10:
                            case 11:
                                if (radio_ARDV1.ArOne.Bandwidth == 0)
                                    lBw.Text = "5.5 " + kHz;//кГц";
                                if (radio_ARDV1.ArOne.Bandwidth == 1)
                                    lBw.Text = "3.8 " + kHz;//кГц";
                                break;
                            case 12:
                            case 13:
                                if (radio_ARDV1.ArOne.Bandwidth == 0)
                                    lBw.Text = "2.6 " + kHz;//кГц";
                                if (radio_ARDV1.ArOne.Bandwidth == 1)
                                    lBw.Text = "1.8 " + kHz;//кГц";
                                break;
                            case 14:
                                if (radio_ARDV1.ArOne.Bandwidth == 0)
                                    lBw.Text = "0.5 " + kHz;//кГц";
                                if (radio_ARDV1.ArOne.Bandwidth == 1)
                                    lBw.Text = "0.2 " + kHz;//кГц";
                                break;
                        }
                        this.comboBoxBW.SelectedIndexChanged += new System.EventHandler(this.comboBoxBW_SelectedIndexChanged);
                    }));

                }
                else
                {
                    this.comboBoxBW.SelectedIndexChanged -= new System.EventHandler(this.comboBoxBW_SelectedIndexChanged);
                    switch (radio_ARDV1.ArOne.Mode)
                    {
                        case 0://fm
                        case 1://fm
                        case 2://fm
                        case 3://fm
                        case 4://fm
                        case 5://fm
                        case 6://fm
                        case 7://fm
                        case 8://fm
                            if (radio_ARDV1.ArOne.Bandwidth == 0)
                                lBw.Text = "200 " + kHz;//кГц";
                            if (radio_ARDV1.ArOne.Bandwidth == 1)
                                lBw.Text = "100 " + kHz;//кГц";
                            if (radio_ARDV1.ArOne.Bandwidth == 2)
                                lBw.Text = "30 " + kHz;//кГц";
                            if (radio_ARDV1.ArOne.Bandwidth == 3)
                                lBw.Text = "15 " + kHz;//кГц";
                            if (radio_ARDV1.ArOne.Bandwidth == 4)
                                lBw.Text = "6 " + kHz;//кГц";
                            break;

                        case 9://fm
                            if (radio_ARDV1.ArOne.Bandwidth == 0)
                                lBw.Text = "15 " + kHz;//кГц";
                            if (radio_ARDV1.ArOne.Bandwidth == 1)
                                lBw.Text = "8 " + kHz;//кГц";
                            if (radio_ARDV1.ArOne.Bandwidth == 2)
                                lBw.Text = "5.5 " + kHz;//кГц";
                            if (radio_ARDV1.ArOne.Bandwidth == 3)
                                lBw.Text = "3.8 " + kHz;//кГц";
                            break;
                        case 10:
                        case 11:
                            if (radio_ARDV1.ArOne.Bandwidth == 0)
                                lBw.Text = "5.5 " + kHz;//кГц";
                            if (radio_ARDV1.ArOne.Bandwidth == 1)
                                lBw.Text = "3.8 " + kHz;//кГц";
                            break;
                        case 13:
                        case 12:
                            if (radio_ARDV1.ArOne.Bandwidth == 0)
                                lBw.Text = "2.6 " + kHz;//кГц";
                            if (radio_ARDV1.ArOne.Bandwidth == 1)
                                lBw.Text = "1.8 " + kHz;//кГц";
                            break;
                        case 14:
                            if (radio_ARDV1.ArOne.Bandwidth == 0)
                                lBw.Text = "0.5 " + kHz;//кГц";
                            if (radio_ARDV1.ArOne.Bandwidth == 1)
                                lBw.Text = "0.2 " + kHz;//кГц";
                            break;
                    }
                    this.comboBoxBW.SelectedIndexChanged += new System.EventHandler(this.comboBoxBW_SelectedIndexChanged);
                }
                BW_obrabatyvaetsa = false;
            }
        }

        private void WriteAOR(string qwe)
        {
            /*  try
              {
                  if (ledAORWrite.InvokeRequired)
                  {
                      ledAORWrite.Invoke((MethodInvoker)(delegate()
                      {
                          ledAORWrite.Value = true;
                          tmWriteAOR = new System.Threading.Timer(TimeWriteAOR, null, TimePause, 0);
                      }));

                  }
                  else
                  {
                      ledAORWrite.Value = true;
                      tmWriteAOR = new System.Threading.Timer(TimeWriteAOR, null, TimePause, 0);
                  }
              }
              catch (SystemException)
              { }*/
        }
        private void ReadAOR(string qwe)
        {
            /*   try
               {
                   if (ledAORRead.InvokeRequired)
                   {
                       ledAORRead.Invoke((MethodInvoker)(delegate()
                       {
                           ledAORRead.Value = true;
                           tmReadAOR = new System.Threading.Timer(TimeReadAOR, null, TimePause, 0);
                       }));

                   }
                   else
                   {
                       ledAORRead.Value = true;
                       tmReadAOR = new System.Threading.Timer(TimeReadAOR, null, TimePause, 0);
                   }
               }
               catch (SystemException)
               { }*/
        }
        private void ConnectPort()
        {
            ThrIntensityGraphPain = new Task(ThrWaterflowPaint);
            bAOR.BackColor = Color.Green;
            if (OnBAORGreen != null)
                OnBAORGreen();
            cbACP.Enabled = true;
            cbAverage.Enabled = true;
            cbRec.Enabled = true;
            cbARec.Enabled = true;
            cbIntensityPaint.Enabled = true;
            b1.Enabled = true;
            b2.Enabled = true;
            b3.Enabled = true;
            b4.Enabled = true;
            b5.Enabled = true;
            b6.Enabled = true;
            b7.Enabled = true;
            b8.Enabled = true;
            b9.Enabled = true;
            b0.Enabled = true;
            bDot.Enabled = true;
            bENT.Enabled = true;
            bESC.Enabled = true;
            groupBox1.Enabled = true;
            groupBox2.Enabled = true;
            groupBox4.Enabled = true;
            waveformGraph1.Enabled = true;
            intensityGraph1.Enabled = true;
            tbWaterflowMax.Enabled = true;
            tbWaterflowMin.Enabled = true;
            pScreen.Enabled = true;
            if (!cbACP.Checked) cbACP.Checked = true;
            cbFrqStep.Enabled = true;
            bFrqMinus.Enabled = true;
            bFrqPlus.Enabled = true;
            groupBox1.Enabled = true;
            bIspPel.Enabled = true;
            bFRSonRS.Enabled = true;
            radioButton1.Enabled = true;
            radioButton2.Enabled = true;
            cbSQL.Enabled = true;
            ThrIntensityGraphPain.Start();
            FlashLedWriteARONE();
            FlashLedReadARONE();

        }
        private void DisconnextPort()
        {
            if (cbACP.Checked) cbACP.Checked = false;
            bAOR.BackColor = Color.Red;
            if (OnBAORRed != null)
                OnBAORRed();
            cbACP.Enabled = false;
            cbAverage.Enabled = false;
            cbRec.Enabled = false;
            cbARec.Enabled = false;
            cbIntensityPaint.Enabled = false;
            b1.Enabled = false;
            b2.Enabled = false;
            b3.Enabled = false;
            b4.Enabled = false;
            b5.Enabled = false;
            b6.Enabled = false;
            b7.Enabled = false;
            b8.Enabled = false;
            b9.Enabled = false;
            b0.Enabled = false;
            bDot.Enabled = false;
            bENT.Enabled = false;
            bESC.Enabled = false;
            groupBox1.Enabled = false;
            groupBox2.Enabled = false;
            groupBox4.Enabled = false;
            waveformGraph1.Enabled = false;
            intensityGraph1.Enabled = false;
            tbWaterflowMax.Enabled = false;
            tbWaterflowMin.Enabled = false;
            pScreen.Enabled = false;
            cbFrqStep.Enabled = false;
            bFrqMinus.Enabled = false;
            bFrqPlus.Enabled = false;
            groupBox1.Enabled = false;
            bIspPel.Enabled = false;
            bFRSonRS.Enabled = false;
            radioButton1.Enabled = false;
            radioButton2.Enabled = false;
            FlashLedWriteARONE();
            FlashLedReadARONE();

        }
        private void UpdateBW()
        {
            this.comboBoxBW.SelectedIndexChanged -= new System.EventHandler(this.comboBoxBW_SelectedIndexChanged);
            comboBoxBW.Items.Clear();
            switch (radio_ARDV1.ArOne.Mode)
            {
                case 0://fm
                case 1://fm
                case 2://fm
                case 3://fm
                case 4://fm
                case 5://fm
                case 6://fm
                case 7://fm
                case 8://fm
                    comboBoxBW.Items.Add( "200 " + kHz);//кГц");
                    comboBoxBW.Items.Add ( "100 " + kHz);//кГц");
                    comboBoxBW.Items.Add ( "30 " + kHz);//кГц");
                    comboBoxBW.Items.Add ( "15 " + kHz);//кГц");
                    comboBoxBW.Items.Add ( "6 " + kHz);//кГц");
                    break;
                case 9://am
                    comboBoxBW.Items.Add("15 " + kHz);//кГц");
                    comboBoxBW.Items.Add("8 " + kHz);//кГц");
                    comboBoxBW.Items.Add("5.5 " + kHz);//кГц");
                    comboBoxBW.Items.Add("3.8 " + kHz);//кГц");
                    break;
                case 11://
                case 10://
                    comboBoxBW.Items.Add("5.5 " + kHz);//кГц");
                    comboBoxBW.Items.Add("3.8 " + kHz);//кГц");
                    break;
                case 13://                    
                case 12://
                    comboBoxBW.Items.Add("2.6 " + kHz);//кГц");
                    comboBoxBW.Items.Add("1.8 " + kHz);//кГц");
                    break;
                case 14://
                    comboBoxBW.Items.Add("0.5 " + kHz);//кГц");
                    comboBoxBW.Items.Add("0.2 " + kHz);//кГц");
                    break;                    
                    }
            if ((comboBoxBW.SelectedIndex == null) || (comboBoxBW.SelectedIndex < 0)) comboBoxBW.SelectedIndex = 0;
            this.comboBoxBW.SelectedIndexChanged += new System.EventHandler(this.comboBoxBW_SelectedIndexChanged);
        }
        private void comboBoxBW_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (BW_obrabatyvaetsa) return;
            FlashLedWriteARONE();
            radio_ARDV1.BandWidthSet(comboBoxBW.SelectedIndex);
            Thread.Sleep(20);
            radio_ARDV1.BandwidthGet();
            FlashLedReadARONE();
        }
        private void comboBoxMode_SelectedIndexChanged(object sender, EventArgs e)
        {
            FlashLedWriteARONE();
            radio_ARDV1.ModeSet(comboBoxMode.SelectedIndex);
            Thread.Sleep(20);
            FlashLedReadARONE();
            radio_ARDV1.ModeGet();
            Thread.Sleep(20);
            UpdateBW();
            FlashLedReadARONE();
            radio_ARDV1.BandwidthGet();
            Thread.Sleep(20); 
        }
        private void b1_Click(object sender, EventArgs e)
        {
            try
            {
                if (countafterdot > 2) return;
                if (!tmFlashFRQ.Enabled)
                    tmFlashFRQ.Start();
                lFRQFromAOR.Visible = false;
                lFrq.Visible = true;
                lFrq.ForeColor = Color.Green;
                if (FrqToAOR == "")
                {
                    FrqToAOR = "1";
                    lFrq.Text = FrqToAOR.ToString() + " "+kHz;// " кГц";
                    return;
                }
                if ((DotInFrq) && (countafterdot == 0))
                {
                    FrqToAOR += "1";
                    countafterdot++;
                    lFrq.Text = FrqToAOR.ToString() + " " + kHz;//" кГц";
                    return;
                }

                if (double.Parse(FrqToAOR) < 3299999)
                {
                    if (DotInFrq) countafterdot++;
                    FrqToAOR += "1";
                    if (double.Parse(FrqToAOR) > 3299999) FrqToAOR = FrqToAOR.Substring(0, FrqToAOR.Length - 1);
                }
                lFrq.Text = FrqToAOR.ToString() + " " + kHz;//" кГц";
            }
            catch (Exception) { }
        }
        private void b2_Click(object sender, EventArgs e)
        {
            try
            {
                if (countafterdot > 2) return;
                if (!tmFlashFRQ.Enabled)
                    tmFlashFRQ.Start();
                lFRQFromAOR.Visible = false;
                lFrq.Visible = true;
                lFrq.ForeColor = Color.Green;
                if (FrqToAOR == "")
                {
                    FrqToAOR = "2";
                    lFrq.Text = FrqToAOR.ToString() + " " + kHz;//" кГц";
                    return;
                }
                if ((DotInFrq) && (countafterdot == 0))
                {
                    FrqToAOR += "2";
                    countafterdot++;
                    lFrq.Text = FrqToAOR.ToString() + " " + kHz;//" кГц";
                    return;
                }

                if (double.Parse(FrqToAOR) < 3299999)
                {
                    if (DotInFrq) countafterdot++;
                    FrqToAOR += "2";
                    if (double.Parse(FrqToAOR) > 3299999) FrqToAOR = FrqToAOR.Substring(0, FrqToAOR.Length - 1);
                }
                lFrq.Text = FrqToAOR.ToString() + " " + kHz;//" кГц";
            }
            catch (Exception) { }
        }
        private void b3_Click(object sender, EventArgs e)
        {
            try
            {
                if (countafterdot > 2) return;
                if (!tmFlashFRQ.Enabled)
                    tmFlashFRQ.Start();
                lFRQFromAOR.Visible = false;
                lFrq.Visible = true;
                lFrq.ForeColor = Color.Green;
                if (FrqToAOR == "")
                {
                    FrqToAOR = "3";
                    lFrq.Text = FrqToAOR.ToString() + " " + kHz;//" кГц";
                    return;
                }
                if ((DotInFrq) && (countafterdot == 0))
                {
                    FrqToAOR += "3";
                    countafterdot++;
                    lFrq.Text = FrqToAOR.ToString() + " " + kHz;//" кГц";
                    return;
                }

                if (double.Parse(FrqToAOR) < 3299999)
                {
                    if (DotInFrq) countafterdot++;
                    FrqToAOR += "3";
                    if (double.Parse(FrqToAOR) > 3299999) FrqToAOR = FrqToAOR.Substring(0, FrqToAOR.Length - 1);
                }
                lFrq.Text = FrqToAOR.ToString() + " " + kHz;//" кГц";
            }
            catch (Exception) { }
        }
        private void b4_Click(object sender, EventArgs e)
        {
            try
            {
                if (countafterdot > 2) return;
                if (!tmFlashFRQ.Enabled)
                    tmFlashFRQ.Start();
                lFRQFromAOR.Visible = false;
                lFrq.Visible = true;
                lFrq.ForeColor = Color.Green;
                if (FrqToAOR == "")
                {
                    FrqToAOR = "4";
                    lFrq.Text = FrqToAOR.ToString() + " " + kHz;//" кГц";
                    return;
                }
                if ((DotInFrq) && (countafterdot == 0))
                {
                    FrqToAOR += "4";
                    countafterdot++;
                    lFrq.Text = FrqToAOR.ToString() + " " + kHz;// " кГц";
                    return;
                }

                if (double.Parse(FrqToAOR) < 3299999)
                {
                    if (DotInFrq) countafterdot++;
                    FrqToAOR += "4";
                    if (double.Parse(FrqToAOR) > 3299999) FrqToAOR = FrqToAOR.Substring(0, FrqToAOR.Length - 1);
                }
                lFrq.Text = FrqToAOR.ToString() + " " + kHz;//" кГц";
            }
            catch (Exception) { }
        }
        private void b5_Click(object sender, EventArgs e)
        {
            try
            {
                if (countafterdot > 2) return;
                if (!tmFlashFRQ.Enabled)
                    tmFlashFRQ.Start();
                lFRQFromAOR.Visible = false;
                lFrq.Visible = true;
                lFrq.ForeColor = Color.Green;
                if (FrqToAOR == "")
                {
                    FrqToAOR = "5";
                    lFrq.Text = FrqToAOR.ToString() + " " + kHz;//" кГц";
                    return;
                }
                if ((DotInFrq) && (countafterdot == 0))
                {
                    FrqToAOR += "5";
                    countafterdot++;
                    lFrq.Text = FrqToAOR.ToString() + " " + kHz;//" кГц";
                    return;
                }

                if (double.Parse(FrqToAOR) < 3299999)
                {
                    if (DotInFrq) countafterdot++;
                    FrqToAOR += "5";
                    if (double.Parse(FrqToAOR) > 3299999) FrqToAOR = FrqToAOR.Substring(0, FrqToAOR.Length - 1);
                }
                lFrq.Text = FrqToAOR.ToString() + " " + kHz;//" кГц";
            }
            catch (Exception) { }
        }
        private void b6_Click(object sender, EventArgs e)
        {
            try
            {
                if (countafterdot > 2) return;
                if (!tmFlashFRQ.Enabled)
                    tmFlashFRQ.Start();
                lFRQFromAOR.Visible = false;
                lFrq.Visible = true;
                lFrq.ForeColor = Color.Green;
                if (FrqToAOR == "")
                {
                    FrqToAOR = "6";
                    lFrq.Text = FrqToAOR.ToString() + " " + kHz;//" кГц";
                    return;
                }
                if ((DotInFrq) && (countafterdot == 0))
                {
                    FrqToAOR += "6";
                    countafterdot++;
                    lFrq.Text = FrqToAOR.ToString() + " " + kHz;//" кГц";
                    return;
                }

                if (double.Parse(FrqToAOR) < 3299999)
                {
                    if (DotInFrq) countafterdot++;
                    FrqToAOR += "6";
                    if (double.Parse(FrqToAOR) > 3299999) FrqToAOR = FrqToAOR.Substring(0, FrqToAOR.Length - 1);
                }
                lFrq.Text = FrqToAOR.ToString() + " " + kHz;// " кГц";
            }
            catch (Exception) { }
        }
        private void b7_Click(object sender, EventArgs e)
        {
            try
            {
                if (countafterdot > 2) return;
                if (!tmFlashFRQ.Enabled)
                    tmFlashFRQ.Start();
                lFRQFromAOR.Visible = false;
                lFrq.Visible = true;
                lFrq.ForeColor = Color.Green;
                if (FrqToAOR == "")
                {
                    FrqToAOR = "7";
                    lFrq.Text = FrqToAOR.ToString() + " " + kHz;// " кГц";
                    return;
                }
                if ((DotInFrq) && (countafterdot == 0))
                {
                    FrqToAOR += "7";
                    countafterdot++;
                    lFrq.Text = FrqToAOR.ToString() + " " + kHz;// " кГц";
                    return;
                }

                if (double.Parse(FrqToAOR) < 3299999)
                {
                    if (DotInFrq) countafterdot++;
                    FrqToAOR += "7";
                    if (double.Parse(FrqToAOR) > 3299999) FrqToAOR = FrqToAOR.Substring(0, FrqToAOR.Length - 1);
                }
                lFrq.Text = FrqToAOR.ToString() + " " + kHz;//" кГц";
            }
            catch (Exception) { }
        }
        private void b8_Click(object sender, EventArgs e)
        {
            try
            {
                if (countafterdot > 2) return;
                if (!tmFlashFRQ.Enabled)
                    tmFlashFRQ.Start();
                lFRQFromAOR.Visible = false;
                lFrq.Visible = true;
                lFrq.ForeColor = Color.Green;
                if (FrqToAOR == "")
                {
                    FrqToAOR = "8";
                    lFrq.Text = FrqToAOR.ToString() + " " + kHz;//" кГц";
                    return;
                }
                if ((DotInFrq) && (countafterdot == 0))
                {
                    FrqToAOR += "8";
                    countafterdot++;
                    lFrq.Text = FrqToAOR.ToString() + " " + kHz;//" кГц";
                    return;
                }

                if (double.Parse(FrqToAOR) < 3299999)
                {
                    if (DotInFrq) countafterdot++;
                    FrqToAOR += "8";
                    if (double.Parse(FrqToAOR) > 3299999) FrqToAOR = FrqToAOR.Substring(0, FrqToAOR.Length - 1);
                }
                lFrq.Text = FrqToAOR.ToString() + " " + kHz;//" кГц";
            }
            catch (Exception) { }
        }
        private void b9_Click(object sender, EventArgs e)
        {
            if (countafterdot > 2) return;
            if (!tmFlashFRQ.Enabled)
                tmFlashFRQ.Start();
            lFRQFromAOR.Visible = false;
            lFrq.Visible = true;
            lFrq.ForeColor = Color.Green;
            if (FrqToAOR == "")
            {
                FrqToAOR = "9";
                lFrq.Text = FrqToAOR.ToString() + " " + kHz;// " кГц";
                return;
            }
            if ((DotInFrq) && (countafterdot == 0))
            {
                FrqToAOR += "9";
                countafterdot++;
                lFrq.Text = FrqToAOR.ToString() + " " + kHz;// " кГц";
                return;
            }

            if (double.Parse(FrqToAOR) < 3299999)
            {
                if (DotInFrq) countafterdot++;
                FrqToAOR += "9";
                if (double.Parse(FrqToAOR) > 3299999) FrqToAOR = FrqToAOR.Substring(0, FrqToAOR.Length - 1);
            }
            lFrq.Text = FrqToAOR.ToString() + " " + kHz;// " кГц";
        }
        private void b0_Click(object sender, EventArgs e)
        {
            if (DotInFrq) countafterdot++;
            if (countafterdot > 3) return;
            if (!tmFlashFRQ.Enabled)
                tmFlashFRQ.Start();
            lFRQFromAOR.Visible = false;
            lFrq.Visible = true;
            lFrq.ForeColor = Color.Green;
            if (FrqToAOR == "")
            {
                FrqToAOR = "";
                lFrq.Text = "0 "  + kHz;//кГц";
                return;
            }
            if (double.Parse(FrqToAOR) < 330000)
                FrqToAOR += "0";
            lFrq.Text = FrqToAOR.ToString() + " " + kHz;//" кГц";
        }
        private void bDot_Click(object sender, EventArgs e)
        {
            if (DotInFrq == true) return;
            if (countafterdot > 3) return;
            DotInFrq = true;
            countafterdot = 0;
            if (!tmFlashFRQ.Enabled)
                tmFlashFRQ.Start();
            lFRQFromAOR.Visible = false;
            lFrq.Visible = true;
            lFrq.ForeColor = Color.Green;
            FrqToAOR += ",";
            lFrq.Text = FrqToAOR.ToString() + "0 " + kHz;//кГц";

        }
        private void bESC_Click(object sender, EventArgs e)
        {
            DotInFrq = false;
            countafterdot = 0;
            if (tmFlashFRQ.Enabled)
                tmFlashFRQ.Stop();
            lFRQFromAOR.Visible = true;
            lFrq.Visible = false;
            FrqToAOR = "";
            lFrq.Text = FrqToAOR.ToString();
        }
        private void bENT_Click(object sender, EventArgs e)
        {
            double frqq = 0;
            if ((DotInFrq) && (countafterdot == 0))
            {
                DotInFrq = false;
                countafterdot = 0;
                if (tmFlashFRQ.Enabled)
                    tmFlashFRQ.Stop();
                lFRQFromAOR.Visible = true;
                lFrq.Visible = false;
                FlashLedWriteARONE();
                double.TryParse(FrqToAOR.Substring(0, FrqToAOR.Length - 1), out frqq);
                if ((frqq > 30) && (frqq < 3300000)) radio_ARDV1.FrequencySet(frqq / 1000);
                FrqToAOR = "";
                Thread.Sleep(20);
                radio_ARDV1.FrequencyGet();
                return;
            }
            DotInFrq = false;
            countafterdot = 0;
            if (tmFlashFRQ.Enabled)
                tmFlashFRQ.Stop();
            lFRQFromAOR.Visible = true;
            lFrq.Visible = false;
            FlashLedWriteARONE();
            double.TryParse(FrqToAOR, out frqq);
            if ((frqq > 30) && (frqq < 3300000)) radio_ARDV1.FrequencySet(frqq / 1000);
            FrqToAOR = "";
            Thread.Sleep(20);
            radio_ARDV1.FrequencyGet();
            lFrq.Text = FrqToAOR.ToString();
        }
        private void trackBarManualGain_Scroll(object sender, EventArgs e)
        {
            try
            {
                FlashLedWriteARONE();
                if (radio_ARDV1.ArOne.AGC == 3)
                    radio_ARDV1.ManualGainSet(tbManualGain.Value);
            }
            catch { }
        }
        private void tmFlashFRQ_Tick(object sender, EventArgs e)
        {
            if (lFrq.ForeColor != Color.Green)
                lFrq.ForeColor = Color.Green;
            else
                lFrq.ForeColor = Color.Black;
        }
        int Poisk_chastoty_v_tabl(string freq, DataGridView DGV)
        {
            {if(DGV.RowCount<1){return -1;}
            for (int i = 0; i < DGV.RowCount; i++)
                if (DGV.Rows[i].Cells[3].Value.ToString().CompareTo(freq.ToString()) == 0) { return i; }
            }
            return -1;
        }
        void SignalYResized_Clear()
        {
            try
            {
                for (int j = 0; j < SignalYResized.Length; j++)
                {
                    SignalYResized[j] = -150;
                }
            }
            catch (Exception) { }
        }
        void Intensity_Clear()
        {
            try
            {
                for (int j = 0; j < WaterflowTime; j++)
                {
                    for (int i = 0; i < intensity.Length / WaterflowTime; i++)
                    {
                        intensity[i, j] = -150;
                    }
                }
            }
            catch (Exception) { }
        }
        void waveIn_DataAvailable(object sender, WaveInEventArgs e)
        {
            if (bAOR.BackColor == Color.Red) return;
            if (this.InvokeRequired)
            {
                this.BeginInvoke(new EventHandler<WaveInEventArgs>(waveIn_DataAvailable), sender, e);
            }
            else
            {
                if (writer != null) writer.WriteData(e.Buffer, 0, e.BytesRecorded);  //запись в файл
                byte[] buffer = e.Buffer;
                int bytesRecorded = e.BytesRecorded;
                Complex[] sig = new Complex[bytesRecorded / 2];
                for (int i = 0, j = 0; i < e.BytesRecorded; i += 2, j++)
                {
                    short sample = (short)((buffer[i + 1] << 8) | buffer[i + 0]);
                    sig[j] = sample / 32768f;
                    //link.AddLast(sig[j].Real.ToString());         //для осциллограммы
                    //if (link.Count > 10000)
                    //{
                    //    link.RemoveFirst();
                    //}
                }
                Transform.FourierForward(sig, FourierOptions.Matlab);

                foreach (Complex c in sig)
                {
                    if (max.Magnitude < c.Magnitude)
                    {
                        max = c;
                    }
                }

                for (int j = 0; j < bytesRecorded / 2; j++)
                {
                    sig1[j + iAverage * bytesRecorded] = sig[j];
                }
                iAverage++;
                if (iAverage == 5) { iAverage = 0; }

                CreateGraph2(sig, max);
            }
        }
        private void CreateGraph2(Complex[] sig, Complex max1)
        {
            int kol_previshenij = 0;
            try
            {
                if (InvokeRequired)
                {
                    Invoke((MethodInvoker)(() => CreateGraph2(sig, max1)));
                    return;
                }
                SignalYResized_Clear();
                qwe = 0;                             //точка сигнала на графике
                sig_sum = 0;                         //среднее за 5 отсчетов
                //double K = sig.Length / 2;
                for (int i = 0; i < K; i++)
                {
                    qwe = (Complex.Abs(sig[i]) * 1000000) / (N * 115 * max1.Magnitude);
                    qwe = 20 * Math.Log10(qwe);
                    SignalY[i] = qwe;
                    PorogObnar[i] = PorogZapisi;
                    if (iNumDataMx == count_baund) { iNumDataMx = 0; }
                    if (iNumDataSigma == count_baund) { iNumDataSigma = 0; }
                }

                for (int j = 0; j < sig.Length; j++)
                {
                    sig1[j + iAverage * sig.Length] = sig[j];
                }
                iAverage++;
                if (iAverage == 5) { iAverage = 0; }
                for (int i = 0; i < K; i++)
                {
                    sig_sum = ((
                        Complex.Abs(sig1[i + sig.Length * 0]) +
                        Complex.Abs(sig1[i + sig.Length * 1]) +
                        Complex.Abs(sig1[i + sig.Length * 2]) +
                        Complex.Abs(sig1[i + sig.Length * 3]) +
                        Complex.Abs(sig1[i + sig.Length * 4])) * 1000000) / (5 * N * 115 * max1.Magnitude);
                    sig_sum = 20 * Math.Log10(sig_sum);
                    SignalYAverage[i] = sig_sum;
                    //SignalYAverage[i] = Complex.FromPolarCoordinates(Math.Sqrt(K * K + sig_sum * sig_sum), Math.Atan2(sig_sum, K));
                    //if (i > 10 && i < 1000 && sig_sum > int.Parse(textBox3.Text)) { kol_previshenij++; }    // условие записи по порогу


                    if (i > 10 && i < 1000 && sig_sum > PorogZapisi) { kol_previshenij++; }    // условие записи по порогу
                }
                int ii = 0;
                for (int i = 0; i < K; i++)
                {
                    ii = i / delitel;
                    if (ii < SignalYResized.Length)
                        if (SignalYResized[ii] < SignalY[i]) SignalYResized[ii] = SignalY[i];
                }

                waterflow_count++;
                if (waterflow_count == Nwaterflow_count)
                {
                    waterflow_count = 0;
                }
                if (cbAverage.Checked) PlotAverage.PlotY(SignalYAverage, 0, Fn / K);
                PlotSignal.PlotY(SignalY, 0, Fn / K);
                PlotPorogObn.PlotY(PorogObnar, 0, Fn / K);
                //waveformPlot1.PlotY(SignalYResized,0, Fn / K);            //по ним рисуется водопад
                if (waterflow_count == 0)
                {
                    for (int i = 0; i < 2205 / delitel; i++)
                    {
                        for (int j = 0; j < WaterflowTime; j++)
                        {
                            if (j < WaterflowTime - 1) intensity[i, j] = intensity[i, j + 1];       // != 1я строчка
                            else
                            {
                                intensity[i, j] = SignalYResized[i]; //SignalY[i];//               // == 1я строчка

                                if (intensity[i, j] >= spektorYMaxPorog)
                                    intensity[i, j] = 0;
                                if (intensity[i, j] <= spektorYMinPorog)
                                    intensity[i, j] = -120;
                                if ((intensity[i, j] > spektorYMinPorog) & (intensity[i, j] < spektorYMaxPorog))
                                    intensity[i, j] = intensity[i, j] * (intensity[i, j] - spektorYMaxPorog) / (spektorYMinPorog - spektorYMaxPorog);
                            }
                        }
                    }
                    //if (cbIntensityPaint.Checked)
                    //{
                    //   // if (iAverage == 0)
                    //        intensityPlot1.Plot(intensity);
                    //        ThrIntensityGraphPain.Start(intensity);
                    //}

                    //if (cbIntensityPaint.Checked)
                    //{
                    //    if (TaskIntensityPaint == null)
                    //    {
                    //        TaskIntensityPaint = Task.Run(() => { intensityPlot1.Plot(intensity); });
                    //    }
                    //    else
                    //    {
                    //        if (TaskIntensityPaint.IsCompleted)
                    //        {
                    //            // ona zakonchena
                    //            TaskIntensityPaint = Task.Run(() => { intensityPlot1.Plot(intensity); });
                    //        }
                    //        else
                    //        {
                    //            // one eshe rabotaet
                    //        }
                    //    }
                    //}

                }


                //автоматическая запись *.wav
                if (cbARec.Checked)
                {
                    if (kol_previshenij_dlia_zapisi_time_count_i < kol_buf_ostanovka_zapisi)
                    {
                        if ((kol_previshenij > kol_previshenij_dlia_zapisi))
                        {
                            kol_previshenij_dlia_zapisi_time_count_i++;
                            if (kol_previshenij_dlia_zapisi_time_count_i > kol_previshenij_dlia_zapisi_time_count)
                            {
                                if (cbRec.Checked == false)
                                {
                                    // if(tmBaraban.Enabled)   tmBaraban.Stop();
                                    cbRec.Checked = true;                        //zapisyvat
                                }
                            }
                            else
                            {
                                //kol_previshenij_dlia_zapisi_time_count_i = 0;   //конец записи
                                // cbRec.Checked = false;
                                //if (tmBaraban.Enabled) tmBaraban.Start();
                            }
                        }

                        else
                        {
                            //if (kol_previshenij_dlia_zapisi_time_count_i > 5) { kol_previshenij_dlia_zapisi_time_count_i -= 5; }
                            //kol_buf_ostanovka_zapisi++;
                            // if (kol_buf_ostanovka_zapisi > 0)
                            // {
                            //   kol_buf_ostanovka_zapisi = 0;
                            //kol_previshenij_dlia_zapisi_time_count_i = 0;
                            cbRec.Checked = false;
                            //}
                        }
                    }
                    else
                    {
                        kol_previshenij_dlia_zapisi_time_count_i = 0;
                        cbRec.Checked = false;
                    }
                }


            }
            catch (Exception) { }
        }
        //if (cbIntensityPaint.Checked)
        //{
        //    if(iAverage==0)
        //    //intensityPlot1.Plot(intensity);
        //    ThrIntensityGraphPain.Start(intensity);
        //}

        async void ThrWaterflowPaint()
        {
            while (!closing)
            {
                if (cbIntensityPaint.Checked)
                    try
                    {
                        //Thread.Sleep200);
                        await Task.Delay(100);
                        ThrIntensityGraphN++;
                        if (ThrIntensityGraphN > ThrIntensityGraphNeedCount)
                        {
                            this.intensityGraph1.BeginInvoke((MethodInvoker)(() => this.intensityPlot1.Plot(intensity)));
                            ThrIntensityGraphN = 0;
                            //Thread.Sleep(200);
                        }
                    }
                    catch { }
            }
        }
        private void tbTableTime_KeyPress(object sender, KeyPressEventArgs e)
        {
            //if (tbTableTime.Text.Length > 8)
            //{
            //    tbTableTime.Text = tbTableTime.Text.Substring(0, 8);
            //    return;
            //}

            //if ((e.KeyChar >= '0') && (e.KeyChar <= '9') || (e.KeyChar == '\b') || (e.KeyChar == ':')) // цифра, Backspace
            //{
            //    return;
            //}

            //// остальные символы запрещены
            //e.Handled = true;  
        }
        private void tbTableLevel_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= '0') && (e.KeyChar <= '9') || (e.KeyChar == '\b') || (e.KeyChar == '-')) // цифра, Backspace
            {
                return;
            }
            e.Handled = true;
        }
        private void tbTableFRQ_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= '0') && (e.KeyChar <= '9') || (e.KeyChar == '\b') || (e.KeyChar == ',')) // цифра, Backspace
            {
                return;
            }
            e.Handled = true;

        }

        private void nudTableFRQ_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= '0') && (e.KeyChar <= '9') || (e.KeyChar == '\b') || (e.KeyChar == ',')) // цифра, Backspace
            {
                return;
            }
            e.Handled = true;

        }
        private void nudTableLevel_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= '0') && (e.KeyChar <= '9') || (e.KeyChar == '\b') || (e.KeyChar == '-')) // цифра, Backspace
            {
                return;
            }
            e.Handled = true;

        }
        private void nudTableFRQ_KeyUp(object sender, KeyEventArgs e)
        {
            Int64 qwe;
            Int64.TryParse(nudTableFRQ.Text.ToString(), out qwe);
            if (qwe > 3300000) nudTableFRQ.Text = nudTableFRQ.Text.Substring(0, 7);
        }
        private void nudTableLevel_Validated(object sender, EventArgs e)
        {
            int qwe;
            int.TryParse(nudTablePorogObnar.Text.ToString(), out qwe);
            if ((Math.Abs(qwe)) > 120)
            {
                nudTablePorogObnar.Text = "-120";
                return;
            }
        }
        private void tm500_Tick(object sender, EventArgs e)
        {
            if (bAOR.BackColor != Color.Green) return;
            FlashLedWriteARONE();
            radio_ARDV1.SMeterGet();

        }
        private void textBoxFrq_Validated(object sender, EventArgs e)
        {

        }
        private void buttn8_Click(object sender, EventArgs e)
        {
            lFRQFromAOR.Text = (123456789.9).ToString("N", nfi);

        }
        private void intensityGraph1_Resize(object sender, EventArgs e)
        {
            intensityGraph1Resize();
        }
        private void intensityGraph1Resize()
        {
            resolutionX = intensityGraph1.Size.Width;
            resolutionY = intensityGraph1.Size.Height;

            delitel = 2205 / resolutionX;
            if (delitel < 2) delitel = 2;
            if (delitel > 10) delitel = 10;
            intensity = new double[2205 / delitel, WaterflowTime];
            SignalYResized = new double[2205 / delitel];
            SignalYResized_Clear();
            Intensity_Clear();
            intensityXAxis1.Range = new NationalInstruments.UI.Range(0, (int)(2205 / delitel));
        }
        private void cbAverage_Click(object sender, EventArgs e)
        {
            if (!cbAverage.Checked)
            {
                PlotAverage.ClearData();
                cbAverage.Image =ControlARDV1.Properties.Resources.average_blue;
            }
            else
            {
                cbAverage.Image = ControlARDV1.Properties.Resources.average_gray;

            }
        }
        private void cbACP_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (cbACP.Checked)
                {
                    cbACP.Image = ControlARDV1.Properties.Resources.stop;
                    waveIn.DataAvailable += waveIn_DataAvailable;   //Прикрепляем к событию DataAvailable обработчик, возникающий при наличии записываемых данных
                }
                else
                {
                    waveIn.DataAvailable -= waveIn_DataAvailable;
                    cbACP.Image = ControlARDV1.Properties.Resources.play;
                }
            }
            catch (Exception) { }
        }
        private void cbIntensityPaint_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (cbIntensityPaint.Checked)
                {
                    cbIntensityPaint.Image = ControlARDV1.Properties.Resources.stop;
                }
                else
                {
                    cbIntensityPaint.Image = ControlARDV1.Properties.Resources.play;
                }
            }
            catch (Exception) { }
        }
        private void cbRec_CheckedChanged(object sender, EventArgs e)
        {
            if (bAOR.BackColor != Color.Green) return;      //?=connected
            try
            {
                lREC.Visible = true;
                taimer_zapisi = 0;
                if (cbRec.Checked)
                {
                    cbRec.Image = ControlARDV1.Properties.Resources.rec_red;


                    string path = String.Format(Application.StartupPath + "\\wav\\" + DateTime.Now.Year.ToString("0000") + "-" + DateTime.Now.Month.ToString("00") + "-" + DateTime.Now.Day.ToString("00"));
                    if (!Directory.Exists(path))
                    {
                        DirectoryInfo di = Directory.CreateDirectory(path);
                    }

                    //string outputFilename = String.Format("{0}{1}-{2}-{3}{4}{5}-{6}:{7}:{8}{9}", "wav\\", DateTime.Now.Year.ToString("0000"), DateTime.Now.Month.ToString("00"), DateTime.Now.Day.ToString("00"), "\\", classLibrary_ARONE.ArOne.frequency.ToString(), DateTime.Now.Hour.ToString("00"), DateTime.Now.Minute.ToString("00"), DateTime.Now.Second.ToString("00"), ".wav");
                    string outputFilename = path + "\\" + radio_ARDV1.ArOne.frequency.ToString() + "_" + DateTime.Now.Hour.ToString("00") + "-" + DateTime.Now.Minute.ToString("00") + "-" + DateTime.Now.Second.ToString("00") + ".wav";


                    lREC.Text = "REC " + outputFilename.Substring(path.Length + 1, outputFilename.Length - path.Length - 5);
                    //outputFilename=outputFilename.Replace("\\","\");
                    writer = new WaveFileWriter(outputFilename, waveIn.WaveFormat);
                }//wav\\98900000-180-(17-38-33).wav
                //D:\\mywork\\Main\\WindowsFormsApplication1\\WindowsFormsApplication1\\bin\\Debug\\wav\\2017-06-29\\98900000-17:39:14.wav
                else
                {
                    lREC.Visible = false;
                    cbRec.Image = ControlARDV1.Properties.Resources.rec_grey;
                    if (writer != null)
                    {
                        writer.Close();
                        writer = null;
                    }
                }
            }
            catch (Exception) { }

        }
        private void cbARec_CheckedChanged(object sender, EventArgs e)
        {

            if (bAOR.BackColor != Color.Green) return;      //?=connected
            try
            {
                if (cbARec.Checked)
                {
                    cbARec.Image = ControlARDV1.Properties.Resources.a_rec_red;
                }
                else
                {
                    cbARec.Image = ControlARDV1.Properties.Resources.a_rec_grey;
                    cbRec.Checked = false;
                }
            }
            catch (Exception) { }
        }
        private void bTableDel_Click(object sender, EventArgs e)
        {
            if (myDataGridView1.CurrentRow == null) return;
            int ID = int.Parse(myDataGridView1.CurrentRow.Cells[0].Value.ToString());//= int.Parse(table_AR_ONE1.dgvAR_ONE.dgv.CurrentRow.Cells[0].Value.ToString());
            //functionsDB.DeleteOneRecordDB(ID, NameTable.AR_ONE, 0);
            try
            {
                sqLiteConnect.Open();
                sqLiteCommand = new System.Data.SQLite.SQLiteCommand("DELETE FROM " + NameTable.ARDV1 + " WHERE ID=" + ID, sqLiteConnect);
                sqLiteCommand.ExecuteNonQuery();
                zagruzka_tablFromBD(NameTable.ARDV1.ToString());
            }
            catch { }
            //if (myDataGridView1.CurrentRow == null) return;
            //int ID = int.Parse(myDataGridView1.CurrentRow.Cells[0].Value.ToString());
            //functionsDB.DeleteOneRecordDB(ID, NameTable.ARDV1, 0);
            //zagruzka_tablFromBD(NameTable.ARDV1.ToString());
        }
        private void bTableAdd_Click(object sender, EventArgs e)
        {
            try
            {
                if ((comboBoxBW.SelectedIndex <0) || (comboBoxBW.SelectedIndex < 0)) return;
                StrARDV1.iFreq = (int)(nudTableFRQ.Value * 100);
                if ((StrARDV1.iFreq < 10000) || (StrARDV1.iFreq > 1300000000)) return;
                StrARDV1.iU = (int)nudTablePorogObnar.Value;
                StrARDV1.bPriority = (byte)cbTablePriority.SelectedIndex;
                StrARDV1.iBW = comboBoxBW.SelectedIndex;
                StrARDV1.iMode = comboBoxMode.SelectedIndex;
                StrARDV1.iOnOff = 1;
                StrARDV1.iPause = (int)nudPause.Value;
                StrARDV1.Note = tbNote.Text.ToString();
                StrARDV1.sTimeFirst = DateTime.Now.Hour.ToString("00") + DateTime.Now.Minute.ToString("00") + DateTime.Now.Second.ToString("00");
                sqLiteConnect = new System.Data.SQLite.SQLiteConnection(functionsDB.ConnectionString());             // подключиться к БД      
                sqLiteConnect.Open();
                sqLiteCommand = new System.Data.SQLite.SQLiteCommand(
                    "INSERT INTO ARDV1(OnOff,Freq, Pause, U, TimeFirst, Mode, BW, Priority, Note) VALUES (" + StrARDV1.iOnOff + "," + StrARDV1.iFreq + "," + StrARDV1.iPause + "," + StrARDV1.iU + ",'" + StrARDV1.sTimeFirst + "'," + StrARDV1.iMode + "," + StrARDV1.iBW + "," + StrARDV1.bPriority + ",'" + StrARDV1.Note.ToString() + "')", sqLiteConnect);
                int qwee = sqLiteCommand.ExecuteNonQuery();
                sqLiteConnect.Close();
                zagruzka_tablFromBD(NameTable.ARDV1.ToString());
            }
            catch { }
        }
        public void AddFrqToTablARDV1(Int64 Freq_Hz, byte Prority)
        {
            if ((Freq_Hz < 10000) || (Freq_Hz > 1300000000)) return;
            int qwe = Poisk_chastoty_v_tabl(nudTableFRQ.Value.ToString(), myDataGridView1);
            if (qwe != -1)
            {
                myDataGridView1.FirstDisplayedScrollingRowIndex = qwe;
                myDataGridView1.Rows[qwe].Selected = true;
                return;
            }
            StrARDV1.iFreq = (Freq_Hz);
            StrARDV1.iU = -50;
            StrARDV1.bPriority = (byte)Prority;
            StrARDV1.iBW = 5;
            StrARDV1.iMode = 5;
            StrARDV1.iOnOff = 1;
            StrARDV1.iPause = 1000;
            StrARDV1.Note = "";
            StrARDV1.sTimeFirst = DateTime.Now.Hour.ToString("00") + DateTime.Now.Minute.ToString("00") + DateTime.Now.Second.ToString("00");
            sqLiteConnect = new System.Data.SQLite.SQLiteConnection(functionsDB.ConnectionString());             // подключиться к БД      
            sqLiteConnect.Open();
            sqLiteCommand = new System.Data.SQLite.SQLiteCommand(
                    "INSERT INTO ARDV1(OnOff,Freq, Pause, U, TimeFirst, Mode, BW, Priority, Note) VALUES (" + StrARDV1.iOnOff + "," + StrARDV1.iFreq + "," + StrARDV1.iPause + "," + StrARDV1.iU + ",'" + StrARDV1.sTimeFirst + "'," + StrARDV1.iMode + "," + StrARDV1.iBW + "," + StrARDV1.bPriority + ",'" + StrARDV1.Note.ToString() + "')", sqLiteConnect);
            int qwee = sqLiteCommand.ExecuteNonQuery();
            sqLiteConnect.Close();
            zagruzka_tablFromBD(NameTable.ARDV1.ToString());
        }
        private int UpdateSaveToBD(int IDn)
        {
            StrARDV1.iFreq = int.Parse(((double)nudTableFRQ.Value * 100).ToString());
            StrARDV1.iU = (int)nudTablePorogObnar.Value;
            StrARDV1.bPriority = (byte)cbTablePriority.SelectedIndex;
            StrARDV1.iBW = comboBoxBW.SelectedIndex;
            StrARDV1.iMode = comboBoxMode.SelectedIndex;
            bool tru = true;
            if (myDataGridView1.CurrentRow.Cells[1].Value.ToString() == tru.ToString())
                StrARDV1.iOnOff = 1;
            else
                StrARDV1.iOnOff = 0;
            StrARDV1.iPause = (int)nudPause.Value;
            StrARDV1.Note = tbNote.Text.ToString();
            StrARDV1.sTimeFirst = DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString();
            sqLiteConnect = new System.Data.SQLite.SQLiteConnection(functionsDB.ConnectionString());             // подключиться к БД      
            sqLiteConnect.Open();
            sqLiteCommand = new System.Data.SQLite.SQLiteCommand(
                "UPDATE ARDV1 SET OnOff=" + StrARDV1.iOnOff + ", Freq=" + StrARDV1.iFreq + ", Note='" + StrARDV1.Note +"', Pause=" + StrARDV1.iPause + ", U=" + StrARDV1.iU + ", TimeFirst='" + StrARDV1.sTimeFirst + "',  Mode=" + StrARDV1.iMode + ", BW=" + StrARDV1.iBW + ", Priority=" + StrARDV1.bPriority + " WHERE ID=" + IDn, sqLiteConnect);
            int qwee = sqLiteCommand.ExecuteNonQuery();
            sqLiteConnect.Close();
            return 1;
        }

        private void bTableClear_Click(object sender, EventArgs e)
        {
            if (myDataGridView1.CurrentRow == null) return;
            try
            {
                sqLiteConnect.Open();
                sqLiteCommand = new System.Data.SQLite.SQLiteCommand("DELETE FROM " + NameTable.ARDV1, sqLiteConnect);
                sqLiteCommand.ExecuteNonQuery();
                zagruzka_tablFromBD(NameTable.ARDV1.ToString());
            }
            catch { }
            //functionsDB.DeleteAllRecordsDB(NameTable.AR_ONE, 0);
            zagruzka_tablFromBD(NameTable.ARDV1.ToString());
            //functionsDB.DeleteAllRecordsDB(NameTable.ARDV1, 0);
            //zagruzka_tablFromBD(NameTable.ARDV1.ToString());
        }
        private void rbSQLNoise_CheckedChanged(object sender, EventArgs e)
        {
            FlashLedWriteARONE();
            radio_ARDV1.SquelchSelectSet(0);

        }
        private void rbSQLLevel_CheckedChanged(object sender, EventArgs e)
        {
            FlashLedWriteARONE();
            radio_ARDV1.SquelchSelectSet(1);
        }
        private void tmSignalLevel_Tick(object sender, EventArgs e)
        {
            FlashLedWriteARONE();
            radio_ARDV1.SMeterGet();
        }
        private void tbWaterflowMax_Scroll(object sender, EventArgs e)
        {
            spektorYMaxPorog = tbWaterflowMax.Value;
        }
        private void tbWaterflowMin_Scroll(object sender, EventArgs e)
        {
            spektorYMinPorog = tbWaterflowMin.Value;
        }
        private void tbWaterflowMin_ValueChanged(object sender, EventArgs e)
        {
            if (tbWaterflowMin.Value < tbWaterflowMax.Value)
            {
                spektorYMinPorog = tbWaterflowMin.Value;
            }
            else { tbWaterflowMin.Value = tbWaterflowMax.Value - 1; }
        }
        private void tbWaterflowMax_ValueChanged(object sender, EventArgs e)
        {
            if (tbWaterflowMax.Value > tbWaterflowMin.Value)
            {
                spektorYMaxPorog = tbWaterflowMax.Value;
            }
            else { tbWaterflowMax.Value = tbWaterflowMin.Value + 1; }

        }
        private void pRecDev_Resize(object sender, EventArgs e)
        {
            cbIntensityPaint.Location = new Point(2, 200 + intensityGraph1.Location.Y);
        } 


        public void zagruzka_tablFromBD(string nameTable)
        {
            int jsql = 0;
            int val = 0;
            int valm = 0;
            try
            {
                myDataGridView1.Rows.Clear();
                jsql = 0;
                //    if (functionsDB.ConnectionString() != "")
                //    {
                sqLiteConnect = new System.Data.SQLite.SQLiteConnection(functionsDB.ConnectionString());             // подключиться к БД     
                //string strConnect = @"Data Source=C:/database.db";
                //sqLiteConnect = new System.Data.SQLite.SQLiteConnection(strConnect);
                sqLiteConnect.Open();
                sqLiteCommand = new System.Data.SQLite.SQLiteCommand("SELECT * FROM " + nameTable, sqLiteConnect);
                sqLiteRead = sqLiteCommand.ExecuteReader();
                while (sqLiteRead.Read())
                {
                    if (jsql >= myDataGridView1.RowCount)
                        myDataGridView1.Rows.Add(null, false, null, null, null, null, null, null, null, null);
                    myDataGridView1.Rows[jsql].Cells[0].Value = Convert.ToString(sqLiteRead["ID"]);
                    if (Convert.ToBoolean(sqLiteRead["OnOff"]) == false)
                        myDataGridView1.Rows[jsql].Cells[1].Value = false;
                    else myDataGridView1.Rows[jsql].Cells[1].Value = true;
                    myDataGridView1.Rows[jsql].Cells[2].Value = String.Format("{0:# ### #00.000}", Convert.ToDouble(sqLiteRead["Freq"]) / 100);//(Convert.ToDouble(sqLiteRead["Freq"])/100).ToString();
                    myDataGridView1.Rows[jsql].Cells[3].Value = Convert.ToString(sqLiteRead["Pause"]);
                    myDataGridView1.Rows[jsql].Cells[4].Value = Convert.ToString(sqLiteRead["U"]);
                    myDataGridView1.Rows[jsql].Cells[5].Value = Convert.ToString(sqLiteRead["TimeFirst"]).Insert(2, ":").Insert(5, ":");

                    val = Convert.ToInt32(sqLiteRead["Mode"]);
                    switch (val)
                    {
                        case 0:
                            myDataGridView1.Rows[jsql].Cells[6].Value = "AUTO(digital)";
                            break;
                        case 1:
                            myDataGridView1.Rows[jsql].Cells[6].Value = "D-STAR";
                            break;
                        case 2:
                            myDataGridView1.Rows[jsql].Cells[6].Value = "YAESU";
                            break;
                        case 3:
                            myDataGridView1.Rows[jsql].Cells[6].Value = "ALINCO";
                            break;
                        case 4:
                            myDataGridView1.Rows[jsql].Cells[6].Value = "D-CR/NXDN";
                            break;
                        case 5:
                            myDataGridView1.Rows[jsql].Cells[6].Value = "P-25(APCO25)";
                            break;
                        case 6:
                            myDataGridView1.Rows[jsql].Cells[6].Value = "dPMR";
                            break;
                        case 7:
                            myDataGridView1.Rows[jsql].Cells[6].Value = "DMR";
                            break;

                        case 8:
                            myDataGridView1.Rows[jsql].Cells[6].Value = "FM";
                            break;
                        case 9:
                            myDataGridView1.Rows[jsql].Cells[6].Value = "AM";
                            break;
                        case 10:
                            myDataGridView1.Rows[jsql].Cells[6].Value = "SAH";
                            break;
                        case 11:
                            myDataGridView1.Rows[jsql].Cells[6].Value = "SAL";
                            break;
                        case 12:
                            myDataGridView1.Rows[jsql].Cells[6].Value = "USB";
                            break;
                        case 13:
                            myDataGridView1.Rows[jsql].Cells[6].Value = "LSB";
                            break;
                        case 14:
                            myDataGridView1.Rows[jsql].Cells[6].Value = "CW";
                            break;
                    }

                    if (val < 9)
                    {
                        valm = Convert.ToInt32(sqLiteRead["BW"]);
                        switch (valm)
                        {
                            case 0:
                                myDataGridView1.Rows[jsql].Cells[7].Value = "200 " + kHz;//кГц";
                                break;
                            case 1:
                                myDataGridView1.Rows[jsql].Cells[7].Value = "100 " + kHz;//кГц";
                                break;
                            case 2:
                                myDataGridView1.Rows[jsql].Cells[7].Value = "30 " + kHz;//кГц";
                                break;
                            case 3:
                                myDataGridView1.Rows[jsql].Cells[7].Value = "15 " + kHz;//кГц";
                                break;
                            case 4:
                                myDataGridView1.Rows[jsql].Cells[7].Value = "6,0 " + kHz;//кГц";
                                break;
                        }
                    }

                    if (val == 9)
                    {
                        valm = Convert.ToInt32(sqLiteRead["BW"]);
                        switch (valm)
                        {
                            case 0:
                                myDataGridView1.Rows[jsql].Cells[7].Value = "15 " + kHz;//кГц";
                                break;
                            case 1:
                                myDataGridView1.Rows[jsql].Cells[7].Value = "8 " + kHz;//кГц";
                                break;
                            case 2:
                                myDataGridView1.Rows[jsql].Cells[7].Value = "5.5 " + kHz;//кГц";
                                break;
                            case 3:
                                myDataGridView1.Rows[jsql].Cells[7].Value = "3.8 " + kHz;//кГц";
                                break;
                        }
                    }


                    if ((val == 11) || (val == 10))
                    {
                        valm = Convert.ToInt32(sqLiteRead["BW"]);
                        switch (valm)
                        {
                            case 0:
                                myDataGridView1.Rows[jsql].Cells[7].Value = "5.5 " + kHz;//кГц";
                                break;
                            case 1:
                                myDataGridView1.Rows[jsql].Cells[7].Value = "3.8 " + kHz;//кГц";
                                break;
                        }
                    }
                    if ((val == 13) || (val == 12))
                    {
                        valm = Convert.ToInt32(sqLiteRead["BW"]);
                        switch (valm)
                        {

                            case 0:
                                myDataGridView1.Rows[jsql].Cells[7].Value = "2.6 " + kHz;//кГц";
                                break;
                            case 1:
                                myDataGridView1.Rows[jsql].Cells[7].Value = "1.8 " + kHz;//кГц";
                                break;
                        }
                    }
                    if (val == 14)
                    {
                        valm = Convert.ToInt32(sqLiteRead["BW"]);
                        switch (valm)
                        {
                            case 0:
                                myDataGridView1.Rows[jsql].Cells[7].Value = "0.5 " + kHz;//кГц";
                                break;
                            case 1:
                                myDataGridView1.Rows[jsql].Cells[7].Value = "0.2 " + kHz;//кГц";
                                break;
                        }
                    }
                    val = Convert.ToInt32(sqLiteRead["Priority"]);
                    switch (val)
                    {
                        case 0:
                            myDataGridView1.Rows[jsql].Cells[8].Value = Obychnyj;//"Обычный";
                            break;
                        case 1:
                            myDataGridView1.Rows[jsql].Cells[8].Value = Vazhnyj;//"Важный";
                            break;
                    }
                    myDataGridView1.Rows[jsql].Cells[9].Value = Convert.ToString(sqLiteRead["Note"]);
                    jsql++;
                }

            }
            catch (Exception)
            {
                MessageBox.Show(OshibkaChtenijaDannyhTablicy,Oshibka, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.ServiceNotification);
                //MessageBox.Show("Ошибка чтения данных таблицы!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.ServiceNotification);
                //fLoad = false;

                //return fLoad;
            }
            finally
            {
                sqLiteConnect.Close();
                if (myDataGridView1.RowCount < 6) myDataGridView1.RowCount = 6;
            }
        }


        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            IDBaraban0Count = 0;
            IDBaraban1Count = 0;
            if (radioButton2.Checked)
            {
                if (myDataGridView1.RowCount < 2) 
                {
                    radioButton1.Checked = true;
                    tmBaraban.Enabled = false;
                    return; 
                }
                int IDNum = myDataGridView1.CurrentRow.Index;
                if (myDataGridView1.Rows[IDNum].Cells[0].Value == null)
                {
                    radioButton1.Checked = true;
                    tmBaraban.Enabled = false;
                    return;
                }
                IDBaraban0 = new int[myDataGridView1.RowCount];
                IDBaraban1 = new int[myDataGridView1.RowCount];

                int countPrior = ZapolnitPrioritety();
                if (countPrior == -1)
                {
                    radioButton1.Checked = true;
                    tmBaraban.Enabled = false;
                    return;
                }
                tmBaraban.Interval = 100;
                //if (BarabanPriorityWorking) tmBaraban.Interval = int.Parse(dgv.Rows[IDBaraban1[0]].Cells[3].Value.ToString());
                //else tmBaraban.Interval = int.Parse(dgv.Rows[IDBaraban0[0]].Cells[3].Value.ToString());

                tmBaraban.Enabled = true;
                //cbARec.Enabled = true;
            }
            else
            {
                tmBaraban.Enabled = false;
                BarabanPriorityWorking = false;
                Baraban0Num = 0;
                Baraban1Num = 0;
                IDBaraban0Count = 0;
                IDBaraban1Count = 0;
            }
            //IDBaraban0Count = 0;
            //IDBaraban1Count = 0;
            //if (radioButton2.Checked)
            //{
            //    int IDNum = myDataGridView1.CurrentRow.Index;
            //    if (myDataGridView1.Rows[IDNum].Cells[0].Value == null)
            //    {
            //        radioButton1.Checked = true;
            //        return;
            //    }
            //    IDBaraban0 = new int[myDataGridView1.RowCount];
            //    IDBaraban1 = new int[myDataGridView1.RowCount];

            //    int countPrior = ZapolnitPrioritety();
            //    if (countPrior < 2)
            //    {
            //        radioButton1.Checked = true;
            //        tmBaraban.Enabled = false;
            //    }

            //    if (BarabanPriorityWorking) tmBaraban.Interval = int.Parse(myDataGridView1.Rows[IDBaraban1[0]].Cells[3].Value.ToString());
            //    else tmBaraban.Interval = int.Parse(myDataGridView1.Rows[IDBaraban0[0]].Cells[3].Value.ToString());
            //    tmBaraban.Enabled = true;
            //    cbARec.Enabled = true;
            //}
            //else
            //{
            //    tmBaraban.Enabled = false;
            //    BarabanPriorityWorking = false;
            //    cbARec.Enabled = false;
            //    Baraban0Num = 0;
            //    Baraban1Num = 0;
            //    IDBaraban0Count = 0;
            //    IDBaraban1Count = 0;
            //}


        }
        private int ZapolnitPrioritety()
        {
            for (int i = 0; i < myDataGridView1.RowCount; i++)
            {
                if ((myDataGridView1.Rows[i].Cells[8].Value != null)&&(myDataGridView1.Rows[i].Cells[1].Value.ToString() == "True"))
                {
                    try
                    {
                        string qwe = myDataGridView1.Rows[i].Cells[8].Value.ToString().Trim();
                        string qwee = myDataGridView1.Rows[i].Cells[1].Value.ToString();
                        if ((myDataGridView1.Rows[i].Cells[8].Value.ToString() == Obychnyj) && (myDataGridView1.Rows[i].Cells[1].Value.ToString() == "True"))
                        {
                            IDBaraban0[IDBaraban0Count] = i;
                            IDBaraban0Count++;
                        }
                        if ((myDataGridView1.Rows[i].Cells[8].Value.ToString() == Vazhnyj) && (myDataGridView1.Rows[i].Cells[1].Value.ToString() == "True"))
                        {
                            IDBaraban1[IDBaraban1Count] = i;
                            IDBaraban1Count++;
                            BarabanPriorityWorking = true;
                        }
                    }
                    catch { return -1; }
                }
            }
            int countZP = IDBaraban0Count + IDBaraban1Count;
            if (IDBaraban0Count * IDBaraban1Count == 0) countZP = (-1) * countZP;
            return countZP;
        }
        //private int ZapolnitPrioritety()
        //{
        //    for (int i = 0; i < myDataGridView1.RowCount; i++)
        //    {
        //        if (myDataGridView1.Rows[i].Cells[8].Value != null)
        //        {
        //            string qwe = myDataGridView1.Rows[i].Cells[8].Value.ToString().Trim();
        //            string qwee = myDataGridView1.Rows[i].Cells[1].Value.ToString();
        //            if (myDataGridView1.Rows[i].Cells[8].Value.ToString() == "Обычный" && myDataGridView1.Rows[i].Cells[1].Value.ToString() == "True")
        //            {
        //                IDBaraban0[IDBaraban0Count] = i;
        //                IDBaraban0Count++;
        //            }
        //            if (myDataGridView1.Rows[i].Cells[8].Value.ToString() == "Важный" && myDataGridView1.Rows[i].Cells[1].Value.ToString() == "True")
        //            {
        //                IDBaraban1[IDBaraban1Count] = i;
        //                IDBaraban1Count++;
        //                BarabanPriorityWorking = true;
        //            }
        //        }
        //    }
        //    int countZP = IDBaraban0Count + IDBaraban1Count;
        //    return countZP;
        //}
        private void tmBaraban_Tick(object sender, EventArgs e)
        {
            if ((cbRec.Checked) & (taimer_zapisi < taimer_zapisi_max) & (radioButton2.Checked)) { return; }//если идет запись, то ждать
            if ((cbRec.Checked) & (taimer_zapisi >= taimer_zapisi_max) & (radioButton2.Checked))
            { taimer_zapisi = 0; Thread.Sleep(0); cbRec.Checked = false; }

            if (((IDBaraban0Count == 0) && (IDBaraban1Count == 0)) || (!radioButton2.Checked))
            {
                tmBaraban.Enabled = false;                //Программная перестройка ВЫКЛ";
                radioButton1.Checked = true;
                return;
            }
            if ((IDBaraban0Count != 0) && (IDBaraban1Count != 0))
            {
                try
                {
                    if (BarabanPriorityWorking) //если приоритетные
                    {
                        Perestroika(IDBaraban1[Baraban1Num]);
                        Baraban1Num++;
                        if (Baraban1Num >= IDBaraban1Count)
                        {
                            Baraban1Num = 0;
                            BarabanPriorityWorking = false;
                        }
                    }
                    else    //если обычные
                    {
                        Perestroika(IDBaraban0[Baraban0Num]);
                        Baraban0Num++;
                        BarabanPriorityWorking = true;
                        if (Baraban0Num >= IDBaraban0Count)
                        {
                            Baraban0Num = 0;
                            //BarabanPriorityWorking = true;
                        }
                    }
                }
                catch { }
            }
            
                        
            if ((IDBaraban0Count * IDBaraban1Count) < 1)        //если с одинаковым приоритетом все
            {
                if (BarabanPriorityWorking)
                {
                    try
                    {
                        Perestroika(IDBaraban1[Baraban1Num]);
                        Baraban1Num++;
                        if (Baraban1Num >= IDBaraban1Count)
                        {
                            Baraban1Num = 0;
                        }
                    }
                    catch { }
                }
                else
                {
                    try
                    {
                        Perestroika(IDBaraban0[Baraban0Num]);
                        Baraban0Num++;
                        if (Baraban0Num >= IDBaraban0Count)
                        {
                            Baraban0Num = 0;
                        }
                    }
                    catch { }
                }
            }



            //if ((IDBaraban0Count * IDBaraban1Count) < 1)        //если с одинаковым приоритетом все
            //{
            //    try
            //    {
            //        Perestroika(ibaraban);
            //    }
            //    catch { }
            //    ibaraban++;
            //    try
            //    {

            //        if ((ibaraban >= myDataGridView1.RowCount) || (myDataGridView1.Rows[ibaraban].Cells[2].Value.ToString() == null))
            //        {
            //            ibaraban = 0;
            //        }
            //    }
            //    catch { ibaraban = 0; }

            //}


            //if (((IDBaraban0Count == 0) && (IDBaraban1Count == 0)) || (!radioButton2.Checked))
            //{
            //    tmBaraban.Enabled = false;                //Программная перестройка ВЫКЛ";
            //    radioButton1.Checked = true;
            //    return;
            //}
            //if (BarabanPriorityWorking) //если приоритетные
            //{
            //    Perestroika(IDBaraban1[Baraban1Num]);
            //    Baraban1Num++;
            //    if (Baraban1Num >= IDBaraban1Count)
            //    {
            //        Baraban1Num = 0;
            //        BarabanPriorityWorking = false;
            //    }
            //}
            //else    //если обычные
            //{
            //    Perestroika(IDBaraban0[Baraban0Num]);
            //    Baraban0Num++;
            //    BarabanPriorityWorking = true;
            //    if (Baraban0Num >= IDBaraban0Count)
            //    {
            //        Baraban0Num = 0;
            //        //BarabanPriorityWorking = true;
            //    }

            //}
        }
        private async void Perestroika(int IDNum)
        {
            myDataGridView1.FirstDisplayedScrollingRowIndex = IDNum;
            myDataGridView1.Rows[IDNum].Selected = true;
            if (myDataGridView1.Rows[IDNum].Cells[2].Value == null) return;
            //double d = double.Parse(myDataGridView1.Rows[IDNum].Cells[2].Value.ToString()) / 1000;
            FlashLedWriteARONE();
            radio_ARDV1.FrequencySet(double.Parse(myDataGridView1.Rows[IDNum].Cells[2].Value.ToString()) / 1000); //перевод в MHz
            radio_ARDV1.FrequencyGet();
            tmBaraban.Interval = int.Parse(myDataGridView1.Rows[IDNum].Cells[3].Value.ToString());
            nudTablePorogObnar.Value = int.Parse(myDataGridView1.Rows[IDNum].Cells[4].Value.ToString());
            sval = myDataGridView1.Rows[IDNum].Cells[6].Value.ToString();
            FlashLedWriteARONE();
            cbTablePriority.Text = myDataGridView1.Rows[IDNum].Cells[8].Value.ToString();
            tbNote.Text = myDataGridView1.Rows[IDNum].Cells[9].Value.ToString();
            PorogZapisi = int.Parse(myDataGridView1.Rows[IDNum].Cells[4].Value.ToString());
            FlashLedWriteARONE();
            sval = myDataGridView1.Rows[IDNum].Cells[6].Value.ToString();
            switch (sval)
            {
                case "AUTO(digital)":
                    radio_ARDV1.ModeSet(0);
                    break;
                case "D-STAR":
                    radio_ARDV1.ModeSet(1);
                    break;
                case "YAESU":
                    radio_ARDV1.ModeSet(2);
                    break;
                case "ALINCO":
                    radio_ARDV1.ModeSet(3);
                    break;
                case "D-CR/NXDN":
                    radio_ARDV1.ModeSet(4);
                    break;
                case "P-25(APCO25)":
                    radio_ARDV1.ModeSet(5);
                    break;
                case "dPMR":
                    radio_ARDV1.ModeSet(6);
                    break;
                case "DMR":
                    radio_ARDV1.ModeSet(7);
                    break;
                case "FM":
                    radio_ARDV1.ModeSet(8);
                    break;
                case "AM":
                    radio_ARDV1.ModeSet(9);
                    break;                
                case "SAH":
                    radio_ARDV1.ModeSet(10);
                    break;
                case "SAL":
                    radio_ARDV1.ModeSet(11);
                    break;
                case "USB":
                    radio_ARDV1.ModeSet(12);
                    break;
                case "LSB":
                    radio_ARDV1.ModeSet(13);
                    break;
                case "CW":
                    radio_ARDV1.ModeSet(14);
                    break;
            }
            await Task.Delay(50);
            radio_ARDV1.ModeGet();
            await Task.Delay(20);
            FlashLedWriteARONE();

            sval = myDataGridView1.Rows[IDNum].Cells[7].Value.ToString();
            if (radio_ARDV1.ArOne.Mode < 9)
            {
                switch (sval)
                {
                    case "6,0 "+kHz://кГц":
                        radio_ARDV1.BandWidthSet(4);
                        break;
                    case "15,0 " + kHz://кГц":
                        radio_ARDV1.BandWidthSet(3);
                        break;
                    case "30 " + kHz://кГц":
                        radio_ARDV1.BandWidthSet(2);
                        break;
                    case "100 " + kHz://кГц":
                        radio_ARDV1.BandWidthSet(1);
                        break;
                    case "200 " + kHz://кГц":
                        radio_ARDV1.BandWidthSet(0);
                        break;
                }
            } 
            if (radio_ARDV1.ArOne.Mode == 9)
            {
                switch (sval)
                {
                    case "3,8 " + kHz://кГц":
                        radio_ARDV1.BandWidthSet(3);
                        break;
                    case "5,5 " + kHz://кГц":
                        radio_ARDV1.BandWidthSet(2);
                        break;
                    case "8 " + kHz://кГц":
                        radio_ARDV1.BandWidthSet(1);
                        break;
                    case "15 " + kHz://кГц":
                        radio_ARDV1.BandWidthSet(0);
                        break;
                    case "200 " + kHz://кГц":
                        radio_ARDV1.BandWidthSet(0);
                        break;
                }
            }
            if ((radio_ARDV1.ArOne.Mode == 11) || (radio_ARDV1.ArOne.Mode == 10))
            {
                switch (sval)
                {
                    case "5,5 " + kHz://кГц":
                        radio_ARDV1.BandWidthSet(1);
                        break;
                    case "3,8 " + kHz://кГц":
                        radio_ARDV1.BandWidthSet(0);
                        break;
                }
            } 
            if ((radio_ARDV1.ArOne.Mode == 13) || (radio_ARDV1.ArOne.Mode == 12))
            {
                switch (sval)
                {
                    case "1,8 " + kHz://кГц":
                        radio_ARDV1.BandWidthSet(1);
                        break;
                    case "2,6 " + kHz://кГц":
                        radio_ARDV1.BandWidthSet(0);
                        break;
                }
            } 
            if (radio_ARDV1.ArOne.Mode == 14) 
            {
                switch (sval)
                {
                    case "0,2 " + kHz://кГц":
                        radio_ARDV1.BandWidthSet(1);
                        break;
                    case "0,5 " + kHz://кГц":
                        radio_ARDV1.BandWidthSet(0);
                        break;
                }
            }
            radio_ARDV1.BandwidthGet();
            await Task.Delay(20);
        }

        private void dgv_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            int IDNum = myDataGridView1.CurrentRow.Index;
            if (myDataGridView1.Rows[IDNum].Cells[0].Value == null) return;
            if (radioButton2.Checked)
                radioButton1.Checked = true;
            Perestroika(myDataGridView1.CurrentRow.Index);
        }

        private void dgv_CellClick(object sender, DataGridViewCellEventArgs e)
        {   // НЕ! настраивает приемник, только отображает данные с датагрида в тоолбоксы на форме
            int IDNum = myDataGridView1.CurrentRow.Index;
            if (myDataGridView1.Rows[IDNum].Cells[0].Value == null) return;
            nudTableFRQ.Value = decimal.Parse(myDataGridView1.Rows[IDNum].Cells[2].Value.ToString());
            nudTablePorogObnar.Value = int.Parse(myDataGridView1.Rows[IDNum].Cells[4].Value.ToString());
            cbTablePriority.Text = myDataGridView1.Rows[IDNum].Cells[12].Value.ToString();
            tbNote.Text = myDataGridView1.Rows[IDNum].Cells[13].Value.ToString();
            this.comboBoxBW.SelectedIndexChanged -= new System.EventHandler(this.comboBoxBW_SelectedIndexChanged);
            this.comboBoxMode.SelectedIndexChanged -= new System.EventHandler(this.comboBoxMode_SelectedIndexChanged);
            this.tmBaraban.Tick -= new System.EventHandler(this.tmBaraban_Tick);
            nudPause.Value = int.Parse(myDataGridView1.Rows[IDNum].Cells[3].Value.ToString());
            comboBoxMode.Text = myDataGridView1.Rows[IDNum].Cells[8].Value.ToString();
            comboBoxBW.Text = myDataGridView1.Rows[IDNum].Cells[9].Value.ToString();
            this.comboBoxBW.SelectedIndexChanged += new System.EventHandler(this.comboBoxBW_SelectedIndexChanged);
            this.comboBoxMode.SelectedIndexChanged += new System.EventHandler(this.comboBoxMode_SelectedIndexChanged);
            this.tmBaraban.Tick += new System.EventHandler(this.tmBaraban_Tick);


        }

        public void bAOR_Click(object sender, EventArgs e)
        {
            try
            {
                if (bAOR.BackColor == Color.Green)
                {
                    try
                    {
                       radio_ARDV1.ClosePort();
                    }
                    catch { }
                }
                else
                {
                    try
                    {
                        radio_ARDV1.OpenPort(Com_ArOne);
                        Ask_Arone();
                    }
                    catch { }
                }
            }
            catch (Exception) { }

        }

        private void dgv_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int IDNum = myDataGridView1.CurrentRow.Index;
            if (myDataGridView1.Rows[IDNum].Cells[0].Value == null) return;
            if (myDataGridView1.CurrentCell.ColumnIndex == 1)
                if (radioButton2.Checked) radioButton1.Checked = true;
        }


        private void changePorogZapisi(int qwe)
        {
            PorogZapisi = qwe;

        }
        private void nudTablePorogObnar_ValueChanged(object sender, EventArgs e)
        {
            changePorogZapisi((int)nudTablePorogObnar.Value);
        }

        async void LibInit()
        {
            var library = await MediaLibrary.MediaLibrary.CreateAsync("papka");
            library.Recorder.StartRecord(99500, 0);
            var result = library.Recorder.StopRecord();
            if (result.HasValue)
            {
                // записалось удачно
            }
            else
            {
                // все плохо
            }
            library.Player.Play(library.Records[0]);
            library.Player.Volume = 0.5f;
            library.Player.Playlist = new[] { library.Records[0], library.Records[1] };
        }
        private void tbNote_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (tbNote.Text.Length > 20)
            {
                tbNote.Text = tbNote.Text.Substring(0, 20);
            }
        }
        private void FlashLedWriteARONE()
        {
            try
            {
                pictureBox14.Image = ControlARDV1.Properties.Resources.red; ;
                timer = new System.Threading.Timer(TimeStepLedWriteArone, null, TimePause, 0);
            }
            catch { }
        }

        private void TimeStepLedWriteArone(object state)
        {
            try
            {
                if (pictureBox14.InvokeRequired)
                {
                    pictureBox14.Invoke((MethodInvoker)(delegate()
                    {
                        pictureBox14.Image = ControlARDV1.Properties.Resources.gray;
                        timer.Dispose();
                    }));
                }
                else
                {
                    pictureBox14.Image = ControlARDV1.Properties.Resources.gray;
                    timer.Dispose();
                }
            }
            catch { }
        }
        private void FlashLedReadARONE()
        {
            try
            {
                pictureBox13.Image = ControlARDV1.Properties.Resources.green;
                timerRead = new System.Threading.Timer(TimeStepLedReadArone, null, TimePause, 0);
            }
            catch { }
        }
        private void TimeStepLedReadArone(object state)
        {
            try
            {
                if (pictureBox13.InvokeRequired)
                {
                    pictureBox13.Invoke((MethodInvoker)(delegate()
                    {
                        pictureBox13.Image = ControlARDV1.Properties.Resources.gray;
                        timerRead.Dispose();
                    }));
                }
                else
                {
                    pictureBox13.Image = ControlARDV1.Properties.Resources.gray;
                    timerRead.Dispose();
                }
            }
            catch { }
        }

        private void dgv_Resize(object sender, EventArgs e)
        {
            if (myDataGridView1.Size.Width < 490)
            {
                myDataGridView1.Columns[4].Visible = false;
                myDataGridView1.Columns[5].Visible = false;
            }
            if ((myDataGridView1.Size.Width > 489) && (myDataGridView1.Size.Width < 541))
            {
                myDataGridView1.Columns[4].Visible = true;
                myDataGridView1.Columns[5].Visible = false;
            }
            if (myDataGridView1.Size.Width > 540)
            {
                myDataGridView1.Columns[4].Visible = true;
                myDataGridView1.Columns[5].Visible = true;
            }
        }
        private void bIspPel_Click(object sender, EventArgs e)
        {
            /*
            double frequency;
            if (double.TryParse(lFrq.Text, out frequency))
            {
                var minFreq = frequency - 0.05;
                var maxFreq = frequency + 0.05;
                var result = await VariableDynamic.VariableWork.aWPtoBearingDSPprotocolNew.ExecutiveDF(minFreq, maxFreq, 3, 3);
                if (result != null)
                {
                    lPel.Text = (result.Direction / 10).ToString();
                }
            }
            */

            double frequency = 0;
            /*
            System.IO.StreamWriter sw = new System.IO.StreamWriter("ARDV1.txt", true, System.Text.Encoding.Default);
            sw.WriteLine("lFrq.Text: " + lFrq.Text);
            sw.WriteLine("lFRQFromAOR.Text: " + lFRQFromAOR.Text);
            sw.WriteLine();

            sw.Close();
            */
            try
            {
                string Frq2 = lFRQFromAOR.Text;
                Frq2 = Frq2.Substring(0, Frq2.Length - 3);
                Frq2 = Frq2.Replace(',', '.');

                if (double.TryParse(Frq2, out frequency))
                {
                    frequency = frequency / 1000d;
                    VariableDynamic.VariableWork VW = new VariableDynamic.VariableWork();
                    VW.RecBRSsend(frequency);
                }
            }
            catch { }

            try
            {
                string Frq2 = lFRQFromAOR.Text;
                Frq2 = Frq2.Substring(0, Frq2.Length - 3);

                if (double.TryParse(Frq2, out frequency))
                {
                    frequency = frequency / 1000d;
                    VariableDynamic.VariableWork VW = new VariableDynamic.VariableWork();
                    VW.RecBRSsend(frequency);
                }
            }
            catch { }


        }

        private void bChange_Click(object sender, EventArgs e)
        {
            try
            {
                if ((comboBoxBW.SelectedIndex < 0) || (comboBoxBW.SelectedIndex < 0)) return;
                if (myDataGridView1.CurrentCell.Value == null) return;
                if (UpdateSaveToBD(int.Parse(myDataGridView1.CurrentRow.Cells[0].Value.ToString())) != 1) { return; }//если не получилось, то делать {тут}
                zagruzka_tablFromBD(NameTable.ARDV1.ToString());
            }
            catch (Exception) { }

        }
        private void RefreshKatalogs()
        {
            try
            {
                listBox1.Items.Clear();
                string dirName = Application.StartupPath + "\\wav";

                string[] dirs = Directory.GetDirectories(dirName);
                foreach (string s in dirs)
                {
                    listBox1.Items.Add(s.Substring(dirName.Length + 1));
                }
                return;

            }
            catch (Exception) { }
        }

        private void RefreshKatalogWavs()
        {
            try
            {
                listBox2.Items.Clear();
                string dirName = Application.StartupPath + "\\wav\\" + listBox1.SelectedItem.ToString();
                if (Directory.Exists(dirName))
                {
                    string[] files = Directory.GetFiles(dirName);
                    foreach (string s in files)
                    {
                        listBox2.Items.Add(s.Substring(dirName.Length + 1, (s.Length - dirName.Length - 5)));
                    }
                }
            }
            catch (Exception) { }
        }
        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            RefreshKatalogWavs();
        }
        bool playing = false;
        //System.Threading.Tasks.Task library ;//= await MediaLibrary.MediaLibrary.CreateAsync(Application.StartupPath + "\\wav\\" + listBox1.SelectedItem.ToString());

        private WaveOutEvent outputDevice;
        private AudioFileReader audioFile;

        void listBox2_DoubleClick(object sender, EventArgs e)
        {
            //if (playing) return;
            //var library = await MediaLibrary.MediaLibrary.CreateAsync(Application.StartupPath + "\\wav\\" + listBox1.SelectedItem.ToString());
            //int PlayIndex = listBox2.SelectedIndex;
            //pictureBox18.Image = ControlARDV1.Properties.Resources.wavPlayer; playing = true;           //equalizer ON
            //bMute = false;
            //library.Player.Play(library.Records[PlayIndex]);
            //library.Player.Volume = 100f;
            //library.Player.PlaybackStateChangedEvent += new EventHandler<SharpExtensions.EventArg<PlaybackState>>(PlayBackStateEventt);

            string fileName = Application.StartupPath + "\\wav\\" + listBox1.SelectedItem.ToString() + "\\" + listBox2.SelectedItem.ToString() + ".wav";

            DisposeWave();

            outputDevice = new WaveOutEvent();
            audioFile = new AudioFileReader(fileName);
            outputDevice.Init(audioFile);

            outputDevice.Play();
            pictureBox18.Image = ControlARDV1.Properties.Resources.wavPlayer;  //equalizer ON

            Task.Run(async () =>
            {
                while (true)
                {
                    if (audioFile.CurrentTime == audioFile.TotalTime)
                        pictureBox18.Image = ControlARDV1.Properties.Resources.wavPlayerGray;  //equalizer OFF

                    await Task.Delay(100);
                }
            });


        }

        public void DisposeWave()
        {
            try
            {
                if (outputDevice != null)
                {
                    if (outputDevice.PlaybackState == NAudio.Wave.PlaybackState.Playing) outputDevice.Stop();
                    outputDevice.Dispose();
                    outputDevice = null;
                }
                if (audioFile != null)
                {
                    audioFile.Dispose();
                    audioFile = null;
                }
            }
            catch { }
        }
        private void PlayBackStateEventt(object sender, SharpExtensions.EventArg<PlaybackState> e)
        {
            if (e.Data.ToString() == "Stopped")
            {
                bMute = true;
                pictureBox18.Image = ControlARDV1.Properties.Resources.wavPlayerGray; playing = false;
            }         //equalizer OFF
            if (e.Data.ToString() == "Playing")
            {
                bMute = false;
                pictureBox18.Image = ControlARDV1.Properties.Resources.wavPlayer; playing = true;
            }              //equalizer ON
            if (e.Data.ToString() == "Paused")
            {
                bMute = false;
                pictureBox18.Image = ControlARDV1.Properties.Resources.wavPlayerGray; playing = false;
            }              //equalizer OFF
        }
        private void tabControl1_Selecting(object sender, TabControlCancelEventArgs e)
        {
            if (tabControl1.SelectedIndex == 1) RefreshKatalogs();
        }
        private void listBox1_DoubleClick(object sender, EventArgs e)
        {
            RefreshKatalogs();
        }
        private void bFrqMinus_Click(object sender, EventArgs e)
        {
            double myfrq = double.Parse(radio_ARDV1.ArOne.frequency.ToString());
            myfrq = myfrq - step_frq_MHz;
            if (myfrq > 30000)
            {
                radio_ARDV1.FrequencySet(myfrq / 1000000);
            }
            else { };//MessageBox.Show("Не может быть меньше!"); }
            Thread.Sleep(20);
            radio_ARDV1.FrequencyGet();
        }
        private void bFrqPlus_Click(object sender, EventArgs e)
        {
            double myfrq = double.Parse(radio_ARDV1.ArOne.frequency.ToString());
            myfrq = myfrq + step_frq_MHz;
            if (myfrq < 3299999999)
            {
                radio_ARDV1.FrequencySet(myfrq / 1000000);
            }
            else { };//MessageBox.Show("Не может быть больше!"); }
            Thread.Sleep(20);
            radio_ARDV1.FrequencyGet();
        }
        private void cbFrqStep_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch ((int)cbFrqStep.SelectedIndex)
            {
                case 0: step_frq_MHz = 10f; break;// / 1000000f; break;
                case 1: step_frq_MHz = 50f; break;// / 1000000f; break;
                case 2: step_frq_MHz = 100f; break;// / 1000000f; break;
                case 3: step_frq_MHz = 500f; break;// / 1000000f; break;
                case 4: step_frq_MHz = 1000f; break;// / 1000000f; break;
                case 5: step_frq_MHz = 5000f; break;// / 1000000f; break;
                case 6: step_frq_MHz = 10000f; break;// / 1000000f; break;
                case 7: step_frq_MHz = 50000f; break;// / 1000000f; break;
                case 8: step_frq_MHz = 100000f; break;// / 1000000f; break;
                case 9: step_frq_MHz = 500000f; break;// / 1000000f; break;
                case 10: step_frq_MHz = 1000000f; break;// / 1000000f; break;
                default: MessageBox.Show("case not worked!"); break;
            }
        }
        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            kol_buf_ostanovka_zapisi = (int)numericUpDown1.Value * 10;
        }

        private void bWavPlayer_Click(object sender, EventArgs e)
        {
            try
            {
                string name = "Player";
                string nameAZ = "PlayerAZ";
                System.Diagnostics.Process[] process = System.Diagnostics.Process.GetProcesses();

                bool blProc = false;
                int i = 0;

                while (i < process.Length)
                {
                    if (process[i].ProcessName == name || process[i].ProcessName == nameAZ)
                    {
                        blProc = true;
                        i = process.Length;
                    }
                    i++;
                }

                VariableStatic.VariableCommon variableCommon = new VariableStatic.VariableCommon();

                string WavPath = "";
                try
                {
                    //rus
                    if (variableCommon.Language == 0)
                    {
                        if (listBox2.SelectedItem != null)
                        {
                            WavPath = Application.StartupPath + "\\wav\\" + listBox1.SelectedItem.ToString() + "\\" + listBox2.SelectedItem.ToString() + ".wav";
                            WritePrivateProfileString("Player", "PathIni", WavPath, Application.StartupPath + "\\Player RU\\Player.ini");
                        }

                        ProcessWavPlayer.StartInfo.FileName = Application.StartupPath + "\\Player RU\\Player.exe";
                    }
                    //az
                    if (variableCommon.Language == 2)
                    {
                        if (listBox2.SelectedItem != null)
                        {
                            WavPath = Application.StartupPath + "\\wav\\" + listBox1.SelectedItem.ToString() + "\\" + listBox2.SelectedItem.ToString() + ".wav";
                            WritePrivateProfileString("Player", "PathIni", WavPath, Application.StartupPath + "\\Player AZ\\Player.ini");
                        }

                        ProcessWavPlayer.StartInfo.FileName = Application.StartupPath + "\\Player AZ\\PlayerAZ.exe";
                    }
                }
                catch { }

                if (blProc == false)
                {
                    ProcessWavPlayer.Start();
                }
            }
            catch (Exception) { }
        }
        private void cbAverage_CheckedChanged(object sender, System.EventArgs e)
        {
            if (!cbAverage.Checked)
            {
                PlotAverage.ClearData();
                cbAverage.Image = ControlARDV1.Properties.Resources.average_gray;
            }
            else
            {
                cbAverage.Image = ControlARDV1.Properties.Resources.average_blue;

            }
        }
        private void cbAGC_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            FlashLedWriteARONE();
            radio_ARDV1.AutomaticGainControlSet(cbAGC.SelectedIndex);
            if (cbAGC.SelectedIndex == 3) tbManualGain.Enabled = true;
                else tbManualGain.Enabled = false;
            FlashLedWriteARONE();
            radio_ARDV1.ManualGainGet();
        }
        private void tbSQL_Scroll(object sender, System.EventArgs e)
        {
            switch (cbSQL.SelectedIndex)
            {
                case 1:
                    FlashLedWriteARONE();
                    radio_ARDV1.NoiseSQuelchSet(tbSQL.Value); 
                    Thread.Sleep(20); 
                    radio_ARDV1.NioseSQGet(); 
                    break;
                case 2:
                    FlashLedWriteARONE();
                    radio_ARDV1.LevelSQuelchSet(tbSQL.Value);
                    Thread.Sleep(20);
                    radio_ARDV1.LevelSQuelchGet(); 
                    break;
            }
        }
        private void cbSQL_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            FlashLedWriteARONE();
            radio_ARDV1.SquelchSelectSet(cbSQL.SelectedIndex);
            Thread.Sleep(20);
            radio_ARDV1.SquelchSelectGet();
            if (cbSQL.SelectedIndex == 0) tbSQL.Enabled = false; 
                else tbSQL.Enabled = true;
        }
        private void tbAudioGain_Scroll(object sender, System.EventArgs e)
        {
            FlashLedWriteARONE();
            radio_ARDV1.AudioGainSet(tbAudioGain.Value);
            Thread.Sleep(20);
            radio_ARDV1.AudioGainGet();
        }

        private void tbManualGain_Scroll(object sender, EventArgs e)
        {
            FlashLedWriteARONE();
            radio_ARDV1.ManualGainSet(tbManualGain.Value);
            Thread.Sleep(20);
            radio_ARDV1.ManualGainGet();

        }

        private void myDataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {   // НЕ! настраивает приемник, только отображает данные с датагрида в тоолбоксы на форме
            try
            {
                int IDNum = myDataGridView1.CurrentRow.Index;
                if (myDataGridView1.Rows[IDNum].Cells[0].Value == null) return;
                nudTableFRQ.Value = decimal.Parse(myDataGridView1.Rows[IDNum].Cells[2].Value.ToString());
                nudTablePorogObnar.Value = int.Parse(myDataGridView1.Rows[IDNum].Cells[4].Value.ToString());
                cbTablePriority.Text = myDataGridView1.Rows[IDNum].Cells[8].Value.ToString();
                tbNote.Text = myDataGridView1.Rows[IDNum].Cells[9].Value.ToString();
                this.comboBoxBW.SelectedIndexChanged -= new System.EventHandler(this.comboBoxBW_SelectedIndexChanged);
                this.comboBoxMode.SelectedIndexChanged -= new System.EventHandler(this.comboBoxMode_SelectedIndexChanged);
                this.tmBaraban.Tick -= new System.EventHandler(this.tmBaraban_Tick);
                nudPause.Value = int.Parse(myDataGridView1.Rows[IDNum].Cells[3].Value.ToString());
                comboBoxMode.Text = myDataGridView1.Rows[IDNum].Cells[6].Value.ToString();
                comboBoxBW.Text = myDataGridView1.Rows[IDNum].Cells[7].Value.ToString();
                this.comboBoxBW.SelectedIndexChanged += new System.EventHandler(this.comboBoxBW_SelectedIndexChanged);
                this.comboBoxMode.SelectedIndexChanged += new System.EventHandler(this.comboBoxMode_SelectedIndexChanged);
                this.tmBaraban.Tick += new System.EventHandler(this.tmBaraban_Tick);
            }
            catch { }
        }
        private void myDataGridView1_CellCellDoubleClickClick(object sender, DataGridViewCellEventArgs e)
        {
            if (radioButton2.Checked)
                radioButton1.Checked = true;
            Perestroika(myDataGridView1.CurrentRow.Index);
        }
        private void myDataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (myDataGridView1.CurrentCell.ColumnIndex == 1)
                if (radioButton2.Checked) radioButton1.Checked = true;
        }

        public void MuteArone(bool mute)
        {

            var channels = AppVolumeLibrary.AppVolumeLibrary.EnumerateApplications().ToArray();
            if (channels.Any(c => c == aroneVolumeChannelName2))
            {
                AppVolumeLibrary.AppVolumeLibrary.SetApplicationMute(aroneVolumeChannelName2, mute);
            }
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            //if (cbMute.Checked) radio_ARDV1.AudioGainSet(0);
            //else radio_ARDV1.AudioGainSet(60);// (tbAudioGain.Value);

            if (cbMute.Checked) MuteArone(true);
            else MuteArone(false);
        }
        public void SetFRQ_MHz(double frqq)
        {
            radio_ARDV1.FrequencySet(frqq);
        }
        public void GetFRQ()
        {
            radio_ARDV1.FrequencyGet();
        }

        private void nudTableFRQ_KeyPress_1(object sender, KeyPressEventArgs e)
        {

        }

        private void nudTableFRQ_KeyUp_1(object sender, KeyEventArgs e)
        {

        }

        private void nudPause_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= '0') && (e.KeyChar <= '9') || (e.KeyChar == '\b')) // цифра, Backspace
            {
                return;
            }
            e.Handled = true;
        }

        private void nudPause_Validated(object sender, EventArgs e)
        {
            if (nudPause.Value > 30000) nudPause.Value = 30000;
        }

        private void bAddToTableFromAOR_Click(object sender, EventArgs e)
        {
            StrARDV1.iFreq = radio_ARDV1.ArOne.frequency / 10;
            StrARDV1.iU = (int)nudTablePorogObnar.Value;
            StrARDV1.bPriority = (byte)cbTablePriority.SelectedIndex;
            StrARDV1.iBW = radio_ARDV1.ArOne.Bandwidth;
            StrARDV1.iMode = radio_ARDV1.ArOne.Mode;
            StrARDV1.iOnOff = 1;
            StrARDV1.iPause = (int)nudPause.Value;
            StrARDV1.Note = tbNote.Text.ToString();
            StrARDV1.sTimeFirst = DateTime.Now.Hour.ToString("00") + DateTime.Now.Minute.ToString("00") + DateTime.Now.Second.ToString("00");
            sqLiteConnect = new System.Data.SQLite.SQLiteConnection(functionsDB.ConnectionString());             // подключиться к БД      
            sqLiteConnect.Open();
            sqLiteCommand = new System.Data.SQLite.SQLiteCommand(
                "INSERT INTO ARDV1(OnOff,Freq, Pause, U, TimeFirst,  Mode, BW,  Priority, Note) VALUES (" + StrARDV1.iOnOff + "," + StrARDV1.iFreq + "," + StrARDV1.iPause + "," + StrARDV1.iU + "," + StrARDV1.sTimeFirst + "," + StrARDV1.iMode + "," + StrARDV1.iBW  + "," + StrARDV1.bPriority + ",'" + StrARDV1.Note.ToString() + "')", sqLiteConnect);
            int qwee = sqLiteCommand.ExecuteNonQuery();
            sqLiteConnect.Close();
            zagruzka_tablFromBD(NameTable.ARDV1.ToString());
        }

        public void LanguageChooser(int NumberOfLanguage)
        {
            var cont = this.Controls;
            this.NumberOfLanguage = NumberOfLanguage;
            InitTableAR6000();

            if (NumberOfLanguage.Equals(0))
            {
                ChangeLanguageToRu(cont);
                //this.Text = "КРПУ AR6000 cлухового контроля";
                waveformGraph1.YAxes[0].Caption = "Уровень, дБм";
                waveformGraph1.XAxes[0].Caption = "Частота, Гц";

                string tempkHz = "кГц";
                if (lFRQFromAOR.Text.Length > 6)
                {
                    lFRQFromAOR.Text = lFRQFromAOR.Text.Remove(lFRQFromAOR.Text.Length - 3, 3);
                    lFRQFromAOR.Text = lFRQFromAOR.Text + tempkHz;
                }

                
            }
            if (NumberOfLanguage.Equals(1))
            {
                ChangeLanguageToEng(cont);
                this.Text = " ";
            }
            if (NumberOfLanguage.Equals(2))
            {
                ChangeLanguageToAzer(cont);
                //this.Text = "YRQQ AR6000 eşitmə nəzarət";
                waveformGraph1.YAxes[0].Caption = "Səviyyə, dBm";
                waveformGraph1.XAxes[0].Caption = "Tezlik, Hs";

                string tempkHz = "kHs";
                if (lFRQFromAOR.Text.Length > 6)
                {
                    lFRQFromAOR.Text = lFRQFromAOR.Text.Remove(lFRQFromAOR.Text.Length - 3, 3);
                    lFRQFromAOR.Text = lFRQFromAOR.Text + tempkHz;
                }
            }

        }


        private void ChangeLanguageToRu(System.Windows.Forms.Control.ControlCollection cont)
        {
            string lang = "ru-RU";
            foreach (System.Windows.Forms.Control cc in cont)
            {
                if (cc is ComboBox)
                {

                    for (int i = 0; i < (cc as ComboBox).Items.Count; i++)
                    {
                        if (i == 0)
                        {
                            string a = resources.GetString(cc.Name + ".Items", new CultureInfo(lang));
                            (cc as ComboBox).Items[i] = a;
                        }
                        else
                        {
                            string a = resources.GetString(cc.Name + ".Items" + i.ToString(), new CultureInfo(lang));
                            (cc as ComboBox).Items[i] = a;
                        }
                    }
                }
                //}
                if (cc is Label || cc is Button || cc is RadioButton || cc is CheckBox || cc is GroupBox || cc is ComboBox || cc is TabPage || cc is TextBox)
                {
                    resources.ApplyResources(cc, cc.Name, new CultureInfo(lang));
                    //toolTip.SetToolTip(cc, resources.GetString(cc.Name + ".ToolTip", new CultureInfo(lang)));
                }
                if (cc is Panel || cc is GroupBox || cc is ListBox || cc is TabControl)
                {
                    ChangeLanguageToRu(cc.Controls);
                }
            }
        }

        private void ChangeLanguageToAzer(System.Windows.Forms.Control.ControlCollection cont)
        {
            string lang = "az-Latn";

            foreach (System.Windows.Forms.Control cc in cont)
            {
                if (cc is ComboBox)
                {

                    for (int i = 0; i < (cc as ComboBox).Items.Count; i++)
                    {
                        if (i == 0)
                        {
                            string a = resources.GetString(cc.Name + ".Items", new CultureInfo(lang));
                            (cc as ComboBox).Items[i] = a;
                        }
                        else
                        {
                            string a = resources.GetString(cc.Name + ".Items" + i.ToString(), new CultureInfo(lang));
                            (cc as ComboBox).Items[i] = a;
                        }
                    }
                }
                //}
                if (cc is Label || cc is Button || cc is RadioButton || cc is CheckBox || cc is GroupBox || cc is ComboBox || cc is TabPage || cc is TextBox)
                {
                    resources.ApplyResources(cc, cc.Name, new CultureInfo(lang));
                    //toolTip.SetToolTip(cc, resources.GetString(cc.Name + ".ToolTip", new CultureInfo(lang)));
                }
                if (cc is Panel || cc is GroupBox || cc is ListBox || cc is TabControl)
                {
                    ChangeLanguageToAzer(cc.Controls);
                }



            }
        }

        private void ChangeLanguageToEng(System.Windows.Forms.Control.ControlCollection cont)
        {



        }

        private void InitTableAR6000()
        {

            switch (NumberOfLanguage)
            {
                case 0:

                    myDataGridView1.Columns[0].HeaderText = "№";
                    myDataGridView1.Columns[1].HeaderText = "ВКЛ";
                    myDataGridView1.Columns[2].HeaderText = "Частота, кГц";
                    myDataGridView1.Columns[3].HeaderText = "Пауза, мс";
                    myDataGridView1.Columns[4].HeaderText = "Порог, дБм";
                    myDataGridView1.Columns[5].HeaderText = "Время";
                    myDataGridView1.Columns[6].HeaderText = "Режим";
                    myDataGridView1.Columns[7].HeaderText = "Полоса";
                    myDataGridView1.Columns[8].HeaderText = "Приор";
                    myDataGridView1.Columns[9].HeaderText = "Примечание";


                    break;
                case 1:

                    myDataGridView1.Columns[0].HeaderText = "";
                    myDataGridView1.Columns[1].HeaderText = "";
                    myDataGridView1.Columns[2].HeaderText = "";
                    myDataGridView1.Columns[3].HeaderText = "";
                    myDataGridView1.Columns[4].HeaderText = "";
                    myDataGridView1.Columns[5].HeaderText = "";
                    myDataGridView1.Columns[6].HeaderText = "";
                    myDataGridView1.Columns[7].HeaderText = "";
                    myDataGridView1.Columns[8].HeaderText = "";
                    myDataGridView1.Columns[9].HeaderText = "";

                    break;
                case 2:

                    myDataGridView1.Columns[0].HeaderText = "№";
                    myDataGridView1.Columns[1].HeaderText = "Qoş";
                    myDataGridView1.Columns[2].HeaderText = "Tezlik, KHs";
                    myDataGridView1.Columns[3].HeaderText = "Fasilə mKs	";
                    myDataGridView1.Columns[4].HeaderText = "Hədd, dBm";
                    myDataGridView1.Columns[5].HeaderText = "Vaxt";
                    myDataGridView1.Columns[6].HeaderText = "Üsul";
                    myDataGridView1.Columns[7].HeaderText = "Zolaq";
                    myDataGridView1.Columns[8].HeaderText = "Üstünlük";
                    myDataGridView1.Columns[9].HeaderText = "Qeyd";

                    break;
                default:
                    break;
            }
        }

        private void bFRSonRS_Click(object sender, EventArgs e)
        {

            double frequency = 0;

            try
            {
                string Frq2 = lFRQFromAOR.Text;
                Frq2 = Frq2.Substring(0, Frq2.Length - 3);
                Frq2 = Frq2.Replace(',', '.');

                if (double.TryParse(Frq2, out frequency))
                {
                    frequency = frequency / 1000d;
                    VariableDynamic.VariableWork VW = new VariableDynamic.VariableWork();
                    VW.RecFtoRJSsend(frequency);
                }
            }
            catch { }

            try
            {
                string Frq2 = lFRQFromAOR.Text;
                Frq2 = Frq2.Substring(0, Frq2.Length - 3);

                if (double.TryParse(Frq2, out frequency))
                {
                    frequency = frequency / 1000d;
                    VariableDynamic.VariableWork VW = new VariableDynamic.VariableWork();
                    VW.RecFtoRJSsend(frequency);
                }
            }
            catch { }


        }

        private void bPause_Click(object sender, EventArgs e)
        {
            try
            {
                if (outputDevice != null)
                {
                    if (outputDevice.PlaybackState == NAudio.Wave.PlaybackState.Playing)
                    {
                        bPause.Image = ControlARDV1.Properties.Resources.Continue;
                        pictureBox18.Image = ControlARDV1.Properties.Resources.wavPlayerGray;

                        outputDevice.Pause();
                    }
                    else if (outputDevice.PlaybackState == NAudio.Wave.PlaybackState.Paused)
                    {
                        bPause.Image = ControlARDV1.Properties.Resources.Pause;
                        pictureBox18.Image = ControlARDV1.Properties.Resources.wavPlayer;  //equalizer ON

                        outputDevice.Play();
                    }
                }
            }
            catch { }// (Exception) { MessageBox.Show(ex.Message); }
        }

        private void bStop_Click(object sender, EventArgs e)
        {
            try
            {
                pictureBox18.Image = ControlARDV1.Properties.Resources.wavPlayerGray;
                bPause.Image = ControlARDV1.Properties.Resources.Pause;

                DisposeWave();
            }
            catch { }
        }


        public void SetCheckBox(bool bCheck)
        {
            cbMute.Checked = bCheck;


        }

    }
}
