﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Numerics;
using System.Threading;
using NAudio.Wave;
using MyDataGridView;
using System.Data.SQLite;
using System.IO.Ports;
using System.Globalization;
using System.Runtime.InteropServices;
using System.IO;
using System.Diagnostics;
using NAudio;
using NAudio.FileFormats;
using NAudio.CoreAudioApi;
using MathNet.Numerics.Signals;
using MathNet.Numerics;
using MathNet.Numerics.IntegralTransforms;
using MediaLibrary;


namespace ControlARONE
{
    public struct Struct_ARONE
    {
        public byte bPriority;
        public int iAmpl;
        public int iAttenuator;
        public int iBW;
        public Int64 iFreq;
        public int iHPF;
        public int iID;
        public int iLPF;
        public int iMode;
        public int iOnOff;
        public int iPause;
        public int iU;
        public string Note;
        public string sTimeFirst;
    }
    public partial class ControlARONE: UserControl
    {
        FunctionsDB functionsDB;
        public ClassLibrary_ARONE classLibrary_ARONE;
        string Com_ArOne = "COM3";
        string FrqToAOR = "";
        bool zapolneno = false;
        int spektorYMinPorog = -110;
        int spektorYMaxPorog = -40;
        //System.Threading.Timer tmWriteAOR;
        //System.Threading.Timer tmReadAOR;
        int TimePause_Baraban = 300;
        double taimer_zapisi_max = 100;
        double taimer_zapisi = 0;
        NumberFormatInfo nfi = new CultureInfo("en-US", false).NumberFormat;
        bool DotInFrq = false;
        int countafterdot = 0;
        static double Fs = 44100;               // Частота дискретизвции 
        static double T = 1.0 / Fs;             // Шаг дискретизации
        static double Fn = Fs / 2;              // Частота Найквиста
        static int N = (int)Fs / 5;                           //Длина сигнала (точек)
        WaveIn waveIn;// WaveIn - поток для записи
        WaveFileWriter writer;
        Complex[] sig1;
        int waterflow_count = 0;
        LinkedList<string> link = new LinkedList<string>();
        LinkedListNode<string> node;
        Complex max = 2400;                         //для корректировки уровня спектра
        int iAverage = 0;
        int val = 0;                            //для заолнения таблицы
        string sval = "";                            //для заолнения таблицы

        double[] SignalX;           //для точек графика БПФ 
        double[] SignalY;           //для точек графика БПФ 
        double[] PorogObnar;           //для точек порог обнаружения
        double[] SignalYAverage;           //для точек графика БПФ
        double[] SignalYMinPorog;   //для мин порога графика БПФ
        double[] SignalYMaxPorog;   //для макс порога графика БПФ
        double[] SignalYResized;
        int iNumDataMx = 0;
        int iNumDataSigma = 0;
        int count_baund = 0;
        int WaterflowTime = 50;             //коэффициент высота водопада
        double[,] intensity;
        public int resolutionX = 1000;
        public int resolutionY = 1000;
        public int delitel;
        double qwe = 0;                             //точка сигнала на графике
        double sig_sum = 0;                         //среднее за 5 отсчетов
        double K = N / 4;
        int Nwaterflow_count = 3;               // коэффициент пропуски водопада
        Struct_ARONE StrARONE = new Struct_ARONE();
        Task ThrIntensityGraphPain;
        int ThrIntensityGraphN;
        int ThrIntensityGraphNeedCount = 3;
        public bool closing = false;
        PropertyDGV propertyDGV;
        int jsql = 0;
        SQLiteConnection sqLiteConnect;
        SQLiteCommand sqLiteCommand;
        SQLiteDataReader sqLiteRead;
        int[] IDBaraban0;       //обычный
        int[] IDBaraban1;       //важный
        int IDBaraban0Count = 0;
        int IDBaraban1Count = 0;
        int NBaraban = 0;
        bool BarabanPriorityWorking = false;
        int Baraban1Num = 0;
        int Baraban0Num = 0;
        bool auto_save_wav = false;
        int kol_previshenij_dlia_zapisi = 10;       //колво превышений за 1 набор БПФ
        int kol_previshenij_dlia_zapisi_time_count = 5; //колво превышений подряд
        int kol_previshenij_dlia_zapisi_time_count_i = 0;
        int kol_buf_ostanovka_zapisi = 300;                  //задержка конца записи
        int PorogZapisi = -120;

        System.Threading.Timer timer;
        System.Threading.Timer timerRead;
        int TimePause = 100;
        public float step_frq_MHz = 10f / 1000000f;
        System.Diagnostics.Process ProcessWavPlayer;
        Task TaskIntensityPaint;
        int ibaraban = 0;
        bool bool_TaskIntensityPaint=false;

        [DllImport("kernel32")]
        private static extern long WritePrivateProfileString(string section, string key, string val, string filePath);
        [DllImport("kernel32")]
        private static extern int GetPrivateProfileString(string section, string key, string def, StringBuilder retVal, int size, string filePath);

        public delegate void ByteEventHandler();
        public delegate void ByteEventHandlerInt(int qwe);
        public event ByteEventHandlerInt OnWrite;
        public event ByteEventHandlerInt OnRead;
        public event ByteEventHandler OnBAORGreen;
        public event ByteEventHandler OnBAORRed;


        public ControlARONE()
        {
            InitializeComponent();
            functionsDB = new FunctionsDB();
            ProcessWavPlayer = new Process();
            ProcessWavPlayer.StartInfo.FileName = Application.StartupPath + "\\Player\\Player.exe";
            InitConnectionArOne();
            InitLabelParamArOne();

            VariableDynamic.VariableWork.OnAddFreqImportantKRPU += new VariableDynamic.VariableWork.AddFreqImportantKRPUEventHandler(VariableWork_OnAddFreqImportantKRPU);
        }

        void VariableWork_OnAddFreqImportantKRPU(USR_DLL.TFreqImportantKRPU[] freqImportantKRPU)
        {
            try
            {
                for (int i = 0; i < freqImportantKRPU.Length; i++)
                {
                    if (freqImportantKRPU[i].iFreqMin == freqImportantKRPU[i].iFreqMax)
                        AddFrqToTablARONE(freqImportantKRPU[i].iFreqMin , 1);
                }
            }
            catch { }
        }


        public void FreqSetToAOR(Int64 f)
        {
            classLibrary_ARONE.FrequencySet(f / 1000000d);
        }
        public static void write_ARONE(string COMPort)
        {
            WritePrivateProfileString("ReceiverAOR", "ComN", COMPort, Application.StartupPath + "\\INI\\Connection.ini");
        }
        public static string read_ARONE_ComN()
        {
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("ReceiverAOR", "ComN", "COM6", temp, 255, Application.StartupPath + "\\INI\\Connection.ini");
            return temp.ToString();
        }

        private void InitConnectionArOne()
        {
            waveIn = new WaveIn();
            if (cbACP.Checked) cbACP.Checked = false;
            bAOR.BackColor = Color.Red;
            cbACP.Enabled = false;
            cbAverage.Enabled = false;
            cbRec.Enabled = false;
            cbARec.Enabled = false;
            cbIntensityPaint.Enabled = false;
            b1.Enabled = false;
            b2.Enabled = false;
            b3.Enabled = false;
            b4.Enabled = false;
            b5.Enabled = false;
            b6.Enabled = false;
            b7.Enabled = false;
            b8.Enabled = false;
            b9.Enabled = false;
            b0.Enabled = false;
            bENT.Enabled = false;
            bESC.Enabled = false;
            //comboBoxAtt.Enabled = false;
            //comboBoxBW.Enabled = false;
            //comboBoxHPF.Enabled = false;
            //comboBoxLPF.Enabled = false;
            //comboBoxMode.Enabled = false;
            //groupBoxAmpl.Enabled = false;
            groupBoxRegime.Enabled = false;
            cbFrqStep.SelectedIndex = 0;
            this.comboBoxAtt.SelectedIndexChanged -= new System.EventHandler(this.comboBoxAtt_SelectedIndexChanged);
            this.comboBoxBW.SelectedIndexChanged -= new System.EventHandler(this.comboBoxBW_SelectedIndexChanged);
            this.comboBoxMode.SelectedIndexChanged -= new System.EventHandler(this.comboBoxMode_SelectedIndexChanged);
            this.comboBoxHPF.SelectedIndexChanged -= new System.EventHandler(this.comboBoxHPF_SelectedIndexChanged);
            this.comboBoxLPF.SelectedIndexChanged -= new System.EventHandler(this.comboBoxLPF_SelectedIndexChanged);
            this.cbAmpl.SelectedIndexChanged -= new EventHandler(this.cbAmpl_SelectedIndexChanged);
            this.tmBaraban.Tick -= new System.EventHandler(this.tmBaraban_Tick);
            try
            {
                cbTablePriority.SelectedIndex = 0;
                comboBoxAtt.SelectedIndex = 0;
                comboBoxBW.SelectedIndex = 0;
                comboBoxHPF.SelectedIndex = 0;
                comboBoxLPF.SelectedIndex = 0;
                comboBoxMode.SelectedIndex = 0;
                cbAmpl.SelectedIndex = 0;
            }
            catch { }
            try
            {
                if (classLibrary_ARONE != null) classLibrary_ARONE = null;
                if (classLibrary_ARONE != null)
                    classLibrary_ARONE = null;
                classLibrary_ARONE = new ClassLibrary_ARONE();
                nfi.NumberGroupSeparator = " ";
                classLibrary_ARONE.OnConnectPort += new ClassLibrary_ARONE.ConnectEventHandler(ConnectPort);
                classLibrary_ARONE.OnDisconnectPort += new ClassLibrary_ARONE.ConnectEventHandler(DisconnextPort);
                classLibrary_ARONE.OnReadByte += new ClassLibrary_ARONE.ByteEventHandler(ReadAOR);
                classLibrary_ARONE.OnWriteByte += new ClassLibrary_ARONE.ByteEventHandler(WriteAOR);
                classLibrary_ARONE.OnDecodedBW += new ClassLibrary_ARONE.ByteEventHandler(DecodedBW);
                classLibrary_ARONE.OnDecodedAGC += new ClassLibrary_ARONE.ByteEventHandler(DecodedAC);
                classLibrary_ARONE.OnDecodedAFGain += new ClassLibrary_ARONE.ByteEventHandler(DecodedAFGain);
                classLibrary_ARONE.OnDecodedAMpl += new ClassLibrary_ARONE.ByteEventHandler(DecodedAmpl);
                classLibrary_ARONE.OnDecodedATT += new ClassLibrary_ARONE.ByteEventHandler(DecodedAtt);
                classLibrary_ARONE.OnDecodedAutoSignalLevel += new ClassLibrary_ARONE.ByteEventHandler(DecodedAutoSignalLevel);
                //classLibrary_ARONE.OnDecodedError += new ClassLibrary_ARONE.ByteEventHandler1(DecodedErr);
                classLibrary_ARONE.OnDecodedHPF += new ClassLibrary_ARONE.ByteEventHandler(DecodedHPF);
                classLibrary_ARONE.OnDecodedLPF += new ClassLibrary_ARONE.ByteEventHandler(DecodedLPF);
                classLibrary_ARONE.OnDecodedIFGain += new ClassLibrary_ARONE.ByteEventHandler(DecodedIFGain);
                classLibrary_ARONE.OnDecodedRFGain += new ClassLibrary_ARONE.ByteEventHandler(DecodedRFGain);
                classLibrary_ARONE.OnDecodedLevelSquelchTreshold += new ClassLibrary_ARONE.ByteEventHandler(DecodedLevelSquelchTreshold);
                classLibrary_ARONE.OnDecodedManualGain += new ClassLibrary_ARONE.ByteEventHandler(DecodedManualGain);
                classLibrary_ARONE.OnDecodedMode += new ClassLibrary_ARONE.ByteEventHandler(DecodedMode);
                classLibrary_ARONE.OnDecodedNoiseSquelchTreshold += new ClassLibrary_ARONE.ByteEventHandler(DecodedNoiseSquelchTreshold);
                classLibrary_ARONE.OnDecodedFrq += new ClassLibrary_ARONE.ByteEventHandler(DecodedFrq);
                classLibrary_ARONE.OnDecodedSignalLevelUnit_dBm += new ClassLibrary_ARONE.ByteEventHandler(DecodedSignalLevelUnit_dBm);
                classLibrary_ARONE.OnDecodedSignalLevelUnit_dBmV += new ClassLibrary_ARONE.ByteEventHandler(DecodedSignalLevelUnit_dBmV);
                classLibrary_ARONE.OnDecodedSquelchSelect += new ClassLibrary_ARONE.ByteEventHandler(DecodedSquelchSelect);
                classLibrary_ARONE.OnDecodedSignalLevel += new ClassLibrary_ARONE.ByteEventHandler(DecodedSignalLevel);
                Com_ArOne = read_ARONE_ComN();
                //try             //автоподключение при загрузке. у нас нету
                //{
                //    classLibrary_ARONE.OpenPort(Com_ArOne);
                //    Thread.Sleep(100);
                //}
                //catch { }
                //FlashLedWriteARONE();
                //classLibrary_ARONE.TurnON();
                //Ask_Arone();
                //int mod = classLibrary_ARONE.ArOne.Mode;
                sig1 = new Complex[44100];
                SignalX = new double[2205];
                SignalY = new double[2205];
                PorogObnar = new double[2205];
                SignalYAverage = new double[2205];
                SignalYMinPorog = new double[2205];
                SignalYMaxPorog = new double[2205];
                
                    ConnectAudio();
                    zapolneno = true;
                    intensityGraph1Resize();
                ThrIntensityGraphPain.Start();
            }
            catch { }
            propertyDGV = new PropertyDGV();
            propertyDGV.SetPropertyDGV(dgv);
            try
            {
                zagruzka_tablFromBD(NameTable.AR_ONE.ToString());
                int IDNum = 0;
                nudTableFRQ.Value = decimal.Parse(dgv.Rows[IDNum].Cells[2].Value.ToString());
                nudTablePorogObnar.Value = int.Parse(dgv.Rows[IDNum].Cells[4].Value.ToString());
                cbAmpl.Text = dgv.Rows[IDNum].Cells[6].Value.ToString();
                cbTablePriority.Text = dgv.Rows[IDNum].Cells[12].Value.ToString();
                tbNote.Text = dgv.Rows[IDNum].Cells[13].Value.ToString();
                comboBoxAtt.Text = dgv.Rows[IDNum].Cells[7].Value.ToString();
                nudPause.Value = int.Parse(dgv.Rows[IDNum].Cells[3].Value.ToString());
                comboBoxMode.Text = dgv.Rows[IDNum].Cells[8].Value.ToString();
                comboBoxBW.Text = dgv.Rows[IDNum].Cells[9].Value.ToString();
                comboBoxHPF.Text = dgv.Rows[IDNum].Cells[10].Value.ToString();
                comboBoxLPF.Text = dgv.Rows[IDNum].Cells[11].Value.ToString();
            }
            catch { }
                this.comboBoxAtt.SelectedIndexChanged += new System.EventHandler(this.comboBoxAtt_SelectedIndexChanged);
            this.comboBoxBW.SelectedIndexChanged += new System.EventHandler(this.comboBoxBW_SelectedIndexChanged);
            this.comboBoxMode.SelectedIndexChanged += new System.EventHandler(this.comboBoxMode_SelectedIndexChanged);
            this.comboBoxHPF.SelectedIndexChanged += new System.EventHandler(this.comboBoxHPF_SelectedIndexChanged);
            this.comboBoxLPF.SelectedIndexChanged += new System.EventHandler(this.comboBoxLPF_SelectedIndexChanged);
            this.cbAmpl.SelectedIndexChanged += new EventHandler(this.cbAmpl_SelectedIndexChanged);
            this.tmBaraban.Tick += new System.EventHandler(this.tmBaraban_Tick);
            var TaskIntensityPaint=Task.Run(() => intensityPlot1.Plot(intensity));
        }
        private void ConnectAudio()
        {
            try
            {
                waveIn.DeviceNumber = 0;//Дефолтное устройство для записи (если оно имеется)
                // waveIn.RecordingStopped += waveIn_RecordingStopped;//Прикрепляем обработчик завершения записи
                //waveIn.DataAvailable += waveIn_DataAvailable;
                waveIn.WaveFormat = new WaveFormat((int)Fs, 1); //Формат wav-файла - принимает параметры - частоту дискретизации и количество каналов(здесь mono)
                waveIn.StartRecording();
                //string outputFilename = String.Format("{0}{1}-{2}-{3}{4}", "C:\\ARM1\\Анализатор\\wav\\", textBox1.Text, DateTime.Now.ToString("dd.MM.yyyy"), DateTime.Now.ToString("hh.mm.ss"), ".wav");
                //"./" + textBox1.Text + "_" + DateTime.Now.ToString("yyyy:MM:dd-HH:mm") + "demo.wav";// 
                //writer = new WaveFileWriter(outputFilename, waveIn.WaveFormat);
                //WaveOut Waveout = new WaveOut();
            }
            catch (Exception ex)
            { MessageBox.Show(ex.Message); }
        }
        void Ask_Arone()
        {
            if (bAOR.BackColor == Color.Red) return;
            try
            {
                FlashLedWriteARONE();
                classLibrary_ARONE.AFGainGet();
                Thread.Sleep(5);
                //FlashLedWriteARONE();
                classLibrary_ARONE.AmplifierGet();
                Thread.Sleep(5);
                //FlashLedWriteARONE();
                classLibrary_ARONE.AttenuatorGet();
                Thread.Sleep(5);
                //FlashLedWriteARONE();
                classLibrary_ARONE.AutomaticGainControlGet();
                Thread.Sleep(5);
                //FlashLedWriteARONE();
                classLibrary_ARONE.BandwidthGet();
                Thread.Sleep(5);
                //FlashLedWriteARONE();
                classLibrary_ARONE.FrequencyGet();
                Thread.Sleep(5);
               // FlashLedWriteARONE();
                classLibrary_ARONE.HighPassFiltrGet();
                Thread.Sleep(5);
                //FlashLedWriteARONE();
                classLibrary_ARONE.IFGainGet();
                Thread.Sleep(5);
                //FlashLedWriteARONE();
                classLibrary_ARONE.LowPassFiltrGet();
                Thread.Sleep(5);
                //FlashLedWriteARONE();
                classLibrary_ARONE.ManualGainGet();
                Thread.Sleep(5);
                //FlashLedWriteARONE();
                classLibrary_ARONE.ModeGet();
                Thread.Sleep(5);
            }
            catch { }
        }
        private void InitLabelParamArOne()
        {
            lFrq.BackColor = pScreen.BackColor;
            lFrq.ForeColor = lFRQFromAOR.ForeColor;
        }
        private void DecodedSquelchSelect()
        {
            FlashLedReadARONE();
        }
        private void DecodedSignalLevelUnit_dBmV()
        {

            FlashLedReadARONE();
        }
        private void DecodedSignalLevelUnit_dBm()
        {
            FlashLedReadARONE();
            if (lLevel.InvokeRequired)
            {
                lLevel.Invoke((MethodInvoker)(delegate()
                {
                    lLevel.Text = (classLibrary_ARONE.ArOne.SignalLevelUnit_dBm).ToString() + " дБм";
                }));

            }
            else
            {
                lLevel.Text = (classLibrary_ARONE.ArOne.SignalLevelUnit_dBm).ToString() + " дБм";
            }
        }
        private void DecodedSignalLevel()
        {
            FlashLedReadARONE();
        }
        private void DecodedFrq()
        {
            FlashLedReadARONE();
            if (lFRQFromAOR.InvokeRequired)
            {
                lFRQFromAOR.Invoke((MethodInvoker)(delegate()
                {
                    float qwwe = classLibrary_ARONE.ArOne.frequency / 1000f;
                    //lFRQFromAOR.Text = qwwe.ToString("0.000"/*"N3", nfi*/) + " кГц";// ("{0:#,###,###.###}");             //ToString("F3");
                    lFRQFromAOR.Text = (classLibrary_ARONE.ArOne.frequency / 1000).ToString() + "," + (classLibrary_ARONE.ArOne.frequency % 1000).ToString("000") + " кГц";// ("{0:#,###,###.###}");             //ToString("F3");
                }));

            }
            else
            {
                lFRQFromAOR.Text = (classLibrary_ARONE.ArOne.frequency / 1000).ToString() + "," + (classLibrary_ARONE.ArOne.frequency % 1000).ToString("000") + " кГц";// ("{0:#,###,###.###}");             //ToString("F3");
            }
        }
        private void DecodedNoiseSquelchTreshold()
        {
            FlashLedReadARONE();
        }
        private void DecodedMode()
        {
            FlashLedReadARONE();
            if (lMode.InvokeRequired)
            {
                lMode.Invoke((MethodInvoker)(delegate()
                {
                    switch (classLibrary_ARONE.ArOne.Mode)
                    {
                        case 0: lMode.Text = "FM"; break;
                        case 1: lMode.Text = "AM"; break;
                        case 2: lMode.Text = "CW"; break;
                        case 3: lMode.Text = "USB"; break;
                        case 4: lMode.Text = "LSB"; break;
                        case 5: lMode.Text = "WFM"; break;
                        case 6: lMode.Text = "NFM"; break;
                    }
                    if (zapolneno == false) comboBoxMode.SelectedIndex = classLibrary_ARONE.ArOne.Mode;
                }));

            }
            else
            {
                switch (classLibrary_ARONE.ArOne.Mode)
                {
                    case 0: lMode.Text = "FM"; break;
                    case 1: lMode.Text = "AM"; break;
                    case 2: lMode.Text = "CW"; break;
                    case 3: lMode.Text = "USB"; break;
                    case 4: lMode.Text = "LSB"; break;
                    case 5: lMode.Text = "WFM"; break;
                    case 6: lMode.Text = "NFM"; break;
                }
                if (zapolneno == false) comboBoxMode.SelectedIndex = classLibrary_ARONE.ArOne.Mode;
            }
        }
        private void DecodedManualGain()
        {
            FlashLedReadARONE();
            if (trackBarManualGain.InvokeRequired)
            {
                trackBarManualGain.Invoke((MethodInvoker)(delegate()
                {
                    trackBarManualGain.Value = classLibrary_ARONE.ArOne.ManualGain;
                }));

            }
            else
            {
                trackBarManualGain.Value = classLibrary_ARONE.ArOne.ManualGain;
            }
        }
        private void DecodedLPF()
        {
            FlashLedReadARONE();
            if (comboBoxLPF.InvokeRequired)
            {
                comboBoxLPF.Invoke((MethodInvoker)(delegate()
                {
                    if (zapolneno == false) comboBoxLPF.SelectedIndex = classLibrary_ARONE.ArOne.LowPassFilter;
                }));

            }
            else
            {
                if (zapolneno == false) comboBoxLPF.SelectedIndex = classLibrary_ARONE.ArOne.LowPassFilter;
            }
        }
        private void DecodedLevelSquelchTreshold()
        {
            FlashLedReadARONE();
        }
        private void DecodedIFGain()
        {
            FlashLedReadARONE();
        }
        private void DecodedErr(string data)
        {
            FlashLedReadARONE();
        }
        private void DecodedHPF()
        {
            FlashLedReadARONE();
            if (comboBoxHPF.InvokeRequired)
            {
                comboBoxHPF.Invoke((MethodInvoker)(delegate()
                {
                    if (zapolneno == false) comboBoxHPF.SelectedIndex = classLibrary_ARONE.ArOne.HighPassFilter;
                }));

            }
            else
            {
                if (zapolneno == false) comboBoxHPF.SelectedIndex = classLibrary_ARONE.ArOne.HighPassFilter;
                // = classLibrary_ARONE.ArOne.HighPassFilter;
            }
        }
        private void DecodedAutoSignalLevel()
        {
            FlashLedReadARONE();
        }
        private void DecodedAtt()
        {
            FlashLedReadARONE();
            if (comboBoxAtt.InvokeRequired)
            {
                comboBoxAtt.Invoke((MethodInvoker)(delegate()
                {
                    switch (classLibrary_ARONE.ArOne.Attenuator)
                    {
                        case 0: lat.Text = "Атт 0дБ"; break;
                        case 1: lat.Text = "Атт -10дБ"; break;
                        case 2: lat.Text = "Атт -20дБ"; break;
                        case 3: lat.Text = "Атт Авто"; break;
                    }
                    if (zapolneno == false) comboBoxAtt.SelectedIndex = classLibrary_ARONE.ArOne.Attenuator;
                }));

            }
            else
            {
                switch (classLibrary_ARONE.ArOne.Attenuator)
                {
                    case 0: lat.Text = "Атт 0дБ"; break;
                    case 1: lat.Text = "Атт -10дБ"; break;
                    case 2: lat.Text = "Атт -20дБ"; break;
                    case 3: lat.Text = "Атт Авто"; break;
                }
                if (zapolneno == false) comboBoxAtt.SelectedIndex = classLibrary_ARONE.ArOne.Attenuator;
            }
        }
        private void DecodedAmpl()
        {
            FlashLedReadARONE();
            if (lAmplifier.InvokeRequired)
            {
                lAmplifier.Invoke((MethodInvoker)(delegate()
                {
                    switch (classLibrary_ARONE.ArOne.Amplifier)
                    {
                        case 0: lAmplifier.Text = "Выкл"; cbAmpl.SelectedIndex = 0; break;
                        case 1: lAmplifier.Text = "Вкл"; cbAmpl.SelectedIndex = 1; break;
                        case 2: lAmplifier.Text = "Авто"; cbAmpl.SelectedIndex = 2; break;
                    }
                }));

            }
            else
            {
                switch (classLibrary_ARONE.ArOne.Amplifier)
                {
                    case 0: lAmplifier.Text = "Выкл"; cbAmpl.SelectedIndex = 0; break;
                    case 1: lAmplifier.Text = "Вкл"; cbAmpl.SelectedIndex = 1; break;
                    case 2: lAmplifier.Text = "Авто"; cbAmpl.SelectedIndex = 2; break;
                }
            }
            if (cbAmpl.InvokeRequired)
            {
                cbAmpl.Invoke((MethodInvoker)(delegate()
                {
                    switch (classLibrary_ARONE.ArOne.Amplifier)
                    {
                        case 0: cbAmpl.SelectedIndex = 0; break;
                        case 1: cbAmpl.SelectedIndex = 1; break;
                        case 2: cbAmpl.SelectedIndex = 2; break;
                    }
                }));

            }
            else
            {
                switch (classLibrary_ARONE.ArOne.Amplifier)
                {
                    case 0: cbAmpl.SelectedIndex = 0; break;
                    case 1: cbAmpl.SelectedIndex = 1; break;
                    case 2: cbAmpl.SelectedIndex = 2; break;
                }
            }
        }
        private void DecodedAFGain()
        {
            FlashLedReadARONE();
            //if (.InvokeRequired)
            //{
            //    .Invoke((MethodInvoker)(delegate()
            //    {
            //        .Text = classLibrary_ARONE.ArOne..ToString();
            //    }));

            //}
            //else
            //{
            //    .Text = classLibrary_ARONE.ArOne..ToString();
            //}
        }
        private void DecodedRFGain()
        {
            FlashLedReadARONE();
            //    //if (trackBarRFGain.InvokeRequired)
            //    //{
            //    //    trackBarRFGain.Invoke((MethodInvoker)(delegate()
            //    //    {
            //    //        //trackBarRFGain.Value = classLibrary_ARONE.ArOne.RFGain;
            //    //    }));

            //    //}
            //    //else
            //    //{
            //    //    //trackBarRFGain.Value = classLibrary_ARONE.ArOne.RFGain;
            //    //}
        }
        private void DecodedAC()
        {
            FlashLedReadARONE();
            //if (comboBoxAGC.InvokeRequired)
            //{
            //    comboBoxAGC.Invoke((MethodInvoker)(delegate()
            //    {
            //        if (zapolneno == false) comboBoxAGC.SelectedIndex = classLibrary_ARONE.ArOne.AGC;
            //    }));

            //}
            //else
            //{
            //    if (zapolneno == false) comboBoxAGC.SelectedIndex = classLibrary_ARONE.ArOne.AGC;
            //}
        }
        private void DecodedBW()
        {
            FlashLedReadARONE();
            if (lBw.InvokeRequired)
            {
                lBw.Invoke((MethodInvoker)(delegate()
                {
                    switch (classLibrary_ARONE.ArOne.Bandwidth)
                    {
                        case 0: lBw.Text = "0.5 кГц"; break;
                        case 1: lBw.Text = "3.0 кГц"; break;
                        case 2: lBw.Text = "6.0 кГц"; break;
                        case 3: lBw.Text = "8.5 кГц"; break;
                        case 4: lBw.Text = "16 кГц"; break;
                        case 5: lBw.Text = "30 кГц"; break;
                        case 6: lBw.Text = "100 кГц"; break;
                        case 7: lBw.Text = "200 кГц"; break;
                        case 8: lBw.Text = "300 кГц"; break;
                    }
                    if (zapolneno == false) comboBoxBW.SelectedIndex = classLibrary_ARONE.ArOne.Bandwidth;
                }));

            }
            else
            {
                switch (classLibrary_ARONE.ArOne.Bandwidth)
                {
                    case 0: lBw.Text = "0.5 кГц"; break;
                    case 1: lBw.Text = "3.0 кГц"; break;
                    case 2: lBw.Text = "6.0 кГц"; break;
                    case 3: lBw.Text = "8.5 кГц"; break;
                    case 4: lBw.Text = "16 кГц"; break;
                    case 5: lBw.Text = "30 кГц"; break;
                    case 6: lBw.Text = "100 кГц"; break;
                    case 7: lBw.Text = "200 кГц"; break;
                    case 8: lBw.Text = "300 кГц"; break;
                }
                if (zapolneno == false) comboBoxBW.SelectedIndex = classLibrary_ARONE.ArOne.Bandwidth;
            }
        }
        private void WriteAOR()
        {
            /*  try
              {
                  if (ledAORWrite.InvokeRequired)
                  {
                      ledAORWrite.Invoke((MethodInvoker)(delegate()
                      {
                          ledAORWrite.Value = true;
                          tmWriteAOR = new System.Threading.Timer(TimeWriteAOR, null, TimePause, 0);
                      }));

                  }
                  else
                  {
                      ledAORWrite.Value = true;
                      tmWriteAOR = new System.Threading.Timer(TimeWriteAOR, null, TimePause, 0);
                  }
              }
              catch (SystemException)
              { }*/
        }
        private void ReadAOR()
        {
            /*   try
               {
                   if (ledAORRead.InvokeRequired)
                   {
                       ledAORRead.Invoke((MethodInvoker)(delegate()
                       {
                           ledAORRead.Value = true;
                           tmReadAOR = new System.Threading.Timer(TimeReadAOR, null, TimePause, 0);
                       }));

                   }
                   else
                   {
                       ledAORRead.Value = true;
                       tmReadAOR = new System.Threading.Timer(TimeReadAOR, null, TimePause, 0);
                   }
               }
               catch (SystemException)
               { }*/
        }
        private void ConnectPort()
        {
            ThrIntensityGraphPain = new Task(ThrWaterflowPaint);
            bAOR.BackColor = Color.Green;
            cbACP.Enabled = true;
            cbAverage.Enabled = true;
            cbRec.Enabled = true;
            cbARec.Enabled = true;
            cbIntensityPaint.Enabled = true;
            b1.Enabled = true;
            b2.Enabled = true;
            b3.Enabled = true;
            b4.Enabled = true;
            b5.Enabled = true;
            b6.Enabled = true;
            b7.Enabled = true;
            b8.Enabled = true;
            b9.Enabled = true;
            b0.Enabled = true;
            bDot.Enabled = true;
            bENT.Enabled = true;
            bESC.Enabled = true;
            //comboBoxAtt.Enabled = true;
            //comboBoxBW.Enabled = true;
            //comboBoxHPF.Enabled = true;
            //comboBoxLPF.Enabled = true;
            //comboBoxMode.Enabled = true;
            //groupBoxAmpl.Enabled = true;
            groupBoxRegime.Enabled = true;
            waveformGraph1.Enabled = true;
            intensityGraph1.Enabled = true;
            tbWaterflowMax.Enabled = true;
            tbWaterflowMin.Enabled = true;
            pScreen.Enabled = true;
            //groupBoxAmpl.Enabled = true;
            //bTableAdd.Enabled = true;
            //bTableChange.Enabled = true;
            //bTableClear.Enabled = true;
            //bTableDel.Enabled = true;
            //nudTableFRQ.Enabled = true;
            //nudTablePorogObnar.Enabled = true;
            //cbTablePriority.Enabled = true;
            //tbNote.Enabled = true;
            //nudPause.Enabled = true;
            //dgv.Enabled = true;
            if (!cbACP.Checked) cbACP.Checked = true;
            cbFrqStep.Enabled = true;
            bFrqMinus.Enabled = true;
            bFrqPlus.Enabled = true;
            groupBoxRegime.Enabled = true;
            bIspPel.Enabled = true;
            radioButton1.Enabled = true;
            radioButton2.Enabled = true;
            closing = false;

            if (OnBAORGreen != null)
            {
                OnBAORGreen();
            }

        }
        private void DisconnextPort()
        {
            if (cbACP.Checked) cbACP.Checked = false;
            bAOR.BackColor = Color.Red;
            closing = true;
            cbACP.Enabled = false;
            cbAverage.Enabled = false;
            cbRec.Enabled = false;
            cbARec.Enabled = false;
            cbIntensityPaint.Enabled = false;
            b1.Enabled = false;
            b2.Enabled = false;
            b3.Enabled = false;
            b4.Enabled = false;
            b5.Enabled = false;
            b6.Enabled = false;
            b7.Enabled = false;
            b8.Enabled = false;
            b9.Enabled = false;
            b0.Enabled = false;
            bDot.Enabled = false;
            bENT.Enabled = false;
            bESC.Enabled = false;
            //comboBoxAtt.Enabled = false;
            //comboBoxBW.Enabled = false;
            //comboBoxHPF.Enabled = false;
            //comboBoxLPF.Enabled = false;
            //comboBoxMode.Enabled = false;
            //groupBoxAmpl.Enabled = false;
            groupBoxRegime.Enabled = false;
            waveformGraph1.Enabled = false;
            intensityGraph1.Enabled = false;
            tbWaterflowMax.Enabled = false;
            tbWaterflowMin.Enabled = false;
            pScreen.Enabled = false;
            //groupBoxAmpl.Enabled = false;
            //bTableAdd.Enabled = false;
            //bTableChange.Enabled = false;
            //bTableClear.Enabled = false;
            //bTableDel.Enabled = false;
            //nudTableFRQ.Enabled = false;
            //nudTablePorogObnar.Enabled = false;
            //cbTablePriority.Enabled = false;
            //tbNote.Enabled = false;
            //nudPause.Enabled = false;
            //dgv.Enabled = false;
            cbFrqStep.Enabled = false;
            bFrqMinus.Enabled = false;
            bFrqPlus.Enabled = false;
            groupBoxRegime.Enabled = false;
            bIspPel.Enabled = false;
            radioButton1.Enabled = false;
            radioButton2.Enabled = false;
            if (OnBAORRed != null)
            {
                OnBAORRed();
            }

        }
        private void comboBoxBW_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (zapolneno == false) return;
            //switch (comboBoxBW.Text)
            //{
            //    case "0,5": classLibrary_ARONE.BandWidthSet(0); break;
            //    case "3,0": classLibrary_ARONE.BandWidthSet(1); break;
            //    case "6,0": classLibrary_ARONE.BandWidthSet(2); break;
            //    case "8,5": classLibrary_ARONE.BandWidthSet(3); break;
            //    case "16": classLibrary_ARONE.BandWidthSet(4); break;
            //    case "30": classLibrary_ARONE.BandWidthSet(5); break;
            //    case "100": classLibrary_ARONE.BandWidthSet(6); break;
            //    case "200": classLibrary_ARONE.BandWidthSet(7); break;
            //    case "300": classLibrary_ARONE.BandWidthSet(8); break;
            //    default: break;
            FlashLedWriteARONE();
            switch (comboBoxBW.SelectedIndex)
            {
                case 0: classLibrary_ARONE.BandWidthSet(0); break;
                case 1: classLibrary_ARONE.BandWidthSet(1); break;
                case 2: classLibrary_ARONE.BandWidthSet(2); break;
                case 3: classLibrary_ARONE.BandWidthSet(3); break;
                case 4: classLibrary_ARONE.BandWidthSet(4); break;
                case 5: classLibrary_ARONE.BandWidthSet(5); break;
                case 6: classLibrary_ARONE.BandWidthSet(6); break;
                case 7: classLibrary_ARONE.BandWidthSet(7); break;
                case 8: classLibrary_ARONE.BandWidthSet(8); break;
                default: break;
            }
        }
        private void comboBoxAtt_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (zapolneno == false) return;
            FlashLedWriteARONE();
            classLibrary_ARONE.AttenuatorSet(comboBoxAtt.SelectedIndex);

        }
        private void comboBoxMode_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (zapolneno == false) return;
            FlashLedWriteARONE();
            classLibrary_ARONE.ModeSet(comboBoxMode.SelectedIndex);

            switch (comboBoxMode.SelectedIndex)             //зависимость AGC от MODE
            {
                case 0: classLibrary_ARONE.AutomaticGainControlSet('3'); break;
                case 1: classLibrary_ARONE.AutomaticGainControlSet('3'); break;
                case 2: classLibrary_ARONE.AutomaticGainControlSet('1'); break;
                case 3: classLibrary_ARONE.AutomaticGainControlSet('2'); break;
                case 4: classLibrary_ARONE.AutomaticGainControlSet('2'); break;
                case 5: classLibrary_ARONE.AutomaticGainControlSet('3'); break;
                case 6: classLibrary_ARONE.AutomaticGainControlSet('3'); break;
                default: MessageBox.Show("case not worked!"); break;
            }

        }
        private void comboBoxLPF_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (zapolneno == false) return;
            FlashLedWriteARONE();
            switch (comboBoxLPF.SelectedIndex)
            {
                case 0: classLibrary_ARONE.LowPassFiltrSet("0"); break;
                case 1: classLibrary_ARONE.LowPassFiltrSet("1"); break;
                case 2: classLibrary_ARONE.LowPassFiltrSet("2"); break;
                case 3: classLibrary_ARONE.LowPassFiltrSet("3"); break;
                case 4: classLibrary_ARONE.LowPassFiltrSet("4"); break;
                default: MessageBox.Show("case not worked!"); break;
            }
        }
        private void comboBoxHPF_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (zapolneno == false) return;
            FlashLedWriteARONE();
            switch (comboBoxHPF.SelectedIndex)
            {
                case 0: classLibrary_ARONE.HighPassFiltrSet("0"); break;
                case 1: classLibrary_ARONE.HighPassFiltrSet("1"); break;
                case 2: classLibrary_ARONE.HighPassFiltrSet("2"); break;
                case 3: classLibrary_ARONE.HighPassFiltrSet("3"); break;
                case 4: classLibrary_ARONE.HighPassFiltrSet("4"); break;
                default: MessageBox.Show("case not worked!"); break;

            }
        }
        private void b1_Click(object sender, EventArgs e)
        {
            try
            {
                if (countafterdot > 2) return;
                if (!tmFlashFRQ.Enabled)
                    tmFlashFRQ.Start();
                lFRQFromAOR.Visible = false;
                lFrq.Visible = true;
                lFrq.ForeColor = Color.Green;
                if (FrqToAOR == "")
                {
                    FrqToAOR = "1";
                    lFrq.Text = FrqToAOR.ToString() + " кГц";
                    return;
                }
                if ((DotInFrq) && (countafterdot == 0))
                {
                    FrqToAOR += "1";
                    countafterdot++;
                    lFrq.Text = FrqToAOR.ToString() + " кГц";
                    return;
                }

                if (double.Parse(FrqToAOR) < 3299999)
                {
                    if (DotInFrq) countafterdot++;
                    FrqToAOR += "1";
                    if (double.Parse(FrqToAOR) > 3299999) FrqToAOR = FrqToAOR.Substring(0, FrqToAOR.Length - 1);
                }
                lFrq.Text = FrqToAOR.ToString() + " кГц";
            }
            catch (Exception) { }
        }
        private void b2_Click(object sender, EventArgs e)
        {
            try
            {
                if (countafterdot > 2) return;
                if (!tmFlashFRQ.Enabled)
                    tmFlashFRQ.Start();
                lFRQFromAOR.Visible = false;
                lFrq.Visible = true;
                lFrq.ForeColor = Color.Green;
                if (FrqToAOR == "")
                {
                    FrqToAOR = "2";
                    lFrq.Text = FrqToAOR.ToString() + " кГц";
                    return;
                }
                if ((DotInFrq) && (countafterdot == 0))
                {
                    FrqToAOR += "2";
                    countafterdot++;
                    lFrq.Text = FrqToAOR.ToString() + " кГц";
                    return;
                }

                if (double.Parse(FrqToAOR) < 3299999)
                {
                    if (DotInFrq) countafterdot++;
                    FrqToAOR += "2";
                    if (double.Parse(FrqToAOR) > 3299999) FrqToAOR = FrqToAOR.Substring(0, FrqToAOR.Length - 1);
                }
                lFrq.Text = FrqToAOR.ToString() + " кГц";
            }
            catch (Exception) { }
        }
        private void b3_Click(object sender, EventArgs e)
        {
            try
            {
                if (countafterdot > 2) return;
                if (!tmFlashFRQ.Enabled)
                    tmFlashFRQ.Start();
                lFRQFromAOR.Visible = false;
                lFrq.Visible = true;
                lFrq.ForeColor = Color.Green;
                if (FrqToAOR == "")
                {
                    FrqToAOR = "3";
                    lFrq.Text = FrqToAOR.ToString() + " кГц";
                    return;
                }
                if ((DotInFrq) && (countafterdot == 0))
                {
                    FrqToAOR += "3";
                    countafterdot++;
                    lFrq.Text = FrqToAOR.ToString() + " кГц";
                    return;
                }

                if (double.Parse(FrqToAOR) < 3299999)
                {
                    if (DotInFrq) countafterdot++;
                    FrqToAOR += "3";
                    if (double.Parse(FrqToAOR) > 3299999) FrqToAOR = FrqToAOR.Substring(0, FrqToAOR.Length - 1);
                }
                lFrq.Text = FrqToAOR.ToString() + " кГц";
            }
            catch (Exception) { }
        }
        private void b4_Click(object sender, EventArgs e)
        {
            try
            {
                if (countafterdot > 2) return;
                if (!tmFlashFRQ.Enabled)
                    tmFlashFRQ.Start();
                lFRQFromAOR.Visible = false;
                lFrq.Visible = true;
                lFrq.ForeColor = Color.Green;
                if (FrqToAOR == "")
                {
                    FrqToAOR = "4";
                    lFrq.Text = FrqToAOR.ToString() + " кГц";
                    return;
                }
                if ((DotInFrq) && (countafterdot == 0))
                {
                    FrqToAOR += "4";
                    countafterdot++;
                    lFrq.Text = FrqToAOR.ToString() + " кГц";
                    return;
                }

                if (double.Parse(FrqToAOR) < 3299999)
                {
                    if (DotInFrq) countafterdot++;
                    FrqToAOR += "4";
                    if (double.Parse(FrqToAOR) > 3299999) FrqToAOR = FrqToAOR.Substring(0, FrqToAOR.Length - 1);
                }
                lFrq.Text = FrqToAOR.ToString() + " кГц";
            }
            catch (Exception) { }
        }
        private void b5_Click(object sender, EventArgs e)
        {
            try
            {
                if (countafterdot > 2) return;
                if (!tmFlashFRQ.Enabled)
                    tmFlashFRQ.Start();
                lFRQFromAOR.Visible = false;
                lFrq.Visible = true;
                lFrq.ForeColor = Color.Green;
                if (FrqToAOR == "")
                {
                    FrqToAOR = "5";
                    lFrq.Text = FrqToAOR.ToString() + " кГц";
                    return;
                }
                if ((DotInFrq) && (countafterdot == 0))
                {
                    FrqToAOR += "5";
                    countafterdot++;
                    lFrq.Text = FrqToAOR.ToString() + " кГц";
                    return;
                }

                if (double.Parse(FrqToAOR) < 3299999)
                {
                    if (DotInFrq) countafterdot++;
                    FrqToAOR += "5";
                    if (double.Parse(FrqToAOR) > 3299999) FrqToAOR = FrqToAOR.Substring(0, FrqToAOR.Length - 1);
                }
                lFrq.Text = FrqToAOR.ToString() + " кГц";
            }
            catch (Exception) { }
        }
        private void b6_Click(object sender, EventArgs e)
        {
            try
            {
                if (countafterdot > 2) return;
                if (!tmFlashFRQ.Enabled)
                    tmFlashFRQ.Start();
                lFRQFromAOR.Visible = false;
                lFrq.Visible = true;
                lFrq.ForeColor = Color.Green;
                if (FrqToAOR == "")
                {
                    FrqToAOR = "6";
                    lFrq.Text = FrqToAOR.ToString() + " кГц";
                    return;
                }
                if ((DotInFrq) && (countafterdot == 0))
                {
                    FrqToAOR += "6";
                    countafterdot++;
                    lFrq.Text = FrqToAOR.ToString() + " кГц";
                    return;
                }

                if (double.Parse(FrqToAOR) < 3299999)
                {
                    if (DotInFrq) countafterdot++;
                    FrqToAOR += "6";
                    if (double.Parse(FrqToAOR) > 3299999) FrqToAOR = FrqToAOR.Substring(0, FrqToAOR.Length - 1);
                }
                lFrq.Text = FrqToAOR.ToString() + " кГц";
            }
            catch (Exception) { }
        }
        private void b7_Click(object sender, EventArgs e)
        {
            try
            {
                if (countafterdot > 2) return;
                if (!tmFlashFRQ.Enabled)
                    tmFlashFRQ.Start();
                lFRQFromAOR.Visible = false;
                lFrq.Visible = true;
                lFrq.ForeColor = Color.Green;
                if (FrqToAOR == "")
                {
                    FrqToAOR = "7";
                    lFrq.Text = FrqToAOR.ToString() + " кГц";
                    return;
                }
                if ((DotInFrq) && (countafterdot == 0))
                {
                    FrqToAOR += "7";
                    countafterdot++;
                    lFrq.Text = FrqToAOR.ToString() + " кГц";
                    return;
                }

                if (double.Parse(FrqToAOR) < 3299999)
                {
                    if (DotInFrq) countafterdot++;
                    FrqToAOR += "7";
                    if (double.Parse(FrqToAOR) > 3299999) FrqToAOR = FrqToAOR.Substring(0, FrqToAOR.Length - 1);
                }
                lFrq.Text = FrqToAOR.ToString() + " кГц";
            }
            catch (Exception) { }
        }
        private void b8_Click(object sender, EventArgs e)
        {
            try
            {
                if (countafterdot > 2) return;
                if (!tmFlashFRQ.Enabled)
                    tmFlashFRQ.Start();
                lFRQFromAOR.Visible = false;
                lFrq.Visible = true;
                lFrq.ForeColor = Color.Green;
                if (FrqToAOR == "")
                {
                    FrqToAOR = "8";
                    lFrq.Text = FrqToAOR.ToString() + " кГц";
                    return;
                }
                if ((DotInFrq) && (countafterdot == 0))
                {
                    FrqToAOR += "8";
                    countafterdot++;
                    lFrq.Text = FrqToAOR.ToString() + " кГц";
                    return;
                }

                if (double.Parse(FrqToAOR) < 3299999)
                {
                    if (DotInFrq) countafterdot++;
                    FrqToAOR += "8";
                    if (double.Parse(FrqToAOR) > 3299999) FrqToAOR = FrqToAOR.Substring(0, FrqToAOR.Length - 1);
                }
                lFrq.Text = FrqToAOR.ToString() + " кГц";
            }
            catch (Exception) { }
        }
        private void b9_Click(object sender, EventArgs e)
        {
            if (countafterdot > 2) return;
            if (!tmFlashFRQ.Enabled)
                tmFlashFRQ.Start();
            lFRQFromAOR.Visible = false;
            lFrq.Visible = true;
            lFrq.ForeColor = Color.Green;
            if (FrqToAOR == "")
            {
                FrqToAOR = "9";
                lFrq.Text = FrqToAOR.ToString() + " кГц";
                return;
            }
            if ((DotInFrq) && (countafterdot == 0))
            {
                FrqToAOR += "9";
                countafterdot++;
                lFrq.Text = FrqToAOR.ToString() + " кГц";
                return;
            }

            if (double.Parse(FrqToAOR) < 3299999)
            {
                if (DotInFrq) countafterdot++;
                FrqToAOR += "9";
                if (double.Parse(FrqToAOR) > 3299999) FrqToAOR = FrqToAOR.Substring(0, FrqToAOR.Length - 1);
            }
            lFrq.Text = FrqToAOR.ToString() + " кГц";
        }
        private void b0_Click(object sender, EventArgs e)
        {
            if (DotInFrq) countafterdot++;
            if (countafterdot > 3) return;
            if (!tmFlashFRQ.Enabled)
                tmFlashFRQ.Start();
            lFRQFromAOR.Visible = false;
            lFrq.Visible = true;
            lFrq.ForeColor = Color.Green;
            if (FrqToAOR == "")
            {
                FrqToAOR = "";
                lFrq.Text = "0 кГц";
                return;
            }
            if (double.Parse(FrqToAOR) < 330000)
                FrqToAOR += "0";
            lFrq.Text = FrqToAOR.ToString() + " кГц";
        }
        private void bDot_Click(object sender, EventArgs e)
        {
            if (DotInFrq == true) return;
            if (countafterdot > 3) return;
            DotInFrq = true;
            countafterdot = 0;
            if (!tmFlashFRQ.Enabled)
                tmFlashFRQ.Start();
            lFRQFromAOR.Visible = false;
            lFrq.Visible = true;
            lFrq.ForeColor = Color.Green;
            FrqToAOR += ",";
            lFrq.Text = FrqToAOR.ToString() + "0 кГц";

        }
        private void bESC_Click(object sender, EventArgs e)
        {
            DotInFrq = false;
            countafterdot = 0;
            if (tmFlashFRQ.Enabled)
                tmFlashFRQ.Stop();
            lFRQFromAOR.Visible = true;
            lFrq.Visible = false;
            FrqToAOR = "";
            lFrq.Text = FrqToAOR.ToString();
        }
        private void bENT_Click(object sender, EventArgs e)
        {
            double frqq = 0;
            if ((DotInFrq) && (countafterdot == 0))
            {
                DotInFrq = false;
                countafterdot = 0;
                if (tmFlashFRQ.Enabled)
                    tmFlashFRQ.Stop();
                lFRQFromAOR.Visible = true;
                lFrq.Visible = false;
                FlashLedWriteARONE();
                double.TryParse(FrqToAOR.Substring(0, FrqToAOR.Length - 1), out frqq);
                if ((frqq > 30) && (frqq < 3300000)) classLibrary_ARONE.FrequencySet(frqq / 1000);
                FrqToAOR = "";
                lFrq.Text = FrqToAOR.ToString();
                return;
            }


            DotInFrq = false;
            countafterdot = 0;
            if (tmFlashFRQ.Enabled)
                tmFlashFRQ.Stop();
            lFRQFromAOR.Visible = true;
            lFrq.Visible = false;
            FlashLedWriteARONE();
            double.TryParse(FrqToAOR, out frqq);
            if ((frqq > 30) && (frqq < 3300000)) classLibrary_ARONE.FrequencySet(frqq / 1000);
            FrqToAOR = "";
            lFrq.Text = FrqToAOR.ToString();
        }
        private void trackBarManualGain_Scroll(object sender, EventArgs e)
        {
            FlashLedWriteARONE();
            if(cbAmpl.SelectedIndex==0)
            classLibrary_ARONE.ManualGainSet(trackBarManualGain.Value, classLibrary_ARONE.ArOne.AGC);
        }
        //private void trackBarRFGain_Scroll(object sender, EventArgs e)
        //{
        //    //classLibrary_ARONE.RFGainSet(trackBarRFGain.Value);
        //}
        private void tmFlashFRQ_Tick(object sender, EventArgs e)
        {
            if (lFrq.ForeColor != Color.Green)
                lFrq.ForeColor = Color.Green;
            else
                lFrq.ForeColor = Color.Black;
        }
        private void knob_valcoder_Enter(object sender, EventArgs e)
        {
            //ValkoderFrq = classLibrary_ARONE.ArOne.frequency;
        }

        /// <summary>
        /// представление значения частоты без пробелов
        /// </summary>
        /// <param name="inputString"> строковое значение частоты </param>
        /// <returns></returns>
        public string StrToFreq(string inputString)
        {
            inputString = inputString.Replace("  ", string.Empty);
            inputString = inputString.Trim().Replace(" ", string.Empty);

            return inputString;
        }
        int Poisk_chastoty_v_tabl(string freq, DataGridView DGV)
        {
            try
            {
                if(DGV.RowCount<1){return -1;}
                for (int i = 0; i < DGV.RowCount; i++)
                {
                    string str = StrToFreq(DGV.Rows[i].Cells[2].Value.ToString()).Replace(",", string.Empty);
                    if (str.CompareTo(freq.ToString()) == 0) { return i; }
                    //if (DGV.Rows[i].Cells[2].Value.ToString().Trim().CompareTo(freq.ToString()) == 0) { return i; }
                }
                return -1;
            }
            catch
            {
                return -1;
            }
        }
        void SignalYResized_Clear()
        {
            try
            {
                for (int j = 0; j < SignalYResized.Length; j++)
                {
                    SignalYResized[j] = -150;
                }
            }
            catch (Exception) { }
        }
        void Intensity_Clear()
        {
            try
            {
                for (int j = 0; j < WaterflowTime; j++)
                {
                    for (int i = 0; i < intensity.Length / WaterflowTime; i++)
                    {
                        intensity[i, j] = -150;
                    }
                }
            }
            catch (Exception) { }
        }
        void waveIn_DataAvailable(object sender, WaveInEventArgs e)
        {
            if (bAOR.BackColor == Color.Red) return;
            //return;
            if (this.InvokeRequired)
            {
                this.BeginInvoke(new EventHandler<WaveInEventArgs>(waveIn_DataAvailable), sender, e);
            }
            else
            {
                if (writer != null) writer.WriteData(e.Buffer, 0, e.BytesRecorded);  //запись в файл
                byte[] buffer = e.Buffer;
                int bytesRecorded = e.BytesRecorded;
                Complex[] sig = new Complex[bytesRecorded / 2];
                for (int i = 0, j = 0; i < e.BytesRecorded; i += 2, j++)
                {
                    short sample = (short)((buffer[i + 1] << 8) | buffer[i + 0]);
                    sig[j] = sample / 32768f;
                    //link.AddLast(sig[j].Real.ToString());         //для осциллограммы
                    //if (link.Count > 10000)
                    //{
                    //    link.RemoveFirst();
                    //}
                }
                Transform.FourierForward(sig, FourierOptions.Matlab);

                foreach (Complex c in sig)
                {
                    if (max.Magnitude < c.Magnitude)
                    {
                        max = c;
                    }
                }

                for (int j = 0; j < bytesRecorded / 2; j++)
                {
                    sig1[j + iAverage * bytesRecorded] = sig[j];
                }
                iAverage++;
                if (iAverage == 5) { iAverage = 0; }

                CreateGraph2(sig, max);
            }
        }
        private void CreateGraph2(Complex[] sig, Complex max1)
        {
            int kol_previshenij = 0;
            try
            {
                if (InvokeRequired)
                {
                    Invoke((MethodInvoker)(() => CreateGraph2(sig, max1)));
                    return;
                }
                SignalYResized_Clear();
                qwe = 0;                             //точка сигнала на графике
                sig_sum = 0;                         //среднее за 5 отсчетов
                //double K = sig.Length / 2;
                for (int i = 0; i < K; i++)
                {
                    qwe = (Complex.Abs(sig[i]) * 1000000) / (N * 115 * max1.Magnitude);
                    qwe = 20 * Math.Log10(qwe);
                    SignalY[i] = qwe;
                    PorogObnar[i] = PorogZapisi;
                    if (iNumDataMx == count_baund) { iNumDataMx = 0; }
                    if (iNumDataSigma == count_baund) { iNumDataSigma = 0; }
                }

                for (int j = 0; j < sig.Length; j++)
                {
                    sig1[j + iAverage * sig.Length] = sig[j];
                }
                iAverage++;
                if (iAverage == 5) { iAverage = 0; }
                for (int i = 0; i < K; i++)
                {
                    sig_sum = ((
                        Complex.Abs(sig1[i + sig.Length * 0]) +
                        Complex.Abs(sig1[i + sig.Length * 1]) +
                        Complex.Abs(sig1[i + sig.Length * 2]) +
                        Complex.Abs(sig1[i + sig.Length * 3]) +
                        Complex.Abs(sig1[i + sig.Length * 4])) * 1000000) / (5 * N * 115 * max1.Magnitude);
                    sig_sum = 20 * Math.Log10(sig_sum);
                    SignalYAverage[i] = sig_sum;
                    //SignalYAverage[i] = Complex.FromPolarCoordinates(Math.Sqrt(K * K + sig_sum * sig_sum), Math.Atan2(sig_sum, K));
                    //if (i > 10 && i < 1000 && sig_sum > int.Parse(textBox3.Text)) { kol_previshenij++; }    // условие записи по порогу


                    if (i > 10 && i < 1000 && sig_sum > PorogZapisi) { kol_previshenij++; }    // условие записи по порогу
                }
                int ii = 0;
                for (int i = 0; i < K; i++)
                {
                    ii = i / delitel;
                    if (ii < SignalYResized.Length)
                        if (SignalYResized[ii] < SignalY[i]) SignalYResized[ii] = SignalY[i];
                }

                waterflow_count++;
                if (waterflow_count == Nwaterflow_count)
                {
                    waterflow_count = 0;
                }
                if (cbAverage.Checked) PlotAverage.PlotY(SignalYAverage, 0, Fn / K);
                PlotSignal.PlotY(SignalY, 0, Fn / K);
                PlotPorogObn.PlotY(PorogObnar, 0, Fn / K);
                //waveformPlot1.PlotY(SignalYResized,0, Fn / K);            //по ним рисуется водопад
                if (waterflow_count == 0)
                {
                    for (int i = 0; i < 2205 / delitel; i++)
                    {
                        for (int j = 0; j < WaterflowTime; j++)
                        {
                            if (j < WaterflowTime - 1) intensity[i, j] = intensity[i, j + 1];       // != 1я строчка
                            else
                            {
                                intensity[i, j] = SignalYResized[i]; //SignalY[i];//               // == 1я строчка

                                if (intensity[i, j] >= spektorYMaxPorog)
                                    intensity[i, j] = 0;
                                if (intensity[i, j] <= spektorYMinPorog)
                                    intensity[i, j] = -120;
                                if ((intensity[i, j] > spektorYMinPorog) & (intensity[i, j] < spektorYMaxPorog))
                                    intensity[i, j] = intensity[i, j] * (intensity[i, j] - spektorYMaxPorog) / (spektorYMinPorog - spektorYMaxPorog);
                            }
                        }
                    }
                    if (cbIntensityPaint.Checked)
                    {
                        if (TaskIntensityPaint == null)
                        {
                            TaskIntensityPaint = Task.Run(() => { intensityPlot1.Plot(intensity);   });
                        }
                        else
                        {
                            if (TaskIntensityPaint.IsCompleted)
                            {
                                // ona zakonchena
                                TaskIntensityPaint = Task.Run(() => { intensityPlot1.Plot(intensity);  });
                            }
                            else
                            {
                                // one eshe rabotaet
                            }
                        }
                    }
                    
                    // { bool_TaskIntensityPaint = true; intensityPlot1.Plot(intensity); Task.Delay(100); bool_TaskIntensityPaint = false; });


                }


                //автоматическая запись *.wav
                if (cbARec.Checked)
                {
                    if (kol_previshenij_dlia_zapisi_time_count_i < kol_buf_ostanovka_zapisi)
                    {
                        if ((kol_previshenij > kol_previshenij_dlia_zapisi))
                        {
                            kol_previshenij_dlia_zapisi_time_count_i++;
                            if (kol_previshenij_dlia_zapisi_time_count_i > kol_previshenij_dlia_zapisi_time_count)
                            {
                                if (cbRec.Checked == false)
                                {
                                    // if(tmBaraban.Enabled)   tmBaraban.Stop();
                                    cbRec.Checked = true;                        //zapisyvat
                                }
                            }
                            else
                            {
                                //kol_previshenij_dlia_zapisi_time_count_i = 0;   //конец записи
                                // cbRec.Checked = false;
                                //if (tmBaraban.Enabled) tmBaraban.Start();
                            }
                        }

                        else
                        {
                            //if (kol_previshenij_dlia_zapisi_time_count_i > 5) { kol_previshenij_dlia_zapisi_time_count_i -= 5; }
                            //kol_buf_ostanovka_zapisi++;
                            // if (kol_buf_ostanovka_zapisi > 0)
                            // {
                            //   kol_buf_ostanovka_zapisi = 0;
                            //kol_previshenij_dlia_zapisi_time_count_i = 0;
                            cbRec.Checked = false;
                            //}
                        }
                    }
                    else
                    {
                        kol_previshenij_dlia_zapisi_time_count_i = 0;
                        cbRec.Checked = false;
                    }
                }


            }
            catch (Exception) { }
        }
        async void ThrWaterflowPaint()
        {
            while (!closing)
            {
                if (cbIntensityPaint.Checked)
                    try
                    {
                        await Task.Delay(100);
                        ThrIntensityGraphN++;
                        if (ThrIntensityGraphN > ThrIntensityGraphNeedCount)
                        {
                            this.intensityGraph1.BeginInvoke((MethodInvoker)(() => this.intensityPlot1.Plot(intensity)));
                            ThrIntensityGraphN = 0;
                            await Task.Delay(5);
                        }
                    }
                    catch { }
            }
        }
        private void tbTableTime_KeyPress(object sender, KeyPressEventArgs e)
        {
            //if (tbTableTime.Text.Length > 8)
            //{
            //    tbTableTime.Text = tbTableTime.Text.Substring(0, 8);
            //    return;
            //}

            //if ((e.KeyChar >= '0') && (e.KeyChar <= '9') || (e.KeyChar == '\b') || (e.KeyChar == ':')) // цифра, Backspace
            //{
            //    return;
            //}

            //// остальные символы запрещены
            //e.Handled = true;  
        }
        private void tbTableLevel_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= '0') && (e.KeyChar <= '9') || (e.KeyChar == '\b') || (e.KeyChar == '-')) // цифра, Backspace
            {
                return;
            }
            e.Handled = true;
        }
        private void tbTableFRQ_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= '0') && (e.KeyChar <= '9') || (e.KeyChar == '\b') || (e.KeyChar == ',')) // цифра, Backspace
            {
                return;
            }
            e.Handled = true;

        }

        private void nudTableFRQ_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= '0') && (e.KeyChar <= '9') || (e.KeyChar == '\b') || (e.KeyChar == ',')) // цифра, Backspace
            {
                return;
            }
            e.Handled = true;

        }
        private void nudTableLevel_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= '0') && (e.KeyChar <= '9') || (e.KeyChar == '\b') || (e.KeyChar == '-')) // цифра, Backspace
            {
                return;
            }
            e.Handled = true;

        }
        private void nudTableFRQ_KeyUp(object sender, KeyEventArgs e)
        {
            Int64 qwe;
            Int64.TryParse(nudTableFRQ.Text.ToString(), out qwe);
            if (qwe > 3300000) nudTableFRQ.Text = nudTableFRQ.Text.Substring(0, 7);
        }
        private void nudTableLevel_Validated(object sender, EventArgs e)
        {
            int qwe;
            int.TryParse(nudTablePorogObnar.Text.ToString(), out qwe);
            if ((Math.Abs(qwe)) > 120)
            {
                nudTablePorogObnar.Text = "-120";
                return;
            }
        }
        private void tm500_Tick(object sender, EventArgs e)
        {
            if (bAOR.BackColor != Color.Green) return;
            FlashLedWriteARONE();
            //classLibrary_ARONE.SignalLevelGet();
            classLibrary_ARONE.SignalLevelUnit_dBm_Get();
        }
        private void textBoxFrq_Validated(object sender, EventArgs e)
        {

        }
        private void buttn8_Click(object sender, EventArgs e)
        {
            lFRQFromAOR.Text = (123456789.9).ToString("N", nfi);

        }
        private void intensityGraph1_Resize(object sender, EventArgs e)
        {
            intensityGraph1Resize();
        }
        private void intensityGraph1Resize()
        {
            resolutionX = intensityGraph1.Size.Width;
            resolutionY = intensityGraph1.Size.Height;

            delitel = 2205 / resolutionX;
            if (delitel < 2) delitel = 2;
            if (delitel > 10) delitel = 10;
            intensity = new double[2205 / delitel, WaterflowTime];
            SignalYResized = new double[2205 / delitel];
            SignalYResized_Clear();
            Intensity_Clear();
            intensityXAxis1.Range = new NationalInstruments.UI.Range(0, (int)(2205 / delitel));
        }
        private void cbAverage_Click(object sender, EventArgs e)
        {
            if (!cbAverage.Checked)
            {
                PlotAverage.ClearData();
                cbAverage.Image = Properties.Resources.average_blue;
            }
            else
            {
                cbAverage.Image = Properties.Resources.average_gray;

            }
        }
        private void cbACP_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (cbACP.Checked)
                {
                    cbACP.Image = Properties.Resources.stop;
                    waveIn.DataAvailable += waveIn_DataAvailable;   //Прикрепляем к событию DataAvailable обработчик, возникающий при наличии записываемых данных
                }
                else
                {
                    waveIn.DataAvailable -= waveIn_DataAvailable;
                    cbACP.Image = Properties.Resources.play;
                }
            }
            catch (Exception) { }
        }
        private void cbIntensityPaint_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (cbIntensityPaint.Checked)
                {
                    cbIntensityPaint.Image = Properties.Resources.stop;
                }
                else
                {
                    cbIntensityPaint.Image = Properties.Resources.play;
                }
            }
            catch (Exception) { }
        }
        //private void cbRec_CheckedChanged(object sender, EventArgs e)
        //{
        //    if (bAOR.BackColor != Color.Green) return;      //?=connected
        //    try
        //    {
        //        lREC.Visible = true;
        //        taimer_zapisi = 0;
        //        if (cbRec.Checked)
        //        {
        //            cbRec.Image = Properties.Resources.rec_red;
        //            string outputFilename = String.Format("{0}{1}-{2}-({3}-{4}-{5}){6}", "wav\\", classLibrary_ARONE.ArOne.frequency.ToString(), DateTime.Now.DayOfYear, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, ".wav");
        //            lREC.Text = "REC " + outputFilename.Substring(4, outputFilename.Length - 4);
        //            writer = new WaveFileWriter(outputFilename, waveIn.WaveFormat);
        //        }
        //        else
        //        {
        //            lREC.Visible = false;
        //            cbRec.Image = Properties.Resources.rec_grey;
        //            if (writer != null)
        //            {
        //                writer.Close();
        //                writer = null;
        //            }
        //        }
        //    }
        //    catch (Exception) { }

        //}
        private void cbRec_CheckedChanged(object sender, EventArgs e)
        {
            if (bAOR.BackColor != Color.Green) return;      //?=connected
            try
            {
                lREC.Visible = true;
                taimer_zapisi = 0;
                if (cbRec.Checked)
                {
                    cbRec.Image = Properties.Resources.rec_red;


                    string path = String.Format(Application.StartupPath + "\\wav\\" + DateTime.Now.Year.ToString("0000") + "-" + DateTime.Now.Month.ToString("00") + "-" + DateTime.Now.Day.ToString("00"));
                    if (!Directory.Exists(path))
                    {
                        DirectoryInfo di = Directory.CreateDirectory(path);
                    }

                    //string outputFilename = String.Format("{0}{1}-{2}-{3}{4}{5}-{6}:{7}:{8}{9}", "wav\\", DateTime.Now.Year.ToString("0000"), DateTime.Now.Month.ToString("00"), DateTime.Now.Day.ToString("00"), "\\", classLibrary_ARONE.ArOne.frequency.ToString(), DateTime.Now.Hour.ToString("00"), DateTime.Now.Minute.ToString("00"), DateTime.Now.Second.ToString("00"), ".wav");
                    string outputFilename = path + "\\" + classLibrary_ARONE.ArOne.frequency.ToString() + "_" + DateTime.Now.Hour.ToString("00") + "-" + DateTime.Now.Minute.ToString("00") + "-" + DateTime.Now.Second.ToString("00") + ".wav";


                    lREC.Text = "REC " + outputFilename.Substring(path.Length + 1, outputFilename.Length - path.Length - 5);
                    //outputFilename=outputFilename.Replace("\\","\");
                    writer = new WaveFileWriter(outputFilename, waveIn.WaveFormat);
                }//wav\\98900000-180-(17-38-33).wav
                //D:\\mywork\\Main\\WindowsFormsApplication1\\WindowsFormsApplication1\\bin\\Debug\\wav\\2017-06-29\\98900000-17:39:14.wav
                else
                {
                    lREC.Visible = false;
                    cbRec.Image = Properties.Resources.rec_grey;
                    if (writer != null)
                    {
                        writer.Close();
                        writer = null;
                    }
                }
            }
            catch (Exception) { }

        }
        private void cbARec_CheckedChanged(object sender, EventArgs e)
        {

            if (bAOR.BackColor != Color.Green) return;      //?=connected
            try
            {
                if (cbARec.Checked)
                {
                    cbARec.Image = Properties.Resources.a_rec_red;
                }
                else
                {
                    cbARec.Image = Properties.Resources.a_rec_grey;
                    cbRec.Checked = false;
                }
            }
            catch (Exception) { }
        }
        private void bTableDel_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgv.CurrentRow == null) return;
                int ID = int.Parse(dgv.CurrentRow.Cells[0].Value.ToString());//= int.Parse(table_AR_ONE1.dgvAR_ONE.dgv.CurrentRow.Cells[0].Value.ToString());
                //functionsDB.DeleteOneRecordDB(ID, NameTable.AR_ONE, 0);
            
                sqLiteConnect.Open();
                sqLiteCommand = new System.Data.SQLite.SQLiteCommand("DELETE FROM " + NameTable.AR_ONE + " WHERE ID=" + ID, sqLiteConnect);
                sqLiteCommand.ExecuteNonQuery();
            zagruzka_tablFromBD(NameTable.AR_ONE.ToString());
            }
            catch { }



        }

        public void AddFrqToTablARONE(Int64 Freq_Hz, byte Prority)
        {
            if ((Freq_Hz < 30000) || (Freq_Hz > 3300000000)) return;
            int qwe = Poisk_chastoty_v_tabl(Freq_Hz.ToString(), dgv);
            if (qwe != -1)
            {
                dgv.FirstDisplayedScrollingRowIndex = qwe;
                dgv.Rows[qwe].Selected = true;
                return;
            }
            StrARONE.iFreq = (Freq_Hz) / 10;
            StrARONE.iU = -50;
            StrARONE.bPriority = (byte)Prority;
            StrARONE.iAttenuator = 1;
            StrARONE.iAmpl = 0;
            StrARONE.iBW = 5;
            StrARONE.iHPF = 4;
            StrARONE.iLPF = 4;
            StrARONE.iMode = 5;
            StrARONE.iOnOff = 1;
            StrARONE.iPause = 1000;
            StrARONE.Note = "";
            StrARONE.sTimeFirst = DateTime.Now.Hour.ToString("00") + DateTime.Now.Minute.ToString("00") + DateTime.Now.Second.ToString("00");
            sqLiteConnect = new System.Data.SQLite.SQLiteConnection(functionsDB.ConnectionString());             // подключиться к БД      
            sqLiteConnect.Open();
            sqLiteCommand = new System.Data.SQLite.SQLiteCommand(
                "INSERT INTO AR_ONE(OnOff,Freq, Pause, U, TimeFirst, Amplifier, Attenuator, Mode, BW, HPF, LPF, Priority, Note) VALUES (" + StrARONE.iOnOff + "," + StrARONE.iFreq + "," + StrARONE.iPause + "," + StrARONE.iU + ",'" + StrARONE.sTimeFirst + "'," + StrARONE.iAmpl + "," + StrARONE.iAttenuator + "," + StrARONE.iMode + "," + StrARONE.iBW + "," + StrARONE.iHPF + "," + StrARONE.iLPF + "," + StrARONE.bPriority + ",'" + StrARONE.Note.ToString() + "')", sqLiteConnect);
            int qwee = sqLiteCommand.ExecuteNonQuery();
            sqLiteConnect.Close();
            zagruzka_tablFromBD(NameTable.AR_ONE.ToString());

        }
        private void bTableAdd_Click(object sender, EventArgs e)
        {
            int qwe=Poisk_chastoty_v_tabl(nudTableFRQ.Value.ToString(),dgv);
            if (qwe != -1)
            {
                dgv.FirstDisplayedScrollingRowIndex = qwe;
                dgv.Rows[qwe].Selected = true;
                return;
            }
            StrARONE.iFreq = (Int64)(nudTableFRQ.Value * 100);
            if ((StrARONE.iFreq<30000)||( StrARONE.iFreq>3300000000))return;
            StrARONE.iU = (int)nudTablePorogObnar.Value;
            StrARONE.bPriority = (byte)cbTablePriority.SelectedIndex;
            StrARONE.iAttenuator = comboBoxAtt.SelectedIndex;
            StrARONE.iAmpl = cbAmpl.SelectedIndex;
            StrARONE.iBW = comboBoxBW.SelectedIndex;
            StrARONE.iHPF = comboBoxHPF.SelectedIndex;
            StrARONE.iLPF = comboBoxLPF.SelectedIndex;
            StrARONE.iMode = comboBoxMode.SelectedIndex;
            StrARONE.iOnOff = 1;
            StrARONE.iPause = (int)nudPause.Value;
            StrARONE.Note = tbNote.Text.ToString();
            StrARONE.sTimeFirst = DateTime.Now.Hour.ToString("00") + DateTime.Now.Minute.ToString("00") + DateTime.Now.Second.ToString("00");
            sqLiteConnect = new System.Data.SQLite.SQLiteConnection(functionsDB.ConnectionString());             // подключиться к БД      
            sqLiteConnect.Open();
            sqLiteCommand = new System.Data.SQLite.SQLiteCommand(
                "INSERT INTO AR_ONE(OnOff,Freq, Pause, U, TimeFirst, Amplifier, Attenuator, Mode, BW, HPF, LPF, Priority, Note) VALUES (" + StrARONE.iOnOff + "," + StrARONE.iFreq + "," + StrARONE.iPause + "," + StrARONE.iU + ",'" + StrARONE.sTimeFirst + "'," + StrARONE.iAmpl + "," + StrARONE.iAttenuator + "," + StrARONE.iMode + "," + StrARONE.iBW + "," + StrARONE.iHPF + "," + StrARONE.iLPF + "," + StrARONE.bPriority + ",'" + StrARONE.Note.ToString() + "')", sqLiteConnect);
            int qwee = sqLiteCommand.ExecuteNonQuery();
            sqLiteConnect.Close();
            zagruzka_tablFromBD(NameTable.AR_ONE.ToString());
        }
        private void bTableChange_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgv.CurrentCell.Value == null) return;
                if (UpdateSaveToBD(int.Parse(dgv.CurrentRow.Cells[0].Value.ToString())) != 1) { return; }//если не получилось, то делать {тут}
                zagruzka_tablFromBD(NameTable.AR_ONE.ToString());
            }
            catch (Exception) { }
        }

        private int UpdateSaveToBD(int IDn)
        {
            StrARONE.iFreq = Int64.Parse(((double)nudTableFRQ.Value * 100).ToString());
            StrARONE.iU = (int)nudTablePorogObnar.Value;
            StrARONE.bPriority = (byte)cbTablePriority.SelectedIndex;
            StrARONE.iAttenuator = comboBoxAtt.SelectedIndex;
            StrARONE.iAmpl = cbAmpl.SelectedIndex;
            StrARONE.iBW = comboBoxBW.SelectedIndex;
            StrARONE.iHPF = comboBoxHPF.SelectedIndex;
            StrARONE.iLPF = comboBoxLPF.SelectedIndex;
            StrARONE.iMode = comboBoxMode.SelectedIndex;
            bool tru = true;
            if (dgv.CurrentRow.Cells[1].Value.ToString() == tru.ToString())
                StrARONE.iOnOff = 1;
            else
                StrARONE.iOnOff = 0;
            StrARONE.iPause = (int)nudPause.Value;
            StrARONE.Note = tbNote.Text.ToString();
            StrARONE.sTimeFirst = DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString();
            sqLiteConnect = new System.Data.SQLite.SQLiteConnection(functionsDB.ConnectionString());             // подключиться к БД      
            sqLiteConnect.Open();
            sqLiteCommand = new System.Data.SQLite.SQLiteCommand(
                "UPDATE AR_ONE SET OnOff=" + StrARONE.iOnOff + ", Freq=" + StrARONE.iFreq + ", Note='" + StrARONE.Note + "', Pause=" + StrARONE.iPause + ", U=" + StrARONE.iU + ", TimeFirst='" + StrARONE.sTimeFirst + "', Amplifier=" + StrARONE.iAmpl + ", Attenuator=" + StrARONE.iAttenuator + ", Mode=" + StrARONE.iMode + ", BW=" + StrARONE.iBW + ", HPF=" + StrARONE.iHPF + ", LPF=" + StrARONE.iLPF + ", Priority=" + StrARONE.bPriority + " WHERE ID=" + IDn, sqLiteConnect);
            int qwee = sqLiteCommand.ExecuteNonQuery();
            sqLiteConnect.Close();
            return 1;
        }

        private void bTableClear_Click(object sender, EventArgs e)
        {
            if (dgv.CurrentRow == null) return;            
            try
            {
                sqLiteConnect.Open();
                sqLiteCommand = new System.Data.SQLite.SQLiteCommand("DELETE FROM " + NameTable.AR_ONE, sqLiteConnect);
                sqLiteCommand.ExecuteNonQuery();
                zagruzka_tablFromBD(NameTable.AR_ONE.ToString());
            }
            catch { }
            //functionsDB.DeleteAllRecordsDB(NameTable.AR_ONE, 0);
            zagruzka_tablFromBD(NameTable.AR_ONE.ToString());
        }
        private void rbSQLNoise_CheckedChanged(object sender, EventArgs e)
        {
            FlashLedWriteARONE();
            classLibrary_ARONE.SquelchSelectSet(0);

        }
        private void rbSQLLevel_CheckedChanged(object sender, EventArgs e)
        {
            FlashLedWriteARONE();
            classLibrary_ARONE.SquelchSelectSet(1);
        }
        private void tmSignalLevel_Tick(object sender, EventArgs e)
        {
            FlashLedWriteARONE();
            classLibrary_ARONE.SignalLevelUnit_dBm_Get();
        }
        private void tbWaterflowMax_Scroll(object sender, EventArgs e)
        {
            spektorYMaxPorog = tbWaterflowMax.Value;
        }
        private void tbWaterflowMin_Scroll(object sender, EventArgs e)
        {
            spektorYMinPorog = tbWaterflowMin.Value;
        }
        private void tbWaterflowMin_ValueChanged(object sender, EventArgs e)
        {
            if (tbWaterflowMin.Value < tbWaterflowMax.Value)
            {
                spektorYMinPorog = tbWaterflowMin.Value;
            }
            else { tbWaterflowMin.Value = tbWaterflowMax.Value - 1; }
        }
        private void tbWaterflowMax_ValueChanged(object sender, EventArgs e)
        {
            if (tbWaterflowMax.Value > tbWaterflowMin.Value)
            {
                spektorYMaxPorog = tbWaterflowMax.Value;
            }
            else { tbWaterflowMax.Value = tbWaterflowMin.Value + 1; }

        }
        private void pRecDev_Resize(object sender, EventArgs e)
        {
            cbIntensityPaint.Location = new Point(2, 200 + intensityGraph1.Location.Y);
        }
        public void zagruzka_tablFromBD(string nameTable)
        {
            try
            {
                dgv.Rows.Clear();
                jsql = 0;
                if (functionsDB.ConnectionString() != "")
                {
                    sqLiteConnect = new System.Data.SQLite.SQLiteConnection(functionsDB.ConnectionString());             // подключиться к БД      
                    sqLiteConnect.Open();
                    sqLiteCommand = new System.Data.SQLite.SQLiteCommand("SELECT * FROM " + nameTable, sqLiteConnect);
                    //sqLiteCommand = new System.Data.SQLite.SQLiteCommand("SELECT * FROM AR_ONE", sqLiteConnect);
                    sqLiteRead = sqLiteCommand.ExecuteReader();
                    while (sqLiteRead.Read())
                    {
                        if (jsql >= dgv.RowCount)
                            dgv.Rows.Add(null, false, null, null, null, null, null, null, null, null, null, null, null, null);
                        dgv.Rows[jsql].Cells[0].Value = Convert.ToString(sqLiteRead["ID"]);
                        if (Convert.ToBoolean(sqLiteRead["OnOff"]) == false)
                            dgv.Rows[jsql].Cells[1].Value = false;
                        else dgv.Rows[jsql].Cells[1].Value = true;
                        dgv.Rows[jsql].Cells[2].Value = String.Format("{0:# ### #00.000}", Convert.ToDouble(sqLiteRead["Freq"]) / 100);//(Convert.ToDouble(sqLiteRead["Freq"])/100).ToString();
                        dgv.Rows[jsql].Cells[3].Value = Convert.ToString(sqLiteRead["Pause"]);
                        dgv.Rows[jsql].Cells[4].Value = Convert.ToString(sqLiteRead["U"]);
                        dgv.Rows[jsql].Cells[5].Value = Convert.ToString(sqLiteRead["TimeFirst"]).Insert(2, ":").Insert(5, ":");
                        //dgv.Rows[jsql].Cells[6].Value = Convert.ToString(sqLiteRead["Amplifier"]);
                        val = Convert.ToInt32(sqLiteRead["Amplifier"]);

                        switch (val)
                        {
                            case 0:
                                dgv.Rows[jsql].Cells[6].Value = "Выключено";
                                break;
                            case 1:
                                dgv.Rows[jsql].Cells[6].Value = "Включено";
                                break;
                            case 2:
                                dgv.Rows[jsql].Cells[6].Value = "Авто";
                                break;
                        }
                        val = Convert.ToInt32(sqLiteRead["Attenuator"]);
                        switch (val)
                        {
                            case 0:
                                dgv.Rows[jsql].Cells[7].Value = "0 дБ";
                                break;
                            case 1:
                                dgv.Rows[jsql].Cells[7].Value = "10 дБ";
                                break;
                            case 2:
                                dgv.Rows[jsql].Cells[7].Value = "20 дБ";
                                break;
                            case 3:
                                dgv.Rows[jsql].Cells[7].Value = "Авто";
                                break;
                        }
                        val = Convert.ToInt32(sqLiteRead["Mode"]);
                        switch (val)
                        {
                            case 0:
                                dgv.Rows[jsql].Cells[8].Value = "FM";
                                break;
                            case 1:
                                dgv.Rows[jsql].Cells[8].Value = "AM";
                                break;
                            case 2:
                                dgv.Rows[jsql].Cells[8].Value = "CW";
                                break;
                            case 3:
                                dgv.Rows[jsql].Cells[8].Value = "USB";
                                break;
                            case 4:
                                dgv.Rows[jsql].Cells[8].Value = "LSB";
                                break;
                            case 5:
                                dgv.Rows[jsql].Cells[8].Value = "WFM";
                                break;
                            case 6:
                                dgv.Rows[jsql].Cells[8].Value = "NFM";
                                break;
                        }

                        val = Convert.ToInt32(sqLiteRead["BW"]);
                        switch (val)
                        {
                            case 0:
                                dgv.Rows[jsql].Cells[9].Value = "0,5 кГц";
                                break;
                            case 1:
                                dgv.Rows[jsql].Cells[9].Value = "3,0 кГц";
                                break;
                            case 2:
                                dgv.Rows[jsql].Cells[9].Value = "6,0 кГц";
                                break;
                            case 3:
                                dgv.Rows[jsql].Cells[9].Value = "8,5 кГц";
                                break;
                            case 4:
                                dgv.Rows[jsql].Cells[9].Value = "16,0 кГц";
                                break;
                            case 5:
                                dgv.Rows[jsql].Cells[9].Value = "30 кГц";
                                break;
                            case 6:
                                dgv.Rows[jsql].Cells[9].Value = "100 кГц";
                                break;
                            case 7:
                                dgv.Rows[jsql].Cells[9].Value = "200 кГц";
                                break;
                            case 8:
                                dgv.Rows[jsql].Cells[9].Value = "300 кГц";
                                break;
                        }
                        sval = Convert.ToString(sqLiteRead["HPF"]);
                        switch (sval)
                        {
                            case "0":
                                dgv.Rows[jsql].Cells[10].Value = "50 Гц";
                                break;
                            case "1":
                                dgv.Rows[jsql].Cells[10].Value = "200 Гц";
                                break;
                            case "2":
                                dgv.Rows[jsql].Cells[10].Value = "300 Гц";
                                break;
                            case "3":
                                dgv.Rows[jsql].Cells[10].Value = "400 Гц";
                                break;
                            case "4":
                                dgv.Rows[jsql].Cells[10].Value = "Авто";
                                break;
                        }
                        sval = Convert.ToString(sqLiteRead["LPF"]);
                        switch (sval)
                        {
                            case "0":
                                dgv.Rows[jsql].Cells[11].Value = "3 КГц";
                                break;
                            case "1":
                                dgv.Rows[jsql].Cells[11].Value = "4 КГц";
                                break;
                            case "2":
                                dgv.Rows[jsql].Cells[11].Value = "6 КГц";
                                break;
                            case "3":
                                dgv.Rows[jsql].Cells[11].Value = "12 КГц";
                                break;
                            case "4":
                                dgv.Rows[jsql].Cells[11].Value = "Авто";
                                break;
                        }
                        val = Convert.ToInt32(sqLiteRead["Priority"]);
                        switch (val)
                        {
                            case 0:
                                dgv.Rows[jsql].Cells[12].Value = "Обычный";
                                break;
                            case 1:
                                dgv.Rows[jsql].Cells[12].Value = "Важный";
                                break;
                        }
                        dgv.Rows[jsql].Cells[13].Value = Convert.ToString(sqLiteRead["Note"]);
                        jsql++;

                    }

                    //fLoad = true;
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Ошибка чтения данных таблицы!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.ServiceNotification);
                //fLoad = false;

                //return fLoad;
            }
            finally
            {
                sqLiteConnect.Close();
                if (dgv.RowCount < 6) dgv.RowCount = 6;
            }
        }


        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            IDBaraban0Count = 0;
            IDBaraban1Count = 0;
            if (radioButton2.Checked)
            {
                int IDNum = dgv.CurrentRow.Index;
                if (dgv.Rows[IDNum].Cells[0].Value == null)
                {
                    radioButton1.Checked = true;
                    tmBaraban.Enabled = false;
                    return;
                }
                IDBaraban0 = new int[dgv.RowCount];
                IDBaraban1 = new int[dgv.RowCount];

                int countPrior = ZapolnitPrioritety();
                if (countPrior == -1)
                {
                    radioButton1.Checked = true;
                    tmBaraban.Enabled = false;
                    return;
                }
                tmBaraban.Interval = 100;
                //if (BarabanPriorityWorking) tmBaraban.Interval = int.Parse(dgv.Rows[IDBaraban1[0]].Cells[3].Value.ToString());
                //else tmBaraban.Interval = int.Parse(dgv.Rows[IDBaraban0[0]].Cells[3].Value.ToString());

                tmBaraban.Enabled = true;
                //cbARec.Enabled = true;
            }
            else
            {
                tmBaraban.Enabled = false;
                BarabanPriorityWorking = false;
                //cbARec.Enabled = false;
                Baraban0Num = 0;
                Baraban1Num = 0;
                IDBaraban0Count = 0;
                IDBaraban1Count = 0;
            }
        }
        private int ZapolnitPrioritety()
        {
            for (int i = 0; i < dgv.RowCount; i++)
            {
                if (dgv.Rows[i].Cells[12].Value != null)
                {
                    try
                    {
                        string qwe = dgv.Rows[i].Cells[12].Value.ToString().Trim();
                        string qwee = dgv.Rows[i].Cells[1].Value.ToString();
                        if (dgv.Rows[i].Cells[12].Value.ToString() == "Обычный" && dgv.Rows[i].Cells[1].Value.ToString() == "True")
                        {
                            IDBaraban0[IDBaraban0Count] = i;
                            IDBaraban0Count++;
                        }
                        if (dgv.Rows[i].Cells[12].Value.ToString() == "Важный" && dgv.Rows[i].Cells[1].Value.ToString() == "True")
                        {
                            IDBaraban1[IDBaraban1Count] = i;
                            IDBaraban1Count++;
                            BarabanPriorityWorking = true;
                        }
                    }
                    catch { }
                }
            }
            int countZP = IDBaraban0Count + IDBaraban1Count;
            if (IDBaraban0Count * IDBaraban1Count == 0) countZP = (-1) * countZP;
            return countZP;
        }
        private void tmBaraban_Tick(object sender, EventArgs e)
        {
            if ((cbRec.Checked) & (taimer_zapisi < taimer_zapisi_max) & (radioButton2.Checked)) { return; }//если идет запись, то ждать
            if ((cbRec.Checked) & (taimer_zapisi >= taimer_zapisi_max) & (radioButton2.Checked))
            { taimer_zapisi = 0; cbRec.Checked = false; }

            if (((IDBaraban0Count == 0) && (IDBaraban1Count == 0)) || (!radioButton2.Checked))
            {
                tmBaraban.Enabled = false;                //Программная перестройка ВЫКЛ";
                radioButton1.Checked = true;
                return;
            }
            if ((IDBaraban0Count != 0) && (IDBaraban1Count != 0))
            {
                try
                {
                    if (BarabanPriorityWorking) //если приоритетные
                    {
                        Perestroika(IDBaraban1[Baraban1Num]);
                        Baraban1Num++;
                        if (Baraban1Num >= IDBaraban1Count)
                        {
                            Baraban1Num = 0;
                            BarabanPriorityWorking = false;
                        }
                    }
                    else    //если обычные
                    {
                        Perestroika(IDBaraban0[Baraban0Num]);
                        Baraban0Num++;
                        BarabanPriorityWorking = true;
                        if (Baraban0Num >= IDBaraban0Count)
                        {
                            Baraban0Num = 0;
                            //BarabanPriorityWorking = true;
                        }
                    }
                }
                catch { }
            }
            if ((IDBaraban0Count * IDBaraban1Count) < 1)
            {
                try
                {
                    Perestroika(ibaraban);
                }
                catch { }
                ibaraban++;
                try
                {
                    if ((ibaraban >= dgv.RowCount) || (dgv.Rows[ibaraban].Cells[2].Value.ToString() == null))
                    {
                        ibaraban = 0;
                    }
                }
                catch { ibaraban = 0; }

            }
        }
        private void Perestroika(int IDNum)
        {
            dgv.FirstDisplayedScrollingRowIndex = IDNum;
            dgv.Rows[IDNum].Selected = true;
            //dgv.CurrentCell
            if (dgv.Rows[IDNum].Cells[2].Value == null) return;
            double d = double.Parse(dgv.Rows[IDNum].Cells[2].Value.ToString()) / 1000;
            FlashLedWriteARONE();
            classLibrary_ARONE.FrequencySet(double.Parse(dgv.Rows[IDNum].Cells[2].Value.ToString()) / 1000); //перевод в MHz
            tmBaraban.Interval = int.Parse(dgv.Rows[IDNum].Cells[3].Value.ToString());
            nudTablePorogObnar.Value = int.Parse(dgv.Rows[IDNum].Cells[4].Value.ToString());
            //classLibrary_ARONE.AmplifierSet(int.Parse(dgv.Rows[IDNum].Cells[6].Value.ToString()));
            sval = dgv.Rows[IDNum].Cells[6].Value.ToString();
            FlashLedWriteARONE();
            switch (sval)
            {
                case "Выключено":
                    classLibrary_ARONE.AmplifierSet(0);
                    break;
                case "Включено":
                    classLibrary_ARONE.AmplifierSet(1);
                    break;
                case "Авто":
                    classLibrary_ARONE.AmplifierSet(2);
                    break;
            }


            cbTablePriority.Text = dgv.Rows[IDNum].Cells[12].Value.ToString();
            tbNote.Text = dgv.Rows[IDNum].Cells[13].Value.ToString();
            PorogZapisi = int.Parse(dgv.Rows[IDNum].Cells[4].Value.ToString());
            sval = dgv.Rows[IDNum].Cells[7].Value.ToString();
            FlashLedWriteARONE();
            switch (sval)
            {
                case "0 дБ":
                    classLibrary_ARONE.AttenuatorSet(0);
                    break;
                case "10 дБ":
                    classLibrary_ARONE.AttenuatorSet(1);
                    break;
                case "20 дБ":
                    classLibrary_ARONE.AttenuatorSet(2);
                    break;
                case "Авто":
                    classLibrary_ARONE.AttenuatorSet(3);
                    break;
            }
            FlashLedWriteARONE();

            sval = dgv.Rows[IDNum].Cells[8].Value.ToString();
            switch (sval)
            {
                case "FM":
                    classLibrary_ARONE.ModeSet(0);
                    break;
                case "AM":
                    classLibrary_ARONE.ModeSet(1);
                    break;
                case "CW":
                    classLibrary_ARONE.ModeSet(2);
                    break;
                case "USB":
                    classLibrary_ARONE.ModeSet(3);
                    break;
                case "LSB":
                    classLibrary_ARONE.ModeSet(4);
                    break;
                case "WFM":
                    classLibrary_ARONE.ModeSet(5);
                    break;
                case "NFM":
                    classLibrary_ARONE.ModeSet(6);
                    break;
            }
            FlashLedWriteARONE();

            sval = dgv.Rows[IDNum].Cells[9].Value.ToString();
            switch (sval)
            {
                case "0,5 кГц":
                    classLibrary_ARONE.BandWidthSet(0);
                    break;
                case "3,0 кГц":
                    classLibrary_ARONE.BandWidthSet(1);
                    break;
                case "6,0 кГц":
                    classLibrary_ARONE.BandWidthSet(2);
                    break;
                case "8,5 кГц":
                    classLibrary_ARONE.BandWidthSet(3);
                    break;
                case "16,0 кГц":
                    classLibrary_ARONE.BandWidthSet(4);
                    break;
                case "30 кГц":
                    classLibrary_ARONE.BandWidthSet(5);
                    break;
                case "100 кГц":
                    classLibrary_ARONE.BandWidthSet(6);
                    break;
                case "200 кГц":
                    classLibrary_ARONE.BandWidthSet(7);
                    break;
                case "300 кГц":
                    classLibrary_ARONE.BandWidthSet(8);
                    break;
            }
            FlashLedWriteARONE();
            sval = dgv.Rows[IDNum].Cells[10].Value.ToString();
            switch (sval)
            {
                case "50 Гц":
                    classLibrary_ARONE.HighPassFiltrSet("0");
                    break;
                case "200 Гц":
                    classLibrary_ARONE.HighPassFiltrSet("1");
                    break;
                case "300 Гц":
                    classLibrary_ARONE.HighPassFiltrSet("2");
                    break;
                case "400 Гц":
                    classLibrary_ARONE.HighPassFiltrSet("3");
                    break;
                case "Авто":
                    classLibrary_ARONE.HighPassFiltrSet("4");
                    break;
            }

            FlashLedWriteARONE();
            sval = dgv.Rows[IDNum].Cells[11].Value.ToString();
            switch (sval)
            {
                case "3 КГц":
                    classLibrary_ARONE.LowPassFiltrSet("0");
                    break;
                case "4 КГц":
                    classLibrary_ARONE.LowPassFiltrSet("1");
                    break;
                case "6 КГц":
                    classLibrary_ARONE.LowPassFiltrSet("2");
                    break;
                case "12 КГц":
                    classLibrary_ARONE.LowPassFiltrSet("3");
                    break;
                case "Авто":
                    classLibrary_ARONE.LowPassFiltrSet("4");
                    break;
            }
        }

        private void dgv_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            int IDNum = dgv.CurrentRow.Index;
            if (dgv.Rows[IDNum].Cells[0].Value == null) return;
            if (radioButton2.Checked)
                radioButton1.Checked = true;
            Perestroika(dgv.CurrentRow.Index);
        }

        private void dgv_CellClick(object sender, DataGridViewCellEventArgs e)
        {   // НЕ! настраивает приемник, только отображает данные с датагрида в тоолбоксы на форме

            try
            {
                int IDNum = dgv.CurrentRow.Index;
                if (dgv.Rows[IDNum].Cells[0].Value == null) return;
                nudTableFRQ.Value = decimal.Parse(dgv.Rows[IDNum].Cells[2].Value.ToString());
                nudTablePorogObnar.Value = int.Parse(dgv.Rows[IDNum].Cells[4].Value.ToString());
                cbTablePriority.Text = dgv.Rows[IDNum].Cells[12].Value.ToString();
                tbNote.Text = dgv.Rows[IDNum].Cells[13].Value.ToString();
                this.comboBoxAtt.SelectedIndexChanged -= new System.EventHandler(this.comboBoxAtt_SelectedIndexChanged);
                this.comboBoxBW.SelectedIndexChanged -= new System.EventHandler(this.comboBoxBW_SelectedIndexChanged);
                this.comboBoxMode.SelectedIndexChanged -= new System.EventHandler(this.comboBoxMode_SelectedIndexChanged);
                this.comboBoxHPF.SelectedIndexChanged -= new System.EventHandler(this.comboBoxHPF_SelectedIndexChanged);
                this.comboBoxLPF.SelectedIndexChanged -= new System.EventHandler(this.comboBoxLPF_SelectedIndexChanged);
                this.cbAmpl.SelectedIndexChanged -= new EventHandler(this.cbAmpl_SelectedIndexChanged);
                this.tmBaraban.Tick -= new System.EventHandler(this.tmBaraban_Tick);
                comboBoxAtt.Text = dgv.Rows[IDNum].Cells[7].Value.ToString();
                nudPause.Value = int.Parse(dgv.Rows[IDNum].Cells[3].Value.ToString());
                comboBoxMode.Text = dgv.Rows[IDNum].Cells[8].Value.ToString();
                comboBoxBW.Text = dgv.Rows[IDNum].Cells[9].Value.ToString();
                comboBoxHPF.Text = dgv.Rows[IDNum].Cells[10].Value.ToString();
                comboBoxLPF.Text = dgv.Rows[IDNum].Cells[11].Value.ToString();
                cbAmpl.Text = dgv.Rows[IDNum].Cells[6].Value.ToString();
                this.comboBoxAtt.SelectedIndexChanged += new System.EventHandler(this.comboBoxAtt_SelectedIndexChanged);
                this.comboBoxBW.SelectedIndexChanged += new System.EventHandler(this.comboBoxBW_SelectedIndexChanged);
                this.comboBoxMode.SelectedIndexChanged += new System.EventHandler(this.comboBoxMode_SelectedIndexChanged);
                this.comboBoxHPF.SelectedIndexChanged += new System.EventHandler(this.comboBoxHPF_SelectedIndexChanged);
                this.comboBoxLPF.SelectedIndexChanged += new System.EventHandler(this.comboBoxLPF_SelectedIndexChanged);
                this.cbAmpl.SelectedIndexChanged += new EventHandler(this.cbAmpl_SelectedIndexChanged);
                this.tmBaraban.Tick += new System.EventHandler(this.tmBaraban_Tick);
            }
            catch { }

        }


        private void dgv_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int IDNum = dgv.CurrentRow.Index;
            if (dgv.Rows[IDNum].Cells[0].Value == null) return;
            if (dgv.CurrentCell.ColumnIndex == 1)
                if (radioButton2.Checked) radioButton1.Checked = true;
        }

        public void bAOR_Click(object sender, EventArgs e)
        {
            try
            {
                if (bAOR.BackColor == Color.Green)
                {
                    classLibrary_ARONE.ClosePort();


                }
                else
                {
                    classLibrary_ARONE.OpenPort(Com_ArOne);
                    Ask_Arone();
                }
            }
            catch (Exception) { }

        }

        private void changePorogZapisi(int qwe)
        {
            PorogZapisi = qwe;

        }
        private void nudTablePorogObnar_ValueChanged(object sender, EventArgs e)
        {
            changePorogZapisi((int)nudTablePorogObnar.Value);
        }

        async void LibInit()
        {
            var library = await MediaLibrary.MediaLibrary.CreateAsync("papka");
            library.Recorder.StartRecord(99500, 0);
            var result = library.Recorder.StopRecord();
            if (result.HasValue)
            {
                // записалось удачно
            }
            else
            {
                // все плохо
            }
            library.Player.Play(library.Records[0]);
            library.Player.Volume = 0.5f;
            library.Player.Playlist = new[] { library.Records[0], library.Records[1] };
        }
        private void tbNote_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (tbNote.Text.Length > 20)
            {
                tbNote.Text = tbNote.Text.Substring(0, 20);
            }
        }
        private void cbAmpl_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                FlashLedWriteARONE();
                classLibrary_ARONE.AmplifierSet(cbAmpl.SelectedIndex);
            }
            catch (Exception) { }
        }
        private void FlashLedWriteARONE()
        {
            pictureBox14.Image = Properties.Resources.red; 
            if (OnWrite != null)
            {
                OnWrite(1);
            }
            timer = new System.Threading.Timer(TimeStepLedWriteArone, null, TimePause, 0);
        }

        private void TimeStepLedWriteArone(object state)
        {
            try
            {
                if (pictureBox14.InvokeRequired)
                {
                    pictureBox14.Invoke((MethodInvoker)(delegate()
                    {
                        pictureBox14.Image = Properties.Resources.gray;
                        if (OnWrite != null)
                        {
                            OnWrite(0);
                        }
                        timer.Dispose();
                    }));
                }
                else
                {
                    pictureBox14.Image = Properties.Resources.gray;
                    if (OnWrite != null)
                    {
                        OnWrite(0);
                    }
                    timer.Dispose();
                }
            }
            catch { }
        }
        private void FlashLedReadARONE()
        {
            pictureBox13.Image = Properties.Resources.green;
            if (OnRead != null)
            {
                OnRead(1);
            }
            timerRead = new System.Threading.Timer(TimeStepLedReadArone, null, TimePause, 0);
        }
        private void TimeStepLedReadArone(object state)
        {
            if (pictureBox13.InvokeRequired)
            {
                pictureBox13.Invoke((MethodInvoker)(delegate()
                {
                    pictureBox13.Image = Properties.Resources.gray;
                    if (OnRead != null)
                    {
                        OnRead(0);
                    }
                    timerRead.Dispose();
                }));
            }
            else
            {
                pictureBox13.Image = Properties.Resources.gray;
                if (OnRead != null)
                {
                    OnRead(0);
                }
                timerRead.Dispose();

            }
        }

        private void dgv_Resize(object sender, EventArgs e)
        {
            if (dgv.Size.Width < 515)
            {
                dgv.Columns[6].Visible = false;
                dgv.Columns[7].Visible = false;
                dgv.Columns[9].Visible = false;
                dgv.Columns[10].Visible = false;
                dgv.Columns[11].Visible = false;
            }
            if ((dgv.Size.Width > 515) && (dgv.Size.Width < 585))
            {
                dgv.Columns[6].Visible = false;
                dgv.Columns[7].Visible = false;
                dgv.Columns[9].Visible = true;
                dgv.Columns[10].Visible = false;
                dgv.Columns[11].Visible = false;
            }
            if ((dgv.Size.Width > 584) && (dgv.Size.Width < 620))
            {
                dgv.Columns[6].Visible = true;
                dgv.Columns[7].Visible = false;
                dgv.Columns[9].Visible = true;
                dgv.Columns[10].Visible = false;
                dgv.Columns[11].Visible = false;
            }
            if ((dgv.Size.Width > 619) && (dgv.Size.Width < 665))
            {
                dgv.Columns[6].Visible = true;
                dgv.Columns[7].Visible = true;
                dgv.Columns[9].Visible = true;
                dgv.Columns[10].Visible = false;
                dgv.Columns[11].Visible = false;
            }
            if ((dgv.Size.Width > 664) && (dgv.Size.Width < 702))
            {
                dgv.Columns[6].Visible = true;
                dgv.Columns[7].Visible = true;
                dgv.Columns[9].Visible = true;
                dgv.Columns[10].Visible = true;
                dgv.Columns[11].Visible = false;
            }
            if ((dgv.Size.Width > 701))
            {
                dgv.Columns[6].Visible = true;
                dgv.Columns[7].Visible = true;
                dgv.Columns[9].Visible = true;
                dgv.Columns[10].Visible = true;
                dgv.Columns[11].Visible = true;
            }
        }
 
        private void RefreshKatalogs()
        {
            try
            {
                listBox1.Items.Clear();
                listBox2.Items.Clear();
                string dirName = Application.StartupPath + "\\wav";

                string[] dirs = Directory.GetDirectories(dirName);
                foreach (string s in dirs)
                {
                    listBox1.Items.Add(s.Substring(dirName.Length + 1));
                }
                return;

            }
            catch (Exception) { }
        }

        private void RefreshKatalogWavs()
        {
            try
            {
                listBox2.Items.Clear();
                string dirName = Application.StartupPath + "\\wav\\" + listBox1.SelectedItem.ToString();
                if (Directory.Exists(dirName))
                {
                    string[] files = Directory.GetFiles(dirName);
                    foreach (string s in files)
                    {
                        listBox2.Items.Add(s.Substring(dirName.Length + 1, (s.Length - dirName.Length - 5)));
                    }
                }
            }
            catch (Exception) { }
        }
        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            RefreshKatalogWavs();
        }
        bool playing = false;
        //System.Threading.Tasks.Task library ;//= await MediaLibrary.MediaLibrary.CreateAsync(Application.StartupPath + "\\wav\\" + listBox1.SelectedItem.ToString());

        private WaveOutEvent outputDevice;
        private AudioFileReader audioFile;

        async void listBox2_DoubleClick(object sender, EventArgs e)
        {
            //if (playing) return;
            //var library = await MediaLibrary.MediaLibrary.CreateAsync(Application.StartupPath + "\\wav\\" + listBox1.SelectedItem.ToString());
            //int PlayIndex = listBox2.SelectedIndex;
            //pictureBox18.Image = Properties.Resources.wavPlayer; playing = true;           //equalizer ON
            //library.Player.Play(library.Records[PlayIndex]);
            //library.Player.Volume = 100f;
            //library.Player.PlaybackStateChangedEvent += new EventHandler<SharpExtensions.EventArg<PlaybackState>>(PlayBackStateEventt);

            try
            {
                string fileName = Application.StartupPath + "\\wav\\" + listBox1.SelectedItem.ToString() + "\\" + listBox2.SelectedItem.ToString() + ".wav";

                DisposeWave();

                outputDevice = new WaveOutEvent();
                audioFile = new AudioFileReader(fileName);
                outputDevice.Init(audioFile);

                outputDevice.Play();
                pictureBox18.Image = Properties.Resources.wavPlayer;  //equalizer ON

                Task.Run(async () =>
                {
                    while (true)
                    {
                        //Console.WriteLine(audioFile.CurrentTime.ToString());

                        if (audioFile.CurrentTime == audioFile.TotalTime)
                            pictureBox18.Image = Properties.Resources.wavPlayerGray;  //equalizer OFF

                        await Task.Delay(100);
                    }
                });
            }
            catch { }
            //catch (Exception ex) { MessageBox.Show(ex.Message); }

        }

        public void DisposeWave()
        {
            try
            {
                if (outputDevice != null)
                {
                    if (outputDevice.PlaybackState == NAudio.Wave.PlaybackState.Playing) outputDevice.Stop();
                    outputDevice.Dispose();
                    outputDevice = null;
                }
                if (audioFile != null)
                {
                    audioFile.Dispose();
                    audioFile = null;
                }
            }
            catch { }
            //catch (Exception ex) { MessageBox.Show(ex.Message); }
        }

        private void PlayBackStateEventt(object sender, SharpExtensions.EventArg<PlaybackState> e)
        {
            if (e.Data.ToString() == "Stopped")
            {
                pictureBox18.Image = Properties.Resources.wavPlayerGray; playing = false;
            }         //equalizer OFF
            if (e.Data.ToString() == "Playing")
            {
                pictureBox18.Image = Properties.Resources.wavPlayer; playing = true;
            }              //equalizer ON
            if (e.Data.ToString() == "Paused")
            {
                pictureBox18.Image = Properties.Resources.wavPlayerGray; playing = false;
            }              //equalizer OFF
        }
        private void tabControl1_Selecting(object sender, TabControlCancelEventArgs e)
        {
            if (tabControl1.SelectedIndex == 1) RefreshKatalogs();
        }
        private void listBox1_DoubleClick(object sender, EventArgs e)
        {
            RefreshKatalogs();
        }

        private void bFrqMinus_Click(object sender, EventArgs e)
        {
            double myfrq = double.Parse(classLibrary_ARONE.ArOne.frequency.ToString());
            myfrq = myfrq - step_frq_MHz;
            if (myfrq > 30000)
            {
                classLibrary_ARONE.FrequencySet(myfrq / 1000000);
            }
            else { };//MessageBox.Show("Не может быть меньше!"); }

        }

        private void bFrqPlus_Click(object sender, EventArgs e)
        {

            double myfrq = double.Parse(classLibrary_ARONE.ArOne.frequency.ToString());
            myfrq = myfrq + step_frq_MHz;
            if (myfrq < 3299999999)
            {
                classLibrary_ARONE.FrequencySet(myfrq / 1000000);
            }
            else { };//MessageBox.Show("Не может быть больше!"); }

        }

        private void cbFrqStep_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch ((int)cbFrqStep.SelectedIndex)
            {
                case 0: step_frq_MHz = 1f; break;// / 1000000f; break;
                case 1: step_frq_MHz = 10f; break;// / 1000000f; break;
                case 2: step_frq_MHz = 50f; break;// / 1000000f; break;
                case 3: step_frq_MHz = 100f; break;// / 1000000f; break;
                case 4: step_frq_MHz = 500f; break;// / 1000000f; break;
                case 5: step_frq_MHz = 1000f; break;// / 1000000f; break;
                case 6: step_frq_MHz = 5000f; break;// / 1000000f; break;
                case 7: step_frq_MHz = 10000f; break;// / 1000000f; break;
                case 8: step_frq_MHz = 50000f; break;// / 1000000f; break;
                case 9: step_frq_MHz = 100000f; break;// / 1000000f; break;
                case 10: step_frq_MHz = 500000f; break;// / 1000000f; break;
                case 11: step_frq_MHz = 1000000f; break;// / 1000000f; break;
                case 12: step_frq_MHz = 5000000f; break;// / 1000000f; break;
                case 13: step_frq_MHz = 10000000f; break;// / 1000000f; break;
                default: MessageBox.Show("case not worked!"); break;
            }

        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            TimePause = (int)numericUpDown1.Value * 10;
        }

        private void bWavPlayer_Click(object sender, EventArgs e)
        {
            //try
            //{
            //    string WavPath = "";
            //    if (listBox2.SelectedItem != null)
            //        WavPath = Application.StartupPath + "\\wav\\" + listBox1.SelectedItem.ToString() + "\\" + listBox2.SelectedItem.ToString() + ".wav";
            //    WritePrivateProfileString("Player", "PathIni", WavPath, Application.StartupPath + "\\Player\\player.ini");
            //    string name = "Player";
            //    System.Diagnostics.Process[] process = System.Diagnostics.Process.GetProcesses();

            //    bool blProc = false;
            //    int i = 0;

            //    while (i < process.Length)
            //    {
            //        if (process[i].ProcessName == name)
            //        {
            //            blProc = true;
            //            i = process.Length;
            //        }
            //        i++;
            //    }

            //    if (blProc == false)
            //    {
            //        ProcessWavPlayer.Start();
            //    }
            //}
            //catch (Exception ex) { }

            try
            {
                string name = "Player";
                System.Diagnostics.Process[] process = System.Diagnostics.Process.GetProcesses();

                bool blProc = false;
                int i = 0;

                while (i < process.Length)
                {
                    if (process[i].ProcessName == name)
                    {
                        blProc = true;
                        i = process.Length;
                    }
                    i++;
                }

                VariableStatic.VariableCommon variableCommon = new VariableStatic.VariableCommon();

                string WavPath = "";
                try
            {
                    if (listBox2.SelectedItem != null)
                    {
                        WavPath = Application.StartupPath + "\\wav\\" + listBox1.SelectedItem.ToString() + "\\" + listBox2.SelectedItem.ToString() + ".wav";
                        WritePrivateProfileString("Player", "PathIni", WavPath, Application.StartupPath + "\\Player RU\\Player.ini");
                    }

                    ProcessWavPlayer.StartInfo.FileName = Application.StartupPath + "\\Player RU\\Player.exe";
                }
                catch { }

                if (blProc == false)
                {
                    ProcessWavPlayer.Start();
                }
            }
            catch (Exception) { }


        }

        private async void bIspPel_Click(object sender, EventArgs e)
        {
            double frequency;
            if (double.TryParse(lFrq.Text, out frequency))
            {
                var minFreq = frequency - 0.05;
                var maxFreq = frequency + 0.05;
                //var result = await VariableDynamic.VariableWork.aWPtoBearingDSPprotocolNew.ExecutiveDF(minFreq, maxFreq, 3, 3);
                //if (result != null)
                //{
                //    lPel.Text = (result.Direction / 10).ToString();
                //}
            }
            try
            {
                string Frq2 = lFRQFromAOR.Text;
                Frq2 = Frq2.Substring(0, Frq2.Length - 3);
                Frq2 = Frq2.Replace(',', '.');

                if (double.TryParse(Frq2, out frequency))
                {
                    frequency = frequency / 1000d;
                    VariableDynamic.VariableWork VW = new VariableDynamic.VariableWork();
                    VW.RecBRSsend(frequency);
                }
            }
            catch { }

            try
            {
                string Frq2 = lFRQFromAOR.Text;
                Frq2 = Frq2.Substring(0, Frq2.Length - 3);

                if (double.TryParse(Frq2, out frequency))
                {
                    frequency = frequency / 1000d;
                    VariableDynamic.VariableWork VW = new VariableDynamic.VariableWork();
                    VW.RecBRSsend(frequency);
                }
            }
            catch { }

        }

        private void cbMute_CheckedChanged(object sender, EventArgs e)
        {
            if (cbMute.Checked) MuteArone(true);
            else MuteArone(false);
        }
         
        private void bAddToTableFromAOR_Click(object sender, EventArgs e)
        {
            StrARONE.iFreq = classLibrary_ARONE.ArOne.frequency/10;
            StrARONE.iU = (int)nudTablePorogObnar.Value;
            StrARONE.bPriority = (byte)cbTablePriority.SelectedIndex;
            StrARONE.iAttenuator = comboBoxAtt.SelectedIndex;
            StrARONE.iAmpl = classLibrary_ARONE.ArOne.Amplifier;
            StrARONE.iBW = classLibrary_ARONE.ArOne.Bandwidth;
            StrARONE.iHPF = classLibrary_ARONE.ArOne.HighPassFilter;
            StrARONE.iLPF = classLibrary_ARONE.ArOne.LowPassFilter;
            StrARONE.iMode = classLibrary_ARONE.ArOne.Mode;
            StrARONE.iOnOff = 1;
            StrARONE.iPause = (int)nudPause.Value;
            StrARONE.Note = tbNote.Text.ToString();
            StrARONE.sTimeFirst = DateTime.Now.Hour.ToString("00") + DateTime.Now.Minute.ToString("00") + DateTime.Now.Second.ToString("00");
            sqLiteConnect = new System.Data.SQLite.SQLiteConnection(functionsDB.ConnectionString());             // подключиться к БД      
            sqLiteConnect.Open();
            sqLiteCommand = new System.Data.SQLite.SQLiteCommand(
                "INSERT INTO AR_ONE(OnOff,Freq, Pause, U, TimeFirst, Amplifier, Attenuator, Mode, BW, HPF, LPF, Priority, Note) VALUES (" + StrARONE.iOnOff + "," + StrARONE.iFreq + "," + StrARONE.iPause + "," + StrARONE.iU + ",'" + StrARONE.sTimeFirst + "'," + StrARONE.iAmpl + "," + StrARONE.iAttenuator + "," + StrARONE.iMode + "," + StrARONE.iBW + "," + StrARONE.iHPF + "," + StrARONE.iLPF + "," + StrARONE.bPriority + ",'" + StrARONE.Note.ToString() + "')", sqLiteConnect);
            int qwee = sqLiteCommand.ExecuteNonQuery();
            sqLiteConnect.Close();
            zagruzka_tablFromBD(NameTable.AR_ONE.ToString());
        }

        private void MuteArone(bool mute)
        {
            //const string aroneVolumeChannelName = "Лин. вход (Realtek High Definition Audio)";
            //const string aroneVolumeChannelName2 = "Микрофон (Устройство с поддержкой High Definition Audio)";
            const string aroneVolumeChannelName2 = "Микрофон (Cirrus Logic CDB4207)";

            var channels = AppVolumeLibrary.AppVolumeLibrary.EnumerateApplications().ToArray();
            if (channels.Any(c => c == aroneVolumeChannelName2))
            {
                AppVolumeLibrary.AppVolumeLibrary.SetApplicationMute(aroneVolumeChannelName2, mute);
            }
        }

        void VariableWork_OnChangeFrequency(long iFreq)
        {
            try
            {
                if ((iFreq > 3300000000) || (iFreq < 30000)) { return; }

                string mesFRQ = "RF" + iFreq.ToString().PadLeft(10, '0');
                //ComPort.Write(mesFRQ+"\r\n");
                //classLibrary_ARONE.SendToArone(mesFRQ);
                if (radioButton1.Checked)
                    classLibrary_ARONE.FrequencySet(iFreq / 1000000d);
            }
            catch { }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ConnectPort();
        }

        private void nudPause_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= '0') && (e.KeyChar <= '9') || (e.KeyChar == '\b')) // цифра, Backspace
            {
                return;
            }
            e.Handled = true;
        }

        private void nudPause_Validated(object sender, EventArgs e)
        {
            if (nudPause.Value > 30000) nudPause.Value = 30000;
        }

        private void bPause_Click(object sender, EventArgs e)
        {
            try
            {
                if (outputDevice != null)
                {
                    if (outputDevice.PlaybackState == NAudio.Wave.PlaybackState.Playing)
                    {
                        bPause.Image = Properties.Resources.Continue;
                        pictureBox18.Image = Properties.Resources.wavPlayerGray;

                        outputDevice.Pause();
                    }
                    else if (outputDevice.PlaybackState == NAudio.Wave.PlaybackState.Paused)
                    {
                        bPause.Image = Properties.Resources.Pause;
                        pictureBox18.Image = Properties.Resources.wavPlayer;  //equalizer ON

                        outputDevice.Play();
                        // --------------------------------------------------------------------------------
                    }
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }

        private void bStop_Click(object sender, EventArgs e)
        {
            try
            {
                pictureBox18.Image = Properties.Resources.wavPlayerGray;
                bPause.Image = Properties.Resources.Pause;

                DisposeWave();
            }
            catch { }
        }
    }
}



