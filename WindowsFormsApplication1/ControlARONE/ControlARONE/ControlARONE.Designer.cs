﻿namespace ControlARONE
{
    partial class ControlARONE
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ControlARONE));
            this.label22 = new System.Windows.Forms.Label();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.label23 = new System.Windows.Forms.Label();
            this.cbFrqStep = new System.Windows.Forms.ComboBox();
            this.bFrqPlus = new System.Windows.Forms.Button();
            this.bFrqMinus = new System.Windows.Forms.Button();
            this.panel22 = new System.Windows.Forms.Panel();
            this.label21 = new System.Windows.Forms.Label();
            this.panel23 = new System.Windows.Forms.Panel();
            this.lREC = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.waveformGraph1 = new NationalInstruments.UI.WindowsForms.WaveformGraph();
            this.PlotSignal = new NationalInstruments.UI.WaveformPlot();
            this.xAxis1 = new NationalInstruments.UI.XAxis();
            this.yAxis1 = new NationalInstruments.UI.YAxis();
            this.PlotAverage = new NationalInstruments.UI.WaveformPlot();
            this.MinPorogPlot = new NationalInstruments.UI.WaveformPlot();
            this.MaxPorogPlot = new NationalInstruments.UI.WaveformPlot();
            this.PlotPorogObn = new NationalInstruments.UI.WaveformPlot();
            this.tbWaterflowMin = new System.Windows.Forms.TrackBar();
            this.tbWaterflowMax = new System.Windows.Forms.TrackBar();
            this.intensityGraph1 = new NationalInstruments.UI.WindowsForms.IntensityGraph();
            this.colorScale1 = new NationalInstruments.UI.ColorScale();
            this.intensityPlot1 = new NationalInstruments.UI.IntensityPlot();
            this.intensityXAxis1 = new NationalInstruments.UI.IntensityXAxis();
            this.intensityYAxis1 = new NationalInstruments.UI.IntensityYAxis();
            this.lPel = new System.Windows.Forms.Label();
            this.bIspPel = new System.Windows.Forms.Button();
            this.comboBoxMode = new System.Windows.Forms.ComboBox();
            this.comboBoxHPF = new System.Windows.Forms.ComboBox();
            this.comboBoxLPF = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.comboBoxBW = new System.Windows.Forms.ComboBox();
            this.b0 = new System.Windows.Forms.Button();
            this.comboBoxAtt = new System.Windows.Forms.ComboBox();
            this.b4 = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.b5 = new System.Windows.Forms.Button();
            this.b2 = new System.Windows.Forms.Button();
            this.label16 = new System.Windows.Forms.Label();
            this.b3 = new System.Windows.Forms.Button();
            this.bESC = new System.Windows.Forms.Button();
            this.bENT = new System.Windows.Forms.Button();
            this.b7 = new System.Windows.Forms.Button();
            this.b6 = new System.Windows.Forms.Button();
            this.label17 = new System.Windows.Forms.Label();
            this.b1 = new System.Windows.Forms.Button();
            this.b9 = new System.Windows.Forms.Button();
            this.b8 = new System.Windows.Forms.Button();
            this.groupBoxAmpl = new System.Windows.Forms.GroupBox();
            this.cbAmpl = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.trackBarManualGain = new System.Windows.Forms.TrackBar();
            this.bDot = new System.Windows.Forms.Button();
            this.pScreen = new System.Windows.Forms.Panel();
            this.lFRQFromAOR = new System.Windows.Forms.Label();
            this.lFrq = new System.Windows.Forms.Label();
            this.lBw = new System.Windows.Forms.Label();
            this.lat = new System.Windows.Forms.Label();
            this.lMode = new System.Windows.Forms.Label();
            this.lLevel = new System.Windows.Forms.Label();
            this.lAmplifier = new System.Windows.Forms.Label();
            this.groupBoxRegime = new System.Windows.Forms.GroupBox();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tpIRI_KRPU = new System.Windows.Forms.TabPage();
            this.bAddToTableFromAOR = new System.Windows.Forms.Button();
            this.nudTableFRQ = new System.Windows.Forms.NumericUpDown();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.Number = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OnOff = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Fequency = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Pause = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Treshold = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TimeFirst = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Amplifier = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Att = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Mode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BW = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HPF = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LPF = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Priority = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Note = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tbNote = new System.Windows.Forms.TextBox();
            this.bTableChange = new System.Windows.Forms.Button();
            this.nudPause = new System.Windows.Forms.NumericUpDown();
            this.bTableAdd = new System.Windows.Forms.Button();
            this.label18 = new System.Windows.Forms.Label();
            this.bTableDel = new System.Windows.Forms.Button();
            this.cbTablePriority = new System.Windows.Forms.ComboBox();
            this.bTableClear = new System.Windows.Forms.Button();
            this.nudTablePorogObnar = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.tpPlayer = new System.Windows.Forms.TabPage();
            this.bStop = new System.Windows.Forms.Button();
            this.bPause = new System.Windows.Forms.Button();
            this.cbMute = new System.Windows.Forms.CheckBox();
            this.bWavPlayer = new System.Windows.Forms.Button();
            this.listBox2 = new System.Windows.Forms.ListBox();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.pictureBox18 = new System.Windows.Forms.PictureBox();
            this.tm500 = new System.Windows.Forms.Timer(this.components);
            this.tmFlashFRQ = new System.Windows.Forms.Timer(this.components);
            this.tmBaraban = new System.Windows.Forms.Timer(this.components);
            this.bAOR = new System.Windows.Forms.Button();
            this.cbARec = new System.Windows.Forms.CheckBox();
            this.cbRec = new System.Windows.Forms.CheckBox();
            this.cbIntensityPaint = new System.Windows.Forms.CheckBox();
            this.cbACP = new System.Windows.Forms.CheckBox();
            this.cbAverage = new System.Windows.Forms.CheckBox();
            this.pictureBox13 = new System.Windows.Forms.PictureBox();
            this.pictureBox14 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.panel22.SuspendLayout();
            this.panel23.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.waveformGraph1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbWaterflowMin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbWaterflowMax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intensityGraph1)).BeginInit();
            this.groupBoxAmpl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarManualGain)).BeginInit();
            this.pScreen.SuspendLayout();
            this.groupBoxRegime.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tpIRI_KRPU.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudTableFRQ)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPause)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudTablePorogObnar)).BeginInit();
            this.tpPlayer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).BeginInit();
            this.SuspendLayout();
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label22.Location = new System.Drawing.Point(10, 3);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(185, 13);
            this.label22.TabIndex = 364;
            this.label22.Text = "КРПУ AR-ONE слухового контроля";
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Location = new System.Drawing.Point(255, 169);
            this.numericUpDown1.Maximum = new decimal(new int[] {
            21600,
            0,
            0,
            0});
            this.numericUpDown1.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(72, 20);
            this.numericUpDown1.TabIndex = 363;
            this.numericUpDown1.ThousandsSeparator = true;
            this.numericUpDown1.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numericUpDown1.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label23.Location = new System.Drawing.Point(79, 175);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(227, 13);
            this.label23.TabIndex = 362;
            this.label23.Text = "Максимальное время записи, с...................";
            // 
            // cbFrqStep
            // 
            this.cbFrqStep.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbFrqStep.Enabled = false;
            this.cbFrqStep.FormattingEnabled = true;
            this.cbFrqStep.Items.AddRange(new object[] {
            "1 Гц",
            "10 Гц",
            "50 Гц",
            "100 Гц",
            "500 Гц",
            "1 КГц",
            "5 КГц",
            "10 КГц",
            "50 КГц",
            "100 КГц",
            "500 КГц",
            "1 МГц",
            "5 МГц",
            "10 МГц"});
            this.cbFrqStep.Location = new System.Drawing.Point(98, 113);
            this.cbFrqStep.Name = "cbFrqStep";
            this.cbFrqStep.Size = new System.Drawing.Size(70, 21);
            this.cbFrqStep.TabIndex = 361;
            this.cbFrqStep.SelectedIndexChanged += new System.EventHandler(this.cbFrqStep_SelectedIndexChanged);
            // 
            // bFrqPlus
            // 
            this.bFrqPlus.Enabled = false;
            this.bFrqPlus.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.bFrqPlus.Location = new System.Drawing.Point(139, 135);
            this.bFrqPlus.Name = "bFrqPlus";
            this.bFrqPlus.Size = new System.Drawing.Size(30, 23);
            this.bFrqPlus.TabIndex = 360;
            this.bFrqPlus.Text = "+";
            this.bFrqPlus.UseVisualStyleBackColor = true;
            this.bFrqPlus.Click += new System.EventHandler(this.bFrqPlus_Click);
            // 
            // bFrqMinus
            // 
            this.bFrqMinus.Enabled = false;
            this.bFrqMinus.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.bFrqMinus.Location = new System.Drawing.Point(97, 135);
            this.bFrqMinus.Name = "bFrqMinus";
            this.bFrqMinus.Size = new System.Drawing.Size(30, 23);
            this.bFrqMinus.TabIndex = 359;
            this.bFrqMinus.Text = "-";
            this.bFrqMinus.UseVisualStyleBackColor = true;
            this.bFrqMinus.Click += new System.EventHandler(this.bFrqMinus_Click);
            // 
            // panel22
            // 
            this.panel22.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel22.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel22.Controls.Add(this.label21);
            this.panel22.Controls.Add(this.panel23);
            this.panel22.Controls.Add(this.intensityGraph1);
            this.panel22.Location = new System.Drawing.Point(38, 195);
            this.panel22.Name = "panel22";
            this.panel22.Size = new System.Drawing.Size(448, 276);
            this.panel22.TabIndex = 358;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label21.Location = new System.Drawing.Point(7, 158);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(88, 13);
            this.label21.TabIndex = 270;
            this.label21.Text = "Спектрограмма";
            // 
            // panel23
            // 
            this.panel23.Controls.Add(this.lREC);
            this.panel23.Controls.Add(this.label20);
            this.panel23.Controls.Add(this.waveformGraph1);
            this.panel23.Controls.Add(this.tbWaterflowMin);
            this.panel23.Controls.Add(this.tbWaterflowMax);
            this.panel23.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel23.Location = new System.Drawing.Point(0, 0);
            this.panel23.Name = "panel23";
            this.panel23.Size = new System.Drawing.Size(446, 165);
            this.panel23.TabIndex = 177;
            // 
            // lREC
            // 
            this.lREC.AutoSize = true;
            this.lREC.BackColor = System.Drawing.Color.Black;
            this.lREC.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.lREC.ForeColor = System.Drawing.Color.Red;
            this.lREC.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lREC.Location = new System.Drawing.Point(12, 21);
            this.lREC.Name = "lREC";
            this.lREC.Size = new System.Drawing.Size(46, 20);
            this.lREC.TabIndex = 236;
            this.lREC.Text = "REC";
            this.lREC.Visible = false;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label20.Location = new System.Drawing.Point(4, 5);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(43, 13);
            this.label20.TabIndex = 269;
            this.label20.Text = "Спектр";
            // 
            // waveformGraph1
            // 
            this.waveformGraph1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.waveformGraph1.Border = NationalInstruments.UI.Border.None;
            this.waveformGraph1.Enabled = false;
            this.waveformGraph1.Location = new System.Drawing.Point(-15, 9);
            this.waveformGraph1.Name = "waveformGraph1";
            this.waveformGraph1.Plots.AddRange(new NationalInstruments.UI.WaveformPlot[] {
            this.PlotSignal,
            this.PlotAverage,
            this.MinPorogPlot,
            this.MaxPorogPlot,
            this.PlotPorogObn});
            this.waveformGraph1.Size = new System.Drawing.Size(437, 152);
            this.waveformGraph1.TabIndex = 139;
            this.waveformGraph1.UseColorGenerator = true;
            this.waveformGraph1.XAxes.AddRange(new NationalInstruments.UI.XAxis[] {
            this.xAxis1});
            this.waveformGraph1.YAxes.AddRange(new NationalInstruments.UI.YAxis[] {
            this.yAxis1});
            // 
            // PlotSignal
            // 
            this.PlotSignal.XAxis = this.xAxis1;
            this.PlotSignal.YAxis = this.yAxis1;
            // 
            // xAxis1
            // 
            this.xAxis1.BaseLineVisible = true;
            this.xAxis1.Caption = "Частота, Гц";
            this.xAxis1.CaptionFont = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.xAxis1.EndLabelsAlwaysVisible = false;
            this.xAxis1.Mode = NationalInstruments.UI.AxisMode.Fixed;
            this.xAxis1.Range = new NationalInstruments.UI.Range(0D, 22050D);
            // 
            // yAxis1
            // 
            this.yAxis1.Caption = "Уровень, дБм";
            this.yAxis1.CaptionFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.yAxis1.CaptionPosition = NationalInstruments.UI.YAxisPosition.Right;
            this.yAxis1.MajorDivisions.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.yAxis1.MajorDivisions.GridLineStyle = NationalInstruments.UI.LineStyle.Dot;
            this.yAxis1.MajorDivisions.GridVisible = true;
            this.yAxis1.MajorDivisions.TickVisible = false;
            this.yAxis1.Mode = NationalInstruments.UI.AxisMode.Fixed;
            this.yAxis1.Position = NationalInstruments.UI.YAxisPosition.Right;
            this.yAxis1.Range = new NationalInstruments.UI.Range(-120D, 0D);
            // 
            // PlotAverage
            // 
            this.PlotAverage.LineColor = System.Drawing.Color.White;
            this.PlotAverage.LineColorPrecedence = NationalInstruments.UI.ColorPrecedence.UserDefinedColor;
            this.PlotAverage.XAxis = this.xAxis1;
            this.PlotAverage.YAxis = this.yAxis1;
            // 
            // MinPorogPlot
            // 
            this.MinPorogPlot.LineColor = System.Drawing.Color.Gray;
            this.MinPorogPlot.LineColorPrecedence = NationalInstruments.UI.ColorPrecedence.UserDefinedColor;
            this.MinPorogPlot.LineStyle = NationalInstruments.UI.LineStyle.Dash;
            this.MinPorogPlot.XAxis = this.xAxis1;
            this.MinPorogPlot.YAxis = this.yAxis1;
            // 
            // MaxPorogPlot
            // 
            this.MaxPorogPlot.LineColor = System.Drawing.Color.Gray;
            this.MaxPorogPlot.LineColorPrecedence = NationalInstruments.UI.ColorPrecedence.UserDefinedColor;
            this.MaxPorogPlot.LineStyle = NationalInstruments.UI.LineStyle.Dash;
            this.MaxPorogPlot.XAxis = this.xAxis1;
            this.MaxPorogPlot.YAxis = this.yAxis1;
            // 
            // PlotPorogObn
            // 
            this.PlotPorogObn.BasePlot = this.PlotSignal;
            this.PlotPorogObn.LineColor = System.Drawing.Color.DarkGray;
            this.PlotPorogObn.LineColorPrecedence = NationalInstruments.UI.ColorPrecedence.UserDefinedColor;
            this.PlotPorogObn.XAxis = this.xAxis1;
            this.PlotPorogObn.YAxis = this.yAxis1;
            // 
            // tbWaterflowMin
            // 
            this.tbWaterflowMin.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.tbWaterflowMin.AutoSize = false;
            this.tbWaterflowMin.Enabled = false;
            this.tbWaterflowMin.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.tbWaterflowMin.Location = new System.Drawing.Point(420, 12);
            this.tbWaterflowMin.Maximum = 0;
            this.tbWaterflowMin.Minimum = -120;
            this.tbWaterflowMin.Name = "tbWaterflowMin";
            this.tbWaterflowMin.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.tbWaterflowMin.Size = new System.Drawing.Size(15, 113);
            this.tbWaterflowMin.TabIndex = 172;
            this.tbWaterflowMin.TickStyle = System.Windows.Forms.TickStyle.None;
            this.tbWaterflowMin.Value = -110;
            this.tbWaterflowMin.Scroll += new System.EventHandler(this.tbWaterflowMin_Scroll);
            // 
            // tbWaterflowMax
            // 
            this.tbWaterflowMax.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.tbWaterflowMax.AutoSize = false;
            this.tbWaterflowMax.Enabled = false;
            this.tbWaterflowMax.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.tbWaterflowMax.Location = new System.Drawing.Point(433, 12);
            this.tbWaterflowMax.Maximum = 0;
            this.tbWaterflowMax.Minimum = -120;
            this.tbWaterflowMax.Name = "tbWaterflowMax";
            this.tbWaterflowMax.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.tbWaterflowMax.Size = new System.Drawing.Size(15, 113);
            this.tbWaterflowMax.TabIndex = 176;
            this.tbWaterflowMax.TickStyle = System.Windows.Forms.TickStyle.None;
            this.tbWaterflowMax.Value = -40;
            this.tbWaterflowMax.Scroll += new System.EventHandler(this.tbWaterflowMax_Scroll);
            // 
            // intensityGraph1
            // 
            this.intensityGraph1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.intensityGraph1.Border = NationalInstruments.UI.Border.None;
            this.intensityGraph1.CaptionVisible = false;
            this.intensityGraph1.ColorScales.AddRange(new NationalInstruments.UI.ColorScale[] {
            this.colorScale1});
            this.intensityGraph1.Enabled = false;
            this.intensityGraph1.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F);
            this.intensityGraph1.Location = new System.Drawing.Point(0, 171);
            this.intensityGraph1.Name = "intensityGraph1";
            this.intensityGraph1.Plots.AddRange(new NationalInstruments.UI.IntensityPlot[] {
            this.intensityPlot1});
            this.intensityGraph1.Size = new System.Drawing.Size(433, 107);
            this.intensityGraph1.TabIndex = 140;
            this.intensityGraph1.XAxes.AddRange(new NationalInstruments.UI.IntensityXAxis[] {
            this.intensityXAxis1});
            this.intensityGraph1.YAxes.AddRange(new NationalInstruments.UI.IntensityYAxis[] {
            this.intensityYAxis1});
            // 
            // colorScale1
            // 
            this.colorScale1.ColorMap.AddRange(new NationalInstruments.UI.ColorMapEntry[] {
            new NationalInstruments.UI.ColorMapEntry(-80D, System.Drawing.Color.Blue),
            new NationalInstruments.UI.ColorMapEntry(-40D, System.Drawing.Color.Lime)});
            this.colorScale1.HighColor = System.Drawing.Color.Red;
            this.colorScale1.Range = new NationalInstruments.UI.Range(-120D, 0D);
            // 
            // intensityPlot1
            // 
            this.intensityPlot1.ColorScale = this.colorScale1;
            this.intensityPlot1.PixelInterpolation = true;
            this.intensityPlot1.XAxis = this.intensityXAxis1;
            this.intensityPlot1.YAxis = this.intensityYAxis1;
            // 
            // intensityXAxis1
            // 
            this.intensityXAxis1.Mode = NationalInstruments.UI.IntensityAxisMode.StripChart;
            this.intensityXAxis1.Range = new NationalInstruments.UI.Range(0D, 2205D);
            this.intensityXAxis1.Visible = false;
            // 
            // intensityYAxis1
            // 
            this.intensityYAxis1.Range = new NationalInstruments.UI.Range(0D, 100D);
            this.intensityYAxis1.Visible = false;
            // 
            // lPel
            // 
            this.lPel.AutoSize = true;
            this.lPel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lPel.Location = new System.Drawing.Point(421, 21);
            this.lPel.Name = "lPel";
            this.lPel.Size = new System.Drawing.Size(0, 13);
            this.lPel.TabIndex = 356;
            this.lPel.Visible = false;
            // 
            // bIspPel
            // 
            this.bIspPel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.bIspPel.Location = new System.Drawing.Point(340, 19);
            this.bIspPel.Name = "bIspPel";
            this.bIspPel.Size = new System.Drawing.Size(61, 23);
            this.bIspPel.TabIndex = 355;
            this.bIspPel.Text = "Исп. П.";
            this.bIspPel.UseVisualStyleBackColor = true;
            this.bIspPel.Click += new System.EventHandler(this.bIspPel_Click);
            // 
            // comboBoxMode
            // 
            this.comboBoxMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxMode.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.comboBoxMode.FormattingEnabled = true;
            this.comboBoxMode.Items.AddRange(new object[] {
            "FM",
            "AM",
            "CW",
            "USB",
            "LSB",
            "WFM",
            "NFM"});
            this.comboBoxMode.Location = new System.Drawing.Point(254, 76);
            this.comboBoxMode.Name = "comboBoxMode";
            this.comboBoxMode.Size = new System.Drawing.Size(73, 21);
            this.comboBoxMode.TabIndex = 336;
            this.comboBoxMode.SelectedIndexChanged += new System.EventHandler(this.comboBoxMode_SelectedIndexChanged);
            // 
            // comboBoxHPF
            // 
            this.comboBoxHPF.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxHPF.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.comboBoxHPF.FormattingEnabled = true;
            this.comboBoxHPF.Items.AddRange(new object[] {
            "50 Гц",
            "200 Гц",
            "300 Гц",
            "400 Гц",
            "Авто"});
            this.comboBoxHPF.Location = new System.Drawing.Point(254, 122);
            this.comboBoxHPF.Name = "comboBoxHPF";
            this.comboBoxHPF.Size = new System.Drawing.Size(73, 21);
            this.comboBoxHPF.TabIndex = 339;
            this.comboBoxHPF.SelectedIndexChanged += new System.EventHandler(this.comboBoxHPF_SelectedIndexChanged);
            // 
            // comboBoxLPF
            // 
            this.comboBoxLPF.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxLPF.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.comboBoxLPF.FormattingEnabled = true;
            this.comboBoxLPF.Items.AddRange(new object[] {
            "3 кГц",
            "4 кГц",
            "6 кГц",
            "12 кГц",
            "Авто"});
            this.comboBoxLPF.Location = new System.Drawing.Point(254, 145);
            this.comboBoxLPF.Name = "comboBoxLPF";
            this.comboBoxLPF.Size = new System.Drawing.Size(73, 21);
            this.comboBoxLPF.TabIndex = 340;
            this.comboBoxLPF.SelectedIndexChanged += new System.EventHandler(this.comboBoxLPF_SelectedIndexChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label9.Location = new System.Drawing.Point(205, 152);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(91, 13);
            this.label9.TabIndex = 335;
            this.label9.Text = "ФНЧ...................";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label10.Location = new System.Drawing.Point(206, 129);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(75, 13);
            this.label10.TabIndex = 334;
            this.label10.Text = "ФВЧ..............";
            // 
            // comboBoxBW
            // 
            this.comboBoxBW.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxBW.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.comboBoxBW.FormattingEnabled = true;
            this.comboBoxBW.IntegralHeight = false;
            this.comboBoxBW.Items.AddRange(new object[] {
            "0,5 кГц",
            "3,0 кГц",
            "6,0 кГц",
            "8,5 кГц",
            "16 кГц",
            "30 кГц",
            "100 кГц",
            "200 кГц",
            "300 кГц"});
            this.comboBoxBW.Location = new System.Drawing.Point(254, 99);
            this.comboBoxBW.Name = "comboBoxBW";
            this.comboBoxBW.Size = new System.Drawing.Size(73, 21);
            this.comboBoxBW.TabIndex = 338;
            this.comboBoxBW.SelectedIndexChanged += new System.EventHandler(this.comboBoxBW_SelectedIndexChanged);
            // 
            // b0
            // 
            this.b0.Enabled = false;
            this.b0.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.b0.Location = new System.Drawing.Point(16, 152);
            this.b0.Name = "b0";
            this.b0.Size = new System.Drawing.Size(19, 21);
            this.b0.TabIndex = 351;
            this.b0.Tag = "OFF";
            this.b0.Text = "0";
            this.b0.UseVisualStyleBackColor = true;
            this.b0.Click += new System.EventHandler(this.b0_Click);
            // 
            // comboBoxAtt
            // 
            this.comboBoxAtt.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxAtt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.comboBoxAtt.FormattingEnabled = true;
            this.comboBoxAtt.Items.AddRange(new object[] {
            "0 дБ",
            "10 дБ",
            "20 дБ",
            "Авто"});
            this.comboBoxAtt.Location = new System.Drawing.Point(254, 53);
            this.comboBoxAtt.Name = "comboBoxAtt";
            this.comboBoxAtt.Size = new System.Drawing.Size(73, 21);
            this.comboBoxAtt.TabIndex = 337;
            this.comboBoxAtt.SelectedIndexChanged += new System.EventHandler(this.comboBoxAtt_SelectedIndexChanged);
            // 
            // b4
            // 
            this.b4.Enabled = false;
            this.b4.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.b4.Location = new System.Drawing.Point(16, 112);
            this.b4.Name = "b4";
            this.b4.Size = new System.Drawing.Size(19, 21);
            this.b4.TabIndex = 344;
            this.b4.Tag = "OFF";
            this.b4.Text = "4";
            this.b4.UseVisualStyleBackColor = true;
            this.b4.Click += new System.EventHandler(this.b4_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label11.Location = new System.Drawing.Point(206, 107);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(69, 13);
            this.label11.TabIndex = 333;
            this.label11.Text = "Полоса........";
            // 
            // b5
            // 
            this.b5.Enabled = false;
            this.b5.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.b5.Location = new System.Drawing.Point(38, 112);
            this.b5.Name = "b5";
            this.b5.Size = new System.Drawing.Size(19, 21);
            this.b5.TabIndex = 345;
            this.b5.Tag = "OFF";
            this.b5.Text = "5";
            this.b5.UseVisualStyleBackColor = true;
            this.b5.Click += new System.EventHandler(this.b5_Click);
            // 
            // b2
            // 
            this.b2.Enabled = false;
            this.b2.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.b2.Location = new System.Drawing.Point(38, 92);
            this.b2.Name = "b2";
            this.b2.Size = new System.Drawing.Size(19, 21);
            this.b2.TabIndex = 342;
            this.b2.Tag = "OFF";
            this.b2.Text = "2";
            this.b2.UseVisualStyleBackColor = true;
            this.b2.Click += new System.EventHandler(this.b2_Click);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label16.Location = new System.Drawing.Point(205, 60);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(72, 13);
            this.label16.TabIndex = 332;
            this.label16.Text = "Атт................";
            // 
            // b3
            // 
            this.b3.Enabled = false;
            this.b3.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.b3.Location = new System.Drawing.Point(60, 92);
            this.b3.Name = "b3";
            this.b3.Size = new System.Drawing.Size(19, 21);
            this.b3.TabIndex = 343;
            this.b3.Tag = "OFF";
            this.b3.Text = "3";
            this.b3.UseVisualStyleBackColor = true;
            this.b3.Click += new System.EventHandler(this.b3_Click);
            // 
            // bESC
            // 
            this.bESC.Enabled = false;
            this.bESC.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.bESC.Location = new System.Drawing.Point(38, 152);
            this.bESC.Name = "bESC";
            this.bESC.Size = new System.Drawing.Size(41, 21);
            this.bESC.TabIndex = 350;
            this.bESC.Tag = "OFF";
            this.bESC.Text = "ESC";
            this.bESC.UseVisualStyleBackColor = true;
            this.bESC.Click += new System.EventHandler(this.bESC_Click);
            // 
            // bENT
            // 
            this.bENT.Enabled = false;
            this.bENT.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F);
            this.bENT.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.bENT.Location = new System.Drawing.Point(38, 172);
            this.bENT.Name = "bENT";
            this.bENT.Size = new System.Drawing.Size(41, 21);
            this.bENT.TabIndex = 352;
            this.bENT.Tag = "OFF";
            this.bENT.Text = "ENT";
            this.bENT.UseVisualStyleBackColor = true;
            this.bENT.Click += new System.EventHandler(this.bENT_Click);
            // 
            // b7
            // 
            this.b7.Enabled = false;
            this.b7.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.b7.Location = new System.Drawing.Point(16, 132);
            this.b7.Name = "b7";
            this.b7.Size = new System.Drawing.Size(19, 21);
            this.b7.TabIndex = 347;
            this.b7.Tag = "OFF";
            this.b7.Text = "7";
            this.b7.UseVisualStyleBackColor = true;
            this.b7.Click += new System.EventHandler(this.b7_Click);
            // 
            // b6
            // 
            this.b6.Enabled = false;
            this.b6.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.b6.Location = new System.Drawing.Point(60, 112);
            this.b6.Name = "b6";
            this.b6.Size = new System.Drawing.Size(19, 21);
            this.b6.TabIndex = 346;
            this.b6.Tag = "OFF";
            this.b6.Text = "6";
            this.b6.UseVisualStyleBackColor = true;
            this.b6.Click += new System.EventHandler(this.b6_Click);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label17.Location = new System.Drawing.Point(206, 83);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(78, 13);
            this.label17.TabIndex = 331;
            this.label17.Text = "Режим............";
            // 
            // b1
            // 
            this.b1.Enabled = false;
            this.b1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.b1.Location = new System.Drawing.Point(16, 92);
            this.b1.Name = "b1";
            this.b1.Size = new System.Drawing.Size(19, 21);
            this.b1.TabIndex = 341;
            this.b1.Tag = "OFF";
            this.b1.Text = "1";
            this.b1.UseVisualStyleBackColor = true;
            this.b1.Click += new System.EventHandler(this.b1_Click);
            // 
            // b9
            // 
            this.b9.Enabled = false;
            this.b9.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.b9.Location = new System.Drawing.Point(60, 132);
            this.b9.Name = "b9";
            this.b9.Size = new System.Drawing.Size(19, 21);
            this.b9.TabIndex = 349;
            this.b9.Tag = "OFF";
            this.b9.Text = "9";
            this.b9.UseVisualStyleBackColor = true;
            this.b9.Click += new System.EventHandler(this.b9_Click);
            // 
            // b8
            // 
            this.b8.Enabled = false;
            this.b8.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.b8.Location = new System.Drawing.Point(38, 132);
            this.b8.Name = "b8";
            this.b8.Size = new System.Drawing.Size(19, 21);
            this.b8.TabIndex = 348;
            this.b8.Tag = "OFF";
            this.b8.Text = "8";
            this.b8.UseVisualStyleBackColor = true;
            this.b8.Click += new System.EventHandler(this.b8_Click);
            // 
            // groupBoxAmpl
            // 
            this.groupBoxAmpl.Controls.Add(this.cbAmpl);
            this.groupBoxAmpl.Controls.Add(this.label13);
            this.groupBoxAmpl.Controls.Add(this.trackBarManualGain);
            this.groupBoxAmpl.Location = new System.Drawing.Point(331, 55);
            this.groupBoxAmpl.Name = "groupBoxAmpl";
            this.groupBoxAmpl.Size = new System.Drawing.Size(122, 110);
            this.groupBoxAmpl.TabIndex = 354;
            this.groupBoxAmpl.TabStop = false;
            this.groupBoxAmpl.Text = "Усиление";
            // 
            // cbAmpl
            // 
            this.cbAmpl.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbAmpl.FormattingEnabled = true;
            this.cbAmpl.Items.AddRange(new object[] {
            "Выключено",
            "Включено",
            "Авто"});
            this.cbAmpl.Location = new System.Drawing.Point(11, 20);
            this.cbAmpl.Name = "cbAmpl";
            this.cbAmpl.Size = new System.Drawing.Size(96, 21);
            this.cbAmpl.TabIndex = 255;
            this.cbAmpl.SelectedIndexChanged += new System.EventHandler(this.cbAmpl_SelectedIndexChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label13.Location = new System.Drawing.Point(10, 53);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(42, 13);
            this.label13.TabIndex = 234;
            this.label13.Text = "Ручное";
            // 
            // trackBarManualGain
            // 
            this.trackBarManualGain.AutoSize = false;
            this.trackBarManualGain.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.trackBarManualGain.Location = new System.Drawing.Point(8, 74);
            this.trackBarManualGain.Maximum = 255;
            this.trackBarManualGain.Name = "trackBarManualGain";
            this.trackBarManualGain.Size = new System.Drawing.Size(110, 18);
            this.trackBarManualGain.TabIndex = 214;
            this.trackBarManualGain.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trackBarManualGain.Value = 25;
            this.trackBarManualGain.Scroll += new System.EventHandler(this.trackBarManualGain_Scroll);
            // 
            // bDot
            // 
            this.bDot.Enabled = false;
            this.bDot.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.bDot.Location = new System.Drawing.Point(16, 172);
            this.bDot.Name = "bDot";
            this.bDot.Size = new System.Drawing.Size(19, 21);
            this.bDot.TabIndex = 353;
            this.bDot.Tag = "OFF";
            this.bDot.Text = ".";
            this.bDot.UseVisualStyleBackColor = true;
            this.bDot.Click += new System.EventHandler(this.bDot_Click);
            // 
            // pScreen
            // 
            this.pScreen.BackColor = System.Drawing.Color.Black;
            this.pScreen.Controls.Add(this.lFRQFromAOR);
            this.pScreen.Controls.Add(this.lFrq);
            this.pScreen.Controls.Add(this.lBw);
            this.pScreen.Controls.Add(this.lat);
            this.pScreen.Controls.Add(this.lMode);
            this.pScreen.Controls.Add(this.lLevel);
            this.pScreen.Controls.Add(this.lAmplifier);
            this.pScreen.Enabled = false;
            this.pScreen.Location = new System.Drawing.Point(16, 47);
            this.pScreen.Name = "pScreen";
            this.pScreen.Size = new System.Drawing.Size(183, 44);
            this.pScreen.TabIndex = 330;
            // 
            // lFRQFromAOR
            // 
            this.lFRQFromAOR.AutoSize = true;
            this.lFRQFromAOR.BackColor = System.Drawing.Color.Black;
            this.lFRQFromAOR.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.lFRQFromAOR.ForeColor = System.Drawing.Color.Green;
            this.lFRQFromAOR.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lFRQFromAOR.Location = new System.Drawing.Point(6, 20);
            this.lFRQFromAOR.Name = "lFRQFromAOR";
            this.lFRQFromAOR.Size = new System.Drawing.Size(0, 20);
            this.lFRQFromAOR.TabIndex = 235;
            // 
            // lFrq
            // 
            this.lFrq.AutoSize = true;
            this.lFrq.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lFrq.ForeColor = System.Drawing.Color.Green;
            this.lFrq.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lFrq.Location = new System.Drawing.Point(9, 25);
            this.lFrq.Name = "lFrq";
            this.lFrq.Size = new System.Drawing.Size(0, 17);
            this.lFrq.TabIndex = 217;
            // 
            // lBw
            // 
            this.lBw.AutoSize = true;
            this.lBw.ForeColor = System.Drawing.Color.Green;
            this.lBw.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lBw.Location = new System.Drawing.Point(87, 3);
            this.lBw.Name = "lBw";
            this.lBw.Size = new System.Drawing.Size(25, 13);
            this.lBw.TabIndex = 128;
            this.lBw.Text = "BW";
            // 
            // lat
            // 
            this.lat.AutoSize = true;
            this.lat.ForeColor = System.Drawing.Color.Green;
            this.lat.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lat.Location = new System.Drawing.Point(35, 3);
            this.lat.Name = "lat";
            this.lat.Size = new System.Drawing.Size(19, 13);
            this.lat.TabIndex = 127;
            this.lat.Text = "att";
            // 
            // lMode
            // 
            this.lMode.AutoSize = true;
            this.lMode.ForeColor = System.Drawing.Color.Green;
            this.lMode.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lMode.Location = new System.Drawing.Point(1, 4);
            this.lMode.Name = "lMode";
            this.lMode.Size = new System.Drawing.Size(24, 13);
            this.lMode.TabIndex = 123;
            this.lMode.Text = "MD";
            // 
            // lLevel
            // 
            this.lLevel.AutoSize = true;
            this.lLevel.ForeColor = System.Drawing.Color.Green;
            this.lLevel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lLevel.Location = new System.Drawing.Point(132, 3);
            this.lLevel.Name = "lLevel";
            this.lLevel.Size = new System.Drawing.Size(28, 13);
            this.lLevel.TabIndex = 125;
            this.lLevel.Text = "dBm";
            // 
            // lAmplifier
            // 
            this.lAmplifier.AutoSize = true;
            this.lAmplifier.ForeColor = System.Drawing.Color.Green;
            this.lAmplifier.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lAmplifier.Location = new System.Drawing.Point(3, 27);
            this.lAmplifier.Name = "lAmplifier";
            this.lAmplifier.Size = new System.Drawing.Size(0, 13);
            this.lAmplifier.TabIndex = 124;
            this.lAmplifier.Visible = false;
            // 
            // groupBoxRegime
            // 
            this.groupBoxRegime.Controls.Add(this.radioButton1);
            this.groupBoxRegime.Controls.Add(this.radioButton2);
            this.groupBoxRegime.Location = new System.Drawing.Point(15, 14);
            this.groupBoxRegime.Name = "groupBoxRegime";
            this.groupBoxRegime.Size = new System.Drawing.Size(313, 30);
            this.groupBoxRegime.TabIndex = 324;
            this.groupBoxRegime.TabStop = false;
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Checked = true;
            this.radioButton1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.radioButton1.Location = new System.Drawing.Point(5, 9);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(60, 17);
            this.radioButton1.TabIndex = 237;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "Ручной";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.radioButton2.Location = new System.Drawing.Point(186, 9);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(124, 17);
            this.radioButton2.TabIndex = 238;
            this.radioButton2.Text = "Сканирование РПУ";
            this.radioButton2.UseVisualStyleBackColor = true;
            this.radioButton2.CheckedChanged += new System.EventHandler(this.radioButton2_CheckedChanged);
            // 
            // tabControl1
            // 
            this.tabControl1.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
            this.tabControl1.Controls.Add(this.tpIRI_KRPU);
            this.tabControl1.Controls.Add(this.tpPlayer);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tabControl1.Location = new System.Drawing.Point(0, 477);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(491, 297);
            this.tabControl1.TabIndex = 365;
            this.tabControl1.Selecting += new System.Windows.Forms.TabControlCancelEventHandler(this.tabControl1_Selecting);
            // 
            // tpIRI_KRPU
            // 
            this.tpIRI_KRPU.Controls.Add(this.bAddToTableFromAOR);
            this.tpIRI_KRPU.Controls.Add(this.nudTableFRQ);
            this.tpIRI_KRPU.Controls.Add(this.dgv);
            this.tpIRI_KRPU.Controls.Add(this.tbNote);
            this.tpIRI_KRPU.Controls.Add(this.bTableChange);
            this.tpIRI_KRPU.Controls.Add(this.nudPause);
            this.tpIRI_KRPU.Controls.Add(this.bTableAdd);
            this.tpIRI_KRPU.Controls.Add(this.label18);
            this.tpIRI_KRPU.Controls.Add(this.bTableDel);
            this.tpIRI_KRPU.Controls.Add(this.cbTablePriority);
            this.tpIRI_KRPU.Controls.Add(this.bTableClear);
            this.tpIRI_KRPU.Controls.Add(this.nudTablePorogObnar);
            this.tpIRI_KRPU.Controls.Add(this.label8);
            this.tpIRI_KRPU.Controls.Add(this.label12);
            this.tpIRI_KRPU.Controls.Add(this.label19);
            this.tpIRI_KRPU.Controls.Add(this.label14);
            this.tpIRI_KRPU.Location = new System.Drawing.Point(4, 25);
            this.tpIRI_KRPU.Name = "tpIRI_KRPU";
            this.tpIRI_KRPU.Padding = new System.Windows.Forms.Padding(3);
            this.tpIRI_KRPU.Size = new System.Drawing.Size(483, 268);
            this.tpIRI_KRPU.TabIndex = 0;
            this.tpIRI_KRPU.Text = "Таблица ИРИ КРПУ";
            this.tpIRI_KRPU.UseVisualStyleBackColor = true;
            // 
            // bAddToTableFromAOR
            // 
            this.bAddToTableFromAOR.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.bAddToTableFromAOR.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.bAddToTableFromAOR.Location = new System.Drawing.Point(345, 238);
            this.bAddToTableFromAOR.Name = "bAddToTableFromAOR";
            this.bAddToTableFromAOR.Size = new System.Drawing.Size(133, 23);
            this.bAddToTableFromAOR.TabIndex = 324;
            this.bAddToTableFromAOR.Text = "Добавить текущую";
            this.bAddToTableFromAOR.UseVisualStyleBackColor = true;
            this.bAddToTableFromAOR.Click += new System.EventHandler(this.bAddToTableFromAOR_Click);
            // 
            // nudTableFRQ
            // 
            this.nudTableFRQ.DecimalPlaces = 2;
            this.nudTableFRQ.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.nudTableFRQ.Location = new System.Drawing.Point(123, 163);
            this.nudTableFRQ.Maximum = new decimal(new int[] {
            3300000,
            0,
            0,
            0});
            this.nudTableFRQ.Minimum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.nudTableFRQ.Name = "nudTableFRQ";
            this.nudTableFRQ.Size = new System.Drawing.Size(95, 20);
            this.nudTableFRQ.TabIndex = 275;
            this.nudTableFRQ.ThousandsSeparator = true;
            this.nudTableFRQ.Value = new decimal(new int[] {
            99500,
            0,
            0,
            0});
            this.nudTableFRQ.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.nudTableFRQ_KeyPress);
            this.nudTableFRQ.KeyUp += new System.Windows.Forms.KeyEventHandler(this.nudTableFRQ_KeyUp);
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Number,
            this.OnOff,
            this.Fequency,
            this.Pause,
            this.Treshold,
            this.TimeFirst,
            this.Amplifier,
            this.Att,
            this.Mode,
            this.BW,
            this.HPF,
            this.LPF,
            this.Priority,
            this.Note});
            this.dgv.Location = new System.Drawing.Point(-1, 3);
            this.dgv.MultiSelect = false;
            this.dgv.Name = "dgv";
            this.dgv.RowHeadersVisible = false;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv.Size = new System.Drawing.Size(479, 154);
            this.dgv.TabIndex = 265;
            this.dgv.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_CellClick);
            this.dgv.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_CellContentClick);
            this.dgv.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_CellDoubleClick);
            this.dgv.Resize += new System.EventHandler(this.dgv_Resize);
            // 
            // Number
            // 
            this.Number.HeaderText = "№";
            this.Number.Name = "Number";
            this.Number.ReadOnly = true;
            this.Number.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Number.Width = 30;
            // 
            // OnOff
            // 
            this.OnOff.HeaderText = "ВКЛ";
            this.OnOff.Name = "OnOff";
            this.OnOff.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.OnOff.Width = 35;
            // 
            // Fequency
            // 
            this.Fequency.HeaderText = "Частота, кГц";
            this.Fequency.MinimumWidth = 50;
            this.Fequency.Name = "Fequency";
            this.Fequency.ReadOnly = true;
            this.Fequency.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Fequency.Width = 70;
            // 
            // Pause
            // 
            this.Pause.HeaderText = "Пауза, мс";
            this.Pause.Name = "Pause";
            this.Pause.ReadOnly = true;
            this.Pause.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Pause.Width = 50;
            // 
            // Treshold
            // 
            this.Treshold.HeaderText = "Порог, дБм";
            this.Treshold.Name = "Treshold";
            this.Treshold.ReadOnly = true;
            this.Treshold.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Treshold.Width = 45;
            // 
            // TimeFirst
            // 
            this.TimeFirst.HeaderText = "Время";
            this.TimeFirst.Name = "TimeFirst";
            this.TimeFirst.ReadOnly = true;
            this.TimeFirst.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.TimeFirst.Width = 55;
            // 
            // Amplifier
            // 
            this.Amplifier.HeaderText = "Усил";
            this.Amplifier.Name = "Amplifier";
            this.Amplifier.ReadOnly = true;
            this.Amplifier.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Amplifier.Visible = false;
            this.Amplifier.Width = 65;
            // 
            // Att
            // 
            this.Att.HeaderText = "Атт";
            this.Att.Name = "Att";
            this.Att.ReadOnly = true;
            this.Att.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Att.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Att.Visible = false;
            this.Att.Width = 35;
            // 
            // Mode
            // 
            this.Mode.HeaderText = "Режим";
            this.Mode.Name = "Mode";
            this.Mode.ReadOnly = true;
            this.Mode.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Mode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Mode.Width = 45;
            // 
            // BW
            // 
            this.BW.HeaderText = "Полоса";
            this.BW.Name = "BW";
            this.BW.ReadOnly = true;
            this.BW.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.BW.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.BW.Visible = false;
            this.BW.Width = 50;
            // 
            // HPF
            // 
            this.HPF.HeaderText = "ФВЧ";
            this.HPF.Name = "HPF";
            this.HPF.ReadOnly = true;
            this.HPF.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.HPF.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.HPF.Visible = false;
            this.HPF.Width = 45;
            // 
            // LPF
            // 
            this.LPF.HeaderText = "ФНЧ";
            this.LPF.Name = "LPF";
            this.LPF.ReadOnly = true;
            this.LPF.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.LPF.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.LPF.Visible = false;
            this.LPF.Width = 38;
            // 
            // Priority
            // 
            this.Priority.HeaderText = "Приор";
            this.Priority.Name = "Priority";
            this.Priority.ReadOnly = true;
            this.Priority.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Priority.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Priority.Width = 50;
            // 
            // Note
            // 
            this.Note.HeaderText = "Примечание";
            this.Note.Name = "Note";
            this.Note.ReadOnly = true;
            this.Note.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Note.Width = 70;
            // 
            // tbNote
            // 
            this.tbNote.Location = new System.Drawing.Point(123, 214);
            this.tbNote.Name = "tbNote";
            this.tbNote.Size = new System.Drawing.Size(353, 20);
            this.tbNote.TabIndex = 273;
            this.tbNote.Text = "Примечание";
            // 
            // bTableChange
            // 
            this.bTableChange.ForeColor = System.Drawing.Color.Green;
            this.bTableChange.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.bTableChange.Location = new System.Drawing.Point(90, 238);
            this.bTableChange.Name = "bTableChange";
            this.bTableChange.Size = new System.Drawing.Size(80, 23);
            this.bTableChange.TabIndex = 268;
            this.bTableChange.Tag = "OFF";
            this.bTableChange.Text = "Изменить";
            this.bTableChange.UseVisualStyleBackColor = true;
            this.bTableChange.Click += new System.EventHandler(this.bTableChange_Click);
            // 
            // nudPause
            // 
            this.nudPause.Increment = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.nudPause.Location = new System.Drawing.Point(381, 163);
            this.nudPause.Maximum = new decimal(new int[] {
            60000,
            0,
            0,
            0});
            this.nudPause.Minimum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.nudPause.Name = "nudPause";
            this.nudPause.Size = new System.Drawing.Size(95, 20);
            this.nudPause.TabIndex = 279;
            this.nudPause.Value = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.nudPause.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.nudPause_KeyPress);
            this.nudPause.Validated += new System.EventHandler(this.nudPause_Validated);
            // 
            // bTableAdd
            // 
            this.bTableAdd.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.bTableAdd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.bTableAdd.Location = new System.Drawing.Point(4, 238);
            this.bTableAdd.Name = "bTableAdd";
            this.bTableAdd.Size = new System.Drawing.Size(80, 23);
            this.bTableAdd.TabIndex = 266;
            this.bTableAdd.Tag = "OFF";
            this.bTableAdd.Text = "Добавить";
            this.bTableAdd.UseVisualStyleBackColor = true;
            this.bTableAdd.Click += new System.EventHandler(this.bTableAdd_Click);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label18.Location = new System.Drawing.Point(265, 169);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(133, 13);
            this.label18.TabIndex = 278;
            this.label18.Text = "Пауза, мс.........................";
            // 
            // bTableDel
            // 
            this.bTableDel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.bTableDel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.bTableDel.Location = new System.Drawing.Point(176, 238);
            this.bTableDel.Name = "bTableDel";
            this.bTableDel.Size = new System.Drawing.Size(80, 23);
            this.bTableDel.TabIndex = 267;
            this.bTableDel.Tag = "OFF";
            this.bTableDel.Text = "Удалить";
            this.bTableDel.UseVisualStyleBackColor = true;
            this.bTableDel.Click += new System.EventHandler(this.bTableDel_Click);
            // 
            // cbTablePriority
            // 
            this.cbTablePriority.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbTablePriority.FormattingEnabled = true;
            this.cbTablePriority.Items.AddRange(new object[] {
            "Обычный",
            "Важный"});
            this.cbTablePriority.Location = new System.Drawing.Point(381, 188);
            this.cbTablePriority.Name = "cbTablePriority";
            this.cbTablePriority.Size = new System.Drawing.Size(95, 21);
            this.cbTablePriority.TabIndex = 277;
            // 
            // bTableClear
            // 
            this.bTableClear.ForeColor = System.Drawing.Color.Red;
            this.bTableClear.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.bTableClear.Location = new System.Drawing.Point(261, 238);
            this.bTableClear.Name = "bTableClear";
            this.bTableClear.Size = new System.Drawing.Size(80, 23);
            this.bTableClear.TabIndex = 269;
            this.bTableClear.Tag = "OFF";
            this.bTableClear.Text = "Очистить";
            this.bTableClear.UseVisualStyleBackColor = true;
            this.bTableClear.Click += new System.EventHandler(this.bTableClear_Click);
            // 
            // nudTablePorogObnar
            // 
            this.nudTablePorogObnar.Location = new System.Drawing.Point(123, 188);
            this.nudTablePorogObnar.Maximum = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.nudTablePorogObnar.Minimum = new decimal(new int[] {
            120,
            0,
            0,
            -2147483648});
            this.nudTablePorogObnar.Name = "nudTablePorogObnar";
            this.nudTablePorogObnar.Size = new System.Drawing.Size(95, 20);
            this.nudTablePorogObnar.TabIndex = 276;
            this.nudTablePorogObnar.Value = new decimal(new int[] {
            110,
            0,
            0,
            -2147483648});
            this.nudTablePorogObnar.ValueChanged += new System.EventHandler(this.nudTablePorogObnar_ValueChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label8.Location = new System.Drawing.Point(5, 169);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(133, 13);
            this.label8.TabIndex = 270;
            this.label8.Text = "Частота, кГц....................";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label12.Location = new System.Drawing.Point(5, 194);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(131, 13);
            this.label12.TabIndex = 271;
            this.label12.Text = "Порог, дБм......................";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label19.Location = new System.Drawing.Point(5, 220);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(151, 13);
            this.label19.TabIndex = 274;
            this.label19.Text = "Примечание...........................";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label14.Location = new System.Drawing.Point(265, 194);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(133, 13);
            this.label14.TabIndex = 272;
            this.label14.Text = "Приоритет........................";
            // 
            // tpPlayer
            // 
            this.tpPlayer.Controls.Add(this.bStop);
            this.tpPlayer.Controls.Add(this.bPause);
            this.tpPlayer.Controls.Add(this.cbMute);
            this.tpPlayer.Controls.Add(this.bWavPlayer);
            this.tpPlayer.Controls.Add(this.listBox2);
            this.tpPlayer.Controls.Add(this.listBox1);
            this.tpPlayer.Controls.Add(this.pictureBox18);
            this.tpPlayer.Location = new System.Drawing.Point(4, 25);
            this.tpPlayer.Name = "tpPlayer";
            this.tpPlayer.Padding = new System.Windows.Forms.Padding(3);
            this.tpPlayer.Size = new System.Drawing.Size(483, 268);
            this.tpPlayer.TabIndex = 1;
            this.tpPlayer.Text = "Проигрыватель";
            this.tpPlayer.UseVisualStyleBackColor = true;
            // 
            // bStop
            // 
            this.bStop.Image = global::ControlARONE.Properties.Resources.Stop1;
            this.bStop.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.bStop.Location = new System.Drawing.Point(361, 102);
            this.bStop.Name = "bStop";
            this.bStop.Size = new System.Drawing.Size(24, 23);
            this.bStop.TabIndex = 23;
            this.bStop.UseVisualStyleBackColor = true;
            this.bStop.Click += new System.EventHandler(this.bStop_Click);
            // 
            // bPause
            // 
            this.bPause.Image = global::ControlARONE.Properties.Resources.Pause;
            this.bPause.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.bPause.Location = new System.Drawing.Point(337, 102);
            this.bPause.Name = "bPause";
            this.bPause.Size = new System.Drawing.Size(24, 23);
            this.bPause.TabIndex = 22;
            this.bPause.UseVisualStyleBackColor = true;
            this.bPause.Click += new System.EventHandler(this.bPause_Click);
            // 
            // cbMute
            // 
            this.cbMute.AutoSize = true;
            this.cbMute.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.cbMute.Location = new System.Drawing.Point(361, 239);
            this.cbMute.Name = "cbMute";
            this.cbMute.Size = new System.Drawing.Size(113, 17);
            this.cbMute.TabIndex = 15;
            this.cbMute.Text = "Откл. звук КРПУ";
            this.cbMute.UseVisualStyleBackColor = true;
            this.cbMute.CheckedChanged += new System.EventHandler(this.cbMute_CheckedChanged);
            // 
            // bWavPlayer
            // 
            this.bWavPlayer.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.bWavPlayer.Location = new System.Drawing.Point(385, 102);
            this.bWavPlayer.Name = "bWavPlayer";
            this.bWavPlayer.Size = new System.Drawing.Size(96, 23);
            this.bWavPlayer.TabIndex = 14;
            this.bWavPlayer.Text = "Проигрыватель";
            this.bWavPlayer.UseVisualStyleBackColor = true;
            this.bWavPlayer.Click += new System.EventHandler(this.bWavPlayer_Click);
            // 
            // listBox2
            // 
            this.listBox2.FormattingEnabled = true;
            this.listBox2.Location = new System.Drawing.Point(169, 5);
            this.listBox2.Name = "listBox2";
            this.listBox2.Size = new System.Drawing.Size(167, 251);
            this.listBox2.TabIndex = 12;
            this.listBox2.DoubleClick += new System.EventHandler(this.listBox2_DoubleClick);
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(5, 5);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(159, 251);
            this.listBox1.TabIndex = 11;
            this.listBox1.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
            // 
            // pictureBox18
            // 
            this.pictureBox18.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox18.Image = global::ControlARONE.Properties.Resources.wavPlayerGray;
            this.pictureBox18.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.pictureBox18.Location = new System.Drawing.Point(341, 5);
            this.pictureBox18.Name = "pictureBox18";
            this.pictureBox18.Size = new System.Drawing.Size(135, 86);
            this.pictureBox18.TabIndex = 10;
            this.pictureBox18.TabStop = false;
            // 
            // tm500
            // 
            this.tm500.Enabled = true;
            this.tm500.Tick += new System.EventHandler(this.tm500_Tick);
            // 
            // tmFlashFRQ
            // 
            this.tmFlashFRQ.Interval = 400;
            this.tmFlashFRQ.Tick += new System.EventHandler(this.tmFlashFRQ_Tick);
            // 
            // tmBaraban
            // 
            this.tmBaraban.Tick += new System.EventHandler(this.tmBaraban_Tick);
            // 
            // bAOR
            // 
            this.bAOR.BackColor = System.Drawing.Color.Red;
            this.bAOR.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.bAOR.Location = new System.Drawing.Point(410, 20);
            this.bAOR.Name = "bAOR";
            this.bAOR.Size = new System.Drawing.Size(22, 23);
            this.bAOR.TabIndex = 366;
            this.bAOR.UseVisualStyleBackColor = false;
            this.bAOR.Visible = false;
            this.bAOR.Click += new System.EventHandler(this.bAOR_Click);
            // 
            // cbARec
            // 
            this.cbARec.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbARec.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.cbARec.Image = global::ControlARONE.Properties.Resources.a_rec_grey;
            this.cbARec.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.cbARec.Location = new System.Drawing.Point(6, 298);
            this.cbARec.Name = "cbARec";
            this.cbARec.Size = new System.Drawing.Size(30, 30);
            this.cbARec.TabIndex = 329;
            this.cbARec.UseVisualStyleBackColor = true;
            this.cbARec.CheckedChanged += new System.EventHandler(this.cbARec_CheckedChanged);
            // 
            // cbRec
            // 
            this.cbRec.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbRec.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.cbRec.Image = global::ControlARONE.Properties.Resources.rec_grey;
            this.cbRec.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.cbRec.Location = new System.Drawing.Point(6, 268);
            this.cbRec.Name = "cbRec";
            this.cbRec.Size = new System.Drawing.Size(30, 30);
            this.cbRec.TabIndex = 328;
            this.cbRec.UseVisualStyleBackColor = true;
            this.cbRec.CheckedChanged += new System.EventHandler(this.cbRec_CheckedChanged);
            // 
            // cbIntensityPaint
            // 
            this.cbIntensityPaint.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbIntensityPaint.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.cbIntensityPaint.Image = global::ControlARONE.Properties.Resources.play;
            this.cbIntensityPaint.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.cbIntensityPaint.Location = new System.Drawing.Point(5, 360);
            this.cbIntensityPaint.Name = "cbIntensityPaint";
            this.cbIntensityPaint.Size = new System.Drawing.Size(30, 30);
            this.cbIntensityPaint.TabIndex = 327;
            this.cbIntensityPaint.UseVisualStyleBackColor = true;
            this.cbIntensityPaint.CheckedChanged += new System.EventHandler(this.cbIntensityPaint_CheckedChanged);
            // 
            // cbACP
            // 
            this.cbACP.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbACP.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.cbACP.Checked = true;
            this.cbACP.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbACP.Image = global::ControlARONE.Properties.Resources.stop;
            this.cbACP.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.cbACP.Location = new System.Drawing.Point(6, 208);
            this.cbACP.Name = "cbACP";
            this.cbACP.Size = new System.Drawing.Size(30, 30);
            this.cbACP.TabIndex = 326;
            this.cbACP.UseVisualStyleBackColor = true;
            this.cbACP.CheckedChanged += new System.EventHandler(this.cbACP_CheckedChanged);
            // 
            // cbAverage
            // 
            this.cbAverage.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbAverage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.cbAverage.Image = global::ControlARONE.Properties.Resources.average_gray;
            this.cbAverage.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.cbAverage.Location = new System.Drawing.Point(6, 238);
            this.cbAverage.Name = "cbAverage";
            this.cbAverage.Size = new System.Drawing.Size(30, 30);
            this.cbAverage.TabIndex = 325;
            this.cbAverage.UseVisualStyleBackColor = true;
            this.cbAverage.CheckedChanged += new System.EventHandler(this.cbAverage_Click);
            // 
            // pictureBox13
            // 
            this.pictureBox13.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox13.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox13.Image")));
            this.pictureBox13.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.pictureBox13.Location = new System.Drawing.Point(438, 28);
            this.pictureBox13.Name = "pictureBox13";
            this.pictureBox13.Size = new System.Drawing.Size(15, 15);
            this.pictureBox13.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox13.TabIndex = 372;
            this.pictureBox13.TabStop = false;
            this.pictureBox13.Visible = false;
            // 
            // pictureBox14
            // 
            this.pictureBox14.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox14.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox14.Image")));
            this.pictureBox14.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.pictureBox14.Location = new System.Drawing.Point(455, 28);
            this.pictureBox14.Name = "pictureBox14";
            this.pictureBox14.Size = new System.Drawing.Size(15, 15);
            this.pictureBox14.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox14.TabIndex = 371;
            this.pictureBox14.TabStop = false;
            this.pictureBox14.Visible = false;
            // 
            // ControlARONE
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pictureBox13);
            this.Controls.Add(this.pictureBox14);
            this.Controls.Add(this.bAOR);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.numericUpDown1);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.cbFrqStep);
            this.Controls.Add(this.bFrqPlus);
            this.Controls.Add(this.bFrqMinus);
            this.Controls.Add(this.panel22);
            this.Controls.Add(this.lPel);
            this.Controls.Add(this.bIspPel);
            this.Controls.Add(this.comboBoxMode);
            this.Controls.Add(this.comboBoxHPF);
            this.Controls.Add(this.comboBoxLPF);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.comboBoxBW);
            this.Controls.Add(this.b0);
            this.Controls.Add(this.comboBoxAtt);
            this.Controls.Add(this.b4);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.b5);
            this.Controls.Add(this.b2);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.b3);
            this.Controls.Add(this.bESC);
            this.Controls.Add(this.bENT);
            this.Controls.Add(this.b7);
            this.Controls.Add(this.b6);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.b1);
            this.Controls.Add(this.b9);
            this.Controls.Add(this.b8);
            this.Controls.Add(this.groupBoxAmpl);
            this.Controls.Add(this.bDot);
            this.Controls.Add(this.pScreen);
            this.Controls.Add(this.groupBoxRegime);
            this.Controls.Add(this.cbARec);
            this.Controls.Add(this.cbRec);
            this.Controls.Add(this.cbIntensityPaint);
            this.Controls.Add(this.cbACP);
            this.Controls.Add(this.cbAverage);
            this.Name = "ControlARONE";
            this.Size = new System.Drawing.Size(491, 774);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.panel22.ResumeLayout(false);
            this.panel22.PerformLayout();
            this.panel23.ResumeLayout(false);
            this.panel23.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.waveformGraph1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbWaterflowMin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbWaterflowMax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intensityGraph1)).EndInit();
            this.groupBoxAmpl.ResumeLayout(false);
            this.groupBoxAmpl.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarManualGain)).EndInit();
            this.pScreen.ResumeLayout(false);
            this.pScreen.PerformLayout();
            this.groupBoxRegime.ResumeLayout(false);
            this.groupBoxRegime.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tpIRI_KRPU.ResumeLayout(false);
            this.tpIRI_KRPU.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudTableFRQ)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPause)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudTablePorogObnar)).EndInit();
            this.tpPlayer.ResumeLayout(false);
            this.tpPlayer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label22;
        public System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.ComboBox cbFrqStep;
        private System.Windows.Forms.Button bFrqPlus;
        private System.Windows.Forms.Button bFrqMinus;
        private System.Windows.Forms.Panel panel22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Panel panel23;
        public System.Windows.Forms.Label lREC;
        private System.Windows.Forms.Label label20;
        public NationalInstruments.UI.WindowsForms.WaveformGraph waveformGraph1;
        public NationalInstruments.UI.WaveformPlot PlotSignal;
        private NationalInstruments.UI.XAxis xAxis1;
        private NationalInstruments.UI.YAxis yAxis1;
        public NationalInstruments.UI.WaveformPlot PlotAverage;
        public NationalInstruments.UI.WaveformPlot MinPorogPlot;
        public NationalInstruments.UI.WaveformPlot MaxPorogPlot;
        public NationalInstruments.UI.WaveformPlot PlotPorogObn;
        public System.Windows.Forms.TrackBar tbWaterflowMin;
        public System.Windows.Forms.TrackBar tbWaterflowMax;
        public NationalInstruments.UI.WindowsForms.IntensityGraph intensityGraph1;
        private NationalInstruments.UI.ColorScale colorScale1;
        public NationalInstruments.UI.IntensityPlot intensityPlot1;
        public NationalInstruments.UI.IntensityXAxis intensityXAxis1;
        public NationalInstruments.UI.IntensityYAxis intensityYAxis1;
        public System.Windows.Forms.Label lPel;
        public System.Windows.Forms.Button bIspPel;
        public System.Windows.Forms.ComboBox comboBoxMode;
        private System.Windows.Forms.ComboBox comboBoxHPF;
        private System.Windows.Forms.ComboBox comboBoxLPF;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        public System.Windows.Forms.ComboBox comboBoxBW;
        private System.Windows.Forms.Button b0;
        private System.Windows.Forms.ComboBox comboBoxAtt;
        private System.Windows.Forms.Button b4;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button b5;
        private System.Windows.Forms.Button b2;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Button b3;
        private System.Windows.Forms.Button bESC;
        private System.Windows.Forms.Button bENT;
        private System.Windows.Forms.Button b7;
        private System.Windows.Forms.Button b6;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Button b1;
        private System.Windows.Forms.Button b9;
        private System.Windows.Forms.Button b8;
        private System.Windows.Forms.GroupBox groupBoxAmpl;
        public System.Windows.Forms.ComboBox cbAmpl;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TrackBar trackBarManualGain;
        private System.Windows.Forms.Button bDot;
        private System.Windows.Forms.Panel pScreen;
        private System.Windows.Forms.Label lFRQFromAOR;
        private System.Windows.Forms.Label lFrq;
        private System.Windows.Forms.Label lBw;
        private System.Windows.Forms.Label lat;
        private System.Windows.Forms.Label lMode;
        private System.Windows.Forms.Label lLevel;
        private System.Windows.Forms.Label lAmplifier;
        public System.Windows.Forms.GroupBox groupBoxRegime;
        public System.Windows.Forms.RadioButton radioButton1;
        public System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.CheckBox cbARec;
        private System.Windows.Forms.CheckBox cbRec;
        private System.Windows.Forms.CheckBox cbIntensityPaint;
        private System.Windows.Forms.CheckBox cbACP;
        private System.Windows.Forms.CheckBox cbAverage;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tpIRI_KRPU;
        private System.Windows.Forms.Button bAddToTableFromAOR;
        private System.Windows.Forms.NumericUpDown nudTableFRQ;
        public System.Windows.Forms.DataGridView dgv;
        private System.Windows.Forms.DataGridViewTextBoxColumn Number;
        private System.Windows.Forms.DataGridViewCheckBoxColumn OnOff;
        private System.Windows.Forms.DataGridViewTextBoxColumn Fequency;
        private System.Windows.Forms.DataGridViewTextBoxColumn Pause;
        private System.Windows.Forms.DataGridViewTextBoxColumn Treshold;
        private System.Windows.Forms.DataGridViewTextBoxColumn TimeFirst;
        private System.Windows.Forms.DataGridViewTextBoxColumn Amplifier;
        private System.Windows.Forms.DataGridViewTextBoxColumn Att;
        private System.Windows.Forms.DataGridViewTextBoxColumn Mode;
        private System.Windows.Forms.DataGridViewTextBoxColumn BW;
        private System.Windows.Forms.DataGridViewTextBoxColumn HPF;
        private System.Windows.Forms.DataGridViewTextBoxColumn LPF;
        private System.Windows.Forms.DataGridViewTextBoxColumn Priority;
        private System.Windows.Forms.DataGridViewTextBoxColumn Note;
        private System.Windows.Forms.TextBox tbNote;
        private System.Windows.Forms.Button bTableChange;
        private System.Windows.Forms.NumericUpDown nudPause;
        private System.Windows.Forms.Button bTableAdd;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Button bTableDel;
        private System.Windows.Forms.ComboBox cbTablePriority;
        private System.Windows.Forms.Button bTableClear;
        private System.Windows.Forms.NumericUpDown nudTablePorogObnar;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TabPage tpPlayer;
        private System.Windows.Forms.CheckBox cbMute;
        private System.Windows.Forms.Button bWavPlayer;
        private System.Windows.Forms.ListBox listBox2;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.PictureBox pictureBox18;
        public System.Windows.Forms.Button bAOR;
        public System.Windows.Forms.Timer tm500;
        private System.Windows.Forms.Timer tmFlashFRQ;
        public System.Windows.Forms.Timer tmBaraban;
        public System.Windows.Forms.PictureBox pictureBox13;
        public System.Windows.Forms.PictureBox pictureBox14;
        private System.Windows.Forms.Button bStop;
        private System.Windows.Forms.Button bPause;

    }
}
