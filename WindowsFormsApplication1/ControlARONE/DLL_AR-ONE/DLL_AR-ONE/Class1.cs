﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NAudio;
using NAudio.Wave;
using NAudio.FileFormats;
using NAudio.CoreAudioApi;
using System.IO;
using System.IO.Ports;
using MathNet.Numerics.IntegralTransforms;
using MathNet.Numerics.Signals;
using System.Threading;

    public class ClassLibrary_ARONE
    {
        public struct ARONE{
            public long frequency;
            public int Mode;
            public int Bandwidth;
            public int HighPassFilter;
            public int LowPassFilter;
            public int AutomaticGainControl;
            public int Attenuator;
            public int Amplifier;
            public int NoiseSquelchTreshold;
            public int LevelSquelchTreshold;
            public int AGC;
            public int AFGain;
            public int ManualGain;
            public int RFGain;
            public int IFGain;
            public int SquelchSelect;
            public int SignalLevel;
            public int AutoSignalLevel;
            public int AutoBackLit;
            public int BackLit_OnOff;
            public int BackLitDimmer;
            public int LCDContrast;
            public int BeepLevel;
            public int SpeakerSelect;
            public int ExternalSpeacker;
            public int DelayTime;
            public int FreeScan;
            public int SignalMeterDisplay;
            public int DuplexMode;
            public int[] DuplexFrequency;
            public int SignalLevelUnit_dBmV;
            public int SignalLevelUnit_dBm;
            public int[] PassFrequencyList; 
            public int SelectMemory_OnOff;
            public int SelectMemoryList;
            public int DEemphasis;
            public int BFOFreq;
        }
           public ARONE ArOne;
            public SerialPort port;
            private Thread thrRead;
            String strRead = "";

            public delegate void ByteEventHandler();
            public delegate void ByteEventHandler2(string cmd, int data);
            public delegate void ConnectEventHandler();
            public event ByteEventHandler OnReadByte;
            public event ByteEventHandler2 OnDecodedByte;
            public event ByteEventHandler OnWriteByte;
            public event ConnectEventHandler OnConnectPort;
            public event ConnectEventHandler OnDisconnectPort;
            public event ByteEventHandler OnDecodedBW;
            public event ByteEventHandler OnDecodedAGC;
            public event ByteEventHandler OnDecodedAFGain;
            public event ByteEventHandler OnDecodedAMpl;
            public event ByteEventHandler OnDecodedATT;
            public event ByteEventHandler OnDecodedAutoSignalLevel;
            public event ByteEventHandler OnDecodedError;
            public event ByteEventHandler OnDecodedHPF;
            public event ByteEventHandler OnDecodedLPF;
            public event ByteEventHandler OnDecodedIFGain;
            public event ByteEventHandler OnDecodedRFGain;
            public event ByteEventHandler OnDecodedLevelSquelchTreshold;
            public event ByteEventHandler OnDecodedManualGain;
            public event ByteEventHandler OnDecodedMode;
            public event ByteEventHandler OnDecodedNoiseSquelchTreshold;
            public event ByteEventHandler OnDecodedFrq;
            public event ByteEventHandler OnDecodedSignalLevelUnit_dBm;
            public event ByteEventHandler OnDecodedSignalLevelUnit_dBmV;
            public event ByteEventHandler OnDecodedSquelchSelect;
            public event ByteEventHandler OnDecodedSignalLevel;


            public bool SendToArone( string message)
            {
                try
                {
                    message += "\x0D\x0A";
                    port.WriteLine(message);
                    return true;
                }
                catch (Exception) { return false; }
            }
            public bool TurnON()
            {
                try
                {
                string message = "X\x0D\x0A";
                port.WriteLine(message);
                Thread.Sleep(40);
                return true;
                }
                catch (Exception) { return false; }
            }
            public bool TurnOFF()
            {
                try
                {
                string message = "QP\x0D\x0A";
                port.WriteLine(message);
                Thread.Sleep(40);
                return true;
                }
                catch (Exception) { return false; }
            }
            public bool FrequencySet(double FrqMhz)
            {
                try
                {
                    long FrqHz = (Int64)(FrqMhz * 1000000);
                    string frq = FrqHz.ToString().PadLeft(10,'0');
                    string message = "RF"+frq+"\x0D\x0A";
                    port.WriteLine(message);
                    Thread.Sleep(40);
                    return true;
                }
                catch (Exception) { return false; }
            }
            public bool FrequencyGet()
            {
                try
                {
                    port.WriteLine("RF\x0D\x0A");
                    Thread.Sleep(20);
                    return true;
                }
                catch (Exception) { return false; }
            }
            public bool FrequencySet( string freq_MHz)//MHz
            {
                try
                {
                    long frql;
                    double frqd;
                    if (!double.TryParse(freq_MHz, out frqd)) { frqd = 0; return false; }
                    frqd = 1000000 * frqd;
                    if ((frqd > 3300000000) || (frqd < 10000)) { return false; }
                    frql = (long)frqd;
                    freq_MHz = frql.ToString();
                    freq_MHz = freq_MHz.PadLeft(10, '0');
                    string message = "RF" + freq_MHz + "\x0D\x0A";
                    port.WriteLine(message);
                    Thread.Sleep(40);
                    return true;
                }
                catch (Exception) { return false; }
            }
            public bool ModeSet( int Mode)
            {
                try
                {
                    if ((Mode > 6) || (Mode < 0)) return false;
                    string message = "MD" + Mode.ToString() + "\x0D\x0A";
                    port.WriteLine(message);
                    Thread.Sleep(20);
                    return true;
                }
                catch (Exception) { return false; }
            }
            public bool ModeGet()
            {
                try
                {
                    port.WriteLine("MD\x0D\x0A");
                    Thread.Sleep(20);
                    return true;
                }
                catch (Exception) { return false; }
            }
            public bool BandWidthSet( int BW)
            {
                try
                {
                    if ((BW > 8) || (BW < 0)) return false;
                    string message = "BW" + BW.ToString() + "\x0D\x0A";
                    port.WriteLine(message);
                    Thread.Sleep(20);
                    return true;
                }
                catch (Exception) { return false; }
            }
            public bool BandwidthGet()
            {
                try
                {
                    port.WriteLine("BW\x0D\x0A");
                    Thread.Sleep(20);
                    return true;
                }
                catch (Exception) { return false; }
            }
            public bool HighPassFiltrSet( string HPF)
            {
                try
                {
                    string message;

                    //if ((HPF.ToString() != "0") || (HPF.ToString() != "1") || (HPF.ToString() != "2")
                    //    || (HPF.ToString() != "3") || (HPF.ToString() != "4")) { return false; }

                    if ((HPF.ToString() == "0") || (HPF.ToString() == "1") || (HPF.ToString()== "2")
                        || (HPF.ToString() == "3") || (HPF.ToString() == "4"))
                    {
                        if (HPF.ToString() != "4") { message = "HP" + HPF.ToString() + "\x0D\x0A"; }
                        else { message = "HP" + "F" + "\x0D\x0A"; }
                        port.WriteLine(message);
                        Thread.Sleep(20);
                        return true;
                    }
                    else { return false; }
                }
                catch (Exception) { return false; }
            }
            public bool HighPassFiltrGet()
            {
                try
                {
                    port.WriteLine("HP\x0D\x0A");
                    Thread.Sleep(20);
                    return true;
                }
                catch (Exception) { return false; }
            }
            public bool LowPassFiltrSet( string LPF)
            {
                try
                {
                    //if ((LPF.ToString() != "0") || (LPF.ToString() != "1") || (LPF.ToString() != "2")
                    //    || (LPF.ToString() != "3") || (LPF.ToString() != "4")) { return false; }
                    string message;// = "LP" + LPF.ToString() + "\x0D\x0A";
                    if ((LPF.ToString() == "0") || (LPF.ToString() == "1") || (LPF.ToString() == "2")
                        || (LPF.ToString() == "3") || (LPF.ToString() == "4"))
                    {
                        if (LPF.ToString() != "4") { message = "LP" + LPF.ToString() + "\x0D\x0A"; }
                        else { message = "LP" + "F" + "\x0D\x0A"; }

                        port.WriteLine(message);
                        Thread.Sleep(20);
                        return true;
                    }
                    else { return false; }
                }
                catch (Exception) { return false; }
            }
            public bool LowPassFiltrGet()
            {
                try
                {
                    port.WriteLine("LP\x0D\x0A");
                    Thread.Sleep(20);
                    return true;
                }
                catch (Exception) { return false; }
            }
            public bool AutomaticGainControlSet( int AGC)
            {
                try
                {
                    if ((AGC > 3) || (AGC < 0)) return false;
                    string message = "AC" + AGC.ToString() + "\x0D\x0A";
                    port.WriteLine(message);
                    Thread.Sleep(20);
                    return true;
                }
                catch (Exception) { return false; }
            }
            public bool AutomaticGainControlGet()
            {
                try
                {
                    port.WriteLine("AC\x0D\x0A");
                    Thread.Sleep(20);
                    return true;
                }
                catch (Exception) { return false; }
            }
            public bool DE_EmphasisSet( int DE_E)
            {
                try
                {
                    if ((DE_E > 5) || (DE_E < 0)) return false;
                    string message = "EN" + DE_E.ToString() + "\x0D\x0A";
                    port.WriteLine(message);
                    Thread.Sleep(20);
                    return true;
                }
                catch (Exception) { return false; }
            }
            public bool DE_EmphasisGet()
            {
                try
                {
                    port.WriteLine("EN\x0D\x0A");
                    Thread.Sleep(20);
                    return true;
                }
                catch (Exception) { return false; }
            }
            public bool AttenuatorSet( int Att)
            {
                try
                {
                    if ((Att > 3) || (Att < 0)) return false;
                    string message = "AT" + Att.ToString() + "\x0D\x0A";
                    port.WriteLine(message);
                    Thread.Sleep(20);
                    return true;
                }
                catch (Exception) { return false; }
            }
            public bool AttenuatorGet()
            {
                try
                {
                    port.WriteLine("AT\x0D\x0A");
                    Thread.Sleep(20);
                    return true;
                }
                catch (Exception) { return false; }
            }
            public bool AmplifierSet( int Ampl)
            {
                try
                {
                    if ((Ampl > 2) || (Ampl < 0)) return false;
                    string message = "AM" + Ampl.ToString() + "\x0D\x0A";
                    port.WriteLine(message);
                    Thread.Sleep(20);
                    return true;
                }
                catch (Exception) { return false; }
            }
            public bool AmplifierGet()
            {
                try
                {
                    port.WriteLine("AM\x0D\x0A");
                    Thread.Sleep(20);
                    return true;
                }
                catch (Exception) { return false; }
            }
            public bool NoiseSQSet( int NSQ)
            {
                try
                {
                    if ((NSQ > 255) || (NSQ < 0)) return false;
                    string message = "RQ" + NSQ.ToString().PadLeft(3,'0') + "\x0D\x0A";
                    port.WriteLine(message);
                    Thread.Sleep(20);
                    return true;
                }
                catch (Exception) { return false; }
            }
            public bool NoiseSQGet()
            {
                try
                {
                    port.WriteLine("RQ\x0D\x0A");
                    Thread.Sleep(20);
                    return true;
                }
                catch (Exception) { return false; }
            }
            public bool LevelSQSet( int LSQ)
            {
                try
                {
                    if ((LSQ > 255) || (LSQ < 0)) return false;
                    string message = "DB" + LSQ.ToString().PadLeft(3, '0') + "\x0D\x0A";
                    port.WriteLine(message);
                    Thread.Sleep(20);
                    return true;
                }
                catch (Exception) { return false; }
            }
            public bool LevelSQGet()
            {
                try
                {
                    port.WriteLine("DB\x0D\x0A");
                    Thread.Sleep(20);
                    return true;
                }
                catch (Exception) { return false; }
            }
            public bool AFGainSet( int AG)
            {
                try
                {
                    if ((AG > 255) || (AG < 0)) return false;
                    string message = "AG" + AG.ToString().PadLeft(3, '0') + "\x0D\x0A";
                    port.WriteLine(message);
                    Thread.Sleep(20);
                    return true;
                }
                catch (Exception) { return false; }
            }
            public bool AFGainGet()
            {
                try
                {
                    port.WriteLine("AG\x0D\x0A");
                    Thread.Sleep(20);
                    return true;
                }
                catch (Exception) { return false; }
            }
            public bool ManualGainSet( int MG, int AGC)//10.7MHz AGC
            {
                try
                {
                    if ((MG > 255) || (MG < 0)||(AGC!=0)) return false;
                    string message = "MG" + MG.ToString().PadLeft(3, '0') + "\x0D\x0A";
                    port.WriteLine(message);
                    Thread.Sleep(20);
                    return true;
                }
                catch (Exception) { return false; }
            }
            public bool ManualGainGet()
            {
                try
                {
                    port.WriteLine("MG\x0D\x0A");
                    Thread.Sleep(20);
                    return true;
                }
                catch (Exception) { return false; }
            }
            public bool RFGainSet(int RFG)
            {
                try
                {
                    if ((RFG > 255) || (RFG < 0) ) return false;
                    string message = "RG" + RFG.ToString().PadLeft(3, '0') + "\x0D\x0A";
                    port.WriteLine(message);
                    Thread.Sleep(20);
                    return true;
                }
                catch (Exception) { return false; }
            }
            public bool RFGainGet()
            {
                try
                {
                    port.WriteLine("RG\x0D\x0A");
                    Thread.Sleep(20);
                    return true;
                }
                catch (Exception) { return false; }
            }
            public bool IFGainSet( int IFG)
            {
                try
                {
                    if ((IFG > 255) || (IFG < 0)) return false;
                    string message = "IG" + IFG.ToString().PadLeft(3, '0') + "\x0D\x0A";
                    port.WriteLine(message);
                    Thread.Sleep(20);
                    return true;
                }
                catch (Exception) { return false; }
            }
            public bool IFGainGet()
            {
                try
                {
                    port.WriteLine("IG\x0D\x0A");
                    Thread.Sleep(20);
                    return true;
                }
                catch (Exception) { return false; }
            }
            public bool SquelchSelectSet( int SS)
            {
                try
                {
                    if ((SS !=0) || (SS !=1)) return false;
                    string message = "SQ" + SS.ToString().PadLeft(3, '0') + "\x0D\x0A";
                    port.WriteLine(message);
                    Thread.Sleep(20);
                    return true;
                }
                catch (Exception) { return false; }
            }
            public bool SquelchSelectGet()
            {
                try
                {
                    port.WriteLine("SQ\x0D\x0A");
                    Thread.Sleep(20);
                    return true;
                }
                catch (Exception) { return false; }
            }
            public bool SignalLevelGet()
            {
                try
                {
                    port.WriteLine("LM\x0D\x0A");
                    Thread.Sleep(20);
                    return true;
                }
                catch (Exception) { return false; }
            }
            public bool AutoSignalLevelSet( int ASL)
            {
                try
                {
                    if ((ASL > 1) || (ASL < 0)) return false;
                    string message = "LC" + ASL.ToString() + "\x0D\x0A";
                    port.WriteLine(message);
                    Thread.Sleep(20);
                    return true;
                }
                catch (Exception) { return false; }
            }
            public bool AutoSignalLevelGet()
            {
                try
                {
                    port.WriteLine("LC\x0D\x0A");
                    Thread.Sleep(20);
                    return true;
                }
                catch (Exception) { return false; }
            }
            public bool AutoBackLitSet( int ABS)
            {
                try
                {
                    if ((ABS > 2) || (ABS < 0)) return false;
                    string message = "LA" + ABS.ToString() + "\x0D\x0A";
                    port.WriteLine(message);
                    Thread.Sleep(20);
                    return true;
                }
                catch (Exception) { return false; }
            }
            public bool AutoBackLitGet()
            {
                try
                {
                    port.WriteLine("LA\x0D\x0A");
                    Thread.Sleep(20);
                    return true;
                }
                catch (Exception) { return false; }
            }
            public bool BackLitSet( int ABS, int AutoBackLit)
            {
                try
                {
                    if ((ABS > 2) || (ABS < 0) || (AutoBackLit!=2)) return false;
                    string message = "BL" + ABS.ToString() + "\x0D\x0A";
                    port.WriteLine(message);
                    Thread.Sleep(20);
                    return true;
                }
                catch (Exception) { return false; }
            }
            public bool BackLitGet()
            {
                try
                {
                    port.WriteLine("BL\x0D\x0A");
                    Thread.Sleep(20);
                    return true;
                }
                catch (Exception) { return false; }
            }
            public bool BackLitDimmerSet( int BD)
            {
                try
                {
                    if ((BD > 1) || (BD < 0)) return false;
                    string message = "LD" + BD.ToString() + "\x0D\x0A";
                    port.WriteLine(message);
                    Thread.Sleep(20);
                    return true;
                }
                catch (Exception) { return false; }
            }
            public bool BackLitDimmerGet()
            {
                try
                {
                    port.WriteLine("LD\x0D\x0A");
                    Thread.Sleep(20);
                    return true;
                }
                catch (Exception) { return false; }
            }
            public bool LCDContrastSet( int LCDC)
            {
                try
                {
                    if ((LCDC > 31) || (LCDC < 0)) return false;
                    string message = "LV" + LCDC.ToString() + "\x0D\x0A";
                    port.WriteLine(message);
                    Thread.Sleep(20);
                    return true;
                }
                catch (Exception) { return false; }
            }
            public bool LCDContrastGet()
            {
                try
                {
                    port.WriteLine("LV\x0D\x0A");
                    Thread.Sleep(20);
                    return true;
                }
                catch (Exception) { return false; }
            }
            public bool BeepLevelSet( int BLev)
            {
                try
                {
                    if ((BLev > 9) || (BLev < 0) ) return false;
                    string message = "BV" + BLev.ToString() + "\x0D\x0A";
                    port.WriteLine(message);
                    Thread.Sleep(20);
                    return true;
                }
                catch (Exception) { return false; }
            }
            public bool BeepLevelGet()
            {
                try
                {
                    port.WriteLine("BV\x0D\x0A");
                    Thread.Sleep(20);
                    return true;
                }
                catch (Exception) { return false; }
            }
            public bool SpeackerSelectSet( int Speacker)
            {
                try
                {
                    if ((Speacker > 3) || (Speacker < 0)) return false;
                    string message = "SO" + Speacker.ToString() + "\x0D\x0A";
                    port.WriteLine(message);
                    Thread.Sleep(20);
                    return true;
                }
                catch (Exception) { return false; }
            }
            public bool SpeackerSelectGet()
            {
                try
                {
                    port.WriteLine("SO\x0D\x0A");
                    Thread.Sleep(20);
                    return true;
                }
                catch (Exception) { return false; }
            }
            public bool ExternalSpeackerSet( int ExtSpk)
            {
                try
                {
                    if ((ExtSpk > 1) || (ExtSpk < 0)) return false;
                    string message = "PO" + ExtSpk.ToString() + "\x0D\x0A";
                    port.WriteLine(message);
                    Thread.Sleep(20);
                    return true;
                }
                catch (Exception) { return false; }
            }
            public bool ExternalSpeackerGet()
            {
                try
                {
                    port.WriteLine("PO\x0D\x0A");
                    Thread.Sleep(20);
                    return true;
                }
                catch (Exception) { return false; }
            }
            public bool DelayTime_ScanDelaySet( double DT)
            {
                try
                {
                    if ((DT > 9.9) || (DT < 0)) return false;
                    string DTStr = DT.ToString();
                    if (DTStr.Length >= 2) DTStr = DTStr.Substring(0, 3); else DTStr = DTStr + ".0";
                    string message = "DD" +DTStr + "\x0D\x0A";
                    port.WriteLine(message);
                    Thread.Sleep(20);
                    return true;
                }
                catch (Exception) { return false; }
            }
            public bool DelayTime_ScanDelayGet()
            {
                try
                {
                    port.WriteLine("DD\x0D\x0A");
                    Thread.Sleep(20);
                    return true;
                }
                catch (Exception) { return false; }
            }        
            public bool FreeScanSet( double FSTime)
            {
                try
                {
                    if ((FSTime > 9.9) || (FSTime < 0)) return false;
                    string DTStr = FSTime.ToString();
                    if (DTStr.Length >= 2) DTStr = DTStr.Substring(0, 3); else DTStr = DTStr + ".0";
                    string message = "SP" +DTStr + "\x0D\x0A";
                    port.WriteLine(message);
                    Thread.Sleep(20);
                    return true;
                }
                catch (Exception) { return false; }
            }
            public bool FreeScanGet()
            {
                try
                {
                    port.WriteLine("SP\x0D\x0A");
                    Thread.Sleep(20);
                    return true;
                }
                catch (Exception) { return false; }
            }
            public bool SignalMeterDisplaySet( int SMDMode)
            {
                try
                {
                    if (( SMDMode> 2) || ( SMDMode< 0) ) return false;
                    string message = "SF" + SMDMode.ToString() + "\x0D\x0A";
                    port.WriteLine(message);
                    Thread.Sleep(20);
                    return true;
                }
                catch (Exception) { return false; }
            }
            public bool SignalMeterDisplayGet()
            {
                try
                {
                    port.WriteLine("SF\x0D\x0A");
                    Thread.Sleep(20);
                    return true;
                }
                catch (Exception) { return false; }
            }
            public bool DuplexModeSet( int DM, string OfsetDirectionPlusOrMinus)
            {
                try
                {
                    if ((OfsetDirectionPlusOrMinus!="+")&&(OfsetDirectionPlusOrMinus!="-")) return false;
                    if (( DM> 47) || ( DM< 0)) return false;
                    string message = "OF" + DM.ToString() + "\x0D\x0A";
                    port.WriteLine(message);
                    Thread.Sleep(20);
                    return true;
                }
                catch (Exception) { return false; }
            }
            public bool DuplexModeGet()
            {
                try
                {
                    port.WriteLine("OF\x0D\x0A");
                    Thread.Sleep(20);
                    return true;
                }
                catch (Exception) { return false; }
            }
            public bool DuplexFrequencySet( int num, int DplFreqHz)
            {
                try
                {
                    if (( num>19) || (num < 1) || ( DplFreqHz<10001)|| ( DplFreqHz>999999999)) return false;
                    string DplFrqStr= DplFreqHz.ToString().PadLeft(10,'0');
                    if(DplFrqStr.Substring(8,2)!="00") return false;
                    string message = "OL" + num.ToString().PadLeft('0') + " " + DplFreqHz.ToString() + "\x0D\x0A";
                    port.WriteLine(message);
                    Thread.Sleep(20);
                    return true;
                }
                catch (Exception) { return false; }
            }
            public bool DuplexFrequencyGet()
            {
                try
                {
                    port.WriteLine("OL\x0D\x0A");
                    Thread.Sleep(20);
                    return true;
                }
                catch (Exception) { return false; }
            }
            public bool SignalLevelUnit_dBmV_Get()
            {
                try
                {
                    port.WriteLine("LU\x0D\x0A");
                    Thread.Sleep(20);
                    return true;
                }
                catch (Exception) { return false; }
            }
            public bool SignalLevelUnit_dBm_Get()
            {
                try
                {
                    port.WriteLine("LB\x0D\x0A");
                    Thread.Sleep(20);
                    return true;
                }
                catch (Exception) { return false; }
            }
            public bool SearchDataSettingSet_nowork( int BankNum )
            {return false;
                //try
                //{
                //    if (( > 2) || ( < 0)) return false;
                //    string message = "" + .ToString() + "\x0D\x0A";
                //    port.WriteLine(message);
                //    Thread.Sleep(20);
                //    return true;
                //}
                //catch (Exception) { return false; }
            }
            public bool SearchDataListGet()
            {
                try
                {
                    port.WriteLine("SR\x0D\x0A");
                    Thread.Sleep(20);
                    return true;
                }
                catch (Exception) { return false; }
            }
            public bool PassFrequencySet( int Frq)
            {
                try
                {
                    if ((Frq > 3300000000) || ( Frq< 30000)) return false;
                    string message = "PW" + Frq.ToString().PadLeft(10,'0') + "\x0D\x0A";
                    port.WriteLine(message);
                    Thread.Sleep(20);
                    return true;
                }
                catch (Exception) { return false; }
            }
            public bool PassFrequencyListGet( int BankNum)
            {
                try
                {if((BankNum>40)||(BankNum<1)) return false;
                    port.WriteLine("PR"+BankNum.ToString().PadLeft(2,'0')+"\x0D\x0A");
                    Thread.Sleep(20);
                    return true;
                }
                catch (Exception) { return false; }
            }
            public bool DeletePassFrequencySet( int BankNum, int PassChannel)
            {
                try
                {
                    if ((BankNum > 40) || (BankNum < 1)||(PassChannel>49)||(PassChannel<0)) return false;
                    string message = "PD" + BankNum.ToString().PadLeft(2,'0')+ PassChannel.ToString().PadLeft(2,'0') + "\x0D\x0A";
                    port.WriteLine(message);
                    Thread.Sleep(20);
                    return true;
                }
                catch (Exception) { return false; }
            }
            public bool DeletePassFrequencyAllChannelsInBankSet( int BankNum)
            {
                try
                {
                    if ((BankNum > 40) || (BankNum < 1)) return false;
                    string message = "PD" + BankNum.ToString().PadLeft(2,'0')+"%%\x0D\x0A";
                    port.WriteLine(message);
                    Thread.Sleep(20);
                    return true;
                }
                catch (Exception) { return false; }
            }
            public bool DeleteSearchDataWithPassFrequencySet(int BankNum)
            {
                try
                {
                    if ((BankNum > 40) || (BankNum < 1)) return false;
                    string message = "QS" + BankNum.ToString().PadLeft(2,'0') + "\x0D\x0A";
                    port.WriteLine(message);
                    Thread.Sleep(20);
                    return true;
                }
                catch (Exception) { return false; }
            }
            public bool DeleteSearchDataWithPassFrequencyAllBanksSet()
            {
                try
                {   string message = "QS%%\x0D\x0A";
                    port.WriteLine(message);
                    Thread.Sleep(20);
                    return true;
                }
                catch (Exception) { return false; }
            }
            public bool TransferCurrentSearchDataToVFOSet(  int NumVFO)
            {
                try
                {
                    if ((NumVFO > 9) || (NumVFO < 0)) return false;
                    string message = "SV" + NumVFO.ToString() + "\x0D\x0A";
                    port.WriteLine(message);
                    Thread.Sleep(20);
                    return true;
                }
                catch (Exception) { return false; }
            }
            public bool MemoryDataSettingSet(  int BankNum, int MemoryChannel)
            {
                try
                {
                    if ((BankNum > 9) || (BankNum < 0)||(MemoryChannel>99)||(MemoryChannel<0)) return false;
                    string message = "MX" + BankNum.ToString()+MemoryChannel.ToString().PadLeft(2,'0') + "\x0D\x0A";
                    port.WriteLine(message);
                    Thread.Sleep(20);
                    return true;
                }
                catch (Exception) { return false; }
            }
            public long FrqStrMhzToLongHz(string FreqMHz)
            {
                long frql;
                double frqd;
                if (!double.TryParse(FreqMHz, out frqd)) { return 0; }//MessageBox.Show("Введите частоту в диапазоне \nот 0,01 до 3 300  МГц"); }
                frqd = 1000000 * frqd;
                frql = (long)frqd;
                return frql;
            }
            public void OpenPort(string portName)
            {
                if (port == null)
                    port = new SerialPort();
                if (port.IsOpen)
                    ClosePort();
                try
                {
                    port.PortName = portName;
                    port.BaudRate = 19200;
                    port.Parity = Parity.None;
                    port.DataBits = 8;
                    port.StopBits = StopBits.Two;
                    port.RtsEnable = true;
                    port.DtrEnable = true;
                    port.ReceivedBytesThreshold = 1000;
                    port.Open();
                    if (thrRead != null)
                    {
                        thrRead.Abort();
                        thrRead.Join(500);
                        thrRead = null;
                    }
                    try
                    {
                        thrRead = new Thread(new ThreadStart(ReadExistingComPort));
                        thrRead.IsBackground = true;
                        thrRead.Start();
                    }
                    catch (System.Exception)                    {                    }
                    ConnectPort();
                }
                catch (System.Exception)
                {
                    DisconnectPort();
                }
            }

            protected virtual void ConnectPort()
            {
                if (OnConnectPort != null)
                {
                    OnConnectPort();//Raise the event
                }
            }
            protected virtual void DisconnectPort()
            {
                if (OnDisconnectPort != null)
                {
                    OnDisconnectPort();//Raise the event
                }
            }
            public void ClosePort()
            {
                try
                {
                    port.DiscardInBuffer();
                }
                catch (System.Exception)                {                }
                try
                {
                    port.DiscardOutBuffer();
                }
                catch (System.Exception)                {                }
                try{

                    port.Close();
                    if (thrRead != null)
                    {
                        thrRead.Abort();
                        thrRead.Join(500);
                        thrRead = null;
                    }
                    DisconnectPort();
                }
                catch (System.Exception)                {                }
            }
            // read byte array
            protected virtual void ReadByte(byte[] bByte)
            {
                if (OnReadByte != null)
                {
                    OnReadByte();//Raise the event
                }
            }
            // write byte array
            protected virtual void WriteByte(byte[] bByte)
            {
                if (OnWriteByte != null)
                {
                    OnWriteByte();//Raise the event
                }
            }
        

            private void ReadExistingComPort()
            {
                string comand=null;
                while (true)
                {
                    try
                    {
                        //comand = null;
                        //string s = port.ReadExisting();
                        comand = "";
                        string s = port.ReadExisting();
                        if (s != "")
                        {
                            comand = s.Substring(0, 2);
                            if (comand == "RF")
                            {
                                Int64.TryParse(s.Substring(3, 10), out ArOne.frequency);
                                //if (OnDecodedByte != null)
                                //{
                                //    OnDecodedByte(comand, ArOne.frequency);
                                //}

                                if (OnDecodedFrq != null)
                                {
                                    OnDecodedFrq();//Raise the event
                                }

                            }

                            switch (comand)
                            {
                                case "MD":
                                    int.TryParse(s.Substring(2, 1), out ArOne.Mode);
                                    if (OnDecodedMode != null)
                                    {
                                        OnDecodedMode();//Raise the event
                                    }
                                    break;
                                case "BW":
                                    int.TryParse(s.Substring(2, 1), out ArOne.Bandwidth);
                                    if (OnDecodedBW != null)
                                    {
                                        OnDecodedBW();
                                    }
                                    break;
                                case "HP":
                                    int.TryParse(s.Substring(2, 1), out ArOne.HighPassFilter);
                                    if (OnDecodedHPF != null)
                                    {
                                        OnDecodedHPF();
                                    }
                                    break;
                                case "LP":
                                    int.TryParse(s.Substring(2, 1), out ArOne.LowPassFilter);
                                    if (OnDecodedLPF != null)
                                    {
                                        OnDecodedLPF();
                                    }
                                    break;
                                case "AC":
                                    int.TryParse(s.Substring(2, 1), out ArOne.AGC);
                                    if (OnDecodedAGC != null)
                                    {
                                        OnDecodedAGC();
                                    }
                                    break;
                                case "EN":
                                    int.TryParse(s.Substring(2, 1), out ArOne.DEemphasis);
                                    if (OnDecodedByte != null)
                                    {
                                        OnDecodedByte(comand, ArOne.DEemphasis);
                                    }
                                    break;
                                case "AT":
                                    int.TryParse(s.Substring(2, 1), out ArOne.Attenuator);
                                    if (OnDecodedATT != null)
                                    {
                                        OnDecodedATT();
                                    }
                                    break;
                                case "AM":
                                    int.TryParse(s.Substring(2, 1), out ArOne.Amplifier);
                                    if (OnDecodedAMpl != null)
                                    {
                                        OnDecodedAMpl();
                                    }
                                    break;
                                case "BF":
                                    int.TryParse(s.Substring(2, 5), out ArOne.BFOFreq);
                                    if (OnDecodedByte != null)
                                    {
                                        OnDecodedByte(comand, ArOne.BFOFreq);
                                    }
                                    break;
                                case "RQ":
                                    int.TryParse(s.Substring(2, 3), out ArOne.NoiseSquelchTreshold);
                                    if (OnDecodedNoiseSquelchTreshold != null)
                                    {
                                        OnDecodedNoiseSquelchTreshold();
                                    }
                                    break;
                                case "DB":
                                    int.TryParse(s.Substring(2, 3), out ArOne.LevelSquelchTreshold);
                                    if (OnDecodedLevelSquelchTreshold != null)
                                    {
                                        OnDecodedLevelSquelchTreshold();
                                    }
                                    break;
                                case "AG":
                                    int.TryParse(s.Substring(2, 3), out ArOne.AFGain);
                                    if (OnDecodedAFGain != null)
                                    {
                                        OnDecodedAFGain();
                                    }
                                    break;
                                case "MG":
                                    int.TryParse(s.Substring(3, 4), out ArOne.ManualGain);
                                    if (OnDecodedManualGain != null)
                                    {
                                        OnDecodedManualGain();
                                    }
                                    break;
                                case "RG":
                                    int.TryParse(s.Substring(2, 3), out ArOne.ManualGain);
                                    if (OnDecodedByte != null)
                                        if (OnDecodedRFGain != null)
                                        {
                                            OnDecodedRFGain();
                                        } break;
                                case "IG":
                                    int.TryParse(s.Substring(2, 3), out ArOne.IFGain);
                                    if (OnDecodedIFGain != null)
                                    {
                                        OnDecodedIFGain();
                                    }
                                    break;
                                case "SQ":
                                    int.TryParse(s.Substring(2, 1), out ArOne.SquelchSelect);
                                    if (OnDecodedSquelchSelect != null)
                                    {
                                        OnDecodedSquelchSelect();
                                    }
                                    break;
                                case "LM":
                                    int.TryParse(s.Substring(2, 3), out ArOne.SignalLevel);
                                    if (OnDecodedSignalLevel != null)
                                    {
                                        OnDecodedSignalLevel();
                                    }
                                    break;
                                case "LC":
                                    int.TryParse(s.Substring(2, 1), out ArOne.AutoSignalLevel);
                                    if (OnDecodedAutoSignalLevel != null)
                                    {
                                        OnDecodedAutoSignalLevel();
                                    }
                                    break;
                                case "LA":
                                    int.TryParse(s.Substring(2, 1), out ArOne.AutoBackLit);
                                    if (OnDecodedByte != null)
                                    {
                                        OnDecodedByte(comand, ArOne.AutoBackLit);
                                    }
                                    break;
                                case "BL":
                                    int.TryParse(s.Substring(2, 1), out ArOne.BackLit_OnOff);
                                    if (OnDecodedByte != null)
                                    {
                                        OnDecodedByte(comand, ArOne.BackLit_OnOff);
                                    }
                                    break;
                                case "LD":
                                    int.TryParse(s.Substring(2, 1), out ArOne.BackLitDimmer);
                                    if (OnDecodedByte != null)
                                    {
                                        OnDecodedByte(comand, ArOne.BackLitDimmer);
                                    }
                                    break;
                                case "LV":
                                    int.TryParse(s.Substring(2, 2), out ArOne.LCDContrast);
                                    if (OnDecodedByte != null)
                                    {
                                        OnDecodedByte(comand, ArOne.LCDContrast);
                                    }
                                    break;
                                case "BV":
                                    int.TryParse(s.Substring(2, 1), out ArOne.BeepLevel);
                                    if (OnDecodedByte != null)
                                    {
                                        OnDecodedByte(comand, ArOne.BeepLevel);
                                    }
                                    break;
                                case "SO":
                                    int.TryParse(s.Substring(2, 1), out ArOne.SpeakerSelect);
                                    if (OnDecodedByte != null)
                                    {
                                        OnDecodedByte(comand, ArOne.SpeakerSelect);
                                    }
                                    break;
                                case "PO":
                                    int.TryParse(s.Substring(2, 1), out ArOne.ExternalSpeacker);
                                    if (OnDecodedByte != null)
                                    {
                                        OnDecodedByte(comand, ArOne.ExternalSpeacker);
                                    }
                                    break;
                                //case "DD": 
                                //int.TryParse(s.Substring(2, 3), out ArOne.DelayTime); break;
                                //if (OnDecodedByte != null)
                                //{
                                //    OnDecodedByte("Err ", ArOne.DelayTime);
                                //}
                                case "SP":
                                    int.TryParse(s.Substring(2, 3), out ArOne.FreeScan);
                                    if (OnDecodedByte != null)
                                    {
                                        OnDecodedByte(comand, ArOne.FreeScan);
                                    }
                                    break;
                                case "SF":
                                    int.TryParse(s.Substring(2, 1), out ArOne.SignalMeterDisplay);
                                    if (OnDecodedByte != null)
                                    {
                                        OnDecodedByte(comand, ArOne.SignalMeterDisplay);
                                    }
                                    break;
                                case "OF":
                                    int.TryParse(s.Substring(2, 3), out ArOne.DuplexMode);
                                    if (OnDecodedByte != null)
                                    {
                                        OnDecodedByte(comand, ArOne.DuplexMode);
                                    }
                                    break;
                                case "OL": int DplFrqN;//запрос: DplFrqN могло быть 0..47, а тут 1..19 ???????????
                                    int.TryParse(s.Substring(2, 2), out  DplFrqN);
                                    int.TryParse(s.Substring(5, 10), out ArOne.DuplexFrequency[DplFrqN]);
                                    if (OnDecodedByte != null)
                                    {
                                        OnDecodedByte(comand, ArOne.DuplexFrequency[DplFrqN]);
                                    }
                                    break;
                                case "LU":
                                    int.TryParse(s.Substring(2, 3), out ArOne.SignalLevelUnit_dBmV);
                                    if (OnDecodedSignalLevelUnit_dBmV != null)
                                    {
                                        OnDecodedSignalLevelUnit_dBmV();
                                    }
                                    break;
                                case "LB":
                                    int.TryParse(s.Substring(3, 4), out ArOne.SignalLevelUnit_dBm);
                                    if (OnDecodedSignalLevelUnit_dBm != null)
                                    {
                                        OnDecodedSignalLevelUnit_dBm();
                                    }
                                    break;
                                //case "SR": 
                                //int.TryParse(s.Substring(2, 1), out ArOne.Mode); 
                                //if (OnDecodedByte != null)
                                //{
                                //    OnDecodedByte(comand, ArOne.Mode);/
                                //}
                                //break;
                                //case "PR": 
                                //int.TryParse(s.Substring(5, 10), out ArOne.PassFrequencyList[int.Parse(s.Substring(2, 2))]); 
                                //if (OnDecodedByte != null)
                                //{
                                //    OnDecodedByte(comand, );
                                //}
                                //break;
                                case "GA":
                                    int.TryParse(s.Substring(2, 1), out ArOne.SelectMemory_OnOff);
                                    if (OnDecodedByte != null)
                                    {
                                        OnDecodedByte(comand, ArOne.SelectMemory_OnOff);
                                    }
                                    break;
                                case "GR":
                                    int.TryParse(s.Substring(2, 2), out ArOne.SelectMemoryList);
                                    if (OnDecodedByte != null)
                                    {
                                        OnDecodedByte(comand, ArOne.SelectMemoryList);//Raise the event
                                    }
                                    break;
                            }
                        }
                        Thread.Sleep(10);
                    }

                    catch (Exception)
                    {
                        if (OnDecodedByte != null)
                        {
                            OnDecodedByte("Err " + comand, -1);//Raise the event
                        }
                    }
                    //if (OnDecodedByte != null)
                    //{
                    //    OnDecodedByte(1);//Raise the event
                    //}

                }
            }

    }