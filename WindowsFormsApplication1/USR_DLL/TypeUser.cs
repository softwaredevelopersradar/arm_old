﻿using Protocols;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace USR_DLL
{
    public struct TSupprFWS
    {
        public int iID;
        public int iFreq;
        public short sBearing;
        public byte bLetter;
        public short sLevel;
        public byte bPrior;
        public byte bModulation;
        public byte bDeviation;
        public byte bManipulation;
        public byte bDuration;

        private static int currentId = -1;

        public static TSupprFWS CreateID()
        {
            var result = new TSupprFWS();
            result.iID = currentId - 1;
            Interlocked.Decrement(ref currentId);
            return result;
        }

        public static void SetIdFWS(int id)
        {
            currentId = id;
        }
    }

    public struct TDistribFWS
    {
        public int iID;
        public int iFreq;        // частота
        public short sBearing1;  // пеленг свой
        public short sBearing2;  // пеленг сопр
        public short sLevel;     // уровень
        public int iDFreq;       // ширина полосы
        public int iCKO;         // СКО
        public double dLatitude; // широта
        public double dLongitude;// долгота
        public byte bView;       // вид
        public int iSP_RR;       // СП радиоразведки
        public int iSP_RP;       // СП радиоподавления 

        private static int currentId = -1;

        public static TDistribFWS CreateID()
        {
            var result = new TDistribFWS();
            result.iID = currentId - 1;
            Interlocked.Decrement(ref currentId);
            return result;
        }

        public static void SetIdFWS(int id)
        {
            currentId = id;
        }
    }

    public struct TDataADSBReceiver
    {
        public int iID;           // номер записи
        public string sICAO;      // номер воздушного объекта
        public string sLatitude;  // широта
        public string sLongitude; // долгота
        public string sAltitude;  // высота
        public string sDatetime;  // дата, время
    }

    public struct TDistribFHSS
    { 
        public int iID;        // номер записи
        public int iFreqMin;   // частота минимальная
        public int iFreqMax;   // частота максимальная
        public int iStep;      // шаг
        public int iDFreq;     // ширина полосы
        public int iDuratImp;  // длительность импульсов
        public int iCountIRI;  // количество ИРИ
        public string sTime;   // время
        public int iSP_RR;     // СП радиоразведки
        public bool bCR;       // ЦР
        public FHSS_Location[] sLocation;
        public FHSS_Freq_Band[] sFreq_Band;
    }

    public struct FHSS_Location
    {
        public int iID;        // номер записи
        public int iQ1;        // пеленг 1
        public int iQ2;        // пеленг 2
        public double dLatitude;  // широта
        public double dLongitude; // долгота
    }

    public struct FHSS_Freq_Band
    {
        public int iID;        // номер записи
        public int iFreq;      // частота
        public int iDFreq;     // ширина полосы
    }

    public struct TDistribFHSS_RP
    { 
        public int iID;        // номер записи
        public int iFreqMin;   // частота минимальная
        public int iFreqMax;   // частота максимальная
        public byte[] bLetter;
        public short sLevel;
        public int iStep;      // шаг
        public byte bCodeFFT;
        public byte bModulation;
        public byte bDeviation;
        public byte bManipulation;
        public byte bDuration;
        public int iDuration;
    }

    public struct TDistribFHSS_RPExclude
    { 
        public int iID;           // номер записи
        public int iFreqExclude;  // частота
        public int iWidthExclude; // полоса

        //private static int currentId = 1;

        //public static TDistribFHSS_RPExclude CreateID()
        //{
        //    var result = new TDistribFHSS_RPExclude();
        //    result.iID = currentId + 1;
        //    Interlocked.Increment(ref currentId);
        //    return result;
        //}

        //public static void SetIdFWS(int id)
        //{
        //    currentId = id;
        //}
    }

    public struct TCoordsGNSS
    { 
        public double Lat;
        public double Lon;
        public byte signLat;
        public byte signLon;
        public double Alt;
    }

    public struct TDirectionAntennas
    {
        public int ARD1;
        public int ARD2;
        public int ARD3;
        public int compass;
        public int LPA13;
        public int LPA24;
    }

    public struct TCoordsIRI
    {
        public int iID;
        public int iFreq;
        public double dLat;
        public double dLon;
        public byte bDelIRI;
        public TDirections tDirections;
        public string sColorIRI;
    }

    public struct TCoordsIRI_PPRCh
    {
        public int iID;
        public int iQ1;        // пеленг 1
        public int iQ2;        // пеленг 2
        public double dLatitude;  // широта
        public double dLongitude; // долгота
        public byte bDelIRI;
        public string sColorIRI; 
    }

    public struct TFreqImportantKRPU
    {
        public int iID;
        public Int64 iFreqMin;
        public Int64 iFreqMax;
    }

    public struct TDirections
    {
        public int iQ1;
        public int iQ2;
    }

    public struct TJammer
    {
        public int iNumSP;
        public double dLat;
        public double dLon;
        public int iCountIRI;
        public LettersRP lettersRP;
        public RangeSector[] rangesRP;
        //public RangesRP[] rangesRP;
    }

    public struct LettersRP
    {
        public byte bLet1;
        public byte bLet2;
        public byte bLet3;
        public byte bLet4;
        public byte bLet5;
        public byte bLet6;
        public byte bLet7;
        public byte bLet8;
        public byte bLet9;
    }

    //public struct RangesRP
    //{
    //    public int iFreqMin;
    //    public int iFreqMax;
    //    public int iAngleMin;
    //    public int iAngleMax;
    //}

    // LENA *******************************************************************

    // ---------------------------------------------------------------------
    // Для диапазонов частот

    public struct S_F_DistrIRI
    {
        public double Fmin;
        public double Fmax;
    }
    // ---------------------------------------------------------------------
    // Для нового массива диапазонов частот и углов с указанием, по какой литере 
    // этот диапазон

    public struct S_FB_DistrIRI
    {
        public double Fmin;
        public double Fmax;
        public double Bmin;
        public double Bmax;
        public int NumLit;
        public int NumKan;
    }
    // ---------------------------------------------------------------------
    // Для создания листа каналов

    public struct S_Kan_DistrIRI
    {
        // MaxIRILit/0
        public int flKan;
        public int NumLit;
    }
    // ---------------------------------------------------------------------
    // Распределение ИРИ

    public struct S_IRI_DistrIRI
    {
        // частота, КГц/0(вся запись пустая)/-1(нет данных по частоте)
        public double F_IRI;


        // Пеленг1,2 (Получены от СП2,СП3)
        // в град 0...360 по часовой стрелке/-1(нет данных)
        public double Pel1;
        public double Pel2;

        // Коды модуляции
        //public int[] kod_mod;

        // X(м) координата X на плоскости/0 (нет данных)
        public double X_IRI;
        // Y(м) координата Y на плоскости (север)/0 (нет данных)
        public double Y_IRI;
        // Z(м) координата Z высота/0 (нет данных)
        public double Z_IRI;

        // номер СП1 (от которой пришли данные по частоте)/-1(нет данных)
        public int NSP1;

        // Признак попадания в диапазоны частот(сформированный как результат пересечения
        // литер и диапазонов СП) и углов(+1/-1)
        public int PriznPop;

        // Приоритет для СП1/СП2
        public int Prior1;
        public int Prior2;

        // Номер станции помех(СП), на которую распределили ИРИ
        // и номер канала СП
        public int NSPRP;
        public int NKanRP;
    }
    // --------------------------------------------------------------------
    // Вложенная структура СП 

    public struct SP_IN
    {
        // диапазон частот в КГц сектора
        public double Fmin;
        public double Fmax;

        // границы сектора по азимуту в град 0...360 по часовой стрелке
        // от СП
        public double Beta_min;
        public double Beta_max;

    }
    // --------------------------------------------------------------------
    // Структура СП (станции помех)

    public struct S_SP_DistrIRI
    {
        // Номер СП
        public int N_SP;

        // X(км) координата X на плоскости
        public double X_SP;
        // Y(км) координата Y на плоскости (север)
        public double Y_SP;
        // Z(км) координата Z высота
        public double Z_SP;

        // Макс. кол-во ИРИ на литеру
        public int MaxIRILit;

        // Массив работы по литерам (1-работает по этой литере)
        public int[] Lit;

        // Массив вложенных структур по максимальному числу секторов
        // (диапазоны частот и углов)
        public SP_IN[] mas_SP_IN;

    }
    // ---------------------------------------------------------------------
    // ---------------------------------------------------------------------
    // Входной массив ИРИ

    public struct S_IRI_RNet
    {
        public int ID;

        // частота
        public double F;

        // Пеленг1,2 
        public double Pel1;
        public double Pel2;

        // X(м) 
        public double X;
        // Y(м) 
        public double Y;
    }
    // --------------------------------------------------------------------
    // Структура РС

    public struct SRNet
    {
        // Номер РС
        public int NRNet;

        // Кол-во ИРИ в РС
        public int numbIRI;

        // Массив ID IRI
        //public int[] mas_IRI;
        //public List<int> list_IRI_ID;
        public List<S_IRI_RNet> list_IRI_ID;


    }
    // --------------------------------------------------------------------

    // ******************************************************************* LENA
   
}
