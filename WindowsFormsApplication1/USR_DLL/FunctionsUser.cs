﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// LENA
using ClassLibraryPeleng;


namespace USR_DLL
{
    public class FunctionsUser
    {
        public delegate void FeatureEventHandler(TSupprFWS SupprFWS, byte bTable);
        public static event FeatureEventHandler FeatureAdd;
        public static event FeatureEventHandler FeaturChange;

        public delegate void ActiveBandIndexEventHandler(int activeBandIndex);
        public static event ActiveBandIndexEventHandler ActiveBandIndexSendEvent;

        public delegate int RequestActiveBandIndexEventHandler();
        public static event RequestActiveBandIndexEventHandler RequestActiveBandIndexSendEvent;


        //public delegate void ChangeVariableAllRangeRREventHandler(bool fChangeDBAllRange, byte bMode);
        //public delegate void ChangeVariableAllRangeRPEventHandler(bool fChangeDBAllRange, byte bMode);

        //public static event ChangeVariableAllRangeRREventHandler OnChangeDBAllRangeRR;
        //public static event ChangeVariableAllRangeRPEventHandler OnChangeDBAllRangeRP;

        VariableDynamic.VariableWork variableWork = new VariableDynamic.VariableWork();
        VariableStatic.VariableSuppression variableSuppression = new VariableStatic.VariableSuppression();
        VariableStatic.VariableCommon variableCommon = new VariableStatic.VariableCommon();

        /// <summary>
        /// Количество ИРИ в данной литере
        /// </summary>
        /// <param name="bTable"> 1 - ведущая, 2 - ведомая </param>
        /// <param name="iLetter"> литера </param>
        /// <returns> сообщение об ошибке (пустая строка - успешно) </returns>
        private string CountFreqLetter(byte bTable, byte bLetter)
        {
            string sMessage = "";

            switch(bTable)
            {
                case 1:
                    {
                        List<TSupprFWS> lTSupprFWS = variableWork.SupprFWS_Own.ToList<TSupprFWS>();

                        int iCount = lTSupprFWS.Count(x => x.bLetter == bLetter);

                        if (iCount == variableSuppression.CountSourceLetter) // Количество ИРИ в данной литере
                        {
                            if (variableCommon.Language.Equals(0))
                            {
                                sMessage = "Количество ИРИ в данной литере равно максимальному значению!";
                            }
                            if (variableCommon.Language.Equals(2))
                            {
                                sMessage = "Verilmiş literlərdə şüalanma mənbələrinin maksimal əmsalı yüksək olmalıdır";
                            }
                        }
                    }
                    break;

                case 2:
                    {
                        List<TSupprFWS> lTSupprFWS = variableWork.SupprFWS_Linked.ToList<TSupprFWS>();

                        int iCount = lTSupprFWS.Count(x => x.bLetter == bLetter);

                        if (iCount == variableSuppression.CountSourceLetter) // Количество ИРИ в данной литере
                        {
                            if (variableCommon.Language.Equals(0))
                            {
                                sMessage = "Количество ИРИ в данной литере равно максимальному значению!";
                            }
                            if (variableCommon.Language.Equals(2))
                            {
                                sMessage = "Verilmiş literlərdə şüalanma mənbələrinin maksimal əmsalı yüksək olmalıdır";
                            }
                        }
                    }
                    break;

                default:
                    break;
            }

            return sMessage;
        }

        /// <summary>
        /// Проверка включения частоты данной литеры для РП
        /// </summary>
        /// <param name="bTable"> 1 - ведущая, 2 - ведомая </param>
        /// <param name="iLetter"> литера </param>
        /// <returns> сообщение об ошибке (пустая строка - успешно) </returns>
        private string CheckIncludeLetter(byte bTable, byte bLetter)
        {
            string sMessage = "";
            string sMesErr = "";
           
            if (variableCommon.Language.Equals(0))
            {
                sMesErr = "Частота данной литеры не включена для РП!";
            }
            if (variableCommon.Language.Equals(2))
            {
                 sMesErr = "Verilmiş literlərin tezliyi radiosusdurmaya qusulmayıb!";
            }

            switch (bTable)
            {
                case 1:
                    {
                        switch (bLetter)
                        { 
                            case 1:
                                if (variableSuppression.OwnLetter1 == 0)
                                    sMessage = sMesErr;
                                break;

                            case 2:
                                if (variableSuppression.OwnLetter2 == 0)
                                    sMessage = sMesErr;
                                break;

                            case 3:
                                if (variableSuppression.OwnLetter3 == 0)
                                    sMessage = sMesErr;
                                break;

                            case 4:
                                if (variableSuppression.OwnLetter4 == 0)
                                    sMessage = sMesErr;
                                break;

                            case 5: 
                                if (variableSuppression.OwnLetter5 == 0)
                                    sMessage = sMesErr;
                                break;

                            case 6:
                                if (variableSuppression.OwnLetter6 == 0)
                                    sMessage = sMesErr;
                                break;

                            case 7:
                                if (variableSuppression.OwnLetter7 == 0)
                                    sMessage = sMesErr;
                                break;

                            case 8:
                                if (variableSuppression.OwnLetter8 == 0)
                                    sMessage = sMesErr;
                                break;

                            case 9:
                                if (variableSuppression.OwnLetter9 == 0)
                                    sMessage = sMesErr;
                                break;

                            default: 
                                break;
                        }
                    }
                    break;

                case 2:
                    {
                        switch (bLetter)
                        {
                            case 1:
                                if (variableSuppression.JammerLinkedLetter1 == 0)
                                    sMessage = sMesErr;
                                break;

                            case 2:
                                if (variableSuppression.JammerLinkedLetter2 == 0)
                                    sMessage = sMesErr;
                                break;

                            case 3:
                                if (variableSuppression.JammerLinkedLetter3 == 0)
                                    sMessage = sMesErr;
                                break;

                            case 4:
                                if (variableSuppression.JammerLinkedLetter4 == 0)
                                    sMessage = sMesErr;
                                break;

                            case 5:
                                if (variableSuppression.JammerLinkedLetter5 == 0)
                                    sMessage = sMesErr;
                                break;

                            case 6:
                                if (variableSuppression.JammerLinkedLetter6 == 0)
                                    sMessage = sMesErr;
                                break;

                            case 7:
                                if (variableSuppression.JammerLinkedLetter7 == 0)
                                    sMessage = sMesErr;
                                break;

                            case 8:
                                if (variableSuppression.JammerLinkedLetter8 == 0)
                                    sMessage = sMesErr;
                                break;

                            case 9:
                                if (variableSuppression.JammerLinkedLetter9 == 0)
                                    sMessage = sMesErr;
                                break;

                            default:
                                break;
                        }
                    }
                    break;
                     
                default:
                    break;
            }

            return sMessage;
        }

        /// <summary>
        /// Принадлежность частоты заданным диапазонам РП
        /// </summary>
        /// <param name="bTable"> 1 - ведущая, 2 - ведомая </param>
        /// <param name="iFreq"> частота </param>
        /// <returns> сообщение об ошибке (пустая строка - успешно) </returns>
        private string CheckRangesRP(byte bTable, int iFreq)
        {
            string sMessage = "";

            if (variableCommon.Language.Equals(0))
            {
                sMessage = "Частота не принадлежит диапазону РП";
            }
            if (variableCommon.Language.Equals(2))
            {
                sMessage = "Tezlik RS aralığına aid deyil!";
            }

            switch (bTable)
            {
                case 1:
                    {
                        int lenOwn = variableWork.RangeSectorSupprOwn.Length;
                        for (int i = 0; i < lenOwn; i++)
                        {
                            if (iFreq >= variableWork.RangeSectorSupprOwn[i].StartFrequency && iFreq <= variableWork.RangeSectorSupprOwn[i].EndFrequency)
                                sMessage = "";
                        }
                    }
                    break;

                case 2:
                    {
                        int lenLinked = variableWork.RangeSectorSupprLinked.Length;
                        for (int i = 0; i < lenLinked; i++)
                        {
                            if (iFreq >= variableWork.RangeSectorSupprLinked[i].StartFrequency && iFreq <= variableWork.RangeSectorSupprLinked[i].EndFrequency)
                                sMessage = "";
                        }
                    }
                    break;

                default:
                    break;
            }

            return sMessage;
        }

        /// <summary>
        /// Принадлежность частоты Запрещенным частотам
        /// </summary>
        /// <param name="bTable"> 1 - ведущая, 2 - ведомая </param>
        /// <param name="iFreq"> частота </param>
        /// <returns></returns>
        private string CheckForbiddenFreq(byte bTable, int iFreq)
        {
            string sMessage = "";

            switch (bTable)
            {
                case 1:
                    {
                        int lenOwn = variableWork.FrequencyRangeForbiddenOwn.Length;
                        for (int i = 0; i < lenOwn; i++)
                        {
                            if (iFreq >= variableWork.FrequencyRangeForbiddenOwn[i].StartFrequency && iFreq <= variableWork.FrequencyRangeForbiddenOwn[i].EndFrequency)
                            if (variableCommon.Language.Equals(0))
                            {
                                sMessage = "Частота не может быть добавлена, так как запрещенная!";
                            }
                            if (variableCommon.Language.Equals(2))
                            {
                                sMessage = "Tezlik əlavə edilmir, çünki qadağan!";
                            }
                        }
                    }
                    break;

                case 2:
                    {
                        int lenLinked = variableWork.FrequencyRangeForbiddenLinked.Length;
                        for (int i = 0; i < lenLinked; i++)
                        {
                            if (variableCommon.Language.Equals(0))
                            {
                                sMessage = "Частота не может быть добавлена, так как запрещенная!";
                            }
                            if (variableCommon.Language.Equals(2))
                            {
                                sMessage = "Tezlik əlavə edilmir, çünki qadağan!";
                            }
                        }
                    }
                    break;

                default:
                    break;
            }

            return sMessage;
        }

        /// <summary>
        /// Добавление/Изменение в variableWork ИРИ
        /// </summary>
        /// <param name="SupprFWS"> структура ИРИ для РП </param>
        /// <param name="tempListSupprFWS"> теущие значения в variableWork </param>
        /// <param name="bTable"> 1 - ведущая, 2 - ведомая </param>
        /// <returns> сообщение об ошибке (пустая строка - успешно) </returns>
        public string ChangeIRI_FRCh_RPToVariableWork(TSupprFWS SupprFWS, List<TSupprFWS> tempListSupprFWS, byte bTable, int indRec)
        {

            string sMessage = CheckRangesRP(bTable, SupprFWS.iFreq);
            if (sMessage != "")
                return sMessage;

            sMessage = CheckForbiddenFreq(bTable, SupprFWS.iFreq);
            if (sMessage != "")
                return sMessage;

            sMessage = CheckIncludeLetter(bTable, SupprFWS.bLetter);
            if (sMessage != "")
                return sMessage;

            sMessage = CountFreqLetter(bTable, SupprFWS.bLetter);

            if (sMessage == "")
            {
                switch (bTable)
                {
                    case 1:
                        {
                            if (indRec != -1) { tempListSupprFWS.Insert(indRec, SupprFWS); }
                            else { tempListSupprFWS.Add(SupprFWS); }

                            //IEnumerable<TSupprFWS> sortPriority = tempListSupprFWS.OrderByDescending(x => x.bPrior);//.OrderBy(x => x.bPrior); // сортировка по приоритету
                            //IEnumerable<TSupprFWS> sortLetter = sortPriority.OrderBy(x => x.bLetter);      // сортировка по литере

                            //variableWork.SupprFWS_Own = sortLetter.ToArray();

                            variableWork.SupprFWS_Own = tempListSupprFWS.ToArray();
                        }
                        break;

                    case 2:
                        {
                            if (indRec != -1) { tempListSupprFWS.Insert(indRec, SupprFWS); }
                            else { tempListSupprFWS.Add(SupprFWS); }

                            //IEnumerable<TSupprFWS> sortPriority = tempListSupprFWS.OrderByDescending(x => x.bPrior);// OrderBy(x => x.bPrior); // сортировка по приоритету
                            //IEnumerable<TSupprFWS> sortLetter = sortPriority.OrderBy(x => x.bLetter);      // сортировка по литере

                            //variableWork.SupprFWS_Linked = sortLetter.ToArray();

                            variableWork.SupprFWS_Linked = tempListSupprFWS.ToArray();
                        }
                        break;

                    default:
                        break;
                }
            }

            return sMessage;
        }

        public void ReSendActiveBandIndex(int activeBandIndex)
        {
            if (ActiveBandIndexSendEvent != null)
                ActiveBandIndexSendEvent(activeBandIndex);
        }

        public void RequestActiveBandIndex()
        {
            if (RequestActiveBandIndexSendEvent != null)
                ActiveBandIndexSendEvent(RequestActiveBandIndexSendEvent());
        }


        // LENA **************************************************************

        // ********************************************************************
        /// <summary>
        /// Целераспределение_Main
        /// </summary>
        /// <param name="Jammer"> Данные станции помех </param>
        /// <param name="DistribFWS"> Данные по ИРИ ФРЧ </param>
        // ********************************************************************

        //public void Distribution(TJammer Jammer, ref TDistribFWS DistribFWS)
        public void Distribution(TJammer[] Jammer, ref TDistribFWS[] DistribFWS)
        {
            // -----------------------------------------------------------------
            int nSP_all = 0;
            int nIRI_all = 0;
            int flout = 0;
            int shSP = 0;
            int shIRI = 0;
            int shLit = 0;
            int shDiap = 0;
            int shDiap1 = 0;
            int nDiap = 0;
            int nDiap1 = 0;
            double b1 = 0;
            double b2 = 0;

            int MaxNumbLit = 0; // Number of liters
            if (variableCommon.TypeStation == 0)
                MaxNumbLit = 7;
            else MaxNumbLit = 9;

            nSP_all = Jammer.Length;
            nIRI_all = DistribFWS.Length;

            S_SP_DistrIRI[] mas_SP = new S_SP_DistrIRI[nSP_all];
            S_IRI_DistrIRI[] mas_IRI = new S_IRI_DistrIRI[nIRI_all];
            // -----------------------------------------------------------------
            // Переписываем массивы

            // ................................................................
            // SP

            for (shSP = 0; shSP < nSP_all; shSP++)
            {
                mas_SP[shSP].N_SP = Jammer[shSP].iNumSP;

                f_DistributionIRI_Coord(
                                        Jammer[shSP].dLat,
                                        Jammer[shSP].dLon,
                                        ref mas_SP[shSP].X_SP,
                                        ref mas_SP[shSP].Y_SP
                                       );
                mas_SP[shSP].Z_SP = 0;

                mas_SP[shSP].MaxIRILit = Jammer[shSP].iCountIRI;

                mas_SP[shSP].Lit = new int[MaxNumbLit];
                for (shLit = 0; shLit < MaxNumbLit; shLit++)
                {
                    mas_SP[shSP].Lit[shLit] = f_LitMas(Jammer[shSP].lettersRP, shLit);

                } // FOR_Lit

                nDiap = Jammer[shSP].rangesRP.Length;
                shDiap1 = 0;
                for (shDiap = 0; shDiap < nDiap; shDiap++)
                {
                    b1 = (double)Jammer[shSP].rangesRP[shDiap].StartDirection / 10d;//.iAngleMin;
                    b2 = (double)Jammer[shSP].rangesRP[shDiap].EndDirection / 10d;//.iAngleMax;

                    if (b1 <= b2)
                    {
                        shDiap1 += 1;
                    }
                    else
                    {
                        shDiap1 += 2;
                    }

                } // FOR_Diap
                nDiap1 = shDiap1;

                mas_SP[shSP].mas_SP_IN = new SP_IN[nDiap1];
                shDiap1 = 0;
                for (shDiap = 0; shDiap < nDiap; shDiap++)
                {
                    b1 = (double)Jammer[shSP].rangesRP[shDiap].StartDirection / 10d;//.iAngleMin;
                    b2 = (double)Jammer[shSP].rangesRP[shDiap].EndDirection / 10d;//.iAngleMax;

                    if (b1 <= b2)
                    {
                        mas_SP[shSP].mas_SP_IN[shDiap1].Fmin = (double)Jammer[shSP].rangesRP[shDiap].StartFrequency / 10d;//.iFreqMin;
                        mas_SP[shSP].mas_SP_IN[shDiap1].Fmax = (double)Jammer[shSP].rangesRP[shDiap].EndFrequency / 10d;//.iFreqMax;
                        mas_SP[shSP].mas_SP_IN[shDiap1].Beta_min = b1;//.iAngleMin;
                        mas_SP[shSP].mas_SP_IN[shDiap1].Beta_max = b2;//.iAngleMax;
                        shDiap1 += 1;
                    }
                    else
                    {
                        mas_SP[shSP].mas_SP_IN[shDiap1].Fmin = (double)Jammer[shSP].rangesRP[shDiap].StartFrequency / 10d;//.iFreqMin;
                        mas_SP[shSP].mas_SP_IN[shDiap1].Fmax = (double)Jammer[shSP].rangesRP[shDiap].EndFrequency / 10d;//.iFreqMax;
                        mas_SP[shSP].mas_SP_IN[shDiap1].Beta_min = b1;//.iAngleMin;
                        mas_SP[shSP].mas_SP_IN[shDiap1].Beta_max = 360;//.iAngleMax;
                        shDiap1 += 1;
                        mas_SP[shSP].mas_SP_IN[shDiap1].Fmin = (double)Jammer[shSP].rangesRP[shDiap].StartFrequency / 10d;//.iFreqMin;
                        mas_SP[shSP].mas_SP_IN[shDiap1].Fmax = (double)Jammer[shSP].rangesRP[shDiap].EndFrequency / 10d;//.iFreqMax;
                        mas_SP[shSP].mas_SP_IN[shDiap1].Beta_min = 0;//.iAngleMin;
                        mas_SP[shSP].mas_SP_IN[shDiap1].Beta_max = b2;//.iAngleMax;
                        shDiap1 += 1;

                    }

                } // FOR_Diap

            } // FOR_SP
            // ................................................................
            // IRI

            for (shIRI = 0; shIRI < nIRI_all; shIRI++)
            {

                mas_IRI[shIRI].F_IRI = (double)DistribFWS[shIRI].iFreq / 10d;
                mas_IRI[shIRI].Pel1 = (double)DistribFWS[shIRI].sBearing1 / 10d;
                mas_IRI[shIRI].Pel2 = (double)DistribFWS[shIRI].sBearing2 / 10d;

                f_DistributionIRI_Coord(
                                        DistribFWS[shIRI].dLatitude,
                                        DistribFWS[shIRI].dLongitude,
                                        ref mas_IRI[shIRI].X_IRI,
                                        ref mas_IRI[shIRI].Y_IRI
                                       );

                if ((DistribFWS[shIRI].dLatitude == -1) || (DistribFWS[shIRI].dLongitude == -1))
                {
                    mas_IRI[shIRI].X_IRI = 0;
                    mas_IRI[shIRI].Y_IRI = 0;
                }


                mas_IRI[shIRI].Z_IRI = 0;

                mas_IRI[shIRI].NSP1 = DistribFWS[shIRI].iSP_RR;

                mas_IRI[shIRI].PriznPop = 0;
                mas_IRI[shIRI].Prior1 = 0;
                mas_IRI[shIRI].Prior2 = 0;
                mas_IRI[shIRI].NSPRP = 0;
                mas_IRI[shIRI].NKanRP = 0;

            } // FOR_IRI
            // ................................................................


            // -----------------------------------------------------------------
            // Функция распределения ИРИ по СП

            flout = f_DistributionIRI_SP(

                              // БД СП
                               mas_SP,

                               // БД ИРИ
                               ref mas_IRI
                               );
            // -----------------------------------------------------------------
            for (shIRI = 0; shIRI < nIRI_all; shIRI++)
            {

                DistribFWS[shIRI].iSP_RP = mas_IRI[shIRI].NSPRP;

            } // FOR_IRI
            // -----------------------------------------------------------------


        } // Целераспределение_Main
        // ********************************************************************

        // ********************************************************************
        public int f_LitMas(
                            LettersRP l,
                            int ind
                            )
        {
            if (ind == 0)
                return (int)l.bLet1;
            else if (ind == 1)
                return (int)l.bLet2;
            else if (ind == 2)
                return (int)l.bLet3;
            else if (ind == 3)
                return (int)l.bLet4;
            else if (ind == 4)
                return (int)l.bLet5;
            else if (ind == 5)
                return (int)l.bLet6;
            else if (ind == 6)
                return (int)l.bLet7;
            else if (ind == 7)
                return (int)l.bLet8;
            else
                return (int)l.bLet9;


        }
        // ********************************************************************

        // ********************************************************************
        // Пересчет координат WGS84(grad)->XY (Гаусс-Крюгер)
        // ********************************************************************
        public void f_DistributionIRI_Coord(
                                           double B,
                                           double L,
                                           ref double X,
                                           ref double Y
                                           )
        {
            // ................................................................
            Peleng objPeleng = new Peleng(null);

            double xtmp1_ed, ytmp1_ed;

            xtmp1_ed = 0;
            ytmp1_ed = 0;
            // ................................................................
            // DATUM
            // ГОСТ 51794_2008
            double dXdat_comm = 25;
            double dYdat_comm = -141;
            double dZdat_comm = -80;

            double dLat_comm = 0;
            double dLong_comm = 0;
            // Эллипсоид Красовского, град
            double LatKrG_comm = 0;
            double LongKrG_comm = 0;
            // Эллипсоид Красовского, rad
            double LatKrR_comm = 0;
            double LongKrR_comm = 0;
            // Гаусс-крюгер(СК42) м
            double X42_comm = 0;
            double Y42_comm = 0;

            // ......................................................................
            // grad(WGS84)

            xtmp1_ed = B;
            ytmp1_ed = L;
            // ......................................................................

            // WGS84(эллипсоид)->элл.Красовского *************************************
            // Перевод координат WGS84(эллипсоид) -> эллипсоид Красовского(Пулково-42)
            // WGS84(широта,долгота)из Панорамы пересчитываем в эллипсод Красовского моей функцией
            // Входные параметры -> град,km
            // Перевод в рад - внутри функции

            // dLong ..................................................................
            // Расчет приращения по долготе при преобразованиях координат WGS84<->SK42
            // (преобразования Молоденского), угл.сек

            objPeleng.f_dLong
                (
                // Входные параметры (град,км)
                    xtmp1_ed,   // широта
                    ytmp1_ed,  // долгота
                    0,     // высота

                    // DATUM,m
                    dXdat_comm,
                    dYdat_comm,
                    dZdat_comm,

                    ref dLong_comm   // приращение по долготе, угл.сек
                );
            // ................................................................ dLong

            // dLat .................................................................
            // Расчет приращения по широте при преобразованиях координат WGS84<->SK42
            // (преобразования Молоденского), угл.сек

            objPeleng.f_dLat
                (
                // Входные параметры (град,км)
                    xtmp1_ed,   // широта
                    ytmp1_ed,  // долгота
                    0,     // высота

                    // DATUM,m
                    dXdat_comm,
                    dYdat_comm,
                    dZdat_comm,

                    ref dLat_comm        // приращение по долготе, угл.сек
                );
            // ................................................................. dLat

            // Lat,Long .............................................................
            // Преобразования широты и долготы при пересчете WGS84->SK42

            objPeleng.f_WGS84_SK42_Lat_Long
                   (
                // Входные параметры (град,км)
                       xtmp1_ed,   // широта
                       ytmp1_ed,  // долгота
                       0,     // высота
                       dLat_comm,       // приращение по долготе, угл.сек
                       dLong_comm,      // приращение по долготе, угл.сек

                       // Выходные параметры (grad)
                       ref LatKrG_comm,   // широта
                       ref LongKrG_comm   // долгота
                   );
            // ............................................................ Lat,Long

            // *********************************** WGS84(эллипсоид)->элл.Красовского

            // SK42(элл.)->Крюгер ****************************************************
            // Преобразование геодезических координат (широта, долгота, высота) 
            // эллипсоида Красовского (СК42) в плоские прямоугольные координаты в
            // проекции Гаусса-Крюгера
            // Входные параметры -> !!!grad

            objPeleng.f_SK42_Krug
                   (
                // Входные параметры (!!! grad)
                // !!! эллипсоид Красовского
                       LatKrG_comm,   // широта
                       LongKrG_comm,  // долгота

                       // Выходные параметры (km)
                       ref X42_comm,
                       ref Y42_comm
                   );

            // km->m
            X = X42_comm * 1000;
            Y = Y42_comm * 1000;

            // **************************************************** SK42(элл.)->Крюгер


        } // f_DistributionIRI_Coord
        // ********************************************************************

        // *************************************************************************
        // Возврат функции: 
        //     0 -> хоть один ИРИ закрепился за СП
        //    -1 -> ни один ИРИ не прикрепился к СП 
        //    -2 -> нет входных данных
        // *************************************************************************
        // DistrIRI

        public int f_DistributionIRI_SP(

                               // БД СП
                               S_SP_DistrIRI[] mas_SP,

                              // БД ИРИ
                               ref S_IRI_DistrIRI[] mas_IRI
                           )
        {

            // ----------------------------------------------------------------
            // Distribution_IRI

            List<S_FB_DistrIRI> list_SP1_DistrIRI = new List<S_FB_DistrIRI>();
            List<S_FB_DistrIRI> list_SP2_DistrIRI = new List<S_FB_DistrIRI>();
            List<S_Kan_DistrIRI> list_KanSP1_DistrIRI = new List<S_Kan_DistrIRI>();
            List<S_Kan_DistrIRI> list_KanSP2_DistrIRI = new List<S_Kan_DistrIRI>();

            List<S_F_DistrIRI> list_LIT_DistrIRI = new List<S_F_DistrIRI>();

            int MaxNumbLit = 0; // Number of liters
            if (variableCommon.TypeStation == 0)
                MaxNumbLit = 7;
            else MaxNumbLit = 9;
            // -----------------------------------------------------------------
            // Количество ИРИ и СП
            int nIRI = 0;
            int nSP = 0;
            int sh_IRI = 0;
            int sh_SP = 0;
            int sh_diap = 0;
            int ndiap = 0;
            int fl_prod = 0;
            int sh_lit = 0;
            double fmin1 = 0;
            double fmin1_dubl = 0;
            double fmax1 = 0;
            int fl_min = 0;
            int fl_min_dubl = 0;
            int num_kan = 0;
            int fl1_SP1 = 0;
            int fl2_SP2 = 0;

            // Расстояние между ИРИ и СП (dX=Xири-Xсп...)
            double dX = 0;
            double dY = 0;
            double dZ = 0;
            double D_IRI_SP1 = 0;
            double D_IRI_SP2 = 0;

            int prizn1 = 0;
            int prizn2 = 0;

            int indSP = 0;
            int indKan = 0;
            int shKan = 0;
            int Count_IRI = 0;
            // -------------------------------------------------------------------------

            // Анализ входной информации **********************************************
            // -------------------------------------------------------------------------
            if ((mas_IRI == null) || (mas_SP == null))
                return -2;
            // -------------------------------------------------------------------------
            // Число ИРИ и СП

            nIRI = mas_IRI.Length;
            nSP = mas_SP.Length;

            if ((nIRI == 0) || (nSP == 0))
                return -2;
            // ------------------------------------------------------------------------

            // ********************************************** Анализ входной информации

            // Литеры *****************************************************************
            list_LIT_DistrIRI.Clear();

            S_F_DistrIRI objS_F_DistrIRI = new S_F_DistrIRI();

            objS_F_DistrIRI.Fmin = 30000;
            objS_F_DistrIRI.Fmax = 50000;
            list_LIT_DistrIRI.Add(objS_F_DistrIRI);
            objS_F_DistrIRI.Fmin = 50000;
            objS_F_DistrIRI.Fmax = 90000;
            list_LIT_DistrIRI.Add(objS_F_DistrIRI);
            objS_F_DistrIRI.Fmin = 90000;
            objS_F_DistrIRI.Fmax = 160000;
            list_LIT_DistrIRI.Add(objS_F_DistrIRI);
            objS_F_DistrIRI.Fmin = 160000;
            objS_F_DistrIRI.Fmax = 290000;
            list_LIT_DistrIRI.Add(objS_F_DistrIRI);
            objS_F_DistrIRI.Fmin = 290000;
            objS_F_DistrIRI.Fmax = 512000;
            list_LIT_DistrIRI.Add(objS_F_DistrIRI);
            objS_F_DistrIRI.Fmin = 512000;
            objS_F_DistrIRI.Fmax = 860000;
            list_LIT_DistrIRI.Add(objS_F_DistrIRI);
            objS_F_DistrIRI.Fmin = 860000;
            objS_F_DistrIRI.Fmax = 1215000;
            list_LIT_DistrIRI.Add(objS_F_DistrIRI);
            objS_F_DistrIRI.Fmin = 1215000;
            objS_F_DistrIRI.Fmax = 2000000;
            list_LIT_DistrIRI.Add(objS_F_DistrIRI);
            objS_F_DistrIRI.Fmin = 2000000;
            objS_F_DistrIRI.Fmax = 3000000;
            list_LIT_DistrIRI.Add(objS_F_DistrIRI);

            // ***************************************************************** Литеры

            // mas_SP_IN-> mas_SP *****************************************************
            // Преобразование входного массива для СП по частотам и углам в соответствии
            // с работой по литерам

            S_FB_DistrIRI objS_FB_DistrIRI = new S_FB_DistrIRI();
            S_Kan_DistrIRI objS_Kan_DistrIRI = new S_Kan_DistrIRI();

            // FOR1
            for (sh_SP = 0; sh_SP < nSP; sh_SP++)
            {

                ndiap = mas_SP[sh_SP].mas_SP_IN.Length;
                num_kan = 0;
                //num_lit = 0;
                // ---------------------------------------------------------------------- 
                // FOR2 diap

                for (sh_diap = 0; sh_diap < ndiap; sh_diap++)
                {

                    fl_prod = 0;
                    fl_min = 0;
                    fl_min_dubl = 0;
                    //fl_max = 0;
                    // ....................................................................
                    // FOR3 litera

                    for (sh_lit = 0; sh_lit < MaxNumbLit; sh_lit++)
                    {

                        if (fl_prod == 1) goto vybmax;

                        // fl_min_dubl==1
                        if (fl_min_dubl == 1)
                        {

                            if (mas_SP[sh_SP].Lit[sh_lit] == 1)   // Litera active
                            {
                                fmin1 = fmin1_dubl;
                                fl_min = 1;
                                fl_min_dubl = 0;
                            }
                            else // Litera NO active
                            {
                                if (mas_SP[sh_SP].mas_SP_IN[sh_diap].Fmax > list_LIT_DistrIRI[sh_lit].Fmax)
                                {
                                    fmin1_dubl = list_LIT_DistrIRI[sh_lit].Fmax;
                                    fl_min = 0;
                                    fl_min_dubl = 1;
                                }

                            }

                        } //fl_min_dubl==1


                        // fl_min_dubl=0
                        else
                        {
                            // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
                            // fmin

                            // fmin этого диапазона меньше fmin Litera[0]
                            if (
                                (sh_lit == 0) &&
                                (mas_SP[sh_SP].mas_SP_IN[sh_diap].Fmin <= list_LIT_DistrIRI[sh_lit].Fmin)
                                //(mas_SP[sh_SP].Lit[sh_lit] == 1)
                                )
                            {
                                if (mas_SP[sh_SP].Lit[sh_lit] == 1)   // Litera active
                                {
                                    fmin1 = list_LIT_DistrIRI[sh_lit].Fmin;
                                    fl_min = 1;
                                    fl_min_dubl = 0;
                                }
                                else // Litera NO active
                                {
                                    if (mas_SP[sh_SP].mas_SP_IN[sh_diap].Fmax > list_LIT_DistrIRI[sh_lit].Fmax)
                                    {
                                        fmin1_dubl = list_LIT_DistrIRI[sh_lit].Fmax;
                                        fl_min = 0;
                                        fl_min_dubl = 1;
                                    }

                                }

                            }
                            // ***
                            else // Находим литеру,где лежит fmin, если на этой станции эта литера активна
                            {
                                if (
                                    (mas_SP[sh_SP].mas_SP_IN[sh_diap].Fmin >= list_LIT_DistrIRI[sh_lit].Fmin) &&
                                    (mas_SP[sh_SP].mas_SP_IN[sh_diap].Fmin < list_LIT_DistrIRI[sh_lit].Fmax)
                                    //(mas_SP[sh_SP].Lit[sh_lit]==1)
                                   )
                                {
                                    if (mas_SP[sh_SP].Lit[sh_lit] == 1) // Litera active
                                    {
                                        fmin1 = mas_SP[sh_SP].mas_SP_IN[sh_diap].Fmin;
                                        fl_min = 1;
                                        fl_min_dubl = 0;

                                    }
                                    else  // Litera NO active
                                    {
                                        if (mas_SP[sh_SP].mas_SP_IN[sh_diap].Fmax > list_LIT_DistrIRI[sh_lit].Fmax)
                                        {
                                            fmin1_dubl = list_LIT_DistrIRI[sh_lit].Fmax;
                                            fl_min = 0;
                                            fl_min_dubl = 1;
                                        }

                                    }

                                }

                            } // else ***

                        } // fl_min_dubl==0
                    // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
                    // fmax

               vybmax: if (fl_min == 1)
                        {
                            // ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                            // Диапазон станции входит в диапазон литеры

                            if (
                                (mas_SP[sh_SP].mas_SP_IN[sh_diap].Fmax <= list_LIT_DistrIRI[sh_lit].Fmax)
                                //(mas_SP[sh_SP].Lit[sh_lit] == 1)
                               )
                            {

                                // Литера неактивна-> переходим к следующему диапазону
                                if (mas_SP[sh_SP].Lit[sh_lit] == 0)
                                {
                                    break;
                                }


                                fmax1 = mas_SP[sh_SP].mas_SP_IN[sh_diap].Fmax;
                                //fl_max = 1;
                                objS_FB_DistrIRI.Fmin = fmin1;
                                objS_FB_DistrIRI.Fmax = fmax1;
                                objS_FB_DistrIRI.Bmin = mas_SP[sh_SP].mas_SP_IN[sh_diap].Beta_min;
                                objS_FB_DistrIRI.Bmax = mas_SP[sh_SP].mas_SP_IN[sh_diap].Beta_max;
                                objS_FB_DistrIRI.NumLit = sh_lit;
                                //objS_FB_DistrIRI.NumKan = num_kan;

                                if (sh_SP == 0)
                                    list_SP1_DistrIRI.Add(objS_FB_DistrIRI);
                                else
                                    list_SP2_DistrIRI.Add(objS_FB_DistrIRI);


                                if (((sh_SP == 0) && (fl1_SP1 == 0)) ||
                                    ((sh_SP == 1) && (fl2_SP2 == 0))) // 1-й раз
                                {
                                    objS_Kan_DistrIRI.flKan = mas_SP[sh_SP].MaxIRILit;
                                    objS_Kan_DistrIRI.NumLit = sh_lit;
                                    num_kan = sh_lit;
                                    if (sh_SP == 0)
                                    {
                                        list_KanSP1_DistrIRI.Add(objS_Kan_DistrIRI);
                                        fl1_SP1 = 1;
                                    }
                                    else
                                    {
                                        list_KanSP2_DistrIRI.Add(objS_Kan_DistrIRI);
                                        fl2_SP2 = 1;
                                    }
                                }  // 1raz

                                else
                                {

                                    objS_Kan_DistrIRI.flKan = mas_SP[sh_SP].MaxIRILit;
                                    objS_Kan_DistrIRI.NumLit = sh_lit;
                                    if (sh_SP == 0)
                                    {
                                        if (list_KanSP1_DistrIRI.FindIndex(x => x.NumLit == sh_lit) < 0)
                                            list_KanSP1_DistrIRI.Add(objS_Kan_DistrIRI);
                                    }
                                    else
                                    {
                                        if (list_KanSP2_DistrIRI.FindIndex(x => x.NumLit == sh_lit) < 0)
                                            list_KanSP2_DistrIRI.Add(objS_Kan_DistrIRI);
                                    }


                                } // NO 1 raz


                                // Переходим к следующему диапазону
                                break;

                            } // Диапазон станции входит в диапазон литеры
                            // ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                            // fmax диапазона>fmax литеры

                            else
                            {
                                // 1111111111111111111111111111111111111111111111111111111111111111
                                //if(mas_SP[sh_SP].Lit[sh_lit] == 1)
                                if (fl_prod == 0)
                                {

                                    fmax1 = list_LIT_DistrIRI[sh_lit].Fmax;
                                    //fl_max = 1;
                                    objS_FB_DistrIRI.Fmin = fmin1;
                                    objS_FB_DistrIRI.Fmax = fmax1;
                                    objS_FB_DistrIRI.Bmin = mas_SP[sh_SP].mas_SP_IN[sh_diap].Beta_min;
                                    objS_FB_DistrIRI.Bmax = mas_SP[sh_SP].mas_SP_IN[sh_diap].Beta_max;
                                    objS_FB_DistrIRI.NumLit = sh_lit;
                                    //objS_FB_DistrIRI.NumKan = num_kan;
                                    //num_kan++;

                                    if (sh_SP == 0)
                                        list_SP1_DistrIRI.Add(objS_FB_DistrIRI);
                                    else
                                        list_SP2_DistrIRI.Add(objS_FB_DistrIRI);

                                    if (((sh_SP == 0) && (fl1_SP1 == 0)) ||
                                        ((sh_SP == 1) && (fl2_SP2 == 0))) // 1-й раз
                                    {
                                        objS_Kan_DistrIRI.flKan = mas_SP[sh_SP].MaxIRILit;
                                        objS_Kan_DistrIRI.NumLit = sh_lit;
                                        num_kan = sh_lit;
                                        if (sh_SP == 0)
                                        {
                                            list_KanSP1_DistrIRI.Add(objS_Kan_DistrIRI);
                                            fl1_SP1 = 1;
                                        }
                                        else
                                        {
                                            list_KanSP2_DistrIRI.Add(objS_Kan_DistrIRI);
                                            fl2_SP2 = 1;
                                        }
                                    } // 1raz

                                    else
                                    {
                                        objS_Kan_DistrIRI.flKan = mas_SP[sh_SP].MaxIRILit;
                                        objS_Kan_DistrIRI.NumLit = sh_lit;
                                        if (sh_SP == 0)
                                        {
                                            if (list_KanSP1_DistrIRI.FindIndex(x => x.NumLit == sh_lit) < 0)
                                                list_KanSP1_DistrIRI.Add(objS_Kan_DistrIRI);
                                        }
                                        else
                                        {
                                            if (list_KanSP2_DistrIRI.FindIndex(x => x.NumLit == sh_lit) < 0)
                                                list_KanSP2_DistrIRI.Add(objS_Kan_DistrIRI);
                                        }

                                    }


                                    fmin1 = list_LIT_DistrIRI[sh_lit].Fmax;
                                    fl_min = 1;
                                    //fl_max = 0;
                                    fl_prod = 1;

                                } // if(fl_prod==0)
                                // 1111111111111111111111111111111111111111111111111111111111111111
                                // fl_prod==1

                                else
                                {
                                    // Литера неактивна-> пропустить ее
                                    if (mas_SP[sh_SP].Lit[sh_lit] == 0)
                                    {
                                        fmin1 = list_LIT_DistrIRI[sh_lit].Fmax;
                                        fl_min = 1;
                                        //fl_max = 0;
                                        fl_prod = 1;

                                    }

                                    // Литера активна -> включить полностью этот диапазон
                                    else
                                    {
                                        fmax1 = list_LIT_DistrIRI[sh_lit].Fmax;
                                        //fl_max = 1;
                                        objS_FB_DistrIRI.Fmin = fmin1;
                                        objS_FB_DistrIRI.Fmax = fmax1;
                                        objS_FB_DistrIRI.Bmin = mas_SP[sh_SP].mas_SP_IN[sh_diap].Beta_min;
                                        objS_FB_DistrIRI.Bmax = mas_SP[sh_SP].mas_SP_IN[sh_diap].Beta_max;
                                        objS_FB_DistrIRI.NumLit = sh_lit;
                                        //objS_FB_DistrIRI.NumKan = num_kan;
                                        //num_kan++;

                                        if (sh_SP == 0)
                                            list_SP1_DistrIRI.Add(objS_FB_DistrIRI);
                                        else
                                            list_SP2_DistrIRI.Add(objS_FB_DistrIRI);

                                        if (((sh_SP == 0) && (fl1_SP1 == 0)) ||
                                            ((sh_SP == 1) && (fl2_SP2 == 0))) // 1-й раз
                                        {
                                            objS_Kan_DistrIRI.flKan = mas_SP[sh_SP].MaxIRILit;
                                            objS_Kan_DistrIRI.NumLit = sh_lit;
                                            num_kan = sh_lit;
                                            if (sh_SP == 0)
                                            {
                                                list_KanSP1_DistrIRI.Add(objS_Kan_DistrIRI);
                                                fl1_SP1 = 1;
                                            }
                                            else
                                            {
                                                list_KanSP2_DistrIRI.Add(objS_Kan_DistrIRI);
                                                fl2_SP2 = 1;
                                            }
                                        } // 1raz

                                        else
                                        {
                                            objS_Kan_DistrIRI.flKan = mas_SP[sh_SP].MaxIRILit;
                                            objS_Kan_DistrIRI.NumLit = sh_lit;
                                            if (sh_SP == 0)
                                            {
                                                if (list_KanSP1_DistrIRI.FindIndex(x => x.NumLit == sh_lit) < 0)
                                                    list_KanSP1_DistrIRI.Add(objS_Kan_DistrIRI);
                                            }
                                            else
                                            {
                                                if (list_KanSP2_DistrIRI.FindIndex(x => x.NumLit == sh_lit) < 0)
                                                    list_KanSP2_DistrIRI.Add(objS_Kan_DistrIRI);
                                            }

                                        }


                                        fmin1 = list_LIT_DistrIRI[sh_lit].Fmax;
                                        fl_min = 1;
                                        //fl_max = 0;
                                        fl_prod = 1;


                                    } // SP works on this litera

                                } // fl_prod==1
                                // 1111111111111111111111111111111111111111111111111111111111111111

                            } // fmax диапазона>fmax литеры
                            // ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                        } // if(fl_min == 1)
                        // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,

                    } // FOR3 Litera
                    // ....................................................................

                } // FOR2 diap
                // ---------------------------------------------------------------------- 

            } // FOR1 SP
            // ***************************************************** mas_SP_IN-> mas_SP

            // TEST_DIAP ***************************************************************
            // Проверка вхождения ИРИ в переформированные диапазоны

            // FOR5
            for (sh_IRI = 0; sh_IRI < nIRI; sh_IRI++)
            {
                fl_min = 0;
                // ----------------------------------------------------------------------
                // SP1 

                ndiap = list_SP1_DistrIRI.Count;
                // FOR6
                for (sh_diap = 0; sh_diap < ndiap; sh_diap++)
                {
                    if (
                        (mas_IRI[sh_IRI].F_IRI >= list_SP1_DistrIRI[sh_diap].Fmin) &&
                        (mas_IRI[sh_IRI].F_IRI <= list_SP1_DistrIRI[sh_diap].Fmax)
                       )
                    {
                        fl_min = 1;
                        sh_diap = ndiap; // Выход из for6
                    }
                } // FOR6
                // ---------------------------------------------------------------------- 
                // SP2

                if (fl_min == 0)
                {
                    ndiap = list_SP2_DistrIRI.Count;
                    // FOR7
                    for (sh_diap = 0; sh_diap < ndiap; sh_diap++)
                    {
                        if (
                            (mas_IRI[sh_IRI].F_IRI >= list_SP2_DistrIRI[sh_diap].Fmin) &&
                            (mas_IRI[sh_IRI].F_IRI <= list_SP2_DistrIRI[sh_diap].Fmax)
                           )
                        {
                            fl_min = 1;
                            sh_diap = ndiap; // Выход из for7
                        }
                    } // FOR7

                } // if(fl_min == 0)
                // ---------------------------------------------------------------------- 
                if (fl_min == 0)
                {
                    mas_IRI[sh_IRI].PriznPop = -1;
                }
                else
                {
                    mas_IRI[sh_IRI].PriznPop = 0;
                }
                // ---------------------------------------------------------------------- 


            } // FOR5 IRI
            // *************************************************************** TEST_DIAP

            // PRIOR IRI XYZ ***********************************************************
            // Приоритеты ИРИ с Координатами

            // FOR9
            for (sh_IRI = 0; sh_IRI < nIRI; sh_IRI++)
            {

                // IF (есть F и координаты)
                if (
                    (mas_IRI[sh_IRI].F_IRI != 0) &&
                    (mas_IRI[sh_IRI].X_IRI != 0) &&
                    (mas_IRI[sh_IRI].Y_IRI != 0) &&
                    //(mas_IRI[sh_IRI].Z_IRI != 0)&&
                    (mas_IRI[sh_IRI].PriznPop != -1)
                   )
                {

                    prizn1 = 0;
                    prizn2 = 0;
                    // ----------------------------------------------------------------------
                    // SP1 

                    ndiap = list_SP1_DistrIRI.Count;
                    // FOR10
                    for (sh_diap = 0; sh_diap < ndiap; sh_diap++)
                    {
                        if (
                            (mas_IRI[sh_IRI].F_IRI >= list_SP1_DistrIRI[sh_diap].Fmin) &&
                            (mas_IRI[sh_IRI].F_IRI <= list_SP1_DistrIRI[sh_diap].Fmax) &&
                            (mas_IRI[sh_IRI].Pel1 >= list_SP1_DistrIRI[sh_diap].Bmin) &&
                            (mas_IRI[sh_IRI].Pel1 <= list_SP1_DistrIRI[sh_diap].Bmax)
                           )
                        {
                            prizn1 = 1;
                            sh_diap = ndiap; // Выход из for
                        }
                    } // FOR10
                    // ---------------------------------------------------------------------- 
                    // SP12

                    ndiap = list_SP2_DistrIRI.Count;
                    // FOR11
                    for (sh_diap = 0; sh_diap < ndiap; sh_diap++)
                    {
                        if (
                            (mas_IRI[sh_IRI].F_IRI >= list_SP2_DistrIRI[sh_diap].Fmin) &&
                            (mas_IRI[sh_IRI].F_IRI <= list_SP2_DistrIRI[sh_diap].Fmax) &&
                            (mas_IRI[sh_IRI].Pel2 >= list_SP2_DistrIRI[sh_diap].Bmin) &&
                            (mas_IRI[sh_IRI].Pel2 <= list_SP2_DistrIRI[sh_diap].Bmax)
                           )
                        {
                            prizn2 = 1;
                            sh_diap = ndiap; // Выход из for
                        }
                    } // FOR11
                    // ---------------------------------------------------------------------- 
                    // Есть попадание в обе СП

                    if ((prizn1 == 1) && (prizn2 == 1))
                    {
                        // .....................................................................
                        // SP1

                        dX = mas_IRI[sh_IRI].X_IRI - mas_SP[0].X_SP;
                        dY = mas_IRI[sh_IRI].Y_IRI - mas_SP[0].Y_SP;
                        dZ = mas_IRI[sh_IRI].Z_IRI - mas_SP[0].Z_SP;
                        D_IRI_SP1 = Math.Sqrt(dX * dX + dY * dY + dZ * dZ);
                        // .....................................................................
                        // SP2

                        dX = mas_IRI[sh_IRI].X_IRI - mas_SP[1].X_SP;
                        dY = mas_IRI[sh_IRI].Y_IRI - mas_SP[1].Y_SP;
                        dZ = mas_IRI[sh_IRI].Z_IRI - mas_SP[1].Z_SP;
                        D_IRI_SP2 = Math.Sqrt(dX * dX + dY * dY + dZ * dZ);
                        // .....................................................................
                        if (D_IRI_SP1 <= D_IRI_SP2)
                        {
                            mas_IRI[sh_IRI].Prior1 = 4;
                            mas_IRI[sh_IRI].Prior2 = 3;
                        }

                        else
                        {
                            mas_IRI[sh_IRI].Prior1 = 3;
                            mas_IRI[sh_IRI].Prior2 = 4;
                        }

                        // .....................................................................
                    }  // if((prizn1==1)&&(prizn2==1))
                    // ---------------------------------------------------------------------- 
                    else if ((prizn1 == 1) && (prizn2 == 0))
                    {
                        mas_IRI[sh_IRI].Prior1 = 4;
                        mas_IRI[sh_IRI].Prior2 = 0;
                    }
                    // ---------------------------------------------------------------------- 
                    else if ((prizn1 == 0) && (prizn2 == 1))
                    {
                        mas_IRI[sh_IRI].Prior1 = 0;
                        mas_IRI[sh_IRI].Prior2 = 4;
                    }
                    // ---------------------------------------------------------------------- 
                    else
                    {
                        mas_IRI[sh_IRI].PriznPop = -1;
                    }
                    // ---------------------------------------------------------------------- 

                } // IF (есть F и координаты)

            } // FOR9 IRI
            // *********************************************************** PRIOR IRI XYZ

            // PRIOR IRI  **************************************************************
            // Приоритеты ИРИ без Координат

            // FOR12
            for (sh_IRI = 0; sh_IRI < nIRI; sh_IRI++)
            {

                // IF (есть F NO coordinates)
                if (
                    (mas_IRI[sh_IRI].F_IRI != 0) &&
                    (mas_IRI[sh_IRI].X_IRI == 0) &&
                    (mas_IRI[sh_IRI].Y_IRI == 0) &&
                    (mas_IRI[sh_IRI].Z_IRI == 0) &&
                    (mas_IRI[sh_IRI].PriznPop != -1)
                   )
                {

                    // ----------------------------------------------------------------------
                    // Информация пришла от СП1 


                    if (mas_IRI[sh_IRI].NSP1 == 1)
                    {
                        prizn1 = 0;
                        prizn2 = 0;

                        // Проверить СП1
                        ndiap = list_SP1_DistrIRI.Count;
                        // FOR
                        for (sh_diap = 0; sh_diap < ndiap; sh_diap++)
                        {
                            if (
                                (mas_IRI[sh_IRI].F_IRI >= list_SP1_DistrIRI[sh_diap].Fmin) &&
                                (mas_IRI[sh_IRI].F_IRI <= list_SP1_DistrIRI[sh_diap].Fmax) &&
                                (mas_IRI[sh_IRI].Pel1 >= list_SP1_DistrIRI[sh_diap].Bmin) &&
                                (mas_IRI[sh_IRI].Pel1 <= list_SP1_DistrIRI[sh_diap].Bmax)
                               )
                            {
                                prizn1 = 1;
                                sh_diap = ndiap; // Выход из for
                            }
                        } // FOR

                        // Проверить СП2
                        ndiap = list_SP2_DistrIRI.Count;
                        // FOR
                        for (sh_diap = 0; sh_diap < ndiap; sh_diap++)
                        {
                            if (
                                (mas_IRI[sh_IRI].F_IRI >= list_SP2_DistrIRI[sh_diap].Fmin) &&
                                (mas_IRI[sh_IRI].F_IRI <= list_SP2_DistrIRI[sh_diap].Fmax)
                                //(mas_IRI[sh_IRI].Pel1 >= list_SP1_DistrIRI[sh_diap].Bmin) && // ??????????????
                                //(mas_IRI[sh_IRI].Pel1 <= list_SP1_DistrIRI[sh_diap].Bmax)
                               )
                            {
                                prizn2 = 1;
                                sh_diap = ndiap; // Выход из for
                            }
                        } // FOR

                        if ((prizn1 == 1) && (prizn2 == 1))
                        {
                            mas_IRI[sh_IRI].Prior1 = 2;
                            mas_IRI[sh_IRI].Prior2 = 1;
                        }
                        else if ((prizn1 == 0) && (prizn2 == 1))
                        {
                            mas_IRI[sh_IRI].Prior1 = 0;
                            mas_IRI[sh_IRI].Prior2 = 1; // ????
                        }

                        else if ((prizn1 == 1) && (prizn2 == 0))
                        {
                            mas_IRI[sh_IRI].Prior1 = 2;
                            mas_IRI[sh_IRI].Prior2 = 0;
                        }

                        else
                        {
                            mas_IRI[sh_IRI].PriznPop = -1;
                        }


                    } // Информация пришла от СП1
                    // ---------------------------------------------------------------------- 
                    // Информация пришла от СП2 

                    if (mas_IRI[sh_IRI].NSP1 == 2)
                    {
                        prizn1 = 0;
                        prizn2 = 0;

                        // Проверить СП1
                        ndiap = list_SP1_DistrIRI.Count;
                        // FOR
                        for (sh_diap = 0; sh_diap < ndiap; sh_diap++)
                        {
                            if (
                                (mas_IRI[sh_IRI].F_IRI >= list_SP1_DistrIRI[sh_diap].Fmin) &&
                                (mas_IRI[sh_IRI].F_IRI <= list_SP1_DistrIRI[sh_diap].Fmax)
                                //(mas_IRI[sh_IRI].Pel1 >= list_SP1_DistrIRI[sh_diap].Bmin) &&
                                //(mas_IRI[sh_IRI].Pel1 <= list_SP1_DistrIRI[sh_diap].Bmax)
                               )
                            {
                                prizn1 = 1;
                                sh_diap = ndiap; // Выход из for
                            }
                        } // FOR

                        // Проверить СП2
                        ndiap = list_SP2_DistrIRI.Count;
                        // FOR
                        for (sh_diap = 0; sh_diap < ndiap; sh_diap++)
                        {
                            if (
                                (mas_IRI[sh_IRI].F_IRI >= list_SP2_DistrIRI[sh_diap].Fmin) &&
                                (mas_IRI[sh_IRI].F_IRI <= list_SP2_DistrIRI[sh_diap].Fmax) &&
                                (mas_IRI[sh_IRI].Pel1 >= list_SP2_DistrIRI[sh_diap].Bmin) &&
                                (mas_IRI[sh_IRI].Pel1 <= list_SP2_DistrIRI[sh_diap].Bmax)
                               )
                            {
                                prizn2 = 1;
                                sh_diap = ndiap; // Выход из for
                            }
                        } // FOR

                        if ((prizn1 == 1) && (prizn2 == 1))
                        {
                            mas_IRI[sh_IRI].Prior1 = 1;
                            mas_IRI[sh_IRI].Prior2 = 2;
                        }
                        else if ((prizn1 == 0) && (prizn2 == 1))
                        {
                            mas_IRI[sh_IRI].Prior1 = 0;
                            mas_IRI[sh_IRI].Prior2 = 2;
                        }

                        else if ((prizn1 == 1) && (prizn2 == 0))
                        {
                            mas_IRI[sh_IRI].Prior1 = 1; // ????
                            mas_IRI[sh_IRI].Prior2 = 0;
                        }

                        else
                        {
                            mas_IRI[sh_IRI].PriznPop = -1;
                        }


                    } // Информация пришла от СП2
                    // ---------------------------------------------------------------------- 

                } // IF  (есть F NO coordinates)

            } // FOR12 IRI
            // ************************************************************** PRIOR IRI 


            // !!!RASPREDELENIE !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

            Count_IRI = 0;

            // SP1_4 ******************************************************************
            // Распределяем на СП1 все подходящие по интервалам частот и углов ИРИ с
            // приоритетом 4 для СП1

            // FOR13
            for (sh_IRI = 0; sh_IRI < nIRI; sh_IRI++)
            {
                // IF (PriznPop!=-1&&Prior1==4)
                if (
                    (mas_IRI[sh_IRI].PriznPop != -1) &&
                    (mas_IRI[sh_IRI].Prior1 == 4)
                   )
                {

                    // Найти нужный диапазон в СП1
                    ndiap = list_SP1_DistrIRI.Count;
                    // FOR14
                    for (sh_diap = 0; sh_diap < ndiap; sh_diap++)
                    {
                        if (
                            (mas_IRI[sh_IRI].F_IRI >= list_SP1_DistrIRI[sh_diap].Fmin) &&
                            (mas_IRI[sh_IRI].F_IRI <= list_SP1_DistrIRI[sh_diap].Fmax) &&
                            (mas_IRI[sh_IRI].Pel1 >= list_SP1_DistrIRI[sh_diap].Bmin) &&
                            (mas_IRI[sh_IRI].Pel1 <= list_SP1_DistrIRI[sh_diap].Bmax)
                           )
                        {
                            // -------------------------------------------------------
                            // Индекс нужного диапазона

                            indSP = sh_diap;
                            // -------------------------------------------------------
                            // Ищем места по этой литере

                            // FOR15
                            for (shKan = 0; shKan < list_KanSP1_DistrIRI.Count; shKan++)
                            {
                                if (
                                    (list_KanSP1_DistrIRI[shKan].NumLit == list_SP1_DistrIRI[indSP].NumLit) &&
                                    (list_KanSP1_DistrIRI[shKan].flKan != 0)
                                  )
                                {
                                    indKan = shKan;
                                    shKan = list_KanSP1_DistrIRI.Count; // For OOUT from FOR

                                    // Уменьшаем число мест по литере
                                    num_kan = list_KanSP1_DistrIRI[indKan].flKan;
                                    num_kan -= 1;
                                    objS_Kan_DistrIRI.flKan = num_kan;
                                    num_kan = list_KanSP1_DistrIRI[indKan].NumLit;
                                    objS_Kan_DistrIRI.NumLit = num_kan;
                                    list_KanSP1_DistrIRI.RemoveAt(indKan);
                                    list_KanSP1_DistrIRI.Insert(indKan, objS_Kan_DistrIRI);

                                    // Присваиваем этот ИРИ 1-й станции
                                    mas_IRI[sh_IRI].NSPRP = mas_SP[0].N_SP;
                                    mas_IRI[sh_IRI].NKanRP = list_KanSP1_DistrIRI[indKan].NumLit; // Litera
                                    mas_IRI[sh_IRI].PriznPop = 1;
                                    Count_IRI++;

                                } // Litera is free

                            } // FOR15 Kan
                            // -------------------------------------------------------
                            if (mas_IRI[sh_IRI].PriznPop == 1) // ИРИ распределился
                                sh_diap = ndiap; // Выход из for

                        }

                    } // FOR 14 Diapazon


                } // IF (PriznPop!=-1&&Prior1==4)


            } // FOR 13 IRI
            // ****************************************************************** SP1_4

            // SP2_4 ******************************************************************
            // Распределяем на СП2 все подходящие по интервалам частот и углов ИРИ с
            // приоритетом 4 для СП2

            // FOR16
            for (sh_IRI = 0; sh_IRI < nIRI; sh_IRI++)
            {
                // IF (PriznPop!=-1&&Prior2==4&&PriznPop!=1)
                if (
                    (mas_IRI[sh_IRI].PriznPop != -1) &&
                    (mas_IRI[sh_IRI].PriznPop != 1) &&  // ИРИ распределился
                    (mas_IRI[sh_IRI].Prior2 == 4)
                   )
                {

                    // Найти нужный диапазон в СП2
                    ndiap = list_SP2_DistrIRI.Count;
                    // FOR17
                    for (sh_diap = 0; sh_diap < ndiap; sh_diap++)
                    {
                        if (
                            (mas_IRI[sh_IRI].F_IRI >= list_SP2_DistrIRI[sh_diap].Fmin) &&
                            (mas_IRI[sh_IRI].F_IRI <= list_SP2_DistrIRI[sh_diap].Fmax) &&
                            (mas_IRI[sh_IRI].Pel2 >= list_SP2_DistrIRI[sh_diap].Bmin) &&
                            (mas_IRI[sh_IRI].Pel2 <= list_SP2_DistrIRI[sh_diap].Bmax)
                           )
                        {
                            // -------------------------------------------------------
                            // Индекс нужного диапазона

                            indSP = sh_diap;
                            // -------------------------------------------------------
                            // Ищем места по этой литере

                            // FOR18
                            for (shKan = 0; shKan < list_KanSP2_DistrIRI.Count; shKan++)
                            {
                                if (
                                    (list_KanSP2_DistrIRI[shKan].NumLit == list_SP2_DistrIRI[indSP].NumLit) &&
                                    (list_KanSP2_DistrIRI[shKan].flKan != 0)
                                  )
                                {
                                    indKan = shKan;
                                    shKan = list_KanSP2_DistrIRI.Count; // For OOUT from FOR

                                    // Уменьшаем число мест по литере
                                    num_kan = list_KanSP2_DistrIRI[indKan].flKan;
                                    num_kan -= 1;
                                    objS_Kan_DistrIRI.flKan = num_kan;
                                    num_kan = list_KanSP2_DistrIRI[indKan].NumLit;
                                    objS_Kan_DistrIRI.NumLit = num_kan;
                                    list_KanSP2_DistrIRI.RemoveAt(indKan);
                                    list_KanSP2_DistrIRI.Insert(indKan, objS_Kan_DistrIRI);

                                    // Присваиваем этот ИРИ 2-й станции
                                    mas_IRI[sh_IRI].NSPRP = mas_SP[1].N_SP;
                                    mas_IRI[sh_IRI].NKanRP = list_KanSP2_DistrIRI[indKan].NumLit; // Litera
                                    mas_IRI[sh_IRI].PriznPop = 1;
                                    Count_IRI++;

                                } // Litera is free

                            } // FOR18 Kan
                            // -------------------------------------------------------
                            if (mas_IRI[sh_IRI].PriznPop == 1) // ИРИ распределился
                                sh_diap = ndiap; // Выход из for

                        }

                    } // FOR 17 Diapazon


                } // IF (PriznPop!=-1&&Prior2==4&&PriznPop!=1)


            } // FOR 16 IRI
            // ****************************************************************** SP2_4

            // SP1(4)->SP2(3) *******************************************************
            // Если остались ИРИ с приоритетом 4 для СП1, пытаемся перебросить их на 
            // СП2 (если по ней у них приоритет 3)

            // FOR19
            for (sh_IRI = 0; sh_IRI < nIRI; sh_IRI++)
            {
                // IF (PriznPop!=-1&&Prior1==4&&PriznPop!=1)
                if (
                    (mas_IRI[sh_IRI].PriznPop != -1) &&
                    (mas_IRI[sh_IRI].PriznPop != 1) &&  // ИРИ распределился
                    (mas_IRI[sh_IRI].Prior1 == 4)
                   )
                {
                    // ---------------------------------------------------------------            
                    if (mas_IRI[sh_IRI].Prior2 == 0)
                        mas_IRI[sh_IRI].PriznPop = -1;
                    // ---------------------------------------------------------------            
                    else if (mas_IRI[sh_IRI].Prior2 == 3)
                    {

                        // Найти нужный диапазон в СП2
                        ndiap = list_SP2_DistrIRI.Count;
                        // FOR20
                        for (sh_diap = 0; sh_diap < ndiap; sh_diap++)
                        {
                            if (
                                (mas_IRI[sh_IRI].F_IRI >= list_SP2_DistrIRI[sh_diap].Fmin) &&
                                (mas_IRI[sh_IRI].F_IRI <= list_SP2_DistrIRI[sh_diap].Fmax) &&
                                (mas_IRI[sh_IRI].Pel2 >= list_SP2_DistrIRI[sh_diap].Bmin) &&
                                (mas_IRI[sh_IRI].Pel2 <= list_SP2_DistrIRI[sh_diap].Bmax)
                               )
                            {
                                // .........................................................
                                // Индекс нужного диапазона

                                indSP = sh_diap;
                                // .........................................................
                                // Ищем места по этой литере

                                // FOR21
                                for (shKan = 0; shKan < list_KanSP2_DistrIRI.Count; shKan++)
                                {
                                    if (
                                        (list_KanSP2_DistrIRI[shKan].NumLit == list_SP2_DistrIRI[indSP].NumLit) &&
                                        (list_KanSP2_DistrIRI[shKan].flKan != 0)
                                      )
                                    {
                                        indKan = shKan;
                                        shKan = list_KanSP2_DistrIRI.Count; // For OOUT from FOR

                                        // Уменьшаем число мест по литере
                                        num_kan = list_KanSP2_DistrIRI[indKan].flKan;
                                        num_kan -= 1;
                                        objS_Kan_DistrIRI.flKan = num_kan;
                                        num_kan = list_KanSP2_DistrIRI[indKan].NumLit;
                                        objS_Kan_DistrIRI.NumLit = num_kan;
                                        list_KanSP2_DistrIRI.RemoveAt(indKan);
                                        list_KanSP2_DistrIRI.Insert(indKan, objS_Kan_DistrIRI);

                                        // Присваиваем этот ИРИ 2-й станции
                                        mas_IRI[sh_IRI].NSPRP = mas_SP[1].N_SP;
                                        mas_IRI[sh_IRI].NKanRP = list_KanSP2_DistrIRI[indKan].NumLit; // Litera
                                        mas_IRI[sh_IRI].PriznPop = 1;
                                        Count_IRI++;

                                    } // Litera is free

                                } // FOR21 Kan
                                // .........................................................
                                if (mas_IRI[sh_IRI].PriznPop == 1) // ИРИ распределился
                                    sh_diap = ndiap; // Выход из for
                                else
                                {
                                    sh_diap = ndiap;
                                    mas_IRI[sh_IRI].PriznPop = -1;
                                }

                            } // IF(попал в диапазон)

                        } // FOR 20 Diapazon

                    } // IF(Prior2==3)
                    // ---------------------------------------------------------------            

                } // IF (PriznPop!=-1&&Prior1==4&&PriznPop!=1)

            } // FOR 19 IRI
            // ******************************************************** SP1(4)->SP2(3)

            // SP2(4)->SP1(3) *******************************************************
            // Если остались ИРИ с приоритетом 4 для СП2, пытаемся перебросить их на 
            // СП1 (если по ней у них приоритет 3)

            // FOR22
            for (sh_IRI = 0; sh_IRI < nIRI; sh_IRI++)
            {
                // IF (PriznPop!=-1&&Prior2==4&&PriznPop!=1)
                if (
                    (mas_IRI[sh_IRI].PriznPop != -1) &&
                    (mas_IRI[sh_IRI].PriznPop != 1) &&  // ИРИ распределился
                    (mas_IRI[sh_IRI].Prior2 == 4)
                   )
                {
                    // ---------------------------------------------------------------            
                    if (mas_IRI[sh_IRI].Prior1 == 0)
                        mas_IRI[sh_IRI].PriznPop = -1;
                    // ---------------------------------------------------------------            
                    else if (mas_IRI[sh_IRI].Prior1 == 3)
                    {

                        // Найти нужный диапазон в СП1
                        ndiap = list_SP1_DistrIRI.Count;
                        // FOR23
                        for (sh_diap = 0; sh_diap < ndiap; sh_diap++)
                        {
                            if (
                                (mas_IRI[sh_IRI].F_IRI >= list_SP1_DistrIRI[sh_diap].Fmin) &&
                                (mas_IRI[sh_IRI].F_IRI <= list_SP1_DistrIRI[sh_diap].Fmax) &&
                                (mas_IRI[sh_IRI].Pel1 >= list_SP1_DistrIRI[sh_diap].Bmin) &&
                                (mas_IRI[sh_IRI].Pel1 <= list_SP1_DistrIRI[sh_diap].Bmax)
                               )
                            {
                                // .........................................................
                                // Индекс нужного диапазона

                                indSP = sh_diap;
                                // .........................................................
                                // Ищем места по этой литере

                                // FOR24
                                for (shKan = 0; shKan < list_KanSP1_DistrIRI.Count; shKan++)
                                {
                                    if (
                                        (list_KanSP1_DistrIRI[shKan].NumLit == list_SP1_DistrIRI[indSP].NumLit) &&
                                        (list_KanSP1_DistrIRI[shKan].flKan != 0)
                                      )
                                    {
                                        indKan = shKan;
                                        shKan = list_KanSP1_DistrIRI.Count; // For OOUT from FOR

                                        // Уменьшаем число мест по литере
                                        num_kan = list_KanSP1_DistrIRI[indKan].flKan;
                                        num_kan -= 1;
                                        objS_Kan_DistrIRI.flKan = num_kan;
                                        num_kan = list_KanSP1_DistrIRI[indKan].NumLit;
                                        objS_Kan_DistrIRI.NumLit = num_kan;
                                        list_KanSP1_DistrIRI.RemoveAt(indKan);
                                        list_KanSP1_DistrIRI.Insert(indKan, objS_Kan_DistrIRI);

                                        // Присваиваем этот ИРИ 2-й станции
                                        mas_IRI[sh_IRI].NSPRP = mas_SP[0].N_SP;
                                        mas_IRI[sh_IRI].NKanRP = list_KanSP1_DistrIRI[indKan].NumLit; // Litera
                                        mas_IRI[sh_IRI].PriznPop = 1;
                                        Count_IRI++;

                                    } // Litera is free

                                } // FOR24 Kan
                                // .........................................................
                                if (mas_IRI[sh_IRI].PriznPop == 1) // ИРИ распределился
                                    sh_diap = ndiap; // Выход из for
                                else
                                {
                                    sh_diap = ndiap;
                                    mas_IRI[sh_IRI].PriznPop = -1;
                                }

                            } // IF(попал в диапазон)

                        } // FOR 23 Diapazon

                    } // IF(Prior1==3)
                    // ---------------------------------------------------------------            

                } // IF (PriznPop!=-1&&Prior2==4&&PriznPop!=1)

            } // FOR 22 IRI
            // ******************************************************** SP2(4)->SP1(3)

            // SP1_2 ******************************************************************
            // Распределяем на СП1 все подходящие по интервалам частот и углов ИРИ с
            // приоритетом 2 для СП1

            // FOR25
            for (sh_IRI = 0; sh_IRI < nIRI; sh_IRI++)
            {
                // IF (PriznPop!=-1&&Prior1==2&&PriznPop!=1)
                if (
                    (mas_IRI[sh_IRI].PriznPop != -1) &&
                    (mas_IRI[sh_IRI].PriznPop != 1) &&  // ИРИ распределился
                    (mas_IRI[sh_IRI].Prior1 == 2)
                   )
                {

                    // Найти нужный диапазон в СП1
                    ndiap = list_SP1_DistrIRI.Count;
                    // FOR26
                    for (sh_diap = 0; sh_diap < ndiap; sh_diap++)
                    {
                        if (
                            (mas_IRI[sh_IRI].F_IRI >= list_SP1_DistrIRI[sh_diap].Fmin) &&
                            (mas_IRI[sh_IRI].F_IRI <= list_SP1_DistrIRI[sh_diap].Fmax) &&
                            (mas_IRI[sh_IRI].Pel1 >= list_SP1_DistrIRI[sh_diap].Bmin) &&
                            (mas_IRI[sh_IRI].Pel1 <= list_SP1_DistrIRI[sh_diap].Bmax)
                           )
                        {
                            // -------------------------------------------------------
                            // Индекс нужного диапазона

                            indSP = sh_diap;
                            // -------------------------------------------------------
                            // Ищем места по этой литере

                            // FOR27
                            for (shKan = 0; shKan < list_KanSP1_DistrIRI.Count; shKan++)
                            {
                                if (
                                    (list_KanSP1_DistrIRI[shKan].NumLit == list_SP1_DistrIRI[indSP].NumLit) &&
                                    (list_KanSP1_DistrIRI[shKan].flKan != 0)
                                  )
                                {
                                    indKan = shKan;
                                    shKan = list_KanSP1_DistrIRI.Count; // For OOUT from FOR

                                    // Уменьшаем число мест по литере
                                    num_kan = list_KanSP1_DistrIRI[indKan].flKan;
                                    num_kan -= 1;
                                    objS_Kan_DistrIRI.flKan = num_kan;
                                    num_kan = list_KanSP1_DistrIRI[indKan].NumLit;
                                    objS_Kan_DistrIRI.NumLit = num_kan;
                                    list_KanSP1_DistrIRI.RemoveAt(indKan);
                                    list_KanSP1_DistrIRI.Insert(indKan, objS_Kan_DistrIRI);

                                    // Присваиваем этот ИРИ 1-й станции
                                    mas_IRI[sh_IRI].NSPRP = mas_SP[0].N_SP;
                                    mas_IRI[sh_IRI].NKanRP = list_KanSP1_DistrIRI[indKan].NumLit; // Litera
                                    mas_IRI[sh_IRI].PriznPop = 1;
                                    Count_IRI++;

                                } // Litera is free

                            } // FOR27 Kan
                            // -------------------------------------------------------
                            if (mas_IRI[sh_IRI].PriznPop == 1) // ИРИ распределился
                                sh_diap = ndiap; // Выход из for

                        }

                    } // FOR 26 Diapazon


                } // IF (PriznPop!=-1&&Prior1==2PriznPop!=1)


            } // FOR 25 IRI
            // ****************************************************************** SP1_2

            // SP2_2 ******************************************************************
            // Распределяем на СП2 все подходящие по интервалам частот и углов ИРИ с
            // приоритетом 2 для СП2

            // FOR28
            for (sh_IRI = 0; sh_IRI < nIRI; sh_IRI++)
            {
                // IF (PriznPop!=-1&&Prior2==2&&PriznPop!=1)
                if (
                    (mas_IRI[sh_IRI].PriznPop != -1) &&
                    (mas_IRI[sh_IRI].PriznPop != 1) &&  // ИРИ распределился
                    (mas_IRI[sh_IRI].Prior2 == 2)
                   )
                {

                    // Найти нужный диапазон в СП2
                    ndiap = list_SP2_DistrIRI.Count;
                    // FOR29
                    for (sh_diap = 0; sh_diap < ndiap; sh_diap++)
                    {
                        if (
                            (mas_IRI[sh_IRI].F_IRI >= list_SP2_DistrIRI[sh_diap].Fmin) &&
                            (mas_IRI[sh_IRI].F_IRI <= list_SP2_DistrIRI[sh_diap].Fmax) &&
                            (mas_IRI[sh_IRI].Pel1 >= list_SP2_DistrIRI[sh_diap].Bmin) &&
                            (mas_IRI[sh_IRI].Pel1 <= list_SP2_DistrIRI[sh_diap].Bmax)
                           )
                        {
                            // -------------------------------------------------------
                            // Индекс нужного диапазона

                            indSP = sh_diap;
                            // -------------------------------------------------------
                            // Ищем места по этой литере

                            // FOR30
                            for (shKan = 0; shKan < list_KanSP2_DistrIRI.Count; shKan++)
                            {
                                if (
                                    (list_KanSP2_DistrIRI[shKan].NumLit == list_SP2_DistrIRI[indSP].NumLit) &&
                                    (list_KanSP2_DistrIRI[shKan].flKan != 0)
                                  )
                                {
                                    indKan = shKan;
                                    shKan = list_KanSP2_DistrIRI.Count; // For OOUT from FOR

                                    // Уменьшаем число мест по литере
                                    num_kan = list_KanSP2_DistrIRI[indKan].flKan;
                                    num_kan -= 1;
                                    objS_Kan_DistrIRI.flKan = num_kan;
                                    num_kan = list_KanSP2_DistrIRI[indKan].NumLit;
                                    objS_Kan_DistrIRI.NumLit = num_kan;
                                    list_KanSP2_DistrIRI.RemoveAt(indKan);
                                    list_KanSP2_DistrIRI.Insert(indKan, objS_Kan_DistrIRI);

                                    // Присваиваем этот ИРИ 2-й станции
                                    mas_IRI[sh_IRI].NSPRP = mas_SP[1].N_SP;
                                    mas_IRI[sh_IRI].NKanRP = list_KanSP2_DistrIRI[indKan].NumLit; // Litera
                                    mas_IRI[sh_IRI].PriznPop = 1;
                                    Count_IRI++;

                                } // Litera is free

                            } // FOR30 Kan
                            // -------------------------------------------------------
                            if (mas_IRI[sh_IRI].PriznPop == 1) // ИРИ распределился
                                sh_diap = ndiap; // Выход из for

                        }

                    } // FOR 29 Diapazon


                } // IF (PriznPop!=-1&&Prior2==4&&PriznPop!=1)


            } // FOR 28 IRI
            // ****************************************************************** SP2_2

            // SP1(2)->SP2(1) *******************************************************
            // Если остались ИРИ с приоритетом 2 для СП1, пытаемся перебросить их на 
            // СП1 (если по ней у них приоритет 1)

            // FOR31
            for (sh_IRI = 0; sh_IRI < nIRI; sh_IRI++)
            {
                // IF (PriznPop!=-1&&Prior1==4&&PriznPop!=1)
                if (
                    (mas_IRI[sh_IRI].PriznPop != -1) &&
                    (mas_IRI[sh_IRI].PriznPop != 1) &&  // ИРИ распределился
                    (mas_IRI[sh_IRI].Prior1 == 2)
                   )
                {
                    // ---------------------------------------------------------------            
                    if (mas_IRI[sh_IRI].Prior2 == 0)
                        mas_IRI[sh_IRI].PriznPop = -1;
                    // ---------------------------------------------------------------            
                    else if (mas_IRI[sh_IRI].Prior2 == 1)
                    {

                        // Найти нужный диапазон в СП2
                        ndiap = list_SP2_DistrIRI.Count;
                        // FOR32
                        for (sh_diap = 0; sh_diap < ndiap; sh_diap++)
                        {
                            if (
                                (mas_IRI[sh_IRI].F_IRI >= list_SP2_DistrIRI[sh_diap].Fmin) &&
                                (mas_IRI[sh_IRI].F_IRI <= list_SP2_DistrIRI[sh_diap].Fmax)
                                //(mas_IRI[sh_IRI].Pel2 >= list_SP2_DistrIRI[sh_diap].Bmin) &&
                                //(mas_IRI[sh_IRI].Pel2 <= list_SP2_DistrIRI[sh_diap].Bmax)
                               )
                            {
                                // .........................................................
                                // Индекс нужного диапазона

                                indSP = sh_diap;
                                // .........................................................
                                // Ищем места по этой литере

                                // FOR33
                                for (shKan = 0; shKan < list_KanSP2_DistrIRI.Count; shKan++)
                                {
                                    if (
                                        (list_KanSP2_DistrIRI[shKan].NumLit == list_SP2_DistrIRI[indSP].NumLit) &&
                                        (list_KanSP2_DistrIRI[shKan].flKan != 0)
                                      )
                                    {
                                        indKan = shKan;
                                        shKan = list_KanSP2_DistrIRI.Count; // For OOUT from FOR

                                        // Уменьшаем число мест по литере
                                        num_kan = list_KanSP2_DistrIRI[indKan].flKan;
                                        num_kan -= 1;
                                        objS_Kan_DistrIRI.flKan = num_kan;
                                        num_kan = list_KanSP2_DistrIRI[indKan].NumLit;
                                        objS_Kan_DistrIRI.NumLit = num_kan;
                                        list_KanSP2_DistrIRI.RemoveAt(indKan);
                                        list_KanSP2_DistrIRI.Insert(indKan, objS_Kan_DistrIRI);

                                        // Присваиваем этот ИРИ 2-й станции
                                        mas_IRI[sh_IRI].NSPRP = mas_SP[1].N_SP;
                                        mas_IRI[sh_IRI].NKanRP = list_KanSP2_DistrIRI[indKan].NumLit; // Litera
                                        mas_IRI[sh_IRI].PriznPop = 1;
                                        Count_IRI++;

                                    } // Litera is free

                                } // FOR33 Kan
                                // .........................................................
                                if (mas_IRI[sh_IRI].PriznPop == 1) // ИРИ распределился
                                    sh_diap = ndiap; // Выход из for
                                else
                                {
                                    sh_diap = ndiap;
                                    mas_IRI[sh_IRI].PriznPop = -1;
                                }

                            } // IF(попал в диапазон)

                        } // FOR 32 Diapazon

                    } // IF(Prior2==3)
                    // ---------------------------------------------------------------            

                } // IF (PriznPop!=-1&&Prior1==4&&PriznPop!=1)

            } // FOR 31 IRI
            // ******************************************************** SP1(2)->SP2(1)

            // SP2(2)->SP1(1) *******************************************************
            // Если остались ИРИ с приоритетом 2 для СП2, пытаемся перебросить их на 
            // СП1 (если по ней у них приоритет 1)

            // FOR34
            for (sh_IRI = 0; sh_IRI < nIRI; sh_IRI++)
            {
                // IF (PriznPop!=-1&&Prior2==4&&PriznPop!=1)
                if (
                    (mas_IRI[sh_IRI].PriznPop != -1) &&
                    (mas_IRI[sh_IRI].PriznPop != 1) &&  // ИРИ распределился
                    (mas_IRI[sh_IRI].Prior2 == 2)
                   )
                {
                    // ---------------------------------------------------------------            
                    if (mas_IRI[sh_IRI].Prior1 == 0)
                        mas_IRI[sh_IRI].PriznPop = -1;
                    // ---------------------------------------------------------------            
                    else if (mas_IRI[sh_IRI].Prior1 == 1)
                    {

                        // Найти нужный диапазон в СП1
                        ndiap = list_SP1_DistrIRI.Count;
                        // FOR35
                        for (sh_diap = 0; sh_diap < ndiap; sh_diap++)
                        {
                            if (
                                (mas_IRI[sh_IRI].F_IRI >= list_SP1_DistrIRI[sh_diap].Fmin) &&
                                (mas_IRI[sh_IRI].F_IRI <= list_SP1_DistrIRI[sh_diap].Fmax)
                                //(mas_IRI[sh_IRI].Pel1 >= list_SP1_DistrIRI[sh_diap].Bmin) &&
                                //(mas_IRI[sh_IRI].Pel1 <= list_SP1_DistrIRI[sh_diap].Bmax)
                               )
                            {
                                // .........................................................
                                // Индекс нужного диапазона

                                indSP = sh_diap;
                                // .........................................................
                                // Ищем места по этой литере

                                // FOR36
                                for (shKan = 0; shKan < list_KanSP1_DistrIRI.Count; shKan++)
                                {
                                    if (
                                        (list_KanSP1_DistrIRI[shKan].NumLit == list_SP1_DistrIRI[indSP].NumLit) &&
                                        (list_KanSP1_DistrIRI[shKan].flKan != 0)
                                      )
                                    {
                                        indKan = shKan;
                                        shKan = list_KanSP1_DistrIRI.Count; // For OOUT from FOR

                                        // Уменьшаем число мест по литере
                                        num_kan = list_KanSP1_DistrIRI[indKan].flKan;
                                        num_kan -= 1;
                                        objS_Kan_DistrIRI.flKan = num_kan;
                                        num_kan = list_KanSP1_DistrIRI[indKan].NumLit;
                                        objS_Kan_DistrIRI.NumLit = num_kan;
                                        list_KanSP1_DistrIRI.RemoveAt(indKan);
                                        list_KanSP1_DistrIRI.Insert(indKan, objS_Kan_DistrIRI);

                                        // Присваиваем этот ИРИ 2-й станции
                                        mas_IRI[sh_IRI].NSPRP = mas_SP[0].N_SP;
                                        mas_IRI[sh_IRI].NKanRP = list_KanSP1_DistrIRI[indKan].NumLit; // Litera
                                        mas_IRI[sh_IRI].PriznPop = 1;
                                        Count_IRI++;

                                    } // Litera is free

                                } // FOR36 Kan
                                // .........................................................
                                if (mas_IRI[sh_IRI].PriznPop == 1) // ИРИ распределился
                                    sh_diap = ndiap; // Выход из for
                                else
                                {
                                    sh_diap = ndiap;
                                    mas_IRI[sh_IRI].PriznPop = -1;
                                }

                            } // IF(попал в диапазон)

                        } // FOR 35 Diapazon

                    } // IF(Prior1==3)
                    // ---------------------------------------------------------------            

                } // IF (PriznPop!=-1&&Prior2==4&&PriznPop!=1)

            } // FOR 34 IRI
            // ******************************************************** SP2(2)->SP1(1)

            // SP1_3 ******************************************************************
            // Ostatki SP1,Prior3

            // FOR33
            for (sh_IRI = 0; sh_IRI < nIRI; sh_IRI++)
            {
                // IF (PriznPop!=-1&&Prior1==3)
                if (
                    (mas_IRI[sh_IRI].PriznPop != -1) &&
                    (mas_IRI[sh_IRI].Prior1 == 3) &&
                    (mas_IRI[sh_IRI].PriznPop != 1)   // ИРИ распределился
                   )
                {

                    // Найти нужный диапазон в СП1
                    ndiap = list_SP1_DistrIRI.Count;
                    // FOR14
                    for (sh_diap = 0; sh_diap < ndiap; sh_diap++)
                    {
                        if (
                            (mas_IRI[sh_IRI].F_IRI >= list_SP1_DistrIRI[sh_diap].Fmin) &&
                            (mas_IRI[sh_IRI].F_IRI <= list_SP1_DistrIRI[sh_diap].Fmax) &&
                            (mas_IRI[sh_IRI].Pel1 >= list_SP1_DistrIRI[sh_diap].Bmin) &&
                            (mas_IRI[sh_IRI].Pel1 <= list_SP1_DistrIRI[sh_diap].Bmax)
                           )
                        {
                            // -------------------------------------------------------
                            // Индекс нужного диапазона

                            indSP = sh_diap;
                            // -------------------------------------------------------
                            // Ищем места по этой литере

                            // FOR15
                            for (shKan = 0; shKan < list_KanSP1_DistrIRI.Count; shKan++)
                            {
                                if (
                                    (list_KanSP1_DistrIRI[shKan].NumLit == list_SP1_DistrIRI[indSP].NumLit) &&
                                    (list_KanSP1_DistrIRI[shKan].flKan != 0)
                                  )
                                {
                                    indKan = shKan;
                                    shKan = list_KanSP1_DistrIRI.Count; // For OOUT from FOR

                                    // Уменьшаем число мест по литере
                                    num_kan = list_KanSP1_DistrIRI[indKan].flKan;
                                    num_kan -= 1;
                                    objS_Kan_DistrIRI.flKan = num_kan;
                                    num_kan = list_KanSP1_DistrIRI[indKan].NumLit;
                                    objS_Kan_DistrIRI.NumLit = num_kan;
                                    list_KanSP1_DistrIRI.RemoveAt(indKan);
                                    list_KanSP1_DistrIRI.Insert(indKan, objS_Kan_DistrIRI);

                                    // Присваиваем этот ИРИ 1-й станции
                                    mas_IRI[sh_IRI].NSPRP = mas_SP[0].N_SP;
                                    mas_IRI[sh_IRI].NKanRP = list_KanSP1_DistrIRI[indKan].NumLit; // Litera
                                    mas_IRI[sh_IRI].PriznPop = 1;
                                    Count_IRI++;

                                } // Litera is free

                            } // FOR15 Kan
                            // -------------------------------------------------------
                            if (mas_IRI[sh_IRI].PriznPop == 1) // ИРИ распределился
                                sh_diap = ndiap; // Выход из for

                        }

                    } // FOR 14 Diapazon


                } // IF (PriznPop!=-1&&Prior1==3)


            } // FOR 33 IRI
            // ****************************************************************** SP1_3

            // SP2_3 ******************************************************************
            // Ostatki SP2,Prior3

            // FOR33
            for (sh_IRI = 0; sh_IRI < nIRI; sh_IRI++)
            {
                // IF (PriznPop!=-1&&Prior1==3)
                if (
                    (mas_IRI[sh_IRI].PriznPop != -1) &&
                    (mas_IRI[sh_IRI].Prior2 == 3) &&
                    (mas_IRI[sh_IRI].PriznPop != 1)   // ИРИ распределился
                   )
                {

                    // Найти нужный диапазон в СП2
                    ndiap = list_SP2_DistrIRI.Count;
                    // FOR14
                    for (sh_diap = 0; sh_diap < ndiap; sh_diap++)
                    {
                        if (
                            (mas_IRI[sh_IRI].F_IRI >= list_SP2_DistrIRI[sh_diap].Fmin) &&
                            (mas_IRI[sh_IRI].F_IRI <= list_SP2_DistrIRI[sh_diap].Fmax) &&
                            (mas_IRI[sh_IRI].Pel1 >= list_SP2_DistrIRI[sh_diap].Bmin) &&
                            (mas_IRI[sh_IRI].Pel1 <= list_SP2_DistrIRI[sh_diap].Bmax)
                           )
                        {
                            // -------------------------------------------------------
                            // Индекс нужного диапазона

                            indSP = sh_diap;
                            // -------------------------------------------------------
                            // Ищем места по этой литере

                            // FOR15
                            for (shKan = 0; shKan < list_KanSP2_DistrIRI.Count; shKan++)
                            {
                                if (
                                    (list_KanSP2_DistrIRI[shKan].NumLit == list_SP2_DistrIRI[indSP].NumLit) &&
                                    (list_KanSP2_DistrIRI[shKan].flKan != 0)
                                  )
                                {
                                    indKan = shKan;
                                    shKan = list_KanSP2_DistrIRI.Count; // For OOUT from FOR

                                    // Уменьшаем число мест по литере
                                    num_kan = list_KanSP2_DistrIRI[indKan].flKan;
                                    num_kan -= 1;
                                    objS_Kan_DistrIRI.flKan = num_kan;
                                    num_kan = list_KanSP2_DistrIRI[indKan].NumLit;
                                    objS_Kan_DistrIRI.NumLit = num_kan;
                                    list_KanSP2_DistrIRI.RemoveAt(indKan);
                                    list_KanSP2_DistrIRI.Insert(indKan, objS_Kan_DistrIRI);

                                    // Присваиваем этот ИРИ 2-й станции
                                    mas_IRI[sh_IRI].NSPRP = mas_SP[1].N_SP;
                                    mas_IRI[sh_IRI].NKanRP = list_KanSP2_DistrIRI[indKan].NumLit; // Litera
                                    mas_IRI[sh_IRI].PriznPop = 1;
                                    Count_IRI++;

                                } // Litera is free

                            } // FOR15 Kan
                            // -------------------------------------------------------
                            if (mas_IRI[sh_IRI].PriznPop == 1) // ИРИ распределился
                                sh_diap = ndiap; // Выход из for

                        }

                    } // FOR 14 Diapazon


                } // IF (PriznPop!=-1&&Prior1==3)


            } // FOR 33 IRI
            // ****************************************************************** SP2_3

            // SP1_1 ******************************************************************
            // Ostatki SP1,Prior1

            // FOR33
            for (sh_IRI = 0; sh_IRI < nIRI; sh_IRI++)
            {
                // IF (PriznPop!=-1&&Prior1==3)
                if (
                    (mas_IRI[sh_IRI].PriznPop != -1) &&
                    (mas_IRI[sh_IRI].Prior1 == 1) &&
                    (mas_IRI[sh_IRI].PriznPop != 1)   // ИРИ распределился
                   )
                {

                    // Найти нужный диапазон в СП1
                    ndiap = list_SP1_DistrIRI.Count;
                    // FOR14
                    for (sh_diap = 0; sh_diap < ndiap; sh_diap++)
                    {
                        if (
                            (mas_IRI[sh_IRI].F_IRI >= list_SP1_DistrIRI[sh_diap].Fmin) &&
                            (mas_IRI[sh_IRI].F_IRI <= list_SP1_DistrIRI[sh_diap].Fmax) &&
                            (mas_IRI[sh_IRI].Pel1 >= list_SP1_DistrIRI[sh_diap].Bmin) &&
                            (mas_IRI[sh_IRI].Pel1 <= list_SP1_DistrIRI[sh_diap].Bmax)
                           )
                        {
                            // -------------------------------------------------------
                            // Индекс нужного диапазона

                            indSP = sh_diap;
                            // -------------------------------------------------------
                            // Ищем места по этой литере

                            // FOR15
                            for (shKan = 0; shKan < list_KanSP1_DistrIRI.Count; shKan++)
                            {
                                if (
                                    (list_KanSP1_DistrIRI[shKan].NumLit == list_SP1_DistrIRI[indSP].NumLit) &&
                                    (list_KanSP1_DistrIRI[shKan].flKan != 0)
                                  )
                                {
                                    indKan = shKan;
                                    shKan = list_KanSP1_DistrIRI.Count; // For OOUT from FOR

                                    // Уменьшаем число мест по литере
                                    num_kan = list_KanSP1_DistrIRI[indKan].flKan;
                                    num_kan -= 1;
                                    objS_Kan_DistrIRI.flKan = num_kan;
                                    num_kan = list_KanSP1_DistrIRI[indKan].NumLit;
                                    objS_Kan_DistrIRI.NumLit = num_kan;
                                    list_KanSP1_DistrIRI.RemoveAt(indKan);
                                    list_KanSP1_DistrIRI.Insert(indKan, objS_Kan_DistrIRI);

                                    // Присваиваем этот ИРИ 1-й станции
                                    mas_IRI[sh_IRI].NSPRP = mas_SP[0].N_SP;
                                    mas_IRI[sh_IRI].NKanRP = list_KanSP1_DistrIRI[indKan].NumLit; // Litera
                                    mas_IRI[sh_IRI].PriznPop = 1;
                                    Count_IRI++;

                                } // Litera is free

                            } // FOR15 Kan
                            // -------------------------------------------------------
                            if (mas_IRI[sh_IRI].PriznPop == 1) // ИРИ распределился
                                sh_diap = ndiap; // Выход из for

                        }

                    } // FOR 14 Diapazon


                } // IF (PriznPop!=-1&&Prior1==3)


            } // FOR 33 IRI
            // ****************************************************************** SP1_1

            // SP2_1 ******************************************************************
            // Ostatki SP2,Prior1

            // FOR33
            for (sh_IRI = 0; sh_IRI < nIRI; sh_IRI++)
            {
                // IF (PriznPop!=-1&&Prior1==3)
                if (
                    (mas_IRI[sh_IRI].PriznPop != -1) &&
                    (mas_IRI[sh_IRI].Prior2 == 1) &&
                    (mas_IRI[sh_IRI].PriznPop != 1)   // ИРИ распределился
                   )
                {

                    // Найти нужный диапазон в СП2
                    ndiap = list_SP2_DistrIRI.Count;
                    // FOR14
                    for (sh_diap = 0; sh_diap < ndiap; sh_diap++)
                    {
                        if (
                            (mas_IRI[sh_IRI].F_IRI >= list_SP2_DistrIRI[sh_diap].Fmin) &&
                            (mas_IRI[sh_IRI].F_IRI <= list_SP2_DistrIRI[sh_diap].Fmax) &&
                            (mas_IRI[sh_IRI].Pel1 >= list_SP2_DistrIRI[sh_diap].Bmin) &&
                            (mas_IRI[sh_IRI].Pel1 <= list_SP2_DistrIRI[sh_diap].Bmax)
                           )
                        {
                            // -------------------------------------------------------
                            // Индекс нужного диапазона

                            indSP = sh_diap;
                            // -------------------------------------------------------
                            // Ищем места по этой литере

                            // FOR15
                            for (shKan = 0; shKan < list_KanSP2_DistrIRI.Count; shKan++)
                            {
                                if (
                                    (list_KanSP2_DistrIRI[shKan].NumLit == list_SP2_DistrIRI[indSP].NumLit) &&
                                    (list_KanSP2_DistrIRI[shKan].flKan != 0)
                                  )
                                {
                                    indKan = shKan;
                                    shKan = list_KanSP2_DistrIRI.Count; // For OOUT from FOR

                                    // Уменьшаем число мест по литере
                                    num_kan = list_KanSP2_DistrIRI[indKan].flKan;
                                    num_kan -= 1;
                                    objS_Kan_DistrIRI.flKan = num_kan;
                                    num_kan = list_KanSP2_DistrIRI[indKan].NumLit;
                                    objS_Kan_DistrIRI.NumLit = num_kan;
                                    list_KanSP2_DistrIRI.RemoveAt(indKan);
                                    list_KanSP2_DistrIRI.Insert(indKan, objS_Kan_DistrIRI);

                                    // Присваиваем этот ИРИ 2-й станции
                                    mas_IRI[sh_IRI].NSPRP = mas_SP[1].N_SP;
                                    mas_IRI[sh_IRI].NKanRP = list_KanSP2_DistrIRI[indKan].NumLit; // Litera
                                    mas_IRI[sh_IRI].PriznPop = 1;
                                    Count_IRI++;

                                } // Litera is free

                            } // FOR15 Kan
                            // -------------------------------------------------------
                            if (mas_IRI[sh_IRI].PriznPop == 1) // ИРИ распределился
                                sh_diap = ndiap; // Выход из for

                        }

                    } // FOR 14 Diapazon


                } // IF (PriznPop!=-1&&Prior1==3)


            } // FOR 33 IRI
            // ****************************************************************** SP2_1



            // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! !!!RASPREDELENIE

            if (Count_IRI == 0)
                return -1;
            else
                return 0;


        } // P/P f_DistributionIRI_SP
        // *************************************************************************





        // ********************************************************************
        /// <summary>
        /// Формирование радиосетей
        /// </summary>
        /// <param name="DistribFWS"> Данные по ИРИ ФРЧ </param>
        // ********************************************************************

        public void Organization_RNet(
                                       TDistribFWS[] IRI_RNet_Comm,
                                       double dD, // m
                                       double dF, //KHz
                                       double dPel, // grad
                                       ref List<SRNet> list_RNet
                                       )
        {


            // -----------------------------------------------------------------
            int nIRI_all = 0;
            int ind1 = 0;
            int shIRI = 0;
            int NumbRNet = 0;
            int NumbRNet1 = 0;
            int flRNet_Coord = 0;
            int ind_first = 0;
            int fl_first_Net = 0;
            int ind_second = 0;
            double dx = 0;
            double dy = 0;
            double D = 0;
            int i_tmp = 0;
            int i_tmp1 = 0;
            int fl_tmp = 0;

            nIRI_all = IRI_RNet_Comm.Length;

            List<S_IRI_RNet> list_IRI_RNet = new List<S_IRI_RNet>();
            List<SRNet> list_SRNet = new List<SRNet>();
            List<SRNet> list_SRNet1 = new List<SRNet>();

            S_IRI_RNet objS_IRI_RNet = new S_IRI_RNet();
            S_IRI_RNet objS_IRI_RNet2 = new S_IRI_RNet();
            S_IRI_RNet objS_IRI = new S_IRI_RNet();

            SRNet objSRNet = new SRNet();

            // -----------------------------------------------------------------
            // Переписываем массив

            // !!!!!!!!!!!!!!!!!!!
            dF = 3; // KHz
            dPel = 2; // grad
            // ................................................................
            // IRI

            for (shIRI = 0; shIRI < nIRI_all; shIRI++)
            {
                objS_IRI_RNet.ID = IRI_RNet_Comm[shIRI].iID;
                objS_IRI_RNet.F = (double)IRI_RNet_Comm[shIRI].iFreq / 10d;
                objS_IRI_RNet.Pel1 = (double)IRI_RNet_Comm[shIRI].sBearing1 / 10d;
                objS_IRI_RNet.Pel2 = (double)IRI_RNet_Comm[shIRI].sBearing2 / 10d;

                f_DistributionIRI_Coord(
                                        IRI_RNet_Comm[shIRI].dLatitude,
                                        IRI_RNet_Comm[shIRI].dLongitude,
                                        ref objS_IRI_RNet.X,
                                        ref objS_IRI_RNet.Y
                                       );

                if ((IRI_RNet_Comm[shIRI].dLatitude == -1) || (IRI_RNet_Comm[shIRI].dLongitude == -1))
                {
                    objS_IRI_RNet.X = 0;
                    objS_IRI_RNet.Y = 0;
                }

                list_IRI_RNet.Add(objS_IRI_RNet);

            } // FOR_IRI
            // ................................................................


            // RNet_XY *******************************************************
            // Выделение радиосетей и радионаправлений по ИРИ с координатами
            // (F==; XY!=)


            // ------------------------------------------------------------
            // WHILE1 (индекс ИРИ, с которым сравниваем остаток массива)

            flRNet_Coord = 0;
            ind_first = 0;
            ind_second = 0;
            fl_first_Net = 0; // Словлена РС

            // WHILE1
            while (ind_first < list_IRI_RNet.Count)
            {

                // -------------------------------------------------------
                // Для поиска опорного ири в РС

                // FOR1
                for (ind1 = ind_first; ind1 < list_IRI_RNet.Count; ind1++)
                {
                    if ((list_IRI_RNet[ind1].X != 0) && (list_IRI_RNet[ind1].Y != 0) && (flRNet_Coord == 0))
                    {
                        // Это опорный
                        flRNet_Coord = 1;
                        ind_first = ind1;

                        // Находим 1-й скоординатами и фиксируем его
                        objS_IRI_RNet.ID = list_IRI_RNet[ind1].ID;
                        objS_IRI_RNet.F = list_IRI_RNet[ind1].F;
                        objS_IRI_RNet.Pel1 = list_IRI_RNet[ind1].Pel1;
                        objS_IRI_RNet.Pel2 = list_IRI_RNet[ind1].Pel2;
                        objS_IRI_RNet.X = list_IRI_RNet[ind1].X;
                        objS_IRI_RNet.Y = list_IRI_RNet[ind1].Y;

                        // Добавляем в РС, НО из основного листа пока не убираем
                        NumbRNet += 1;
                        objSRNet.NRNet = NumbRNet;
                        objSRNet.numbIRI = 1;
                        objSRNet.list_IRI_ID = new List<S_IRI_RNet>();
                        objS_IRI.F = objS_IRI_RNet.F;
                        objS_IRI.ID = objS_IRI_RNet.ID;
                        objS_IRI.Pel1 = objS_IRI_RNet.Pel1;
                        objS_IRI.Pel2 = objS_IRI_RNet.Pel2;
                        objS_IRI.X = objS_IRI_RNet.X;
                        objS_IRI.Y = objS_IRI_RNet.Y;
                        objSRNet.list_IRI_ID.Add(objS_IRI);

                        list_SRNet.Add(objSRNet);

                        // Выходим из FOR 
                        ind1 = list_IRI_RNet.Count;

                    } // нашли 1-й с координатами


                } // FOR1 (индекс ИРИ, с которым сравниваем остаток массива)
                // -------------------------------------------------------
                // Не нашли ни одного с координатами-идем дальше

                // IF1
                if (flRNet_Coord == 0)
                {
                    // Выходим из WHILE1
                    ind_first = list_IRI_RNet.Count;
                } // IF1
                // -------------------------------------------------------
                // Нашли с координатами - проверяем остаток массива на предмет РС

                else
                {

                    ind_second = ind_first + 1;

                    // """""""""""""""""""""""""""""""""""""""""""""""""""""""""
                    // Проверка остатка массива на вопрос присоединения к опорному ИРИ

                    // WHILE2
                    while (ind_second < list_IRI_RNet.Count)
                    {

                        // .......................................................                  
                        // есть координаты

                        // IF2
                        if ((list_IRI_RNet[ind_second].X != 0) && (list_IRI_RNet[ind_second].Y != 0))
                        {

                            // фиксируем его
                            objS_IRI_RNet2.ID = list_IRI_RNet[ind_second].ID;
                            objS_IRI_RNet2.F = list_IRI_RNet[ind_second].F;
                            objS_IRI_RNet2.Pel1 = list_IRI_RNet[ind_second].Pel1;
                            objS_IRI_RNet2.Pel2 = list_IRI_RNet[ind_second].Pel2;
                            objS_IRI_RNet2.X = list_IRI_RNet[ind_second].X;
                            objS_IRI_RNet2.Y = list_IRI_RNet[ind_second].Y;

                            dx = objS_IRI_RNet.X - objS_IRI_RNet2.X;
                            dy = objS_IRI_RNet.Y - objS_IRI_RNet2.Y;
                            D = Math.Sqrt(dx * dx + dy * dy);
                            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                            // Подходит для РС

                            // IF3
                            if ((D > dD) && (Math.Abs(objS_IRI_RNet.F - objS_IRI_RNet2.F) < dF))
                            {
                                // Формируем РС
                                // IF4
                                if (fl_first_Net == 0)
                                {
                                    fl_first_Net = 1;

                                    // Добавляем в РС (!!! опорный уже добавлен)
                                    // Т.е. переписываем текущий элемент List в стр-ру, меняем и кидаем обратно
                                    objSRNet.NRNet = NumbRNet;
                                    objSRNet.list_IRI_ID = new List<S_IRI_RNet>();
                                    for (i_tmp = 0; i_tmp < list_SRNet[NumbRNet - 1].list_IRI_ID.Count; i_tmp++)
                                    {

                                        objS_IRI.ID = list_SRNet[NumbRNet - 1].list_IRI_ID[i_tmp].ID;
                                        objS_IRI.F = list_SRNet[NumbRNet - 1].list_IRI_ID[i_tmp].F;
                                        objS_IRI.X = list_SRNet[NumbRNet - 1].list_IRI_ID[i_tmp].X;
                                        objS_IRI.Y = list_SRNet[NumbRNet - 1].list_IRI_ID[i_tmp].Y;
                                        objS_IRI.Pel1 = list_SRNet[NumbRNet - 1].list_IRI_ID[i_tmp].Pel1;
                                        objS_IRI.Pel2 = list_SRNet[NumbRNet - 1].list_IRI_ID[i_tmp].Pel2;
                                        objSRNet.list_IRI_ID.Add(objS_IRI);

                                    }
                                    // Новый ИРИ
                                    objS_IRI.ID = objS_IRI_RNet2.ID;
                                    objS_IRI.F = objS_IRI_RNet2.F;
                                    objS_IRI.X = objS_IRI_RNet2.X;
                                    objS_IRI.Y = objS_IRI_RNet2.Y;
                                    objS_IRI.Pel1 = objS_IRI_RNet2.Pel1;
                                    objS_IRI.Pel2 = objS_IRI_RNet2.Pel2;

                                    objSRNet.list_IRI_ID.Add(objS_IRI);

                                    objSRNet.numbIRI = list_SRNet[NumbRNet - 1].numbIRI + 1;
                                    list_SRNet.RemoveAt(NumbRNet - 1);
                                    list_SRNet.Insert(NumbRNet - 1, objSRNet);

                                    // Убрать этот ИРИ из основного списка
                                    list_IRI_RNet.RemoveAt(ind_second);
                                    // Убрать опорный
                                    list_IRI_RNet.RemoveAt(ind_first);
                                    ind_second -= 1; // Т.к. убрали сразу два

                                } // IF4 Формируем РС

                                // Добавляем ИРИ к РС
                                else // On IF4
                                {
                                    // ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                                    // Проверяем, не расположен ли он близко к уже имеющимся в РС

                                    fl_tmp = 0;

                                    for (i_tmp = 0; i_tmp < list_SRNet[NumbRNet - 1].list_IRI_ID.Count; i_tmp++)
                                    {
                                        dx = list_SRNet[NumbRNet - 1].list_IRI_ID[i_tmp].X - objS_IRI_RNet2.X;
                                        dy = list_SRNet[NumbRNet - 1].list_IRI_ID[i_tmp].Y - objS_IRI_RNet2.Y;
                                        D = Math.Sqrt(dx * dx + dy * dy);
                                        if (D < dD)
                                        {
                                            fl_tmp = 1;
                                            i_tmp = list_SRNet[NumbRNet - 1].list_IRI_ID.Count; // Out fromFOR
                                        }
                                    }

                                    // ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                                    // Добавляем в РС

                                    // IF6
                                    if (fl_tmp == 0)
                                    {
                                        // Добавляем в РС (!!! опорный уже добавлен)
                                        // Т.е. переписываем текущий элемент List в стр-ру, меняем и кидаем обратно
                                        objSRNet.NRNet = NumbRNet;
                                        objSRNet.list_IRI_ID = new List<S_IRI_RNet>();
                                        for (i_tmp = 0; i_tmp < list_SRNet[NumbRNet - 1].list_IRI_ID.Count; i_tmp++)
                                        {

                                            objS_IRI.ID = list_SRNet[NumbRNet - 1].list_IRI_ID[i_tmp].ID;
                                            objS_IRI.F = list_SRNet[NumbRNet - 1].list_IRI_ID[i_tmp].F;
                                            objS_IRI.X = list_SRNet[NumbRNet - 1].list_IRI_ID[i_tmp].X;
                                            objS_IRI.Y = list_SRNet[NumbRNet - 1].list_IRI_ID[i_tmp].Y;
                                            objS_IRI.Pel1 = list_SRNet[NumbRNet - 1].list_IRI_ID[i_tmp].Pel1;
                                            objS_IRI.Pel2 = list_SRNet[NumbRNet - 1].list_IRI_ID[i_tmp].Pel2;
                                            objSRNet.list_IRI_ID.Add(objS_IRI);

                                        }

                                        // Новый ИРИ
                                        objS_IRI.ID = objS_IRI_RNet2.ID;
                                        objS_IRI.F = objS_IRI_RNet2.F;
                                        objS_IRI.X = objS_IRI_RNet2.X;
                                        objS_IRI.Y = objS_IRI_RNet2.Y;
                                        objS_IRI.Pel1 = objS_IRI_RNet2.Pel1;
                                        objS_IRI.Pel2 = objS_IRI_RNet2.Pel2;

                                        objSRNet.list_IRI_ID.Add(objS_IRI);
                                        objSRNet.numbIRI = list_SRNet[NumbRNet - 1].numbIRI + 1;
                                        list_SRNet.RemoveAt(NumbRNet - 1);
                                        list_SRNet.Insert(NumbRNet - 1, objSRNet);

                                        // Убрать этот ИРИ из основного списка
                                        list_IRI_RNet.RemoveAt(ind_second);
                                    } // IF6
                                    // ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                                    // Если этот ИРИ стоял близко к какому-то из ИРИ в этой РС

                                    else  // On IF6
                                    {
                                        // Убрать этот ИРИ из основного списка
                                        list_IRI_RNet.RemoveAt(ind_second);

                                    }
                                    // ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

                                } // else onIF4


                            } // IF3 RNet Добавляем ИРИ к РС
                            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                            // Не подходит для РС - пропускаем

                            else // On IF3
                            {
                                if ((D <= dD) && (Math.Abs(objS_IRI_RNet.F - objS_IRI_RNet2.F) < dF))
                                {
                                    // Убрать этот ИРИ из основного списка
                                    list_IRI_RNet.RemoveAt(ind_second);
                                }
                                else
                                    ind_second += 1;
                            }
                            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>


                        } // IF2 есть координаты
                        // .......................................................                  
                        // Нет координат- пропускаем

                        else // On IF2
                        {
                            ind_second += 1;
                        }
                        // .......................................................                  


                    } // WHILE2 (проверка остатка массива)

                    // """""""""""""""""""""""""""""""""""""""""""""""""""""""""
                    // Проверяем, сформировалась ли РС
                    // РС сформировалась, т.е. опорный убрали -> переходим к следующему, но индекс тот же
                    // т.к. массив сдвинулся

                    // РС не сформировалась -> Переходим к следующему опорному
                    if (fl_first_Net == 0)
                    {
                        ind_first += 1;
                        flRNet_Coord = 0;
                        list_SRNet.RemoveAt(NumbRNet - 1);
                        NumbRNet -= 1;
                        ind_second = ind_first + 1;
                        fl_first_Net = 0; // Словлена РС

                    } // РС не сформировалась

                    else
                    {
                        flRNet_Coord = 0;
                        ind_second = ind_first + 1;
                        fl_first_Net = 0; // Словлена РС
                    }
                    // """""""""""""""""""""""""""""""""""""""""""""""""""""""""



                } // else po IF1 (был опорный ИРИ с координатами)
                // -------------------------------------------------------


            } // while1(ind_first<list_IRI_RNet.Count)
            // ------------------------------------------------------------

            // ******************************************************* RNet_XY

            // RNet_Pel *******************************************************
            // Выделение радиосетей и радионаправлений по ИРИ без координат
            // (F==; Pel!=)

            // 11111111111111111111111111111111111111111111111111111111111
            // Сначала проходимся по уже сформированным РС и проверяем, если
            // частото и пеленг совпадают с ... ИРИ в РС-> убираем.
            // Если частоа==, а пеленги != -> добавляем в эту РС

            int fl_add = 0;
            int i_tmp2 = 0;

            ind1 = 0;

            // WHILE20
            while (ind1 < list_IRI_RNet.Count)
            {
                // ------------------------------------------                
                // IF20
                if ((list_IRI_RNet[ind1].X == 0) && (list_IRI_RNet[ind1].Y == 0))
                {
                    // ......................................
                    // Проходим по РС-ям

                    fl_tmp = 0;
                    fl_add = 0;

                    // FOR20 
                    for (i_tmp = 0; i_tmp < list_SRNet.Count; i_tmp++)
                    {
                        // ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
                        // FOR21 IRI in RSi
                        for (i_tmp1 = 0; i_tmp1 < list_SRNet[i_tmp].list_IRI_ID.Count; i_tmp1++)
                        {
                            // ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                            // Подходит по частоте, но входит в этот же пеленг

                            // IF22
                            if (
                                (
                                (Math.Abs(list_IRI_RNet[ind1].Pel1 - list_SRNet[i_tmp].list_IRI_ID[i_tmp1].Pel1) < dPel) ||
                                (Math.Abs(list_IRI_RNet[ind1].Pel1 - list_SRNet[i_tmp].list_IRI_ID[i_tmp1].Pel2) < dPel)
                                ) &&
                                (Math.Abs(list_IRI_RNet[ind1].F - list_SRNet[i_tmp].list_IRI_ID[i_tmp1].F) < dF)
                                )
                            {
                                // Del
                                fl_tmp = 1;
                                i_tmp1 = list_SRNet[i_tmp].list_IRI_ID.Count; // Out from FOR21

                            } // IF22
                            // ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                            // Подходит по частоте, пеленги не равны->
                            // Добавляем в эту РС, убираем из основного списка

                            // else on IF22 Add to RS
                            else if (
                             (Math.Abs(list_IRI_RNet[ind1].F - list_SRNet[i_tmp].list_IRI_ID[i_tmp1].F) < dF) &&
                             (Math.Abs(list_IRI_RNet[ind1].Pel1 - list_SRNet[i_tmp].list_IRI_ID[i_tmp1].Pel1) >= dPel) &&
                             (Math.Abs(list_IRI_RNet[ind1].Pel1 - list_SRNet[i_tmp].list_IRI_ID[i_tmp1].Pel2) >= dPel) &&
                             (i_tmp1 == 0)
                                   )
                            {
                                fl_add = 1;

                            } // else IF22 Add to RS
                            // ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                            // ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

                        } // FOR21 IRI in RSi
                        // ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
                        // DelIRI

                        // IF23
                        if (fl_tmp == 1)
                        {
                            fl_add = 0;

                            //fl_tmp = 0;
                            list_IRI_RNet.RemoveAt(ind1);
                            //!!! ind1 no change
                            i_tmp = list_SRNet.Count; // Out from FOR20

                        } // IF23
                        // ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
                        // Add to RS
                        // Т.е. переписываем текущий элемент List в стр-ру, меняем и кидаем обратно

                        // IF24
                        if (fl_add == 1)
                        {
                            //fl_add = 0;

                            objSRNet.NRNet = list_SRNet[i_tmp].NRNet;
                            objSRNet.list_IRI_ID = new List<S_IRI_RNet>();
                            for (i_tmp2 = 0; i_tmp2 < list_SRNet[i_tmp].list_IRI_ID.Count; i_tmp2++)
                            {
                                objS_IRI.ID = list_SRNet[i_tmp].list_IRI_ID[i_tmp2].ID;
                                objS_IRI.F = list_SRNet[i_tmp].list_IRI_ID[i_tmp2].F;
                                objS_IRI.X = list_SRNet[i_tmp].list_IRI_ID[i_tmp2].X;
                                objS_IRI.Y = list_SRNet[i_tmp].list_IRI_ID[i_tmp2].Y;
                                objS_IRI.Pel1 = list_SRNet[i_tmp].list_IRI_ID[i_tmp2].Pel1;
                                objS_IRI.Pel2 = list_SRNet[i_tmp].list_IRI_ID[i_tmp2].Pel2;
                                objSRNet.list_IRI_ID.Add(objS_IRI);

                            }

                            // Новый ИРИ
                            objS_IRI.ID = list_IRI_RNet[ind1].ID;
                            objS_IRI.F = list_IRI_RNet[ind1].F;
                            objS_IRI.X = list_IRI_RNet[ind1].X;
                            objS_IRI.Y = list_IRI_RNet[ind1].Y;
                            objS_IRI.Pel1 = list_IRI_RNet[ind1].Pel1;
                            objS_IRI.Pel2 = list_IRI_RNet[ind1].Pel2;
                            objSRNet.list_IRI_ID.Add(objS_IRI);
                            objSRNet.numbIRI = list_SRNet[i_tmp].numbIRI + 1;
                            list_SRNet.RemoveAt(i_tmp);
                            list_SRNet.Insert(i_tmp, objSRNet);

                            // Убрать этот ИРИ из основного списка
                            list_IRI_RNet.RemoveAt(ind1);

                            //!!! ind1 no change
                            i_tmp = list_SRNet.Count; // Out from FOR20

                        } // IF24 AddtoRS
                        // ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


                    } // FOR20 Проходим по РС-ям
                    // ......................................

                    if ((fl_tmp == 1) || (fl_add == 1)) // DelIRI/Add IRI
                    {
                        fl_tmp = 0; //!!! ind1 no change
                        fl_add = 0;
                    }
                    else ind1 += 1;

                } // IF20 NO coordinates
                // ------------------------------------------                
                // Есть координаты -> Пропускаем

                else // On IF20 (No coordinates)
                {
                    ind1 += 1;
                }
                // ------------------------------------------                

            } // WHILLE20  list_IRI_RNet
            // 11111111111111111111111111111111111111111111111111111111111

            // 222222222222222222222222222222222222222222222222222222222222
            // Из оставшихся формируем РС по пеленгу(это м.б. и оставшиеся с координатами)

            double dp1 = 0;
            double dp2 = 0;
            double fp = 0;
            double dff = 0;
            // ------------------------------------------------------------
            // WHILE1 (индекс ИРИ, с которым сравниваем остаток массива)

            flRNet_Coord = 0;
            ind_first = 0;
            ind_second = 0;
            fl_first_Net = 0; // Словлена РС

            // WHILE1
            while (ind_first < list_IRI_RNet.Count)
            {

                // -------------------------------------------------------
                // Для поиска опорного ири в РС

                // FOR1
                for (ind1 = ind_first; ind1 < list_IRI_RNet.Count; ind1++)
                {
                    //if ((list_IRI_RNet[ind1].X != 0) && (list_IRI_RNet[ind1].Y != 0) && (flRNet_Coord == 0))
                    if (flRNet_Coord == 0)
                    {
                        // Это опорный
                        flRNet_Coord = 1;
                        ind_first = ind1;

                        // фиксируем его
                        objS_IRI_RNet.ID = list_IRI_RNet[ind1].ID;
                        objS_IRI_RNet.F = list_IRI_RNet[ind1].F;
                        objS_IRI_RNet.Pel1 = list_IRI_RNet[ind1].Pel1;
                        objS_IRI_RNet.Pel2 = list_IRI_RNet[ind1].Pel2;
                        objS_IRI_RNet.X = list_IRI_RNet[ind1].X;
                        objS_IRI_RNet.Y = list_IRI_RNet[ind1].Y;

                        // Добавляем в РС, НО из основного листа пока не убираем
                        NumbRNet1 += 1;
                        objSRNet.NRNet = NumbRNet1;

                        objSRNet.numbIRI = 1;
                        objSRNet.list_IRI_ID = new List<S_IRI_RNet>();
                        objS_IRI.F = objS_IRI_RNet.F;
                        objS_IRI.ID = objS_IRI_RNet.ID;
                        objS_IRI.Pel1 = objS_IRI_RNet.Pel1;
                        objS_IRI.Pel2 = objS_IRI_RNet.Pel2;
                        objS_IRI.X = objS_IRI_RNet.X;
                        objS_IRI.Y = objS_IRI_RNet.Y;
                        objSRNet.list_IRI_ID.Add(objS_IRI);

                        list_SRNet1.Add(objSRNet);

                        // Выходим из FOR 
                        ind1 = list_IRI_RNet.Count;

                    } // нашли 1-й 


                } // FOR1 (индекс ИРИ, с которым сравниваем остаток массива)
                // -------------------------------------------------------
                // Не нашли ни одного с координатами-идем дальше

                // IF1
                if (flRNet_Coord == 0)
                {
                    // Выходим из WHILE1
                    ind_first = list_IRI_RNet.Count;
                } // IF1
                // -------------------------------------------------------
                // Нашли с координатами - проверяем остаток массива на предмет РС

                else
                {

                    ind_second = ind_first + 1;

                    // """""""""""""""""""""""""""""""""""""""""""""""""""""""""
                    // Проверка остатка массива на вопрос присоединения к опорному ИРИ

                    // WHILE2
                    while (ind_second < list_IRI_RNet.Count)
                    {

                        // .......................................................                  

                        // фиксируем его
                        objS_IRI_RNet2.ID = list_IRI_RNet[ind_second].ID;
                        objS_IRI_RNet2.F = list_IRI_RNet[ind_second].F;
                        objS_IRI_RNet2.Pel1 = list_IRI_RNet[ind_second].Pel1;
                        objS_IRI_RNet2.Pel2 = list_IRI_RNet[ind_second].Pel2;
                        objS_IRI_RNet2.X = list_IRI_RNet[ind_second].X;
                        objS_IRI_RNet2.Y = list_IRI_RNet[ind_second].Y;

                        if (
                            (objS_IRI_RNet.X != 0) &&// oporny
                            (objS_IRI_RNet.Y != 0) &&
                            (objS_IRI_RNet2.X != 0) &&
                            (objS_IRI_RNet2.Y != 0)
                          )
                        {
                            ind_second += 1;
                            goto PPP; // propusk
                        }
                        fp = 0;
                        if (
                            (objS_IRI_RNet.X != 0) &&// oporny
                            (objS_IRI_RNet.Y != 0) &&
                            (objS_IRI_RNet2.X == 0) &&
                            (objS_IRI_RNet2.Y == 0)
                          )
                        {
                            fp = 1;
                            dp1 = Math.Abs(objS_IRI_RNet.Pel1 - objS_IRI_RNet2.Pel1);
                            dp2 = Math.Abs(objS_IRI_RNet.Pel2 - objS_IRI_RNet2.Pel1);
                            dff = Math.Abs(objS_IRI_RNet.F - objS_IRI_RNet2.F);
                        }
                        else if (
                            (objS_IRI_RNet.X == 0) &&// oporny
                            (objS_IRI_RNet.Y == 0) &&
                            (objS_IRI_RNet2.X != 0) &&
                            (objS_IRI_RNet2.Y != 0)
                          )
                        {
                            fp = 1;
                            dp1 = Math.Abs(objS_IRI_RNet.Pel1 - objS_IRI_RNet2.Pel1);
                            dp2 = Math.Abs(objS_IRI_RNet.Pel1 - objS_IRI_RNet2.Pel2);
                            dff = Math.Abs(objS_IRI_RNet.F - objS_IRI_RNet2.F);
                        }
                        else
                        {
                            dp1 = Math.Abs(objS_IRI_RNet.Pel1 - objS_IRI_RNet2.Pel1);
                            dff = Math.Abs(objS_IRI_RNet.F - objS_IRI_RNet2.F);
                        }

                        // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                        // Подходит для РС

                        // IF3
                        if (
                           ((fp == 0) && (dff < dF) && (dp1 > dPel)) ||
                           ((fp == 1) && (dff < dF) && (dp1 > dPel) && (dp2 > dPel))
                          )
                        {
                            // Формируем РС
                            // IF4
                            if (fl_first_Net == 0)
                            {
                                fl_first_Net = 1;

                                // Добавляем в РС (!!! опорный уже добавлен)
                                // Т.е. переписываем текущий элемент List в стр-ру, меняем и кидаем обратно
                                objSRNet.NRNet = NumbRNet1;
                                objSRNet.list_IRI_ID = new List<S_IRI_RNet>();
                                for (i_tmp = 0; i_tmp < list_SRNet1[NumbRNet1 - 1].list_IRI_ID.Count; i_tmp++)
                                {
                                    objS_IRI.ID = list_SRNet1[NumbRNet1 - 1].list_IRI_ID[i_tmp].ID;
                                    objS_IRI.F = list_SRNet1[NumbRNet1 - 1].list_IRI_ID[i_tmp].F;
                                    objS_IRI.X = list_SRNet1[NumbRNet1 - 1].list_IRI_ID[i_tmp].X;
                                    objS_IRI.Y = list_SRNet1[NumbRNet1 - 1].list_IRI_ID[i_tmp].Y;
                                    objS_IRI.Pel1 = list_SRNet1[NumbRNet1 - 1].list_IRI_ID[i_tmp].Pel1;
                                    objS_IRI.Pel2 = list_SRNet1[NumbRNet1 - 1].list_IRI_ID[i_tmp].Pel2;
                                    objSRNet.list_IRI_ID.Add(objS_IRI);

                                }
                                // Новый ИРИ
                                objS_IRI.ID = objS_IRI_RNet2.ID;
                                objS_IRI.F = objS_IRI_RNet2.F;
                                objS_IRI.X = objS_IRI_RNet2.X;
                                objS_IRI.Y = objS_IRI_RNet2.Y;
                                objS_IRI.Pel1 = objS_IRI_RNet2.Pel1;
                                objS_IRI.Pel2 = objS_IRI_RNet2.Pel2;

                                objSRNet.list_IRI_ID.Add(objS_IRI);

                                objSRNet.numbIRI = list_SRNet1[NumbRNet1 - 1].numbIRI + 1;
                                list_SRNet1.RemoveAt(NumbRNet1 - 1);
                                list_SRNet1.Insert(NumbRNet1 - 1, objSRNet);

                                // Убрать этот ИРИ из основного списка
                                list_IRI_RNet.RemoveAt(ind_second);
                                // Убрать опорный
                                list_IRI_RNet.RemoveAt(ind_first);
                                ind_second -= 1; // Т.к. убрали сразу два

                            } // IF4 Формируем РС

                            // Добавляем ИРИ к РС
                            else // On IF4
                            {
                                // ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                                // Проверяем, не расположен ли он близко по пеленгу к уже имеющимся в РС

                                fl_tmp = 0;

                                // FOR33
                                for (i_tmp = 0; i_tmp < list_SRNet1[NumbRNet1 - 1].list_IRI_ID.Count; i_tmp++)
                                {
                                    fp = 0;
                                    if (
                                        (list_SRNet1[NumbRNet1 - 1].list_IRI_ID[i_tmp].X != 0) &&
                                        (list_SRNet1[NumbRNet1 - 1].list_IRI_ID[i_tmp].Y != 0) &&
                                        (objS_IRI_RNet2.X != 0) &&
                                        (objS_IRI_RNet2.Y != 0)
                                      )
                                    {
                                        fp = 2;
                                    }
                                    else if (
                                            (list_SRNet1[NumbRNet1 - 1].list_IRI_ID[i_tmp].X != 0) &&
                                            (list_SRNet1[NumbRNet1 - 1].list_IRI_ID[i_tmp].Y != 0) &&
                                            (objS_IRI_RNet2.X == 0) &&
                                            (objS_IRI_RNet2.Y == 0)
                                      )
                                    {
                                        fp = 1;
                                        dp1 = Math.Abs(list_SRNet1[NumbRNet1 - 1].list_IRI_ID[i_tmp].Pel1 - objS_IRI_RNet2.Pel1);
                                        dp2 = Math.Abs(list_SRNet1[NumbRNet1 - 1].list_IRI_ID[i_tmp].Pel2 - objS_IRI_RNet2.Pel1);
                                    }
                                    else if (
                                            (list_SRNet1[NumbRNet1 - 1].list_IRI_ID[i_tmp].X == 0) &&
                                            (list_SRNet1[NumbRNet1 - 1].list_IRI_ID[i_tmp].Y == 0) &&
                                            (objS_IRI_RNet2.X != 0) &&
                                            (objS_IRI_RNet2.Y != 0)
                                           )
                                    {
                                        fp = 1;
                                        dp1 = Math.Abs(list_SRNet1[NumbRNet1 - 1].list_IRI_ID[i_tmp].Pel1 - objS_IRI_RNet2.Pel1);
                                        dp2 = Math.Abs(list_SRNet1[NumbRNet1 - 1].list_IRI_ID[i_tmp].Pel1 - objS_IRI_RNet2.Pel2);
                                    }
                                    else
                                    {
                                        dp1 = Math.Abs(list_SRNet1[NumbRNet1 - 1].list_IRI_ID[i_tmp].Pel1 - objS_IRI_RNet2.Pel1);
                                    }

                                    if (
                                        (fp == 2) ||
                                        ((fp == 0) && (dp1 <= dPel)) ||
                                        ((fp == 1) && ((dp1 <= dPel) || (dp2 <= dPel)))

                                      )
                                    {
                                        fl_tmp = 1;
                                        i_tmp = list_SRNet1[NumbRNet1 - 1].list_IRI_ID.Count; // Out fromFOR
                                    }

                                } // FOR33

                                // ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                                // Добавляем в РС

                                // IF6
                                if (fl_tmp == 0)
                                {
                                    // Добавляем в РС (!!! опорный уже добавлен)
                                    // Т.е. переписываем текущий элемент List в стр-ру, меняем и кидаем обратно
                                    objSRNet.NRNet = NumbRNet1;
                                    objSRNet.list_IRI_ID = new List<S_IRI_RNet>();
                                    for (i_tmp = 0; i_tmp < list_SRNet1[NumbRNet1 - 1].list_IRI_ID.Count; i_tmp++)
                                    {

                                        objS_IRI.ID = list_SRNet1[NumbRNet1 - 1].list_IRI_ID[i_tmp].ID;
                                        objS_IRI.F = list_SRNet1[NumbRNet1 - 1].list_IRI_ID[i_tmp].F;
                                        objS_IRI.X = list_SRNet1[NumbRNet1 - 1].list_IRI_ID[i_tmp].X;
                                        objS_IRI.Y = list_SRNet1[NumbRNet1 - 1].list_IRI_ID[i_tmp].Y;
                                        objS_IRI.Pel1 = list_SRNet1[NumbRNet1 - 1].list_IRI_ID[i_tmp].Pel1;
                                        objS_IRI.Pel2 = list_SRNet1[NumbRNet1 - 1].list_IRI_ID[i_tmp].Pel2;
                                        objSRNet.list_IRI_ID.Add(objS_IRI);

                                    }

                                    // Новый ИРИ
                                    objS_IRI.ID = objS_IRI_RNet2.ID;
                                    objS_IRI.F = objS_IRI_RNet2.F;
                                    objS_IRI.X = objS_IRI_RNet2.X;
                                    objS_IRI.Y = objS_IRI_RNet2.Y;
                                    objS_IRI.Pel1 = objS_IRI_RNet2.Pel1;
                                    objS_IRI.Pel2 = objS_IRI_RNet2.Pel2;

                                    objSRNet.list_IRI_ID.Add(objS_IRI);
                                    objSRNet.numbIRI = list_SRNet1[NumbRNet1 - 1].numbIRI + 1;
                                    list_SRNet1.RemoveAt(NumbRNet1 - 1);
                                    list_SRNet1.Insert(NumbRNet1 - 1, objSRNet);

                                    // Убрать этот ИРИ из основного списка
                                    list_IRI_RNet.RemoveAt(ind_second);
                                } // IF6
                                // ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                                // Если этот ИРИ стоял близко к какому-то из ИРИ в этой РС

                                else  // On IF6
                                {
                                    // Убрать этот ИРИ из основного списка
                                    list_IRI_RNet.RemoveAt(ind_second);

                                }
                                // ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

                            } // else onIF4


                        } // IF3 RNet Добавляем ИРИ к РС
                        // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                        // Не подходит для РС - пропускаем

                        else // On IF3
                        {

                            if (
                               ((fp == 0) && (dff < dF) && (dp1 <= dPel)) ||
                               ((fp == 1) && (dff < dF) && ((dp1 <= dPel) || (dp2 <= dPel)))
                              )
                            {
                                // Убрать этот ИРИ из основного списка
                                list_IRI_RNet.RemoveAt(ind_second);
                            }
                            else
                                ind_second += 1;
                        }
                    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

                    // .......................................................                  


                     PPP:
                        ind_second = ind_second;

                    } // WHILE2 (проверка остатка массива)

                    // """""""""""""""""""""""""""""""""""""""""""""""""""""""""
                    // Проверяем, сформировалась ли РС
                    // РС сформировалась, т.е. опорный убрали -> переходим к следующему, но индекс тот же
                    // т.к. массив сдвинулся

                    // РС не сформировалась -> Переходим к следующему опорному
                    if (fl_first_Net == 0)
                    {
                        ind_first += 1;
                        flRNet_Coord = 0;
                        list_SRNet1.RemoveAt(NumbRNet1 - 1);
                        NumbRNet1 -= 1;
                        ind_second = ind_first + 1;
                        fl_first_Net = 0; // Словлена РС

                    } // РС не сформировалась

                    else
                    {
                        flRNet_Coord = 0;
                        ind_second = ind_first + 1;
                        fl_first_Net = 0; // Словлена РС
                    }
                    // """""""""""""""""""""""""""""""""""""""""""""""""""""""""


                } // else po IF1 (был опорный ИРИ с координатами)
                // -------------------------------------------------------


            } // while1(ind_first<list_IRI_RNet.Count)
            // ------------------------------------------------------------

            // 222222222222222222222222222222222222222222222222222222222222

            // ******************************************************* RNet_Pel

            // **********************************************************
            // Формирование выходных массивов

            int nmm = 0;

            nmm = list_SRNet.Count + 1;
            ind1 = list_SRNet1.Count;
            var vv = list_SRNet1.ToArray();

            for (i_tmp = 0; i_tmp < ind1; i_tmp++)
            {
                vv[i_tmp].NRNet = nmm;
                list_SRNet.Add(vv[i_tmp]);
                nmm += 1;
            }

            for (i_tmp = 0; i_tmp < list_SRNet.Count; i_tmp++)
            {
                list_RNet.Add(list_SRNet[i_tmp]);
            }

            // **********************************************************


        } // P/P Organization_RNet
        // *******************************************************************


        // ********************************************************************
        /// <summary>
        /// Формирование узлов связи
        /// </summary>
        /// <param name="DistribFWS"> Данные по ИРИ ФРЧ </param>
        // ********************************************************************

        public void Organization_Communication(
                                             TDistribFWS[] IRI_RNet_Comm,
                                             double dD, // m
                                             double dF, // KHz
                                             ref List<SRNet> list_Comm
                                             )
        {

            // -----------------------------------------------------------------
            int nIRI_all = 0;
            int ind1 = 0;
            int shIRI = 0;
            int NumbYS = 0;
            int flRNet_Coord = 0;
            int ind_first = 0;
            int fl_first_Net = 0;
            int ind_second = 0;
            double dx = 0;
            double dy = 0;
            double D = 0;
            int i_tmp = 0;
            int fl_tmp = 0;

            nIRI_all = IRI_RNet_Comm.Length;

            List<S_IRI_RNet> list_IRI_RNet = new List<S_IRI_RNet>();
            List<SRNet> list_S_YS_RNet = new List<SRNet>();

            S_IRI_RNet objS_IRI_RNet = new S_IRI_RNet();
            S_IRI_RNet objS_IRI_RNet2 = new S_IRI_RNet();
            S_IRI_RNet objS_IRI = new S_IRI_RNet();

            SRNet objSRNet = new SRNet();
            // -----------------------------------------------------------------
            // !!!!!!!!!!!!!!!!

            dF = 3; // KHz
            // -----------------------------------------------------------------
            // Переписываем массив

            // ................................................................
            // IRI

            for (shIRI = 0; shIRI < nIRI_all; shIRI++)
            {
                objS_IRI_RNet.ID = IRI_RNet_Comm[shIRI].iID;
                objS_IRI_RNet.F = (double)IRI_RNet_Comm[shIRI].iFreq / 10d;
                objS_IRI_RNet.Pel1 = (double)IRI_RNet_Comm[shIRI].sBearing1 / 10d;
                objS_IRI_RNet.Pel2 = (double)IRI_RNet_Comm[shIRI].sBearing2 / 10d;

                f_DistributionIRI_Coord(
                                        IRI_RNet_Comm[shIRI].dLatitude,
                                        IRI_RNet_Comm[shIRI].dLongitude,
                                        ref objS_IRI_RNet.X,
                                        ref objS_IRI_RNet.Y
                                       );

                if ((IRI_RNet_Comm[shIRI].dLatitude == -1) || (IRI_RNet_Comm[shIRI].dLongitude == -1))
                {
                    objS_IRI_RNet.X = 0;
                    objS_IRI_RNet.Y = 0;
                }

                list_IRI_RNet.Add(objS_IRI_RNet);

            } // FOR_IRI
            // ................................................................

            // RYS_XY *******************************************************
            // Выделение YS по ИРИ с координатами
            // (F!=; XY=)

            // ------------------------------------------------------------
            // WHILE1 (индекс ИРИ, с которым сравниваем остаток массива)

            flRNet_Coord = 0;
            ind_first = 0;
            ind_second = 0;
            fl_first_Net = 0; // Словлен YS 

            // WHILE1
            while (ind_first < list_IRI_RNet.Count)
            {

                // -------------------------------------------------------
                // Для поиска опорного ири в YS

                // FOR1
                for (ind1 = ind_first; ind1 < list_IRI_RNet.Count; ind1++)
                {
                    if ((list_IRI_RNet[ind1].X != 0) && (list_IRI_RNet[ind1].Y != 0) && (flRNet_Coord == 0))
                    {
                        // Это опорный
                        flRNet_Coord = 1;
                        ind_first = ind1;

                        // Находим 1-й скоординатами и фиксируем его
                        objS_IRI_RNet.ID = list_IRI_RNet[ind1].ID;
                        objS_IRI_RNet.F = list_IRI_RNet[ind1].F;
                        objS_IRI_RNet.Pel1 = list_IRI_RNet[ind1].Pel1;
                        objS_IRI_RNet.Pel2 = list_IRI_RNet[ind1].Pel2;
                        objS_IRI_RNet.X = list_IRI_RNet[ind1].X;
                        objS_IRI_RNet.Y = list_IRI_RNet[ind1].Y;

                        // Добавляем в YS, НО из основного листа пока не убираем
                        NumbYS += 1;
                        objSRNet.NRNet = NumbYS;
                        objSRNet.numbIRI = 1;
                        objSRNet.list_IRI_ID = new List<S_IRI_RNet>();
                        objS_IRI.F = objS_IRI_RNet.F;
                        objS_IRI.ID = objS_IRI_RNet.ID;
                        objS_IRI.Pel1 = objS_IRI_RNet.Pel1;
                        objS_IRI.Pel2 = objS_IRI_RNet.Pel2;
                        objS_IRI.X = objS_IRI_RNet.X;
                        objS_IRI.Y = objS_IRI_RNet.Y;
                        objSRNet.list_IRI_ID.Add(objS_IRI);

                        list_S_YS_RNet.Add(objSRNet);

                        // Выходим из FOR 
                        ind1 = list_IRI_RNet.Count;

                    } // нашли 1-й с координатами


                } // FOR1 (индекс ИРИ, с которым сравниваем остаток массива)
                // -------------------------------------------------------
                // Не нашли ни одного с координатами-идем дальше

                // IF1
                if (flRNet_Coord == 0)
                {
                    // Выходим из WHILE1
                    ind_first = list_IRI_RNet.Count;
                } // IF1
                // -------------------------------------------------------
                // Нашли с координатами - проверяем остаток массива на предмет YS

                else
                {

                    ind_second = ind_first + 1;

                    // """""""""""""""""""""""""""""""""""""""""""""""""""""""""
                    // Проверка остатка массива на вопрос присоединения к опорному ИРИ

                    // WHILE2
                    while (ind_second < list_IRI_RNet.Count)
                    {

                        // .......................................................                  
                        // есть координаты

                        // IF2
                        if ((list_IRI_RNet[ind_second].X != 0) && (list_IRI_RNet[ind_second].Y != 0))
                        {

                            // фиксируем его
                            objS_IRI_RNet2.ID = list_IRI_RNet[ind_second].ID;
                            objS_IRI_RNet2.F = list_IRI_RNet[ind_second].F;
                            objS_IRI_RNet2.Pel1 = list_IRI_RNet[ind_second].Pel1;
                            objS_IRI_RNet2.Pel2 = list_IRI_RNet[ind_second].Pel2;
                            objS_IRI_RNet2.X = list_IRI_RNet[ind_second].X;
                            objS_IRI_RNet2.Y = list_IRI_RNet[ind_second].Y;

                            dx = objS_IRI_RNet.X - objS_IRI_RNet2.X;
                            dy = objS_IRI_RNet.Y - objS_IRI_RNet2.Y;
                            D = Math.Sqrt(dx * dx + dy * dy);
                            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                            // Подходит для YS

                            // IF3
                            if ((D < dD) && (Math.Abs(objS_IRI_RNet.F - objS_IRI_RNet2.F) > dF))
                            {
                                // Формируем YS
                                // IF4
                                if (fl_first_Net == 0)
                                {
                                    fl_first_Net = 1;

                                    // Добавляем в YS (!!! опорный уже добавлен)
                                    // Т.е. переписываем текущий элемент List в стр-ру, меняем и кидаем обратно
                                    objSRNet.NRNet = NumbYS;
                                    objSRNet.list_IRI_ID = new List<S_IRI_RNet>();
                                    for (i_tmp = 0; i_tmp < list_S_YS_RNet[NumbYS - 1].list_IRI_ID.Count; i_tmp++)
                                    {
                                        objS_IRI.ID = list_S_YS_RNet[NumbYS - 1].list_IRI_ID[i_tmp].ID;
                                        objS_IRI.F = list_S_YS_RNet[NumbYS - 1].list_IRI_ID[i_tmp].F;
                                        objS_IRI.X = list_S_YS_RNet[NumbYS - 1].list_IRI_ID[i_tmp].X;
                                        objS_IRI.Y = list_S_YS_RNet[NumbYS - 1].list_IRI_ID[i_tmp].Y;
                                        objS_IRI.Pel1 = list_S_YS_RNet[NumbYS - 1].list_IRI_ID[i_tmp].Pel1;
                                        objS_IRI.Pel2 = list_S_YS_RNet[NumbYS - 1].list_IRI_ID[i_tmp].Pel2;
                                        objSRNet.list_IRI_ID.Add(objS_IRI);

                                    }
                                    // Новый ИРИ
                                    objS_IRI.ID = objS_IRI_RNet2.ID;
                                    objS_IRI.F = objS_IRI_RNet2.F;
                                    objS_IRI.X = objS_IRI_RNet2.X;
                                    objS_IRI.Y = objS_IRI_RNet2.Y;
                                    objS_IRI.Pel1 = objS_IRI_RNet2.Pel1;
                                    objS_IRI.Pel2 = objS_IRI_RNet2.Pel2;

                                    objSRNet.list_IRI_ID.Add(objS_IRI);

                                    objSRNet.numbIRI = list_S_YS_RNet[NumbYS - 1].numbIRI + 1;
                                    list_S_YS_RNet.RemoveAt(NumbYS - 1);
                                    list_S_YS_RNet.Insert(NumbYS - 1, objSRNet);

                                    // Убрать этот ИРИ из основного списка
                                    list_IRI_RNet.RemoveAt(ind_second);
                                    // Убрать опорный
                                    list_IRI_RNet.RemoveAt(ind_first);
                                    ind_second -= 1; // Т.к. убрали сразу два

                                } // IF4 Формируем РС

                                // Добавляем ИРИ к YS
                                else // On IF4
                                {
                                    // ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                                    // Проверяем, не = ли поF  к уже имеющимся в YS

                                    fl_tmp = 0;

                                    for (i_tmp = 0; i_tmp < list_S_YS_RNet[NumbYS - 1].list_IRI_ID.Count; i_tmp++)
                                    {
                                        if (Math.Abs(list_S_YS_RNet[NumbYS - 1].list_IRI_ID[i_tmp].F - objS_IRI_RNet2.F) < dF)
                                        {
                                            fl_tmp = 1;
                                            i_tmp = list_S_YS_RNet[NumbYS - 1].list_IRI_ID.Count; // Out fromFOR
                                        }
                                    }

                                    // ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                                    // Добавляем в YS

                                    // IF6
                                    if (fl_tmp == 0)
                                    {
                                        // Добавляем в YS (!!! опорный уже добавлен)
                                        // Т.е. переписываем текущий элемент List в стр-ру, меняем и кидаем обратно
                                        objSRNet.NRNet = NumbYS;
                                        objSRNet.list_IRI_ID = new List<S_IRI_RNet>();
                                        for (i_tmp = 0; i_tmp < list_S_YS_RNet[NumbYS - 1].list_IRI_ID.Count; i_tmp++)
                                        {
                                            objS_IRI.ID = list_S_YS_RNet[NumbYS - 1].list_IRI_ID[i_tmp].ID;
                                            objS_IRI.F = list_S_YS_RNet[NumbYS - 1].list_IRI_ID[i_tmp].F;
                                            objS_IRI.X = list_S_YS_RNet[NumbYS - 1].list_IRI_ID[i_tmp].X;
                                            objS_IRI.Y = list_S_YS_RNet[NumbYS - 1].list_IRI_ID[i_tmp].Y;
                                            objS_IRI.Pel1 = list_S_YS_RNet[NumbYS - 1].list_IRI_ID[i_tmp].Pel1;
                                            objS_IRI.Pel2 = list_S_YS_RNet[NumbYS - 1].list_IRI_ID[i_tmp].Pel2;
                                            objSRNet.list_IRI_ID.Add(objS_IRI);

                                        }

                                        // Новый ИРИ
                                        objS_IRI.ID = objS_IRI_RNet2.ID;
                                        objS_IRI.F = objS_IRI_RNet2.F;
                                        objS_IRI.X = objS_IRI_RNet2.X;
                                        objS_IRI.Y = objS_IRI_RNet2.Y;
                                        objS_IRI.Pel1 = objS_IRI_RNet2.Pel1;
                                        objS_IRI.Pel2 = objS_IRI_RNet2.Pel2;

                                        objSRNet.list_IRI_ID.Add(objS_IRI);
                                        objSRNet.numbIRI = list_S_YS_RNet[NumbYS - 1].numbIRI + 1;
                                        list_S_YS_RNet.RemoveAt(NumbYS - 1);
                                        list_S_YS_RNet.Insert(NumbYS - 1, objSRNet);

                                        // Убрать этот ИРИ из основного списка
                                        list_IRI_RNet.RemoveAt(ind_second);
                                    } // IF6
                                    // ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                                    // Если этот ИРИ стоял близко к какому-то из ИРИ в этой РС

                                    else  // On IF6
                                    {
                                        // Убрать этот ИРИ из основного списка
                                        list_IRI_RNet.RemoveAt(ind_second);

                                    }
                                    // ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

                                } // else onIF4


                            } // IF3 RNet Добавляем ИРИ к YS
                            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                            // Не подходит для YS - пропускаем

                            else // On IF3
                            {
                                if ((D < dD) && (Math.Abs(objS_IRI_RNet.F - objS_IRI_RNet2.F) <= dF))
                                {
                                    // Убрать этот ИРИ из основного списка
                                    list_IRI_RNet.RemoveAt(ind_second);
                                }
                                else
                                    ind_second += 1;
                            }
                            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>


                        } // IF2 есть координаты
                        // .......................................................                  
                        // Нет координат- пропускаем

                        else // On IF2
                        {
                            ind_second += 1;
                        }
                        // .......................................................                  


                    } // WHILE2 (проверка остатка массива)

                    // """""""""""""""""""""""""""""""""""""""""""""""""""""""""
                    // Проверяем, сформировалась ли YS
                    // YS сформировалась, т.е. опорный убрали -> переходим к следующему, но индекс тот же
                    // т.к. массив сдвинулся

                    // РС не сформировалась -> Переходим к следующему опорному
                    if (fl_first_Net == 0)
                    {
                        ind_first += 1;
                        flRNet_Coord = 0;
                        list_S_YS_RNet.RemoveAt(NumbYS - 1);
                        NumbYS -= 1;
                        ind_second = ind_first + 1;
                        fl_first_Net = 0; // Словлена РС

                    } // РС не сформировалась

                    else
                    {
                        flRNet_Coord = 0;
                        ind_second = ind_first + 1;
                        fl_first_Net = 0; // Словлена РС
                    }

                    // """""""""""""""""""""""""""""""""""""""""""""""""""""""""

                } // else po IF1 (был опорный ИРИ с координатами)
                // -------------------------------------------------------


            } // while1(ind_first<list_IRI_RNet.Count)
            // ------------------------------------------------------------

            // ******************************************************* RYS_XY

            // **********************************************************
            // Формирование выходных массивов

            for (i_tmp = 0; i_tmp < list_S_YS_RNet.Count; i_tmp++)
            {
                list_Comm.Add(list_S_YS_RNet[i_tmp]);
            }
            // **********************************************************


        } // P/P Organization_Communication
        // ************************************************************** LENA



    }
}
