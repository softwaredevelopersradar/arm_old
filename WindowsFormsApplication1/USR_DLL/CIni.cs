﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace USR_DLL
{
    public class CIni
    {
        //************** VARIABLE **************//
        string strPathFile;

        [DllImport("kernel32")]
        private static extern long WritePrivateProfileString(string section, string key, string val, string filePath);

        [DllImport("KERNEL32.DLL", CharSet = CharSet.Auto, EntryPoint = "WritePrivateProfileStringW")]
        private static extern bool WritePrivateProfileStringW(string section, string key, string val, string filePath);

        [DllImport("kernel32")]
        private static extern int GetPrivateProfileString(string section, string key, string def, StringBuilder retVal, int size, string filePath);

        //************** CONSTRUCTOR **************//
        public CIni(string strPath)
        {
            strPathFile = strPath;
        }

        //************** FUNCTION **************//    

        // write string to *.ini file
        public void WriteString(string Section, string Key, string Value)
        {
            WritePrivateProfileString(Section, Key, Value, this.strPathFile);
        }

        // read string from *.ini file
        public string ReadString(string Section, string Key)
        {
            StringBuilder temp = new StringBuilder(255);
            int i = GetPrivateProfileString(Section, Key, "", temp,
            255, this.strPathFile);
            return temp.ToString();
        }

        // read int from *.ini file
        public int ReadInt(string Section, string Key, int Default)
        {
            StringBuilder sb = new StringBuilder(255);
            int i = GetPrivateProfileString(Section, Key, "", sb, 255, this.strPathFile);

            if (i == 0)
                return Default;

            int iValue = 0;

            try
            {
                iValue = Convert.ToInt32(sb.ToString());
            }
            catch (System.Exception)
            {
                return Default;
            }

            return iValue;
        }

        // write int to *.ini file
        public bool WriteInt(string Section, string Key, int Value)
        {
            return WritePrivateProfileStringW(Section, Key, Value.ToString(), this.strPathFile);
        }

        // read byte from *.ini file
        public byte ReadByte(string Section, string Key, byte Default)
        {
            StringBuilder sb = new StringBuilder(255);
            int i = GetPrivateProfileString(Section, Key, "", sb, 255, this.strPathFile);

            if (i == 0)
                return Default;

            byte bValue = 0;

            try
            {
                bValue = Convert.ToByte(sb.ToString());
            }
            catch (System.Exception)
            {
                return Default;
            }

            return bValue;
        }

        // write byte to *.ini file
        public bool WriteByte(string Section, string Key, byte Value)
        {
            return WritePrivateProfileStringW(Section, Key, Value.ToString(), this.strPathFile);
        }
    }
}
