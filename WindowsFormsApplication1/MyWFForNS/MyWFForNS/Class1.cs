﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Windows.Forms;
namespace MyWFForNS
{
    // параметр запроса излучения
    public struct TRequestParam
    {
        public byte bLetter;           // литера
        public void Init()
        {
            bLetter = 0;
        }
    }
    public struct TSwitchRadiat
    {
        public int iDuration;           // длительность
        public void Init()
        {
            iDuration = 0;
        }
    }
    // тип загруженных
    public struct TTypeLoad
    {
        public byte bLetter;           // литера
        public byte bType;             // тип: 1 - , 0 - 
        public void Init()
        {
            bLetter = 0;
            bType = 0;
        }
    }
    // структура одного ИРИ ФРЧ на РП
    public struct TOneSourceSupress
    {
        public uint uiFreq;         // частота
        public byte bModulation;    // вид модуляции
        public byte bDeviation;     // вид девиации
        public byte bManipulation;  // вид манипуляции           
        public byte bDuration;      // длительность     
        public void Init()
        {
            uiFreq = 0;           // частота
            bModulation = 0;    // вид модуляции
            bDeviation = 0;     // вид девиации
            bManipulation = 0;  // вид манипуляции           
            bDuration = 0;      // длительность    
        }
    }
    // структура ФРЧ на РП для отправки в ФП
    public struct TParamFWS
    {
        public byte bCount;
        public TOneSourceSupress[] SourceSupress;
        public void Init()
        {
            bCount = 0;
        }
    }
    // структура Параметры РП + длительность для отправки в ФП
    public struct TDurationParamFWS
    {
        public int iDuration;           // длительность
        public byte bCount;
        public TOneSourceSupress[] SourceSupress;
    }
    // структура ППРЧ на РП для отправки в ФП
    public struct TDurationParamFHSS
    {
        public int iFreqMin;        // частота
        public int iDuration;       // длительность импульса излучения
        public byte bCodeFFT;       // код БПФ
        public byte bModulation;    // вид модуляции
        public byte bDeviation;     // вид девиации
        public byte bManipulation;  // вид манипуляции           
        public byte bDuration;      // длительность    - всегда 0
    }
    //след 2 структуры для 18 шифра
    public struct TParamFHSS//исправить
    {
        public int iFreqMin;        // частота
        public byte bCodeFFT;       // код БПФ
        public byte bModulation;    // вид модуляции
        public byte bDeviation;     // вид девиации
        public byte bManipulation;  // вид манипуляции           
    }
    public struct TDurationParamFHSSForMoreOneNetworks
    {
        public int iDuration;           // длительность
        public byte bCount;
        public TParamFHSS[] AllParamFHSS;
    }
    //
    //код ошибки
    public struct TCodeError
    {
        public byte bCodeError;
        public void Init()
        {
            bCodeError = 0;
        }
    }
    public struct TCodeErrorInform
    {
        public byte bCodeError;
        public byte[] bInform;
        public void Init()
        {
            bCodeError = 0;
        }
    }
    // служебное поле
    public struct TServiceField
    {
        public byte bAdressReceiver;
        public byte bAdressSender;
        public byte bCode;
        public byte bCounter;
        public byte usLengthInform;// длина информационного поля
        public void Init()
        {
            bAdressReceiver = 0;
            bAdressSender = 0;
            bCode = 0;
            bCounter = 0;
            usLengthInform = 0;
        }
    }
    public class ModuleSHS
    {
        //константы для шифров
        const byte RADIAT_ON = 3;
        const byte RADIAT_OFF = 10;
        const byte REQUEST_STATE = 1;
        const byte PARAM_FWS = 4;
        const byte DURAT_PARAM_FWS = 5;
        const byte PARAM_FHSS = 11;
        const byte REQUEST_VOLTAGE = 6;
        const byte REQUEST_POWER = 7;
        const byte REQUEST_CURRENT = 8;
        const byte REQUEST_TEMP = 9;
        const byte TYPE_LOAD = 2;
        const byte RESET = 15;
        const byte STOP_FHSS = 12;//Выход из режима РП-ППРЧ
        const byte TEST = 13;
        const byte PARAM_FHSSForMoreOneNetwork= 18;
        public byte bLastStateQuery;            // номер литеры при запросе состояния
        public byte bLastPowerQuery;            // номер литеры при запросе мощности
        public byte bLastVoltageQuery;          // номер литеры при запросе напряжения
        public byte bLastCurrentQuery;          // номер литеры при запросе тока
        public byte bLastTempQuery;             // номер литеры при запросе температуры
        UdpClient udpSIS;              // udp клиент для ФП
        IPEndPoint localIPEndPoint;    // точка для udp   ;IPEndPoint Представляет сетевую конечную точка в виде IP-адреса и номера порта.     
        IPEndPoint remoteIPEndPoint;   // точка для udp ; remote - удаленный       
        Thread thrRead;                // поток приема кодограмм от ФП
        public byte bAdressSIS;
        public byte bAdressClient;
        TServiceField ServiceFieldRead;
        int iDelayTime = 1000;//может потом сделать конструктор
        TCodeError tCodeErrorRead;
        TCodeErrorInform tCodeErrorReadInform;
        //
        //для счетчика кодограм
        private Semaphore smphForCount;
        byte bCountSendCmd;
        byte bCountReceiveCmd;
        //
        //
        String NameFolder;
        String NameFile;
        int AmountOfRecordPart;
        int NumberOfRecordPart;
        byte[] ArrayForFile;
        int AmountRecordsByte;
        int SizeOfPart = 1024;
        int SizeOfNumber = 2;
        int ToTheEnd;
        int SendingPart = 0;
        bool bBeingSendRecord;
        int iSizeOfPartForOnline=1200;//сделать ф-ию изменения
        bool bOnlineContinuing = false;
        bool bLastPartOfRecordForOnline = false;
        // СОБЫТИЯ 
        // наличие (отсутствие) подключения
        public delegate void ConnectEventHandler();//делегат обработка события подключения
        public event ConnectEventHandler ConnectNet;// событие подключния, связанное с делегатом ConnectEventHandler
        public event ConnectEventHandler DisconnectNet;// событие отключения
        protected virtual void OnConnectNet()// функция передачи параметров в событие
        {
            if (ConnectNet != null)//если событию присвоена некоторая функция
            {
                ConnectNet();//Raise the event
            }
        }
        protected virtual void OnDisconnectNet()
        {
            if (DisconnectNet != null)
            {
                DisconnectNet();//Raise the event
            }
        }
        // принята кодограмма
        public delegate void ReceiveEventHandler(byte bCode, object obj);//делегат обработки события получения кодограммы
        public event ReceiveEventHandler ReceiveCmd;// событие получения команды
        public virtual void OnReceiveCmd(byte bCode, object obj)
        {
            if (ReceiveCmd != null)// если кому-то надо, т.е. я не приписываю эти функции
            {
                ReceiveCmd(bCode, obj);//Raise the event
            }
        }
        // отправлена кодограмма
        public delegate void SendEventHandler(byte bCode, object obj);
        public event SendEventHandler SendCmd;
        protected virtual void OnSendCmd(byte bCode, object obj)
        {
            if (SendCmd != null)
            {
                SendCmd(bCode, obj);//Raise the event
            }
        }
        // принят массив байт
        public delegate void ReceiveByteEventHandler(byte[] bByte);
        public event ReceiveByteEventHandler ReceiveByte;
        protected virtual void OnReceiveByte(byte[] bByte)
        {
            if (ReceiveByte != null)
            {
                ReceiveByte(bByte);//Raise the event
            }
        }
        // отпрвлен массив байт
        public delegate void SendByteEventHandler(byte[] bByte);
        public event SendByteEventHandler SendByte;
        protected void OnSendByte(byte[] bByte)
        {
            if (SendByte != null)
            {
                SendByte(bByte);//Raise the event
            }
        }
        // обновить состояние 
        public delegate void UpdateEventHandler(byte bLetter, TCodeErrorInform tCodeErrorInform);
        public event UpdateEventHandler UpdateState;
        public event UpdateEventHandler UpdatePower;
        public event UpdateEventHandler UpdateVoltage;
        public event UpdateEventHandler UpdateCurrent;
        public event UpdateEventHandler UpdateTemp;
        protected void OnUpdatePower(byte bLetter, TCodeErrorInform tCodeErrorInform)
        {
            if (UpdatePower != null)
            {
                UpdatePower(bLetter, tCodeErrorInform);//Raise the event
            }
        }
        //для перессылки аудиозаписи
        public delegate void SendRecordEventHandler(bool CorrectSending);
        public event SendRecordEventHandler SendRecord;
        protected void OnSendRecord(bool CorrectSending)
        {
            if (SendRecord != null)
            {
                SendRecord(CorrectSending);//Raise the event
            }
        }
        public delegate void SendRecorPartdEventHandler(int count, int number);
        public event SendRecorPartdEventHandler SendRecordPart;
        protected void OnSendRecordPart(int count, int number)
        {
            if (SendRecordPart != null)
            {
                SendRecordPart(count, number);//Raise the event
            }
        }
        //для отправки аудиозаписи онлайн
        public delegate void AgreementForOnlineRecordSendingEventHandler(bool YouCan);
        public event AgreementForOnlineRecordSendingEventHandler AgreementForOnlineRecordSending;
        protected void OnAgreementForOnlineRecordSendingEventHandler(bool YouCan)
        {
            if (AgreementForOnlineRecordSending != null)
            {
                AgreementForOnlineRecordSending(YouCan);//Raise the event
            }
        }
        public delegate void SendRecorPartForOnlinedEventHandler( bool bSent);
        public event SendRecorPartForOnlinedEventHandler SendRecordPartForOnline;
        protected void OnSendRecordPartForOnline(bool bSent)
        {
            if (SendRecordPart != null)
            {
                SendRecordPartForOnline(bSent);//Raise the event
            }
        }
        //для Hex
        public delegate void SendOrReceiveArray(byte[] bArray);
        public event SendOrReceiveArray SendArray;
        protected void OnSendArray(byte[] bArray)
        {
            if (SendArray != null)
            {
                SendArray(bArray);//Raise the event
            }
        }
        public event SendOrReceiveArray ReceiveArray;
        protected void OnReceiveArray(byte[] bArray)
        {
            if (ReceiveArray != null)
            {
                ReceiveArray(bArray);//Raise the event
            }
        }
        //Утеряна кодограмма
        public delegate void LostCmd();
        public event LostCmd LostAnyCmd;
        //
        /*public static void TestFunk(byte bLetter, byte bCode, object obj)//??  //
        {
            ReceiveCmd(bCode, obj);//передали параметры в событие получения кодограмм

            switch (bCode)//в зависимости от кода
            {
                //требуют выключить излучение
                case REQUEST_POWER:
                    UpdatePower(bLetter, (TCodeErrorInform)obj);//передаем в событие изменения силы тока параметры 
                    break;
                //
                case REQUEST_VOLTAGE:
                    UpdateVoltage(bLetter, (TCodeErrorInform)obj);
                    break;

                case REQUEST_CURRENT:
                    UpdateCurrent(bLetter, (TCodeErrorInform)obj);
                    break;
                //
                case REQUEST_TEMP:
                    UpdateTemp(bLetter, (TCodeErrorInform)obj);
                    break;
                //
                case REQUEST_STATE:
                    UpdateState(bLetter, (TCodeErrorInform)obj);
                    break;
            }
        }*/
        public ModuleSHS()
        {
            smphForCount = new Semaphore(0, 1);
            bCountSendCmd = 0;
            bCountReceiveCmd = 0;
        }
        public void InitConst(byte BAdressSIS1, byte BAdressClient1)//инициализируем адреса
        {
            bAdressSIS = BAdressSIS1;
            bAdressClient = BAdressClient1;
        }
        public void ChangeDelayTime(int iNewDelayTime)
        {
            iDelayTime = iNewDelayTime;
        }
        /*private void UpdateStateWork(byte bLetter, TCodeErrorInform tCodeErrorInform)// обновление рабочего состояния, приходит номер литеры и код шибки  //
        {//blLetterSet формируется код ошибки bl 
            switch (tCodeErrorInform.bInform.Length)// длина массива информации об ошибке, т.е. был запрос о состоянии одной литеры или всех
            {
                case 1:
                    if (tCodeErrorInform.bCodeError == 0)// одна литера
                    {
                        bool[] blLetterSet = new bool[7];
                        if (tCodeErrorInform.bInform[0] == 0)// если все хорошо т.е. все биты нулевые, т.е и байт нулевой
                            blLetterSet[bLetter - 1] = true;
                        else
                            blLetterSet[bLetter - 1] = false;
                    }
                    break;
                case 7:
                    if (tCodeErrorInform.bCodeError == 0)
                    {
                        bool[] blLetterSet = new bool[7];
                        for (int i = 0; i < 7; i++)
                        {
                            if (tCodeErrorInform.bInform[i] == 0)
                                blLetterSet[i] = true;
                            else
                                blLetterSet[i] = false;
                        }
                    }
                    break;
                default:
                    break;
            }
        }*/
        // поток приема кодограмм от ФП
        private void ReceiveData()// получение данных
        {
            int i = 0;
            while (true)// работает постоянно
            {
                try
                {
                    byte[] bRead = udpSIS.Receive(ref localIPEndPoint);//Receive Возвращает UDP-датаграмму, которая была послана удаленным узлом.
                    i = bRead.Length;// длина полученной кодограммы
                    /*if (i == 0)// перестал передавать
                    {
                        DisconnectClient();//отсоединяем
                        return;
                    }*/
                    //Array.Resize(ref bRead, i);
                    if (i > 0)
                    {
                        OnReceiveByte(bRead);// событие получения кодограммы
                        byte bCode = 0;
                        if (bRead.Length >= 5)//не должно быть короче 5
                        {
                            bCode = bRead[2];// шифр кодограммы
                            //byte[] bReadInform = new byte[1];
                            object obj;
                            // считываем служебную часть кодограммы
                            ServiceFieldRead.Init();// инициализируем объект структыры служебного сообщения
                            ServiceFieldRead.bAdressReceiver = bRead[0];// Заполняем отправителя
                            ServiceFieldRead.bAdressSender = bRead[1];
                            ServiceFieldRead.bCode = bRead[2];
                            ServiceFieldRead.bCounter = bRead[3];
                            ServiceFieldRead.usLengthInform = bRead[4];
                            byte[] bReadInform = new byte[ServiceFieldRead.usLengthInform];// считанная информация из информационного поля, массив длинной  длины информационного поля
                            switch (bCode)//в зависимости от шифра, ответы на запросы
                            {
                                // состояние литеры
                                case REQUEST_STATE:
                                    if (tCodeErrorReadInform.bInform != null)
                                        tCodeErrorReadInform.bInform = null;
                                    tCodeErrorReadInform.bInform = new byte[bRead.Length - 6];// считанная информация об ошибке без кода ошибки( размер полученной инфы - 6)
                                    tCodeErrorReadInform.Init();
                                    tCodeErrorReadInform.bCodeError = bRead[5];// код ошибки
                                    //копируем с 6 элемента из считанной инфы в информацию об ошибки в место с первого bRead.Length - 6 количество раз
                                    Array.Copy(bRead, 6, tCodeErrorReadInform.bInform, 0, bRead.Length - 6);
                                    ReceiveCmd(REQUEST_STATE, tCodeErrorReadInform);// передать параметры в событие ReceiveCMD// подтверждение принятия
                                    //UpdateStateWork(bLastStateQuery, tCodeErrorReadInform);// вызов функции обновления состояния литеры, заполняет байты состояния литер
                                    UpdateState(bLastStateQuery, tCodeErrorReadInform);
                                    //дублировать
                                    smphForCount.Release();
                                    bCountReceiveCmd++;
                                    if (bCountReceiveCmd != bCountSendCmd)
                                    {
                                        LostAnyCmd();
                                        bCountReceiveCmd = 0;
                                        bCountSendCmd = 0;
                                    }
                                    else if (bCountReceiveCmd==255)
                                    {
                                        bCountReceiveCmd = 0;
                                        bCountSendCmd = 0;
                                    }
                                    smphForCount.WaitOne();
                                    //
                                    break;
                                // установить тип нагрузки
                                case TYPE_LOAD:
                                    //tCodeErrorRead.Init();
                                    tCodeErrorRead.bCodeError = bRead[5];
                                    ReceiveCmd(TYPE_LOAD, tCodeErrorRead);
                                    //дублировать
                                     smphForCount.Release();
                                    bCountReceiveCmd++;
                                    if (bCountReceiveCmd != bCountSendCmd)
                                    {
                                        LostAnyCmd();
                                        bCountReceiveCmd = 0;
                                        bCountSendCmd = 0;
                                    }
                                    else if (bCountReceiveCmd==255)
                                    {
                                        bCountReceiveCmd = 0;
                                        bCountSendCmd = 0;
                                    }
                                    smphForCount.WaitOne();
                                    //
                                    break;
                                // включить излучение
                                case RADIAT_ON:
                                    //tCodeErrorRead.Init();
                                    tCodeErrorRead.bCodeError = bRead[5];
                                    ReceiveCmd(RADIAT_ON, tCodeErrorRead);
                                    //дублировать
                                     smphForCount.Release();
                                    bCountReceiveCmd++;
                                    if (bCountReceiveCmd != bCountSendCmd)
                                    {
                                        LostAnyCmd();
                                        bCountReceiveCmd = 0;
                                        bCountSendCmd = 0;
                                    }
                                    else if (bCountReceiveCmd==255)
                                    {
                                        bCountReceiveCmd = 0;
                                        bCountSendCmd = 0;
                                    }
                                    smphForCount.WaitOne();
                                    //
                                    break;
                                // установить параметры РП ФРЧ
                                case PARAM_FWS:
                                    //tCodeErrorRead.Init();
                                    tCodeErrorRead.bCodeError = bRead[5];
                                    ReceiveCmd(PARAM_FWS, tCodeErrorRead);
                                    //дублировать
                                     smphForCount.Release();
                                    bCountReceiveCmd++;
                                    if (bCountReceiveCmd != bCountSendCmd)
                                    {
                                        LostAnyCmd();
                                        bCountReceiveCmd = 0;
                                        bCountSendCmd = 0;
                                    }
                                    else if (bCountReceiveCmd==255)
                                    {
                                        bCountReceiveCmd = 0;
                                        bCountSendCmd = 0;
                                    }
                                    smphForCount.WaitOne();
                                    //
                                    break;
                                // установить параметры РП ФРЧ + вкл. излучение 
                                case DURAT_PARAM_FWS:
                                    //tCodeErrorRead.Init();
                                    tCodeErrorRead.bCodeError = bRead[5];
                                    ReceiveCmd(DURAT_PARAM_FWS, tCodeErrorRead);
                                    //дублировать
                                    smphForCount.Release();
                                    bCountReceiveCmd++;
                                    if (bCountReceiveCmd != bCountSendCmd)
                                    {
                                        LostAnyCmd();
                                        bCountReceiveCmd = 0;
                                        bCountSendCmd = 0;
                                    }
                                    else if (bCountReceiveCmd==255)
                                    {
                                        bCountReceiveCmd = 0;
                                        bCountSendCmd = 0;
                                    }
                                    smphForCount.WaitOne();
                                    //
                                    break;
                                // напряжение
                                case REQUEST_VOLTAGE:
                                    if (tCodeErrorReadInform.bInform != null)
                                        tCodeErrorReadInform.bInform = null;
                                    tCodeErrorReadInform.bInform = new byte[bRead.Length - 6];
                                    tCodeErrorReadInform.Init();
                                    tCodeErrorReadInform.bCodeError = bRead[5];
                                    Array.Copy(bRead, 6, tCodeErrorReadInform.bInform, 0, bRead.Length - 6);
                                    ReceiveCmd(REQUEST_VOLTAGE, tCodeErrorReadInform);
                                    UpdateVoltage(bLastVoltageQuery, tCodeErrorReadInform);
                                    //дублировать
                                     smphForCount.Release();
                                    bCountReceiveCmd++;
                                    if (bCountReceiveCmd != bCountSendCmd)
                                    {
                                        LostAnyCmd();
                                        bCountReceiveCmd = 0;
                                        bCountSendCmd = 0;
                                    }
                                    else if (bCountReceiveCmd==255)
                                    {
                                        bCountReceiveCmd = 0;
                                        bCountSendCmd = 0;
                                    }
                                    smphForCount.WaitOne();
                                    //
                                    break;
                                // мощность
                                case REQUEST_POWER:
                                    if (tCodeErrorReadInform.bInform != null)
                                        tCodeErrorReadInform.bInform = null;
                                    tCodeErrorReadInform.bInform = new byte[bRead.Length - 6];
                                    tCodeErrorReadInform.Init();
                                    tCodeErrorReadInform.bCodeError = bRead[5];
                                    Array.Copy(bRead, 6, tCodeErrorReadInform.bInform, 0, bRead.Length - 6);
                                    ReceiveCmd(REQUEST_POWER, tCodeErrorReadInform);
                                    UpdatePower(bLastPowerQuery, tCodeErrorReadInform);
                                    //дублировать
                                     smphForCount.Release();
                                    bCountReceiveCmd++;
                                    if (bCountReceiveCmd != bCountSendCmd)
                                    {
                                        LostAnyCmd();
                                        bCountReceiveCmd = 0;
                                        bCountSendCmd = 0;
                                    }
                                    else if (bCountReceiveCmd==255)
                                    {
                                        bCountReceiveCmd = 0;
                                        bCountSendCmd = 0;
                                    }
                                    smphForCount.WaitOne();
                                    //
                                    break;
                                // ток
                                case REQUEST_CURRENT:
                                    if (tCodeErrorReadInform.bInform != null)
                                        tCodeErrorReadInform.bInform = null;
                                    tCodeErrorReadInform.bInform = new byte[bRead.Length - 6];
                                    tCodeErrorReadInform.Init();
                                    tCodeErrorReadInform.bCodeError = bRead[5];
                                    Array.Copy(bRead, 6, tCodeErrorReadInform.bInform, 0, bRead.Length - 6);
                                    ReceiveCmd(REQUEST_CURRENT, tCodeErrorReadInform);
                                    UpdateCurrent(bLastCurrentQuery, tCodeErrorReadInform);
                                    //дублировать
                                     smphForCount.Release();
                                    bCountReceiveCmd++;
                                    if (bCountReceiveCmd != bCountSendCmd)
                                    {
                                        LostAnyCmd();
                                        bCountReceiveCmd = 0;
                                        bCountSendCmd = 0;
                                    }
                                    else if (bCountReceiveCmd==255)
                                    {
                                        bCountReceiveCmd = 0;
                                        bCountSendCmd = 0;
                                    }
                                    smphForCount.WaitOne();
                                    //
                                    break;
                                // температура
                                case REQUEST_TEMP:
                                    if (tCodeErrorReadInform.bInform != null)
                                        tCodeErrorReadInform.bInform = null;
                                    tCodeErrorReadInform.bInform = new byte[bRead.Length - 6];
                                    tCodeErrorReadInform.Init();
                                    tCodeErrorReadInform.bCodeError = bRead[5];
                                    Array.Copy(bRead, 6, tCodeErrorReadInform.bInform, 0, bRead.Length - 6);
                                    ReceiveCmd(REQUEST_TEMP, tCodeErrorReadInform);
                                    UpdateTemp(bLastTempQuery, tCodeErrorReadInform);
                                    //дублировать
                                     smphForCount.Release();
                                    bCountReceiveCmd++;
                                    if (bCountReceiveCmd != bCountSendCmd)
                                    {
                                        LostAnyCmd();
                                        bCountReceiveCmd = 0;
                                        bCountSendCmd = 0;
                                    }
                                    else if (bCountReceiveCmd==255)
                                    {
                                        bCountReceiveCmd = 0;
                                        bCountSendCmd = 0;
                                    }
                                    smphForCount.WaitOne();
                                    //
                                    break;
                                // выключить излучение
                                case RADIAT_OFF:
                                    //tCodeErrorRead.Init();
                                    tCodeErrorRead.bCodeError = bRead[5];
                                    ReceiveCmd(RADIAT_OFF, tCodeErrorRead);
                                    //дублировать
                                     smphForCount.Release();
                                    bCountReceiveCmd++;
                                    if (bCountReceiveCmd != bCountSendCmd)
                                    {
                                        LostAnyCmd();
                                        bCountReceiveCmd = 0;
                                        bCountSendCmd = 0;
                                    }
                                    else if (bCountReceiveCmd==255)
                                    {
                                        bCountReceiveCmd = 0;
                                        bCountSendCmd = 0;
                                    }
                                    smphForCount.WaitOne();
                                    //
                                    break;
                                // установить параметры РП-ППРЧ
                                case PARAM_FHSS:
                                    //tCodeErrorRead.Init();
                                    tCodeErrorRead.bCodeError = bRead[5];
                                    ReceiveCmd(PARAM_FHSS, tCodeErrorRead);
                                    //дублировать
                                    smphForCount.Release();
                                    bCountReceiveCmd++;
                                    if (bCountReceiveCmd != bCountSendCmd)
                                    {
                                        LostAnyCmd();
                                        bCountReceiveCmd = 0;
                                        bCountSendCmd = 0;
                                    }
                                    else if (bCountReceiveCmd==255)
                                    {
                                        bCountReceiveCmd = 0;
                                        bCountSendCmd = 0;
                                    }
                                    smphForCount.WaitOne();
                                    //
                                    break;
                                // выход из режима РП-ППРЧ
                                case STOP_FHSS:
                                    //tCodeErrorRead.Init();
                                    tCodeErrorRead.bCodeError = bRead[5];
                                    ReceiveCmd(STOP_FHSS, tCodeErrorRead);
                                    //дублировать
                                     smphForCount.Release();
                                    bCountReceiveCmd++;
                                    if (bCountReceiveCmd != bCountSendCmd)
                                    {
                                        LostAnyCmd();
                                        bCountReceiveCmd = 0;
                                        bCountSendCmd = 0;
                                    }
                                    else if (bCountReceiveCmd==255)
                                    {
                                        bCountReceiveCmd = 0;
                                        bCountSendCmd = 0;
                                    }
                                    smphForCount.WaitOne();
                                    //
                                    break;
                                // сброс аварии
                                case RESET:
                                    tCodeErrorRead.Init();
                                    tCodeErrorRead.bCodeError = bRead[5];
                                    ReceiveCmd(RESET, tCodeErrorRead);
                                    //дублировать
                                     smphForCount.Release();
                                    bCountReceiveCmd++;
                                    if (bCountReceiveCmd != bCountSendCmd)
                                    {
                                        LostAnyCmd();
                                        bCountReceiveCmd = 0;
                                        bCountSendCmd = 0;
                                    }
                                    else if (bCountReceiveCmd==255)
                                    {
                                        bCountReceiveCmd = 0;
                                        bCountSendCmd = 0;
                                    }
                                    smphForCount.WaitOne();
                                    //
                                    break;
                                case PARAM_FHSSForMoreOneNetwork:
                                    //tCodeErrorRead.Init();
                                    tCodeErrorRead.bCodeError = bRead[5];
                                    ReceiveCmd(PARAM_FHSSForMoreOneNetwork, tCodeErrorRead);
                                    //дублировать
                                     smphForCount.Release();
                                    bCountReceiveCmd++;
                                    if (bCountReceiveCmd != bCountSendCmd)
                                    {
                                        LostAnyCmd();
                                        bCountReceiveCmd = 0;
                                        bCountSendCmd = 0;
                                    }
                                    else if (bCountReceiveCmd==255)
                                    {
                                        bCountReceiveCmd = 0;
                                        bCountSendCmd = 0;
                                    }
                                    smphForCount.WaitOne();
                                    //
                                    break;
                                case 16:
                                    tCodeErrorRead.Init();
                                    tCodeErrorRead.bCodeError = bRead[5];
                                    //ReceiveCmd(16, tCodeErrorRead);
                                    if (bRead[5] == 0)
                                    {
                                        if (bOnlineContinuing == false)
                                        {
                                            bool F = SendFristPartsOfRecord();
                                            if (F == false)
                                                SendRecord(false);// событие корректности отправки
                                        }
                                        else
                                        {
                                            AgreementForOnlineRecordSending(true);// событие того, что разрешили отправлять онлайн
                                        }
                                    }
                                    else
                                        SendRecord(false);
                                    //дублировать
                                    smphForCount.Release();
                                    bCountReceiveCmd++;
                                    if (bCountReceiveCmd != bCountSendCmd)
                                    {
                                        LostAnyCmd();
                                        bCountReceiveCmd = 0;
                                        bCountSendCmd = 0;
                                    }
                                    else if (bCountReceiveCmd==255)
                                    {
                                        bCountReceiveCmd = 0;
                                        bCountSendCmd = 0;
                                    }
                                    smphForCount.WaitOne();
                                    //
                                    break;
                                case 17:
                                    if (bOnlineContinuing == false)//поменять код под новый протокол
                                    {
                                        tCodeErrorRead.Init();
                                        tCodeErrorRead.bCodeError = bRead[5];
                                        // ReceiveCmd(17, tCodeErrorRead);
                                        byte[] bAmountFromPG = new byte[4];
                                        Array.Copy(bRead, 8, bAmountFromPG, 0, 2);
                                        int iAmountFromPG = BitConverter.ToInt32(bAmountFromPG, 0);
                                        if (bRead[5] == 0 && iAmountFromPG == AmountOfRecordPart)  
                                            SendRecord(true);
                                        else if (bRead[5] == 0 && iAmountFromPG != AmountOfRecordPart)
                                        {
                                            bool F = SendNotFristPartsOfRecord();
                                            if (F == false)
                                                SendRecord(false);
                                        }
                                        else if (bRead[5] != 0)
                                            SendRecord(false);
                                    }
                                    else
                                    {
                                        tCodeErrorRead.Init();
                                        tCodeErrorRead.bCodeError = bRead[5];
                                        // ReceiveCmd(17, tCodeErrorRead);
                                        if (bRead[5] == 0)
                                        {
                                            if (bLastPartOfRecordForOnline == true)
                                                SendRecord(true);
                                            else
                                                SendRecordPartForOnline(true);
                                        }
                                        else
                                            SendRecord(false);
                                    }
                                    //дублировать
                                     smphForCount.Release();
                                    bCountReceiveCmd++;
                                    if (bCountReceiveCmd != bCountSendCmd)
                                    {
                                        LostAnyCmd();
                                        bCountReceiveCmd = 0;
                                        bCountSendCmd = 0;
                                    }
                                    else if (bCountReceiveCmd==255)
                                    {
                                        bCountReceiveCmd = 0;
                                        bCountSendCmd = 0;
                                    }
                                    smphForCount.WaitOne();
                                    //
                                    break;
                                // неверный шифр
                                default:
                                    break;
                            }
                        }
                    }
                    ReceiveArray(bRead);// мое получение кодограммы
                }
                catch (System.Exception)
                {
                    //MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.ServiceNotification);
                }
            }
        }
        public bool ConnectClient(string strIP_SIS, string strIP_Client, int iPortSIS, int iPortClient)// соединение с клиентом
        {//
            if (udpSIS != null)// если существующее подключение не пусто
            {
                udpSIS.Close();//Close Закрывает UDP-подключения. 
                udpSIS = null;
            }
            if (thrRead != null)// если поток не пуст
            {
                thrRead.Abort();//прерывание потока
                thrRead.Join(500);//завершит поток через 500 млс
                thrRead = null;
            }
            if (remoteIPEndPoint != null)
                remoteIPEndPoint = null;
            try
            {
                localIPEndPoint = new IPEndPoint(IPAddress.Parse(strIP_Client), iPortClient);// присваиваем айпишник и номер порта сокету
                remoteIPEndPoint = new IPEndPoint(IPAddress.Parse(strIP_SIS), iPortSIS);
                udpSIS = new UdpClient(localIPEndPoint);
                thrRead = new Thread(new ThreadStart(ReceiveData));
                thrRead.IsBackground = true;// задаем поток фоновым
                thrRead.Start();// запускаем поток
                OnConnectNet();// коннектимся
                return true;
            }
            catch (System.Exception)
            {
                OnDisconnectNet();// разрываем соединение
                return false;
            }
        }
        public void DisconnectClient()// разъединение с клиентом
        {
            if (udpSIS != null)
            {
                udpSIS.Close();
                udpSIS = null;
            }
            if (thrRead != null)
            {
                thrRead.Abort();
                thrRead.Join(500);
                thrRead = null;
            }
            OnDisconnectNet();// разъединение
        }
        public bool SendRadiatOn(TSwitchRadiat tSwitchRadiat)// Установить параметры РП для ИРИ ФРЧ и включить излучение заданной длительности
        {
            //создатся запрос
            byte[] bSend = new byte[8];
            bSend[0] = bAdressClient;
            bSend[1] = bAdressSIS;
            bSend[2] = RADIAT_ON;
            bSend[3] = 0;
            bSend[4] = 3;
            Array.Copy(BitConverter.GetBytes(tSwitchRadiat.iDuration), 0, bSend, 5, 3);// копрует эелементы из BitConverter.GetBytes(tSwitchRadiat.iDuration) в bSend начиная c 0ого 3 раза с 5 позиции
            //подтвержение корректности
            SendCmd(RADIAT_ON, tSwitchRadiat);
            OnSendArray(bSend);
            //отправление данных ФП
            if (SendData(bSend))//если отправилось
            {
                bSend = null;
                //дублировать
                smphForCount.Release();
                bCountSendCmd++;
                smphForCount.WaitOne();
                //
                return true;
            }
            else
            {
                bSend = null;
                return false;
            }
        }
        public bool SendRadiatOff()//запрос выключить излучение
        {
            byte[] bSend = new byte[5];
            bSend[0] = bAdressClient;
            bSend[1] = bAdressSIS;
            bSend[2] = RADIAT_OFF;
            bSend[3] = 0;
            bSend[4] = 0;
            SendCmd(RADIAT_OFF, null);
            OnSendArray(bSend);
            if (SendData(bSend))
            {
                bSend = null;
                //дублировать
                smphForCount.Release();
                bCountSendCmd++;
                smphForCount.WaitOne();
                //
                return true;
            }
            else
            {
                bSend = null;
                return false;
            }
        }
        public bool SendTestArray()
        {
            byte[] bSend = new byte[1400];
            bSend[0] = bAdressClient;
            bSend[1] = bAdressSIS;
            bSend[2] = TEST;
            bSend[3] = 0;
            bSend[4] = 0;
            SendCmd(TEST, null);
            OnSendArray(bSend);
            if (SendData(bSend))
            {
                bSend = null;
                //дублировать
                smphForCount.Release();
                bCountSendCmd++;
                smphForCount.WaitOne();
                //
                return true;
            }
            else
            {
                bSend = null;
                return false;
            }
        }
        public bool SendRequestState(TRequestParam tRequestParam)//состояние одной литеры
        {
            byte[] bSend = new byte[6];// состояние литеры 6 байт
            bSend[0] = bAdressClient;
            bSend[1] = bAdressSIS;
            bSend[2] = REQUEST_STATE;// состояние литеры
            bSend[3] = 0;
            bSend[4] = 1;
            bSend[5] = tRequestParam.bLetter;// номер литеры
            bLastStateQuery = tRequestParam.bLetter;// номер запрашиваемой литеры
            SendCmd(REQUEST_STATE, tRequestParam);// передача параметра в событие передачи команды
            OnSendArray(bSend);
            if (SendData(bSend))
            {
                bSend = null;
                //дублировать
                smphForCount.Release();
                bCountSendCmd++;
                smphForCount.WaitOne();
                //
                return true;
            }
            else
            {
                bSend = null;
                return false;
            }
        }
        public bool SendParamFWS(TParamFWS tParamFWS)//Отправить параметры РП для ИРИ ФРЧ
        {
            byte[] bSend = new byte[5 + tParamFWS.bCount * 7];//слуебное поле и по 7 на каждый ИРИ
            bSend[0] = bAdressClient;
            bSend[1] = bAdressSIS;
            bSend[2] = PARAM_FWS;
            bSend[3] = 0;
            bSend[4] = Convert.ToByte(tParamFWS.bCount * 7);
            for (int i = 0; i < tParamFWS.bCount; i++)
            {
                Array.Copy(BitConverter.GetBytes(tParamFWS.SourceSupress[i].uiFreq / 10), 0, bSend, i * 7 + 5, 3);//передается частота
                bSend[i * 7 + 8] = tParamFWS.SourceSupress[i].bModulation;
                bSend[i * 7 + 9] = tParamFWS.SourceSupress[i].bDeviation;
                bSend[i * 7 + 10] = tParamFWS.SourceSupress[i].bManipulation;
                bSend[i * 7 + 11] = tParamFWS.SourceSupress[i].bDuration;
            }
            SendCmd(PARAM_FWS, tParamFWS);
            OnSendArray(bSend);
            if (SendData(bSend))
            {
                bSend = null;
                //дублировать
                smphForCount.Release();
                bCountSendCmd++;
                smphForCount.WaitOne();
                //
                return true;
            }
            else
            {
                bSend = null;
                return false;
            }
        }
        public bool SendDurationParamFWS(TDurationParamFWS tDurationParamFWS)// Установить параметры РП для ИРИ ФРЧ и включить излучение заданной длительности
        {
            byte[] bSend = new byte[8 + tDurationParamFWS.bCount * 7];
            bSend[0] = bAdressClient;
            bSend[1] = bAdressSIS;
            bSend[2] = DURAT_PARAM_FWS;
            bSend[3] = 0;
            bSend[4] = Convert.ToByte(3 + tDurationParamFWS.bCount * 7);// длина информационного поля
            Array.Copy(BitConverter.GetBytes(tDurationParamFWS.iDuration), 0, bSend, 5, 3);
            for (int i = 0; i < tDurationParamFWS.bCount; i++)
            {
                Array.Copy(BitConverter.GetBytes(tDurationParamFWS.SourceSupress[i].uiFreq / 10), 0, bSend, i * 7 + 8, 3);
                bSend[i * 7 + 11] = tDurationParamFWS.SourceSupress[i].bModulation;
                bSend[i * 7 + 12] = tDurationParamFWS.SourceSupress[i].bDeviation;
                bSend[i * 7 + 13] = tDurationParamFWS.SourceSupress[i].bManipulation;
                bSend[i * 7 + 14] = tDurationParamFWS.SourceSupress[i].bDuration;
            }
            SendCmd(DURAT_PARAM_FWS, tDurationParamFWS);
            OnSendArray(bSend);
            if (SendData(bSend))
            {
                bSend = null;
                //дублировать
                smphForCount.Release();
                bCountSendCmd++;
                smphForCount.WaitOne();
                //
                return true;
            }
            else
            {
                bSend = null;
                return false;
            }
        }
        public bool SendParamFHSS(TDurationParamFHSS tDurationParamFHSS)//запрос "Установить параметры РП для ИРИ ППРЧ"
        {
            byte[] bSend = new byte[15];
            bSend[0] = bAdressClient;
            bSend[1] = bAdressSIS;
            bSend[2] = PARAM_FHSS;
            bSend[3] = 0;
            bSend[4] = 10;
            //BitConverter.GetBytes(tDurationParamFHSS.iFreqMin) возвращает указанное значение 32-битового целого числа со знаком в виде массива байтов.
            Array.Copy(BitConverter.GetBytes(tDurationParamFHSS.iFreqMin), 0, bSend, 5, 3);// копирует BitConverter.GetBytes(tDurationParamFHSS.iFreqMin) с 0-ого лемента в массив bSend с 5 номера в размере 3 элементов
            Array.Copy(BitConverter.GetBytes(tDurationParamFHSS.iDuration), 0, bSend, 8, 3);
            bSend[11] = tDurationParamFHSS.bCodeFFT;
            bSend[12] = tDurationParamFHSS.bModulation;
            bSend[13] = tDurationParamFHSS.bDeviation;
            bSend[14] = tDurationParamFHSS.bManipulation;
            SendCmd(PARAM_FHSS, tDurationParamFHSS);
            OnSendArray(bSend);
            if (SendData(bSend))
            {
                bSend = null;
                //дублировать
                smphForCount.Release();
                bCountSendCmd++;
                smphForCount.WaitOne();
                //
                return true;
            }
            else
            {
                bSend = null;
                return false;
            }
        }
        public bool SendParamFHSSForMoreOneNetwork(TDurationParamFHSSForMoreOneNetworks tDurationParamFHSSForMoreOneNetworks)//проверить
        {
            byte[] bSend = new byte[15];
            bSend[0] = bAdressClient;
            bSend[1] = bAdressSIS;
            bSend[2] = PARAM_FHSSForMoreOneNetwork;
            bSend[3] = 0;
            bSend[4] = Convert.ToByte(3 + 7 * tDurationParamFHSSForMoreOneNetworks.bCount);//длина информационного поля; проверить 
            Array.Copy(BitConverter.GetBytes(tDurationParamFHSSForMoreOneNetworks.iDuration), 0, bSend, 5, 3);// копирует BitConverter.GetBytes(tDurationParamFHSS.iFreqMin) с 0-ого лемента в массив bSend с 5 номера в размере 3 элементов
            for (int i = 0; i < tDurationParamFHSSForMoreOneNetworks.bCount; i++)// проверить насчет длины
            {
                Array.Copy(BitConverter.GetBytes(tDurationParamFHSSForMoreOneNetworks.iDuration), 0, bSend, 7*i+8, 3);
                bSend[7 * i + 11] = tDurationParamFHSSForMoreOneNetworks.AllParamFHSS[i].bCodeFFT;
                bSend[7 * i + 12] = tDurationParamFHSSForMoreOneNetworks.AllParamFHSS[i].bModulation;
                bSend[7 * i + 13] = tDurationParamFHSSForMoreOneNetworks.AllParamFHSS[i].bDeviation;
                bSend[7 * i + 14] = tDurationParamFHSSForMoreOneNetworks.AllParamFHSS[i].bManipulation;
            }
            SendCmd(PARAM_FHSSForMoreOneNetwork, tDurationParamFHSSForMoreOneNetworks);
            OnSendArray(bSend);
            if (SendData(bSend))
            {
                bSend = null;
                //дублировать
                smphForCount.Release();
                bCountSendCmd++;
                smphForCount.WaitOne();
                //
                return true;
            }
            else
            {
                bSend = null;
                return false;
            }
        }
        public bool SendRequestVoltage(TRequestParam tRequestParam)// Запрос "	Параметры усилителя мощности для литеры"
        {
            byte[] bSend = new byte[6];
            bSend[0] = bAdressClient;
            bSend[1] = bAdressSIS;
            bSend[2] = REQUEST_VOLTAGE;
            bSend[3] = 0;
            bSend[4] = 1;
            bSend[5] = tRequestParam.bLetter;
            bLastVoltageQuery = tRequestParam.bLetter;
            SendCmd(REQUEST_VOLTAGE, tRequestParam);
            OnSendArray(bSend);
            if (SendData(bSend))
            {
                bSend = null;
                //дублировать
                smphForCount.Release();
                bCountSendCmd++;
                smphForCount.WaitOne();
                //
                return true;
            }
            else
            {
                bSend = null;
                return false;
            }
        }
        public bool SendRequestCurrent(TRequestParam tRequestParam)
        {
            byte[] bSend = new byte[6];
            bSend[0] = bAdressClient;
            bSend[1] = bAdressSIS;
            bSend[2] = REQUEST_CURRENT;
            bSend[3] = 0;
            bSend[4] = 1;
            bSend[5] = tRequestParam.bLetter;
            bLastCurrentQuery = tRequestParam.bLetter;
            SendCmd(REQUEST_CURRENT, tRequestParam);
            OnSendArray(bSend);
            if (SendData(bSend))
            {
                bSend = null;
                //дублировать
                smphForCount.Release();
                bCountSendCmd++;
                smphForCount.WaitOne();
                //
                return true;
            }
            else
            {
                bSend = null;
                return false;
            }
        }
        public bool SendRequestTemp(TRequestParam tRequestParam)
        {
            byte[] bSend = new byte[6];
            bSend[0] = bAdressClient;
            bSend[1] = bAdressSIS;
            bSend[2] = REQUEST_TEMP;
            bSend[3] = 0;
            bSend[4] = 1;
            bSend[5] = tRequestParam.bLetter;
            bLastTempQuery = tRequestParam.bLetter;
            SendCmd(REQUEST_TEMP, tRequestParam);
            OnSendArray(bSend);
            if (SendData(bSend))
            {
                bSend = null;
                //дублировать
                smphForCount.Release();
                bCountSendCmd++;
                smphForCount.WaitOne();
                //
                return true;
            }
            else
            {
                bSend = null;
                return false;
            }
        }
        public bool SendRequestPower(TRequestParam tRequestParam)
        {
            byte[] bSend = new byte[6];
            bSend[0] = bAdressClient;
            bSend[1] = bAdressSIS;
            bSend[2] = REQUEST_POWER;
            bSend[3] = 0;
            bSend[4] = 1;
            bSend[5] = tRequestParam.bLetter;
            bLastPowerQuery = tRequestParam.bLetter;
            SendCmd(REQUEST_POWER, tRequestParam);
            OnSendArray(bSend);
            if (SendData(bSend))
            {
                bSend = null;
                //дублировать
                smphForCount.Release();
                bCountSendCmd++;
                smphForCount.WaitOne();
                //
                return true;
            }
            else
            {
                bSend = null;
                return false;
            }
        }
        public bool SendTypeLoad(TTypeLoad tTypeLoad)// Запрос "Установить тип нагрузки"
        {
            byte[] bSend = new byte[7];
            bSend[0] = bAdressClient;
            bSend[1] = bAdressSIS;
            bSend[2] = TYPE_LOAD;
            bSend[3] = 0;
            bSend[4] = 2;
            bSend[5] = tTypeLoad.bLetter;//номер литеры
            bSend[6] = tTypeLoad.bType;// тип нагрузки
            SendCmd(TYPE_LOAD, tTypeLoad);
            OnSendArray(bSend);
            if (SendData(bSend))
            {
                bSend = null;
                //дублировать
                smphForCount.Release();
                bCountSendCmd++;
                smphForCount.WaitOne();
                //
                return true;
            }
            else
            {
                bSend = null;
                return false;
            }
        }
        public bool SendReset()// отправить перезагрузку
        {
            byte[] bSend = new byte[5];
            bSend[0] = bAdressClient;
            bSend[1] = bAdressSIS;
            bSend[2] = RESET;
            bSend[3] = 0;
            bSend[4] = 0;
            SendCmd(RESET, null);
            OnSendArray(bSend);
            if (SendData(bSend))
            {
                bSend = null;
                //дублировать
                smphForCount.Release();
                bCountSendCmd++;
                smphForCount.WaitOne();
                //
                return true;
            }
            else
            {
                bSend = null;
                return false;
            }
        }
        public bool SendStopFHSS()//отправить стоп ФПРЧ
        {
            byte[] bSend = new byte[5];
            bSend[0] = bAdressClient;
            bSend[1] = bAdressSIS;
            bSend[2] = STOP_FHSS;//Выход из режима РП-ППРЧ
            bSend[3] = 0;// счетчик кодограм равен 0
            bSend[4] = 0;// длина нформационного поля равна 0
            SendCmd(STOP_FHSS, null);//отправить байт выхода из режима РП-ППРЧ
            OnSendArray(bSend);
            if (SendData(bSend))//если отправилась кодограмма
            {
                bSend = null;
                //дублировать
                smphForCount.Release();
                bCountSendCmd++;
                smphForCount.WaitOne();
                //
                return true;
            }
            else
            {
                bSend = null;
                return false;
            }
        }
        /*public static bool SendRecordForPG(String FolderWithCurrrentRecord, String NameCurrentFileForSending)
        {
            byte[] bSend = new byte[5];// 5+ SizeOfNameслужебное + байтовое предствление имени
            bSend[0] = bAdressClient;
            bSend[1] = bAdressSIS;
            bSend[2] = 16;//запись сделана
            bSend[3] = 0;// счетчик кодограм равен 0
            bSend[4] = 0;//SizeOfName
            SendCmd(16, null);
            return true;
            if (!SendData(bSend))
            {
                bSend = null;
                return false;
            }
            while (bWaiting)
            {
            }
        }*/
        //
        public bool SendSoundRecorded(String FolderWithCurrrentRecord, String NameCurrentFileForSending)//String NameFile кодограмма о том, что запись сделана
        {
            if (FolderWithCurrrentRecord != "NaN" && NameCurrentFileForSending != "NaN")
            {
                NameFolder = FolderWithCurrrentRecord;
                NameFile = NameCurrentFileForSending;
            }
            else
                bOnlineContinuing = true;
            byte[] bSend = new byte[5];
            bSend[0] = bAdressClient;
            bSend[1] = bAdressSIS;
            bSend[2] = 16;//запись сделана
            bSend[3] = 0;// счетчик кодограм равен 0
            bSend[4] = 0;//длина иформационного поля
            if (SendData(bSend))//если отправилась кодограмма
            {
                bSend = null;
                //дублировать
                smphForCount.Release();
                bCountSendCmd++;
                smphForCount.WaitOne();
                //
                return true;
            }
            else
            {
                bSend = null;
                return false;
            }
        }
        public bool SendFristPartsOfRecord()
        {
            ArrayForFile = File.ReadAllBytes(String.Concat(NameFolder, NameFile));
            AmountRecordsByte = ArrayForFile.Length;
            ToTheEnd = AmountRecordsByte;
            AmountOfRecordPart = (int)Math.Ceiling((double)AmountRecordsByte / (double)SizeOfPart);
            NumberOfRecordPart = 1;
            byte[] bSend = new byte[5 + 2 * SizeOfNumber + SizeOfPart];
            bSend[0] = bAdressClient;
            bSend[1] = bAdressSIS;
            bSend[2] = 17;//шифр "очередная порция"
            bSend[3] = 0;// счетчик кодограм равен 0
            bSend[4] = 0;//
            Array.Copy(BitConverter.GetBytes(AmountOfRecordPart), 0, bSend, 5, 2);
            Array.Copy(BitConverter.GetBytes(1), 0, bSend, 7, 2);
            for (int j = 0; j < Math.Min(SizeOfPart, ToTheEnd); j++)
            {
                bSend[9 + j] = ArrayForFile[j];
            }
            if (AmountOfRecordPart == NumberOfRecordPart)
                ArrayForFile = null;
            //ToTheEnd -= SizeOfPart;
            //SendCmd(17, null);//отправить байт выхода из режима РП-ППРЧ
            bool bForesult = SendData(bSend);
            if (bForesult)//если отправилась кодограмма
            {
                bSend = null;
                //дублировать
                smphForCount.Release();
                bCountSendCmd++;
                smphForCount.WaitOne();
                //
                return true;
            }
            else
            {
                bSend = null;
                return false;
            }
        }
        //
        public bool SendPartsOnline(byte[] bFirstPart, bool bLastPart)
        {
            bLastPartOfRecordForOnline = bLastPart;
            //bOnlineContinuing = true;
            byte[] bSend = new byte[5 + 2 * SizeOfNumber + iSizeOfPartForOnline];
            bSend[0] = bAdressClient;
            bSend[1] = bAdressSIS;
            bSend[2] = 17;//шифр "очередная порция"
            bSend[3] = 0;// счетчик кодограм равен 0
            bSend[4] = 1;//т.е. идет запись онлайн
            Array.Copy(BitConverter.GetBytes(bFirstPart.Length), 0, bSend, 5, 2);
            Array.Copy(BitConverter.GetBytes(1), 0, bSend, 7, 2);
            for (int j = 0; j < Math.Min(iSizeOfPartForOnline, bFirstPart.Length); j++)
            {
                bSend[9 + j] = bFirstPart[j];
            }
            //SendCmd(17, null);//отправить байт выхода из режима РП-ППРЧ
            bool bForesult = SendData(bSend);
            if (bForesult)//если отправилась кодограмма
            {
                bSend = null;
                //дублировать
                smphForCount.Release();
                bCountSendCmd++;
                smphForCount.WaitOne();
                //
                return true;
            }
            else
            {
                bSend = null;
                return false;
            }
        }
        //
        public bool SendNotFristPartsOfRecord()//проверка корректного закрытия
        {
            byte[] bSend = new byte[5 + 2 * SizeOfNumber + SizeOfPart];
            bSend[0] = bAdressClient;
            bSend[1] = bAdressSIS;
            bSend[2] = 17;//шифр "очередная порция"
            bSend[3] = 0;// счетчик кодограм равен 0
            bSend[4] = 0;//
            NumberOfRecordPart++;
            Array.Copy(BitConverter.GetBytes(AmountOfRecordPart), 0, bSend, 5, 2);
            Array.Copy(BitConverter.GetBytes(NumberOfRecordPart), 0, bSend, 7, 2);
            ToTheEnd -= SizeOfPart;
            for (int j = 0; j < Math.Min(SizeOfPart, ToTheEnd); j++)
            {
                bSend[9 + j] = ArrayForFile[(NumberOfRecordPart-1)*SizeOfPart +j];
            }
            if (AmountOfRecordPart == NumberOfRecordPart)
                ArrayForFile = null;
            //SendCmd(17, null);//отправить байт выхода из режима РП-ППРЧ
            if (SendData(bSend))//если отправилась кодограмма
            {
                bSend = null;
                //дублировать
                smphForCount.Release();
                bCountSendCmd++;
                smphForCount.WaitOne();
                //
                return true;
            }
            else
            {
                bSend = null;
                return false;
            }
        }
        //
        /*public static bool SendPartsOfRecord(String Folder, String NameFile)//отправка самих частей звукового файла
        {
            byte[] ArrayFoFile = File.ReadAllBytes(String.Concat(Folder, NameFile));
            int SizeOfFile = ArrayFoFile.Length;
            int ToTheEnd = SizeOfFile;
            int SizeOfPart = 1024;
            int CountOfParts = (int)Math.Ceiling((double)SizeOfFile / (double)SizeOfPart);//думать что быстрее
            int SizeOfNamber = 2;
            for (int i = 0; i < CountOfParts; i++)
            {
                byte[] bSend = new byte[5 + 2 * SizeOfNamber + SizeOfPart];
                bSend[0] = bAdressClient;
                bSend[1] = bAdressSIS;
                bSend[2] = 17;//шифр "очередная порция"
                bSend[3] = 0;// счетчик кодограм равен 0
                bSend[4] = 0;//
                Array.Copy(BitConverter.GetBytes(CountOfParts), 0, bSend, 5, 2);
                Array.Copy(BitConverter.GetBytes(i + 1), 0, bSend, 7, 2);
                for (int j = 0; j < Math.Min(SizeOfPart, ToTheEnd); j++)
                {
                    bSend[9 + j] = ArrayFoFile[i * 1024 + j];
                }
                ToTheEnd -= SizeOfPart;
                bool Sending = SendOnePartOfRecord(bSend, CountOfParts, i + 1);
                if (Sending == false)
                    return false;
                Thread.Sleep(iDelayTime);
            }
            return true;
        }
        //
        public static bool SendOnePartOfRecord(byte[] bArraySend, int count, int number)//отправка самих, уже готовых, частей звукового файла
        {
            //SendRecordPart(int count,int number);
            //SendCmd(17, null);//добалено  параметр "запись сделана" и null
            if (SendData(bArraySend))//если отправилась кодограмма
            {
                bArraySend = null;
                return true;
            }
            else
            {
                bArraySend = null;
                return false;
            }
        }*/
        //
        private bool SendData(byte[] bSend)
        {
            try
            {
                OnSendByte(bSend);// выполнить функцию "отправить байты"
                udpSIS.Send(bSend, bSend.Length, remoteIPEndPoint); //Отправляет UDP-датаграмму в узел в указанной удаленн. конечн. т-ке: что отправить, какая длина этого, куда
                return true;
            }
            catch (System.Exception)
            {
                return false;
            }
        }
    }
}

