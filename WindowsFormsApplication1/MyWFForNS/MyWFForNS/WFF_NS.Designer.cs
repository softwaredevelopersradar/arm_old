﻿namespace WndProject
{
    partial class FormForNoiseShaper
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dataGridViewForLetter = new System.Windows.Forms.DataGridView();
            this.dgvPowerLetter = new System.Windows.Forms.DataGridView();
            this.dgvCurrentLetter = new System.Windows.Forms.DataGridView();
            this.dgvVoltageLetter = new System.Windows.Forms.DataGridView();
            this.dgvTempLetter = new System.Windows.Forms.DataGridView();
            this.bConnect = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.bStateButton = new System.Windows.Forms.Button();
            this.bReset = new System.Windows.Forms.Button();
            this.bCurrent = new System.Windows.Forms.Button();
            this.bVoltage = new System.Windows.Forms.Button();
            this.bTemp = new System.Windows.Forms.Button();
            this.bPowerLetter = new System.Windows.Forms.Button();
            this.bExitFWS = new System.Windows.Forms.Button();
            this.bFHSS = new System.Windows.Forms.Button();
            this.bFWS = new System.Windows.Forms.Button();
            this.bExitFHSS = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.bForEL = new System.Windows.Forms.Button();
            this.tlpForButtons = new System.Windows.Forms.TableLayoutPanel();
            this.cbForLetterChosing = new System.Windows.Forms.ComboBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.label2 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.tbFWSTime = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewForLetter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPowerLetter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCurrentLetter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVoltageLetter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTempLetter)).BeginInit();
            this.tlpForButtons.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridViewForLetter
            // 
            this.dataGridViewForLetter.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewForLetter.Location = new System.Drawing.Point(0, 0);
            this.dataGridViewForLetter.Name = "dataGridViewForLetter";
            this.dataGridViewForLetter.Size = new System.Drawing.Size(262, 154);
            this.dataGridViewForLetter.TabIndex = 0;
            this.dataGridViewForLetter.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewForLetter_CellContentClick);
            // 
            // dgvPowerLetter
            // 
            this.dgvPowerLetter.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPowerLetter.Location = new System.Drawing.Point(327, 0);
            this.dgvPowerLetter.Name = "dgvPowerLetter";
            this.dgvPowerLetter.Size = new System.Drawing.Size(70, 154);
            this.dgvPowerLetter.TabIndex = 1;
            this.dgvPowerLetter.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvPowerLetter_CellContentClick);
            // 
            // dgvCurrentLetter
            // 
            this.dgvCurrentLetter.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCurrentLetter.Location = new System.Drawing.Point(439, 0);
            this.dgvCurrentLetter.Name = "dgvCurrentLetter";
            this.dgvCurrentLetter.Size = new System.Drawing.Size(240, 154);
            this.dgvCurrentLetter.TabIndex = 2;
            this.dgvCurrentLetter.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvCurrentLetter_CellContentClick);
            // 
            // dgvVoltageLetter
            // 
            this.dgvVoltageLetter.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvVoltageLetter.Location = new System.Drawing.Point(327, 197);
            this.dgvVoltageLetter.Name = "dgvVoltageLetter";
            this.dgvVoltageLetter.Size = new System.Drawing.Size(70, 150);
            this.dgvVoltageLetter.TabIndex = 3;
            this.dgvVoltageLetter.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvVoltageLetter_CellContentClick);
            // 
            // dgvTempLetter
            // 
            this.dgvTempLetter.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTempLetter.Location = new System.Drawing.Point(439, 197);
            this.dgvTempLetter.Name = "dgvTempLetter";
            this.dgvTempLetter.Size = new System.Drawing.Size(240, 150);
            this.dgvTempLetter.TabIndex = 4;
            this.dgvTempLetter.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvTempLetter_CellContentClick);
            // 
            // bConnect
            // 
            this.bConnect.Location = new System.Drawing.Point(0, 589);
            this.bConnect.Name = "bConnect";
            this.bConnect.Size = new System.Drawing.Size(84, 23);
            this.bConnect.TabIndex = 9;
            this.bConnect.Text = "Подключить";
            this.bConnect.UseVisualStyleBackColor = true;
            this.bConnect.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(96, 28);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 1;
            this.button2.Text = "Сброс УМ";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(4, 70);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 3;
            this.button4.Text = "Напряжение";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(187, 70);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(75, 23);
            this.button6.TabIndex = 6;
            this.button6.Text = "Мощность";
            this.button6.UseVisualStyleBackColor = true;
            // 
            // bStateButton
            // 
            this.bStateButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bStateButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.bStateButton.Location = new System.Drawing.Point(1, 1);
            this.bStateButton.Margin = new System.Windows.Forms.Padding(1);
            this.bStateButton.Name = "bStateButton";
            this.bStateButton.Size = new System.Drawing.Size(85, 35);
            this.bStateButton.TabIndex = 0;
            this.bStateButton.Text = "Сост. лит.";
            this.bStateButton.UseVisualStyleBackColor = true;
            this.bStateButton.Click += new System.EventHandler(this.bStateButton_Click);
            // 
            // bReset
            // 
            this.bReset.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bReset.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.bReset.Location = new System.Drawing.Point(175, 38);
            this.bReset.Margin = new System.Windows.Forms.Padding(1);
            this.bReset.Name = "bReset";
            this.bReset.Size = new System.Drawing.Size(86, 35);
            this.bReset.TabIndex = 1;
            this.bReset.Text = "Сброс аварии";
            this.bReset.UseVisualStyleBackColor = true;
            this.bReset.Click += new System.EventHandler(this.button2_Click);
            // 
            // bCurrent
            // 
            this.bCurrent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bCurrent.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.bCurrent.Location = new System.Drawing.Point(175, 1);
            this.bCurrent.Margin = new System.Windows.Forms.Padding(1);
            this.bCurrent.Name = "bCurrent";
            this.bCurrent.Size = new System.Drawing.Size(86, 35);
            this.bCurrent.TabIndex = 2;
            this.bCurrent.Text = "Ток";
            this.bCurrent.UseVisualStyleBackColor = true;
            this.bCurrent.Click += new System.EventHandler(this.bCurrent_Click);
            // 
            // bVoltage
            // 
            this.bVoltage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bVoltage.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.bVoltage.Location = new System.Drawing.Point(1, 38);
            this.bVoltage.Margin = new System.Windows.Forms.Padding(1);
            this.bVoltage.Name = "bVoltage";
            this.bVoltage.Size = new System.Drawing.Size(85, 35);
            this.bVoltage.TabIndex = 3;
            this.bVoltage.Text = "Напряжение";
            this.bVoltage.UseVisualStyleBackColor = true;
            this.bVoltage.Click += new System.EventHandler(this.button4_Click);
            // 
            // bTemp
            // 
            this.bTemp.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bTemp.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.bTemp.Location = new System.Drawing.Point(88, 38);
            this.bTemp.Margin = new System.Windows.Forms.Padding(1);
            this.bTemp.Name = "bTemp";
            this.bTemp.Size = new System.Drawing.Size(85, 35);
            this.bTemp.TabIndex = 4;
            this.bTemp.Text = "Температура";
            this.bTemp.UseVisualStyleBackColor = true;
            this.bTemp.Click += new System.EventHandler(this.bTemp_Click);
            // 
            // bPowerLetter
            // 
            this.bPowerLetter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bPowerLetter.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.bPowerLetter.Location = new System.Drawing.Point(88, 1);
            this.bPowerLetter.Margin = new System.Windows.Forms.Padding(1);
            this.bPowerLetter.Name = "bPowerLetter";
            this.bPowerLetter.Size = new System.Drawing.Size(85, 35);
            this.bPowerLetter.TabIndex = 6;
            this.bPowerLetter.Text = "Мощность";
            this.bPowerLetter.UseVisualStyleBackColor = true;
            this.bPowerLetter.Click += new System.EventHandler(this.button6_Click);
            // 
            // bExitFWS
            // 
            this.bExitFWS.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bExitFWS.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.bExitFWS.Location = new System.Drawing.Point(175, 112);
            this.bExitFWS.Margin = new System.Windows.Forms.Padding(1);
            this.bExitFWS.Name = "bExitFWS";
            this.bExitFWS.Size = new System.Drawing.Size(86, 35);
            this.bExitFWS.TabIndex = 10;
            this.bExitFWS.Text = "Выкл. изл.";
            this.bExitFWS.UseVisualStyleBackColor = true;
            this.bExitFWS.Click += new System.EventHandler(this.button5_Click);
            // 
            // bFHSS
            // 
            this.bFHSS.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bFHSS.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.bFHSS.Location = new System.Drawing.Point(88, 75);
            this.bFHSS.Margin = new System.Windows.Forms.Padding(1);
            this.bFHSS.Name = "bFHSS";
            this.bFHSS.Size = new System.Drawing.Size(85, 35);
            this.bFHSS.TabIndex = 11;
            this.bFHSS.Text = "ППРЧ на РП";
            this.bFHSS.UseVisualStyleBackColor = true;
            this.bFHSS.Click += new System.EventHandler(this.button7_Click);
            // 
            // bFWS
            // 
            this.bFWS.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bFWS.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.bFWS.Location = new System.Drawing.Point(1, 112);
            this.bFWS.Margin = new System.Windows.Forms.Padding(1);
            this.bFWS.Name = "bFWS";
            this.bFWS.Size = new System.Drawing.Size(85, 35);
            this.bFWS.TabIndex = 12;
            this.bFWS.Text = "ФРЧ на РП";
            this.bFWS.UseVisualStyleBackColor = true;
            this.bFWS.Click += new System.EventHandler(this.button8_Click);
            // 
            // bExitFHSS
            // 
            this.bExitFHSS.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bExitFHSS.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.bExitFHSS.Location = new System.Drawing.Point(175, 75);
            this.bExitFHSS.Margin = new System.Windows.Forms.Padding(1);
            this.bExitFHSS.Name = "bExitFHSS";
            this.bExitFHSS.Size = new System.Drawing.Size(86, 35);
            this.bExitFHSS.TabIndex = 13;
            this.bExitFHSS.Text = "Вых. ППРЧ";
            this.bExitFHSS.UseVisualStyleBackColor = true;
            this.bExitFHSS.Click += new System.EventHandler(this.button9_Click);
            // 
            // button10
            // 
            this.button10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button10.Location = new System.Drawing.Point(88, 149);
            this.button10.Margin = new System.Windows.Forms.Padding(1);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(85, 36);
            this.button10.TabIndex = 14;
            this.button10.Text = "Эквивалент";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // button12
            // 
            this.button12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button12.Location = new System.Drawing.Point(1, 149);
            this.button12.Margin = new System.Windows.Forms.Padding(1);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(85, 36);
            this.button12.TabIndex = 17;
            this.button12.Text = "Антенна";
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Click += new System.EventHandler(this.button12_Click);
            // 
            // bForEL
            // 
            this.bForEL.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bForEL.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.bForEL.Location = new System.Drawing.Point(1, 75);
            this.bForEL.Margin = new System.Windows.Forms.Padding(1);
            this.bForEL.Name = "bForEL";
            this.bForEL.Size = new System.Drawing.Size(85, 35);
            this.bForEL.TabIndex = 6;
            this.bForEL.Text = "Журнал обмена";
            this.bForEL.UseVisualStyleBackColor = true;
            this.bForEL.Click += new System.EventHandler(this.bForEL_Click);
            // 
            // tlpForButtons
            // 
            this.tlpForButtons.ColumnCount = 3;
            this.tlpForButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33F));
            this.tlpForButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.34F));
            this.tlpForButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33F));
            this.tlpForButtons.Controls.Add(this.bStateButton, 0, 0);
            this.tlpForButtons.Controls.Add(this.bReset, 2, 1);
            this.tlpForButtons.Controls.Add(this.bExitFWS, 2, 3);
            this.tlpForButtons.Controls.Add(this.bPowerLetter, 1, 0);
            this.tlpForButtons.Controls.Add(this.bCurrent, 2, 0);
            this.tlpForButtons.Controls.Add(this.bFWS, 0, 3);
            this.tlpForButtons.Controls.Add(this.bVoltage, 0, 1);
            this.tlpForButtons.Controls.Add(this.bTemp, 1, 1);
            this.tlpForButtons.Controls.Add(this.button12, 0, 4);
            this.tlpForButtons.Controls.Add(this.button10, 1, 4);
            this.tlpForButtons.Controls.Add(this.tableLayoutPanel2, 2, 4);
            this.tlpForButtons.Controls.Add(this.bForEL, 0, 2);
            this.tlpForButtons.Controls.Add(this.bFHSS, 1, 2);
            this.tlpForButtons.Controls.Add(this.bExitFHSS, 2, 2);
            this.tlpForButtons.Controls.Add(this.tableLayoutPanel1, 1, 3);
            this.tlpForButtons.Location = new System.Drawing.Point(0, 161);
            this.tlpForButtons.Margin = new System.Windows.Forms.Padding(1);
            this.tlpForButtons.Name = "tlpForButtons";
            this.tlpForButtons.RowCount = 5;
            this.tlpForButtons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tlpForButtons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tlpForButtons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tlpForButtons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tlpForButtons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tlpForButtons.Size = new System.Drawing.Size(262, 186);
            this.tlpForButtons.TabIndex = 20;
            // 
            // cbForLetterChosing
            // 
            this.cbForLetterChosing.Dock = System.Windows.Forms.DockStyle.Top;
            this.cbForLetterChosing.FormattingEnabled = true;
            this.cbForLetterChosing.Location = new System.Drawing.Point(3, 19);
            this.cbForLetterChosing.Name = "cbForLetterChosing";
            this.cbForLetterChosing.Size = new System.Drawing.Size(76, 21);
            this.cbForLetterChosing.TabIndex = 19;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.cbForLetterChosing, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.label2, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(177, 151);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(82, 32);
            this.tableLayoutPanel2.TabIndex = 21;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Location = new System.Drawing.Point(3, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 16);
            this.label2.TabIndex = 20;
            this.label2.Text = "Литера";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tbFWSTime, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(90, 114);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(81, 31);
            this.tableLayoutPanel1.TabIndex = 22;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Время изл., мс";
            // 
            // tbFWSTime
            // 
            this.tbFWSTime.Dock = System.Windows.Forms.DockStyle.Top;
            this.tbFWSTime.Location = new System.Drawing.Point(3, 18);
            this.tbFWSTime.Name = "tbFWSTime";
            this.tbFWSTime.Size = new System.Drawing.Size(75, 20);
            this.tbFWSTime.TabIndex = 1;
            this.tbFWSTime.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbFWSTime_KeyPress);
            this.tbFWSTime.KeyUp += new System.Windows.Forms.KeyEventHandler(this.tbFWSTime_KeyUp);
            // 
            // FormForNoiseShaper
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(806, 624);
            this.Controls.Add(this.tlpForButtons);
            this.Controls.Add(this.dgvTempLetter);
            this.Controls.Add(this.dgvVoltageLetter);
            this.Controls.Add(this.dgvCurrentLetter);
            this.Controls.Add(this.dgvPowerLetter);
            this.Controls.Add(this.dataGridViewForLetter);
            this.Controls.Add(this.bConnect);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormForNoiseShaper";
            this.Text = "Параметры ФПС и УМ";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormForNoiseShaper_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewForLetter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPowerLetter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCurrentLetter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVoltageLetter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTempLetter)).EndInit();
            this.tlpForButtons.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridViewForLetter;
        private System.Windows.Forms.DataGridView dgvPowerLetter;
        private System.Windows.Forms.DataGridView dgvCurrentLetter;
        private System.Windows.Forms.DataGridView dgvVoltageLetter;
        private System.Windows.Forms.DataGridView dgvTempLetter;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button bConnect;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Button bStateButton;
        private System.Windows.Forms.Button bReset;
        private System.Windows.Forms.Button bCurrent;
        private System.Windows.Forms.Button bVoltage;
        private System.Windows.Forms.Button bTemp;
        private System.Windows.Forms.Button bPowerLetter;
        private System.Windows.Forms.Button bExitFWS;
        private System.Windows.Forms.Button bFHSS;
        private System.Windows.Forms.Button bFWS;
        private System.Windows.Forms.Button bExitFHSS;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button bForEL;
        private System.Windows.Forms.TableLayoutPanel tlpForButtons;
        private System.Windows.Forms.ComboBox cbForLetterChosing;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbFWSTime;



    }
}

