﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
namespace MyWFForNS
{
    public class GlobalVar
    {
        public Image imgRedI;
        public Image imgEmptyI;
        string sPath;
        public Icon imgRed;
        public Icon imgEmpty;
        public GlobalVar()
        {
            //imgRedI= Image.FromFile("imgRed.bmp", true);
            //imgEmptyI = Image.FromFile("imgEmpty.bmp", true);
            sPath = Directory.GetCurrentDirectory();
            imgRed = new Icon(String.Concat(sPath, "/imgRed.ico"), 10, 10);
            imgEmpty = new Icon(String.Concat(sPath, "/imgEmpty.ico"), 10, 10);
        }
    }
}
/*
 *public Icon imgRed;
        public Icon imgEmpty;
        public GlobalVar()
        {
            string path = Directory.GetCurrentDirectory();
            imgRed = new Icon(String.Concat(path,"/imgRed.ico"), 10, 10);
            imgEmpty = new Icon(String.Concat(path, "/imgEmpty.ico"), 10, 10);
        }
*/