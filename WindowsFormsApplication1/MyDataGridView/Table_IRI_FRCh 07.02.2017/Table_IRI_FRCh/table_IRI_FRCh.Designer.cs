﻿namespace Table_IRI_FRCh
{
    partial class table_IRI_FRCh
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbImp = new System.Windows.Forms.TextBox();
            this.tbVedom = new System.Windows.Forms.TextBox();
            this.tbVed = new System.Windows.Forms.TextBox();
            this.tbAll = new System.Windows.Forms.TextBox();
            this.lImp = new System.Windows.Forms.Label();
            this.lVedom = new System.Windows.Forms.Label();
            this.lVed = new System.Windows.Forms.Label();
            this.lAll = new System.Windows.Forms.Label();
            this.bIRI_RP = new System.Windows.Forms.Button();
            this.bIRI_CR = new System.Windows.Forms.Button();
            this.bDelete = new System.Windows.Forms.Button();
            this.bClear = new System.Windows.Forms.Button();
            this.pLabels = new System.Windows.Forms.Panel();
            this.bGetData = new System.Windows.Forms.Button();
            this.bConnect = new System.Windows.Forms.Button();
            this.bIRI_KRPU = new System.Windows.Forms.Button();
            this.bIRI_KvP = new System.Windows.Forms.Button();
            this.dgvIRI_FRCh = new MyDataGridView.myDataGridView();
            this.rtb = new System.Windows.Forms.RichTextBox();
            this.pLabels.SuspendLayout();
            this.SuspendLayout();
            // 
            // tbImp
            // 
            this.tbImp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.tbImp.Location = new System.Drawing.Point(522, 5);
            this.tbImp.Name = "tbImp";
            this.tbImp.ReadOnly = true;
            this.tbImp.Size = new System.Drawing.Size(50, 20);
            this.tbImp.TabIndex = 26;
            this.tbImp.Text = "0";
            // 
            // tbVedom
            // 
            this.tbVedom.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.tbVedom.Location = new System.Drawing.Point(353, 5);
            this.tbVedom.Name = "tbVedom";
            this.tbVedom.ReadOnly = true;
            this.tbVedom.Size = new System.Drawing.Size(50, 20);
            this.tbVedom.TabIndex = 25;
            this.tbVedom.Text = "0";
            // 
            // tbVed
            // 
            this.tbVed.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.tbVed.Location = new System.Drawing.Point(227, 5);
            this.tbVed.Name = "tbVed";
            this.tbVed.ReadOnly = true;
            this.tbVed.Size = new System.Drawing.Size(50, 20);
            this.tbVed.TabIndex = 24;
            this.tbVed.Text = "0";
            // 
            // tbAll
            // 
            this.tbAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.tbAll.Location = new System.Drawing.Point(56, 5);
            this.tbAll.Name = "tbAll";
            this.tbAll.ReadOnly = true;
            this.tbAll.Size = new System.Drawing.Size(50, 20);
            this.tbAll.TabIndex = 23;
            this.tbAll.Text = "0";
            // 
            // lImp
            // 
            this.lImp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lImp.AutoSize = true;
            this.lImp.ForeColor = System.Drawing.Color.Navy;
            this.lImp.Location = new System.Drawing.Point(461, 12);
            this.lImp.Name = "lImp";
            this.lImp.Size = new System.Drawing.Size(71, 13);
            this.lImp.TabIndex = 22;
            this.lImp.Text = "Важных........";
            // 
            // lVedom
            // 
            this.lVedom.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lVedom.AutoSize = true;
            this.lVedom.ForeColor = System.Drawing.Color.Navy;
            this.lVedom.Location = new System.Drawing.Point(287, 12);
            this.lVedom.Name = "lVedom";
            this.lVedom.Size = new System.Drawing.Size(76, 13);
            this.lVedom.TabIndex = 21;
            this.lVedom.Text = "Ведомая........";
            // 
            // lVed
            // 
            this.lVed.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lVed.AutoSize = true;
            this.lVed.ForeColor = System.Drawing.Color.Navy;
            this.lVed.Location = new System.Drawing.Point(162, 12);
            this.lVed.Name = "lVed";
            this.lVed.Size = new System.Drawing.Size(76, 13);
            this.lVed.TabIndex = 20;
            this.lVed.Text = "Ведущая........";
            // 
            // lAll
            // 
            this.lAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lAll.AutoSize = true;
            this.lAll.ForeColor = System.Drawing.Color.Navy;
            this.lAll.Location = new System.Drawing.Point(6, 12);
            this.lAll.Name = "lAll";
            this.lAll.Size = new System.Drawing.Size(61, 13);
            this.lAll.TabIndex = 19;
            this.lAll.Text = "Всего........";
            // 
            // bIRI_RP
            // 
            this.bIRI_RP.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bIRI_RP.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.bIRI_RP.Location = new System.Drawing.Point(314, 310);
            this.bIRI_RP.Name = "bIRI_RP";
            this.bIRI_RP.Size = new System.Drawing.Size(75, 23);
            this.bIRI_RP.TabIndex = 18;
            this.bIRI_RP.Text = "ИРИ на РП";
            this.bIRI_RP.UseVisualStyleBackColor = true;
            this.bIRI_RP.Click += new System.EventHandler(this.bIRI_RP_Click);
            // 
            // bIRI_CR
            // 
            this.bIRI_CR.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bIRI_CR.ForeColor = System.Drawing.Color.Green;
            this.bIRI_CR.Location = new System.Drawing.Point(233, 310);
            this.bIRI_CR.Name = "bIRI_CR";
            this.bIRI_CR.Size = new System.Drawing.Size(75, 23);
            this.bIRI_CR.TabIndex = 17;
            this.bIRI_CR.Text = "ИРИ на ЦР";
            this.bIRI_CR.UseVisualStyleBackColor = true;
            this.bIRI_CR.Click += new System.EventHandler(this.bIRI_CR_Click);
            // 
            // bDelete
            // 
            this.bDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bDelete.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.bDelete.Location = new System.Drawing.Point(81, 310);
            this.bDelete.Name = "bDelete";
            this.bDelete.Size = new System.Drawing.Size(75, 23);
            this.bDelete.TabIndex = 16;
            this.bDelete.Text = "Удалить";
            this.bDelete.UseVisualStyleBackColor = true;
            this.bDelete.Click += new System.EventHandler(this.bDelete_Click);
            // 
            // bClear
            // 
            this.bClear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bClear.ForeColor = System.Drawing.Color.Red;
            this.bClear.Location = new System.Drawing.Point(4, 310);
            this.bClear.Name = "bClear";
            this.bClear.Size = new System.Drawing.Size(75, 23);
            this.bClear.TabIndex = 15;
            this.bClear.Text = "Очистить";
            this.bClear.UseVisualStyleBackColor = true;
            this.bClear.Click += new System.EventHandler(this.bClear_Click);
            // 
            // pLabels
            // 
            this.pLabels.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pLabels.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pLabels.Controls.Add(this.tbVed);
            this.pLabels.Controls.Add(this.tbAll);
            this.pLabels.Controls.Add(this.tbImp);
            this.pLabels.Controls.Add(this.lAll);
            this.pLabels.Controls.Add(this.tbVedom);
            this.pLabels.Controls.Add(this.lVed);
            this.pLabels.Controls.Add(this.lVedom);
            this.pLabels.Controls.Add(this.lImp);
            this.pLabels.Location = new System.Drawing.Point(0, 274);
            this.pLabels.Name = "pLabels";
            this.pLabels.Size = new System.Drawing.Size(665, 30);
            this.pLabels.TabIndex = 27;
            // 
            // bGetData
            // 
            this.bGetData.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bGetData.ForeColor = System.Drawing.Color.Blue;
            this.bGetData.Location = new System.Drawing.Point(639, 309);
            this.bGetData.Name = "bGetData";
            this.bGetData.Size = new System.Drawing.Size(19, 23);
            this.bGetData.TabIndex = 29;
            this.bGetData.Text = "Get data";
            this.bGetData.UseVisualStyleBackColor = true;
            this.bGetData.Visible = false;
            this.bGetData.Click += new System.EventHandler(this.bGetData_Click);
            // 
            // bConnect
            // 
            this.bConnect.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bConnect.Location = new System.Drawing.Point(615, 309);
            this.bConnect.Name = "bConnect";
            this.bConnect.Size = new System.Drawing.Size(18, 23);
            this.bConnect.TabIndex = 30;
            this.bConnect.Text = "Connect";
            this.bConnect.UseVisualStyleBackColor = true;
            this.bConnect.Visible = false;
            this.bConnect.Click += new System.EventHandler(this.bConnect_Click);
            // 
            // bIRI_KRPU
            // 
            this.bIRI_KRPU.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bIRI_KRPU.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.bIRI_KRPU.Location = new System.Drawing.Point(395, 310);
            this.bIRI_KRPU.Name = "bIRI_KRPU";
            this.bIRI_KRPU.Size = new System.Drawing.Size(87, 23);
            this.bIRI_KRPU.TabIndex = 31;
            this.bIRI_KRPU.Text = "ИРИ на КРПУ";
            this.bIRI_KRPU.UseVisualStyleBackColor = true;
            this.bIRI_KRPU.Click += new System.EventHandler(this.bIRI_KRPU_Click);
            // 
            // bIRI_KvP
            // 
            this.bIRI_KvP.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bIRI_KvP.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.bIRI_KvP.Location = new System.Drawing.Point(488, 310);
            this.bIRI_KvP.Name = "bIRI_KvP";
            this.bIRI_KvP.Size = new System.Drawing.Size(87, 23);
            this.bIRI_KvP.TabIndex = 32;
            this.bIRI_KvP.Text = "ИРИ на Кв.П.";
            this.bIRI_KvP.UseVisualStyleBackColor = true;
            this.bIRI_KvP.Click += new System.EventHandler(this.bIRI_KvP_Click);
            // 
            // dgvIRI_FRCh
            // 
            this.dgvIRI_FRCh.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvIRI_FRCh.Location = new System.Drawing.Point(0, 0);
            this.dgvIRI_FRCh.Name = "dgvIRI_FRCh";
            this.dgvIRI_FRCh.Size = new System.Drawing.Size(665, 274);
            this.dgvIRI_FRCh.TabIndex = 0;
            // 
            // rtb
            // 
            this.rtb.Location = new System.Drawing.Point(446, 76);
            this.rtb.Name = "rtb";
            this.rtb.Size = new System.Drawing.Size(216, 198);
            this.rtb.TabIndex = 33;
            this.rtb.Text = "";
            this.rtb.Visible = false;
            // 
            // table_IRI_FRCh
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.rtb);
            this.Controls.Add(this.bIRI_KvP);
            this.Controls.Add(this.bIRI_KRPU);
            this.Controls.Add(this.bConnect);
            this.Controls.Add(this.bGetData);
            this.Controls.Add(this.pLabels);
            this.Controls.Add(this.bIRI_RP);
            this.Controls.Add(this.bIRI_CR);
            this.Controls.Add(this.bDelete);
            this.Controls.Add(this.bClear);
            this.Controls.Add(this.dgvIRI_FRCh);
            this.Name = "table_IRI_FRCh";
            this.Size = new System.Drawing.Size(665, 337);
            this.pLabels.ResumeLayout(false);
            this.pLabels.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lImp;
        private System.Windows.Forms.Label lVedom;
        private System.Windows.Forms.Label lVed;
        private System.Windows.Forms.Label lAll;
        public MyDataGridView.myDataGridView dgvIRI_FRCh;
        public System.Windows.Forms.TextBox tbImp;
        public System.Windows.Forms.TextBox tbVedom;
        public System.Windows.Forms.TextBox tbVed;
        public System.Windows.Forms.TextBox tbAll;
        public System.Windows.Forms.Button bIRI_RP;
        public System.Windows.Forms.Button bIRI_CR;
        public System.Windows.Forms.Button bDelete;
        public System.Windows.Forms.Button bClear;
        private System.Windows.Forms.Panel pLabels;
        private System.Windows.Forms.Button bGetData;
        private System.Windows.Forms.Button bConnect;
        private System.Windows.Forms.Button bIRI_KRPU;
        private System.Windows.Forms.Button bIRI_KvP;
        private System.Windows.Forms.RichTextBox rtb;
    }
}
