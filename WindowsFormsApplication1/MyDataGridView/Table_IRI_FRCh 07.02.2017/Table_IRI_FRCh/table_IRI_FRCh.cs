﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using MyDataGridView;
using VariableDynamic;
using USR_DLL;
using System.Globalization;
using VariableStatic;

namespace Table_IRI_FRCh
{
    public partial class table_IRI_FRCh: UserControl
    {
        FuncDGV_IRI_FRCh funcDGV_IRI_FRCh;
        FuncDB_IRI_FRCh_CR funcDB_IRI_FRCh_CR;
        FunctionsDGV functionsDGV;
        Functions functions;
        FunctionsTranslate functionsTranslate;
        DecoderParams decoderParams;
        InfoMessages infoMessage;

        ////////////////для примера (так можно задать для переменной, функции, класса) ///////////////////////////////////
        // public DecoderParams decoderParams { get; private set; } // чтение ото всюду, а изменение/задание значения только изнутри класса
        ////////////////для примера///////////////////////////////////

        Struct_IRI_FRCh[] struct_IRI_FRCh;
        List<Struct_IRI_FRCh> listFRCh;

        //TCoordsIRI tCoordsIRI;
        TCoordsIRI[] tCoordsIRI;

        const Int16 constLevel = -130;

        public IReadOnlyList<Struct_IRI_FRCh> ListFRChPublic // для чтения кому-то (без возможности изменения)
        {
            get { return listFRCh; }
        }

        public Struct_IRI_FRCh GetCurrentIRI()
        {
            var index = dgvIRI_FRCh.dgv.SelectedRows[0].Index;
            if (index < listFRCh.Count)
            {
                return listFRCh[index];
            }
            return default(Struct_IRI_FRCh);
        }

        FunctionsUser functionsUser;
        VariableWork variableWork;
        VariableCommon variableCommon;
        AWPtoBearingDSPprotocolNew dspProtocol;

        FuncDB_IRI_FRCh_RP funcDB_IRI_FRCh_RP;
        FuncVW_IRI_FRCh_CR funcVW_IRI_FRCh_CR;

        List<TDistribFWS> lDistribFWS;

        byte bLanguage = 0;

        int indTabControl = 0; // индекс TabControl в главном окне (ФРЧ)

        public int iIndSelectRowFRCh; // индекс выделенной строки

     

        public table_IRI_FRCh()
        {
            InitializeComponent();

            infoMessage = new InfoMessages();
            infoMessage.MessageErr = "";

            funcDGV_IRI_FRCh = new FuncDGV_IRI_FRCh();
            functionsDGV = new FunctionsDGV();
            functions = new Functions();
            functionsTranslate = new FunctionsTranslate();
            decoderParams = new DecoderParams();

            funcDB_IRI_FRCh_RP = new FuncDB_IRI_FRCh_RP();
            funcDB_IRI_FRCh_CR = new FuncDB_IRI_FRCh_CR();
            funcVW_IRI_FRCh_CR = new FuncVW_IRI_FRCh_CR();

            functionsUser = new FunctionsUser();
            variableWork = new VariableWork();
            variableCommon = new VariableCommon();

            tCoordsIRI = new TCoordsIRI[1];
            tCoordsIRI[0].iID = -1;
            tCoordsIRI[0].iFreq = -1;
            tCoordsIRI[0].dLat = -1;
            tCoordsIRI[0].dLon = -1;
            tCoordsIRI[0].bDelIRI = 2;
            tCoordsIRI[0].tDirections = new TDirections();
            tCoordsIRI[0].tDirections.iQ1 = -1;
            tCoordsIRI[0].tDirections.iQ2 = -1;
          

            bLanguage = variableCommon.Language;

            dspProtocol = new AWPtoBearingDSPprotocolNew();

            listFRCh = new List<Struct_IRI_FRCh>();

            lDistribFWS = new List<TDistribFWS>();

            dgvIRI_FRCh.InitTableDB(dgvIRI_FRCh.dgv, NameTable.IRI_FRCh);

            Task.Run(() => GetDataFRCh());

            dgvIRI_FRCh.dgv.SelectionChanged += dgv_SelectionChanged;

            VariableDynamic.VariableWork.aWPtoBearingDSPprotocolNew.StorageActionMessageUpdate += aWPtoBearingDSPprotocolNew_StorageActionMessageUpdate;

            VariableCommon.OnChangeCommonLanguage += new VariableCommon.ChangeCommonEventHandler(VariableCommon_OnChangeCommonLanguage);

            VariableDynamic.VariableWork.OnChangeRegime +=  new VariableWork.ChangeRegimeEventHandler(VariableWork_OnChangeRegime);

            myDataGridView.OnDGVKeyUp += myDataGridView_OnDGVKeyUp;

            VariableDynamic.VariableWork.OnChangeCheckIridiumInmarsat += new VariableWork.ChangeCheckIridiumInmarsatEventHandler(VariableWork_OnChangeCheckIridiumInmarsat);
            VariableDynamic.VariableWork.OnChangeTabControlTables += VariableWork_OnChangeTabControlTables;
        }

        void VariableWork_OnChangeTabControlTables(int index)
        {
            try
            {
                indTabControl = index;

                if (index != 0)
                {
                    tCoordsIRI[0].iID = -1;
                    tCoordsIRI[0].iFreq = -1;
                    tCoordsIRI[0].bDelIRI = 3;
                    tCoordsIRI[0].tDirections.iQ1 = -1;
                    tCoordsIRI[0].tDirections.iQ2 = -1;

                    variableWork.CoordsIRI = tCoordsIRI;
                }
            }
            catch { }
        }

        private bool bCheckIridiumInmarsat = false;
        void VariableWork_OnChangeCheckIridiumInmarsat(int iCheckIridiumInmarsat)
        {
            try
            {
                if (iCheckIridiumInmarsat > 0) bCheckIridiumInmarsat = true;
                else bCheckIridiumInmarsat = false;
            }
            catch { }
        }

        void myDataGridView_OnDGVKeyUp(KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Space)
                bIRI_KRPU_Click(this, null);
        }

        void VariableWork_OnChangeRegime()
        {
            if (variableWork.Regime == 1 || variableWork.Regime == 2)
                bClear_Click(this, null);
        }

        void aWPtoBearingDSPprotocolNew_StorageActionMessageUpdate(Protocols.StorageActionMessage answer)
        {
            if (InvokeRequired)
            {
                BeginInvoke((MethodInvoker)(()
                      => aWPtoBearingDSPprotocolNew_StorageActionMessageUpdate(answer)));
                return;
            }

            if (answer != null)
            {
                if (answer.Header.ErrorCode == 0)
                {
                    if (answer.Storage == DspDataModel.StorageType.Frs)
                    {
                        if (answer.SignalsId.Length == 0) //Полностью очищаем таблицу
                        {
                            int count = 0;

                            for (int i = 0; i < dgvIRI_FRCh.dgv.RowCount; i++)
                            {
                                if (dgvIRI_FRCh.dgv.Rows[i].Cells[0].Value == null)
                                    count++;
                            }

                            if (count == dgvIRI_FRCh.dgv.RowCount)
                                return;

                            // удалить все записи из dgv
                            functionsDGV.DeleteAllRecordsDGV(dgvIRI_FRCh.dgv);
                            // добавить пустые строки в dgv при необходимости
                            dgvIRI_FRCh.SetInitialNumberOfRows(dgvIRI_FRCh.dgv, dgvIRI_FRCh.tableIRI_FRCh, NameTable.IRI_FRCh);

                            listFRCh.Clear();

                            tbAll.Text = "";
                            tbVed.Text = "";
                            tbVedom.Text = "";
                            tbImp.Text = "";
                        }
                        else //Выборочно очищаем или восстанавливаем
                        {
                            if (answer.Action == DspDataModel.SignalAction.Hide)
                            {
                                for (int i = 0; i < answer.SignalsId.Count(); i++)
                                {
                                    // удалить запись из dgv
                                    int iID = functionsDGV.DeleteOneRecordDGV(dgvIRI_FRCh.dgv, answer.SignalsId[i]);
                                    // добавить пустые строки в dgv при необходимости
                                    dgvIRI_FRCh.SetInitialNumberOfRows(dgvIRI_FRCh.dgv, dgvIRI_FRCh.tableIRI_FRCh, NameTable.IRI_FRCh);

                                    int ind = listFRCh.FindIndex(x => x.iID == iID);
                                    if (ind >= 0 && ind < listFRCh.Count())
                                    {
                                        AddFRChToDistribFWS(ind);
                                        listFRCh.RemoveAt(ind);
                                    }
                                }
                            }
                            if (answer.Action == DspDataModel.SignalAction.Restore)
                            {
                                for (int i = 0; i < answer.SignalsId.Count(); i++)
                                {
                                    variableWork.ID_FRCh_CR = answer.SignalsId[i];
                                }
                            }
                        }
                    }
                }
            }
        }

        private void AddFRChToDistribFWS(int ind)
        {
            TDistribFWS DistribFWS = new TDistribFWS();

            DistribFWS.iID = listFRCh[ind].iID;
            DistribFWS.iFreq = listFRCh[ind].iFreq;
            DistribFWS.sBearing1 = Convert.ToInt16(listFRCh[ind].iQ1);
            DistribFWS.sBearing2 = Convert.ToInt16(listFRCh[ind].iQ2);
            DistribFWS.sLevel = Convert.ToInt16(listFRCh[ind].iLevel);
            DistribFWS.iDFreq = listFRCh[ind].iDFreq;
            DistribFWS.iCKO = listFRCh[ind].iCKO;
            DistribFWS.dLatitude = listFRCh[ind].dLatitude;
            DistribFWS.dLongitude = listFRCh[ind].dLongitude;
            DistribFWS.bView = listFRCh[ind].bView;
            DistribFWS.iSP_RR = 1;
            DistribFWS.iSP_RP = 0;

            List<TDistribFWS> lDistribFWS = new List<TDistribFWS>();
            lDistribFWS.Add(DistribFWS);
            funcVW_IRI_FRCh_CR.AddIRI_FRCh_CRToVariableWork(variableWork, lDistribFWS.ToArray());

        }


        private void VariableCommon_OnChangeCommonLanguage()
        {
            switch (variableCommon.Language)
            {
                case 0:
                    functionsTranslate.SetLanguage("rus");
                    ChangeLanguage();
                    break;

                case 1:
                    functionsTranslate.SetLanguage("eng");
                    ChangeLanguage();
                    break;

                case 2:
                    functionsTranslate.SetLanguage("az");
                    ChangeLanguage();
                    break;

                default:
                    functionsTranslate.SetLanguage("rus");
                    ChangeLanguage();
                    break;
            }
        }

        /// <summary>
        /// Для инициализации языка при запуске приложения 
        /// </summary>
        /// <param name="bLanguage"></param>
        public void ChangeControlLanguage(byte bLanguage)
        {

            switch (bLanguage)
            {
                case 0:
                    functionsTranslate.SetLanguage("rus");
                    ChangeLanguage();
                    break;

                case 1:
                    functionsTranslate.SetLanguage("eng");
                    ChangeLanguage();
                    break;

                case 2:
                    functionsTranslate.SetLanguage("az");
                    ChangeLanguage();
                    break;

                default:
                    functionsTranslate.SetLanguage("rus");
                    ChangeLanguage();
                    break;
            }
        }


        /// <summary>
        /// Изменение языка интерфейса
        /// </summary>
        private void ChangeLanguage()
        {
            functionsTranslate.RenameButtons(functionsTranslate.Dictionary, this);
            functionsTranslate.RenameLabels(functionsTranslate.Dictionary, pLabels);
            functionsTranslate.RenameFieldsTables(functionsTranslate.Dictionary, dgvIRI_FRCh.dgv);


        }

        void dgv_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                iIndSelectRowFRCh = Convert.ToInt32(dgvIRI_FRCh.dgv.Rows[dgvIRI_FRCh.dgv.SelectedRows[0].Index].Cells[0].Value);

                if (dgvIRI_FRCh.dgv.Rows[dgvIRI_FRCh.dgv.SelectedRows[0].Index].Cells[0].Value == null)
                {
                    // Отправить пеленги для отображения на карте//////////////
                    tCoordsIRI[0].iID = -1;
                    tCoordsIRI[0].iFreq = -1;
                    tCoordsIRI[0].bDelIRI = 3;
                    tCoordsIRI[0].tDirections.iQ1 = -1;
                    tCoordsIRI[0].tDirections.iQ2 = -1;

                    ///////////////////TEST
                    //if (rtb.InvokeRequired)
                    //{
                    //    Invoke((MethodInvoker)(() =>
                    //    {
                    //        rtb.AppendText(
                    //        "ID: " + tCoordsIRI[0].iID.ToString() + "\n" +
                    //        "Freq: " + tCoordsIRI[0].iFreq.ToString() + "\n" +
                    //        "Q1: " + tCoordsIRI[0].tDirections.iQ1.ToString() + "\n" +
                    //        "Q2: " + tCoordsIRI[0].tDirections.iQ2.ToString() + "\n");
                    //    }));
                    //}
                    //else
                    //{
                    //    rtb.AppendText(
                    //    "ID: " + tCoordsIRI[0].iID.ToString() + "\n" +
                    //    "Freq: " + tCoordsIRI[0].iFreq.ToString() + "\n" +
                    //    "Q1: " + tCoordsIRI[0].tDirections.iQ1.ToString() + "\n" +
                    //    "Q2: " + tCoordsIRI[0].tDirections.iQ2.ToString() + "\n");
                    //}
                    //TEST///////////////////////

                    variableWork.CoordsIRI = tCoordsIRI;
                }
                else
                {
                    // Отправить пеленги для отображения на карте//////////////
                    tCoordsIRI[0].iID = Convert.ToInt32(dgvIRI_FRCh.dgv.Rows[dgvIRI_FRCh.dgv.SelectedRows[0].Index].Cells[0].Value);
                        
                    string sFreq = functions.StrToFreq(Convert.ToString(dgvIRI_FRCh.dgv.Rows[dgvIRI_FRCh.dgv.SelectedRows[0].Index].Cells[2].Value));
                    tCoordsIRI[0].iFreq = Convert.ToInt32(Convert.ToDecimal(sFreq, Functions.format) * 10);
                      
                    string sQ1 = functions.StrToFreq(Convert.ToString(dgvIRI_FRCh.dgv.Rows[dgvIRI_FRCh.dgv.SelectedRows[0].Index].Cells[3].Value));
                    if (sQ1 == "-") { tCoordsIRI[0].tDirections.iQ1 = -1; }
                    else { tCoordsIRI[0].tDirections.iQ1 = Convert.ToInt16(Convert.ToDecimal(sQ1, Functions.format) * 10); }

                    string sQ2 = functions.StrToFreq(Convert.ToString(dgvIRI_FRCh.dgv.Rows[dgvIRI_FRCh.dgv.SelectedRows[0].Index].Cells[4].Value));
                    if (sQ2 == "-") { tCoordsIRI[0].tDirections.iQ2 = -1; }
                    else { tCoordsIRI[0].tDirections.iQ2 = Convert.ToInt16(Convert.ToDecimal(sQ2, Functions.format) * 10); }

                    tCoordsIRI[0].bDelIRI = 3;
                    ///////////////////TEST
                    //if (dgvIRI_FRCh.dgv.InvokeRequired)
                    //{
                    //    Invoke((MethodInvoker)(() =>
                    //    {
                    //        rtb.AppendText(
                    //        "ID: " + tCoordsIRI[0].iID.ToString() + "\n" +
                    //        "Freq: " + tCoordsIRI[0].iFreq.ToString() + "\n" +
                    //        "Q1: " + tCoordsIRI[0].tDirections.iQ1.ToString() + "\n" +
                    //        "Q2: " + tCoordsIRI[0].tDirections.iQ2.ToString() + "\n");
                    //    }));
                    //}
                    //else
                    //{
                    //    rtb.AppendText(
                    //    "ID: " + tCoordsIRI[0].iID.ToString() + "\n" +
                    //    "Freq: " + tCoordsIRI[0].iFreq.ToString() + "\n" +
                    //    "Q1: " + tCoordsIRI[0].tDirections.iQ1.ToString() + "\n" +
                    //    "Q2: " + tCoordsIRI[0].tDirections.iQ2.ToString() + "\n");
                    //}
                    //TEST///////////////////////
                    variableWork.CoordsIRI = tCoordsIRI;
                }
            }
            catch { }
        }

        private async void bClear_Click(object sender, EventArgs e)
        {
            int count = 0;

            for (int i = 0; i < dgvIRI_FRCh.dgv.RowCount; i++)
            {
                if (dgvIRI_FRCh.dgv.Rows[i].Cells[0].Value == null)
                    count++;
            }

            if (count == dgvIRI_FRCh.dgv.RowCount)
                return;

            int[] SignalsId = new int[0];
            //await VariableWork.aWPtoBearingDSPprotocolNew.StorageAction(0 - ФРЧ, 1 - ППРЧ; 0 - удалить, 1 - восстановить; массив id);
            await VariableWork.aWPtoBearingDSPprotocolNew.StorageAction(0, 0, SignalsId); // отправить запрос на очистку ИРИ ФРЧ на сервер

            tCoordsIRI[0].iID = -1;
            tCoordsIRI[0].iFreq = -1;
            tCoordsIRI[0].dLat = -1;
            tCoordsIRI[0].dLon = -1;
            tCoordsIRI[0].tDirections.iQ1 = -1;
            tCoordsIRI[0].tDirections.iQ2 = -1;
            tCoordsIRI[0].bDelIRI = 2;


            ///////////////////TEST
            //if (rtb.InvokeRequired)
            //{
            //    Invoke((MethodInvoker)(() =>
            //    {
            //        rtb.AppendText(
            //        "ID: " + tCoordsIRI[0].iID.ToString() + "\n" +
            //        "Freq: " + tCoordsIRI[0].iFreq.ToString() + "\n" +
            //        "Q1: " + tCoordsIRI[0].tDirections.iQ1.ToString() + "\n" +
            //        "Q2: " + tCoordsIRI[0].tDirections.iQ2.ToString() + "\n");
            //    }));
            //}
            //TEST///////////////////////

            variableWork.CoordsIRI = tCoordsIRI;

            // удалить все записи из dgv
            functionsDGV.DeleteAllRecordsDGV(dgvIRI_FRCh.dgv);
            // добавить пустые строки в dgv при необходимости
            dgvIRI_FRCh.SetInitialNumberOfRows(dgvIRI_FRCh.dgv, dgvIRI_FRCh.tableIRI_FRCh, NameTable.IRI_FRCh);

            listFRCh.Clear();

            tbAll.Text = "";
            tbVed.Text = "";
            tbVedom.Text = "";
            tbImp.Text = "";
          
        }

        private async void bDelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvIRI_FRCh.dgv.Rows[dgvIRI_FRCh.dgv.SelectedRows[0].Index].Cells[0].Value == null)
                    return;

                int[] SignalId = new int[1];
                SignalId[0] = Convert.ToInt32(dgvIRI_FRCh.dgv.Rows[dgvIRI_FRCh.dgv.SelectedRows[0].Index].Cells[0].Value);
                await VariableWork.aWPtoBearingDSPprotocolNew.StorageAction(0, 0, SignalId); // отправить запрос на очистку ИРИ ФРЧ на сервер

                tCoordsIRI[0].iID = Convert.ToInt32(dgvIRI_FRCh.dgv.Rows[dgvIRI_FRCh.dgv.SelectedRows[0].Index].Cells[0].Value);
                string sFreq = functions.StrToFreq(Convert.ToString(dgvIRI_FRCh.dgv.Rows[dgvIRI_FRCh.dgv.SelectedRows[0].Index].Cells[2].Value));
                tCoordsIRI[0].iFreq = Convert.ToInt32(Convert.ToDecimal(sFreq, Functions.format) * 10);
                if (dgvIRI_FRCh.dgv.Rows[dgvIRI_FRCh.dgv.SelectedRows[0].Index].Cells[8].Value.ToString() == "-") { tCoordsIRI[0].dLat = -1; }
                else { tCoordsIRI[0].dLat = Convert.ToDouble(dgvIRI_FRCh.dgv.Rows[dgvIRI_FRCh.dgv.SelectedRows[0].Index].Cells[8].Value); }
                if (dgvIRI_FRCh.dgv.Rows[dgvIRI_FRCh.dgv.SelectedRows[0].Index].Cells[9].Value.ToString() == "-") { tCoordsIRI[0].dLon = -1; }
                else { tCoordsIRI[0].dLon = Convert.ToDouble(dgvIRI_FRCh.dgv.Rows[dgvIRI_FRCh.dgv.SelectedRows[0].Index].Cells[9].Value); }
                tCoordsIRI[0].bDelIRI = 1;

                ///////////////////TEST
                //if (rtb.InvokeRequired)
                //{
                //    Invoke((MethodInvoker)(() =>
                //    {
                //        rtb.AppendText(
                //        "ID: " + tCoordsIRI[0].iID.ToString() + "\n" +
                //        "Freq: " + tCoordsIRI[0].iFreq.ToString() + "\n" +
                //        "Lat: " + tCoordsIRI[0].dLat.ToString() + "\n" +
                //        "Lon: " + tCoordsIRI[0].dLon.ToString() + "\n");
                //    }));
                //}
                //TEST///////////////////////
                variableWork.CoordsIRI = tCoordsIRI;

                // удалить запись из dgv
                int iID = functionsDGV.DeleteOneRecordDGV(dgvIRI_FRCh.dgv);
                // добавить пустые строки в dgv при необходимости
                dgvIRI_FRCh.SetInitialNumberOfRows(dgvIRI_FRCh.dgv, dgvIRI_FRCh.tableIRI_FRCh, NameTable.IRI_FRCh);

                int ind = listFRCh.FindIndex(x => x.iID == iID);
                listFRCh.RemoveAt(ind);
            }
            catch { }
           
        }

        #region button Connect
        private async void bConnect_Click(object sender, EventArgs e)
        {
            //**************************************OTL********************************************************
            //await dspProtocol.ConnectToBearingDSP("127.0.0.1", 10001); // button beginconnect
            //var answer2 = await dspProtocol.SetSectorsAndRanges("25", "100", "0", "360");     // button3
            //var answer3 = await dspProtocol.SetFilters(80, 0, 0, 0);                          // button18
            //var answer4 = await dspProtocol.SetMode(1);                                       // button2
            //**************************************OTL********************************************************
        }
        #endregion

        #region button GetData
        private async void bGetData_Click(object sender, EventArgs e)
        {
            //var answer = await VariableWork.aWPtoBearingDSPprotocolNew.GetRadioSources();   // button11
            
            //**************************************OTL********************************************************
            //var answer = await dspProtocol.GetRadioSources(); 
            //*************************************************************************************************
           
            //if (answer != null)
            //{
            //    struct_IRI_FRCh = new Struct_IRI_FRCh[answer.RadioSources.Count()];

            //    for (int i = 0; i < answer.RadioSources.Count(); i++)
            //    {
            //        struct_IRI_FRCh[i].iID = answer.RadioSources[i].Id;
            //        struct_IRI_FRCh[i].iFreq = answer.RadioSources[i].Frequency;
            //        struct_IRI_FRCh[i].iQ1 = answer.RadioSources[i].Direction;
            //        struct_IRI_FRCh[i].iQ2 = answer.RadioSources[i].Direction2;
            //        struct_IRI_FRCh[i].iLevel = answer.RadioSources[i].Amplitude;
            //        struct_IRI_FRCh[i].iDFreq = answer.RadioSources[i].Bandwidth;
            //        struct_IRI_FRCh[i].iCKO = answer.RadioSources[i].StandardDeviation;
            //        struct_IRI_FRCh[i].bView = answer.RadioSources[i].Modulation;
            //        struct_IRI_FRCh[i].sTime = string.Format("{0}:{1}:{2}", answer.RadioSources[i].Time.Hour, answer.RadioSources[i].Time.Minute, answer.RadioSources[i].Time.Second);
            //        struct_IRI_FRCh[i].iDurationSignal = answer.RadioSources[i].Duration;
            //        struct_IRI_FRCh[i].iCountExits = answer.RadioSources[i].BroadcastCount;
            //        struct_IRI_FRCh[i].bKR = Convert.ToByte(answer.RadioSources[i].IsActive);

            //        if (listFRCh.Count == 0)
            //            listFRCh.Add(struct_IRI_FRCh[i]);
            //        else
            //        {
            //            if (listFRCh.Exists(x => x.iID == struct_IRI_FRCh[i].iID))
            //            {
            //                int ind = listFRCh.FindIndex(x => x.iID == struct_IRI_FRCh[i].iID);
            //                listFRCh[ind] = struct_IRI_FRCh[i]; // заменить
            //            }
            //            else listFRCh.Add(struct_IRI_FRCh[i]); // добавить в конец листа 
            //        }
            //    }

            //    var sortedFreq = listFRCh.OrderBy(sF => sF.iFreq); // сортировка по частоте по возрастанию
            //    struct_IRI_FRCh = sortedFreq.ToArray();

            //    functionsDGV.DeleteAllRecordsDGV(dgvIRI_FRCh.dgv);
            //    dgvIRI_FRCh.SetInitialNumberOfRows(dgvIRI_FRCh.dgv, dgvIRI_FRCh.tableIRI_FRCh, NameTable.IRI_FRCh); // добавить пустые строки при необходимости
            //    functionsDGV.AddRecordToDGV(dgvIRI_FRCh.dgv, NameTable.IRI_FRCh, struct_IRI_FRCh);


                //********************************Example*******************************************************
                
                // Сортировка по частоте по возрастанию

                //IEnumerable<Struct_IRI_FRCh> query = struct_IRI_FRCh.OrderBy(s => s.iFreq);
                //struct_IRI_FRCh = new Struct_IRI_FRCh[query.Count()];
                //int j = 0;

                //foreach (Struct_IRI_FRCh s in query)
                //{
                //    struct_IRI_FRCh[j].iID = s.iID;
                //    struct_IRI_FRCh[j].iFreq = s.iFreq;
                //    struct_IRI_FRCh[j].iQ1 = s.iQ1;
                //    struct_IRI_FRCh[j].iQ2 = s.iQ2;
                //    struct_IRI_FRCh[j].iLevel = s.iLevel;
                //    struct_IRI_FRCh[j].iDFreq = s.iDFreq;
                //    struct_IRI_FRCh[j].iCKO = s.iCKO;
                //    struct_IRI_FRCh[j].bView = s.bView;
                //    struct_IRI_FRCh[j].sTime = s.sTime;
                //    struct_IRI_FRCh[j].iDurationSignal = s.iDurationSignal;
                //    struct_IRI_FRCh[j].iCountExits = s.iCountExits;
                //    struct_IRI_FRCh[j].bKR = s.bKR;

                //    j++;
                //}
                //*************************************************************************************************
            //}
        }
        #endregion

        private void DataToTextBoxes(int countVed, int countVedom, int countImport)
        {
            if (InvokeRequired)
            {
                Invoke((MethodInvoker)(() => DataToTextBoxes(countVed, countVedom, countImport)));
                return;
            }

            tbAll.Text = listFRCh.Count.ToString();
            tbVed.Text = countVed.ToString();
            tbVedom.Text = countVedom.ToString();
            tbImp.Text = countImport.ToString(); 
        }

        Random rand = new Random();
        int countIRI = 0;
        /// <summary>
        /// Получить данные ИРИ ФРЧ
        /// </summary>
        private async void GetDataFRCh()
        {
            while (true)
            {
                if (variableWork.Regime == 1 || variableWork.Regime == 2)
                {
                    int countVed = 0, countVedom = 0, countImport = 0;

                    var answer = await VariableWork.aWPtoBearingDSPprotocolNew.GetRadioSources();   // button11

                    if (answer != null)
                    {
                        struct_IRI_FRCh = new Struct_IRI_FRCh[answer.RadioSources.Count()];
                        List<TCoordsIRI> listCoordsIRI = new List<TCoordsIRI>();
                        for (int i = 0; i < answer.RadioSources.Count(); i++)
                        {
                            struct_IRI_FRCh[i].iID = answer.RadioSources[i].Id;
                            struct_IRI_FRCh[i].iFreq = answer.RadioSources[i].Frequency;
                            struct_IRI_FRCh[i].iQ1 = answer.RadioSources[i].Direction;
                            struct_IRI_FRCh[i].iQ2 = answer.RadioSources[i].Direction2;
                            struct_IRI_FRCh[i].iLevel = answer.RadioSources[i].Amplitude;
                            struct_IRI_FRCh[i].iDFreq = answer.RadioSources[i].Bandwidth;
                            struct_IRI_FRCh[i].iCKO = answer.RadioSources[i].StandardDeviation;
                            struct_IRI_FRCh[i].dLatitude = answer.RadioSources[i].Latitude;
                            struct_IRI_FRCh[i].dLongitude = answer.RadioSources[i].Longitude;
                            struct_IRI_FRCh[i].bView = answer.RadioSources[i].Modulation;
                            struct_IRI_FRCh[i].sTime = string.Format("{0}:{1}:{2}", answer.RadioSources[i].Time.Hour.ToString("00"), answer.RadioSources[i].Time.Minute.ToString("00"), answer.RadioSources[i].Time.Second.ToString("00"));
                            struct_IRI_FRCh[i].iDurationSignal = answer.RadioSources[i].Duration;
                            struct_IRI_FRCh[i].iCountExits = answer.RadioSources[i].BroadcastCount;
                            struct_IRI_FRCh[i].bKR = Convert.ToByte(answer.RadioSources[i].IsActive);
                            struct_IRI_FRCh[i].bView = 1;

                            //TEST////////////////////////////////////////////////////
                            //Random rand = new Random();
                            //struct_IRI_FRCh[i].dLatitude =  53.0 + rand.NextDouble();
                            //struct_IRI_FRCh[i].dLongitude = 27.0 + rand.NextDouble();
                            //TEST////////////////////////////////////////////////////

                            // Отправить ИРИ для отображения на карте//////////////
                            if (struct_IRI_FRCh[i].dLatitude != -1 && struct_IRI_FRCh[i].dLongitude != -1)
                            {
                                TCoordsIRI tempCoordsIRI = new TCoordsIRI();
                                tempCoordsIRI.iID = struct_IRI_FRCh[i].iID;
                                tempCoordsIRI.iFreq = struct_IRI_FRCh[i].iFreq;
                                tempCoordsIRI.dLat = struct_IRI_FRCh[i].dLatitude;
                                tempCoordsIRI.dLon = struct_IRI_FRCh[i].dLongitude;
                                tempCoordsIRI.bDelIRI = 0;
                                tempCoordsIRI.sColorIRI = functions.ColorFromFreqRange(tempCoordsIRI.iFreq);
                                listCoordsIRI.Add(tempCoordsIRI);
                            }

                            ///////////////////////////////////////////////////////////////

                            int lenImportant = variableWork.FrequencyRangeImportantOwn.Length;
                            for (int j = 0; j < lenImportant; j++)
                            {
                                if (struct_IRI_FRCh[i].iFreq >= variableWork.FrequencyRangeImportantOwn[j].StartFrequency && struct_IRI_FRCh[i].iFreq <= variableWork.FrequencyRangeImportantOwn[j].EndFrequency) { countImport++; }
                            }

                            if (struct_IRI_FRCh[i].bView == 1) { countVed++; }
                            if (struct_IRI_FRCh[i].bView == 2) { countVedom++; }

                            if (listFRCh.Count == 0)
                                listFRCh.Add(struct_IRI_FRCh[i]);
                            else
                            {
                                if (listFRCh.Exists(x => x.iID == struct_IRI_FRCh[i].iID))
                                {
                                    int ind = listFRCh.FindIndex(x => x.iID == struct_IRI_FRCh[i].iID);
                                    listFRCh[ind] = struct_IRI_FRCh[i]; // заменить
                                }
                                else listFRCh.Add(struct_IRI_FRCh[i]); // добавить в конец листа 
                            }
                        }

                        // источники на карту
                        countIRI++;
                        if (countIRI == 10)
                        {
                            countIRI = 0;
                            if (listCoordsIRI != null && listCoordsIRI.Count != 0)
                            {
                                variableWork.CoordsIRI = listCoordsIRI.ToArray();
                            }
                        }

                        var sortedFreq = listFRCh.OrderBy(sF => sF.iFreq); // сортировка по частоте по возрастанию

                        DataToTextBoxes(countVed, countVedom, countImport); // добавить значения в textBox

                        struct_IRI_FRCh = sortedFreq.ToArray();

                        var nullRow = funcDGV_IRI_FRCh.NullRow(dgvIRI_FRCh.dgv, NameTable.IRI_FRCh, struct_IRI_FRCh);

                        var lrs = funcDGV_IRI_FRCh.AddRecord_IRI_FRChToDGV2(dgvIRI_FRCh.dgv, NameTable.IRI_FRCh, struct_IRI_FRCh);

                        int countRows = dgvIRI_FRCh.CountOfBeautifulRows(dgvIRI_FRCh.dgv, dgvIRI_FRCh.tableIRI_FRCh, NameTable.IRI_FRCh); // добавить пустые строки при необходимости

                        Invoke((MethodInvoker)(() =>
                        {
                            int totalcount = dgvIRI_FRCh.dgv.Rows.Count;

                            for (int i = 0; i < lrs.Count; i++)
                            {
                                if (i < totalcount)
                                {
                                    dgvIRI_FRCh.dgv.Rows[i].SetValues(
                                        lrs[i].Cells[0].Value,
                                        lrs[i].Cells[1].Value,
                                        lrs[i].Cells[2].Value,
                                        lrs[i].Cells[3].Value,
                                        lrs[i].Cells[4].Value,
                                        lrs[i].Cells[5].Value,
                                        lrs[i].Cells[6].Value,
                                        lrs[i].Cells[7].Value,
                                        lrs[i].Cells[8].Value,
                                        lrs[i].Cells[9].Value,
                                        lrs[i].Cells[10].Value,
                                        lrs[i].Cells[11].Value,
                                        lrs[i].Cells[12].Value,
                                        lrs[i].Cells[13].Value,
                                        lrs[i].Cells[14].Value);
                                }
                                else
                                {
                                    dgvIRI_FRCh.dgv.Rows.Add(lrs[i]);
                                }
                            }

                            if (lrs.Count <= countRows)
                            {
                                for (int i = lrs.Count; i < countRows; i++)
                                    dgvIRI_FRCh.dgv.Rows[i].SetValues(
                                        nullRow.Cells[0].Value,
                                        nullRow.Cells[1].Value,
                                        nullRow.Cells[2].Value,
                                        nullRow.Cells[3].Value,
                                        nullRow.Cells[4].Value,
                                        nullRow.Cells[5].Value,
                                        nullRow.Cells[6].Value,
                                        nullRow.Cells[7].Value,
                                        nullRow.Cells[8].Value,
                                        nullRow.Cells[9].Value,
                                        nullRow.Cells[10].Value,
                                        nullRow.Cells[11].Value,
                                        nullRow.Cells[12].Value,
                                        nullRow.Cells[13].Value,
                                        nullRow.Cells[14].Value);
                            }

                            totalcount = dgvIRI_FRCh.dgv.Rows.Count;
                            if (lrs.Count < countRows && countRows < totalcount)
                            {
                                for (int i = totalcount - 1; i >= countRows; i--)
                                    dgvIRI_FRCh.dgv.Rows.RemoveAt(i);
                            }
                        }));


                        ///////////////////////////////////////////////////////
                        var index = sortedFreq.ToList().FindIndex(item => item.iID == iIndSelectRowFRCh); // сохранение текущего выделения строки в таблице
                        if (index != -1)
                        {
                            dgvIRI_FRCh.dgv.Rows[index].Selected = true;

                            // Отправить пеленг для отображения на карте//////////////
                            if (dgvIRI_FRCh.dgv.InvokeRequired)
                            {
                                Invoke((MethodInvoker)(() =>
                                {
                                    if (indTabControl == 0)
                                    {
                                        TCoordsIRI[] tCoordsIRI = new TCoordsIRI[1];
                                        tCoordsIRI[0].iID = sortedFreq.ToList()[index].iID;
                                        tCoordsIRI[0].iFreq = sortedFreq.ToList()[index].iFreq;
                                        tCoordsIRI[0].dLat = sortedFreq.ToList()[index].dLatitude;
                                        tCoordsIRI[0].dLon = sortedFreq.ToList()[index].dLongitude;
                                        tCoordsIRI[0].bDelIRI = 3;
                                        tCoordsIRI[0].tDirections.iQ1 = sortedFreq.ToList()[index].iQ1;
                                        tCoordsIRI[0].tDirections.iQ2 = sortedFreq.ToList()[index].iQ2;
                                        tCoordsIRI[0].sColorIRI = functions.ColorFromFreqRange(tCoordsIRI[0].iFreq);

                                        variableWork.CoordsIRI = tCoordsIRI;
                                    }
                                }));
                            }
                            else
                            {
                                if (indTabControl == 0)
                                {
                                    TCoordsIRI[] tCoordsIRI = new TCoordsIRI[1];
                                    tCoordsIRI[0].iID = sortedFreq.ToList()[index].iID;
                                    tCoordsIRI[0].iFreq = sortedFreq.ToList()[index].iFreq;
                                    tCoordsIRI[0].dLat = sortedFreq.ToList()[index].dLatitude;
                                    tCoordsIRI[0].dLon = sortedFreq.ToList()[index].dLongitude;
                                    tCoordsIRI[0].bDelIRI = 3;
                                    tCoordsIRI[0].tDirections.iQ1 = sortedFreq.ToList()[index].iQ1;
                                    tCoordsIRI[0].tDirections.iQ2 = sortedFreq.ToList()[index].iQ2;
                                    tCoordsIRI[0].sColorIRI = functions.ColorFromFreqRange(tCoordsIRI[0].iFreq);

                                    variableWork.CoordsIRI = tCoordsIRI;
                                }
                            }
                            ///////////////////////////////////////////////////////
                        }

                    }
                }

                await Task.Delay(1000);
            }
        }

        private void bIRI_RP_Click(object sender, EventArgs e)
        {
            if (dgvIRI_FRCh.dgv.Rows[dgvIRI_FRCh.dgv.SelectedRows[0].Index].Cells[0].Value == null) { return; }
            else
            {
                if (bCheckIridiumInmarsat)
                {
                    MessageBox.Show("В диапазоне литеры 8 осуществляется подавление IRIDIUM (INMARSAT)!", SMessages.mesMessage, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                TSupprFWS SupprFWS = AddIRIToRP(dgvIRI_FRCh.dgv);

                if (dgvIRI_FRCh.dgv.Rows[dgvIRI_FRCh.dgv.SelectedRows[0].Index].Cells[11].Value.ToString() == SMeaning.meaningOwn) // ведущая СП
                {
                    infoMessage.MessageErr = functionsUser.ChangeIRI_FRCh_RPToVariableWork(SupprFWS, variableWork.SupprFWS_Own.ToList(), 1, -1);

                    if (infoMessage.MessageErr != "")
                    {
                        MessageBox.Show(infoMessage.MessageErr, SMessages.mesMessage, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        infoMessage.MessageErr = "";
                        return;
                    }
                }
                else // ведомая СП
                {
                    infoMessage.MessageErr = functionsUser.ChangeIRI_FRCh_RPToVariableWork(AddIRIToRP(dgvIRI_FRCh.dgv), variableWork.SupprFWS_Linked.ToList(), 2, -1);

                    if (infoMessage.MessageErr != "")
                    {
                        MessageBox.Show(infoMessage.MessageErr, SMessages.mesMessage, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        infoMessage.MessageErr = "";
                        return;
                    }
                }
            }
        }

        private TSupprFWS AddIRIToRP(DataGridView dgv)
        {
            TSupprFWS SupprFWS = new TSupprFWS();

            SupprFWS.iID = Convert.ToInt32(dgv.Rows[dgv.SelectedRows[0].Index].Cells[0].Value);

            string sFreq = functions.StrToFreq(Convert.ToString(dgv.Rows[dgv.SelectedRows[0].Index].Cells[2].Value));
            SupprFWS.iFreq = Convert.ToInt32(Convert.ToDecimal(sFreq, Functions.format) * 10);

            string sQ1 = functions.StrToFreq(Convert.ToString(dgv.Rows[dgv.SelectedRows[0].Index].Cells[3].Value));
            if (sQ1 == "-") { SupprFWS.sBearing = -1; }
            else { SupprFWS.sBearing = Convert.ToInt16(Convert.ToDecimal(sQ1, Functions.format) * 10); }

            // определить литеру
            if (SupprFWS.iFreq >= RangesLetters.FREQ_START_LETTER_1 & SupprFWS.iFreq < RangesLetters.FREQ_START_LETTER_2)
                SupprFWS.bLetter = 1;
            if (SupprFWS.iFreq >= RangesLetters.FREQ_START_LETTER_2 & SupprFWS.iFreq < RangesLetters.FREQ_START_LETTER_3)
                SupprFWS.bLetter = 2;
            if (SupprFWS.iFreq >= RangesLetters.FREQ_START_LETTER_3 & SupprFWS.iFreq < RangesLetters.FREQ_START_LETTER_4)
                SupprFWS.bLetter = 3;
            if (SupprFWS.iFreq >= RangesLetters.FREQ_START_LETTER_4 & SupprFWS.iFreq < RangesLetters.FREQ_START_LETTER_5)
                SupprFWS.bLetter = 4;
            if (SupprFWS.iFreq >= RangesLetters.FREQ_START_LETTER_5 & SupprFWS.iFreq < RangesLetters.FREQ_START_LETTER_6)
                SupprFWS.bLetter = 5;
            if (SupprFWS.iFreq >= RangesLetters.FREQ_START_LETTER_6 & SupprFWS.iFreq < RangesLetters.FREQ_START_LETTER_7)
                SupprFWS.bLetter = 6;
            if (variableCommon.TypeStation == 0)
            {
                if (SupprFWS.iFreq >= RangesLetters.FREQ_START_LETTER_7 & SupprFWS.iFreq < RangesLetters.FREQ_STOP_LETTER_7)
                    SupprFWS.bLetter = 7;
            }
            else if (variableCommon.TypeStation == 1)
            {
                if (SupprFWS.iFreq >= RangesLetters.FREQ_START_LETTER_7 & SupprFWS.iFreq < RangesLetters.FREQ_START_LETTER_8)
                    SupprFWS.bLetter = 7;
                if (SupprFWS.iFreq >= RangesLetters.FREQ_START_LETTER_8 & SupprFWS.iFreq < RangesLetters.FREQ_START_LETTER_9)
                    SupprFWS.bLetter = 8;
                if (SupprFWS.iFreq >= RangesLetters.FREQ_START_LETTER_9 & SupprFWS.iFreq <= RangesLetters.FREQ_STOP_LETTER_9)
                    SupprFWS.bLetter = 9;
            }


            int iLevel = (int)dgv.Rows[dgv.SelectedRows[0].Index].Cells[5].Value;
            iLevel = iLevel - 10;

            if (iLevel < constLevel) { SupprFWS.sLevel = constLevel; }
            else SupprFWS.sLevel = (short)iLevel;
            //else
            //{
            //    iLevel = iLevel - 10;
            //    SupprFWS.sLevel = Convert.ToInt16(iLevel);
            //}

            SupprFWS.bPrior = 2;
            SupprFWS.bManipulation = 0;
            SupprFWS.bModulation = 1;
            SupprFWS.bDeviation = 1;
            SupprFWS.bDuration = 1;

            return SupprFWS;
        }

        private void bIRI_CR_Click(object sender, EventArgs e)
        {
            //lDistribFWS = funcDB_IRI_FRCh_CR.LoadTableIRI_FRCh_CRFromDB(NameTable.IRI_FRCh_CR);

            if (dgvIRI_FRCh.dgv.Rows[dgvIRI_FRCh.dgv.SelectedRows[0].Index].Cells[0].Value == null)
                return;
            else
            {
                TDistribFWS DistribFWS = new TDistribFWS();

                DistribFWS.iID = Convert.ToInt32(dgvIRI_FRCh.dgv.Rows[dgvIRI_FRCh.dgv.SelectedRows[0].Index].Cells[0].Value);

                string sFreq = functions.StrToFreq(Convert.ToString(dgvIRI_FRCh.dgv.Rows[dgvIRI_FRCh.dgv.SelectedRows[0].Index].Cells[2].Value));
                DistribFWS.iFreq = Convert.ToInt32(Convert.ToDecimal(sFreq, Functions.format) * 10);

                string sQ1 = functions.StrToFreq(Convert.ToString(dgvIRI_FRCh.dgv.Rows[dgvIRI_FRCh.dgv.SelectedRows[0].Index].Cells[3].Value));
                if (sQ1 == "-") { DistribFWS.sBearing1 = -1; }
                else { DistribFWS.sBearing1 = Convert.ToInt16(Convert.ToDecimal(sQ1, Functions.format) * 10); }

                string sQ2 = functions.StrToFreq(Convert.ToString(dgvIRI_FRCh.dgv.Rows[dgvIRI_FRCh.dgv.SelectedRows[0].Index].Cells[4].Value));
                if (sQ2 == "-") { DistribFWS.sBearing2 = -1; }
                else { DistribFWS.sBearing2 = Convert.ToInt16(Convert.ToDecimal(sQ2, Functions.format) * 10); }

                DistribFWS.sLevel = Convert.ToInt16(dgvIRI_FRCh.dgv.Rows[dgvIRI_FRCh.dgv.SelectedRows[0].Index].Cells[5].Value);

                string sDFreq = functions.StrToFreq(Convert.ToString(dgvIRI_FRCh.dgv.Rows[dgvIRI_FRCh.dgv.SelectedRows[0].Index].Cells[6].Value));
                DistribFWS.iDFreq = Convert.ToInt32(Convert.ToDecimal(sDFreq, Functions.format) * 10);

                string sCKO = functions.StrToFreq(Convert.ToString(dgvIRI_FRCh.dgv.Rows[dgvIRI_FRCh.dgv.SelectedRows[0].Index].Cells[7].Value));
                DistribFWS.iCKO = Convert.ToInt32(Convert.ToDecimal(sCKO, Functions.format) * 10);

                string sLat = Convert.ToString(dgvIRI_FRCh.dgv.Rows[dgvIRI_FRCh.dgv.SelectedRows[0].Index].Cells[8].Value);
                string sLon = Convert.ToString(dgvIRI_FRCh.dgv.Rows[dgvIRI_FRCh.dgv.SelectedRows[0].Index].Cells[9].Value);
                if (sLat == "-" && sLon == "-")
                {
                     DistribFWS.dLatitude = -1;
                     DistribFWS.dLongitude = -1; 
                }
                else
                {
                    DistribFWS.dLatitude = Convert.ToDouble(dgvIRI_FRCh.dgv.Rows[dgvIRI_FRCh.dgv.SelectedRows[0].Index].Cells[8].Value, Functions.format);
                    DistribFWS.dLongitude = Convert.ToDouble(dgvIRI_FRCh.dgv.Rows[dgvIRI_FRCh.dgv.SelectedRows[0].Index].Cells[9].Value, Functions.format);
                }

                DistribFWS.bView = decoderParams.ViewModulation(Convert.ToString(dgvIRI_FRCh.dgv.Rows[dgvIRI_FRCh.dgv.SelectedRows[0].Index].Cells[10].Value)); 
                DistribFWS.iSP_RR = 1; 
                DistribFWS.iSP_RP = 0;

                List<TDistribFWS> lDistribFWS = new List<TDistribFWS>();
                lDistribFWS.Add(DistribFWS);
                funcVW_IRI_FRCh_CR.AddIRI_FRCh_CRToVariableWork(variableWork, lDistribFWS.ToArray());


                //if (funcDB_IRI_FRCh_CR.AddRecord_IRI_FRCh_CRToDB(NameTable.IRI_FRCh_CR, DistribFWS))
                //{
                //    funcDB_IRI_FRCh_CR.AddIRI_FRCh_CRToVariableWork(lDistribFWS);
                //}

                bDelete_Click(this, null);
            }
        }

        private void bIRI_KRPU_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvIRI_FRCh.dgv.Rows[dgvIRI_FRCh.dgv.SelectedRows[0].Index].Cells[0].Value == null) { return; }
                else
                {
                    string sFreq = functions.StrToFreq(Convert.ToString(dgvIRI_FRCh.dgv.Rows[dgvIRI_FRCh.dgv.SelectedRows[0].Index].Cells[2].Value));
                    variableWork.Frequency = Convert.ToInt64(Convert.ToDecimal(sFreq, Functions.format) * 1000);
                }
            }
            catch { }
        }

        private async void bIRI_KvP_Click(object sender, EventArgs e)
        {
            try
            {
                int iFmin = 0;
                int iFmax = 0;

                if (dgvIRI_FRCh.dgv.Rows[dgvIRI_FRCh.dgv.SelectedRows[0].Index].Cells[0].Value == null) { return; }
                else
                {
                    string sDFreq = functions.StrToFreq(Convert.ToString(dgvIRI_FRCh.dgv.Rows[dgvIRI_FRCh.dgv.SelectedRows[0].Index].Cells[6].Value));
                    int iDFreq = Convert.ToInt32(Convert.ToDecimal(sDFreq, Functions.format) * 10);

                    string sFreq = functions.StrToFreq(Convert.ToString(dgvIRI_FRCh.dgv.Rows[dgvIRI_FRCh.dgv.SelectedRows[0].Index].Cells[2].Value));
                    int iFreq = Convert.ToInt32(Convert.ToDecimal(sFreq, Functions.format) * 10);

                    if (iDFreq < 500)
                    {
                        iFmin = iFreq - 250;
                        iFmax = iFreq + 250;
                    }
                    else
                    {
                        iFmin = iFreq - iDFreq;
                        iFmax = iFreq + iDFreq;
                    }
                }

                var answer = await VariableWork.aWPtoBearingDSPprotocolNew.QuasiSimultaneouslyDFX10(iFmin, iFmax, 3, 3);

                if (answer != null)
                {
                    Struct_IRI_FRCh changeRec = new Struct_IRI_FRCh();
                    changeRec.iID = answer.Source.Id;
                    changeRec.iFreq = answer.Source.Frequency;
                    changeRec.iQ1 = answer.Source.Direction;
                    changeRec.iQ2 = answer.Source.Direction2;
                    changeRec.iLevel = answer.Source.Amplitude;
                    changeRec.iDFreq = answer.Source.Bandwidth;
                    changeRec.iCKO = answer.Source.StandardDeviation;
                    changeRec.dLatitude = answer.Source.Latitude;   // 53.1234; 
                    changeRec.dLongitude = answer.Source.Longitude; // 27.34344;
                    changeRec.bView = answer.Source.Modulation;
                    changeRec.sTime = string.Format("{0}:{1}:{2}", answer.Source.Time.Hour.ToString("00"), answer.Source.Time.Minute.ToString("00"), answer.Source.Time.Second.ToString("00"));
                    changeRec.iDurationSignal = answer.Source.Duration;
                    changeRec.iCountExits = answer.Source.BroadcastCount;
                    changeRec.bKR = Convert.ToByte(answer.Source.IsActive);

                    for (int i = 0; i < listFRCh.Count; i++)
                    {
                        if (listFRCh.Exists(x => x.iID == changeRec.iID))
                        {
                            int ind = listFRCh.FindIndex(x => x.iID == changeRec.iID);
                            Invoke((MethodInvoker)(() => funcDGV_IRI_FRCh.ChangeRecord_IRI_FRChToDGV(dgvIRI_FRCh.dgv, NameTable.IRI_FRCh, changeRec, ind))); 
                            
                            return;
                        }
                    }
                }
            }
            catch { }
        }
    }
}
