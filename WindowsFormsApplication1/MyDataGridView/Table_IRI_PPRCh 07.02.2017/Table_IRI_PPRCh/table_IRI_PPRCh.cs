﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using USR_DLL;
using MyDataGridView;
using VariableDynamic;
using VariableStatic;

namespace Table_IRI_PPRCh
{
    public partial class table_IRI_PPRCh: UserControl
    {
        FuncDGV_IRI_PPRCh funcDGV_IRI_PPRCh;
        FuncDB_IRI_PPRCh funcDB_IRI_PPRCh;
        FunctionsDGV functionsDGV;
        FunctionsDB functionsDB;
        Functions functions;
        FunctionsTranslate functionsTranslate;

        FunctionsUser functionsUser;

        //Struct_IRI_PPRCh[] struct_IRI_PPRCh;
        public List<TDistribFHSS> listPPRCh;
        
        public IReadOnlyList<TDistribFHSS> ListPPRChPublic // для чтения кому-то (без возможности изменения)
        {
            get { return listPPRCh; }
        }

        VariableWork variableWork;
        TDistribFHSS[] DistribFHSS;

        FuncDB_IRI_PPRCh_RP funcDB_IRI_PPRCh_RP;
        FuncVW_IRI_PPRCh_RP funcVW_IRI_PPRCh_RP;

        //TCoordsIRI_PPRCh[] tCoordsIRI_PPRCh;
        List<TCoordsIRI_PPRCh> listCoordsIRI_PPRCh;
        TCoordsIRI_PPRCh[] pelengsIRI_PPRCh;

        public int iIndSelectRowPPRCh; // индекс выделенной строки

        int indTabControl = 3; // индекс TabControl в главном окне (ППРЧ)

        public table_IRI_PPRCh()
        {
            InitializeComponent();

            funcDGV_IRI_PPRCh = new FuncDGV_IRI_PPRCh();
            funcDB_IRI_PPRCh = new FuncDB_IRI_PPRCh();
            functionsDGV = new FunctionsDGV();
            functionsDB = new FunctionsDB();
            functions = new Functions();
            functionsTranslate = new FunctionsTranslate();

            functionsUser = new FunctionsUser();

            listPPRCh = new List<TDistribFHSS>();

            funcDB_IRI_PPRCh_RP = new FuncDB_IRI_PPRCh_RP();
            funcVW_IRI_PPRCh_RP = new FuncVW_IRI_PPRCh_RP();

            dgvIRI_PPRCh1.InitTableDB(dgvIRI_PPRCh1.dgv, NameTable.IRI_PPRCh);
            dgvIRI_PPRCh2.InitTableDB(dgvIRI_PPRCh2.dgv, NameTable.IRI_PPRCh2);
            dgvIRI_PPRCh3.InitTableDB(dgvIRI_PPRCh3.dgv, NameTable.IRI_PPRCh3);

            variableWork = new VariableWork(); 

            Task.Run(() => GetDataPPRCh());

            VariableCommon.OnChangeCommonLanguage += new VariableCommon.ChangeCommonEventHandler(VariableCommon_OnChangeCommonLanguage);

            dgvIRI_PPRCh1.dgv.SelectionChanged += dgv_SelectionChanged;

            // Событие возникает при нажатии на ячейку таблицы
            dgvIRI_PPRCh1.dgv.CellClick += new DataGridViewCellEventHandler(OnDGV_PPRCh1_CellClick);

            VariableDynamic.VariableWork.aWPtoBearingDSPprotocolNew.StorageActionMessageUpdate += aWPtoBearingDSPprotocolNew_StorageActionMessageUpdate;

            VariableDynamic.VariableWork.OnChangeTabControlTables += VariableWork_OnChangeTabControlTables;
            // Событие возникает при изменении значений таблицы ИРИ ППРЧ
            VariableWork.OnChangeDistribFHSS += new VariableWork.ChangeVariableEventHandler(VariableWork_OnChangeDistribFHSS); 

        }

        void VariableWork_OnChangeTabControlTables(int index)
        {
            try
            {
                indTabControl = index;

                if (index != 3)
                {
                    pelengsIRI_PPRCh = new TCoordsIRI_PPRCh[1];

                    pelengsIRI_PPRCh[0].iID = -1;
                    pelengsIRI_PPRCh[0].iQ1 = -1; // пеленг 1
                    pelengsIRI_PPRCh[0].iQ2 = -1; // пеленг 2
                    pelengsIRI_PPRCh[0].dLatitude = -1.0;   // широта
                    pelengsIRI_PPRCh[0].dLongitude = -1.0; // долгота
                    pelengsIRI_PPRCh[0].bDelIRI = 3;

                    variableWork.PelengsIRI_PPRCh = pelengsIRI_PPRCh;
                }
            }
            catch { }
        }

        void aWPtoBearingDSPprotocolNew_StorageActionMessageUpdate(Protocols.StorageActionMessage answer)
        {
            if (InvokeRequired)
            {
                BeginInvoke((MethodInvoker)(()
                    => aWPtoBearingDSPprotocolNew_StorageActionMessageUpdate(answer)));
                return;
            }

            if (answer != null)
            {
                if (answer.Header.ErrorCode == 0)
                {
                    if (answer.Storage == DspDataModel.StorageType.Fhss)
                    {
                        if (answer.SignalsId.Length == 0) //Полностью очищаем таблицу
                        {
                            int count = 0;

                            for (int i = 0; i < dgvIRI_PPRCh1.dgv.RowCount; i++)
                            {
                                if (dgvIRI_PPRCh1.dgv.Rows[i].Cells[0].Value == null)
                                    count++;
                            }

                            if (count == dgvIRI_PPRCh1.dgv.RowCount)
                                return;

                            // удалить все записи из dgv
                            functionsDGV.DeleteAllRecordsDGV(dgvIRI_PPRCh1.dgv);
                            // добавить пустые строки в dgv при необходимости
                            dgvIRI_PPRCh1.SetInitialNumberOfRows(dgvIRI_PPRCh1.dgv, dgvIRI_PPRCh1.tableIRI_PPRCh, NameTable.IRI_PPRCh);

                            // удалить все записи из dgv
                            functionsDGV.DeleteAllRecordsDGV(dgvIRI_PPRCh2.dgv);
                            // добавить пустые строки в dgv при необходимости
                            dgvIRI_PPRCh2.SetInitialNumberOfRows(dgvIRI_PPRCh2.dgv, dgvIRI_PPRCh2.tableIRI_PPRCh2, NameTable.IRI_PPRCh2);

                            // удалить все записи из dgv
                            functionsDGV.DeleteAllRecordsDGV(dgvIRI_PPRCh3.dgv);
                            // добавить пустые строки в dgv при необходимости
                            dgvIRI_PPRCh3.SetInitialNumberOfRows(dgvIRI_PPRCh3.dgv, dgvIRI_PPRCh3.tableIRI_PPRCh3, NameTable.IRI_PPRCh3);

                            listPPRCh.Clear();

                            if (functionsDB.DeleteAllRecordsDB(NameTable.IRI_PPRCh, 0) &&
                                functionsDB.DeleteAllRecordsDB(NameTable.IRI_PPRCh2, 0) &&
                                functionsDB.DeleteAllRecordsDB(NameTable.IRI_PPRCh3, 0))
                            {
                                variableWork.DistribFHSS = listPPRCh.ToArray();
                            }
                        }
                        else //Выборочно очищаем или восстанавливаем
                        {
                            if (answer.Action == DspDataModel.SignalAction.Hide)
                            {

                            }
                            if (answer.Action == DspDataModel.SignalAction.Restore)
                            {

                            }
                        }
                    }
                }
            }
        }


        private void VariableCommon_OnChangeCommonLanguage()
        {
            VariableCommon variableCommon = new VariableCommon();
            switch (variableCommon.Language)
            {
                case 0:
                    functionsTranslate.SetLanguage("rus");
                    ChangeLanguage();
                    break;

                case 1:
                    functionsTranslate.SetLanguage("eng");
                    ChangeLanguage();
                    break;

                case 2:
                    functionsTranslate.SetLanguage("az");
                    ChangeLanguage();
                    break;

                default:
                    functionsTranslate.SetLanguage("rus");
                    ChangeLanguage();
                    break;
            }
        }

        /// <summary>
        /// Для инициализации языка при запуске приложения 
        /// </summary>
        /// <param name="bLanguage"></param>
        public void ChangeControlLanguage(byte bLanguage)
        {

            switch (bLanguage)
            {
                case 0:
                    functionsTranslate.SetLanguage("rus");
                    ChangeLanguage();
                    break;

                case 1:
                    functionsTranslate.SetLanguage("eng");
                    ChangeLanguage();
                    break;

                case 2:
                    functionsTranslate.SetLanguage("az");
                    ChangeLanguage();
                    break;

                default:
                    functionsTranslate.SetLanguage("rus");
                    ChangeLanguage();
                    break;
            }
        }

        /// <summary>
        /// Изменение языка интерфейса
        /// </summary>
        private void ChangeLanguage()
        {
            functionsTranslate.RenameButtons(functionsTranslate.Dictionary, pButtons);
            functionsTranslate.RenameFieldsTables(functionsTranslate.Dictionary, dgvIRI_PPRCh1.dgv);
            functionsTranslate.RenameFieldsTables(functionsTranslate.Dictionary, dgvIRI_PPRCh2.dgv);
            functionsTranslate.RenameFieldsTables(functionsTranslate.Dictionary, dgvIRI_PPRCh3.dgv);
        }

        void dgv_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                iIndSelectRowPPRCh = Convert.ToInt32(dgvIRI_PPRCh1.dgv.Rows[dgvIRI_PPRCh1.dgv.SelectedRows[0].Index].Cells[0].Value);

                // Отправить пеленги для отображения на карте
                if (dgvIRI_PPRCh1.dgv.Rows[dgvIRI_PPRCh1.dgv.SelectedRows[0].Index].Cells[0].Value == null)
                {
                    //tCoordsIRI_PPRCh = new TCoordsIRI_PPRCh[1];

                    //tCoordsIRI_PPRCh[0].iID = -1;
                    //tCoordsIRI_PPRCh[0].iQ1 = -1; // пеленг 1
                    //tCoordsIRI_PPRCh[0].iQ2 = -1; // пеленг 2
                    //tCoordsIRI_PPRCh[0].dLatitude = -1.0;   // широта
                    //tCoordsIRI_PPRCh[0].dLongitude = -1.0; // долгота
                    //tCoordsIRI_PPRCh[0].bDelIRI = 3;
                    //variableWork.CoordsIRI_PPRCh = tCoordsIRI_PPRCh;

                    pelengsIRI_PPRCh = new TCoordsIRI_PPRCh[1];

                    pelengsIRI_PPRCh[0].iID = -1;
                    pelengsIRI_PPRCh[0].iQ1 = -1; // пеленг 1
                    pelengsIRI_PPRCh[0].iQ2 = -1; // пеленг 2
                    pelengsIRI_PPRCh[0].dLatitude = -1.0;   // широта
                    pelengsIRI_PPRCh[0].dLongitude = -1.0; // долгота
                    pelengsIRI_PPRCh[0].bDelIRI = 3;

                    variableWork.PelengsIRI_PPRCh = pelengsIRI_PPRCh;
                    
                }
                else 
                {
                    var index = listPPRCh.FindIndex(item => item.iID == iIndSelectRowPPRCh); // сохранение текущего выделения строки в таблице
                    if (index != -1)
                    {
                        //tCoordsIRI_PPRCh = new TCoordsIRI_PPRCh[listPPRCh[index].sLocation.Count()];

                        //for (int j = 0; j < listPPRCh[index].sLocation.Count(); j++)
                        //{
                        //    tCoordsIRI_PPRCh[j].iID = listPPRCh[index].sLocation[j].iID;
                        //    tCoordsIRI_PPRCh[j].iQ1 = listPPRCh[index].sLocation[j].iQ1; // пеленг 1
                        //    tCoordsIRI_PPRCh[j].iQ2 = listPPRCh[index].sLocation[j].iQ2; // пеленг 2
                        //    tCoordsIRI_PPRCh[j].dLatitude = -1.0;   // широта
                        //    tCoordsIRI_PPRCh[j].dLongitude = -1.0; // долгота
                        //    tCoordsIRI_PPRCh[j].bDelIRI = 3;
                        //    tCoordsIRI_PPRCh[j].sColorIRI = functions.ColorFromFreqRange(listPPRCh[index].iFreqMin + ((listPPRCh[index].iFreqMax - listPPRCh[index].iFreqMin) / 2));
                        //}

                        //variableWork.CoordsIRI_PPRCh = tCoordsIRI_PPRCh;

                        pelengsIRI_PPRCh = new TCoordsIRI_PPRCh[listPPRCh[index].sLocation.Count()];

                        for (int j = 0; j < listPPRCh[index].sLocation.Count(); j++)
                        {
                            pelengsIRI_PPRCh[j].iID = listPPRCh[index].sLocation[j].iID;
                            pelengsIRI_PPRCh[j].iQ1 = listPPRCh[index].sLocation[j].iQ1; // пеленг 1
                            pelengsIRI_PPRCh[j].iQ2 = listPPRCh[index].sLocation[j].iQ2; // пеленг 2
                            pelengsIRI_PPRCh[j].dLatitude = -1.0;   // широта
                            pelengsIRI_PPRCh[j].dLongitude = -1.0; // долгота
                            pelengsIRI_PPRCh[j].bDelIRI = 3;
                            pelengsIRI_PPRCh[j].sColorIRI = functions.ColorFromFreqRange(listPPRCh[index].iFreqMin + ((listPPRCh[index].iFreqMax - listPPRCh[index].iFreqMin) / 2));
                        }
                        variableWork.PelengsIRI_PPRCh = pelengsIRI_PPRCh;
                    }
                }
                //if (dgvIRI_PPRCh1.dgv.Rows[dgvIRI_PPRCh1.dgv.SelectedRows[0].Index].Cells[0].Value == null)
                //    return;
                //else
                //    iIndSelectRowPPRCh = Convert.ToInt32(dgvIRI_PPRCh1.dgv.Rows[dgvIRI_PPRCh1.dgv.SelectedRows[0].Index].Cells[0].Value);
            }
            catch { }
        }

        void VariableWork_OnChangeDistribFHSS()
        {
            if (variableWork.DistribFHSS != null)
            {
                int len = variableWork.DistribFHSS.Length;

                if (dgvIRI_PPRCh1.dgv.InvokeRequired)
                {
                    Invoke((MethodInvoker)(() =>
                        {
                            while (len < dgvIRI_PPRCh1.dgv.RowCount)
                            {
                                dgvIRI_PPRCh1.dgv.Rows.RemoveAt(dgvIRI_PPRCh1.dgv.RowCount - 1);
                            }

                             // удалить все записи из dgv
                            //functionsDGV.DeleteAllRecordsDGV(dgvIRI_PPRCh1.dgv);
                            // добавить пустые строки в dgv при необходимости
                            dgvIRI_PPRCh1.SetInitialNumberOfRows(dgvIRI_PPRCh1.dgv, dgvIRI_PPRCh1.tableIRI_PPRCh, NameTable.IRI_PPRCh);
                        }));
                }
                else
                {
                    while (len < dgvIRI_PPRCh1.dgv.RowCount)
                    {
                        dgvIRI_PPRCh1.dgv.Rows.RemoveAt(dgvIRI_PPRCh1.dgv.RowCount - 1);
                    }

                    // удалить все записи из dgv
                    //functionsDGV.DeleteAllRecordsDGV(dgvIRI_PPRCh1.dgv);
                    // добавить пустые строки в dgv при необходимости
                    dgvIRI_PPRCh1.SetInitialNumberOfRows(dgvIRI_PPRCh1.dgv, dgvIRI_PPRCh1.tableIRI_PPRCh, NameTable.IRI_PPRCh);
                }
               
                List<Struct_IRI_PPRCh> lFHSS = new List<Struct_IRI_PPRCh>();
                for (int i = 0; i < len; i++)
                {
                    Struct_IRI_PPRCh DistribFHSS = new Struct_IRI_PPRCh();

                    DistribFHSS.iID = variableWork.DistribFHSS[i].iID;
                    DistribFHSS.iFreqMin = variableWork.DistribFHSS[i].iFreqMin;
                    DistribFHSS.iFreqMax = variableWork.DistribFHSS[i].iFreqMax;
                    DistribFHSS.iStep = variableWork.DistribFHSS[i].iStep;
                    DistribFHSS.iDFreq = variableWork.DistribFHSS[i].iDFreq;
                    DistribFHSS.iDuratImp = variableWork.DistribFHSS[i].iDuratImp;
                    DistribFHSS.iCountIRI = variableWork.DistribFHSS[i].iCountIRI;
                    DistribFHSS.sTime = variableWork.DistribFHSS[i].sTime;
                    DistribFHSS.iSP_RR = variableWork.DistribFHSS[i].iSP_RR;
                    DistribFHSS.bCR = variableWork.DistribFHSS[i].bCR;

                    DistribFHSS.sLocation = new Struct_IRI_PPRCh_Location[variableWork.DistribFHSS[i].sLocation.Length];
                    for (int j = 0; j < variableWork.DistribFHSS[i].sLocation.Length; j++)
                    {
                        DistribFHSS.sLocation[j].iID = variableWork.DistribFHSS[i].sLocation[j].iID;
                        DistribFHSS.sLocation[j].iQ1 = variableWork.DistribFHSS[i].sLocation[j].iQ1;
                        DistribFHSS.sLocation[j].iQ2 = variableWork.DistribFHSS[i].sLocation[j].iQ2;
                        DistribFHSS.sLocation[j].dLatitude = variableWork.DistribFHSS[i].sLocation[j].dLatitude;
                        DistribFHSS.sLocation[j].dLongitude = variableWork.DistribFHSS[i].sLocation[j].dLongitude;
                    }

                    DistribFHSS.sFreq_Band = new Struct_IRI_PPRCh_Freq_Band[variableWork.DistribFHSS[i].sFreq_Band.Length];
                    for (int j = 0; j < variableWork.DistribFHSS[i].sFreq_Band.Length; j++ )
                    {
                        DistribFHSS.sFreq_Band[j].iID = variableWork.DistribFHSS[i].sFreq_Band[j].iID;
                        DistribFHSS.sFreq_Band[j].iFreq = variableWork.DistribFHSS[i].sFreq_Band[j].iFreq;
                        DistribFHSS.sFreq_Band[j].iDFreq = variableWork.DistribFHSS[i].sFreq_Band[j].iDFreq;
                    }

                    lFHSS.Add(DistribFHSS);
                }

                if (dgvIRI_PPRCh1.dgv.InvokeRequired)
                {
                    Invoke((MethodInvoker)(() => funcDGV_IRI_PPRCh.AddRecord_IRI_PPRChToDGV(dgvIRI_PPRCh1.dgv, NameTable.IRI_PPRCh, lFHSS.ToArray())));
                }
                else
                {
                    funcDGV_IRI_PPRCh.AddRecord_IRI_PPRChToDGV(dgvIRI_PPRCh1.dgv, NameTable.IRI_PPRCh, lFHSS.ToArray());
                }
            }
        }

        public void LoadTableIRI_PPRChfromDB()
        {
            funcDB_IRI_PPRCh.LoadTableIRI_PPRChFromDB(NameTable.IRI_PPRCh);

            int len = variableWork.DistribFHSS.Length;
            for (int i = 0; i < len; i++)
            {
                listPPRCh.Add(variableWork.DistribFHSS[i]);
            }

            OnDGV_PPRCh1_CellClick(this, null);
        }

        private void OnDGV_PPRCh1_CellClick(object sender, EventArgs e)
        {
            if (InvokeRequired)
            {
                Invoke((MethodInvoker)(() => OnDGV_PPRCh1_CellClick(sender, e)));
                return;
            }

            if (dgvIRI_PPRCh1.dgv.SelectedRows.Count != 1) { return; }

            // удалить все записи из dgv
            functionsDGV.DeleteAllRecordsDGV(dgvIRI_PPRCh2.dgv);
            // добавить пустые строки в dgv при необходимости
            dgvIRI_PPRCh2.SetInitialNumberOfRows(dgvIRI_PPRCh2.dgv, dgvIRI_PPRCh2.tableIRI_PPRCh2, NameTable.IRI_PPRCh2);

            // удалить все записи из dgv
            functionsDGV.DeleteAllRecordsDGV(dgvIRI_PPRCh3.dgv);
            // добавить пустые строки в dgv при необходимости
            dgvIRI_PPRCh3.SetInitialNumberOfRows(dgvIRI_PPRCh3.dgv, dgvIRI_PPRCh2.tableIRI_PPRCh3, NameTable.IRI_PPRCh3);

            if (dgvIRI_PPRCh1.dgv.Rows[dgvIRI_PPRCh1.dgv.SelectedRows[0].Index].Cells[1].Value == null) { return; }

            try
            {
                var row = dgvIRI_PPRCh1.dgv.SelectedRows[0];
                string sCellValueFmin = functions.StrToFreq(Convert.ToString(row.Cells[1].Value));
                int cellValueFmin = Convert.ToInt32(Convert.ToDecimal(sCellValueFmin, Functions.format) * 10);

                string sCellValueFmax = functions.StrToFreq(Convert.ToString(row.Cells[2].Value));
                int cellValueFmax = Convert.ToInt32(Convert.ToDecimal(sCellValueFmax, Functions.format) * 10);
             
                if (cellValueFmin < 0 && cellValueFmax < 0) { return; }

                List<Struct_IRI_PPRCh_Location> lLocation = new List<Struct_IRI_PPRCh_Location>();
                List<Struct_IRI_PPRCh_Freq_Band> lFreqBand = new List<Struct_IRI_PPRCh_Freq_Band>();

                for (int i = 0; i < variableWork.DistribFHSS.Count(); i++)
                {
                    if (cellValueFmin == variableWork.DistribFHSS[i].iFreqMin && cellValueFmax == variableWork.DistribFHSS[i].iFreqMax)
                    {
                        Struct_IRI_PPRCh_Location[] location = new Struct_IRI_PPRCh_Location[variableWork.DistribFHSS[i].sLocation.Count()];
                        for (int j = 0; j < variableWork.DistribFHSS[i].sLocation.Count(); j++)
                        {
                            location[j].iID = variableWork.DistribFHSS[i].sLocation[j].iID;
                            location[j].iQ1 = variableWork.DistribFHSS[i].sLocation[j].iQ1;
                            location[j].iQ2 = variableWork.DistribFHSS[i].sLocation[j].iQ2;
                            location[j].dLatitude = variableWork.DistribFHSS[i].sLocation[j].dLatitude;
                            location[j].dLongitude = variableWork.DistribFHSS[i].sLocation[j].dLongitude;

                            lLocation.Add(location[j]);
                        }
                        funcDGV_IRI_PPRCh.AddRecord_IRI_PPRCh_LocationToDGV(dgvIRI_PPRCh2.dgv, NameTable.IRI_PPRCh2, lLocation.ToArray());

                        Struct_IRI_PPRCh_Freq_Band[] freq_band = new Struct_IRI_PPRCh_Freq_Band[variableWork.DistribFHSS[i].sFreq_Band.Count()];
                        for (int j = 0; j < variableWork.DistribFHSS[i].sFreq_Band.Count(); j++)
                        {
                            freq_band[j].iID = variableWork.DistribFHSS[i].sFreq_Band[j].iID;
                            freq_band[j].iFreq = variableWork.DistribFHSS[i].sFreq_Band[j].iFreq;
                            freq_band[j].iDFreq = variableWork.DistribFHSS[i].sFreq_Band[j].iDFreq;

                            lFreqBand.Add(freq_band[j]);
                        }
                        funcDGV_IRI_PPRCh.AddRecord_IRI_PPRCh_Freq_BandToDGV(dgvIRI_PPRCh3.dgv, NameTable.IRI_PPRCh3, lFreqBand.ToArray());
                    }
                }
            }
            catch { }
        }

        /// <summary>
        /// Получить данные ИРИ ППРЧ
        /// Режимы:
        /// 0 - подготовка
        /// 1 - радиоразведка без пеленгования
        /// 2 - радиоразведка с пеленгованием
        /// 3 - радиоподавление
        /// </summary>
        private async void GetDataPPRCh()
        {
            while (true)
            {
                if (variableWork.Regime == 1 || variableWork.Regime == 2)
                {
                    var answer = await VariableWork.aWPtoBearingDSPprotocolNew.GetFhssNetworks();

                    if (answer != null && answer.FhssNetworks.Count() > 0)
                    {
                        DistribFHSS = new TDistribFHSS[answer.FhssNetworks.Count()];

                        for (int i = 0; i < answer.FhssNetworks.Count(); i++)
                        {
                            DistribFHSS[i].iID = answer.FhssNetworks[i].Id;
                            DistribFHSS[i].iFreqMin = answer.FhssNetworks[i].StartFrequency;   // частота min
                            DistribFHSS[i].iFreqMax = answer.FhssNetworks[i].EndFrequency;     // частота max
                            DistribFHSS[i].iStep = answer.FhssNetworks[i].Step;                // шаг
                            DistribFHSS[i].iDFreq = answer.FhssNetworks[i].Bandwidth;          // ширина полосы
                            DistribFHSS[i].iDuratImp = answer.FhssNetworks[i].ImpulseDuration; // количество частот // длительность импульсов
                            DistribFHSS[i].iCountIRI = answer.FhssNetworks[i].LocationsCount;  // количество ИРИ
                            DistribFHSS[i].sTime = string.Format("{0}:{1}:{2}", answer.FhssNetworks[i].Time.Hour.ToString("00"), answer.FhssNetworks[i].Time.Minute.ToString("00"), answer.FhssNetworks[i].Time.Second.ToString("00")); // время

                            DistribFHSS[i].sLocation = new FHSS_Location[answer.FhssNetworks[i].Locations.Count()];
                            for (int j = 0; j < answer.FhssNetworks[i].Locations.Count(); j++)
                            {
                                DistribFHSS[i].sLocation[j].iID = answer.FhssNetworks[i].Id;
                                DistribFHSS[i].sLocation[j].iQ1 = answer.FhssNetworks[i].Locations[j].Direction1; // пеленг 1
                                DistribFHSS[i].sLocation[j].iQ2 = answer.FhssNetworks[i].Locations[j].Direction2; // пеленг 2
                                DistribFHSS[i].sLocation[j].dLatitude = answer.FhssNetworks[i].Locations[j].Latitude;   // широта
                                DistribFHSS[i].sLocation[j].dLongitude = answer.FhssNetworks[i].Locations[j].Longitude; // долгота
                            }

                            DistribFHSS[i].sFreq_Band = new FHSS_Freq_Band[answer.FhssNetworks[i].FixedRadioSources.Count()];
                            for (int j = 0; j < answer.FhssNetworks[i].FixedRadioSources.Count(); j++)
                            {
                                DistribFHSS[i].sFreq_Band[j].iID = answer.FhssNetworks[i].Id;
                                DistribFHSS[i].sFreq_Band[j].iFreq = answer.FhssNetworks[i].FixedRadioSources[j].Frequency;  // частота
                                DistribFHSS[i].sFreq_Band[j].iDFreq = answer.FhssNetworks[i].FixedRadioSources[j].Bandwidth; // ширина полосы
                            }

                            if (listPPRCh.Count == 0)
                                listPPRCh.Add(DistribFHSS[i]);
                            else
                            {
                                if (listPPRCh.Exists(x => x.iID == DistribFHSS[i].iID))
                                {
                                    int ind = listPPRCh.FindIndex(x => x.iID == DistribFHSS[i].iID);
                                    listPPRCh[ind] = DistribFHSS[i]; // заменить
                                }
                                else listPPRCh.Add(DistribFHSS[i]); // добавить в конец листа 
                            }
                        }

                        if (functionsDB.DeleteAllRecordsDB(NameTable.IRI_PPRCh, 0) &&
                            functionsDB.DeleteAllRecordsDB(NameTable.IRI_PPRCh2, 0) &&
                            functionsDB.DeleteAllRecordsDB(NameTable.IRI_PPRCh3, 0))
                        {
                            if (funcDB_IRI_PPRCh.AddRecords_IRI_PPRChToDB(NameTable.IRI_PPRCh, DistribFHSS))
                            {
                                variableWork.DistribFHSS = listPPRCh.ToArray();
                            }
                        }

                        var index = listPPRCh.FindIndex(item => item.iID == iIndSelectRowPPRCh); // сохранение текущего выделения строки в таблице
                        if (index != -1)
                        {
                            if (dgvIRI_PPRCh1.dgv.InvokeRequired)
                            {
                                Invoke((MethodInvoker)(() => dgvIRI_PPRCh1.dgv.Rows[index].Selected = true));
                            }
                            else
                            {
                                dgvIRI_PPRCh1.dgv.Rows[index].Selected = true;
                            }

                            // Отправить ИРИ для отображения на карте//////////////
                            //tCoordsIRI_PPRCh = new TCoordsIRI_PPRCh[listPPRCh[index].sLocation.Count()];

                            //for (int j = 0; j < listPPRCh[index].sLocation.Count(); j++)
                            //{
                            //    tCoordsIRI_PPRCh[j].iID = listPPRCh[index].sLocation[j].iID;
                            //    tCoordsIRI_PPRCh[j].iQ1 = listPPRCh[index].sLocation[j].iQ1; // пеленг 1
                            //    tCoordsIRI_PPRCh[j].iQ2 = listPPRCh[index].sLocation[j].iQ2; // пеленг 2
                            //    tCoordsIRI_PPRCh[j].dLatitude = listPPRCh[index].sLocation[j].dLatitude;   // широта
                            //    tCoordsIRI_PPRCh[j].dLongitude = listPPRCh[index].sLocation[j].dLongitude; // долгота
                            //    tCoordsIRI_PPRCh[j].bDelIRI = 0;
                            //    tCoordsIRI_PPRCh[j].sColorIRI = functions.ColorFromFreqRange(listPPRCh[index].iFreqMin + ((listPPRCh[index].iFreqMax - listPPRCh[index].iFreqMin) / 2));
                            //}

                            //variableWork.CoordsIRI_PPRCh = tCoordsIRI_PPRCh;
                            ///////////////////////////////////////////////////////
                        }
                        // Отправить ИРИ для отображения на карте///////////////////////////////////
                        List<TCoordsIRI_PPRCh> listCoordsIRI_PPRCh = new List<TCoordsIRI_PPRCh>();

                        for (int j = 0; j < listPPRCh.Count(); j++)
                        {
                            for (int l = 0; l < listPPRCh[j].sLocation.Count(); l++)
                            {
                                ////TEST////////////////////////////////////////////////////
                                //System.Threading.Thread.Sleep(25);
                                //Random rand = new Random();
                                //listPPRCh[j].sLocation[l].dLatitude = new double();
                                //listPPRCh[j].sLocation[l].dLatitude = 53.0 + rand.NextDouble();  // широта
                                //listPPRCh[j].sLocation[l].dLongitude = new double();
                                //listPPRCh[j].sLocation[l].dLongitude = 27.0 + rand.NextDouble(); // долгота
                                //TEST////////////////////////////////////////////////////

                                if (listPPRCh[j].sLocation[l].dLatitude != -1 && listPPRCh[j].sLocation[l].dLongitude != -1)
                                {
                                    TCoordsIRI_PPRCh temp = new TCoordsIRI_PPRCh();
                                    temp.iID = listPPRCh[j].sLocation[l].iID;
                                    temp.iQ1 = -1; // listPPRCh[j].sLocation[l].iQ1; // пеленг 1
                                    temp.iQ2 = -1; // listPPRCh[j].sLocation[l].iQ2; // пеленг 2
                                    temp.dLatitude = listPPRCh[j].sLocation[l].dLatitude;   // широта
                                    temp.dLongitude = listPPRCh[j].sLocation[l].dLongitude; // долгота
                                    temp.bDelIRI = 0;
                                    temp.sColorIRI = functions.ColorFromFreqRange(listPPRCh[j].iFreqMin + ((listPPRCh[j].iFreqMax - listPPRCh[j].iFreqMin) / 2));

                                    listCoordsIRI_PPRCh.Add(temp);
                                }
                            }
                        } 

                        if (listCoordsIRI_PPRCh != null && listCoordsIRI_PPRCh.Count != 0)
                        {
                            variableWork.CoordsIRI_PPRCh = listCoordsIRI_PPRCh;
                        }
                        // /////////////////////////////////////////////////////////////////////////
                       
                    }

                    OnDGV_PPRCh1_CellClick(this, null);
                }

                await Task.Delay(3000);
            }
        }

        private async void bClear_Click(object sender, EventArgs e)
        {
            if (InvokeRequired)
            {
                BeginInvoke((MethodInvoker)(() =>  bClear_Click(sender, e)));
                return;
            }

            try
            {
                int count = 0;

                for (int i = 0; i < dgvIRI_PPRCh1.dgv.RowCount; i++)
                {
                    if (dgvIRI_PPRCh1.dgv.Rows[i].Cells[0].Value == null)
                        count++;
                }

                if (count == dgvIRI_PPRCh1.dgv.RowCount)
                    return;

                int[] SignalsId = new int[0];
                //await VariableWork.aWPtoBearingDSPprotocolNew.StorageAction(0 - ФРЧ, 1 - ППРЧ; 0 - удалить, 1 - восстановить; массив id);
                await VariableWork.aWPtoBearingDSPprotocolNew.StorageAction(1, 0, SignalsId); // отправить запрос на очистку ИРИ ППРЧ на сервер

                // Очистить все ИРИ и пеленги на карте//////////////
                // Источники
                listCoordsIRI_PPRCh = new List<TCoordsIRI_PPRCh>();// TCoordsIRI_PPRCh[1];
                TCoordsIRI_PPRCh tCoordsIRI_PPRCh = new TCoordsIRI_PPRCh();

                tCoordsIRI_PPRCh.bDelIRI = 2;
                listCoordsIRI_PPRCh.Add(tCoordsIRI_PPRCh);

                if (listCoordsIRI_PPRCh != null && listCoordsIRI_PPRCh.Count != 0)
                {
                    variableWork.CoordsIRI_PPRCh = listCoordsIRI_PPRCh;
                }

                // Пеленги
                pelengsIRI_PPRCh = new TCoordsIRI_PPRCh[1];

                pelengsIRI_PPRCh[0].iID = -1;
                pelengsIRI_PPRCh[0].iQ1 = -1; // пеленг 1
                pelengsIRI_PPRCh[0].iQ2 = -1; // пеленг 2
                pelengsIRI_PPRCh[0].dLatitude = -1.0;   // широта
                pelengsIRI_PPRCh[0].dLongitude = -1.0; // долгота
                pelengsIRI_PPRCh[0].bDelIRI = 3;

                variableWork.PelengsIRI_PPRCh = pelengsIRI_PPRCh;
                ///////////////////////////////////////////////////////

                // удалить все записи из dgv
                functionsDGV.DeleteAllRecordsDGV(dgvIRI_PPRCh1.dgv);
                // добавить пустые строки в dgv при необходимости
                dgvIRI_PPRCh1.SetInitialNumberOfRows(dgvIRI_PPRCh1.dgv, dgvIRI_PPRCh1.tableIRI_PPRCh, NameTable.IRI_PPRCh);

                // удалить все записи из dgv
                functionsDGV.DeleteAllRecordsDGV(dgvIRI_PPRCh2.dgv);
                // добавить пустые строки в dgv при необходимости
                dgvIRI_PPRCh2.SetInitialNumberOfRows(dgvIRI_PPRCh2.dgv, dgvIRI_PPRCh2.tableIRI_PPRCh2, NameTable.IRI_PPRCh2);

                // удалить все записи из dgv
                functionsDGV.DeleteAllRecordsDGV(dgvIRI_PPRCh3.dgv);
                // добавить пустые строки в dgv при необходимости
                dgvIRI_PPRCh3.SetInitialNumberOfRows(dgvIRI_PPRCh3.dgv, dgvIRI_PPRCh3.tableIRI_PPRCh3, NameTable.IRI_PPRCh3);

                listPPRCh.Clear();

                if (functionsDB.DeleteAllRecordsDB(NameTable.IRI_PPRCh, 0) && 
                    functionsDB.DeleteAllRecordsDB(NameTable.IRI_PPRCh2, 0) &&
                    functionsDB.DeleteAllRecordsDB(NameTable.IRI_PPRCh3, 0))
                {
                    variableWork.DistribFHSS = listPPRCh.ToArray();
                }
            }
            catch { }
        }

        private async void bDelete_Click(object sender, EventArgs e)
        {
            if (InvokeRequired)
            {
                BeginInvoke((MethodInvoker)(() => bDelete_Click(sender, e)));
                return;
            }

            try
            {
                if (dgvIRI_PPRCh1.dgv.Rows[dgvIRI_PPRCh1.dgv.SelectedRows[0].Index].Cells[0].Value == null)
                    return;

                int iID = Convert.ToInt32(dgvIRI_PPRCh1.dgv.Rows[dgvIRI_PPRCh1.dgv.SelectedRows[0].Index].Cells[0].Value);

                int[] SignalId = new int[1];
                SignalId[0] = Convert.ToInt32(dgvIRI_PPRCh1.dgv.Rows[dgvIRI_PPRCh1.dgv.SelectedRows[0].Index].Cells[0].Value);
                await VariableWork.aWPtoBearingDSPprotocolNew.StorageAction(1, 0, SignalId); // отправить запрос на очистку ИРИ ФРЧ на сервер

                // удалить запись из базы данных
                if (funcDB_IRI_PPRCh.DeleteRecordsPPRChDB(iID, NameTable.IRI_PPRCh, Table.Default) &&
                    funcDB_IRI_PPRCh.DeleteRecordsPPRChDB(iID, NameTable.IRI_PPRCh2, Table.Default) &&
                    funcDB_IRI_PPRCh.DeleteRecordsPPRChDB(iID, NameTable.IRI_PPRCh3, Table.Default))
                {
                    int ind = listPPRCh.FindIndex(x => x.iID == iID);

                    // Отправить все ИРИ для удаления на карте//////////////
                    listCoordsIRI_PPRCh = new List<TCoordsIRI_PPRCh>();// TCoordsIRI_PPRCh[listPPRCh[ind].sLocation.Count()];

                    for (int j = 0; j < listPPRCh[ind].sLocation.Count(); j++)
                    {
                        TCoordsIRI_PPRCh tCoordsIRI_PPRCh = new TCoordsIRI_PPRCh();
                        tCoordsIRI_PPRCh.iID = listPPRCh[ind].sLocation[j].iID;
                        tCoordsIRI_PPRCh.iQ1 = listPPRCh[ind].sLocation[j].iQ1; // пеленг 1
                        tCoordsIRI_PPRCh.iQ2 = listPPRCh[ind].sLocation[j].iQ2; // пеленг 2
                        tCoordsIRI_PPRCh.dLatitude = listPPRCh[ind].sLocation[j].dLatitude;   // широта
                        tCoordsIRI_PPRCh.dLongitude = listPPRCh[ind].sLocation[j].dLongitude; // долгота
                        tCoordsIRI_PPRCh.bDelIRI = 1;

                        listCoordsIRI_PPRCh.Add(tCoordsIRI_PPRCh);
                    }

                    if (listCoordsIRI_PPRCh != null && listCoordsIRI_PPRCh.Count != 0)
                    {
                        variableWork.CoordsIRI_PPRCh = listCoordsIRI_PPRCh;
                    }
                    ///////////////////////////////////////////////////////

                    listPPRCh.RemoveAt(ind);

                    variableWork.DistribFHSS = listPPRCh.ToArray();

                    // удалить все записи из dgv
                    functionsDGV.DeleteAllRecordsDGV(dgvIRI_PPRCh2.dgv);
                    // добавить пустые строки в dgv при необходимости
                    dgvIRI_PPRCh2.SetInitialNumberOfRows(dgvIRI_PPRCh2.dgv, dgvIRI_PPRCh2.tableIRI_PPRCh2, NameTable.IRI_PPRCh2);

                    // удалить все записи из dgv
                    functionsDGV.DeleteAllRecordsDGV(dgvIRI_PPRCh3.dgv);
                    // добавить пустые строки в dgv при необходимости
                    dgvIRI_PPRCh3.SetInitialNumberOfRows(dgvIRI_PPRCh3.dgv, dgvIRI_PPRCh3.tableIRI_PPRCh3, NameTable.IRI_PPRCh3);
                }

            }
            catch { }
        }

        private void bPPRCh_RP_Click(object sender, EventArgs e)
        {
            if (dgvIRI_PPRCh1.dgv.Rows[dgvIRI_PPRCh1.dgv.SelectedRows[0].Index].Cells[0].Value == null)
                return;
            else
            {
                TDistribFHSS_RP DistribFHSS_RP = new TDistribFHSS_RP();
                List<TDistribFHSS_RP> lDistribFHSS_RP = new List<TDistribFHSS_RP>();

                DistribFHSS_RP.iID = Convert.ToInt32(dgvIRI_PPRCh1.dgv.Rows[dgvIRI_PPRCh1.dgv.SelectedRows[0].Index].Cells[0].Value);


                string sFreqMin = functions.StrToFreq(Convert.ToString(dgvIRI_PPRCh1.dgv.Rows[dgvIRI_PPRCh1.dgv.SelectedRows[0].Index].Cells[1].Value));
                DistribFHSS_RP.iFreqMin = Convert.ToInt32(Convert.ToDecimal(sFreqMin, Functions.format) * 10);

                string sFreqMax = functions.StrToFreq(Convert.ToString(dgvIRI_PPRCh1.dgv.Rows[dgvIRI_PPRCh1.dgv.SelectedRows[0].Index].Cells[2].Value));
                DistribFHSS_RP.iFreqMax = Convert.ToInt32(Convert.ToDecimal(sFreqMax, Functions.format) * 10);

                DistribFHSS_RP.bLetter = functions.DefineLetter(DistribFHSS_RP.iFreqMin, DistribFHSS_RP.iFreqMax); 

                DistribFHSS_RP.sLevel = -60;

                DistribFHSS_RP.iStep = 100;

                DistribFHSS_RP.bManipulation = 0;
                DistribFHSS_RP.bModulation = 1;
                DistribFHSS_RP.bDeviation = 1;
                DistribFHSS_RP.bDuration = 1;

                DistribFHSS_RP.iDuration = 1000;
                DistribFHSS_RP.bCodeFFT = 4;


                string sMessage = functions.CheckRangesRP(1, DistribFHSS_RP.iFreqMin, DistribFHSS_RP.iFreqMax);
                if (sMessage != "")
                {
                    MessageBox.Show(sMessage, SMessageError.mesErr, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                sMessage = functions.CheckForbiddenFreq(1, DistribFHSS_RP.iFreqMin, DistribFHSS_RP.iFreqMax);
                if (sMessage == "")
                {
                    lDistribFHSS_RP = new List<TDistribFHSS_RP>();
                    lDistribFHSS_RP = variableWork.DistribFHSS_RPOwn.ToList();

                    // Добавить lDistribFHSS_RPOwn в variableWork
                    sMessage = funcVW_IRI_PPRCh_RP.IsAddIRI_PPRCh_CRToVariableWork(variableWork, DistribFHSS_RP, Table.Own);
                    if (sMessage == "")
                    {
                        if (lDistribFHSS_RP.Count() > 0)
                        {
                            int id = lDistribFHSS_RP.Max(x => x.iID);
                            DistribFHSS_RP.iID = id + 1;
                        }
                        else
                        {
                            DistribFHSS_RP.iID = 1;
                        }
                        lDistribFHSS_RP.Add(DistribFHSS_RP);
                        variableWork.DistribFHSS_RPOwn = lDistribFHSS_RP.ToArray();
                    }
                    else
                    {
                        MessageBox.Show(sMessage, SMessageError.mesErr, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }
                else
                {
                    MessageBox.Show(sMessage, SMessageError.mesErr, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

            }
        }


        private async void bDuration_Click(object sender, EventArgs e)
        {
            try
            {
                await VariableWork.aWPtoBearingDSPprotocolNew.InquireFHHS(iIndSelectRowPPRCh); // отправить запрос на измерение длительности ППРЧ
            }
            catch { }
        }
    }
}
