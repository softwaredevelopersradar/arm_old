﻿namespace Table_IRI_PPRCh
{
    partial class table_IRI_PPRCh
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.bPPRCh_RP = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.pButtons = new System.Windows.Forms.Panel();
            this.bClear = new System.Windows.Forms.Button();
            this.bDelete = new System.Windows.Forms.Button();
            this.dgvIRI_PPRCh1 = new MyDataGridView.myDataGridView();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.dgvIRI_PPRCh2 = new MyDataGridView.myDataGridView();
            this.dgvIRI_PPRCh3 = new MyDataGridView.myDataGridView();
            this.bDuration = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            this.pButtons.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // bPPRCh_RP
            // 
            this.bPPRCh_RP.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bPPRCh_RP.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.bPPRCh_RP.Location = new System.Drawing.Point(230, 3);
            this.bPPRCh_RP.Name = "bPPRCh_RP";
            this.bPPRCh_RP.Size = new System.Drawing.Size(86, 23);
            this.bPPRCh_RP.TabIndex = 5;
            this.bPPRCh_RP.Text = "ППРЧ на РП";
            this.bPPRCh_RP.UseVisualStyleBackColor = true;
            this.bPPRCh_RP.Click += new System.EventHandler(this.bPPRCh_RP_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.pButtons, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.dgvIRI_PPRCh1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(566, 332);
            this.tableLayoutPanel1.TabIndex = 6;
            // 
            // pButtons
            // 
            this.pButtons.Controls.Add(this.bDuration);
            this.pButtons.Controls.Add(this.bClear);
            this.pButtons.Controls.Add(this.bDelete);
            this.pButtons.Controls.Add(this.bPPRCh_RP);
            this.pButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pButtons.Location = new System.Drawing.Point(0, 302);
            this.pButtons.Margin = new System.Windows.Forms.Padding(0);
            this.pButtons.Name = "pButtons";
            this.pButtons.Size = new System.Drawing.Size(566, 30);
            this.pButtons.TabIndex = 7;
            // 
            // bClear
            // 
            this.bClear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bClear.ForeColor = System.Drawing.Color.Red;
            this.bClear.Location = new System.Drawing.Point(4, 3);
            this.bClear.Name = "bClear";
            this.bClear.Size = new System.Drawing.Size(75, 23);
            this.bClear.TabIndex = 3;
            this.bClear.Text = "Очистить";
            this.bClear.UseVisualStyleBackColor = true;
            this.bClear.Click += new System.EventHandler(this.bClear_Click);
            // 
            // bDelete
            // 
            this.bDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bDelete.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.bDelete.Location = new System.Drawing.Point(80, 3);
            this.bDelete.Name = "bDelete";
            this.bDelete.Size = new System.Drawing.Size(75, 23);
            this.bDelete.TabIndex = 4;
            this.bDelete.Text = "Удалить";
            this.bDelete.UseVisualStyleBackColor = true;
            this.bDelete.Click += new System.EventHandler(this.bDelete_Click);
            // 
            // dgvIRI_PPRCh1
            // 
            this.dgvIRI_PPRCh1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvIRI_PPRCh1.Location = new System.Drawing.Point(3, 3);
            this.dgvIRI_PPRCh1.Name = "dgvIRI_PPRCh1";
            this.dgvIRI_PPRCh1.Size = new System.Drawing.Size(560, 145);
            this.dgvIRI_PPRCh1.TabIndex = 0;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 55F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Controls.Add(this.dgvIRI_PPRCh2, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.dgvIRI_PPRCh3, 1, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 154);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(563, 145);
            this.tableLayoutPanel2.TabIndex = 4;
            // 
            // dgvIRI_PPRCh2
            // 
            this.dgvIRI_PPRCh2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvIRI_PPRCh2.Location = new System.Drawing.Point(3, 3);
            this.dgvIRI_PPRCh2.Name = "dgvIRI_PPRCh2";
            this.dgvIRI_PPRCh2.Size = new System.Drawing.Size(303, 139);
            this.dgvIRI_PPRCh2.TabIndex = 1;
            // 
            // dgvIRI_PPRCh3
            // 
            this.dgvIRI_PPRCh3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvIRI_PPRCh3.Location = new System.Drawing.Point(312, 3);
            this.dgvIRI_PPRCh3.Name = "dgvIRI_PPRCh3";
            this.dgvIRI_PPRCh3.Size = new System.Drawing.Size(248, 139);
            this.dgvIRI_PPRCh3.TabIndex = 2;
            // 
            // bDuration
            // 
            this.bDuration.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bDuration.ForeColor = System.Drawing.Color.DarkCyan;
            this.bDuration.Location = new System.Drawing.Point(317, 3);
            this.bDuration.Name = "bDuration";
            this.bDuration.Size = new System.Drawing.Size(86, 23);
            this.bDuration.TabIndex = 6;
            this.bDuration.Text = "Изм. длит.";
            this.bDuration.UseVisualStyleBackColor = true;
            this.bDuration.Click += new System.EventHandler(this.bDuration_Click);
            // 
            // table_IRI_PPRCh
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "table_IRI_PPRCh";
            this.Size = new System.Drawing.Size(566, 332);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.pButtons.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        public MyDataGridView.myDataGridView dgvIRI_PPRCh1;
        public MyDataGridView.myDataGridView dgvIRI_PPRCh2;
        public MyDataGridView.myDataGridView dgvIRI_PPRCh3;
        public System.Windows.Forms.Button bPPRCh_RP;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        public System.Windows.Forms.Button bClear;
        public System.Windows.Forms.Button bDelete;
        private System.Windows.Forms.Panel pButtons;
        private System.Windows.Forms.Button bDuration;

    }
}
