﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using MyDataGridView;
using System.Data.SQLite;
using VariableDynamic;
using USR_DLL;
using System.Globalization;
using VariableStatic;

namespace Table_IRI_FRCh_RP
{
    public partial class table_IRI_FRCh_RP: UserControl
    {
        InitParams initParams;
        FuncDB_IRI_FRCh_RP funcDB_IRI_FRCh_RP;
        FuncDGV_IRI_FRCh_RP funcDGV_IRI_FRCh_RP;
        FuncVW_IRI_FRCh_RP funcVW_IRI_FRCh_RP;
        
        FunctionsDB functionsDB;
        FunctionsDGV functionsDGV;
        DecoderParams decoderParams;

        CurVal_IRI_FRCh_RP curVal_IRI_FRCh_RP;

        Functions functions;
        FunctionsTranslate functionsTranslate;

        VariableWork variableWork;
        VariableCommon variableCommon;
        FunctionsUser functionsUser;
        TSupprFWS SupprFWS;

        InfoMessages infoMessage;

        const int iFreqIridium = 16160000;
        const int iFreqInmarsat = 15250000;

        public table_IRI_FRCh_RP()
        {
            InitializeComponent();

            initParams = new InitParams();
            functionsDB = new FunctionsDB();
            funcDB_IRI_FRCh_RP = new FuncDB_IRI_FRCh_RP();
            funcDGV_IRI_FRCh_RP = new FuncDGV_IRI_FRCh_RP();
            funcVW_IRI_FRCh_RP = new FuncVW_IRI_FRCh_RP();
            functionsDGV = new FunctionsDGV();
            decoderParams = new DecoderParams();

            curVal_IRI_FRCh_RP = new CurVal_IRI_FRCh_RP();

            functions = new Functions();
            functionsTranslate = new FunctionsTranslate();

            variableWork = new VariableWork();
            variableCommon = new VariableCommon();
            functionsUser = new FunctionsUser();
            SupprFWS = new TSupprFWS();

            infoMessage = new InfoMessages();

            InitTableIRI_FRCh_RP();

            SetViewWindowForRole();

            SetViewWindowForTypeStation();

            //установить значение мин., макс. для поля
            initParams.Init3000_6000_FRCh_RP(variableCommon.TypeStation, nudFreq, gbIRID_INM);

            VariableCommon.OnChangeCommonLanguage += new VariableCommon.ChangeCommonEventHandler(VariableCommon_OnChangeCommonLanguage);
            VariableCommon.OnChangeCommonRole += new VariableCommon.ChangeCommonEventHandler(VariableCommon_OnChangeCommonRole);

            // Событие возникает при изменении значений таблицы ИРИ ФРЧ на РП (ведущая)
            VariableWork.OnChangeSupprFWS_Own += new VariableWork.ChangeVariableEventHandler(VariableWork_OnChangeSupprFWS_Own);
            // Событие возникает при изменении значений таблицы ИРИ ФРЧ на РП (ведомая)
            VariableWork.OnChangeSupprFWS_Linked += new VariableWork.ChangeVariableEventHandler(VariableWork_OnChangeSupprFWS_Linked);

            VariableWork.aWPtoBearingDSPprotocolNew.RadioJamStateUpdate += new AWPtoBearingDSPprotocolNew.RadioJamStateUpdateEventHandler(aWPtoBearingDSPprotocolNew_RadioJamStateUpdate);

            VariableWork.OnChangeRegime += new VariableWork.ChangeRegimeEventHandler(VariableWork_OnChangeRegime);

            //ChangeControlLanguage(variableCommon.Language);

            //initParams.InitModulation(cmbTypeModulation);
            //initParams.InitDuration(cmbDuration);
            //initParams.SetDeviation(cmbTypeModulation.SelectedIndex, 0, cmbDeviation);
            //initParams.SetManipulation(cmbTypeModulation.SelectedIndex, 6, cmbManipulation);
           
            ////////////////////////////////////////OTL
            //funcDB_IRI_FRCh_RP.LoadTableIRI_FRCh_RPFromDBToVW(dgvIRI_FRCh_RP1.dgv, NameTable.IRI_FRCh_RP, 1);
            //funcDB_IRI_FRCh_RP.LoadTableIRI_FRCh_RPFromDBToVW(dgvIRI_FRCh_RP2.dgv, NameTable.IRI_FRCh_RP, 2);
            ////////////////////////////////////////OTL

            // Событие возникает при выделении строки в dgv
            dgvIRI_FRCh_RP1.dgv.SelectionChanged += new EventHandler(dgv_SelectionChanged);
            dgvIRI_FRCh_RP2.dgv.SelectionChanged += new EventHandler(dgv_SelectionChanged); 
        }

        void VariableCommon_OnChangeCommonRole()
        {
            SetViewWindowForRole();
        }

        /// <summary>
        /// Установить вид таблицы в зависимости от роли (автономная, ведущая, ведомая)
        /// </summary>
        public void SetViewWindowForRole()
        {
            try
            {
                if (variableCommon.Role == 2)
                {
                    grbRadioButtons.Visible = false;
                    chbShowMated.Visible = false;

                    tableLayoutPanel2.RowStyles[1] = new RowStyle(SizeType.Absolute, 0);

                    dgvIRI_FRCh_RP1.Visible = true;
                    dgvIRI_FRCh_RP2.Visible = false;
                }
                else if (variableCommon.Role == 0 || variableCommon.Role == 1)
                {
                    grbRadioButtons.Visible = true;
                    chbShowMated.Visible = true;

                    if (!chbShowMated.Checked)
                    {
                        tableLayoutPanel2.RowStyles[1] = new RowStyle(SizeType.Absolute, 0);
                    }
                    else
                    {
                        tableLayoutPanel2.RowStyles[1] = new RowStyle(SizeType.Percent, 100);
                    }

                    //tableLayoutPanel2.RowStyles[1] = new RowStyle(SizeType.Percent, 100);

                    dgvIRI_FRCh_RP1.Visible = true;
                    dgvIRI_FRCh_RP2.Visible = true;
                }
            }
            catch { }
        }

        /// <summary>
        /// Установить вид таблицы в зависимости от типа станции (0 : Р-934УМ2; 1 : Гроза-6; 2 : Р-934УМ2 4,5)
        /// </summary>
        public void SetViewWindowForTypeStation()
        {
            if (variableCommon.TypeStation == 0 || variableCommon.TypeStation == 1)
                gbIRID_INM.Visible = false;
            else gbIRID_INM.Visible = true;
        }

        public void InitParamTranslateFRChRP()
        {
            ChangeControlLanguage(variableCommon.Language);

            initParams.InitModulation(cmbTypeModulation);
            initParams.InitDuration(cmbDuration);
            initParams.SetDeviation(cmbTypeModulation.SelectedIndex, 0, cmbDeviation);

            if (variableCommon.TypeStation == 2)
                initParams.SetManipulation(cmbTypeModulation.SelectedIndex, 7, cmbManipulation, variableCommon.TypeStation);
            else
                initParams.SetManipulation(cmbTypeModulation.SelectedIndex, 6, cmbManipulation, variableCommon.TypeStation);
        }

        private void VariableCommon_OnChangeCommonLanguage()
        {
            VariableCommon variableCommon = new VariableCommon();
            switch (variableCommon.Language)
            {
                case 0:
                    functionsTranslate.SetLanguage("rus");
                    ChangeLanguage();
                    break;

                case 1:
                    functionsTranslate.SetLanguage("eng");
                    ChangeLanguage();
                    break;

                case 2:
                    functionsTranslate.SetLanguage("az");
                    ChangeLanguage();
                    break;

                default:
                    functionsTranslate.SetLanguage("rus");
                    ChangeLanguage();
                    break;
            }
        }

        /// <summary>
        /// Для инициализации языка при запуске приложения 
        /// </summary>
        /// <param name="bLanguage"></param>
        public void ChangeControlLanguage(byte bLanguage)
        {

            switch (bLanguage)
            {
                case 0:
                    functionsTranslate.SetLanguage("rus");
                    ChangeLanguage();
                    break;

                case 1:
                    functionsTranslate.SetLanguage("eng");
                    ChangeLanguage();
                    break;

                case 2:
                    functionsTranslate.SetLanguage("az");
                    ChangeLanguage();
                    break;

                default:
                    functionsTranslate.SetLanguage("rus");
                    ChangeLanguage();
                    break;
            }
        }

        /// <summary>
        /// Изменение языка интерфейса
        /// </summary>
        private void ChangeLanguage()
        {
            functionsTranslate.RenameMessages(functionsTranslate.Dictionary);
            functionsTranslate.RenameMeaning(functionsTranslate.Dictionary);
            functionsTranslate.RenameParams(functionsTranslate.Dictionary);


            functionsTranslate.RenameButtons(functionsTranslate.Dictionary, pButtons);
            functionsTranslate.RenameLabels(functionsTranslate.Dictionary, pParamSignal);
            functionsTranslate.RenameLabels(functionsTranslate.Dictionary, pIndications);
            functionsTranslate.RenameLabels(functionsTranslate.Dictionary, grbParamHind);
            functionsTranslate.RenameRadioButtons(functionsTranslate.Dictionary, grbRadioButtons);
            functionsTranslate.RenameCheckBoxes(functionsTranslate.Dictionary, pParam);
            functionsTranslate.RenameGroupBoxes(functionsTranslate.Dictionary, pParam);
            functionsTranslate.RenameFieldsTables(functionsTranslate.Dictionary, dgvIRI_FRCh_RP1.dgv);
            functionsTranslate.RenameFieldsTables(functionsTranslate.Dictionary, dgvIRI_FRCh_RP2.dgv);

            initParams.InitModulation(cmbTypeModulation);
            VariableWork_OnChangeSupprFWS_Own();
        }

        void VariableWork_OnChangeRegime()
        {
            try
            {
                for (int i = 0; i < dgvIRI_FRCh_RP1.dgv.RowCount; i++)
                {
                    dgvIRI_FRCh_RP1.dgv.Rows[i].Cells[7].Value = Properties.Resources.empty;
                    dgvIRI_FRCh_RP1.dgv.Rows[i].Cells[8].Value = Properties.Resources.empty;
                    dgvIRI_FRCh_RP1.dgv.Rows[i].Cells[9].Value = Properties.Resources.empty;
                }

                for (int i = 0; i < dgvIRI_FRCh_RP2.dgv.RowCount; i++)
                {
                    dgvIRI_FRCh_RP2.dgv.Rows[i].Cells[7].Value = Properties.Resources.empty;
                    dgvIRI_FRCh_RP2.dgv.Rows[i].Cells[8].Value = Properties.Resources.empty;
                    dgvIRI_FRCh_RP2.dgv.Rows[i].Cells[9].Value = Properties.Resources.empty;
                }
            }
            catch { }
        }

        public int FoundIndRowDGV(DataGridView dgv, Protocols.RadioJamStateUpdateEvent answer, int iNumAnswer)
        {
            int iNum = 0;

            try
            {
                for (int i = 0; i < dgv.Rows.Count; i++)
                {
                    if (dgv[0, i].Value.ToString() == Convert.ToString(answer.TargetStates[iNumAnswer].Id))
                    {
                        iNum = dgv[0, i].RowIndex;
                        return iNum;
                    }
                }
            }
            catch { }

            return -1;
        }

        /// <summary>
        /// ControlState -- КР
        /// EmitState -- Излучение
        /// RadioJamState -- РП
        /// </summary>
        /// <param name="answer"></param>
        private void aWPtoBearingDSPprotocolNew_RadioJamStateUpdate(Protocols.RadioJamStateUpdateEvent answer)
        {
            byte bControlState;
            byte bRadioJamState;
            byte bEmitState;

            if (variableWork.Regime == 3 || variableWork.Regime == 4 || variableWork.Regime == 5)
            {
                if (answer.TargetStates.Length > 0)
                {  
                    for (int i = 0; i < answer.TargetStates.Length; i++)
                    {
                        int ind = FoundIndRowDGV(dgvIRI_FRCh_RP1.dgv, answer, i);
                        if (ind != -1)
                        {
                            dgvIRI_FRCh_RP1.dgv.Rows[ind].Cells[1].Value = functions.FreqToStr(answer.TargetStates[i].Frequency);

                            bControlState = answer.TargetStates[i].ControlState;
                            switch (bControlState)
                            {
                                case 0: // нет сигнала (ничего)
                                    dgvIRI_FRCh_RP1.dgv.Rows[ind].Cells[7].Value = Properties.Resources.empty;
                                    break;

                                case 1: // есть сигнал (красный)
                                    dgvIRI_FRCh_RP1.dgv.Rows[ind].Cells[7].Value = Properties.Resources.red;
                                    break;

                                case 2: // давно есть сигнал (зеленый)
                                    dgvIRI_FRCh_RP1.dgv.Rows[ind].Cells[7].Value = Properties.Resources.green;
                                    break;

                                case 3: // давно нет сигнала (желтый)
                                    dgvIRI_FRCh_RP1.dgv.Rows[ind].Cells[7].Value = Properties.Resources.yellow;
                                    break;
                            }

                            bRadioJamState = answer.TargetStates[i].RadioJamState;
                            switch (bRadioJamState)
                            {
                                case 0: // нет подавления (ничего)
                                    dgvIRI_FRCh_RP1.dgv.Rows[ind].Cells[8].Value = Properties.Resources.empty;
                                    break;

                                case 1: // есть подавление (красный)
                                    dgvIRI_FRCh_RP1.dgv.Rows[ind].Cells[8].Value = Properties.Resources.red;
                                    break;

                                case 2: // давно есть подавление (зеленый)
                                    dgvIRI_FRCh_RP1.dgv.Rows[ind].Cells[8].Value = Properties.Resources.green;
                                    break;
                            }

                            bEmitState = answer.TargetStates[i].EmitState;
                            switch (bEmitState)
                            {
                                case 0: // нет излучения (ничего)
                                    dgvIRI_FRCh_RP1.dgv.Rows[ind].Cells[9].Value = Properties.Resources.empty;
                                    break;

                                case 1: // есть излучение (синий)
                                    dgvIRI_FRCh_RP1.dgv.Rows[ind].Cells[9].Value = Properties.Resources.blue;
                                    break;
                            }
                        }
                    }
                }
            }
        }

        void VariableWork_OnChangeSupprFWS_Linked()
        {
            if (variableWork.SupprFWS_Linked != null)
            {
                USR_DLL.TSupprFWS[] tempSupprFWS = variableWork.SupprFWS_Linked;
               
                //IEnumerable<TSupprFWS> sortPriority = tempSupprFWS.OrderByDescending(x => x.bPrior);// OrderBy(x => x.bPrior); // сортировка по приоритету
                //IEnumerable<TSupprFWS> sortLetter = sortPriority.OrderBy(x => x.bLetter);      // сортировка по литере

                IEnumerable<TSupprFWS> sortFreq = tempSupprFWS.OrderBy(x => x.iFreq);   // сортировка по частоте
                IEnumerable<TSupprFWS> sortPriority = sortFreq.OrderByDescending(x => x.bPrior); // сортировка по приоритету
                IEnumerable<TSupprFWS> sortLetter = sortPriority.OrderBy(x => x.bLetter);            // сортировка по литере

                tempSupprFWS = sortLetter.ToArray();

                int len = variableWork.SupprFWS_Linked.Length;
               
                if (infoMessage.MessageErr == "")
                {
                    if (functionsDB.DeleteAllRecordsDB(NameTable.IRI_FRCh_RP, Table.Linked))
                    {
                        if (dgvIRI_FRCh_RP2.dgv.InvokeRequired)
                        {
                            Invoke((MethodInvoker)(() =>
                            {
                                functionsDGV.DeleteAllRecordsDGV(dgvIRI_FRCh_RP2.dgv);
                                dgvIRI_FRCh_RP2.SetInitialNumberOfRows(dgvIRI_FRCh_RP2.dgv, dgvIRI_FRCh_RP2.tableIRI_FRCh_RP, NameTable.IRI_FRCh_RP);
                            }));
                        }
                        else
                        {
                            functionsDGV.DeleteAllRecordsDGV(dgvIRI_FRCh_RP2.dgv);
                            dgvIRI_FRCh_RP2.SetInitialNumberOfRows(dgvIRI_FRCh_RP2.dgv, dgvIRI_FRCh_RP2.tableIRI_FRCh_RP, NameTable.IRI_FRCh_RP);
                        }

                        for (int i = 0; i < len; i++)
                        {
                            SupprFWS = new TSupprFWS();

                            SupprFWS.iID = tempSupprFWS[i].iID;
                            SupprFWS.iFreq = tempSupprFWS[i].iFreq;
                            SupprFWS.sBearing = tempSupprFWS[i].sBearing;
                            SupprFWS.bLetter = tempSupprFWS[i].bLetter;
                            SupprFWS.sLevel = tempSupprFWS[i].sLevel;
                            SupprFWS.bPrior = tempSupprFWS[i].bPrior;
                            SupprFWS.bModulation = tempSupprFWS[i].bModulation;
                            SupprFWS.bDeviation = tempSupprFWS[i].bDeviation;
                            SupprFWS.bManipulation = tempSupprFWS[i].bManipulation;
                            SupprFWS.bDuration = tempSupprFWS[i].bDuration;

                            if (funcDB_IRI_FRCh_RP.AddRecord_IRI_FRCh_RPToDB(NameTable.IRI_FRCh_RP, SupprFWS, Table.Linked))
                            {
                                if (dgvIRI_FRCh_RP2.dgv.InvokeRequired)
                                {
                                    Invoke((MethodInvoker)(() => funcDGV_IRI_FRCh_RP.AddRecord_IRI_FRCh_RPToDGV(dgvIRI_FRCh_RP2.dgv, NameTable.IRI_FRCh_RP, SupprFWS, Table.Linked)));
                                }
                                else
                                {
                                    funcDGV_IRI_FRCh_RP.AddRecord_IRI_FRCh_RPToDGV(dgvIRI_FRCh_RP2.dgv, NameTable.IRI_FRCh_RP, SupprFWS, Table.Linked);
                                }
                            }
                        }
                    }
                }
                else
                {
                    MessageBox.Show(infoMessage.MessageErr, SMessageError.mesErr, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

            }

            dgv_SelectionChanged(this, null);
        }

        void VariableWork_OnChangeSupprFWS_Own()
        {
            if (variableWork.SupprFWS_Own != null)
            {
                //убрать значения в tempSupprFWS из 8 литеры для добавления в таблицу (если установлена галочка первый раз)------
                List<TSupprFWS> lSupprFWS = variableWork.SupprFWS_Own.ToList();

                if (chbIRIDIUM.Checked)
                {
                    int ind1 = lSupprFWS.FindIndex(x => x.iFreq == iFreqIridium);
                    if (ind1 != -1) lSupprFWS.RemoveAt(ind1);
                }

                if (chbINMARSAT.Checked)
                {
                    int ind2 = lSupprFWS.FindIndex(x => x.iFreq == iFreqInmarsat);
                    if (ind2 != -1) lSupprFWS.RemoveAt(ind2);
                }
              
                USR_DLL.TSupprFWS[] tempSupprFWS = lSupprFWS.ToArray();
                //-----------------------------------------------------------------------------------------------------

                //IEnumerable<TSupprFWS> sortPriority = tempSupprFWS.OrderByDescending(x => x.bPrior); // сортировка по приоритету
                //IEnumerable<TSupprFWS> sortLetter = sortPriority.OrderBy(x => x.bLetter);            // сортировка по литере

                IEnumerable<TSupprFWS> sortFreq = tempSupprFWS.OrderBy(x => x.iFreq);   // сортировка по частоте
                IEnumerable<TSupprFWS> sortPriority = sortFreq.OrderByDescending(x => x.bPrior); // сортировка по приоритету
                IEnumerable<TSupprFWS> sortLetter = sortPriority.OrderBy(x => x.bLetter);            // сортировка по литере

                tempSupprFWS = sortLetter.ToArray();

                int len = tempSupprFWS.Length;
                //int len = variableWork.SupprFWS_Own.Length;
                
                if (infoMessage.MessageErr == "")
                {
                    if (functionsDB.DeleteAllRecordsDB(NameTable.IRI_FRCh_RP, Table.Own))
                    { 
                        if (dgvIRI_FRCh_RP1.dgv.InvokeRequired)
                        {
                            Invoke((MethodInvoker)(() =>
                            {
                                functionsDGV.DeleteAllRecordsDGV(dgvIRI_FRCh_RP1.dgv);
                                dgvIRI_FRCh_RP1.SetInitialNumberOfRows(dgvIRI_FRCh_RP1.dgv, dgvIRI_FRCh_RP1.tableIRI_FRCh_RP, NameTable.IRI_FRCh_RP);
                            }));
                        }
                        else
                        {
                            functionsDGV.DeleteAllRecordsDGV(dgvIRI_FRCh_RP1.dgv);
                            dgvIRI_FRCh_RP1.SetInitialNumberOfRows(dgvIRI_FRCh_RP1.dgv, dgvIRI_FRCh_RP1.tableIRI_FRCh_RP, NameTable.IRI_FRCh_RP);
                        }

                        for (int i = 0; i < len; i++)
                        {
                            SupprFWS = new TSupprFWS();

                            SupprFWS.iID = tempSupprFWS[i].iID;
                            SupprFWS.iFreq = tempSupprFWS[i].iFreq;
                            SupprFWS.sBearing = tempSupprFWS[i].sBearing;
                            SupprFWS.bLetter = tempSupprFWS[i].bLetter;
                            SupprFWS.sLevel = tempSupprFWS[i].sLevel;
                            SupprFWS.bPrior = tempSupprFWS[i].bPrior;
                            SupprFWS.bModulation = tempSupprFWS[i].bModulation;
                            SupprFWS.bDeviation = tempSupprFWS[i].bDeviation;
                            SupprFWS.bManipulation = tempSupprFWS[i].bManipulation;
                            SupprFWS.bDuration = tempSupprFWS[i].bDuration;

                            if (funcDB_IRI_FRCh_RP.AddRecord_IRI_FRCh_RPToDB(NameTable.IRI_FRCh_RP, SupprFWS, Table.Own))
                            {
                                if (dgvIRI_FRCh_RP1.dgv.InvokeRequired)
                                {
                                    Invoke((MethodInvoker)(() => funcDGV_IRI_FRCh_RP.AddRecord_IRI_FRCh_RPToDGV(dgvIRI_FRCh_RP1.dgv, NameTable.IRI_FRCh_RP, SupprFWS, Table.Own)));
                                }
                                else
                                {
                                    funcDGV_IRI_FRCh_RP.AddRecord_IRI_FRCh_RPToDGV(dgvIRI_FRCh_RP1.dgv, NameTable.IRI_FRCh_RP, SupprFWS, Table.Own);
                                }
                            }
                        }
                    }
                }
                else
                {
                    MessageBox.Show(infoMessage.MessageErr, SMessageError.mesErr, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }

            dgv_SelectionChanged(this, null);
        }

        public void InitTableIRI_FRCh_RP()
        {
            dgvIRI_FRCh_RP1.InitTableDB(dgvIRI_FRCh_RP1.dgv, NameTable.IRI_FRCh_RP);
            dgvIRI_FRCh_RP2.InitTableDB(dgvIRI_FRCh_RP2.dgv, NameTable.IRI_FRCh_RP);
        }

        /// <summary>
        /// Событие возникает при выделении строки в dgv
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_SelectionChanged(object sender, EventArgs e)
        {
            if (rbOwn.Checked)
            {
                if (dgvIRI_FRCh_RP1.dgv.SelectedRows.Count > 0)
                {
                    if (dgvIRI_FRCh_RP1.dgv.Rows[dgvIRI_FRCh_RP1.dgv.SelectedRows[0].Index].Cells[0].Value != null)
                    {
                        try
                        {
                            SetParams(dgvIRI_FRCh_RP1.dgv);
                        }
                        catch (System.Exception)
                        {
                            return;
                        }
                    }
                }
            }
            else if (rbMated.Checked)
            {
                if (dgvIRI_FRCh_RP2.dgv.SelectedRows.Count > 0)
                {
                    if (dgvIRI_FRCh_RP2.dgv.Rows[dgvIRI_FRCh_RP2.dgv.SelectedRows[0].Index].Cells[0].Value != null)
                    {
                        try
                        {
                            SetParams(dgvIRI_FRCh_RP2.dgv);
                        }
                        catch (System.Exception)
                        {
                            return;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Установка параметров в элементы при выделении строки в dgv
        /// </summary>
        /// <param name="myDGV"></param>
        private void SetParams(DataGridView dgv)
        {
            try
            {
                // ID строки
                curVal_IRI_FRCh_RP.IdDGV = Convert.ToInt32(dgv.Rows[dgv.SelectedRows[0].Index].Cells[0].Value);
            }
            catch { }

            try
            {
                // значение частоты
                decimal dFreq = Convert.ToDecimal(dgv.Rows[dgv.SelectedRows[0].Index].Cells[1].Value, Functions.format);
                string sFreq1 = functions.FreqToStr((int)(dFreq * 10));
                string sFreq2 = functions.StrToFreq(sFreq1);

                nudFreq.Value = Convert.ToDecimal(sFreq2, Functions.format);
                curVal_IRI_FRCh_RP.FreqDGV = Convert.ToInt32(Convert.ToDecimal(dFreq, Functions.format) * 10);
            }
            catch { }

            try
            {
                // значение пеленга
                decimal dQ = Convert.ToDecimal(dgv.Rows[dgv.SelectedRows[0].Index].Cells[2].Value, Functions.format);
                string sQ = dQ.ToString();
                curVal_IRI_FRCh_RP.BearingDGV = Convert.ToInt32(dQ * 10);
                //curVal_IRI_FRCh_RP.BearingDGV = Convert.ToInt32(dgv.Rows[dgv.SelectedRows[0].Index].Cells[2].Value);
                nudBearing.Value = dQ;
            }
            catch 
            {
                curVal_IRI_FRCh_RP.BearingDGV = -1;
                nudBearing.Value = 0;
            }

            try
            {
                // параметры помехи
                curVal_IRI_FRCh_RP.ParamsNoiseDGV = Convert.ToString(dgv.Rows[dgv.SelectedRows[0].Index].Cells[4].Value);
            }
            catch { }

            try
            {
                // значение порогового уровня
                curVal_IRI_FRCh_RP.LevelDGV = Convert.ToInt32(dgv.Rows[dgv.SelectedRows[0].Index].Cells[5].Value);
                nudLevel.Value = curVal_IRI_FRCh_RP.LevelDGV;
            }
            catch 
            {
                nudLevel.Value = -60;
            }

            try
            {
                // значение приоритета
                curVal_IRI_FRCh_RP.PriorDGV = Convert.ToInt32(dgv.Rows[dgv.SelectedRows[0].Index].Cells[6].Value);
                nudPrior.Value = curVal_IRI_FRCh_RP.PriorDGV;
            }
            catch { }

            try
            {
                // значение индекса литеры
                curVal_IRI_FRCh_RP.LetterDGV = Convert.ToInt32(dgv.Rows[dgv.SelectedRows[0].Index].Cells[3].Value);// -1;
            }
            catch { }

            try
            {
                // режим (ведущая, ведомая)
                curVal_IRI_FRCh_RP.TableDGV = Convert.ToByte(dgv.Rows[dgv.SelectedRows[0].Index].Cells[14].Value);
            }
            catch { }


            try
            {
                // значение модуляции
                curVal_IRI_FRCh_RP.ModulationDGV = Convert.ToByte(dgv.Rows[dgv.SelectedRows[0].Index].Cells[10].Value);
                cmbTypeModulation.SelectedIndex = Convert.ToInt32(curVal_IRI_FRCh_RP.ModulationDGV - 1);

            }
            catch { }

            try
            {
                // значение девиации
                curVal_IRI_FRCh_RP.DeviationDGV = Convert.ToByte(dgv.Rows[dgv.SelectedRows[0].Index].Cells[11].Value);
                initParams.SetDeviation(cmbTypeModulation.SelectedIndex, curVal_IRI_FRCh_RP.DeviationDGV - 1, cmbDeviation);

            }
            catch { }

            try
            {
                // значение манипуляции   
                curVal_IRI_FRCh_RP.ManipulationDGV = Convert.ToByte(dgv.Rows[dgv.SelectedRows[0].Index].Cells[12].Value);
                initParams.SetManipulation(cmbTypeModulation.SelectedIndex, curVal_IRI_FRCh_RP.ManipulationDGV - 1, cmbManipulation, variableCommon.TypeStation);
                //initParams.SetManipulation(cmbTypeModulation.SelectedIndex, curVal_IRI_FRCh_RP.ManipulationDGV, cmbManipulation, variableCommon.TypeStation);
            }
            catch { }

            try
            {
                // значение девиации
                curVal_IRI_FRCh_RP.DurationDGV = Convert.ToByte(dgv.Rows[dgv.SelectedRows[0].Index].Cells[13].Value);
                initParams.SetDuration(curVal_IRI_FRCh_RP.DurationDGV - 1, cmbDuration);
            }
            catch { }
        }

        /// <summary>
        /// загрузка данных из таблицы БД в variableWork
        /// </summary>
        public void LoadTableIRI_FRCh_RPfromDB()
        {
            funcDB_IRI_FRCh_RP.LoadTableIRI_FRCh_RPFromDBToVW(NameTable.IRI_FRCh_RP, Table.Own);
            funcDB_IRI_FRCh_RP.LoadTableIRI_FRCh_RPFromDBToVW(NameTable.IRI_FRCh_RP, Table.Linked);

            funcDB_IRI_FRCh_RP.FindMinIDIRI_FRCh_RP(NameTable.IRI_FRCh_RP);
        }

        private void chbShowMated_CheckedChanged(object sender, EventArgs e)
        {
            if (!chbShowMated.Checked)
            {
                tableLayoutPanel2.RowStyles[1] = new RowStyle(SizeType.Absolute, 0);
            }
            else
            {
                tableLayoutPanel2.RowStyles[1] = new RowStyle(SizeType.Percent, 100);
            }
        }

        private void cmbTypeModulation_SelectedIndexChanged(object sender, EventArgs e)
        {
            initParams.SetDeviation(cmbTypeModulation.SelectedIndex, 0, cmbDeviation);

            if (cmbTypeModulation.SelectedIndex > 3 & cmbTypeModulation.SelectedIndex < 7)
            {
                if (variableCommon.TypeStation == 2)
                    initParams.SetManipulation(cmbTypeModulation.SelectedIndex, 7, cmbManipulation, variableCommon.TypeStation);
                else
                    initParams.SetManipulation(cmbTypeModulation.SelectedIndex, 6, cmbManipulation, variableCommon.TypeStation);
            }
            else
                initParams.SetManipulation(cmbTypeModulation.SelectedIndex, 0, cmbManipulation, variableCommon.TypeStation);
        }

        private void bAdd_Click(object sender, EventArgs e)
        {
            SupprFWS = TSupprFWS.CreateID();

            // считать частоту
            try
            {
                SupprFWS.iFreq = Convert.ToInt32(nudFreq.Value * 10);

                if (chbINMARSAT.Checked || chbIRIDIUM.Checked)
                {
                    // проверить частоту на принадлежность литере 8
                    if (SupprFWS.iFreq > RangesLetters.FREQ_START_LETTER_8 && SupprFWS.iFreq < RangesLetters.FREQ_START_LETTER_9)
                    {
                        MessageBox.Show("В диапазоне литеры 8 осуществляется подавление IRIDIUM (INMARSAT)!", SMessages.mesMessage, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                }
            }
            catch (System.Exception)
            {
                MessageBox.Show(SMessageError.mesErrIncorrSetFreq, SMessageError.mesErr, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.ServiceNotification);
                return;
            }

            // проверить частоту на принадлежность частотному диапазону 
            if (SupprFWS.iFreq < RangesLetters.FREQ_START_LETTER_1 | SupprFWS.iFreq > RangesLetters.FREQ_STOP_LETTER_9)
            {
                MessageBox.Show(SMessageError.mesErrFreqWithoutRange, SMessageError.mesErr, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.ServiceNotification);
                return;
            }

            // определить литеру
            if (SupprFWS.iFreq >= RangesLetters.FREQ_START_LETTER_1 & SupprFWS.iFreq < RangesLetters.FREQ_START_LETTER_2)
                SupprFWS.bLetter = 1;
            if (SupprFWS.iFreq >= RangesLetters.FREQ_START_LETTER_2 & SupprFWS.iFreq < RangesLetters.FREQ_START_LETTER_3)
                SupprFWS.bLetter = 2;
            if (SupprFWS.iFreq >= RangesLetters.FREQ_START_LETTER_3 & SupprFWS.iFreq < RangesLetters.FREQ_START_LETTER_4)
                SupprFWS.bLetter = 3;
            if (SupprFWS.iFreq >= RangesLetters.FREQ_START_LETTER_4 & SupprFWS.iFreq < RangesLetters.FREQ_START_LETTER_5)
                SupprFWS.bLetter = 4;
            if (SupprFWS.iFreq >= RangesLetters.FREQ_START_LETTER_5 & SupprFWS.iFreq < RangesLetters.FREQ_START_LETTER_6)
                SupprFWS.bLetter = 5;
            if (SupprFWS.iFreq >= RangesLetters.FREQ_START_LETTER_6 & SupprFWS.iFreq < RangesLetters.FREQ_START_LETTER_7)
                SupprFWS.bLetter = 6;
            if (variableCommon.TypeStation == 0)
            {
                if (SupprFWS.iFreq >= RangesLetters.FREQ_START_LETTER_7 & SupprFWS.iFreq < RangesLetters.FREQ_STOP_LETTER_7)
                    SupprFWS.bLetter = 7;
            }
            else if (variableCommon.TypeStation == 1 || variableCommon.TypeStation == 2)
            {
                if (SupprFWS.iFreq >= RangesLetters.FREQ_START_LETTER_7 & SupprFWS.iFreq < RangesLetters.FREQ_START_LETTER_8)
                    SupprFWS.bLetter = 7;
                if (SupprFWS.iFreq >= RangesLetters.FREQ_START_LETTER_8 & SupprFWS.iFreq < RangesLetters.FREQ_START_LETTER_9)
                    SupprFWS.bLetter = 8;
                if (SupprFWS.iFreq >= RangesLetters.FREQ_START_LETTER_9 & SupprFWS.iFreq <= RangesLetters.FREQ_STOP_LETTER_9)
                    SupprFWS.bLetter = 9;
            }

            // считать пеленг
            try
            {
                SupprFWS.sBearing = Convert.ToInt16(Convert.ToDecimal(nudBearing.Value, Functions.format) * 10);
                //SupprFWS.sBearing = Convert.ToInt16(nudBearing.Value);
            }
            catch (System.Exception)
            {
                MessageBox.Show(SMessageError.mesErrIncorrSetPel, SMessageError.mesErr, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.ServiceNotification);
                return;
            }

            // проверить пеленг на принадлежность диапазону 
            if (SupprFWS.sBearing < 0 | SupprFWS.sBearing > 3599)
            {
                MessageBox.Show(SMessageError.mesErrPelWithoutRange, SMessageError.mesErr, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.ServiceNotification);
                return;
            }

            // считать приоритет
            try
            {
                SupprFWS.bPrior = Convert.ToByte(nudPrior.Value);
            }
            catch (System.Exception)
            {
                MessageBox.Show(SMessageError.mesErrIncorrSetPrior, SMessageError.mesErr, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.ServiceNotification);
                return;
            }

            // считать порог
            try
            {
                SupprFWS.sLevel = Convert.ToInt16(nudLevel.Value);
            }
            catch (System.Exception)
            {
                MessageBox.Show(SMessageError.mesErrIncorrSetLevel, SMessageError.mesErr, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.ServiceNotification);
                return;
            }

            // считать параметры помехи
            double dDev = 0;

            switch (cmbTypeModulation.SelectedIndex)
            {
                // ЧМШ
                case 0:
                    SupprFWS.bModulation = 0x01;
                    SupprFWS.bDeviation = Convert.ToByte(cmbDeviation.SelectedIndex + 1);
                    SupprFWS.bManipulation = 0;
                    break;

                // ЧМ-2
                case 1:
                    SupprFWS.bModulation = 0x02;

                    dDev = Convert.ToDouble(cmbDeviation.Text);
                    dDev = (1 / (2000 * dDev)) * 1000000;

                    if (Convert.ToDouble(cmbManipulation.Text) < dDev)
                    {
                        MessageBox.Show(SMessageError.mesErrIncorrSetManip, SMessageError.mesErr, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.ServiceNotification);
                        return;
                    }

                    SupprFWS.bDeviation = Convert.ToByte(cmbDeviation.SelectedIndex + 1);
                    SupprFWS.bManipulation = Convert.ToByte(cmbManipulation.SelectedIndex + 1);
                    break;

                // ЧМ-4
                case 2:
                    SupprFWS.bModulation = 0x03;

                    dDev = Convert.ToDouble(cmbDeviation.Text);
                    dDev = (3 / (2000 * dDev)) * 1000000;

                    if (Convert.ToDouble(cmbManipulation.Text) < dDev)
                    {
                        MessageBox.Show(SMessageError.mesErrIncorrSetManip, SMessageError.mesErr, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.ServiceNotification);
                        return;
                    }

                    SupprFWS.bDeviation = Convert.ToByte(cmbDeviation.SelectedIndex + 1);
                    SupprFWS.bManipulation = Convert.ToByte(cmbManipulation.SelectedIndex + 1);
                    break;

                // ЧМ-8
                case 3:
                    SupprFWS.bModulation = 0x04;

                    dDev = Convert.ToDouble(cmbDeviation.Text);
                    dDev = (7 / (2000 * dDev)) * 1000000;

                    if (Convert.ToDouble(cmbManipulation.Text) < dDev)
                    {
                        MessageBox.Show(SMessageError.mesErrIncorrSetManip, SMessageError.mesErr, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.ServiceNotification);
                        return;
                    }

                    SupprFWS.bDeviation = Convert.ToByte(cmbDeviation.SelectedIndex + 1);
                    SupprFWS.bManipulation = Convert.ToByte(cmbManipulation.SelectedIndex + 1);
                    break;

                // ФМн
                case 4:
                    SupprFWS.bModulation = 0x05;
                    // в байт девиации значение манипуляции
                    SupprFWS.bDeviation = 0;
                    if (variableCommon.TypeStation == 2)
                    {
                        if (cmbManipulation.SelectedIndex == 0) { SupprFWS.bManipulation = Convert.ToByte(cmbManipulation.Items.Count); }
                        else 
                        {
                            SupprFWS.bManipulation = Convert.ToByte(cmbManipulation.SelectedIndex);
                            //SupprFWS.bManipulation = Convert.ToByte(cmbManipulation.SelectedIndex + 1); 
                        }
                    }
                    else { SupprFWS.bManipulation = Convert.ToByte(cmbManipulation.SelectedIndex + 1); }
                    break;

                // ФМн-4
                case 5:
                    SupprFWS.bModulation = 0x06;
                    // в байт девиации значение манипуляции
                    SupprFWS.bDeviation = 0;
                    if (variableCommon.TypeStation == 2)
                    {
                        if (cmbManipulation.SelectedIndex == 0) { SupprFWS.bManipulation = Convert.ToByte(cmbManipulation.Items.Count); }
                        else 
                        {
                            SupprFWS.bManipulation = Convert.ToByte(cmbManipulation.SelectedIndex); 
                            //SupprFWS.bManipulation = Convert.ToByte(cmbManipulation.SelectedIndex + 1); 
                        }
                    }
                    else { SupprFWS.bManipulation = Convert.ToByte(cmbManipulation.SelectedIndex + 1); }
                    break;

                // ФМн-8
                case 6:
                    SupprFWS.bModulation = 0x07;
                    // в байт девиации значение манипуляции
                    SupprFWS.bDeviation = 0;
                    if (variableCommon.TypeStation == 2)
                    {
                        if (cmbManipulation.SelectedIndex == 0) { SupprFWS.bManipulation = Convert.ToByte(cmbManipulation.Items.Count); }
                        else 
                        {
                            SupprFWS.bManipulation = Convert.ToByte(cmbManipulation.SelectedIndex);
                            //SupprFWS.bManipulation = Convert.ToByte(cmbManipulation.SelectedIndex + 1); 
                        }
                    }
                    else { SupprFWS.bManipulation = Convert.ToByte(cmbManipulation.SelectedIndex + 1); }
                    break;

                // ЗШ
                case 7:
                    SupprFWS.bModulation = 0x08;
                    SupprFWS.bDeviation = 0;
                    SupprFWS.bManipulation = 1;
                    break;

                default:
                    SupprFWS.bModulation = 0x06;
                    SupprFWS.bDeviation = 0;
                    SupprFWS.bManipulation = 1;
                    break;
            }

            SupprFWS.bDuration = Convert.ToByte(cmbDuration.SelectedIndex + 1);

            if (variableCommon.TypeStation == 2)
            {
                if ((SupprFWS.bLetter != 8 && SupprFWS.bLetter != 9) && ((SupprFWS.bModulation == 0x05 ||
                     SupprFWS.bModulation == 0x06 || SupprFWS.bModulation == 0x07) && SupprFWS.bManipulation == 10))
                {
                    MessageBox.Show("Манипуляция 0,08 доступна для частоты из диапазона 8 и 9 литеры!", SMessages.mesMessage, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
            }

            if (rbOwn.Checked) // ведущая СП
            {
                infoMessage.MessageErr = functionsUser.ChangeIRI_FRCh_RPToVariableWork(SupprFWS, variableWork.SupprFWS_Own.ToList(), 1, -1);

                if (infoMessage.MessageErr != "")
                {
                    MessageBox.Show(infoMessage.MessageErr, SMessages.mesMessage, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    infoMessage.MessageErr = "";
                    return;
                }
            }
            else // ведомая СП
            {
                infoMessage.MessageErr = functionsUser.ChangeIRI_FRCh_RPToVariableWork(SupprFWS, variableWork.SupprFWS_Linked.ToList(), 2, -1);

                if (infoMessage.MessageErr != "")
                {
                    MessageBox.Show(infoMessage.MessageErr, SMessages.mesMessage, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    infoMessage.MessageErr = "";
                    return;
                }
            }
        }

        private void bDelete_Click(object sender, EventArgs e)
        {
            if (rbOwn.Checked)
            {
                funcVW_IRI_FRCh_RP.DeleteOneIRI_FRCh_RPToVariableWork(dgvIRI_FRCh_RP1.dgv, variableWork, Table.Own);
            }
            if (rbMated.Checked && chbShowMated.Checked)
            {
                funcVW_IRI_FRCh_RP.DeleteOneIRI_FRCh_RPToVariableWork(dgvIRI_FRCh_RP2.dgv, variableWork, Table.Linked);
            }
        }

        private void bClear_Click(object sender, EventArgs e)
        {
            if (rbOwn.Checked)
            {
                funcVW_IRI_FRCh_RP.DeletAllIRI_FRCh_RPToVariableWork(variableWork, Table.Own);
            }
            if (rbMated.Checked && chbShowMated.Checked)
            {
                funcVW_IRI_FRCh_RP.DeletAllIRI_FRCh_RPToVariableWork(variableWork, Table.Linked);
            }
        }

        private void bChange_Click(object sender, EventArgs e)
        {
            SupprFWS = new TSupprFWS();

            // считать частоту
            try
            {
                SupprFWS.iFreq = Convert.ToInt32(nudFreq.Value * 10);

                if (chbINMARSAT.Checked || chbIRIDIUM.Checked)
                {
                    // проверить частоту на принадлежность литере 8
                    if (SupprFWS.iFreq > RangesLetters.FREQ_START_LETTER_8 && SupprFWS.iFreq < RangesLetters.FREQ_START_LETTER_9)
                    {
                        MessageBox.Show("В диапазоне литеры 8 осуществляется подавление IRIDIUM (INMARSAT)!", SMessages.mesMessage, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                }
            }
            catch (System.Exception)
            {
                MessageBox.Show(SMessageError.mesErrIncorrSetFreq, SMessageError.mesErr, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.ServiceNotification);
                return;
            }


            // проверить частоту на принадлежность частотному диапазону 
            if (SupprFWS.iFreq < RangesLetters.FREQ_START_LETTER_1 | SupprFWS.iFreq > RangesLetters.FREQ_STOP_LETTER_9)
            {
                MessageBox.Show(SMessageError.mesErrFreqWithoutRange, SMessageError.mesErr, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.ServiceNotification);
                return;
            }

            // определить литеру
            if (SupprFWS.iFreq >= RangesLetters.FREQ_START_LETTER_1 & SupprFWS.iFreq < RangesLetters.FREQ_START_LETTER_2)
                SupprFWS.bLetter = 1;
            if (SupprFWS.iFreq >= RangesLetters.FREQ_START_LETTER_2 & SupprFWS.iFreq < RangesLetters.FREQ_START_LETTER_3)
                SupprFWS.bLetter = 2;
            if (SupprFWS.iFreq >= RangesLetters.FREQ_START_LETTER_3 & SupprFWS.iFreq < RangesLetters.FREQ_START_LETTER_4)
                SupprFWS.bLetter = 3;
            if (SupprFWS.iFreq >= RangesLetters.FREQ_START_LETTER_4 & SupprFWS.iFreq < RangesLetters.FREQ_START_LETTER_5)
                SupprFWS.bLetter = 4;
            if (SupprFWS.iFreq >= RangesLetters.FREQ_START_LETTER_5 & SupprFWS.iFreq < RangesLetters.FREQ_START_LETTER_6)
                SupprFWS.bLetter = 5;
            if (SupprFWS.iFreq >= RangesLetters.FREQ_START_LETTER_6 & SupprFWS.iFreq < RangesLetters.FREQ_START_LETTER_7)
                SupprFWS.bLetter = 6;
            if (variableCommon.TypeStation == 0)
            {
                if (SupprFWS.iFreq >= RangesLetters.FREQ_START_LETTER_7 & SupprFWS.iFreq < RangesLetters.FREQ_STOP_LETTER_7)
                    SupprFWS.bLetter = 7;
            }
            else if (variableCommon.TypeStation == 1 || variableCommon.TypeStation == 2)
            {
                if (SupprFWS.iFreq >= RangesLetters.FREQ_START_LETTER_7 & SupprFWS.iFreq < RangesLetters.FREQ_START_LETTER_8)
                    SupprFWS.bLetter = 7;
                if (SupprFWS.iFreq >= RangesLetters.FREQ_START_LETTER_8 & SupprFWS.iFreq < RangesLetters.FREQ_START_LETTER_9)
                    SupprFWS.bLetter = 8;
                if (SupprFWS.iFreq >= RangesLetters.FREQ_START_LETTER_9 & SupprFWS.iFreq <= RangesLetters.FREQ_STOP_LETTER_9)
                    SupprFWS.bLetter = 9;
            }

            // считать пеленг
            try
            {
                SupprFWS.sBearing = Convert.ToInt16(Convert.ToDecimal(nudBearing.Value, Functions.format) * 10);
                //SupprFWS.sBearing = Convert.ToInt16(nudBearing.Value);
            }
            catch (System.Exception)
            {
                MessageBox.Show(SMessageError.mesErrIncorrSetPel, SMessageError.mesErr, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.ServiceNotification);
                return;
            }

            // проверить пеленг на принадлежность диапазону 
            if (SupprFWS.sBearing < 0 | SupprFWS.sBearing > 3599)
            {
                MessageBox.Show(SMessageError.mesErrPelWithoutRange, SMessageError.mesErr, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.ServiceNotification);
                return;
            }

            // считать приоритет
            try
            {
                SupprFWS.bPrior = Convert.ToByte(nudPrior.Value);
            }
            catch (System.Exception)
            {
                MessageBox.Show(SMessageError.mesErrIncorrSetPrior, SMessageError.mesErr, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.ServiceNotification);
                return;
            }

            // считать порог
            try
            {
                SupprFWS.sLevel = Convert.ToInt16(nudLevel.Value);
            }
            catch (System.Exception)
            {
                MessageBox.Show(SMessageError.mesErrIncorrSetLevel, SMessageError.mesErr, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.ServiceNotification);
                return;
            }

            // считать параметры помехи
            double dDev = 0;

            switch (cmbTypeModulation.SelectedIndex)
            {
                // ЧМШ
                case 0:
                    SupprFWS.bModulation = 0x01;
                    SupprFWS.bDeviation = Convert.ToByte(cmbDeviation.SelectedIndex + 1);
                    SupprFWS.bManipulation = 0;
                    break;

                // ЧМ-2
                case 1:
                    SupprFWS.bModulation = 0x02;

                    dDev = Convert.ToDouble(cmbDeviation.Text);
                    dDev = (1 / (2000 * dDev)) * 1000000;

                    if (Convert.ToDouble(cmbManipulation.Text) < dDev)
                    {
                        MessageBox.Show(SMessageError.mesErrIncorrSetManip, SMessageError.mesErr, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.ServiceNotification);
                        return;
                    }

                    SupprFWS.bDeviation = Convert.ToByte(cmbDeviation.SelectedIndex + 1);
                    SupprFWS.bManipulation = Convert.ToByte(cmbManipulation.SelectedIndex + 1);
                    break;

                // ЧМ-4
                case 2:
                    SupprFWS.bModulation = 0x03;

                    dDev = Convert.ToDouble(cmbDeviation.Text);
                    dDev = (1 / (2000 * dDev)) * 1000000;

                    if (Convert.ToDouble(cmbManipulation.Text) < dDev)
                    {
                        MessageBox.Show(SMessageError.mesErrIncorrSetManip, SMessageError.mesErr, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.ServiceNotification);
                        return;
                    }

                    SupprFWS.bDeviation = Convert.ToByte(cmbDeviation.SelectedIndex + 1);
                    SupprFWS.bManipulation = Convert.ToByte(cmbManipulation.SelectedIndex + 1);
                    break;

                // ЧМ-8
                case 3:
                    SupprFWS.bModulation = 0x04;

                    dDev = Convert.ToDouble(cmbDeviation.Text);
                    dDev = (1 / (2000 * dDev)) * 1000000;

                    if (Convert.ToDouble(cmbManipulation.Text) < dDev)
                    {
                        MessageBox.Show(SMessageError.mesErrIncorrSetManip, SMessageError.mesErr, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.ServiceNotification);
                        return;
                    }

                    SupprFWS.bDeviation = Convert.ToByte(cmbDeviation.SelectedIndex + 1);
                    SupprFWS.bManipulation = Convert.ToByte(cmbManipulation.SelectedIndex + 1);
                    break;

                // ФМн
                case 4:
                    SupprFWS.bModulation = 0x05;
                    // в байт девиации значение манипуляции
                    SupprFWS.bDeviation = 0;
                    if (variableCommon.TypeStation == 2)
                    {
                        if (cmbManipulation.SelectedIndex == 0) { SupprFWS.bManipulation = Convert.ToByte(cmbManipulation.Items.Count); }
                        else 
                        {
                            SupprFWS.bManipulation = Convert.ToByte(cmbManipulation.SelectedIndex); 
                            //SupprFWS.bManipulation = Convert.ToByte(cmbManipulation.SelectedIndex + 1); 
                        }
                    }
                    else { SupprFWS.bManipulation = Convert.ToByte(cmbManipulation.SelectedIndex + 1); }
                    break;

                // ФМн-4
                case 5:
                    SupprFWS.bModulation = 0x06;
                    // в байт девиации значение манипуляции
                    SupprFWS.bDeviation = 0;
                    if (variableCommon.TypeStation == 2)
                    {
                        if (cmbManipulation.SelectedIndex == 0) { SupprFWS.bManipulation = Convert.ToByte(cmbManipulation.Items.Count); }
                        else 
                        {
                            SupprFWS.bManipulation = Convert.ToByte(cmbManipulation.SelectedIndex); 
                            //SupprFWS.bManipulation = Convert.ToByte(cmbManipulation.SelectedIndex + 1); 
                        }
                    }
                    else { SupprFWS.bManipulation = Convert.ToByte(cmbManipulation.SelectedIndex + 1); }
                    break;

                // ФМн-8
                case 6:
                    SupprFWS.bModulation = 0x07;
                    // в байт девиации значение манипуляции
                    SupprFWS.bDeviation = 0;
                    if (variableCommon.TypeStation == 2)
                    {
                        if (cmbManipulation.SelectedIndex == 0) { SupprFWS.bManipulation = Convert.ToByte(cmbManipulation.Items.Count); }
                        else 
                        {
                            SupprFWS.bManipulation = Convert.ToByte(cmbManipulation.SelectedIndex);
                            //SupprFWS.bManipulation = Convert.ToByte(cmbManipulation.SelectedIndex + 1); 
                        }
                    }
                    else { SupprFWS.bManipulation = Convert.ToByte(cmbManipulation.SelectedIndex + 1); }
                    break;

                // ЗШ
                case 7:
                    SupprFWS.bModulation = 0x08;
                    SupprFWS.bDeviation = 0;
                    SupprFWS.bManipulation = 1;
                    break;

                default:
                    SupprFWS.bModulation = 0x06;
                    SupprFWS.bDeviation = 0;
                    SupprFWS.bManipulation = 1;
                    break;
            }

            SupprFWS.bDuration = Convert.ToByte(cmbDuration.SelectedIndex + 1);

            if (variableCommon.TypeStation == 2)
            {
                if ((SupprFWS.bLetter != 8 && SupprFWS.bLetter != 9) && ((SupprFWS.bModulation == 0x05 || 
                    SupprFWS.bModulation == 0x06 || SupprFWS.bModulation == 0x07) && SupprFWS.bManipulation == 10))
                {
                    MessageBox.Show("Манипуляция 0,08 доступна для частоты из диапазона 8 и 9 литеры!", SMessages.mesMessage, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
            }

            if (rbOwn.Checked) // ведущая СП
            {
                int tempID = Convert.ToInt32(dgvIRI_FRCh_RP1.dgv.Rows[dgvIRI_FRCh_RP1.dgv.SelectedRows[0].Index].Cells[0].Value);
                SupprFWS.iID = tempID;
                
                TSupprFWS tempSupprFWS_Own = SupprFWS;// для временного хранения

                List<TSupprFWS> lTSupprFWS_Own = variableWork.SupprFWS_Own.ToList();

                int ind = lTSupprFWS_Own.FindIndex(x => x.iID == tempID);
                curVal_IRI_FRCh_RP.СurIndexRP = ind; // для выделения строки в dgv
                if (ind != -1) 
                {
                    lTSupprFWS_Own.RemoveAt(ind);

                    infoMessage.MessageErr = functionsUser.ChangeIRI_FRCh_RPToVariableWork(SupprFWS, lTSupprFWS_Own, 1, ind);
                    dgvIRI_FRCh_RP1.dgv.Rows[ind].Selected = true;

                    if (infoMessage.MessageErr != "")
                    {
                        MessageBox.Show(infoMessage.MessageErr, SMessages.mesMessage, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        infoMessage.MessageErr = "";
                        return;
                    }
                }
                else return;
            }
            else // ведомая СП
            {
                int tempID = Convert.ToInt32(dgvIRI_FRCh_RP2.dgv.Rows[dgvIRI_FRCh_RP2.dgv.SelectedRows[0].Index].Cells[0].Value);
                SupprFWS.iID = tempID;

                TSupprFWS tempSupprFWS_Linked = SupprFWS; // для временного хранения
                
                List<TSupprFWS> lTSupprFWS_Linked = variableWork.SupprFWS_Linked.ToList();

                int ind = lTSupprFWS_Linked.FindIndex(x => x.iID == tempID);
                curVal_IRI_FRCh_RP.СurIndexRP = ind; // для в ыделения строки в dgv

                if (ind != -1)
                {
                    lTSupprFWS_Linked.RemoveAt(ind);

                    infoMessage.MessageErr = functionsUser.ChangeIRI_FRCh_RPToVariableWork(SupprFWS, lTSupprFWS_Linked, 2, ind);
                    dgvIRI_FRCh_RP2.dgv.Rows[ind].Selected = true;

                    if (infoMessage.MessageErr != "")
                    {
                        MessageBox.Show(infoMessage.MessageErr, SMessages.mesMessage, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        infoMessage.MessageErr = "";
                        return;
                    }
                }
                else return;
            }
        }

       
        private bool SetChecked(CheckBox chb)
        {
            if (chb.Checked)
            {
                chb.Checked = false;
                return false;
            }
           
            int count = functionsDGV.AmountRecordsDGV(dgvIRI_FRCh_RP1.dgv);
           
            if (count == 0) chb.Checked = true;

            for (int i = 0; i < count; i++)
            {
                byte bLet = Convert.ToByte(dgvIRI_FRCh_RP1.dgv.Rows[i].Cells[3].Value); 
                // проверка на наличие частот в таблице из литеры 8 ----------------------
                if (bLet == 8)
                {
                    chb.Checked = false;
                    MessageBox.Show("В диапазоне литеры 8 уже имеются ИРИ для подавления!", SMessages.mesMessage, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return false;
                }
                else
                {
                    chb.Checked = true;
                }
            }

            return true;
        }

        private int iCheckIridium = 0;
        private void chbIRIDIUM_Click(object sender, EventArgs e)
        {
            if (!SetChecked(chbIRIDIUM))
            {
                iCheckIridium = 0;
                variableWork.CheckIridiumInmarsat = iCheckIridium;
                
                List<TSupprFWS> temp = variableWork.SupprFWS_Own.ToList();
                int ind = temp.FindIndex(x => x.iFreq == iFreqIridium);
                if (ind != -1)
                {
                    temp.RemoveAt(ind);
                    variableWork.SupprFWS_Own = temp.ToArray();
                }
                return;
            }
           
            if (chbIRIDIUM.Checked)
            {
                iCheckIridium++;
                variableWork.CheckIridiumInmarsat = iCheckIridium;

                List<TSupprFWS> tempSupprFWS = variableWork.SupprFWS_Own.ToList();

                TSupprFWS SupprFWS = new TSupprFWS();
                SupprFWS = TSupprFWS.CreateID();
                SupprFWS.iFreq = iFreqIridium;
                SupprFWS.bLetter = 8;
                SupprFWS.sBearing = 0;
                SupprFWS.bPrior = 1;
                SupprFWS.sLevel = -130;
                SupprFWS.bDeviation = 0;// Convert.ToByte(cmbDeviation.SelectedIndex + 1);
                SupprFWS.bManipulation = 0;// Convert.ToByte(cmbManipulation.SelectedIndex + 1);
                SupprFWS.bModulation = 0x09;// 0x04;
                SupprFWS.bDuration = 0;// Convert.ToByte(cmbDuration.SelectedIndex + 1);

                tempSupprFWS.Add(SupprFWS);

                variableWork.SupprFWS_Own = tempSupprFWS.ToArray();

            }
        }

        private int iCheckInmarsat = 0;
        private void chbINMARSAT_Click(object sender, EventArgs e)
        {
            if (!SetChecked(chbINMARSAT))
            {
                iCheckInmarsat = 0;
                variableWork.CheckIridiumInmarsat = iCheckInmarsat;

                List<TSupprFWS> temp = variableWork.SupprFWS_Own.ToList();
                int ind = temp.FindIndex(x => x.iFreq == iFreqInmarsat);
                if (ind != -1)
                {
                    temp.RemoveAt(ind);
                    variableWork.SupprFWS_Own = temp.ToArray();
                }
                return;
            }

            if (chbINMARSAT.Checked)
            {
                iCheckInmarsat++;
                variableWork.CheckIridiumInmarsat = iCheckInmarsat;

                List<TSupprFWS> tempSupprFWS = variableWork.SupprFWS_Own.ToList();

                TSupprFWS SupprFWS = new TSupprFWS();
                SupprFWS = TSupprFWS.CreateID();
                SupprFWS.iFreq = iFreqInmarsat;
                SupprFWS.bLetter = 8;
                SupprFWS.sBearing = 0;
                SupprFWS.bPrior = 1;
                SupprFWS.sLevel = -130;
                SupprFWS.bDeviation = 0;// Convert.ToByte(cmbDeviation.SelectedIndex + 1);
                SupprFWS.bManipulation = 0;// Convert.ToByte(cmbManipulation.SelectedIndex + 1);
                SupprFWS.bModulation = 0x0A;// 0x04;
                SupprFWS.bDuration = 0;// Convert.ToByte(cmbDuration.SelectedIndex + 1);

                tempSupprFWS.Add(SupprFWS);

                variableWork.SupprFWS_Own = tempSupprFWS.ToArray();
            }
        }
    }
}
