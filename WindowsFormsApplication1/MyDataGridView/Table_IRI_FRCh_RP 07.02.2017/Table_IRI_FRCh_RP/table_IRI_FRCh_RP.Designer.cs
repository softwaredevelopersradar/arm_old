﻿namespace Table_IRI_FRCh_RP
{
    partial class table_IRI_FRCh_RP
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.pParam = new System.Windows.Forms.Panel();
            this.gbIRID_INM = new System.Windows.Forms.GroupBox();
            this.chbIRIDIUM = new System.Windows.Forms.CheckBox();
            this.chbINMARSAT = new System.Windows.Forms.CheckBox();
            this.grbRadioButtons = new System.Windows.Forms.GroupBox();
            this.rbMated = new System.Windows.Forms.RadioButton();
            this.rbOwn = new System.Windows.Forms.RadioButton();
            this.pParamSignal = new System.Windows.Forms.Panel();
            this.nudFreq = new System.Windows.Forms.NumericUpDown();
            this.nudPrior = new System.Windows.Forms.NumericUpDown();
            this.nudLevel = new System.Windows.Forms.NumericUpDown();
            this.nudBearing = new System.Windows.Forms.NumericUpDown();
            this.lBearing = new System.Windows.Forms.Label();
            this.lPrior = new System.Windows.Forms.Label();
            this.lLevel = new System.Windows.Forms.Label();
            this.lFreq = new System.Windows.Forms.Label();
            this.pButtons = new System.Windows.Forms.Panel();
            this.bChange = new System.Windows.Forms.Button();
            this.bAdd = new System.Windows.Forms.Button();
            this.bClear = new System.Windows.Forms.Button();
            this.bDelete = new System.Windows.Forms.Button();
            this.pIndications = new System.Windows.Forms.Panel();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lYellow = new System.Windows.Forms.Label();
            this.lGreen = new System.Windows.Forms.Label();
            this.lRed = new System.Windows.Forms.Label();
            this.grbParamHind = new System.Windows.Forms.GroupBox();
            this.cmbDuration = new System.Windows.Forms.ComboBox();
            this.lDurat = new System.Windows.Forms.Label();
            this.cmbManipulation = new System.Windows.Forms.ComboBox();
            this.cmbDeviation = new System.Windows.Forms.ComboBox();
            this.cmbTypeModulation = new System.Windows.Forms.ComboBox();
            this.lManipulation = new System.Windows.Forms.Label();
            this.lDeviation = new System.Windows.Forms.Label();
            this.lTypeModulation = new System.Windows.Forms.Label();
            this.chbShowMated = new System.Windows.Forms.CheckBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.dgvIRI_FRCh_RP1 = new MyDataGridView.myDataGridView();
            this.dgvIRI_FRCh_RP2 = new MyDataGridView.myDataGridView();
            this.tableLayoutPanel1.SuspendLayout();
            this.pParam.SuspendLayout();
            this.gbIRID_INM.SuspendLayout();
            this.grbRadioButtons.SuspendLayout();
            this.pParamSignal.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudFreq)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPrior)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudLevel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBearing)).BeginInit();
            this.pButtons.SuspendLayout();
            this.pIndications.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.grbParamHind.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 197F));
            this.tableLayoutPanel1.Controls.Add(this.pParam, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(750, 402);
            this.tableLayoutPanel1.TabIndex = 24;
            // 
            // pParam
            // 
            this.pParam.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pParam.BackColor = System.Drawing.SystemColors.Control;
            this.pParam.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pParam.Controls.Add(this.gbIRID_INM);
            this.pParam.Controls.Add(this.grbRadioButtons);
            this.pParam.Controls.Add(this.pParamSignal);
            this.pParam.Controls.Add(this.pButtons);
            this.pParam.Controls.Add(this.pIndications);
            this.pParam.Controls.Add(this.grbParamHind);
            this.pParam.Controls.Add(this.chbShowMated);
            this.pParam.Location = new System.Drawing.Point(556, 3);
            this.pParam.Name = "pParam";
            this.pParam.Size = new System.Drawing.Size(191, 396);
            this.pParam.TabIndex = 21;
            // 
            // gbIRID_INM
            // 
            this.gbIRID_INM.Controls.Add(this.chbIRIDIUM);
            this.gbIRID_INM.Controls.Add(this.chbINMARSAT);
            this.gbIRID_INM.Location = new System.Drawing.Point(4, 350);
            this.gbIRID_INM.Name = "gbIRID_INM";
            this.gbIRID_INM.Size = new System.Drawing.Size(182, 43);
            this.gbIRID_INM.TabIndex = 27;
            this.gbIRID_INM.TabStop = false;
            // 
            // chbIRIDIUM
            // 
            this.chbIRIDIUM.AutoCheck = false;
            this.chbIRIDIUM.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.chbIRIDIUM.ForeColor = System.Drawing.Color.Blue;
            this.chbIRIDIUM.Location = new System.Drawing.Point(4, 11);
            this.chbIRIDIUM.Name = "chbIRIDIUM";
            this.chbIRIDIUM.Size = new System.Drawing.Size(71, 27);
            this.chbIRIDIUM.TabIndex = 25;
            this.chbIRIDIUM.Text = "IRIDIUM";
            this.chbIRIDIUM.UseVisualStyleBackColor = true;
            this.chbIRIDIUM.Click += new System.EventHandler(this.chbIRIDIUM_Click);
            // 
            // chbINMARSAT
            // 
            this.chbINMARSAT.AutoCheck = false;
            this.chbINMARSAT.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.chbINMARSAT.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.chbINMARSAT.Location = new System.Drawing.Point(89, 7);
            this.chbINMARSAT.Name = "chbINMARSAT";
            this.chbINMARSAT.Size = new System.Drawing.Size(91, 34);
            this.chbINMARSAT.TabIndex = 26;
            this.chbINMARSAT.Text = "INMARSAT (THURAYA)";
            this.chbINMARSAT.UseVisualStyleBackColor = true;
            this.chbINMARSAT.Click += new System.EventHandler(this.chbINMARSAT_Click);
            // 
            // grbRadioButtons
            // 
            this.grbRadioButtons.Controls.Add(this.rbMated);
            this.grbRadioButtons.Controls.Add(this.rbOwn);
            this.grbRadioButtons.Location = new System.Drawing.Point(4, -3);
            this.grbRadioButtons.Name = "grbRadioButtons";
            this.grbRadioButtons.Size = new System.Drawing.Size(182, 29);
            this.grbRadioButtons.TabIndex = 23;
            this.grbRadioButtons.TabStop = false;
            // 
            // rbMated
            // 
            this.rbMated.AutoSize = true;
            this.rbMated.Location = new System.Drawing.Point(96, 8);
            this.rbMated.Name = "rbMated";
            this.rbMated.Size = new System.Drawing.Size(72, 17);
            this.rbMated.TabIndex = 14;
            this.rbMated.Text = "Ведомый";
            this.rbMated.UseVisualStyleBackColor = true;
            // 
            // rbOwn
            // 
            this.rbOwn.AutoSize = true;
            this.rbOwn.Checked = true;
            this.rbOwn.ForeColor = System.Drawing.Color.Red;
            this.rbOwn.Location = new System.Drawing.Point(5, 8);
            this.rbOwn.Name = "rbOwn";
            this.rbOwn.Size = new System.Drawing.Size(70, 17);
            this.rbOwn.TabIndex = 13;
            this.rbOwn.TabStop = true;
            this.rbOwn.Text = "Ведущий";
            this.rbOwn.UseVisualStyleBackColor = true;
            // 
            // pParamSignal
            // 
            this.pParamSignal.Controls.Add(this.nudFreq);
            this.pParamSignal.Controls.Add(this.nudPrior);
            this.pParamSignal.Controls.Add(this.nudLevel);
            this.pParamSignal.Controls.Add(this.nudBearing);
            this.pParamSignal.Controls.Add(this.lBearing);
            this.pParamSignal.Controls.Add(this.lPrior);
            this.pParamSignal.Controls.Add(this.lLevel);
            this.pParamSignal.Controls.Add(this.lFreq);
            this.pParamSignal.Location = new System.Drawing.Point(4, 41);
            this.pParamSignal.Name = "pParamSignal";
            this.pParamSignal.Size = new System.Drawing.Size(183, 90);
            this.pParamSignal.TabIndex = 15;
            // 
            // nudFreq
            // 
            this.nudFreq.DecimalPlaces = 1;
            this.nudFreq.Location = new System.Drawing.Point(98, 3);
            this.nudFreq.Maximum = new decimal(new int[] {
            3000000,
            0,
            0,
            0});
            this.nudFreq.Minimum = new decimal(new int[] {
            30000,
            0,
            0,
            0});
            this.nudFreq.Name = "nudFreq";
            this.nudFreq.Size = new System.Drawing.Size(78, 20);
            this.nudFreq.TabIndex = 9;
            this.nudFreq.ThousandsSeparator = true;
            this.nudFreq.Value = new decimal(new int[] {
            30000,
            0,
            0,
            0});
            // 
            // nudPrior
            // 
            this.nudPrior.AutoSize = true;
            this.nudPrior.Location = new System.Drawing.Point(98, 66);
            this.nudPrior.Maximum = new decimal(new int[] {
            9,
            0,
            0,
            0});
            this.nudPrior.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudPrior.Name = "nudPrior";
            this.nudPrior.Size = new System.Drawing.Size(78, 20);
            this.nudPrior.TabIndex = 13;
            this.nudPrior.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // nudLevel
            // 
            this.nudLevel.AutoSize = true;
            this.nudLevel.Location = new System.Drawing.Point(98, 45);
            this.nudLevel.Maximum = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.nudLevel.Minimum = new decimal(new int[] {
            130,
            0,
            0,
            -2147483648});
            this.nudLevel.Name = "nudLevel";
            this.nudLevel.Size = new System.Drawing.Size(78, 20);
            this.nudLevel.TabIndex = 11;
            this.nudLevel.Value = new decimal(new int[] {
            80,
            0,
            0,
            -2147483648});
            // 
            // nudBearing
            // 
            this.nudBearing.AutoSize = true;
            this.nudBearing.DecimalPlaces = 1;
            this.nudBearing.Location = new System.Drawing.Point(98, 24);
            this.nudBearing.Maximum = new decimal(new int[] {
            3599,
            0,
            0,
            65536});
            this.nudBearing.Name = "nudBearing";
            this.nudBearing.Size = new System.Drawing.Size(78, 20);
            this.nudBearing.TabIndex = 15;
            // 
            // lBearing
            // 
            this.lBearing.AutoSize = true;
            this.lBearing.Location = new System.Drawing.Point(3, 31);
            this.lBearing.Name = "lBearing";
            this.lBearing.Size = new System.Drawing.Size(123, 13);
            this.lBearing.TabIndex = 14;
            this.lBearing.Text = "Пеленг, °.......................";
            // 
            // lPrior
            // 
            this.lPrior.AutoSize = true;
            this.lPrior.Location = new System.Drawing.Point(4, 73);
            this.lPrior.Name = "lPrior";
            this.lPrior.Size = new System.Drawing.Size(130, 13);
            this.lPrior.TabIndex = 12;
            this.lPrior.Text = "Приоритет.......................";
            // 
            // lLevel
            // 
            this.lLevel.AutoSize = true;
            this.lLevel.Location = new System.Drawing.Point(3, 52);
            this.lLevel.Name = "lLevel";
            this.lLevel.Size = new System.Drawing.Size(126, 13);
            this.lLevel.TabIndex = 10;
            this.lLevel.Text = "Порог, дБ.......................";
            // 
            // lFreq
            // 
            this.lFreq.AutoSize = true;
            this.lFreq.Location = new System.Drawing.Point(1, 9);
            this.lFreq.Name = "lFreq";
            this.lFreq.Size = new System.Drawing.Size(142, 13);
            this.lFreq.TabIndex = 8;
            this.lFreq.Text = "Частота, кГц.......................";
            // 
            // pButtons
            // 
            this.pButtons.Controls.Add(this.bChange);
            this.pButtons.Controls.Add(this.bAdd);
            this.pButtons.Controls.Add(this.bClear);
            this.pButtons.Controls.Add(this.bDelete);
            this.pButtons.Location = new System.Drawing.Point(6, 230);
            this.pButtons.Name = "pButtons";
            this.pButtons.Size = new System.Drawing.Size(180, 52);
            this.pButtons.TabIndex = 11;
            // 
            // bChange
            // 
            this.bChange.ForeColor = System.Drawing.Color.Green;
            this.bChange.Location = new System.Drawing.Point(3, 26);
            this.bChange.Name = "bChange";
            this.bChange.Size = new System.Drawing.Size(83, 23);
            this.bChange.TabIndex = 15;
            this.bChange.Text = "Изменить";
            this.bChange.UseVisualStyleBackColor = true;
            this.bChange.Click += new System.EventHandler(this.bChange_Click);
            // 
            // bAdd
            // 
            this.bAdd.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.bAdd.Location = new System.Drawing.Point(96, 26);
            this.bAdd.Name = "bAdd";
            this.bAdd.Size = new System.Drawing.Size(80, 23);
            this.bAdd.TabIndex = 14;
            this.bAdd.Text = "Добавить";
            this.bAdd.UseVisualStyleBackColor = true;
            this.bAdd.Click += new System.EventHandler(this.bAdd_Click);
            // 
            // bClear
            // 
            this.bClear.ForeColor = System.Drawing.Color.Red;
            this.bClear.Location = new System.Drawing.Point(3, 3);
            this.bClear.Name = "bClear";
            this.bClear.Size = new System.Drawing.Size(83, 23);
            this.bClear.TabIndex = 13;
            this.bClear.Text = "Очистить";
            this.bClear.UseVisualStyleBackColor = true;
            this.bClear.Click += new System.EventHandler(this.bClear_Click);
            // 
            // bDelete
            // 
            this.bDelete.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.bDelete.Location = new System.Drawing.Point(96, 3);
            this.bDelete.Name = "bDelete";
            this.bDelete.Size = new System.Drawing.Size(80, 23);
            this.bDelete.TabIndex = 10;
            this.bDelete.Text = "Удалить";
            this.bDelete.UseVisualStyleBackColor = true;
            this.bDelete.Click += new System.EventHandler(this.bDelete_Click);
            // 
            // pIndications
            // 
            this.pIndications.Controls.Add(this.pictureBox3);
            this.pIndications.Controls.Add(this.pictureBox2);
            this.pIndications.Controls.Add(this.pictureBox1);
            this.pIndications.Controls.Add(this.lYellow);
            this.pIndications.Controls.Add(this.lGreen);
            this.pIndications.Controls.Add(this.lRed);
            this.pIndications.Location = new System.Drawing.Point(5, 280);
            this.pIndications.Name = "pIndications";
            this.pIndications.Size = new System.Drawing.Size(181, 73);
            this.pIndications.TabIndex = 10;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::Table_IRI_FRCh_RP.Properties.Resources.yellow;
            this.pictureBox3.Location = new System.Drawing.Point(5, 56);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(15, 17);
            this.pictureBox3.TabIndex = 9;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::Table_IRI_FRCh_RP.Properties.Resources.green;
            this.pictureBox2.Location = new System.Drawing.Point(5, 32);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(15, 17);
            this.pictureBox2.TabIndex = 8;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Table_IRI_FRCh_RP.Properties.Resources.red;
            this.pictureBox1.Location = new System.Drawing.Point(5, 7);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(15, 18);
            this.pictureBox1.TabIndex = 7;
            this.pictureBox1.TabStop = false;
            // 
            // lYellow
            // 
            this.lYellow.AutoSize = true;
            this.lYellow.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lYellow.Location = new System.Drawing.Point(24, 56);
            this.lYellow.Name = "lYellow";
            this.lYellow.Size = new System.Drawing.Size(142, 14);
            this.lYellow.TabIndex = 3;
            this.lYellow.Text = "давно неработающий ИРИ";
            // 
            // lGreen
            // 
            this.lGreen.AutoSize = true;
            this.lGreen.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lGreen.Location = new System.Drawing.Point(24, 26);
            this.lGreen.Name = "lGreen";
            this.lGreen.Size = new System.Drawing.Size(109, 28);
            this.lGreen.TabIndex = 2;
            this.lGreen.Text = "давно работающий \r\n(подавляемый) ИРИ";
            // 
            // lRed
            // 
            this.lRed.AutoSize = true;
            this.lRed.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lRed.Location = new System.Drawing.Point(24, 0);
            this.lRed.Name = "lRed";
            this.lRed.Size = new System.Drawing.Size(156, 28);
            this.lRed.TabIndex = 1;
            this.lRed.Text = "работающий (подавляемый) \r\nИРИ";
            // 
            // grbParamHind
            // 
            this.grbParamHind.Controls.Add(this.cmbDuration);
            this.grbParamHind.Controls.Add(this.lDurat);
            this.grbParamHind.Controls.Add(this.cmbManipulation);
            this.grbParamHind.Controls.Add(this.cmbDeviation);
            this.grbParamHind.Controls.Add(this.cmbTypeModulation);
            this.grbParamHind.Controls.Add(this.lManipulation);
            this.grbParamHind.Controls.Add(this.lDeviation);
            this.grbParamHind.Controls.Add(this.lTypeModulation);
            this.grbParamHind.ForeColor = System.Drawing.Color.Blue;
            this.grbParamHind.Location = new System.Drawing.Point(5, 129);
            this.grbParamHind.Name = "grbParamHind";
            this.grbParamHind.Size = new System.Drawing.Size(181, 102);
            this.grbParamHind.TabIndex = 4;
            this.grbParamHind.TabStop = false;
            this.grbParamHind.Text = "Параметры помехи";
            // 
            // cmbDuration
            // 
            this.cmbDuration.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDuration.FormattingEnabled = true;
            this.cmbDuration.Location = new System.Drawing.Point(110, 75);
            this.cmbDuration.Name = "cmbDuration";
            this.cmbDuration.Size = new System.Drawing.Size(67, 21);
            this.cmbDuration.TabIndex = 9;
            // 
            // lDurat
            // 
            this.lDurat.AutoSize = true;
            this.lDurat.ForeColor = System.Drawing.Color.Black;
            this.lDurat.Location = new System.Drawing.Point(4, 82);
            this.lDurat.Name = "lDurat";
            this.lDurat.Size = new System.Drawing.Size(129, 13);
            this.lDurat.TabIndex = 8;
            this.lDurat.Text = "Длит.  излуч., мс............";
            // 
            // cmbManipulation
            // 
            this.cmbManipulation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbManipulation.FormattingEnabled = true;
            this.cmbManipulation.Location = new System.Drawing.Point(110, 53);
            this.cmbManipulation.Name = "cmbManipulation";
            this.cmbManipulation.Size = new System.Drawing.Size(67, 21);
            this.cmbManipulation.TabIndex = 7;
            // 
            // cmbDeviation
            // 
            this.cmbDeviation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDeviation.FormattingEnabled = true;
            this.cmbDeviation.Location = new System.Drawing.Point(110, 31);
            this.cmbDeviation.Name = "cmbDeviation";
            this.cmbDeviation.Size = new System.Drawing.Size(67, 21);
            this.cmbDeviation.TabIndex = 5;
            // 
            // cmbTypeModulation
            // 
            this.cmbTypeModulation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTypeModulation.FormattingEnabled = true;
            this.cmbTypeModulation.Location = new System.Drawing.Point(110, 9);
            this.cmbTypeModulation.Name = "cmbTypeModulation";
            this.cmbTypeModulation.Size = new System.Drawing.Size(67, 21);
            this.cmbTypeModulation.TabIndex = 3;
            this.cmbTypeModulation.SelectedIndexChanged += new System.EventHandler(this.cmbTypeModulation_SelectedIndexChanged);
            // 
            // lManipulation
            // 
            this.lManipulation.AutoSize = true;
            this.lManipulation.ForeColor = System.Drawing.Color.Black;
            this.lManipulation.Location = new System.Drawing.Point(4, 60);
            this.lManipulation.Name = "lManipulation";
            this.lManipulation.Size = new System.Drawing.Size(170, 13);
            this.lManipulation.TabIndex = 6;
            this.lManipulation.Text = "Манипуляция, мкс.......................";
            // 
            // lDeviation
            // 
            this.lDeviation.AutoSize = true;
            this.lDeviation.ForeColor = System.Drawing.Color.Black;
            this.lDeviation.Location = new System.Drawing.Point(3, 38);
            this.lDeviation.Name = "lDeviation";
            this.lDeviation.Size = new System.Drawing.Size(174, 13);
            this.lDeviation.TabIndex = 4;
            this.lDeviation.Text = "Девиация(+/-), кГц........................";
            // 
            // lTypeModulation
            // 
            this.lTypeModulation.AutoSize = true;
            this.lTypeModulation.ForeColor = System.Drawing.Color.Black;
            this.lTypeModulation.Location = new System.Drawing.Point(3, 16);
            this.lTypeModulation.Name = "lTypeModulation";
            this.lTypeModulation.Size = new System.Drawing.Size(168, 13);
            this.lTypeModulation.TabIndex = 2;
            this.lTypeModulation.Text = "Вид модуляции............................";
            // 
            // chbShowMated
            // 
            this.chbShowMated.AutoSize = true;
            this.chbShowMated.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chbShowMated.Location = new System.Drawing.Point(4, 25);
            this.chbShowMated.Name = "chbShowMated";
            this.chbShowMated.Size = new System.Drawing.Size(172, 17);
            this.chbShowMated.TabIndex = 24;
            this.chbShowMated.Text = "Ведомый.................................";
            this.chbShowMated.UseVisualStyleBackColor = true;
            this.chbShowMated.CheckedChanged += new System.EventHandler(this.chbShowMated_CheckedChanged);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.dgvIRI_FRCh_RP1, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.dgvIRI_FRCh_RP2, 0, 1);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(1, 1);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(1);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 0F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(551, 400);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // dgvIRI_FRCh_RP1
            // 
            this.dgvIRI_FRCh_RP1.AutoSize = true;
            this.dgvIRI_FRCh_RP1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvIRI_FRCh_RP1.Location = new System.Drawing.Point(3, 3);
            this.dgvIRI_FRCh_RP1.Name = "dgvIRI_FRCh_RP1";
            this.dgvIRI_FRCh_RP1.Size = new System.Drawing.Size(545, 394);
            this.dgvIRI_FRCh_RP1.TabIndex = 0;
            // 
            // dgvIRI_FRCh_RP2
            // 
            this.dgvIRI_FRCh_RP2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvIRI_FRCh_RP2.Location = new System.Drawing.Point(3, 403);
            this.dgvIRI_FRCh_RP2.Name = "dgvIRI_FRCh_RP2";
            this.dgvIRI_FRCh_RP2.Size = new System.Drawing.Size(545, 1);
            this.dgvIRI_FRCh_RP2.TabIndex = 23;
            // 
            // table_IRI_FRCh_RP
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "table_IRI_FRCh_RP";
            this.Size = new System.Drawing.Size(750, 402);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.pParam.ResumeLayout(false);
            this.pParam.PerformLayout();
            this.gbIRID_INM.ResumeLayout(false);
            this.grbRadioButtons.ResumeLayout(false);
            this.grbRadioButtons.PerformLayout();
            this.pParamSignal.ResumeLayout(false);
            this.pParamSignal.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudFreq)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPrior)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudLevel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBearing)).EndInit();
            this.pButtons.ResumeLayout(false);
            this.pIndications.ResumeLayout(false);
            this.pIndications.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.grbParamHind.ResumeLayout(false);
            this.grbParamHind.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel pParam;
        private System.Windows.Forms.GroupBox grbRadioButtons;
        public System.Windows.Forms.RadioButton rbMated;
        public System.Windows.Forms.RadioButton rbOwn;
        private System.Windows.Forms.Panel pParamSignal;
        public System.Windows.Forms.NumericUpDown nudFreq;
        public System.Windows.Forms.NumericUpDown nudPrior;
        public System.Windows.Forms.NumericUpDown nudLevel;
        public System.Windows.Forms.NumericUpDown nudBearing;
        private System.Windows.Forms.Label lBearing;
        private System.Windows.Forms.Label lPrior;
        private System.Windows.Forms.Label lLevel;
        private System.Windows.Forms.Label lFreq;
        private System.Windows.Forms.Panel pButtons;
        public System.Windows.Forms.Button bChange;
        public System.Windows.Forms.Button bAdd;
        public System.Windows.Forms.Button bClear;
        public System.Windows.Forms.Button bDelete;
        private System.Windows.Forms.Panel pIndications;
        private System.Windows.Forms.Label lYellow;
        private System.Windows.Forms.Label lGreen;
        private System.Windows.Forms.Label lRed;
        private System.Windows.Forms.GroupBox grbParamHind;
        public System.Windows.Forms.ComboBox cmbDuration;
        private System.Windows.Forms.Label lDurat;
        public System.Windows.Forms.ComboBox cmbManipulation;
        private System.Windows.Forms.Label lManipulation;
        public System.Windows.Forms.ComboBox cmbDeviation;
        private System.Windows.Forms.Label lDeviation;
        public System.Windows.Forms.ComboBox cmbTypeModulation;
        private System.Windows.Forms.Label lTypeModulation;
        public System.Windows.Forms.CheckBox chbShowMated;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        public MyDataGridView.myDataGridView dgvIRI_FRCh_RP1;
        public MyDataGridView.myDataGridView dgvIRI_FRCh_RP2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.GroupBox gbIRID_INM;
        public System.Windows.Forms.CheckBox chbINMARSAT;
        public System.Windows.Forms.CheckBox chbIRIDIUM;

    }
}
