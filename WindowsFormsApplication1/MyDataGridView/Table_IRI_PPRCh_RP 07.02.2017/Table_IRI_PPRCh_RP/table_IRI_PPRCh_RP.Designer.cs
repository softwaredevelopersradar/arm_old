﻿namespace Table_IRI_PPRCh_RP
{
    partial class table_IRI_PPRCh_RP
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pParam = new System.Windows.Forms.Panel();
            this.nudDurIzl = new System.Windows.Forms.NumericUpDown();
            this.bAddParam = new System.Windows.Forms.Button();
            this.lDuratIzl = new System.Windows.Forms.Label();
            this.grbFRCh = new System.Windows.Forms.GroupBox();
            this.bAdd = new System.Windows.Forms.Button();
            this.nudWidthExclude = new System.Windows.Forms.NumericUpDown();
            this.bClearFRCh = new System.Windows.Forms.Button();
            this.nudFreqExclude = new System.Windows.Forms.NumericUpDown();
            this.lFreq = new System.Windows.Forms.Label();
            this.lBand = new System.Windows.Forms.Label();
            this.bClear = new System.Windows.Forms.Button();
            this.grbRadioButtons = new System.Windows.Forms.GroupBox();
            this.rbMated = new System.Windows.Forms.RadioButton();
            this.rbOwn = new System.Windows.Forms.RadioButton();
            this.cmbTypeFFT = new System.Windows.Forms.ComboBox();
            this.lAccuracyFFT = new System.Windows.Forms.Label();
            this.pParamSignal = new System.Windows.Forms.Panel();
            this.nudFreqMax = new System.Windows.Forms.NumericUpDown();
            this.lFreqMax = new System.Windows.Forms.Label();
            this.nudFreqMin = new System.Windows.Forms.NumericUpDown();
            this.nudStep = new System.Windows.Forms.NumericUpDown();
            this.nudLevel = new System.Windows.Forms.NumericUpDown();
            this.lStep = new System.Windows.Forms.Label();
            this.lLevel = new System.Windows.Forms.Label();
            this.lFreqMin = new System.Windows.Forms.Label();
            this.grbParamHind = new System.Windows.Forms.GroupBox();
            this.cmbManipulation = new System.Windows.Forms.ComboBox();
            this.lManipulation = new System.Windows.Forms.Label();
            this.cmbDeviation = new System.Windows.Forms.ComboBox();
            this.lDeviation = new System.Windows.Forms.Label();
            this.cmbModulation = new System.Windows.Forms.ComboBox();
            this.lTypeModulation = new System.Windows.Forms.Label();
            this.chbShowMated = new System.Windows.Forms.CheckBox();
            this.tableLayoutPanelGeneral = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.dgvIRI_PPRCh_RPLinked = new MyDataGridView.myDataGridView();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.dgvIRI_PPRCh_RP2 = new MyDataGridView.myDataGridView();
            this.dgvIRI_PPRCh_RP_EXCLUDE = new MyDataGridView.myDataGridView();
            this.contMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsmiDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.dgvIRI_PPRCh_RPOwn = new MyDataGridView.myDataGridView();
            this.pParam.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudDurIzl)).BeginInit();
            this.grbFRCh.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudWidthExclude)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudFreqExclude)).BeginInit();
            this.grbRadioButtons.SuspendLayout();
            this.pParamSignal.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudFreqMax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudFreqMin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudStep)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudLevel)).BeginInit();
            this.grbParamHind.SuspendLayout();
            this.tableLayoutPanelGeneral.SuspendLayout();
            this.tableLayoutPanel.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.contMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // pParam
            // 
            this.pParam.BackColor = System.Drawing.SystemColors.Control;
            this.pParam.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pParam.Controls.Add(this.nudDurIzl);
            this.pParam.Controls.Add(this.bAddParam);
            this.pParam.Controls.Add(this.lDuratIzl);
            this.pParam.Controls.Add(this.grbFRCh);
            this.pParam.Controls.Add(this.bClear);
            this.pParam.Controls.Add(this.grbRadioButtons);
            this.pParam.Controls.Add(this.cmbTypeFFT);
            this.pParam.Controls.Add(this.lAccuracyFFT);
            this.pParam.Controls.Add(this.pParamSignal);
            this.pParam.Controls.Add(this.grbParamHind);
            this.pParam.Controls.Add(this.chbShowMated);
            this.pParam.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pParam.Location = new System.Drawing.Point(687, 3);
            this.pParam.Name = "pParam";
            this.pParam.Size = new System.Drawing.Size(194, 380);
            this.pParam.TabIndex = 22;
            // 
            // nudDurIzl
            // 
            this.nudDurIzl.Location = new System.Drawing.Point(117, 217);
            this.nudDurIzl.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.nudDurIzl.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.nudDurIzl.Name = "nudDurIzl";
            this.nudDurIzl.Size = new System.Drawing.Size(68, 20);
            this.nudDurIzl.TabIndex = 27;
            this.nudDurIzl.Value = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            // 
            // bAddParam
            // 
            this.bAddParam.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.bAddParam.ForeColor = System.Drawing.Color.Green;
            this.bAddParam.Location = new System.Drawing.Point(8, 266);
            this.bAddParam.Name = "bAddParam";
            this.bAddParam.Size = new System.Drawing.Size(80, 23);
            this.bAddParam.TabIndex = 15;
            this.bAddParam.Text = "Добавить";
            this.bAddParam.UseVisualStyleBackColor = true;
            this.bAddParam.Click += new System.EventHandler(this.bAddParam_Click);
            // 
            // lDuratIzl
            // 
            this.lDuratIzl.AutoSize = true;
            this.lDuratIzl.ForeColor = System.Drawing.Color.Black;
            this.lDuratIzl.Location = new System.Drawing.Point(6, 224);
            this.lDuratIzl.Name = "lDuratIzl";
            this.lDuratIzl.Size = new System.Drawing.Size(135, 13);
            this.lDuratIzl.TabIndex = 8;
            this.lDuratIzl.Text = "Длит.  излуч., мкс............";
            // 
            // grbFRCh
            // 
            this.grbFRCh.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grbFRCh.Controls.Add(this.bAdd);
            this.grbFRCh.Controls.Add(this.nudWidthExclude);
            this.grbFRCh.Controls.Add(this.bClearFRCh);
            this.grbFRCh.Controls.Add(this.nudFreqExclude);
            this.grbFRCh.Controls.Add(this.lFreq);
            this.grbFRCh.Controls.Add(this.lBand);
            this.grbFRCh.ForeColor = System.Drawing.Color.Blue;
            this.grbFRCh.Location = new System.Drawing.Point(3, 290);
            this.grbFRCh.Name = "grbFRCh";
            this.grbFRCh.Size = new System.Drawing.Size(185, 85);
            this.grbFRCh.TabIndex = 23;
            this.grbFRCh.TabStop = false;
            this.grbFRCh.Text = "ФРЧ";
            // 
            // bAdd
            // 
            this.bAdd.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.bAdd.Location = new System.Drawing.Point(5, 56);
            this.bAdd.Name = "bAdd";
            this.bAdd.Size = new System.Drawing.Size(80, 23);
            this.bAdd.TabIndex = 15;
            this.bAdd.Text = "Добавить";
            this.bAdd.UseVisualStyleBackColor = true;
            this.bAdd.Click += new System.EventHandler(this.bAdd_Click);
            // 
            // nudWidthExclude
            // 
            this.nudWidthExclude.AutoSize = true;
            this.nudWidthExclude.Location = new System.Drawing.Point(103, 32);
            this.nudWidthExclude.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.nudWidthExclude.Name = "nudWidthExclude";
            this.nudWidthExclude.Size = new System.Drawing.Size(78, 20);
            this.nudWidthExclude.TabIndex = 19;
            // 
            // bClearFRCh
            // 
            this.bClearFRCh.ForeColor = System.Drawing.Color.Red;
            this.bClearFRCh.Location = new System.Drawing.Point(100, 56);
            this.bClearFRCh.Name = "bClearFRCh";
            this.bClearFRCh.Size = new System.Drawing.Size(80, 23);
            this.bClearFRCh.TabIndex = 13;
            this.bClearFRCh.Text = "Очистить";
            this.bClearFRCh.UseVisualStyleBackColor = true;
            this.bClearFRCh.Click += new System.EventHandler(this.bClearFRCh_Click);
            // 
            // nudFreqExclude
            // 
            this.nudFreqExclude.DecimalPlaces = 1;
            this.nudFreqExclude.Location = new System.Drawing.Point(102, 10);
            this.nudFreqExclude.Maximum = new decimal(new int[] {
            3000000,
            0,
            0,
            0});
            this.nudFreqExclude.Minimum = new decimal(new int[] {
            30000,
            0,
            0,
            0});
            this.nudFreqExclude.Name = "nudFreqExclude";
            this.nudFreqExclude.Size = new System.Drawing.Size(78, 20);
            this.nudFreqExclude.TabIndex = 21;
            this.nudFreqExclude.ThousandsSeparator = true;
            this.nudFreqExclude.Value = new decimal(new int[] {
            30000,
            0,
            0,
            0});
            // 
            // lFreq
            // 
            this.lFreq.AutoSize = true;
            this.lFreq.ForeColor = System.Drawing.Color.Black;
            this.lFreq.Location = new System.Drawing.Point(8, 17);
            this.lFreq.Name = "lFreq";
            this.lFreq.Size = new System.Drawing.Size(142, 13);
            this.lFreq.TabIndex = 20;
            this.lFreq.Text = "Частота, кГц.......................";
            // 
            // lBand
            // 
            this.lBand.AutoSize = true;
            this.lBand.ForeColor = System.Drawing.Color.Black;
            this.lBand.Location = new System.Drawing.Point(10, 37);
            this.lBand.Name = "lBand";
            this.lBand.Size = new System.Drawing.Size(138, 13);
            this.lBand.TabIndex = 18;
            this.lBand.Text = "Полоса, кГц.......................";
            // 
            // bClear
            // 
            this.bClear.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.bClear.ForeColor = System.Drawing.Color.Red;
            this.bClear.Location = new System.Drawing.Point(103, 266);
            this.bClear.Name = "bClear";
            this.bClear.Size = new System.Drawing.Size(80, 23);
            this.bClear.TabIndex = 13;
            this.bClear.Text = "Очистить";
            this.bClear.UseVisualStyleBackColor = true;
            this.bClear.Click += new System.EventHandler(this.bClear_Click);
            // 
            // grbRadioButtons
            // 
            this.grbRadioButtons.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grbRadioButtons.Controls.Add(this.rbMated);
            this.grbRadioButtons.Controls.Add(this.rbOwn);
            this.grbRadioButtons.Location = new System.Drawing.Point(5, -2);
            this.grbRadioButtons.Name = "grbRadioButtons";
            this.grbRadioButtons.Size = new System.Drawing.Size(183, 26);
            this.grbRadioButtons.TabIndex = 23;
            this.grbRadioButtons.TabStop = false;
            // 
            // rbMated
            // 
            this.rbMated.AutoSize = true;
            this.rbMated.Location = new System.Drawing.Point(108, 7);
            this.rbMated.Name = "rbMated";
            this.rbMated.Size = new System.Drawing.Size(72, 17);
            this.rbMated.TabIndex = 14;
            this.rbMated.Text = "Ведомый";
            this.rbMated.UseVisualStyleBackColor = true;
            this.rbMated.CheckedChanged += new System.EventHandler(this.rbMated_CheckedChanged);
            // 
            // rbOwn
            // 
            this.rbOwn.AutoSize = true;
            this.rbOwn.Checked = true;
            this.rbOwn.ForeColor = System.Drawing.Color.Red;
            this.rbOwn.Location = new System.Drawing.Point(8, 7);
            this.rbOwn.Name = "rbOwn";
            this.rbOwn.Size = new System.Drawing.Size(70, 17);
            this.rbOwn.TabIndex = 13;
            this.rbOwn.TabStop = true;
            this.rbOwn.Text = "Ведущий";
            this.rbOwn.UseVisualStyleBackColor = true;
            // 
            // cmbTypeFFT
            // 
            this.cmbTypeFFT.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbTypeFFT.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTypeFFT.FormattingEnabled = true;
            this.cmbTypeFFT.Items.AddRange(new object[] {
            "3052",
            "6104",
            "12208"});
            this.cmbTypeFFT.Location = new System.Drawing.Point(117, 239);
            this.cmbTypeFFT.Name = "cmbTypeFFT";
            this.cmbTypeFFT.Size = new System.Drawing.Size(67, 21);
            this.cmbTypeFFT.TabIndex = 26;
            // 
            // lAccuracyFFT
            // 
            this.lAccuracyFFT.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lAccuracyFFT.AutoSize = true;
            this.lAccuracyFFT.ForeColor = System.Drawing.Color.Black;
            this.lAccuracyFFT.Location = new System.Drawing.Point(3, 247);
            this.lAccuracyFFT.Name = "lAccuracyFFT";
            this.lAccuracyFFT.Size = new System.Drawing.Size(137, 13);
            this.lAccuracyFFT.TabIndex = 25;
            this.lAccuracyFFT.Text = "Точность БПФ, Гц............";
            // 
            // pParamSignal
            // 
            this.pParamSignal.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pParamSignal.Controls.Add(this.nudFreqMax);
            this.pParamSignal.Controls.Add(this.lFreqMax);
            this.pParamSignal.Controls.Add(this.nudFreqMin);
            this.pParamSignal.Controls.Add(this.nudStep);
            this.pParamSignal.Controls.Add(this.nudLevel);
            this.pParamSignal.Controls.Add(this.lStep);
            this.pParamSignal.Controls.Add(this.lLevel);
            this.pParamSignal.Controls.Add(this.lFreqMin);
            this.pParamSignal.Location = new System.Drawing.Point(1, 41);
            this.pParamSignal.Name = "pParamSignal";
            this.pParamSignal.Size = new System.Drawing.Size(189, 93);
            this.pParamSignal.TabIndex = 15;
            // 
            // nudFreqMax
            // 
            this.nudFreqMax.DecimalPlaces = 1;
            this.nudFreqMax.Location = new System.Drawing.Point(109, 25);
            this.nudFreqMax.Maximum = new decimal(new int[] {
            3000000,
            0,
            0,
            0});
            this.nudFreqMax.Minimum = new decimal(new int[] {
            30000,
            0,
            0,
            0});
            this.nudFreqMax.Name = "nudFreqMax";
            this.nudFreqMax.Size = new System.Drawing.Size(76, 20);
            this.nudFreqMax.TabIndex = 17;
            this.nudFreqMax.ThousandsSeparator = true;
            this.nudFreqMax.Value = new decimal(new int[] {
            50000,
            0,
            0,
            0});
            // 
            // lFreqMax
            // 
            this.lFreqMax.AutoSize = true;
            this.lFreqMax.Location = new System.Drawing.Point(1, 31);
            this.lFreqMax.Name = "lFreqMax";
            this.lFreqMax.Size = new System.Drawing.Size(174, 13);
            this.lFreqMax.TabIndex = 16;
            this.lFreqMax.Text = "Частота макс., кГц.......................";
            // 
            // nudFreqMin
            // 
            this.nudFreqMin.DecimalPlaces = 1;
            this.nudFreqMin.Location = new System.Drawing.Point(109, 3);
            this.nudFreqMin.Maximum = new decimal(new int[] {
            3000000,
            0,
            0,
            0});
            this.nudFreqMin.Minimum = new decimal(new int[] {
            30000,
            0,
            0,
            0});
            this.nudFreqMin.Name = "nudFreqMin";
            this.nudFreqMin.Size = new System.Drawing.Size(76, 20);
            this.nudFreqMin.TabIndex = 9;
            this.nudFreqMin.ThousandsSeparator = true;
            this.nudFreqMin.Value = new decimal(new int[] {
            30000,
            0,
            0,
            0});
            // 
            // nudStep
            // 
            this.nudStep.AutoSize = true;
            this.nudStep.Location = new System.Drawing.Point(109, 69);
            this.nudStep.Maximum = new decimal(new int[] {
            30000,
            0,
            0,
            0});
            this.nudStep.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudStep.Name = "nudStep";
            this.nudStep.Size = new System.Drawing.Size(76, 20);
            this.nudStep.TabIndex = 13;
            this.nudStep.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // nudLevel
            // 
            this.nudLevel.AutoSize = true;
            this.nudLevel.Location = new System.Drawing.Point(109, 47);
            this.nudLevel.Maximum = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.nudLevel.Minimum = new decimal(new int[] {
            130,
            0,
            0,
            -2147483648});
            this.nudLevel.Name = "nudLevel";
            this.nudLevel.Size = new System.Drawing.Size(76, 20);
            this.nudLevel.TabIndex = 11;
            this.nudLevel.Value = new decimal(new int[] {
            80,
            0,
            0,
            -2147483648});
            // 
            // lStep
            // 
            this.lStep.AutoSize = true;
            this.lStep.Location = new System.Drawing.Point(4, 76);
            this.lStep.Name = "lStep";
            this.lStep.Size = new System.Drawing.Size(152, 13);
            this.lStep.TabIndex = 12;
            this.lStep.Text = "Шаг сетки, кГц.......................";
            // 
            // lLevel
            // 
            this.lLevel.AutoSize = true;
            this.lLevel.Location = new System.Drawing.Point(3, 53);
            this.lLevel.Name = "lLevel";
            this.lLevel.Size = new System.Drawing.Size(126, 13);
            this.lLevel.TabIndex = 10;
            this.lLevel.Text = "Порог, дБ.......................";
            // 
            // lFreqMin
            // 
            this.lFreqMin.AutoSize = true;
            this.lFreqMin.Location = new System.Drawing.Point(1, 9);
            this.lFreqMin.Name = "lFreqMin";
            this.lFreqMin.Size = new System.Drawing.Size(168, 13);
            this.lFreqMin.TabIndex = 8;
            this.lFreqMin.Text = "Частота мин., кГц.......................";
            // 
            // grbParamHind
            // 
            this.grbParamHind.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grbParamHind.Controls.Add(this.cmbManipulation);
            this.grbParamHind.Controls.Add(this.lManipulation);
            this.grbParamHind.Controls.Add(this.cmbDeviation);
            this.grbParamHind.Controls.Add(this.lDeviation);
            this.grbParamHind.Controls.Add(this.cmbModulation);
            this.grbParamHind.Controls.Add(this.lTypeModulation);
            this.grbParamHind.ForeColor = System.Drawing.Color.Blue;
            this.grbParamHind.Location = new System.Drawing.Point(2, 133);
            this.grbParamHind.Name = "grbParamHind";
            this.grbParamHind.Size = new System.Drawing.Size(186, 82);
            this.grbParamHind.TabIndex = 4;
            this.grbParamHind.TabStop = false;
            this.grbParamHind.Text = "Параметры помехи";
            // 
            // cmbManipulation
            // 
            this.cmbManipulation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbManipulation.FormattingEnabled = true;
            this.cmbManipulation.Location = new System.Drawing.Point(115, 57);
            this.cmbManipulation.Name = "cmbManipulation";
            this.cmbManipulation.Size = new System.Drawing.Size(67, 21);
            this.cmbManipulation.TabIndex = 7;
            // 
            // lManipulation
            // 
            this.lManipulation.AutoSize = true;
            this.lManipulation.ForeColor = System.Drawing.Color.Black;
            this.lManipulation.Location = new System.Drawing.Point(4, 65);
            this.lManipulation.Name = "lManipulation";
            this.lManipulation.Size = new System.Drawing.Size(170, 13);
            this.lManipulation.TabIndex = 6;
            this.lManipulation.Text = "Манипуляция, мкс.......................";
            // 
            // cmbDeviation
            // 
            this.cmbDeviation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDeviation.FormattingEnabled = true;
            this.cmbDeviation.Location = new System.Drawing.Point(115, 34);
            this.cmbDeviation.Name = "cmbDeviation";
            this.cmbDeviation.Size = new System.Drawing.Size(67, 21);
            this.cmbDeviation.TabIndex = 5;
            // 
            // lDeviation
            // 
            this.lDeviation.AutoSize = true;
            this.lDeviation.ForeColor = System.Drawing.Color.Black;
            this.lDeviation.Location = new System.Drawing.Point(3, 42);
            this.lDeviation.Name = "lDeviation";
            this.lDeviation.Size = new System.Drawing.Size(174, 13);
            this.lDeviation.TabIndex = 4;
            this.lDeviation.Text = "Девиация(+/-), кГц........................";
            // 
            // cmbModulation
            // 
            this.cmbModulation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbModulation.FormattingEnabled = true;
            this.cmbModulation.Location = new System.Drawing.Point(115, 11);
            this.cmbModulation.Name = "cmbModulation";
            this.cmbModulation.Size = new System.Drawing.Size(67, 21);
            this.cmbModulation.TabIndex = 3;
            this.cmbModulation.SelectedIndexChanged += new System.EventHandler(this.cmbModulation_SelectedIndexChanged);
            // 
            // lTypeModulation
            // 
            this.lTypeModulation.AutoSize = true;
            this.lTypeModulation.ForeColor = System.Drawing.Color.Black;
            this.lTypeModulation.Location = new System.Drawing.Point(3, 19);
            this.lTypeModulation.Name = "lTypeModulation";
            this.lTypeModulation.Size = new System.Drawing.Size(168, 13);
            this.lTypeModulation.TabIndex = 2;
            this.lTypeModulation.Text = "Вид модуляции............................";
            // 
            // chbShowMated
            // 
            this.chbShowMated.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.chbShowMated.AutoSize = true;
            this.chbShowMated.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chbShowMated.Checked = true;
            this.chbShowMated.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chbShowMated.Location = new System.Drawing.Point(7, 24);
            this.chbShowMated.Name = "chbShowMated";
            this.chbShowMated.Size = new System.Drawing.Size(178, 17);
            this.chbShowMated.TabIndex = 24;
            this.chbShowMated.Text = "Ведомый...................................";
            this.chbShowMated.UseVisualStyleBackColor = true;
            this.chbShowMated.CheckedChanged += new System.EventHandler(this.chbShowMated_CheckedChanged);
            // 
            // tableLayoutPanelGeneral
            // 
            this.tableLayoutPanelGeneral.ColumnCount = 2;
            this.tableLayoutPanelGeneral.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelGeneral.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanelGeneral.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanelGeneral.Controls.Add(this.pParam, 1, 0);
            this.tableLayoutPanelGeneral.Controls.Add(this.tableLayoutPanel, 0, 0);
            this.tableLayoutPanelGeneral.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelGeneral.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanelGeneral.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanelGeneral.Name = "tableLayoutPanelGeneral";
            this.tableLayoutPanelGeneral.RowCount = 1;
            this.tableLayoutPanelGeneral.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelGeneral.Size = new System.Drawing.Size(884, 386);
            this.tableLayoutPanelGeneral.TabIndex = 23;
            // 
            // tableLayoutPanel
            // 
            this.tableLayoutPanel.ColumnCount = 1;
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel.Controls.Add(this.dgvIRI_PPRCh_RPLinked, 0, 1);
            this.tableLayoutPanel.Controls.Add(this.tableLayoutPanel1, 0, 2);
            this.tableLayoutPanel.Controls.Add(this.dgvIRI_PPRCh_RPOwn, 0, 0);
            this.tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel.Name = "tableLayoutPanel";
            this.tableLayoutPanel.RowCount = 3;
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel.Size = new System.Drawing.Size(684, 386);
            this.tableLayoutPanel.TabIndex = 23;
            // 
            // dgvIRI_PPRCh_RPLinked
            // 
            this.dgvIRI_PPRCh_RPLinked.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvIRI_PPRCh_RPLinked.Location = new System.Drawing.Point(3, 123);
            this.dgvIRI_PPRCh_RPLinked.Name = "dgvIRI_PPRCh_RPLinked";
            this.dgvIRI_PPRCh_RPLinked.Size = new System.Drawing.Size(678, 114);
            this.dgvIRI_PPRCh_RPLinked.TabIndex = 1;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tableLayoutPanel1.Controls.Add(this.dgvIRI_PPRCh_RP2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.dgvIRI_PPRCh_RP_EXCLUDE, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 240);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(684, 146);
            this.tableLayoutPanel1.TabIndex = 2;
            // 
            // dgvIRI_PPRCh_RP2
            // 
            this.dgvIRI_PPRCh_RP2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvIRI_PPRCh_RP2.Location = new System.Drawing.Point(3, 3);
            this.dgvIRI_PPRCh_RP2.Name = "dgvIRI_PPRCh_RP2";
            this.dgvIRI_PPRCh_RP2.Size = new System.Drawing.Size(267, 140);
            this.dgvIRI_PPRCh_RP2.TabIndex = 1;
            // 
            // dgvIRI_PPRCh_RP_EXCLUDE
            // 
            this.dgvIRI_PPRCh_RP_EXCLUDE.ContextMenuStrip = this.contMenu;
            this.dgvIRI_PPRCh_RP_EXCLUDE.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvIRI_PPRCh_RP_EXCLUDE.Location = new System.Drawing.Point(276, 3);
            this.dgvIRI_PPRCh_RP_EXCLUDE.Name = "dgvIRI_PPRCh_RP_EXCLUDE";
            this.dgvIRI_PPRCh_RP_EXCLUDE.Size = new System.Drawing.Size(405, 140);
            this.dgvIRI_PPRCh_RP_EXCLUDE.TabIndex = 2;
            // 
            // contMenu
            // 
            this.contMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiDelete});
            this.contMenu.Name = "contMenu";
            this.contMenu.Size = new System.Drawing.Size(119, 26);
            // 
            // tsmiDelete
            // 
            this.tsmiDelete.Name = "tsmiDelete";
            this.tsmiDelete.Size = new System.Drawing.Size(118, 22);
            this.tsmiDelete.Text = "Удалить";
            this.tsmiDelete.Click += new System.EventHandler(this.tsmiDelete_Click);
            // 
            // dgvIRI_PPRCh_RPOwn
            // 
            this.dgvIRI_PPRCh_RPOwn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvIRI_PPRCh_RPOwn.Location = new System.Drawing.Point(3, 3);
            this.dgvIRI_PPRCh_RPOwn.Name = "dgvIRI_PPRCh_RPOwn";
            this.dgvIRI_PPRCh_RPOwn.Size = new System.Drawing.Size(678, 114);
            this.dgvIRI_PPRCh_RPOwn.TabIndex = 0;
            // 
            // table_IRI_PPRCh_RP
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.tableLayoutPanelGeneral);
            this.Name = "table_IRI_PPRCh_RP";
            this.Size = new System.Drawing.Size(884, 386);
            this.pParam.ResumeLayout(false);
            this.pParam.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudDurIzl)).EndInit();
            this.grbFRCh.ResumeLayout(false);
            this.grbFRCh.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudWidthExclude)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudFreqExclude)).EndInit();
            this.grbRadioButtons.ResumeLayout(false);
            this.grbRadioButtons.PerformLayout();
            this.pParamSignal.ResumeLayout(false);
            this.pParamSignal.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudFreqMax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudFreqMin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudStep)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudLevel)).EndInit();
            this.grbParamHind.ResumeLayout(false);
            this.grbParamHind.PerformLayout();
            this.tableLayoutPanelGeneral.ResumeLayout(false);
            this.tableLayoutPanel.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.contMenu.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pParam;
        private System.Windows.Forms.GroupBox grbRadioButtons;
        public System.Windows.Forms.RadioButton rbMated;
        public System.Windows.Forms.RadioButton rbOwn;
        private System.Windows.Forms.Panel pParamSignal;
        public System.Windows.Forms.NumericUpDown nudFreqMin;
        public System.Windows.Forms.NumericUpDown nudStep;
        public System.Windows.Forms.NumericUpDown nudLevel;
        private System.Windows.Forms.Label lStep;
        private System.Windows.Forms.Label lLevel;
        private System.Windows.Forms.Label lFreqMin;
        public System.Windows.Forms.Button bAddParam;
        public System.Windows.Forms.Button bClear;
        private System.Windows.Forms.GroupBox grbParamHind;
        private System.Windows.Forms.Label lDuratIzl;
        public System.Windows.Forms.ComboBox cmbManipulation;
        private System.Windows.Forms.Label lManipulation;
        public System.Windows.Forms.ComboBox cmbDeviation;
        private System.Windows.Forms.Label lDeviation;
        public System.Windows.Forms.ComboBox cmbModulation;
        private System.Windows.Forms.Label lTypeModulation;
        public System.Windows.Forms.CheckBox chbShowMated;
        public System.Windows.Forms.NumericUpDown nudFreqMax;
        private System.Windows.Forms.Label lFreqMax;
        public System.Windows.Forms.ComboBox cmbTypeFFT;
        private System.Windows.Forms.Label lAccuracyFFT;
        private System.Windows.Forms.GroupBox grbFRCh;
        public System.Windows.Forms.NumericUpDown nudFreqExclude;
        private System.Windows.Forms.Label lFreq;
        public System.Windows.Forms.NumericUpDown nudWidthExclude;
        private System.Windows.Forms.Label lBand;
        public System.Windows.Forms.Button bAdd;
        public System.Windows.Forms.Button bClearFRCh;
        private MyDataGridView.myDataGridView dgvIRI_PPRCh_RPOwn;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelGeneral;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
        private MyDataGridView.myDataGridView dgvIRI_PPRCh_RPLinked;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private MyDataGridView.myDataGridView dgvIRI_PPRCh_RP2;
        private MyDataGridView.myDataGridView dgvIRI_PPRCh_RP_EXCLUDE;
        private System.Windows.Forms.ContextMenuStrip contMenu;
        private System.Windows.Forms.ToolStripMenuItem tsmiDelete;
        private System.Windows.Forms.NumericUpDown nudDurIzl;
    }
}
