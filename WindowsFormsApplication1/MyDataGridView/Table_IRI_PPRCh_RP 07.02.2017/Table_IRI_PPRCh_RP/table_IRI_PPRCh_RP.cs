﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using MyDataGridView;
using VariableDynamic;
using USR_DLL;
using VariableStatic;

namespace Table_IRI_PPRCh_RP
{
    public partial class table_IRI_PPRCh_RP: UserControl
    {
        VariableWork variableWork;
        VariableCommon variableCommon;

        TDistribFHSS_RP DistribFHSS_RP;
        TDistribFHSS_RP CurDistribFHSS_RP;

        TDistribFHSS_RPExclude DistribFHSS_RPExclude;

        List<TDistribFHSS_RP> lDistribFHSS_RPOwn;
        public IReadOnlyList<TDistribFHSS_RP> ListPPRChPublicOwn // для чтения кому-то (без возможности изменения)
        {
            get { return lDistribFHSS_RPOwn; }
        }

        List<TDistribFHSS_RP> lDistribFHSS_RPLinked;
        public IReadOnlyList<TDistribFHSS_RP> ListPPRChPublicLinked // для чтения кому-то (без возможности изменения)
        {
            get { return lDistribFHSS_RPLinked; }
        }

        List<TDistribFHSS_RPExclude> lDistribFHSS_RPExcludeOwn;
        public IReadOnlyList<TDistribFHSS_RPExclude> ListPPRChRPExcludePublicOwn // для чтения кому-то (без возможности изменения)
        {
            get { return lDistribFHSS_RPExcludeOwn; }
        }

        List<TDistribFHSS_RPExclude> lDistribFHSS_RPExcludeLinked;
        public IReadOnlyList<TDistribFHSS_RPExclude> ListPPRChRPExcludePublicLinked // для чтения кому-то (без возможности изменения)
        {
            get { return lDistribFHSS_RPExcludeLinked; }
        }

        InitParams initParams;
        Functions functions;
        FunctionsTranslate functionsTranslate;
        FunctionsDB functionsDB;
        FunctionsDGV functionsDGV;
        DecoderParams decoderParams;

        FuncDB_IRI_PPRCh_RP funcDB_IRI_PPRCh_RP;
        FuncDGV_IRI_PPRCh_RP funcDGV_IRI_PPRCh_RP;
        FuncVW_IRI_PPRCh_RP funcVW_IRI_PPRCh_RP;

        public table_IRI_PPRCh_RP()
        {
            InitializeComponent();

            InitTableIRI_PPRCh_RP();

            SetViewWindowForRole();

            initParams = new InitParams();
            functions = new Functions();
            functionsTranslate = new FunctionsTranslate();
            functionsDB = new FunctionsDB();
            functionsDGV = new FunctionsDGV();

            decoderParams = new DecoderParams();

            funcDB_IRI_PPRCh_RP = new FuncDB_IRI_PPRCh_RP();
            funcDGV_IRI_PPRCh_RP = new FuncDGV_IRI_PPRCh_RP();
            funcVW_IRI_PPRCh_RP = new FuncVW_IRI_PPRCh_RP();

            lDistribFHSS_RPOwn = new List<TDistribFHSS_RP>();
            lDistribFHSS_RPLinked = new List<TDistribFHSS_RP>();

            lDistribFHSS_RPExcludeOwn = new List<TDistribFHSS_RPExclude>();
            lDistribFHSS_RPExcludeLinked = new List<TDistribFHSS_RPExclude>();

            CurDistribFHSS_RP = new TDistribFHSS_RP();

            variableWork = new VariableWork();
            variableCommon = new VariableCommon();

            //ChangeControlLanguage(variableCommon.Language);

            //initParams.InitModulation(cmbModulation);
            //initParams.SetDeviation(cmbModulation.SelectedIndex, 0, cmbDeviation);
            //initParams.SetManipulation(cmbModulation.SelectedIndex, 6, cmbManipulation);

            cmbTypeFFT.SelectedIndex = 2;

            initParams.Init3000_6000_PPRCh_RP(variableCommon.TypeStation, nudFreqMin, nudFreqMax, nudFreqExclude);

            VariableWork.OnChangeDistribFHSS_RPOwn += new VariableWork.ChangeVariableEventHandler(VariableWork_OnChangeDistribFHSS_RPOwn);
            VariableWork.OnChangeDistribFHSS_RPLinked += new VariableWork.ChangeVariableEventHandler(VariableWork_OnChangeDistribFHSS_RPLinked);

            VariableWork.OnChangeDistribFHSS_RPExcludeLinked += new VariableWork.ChangeVariableEventHandler(VariableWork_OnChangeDistribFHSS_RPExcludeLinked);
            VariableWork.OnChangeDistribFHSS_RPExcludeOwn += new VariableWork.ChangeVariableEventHandler(VariableWork_OnChangeDistribFHSS_RPExcludeOwn);

            VariableCommon.OnChangeCommonLanguage += new VariableCommon.ChangeCommonEventHandler(VariableCommon_OnChangeCommonLanguage);
            VariableCommon.OnChangeCommonRole += new VariableCommon.ChangeCommonEventHandler(VariableCommon_OnChangeCommonRole);

            VariableWork.aWPtoBearingDSPprotocolNew.FhssRadioJamUpdate += new AWPtoBearingDSPprotocolNew.FhssRadioJamUpdateEventHandler(aWPtoBearingDSPprotocolNew_FhssRadioJamUpdate);

            VariableWork.OnChangeRegime += new VariableWork.ChangeRegimeEventHandler(VariableWork_OnChangeRegime);

            // Событие возникает при выделении строки в dgv
            dgvIRI_PPRCh_RPOwn.dgv.SelectionChanged += new EventHandler(dgv_SelectionChanged);
            dgvIRI_PPRCh_RPLinked.dgv.SelectionChanged += new EventHandler(dgv_SelectionChanged);

            // Событие возникает при нажатии на ячейку таблицы
            dgvIRI_PPRCh_RPOwn.dgv.CellClick += OnDGV_PPRCh_RPOwn_CellClick;
            dgvIRI_PPRCh_RPLinked.dgv.CellClick += OnDGV_PPRCh_RPLinked_CellClick;

           
        }

        private void OnDGV_PPRCh_RPLinked_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (InvokeRequired)
            {
                Invoke((MethodInvoker)(() => OnDGV_PPRCh_RPLinked_CellClick(sender, e)));
                return;
            }

            if (dgvIRI_PPRCh_RP2.dgv.SelectedRows.Count != 1) { return; }

            // удалить все записи из dgv
            functionsDGV.DeleteAllRecordsDGV(dgvIRI_PPRCh_RP2.dgv);
            // добавить пустые строки в dgv при необходимости
            dgvIRI_PPRCh_RP2.SetInitialNumberOfRows(dgvIRI_PPRCh_RP2.dgv, dgvIRI_PPRCh_RP2.tableIRI_PPRCh_RP2, NameTable.IRI_PPRCh_RP2);

            if (dgvIRI_PPRCh_RPLinked.dgv.Rows[dgvIRI_PPRCh_RPLinked.dgv.SelectedRows[0].Index].Cells[1].Value == null) { return; }

            try
            {
                if (rbMated.Checked)
                {
                    if (variableWork.DistribFHSS_RPLinked != null)
                    {
                        int lenLinked = variableWork.DistribFHSS_RPLinked.Length;
                        int id = Convert.ToInt32(dgvIRI_PPRCh_RPLinked.dgv.Rows[dgvIRI_PPRCh_RPLinked.dgv.SelectedRows[0].Index].Cells[0].Value);

                        TDistribFHSS_RP findDistribFHSS_RP = variableWork.DistribFHSS_RPLinked.ToList().Find(x => x.iID == id);

                        if (dgvIRI_PPRCh_RP2.dgv.InvokeRequired)
                        {
                            Invoke((MethodInvoker)(() => funcDGV_IRI_PPRCh_RP.AddRecord_IRI_PPRChFreq_RPToDGV(dgvIRI_PPRCh_RP2.dgv, findDistribFHSS_RP.iID, findDistribFHSS_RP.iFreqMin, findDistribFHSS_RP.iFreqMax, findDistribFHSS_RP.iStep * 10, Table.Linked)));
                        }
                        else
                        {
                            funcDGV_IRI_PPRCh_RP.AddRecord_IRI_PPRChFreq_RPToDGV(dgvIRI_PPRCh_RP2.dgv, findDistribFHSS_RP.iID, findDistribFHSS_RP.iFreqMin, findDistribFHSS_RP.iFreqMax, findDistribFHSS_RP.iStep * 10, Table.Linked);
                        }

                        CheckExclude(dgvIRI_PPRCh_RP2.dgv, lDistribFHSS_RPExcludeLinked, Color.Red);
                    }
                }
            }
            catch { }
        }

        private void OnDGV_PPRCh_RPOwn_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (InvokeRequired)
            {
                Invoke((MethodInvoker)(() => OnDGV_PPRCh_RPOwn_CellClick(sender, e)));
                return;
            }

            if (dgvIRI_PPRCh_RP2.dgv.SelectedRows.Count != 1) { return; }

            // удалить все записи из dgv
            functionsDGV.DeleteAllRecordsDGV(dgvIRI_PPRCh_RP2.dgv);
            // добавить пустые строки в dgv при необходимости
            dgvIRI_PPRCh_RP2.SetInitialNumberOfRows(dgvIRI_PPRCh_RP2.dgv, dgvIRI_PPRCh_RP2.tableIRI_PPRCh_RP2, NameTable.IRI_PPRCh_RP2);

            if (dgvIRI_PPRCh_RPOwn.dgv.Rows[dgvIRI_PPRCh_RPOwn.dgv.SelectedRows[0].Index].Cells[1].Value == null) { return; }

            try
            {
                if (rbOwn.Checked)
                {
                    if (variableWork.DistribFHSS_RPOwn != null)
                    {
                        int lenOwn = variableWork.DistribFHSS_RPOwn.Length;
                        int id = Convert.ToInt32(dgvIRI_PPRCh_RPOwn.dgv.Rows[dgvIRI_PPRCh_RPOwn.dgv.SelectedRows[0].Index].Cells[0].Value);

                        TDistribFHSS_RP findDistribFHSS_RP = variableWork.DistribFHSS_RPOwn.ToList().Find(x => x.iID == id);
                       
                        if (dgvIRI_PPRCh_RP2.dgv.InvokeRequired)
                        {
                            Invoke((MethodInvoker)(() => funcDGV_IRI_PPRCh_RP.AddRecord_IRI_PPRChFreq_RPToDGV(dgvIRI_PPRCh_RP2.dgv, findDistribFHSS_RP.iID, findDistribFHSS_RP.iFreqMin, findDistribFHSS_RP.iFreqMax, findDistribFHSS_RP.iStep * 10, Table.Own)));
                        }
                        else
                        {
                            funcDGV_IRI_PPRCh_RP.AddRecord_IRI_PPRChFreq_RPToDGV(dgvIRI_PPRCh_RP2.dgv, findDistribFHSS_RP.iID, findDistribFHSS_RP.iFreqMin, findDistribFHSS_RP.iFreqMax, findDistribFHSS_RP.iStep * 10, Table.Own);
                        }

                        CheckExclude(dgvIRI_PPRCh_RP2.dgv, lDistribFHSS_RPExcludeOwn, Color.Red);
                    }
                }
            }
            catch { }
        }

        void VariableCommon_OnChangeCommonRole()
        {
            SetViewWindowForRole();
        }

        /// <summary>
        /// Установить вид окна в зависимости от роли (автономная, ведущая, ведомая)
        /// </summary>
        public void SetViewWindowForRole()
        {
            try
            {
                if (variableCommon.Role == 2)
                {
                    grbRadioButtons.Visible = false;
                    chbShowMated.Visible = false;

                    tableLayoutPanel.RowStyles[1] = new RowStyle(SizeType.Absolute, 0);

                    dgvIRI_PPRCh_RPOwn.Visible = true;
                    dgvIRI_PPRCh_RPLinked.Visible = false;
                }
                else if (variableCommon.Role == 0 || variableCommon.Role == 1)
                {
                    grbRadioButtons.Visible = true;
                    chbShowMated.Visible = true;

                    tableLayoutPanel.RowStyles[1] = new RowStyle(SizeType.Absolute, 120);

                    dgvIRI_PPRCh_RPOwn.Visible = true;
                    dgvIRI_PPRCh_RPLinked.Visible = true;

                }
            }
            catch { }
        }

        public void InitParamTranslatePPRChRP()
        {
            ChangeControlLanguage(variableCommon.Language);

            initParams.InitModulation(cmbModulation);
            initParams.SetDeviation(cmbModulation.SelectedIndex, 0, cmbDeviation);
            initParams.SetManipulation(cmbModulation.SelectedIndex, 6, cmbManipulation, 0);
        }

        void VariableWork_OnChangeRegime()
        {
            if (variableWork.Regime != 6)
            {
                for (int i = 0; i < dgvIRI_PPRCh_RPOwn.dgv.RowCount; i++)
                {
                    dgvIRI_PPRCh_RPOwn.dgv.Rows[i].Cells[6].Value = Properties.Resources.empty;
                }
            }
        }

        private void aWPtoBearingDSPprotocolNew_FhssRadioJamUpdate(Protocols.FhssRadioJamUpdateEvent answer)
        {
            bool jammingState;
            try
            {
                if (variableWork.Regime == 6)
                {
                    if (answer.JammingStates.Length > 0)
                    {
                        for (int i = 0; i < answer.JammingStates.Length; i++)
                        {
                            int ind = FoundIndRowDGV(dgvIRI_PPRCh_RPOwn.dgv, answer, i);

                            dgvIRI_PPRCh_RPOwn.dgv.Rows[ind].Cells[6].Value = Properties.Resources.empty;

                            if (ind != -1)
                            {
                                jammingState = answer.JammingStates[i].IsJammed;

                                if (jammingState) { dgvIRI_PPRCh_RPOwn.dgv.Rows[ind].Cells[6].Value = Properties.Resources.green; }
                                else { dgvIRI_PPRCh_RPOwn.dgv.Rows[ind].Cells[6].Value = Properties.Resources.empty; }
                            }
                        }
                    }
                }
            }
            catch { }
        }

        public int FoundIndRowDGV(DataGridView dgv, Protocols.FhssRadioJamUpdateEvent answer, int iNumAnswer)
        {
            int iNum = 0;

            try
            {
                for (int i = 0; i < dgv.Rows.Count; i++)
                {
                    if (dgv[0, i].Value.ToString() == Convert.ToString(answer.JammingStates[iNumAnswer].Id))
                    {
                        iNum = dgv[0, i].RowIndex;
                        return iNum;
                    }
                }
            }
            catch { }

            return -1;
        }

        private void VariableCommon_OnChangeCommonLanguage()
        {
            VariableCommon variableCommon = new VariableCommon();
            switch (variableCommon.Language)
            {
                case 0:
                    functionsTranslate.SetLanguage("rus");
                    ChangeLanguage();
                    break;

                case 1:
                    functionsTranslate.SetLanguage("eng");
                    ChangeLanguage();
                    break;

                case 2:
                    functionsTranslate.SetLanguage("az");
                    ChangeLanguage();
                    break;

                default:
                    functionsTranslate.SetLanguage("rus");
                    ChangeLanguage();
                    break;
            }
        }

        /// <summary>
        /// Для инициализации языка при запуске приложения 
        /// </summary>
        /// <param name="bLanguage"></param>
        public void ChangeControlLanguage(byte bLanguage)
        {

            switch (bLanguage)
            {
                case 0:
                    functionsTranslate.SetLanguage("rus");
                    ChangeLanguage();
                    break;

                case 1:
                    functionsTranslate.SetLanguage("eng");
                    ChangeLanguage();
                    break;

                case 2:
                    functionsTranslate.SetLanguage("az");
                    ChangeLanguage();
                    break;

                default:
                    functionsTranslate.SetLanguage("rus");
                    ChangeLanguage();
                    break;
            }
        }

        /// <summary>
        /// Изменение языка интерфейса
        /// </summary>
        private void ChangeLanguage()
        {
            functionsTranslate.RenameRadioButtons(functionsTranslate.Dictionary, grbRadioButtons);
            functionsTranslate.RenameCheckBoxes(functionsTranslate.Dictionary, pParam);
            functionsTranslate.RenameGroupBoxes(functionsTranslate.Dictionary, pParam);
            functionsTranslate.RenameLabels(functionsTranslate.Dictionary, grbParamHind);
            functionsTranslate.RenameLabels(functionsTranslate.Dictionary, grbFRCh);
            functionsTranslate.RenameLabels(functionsTranslate.Dictionary, pParamSignal);
            functionsTranslate.RenameLabels(functionsTranslate.Dictionary, pParam);
            functionsTranslate.RenameButtons(functionsTranslate.Dictionary, pParam);
            functionsTranslate.RenameButtons(functionsTranslate.Dictionary, grbFRCh);
            functionsTranslate.RenameFieldsTables(functionsTranslate.Dictionary, dgvIRI_PPRCh_RP_EXCLUDE.dgv);
            functionsTranslate.RenameFieldsTables(functionsTranslate.Dictionary, dgvIRI_PPRCh_RP2.dgv);
            functionsTranslate.RenameFieldsTables(functionsTranslate.Dictionary, dgvIRI_PPRCh_RPLinked.dgv);
            functionsTranslate.RenameFieldsTables(functionsTranslate.Dictionary, dgvIRI_PPRCh_RPOwn.dgv);

            initParams.InitModulation(cmbModulation);
            VariableWork_OnChangeDistribFHSS_RPOwn();
            VariableWork_OnChangeDistribFHSS_RPLinked();
        }

        void VariableWork_OnChangeDistribFHSS_RPExcludeOwn()
        {
            if (rbOwn.Checked)
            {
                if (variableWork.DistribFHSS_RPExcludeOwn != null)
                {
                    int len = variableWork.DistribFHSS_RPExcludeOwn.Length;

                    DistribFHSS_RPExclude = new TDistribFHSS_RPExclude();

                    if (functionsDB.DeleteAllRecordsDB(NameTable.IRI_PPRCh_RP_EXCLUDE, Table.Own))
                    {
                        if (dgvIRI_PPRCh_RP_EXCLUDE.dgv.InvokeRequired)
                        {
                            Invoke((MethodInvoker)(() =>
                            {
                                functionsDGV.DeleteAllRecordsDGV(dgvIRI_PPRCh_RP_EXCLUDE.dgv);
                                dgvIRI_PPRCh_RP_EXCLUDE.SetInitialNumberOfRows(dgvIRI_PPRCh_RP_EXCLUDE.dgv, dgvIRI_PPRCh_RP_EXCLUDE.tableIRI_PPRCh_RP_Exclude, NameTable.IRI_PPRCh_RP_EXCLUDE);
                            }));
                        }
                        else
                        {
                            functionsDGV.DeleteAllRecordsDGV(dgvIRI_PPRCh_RP_EXCLUDE.dgv);
                            dgvIRI_PPRCh_RP_EXCLUDE.SetInitialNumberOfRows(dgvIRI_PPRCh_RP_EXCLUDE.dgv, dgvIRI_PPRCh_RP_EXCLUDE.tableIRI_PPRCh_RP_Exclude, NameTable.IRI_PPRCh_RP_EXCLUDE);
                        }
                    }

                    for (int i = 0; i < len; i++)
                    {
                        DistribFHSS_RPExclude.iID = variableWork.DistribFHSS_RPExcludeOwn[i].iID;
                        DistribFHSS_RPExclude.iFreqExclude = variableWork.DistribFHSS_RPExcludeOwn[i].iFreqExclude;
                        DistribFHSS_RPExclude.iWidthExclude = variableWork.DistribFHSS_RPExcludeOwn[i].iWidthExclude;

                        if (funcDB_IRI_PPRCh_RP.AddRecord_IRI_PPRCh_RPExcludeToDB(NameTable.IRI_PPRCh_RP_EXCLUDE, DistribFHSS_RPExclude, Table.Own))
                        {
                            if (dgvIRI_PPRCh_RP_EXCLUDE.dgv.InvokeRequired)
                            {
                                Invoke((MethodInvoker)(() => funcDGV_IRI_PPRCh_RP.AddRecord_IRI_PPRCh_RPExcludeToDGV(dgvIRI_PPRCh_RP_EXCLUDE.dgv, NameTable.IRI_PPRCh_RP_EXCLUDE, DistribFHSS_RPExclude, Table.Own)));
                            }
                            else
                            {
                                funcDGV_IRI_PPRCh_RP.AddRecord_IRI_PPRCh_RPExcludeToDGV(dgvIRI_PPRCh_RP_EXCLUDE.dgv, NameTable.IRI_PPRCh_RP_EXCLUDE, DistribFHSS_RPExclude, Table.Own);
                            }
                        }
                    }

                    lDistribFHSS_RPExcludeOwn = new List<TDistribFHSS_RPExclude>();
                    lDistribFHSS_RPExcludeOwn = variableWork.DistribFHSS_RPExcludeOwn.ToList();

                    CheckExclude(dgvIRI_PPRCh_RP2.dgv, lDistribFHSS_RPExcludeOwn, Color.Red);
                }
            }
        }

        void VariableWork_OnChangeDistribFHSS_RPExcludeLinked()
        {
            if (rbMated.Checked)
            {
                if (variableWork.DistribFHSS_RPExcludeLinked != null)
                {
                    int len = variableWork.DistribFHSS_RPExcludeLinked.Length;

                    DistribFHSS_RPExclude = new TDistribFHSS_RPExclude();

                    if (functionsDB.DeleteAllRecordsDB(NameTable.IRI_PPRCh_RP_EXCLUDE, Table.Linked))
                    {
                        if (dgvIRI_PPRCh_RP_EXCLUDE.dgv.InvokeRequired)
                        {
                            Invoke((MethodInvoker)(() =>
                            {
                                functionsDGV.DeleteAllRecordsDGV(dgvIRI_PPRCh_RP_EXCLUDE.dgv);
                                dgvIRI_PPRCh_RP_EXCLUDE.SetInitialNumberOfRows(dgvIRI_PPRCh_RP_EXCLUDE.dgv, dgvIRI_PPRCh_RP_EXCLUDE.tableIRI_PPRCh_RP_Exclude, NameTable.IRI_PPRCh_RP_EXCLUDE);

                            }));
                        }
                        else
                        {
                            functionsDGV.DeleteAllRecordsDGV(dgvIRI_PPRCh_RP_EXCLUDE.dgv);
                            dgvIRI_PPRCh_RP_EXCLUDE.SetInitialNumberOfRows(dgvIRI_PPRCh_RP_EXCLUDE.dgv, dgvIRI_PPRCh_RP_EXCLUDE.tableIRI_PPRCh_RP_Exclude, NameTable.IRI_PPRCh_RP_EXCLUDE);

                        }
                    }
                   
                    for (int i = 0; i < len; i++)
                    {
                        DistribFHSS_RPExclude.iID = variableWork.DistribFHSS_RPExcludeLinked[i].iID;
                        DistribFHSS_RPExclude.iFreqExclude = variableWork.DistribFHSS_RPExcludeLinked[i].iFreqExclude;
                        DistribFHSS_RPExclude.iWidthExclude = variableWork.DistribFHSS_RPExcludeLinked[i].iWidthExclude;

                        if (funcDB_IRI_PPRCh_RP.AddRecord_IRI_PPRCh_RPExcludeToDB(NameTable.IRI_PPRCh_RP_EXCLUDE, DistribFHSS_RPExclude, Table.Linked))
                        {
                            if (dgvIRI_PPRCh_RP_EXCLUDE.dgv.InvokeRequired)
                            {
                                Invoke((MethodInvoker)(() => funcDGV_IRI_PPRCh_RP.AddRecord_IRI_PPRCh_RPExcludeToDGV(dgvIRI_PPRCh_RP_EXCLUDE.dgv, NameTable.IRI_PPRCh_RP_EXCLUDE, DistribFHSS_RPExclude, Table.Linked)));
                            }
                            else
                            {
                                funcDGV_IRI_PPRCh_RP.AddRecord_IRI_PPRCh_RPExcludeToDGV(dgvIRI_PPRCh_RP_EXCLUDE.dgv, NameTable.IRI_PPRCh_RP_EXCLUDE, DistribFHSS_RPExclude, Table.Linked);
                            }
                        }
                    }

                    lDistribFHSS_RPExcludeLinked = new List<TDistribFHSS_RPExclude>();
                    lDistribFHSS_RPExcludeLinked = variableWork.DistribFHSS_RPExcludeLinked.ToList();

                    CheckExclude(dgvIRI_PPRCh_RP2.dgv, lDistribFHSS_RPExcludeLinked, Color.Red);
                }
            }
        }

        void VariableWork_OnChangeDistribFHSS_RPLinked()
        {
            if (variableWork.DistribFHSS_RPLinked != null)
            {
                int len = variableWork.DistribFHSS_RPLinked.Length;

                DistribFHSS_RP = new TDistribFHSS_RP();

                if (functionsDB.DeleteAllRecordsDB(NameTable.IRI_PPRCh_RP, Table.Linked))
                {
                    if (dgvIRI_PPRCh_RPLinked.dgv.InvokeRequired)
                    {
                        Invoke((MethodInvoker)(() =>
                        {
                            functionsDGV.DeleteAllRecordsDGV(dgvIRI_PPRCh_RPLinked.dgv);
                            dgvIRI_PPRCh_RPLinked.SetInitialNumberOfRows(dgvIRI_PPRCh_RPLinked.dgv, dgvIRI_PPRCh_RPLinked.tableIRI_PPRCh_RP, NameTable.IRI_PPRCh_RP);
                        }));
                    }
                    else
                    {
                        functionsDGV.DeleteAllRecordsDGV(dgvIRI_PPRCh_RPLinked.dgv);
                        dgvIRI_PPRCh_RPLinked.SetInitialNumberOfRows(dgvIRI_PPRCh_RPLinked.dgv, dgvIRI_PPRCh_RPLinked.tableIRI_PPRCh_RP, NameTable.IRI_PPRCh_RP);
                    }
                }

                for (int i = 0; i < len; i++)
                {
                    DistribFHSS_RP.iID = i + 1;// variableWork.DistribFHSS_RPLinked[i].iID;
                    DistribFHSS_RP.iFreqMin = variableWork.DistribFHSS_RPLinked[i].iFreqMin;
                    DistribFHSS_RP.iFreqMax = variableWork.DistribFHSS_RPLinked[i].iFreqMax;
                    DistribFHSS_RP.bLetter = variableWork.DistribFHSS_RPLinked[i].bLetter;
                    DistribFHSS_RP.sLevel = variableWork.DistribFHSS_RPLinked[i].sLevel;
                    DistribFHSS_RP.iStep = variableWork.DistribFHSS_RPLinked[i].iStep;
                    DistribFHSS_RP.bModulation = variableWork.DistribFHSS_RPLinked[i].bModulation;
                    DistribFHSS_RP.bDeviation = variableWork.DistribFHSS_RPLinked[i].bDeviation;
                    DistribFHSS_RP.bManipulation = variableWork.DistribFHSS_RPLinked[i].bManipulation;
                    DistribFHSS_RP.bDuration = variableWork.DistribFHSS_RPLinked[i].bDuration;
                    DistribFHSS_RP.iDuration = variableWork.DistribFHSS_RPLinked[i].iDuration;
                    DistribFHSS_RP.bCodeFFT = variableWork.DistribFHSS_RPLinked[i].bCodeFFT;

                    if (funcDB_IRI_PPRCh_RP.AddRecord_IRI_PPRCh_RPToDB(NameTable.IRI_PPRCh_RP, DistribFHSS_RP, Table.Linked))
                    {
                        if (dgvIRI_PPRCh_RPLinked.dgv.InvokeRequired)
                        {
                            Invoke((MethodInvoker)(() => funcDGV_IRI_PPRCh_RP.AddRecord_IRI_PPRCh_RPToDGV(dgvIRI_PPRCh_RPLinked.dgv, NameTable.IRI_PPRCh_RP, DistribFHSS_RP, Table.Linked)));
                        }
                        else
                        {
                            funcDGV_IRI_PPRCh_RP.AddRecord_IRI_PPRCh_RPToDGV(dgvIRI_PPRCh_RPLinked.dgv, NameTable.IRI_PPRCh_RP, DistribFHSS_RP, Table.Linked);
                        }
                    }

                    if (rbMated.Checked)
                    {
                        if (dgvIRI_PPRCh_RP2.dgv.InvokeRequired)
                        {
                            Invoke((MethodInvoker)(() =>
                            {
                                functionsDGV.DeleteAllRecordsDGV(dgvIRI_PPRCh_RP2.dgv);
                                dgvIRI_PPRCh_RP2.SetInitialNumberOfRows(dgvIRI_PPRCh_RP2.dgv, dgvIRI_PPRCh_RP2.tableIRI_PPRCh_RP2, NameTable.IRI_PPRCh_RP2);
                            }));
                        }
                        else
                        {
                            functionsDGV.DeleteAllRecordsDGV(dgvIRI_PPRCh_RP2.dgv);
                            dgvIRI_PPRCh_RP2.SetInitialNumberOfRows(dgvIRI_PPRCh_RP2.dgv, dgvIRI_PPRCh_RP2.tableIRI_PPRCh_RP2, NameTable.IRI_PPRCh_RP2);
                        }

                        if (variableWork.DistribFHSS_RPExcludeLinked != null)
                        {
                            if (dgvIRI_PPRCh_RP2.dgv.InvokeRequired)
                            {
                                Invoke((MethodInvoker)(() => funcDGV_IRI_PPRCh_RP.AddRecord_IRI_PPRChFreq_RPToDGV(dgvIRI_PPRCh_RP2.dgv, variableWork.DistribFHSS_RPLinked[0].iID, variableWork.DistribFHSS_RPLinked[0].iFreqMin, variableWork.DistribFHSS_RPLinked[0].iFreqMax, variableWork.DistribFHSS_RPLinked[0].iStep * 10, Table.Linked)));
                            }
                            else
                            {
                                funcDGV_IRI_PPRCh_RP.AddRecord_IRI_PPRChFreq_RPToDGV(dgvIRI_PPRCh_RP2.dgv, variableWork.DistribFHSS_RPLinked[0].iID, variableWork.DistribFHSS_RPLinked[0].iFreqMin, variableWork.DistribFHSS_RPLinked[0].iFreqMax, variableWork.DistribFHSS_RPLinked[0].iStep * 10, Table.Linked);
                            }
                            //if (dgvIRI_PPRCh_RP2.dgv.InvokeRequired)
                            //{
                            //    Invoke((MethodInvoker)(() => funcDGV_IRI_PPRCh_RP.AddRecord_IRI_PPRChFreq_RPToDGV(dgvIRI_PPRCh_RP2.dgv, DistribFHSS_RP.iID, DistribFHSS_RP.iFreqMin, DistribFHSS_RP.iFreqMax, DistribFHSS_RP.iStep * 10, Table.Linked)));
                            //}
                            //else
                            //{
                            //    funcDGV_IRI_PPRCh_RP.AddRecord_IRI_PPRChFreq_RPToDGV(dgvIRI_PPRCh_RP2.dgv, DistribFHSS_RP.iID, DistribFHSS_RP.iFreqMin, DistribFHSS_RP.iFreqMax, DistribFHSS_RP.iStep * 10, Table.Linked);
                            //}
                        }
                    }
                }

                CheckExclude(dgvIRI_PPRCh_RP2.dgv, lDistribFHSS_RPExcludeLinked, Color.Red);
            }
        }

        void VariableWork_OnChangeDistribFHSS_RPOwn()
        {
            if (variableWork.DistribFHSS_RPOwn != null)
            {
                int len = variableWork.DistribFHSS_RPOwn.Length;

                DistribFHSS_RP = new TDistribFHSS_RP();

                if (functionsDB.DeleteAllRecordsDB(NameTable.IRI_PPRCh_RP, Table.Own))
                {
                    if (dgvIRI_PPRCh_RPOwn.dgv.InvokeRequired)
                    {
                        Invoke((MethodInvoker)(() =>
                        {
                            functionsDGV.DeleteAllRecordsDGV(dgvIRI_PPRCh_RPOwn.dgv);
                            dgvIRI_PPRCh_RPOwn.SetInitialNumberOfRows(dgvIRI_PPRCh_RPOwn.dgv, dgvIRI_PPRCh_RPOwn.tableIRI_PPRCh_RP, NameTable.IRI_PPRCh_RP);
                        }));
                    }
                    else
                    {
                        functionsDGV.DeleteAllRecordsDGV(dgvIRI_PPRCh_RPOwn.dgv);
                        dgvIRI_PPRCh_RPOwn.SetInitialNumberOfRows(dgvIRI_PPRCh_RPOwn.dgv, dgvIRI_PPRCh_RPOwn.tableIRI_PPRCh_RP, NameTable.IRI_PPRCh_RP);
                    }
                }
                
                for (int i = 0; i < len; i++)
                {
                    DistribFHSS_RP.iID = variableWork.DistribFHSS_RPOwn[i].iID;//i + 1;
                    DistribFHSS_RP.iFreqMin = variableWork.DistribFHSS_RPOwn[i].iFreqMin;
                    DistribFHSS_RP.iFreqMax = variableWork.DistribFHSS_RPOwn[i].iFreqMax;
                    DistribFHSS_RP.bLetter = variableWork.DistribFHSS_RPOwn[i].bLetter;
                    DistribFHSS_RP.sLevel = variableWork.DistribFHSS_RPOwn[i].sLevel;
                    DistribFHSS_RP.iStep = variableWork.DistribFHSS_RPOwn[i].iStep;
                    DistribFHSS_RP.bModulation = variableWork.DistribFHSS_RPOwn[i].bModulation;
                    DistribFHSS_RP.bDeviation = variableWork.DistribFHSS_RPOwn[i].bDeviation;
                    DistribFHSS_RP.bManipulation = variableWork.DistribFHSS_RPOwn[i].bManipulation;
                    DistribFHSS_RP.bDuration = variableWork.DistribFHSS_RPOwn[i].bDuration;
                    DistribFHSS_RP.iDuration = variableWork.DistribFHSS_RPOwn[i].iDuration;
                    DistribFHSS_RP.bCodeFFT = variableWork.DistribFHSS_RPOwn[i].bCodeFFT;

                    if (funcDB_IRI_PPRCh_RP.AddRecord_IRI_PPRCh_RPToDB(NameTable.IRI_PPRCh_RP, DistribFHSS_RP, Table.Own))
                    {
                        if (dgvIRI_PPRCh_RPOwn.dgv.InvokeRequired)
                        {
                            Invoke((MethodInvoker)(() => funcDGV_IRI_PPRCh_RP.AddRecord_IRI_PPRCh_RPToDGV(dgvIRI_PPRCh_RPOwn.dgv, NameTable.IRI_PPRCh_RP, DistribFHSS_RP, Table.Own)));
                        }
                        else
                        {
                            funcDGV_IRI_PPRCh_RP.AddRecord_IRI_PPRCh_RPToDGV(dgvIRI_PPRCh_RPOwn.dgv, NameTable.IRI_PPRCh_RP, DistribFHSS_RP, Table.Own);
                        }
                    }

                    if (rbOwn.Checked)
                    {
                        if (dgvIRI_PPRCh_RP2.dgv.InvokeRequired)
                        {
                            Invoke((MethodInvoker)(() =>
                            {
                                functionsDGV.DeleteAllRecordsDGV(dgvIRI_PPRCh_RP2.dgv);
                                dgvIRI_PPRCh_RP2.SetInitialNumberOfRows(dgvIRI_PPRCh_RP2.dgv, dgvIRI_PPRCh_RP2.tableIRI_PPRCh_RP2, NameTable.IRI_PPRCh_RP2);
                            }));
                        }
                        else
                        {
                            functionsDGV.DeleteAllRecordsDGV(dgvIRI_PPRCh_RP2.dgv);
                            dgvIRI_PPRCh_RP2.SetInitialNumberOfRows(dgvIRI_PPRCh_RP2.dgv, dgvIRI_PPRCh_RP2.tableIRI_PPRCh_RP2, NameTable.IRI_PPRCh_RP2);
                        }

                        if (variableWork.DistribFHSS_RPExcludeOwn != null)
                        {
                            if (dgvIRI_PPRCh_RP2.dgv.InvokeRequired)
                            {
                                Invoke((MethodInvoker)(() => funcDGV_IRI_PPRCh_RP.AddRecord_IRI_PPRChFreq_RPToDGV(dgvIRI_PPRCh_RP2.dgv, variableWork.DistribFHSS_RPOwn[0].iID, variableWork.DistribFHSS_RPOwn[0].iFreqMin, variableWork.DistribFHSS_RPOwn[0].iFreqMax, variableWork.DistribFHSS_RPOwn[0].iStep * 10, Table.Own)));
                            }
                            else
                            {
                                funcDGV_IRI_PPRCh_RP.AddRecord_IRI_PPRChFreq_RPToDGV(dgvIRI_PPRCh_RP2.dgv, variableWork.DistribFHSS_RPOwn[0].iID, variableWork.DistribFHSS_RPOwn[0].iFreqMin, variableWork.DistribFHSS_RPOwn[0].iFreqMax, variableWork.DistribFHSS_RPOwn[0].iStep * 10, Table.Own);
                            }
                            //if (dgvIRI_PPRCh_RP2.dgv.InvokeRequired)
                            //{
                            //    Invoke((MethodInvoker)(() => funcDGV_IRI_PPRCh_RP.AddRecord_IRI_PPRChFreq_RPToDGV(dgvIRI_PPRCh_RP2.dgv, DistribFHSS_RP.iID, DistribFHSS_RP.iFreqMin, DistribFHSS_RP.iFreqMax, DistribFHSS_RP.iStep * 10, Table.Own)));
                            //}
                            //else
                            //{
                            //    funcDGV_IRI_PPRCh_RP.AddRecord_IRI_PPRChFreq_RPToDGV(dgvIRI_PPRCh_RP2.dgv, DistribFHSS_RP.iID, DistribFHSS_RP.iFreqMin, DistribFHSS_RP.iFreqMax, DistribFHSS_RP.iStep * 10, Table.Own);
                            //}
                        }
                    }

                    CheckExclude(dgvIRI_PPRCh_RP2.dgv, lDistribFHSS_RPExcludeOwn, Color.Red);

                }
            }
        }

        private void dgv_SelectionChanged(object sender, EventArgs e)
        {
            if (rbOwn.Checked)
            {
                if (dgvIRI_PPRCh_RPOwn.dgv.SelectedRows.Count > 0)
                {
                    if (dgvIRI_PPRCh_RPOwn.dgv.Rows[dgvIRI_PPRCh_RPOwn.dgv.SelectedRows[0].Index].Cells[0].Value != null)
                    {
                        try
                        {
                            SetParams(dgvIRI_PPRCh_RPOwn.dgv);
                        }
                        catch (System.Exception)
                        {
                            return;
                        }
                    }
                }
            }
            else if (rbMated.Checked)
            {
                if (dgvIRI_PPRCh_RPLinked.dgv.SelectedRows.Count > 0)
                {
                    if (dgvIRI_PPRCh_RPLinked.dgv.Rows[dgvIRI_PPRCh_RPLinked.dgv.SelectedRows[0].Index].Cells[0].Value != null)
                    {
                        try
                        {
                            SetParams(dgvIRI_PPRCh_RPLinked.dgv);
                        }
                        catch (System.Exception)
                        {
                            return;
                        }
                    }
                }
            }
        }

        public void LoadTableIRI_PPRCh_RPfromDB()
        {
            funcDB_IRI_PPRCh_RP.LoadIRI_PPRCh_RPExcludeFromDBToVW(NameTable.IRI_PPRCh_RP_EXCLUDE, Table.Own);
            funcDB_IRI_PPRCh_RP.LoadIRI_PPRCh_RPExcludeFromDBToVW(NameTable.IRI_PPRCh_RP_EXCLUDE, Table.Linked);

            funcDB_IRI_PPRCh_RP.LoadTableIRI_PPRCh_RPFromDB(NameTable.IRI_PPRCh_RP, Table.Own);
            funcDB_IRI_PPRCh_RP.LoadTableIRI_PPRCh_RPFromDB(NameTable.IRI_PPRCh_RP, Table.Linked);
        }

        public void InitTableIRI_PPRCh_RP()
        {
            dgvIRI_PPRCh_RPOwn.InitTableDB(dgvIRI_PPRCh_RPOwn.dgv, NameTable.IRI_PPRCh_RP);
            dgvIRI_PPRCh_RP2.InitTableDB(dgvIRI_PPRCh_RP2.dgv, NameTable.IRI_PPRCh_RP2);
            dgvIRI_PPRCh_RP_EXCLUDE.InitTableDB(dgvIRI_PPRCh_RP_EXCLUDE.dgv, NameTable.IRI_PPRCh_RP_EXCLUDE);

            dgvIRI_PPRCh_RPLinked.InitTableDB(dgvIRI_PPRCh_RPLinked.dgv, NameTable.IRI_PPRCh_RP);
        }

        /// <summary>
        /// Установка параметров в элементы при выделении строки в dgv
        /// </summary>
        /// <param name="myDGV"></param>
        private void SetParams(DataGridView dgv)
        {
            try
            {
                // ID строки
                CurDistribFHSS_RP.iID = Convert.ToInt32(dgv.Rows[dgv.SelectedRows[0].Index].Cells[0].Value);
            }
            catch { }

            try
            {
                // значение частоты min
                decimal dFmin = Convert.ToDecimal(dgv.Rows[dgv.SelectedRows[0].Index].Cells[1].Value, Functions.format);
                string sFmin1 = functions.FreqToStr((int)(dFmin * 10));
                string sFmin2 = functions.StrToFreq(sFmin1);


                nudFreqMin.Value = Convert.ToDecimal(sFmin2, Functions.format);
                CurDistribFHSS_RP.iFreqMin = Convert.ToInt32(Convert.ToDecimal(dFmin, Functions.format) * 10);
            }
            catch { }

            try
            {
                // значение частоты max
                decimal dFmax = Convert.ToDecimal(dgv.Rows[dgv.SelectedRows[0].Index].Cells[2].Value, Functions.format);
                string sFmax1 = functions.FreqToStr((int)(dFmax * 10));
                string sFmax2 = functions.StrToFreq(sFmax1);

                nudFreqMax.Value = Convert.ToDecimal(sFmax2, Functions.format);
                CurDistribFHSS_RP.iFreqMax = Convert.ToInt32(Convert.ToDecimal(dFmax, Functions.format) * 10);
            }
            catch { }

            try
            {
                // значение порогового уровня
                CurDistribFHSS_RP.sLevel = Convert.ToInt16(dgv.Rows[dgv.SelectedRows[0].Index].Cells[4].Value);
                nudLevel.Value = CurDistribFHSS_RP.sLevel;
            }
            catch 
            {
                nudLevel.Value = -60;
            }

            try
            {
                // шаг
                CurDistribFHSS_RP.iStep = 100;
            }
            catch { }

            try
            {
                // значение модуляции
                CurDistribFHSS_RP.bModulation = Convert.ToByte(dgv.Rows[dgv.SelectedRows[0].Index].Cells[8].Value);
                cmbModulation.SelectedIndex = Convert.ToInt32(CurDistribFHSS_RP.bModulation - 1);
            }
            catch { }

            try
            {
                // значение девиации
                CurDistribFHSS_RP.bDeviation = Convert.ToByte(dgv.Rows[dgv.SelectedRows[0].Index].Cells[9].Value);
                initParams.SetDeviation(cmbModulation.SelectedIndex, CurDistribFHSS_RP.bDeviation - 1, cmbDeviation);
            }
            catch { }

            try
            {
                // значение манипуляции   
                CurDistribFHSS_RP.bManipulation = Convert.ToByte(dgv.Rows[dgv.SelectedRows[0].Index].Cells[10].Value);
                initParams.SetManipulation(cmbModulation.SelectedIndex, CurDistribFHSS_RP.bManipulation - /*2*/1, cmbManipulation, 0);
            }
            catch { }
        }

        private void bAddParam_Click(object sender, EventArgs e)
        {
            DistribFHSS_RP = new TDistribFHSS_RP();

            // считать частоту min
            try
            {
                DistribFHSS_RP.iFreqMin = Convert.ToInt32(nudFreqMin.Value * 10);
            }
            catch (System.Exception)
            {
                MessageBox.Show(SMessageError.mesErrIncorrSetFreq, SMessageError.mesErr, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.ServiceNotification);
                return;
            }

            // считать частоту max
            try
            {
                DistribFHSS_RP.iFreqMax = Convert.ToInt32(nudFreqMax.Value * 10);
            }
            catch (System.Exception)
            {
                MessageBox.Show(SMessageError.mesErrIncorrSetFreq, SMessageError.mesErr, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.ServiceNotification);
                return;
            }

            if (functions.CorrectFreqMinMax(DistribFHSS_RP.iFreqMin, DistribFHSS_RP.iFreqMax))
            {
                DistribFHSS_RP.bLetter = functions.DefineLetter(DistribFHSS_RP.iFreqMin, DistribFHSS_RP.iFreqMax);

                // порог
                DistribFHSS_RP.sLevel = Convert.ToInt16(nudLevel.Value);

                // Шаг
                DistribFHSS_RP.iStep = Convert.ToInt32(nudStep.Value);

                double dDev = 0;

                // считать параметры помехи
                switch (cmbModulation.SelectedIndex)
                {
                    // ЧМШ
                    case 0:
                        DistribFHSS_RP.bModulation = 0x01;
                        DistribFHSS_RP.bDeviation = Convert.ToByte(cmbDeviation.SelectedIndex + 1);
                        DistribFHSS_RP.bManipulation = 0;
                        break;

                    // ЧМ-2
                    case 1:
                        DistribFHSS_RP.bModulation = 0x02;

                        dDev = Convert.ToDouble(cmbDeviation.Text);
                        dDev = (1 / (2000 * dDev)) * 1000000;

                        if (Convert.ToDouble(cmbManipulation.Text) < dDev)
                        {
                            MessageBox.Show(SMessageError.mesErrIncorrSetManip, SMessageError.mesErr, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.ServiceNotification);
                            return;
                        }

                        DistribFHSS_RP.bDeviation = Convert.ToByte(cmbDeviation.SelectedIndex + 1);
                        DistribFHSS_RP.bManipulation = Convert.ToByte(cmbManipulation.SelectedIndex + 1);
                        break;

                    // ЧМ-4
                    case 2:
                        DistribFHSS_RP.bModulation = 0x03;

                        dDev = Convert.ToDouble(cmbDeviation.Text);
                        dDev = (3 / (2000 * dDev)) * 1000000;

                        if (Convert.ToDouble(cmbManipulation.Text) < dDev)
                        {
                            MessageBox.Show(SMessageError.mesErrIncorrSetManip, SMessageError.mesErr, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.ServiceNotification);
                            return;
                        }

                        DistribFHSS_RP.bDeviation = Convert.ToByte(cmbDeviation.SelectedIndex + 1);
                        DistribFHSS_RP.bManipulation = Convert.ToByte(cmbManipulation.SelectedIndex + 1);
                        break;

                    // ЧМ-8
                    case 3:
                        DistribFHSS_RP.bModulation = 0x04;

                        dDev = Convert.ToDouble(cmbDeviation.Text);
                        dDev = (7 / (2000 * dDev)) * 1000000;

                        if (Convert.ToDouble(cmbManipulation.Text) < dDev)
                        {
                            MessageBox.Show(SMessageError.mesErrIncorrSetManip, SMessageError.mesErr, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.ServiceNotification);
                            return;
                        }

                        DistribFHSS_RP.bDeviation = Convert.ToByte(cmbDeviation.SelectedIndex + 1);
                        DistribFHSS_RP.bManipulation = Convert.ToByte(cmbManipulation.SelectedIndex + 1);
                        break;

                    // ФМн
                    case 4:
                        DistribFHSS_RP.bModulation = 0x05;
                        // в байт девиации значение манипуляции
                        DistribFHSS_RP.bDeviation = 0;
                        DistribFHSS_RP.bManipulation = Convert.ToByte(cmbManipulation.SelectedIndex + 1);
                        break;

                    // ФМн-4
                    case 5:
                        DistribFHSS_RP.bModulation = 0x06;
                        // в байт девиации значение манипуляции
                        DistribFHSS_RP.bDeviation = 0;
                        DistribFHSS_RP.bManipulation = Convert.ToByte(cmbManipulation.SelectedIndex + 1);
                        break;

                    // ФМн-8
                    case 6:
                        DistribFHSS_RP.bModulation = 0x07;
                        // в байт девиации значение манипуляции
                        DistribFHSS_RP.bDeviation = 0;
                        DistribFHSS_RP.bManipulation = Convert.ToByte(cmbManipulation.SelectedIndex + 1);
                        break;

                    // ЗШ
                    case 7:
                        DistribFHSS_RP.bModulation = 0x08;
                        DistribFHSS_RP.bDeviation = 0;
                        DistribFHSS_RP.bManipulation = 1;
                        break;

                    default:
                        DistribFHSS_RP.bModulation = 0x06;
                        DistribFHSS_RP.bDeviation = 0;
                        DistribFHSS_RP.bManipulation = 1;
                        break;
                }

                DistribFHSS_RP.bDuration = 1;// Convert.ToByte(cmbDuration.SelectedIndex + 1);
                
                DistribFHSS_RP.iDuration= Convert.ToInt32(nudDurIzl.Value);

                byte codeFFT = Convert.ToByte(cmbTypeFFT.SelectedIndex + 1);
                switch (codeFFT)
                { 
                    case 1:
                        DistribFHSS_RP.bCodeFFT = 2;
                        break;

                    case 2:
                        DistribFHSS_RP.bCodeFFT = 3;
                        break;

                    case 3:
                        DistribFHSS_RP.bCodeFFT = 4;
                        break;

                    default:
                        DistribFHSS_RP.bCodeFFT = 4;
                        break;
                }

                if (rbOwn.Checked) // ведущая СП
                {
                    string sMessage = functions.CheckRangesRP(1, DistribFHSS_RP.iFreqMin, DistribFHSS_RP.iFreqMax);
                    if (sMessage != "")
                    {
                        MessageBox.Show(sMessage, SMessageError.mesErr, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }

                    sMessage = functions.CheckForbiddenFreq(1, DistribFHSS_RP.iFreqMin, DistribFHSS_RP.iFreqMax);
                    if (sMessage == "")
                    {
                        lDistribFHSS_RPOwn = new List<TDistribFHSS_RP>();
                        lDistribFHSS_RPOwn = variableWork.DistribFHSS_RPOwn.ToList();

                        // Добавить lDistribFHSS_RPOwn в variableWork
                        sMessage = funcVW_IRI_PPRCh_RP.IsAddIRI_PPRCh_CRToVariableWork(variableWork, DistribFHSS_RP, Table.Own);
                        if (sMessage == "")
                        {
                            if (lDistribFHSS_RPOwn.Count() > 0)
                            {
                                int id = lDistribFHSS_RPOwn.Max(x => x.iID);
                                DistribFHSS_RP.iID = id + 1;
                            }
                            else
                            {
                                DistribFHSS_RP.iID = 1;
                            }
                            lDistribFHSS_RPOwn.Add(DistribFHSS_RP);
                            variableWork.DistribFHSS_RPOwn = lDistribFHSS_RPOwn.ToArray();
                        }
                        else
                        {
                            MessageBox.Show(sMessage, SMessageError.mesErr, MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return;
                        }
                    }
                    else
                    {
                        MessageBox.Show(sMessage, SMessageError.mesErr, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }
                else // ведомая СП
                {
                     string sMessage = functions.CheckRangesRP(2, DistribFHSS_RP.iFreqMin, DistribFHSS_RP.iFreqMax);
                     if (sMessage != "")
                     {
                         MessageBox.Show(sMessage, SMessageError.mesErr, MessageBoxButtons.OK, MessageBoxIcon.Error);
                         return;
                     }

                     sMessage = functions.CheckForbiddenFreq(2, DistribFHSS_RP.iFreqMin, DistribFHSS_RP.iFreqMax);
                     if (sMessage == "")
                     {
                         lDistribFHSS_RPLinked = new List<TDistribFHSS_RP>();
                         lDistribFHSS_RPLinked = variableWork.DistribFHSS_RPLinked.ToList();
                         
                         // Добавить lDistribFHSS_RPLinked в variableWork
                         sMessage = funcVW_IRI_PPRCh_RP.IsAddIRI_PPRCh_CRToVariableWork(variableWork, DistribFHSS_RP, Table.Linked);
                         if (sMessage == "")
                         {
                             if (lDistribFHSS_RPLinked.Count() > 0)
                             {
                                 int id = lDistribFHSS_RPLinked.Max(x => x.iID);
                                 DistribFHSS_RP.iID = id + 1;
                             }
                             else
                             {
                                 DistribFHSS_RP.iID = 1;
                             }
                             lDistribFHSS_RPLinked.Add(DistribFHSS_RP);
                             variableWork.DistribFHSS_RPLinked = lDistribFHSS_RPLinked.ToArray();
                         }
                         else
                         {
                             MessageBox.Show(sMessage, SMessageError.mesErr, MessageBoxButtons.OK, MessageBoxIcon.Error);
                             return;
                         }
                     }
                     else
                     {
                         MessageBox.Show(sMessage, SMessageError.mesErr, MessageBoxButtons.OK, MessageBoxIcon.Error);
                         return;
                     }
                }
            }
        }

        private void cmbModulation_SelectedIndexChanged(object sender, EventArgs e)
        {
            initParams.SetDeviation(cmbModulation.SelectedIndex, 0, cmbDeviation);

            if (cmbModulation.SelectedIndex > 3 & cmbModulation.SelectedIndex < 7)
                initParams.SetManipulation(cmbModulation.SelectedIndex, 6, cmbManipulation, 0);
            else
                initParams.SetManipulation(cmbModulation.SelectedIndex, 0, cmbManipulation, 0);
        }

        private void bClear_Click(object sender, EventArgs e)
        {
            if (rbOwn.Checked) // ведущая СП
            {
                //functionsDB.DeleteAllRecordsDB(NameTable.IRI_PPRCh_RP, Table.Own);
                lDistribFHSS_RPOwn.Clear();
                variableWork.DistribFHSS_RPOwn = lDistribFHSS_RPOwn.ToArray();
                //functionsDB.DeleteAllRecordsDB(NameTable.IRI_PPRCh_RP, Table.Own);

                lDistribFHSS_RPExcludeOwn.Clear();
                functionsDGV.DeleteAllRecordsDGV(dgvIRI_PPRCh_RP2.dgv);
                dgvIRI_PPRCh_RP2.SetInitialNumberOfRows(dgvIRI_PPRCh_RP2.dgv, dgvIRI_PPRCh_RP2.tableIRI_PPRCh_RP2, NameTable.IRI_PPRCh_RP2);

                variableWork.DistribFHSS_RPExcludeOwn = lDistribFHSS_RPExcludeOwn.ToArray();
            }
            else // ведомая СП
            {
                //functionsDB.DeleteAllRecordsDB(NameTable.IRI_PPRCh_RP, Table.Linked);
                lDistribFHSS_RPLinked.Clear();
                variableWork.DistribFHSS_RPLinked = lDistribFHSS_RPLinked.ToArray();
                //functionsDB.DeleteAllRecordsDB(NameTable.IRI_PPRCh_RP, Table.Linked);

                lDistribFHSS_RPExcludeLinked.Clear();
                functionsDGV.DeleteAllRecordsDGV(dgvIRI_PPRCh_RP2.dgv);
                dgvIRI_PPRCh_RP2.SetInitialNumberOfRows(dgvIRI_PPRCh_RP2.dgv, dgvIRI_PPRCh_RP2.tableIRI_PPRCh_RP2, NameTable.IRI_PPRCh_RP2);

                functionsDGV.DeleteAllRecordsDGV(dgvIRI_PPRCh_RP_EXCLUDE.dgv);
                dgvIRI_PPRCh_RP_EXCLUDE.SetInitialNumberOfRows(dgvIRI_PPRCh_RP_EXCLUDE.dgv, dgvIRI_PPRCh_RP_EXCLUDE.tableIRI_PPRCh_RP_Exclude, NameTable.IRI_PPRCh_RP_EXCLUDE);

                variableWork.DistribFHSS_RPExcludeLinked = lDistribFHSS_RPExcludeLinked.ToArray();
            }
        }

        private void bAdd_Click(object sender, EventArgs e)
        {
            DistribFHSS_RPExclude = new TDistribFHSS_RPExclude();

            try
            {
                //DistribFHSS_RPExclude = TDistribFHSS_RPExclude.CreateID();
                if (rbOwn.Checked) // ведущая СП
                {
                    DistribFHSS_RPExclude.iID = Convert.ToInt32(dgvIRI_PPRCh_RPOwn.dgv.Rows[dgvIRI_PPRCh_RPOwn.dgv.SelectedRows[0].Index].Cells[0].Value);
                }
                else
                {
                    DistribFHSS_RPExclude.iID = Convert.ToInt32(dgvIRI_PPRCh_RPLinked.dgv.Rows[dgvIRI_PPRCh_RPLinked.dgv.SelectedRows[0].Index].Cells[0].Value);
                }
               
            }
            catch { DistribFHSS_RPExclude.iID = -1; }

            // считать частоту
            try
            {
                DistribFHSS_RPExclude.iFreqExclude = Convert.ToInt32(nudFreqExclude.Value * 10);
            }
            catch (System.Exception)
            {
                MessageBox.Show(SMessageError.mesErrIncorrSetFreq, SMessageError.mesErr, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.ServiceNotification);
                return;
            }

            // проверить частоту на принадлежность частотному диапазону 
            if (DistribFHSS_RPExclude.iFreqExclude < RangesLetters.FREQ_START_LETTER_1 | DistribFHSS_RPExclude.iFreqExclude > RangesLetters.FREQ_STOP_LETTER_9)
            {
                MessageBox.Show(SMessageError.mesErrFreqWithoutRange, SMessageError.mesErr, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.ServiceNotification);
                return;
            }

            // считать ширину полосы
            try
            {
                DistribFHSS_RPExclude.iWidthExclude = Convert.ToInt32(nudWidthExclude.Value * 10);
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.Message, SMessageError.mesErr, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.ServiceNotification);
            }

            if (rbOwn.Checked) // ведущая СП
            {
                if (funcDB_IRI_PPRCh_RP.ChangeIdenticalIRI_PPRCh_RPExclude(NameTable.IRI_PPRCh_RP_EXCLUDE, DistribFHSS_RPExclude, Table.Own))
                {
                    funcDB_IRI_PPRCh_RP.LoadIRI_PPRCh_RPExcludeFromDBToVW(NameTable.IRI_PPRCh_RP_EXCLUDE, Table.Own);
                }
                else if (funcDB_IRI_PPRCh_RP.AddRecord_IRI_PPRCh_RPExcludeToDB(NameTable.IRI_PPRCh_RP_EXCLUDE, DistribFHSS_RPExclude, Table.Own))
                {
                    funcDB_IRI_PPRCh_RP.LoadIRI_PPRCh_RPExcludeFromDBToVW(NameTable.IRI_PPRCh_RP_EXCLUDE, Table.Own);
                }
            }
            else // ведомая СП
            {
                if (funcDB_IRI_PPRCh_RP.ChangeIdenticalIRI_PPRCh_RPExclude(NameTable.IRI_PPRCh_RP_EXCLUDE, DistribFHSS_RPExclude, Table.Linked))
                {
                    funcDB_IRI_PPRCh_RP.LoadIRI_PPRCh_RPExcludeFromDBToVW(NameTable.IRI_PPRCh_RP_EXCLUDE, Table.Linked);
                }
                else if (funcDB_IRI_PPRCh_RP.AddRecord_IRI_PPRCh_RPExcludeToDB(NameTable.IRI_PPRCh_RP_EXCLUDE, DistribFHSS_RPExclude, Table.Linked))
                {
                    funcDB_IRI_PPRCh_RP.LoadIRI_PPRCh_RPExcludeFromDBToVW(NameTable.IRI_PPRCh_RP_EXCLUDE, Table.Linked);
                }
            }
        }

        /// <summary>
        /// Определить выколотые частоты
        /// </summary>
        /// <param name="lDistribFHSS_RPExclude"></param>
        /// <param name="lDistribFHSS_RP"></param>
        /// <param name="dgv"></param>
        public void CheckExclude(DataGridView dgv, List<TDistribFHSS_RPExclude> lDistribFHSS_RPExclude, Color color)
        {
            try
            {
                for (int i = 0; i < dgv.RowCount; i++)
                {
                    if (dgv.Rows[i].Cells[1].Value == null) { return; }
                    else
                    {
                        // значение частоты 
                        decimal dFmin = Convert.ToDecimal(dgv.Rows[i].Cells[1].Value, Functions.format);
                        int iFreq = Convert.ToInt32(Convert.ToDecimal(dFmin, Functions.format) * 10);
                      
                        for (int j = 0; j < lDistribFHSS_RPExclude.Count(); j++)
                        {
                            if ( iFreq <= lDistribFHSS_RPExclude[j].iFreqExclude + (lDistribFHSS_RPExclude[j].iWidthExclude / 2) &&
                                 iFreq >= lDistribFHSS_RPExclude[j].iFreqExclude - (lDistribFHSS_RPExclude[j].iWidthExclude / 2) )
                            {
                                dgv.Rows[i].Cells[1].Style.ForeColor = color;
                            }
                        }
                    }
                }
            }
            catch { }
        }

        private void bClearFRCh_Click(object sender, EventArgs e)
        {
            if (rbOwn.Checked) // ведущая СП
            {
                functionsDB.DeleteAllRecordsDB(NameTable.IRI_PPRCh_RP_EXCLUDE, Table.Own);
                //if (variableWork.DistribFHSS_RPExcludeLinked == null) { TDistribFHSS_RPExclude.SetIdFWS(0); }
                lDistribFHSS_RPExcludeOwn.Clear();
                variableWork.DistribFHSS_RPExcludeOwn = lDistribFHSS_RPExcludeOwn.ToArray();

                for (int i = 0; i < dgvIRI_PPRCh_RP2.dgv.RowCount; i++)
                {
                    dgvIRI_PPRCh_RP2.dgv.Rows[i].Cells[1].Style.ForeColor = Color.Black;
                }
            }
            else // ведомая СП
            {
                functionsDB.DeleteAllRecordsDB(NameTable.IRI_PPRCh_RP_EXCLUDE, Table.Linked);
                //if (variableWork.DistribFHSS_RPExcludeOwn == null) { TDistribFHSS_RPExclude.SetIdFWS(0); }
                lDistribFHSS_RPExcludeLinked.Clear();
                variableWork.DistribFHSS_RPExcludeLinked = lDistribFHSS_RPExcludeLinked.ToArray();

                for (int i = 0; i < dgvIRI_PPRCh_RP2.dgv.RowCount; i++)
                {
                    dgvIRI_PPRCh_RP2.dgv.Rows[i].Cells[1].Style.ForeColor = Color.Black;
                }
            }
        }

        private void chbShowMated_CheckedChanged(object sender, EventArgs e)
        {
            if (!chbShowMated.Checked)
            {
                tableLayoutPanel.RowStyles[1] = new RowStyle(SizeType.Absolute, 0);
            }
            else
            {
                tableLayoutPanel.RowStyles[1] = new RowStyle(SizeType.Absolute, 120);
            }
        }

        private void rbMated_CheckedChanged(object sender, EventArgs e)
        {
            functionsDGV.DeleteAllRecordsDGV(dgvIRI_PPRCh_RP2.dgv);
            dgvIRI_PPRCh_RP2.SetInitialNumberOfRows(dgvIRI_PPRCh_RP2.dgv, dgvIRI_PPRCh_RP2.tableIRI_PPRCh_RP2, NameTable.IRI_PPRCh_RP2);

            functionsDGV.DeleteAllRecordsDGV(dgvIRI_PPRCh_RP_EXCLUDE.dgv);
            dgvIRI_PPRCh_RP_EXCLUDE.SetInitialNumberOfRows(dgvIRI_PPRCh_RP_EXCLUDE.dgv, dgvIRI_PPRCh_RP_EXCLUDE.tableIRI_PPRCh_RP_Exclude, NameTable.IRI_PPRCh_RP_EXCLUDE);

            if (rbMated.Checked)
            {
                // ID строки
                int id = Convert.ToInt32(dgvIRI_PPRCh_RPLinked.dgv.Rows[dgvIRI_PPRCh_RPLinked.dgv.SelectedRows[0].Index].Cells[0].Value);

                // значение частоты min
                decimal dFmin = Convert.ToDecimal(dgvIRI_PPRCh_RPLinked.dgv.Rows[dgvIRI_PPRCh_RPLinked.dgv.SelectedRows[0].Index].Cells[1].Value, Functions.format);
                int iFmin = Convert.ToInt32(Convert.ToDecimal(dFmin, Functions.format) * 10);

                // значение частоты max
                decimal dFmax = Convert.ToDecimal(dgvIRI_PPRCh_RPLinked.dgv.Rows[dgvIRI_PPRCh_RPLinked.dgv.SelectedRows[0].Index].Cells[2].Value, Functions.format);
                int iFmax = Convert.ToInt32(Convert.ToDecimal(dFmax, Functions.format) * 10);
               
                funcDGV_IRI_PPRCh_RP.AddRecord_IRI_PPRChFreq_RPToDGV(dgvIRI_PPRCh_RP2.dgv, id, iFmin, iFmax, 100 * 10, Table.Linked);

                if (variableWork.DistribFHSS_RPExcludeLinked != null)
                {
                    int len = variableWork.DistribFHSS_RPExcludeLinked.Length;

                    DistribFHSS_RPExclude = new TDistribFHSS_RPExclude();

                    for (int i = 0; i < len; i++)
                    {
                        DistribFHSS_RPExclude.iID = variableWork.DistribFHSS_RPExcludeLinked[i].iID;
                        DistribFHSS_RPExclude.iFreqExclude = variableWork.DistribFHSS_RPExcludeLinked[i].iFreqExclude;
                        DistribFHSS_RPExclude.iWidthExclude = variableWork.DistribFHSS_RPExcludeLinked[i].iWidthExclude;

                        funcDGV_IRI_PPRCh_RP.AddRecord_IRI_PPRCh_RPExcludeToDGV(dgvIRI_PPRCh_RP_EXCLUDE.dgv, NameTable.IRI_PPRCh_RP_EXCLUDE, DistribFHSS_RPExclude, Table.Own);
                    }

                    lDistribFHSS_RPExcludeLinked = new List<TDistribFHSS_RPExclude>();
                    lDistribFHSS_RPExcludeLinked = variableWork.DistribFHSS_RPExcludeLinked.ToList();

                    CheckExclude(dgvIRI_PPRCh_RP2.dgv, lDistribFHSS_RPExcludeLinked, Color.Red);
                }
            }
            else if (rbOwn.Checked)
            {
                // ID строки
                int id = Convert.ToInt32(dgvIRI_PPRCh_RPOwn.dgv.Rows[dgvIRI_PPRCh_RPOwn.dgv.SelectedRows[0].Index].Cells[0].Value);

                // значение частоты min
                decimal dFmin = Convert.ToDecimal(dgvIRI_PPRCh_RPOwn.dgv.Rows[dgvIRI_PPRCh_RPOwn.dgv.SelectedRows[0].Index].Cells[1].Value, Functions.format);
                int iFmin = Convert.ToInt32(Convert.ToDecimal(dFmin, Functions.format) * 10);

                // значение частоты max
                decimal dFmax = Convert.ToDecimal(dgvIRI_PPRCh_RPOwn.dgv.Rows[dgvIRI_PPRCh_RPOwn.dgv.SelectedRows[0].Index].Cells[2].Value, Functions.format);
                int iFmax = Convert.ToInt32(Convert.ToDecimal(dFmax, Functions.format) * 10);

                funcDGV_IRI_PPRCh_RP.AddRecord_IRI_PPRChFreq_RPToDGV(dgvIRI_PPRCh_RP2.dgv, id, iFmin, iFmax, 100 * 10, Table.Own);

                if (variableWork.DistribFHSS_RPExcludeOwn != null)
                {
                    int len = variableWork.DistribFHSS_RPExcludeOwn.Length;

                    DistribFHSS_RPExclude = new TDistribFHSS_RPExclude();

                    for (int i = 0; i < len; i++)
                    {
                        DistribFHSS_RPExclude.iID = variableWork.DistribFHSS_RPExcludeOwn[i].iID;
                        DistribFHSS_RPExclude.iFreqExclude = variableWork.DistribFHSS_RPExcludeOwn[i].iFreqExclude;
                        DistribFHSS_RPExclude.iWidthExclude = variableWork.DistribFHSS_RPExcludeOwn[i].iWidthExclude;

                        funcDGV_IRI_PPRCh_RP.AddRecord_IRI_PPRCh_RPExcludeToDGV(dgvIRI_PPRCh_RP_EXCLUDE.dgv, NameTable.IRI_PPRCh_RP_EXCLUDE, DistribFHSS_RPExclude, Table.Own);
                    }

                    lDistribFHSS_RPExcludeOwn = new List<TDistribFHSS_RPExclude>();
                    lDistribFHSS_RPExcludeOwn = variableWork.DistribFHSS_RPExcludeOwn.ToList();

                    CheckExclude(dgvIRI_PPRCh_RP2.dgv, lDistribFHSS_RPExcludeOwn, Color.Red);
                }
            }
        }

        private void tsmiDelete_Click(object sender, EventArgs e)
        {
            int iID = 0;
            int ind = 0;

            List<TDistribFHSS_RPExclude> lTemp = new List<TDistribFHSS_RPExclude>();
            TDistribFHSS_RPExclude temp = new TDistribFHSS_RPExclude();

            if (dgvIRI_PPRCh_RP_EXCLUDE.dgv.Rows[dgvIRI_PPRCh_RP_EXCLUDE.dgv.SelectedRows[0].Index].Cells[0].Value == null)
                return;
            else
            {
                if (rbOwn.Checked)
                {
                    iID = Convert.ToInt32(dgvIRI_PPRCh_RP_EXCLUDE.dgv.Rows[dgvIRI_PPRCh_RP_EXCLUDE.dgv.SelectedRows[0].Index].Cells[0].Value);

                    ind = lDistribFHSS_RPExcludeOwn.FindIndex(x => x.iID == iID);

                    if (ind != -1)
                    {
                        temp.iID = lDistribFHSS_RPExcludeOwn[ind].iID;
                        temp.iFreqExclude = lDistribFHSS_RPExcludeOwn[ind].iFreqExclude;
                        temp.iWidthExclude = lDistribFHSS_RPExcludeOwn[ind].iWidthExclude;
                        lTemp.Add(temp);

                        CheckExclude(dgvIRI_PPRCh_RP2.dgv, lTemp, Color.Black);

                        lDistribFHSS_RPExcludeOwn.RemoveAt(ind);

                        // удалить запись из базы данных
                        if (funcDB_IRI_PPRCh_RP.DeleteOneRecord_IRI_PPRCh_RPExcludefromDB(iID, NameTable.IRI_PPRCh_RP_EXCLUDE, Table.Own))
                        {
                            funcDB_IRI_PPRCh_RP.ChangeIRI_PPRCh_RPExcludeToVariableWork(NameTable.IRI_PPRCh_RP_EXCLUDE, Table.Own);
                        }
                    }
                }
                if (rbMated.Checked && chbShowMated.Checked)
                {
                    iID = Convert.ToInt32(dgvIRI_PPRCh_RP_EXCLUDE.dgv.Rows[dgvIRI_PPRCh_RP_EXCLUDE.dgv.SelectedRows[0].Index].Cells[0].Value);

                    ind = lDistribFHSS_RPExcludeLinked.FindIndex(x => x.iID == iID);

                    if (ind != -1)
                    {
                        temp.iID = lDistribFHSS_RPExcludeLinked[ind].iID;
                        temp.iFreqExclude = lDistribFHSS_RPExcludeLinked[ind].iFreqExclude;
                        temp.iWidthExclude = lDistribFHSS_RPExcludeLinked[ind].iWidthExclude;
                        lTemp.Add(temp);

                        CheckExclude(dgvIRI_PPRCh_RP2.dgv, lTemp, Color.Black);

                        lDistribFHSS_RPExcludeLinked.RemoveAt(ind);

                        // удалить запись из базы данных
                        if (funcDB_IRI_PPRCh_RP.DeleteOneRecord_IRI_PPRCh_RPExcludefromDB(iID, NameTable.IRI_PPRCh_RP_EXCLUDE, Table.Linked))
                        {

                            funcDB_IRI_PPRCh_RP.ChangeIRI_PPRCh_RPExcludeToVariableWork(NameTable.IRI_PPRCh_RP_EXCLUDE, Table.Linked);
                        }
                    }
                }
            }
        }
    }
}
