﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MyDataGridView;

using DeviceLib.MessageSplitting;
using DeviceLib.DataSource;
using System.Net;
using DeviceLib;
using System.Text.RegularExpressions;
using System.Collections;
using DeviceLib.Listenning;
using DeviceLib.HeaderSplitting;
using VariableDynamic;
using VariableStatic;
using USR_DLL;
using System.Globalization;
using System.Threading;



namespace Table_ADSB_Receiver
{
    public partial class table_ADSB_Receiver: UserControl
    {
        private bool fButton = true;

        public delegate void IndReadADSBEventHandler();
        public event IndReadADSBEventHandler IndReadADSB;


        public TcpDataSource data_source;

        public DeviceHolder<string> deviceHolder;
        public PassiveListener<string> allMessageListener;

        VariableWork variableWork;
        VariableIntellegence variableIntellegence;

        FuncDB_ADSB funcDB_ADSB;
        FuncDGV_ADSB funcDGV_ADSB;

        List<TDataADSBReceiver> listADSB;

        public List<Struct_ADSB_Receiver> listStructADSB;

        NumberFormatInfo nfi;

     //   StreamWriter sw = new StreamWriter(@"E:\Table_ADSB_Receiver.txt", true);

        //StreamWriter sw = new StreamWriter(@"E:\DataSourceConnected.txt", true);
        //public bool connect;
        //StringBuilder sbCon = new StringBuilder(100000);

        private bool fDF = false;
        public int created;
        public int id = 0;

        FunctionsDB functionsDB;
        FunctionsDGV functionsDGV;
        Functions functions;
        FunctionsTranslate functionsTranslate;

        Struct_ADSB_Receiver struct_ADSB_Receiver;

        public table_ADSB_Receiver()
        {
            InitializeComponent();

            nfi = new NumberFormatInfo();
            nfi.NumberDecimalSeparator = ",";

            funcDB_ADSB = new FuncDB_ADSB();
            funcDGV_ADSB = new FuncDGV_ADSB();

            functionsDB = new FunctionsDB();
            functionsDGV = new FunctionsDGV();
            functions = new Functions();
            functionsTranslate = new FunctionsTranslate();

            struct_ADSB_Receiver = new Struct_ADSB_Receiver();

            listADSB = new List<TDataADSBReceiver>();
            listStructADSB = new List<Struct_ADSB_Receiver>();

            variableWork = new VariableWork();
            variableIntellegence = new VariableIntellegence();
            
            InitTableADSB_Receiver();

            VariableCommon.OnChangeCommonLanguage += new VariableCommon.ChangeCommonEventHandler(VariableCommon_OnChangeCommonLanguage);

            //create device holder
            IEnumerable<byte> bMas = new List<byte>() { 0x3b, 0x0a, 0x0d };
            deviceHolder = new DeviceHolder<string>(new EndLineMessageSplitter(bMas), new  EmptyGPSHeaderSplitter());

            deviceHolder.onConnectionStatusChanged += new EventHandler<bool>( deviceHolder_onConnectionStatusChanged); 
         
            //stateIndicateRead = new StateIndicate(pbReadModeS, Properties.Resources.green, Properties.Resources.gray, TIME_INDICATE);
        }

        private void VariableCommon_OnChangeCommonLanguage()
        {
            VariableCommon variableCommon = new VariableCommon();
            switch (variableCommon.Language)
            {
                case 0:
                    functionsTranslate.SetLanguage("rus");
                    ChangeLanguage();
                    break;

                case 1:
                    functionsTranslate.SetLanguage("eng");
                    ChangeLanguage();
                    break;

                case 2:
                    functionsTranslate.SetLanguage("az");
                    ChangeLanguage();
                    break;

                default:
                    functionsTranslate.SetLanguage("rus");
                    ChangeLanguage();
                    break;
            }
        }

        /// <summary>
        /// Для инициализации языка при запуске приложения 
        /// </summary>
        /// <param name="bLanguage"></param>
        public void ChangeControlLanguage(byte bLanguage)
        {

            switch (bLanguage)
            {
                case 0:
                    functionsTranslate.SetLanguage("rus");
                    ChangeLanguage();
                    break;

                case 1:
                    functionsTranslate.SetLanguage("eng");
                    ChangeLanguage();
                    break;

                case 2:
                    functionsTranslate.SetLanguage("az");
                    ChangeLanguage();
                    break;

                default:
                    functionsTranslate.SetLanguage("rus");
                    ChangeLanguage();
                    break;
            }
        }

        /// <summary>
        /// Изменение языка интерфейса
        /// </summary>
        private void ChangeLanguage()
        {
            functionsTranslate.RenameFieldsTables(functionsTranslate.Dictionary, dgvTableADSB.dgv);
            functionsTranslate.RenameFieldsTables(functionsTranslate.Dictionary, dgvEditorADSB.dgv);
        }

        private void allMessageListener_onDataArrived(object sender, IEnumerable<byte> bMas)
        {
            StringBuilder sbDecoder = new StringBuilder();
            string sMasHEX = "";
            string s5Bit = "";

            sbDecoder.Append("1,");

            byte[] mess = bMas.ToArray();

            if (mess.Length < 18)
                return;

            sMasHEX = Encoding.ASCII.GetString(mess);
            sMasHEX = new Regex(@"[^0-9ABCDEF]").Replace(sMasHEX, ""); // удалить все символы кроме цифр и букв

            string sDF = sMasHEX.Substring(0, 2);

            int num = Int32.Parse(sDF, System.Globalization.NumberStyles.HexNumber);

            BitArray ba = new BitArray(new byte[] { Convert.ToByte(num) });
            s5Bit = Convert.ToByte(ba[7]).ToString() + Convert.ToByte(ba[6]).ToString() + Convert.ToByte(ba[5]).ToString() + Convert.ToByte(ba[4]).ToString() + Convert.ToByte(ba[3]).ToString();

            if ((sMasHEX.Length == 28) || (sMasHEX.Length == 14))
            {
                switch (s5Bit)
                {
                    // case "00000": // DF0
                    // case "00100": // DF4
                    // case "00101": // DF5
                    // case "01011": // DF11
                    // case "10000": // DF16
                    case "10001": // DF17
                    case "10010": // DF18
                    // case "10011": // DF19
                    // case "10100": // DF20
                    // case "10101": // DF21
                    // case "00011": // DF24
                    fDF = true;
                    break;
                }

                if (fDF)
                {
                    sbDecoder.Append(sMasHEX + ",");

                    string dt = DateTime.Now.ToString(@"yyyy/MM/dd HH:mm:ss.fff");

                    sbDecoder.Append(dt + ",,,,\r\n");

                    try
                    {
                        StringBuilder sb = new StringBuilder(100000);
                        if (created > 0)
                        {
                            if (IndReadADSB != null)
                                IndReadADSB();
                            //IndicateReadDataADSB();

                            DecoderDLL.Decode(sbDecoder.ToString(), sb);
                           
                            string[] strDecode = sb.ToString().Split(',');

                            struct_ADSB_Receiver.iID = functions.LastID(dgvTableADSB.dgv);
                            struct_ADSB_Receiver.sICAO = strDecode[1].Substring(2, 6); // ICAO
                            struct_ADSB_Receiver.sDatetime = strDecode[2];  // Date, time
                            struct_ADSB_Receiver.sLatitude = strDecode[7]; //.ToString(nfi);  // Latitude, deg
                            struct_ADSB_Receiver.sLongitude = strDecode[8]; //.ToString(nfi); // Longitude, deg
                            struct_ADSB_Receiver.sAltitude = strDecode[9]; //.ToString(nfi);  // Altitude, m

                            // Добавить в VariableWork struct_ADSB_Receiver ......
                            AddModeSToVariableWork(struct_ADSB_Receiver);

                            if (InvokeRequired)
                            {
                                try
                                {
                                    Invoke((MethodInvoker)(() => funcDGV_ADSB.AddRecord_ADSBToDGV(dgvTableADSB.dgv, NameTable.ADSB_RECEIVER, struct_ADSB_Receiver)));

                                    id++;
                                    struct_ADSB_Receiver.iID = id;

                                    //listStructADSB.Add(struct_ADSB_Receiver);
                                }
                                catch { }
                            }

                            //funcDB_ADSB.AddRecordsADSBToDB(NameTable.ADSB_RECEIVER, struct_ADSB_Receiver); // запись в БД для тестирования закомментировала

                            // sw.WriteLine(sb); запись в текстовый файл (для тестирования)
                        }
                        fDF = false;
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }
            }
        }

        public void AddModeSToVariableWork(object obj)
        {
            struct_ADSB_Receiver = (Struct_ADSB_Receiver)obj;

            TDataADSBReceiver temp = new TDataADSBReceiver();

            bool fID = false;
            int iNum = 0;

            if ((struct_ADSB_Receiver.sLatitude != "") && (struct_ADSB_Receiver.sLongitude != ""))
            {
                temp.iID = struct_ADSB_Receiver.iID;
                temp.sICAO = struct_ADSB_Receiver.sICAO;
                temp.sLatitude = struct_ADSB_Receiver.sLatitude;
                temp.sLongitude = struct_ADSB_Receiver.sLongitude;
                temp.sAltitude = struct_ADSB_Receiver.sAltitude;
                temp.sDatetime = struct_ADSB_Receiver.sDatetime;

                for (int j = 0; j < listADSB.Count; j++)
                {
                    if(listADSB[j].sICAO == struct_ADSB_Receiver.sICAO)
                    {
                        fID = true;
                        iNum = listADSB.FindIndex(x => x.sICAO == struct_ADSB_Receiver.sICAO);
                        break;
                    }
                }

                if (!fID)
                {
                    listADSB.Add(temp);
                }
                else
                {
                    listADSB[iNum] = temp;
                }

                variableWork.DataADSBReceiver = listADSB.ToArray();
            }
        }


        public void IndicateReadDataADSB(StateIndicate stateIndicateRead)
        {
            if (data_source.Connected)
                stateIndicateRead.SetIndicateOn(); // индикатор приема данных
            else
            {
                //bConnectModeS.BackColor = Color.Red;
                functionsDGV.DeleteAllRecordsDGV(dgvTableADSB.dgv);
                dgvTableADSB.SetInitialNumberOfRows(dgvTableADSB.dgv, dgvTableADSB.tableADSB_Receiver, NameTable.ADSB_RECEIVER);
            }


        }

        private void deviceHolder_onConnectionStatusChanged(object sender, bool e)
        {
            Console.WriteLine("ADSB_Receiver connected: {0}", e);

            if (e)
            {
                //bConnectModeS.BackColor = Color.Green;
            }
            else
            {
                //bConnectModeS.BackColor = Color.Red;

            }
        }

        public void InitTableADSB_Receiver()
        {
            dgvTableADSB.InitTableDB(dgvTableADSB.dgv, NameTable.ADSB_RECEIVER);
            dgvEditorADSB.InitTableDB(dgvEditorADSB.dgv, NameTable.ADSB_RECEIVER);
        }

        public void LoadTableADSB_Receiver()
        {
            funcDB_ADSB.LoadTableADSBFromDB(dgvEditorADSB.dgv, NameTable.ADSB_RECEIVER);
          //  id = functionsDB.CountRecordsDB(NameTable.ADSB_RECEIVER);
        }

        private void bOpenEditorDB_Click(object sender, EventArgs e)
        {
            //bConnectModeS.Enabled = false;

            bCloseEditorDB.Visible = true;
            bDeleteOneRecordDB.Visible = true;
            bDeleteAllRecordsDB.Visible = true;
            dgvEditorADSB.Visible = true;

            bOpenEditorDB.Visible = false;
            dgvTableADSB.Visible = false;
            bDeleteOneRecord.Visible = false;
            bDeleteAllRecords.Visible = false;
            bDeleteInactive.Visible = false;

            LoadTableADSB_Receiver();
        }

        private void bCloseEditorDB_Click(object sender, EventArgs e)
        {
            //bConnectModeS.Enabled = true;

            bCloseEditorDB.Visible = false;
            bDeleteOneRecordDB.Visible = false;
            bDeleteAllRecordsDB.Visible = false;
            dgvEditorADSB.Visible = false;

            bOpenEditorDB.Visible = true;
            dgvTableADSB.Visible = true;
            bDeleteOneRecord.Visible = true;
            bDeleteAllRecords.Visible = true;
            bDeleteInactive.Visible = true;

            functionsDGV.DeleteAllRecordsDGV(dgvEditorADSB.dgv);
            dgvEditorADSB.SetInitialNumberOfRows(dgvEditorADSB.dgv, dgvEditorADSB.tableADSB_Receiver, NameTable.ADSB_RECEIVER);
        }

        public void CloseADSB()
        {
            allMessageListener.onDataArrived -= new EventHandler<IEnumerable<byte>>(allMessageListener_onDataArrived);

            Thread.Sleep(1000);
            deviceHolder.Close();

            bOpenEditorDB.Enabled = true;

            Thread.Sleep(1000);

            functionsDGV.DeleteAllRecordsDGV(dgvTableADSB.dgv);
            dgvTableADSB.SetInitialNumberOfRows(dgvTableADSB.dgv, dgvTableADSB.tableADSB_Receiver, NameTable.ADSB_RECEIVER);
        }

        private void byte_collector_onStopped(object sender, Exception e)
        {
            if (e != null)
                Console.WriteLine(e.Message);
        }

        private void bDeleteOneRecordDB_Click(object sender, EventArgs e)
        {
            int iID = 0;

            if (dgvEditorADSB.dgv.Rows[dgvEditorADSB.dgv.SelectedRows[0].Index].Cells[0].Value == null)
                return;

            iID = Convert.ToInt32(dgvEditorADSB.dgv.Rows[dgvEditorADSB.dgv.SelectedRows[0].Index].Cells[0].Value);

            // удалить запись из базы данных
            if (functionsDB.DeleteOneRecordDB(iID, NameTable.ADSB_RECEIVER, 0))
            {
                // удалить запись из dgv
                functionsDGV.DeleteOneRecordDGV(dgvEditorADSB.dgv);

                if(id != 0)
                    id = id - 1;

                // добавить пустые строки в dgv при необходимости
                dgvEditorADSB.SetInitialNumberOfRows(dgvEditorADSB.dgv, dgvEditorADSB.tableADSB_Receiver, NameTable.ADSB_RECEIVER);
            }
        }

        private void bDeleteAllRecordsDB_Click(object sender, EventArgs e)
        {
            int count = 0;
            
            // если нет значений в таблице
            for (int i = 0; i < dgvEditorADSB.dgv.RowCount; i++)
            {
                if (dgvEditorADSB.dgv.Rows[i].Cells[0].Value == null)
                    count++;
            }

            if (count == dgvEditorADSB.dgv.RowCount)
                return;
            
            // удалить все записи из базы данных
            if (functionsDB.DeleteAllRecordsDB(NameTable.ADSB_RECEIVER, 0))
            {
                id = 0;

                // удалить все записи из dgv
                functionsDGV.DeleteAllRecordsDGV(dgvEditorADSB.dgv);

                // добавить пустые строки при необходимости
                dgvEditorADSB.SetInitialNumberOfRows(dgvEditorADSB.dgv, dgvEditorADSB.tableADSB_Receiver, NameTable.ADSB_RECEIVER);
            }
        }

        private void table_ADSB_Receiver_Load(object sender, EventArgs e)
        {
            id = functionsDB.CountRecordsDB(NameTable.ADSB_RECEIVER, Table.Default);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            //OTL

            //sbCon = new StringBuilder();
            //connect = data_source.Connected;
            //sbCon.Append(connect + "     ");

            //sbCon.Append(DateTime.Now.ToString(@"HH:mm:ss.fff"));
            
            //sw.WriteLine(sbCon);
            
        }

        private void bDeleteOneRecord_Click(object sender, EventArgs e)
        {
            try
            { 
                string dgvICAO = Convert.ToString(dgvTableADSB.dgv.Rows[dgvTableADSB.dgv.Rows.GetFirstRow(DataGridViewElementStates.Selected)].Cells[1].Value);

                dgvTableADSB.dgv.SuspendLayout();

                int id = functionsDGV.DeleteOneRecordDGV(dgvTableADSB.dgv);
                //functionsDGV.DeleteOneRecordDGV(dgvTableADSB.dgv);

                // добавить пустые строки при необходимости
                dgvTableADSB.SetInitialNumberOfRows(dgvTableADSB.dgv, dgvTableADSB.tableADSB_Receiver, NameTable.ADSB_RECEIVER);

                if (id > 1)
                {
                    for (int i = id - 1; i < dgvTableADSB.dgv.RowCount; i++)
                    {
                        if (dgvTableADSB.dgv.Rows[i].Cells[0].Value != null)
                            dgvTableADSB.dgv.Rows[i].Cells[0].Value = i + 1;
                    }
                }

                dgvTableADSB.dgv.ResumeLayout();

                int iNum = listADSB.FindIndex(x => x.sICAO == dgvICAO);
                if (iNum >= 0)
                {
                    listADSB.RemoveAt(iNum);
                    variableWork.DataADSBReceiver = listADSB.ToArray();
                }
            }
            catch { }
        }

        /// <summary>
        /// Удаление всех записей из таблицы и variableWork
        /// </summary>
        public void DeleteRecords()
        { 
            listADSB.Clear();
            variableWork.DataADSBReceiver = listADSB.ToArray();
            
            functionsDGV.DeleteAllRecordsDGV(dgvTableADSB.dgv);
            // добавить пустые строки при необходимости
            dgvTableADSB.SetInitialNumberOfRows(dgvTableADSB.dgv, dgvTableADSB.tableADSB_Receiver, NameTable.ADSB_RECEIVER);
        }

        private void bDeleteAllRecords_Click(object sender, EventArgs e)
        {
            try
            {
                allMessageListener.onDataArrived -= new EventHandler<IEnumerable<byte>>(allMessageListener_onDataArrived);
                DeleteRecords();
                allMessageListener.onDataArrived += new EventHandler<IEnumerable<byte>>(allMessageListener_onDataArrived);
            }
            catch { }
        }

        /// <summary>
        /// Удалить неактивные воздушные суда
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bDeleteInactive_Click(object sender, EventArgs e)
        {
            string localTime = DateTime.Now.ToString(@"HH:mm:ss");
            string[] time = localTime.Split(':');
            int secondsLocal = Convert.ToInt32(time[0]) * 3600 + Convert.ToInt32(time[1]) * 60 + Convert.ToInt32(time[2]);

            Struct_ADSB_Receiver[] temp = new Struct_ADSB_Receiver[dgvTableADSB.dgv.RowCount];
            List<Struct_ADSB_Receiver> listTempDGV = new List<Struct_ADSB_Receiver>();

            dgvTableADSB.dgv.SuspendLayout();

            for (int i = 0; i < dgvTableADSB.dgv.RowCount; i++)
            {
                if (dgvTableADSB.dgv.Rows[i].Cells[0].Value != null)
                {
                    temp[i].iID = Convert.ToInt32(dgvTableADSB.dgv.Rows[i].Cells[0].Value);
                    temp[i].sICAO = Convert.ToString(dgvTableADSB.dgv.Rows[i].Cells[1].Value);
                    temp[i].sLatitude = Convert.ToString(dgvTableADSB.dgv.Rows[i].Cells[2].Value);
                    temp[i].sLongitude = Convert.ToString(dgvTableADSB.dgv.Rows[i].Cells[3].Value);
                    temp[i].sAltitude = Convert.ToString(dgvTableADSB.dgv.Rows[i].Cells[4].Value);
                    temp[i].sDatetime = Convert.ToString(dgvTableADSB.dgv.Rows[i].Cells[5].Value);

                    listTempDGV.Add(temp[i]);
                }
            }

            functionsDGV.DeleteAllRecordsDGV(dgvTableADSB.dgv);
            // добавить пустые строки при необходимости
            dgvTableADSB.SetInitialNumberOfRows(dgvTableADSB.dgv, dgvTableADSB.tableADSB_Receiver, NameTable.ADSB_RECEIVER);

            for (int i = 0; i < listTempDGV.Count; i++)
            {
                try
                {
                    string[] timeADSB = listTempDGV[i].sDatetime.Substring(11, 8).Split(':');
                    int secondsADSB = Convert.ToInt32(timeADSB[0]) * 3600 + Convert.ToInt32(timeADSB[1]) * 60 + Convert.ToInt32(timeADSB[2]);

                    if ((secondsLocal - secondsADSB) >= variableIntellegence.TimeMonitor)
                    {
                        int iNum = listADSB.FindIndex(x => x.sICAO == listTempDGV[i].sICAO);
                        if (iNum >= 0)
                        {
                            listADSB.RemoveAt(iNum);
                        }

                        listTempDGV.RemoveAt(i);
                    }
                }
                catch { }
            }

            for (int i = 0; i < listTempDGV.Count; i++)
            {
                if (i >= dgvTableADSB.dgv.RowCount)
                    dgvTableADSB.dgv.Rows.Add(); //добавить строку в таблицу

                if (dgvTableADSB.dgv.Rows[i].Cells[0].Value == null)
                {
                    dgvTableADSB.dgv.Rows[i].Cells[0].Value = i + 1;
                    dgvTableADSB.dgv.Rows[i].Cells[1].Value = listTempDGV[i].sICAO;
                    dgvTableADSB.dgv.Rows[i].Cells[2].Value = listTempDGV[i].sLatitude;
                    dgvTableADSB.dgv.Rows[i].Cells[3].Value = listTempDGV[i].sLongitude;
                    dgvTableADSB.dgv.Rows[i].Cells[4].Value = listTempDGV[i].sAltitude;
                    dgvTableADSB.dgv.Rows[i].Cells[5].Value = listTempDGV[i].sDatetime;
                }
            }

            dgvTableADSB.dgv.ResumeLayout();

            variableWork.DataADSBReceiver = listADSB.ToArray();
        }

        public void ConnectADSB()
        {
            if (!deviceHolder.IsConnected)
            {
                //connect listener
                allMessageListener = new PassiveListener<string> { ID = string.Empty };
                allMessageListener.onDataArrived += new EventHandler<IEnumerable<byte>>(allMessageListener_onDataArrived);
                allMessageListener.Start();
                deviceHolder.Subscribe(allMessageListener);

                bOpenEditorDB.Enabled = false;

                data_source = new TcpDataSource(new IPEndPoint(IPAddress.Parse("192.168.0.11"), 30005)) { ReadTimeout = 500 };

                try
                {
                    deviceHolder.Open(data_source);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, SMessageError.mesErr, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                created = DecoderDLL.CreateDecoder(1800, 0); // функция создает класс

            }
        }

      
        
    }
}
