﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Table_ADSB_Receiver
{
    class DecoderDLL
    {
        [DllImport("DecoderDLL", CallingConvention = CallingConvention.Cdecl, EntryPoint = "createQDecoderDialog")]
        public static extern int CreateDecoder(int timePeriod, int writeLogLevel);

        [DllImport("DecoderDLL", CallingConvention = CallingConvention.Cdecl, EntryPoint = "destroyQDecoderDialog")]
        public static extern void DestroyDecoder();

        [DllImport("DecoderDLL", CallingConvention = CallingConvention.Cdecl, EntryPoint = "decode")]
        public static extern void Decode(string src, StringBuilder dst);
    }
}
