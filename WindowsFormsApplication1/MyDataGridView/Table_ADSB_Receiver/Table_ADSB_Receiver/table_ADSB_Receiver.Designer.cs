﻿namespace Table_ADSB_Receiver
{
    partial class table_ADSB_Receiver
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.bDeleteInactive = new System.Windows.Forms.Button();
            this.bDeleteOneRecord = new System.Windows.Forms.Button();
            this.bDeleteAllRecords = new System.Windows.Forms.Button();
            this.bDeleteOneRecordDB = new System.Windows.Forms.Button();
            this.bDeleteAllRecordsDB = new System.Windows.Forms.Button();
            this.bOpenEditorDB = new System.Windows.Forms.Button();
            this.bCloseEditorDB = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.dgvEditorADSB = new MyDataGridView.myDataGridView();
            this.dgvTableADSB = new MyDataGridView.myDataGridView();
            this.SuspendLayout();
            // 
            // bDeleteInactive
            // 
            this.bDeleteInactive.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.bDeleteInactive.AutoSize = true;
            this.bDeleteInactive.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bDeleteInactive.Image = global::Table_ADSB_Receiver.Properties.Resources.DelAirplanesInactive;
            this.bDeleteInactive.Location = new System.Drawing.Point(115, 0);
            this.bDeleteInactive.Name = "bDeleteInactive";
            this.bDeleteInactive.Size = new System.Drawing.Size(38, 38);
            this.bDeleteInactive.TabIndex = 90;
            this.toolTip1.SetToolTip(this.bDeleteInactive, "Удалить неактивные воздушные суда");
            this.bDeleteInactive.UseVisualStyleBackColor = true;
            this.bDeleteInactive.Click += new System.EventHandler(this.bDeleteInactive_Click);
            // 
            // bDeleteOneRecord
            // 
            this.bDeleteOneRecord.AutoSize = true;
            this.bDeleteOneRecord.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bDeleteOneRecord.Image = global::Table_ADSB_Receiver.Properties.Resources.DelOne;
            this.bDeleteOneRecord.Location = new System.Drawing.Point(2, 0);
            this.bDeleteOneRecord.Name = "bDeleteOneRecord";
            this.bDeleteOneRecord.Size = new System.Drawing.Size(38, 38);
            this.bDeleteOneRecord.TabIndex = 88;
            this.toolTip1.SetToolTip(this.bDeleteOneRecord, "Удалить одну запись");
            this.bDeleteOneRecord.UseVisualStyleBackColor = true;
            this.bDeleteOneRecord.Click += new System.EventHandler(this.bDeleteOneRecord_Click);
            // 
            // bDeleteAllRecords
            // 
            this.bDeleteAllRecords.AutoSize = true;
            this.bDeleteAllRecords.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bDeleteAllRecords.Image = global::Table_ADSB_Receiver.Properties.Resources.DelAll;
            this.bDeleteAllRecords.Location = new System.Drawing.Point(42, 0);
            this.bDeleteAllRecords.Name = "bDeleteAllRecords";
            this.bDeleteAllRecords.Size = new System.Drawing.Size(38, 38);
            this.bDeleteAllRecords.TabIndex = 89;
            this.toolTip1.SetToolTip(this.bDeleteAllRecords, "Удалить все записи");
            this.bDeleteAllRecords.UseVisualStyleBackColor = true;
            this.bDeleteAllRecords.Click += new System.EventHandler(this.bDeleteAllRecords_Click);
            // 
            // bDeleteOneRecordDB
            // 
            this.bDeleteOneRecordDB.AutoSize = true;
            this.bDeleteOneRecordDB.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bDeleteOneRecordDB.Image = global::Table_ADSB_Receiver.Properties.Resources.DelOneDB;
            this.bDeleteOneRecordDB.Location = new System.Drawing.Point(2, 0);
            this.bDeleteOneRecordDB.Name = "bDeleteOneRecordDB";
            this.bDeleteOneRecordDB.Size = new System.Drawing.Size(38, 38);
            this.bDeleteOneRecordDB.TabIndex = 1;
            this.toolTip1.SetToolTip(this.bDeleteOneRecordDB, "Удалить одну запись");
            this.bDeleteOneRecordDB.UseVisualStyleBackColor = true;
            this.bDeleteOneRecordDB.Visible = false;
            this.bDeleteOneRecordDB.Click += new System.EventHandler(this.bDeleteOneRecordDB_Click);
            // 
            // bDeleteAllRecordsDB
            // 
            this.bDeleteAllRecordsDB.AutoSize = true;
            this.bDeleteAllRecordsDB.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bDeleteAllRecordsDB.Image = global::Table_ADSB_Receiver.Properties.Resources.DelAllDB;
            this.bDeleteAllRecordsDB.Location = new System.Drawing.Point(42, 0);
            this.bDeleteAllRecordsDB.Name = "bDeleteAllRecordsDB";
            this.bDeleteAllRecordsDB.Size = new System.Drawing.Size(38, 38);
            this.bDeleteAllRecordsDB.TabIndex = 2;
            this.toolTip1.SetToolTip(this.bDeleteAllRecordsDB, "Удалить все записи");
            this.bDeleteAllRecordsDB.UseVisualStyleBackColor = true;
            this.bDeleteAllRecordsDB.Visible = false;
            this.bDeleteAllRecordsDB.Click += new System.EventHandler(this.bDeleteAllRecordsDB_Click);
            // 
            // bOpenEditorDB
            // 
            this.bOpenEditorDB.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.bOpenEditorDB.AutoSize = true;
            this.bOpenEditorDB.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bOpenEditorDB.Image = global::Table_ADSB_Receiver.Properties.Resources.database;
            this.bOpenEditorDB.Location = new System.Drawing.Point(350, 0);
            this.bOpenEditorDB.Name = "bOpenEditorDB";
            this.bOpenEditorDB.Size = new System.Drawing.Size(38, 38);
            this.bOpenEditorDB.TabIndex = 0;
            this.toolTip1.SetToolTip(this.bOpenEditorDB, "Открыть редактор БД");
            this.bOpenEditorDB.UseVisualStyleBackColor = true;
            this.bOpenEditorDB.Click += new System.EventHandler(this.bOpenEditorDB_Click);
            // 
            // bCloseEditorDB
            // 
            this.bCloseEditorDB.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.bCloseEditorDB.AutoSize = true;
            this.bCloseEditorDB.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bCloseEditorDB.Image = global::Table_ADSB_Receiver.Properties.Resources.databaseClose;
            this.bCloseEditorDB.Location = new System.Drawing.Point(350, 0);
            this.bCloseEditorDB.Name = "bCloseEditorDB";
            this.bCloseEditorDB.Size = new System.Drawing.Size(38, 38);
            this.bCloseEditorDB.TabIndex = 3;
            this.toolTip1.SetToolTip(this.bCloseEditorDB, "Закрыть редактор БД");
            this.bCloseEditorDB.UseVisualStyleBackColor = true;
            this.bCloseEditorDB.Visible = false;
            this.bCloseEditorDB.Click += new System.EventHandler(this.bCloseEditorDB_Click);
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // dgvEditorADSB
            // 
            this.dgvEditorADSB.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvEditorADSB.Location = new System.Drawing.Point(0, 39);
            this.dgvEditorADSB.Name = "dgvEditorADSB";
            this.dgvEditorADSB.Size = new System.Drawing.Size(390, 475);
            this.dgvEditorADSB.TabIndex = 4;
            this.dgvEditorADSB.Visible = false;
            // 
            // dgvTableADSB
            // 
            this.dgvTableADSB.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvTableADSB.Location = new System.Drawing.Point(0, 39);
            this.dgvTableADSB.Name = "dgvTableADSB";
            this.dgvTableADSB.Size = new System.Drawing.Size(390, 475);
            this.dgvTableADSB.TabIndex = 1;
            // 
            // table_ADSB_Receiver
            // 
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.bDeleteInactive);
            this.Controls.Add(this.dgvEditorADSB);
            this.Controls.Add(this.dgvTableADSB);
            this.Controls.Add(this.bOpenEditorDB);
            this.Controls.Add(this.bCloseEditorDB);
            this.Controls.Add(this.bDeleteOneRecord);
            this.Controls.Add(this.bDeleteOneRecordDB);
            this.Controls.Add(this.bDeleteAllRecords);
            this.Controls.Add(this.bDeleteAllRecordsDB);
            this.Name = "table_ADSB_Receiver";
            this.Size = new System.Drawing.Size(390, 513);
            this.Load += new System.EventHandler(this.table_ADSB_Receiver_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public MyDataGridView.myDataGridView dgvTableADSB;
        public MyDataGridView.myDataGridView dgvEditorADSB;
        public System.Windows.Forms.Button bOpenEditorDB;
        public System.Windows.Forms.Button bCloseEditorDB;
        public System.Windows.Forms.Button bDeleteAllRecordsDB;
        public System.Windows.Forms.Button bDeleteOneRecordDB;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Timer timer1;
        public System.Windows.Forms.Button bDeleteAllRecords;
        public System.Windows.Forms.Button bDeleteOneRecord;
        public System.Windows.Forms.Button bDeleteInactive;

      
    }
}
