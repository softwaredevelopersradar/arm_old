﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using USR_DLL;
using VariableDynamic;

namespace MyDataGridView
{
    public class FuncVW_IRI_FRCh_RP
    {
        VariableWork variableWork;

        public FuncVW_IRI_FRCh_RP()
        {
            variableWork = new VariableWork();
        }

        /// <summary>
        /// Удалить одну запись из VariableWork
        /// </summary>
        /// <param name="dgv"></param>
        public void DeleteOneIRI_FRCh_RPToVariableWork(DataGridView dgv, VariableWork vw, Table table)
        {
            byte bTable = 0;
            if (table == Table.Own) bTable = 1;
            if (table == Table.Linked) bTable = 2;

            try
            {
                if (dgv.Rows[dgv.SelectedRows[0].Index].Cells[0].Value == null)
                    return;

                int iID = Convert.ToInt32(dgv.Rows[dgv.SelectedRows[0].Index].Cells[0].Value);
                
                List<TSupprFWS> lSupprFWS = new List<TSupprFWS>();

                if (bTable == 1)
                {
                    lSupprFWS = vw.SupprFWS_Own.ToList();

                    int ind = lSupprFWS.FindIndex(x => x.iID == iID);

                    if (ind != -1)
                    {
                        lSupprFWS.RemoveAt(ind);
                        vw.SupprFWS_Own = lSupprFWS.ToArray();

                        //IEnumerable<TSupprFWS> sortPriority = lSupprFWS.OrderByDescending(x => x.bPrior);// OrderBy(x => x.bPrior); // сортировка по приоритету
                        //IEnumerable<TSupprFWS> sortLetter = sortPriority.OrderBy(x => x.bLetter);      // сортировка по литере
                        //vw.SupprFWS_Own = sortLetter.ToArray();
                    }
                }

                if (bTable == 2)
                {
                    lSupprFWS = vw.SupprFWS_Linked.ToList();

                    int ind = lSupprFWS.FindIndex(x => x.iID == iID);

                    if (ind != -1)
                    {
                        lSupprFWS.RemoveAt(ind);
                        vw.SupprFWS_Linked = lSupprFWS.ToArray();

                        //IEnumerable<TSupprFWS> sortPriority = lSupprFWS.OrderByDescending(x => x.bPrior);// OrderBy(x => x.bPrior); // сортировка по приоритету
                        //IEnumerable<TSupprFWS> sortLetter = sortPriority.OrderBy(x => x.bLetter);      // сортировка по литере
                        //vw.SupprFWS_Linked = sortLetter.ToArray();
                    }
                }
            }
            catch { }
        
        }

        /// <summary>
        /// Удалить все записи из VariableWork
        /// </summary>
        /// <param name="dgv"></param>
        public void DeletAllIRI_FRCh_RPToVariableWork(VariableWork vw, Table table)
        {
            byte bTable = 0;
            if (table == Table.Own) bTable = 1;
            if (table == Table.Linked) bTable = 2;

            List<TSupprFWS> lSupprFWS = new List<TSupprFWS>();

            try
            {
                if (bTable == 1)
                {
                    lSupprFWS = vw.SupprFWS_Own.ToList();
                    lSupprFWS.Clear();
                    vw.SupprFWS_Own = lSupprFWS.ToArray();
                    if (vw.SupprFWS_Linked == null) { TSupprFWS.SetIdFWS(-1); }
                }

                if (bTable == 2)
                {
                    lSupprFWS = vw.SupprFWS_Linked.ToList();
                    lSupprFWS.Clear();
                    vw.SupprFWS_Linked= lSupprFWS.ToArray();
                    if (vw.SupprFWS_Own == null) { TSupprFWS.SetIdFWS(-1); }
                }
                
            }
            catch { }

        }
    }

}
