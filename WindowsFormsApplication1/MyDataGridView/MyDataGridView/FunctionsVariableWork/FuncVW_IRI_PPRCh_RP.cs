﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using USR_DLL;
using VariableDynamic;

namespace MyDataGridView
{
    public class FuncVW_IRI_PPRCh_RP
    {
        VariableWork variableWork;

        public FuncVW_IRI_PPRCh_RP()
        {
            variableWork = new VariableWork();
        }

        /// <summary>
        /// Проверка возможности добавления записи в VariableWork
        /// </summary>
        /// <param name="dgv"></param>
        public string IsAddIRI_PPRCh_CRToVariableWork(VariableWork vw, TDistribFHSS_RP DistribFHSS_RP, Table table)
        {
            string sMessage = string.Empty;

            byte bTable = 0;
            if (table == Table.Own) bTable = 1;
            if (table == Table.Linked) bTable = 2;

            List<TDistribFHSS_RP> lDistribFHSS_RP = new List<TDistribFHSS_RP>();

            try
            {
                if (bTable == 1)
                {
                    lDistribFHSS_RP = vw.DistribFHSS_RPOwn.ToList();
                }
                else if (bTable == 2)
                {
                    lDistribFHSS_RP = vw.DistribFHSS_RPLinked.ToList();
                }

                // Функция проверки
                if (lDistribFHSS_RP.Count == 4)
                {
                    sMessage = "Невозможно добавить запись! Количество записей превысит допустимое!";
                    return sMessage;
                }
                else
                {
                    if (!IsCheckCountChannel(lDistribFHSS_RP, DistribFHSS_RP))
                    {
                        sMessage = "Суммарное количество каналов превышает допустимое!";
                        return sMessage;
                    }
                }
            }
                   
            catch { }
 
            return sMessage;
        }


        private bool IsCheckCountChannel(List<TDistribFHSS_RP> lDistribFHSS_RP, TDistribFHSS_RP DistribFHSS_RP)
        {
            bool fCheckCount = false;

            int iCountChannel = 0;

            try
            {
                lDistribFHSS_RP.Add(DistribFHSS_RP);

                for (int i = 0; i < lDistribFHSS_RP.Count; i++)
                {
                    if (((lDistribFHSS_RP[i].iFreqMax - lDistribFHSS_RP[i].iFreqMin) % 300000) == 0)
                    {
                        iCountChannel += ((lDistribFHSS_RP[i].iFreqMax - lDistribFHSS_RP[i].iFreqMin) / 300000);
                    }
                    else
                    {
                        iCountChannel += ((lDistribFHSS_RP[i].iFreqMax - lDistribFHSS_RP[i].iFreqMin) / 300000) + 1;
                    }
                    
                }

                if (iCountChannel < 5)
                {
                    fCheckCount = true;
                }
                else
                {
                    fCheckCount = false;
                }
            }
            catch { }

            return fCheckCount;
        }
    }
}
