﻿using Protocols;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using VariableDynamic;

namespace MyDataGridView
{
    public class FuncVW_Range_RR_RP
    {
        VariableWork variableWork;

        public FuncVW_Range_RR_RP()
        {
            variableWork = new VariableWork();
        }

        /// <summary>
        /// Удалить одну запись из VariableWork
        /// </summary>
        /// <param name="dgv"></param>
        public void DeleteOneRange_RR_RPToVariableWork(DataGridView dgv, VariableWork vw, Table table, NameTable nameTable)
        {
            byte bTable = 0;
            if (table == Table.Own) bTable = 1;
            if (table == Table.Linked) bTable = 2;

            try
            {
                if (dgv.Rows[dgv.SelectedRows[0].Index].Cells[0].Value == null)
                    return;

                int iID = Convert.ToInt32(dgv.Rows[dgv.SelectedRows[0].Index].Cells[0].Value) - 1;

                List<RangeSector> lRangeSector = new List<RangeSector>();

                switch (nameTable)
                {
                    case NameTable.RANGE_RR:

                        if (bTable == 1)
                        {
                            lRangeSector = vw.RangeSectorReconOwn.ToList();

                            if (iID != -1)
                            {
                                lRangeSector.RemoveAt(iID);
                                vw.RangeSectorReconOwn = lRangeSector.ToArray();
                            }
                        }

                        if (bTable == 2)
                        {
                            lRangeSector = vw.RangeSectorReconLinked.ToList();

                            if (iID != -1)
                            {
                                lRangeSector.RemoveAt(iID);
                                vw.RangeSectorReconLinked = lRangeSector.ToArray();
                            }
                        }
                        break;

                    case NameTable.RANGE_RP:

                        if (bTable == 1)
                        {
                            lRangeSector = vw.RangeSectorSupprOwn.ToList();

                            if (iID != -1)
                            {
                                lRangeSector.RemoveAt(iID);
                                vw.RangeSectorSupprOwn = lRangeSector.ToArray();
                            }
                        }

                        if (bTable == 2)
                        {
                            lRangeSector = vw.RangeSectorSupprLinked.ToList();

                            if (iID != -1)
                            {
                                lRangeSector.RemoveAt(iID);
                                vw.RangeSectorSupprLinked = lRangeSector.ToArray();
                            }
                        }
                        break;

                    default:
                        break;
                }
            }
            catch { }
        }

        /// <summary>
        /// Удалить все записи из VariableWork
        /// </summary>
        /// <param name="dgv"></param>
        public void DeleteAllRange_RR_RPToVariableWork(VariableWork vw, Table table, NameTable nameTable)
        {
            byte bTable = 0;
            if (table == Table.Own) bTable = 1;
            if (table == Table.Linked) bTable = 2;

            List<RangeSector> lRangeSector = new List<RangeSector>();

            try
            {
                switch (nameTable)
                {
                    case NameTable.RANGE_RR:

                        if (bTable == 1)
                        {
                            lRangeSector = vw.RangeSectorReconOwn.ToList();
                            lRangeSector.Clear();
                            vw.RangeSectorReconOwn = lRangeSector.ToArray();
                        }

                        if (bTable == 2)
                        {
                            lRangeSector = vw.RangeSectorReconLinked.ToList();
                            lRangeSector.Clear();
                            vw.RangeSectorReconLinked = lRangeSector.ToArray();
                        }

                        break;

                    case NameTable.RANGE_RP:

                        if (bTable == 1)
                        {
                            lRangeSector = vw.RangeSectorSupprOwn.ToList();
                            lRangeSector.Clear();
                            vw.RangeSectorSupprOwn = lRangeSector.ToArray();
                        }

                        if (bTable == 2)
                        {
                            lRangeSector = vw.RangeSectorSupprLinked.ToList();
                            lRangeSector.Clear();
                            vw.RangeSectorSupprLinked = lRangeSector.ToArray();
                        }

                        break;

                    default:
                        break;
                }
            }
            catch { }
        }

        /// <summary>
        /// Добавление/Изменение в variableWork ИРИ
        /// </summary>
        /// <param name="rangeSector"> сектора и диапазоны </param>
        /// <param name="tempListRangeSector"> теущие значения в variableWork </param>
        /// <param name="table"> Own - ведущая, Linked - ведомая </param>
        public void ChangeRange_RR_RPToVariableWork(RangeSector rangeSector, List<RangeSector> tempListRangeSector, Table table, int indRec, NameTable nameTable)
        {
            byte bTable = 0;
            if (table == Table.Own) bTable = 1;
            if (table == Table.Linked) bTable = 2;

            try
            {
                switch (nameTable)
                {
                    case NameTable.RANGE_RR:
                        {
                            if (bTable == 1)
                            {
                                if (indRec != -1) { tempListRangeSector.Insert(indRec, rangeSector); }
                                else { tempListRangeSector.Add(rangeSector); }

                                variableWork.RangeSectorReconOwn = tempListRangeSector.ToArray();
                            }

                            if (bTable == 2)
                            {
                                if (indRec != -1) { tempListRangeSector.Insert(indRec, rangeSector); }
                                else { tempListRangeSector.Add(rangeSector); }

                                variableWork.RangeSectorReconLinked = tempListRangeSector.ToArray();
                            }
                        }
                        break;

                    case NameTable.RANGE_RP:
                        {
                            if (bTable == 1)
                            {
                                if (indRec != -1) { tempListRangeSector.Insert(indRec, rangeSector); }
                                else { tempListRangeSector.Add(rangeSector); }

                                variableWork.RangeSectorSupprOwn = tempListRangeSector.ToArray();
                            }

                            if (bTable == 2)
                            {
                                if (indRec != -1) { tempListRangeSector.Insert(indRec, rangeSector); }
                                else { tempListRangeSector.Add(rangeSector); }

                                variableWork.RangeSectorSupprLinked = tempListRangeSector.ToArray();
                            }
                        }
                        break;

                    default:
                        break;
                }
            }
            catch { }
        }
    }
}
