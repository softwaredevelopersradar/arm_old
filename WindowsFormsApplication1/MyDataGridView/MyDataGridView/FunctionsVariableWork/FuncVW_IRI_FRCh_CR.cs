﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using USR_DLL;
using VariableDynamic;

namespace MyDataGridView
{
    public class FuncVW_IRI_FRCh_CR
    {
        VariableWork variableWork;

        public FuncVW_IRI_FRCh_CR()
        {
            variableWork = new VariableWork();
        }

        /// <summary>
        /// Удалить одну запись из VariableWork
        /// </summary>
        /// <param name="dgv"></param>
        public void DeleteOneIRI_FRCh_CRToVariableWork(DataGridView dgv, VariableWork vw)
        {
            try
            {
                if (dgv.Rows[dgv.SelectedRows[0].Index].Cells[0].Value == null)
                    return;

                int iID = Convert.ToInt32(dgv.Rows[dgv.SelectedRows[0].Index].Cells[0].Value);

                List<TDistribFWS> lDistribFWS = new List<TDistribFWS>();

                lDistribFWS = vw.DistribFWS.ToList();

                int ind = lDistribFWS.FindIndex(x => x.iID == iID);

                if (ind != -1)
                {
                    lDistribFWS.RemoveAt(ind);
                    vw.DistribFWS = lDistribFWS.ToArray();
                }
            }
            catch { }
        }

        /// <summary>
        /// Удалить одну запись из VariableWork
        /// </summary>
        /// <param name="dgv"></param>
        public void DeleteOneIRI_FRCh_CRToVariableWork(DataGridView dgv, VariableWork vw, int id)
        {
            try
            {
                if (dgv.Rows[dgv.SelectedRows[0].Index].Cells[0].Value == null)
                    return;

                //int iID = Convert.ToInt32(dgv.Rows[dgv.SelectedRows[0].Index].Cells[0].Value);

                List<TDistribFWS> lDistribFWS = new List<TDistribFWS>();

                lDistribFWS = vw.DistribFWS.ToList();

                int ind = lDistribFWS.FindIndex(x => x.iID == id);

                if (ind != -1)
                {
                    lDistribFWS.RemoveAt(ind);
                    vw.DistribFWS = lDistribFWS.ToArray();
                }
            }
            catch { }
        }

        /// <summary>
        /// Удалить все записи из VariableWork
        /// </summary>
        /// <param name="dgv"></param>
        public void DeleteAllIRI_FRCh_CRToVariableWork(VariableWork vw)
        {
            List<TDistribFWS> lDistribFWS = new List<TDistribFWS>();

            try
            {
                lDistribFWS = vw.DistribFWS.ToList();
                lDistribFWS.Clear();
                vw.DistribFWS = lDistribFWS.ToArray();
                TDistribFWS.SetIdFWS(0);
            }
            catch { }
        }

        /// <summary>
        /// Добавить запись в VariableWork
        /// </summary>
        /// <param name="dgv"></param>
        public void AddIRI_FRCh_CRToVariableWork(VariableWork vw, TDistribFWS[] DistribFWS)
        {
            List<TDistribFWS> lDistribFWS = new List<TDistribFWS>();

            try
            {
                lDistribFWS = vw.DistribFWS.ToList();
                for (int i = 0; i < DistribFWS.Length; i++)
                { 
                    lDistribFWS.Add(DistribFWS[i]);
                }
                    
                vw.DistribFWS = lDistribFWS.ToArray();
            }
            catch { }
        }
    }
}
