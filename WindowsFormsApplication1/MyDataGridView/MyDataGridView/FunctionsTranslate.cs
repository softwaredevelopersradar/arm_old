﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace MyDataGridView
{
    public class FunctionsTranslate
    {
        public SortedList<string, string> Dictionary;
        SMessages messages;

        // функция смены языка
        public void SetLanguage(string language)
        {
            XmlDocument xDoc = new XmlDocument();
            if (System.IO.File.Exists("XMLTranslation.xml"))
                xDoc.Load("XMLTranslation.xml");
            else
            {
                switch (language)
                {

                    case "rus":
                        MessageBox.Show("Файл XMLTranslation.xml не найден!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        break;
                    case "eng":
                        MessageBox.Show("File XMLTranslation.xml not found!", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        break;
                    case "az":
                        MessageBox.Show("Fayl XMLTranslation.xml tapılmadı!", "Səhv!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        break;
                    case "azlat":
                        //MessageBox.Show("File XMLTranslation.xml not found!", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        break;
                    case "azkir":
                        //MessageBox.Show("File XMLTranslation.xml not found!", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        break;
                }

                return;
            }

            Dictionary = new SortedList<string, string>();

            // получим корневой элемент
            XmlElement xRoot = xDoc.DocumentElement;

            string SectionName = "Tables_Translation";
            foreach (XmlNode x1Node in xRoot)
            {
                if (x1Node.Name == SectionName)
                {
                    foreach (XmlNode x2Node in x1Node.ChildNodes)
                    {
                        if (x2Node.NodeType == XmlNodeType.Comment)
                            continue;

                        // получаем атрибут ID
                        if (x2Node.Attributes.Count > 0)
                        {
                            XmlNode attr = x2Node.Attributes.GetNamedItem("ID");
                            if (attr != null)
                            {
                                foreach (XmlNode childnode in x2Node.ChildNodes)
                                {
                                    // если узел - language
                                    if (childnode.Name == language)
                                    {
                                        Dictionary.Add(attr.Value, childnode.InnerText);
                                    }

                                }
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// переименование полей таблиц при смене языка
        /// </summary>
        /// <param name="Dictionary"></param>
        public void RenameFieldsTables(SortedList<string, string> Dictionary, DataGridView dgv)
        {
            foreach (DataGridViewColumn column in dgv.Columns)
            {
                int index = Dictionary.Keys.IndexOf(column.Name);
                column.HeaderText = Dictionary.Values[index];
            }
        }

        /// <summary>
        /// Переименование Button при смене языка
        /// </summary>
        /// <param name="Dictionary"></param>
        /// <param name="userControl"></param>
        public void RenameButtons(SortedList<string, string> Dictionary, UserControl userControl)
        {
            foreach (Control control in userControl.Controls)
            {
                if (control is Button)
                {
                    if (Dictionary.ContainsKey(control.Name))
                        control.Text = Dictionary[control.Name];
                }
            }
        }

        /// <summary>
        /// Переименование Button при смене языка
        /// </summary>
        /// <param name="Dictionary"></param>
        /// <param name="panel"></param>
        public void RenameButtons(SortedList<string, string> Dictionary, Panel panel)
        {
            foreach (Control control in panel.Controls)
            {
                if (control is Button)
                {
                    if (Dictionary.ContainsKey(control.Name))
                        control.Text = Dictionary[control.Name];
                }
            }
        }

        /// <summary>
        /// Переименование Button при смене языка
        /// </summary>
        /// <param name="Dictionary"></param>
        /// <param name="groupBox"></param>
        public void RenameButtons(SortedList<string, string> Dictionary, GroupBox groupBox)
        {
            foreach (Control control in groupBox.Controls)
            {
                if (control is Button)
                {
                    if (Dictionary.ContainsKey(control.Name))
                        control.Text = Dictionary[control.Name];
                }
            }
        }

        /// <summary>
        /// Переименование Labels при смене языка
        /// </summary>
        /// <param name="Dictionary"></param>
        /// <param name="panel"></param>
        public void RenameLabels(SortedList<string, string> Dictionary, Panel panel)
        {
            foreach (Control control in panel.Controls)
            {
                if (control is Label)
                {
                    if (Dictionary.ContainsKey(control.Name))
                        control.Text = Dictionary[control.Name];
                }
            }
        }

        /// <summary>
        /// Переименование Labels при смене языка
        /// </summary>
        /// <param name="Dictionary"></param>
        /// <param name="userControl"></param>
        public void RenameLabels(SortedList<string, string> Dictionary, UserControl userControl)
        {
            foreach (Control control in userControl.Controls)
            {
                if (control is Label)
                {
                    if (Dictionary.ContainsKey(control.Name))
                        control.Text = Dictionary[control.Name];
                }
            }
        }

        /// <summary>
        /// Переименование Labels при смене языка
        /// </summary>
        /// <param name="Dictionary"></param>
        /// <param name="groupBox"></param>
        public void RenameLabels(SortedList<string, string> Dictionary, GroupBox groupBox)
        {
            foreach (Control control in groupBox.Controls)
            {
                if (control is Label)
                {
                    if (Dictionary.ContainsKey(control.Name))
                        control.Text = Dictionary[control.Name];
                }
            }
        }

        /// <summary>
        /// Переименование RadioButtons при смене языка
        /// </summary>
        /// <param name="Dictionary"></param>
        /// <param name="groupBox"></param>
        public void RenameRadioButtons(SortedList<string, string> Dictionary, GroupBox groupBox)
        {
            foreach (Control control in groupBox.Controls)
            {
                if (control is RadioButton)
                {
                    if (Dictionary.ContainsKey(control.Name))
                        control.Text = Dictionary[control.Name];
                }
            }
        }

        /// <summary>
        /// Переименование CheckBoxes при смене языка
        /// </summary>
        /// <param name="Dictionary"></param>
        /// <param name="panel"></param>
        public void RenameCheckBoxes(SortedList<string, string> Dictionary, Panel panel)
        {
            foreach (Control control in panel.Controls)
            {
                if (control is CheckBox)
                {
                    if (Dictionary.ContainsKey(control.Name))
                        control.Text = Dictionary[control.Name];
                }
            }
        }

        /// <summary>
        /// Переименование CheckBoxes при смене языка
        /// </summary>
        /// <param name="Dictionary"></param>
        /// <param name="groupBox"></param>
        public void RenameCheckBoxes(SortedList<string, string> Dictionary, GroupBox groupBox)
        {
            foreach (Control control in groupBox.Controls)
            {
                if (control is CheckBox)
                {
                    if (Dictionary.ContainsKey(control.Name))
                        control.Text = Dictionary[control.Name];
                }
            }
        }

        /// <summary>
        /// Переименование RadioButtons при смене языка
        /// </summary>
        /// <param name="Dictionary"></param>
        /// <param name="panel"></param>
        public void RenameGroupBoxes(SortedList<string, string> Dictionary, Panel panel)
        {
            foreach (Control control in panel.Controls)
            {
                if (control is GroupBox)
                {
                    if (Dictionary.ContainsKey(control.Name))
                        control.Text = Dictionary[control.Name];
                }
            }
        }

        /// <summary>
        /// Переименование TabPages при смене языка
        /// </summary>
        /// <param name="Dictionary"></param>
        /// <param name="tabControl"></param>
        public void RenameTabPages(SortedList<string, string> Dictionary, TabControl tabControl)
        {
            foreach (Control control in tabControl.Controls)
            {
                if (control is TabPage)
                {
                    if (Dictionary.ContainsKey(control.Name))
                        control.Text = Dictionary[control.Name];
                }
            }
        }

        /// <summary>
        /// Переименование сообщений при смене языка
        /// </summary>
        /// <param name="Dictionary"></param>
        public void RenameMessages(SortedList<string, string> Dictionary)
        {
            if (Dictionary.ContainsKey("mesMessage"))
                SMessages.mesMessage = Dictionary["mesMessage"];

            if (Dictionary.ContainsKey("mesRange"))
                SMessages.mesRange = Dictionary["mesRange"];

            if (Dictionary.ContainsKey("mesAndRange"))
                SMessages.mesAndRange = Dictionary["mesAndRange"];

            if (Dictionary.ContainsKey("mesSector"))
                SMessages.mesSector = Dictionary["mesSector"];

            if (Dictionary.ContainsKey("mesContainSetSector"))
                SMessages.mesContainSetSector = Dictionary["mesContainSetSector"];

            if (Dictionary.ContainsKey("mesCrossingRange"))
                SMessages.mesCrossingRange = Dictionary["mesCrossingRange"];

            if (Dictionary.ContainsKey("mesFreq"))
                SMessages.mesFreq = Dictionary["mesFreq"];

            if (Dictionary.ContainsKey("mesExist"))
                SMessages.mesExist = Dictionary["mesExist"];

            if (Dictionary.ContainsKey("mesValuesMaxMin"))
                SMessages.mesValuesMaxMin = Dictionary["mesValuesMaxMin"];

            if (Dictionary.ContainsKey("mesFreqWithoutRangeRP"))
                SMessages.mesFreqWithoutRangeRP = Dictionary["mesFreqWithoutRangeRP"];

            if (Dictionary.ContainsKey("mesFreqForbidNotAdd"))
                SMessages.mesFreqForbidNotAdd = Dictionary["mesFreqForbidNotAdd"];

            if (Dictionary.ContainsKey("mesErr"))
                SMessageError.mesErr = Dictionary["mesErr"];

            if (Dictionary.ContainsKey("mesErrAddRecord"))
                SMessageError.mesErrAddRecord = Dictionary["mesErrAddRecord"];

            if (Dictionary.ContainsKey("mesErrReadDataTable"))
                SMessageError.mesErrReadDataTable = Dictionary["mesErrReadDataTable"];

            if (Dictionary.ContainsKey("mesErrDeleteRecord"))
                SMessageError.mesErrDeleteRecord = Dictionary["mesErrDeleteRecord"];

            if (Dictionary.ContainsKey("mesErrDeleteRecords"))
                SMessageError.mesErrDeleteRecords = Dictionary["mesErrDeleteRecords"];

            if (Dictionary.ContainsKey("mesErrChangeRecord"))
                SMessageError.mesErrChangeRecord = Dictionary["mesErrChangeRecord"];

            if (Dictionary.ContainsKey("mesErrReadDataOwn"))
                SMessageError.mesErrReadDataOwn = Dictionary["mesErrReadDataOwn"];

            if (Dictionary.ContainsKey("mesErrReadDataLinked"))
                SMessageError.mesErrReadDataLinked = Dictionary["mesErrReadDataLinked"];

            if (Dictionary.ContainsKey("mesErrFileDBNotFound"))
                SMessageError.mesErrFileDBNotFound = Dictionary["mesErrFileDBNotFound"];

            if (Dictionary.ContainsKey("mesErrIncorrSetFreq"))
                SMessageError.mesErrIncorrSetFreq = Dictionary["mesErrIncorrSetFreq"];

            if (Dictionary.ContainsKey("mesErrIncorrSetManip"))
                SMessageError.mesErrIncorrSetManip = Dictionary["mesErrIncorrSetManip"];

            if (Dictionary.ContainsKey("mesErrFreqWithoutRange"))
                SMessageError.mesErrFreqWithoutRange = Dictionary["mesErrFreqWithoutRange"];

            if (Dictionary.ContainsKey("mesErrIncorrSetPel"))
                SMessageError.mesErrIncorrSetPel = Dictionary["mesErrIncorrSetPel"];

            if (Dictionary.ContainsKey("mesErrPelWithoutRange"))
                SMessageError.mesErrPelWithoutRange = Dictionary["mesErrPelWithoutRange"];

            if (Dictionary.ContainsKey("mesErrIncorrSetPrior"))
                SMessageError.mesErrIncorrSetPrior = Dictionary["mesErrIncorrSetPrior"];

            if (Dictionary.ContainsKey("mesErrIncorrSetLevel"))
                SMessageError.mesErrIncorrSetLevel = Dictionary["mesErrIncorrSetLevel"];
        }

        /// <summary>
        /// Переименование значений при смене языка
        /// </summary>
        /// <param name="Dictionary"></param>
        public void RenameMeaning(SortedList<string, string> Dictionary)
        {
            if (Dictionary.ContainsKey("meaningOwn"))
                SMeaning.meaningOwn = Dictionary["meaningOwn"];

            if (Dictionary.ContainsKey("meaningLinked"))
                SMeaning.meaningLinked = Dictionary["meaningLinked"];

            if (Dictionary.ContainsKey("meaningMHz"))
                SMeaning.meaningMHz = Dictionary["meaningMHz"];

            if (Dictionary.ContainsKey("meaningkHz"))
                SMeaning.meaningkHz = Dictionary["meaningkHz"];

            if (Dictionary.ContainsKey("meaningmks"))
                SMeaning.meaningmks = Dictionary["meaningmks"];

            if (Dictionary.ContainsKey("meaningms"))
                SMeaning.meaningms = Dictionary["meaningms"];

            if (Dictionary.ContainsKey("meaningShum"))
                SMeaning.meaningShum = Dictionary["meaningShum"];

            if (Dictionary.ContainsKey("meaningNes"))
                SMeaning.meaningNes = Dictionary["meaningNes"];

            if (Dictionary.ContainsKey("meaningAMn"))
                SMeaning.meaningAMn = Dictionary["meaningAMn"];

            if (Dictionary.ContainsKey("meaningFMn"))
                SMeaning.meaningFMn = Dictionary["meaningFMn"];

            if (Dictionary.ContainsKey("meaningChMn2"))
                SMeaning.meaningChMn2 = Dictionary["meaningChMn2"];

            if (Dictionary.ContainsKey("meaningAMChM"))
                SMeaning.meaningAMChM = Dictionary["meaningAMChM"];

            if (Dictionary.ContainsKey("meaningChMn4"))
                SMeaning.meaningChMn4 = Dictionary["meaningChMn4"];

            if (Dictionary.ContainsKey("meaningChMn8"))
                SMeaning.meaningChMn8 = Dictionary["meaningChMn8"];

            if (Dictionary.ContainsKey("meaningChM"))
                SMeaning.meaningChM = Dictionary["meaningChM"];

            if (Dictionary.ContainsKey("meaningChMn"))
                SMeaning.meaningChMn = Dictionary["meaningChMn"];

            if (Dictionary.ContainsKey("meaningShPS"))
                SMeaning.meaningShPS = Dictionary["meaningShPS"];

            if (Dictionary.ContainsKey("meaningUnkn"))
                SMeaning.meaningUnkn = Dictionary["meaningUnkn"];

        }

        /// <summary>
        /// Переименование значений при смене языка
        /// </summary>
        /// <param name="Dictionary"></param>
        public void RenameParams(SortedList<string, string> Dictionary)
        {
            if (Dictionary.ContainsKey("paramChMSh"))
                SParams.paramChMSh = Dictionary["paramChMSh"];

            if (Dictionary.ContainsKey("paramChM2"))
                SParams.paramChM2 = Dictionary["paramChM2"];

            if (Dictionary.ContainsKey("paramChM4"))
                SParams.paramChM4 = Dictionary["paramChM4"];

            if (Dictionary.ContainsKey("paramChM8"))
                SParams.paramChM8 = Dictionary["paramChM8"];

            if (Dictionary.ContainsKey("paramFMn"))
                SParams.paramFMn = Dictionary["paramFMn"];

            if (Dictionary.ContainsKey("paramFMn4"))
                SParams.paramFMn4 = Dictionary["paramFMn4"];

            if (Dictionary.ContainsKey("paramFMn8"))
                SParams.paramFMn8 = Dictionary["paramFMn8"];

            if (Dictionary.ContainsKey("paramZSh"))
                SParams.paramZSh = Dictionary["paramZSh"];
        }


    }
}
