﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyDataGridView
{
    public class PropertyDGV
    {
        /// <summary>
        /// Установить свойства dgv
        /// </summary>
        public void SetPropertyDGV(DataGridView dgv)
        {
            // Заголовок таблицы
            dgv.RowHeadersVisible = false; // отображение столбца, содержащего заголовки строк
            dgv.EnableHeadersVisualStyles = false; // использование текущей темы для заголовков строк и столбцов
            dgv.RowHeadersBorderStyle = DataGridViewHeaderBorderStyle.Sunken; // стиль границ для ячеек заголовков строк
            dgv.ColumnHeadersDefaultCellStyle.BackColor = System.Drawing.Color.LightGray; // Цвет фона ячейки
            dgv.ColumnHeadersDefaultCellStyle.ForeColor = System.Drawing.Color.Blue; // Основной цвет ячейки
            dgv.ColumnHeadersDefaultCellStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 8, System.Drawing.FontStyle.Regular);
            dgv.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter; // положение содержимого ячейки
            dgv.SelectionMode = DataGridViewSelectionMode.FullRowSelect; // как можно выбрать ячейки
            dgv.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize; // поведение при изменении высоты заголовков столбцов
            dgv.ScrollBars = ScrollBars.Vertical;//.Both; // полоса прокрутки

            dgv.Font = new System.Drawing.Font("Arial Narrow", 8, System.Drawing.FontStyle.Regular);

            dgv.ColumnHeadersVisible = true; // Отображение строки заголовков столбцов
            dgv.EnableHeadersVisualStyles = false; // Использование визуальный стилей

            dgv.AllowUserToAddRows = false; // Разрешение пользователю добавлять строки
            dgv.AllowUserToDeleteRows = false; // Разрешение пользователю удалять строки
            dgv.AllowUserToResizeRows = false; // Разрешение пользователю изменять  размер строк
            dgv.AllowUserToResizeColumns = false; // Разрешение пользователю изменять размер столбцов

            dgv.RowHeadersBorderStyle = DataGridViewHeaderBorderStyle.Raised; // Стиль границы ячеек заголовков строк
            dgv.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.Raised; // Стиль границы, применяемый к заголовкам столбцов
            dgv.RowHeadersVisible = false; // Отображение столбца, содержащего заголовки строк

            dgv.BackgroundColor = System.Drawing.Color.Gainsboro; // Цвет фона для объекта
            dgv.CellBorderStyle = DataGridViewCellBorderStyle.Sunken; // Стиль границы ячейки

            dgv.MultiSelect = false; // Разрешение пользователю одновременно выбирать более одной ячейки, строки или столбца
            dgv.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.LightSteelBlue; // Цвет фона ячейки, когда она выбрана
            dgv.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.Black; // Основной цвет ячейки, когда она выбрана
            dgv.RowTemplate.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter; // Выравнивание текста
            dgv.RowTemplate.DefaultCellStyle.BackColor = System.Drawing.Color.Gainsboro; // Цвет фона ячейки
            dgv.RowTemplate.Height = 20; // Текущая высота строки
        }

        /// <summary>
        /// Установить цвет шрифта для строки dgv
        /// </summary>
        public void SetForeColorForRowDGV(DataGridView dgv, int numRow, System.Drawing.Color color)
        {
            dgv.Rows[numRow].DefaultCellStyle.ForeColor = color;
        
        }
    }
}
