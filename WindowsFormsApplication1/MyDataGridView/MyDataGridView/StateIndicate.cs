﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyDataGridView
{
    public class StateIndicate
    {
        private PictureBox pbIndicateByte;
        private Image imgStateActive;
        private Image imgStateNonactive;
        short sTimeIndicate;

        private System.Threading.Timer tmShowByte;

        public StateIndicate(PictureBox pbByte, Image imgActive, Image imgNonactive, short sTime)
        {
            pbIndicateByte = pbByte;
            imgStateActive = imgActive;
            imgStateNonactive = imgNonactive;

            sTimeIndicate = sTime;
        }

        public void SetIndicateOn()
        {
            if (pbIndicateByte.InvokeRequired)
            {
                pbIndicateByte.Invoke((MethodInvoker)(delegate()
                {
                    pbIndicateByte.Image = imgStateActive;
                    tmShowByte = new System.Threading.Timer(TimeShowByte, null, sTimeIndicate, 0);
                }));
            }
            else
            {
                pbIndicateByte.Image = imgStateActive;
                tmShowByte = new System.Threading.Timer(TimeShowByte, null, sTimeIndicate, 0);
            }
        }

        private void TimeShowByte(object o)
        {
            SetIndicateOff();
        }

        private void SetIndicateOff()
        {
            if (pbIndicateByte.InvokeRequired)
            {
                pbIndicateByte.Invoke((MethodInvoker)(delegate()
                {
                    pbIndicateByte.Image = imgStateNonactive;
                }));
            }
            else
            {
                pbIndicateByte.Image = imgStateNonactive;
            }
        }
    }
}
