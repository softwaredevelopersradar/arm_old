﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using VariableStatic;

namespace MyDataGridView
{
    public class InitParams
    {
        VariableCommon vc = new VariableCommon();
        /// <summary>
        /// установить возможные значения модуляции
        /// </summary>
        public void InitModulation(ComboBox cb)
        {
            // виды модуляции
            cb.Items.Clear();

            cb.Items.Add(SParams.paramChMSh);
            cb.Items.Add(SParams.paramChM2);
            cb.Items.Add(SParams.paramChM4);
            cb.Items.Add(SParams.paramChM8);
            cb.Items.Add(SParams.paramFMn);
            cb.Items.Add(SParams.paramFMn4);
            cb.Items.Add(SParams.paramFMn8);
            cb.Items.Add(SParams.paramZSh);

            cb.SelectedIndex = 4;
        }

        /// <summary>
        /// установить возможные значения длительности излучения
        /// </summary>
        public void InitDuration(ComboBox cb)
        {
            // значения длительностей
            cb.Items.Clear();

            cb.Items.Add("0.5");
            cb.Items.Add("1");
            cb.Items.Add("1.5");
            cb.Items.Add("2");

            cb.SelectedIndex = 0;
        }

        /// <summary>
        /// установить возможные виды девиации (12 значений)
        /// </summary>
        public void InitDeviation(ComboBox cb, int countItems)
        {
            cb.Items.Clear();

            switch (countItems)
            {
                case 1:
                    cb.Items.Add("2500");

                    cb.SelectedIndex = 0;

                    break;

                case 12:
                    cb.Items.Add("2");
                    cb.Items.Add("4");
                    cb.Items.Add("6");
                    cb.Items.Add("8");
                    cb.Items.Add("10");
                    cb.Items.Add("12");
                    cb.Items.Add("14");
                    cb.Items.Add("16");
                    cb.Items.Add("18");
                    cb.Items.Add("20");
                    cb.Items.Add("22");
                    cb.Items.Add("24");

                    cb.SelectedIndex = 0;

                    break;

                case 19:
                    cb.Items.Add("1,75");
                    cb.Items.Add("2,47");
                    cb.Items.Add("3,5");
                    cb.Items.Add("4,2");
                    cb.Items.Add("5,0");
                    cb.Items.Add("7,07");
                    cb.Items.Add("10");
                    cb.Items.Add("15,8");
                    cb.Items.Add("25");
                    cb.Items.Add("35,35");
                    cb.Items.Add("50");
                    cb.Items.Add("70");
                    cb.Items.Add("100");
                    cb.Items.Add("150");
                    cb.Items.Add("250");
                    cb.Items.Add("350");
                    cb.Items.Add("500");
                    cb.Items.Add("700");
                    cb.Items.Add("1000");

                    cb.SelectedIndex = 0;

                    break;
            }
        }

        /// <summary>
        /// установить возможные значения манипуляции
        /// </summary>
        public void InitManipulation(ComboBox cb, int countItems, byte typeStation)
        {
            cb.Items.Clear();

            switch (countItems)
            {
                case 1:
                    cb.Items.Add("0,2");

                    cb.SelectedIndex = 0;

                    break;

                case 6:
                    cb.Items.Add("62,5");
                    cb.Items.Add("125");
                    cb.Items.Add("250");
                    cb.Items.Add("500");
                    cb.Items.Add("1000");
                    cb.Items.Add("2000");

                    cb.SelectedIndex = 0;

                    break;

                case 9:
                    cb.Items.Add("0,2");
                    cb.Items.Add("0,5");
                    cb.Items.Add("1");
                    cb.Items.Add("5");
                    cb.Items.Add("10");
                    cb.Items.Add("20");
                    cb.Items.Add("40");
                    cb.Items.Add("80");
                    cb.Items.Add("400");

                    cb.SelectedIndex = 0;

                    break;

                case 10:
                    cb.Items.Add("0,08");
                    cb.Items.Add("0,2");
                    cb.Items.Add("0,5");
                    cb.Items.Add("1");
                    cb.Items.Add("5");
                    cb.Items.Add("10");
                    cb.Items.Add("20");
                    cb.Items.Add("40");
                    cb.Items.Add("80");
                    cb.Items.Add("400");

                    cb.SelectedIndex = 0;

                    break;
            }
        }

        /// <summary>
        /// значение девиации
        /// </summary>
        /// <param name="iInd"> индекс в ComboBox </param>
        public void SetDuration(int iInd, ComboBox cb)
        {
            if (iInd == -1) { cb.SelectedIndex = 0; }
            else { cb.SelectedIndex = iInd; }
        }

        /// <summary>
        /// отобразить виды девиации (в зависимости от модуляции)
        /// </summary>
        /// <param name="iTypeModulation"> тип модуляции </param>
        /// <param name="iInd"> индекс </param>
        public void SetDeviation(int iTypeModulation, int iInd, ComboBox cb)
        {
            switch (iTypeModulation)
            {
                // ЧМШ
                case 0:
                    cb.Enabled = true;
                    InitDeviation(cb, 19);
                    cb.SelectedIndex = iInd;
                    break;

                // ЧМ-2
                case 1:
                    cb.Enabled = true;
                    InitDeviation(cb, 12);
                    cb.SelectedIndex = iInd;
                    break;

                // ЧМ-4
                case 2:
                    cb.Enabled = true;
                    InitDeviation(cb, 12);
                    cb.SelectedIndex = iInd;
                    break;

                // ЧМ-8
                case 3:
                    cb.Enabled = true;
                    InitDeviation(cb, 12);
                    cb.SelectedIndex = iInd;
                    break;

                // ЗШ
                case 7:
                    cb.Enabled = true;
                    InitDeviation(cb, 1);
                    break;

                default:
                    cb.Items.Clear();
                    cb.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// отобразить виды манипуляции (в зависимости от модуляции)
        /// </summary>
        /// <param name="iTypeModulation"> тип модуляции </param>
        /// <param name="iInd"> индекс </param>
        public void SetManipulation(int iTypeModulation, int iInd, ComboBox cb, byte typeStation)
        {
            switch (iTypeModulation)
            {
                // ЧМ-2
                case 1:
                    cb.Enabled = true;
                    InitManipulation(cb, 6, typeStation);
                    cb.SelectedIndex = iInd;
                    break;

                // ЧМ-4
                case 2:
                    cb.Enabled = true;
                    InitManipulation(cb, 6, typeStation);
                    cb.SelectedIndex = iInd;
                    break;

                // ЧМ-8
                case 3:
                    cb.Enabled = true;
                    InitManipulation(cb, 6, typeStation);
                    cb.SelectedIndex = iInd;
                    break;

                // ФМн
                case 4:
                    //cb.Enabled = true;
                    //InitManipulation(cb, 9, typeStation);
                    //cb.SelectedIndex = iInd;
                    //break;
                // ФМн-4
                case 5:
                    //cb.Enabled = true;
                    //InitManipulation(cb, 9, typeStation);
                    //cb.SelectedIndex = iInd;
                    //break;
                // ФМн-8
                case 6:
                    if (typeStation == 2)
                    {
                        cb.Enabled = true;
                        InitManipulation(cb, 10, typeStation);
                        cb.SelectedIndex = iInd + 1;
                    }
                    else
                    {
                        cb.Enabled = true;
                        InitManipulation(cb, 9, typeStation);
                        cb.SelectedIndex = iInd;
                    }
                    break;

                // ЗШ
                case 7:
                    cb.Enabled = true;
                    InitManipulation(cb, 1, typeStation);
                    cb.SelectedIndex = iInd;
                    break;

                default:
                    cb.Enabled = false;
                    cb.Items.Clear();
                    break;
            }

        }

        private void InitRangeFrequencies(NumericUpDown nudFmin, NumericUpDown nudFmax, int min, int max)
        {
            nudFmin.Minimum = min;
            nudFmin.Maximum = max;
            nudFmin.Value = min;

            nudFmax.Minimum = min;
            nudFmax.Maximum = max;
            nudFmax.Value = min;
        }

        private void InitRangeFrequencies(NumericUpDown nudFmin, NumericUpDown nudFmax, NumericUpDown nudFexclude, int min, int max)
        {
            nudFmin.Minimum = min;
            nudFmin.Maximum = max;

            nudFmax.Minimum = min;
            nudFmax.Maximum = max;

            nudFexclude.Minimum = min;
            nudFexclude.Maximum = max;
        }

        private void InitRangeFrequencies(NumericUpDown nudFreq, int min, int max)
        {
            nudFreq.Minimum = min;
            nudFreq.Maximum = max;
        }

        /// <summary>
        /// Инициализация диапазона ППРЧ на РП
        /// typeStation = 0 - до 1215000 Гц
        /// typeStation = 1 = до 3000000 Гц
        /// typeStation = 2 - до 3000000 Гц
        /// </summary>
        public void Init3000_6000_PPRCh_RP(byte typeStation, NumericUpDown nudFmin, NumericUpDown nudFmax, NumericUpDown nudFexclude)
        {
            switch (typeStation)
            {
                case 0:
                    InitRangeFrequencies(nudFmin, nudFmax, nudFexclude, 30000, 1215000);
                    break;

                case 1:
                case 2:
                    InitRangeFrequencies(nudFmin, nudFmax, nudFexclude, 30000, 3000000);
                    break;

                default:
                    InitRangeFrequencies(nudFmin, nudFmax, nudFexclude, 30000, 1215000);
                    break;
            }
        }

        /// <summary>
        /// Инициализация диапазона ФРЧ на РП
        /// typeStation = 0 - до 1215000 Гц
        /// typeStation = 1 = до 3000000 Гц
        /// typeStation = 2 - до 3000000 Гц
        /// </summary>
        public void Init3000_6000_FRCh_RP(byte typeStation, NumericUpDown nudFreq, GroupBox gbIRID_INM)
        {
            switch (typeStation)
            {
                case 0:
                    InitRangeFrequencies(nudFreq, 30000, 1215000);
                    gbIRID_INM.Visible = false;
                    break;

                case 1:
                    InitRangeFrequencies(nudFreq, 30000, 3000000);
                    gbIRID_INM.Visible = false;
                    break;

                case 2:
                    InitRangeFrequencies(nudFreq, 30000, 3000000);
                    gbIRID_INM.Visible = true;
                    break;

                default:
                    InitRangeFrequencies(nudFreq, 30000, 1215000);
                    gbIRID_INM.Visible = false;
                    break;
            }
        }

        /// <summary>
        /// Инициализация диапазона Сектора и диапазоны РР
        /// typeStation = 0 - до 3025000 Гц
        /// typeStation = 1 = до 6025000 Гц
        /// typeStation = 2 - до 3025000 Гц
        /// </summary>
        public void Init3000_6000_RangeRR(byte typeStation, NumericUpDown nudFmin, NumericUpDown nudFmax)
        {
            switch (typeStation)
            {
                case 0:
                case 2:
                    InitRangeFrequencies(nudFmin, nudFmax, 25000, 3025000);
                    break;

                case 1:
                    InitRangeFrequencies(nudFmin, nudFmax, 25000, 6025000);
                    break;

                default:
                    InitRangeFrequencies(nudFmin, nudFmax, 25000, 3025000);
                    break;
            }
            
        }

        /// <summary>
        /// Инициализация диапазона Сектора и диапазоны РП
        /// typeStation = 0 - до 1215000 Гц
        /// typeStation = 1 = до 3000000 Гц
        /// </summary>
        public void Init3000_6000_RangeRP(byte typeStation, NumericUpDown nudFmin, NumericUpDown nudFmax)
        {
            switch (typeStation)
            {
                case 0:
                    InitRangeFrequencies(nudFmin, nudFmax, 30000, 1215000);
                    break;

                case 1:
                case 2:
                    InitRangeFrequencies(nudFmin, nudFmax, 30000, 3000000);
                    break;

                default:
                    InitRangeFrequencies(nudFmin, nudFmax, 30000, 1215000);
                    break;
            }
        }
    }
}
