﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyDataGridView
{
    #region диапазоны литер
    public struct RangesLetters
    {
        public static int FREQ_START_LETTER_1 = 300000;
        public static int FREQ_START_LETTER_2 = 500000;
        public static int FREQ_START_LETTER_3 = 900000;
        public static int FREQ_START_LETTER_4 = 1600000;
        public static int FREQ_START_LETTER_5 = 2900000;
        public static int FREQ_START_LETTER_6 = 5120000;
        public static int FREQ_START_LETTER_7 = 8600000;
        public static int FREQ_STOP_LETTER_7 = 12150000;
        public static int FREQ_START_LETTER_8 = 12150000;
        public static int FREQ_START_LETTER_9 = 20000000;
        public static int FREQ_STOP_LETTER_9 = 30000000;
    }
    #endregion
}
