﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Reflection;
using VariableStatic;

namespace MyDataGridView
{
    public partial class myDataGridView: UserControl
    {
        #region count column
        public const int COUNT_COLUMN_IRI_FRCh = 15;
        public const int COUNT_COLUMN_IRI_FRCh_CR = 12;
        public const int COUNT_COLUMN_IRI_FRCh_RP = 15;
        public const int COUNT_COLUMN_IRI_PPRCh = 10;
        public const int COUNT_COLUMN_IRI_PPRCh2 = 5;
        public const int COUNT_COLUMN_IRI_PPRCh3 = 3;

        public const int COUNT_COLUMN_IRI_PPRCh_RP = 13;
        public const int COUNT_COLUMN_IRI_PPRCh_RP2 = 3;
        public const int COUNT_COLUMN_IRI_PPRCh_RP_EXCLUDE = 4;

        public const int COUNT_COLUMN_AR_ONE = 14;
        public const int COUNT_COLUMN_RANGE_RR = 6;
        public const int COUNT_COLUMN_RANGE_RP = 6;
        public const int COUNT_COLUMN_FREQ_FORBIDDEN = 4;
        public const int COUNT_COLUMN_FREQ_KNOWN = 4;
        public const int COUNT_COLUMN_FREQ_IMPORTANT = 4;
        public const int COUNT_COLUMN_ADSB_RECEIVER = 6;
        #endregion

        TableFieldHeader tableFieldHeader;
        STableFieldName tableFieldName;

        public TableColumn[] tableIRI_FRCh, tableIRI_FRCh_CR, tableIRI_FRCh_RP, tableIRI_PPRCh, tableIRI_PPRCh2, tableIRI_PPRCh3,
            tableIRI_PPRCh_RP, tableIRI_PPRCh_RP2, tableIRI_PPRCh_RP_Exclude,
            tableAR_ONE, tableRangeRR, tableRangeRP, tableFreqForbidden, tableFreqKnown, tableFreqImportant, tableADSB_Receiver, tableDEFAULT;
        
        PropertyDGV propertyDGV;
        Functions functions;
        FunctionsTranslate functionsTranslate;


        #region flagInitTable
        private bool fInitIRI_FRCh = false;
        private bool fInitIRI_FRCh_CR = false;
        private bool fInitIRI_FRCh_RP = false;
        private bool fInitIRI_PPRCh = false;
        private bool fInitIRI_PPRCh2 = false;
        private bool fInitIRI_PPRCh3 = false;
        private bool fInitIRI_PPRCh_RP = false;
        private bool fInitIRI_PPRCh_RP2 = false;
        private bool fInitIRI_PPRCh_RP_Exclude = false;
        private bool fInitAR_ONE = false;
        private bool fInitRangeRR = false;
        private bool fInitRangeRP = false;
        private bool fInitFreqForbidden = false;
        private bool fInitFreqKnown = false;
        private bool fInitFreqImportant = false;
        private bool fInitADSBReceiver = false;

        #endregion

        public myDataGridView()
        {
            InitializeComponent();

            tableFieldHeader = new TableFieldHeader();
            tableFieldHeader.initTableFieldHeader();

            tableFieldName = new STableFieldName();
            tableFieldName.initTableFieldName();

            propertyDGV = new PropertyDGV();
            functions = new Functions();
            functionsTranslate = new FunctionsTranslate();

            tableIRI_FRCh = new TableColumn[COUNT_COLUMN_IRI_FRCh];
            tableIRI_FRCh_CR = new TableColumn[COUNT_COLUMN_IRI_FRCh_CR];
            tableIRI_FRCh_RP = new TableColumn[COUNT_COLUMN_IRI_FRCh_RP];
            tableIRI_PPRCh = new TableColumn[COUNT_COLUMN_IRI_PPRCh];
            tableIRI_PPRCh2 = new TableColumn[COUNT_COLUMN_IRI_PPRCh2];
            tableIRI_PPRCh3 = new TableColumn[COUNT_COLUMN_IRI_PPRCh3];
            tableIRI_PPRCh_RP = new TableColumn[COUNT_COLUMN_IRI_PPRCh_RP];
            tableIRI_PPRCh_RP2 = new TableColumn[COUNT_COLUMN_IRI_PPRCh_RP2];
            tableIRI_PPRCh_RP_Exclude = new TableColumn[COUNT_COLUMN_IRI_PPRCh_RP_EXCLUDE];
            tableAR_ONE = new TableColumn[COUNT_COLUMN_AR_ONE];
            tableRangeRR = new TableColumn[COUNT_COLUMN_RANGE_RR];
            tableRangeRP = new TableColumn[COUNT_COLUMN_RANGE_RP];
            tableFreqForbidden = new TableColumn[COUNT_COLUMN_FREQ_FORBIDDEN]; 
            tableFreqKnown = new TableColumn[COUNT_COLUMN_FREQ_KNOWN]; 
            tableFreqImportant = new TableColumn[COUNT_COLUMN_FREQ_IMPORTANT];
            tableADSB_Receiver = new TableColumn[COUNT_COLUMN_ADSB_RECEIVER];

            FillStructTableIRI_FRCh();
            FillStructTableIRI_FRCh_CR();
            FillStructTableIRI_FRCh_RP();
            FillStructTableIRI_PPRCh();
            FillStructTableIRI_PPRCh2(); 
            FillStructTableIRI_PPRCh3();
            FillStructTableIRI_PPRCh_RP();
            FillStructTableIRI_PPRCh_RP2();
            FillStructTableIRI_PPRCh_RP_Exclude();
            FillStructTableAR_ONE();
            FillStructTableRangeRR();
            FillStructTableRangeRP();
            FillStructTableFreqForbidden();
            FillStructTableFreqKnown();
            FillStructTableFreqImportant();
            FillStructTableADSBReceiver();

            ///////////////////////////////////////////// OTL
            //InitTableDB(dgv, NameTable.IRI_FRCh);
            //InitTableDB(dgv, NameTable.IRI_FRCh_CR);
            //InitTableDB(dgv, NameTable.IRI_FRCh_RP);
            //InitTableDB(dgv, NameTable.IRI_PPRCh);
            //InitTableDB(dgv, NameTable.IRI_PPRCh2);
            //InitTableDB(dgv, NameTable.IRI_PPRCh3);
            //InitTableDB(dgv, NameTable.IRI_PPRCh_RP);
            //InitTableDB(dgv, NameTable.IRI_PPRCh_RP2);
            //InitTableDB(dgv, NameTable.AR_ONE);
            //InitTableDB(dgv, NameTable.RANGE_RR);
            //InitTableDB(dgv, NameTable.RANGE_RP);
            //InitTableDB(dgv, NameTable.FREQ_FORBIDDEN);
            //InitTableDB(dgv, NameTable.FREQ_KNOWN);
            //InitTableDB(dgv, NameTable.FREQ_IMPORTANT);
            //InitTableDB(dgv, NameTable.ADSB_RECEIVER);
            ///////////////////////////////////////////// OTL

            VariableCommon.OnChangeCommonLanguage += new VariableCommon.ChangeCommonEventHandler(VariableCommon_OnChangeCommonLanguage);
        }

        private void VariableCommon_OnChangeCommonLanguage()
        {
            VariableCommon variableCommon = new VariableCommon();
            switch (variableCommon.Language)
            {
                case 0:
                    functionsTranslate.SetLanguage("rus");
                    ChangeLanguage();
                    break;

                case 1:
                    functionsTranslate.SetLanguage("eng");
                    ChangeLanguage();
                    break;

                case 2:
                    functionsTranslate.SetLanguage("az");
                    ChangeLanguage();
                    break;

                default:
                    functionsTranslate.SetLanguage("rus");
                    ChangeLanguage();
                    break;
            }
        }

        /// <summary>
        /// Для инициализации языка при запуске приложения 
        /// </summary>
        /// <param name="bLanguage"></param>
        public void ChangeMessageLanguage(byte bLanguage)
        {

            switch (bLanguage)
            {
                case 0:
                    functionsTranslate.SetLanguage("rus");
                    ChangeLanguage();
                    break;

                case 1:
                    functionsTranslate.SetLanguage("eng");
                    ChangeLanguage();
                    break;

                case 2:
                    functionsTranslate.SetLanguage("az");
                    ChangeLanguage();
                    break;

                default:
                    functionsTranslate.SetLanguage("rus");
                    ChangeLanguage();
                    break;
            }
        }

        /// <summary>
        /// Изменение языка сообщений
        /// </summary>
        private void ChangeLanguage()
        {
            functionsTranslate.RenameMessages(functionsTranslate.Dictionary);
            functionsTranslate.RenameMeaning(functionsTranslate.Dictionary);
            functionsTranslate.RenameParams(functionsTranslate.Dictionary);
        }



        /// <summary>
        /// Добавить поля в таблицу (в зависимости от названия таблицы)
        /// </summary>
        /// <param name="nameTable"> название таблицы </param>
        public void InitTableDB(DataGridView dgv, NameTable nameTable)
        {
            propertyDGV.SetPropertyDGV(dgv);

            switch (nameTable)
            { 
                case NameTable.IRI_FRCh:

                    AddColumnToTable(dgv, tableIRI_FRCh);
                    SetInitialNumberOfRows(dgv, tableIRI_FRCh, NameTable.IRI_FRCh);
                    fInitIRI_FRCh = true;
                    break;

                case NameTable.IRI_FRCh_CR:

                     AddColumnToTable(dgv, tableIRI_FRCh_CR);
                     SetInitialNumberOfRows(dgv, tableIRI_FRCh_CR, NameTable.IRI_FRCh_CR);
                     fInitIRI_FRCh_CR = true;
                     break;

                case NameTable.IRI_FRCh_RP:

                     AddColumnToTable(dgv, tableIRI_FRCh_RP);
                     SetInitialNumberOfRows(dgv, tableIRI_FRCh_RP, NameTable.IRI_FRCh_RP);
                     fInitIRI_FRCh_RP = true;
                     break;

                case NameTable.IRI_PPRCh:
                    
                     AddColumnToTable(dgv, tableIRI_PPRCh);
                     SetInitialNumberOfRows(dgv, tableIRI_PPRCh, NameTable.IRI_PPRCh);
                     fInitIRI_PPRCh = true;
                     break;

                case NameTable.IRI_PPRCh2:

                     AddColumnToTable(dgv, tableIRI_PPRCh2);
                     SetInitialNumberOfRows(dgv, tableIRI_PPRCh2, NameTable.IRI_PPRCh2);
                     fInitIRI_PPRCh2 = true;
                     break;

                case NameTable.IRI_PPRCh3:

                     AddColumnToTable(dgv, tableIRI_PPRCh3);
                     SetInitialNumberOfRows(dgv, tableIRI_PPRCh3, NameTable.IRI_PPRCh3);
                     fInitIRI_PPRCh3 = true;
                     break;

                case NameTable.IRI_PPRCh_RP:

                     AddColumnToTable(dgv, tableIRI_PPRCh_RP);
                     SetInitialNumberOfRows(dgv, tableIRI_PPRCh_RP, NameTable.IRI_PPRCh_RP);
                     fInitIRI_PPRCh_RP = true;
                     break;

                case NameTable.IRI_PPRCh_RP2:

                     AddColumnToTable(dgv, tableIRI_PPRCh_RP2);
                     SetInitialNumberOfRows(dgv, tableIRI_PPRCh_RP2, NameTable.IRI_PPRCh_RP2);
                     fInitIRI_PPRCh_RP2 = true;
                     break;

                case NameTable.IRI_PPRCh_RP_EXCLUDE:

                     AddColumnToTable(dgv, tableIRI_PPRCh_RP_Exclude);
                     SetInitialNumberOfRows(dgv, tableIRI_PPRCh_RP_Exclude, NameTable.IRI_PPRCh_RP_EXCLUDE);
                     fInitIRI_PPRCh_RP_Exclude = true;
                     break;

                case NameTable.AR_ONE:

                     AddColumnToTable(dgv, tableAR_ONE);
                     SetInitialNumberOfRows(dgv, tableAR_ONE, NameTable.AR_ONE);
                     fInitAR_ONE = true;
                     break;

                case NameTable.RANGE_RR:

                     AddColumnToTable(dgv, tableRangeRR);
                     SetInitialNumberOfRows(dgv, tableRangeRR, NameTable.RANGE_RR);
                     fInitRangeRR = true;
                     break;

                case NameTable.RANGE_RP:

                     AddColumnToTable(dgv, tableRangeRP);
                     SetInitialNumberOfRows(dgv, tableRangeRP, NameTable.RANGE_RP);
                     fInitRangeRP = true;
                     break;

                case NameTable.FREQ_FORBIDDEN:

                     AddColumnToTable(dgv, tableFreqForbidden);
                     SetInitialNumberOfRows(dgv, tableFreqForbidden, NameTable.FREQ_FORBIDDEN);
                     fInitFreqForbidden = true;
                     break;

                case NameTable.FREQ_KNOWN:

                     AddColumnToTable(dgv, tableFreqKnown);
                     SetInitialNumberOfRows(dgv, tableFreqKnown, NameTable.FREQ_KNOWN);
                     fInitFreqKnown = true;
                     break;

                case NameTable.FREQ_IMPORTANT:

                     AddColumnToTable(dgv, tableFreqImportant);
                     SetInitialNumberOfRows(dgv, tableFreqImportant, NameTable.FREQ_IMPORTANT);
                     fInitFreqImportant = true;
                     break;

                case NameTable.ADSB_RECEIVER:

                     AddColumnToTable(dgv, tableADSB_Receiver);
                     SetInitialNumberOfRows(dgv, tableADSB_Receiver, NameTable.ADSB_RECEIVER);
                     fInitADSBReceiver = true;
                     break;

                default:
                     break;
                 
            }
        }

        /// <summary>
        /// Добавить поля в таблицу (в зависимости от названия таблицы)
        /// </summary>
        /// <param name="nameTable"> название таблицы </param>
        public void InitTableDefault(DataGridView dgv, TableColumn[] tableColumn)
        {
            if (tableColumn.Length != 0)
            {
                AddColumnToTable(dgv, tableColumn);
                SetInitialNumberOfRows(dgv, tableColumn, NameTable.DEFAULT);
            }
        }

        /// <summary>
        /// Установить ширину столбцов таблицы
        /// </summary>
        /// <param name="countColumns"></param>
        public void SetWidthColumns(DataGridView dgv, TableColumn[] table)
        {
            int coefWidthHederTable = 0;
            int widthHederTable = new int();
            int len = 0;

            for (int i = 0; i < table.Length; i++)
            {
                if (table[i].widthField != 0)
                {
                    widthHederTable += table[i].widthField; // длина всех столбцов, не равных 0
                    len++; // количество столбцов, не равных 0
                }
            }

            if (dgv.Width > widthHederTable)
            {
                coefWidthHederTable = Convert.ToInt32(Math.Floor(Convert.ToDouble(dgv.Width) - Convert.ToDouble(widthHederTable)) / len);

                if (table.Length != 0)
                {
                    for (int i = 0; i < table.Length; i++)
                    {
                        if (table[i].widthField != 0)
                        {
                            table[i].widthField = table[i].widthField + coefWidthHederTable;
                            dgv.Columns[i].Width = table[i].widthField;
                        }
                    }
                }
            }
            else if (dgv.Width < widthHederTable)
            {
                coefWidthHederTable = Convert.ToInt32(Math.Floor(Convert.ToDouble(widthHederTable) - Convert.ToDouble(dgv.Width)) / table.Length);

                if (table.Length != 0)
                {
                    for (int i = 0; i < table.Length; i++)
                    {
                        if (table[i].widthField != 0)
                        {
                            table[i].widthField = table[i].widthField - coefWidthHederTable;
                            dgv.Columns[i].Width = table[i].widthField;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Установить количество строк в таблице
        /// </summary>
        /// <param name="dgv"></param>
        /// <param name="table"></param>
        public void SetInitialNumberOfRows(DataGridView dgv, TableColumn[] tableColumn, NameTable nameTable)
        {
            int countRows = Convert.ToInt32(Math.Floor(Convert.ToDouble(dgv.Height) - Convert.ToDouble(dgv.ColumnHeadersHeight)) / dgv.RowTemplate.Height); // количество строк = (высота таблицы - заголовок таблицы) / высота строки
            
            string[] columns = new string[tableColumn.Length];

            switch (nameTable)
            {
                case NameTable.AR_ONE:

                    while (dgv.RowCount < countRows)
                        dgv.Rows.Add(null, false, null, null, null, null, null, null, null, null, null, null, null, null);

                    break;

                case NameTable.IRI_FRCh_RP:

                    while (dgv.RowCount < countRows)
                        dgv.Rows.Add(null, null, null, null, null, null, null, Properties.Resources.empty, Properties.Resources.empty, Properties.Resources.empty, null, null, null, null, null);

                    break;

                case NameTable.IRI_FRCh:

                    while (dgv.RowCount < countRows)
                        dgv.Rows.Add(null, Properties.Resources.empty, null, null, null, null, null, null, null, null, null, null, null, null, null);

                    break;

                case NameTable.IRI_PPRCh_RP:

                    while (dgv.RowCount < 4)
                        dgv.Rows.Add(null, null, null, null, null, null, Properties.Resources.empty, null, null, null, null, null, null);
                    //while (dgv.RowCount < countRows)
                    //    dgv.Rows.Add(null, null, null, null, null, null, Properties.Resources.empty, null, null, null, null, null, null);

                    break;

                default:

                    for (int i = 0; i < tableColumn.Length; i++)
                        columns[i] = null;

                    while (dgv.RowCount < countRows/* - 1*/)
                        dgv.Rows.Add(columns);

                    break;
            }
        }

        public int CountOfBeautifulRows(DataGridView dgv, TableColumn[] tableColumn, NameTable nameTable)
        {
            return Convert.ToInt32(Math.Floor(Convert.ToDouble(dgv.Height) - Convert.ToDouble(dgv.ColumnHeadersHeight)) / dgv.RowTemplate.Height); // количество строк = (высота таблицы - заголовок таблицы) / высота строки
        }

        /// <summary>
        /// Добавить колонки в таблицу
        /// </summary>
        /// <param name="countColumn"></param>
        /// <param name="table"></param>
        public void AddColumnToTable(DataGridView dgv, TableColumn[] table)
        {
            if (dgv.Columns.Count != 0)
            {
                while (dgv.Columns.Count > 0)
                    dgv.Columns.RemoveAt(0);
            }

            if (table.Length != 0)
            {
                for (int i = 0; i < table.Length; i++)
                {

                    switch (table[i].typeColumn)
                    {
                        case TypeColumn.TextBoxColumn:
                            dgv.Columns.Add(new DataGridViewTextBoxColumn()
                            {
                                HeaderText = table[i].fields,
                                Width = table[i].widthField,
                                Visible = table[i].bVisible,
                                Resizable = table[i].resizable,
                                Name = table[i].nameField
                            });
                            break;

                        case TypeColumn.ButtonColumn:

                            dgv.Columns.Add(new DataGridViewButtonColumn()
                            {
                                HeaderText = table[i].fields,
                                Width = table[i].widthField,
                                Visible = table[i].bVisible,
                                Resizable = table[i].resizable,
                                Name = table[i].nameField
                            });
                            break;

                        case TypeColumn.CheckBoxColumn:

                            dgv.Columns.Add(new DataGridViewCheckBoxColumn()
                            {
                                HeaderText = table[i].fields,
                                Width = table[i].widthField,
                                Visible = table[i].bVisible,
                                Resizable = table[i].resizable,
                                Name = table[i].nameField
                            });
                            break;

                        case TypeColumn.ComboBoxColumn:

                            dgv.Columns.Add(new DataGridViewComboBoxColumn()
                            {
                                HeaderText = table[i].fields,
                                Width = table[i].widthField,
                                Visible = table[i].bVisible,
                                Resizable = table[i].resizable,
                                Name = table[i].nameField
                            });
                            break;

                        case TypeColumn.ImageColumn:

                            dgv.Columns.Add(new DataGridViewImageColumn()
                            {
                                HeaderText = table[i].fields,
                                Width = table[i].widthField,
                                Visible = table[i].bVisible,
                                Resizable = table[i].resizable,
                                Name = table[i].nameField
                            });
                            break;

                        case TypeColumn.LinkColumn:

                            dgv.Columns.Add(new DataGridViewLinkColumn()
                            {
                                HeaderText = table[i].fields,
                                Width = table[i].widthField,
                                Visible = table[i].bVisible,
                                Resizable = table[i].resizable,
                                Name = table[i].nameField
                            });
                            break;

                        default:
                            break;
                    }

                    dgv.Columns[i].SortMode = DataGridViewColumnSortMode.NotSortable;
                    dgv.Columns[i].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

                    if ((table[i].typeColumn != TypeColumn.ComboBoxColumn) && (table[i].typeColumn != TypeColumn.CheckBoxColumn))
                    {
                        dgv.Columns[i].ReadOnly = true;
                    }

                }
            }
             
            SetWidthColumns(dgv, table);
        }

        /// <summary>
        /// Заполнить структуру таблицы ИРИ ФРЧ
        /// </summary>
        public void FillStructTableIRI_FRCh()
        {
            tableIRI_FRCh = new TableColumn[COUNT_COLUMN_IRI_FRCh];

            // заголовки полей
            tableIRI_FRCh[0].fields = tableFieldHeader.id; // номер
            tableIRI_FRCh[1].fields = tableFieldHeader.KR; // индикатор появления сигнала в эфире
            tableIRI_FRCh[2].fields = tableFieldHeader.F; // частота
            tableIRI_FRCh[3].fields = tableFieldHeader.Q1; // пеленг 1
            tableIRI_FRCh[4].fields = tableFieldHeader.Q2; // пеленг 2
            tableIRI_FRCh[5].fields = tableFieldHeader.level; // уровень
            tableIRI_FRCh[6].fields = tableFieldHeader.deltaF; // ширина полосы
            tableIRI_FRCh[7].fields = tableFieldHeader.CKO; // среднеквадр. отклонение
            tableIRI_FRCh[8].fields = tableFieldHeader.latitude; // широта
            tableIRI_FRCh[9].fields = tableFieldHeader.longitude; // долгота
            tableIRI_FRCh[10].fields = tableFieldHeader.view; // вид
            tableIRI_FRCh[11].fields = tableFieldHeader.SP; // станция помех
            tableIRI_FRCh[12].fields = tableFieldHeader.time; // время
            tableIRI_FRCh[13].fields = tableFieldHeader.durationSignal; // длительность сигнала в эфире
            tableIRI_FRCh[14].fields = tableFieldHeader.countExits; // количество выходов в эфир

            // имена полей
            tableIRI_FRCh[0].nameField = tableFieldName.id; // номер
            tableIRI_FRCh[1].nameField = tableFieldName.KR; // индикатор появления сигнала в эфире
            tableIRI_FRCh[2].nameField = tableFieldName.F; // частота
            tableIRI_FRCh[3].nameField = tableFieldName.Q1; // пеленг 1
            tableIRI_FRCh[4].nameField = tableFieldName.Q2; // пеленг 2
            tableIRI_FRCh[5].nameField = tableFieldName.level; // уровень
            tableIRI_FRCh[6].nameField = tableFieldName.deltaF; // ширина полосы
            tableIRI_FRCh[7].nameField = tableFieldName.CKO; // среднеквадр. отклонение
            tableIRI_FRCh[8].nameField = tableFieldName.latitude; // широта
            tableIRI_FRCh[9].nameField = tableFieldName.longitude; // долгота
            tableIRI_FRCh[10].nameField = tableFieldName.view; // вид
            tableIRI_FRCh[11].nameField = tableFieldName.SP; // станция помех
            tableIRI_FRCh[12].nameField = tableFieldName.time; // время
            tableIRI_FRCh[13].nameField = tableFieldName.durationSignal; // длительность сигнала в эфире
            tableIRI_FRCh[14].nameField = tableFieldName.countExits; // количество выходов в эфир

            // ширина столбцов
            tableIRI_FRCh[0].widthField = 0;  // номер
            tableIRI_FRCh[1].widthField = 40; // индикатор появления сигнала в эфире
            tableIRI_FRCh[2].widthField = 75; // частота
            tableIRI_FRCh[3].widthField = 45; // пеленг 1
            tableIRI_FRCh[4].widthField = 45; // пеленг 2
            tableIRI_FRCh[5].widthField = 55; // уровень
            tableIRI_FRCh[6].widthField = 55; // ширина полосы
            tableIRI_FRCh[7].widthField = 40; // среднеквадр. отклонение
            tableIRI_FRCh[8].widthField = 70; // широта
            tableIRI_FRCh[9].widthField = 70; // долгота
            tableIRI_FRCh[10].widthField = 50; // вид
            tableIRI_FRCh[11].widthField = 50; // станция помех
            tableIRI_FRCh[12].widthField = 60; // время
            tableIRI_FRCh[13].widthField = 50; // длительность сигнала в эфире
            tableIRI_FRCh[14].widthField = 0;// 50; // количество выходов в эфир
           
            // видимый/невидимый столбец, изменение размера столбца
            tableIRI_FRCh[0].bVisible = false;
            for (int i = 1; i < COUNT_COLUMN_IRI_FRCh - 1; i++)
                tableIRI_FRCh[i].bVisible = true;
            tableIRI_FRCh[14].bVisible = false;

            // изменение размера столбца
            tableIRI_FRCh[0].resizable = DataGridViewTriState.False;
            for (int i = 1; i < COUNT_COLUMN_IRI_FRCh - 1; i++)
                tableIRI_FRCh[i].resizable = DataGridViewTriState.True;
            tableIRI_FRCh[14].resizable = DataGridViewTriState.False;

            // тип столбцов
            tableIRI_FRCh[0].typeColumn = TypeColumn.TextBoxColumn;
            tableIRI_FRCh[1].typeColumn = TypeColumn.ImageColumn;
            for (int i = 2; i < COUNT_COLUMN_IRI_FRCh; i++)
                tableIRI_FRCh[i].typeColumn = TypeColumn.TextBoxColumn;
        }

        /// <summary>
        /// Заполнить структуру таблицы ИРИ ФРЧ на ЦР
        /// </summary>
        public void FillStructTableIRI_FRCh_CR()
        {
            tableIRI_FRCh_CR = new TableColumn[COUNT_COLUMN_IRI_FRCh_CR];

            // заголовки полей
            tableIRI_FRCh_CR[0].fields = tableFieldHeader.id;
            tableIRI_FRCh_CR[1].fields = tableFieldHeader.F;
            tableIRI_FRCh_CR[2].fields = tableFieldHeader.Q1;
            tableIRI_FRCh_CR[3].fields = tableFieldHeader.Q2;
            tableIRI_FRCh_CR[4].fields = tableFieldHeader.level;
            tableIRI_FRCh_CR[5].fields = tableFieldHeader.deltaF;
            tableIRI_FRCh_CR[6].fields = tableFieldHeader.CKO;
            tableIRI_FRCh_CR[7].fields = tableFieldHeader.latitude;
            tableIRI_FRCh_CR[8].fields = tableFieldHeader.longitude;
            tableIRI_FRCh_CR[9].fields = tableFieldHeader.view;
            tableIRI_FRCh_CR[10].fields = tableFieldHeader.SP_RR;
            tableIRI_FRCh_CR[11].fields = tableFieldHeader.SP_RP;

            // имена полей
            tableIRI_FRCh_CR[0].nameField = tableFieldName.id; 
            tableIRI_FRCh_CR[1].nameField = tableFieldName.F;  
            tableIRI_FRCh_CR[2].nameField = tableFieldName.Q1; 
            tableIRI_FRCh_CR[3].nameField = tableFieldName.Q2; 
            tableIRI_FRCh_CR[4].nameField = tableFieldName.level;
            tableIRI_FRCh_CR[5].nameField = tableFieldName.deltaF;
            tableIRI_FRCh_CR[6].nameField = tableFieldName.CKO; 
            tableIRI_FRCh_CR[7].nameField = tableFieldName.latitude; 
            tableIRI_FRCh_CR[8].nameField = tableFieldName.longitude; 
            tableIRI_FRCh_CR[9].nameField = tableFieldName.view; 
            tableIRI_FRCh_CR[10].nameField = tableFieldName.SP_RR; 
            tableIRI_FRCh_CR[11].nameField = tableFieldName.SP_RP; 

            // ширина столбцов
            tableIRI_FRCh_CR[0].widthField = 0;  // номер
            tableIRI_FRCh_CR[1].widthField = 75; // частота
            tableIRI_FRCh_CR[2].widthField = 45; // пеленг 1
            tableIRI_FRCh_CR[3].widthField = 45; // пеленг 2
            tableIRI_FRCh_CR[4].widthField = 45; // уровень
            tableIRI_FRCh_CR[5].widthField = 55; // ширина полосы
            tableIRI_FRCh_CR[6].widthField = 35; // среднеквадр. отклонение
            tableIRI_FRCh_CR[7].widthField = 70; // широта
            tableIRI_FRCh_CR[8].widthField = 70; // долгота
            tableIRI_FRCh_CR[9].widthField = 50; // вид
            tableIRI_FRCh_CR[10].widthField = 50;
            tableIRI_FRCh_CR[11].widthField = 50;

            // видимый/невидимый столбец
            tableIRI_FRCh_CR[0].bVisible = false;
            for (int i = 1; i < COUNT_COLUMN_IRI_FRCh_CR; i++)
                tableIRI_FRCh_CR[i].bVisible = true;

            // изменение размера столбца
            tableIRI_FRCh_CR[0].resizable = DataGridViewTriState.False;
            for (int i = 1; i < COUNT_COLUMN_IRI_FRCh_CR; i++)
                tableIRI_FRCh_CR[i].resizable = DataGridViewTriState.True;

            // тип столбцов
            for (int i = 0; i < COUNT_COLUMN_IRI_FRCh_CR; i++)
                tableIRI_FRCh_CR[i].typeColumn = TypeColumn.TextBoxColumn;
        }

        /// <summary>
        /// Заполнить структуру таблицы ИРИ ФРЧ на РП
        /// </summary>
        public void FillStructTableIRI_FRCh_RP()
        {
            tableIRI_FRCh_RP = new TableColumn[COUNT_COLUMN_IRI_FRCh_RP];

            // заголовки полей
            tableIRI_FRCh_RP[0].fields = tableFieldHeader.id;
            tableIRI_FRCh_RP[1].fields = tableFieldHeader.F;
            tableIRI_FRCh_RP[2].fields = tableFieldHeader.Q;
            tableIRI_FRCh_RP[3].fields = tableFieldHeader.litera;
            tableIRI_FRCh_RP[4].fields = tableFieldHeader.paramsNoise;
            tableIRI_FRCh_RP[5].fields = tableFieldHeader.U;
            tableIRI_FRCh_RP[6].fields = tableFieldHeader.priority;
            tableIRI_FRCh_RP[7].fields = tableFieldHeader.KR;
            tableIRI_FRCh_RP[8].fields = tableFieldHeader.RP;
            tableIRI_FRCh_RP[9].fields = tableFieldHeader.izl;
            tableIRI_FRCh_RP[10].fields = tableFieldHeader.modulation;
            tableIRI_FRCh_RP[11].fields = tableFieldHeader.deviation;
            tableIRI_FRCh_RP[12].fields = tableFieldHeader.manipulation;
            tableIRI_FRCh_RP[13].fields = tableFieldHeader.duration;
            tableIRI_FRCh_RP[14].fields = tableFieldHeader.mode;

            // имена полей
            tableIRI_FRCh_RP[0].nameField = tableFieldName.id;
            tableIRI_FRCh_RP[1].nameField = tableFieldName.F;
            tableIRI_FRCh_RP[2].nameField = tableFieldName.Q;
            tableIRI_FRCh_RP[3].nameField = tableFieldName.litera;
            tableIRI_FRCh_RP[4].nameField = tableFieldName.paramsNoise;
            tableIRI_FRCh_RP[5].nameField = tableFieldName.U;
            tableIRI_FRCh_RP[6].nameField = tableFieldName.priority;
            tableIRI_FRCh_RP[7].nameField = tableFieldName.KR;
            tableIRI_FRCh_RP[8].nameField = tableFieldName.RP;
            tableIRI_FRCh_RP[9].nameField = tableFieldName.izl;
            tableIRI_FRCh_RP[10].nameField = tableFieldName.modulation;
            tableIRI_FRCh_RP[11].nameField = tableFieldName.deviation;
            tableIRI_FRCh_RP[12].nameField = tableFieldName.manipulation;
            tableIRI_FRCh_RP[13].nameField = tableFieldName.duration;
            tableIRI_FRCh_RP[14].nameField = tableFieldName.mode;

            // ширина столбцов
            tableIRI_FRCh_RP[0].widthField = 0;  // номер
            tableIRI_FRCh_RP[1].widthField = 75; // частота
            tableIRI_FRCh_RP[2].widthField = 45; // пеленг 
            tableIRI_FRCh_RP[3].widthField = 25; // литера
            tableIRI_FRCh_RP[4].widthField = 150;// параметры помехи
            tableIRI_FRCh_RP[5].widthField = 45; // порог
            tableIRI_FRCh_RP[6].widthField = 30; // приоритет
            tableIRI_FRCh_RP[7].widthField = 25; // КР (контроль)
            tableIRI_FRCh_RP[8].widthField = 25; // РП (радиоподавление)
            tableIRI_FRCh_RP[9].widthField = 25; // Изл. (излучение)
            tableIRI_FRCh_RP[10].widthField = 0; // модуляция
            tableIRI_FRCh_RP[11].widthField = 0; // девиация
            tableIRI_FRCh_RP[12].widthField = 0; // манипуляция
            tableIRI_FRCh_RP[13].widthField = 0; // длительность
            tableIRI_FRCh_RP[14].widthField = 0; // режим (1 - ведущая, 2 - ведомая)

            // видимый/невидимый столбец
            tableIRI_FRCh_RP[0].bVisible = false;
            for (int i = 1; i < COUNT_COLUMN_IRI_FRCh_RP - 5; i++)
                tableIRI_FRCh_RP[i].bVisible = true;

            for (int i = 10; i < COUNT_COLUMN_IRI_FRCh_RP; i++)
                tableIRI_FRCh_RP[i].bVisible = false;

            // изменение размера столбца
            tableIRI_FRCh_RP[0].resizable = DataGridViewTriState.False;
            for (int i = 1; i < COUNT_COLUMN_IRI_FRCh_RP - 5; i++)
                tableIRI_FRCh_RP[i].resizable = DataGridViewTriState.True;

            for (int i = 10; i < COUNT_COLUMN_IRI_FRCh_RP; i++)
                tableIRI_FRCh_RP[i].resizable = DataGridViewTriState.False;

            // тип столбцов
            for (int i = 0; i < COUNT_COLUMN_IRI_FRCh_RP - 8; i++)
                tableIRI_FRCh_RP[i].typeColumn = TypeColumn.TextBoxColumn;

            for (int i = 7; i < COUNT_COLUMN_IRI_FRCh_RP - 5; i++)
                tableIRI_FRCh_RP[i].typeColumn = TypeColumn.ImageColumn;

            for (int i = 10; i < COUNT_COLUMN_IRI_FRCh_RP; i++)
                tableIRI_FRCh_RP[i].typeColumn = TypeColumn.TextBoxColumn;
        }

        /// <summary>
        /// Заполнить структуру таблицы ИРИ ППРЧ
        /// </summary>
        public void FillStructTableIRI_PPRCh()
        {
            tableIRI_PPRCh = new TableColumn[COUNT_COLUMN_IRI_PPRCh];

            // заголовки полей
            tableIRI_PPRCh[0].fields = tableFieldHeader.id;
            tableIRI_PPRCh[1].fields = tableFieldHeader.Fmin;
            tableIRI_PPRCh[2].fields = tableFieldHeader.Fmax;
            tableIRI_PPRCh[3].fields = tableFieldHeader.step;
            tableIRI_PPRCh[4].fields = tableFieldHeader.deltaF;
            tableIRI_PPRCh[5].fields = tableFieldHeader.durationImpulses;
            tableIRI_PPRCh[6].fields = tableFieldHeader.countIRI;
            tableIRI_PPRCh[7].fields = tableFieldHeader.time;
            tableIRI_PPRCh[8].fields = tableFieldHeader.SP_RR;
            tableIRI_PPRCh[9].fields = tableFieldHeader.CR;

            // имена полей
            tableIRI_PPRCh[0].nameField = tableFieldName.id;
            tableIRI_PPRCh[1].nameField = tableFieldName.Fmin;
            tableIRI_PPRCh[2].nameField = tableFieldName.Fmax;
            tableIRI_PPRCh[3].nameField = tableFieldName.step;
            tableIRI_PPRCh[4].nameField = tableFieldName.deltaF;
            tableIRI_PPRCh[5].nameField = tableFieldName.durationImpulses;
            tableIRI_PPRCh[6].nameField = tableFieldName.countIRI;
            tableIRI_PPRCh[7].nameField = tableFieldName.time;
            tableIRI_PPRCh[8].nameField = tableFieldName.SP_RR;
            tableIRI_PPRCh[9].nameField = tableFieldName.CR;

            // ширина столбцов
            tableIRI_PPRCh[0].widthField = 0;  // номер
            tableIRI_PPRCh[1].widthField = 75; // частота мин.
            tableIRI_PPRCh[2].widthField = 75; // частота макс. 
            tableIRI_PPRCh[3].widthField = 35; // шаг
            tableIRI_PPRCh[4].widthField = 45; // ширина полосы
            tableIRI_PPRCh[5].widthField = 50; // длительность импульсов
            tableIRI_PPRCh[6].widthField = 35; // количество ИРИ
            tableIRI_PPRCh[7].widthField = 50; // время
            tableIRI_PPRCh[8].widthField = 35; // СП РР 
            tableIRI_PPRCh[9].widthField = 25; // ЦР

            // видимый/невидимый столбец
            tableIRI_PPRCh[0].bVisible = false;
            for (int i = 1; i < COUNT_COLUMN_IRI_PPRCh; i++)
                tableIRI_PPRCh[i].bVisible = true;

            // изменение размера столбца
            tableIRI_PPRCh[0].resizable = DataGridViewTriState.False;
            for (int i = 1; i < COUNT_COLUMN_IRI_PPRCh; i++)
                tableIRI_PPRCh[i].resizable = DataGridViewTriState.True;

            // тип столбцов
            for (int i = 0; i < COUNT_COLUMN_IRI_PPRCh - 1; i++)
                tableIRI_PPRCh[i].typeColumn = TypeColumn.TextBoxColumn;

            tableIRI_PPRCh[9].typeColumn = TypeColumn.CheckBoxColumn;
        }

        /// <summary>
        /// Заполнить структуру таблицы ИРИ ППРЧ 2
        /// </summary>
        public void FillStructTableIRI_PPRCh2()
        {
            tableIRI_PPRCh2 = new TableColumn[COUNT_COLUMN_IRI_PPRCh2];

            // заголовки полей
            tableIRI_PPRCh2[0].fields = tableFieldHeader.id;
            tableIRI_PPRCh2[1].fields = tableFieldHeader.Q1;
            tableIRI_PPRCh2[2].fields = tableFieldHeader.Q2;
            tableIRI_PPRCh2[3].fields = tableFieldHeader.latitude;
            tableIRI_PPRCh2[4].fields = tableFieldHeader.longitude;

            // имена полей
            tableIRI_PPRCh2[0].nameField = tableFieldName.id;
            tableIRI_PPRCh2[1].nameField = tableFieldName.Q1;
            tableIRI_PPRCh2[2].nameField = tableFieldName.Q2;
            tableIRI_PPRCh2[3].nameField = tableFieldName.latitude;
            tableIRI_PPRCh2[4].nameField = tableFieldName.longitude;

            // ширина столбцов
            tableIRI_PPRCh2[0].widthField = 0;   // номер
            tableIRI_PPRCh2[1].widthField = 45;  // пеленг 1
            tableIRI_PPRCh2[2].widthField = 45;  // пеленг 2 
            tableIRI_PPRCh2[3].widthField = 70;  // широта
            tableIRI_PPRCh2[4].widthField = 70;  // долгота

            // видимый/невидимый столбец
            tableIRI_PPRCh2[0].bVisible = false;
            for (int i = 1; i < COUNT_COLUMN_IRI_PPRCh2; i++)
                tableIRI_PPRCh2[i].bVisible = true;

            // изменение размера столбца
            tableIRI_PPRCh2[0].resizable = DataGridViewTriState.False;
            for (int i = 1; i < COUNT_COLUMN_IRI_PPRCh2; i++)
                tableIRI_PPRCh2[i].resizable = DataGridViewTriState.True;

            // тип столбцов
            for (int i = 0; i < COUNT_COLUMN_IRI_PPRCh2; i++)
                tableIRI_PPRCh2[i].typeColumn = TypeColumn.TextBoxColumn;
        }

        /// <summary>
        /// Заполнить структуру таблицы ИРИ ППРЧ 3
        /// </summary>
        public void FillStructTableIRI_PPRCh3()
        {
            tableIRI_PPRCh3 = new TableColumn[COUNT_COLUMN_IRI_PPRCh3];

            // заголовки полей
            tableIRI_PPRCh3[0].fields = tableFieldHeader.id;
            tableIRI_PPRCh3[1].fields = tableFieldHeader.F;
            tableIRI_PPRCh3[2].fields = tableFieldHeader.deltaF;
            
            // имена полей
            tableIRI_PPRCh3[0].nameField = tableFieldName.id;
            tableIRI_PPRCh3[1].nameField = tableFieldName.F;
            tableIRI_PPRCh3[2].nameField = tableFieldName.deltaF;

            // ширина столбцов
            tableIRI_PPRCh3[0].widthField = 0;   // номер
            tableIRI_PPRCh3[1].widthField = 75;  // частота
            tableIRI_PPRCh3[2].widthField = 45;  // ширина полосы 

            // видимый/невидимый столбец
            tableIRI_PPRCh3[0].bVisible = false;
            for (int i = 1; i < COUNT_COLUMN_IRI_PPRCh3; i++)
                tableIRI_PPRCh3[i].bVisible = true;

            // изменение размера столбца
            tableIRI_PPRCh3[0].resizable = DataGridViewTriState.False;
            for (int i = 1; i < COUNT_COLUMN_IRI_PPRCh3; i++)
                tableIRI_PPRCh3[i].resizable = DataGridViewTriState.True;

            // тип столбцов
            for (int i = 0; i < COUNT_COLUMN_IRI_PPRCh3; i++)
                tableIRI_PPRCh3[i].typeColumn = TypeColumn.TextBoxColumn;
        }

        /// <summary>
        /// Заполнить структуру таблицы ИРИ ППРЧ на РП
        /// </summary>
        public void FillStructTableIRI_PPRCh_RP()
        {
            tableIRI_PPRCh_RP = new TableColumn[COUNT_COLUMN_IRI_PPRCh_RP];

            // заголовки полей
            tableIRI_PPRCh_RP[0].fields = tableFieldHeader.id;
            tableIRI_PPRCh_RP[1].fields = tableFieldHeader.Fmin;
            tableIRI_PPRCh_RP[2].fields = tableFieldHeader.Fmax;
            tableIRI_PPRCh_RP[3].fields = tableFieldHeader.litera;
            tableIRI_PPRCh_RP[4].fields = tableFieldHeader.U;
            tableIRI_PPRCh_RP[5].fields = tableFieldHeader.paramsNoise;
            tableIRI_PPRCh_RP[6].fields = tableFieldHeader.RP;
            tableIRI_PPRCh_RP[7].fields = tableFieldHeader.epo;
            tableIRI_PPRCh_RP[8].fields = tableFieldHeader.modulation;
            tableIRI_PPRCh_RP[9].fields = tableFieldHeader.deviation;
            tableIRI_PPRCh_RP[10].fields = tableFieldHeader.manipulation;
            tableIRI_PPRCh_RP[11].fields = tableFieldHeader.duration;
            tableIRI_PPRCh_RP[12].fields = tableFieldHeader.mode;

            // имена полей
            tableIRI_PPRCh_RP[0].nameField = tableFieldName.id;
            tableIRI_PPRCh_RP[1].nameField = tableFieldName.Fmin;
            tableIRI_PPRCh_RP[2].nameField = tableFieldName.Fmax;
            tableIRI_PPRCh_RP[3].nameField = tableFieldName.litera;
            tableIRI_PPRCh_RP[4].nameField = tableFieldName.U;
            tableIRI_PPRCh_RP[5].nameField = tableFieldName.paramsNoise;
            tableIRI_PPRCh_RP[6].nameField = tableFieldName.RP;
            tableIRI_PPRCh_RP[7].nameField = tableFieldName.epo;
            tableIRI_PPRCh_RP[8].nameField = tableFieldName.modulation;
            tableIRI_PPRCh_RP[9].nameField = tableFieldName.deviation;
            tableIRI_PPRCh_RP[10].nameField = tableFieldName.manipulation;
            tableIRI_PPRCh_RP[11].nameField = tableFieldName.duration;
            tableIRI_PPRCh_RP[12].nameField = tableFieldName.mode;

            // ширина столбцов
            tableIRI_PPRCh_RP[0].widthField = 0;  // номер
            tableIRI_PPRCh_RP[1].widthField = 50; // частота min
            tableIRI_PPRCh_RP[2].widthField = 50; // частота max
            tableIRI_PPRCh_RP[3].widthField = 25; // литера
            tableIRI_PPRCh_RP[4].widthField = 30; // порог 
            tableIRI_PPRCh_RP[5].widthField = 75; // параметры помехи
            tableIRI_PPRCh_RP[6].widthField = 25; // РП (радиоподавление)
            tableIRI_PPRCh_RP[7].widthField = 25; // ЕПО
            tableIRI_PPRCh_RP[8].widthField = 0;  // модуляция
            tableIRI_PPRCh_RP[9].widthField = 0;  // девиация
            tableIRI_PPRCh_RP[10].widthField = 0; // манипуляция
            tableIRI_PPRCh_RP[11].widthField = 0; // длительность
            tableIRI_PPRCh_RP[12].widthField = 0; // режим (1 - ведущая, 2 - ведомая)

            // видимый/невидимый столбец
            tableIRI_PPRCh_RP[0].bVisible = false;
            for (int i = 1; i < COUNT_COLUMN_IRI_PPRCh_RP - 5; i++)
                tableIRI_PPRCh_RP[i].bVisible = true;

            for (int i = 8; i < COUNT_COLUMN_IRI_PPRCh_RP; i++)
                tableIRI_PPRCh_RP[i].bVisible = false;

            // изменение размера столбца
            tableIRI_PPRCh_RP[0].resizable = DataGridViewTriState.False;
            for (int i = 1; i < COUNT_COLUMN_IRI_PPRCh_RP - 5; i++)
                tableIRI_PPRCh_RP[i].resizable = DataGridViewTriState.True;

            for (int i = 8; i < COUNT_COLUMN_IRI_PPRCh_RP; i++)
                tableIRI_PPRCh_RP[i].resizable = DataGridViewTriState.False;

            // тип столбцов
            for (int i = 0; i < COUNT_COLUMN_IRI_PPRCh_RP - 6; i++)
                tableIRI_PPRCh_RP[i].typeColumn = TypeColumn.TextBoxColumn;

            for (int i = 6; i < 7; i++)
                tableIRI_PPRCh_RP[i].typeColumn = TypeColumn.ImageColumn;

            for (int i = 7; i < COUNT_COLUMN_IRI_PPRCh_RP; i++)
                tableIRI_PPRCh_RP[i].typeColumn = TypeColumn.TextBoxColumn;
        }

        /// <summary>
        /// Заполнить структуру таблицы ИРИ ППРЧ на РП 2
        /// </summary>
        public void FillStructTableIRI_PPRCh_RP2()
        {
            tableIRI_PPRCh_RP2 = new TableColumn[COUNT_COLUMN_IRI_PPRCh_RP2];

            // заголовки полей
            tableIRI_PPRCh_RP2[0].fields = tableFieldHeader.id;
            tableIRI_PPRCh_RP2[1].fields = tableFieldHeader.F;
            tableIRI_PPRCh_RP2[2].fields = tableFieldHeader.mode;

            // имена полей
            tableIRI_PPRCh_RP2[0].nameField = tableFieldName.id;
            tableIRI_PPRCh_RP2[1].nameField = tableFieldName.F;
            tableIRI_PPRCh_RP2[2].nameField = tableFieldName.mode;

            // ширина столбцов
            tableIRI_PPRCh_RP2[0].widthField = 0;   // номер
            tableIRI_PPRCh_RP2[1].widthField = 75;  // частота
            tableIRI_PPRCh_RP2[2].widthField = 0;   // mode

            // видимый/невидимый столбец
            tableIRI_PPRCh_RP2[0].bVisible = false;
            tableIRI_PPRCh_RP2[1].bVisible = true;
            tableIRI_PPRCh_RP2[2].bVisible = false;

            // изменение размера столбца
            tableIRI_PPRCh_RP2[0].resizable = DataGridViewTriState.False;
            tableIRI_PPRCh_RP2[1].resizable = DataGridViewTriState.True;
            tableIRI_PPRCh_RP2[2].resizable = DataGridViewTriState.False;

            // тип столбцов
            for (int i = 0; i < COUNT_COLUMN_IRI_PPRCh_RP2; i++)
                tableIRI_PPRCh_RP2[i].typeColumn = TypeColumn.TextBoxColumn;
        }

        /// <summary>
        /// Заполнить структуру таблицы ИРИ ППРЧ на РП 3
        /// </summary>
        public void FillStructTableIRI_PPRCh_RP_Exclude()
        {
            tableIRI_PPRCh_RP_Exclude = new TableColumn[COUNT_COLUMN_IRI_PPRCh_RP_EXCLUDE];

            // заголовки полей
            tableIRI_PPRCh_RP_Exclude[0].fields = tableFieldHeader.id;
            tableIRI_PPRCh_RP_Exclude[1].fields = tableFieldHeader.F;
            tableIRI_PPRCh_RP_Exclude[2].fields = tableFieldHeader.deltaF;
            tableIRI_PPRCh_RP_Exclude[3].fields = tableFieldHeader.mode;

            // имена полей
            tableIRI_PPRCh_RP_Exclude[0].nameField = tableFieldName.id;
            tableIRI_PPRCh_RP_Exclude[1].nameField = tableFieldName.F;
            tableIRI_PPRCh_RP_Exclude[2].nameField = tableFieldName.deltaF;
            tableIRI_PPRCh_RP_Exclude[3].nameField = tableFieldName.mode;

            // ширина столбцов
            tableIRI_PPRCh_RP_Exclude[0].widthField = 0;   // номер
            tableIRI_PPRCh_RP_Exclude[1].widthField = 75;  // частота
            tableIRI_PPRCh_RP_Exclude[2].widthField = 45;  // ширина полосы
            tableIRI_PPRCh_RP_Exclude[3].widthField = 0;   // mode

            // видимый/невидимый столбец
            tableIRI_PPRCh_RP_Exclude[0].bVisible = false;
            tableIRI_PPRCh_RP_Exclude[1].bVisible = true;
            tableIRI_PPRCh_RP_Exclude[2].bVisible = true;
            tableIRI_PPRCh_RP_Exclude[3].bVisible = false;
           
            // изменение размера столбца
            tableIRI_PPRCh_RP_Exclude[0].resizable = DataGridViewTriState.False;
            tableIRI_PPRCh_RP_Exclude[1].resizable = DataGridViewTriState.True;
            tableIRI_PPRCh_RP_Exclude[2].resizable = DataGridViewTriState.True;
            tableIRI_PPRCh_RP_Exclude[3].resizable = DataGridViewTriState.False;

            // тип столбцов
            for (int i = 0; i < COUNT_COLUMN_IRI_PPRCh_RP_EXCLUDE; i++)
                tableIRI_PPRCh_RP_Exclude[i].typeColumn = TypeColumn.TextBoxColumn;
        }

        /// <summary>
        /// Заполнить структуру таблицы AR-ONE
        /// </summary>
        public void FillStructTableAR_ONE()
        {
            tableAR_ONE = new TableColumn[COUNT_COLUMN_AR_ONE];

            // заголовки полей
            tableAR_ONE[0].fields = tableFieldHeader.id;
            tableAR_ONE[1].fields = tableFieldHeader.OnOff;
            tableAR_ONE[2].fields = tableFieldHeader.F;
            tableAR_ONE[3].fields = tableFieldHeader.U;
            tableAR_ONE[4].fields = tableFieldHeader.time;
            tableAR_ONE[5].fields = tableFieldHeader.priority;
            tableAR_ONE[6].fields = tableFieldHeader.note;
            tableAR_ONE[7].fields = tableFieldHeader.pause;
            tableAR_ONE[8].fields = tableFieldHeader.ampl;
            tableAR_ONE[9].fields = tableFieldHeader.attenuator;
            tableAR_ONE[10].fields = tableFieldHeader.mode;
            tableAR_ONE[11].fields = tableFieldHeader.BW;
            tableAR_ONE[12].fields = tableFieldHeader.HPF;
            tableAR_ONE[13].fields = tableFieldHeader.LPF;

            // имена полей
            tableAR_ONE[0].nameField = tableFieldName.id;
            tableAR_ONE[1].nameField = tableFieldName.OnOff;
            tableAR_ONE[2].nameField = tableFieldName.F;
            tableAR_ONE[3].nameField = tableFieldName.U;
            tableAR_ONE[4].nameField = tableFieldName.time;
            tableAR_ONE[5].nameField = tableFieldName.priority;
            tableAR_ONE[6].nameField = tableFieldName.note;
            tableAR_ONE[7].nameField = tableFieldName.pause;
            tableAR_ONE[8].nameField = tableFieldName.ampl;
            tableAR_ONE[9].nameField = tableFieldName.attenuator;
            tableAR_ONE[10].nameField = tableFieldName.mode;
            tableAR_ONE[11].nameField = tableFieldName.BW;
            tableAR_ONE[12].nameField = tableFieldName.HPF;
            tableAR_ONE[13].nameField = tableFieldName.LPF;

            // ширина столбцов
            tableAR_ONE[0].widthField = 0;   // номер
            tableAR_ONE[1].widthField = 40;  // вкл./выкл.
            tableAR_ONE[2].widthField = 75;  // частота
            tableAR_ONE[3].widthField = 40;  // порог
            tableAR_ONE[4].widthField = 50;  // время первого обнаружения
            tableAR_ONE[5].widthField = 30;  // приоритет
            tableAR_ONE[6].widthField = 70;  // примечание
            tableAR_ONE[7].widthField = 0;   // пауза
            tableAR_ONE[8].widthField = 0;   // амплитуда 
            tableAR_ONE[9].widthField = 0;   // аттенюатор
            tableAR_ONE[10].widthField = 0;  // режим
            tableAR_ONE[11].widthField = 0;  // полоса
            tableAR_ONE[12].widthField = 0;  // ФВЧ
            tableAR_ONE[13].widthField = 0;  // ФНЧ

            // видимый/невидимый столбец
            tableAR_ONE[0].bVisible = false;
            for (int i = 1; i < COUNT_COLUMN_AR_ONE - 7; i++)
                tableAR_ONE[i].bVisible = true;

            for (int i = 7; i < COUNT_COLUMN_AR_ONE; i++)
                tableAR_ONE[i].bVisible = false;

            // изменение размера столбца
            tableAR_ONE[0].resizable = DataGridViewTriState.False;
            for (int i = 1; i < COUNT_COLUMN_AR_ONE - 7; i++)
                tableAR_ONE[i].resizable = DataGridViewTriState.True;

            for (int i = 7; i < COUNT_COLUMN_AR_ONE; i++)
                tableAR_ONE[i].resizable = DataGridViewTriState.False;
           
            // тип столбцов
            tableAR_ONE[0].typeColumn = TypeColumn.TextBoxColumn;
            tableAR_ONE[1].typeColumn = TypeColumn.CheckBoxColumn;

            for (int i = 2; i < COUNT_COLUMN_AR_ONE; i++)
                tableAR_ONE[i].typeColumn = TypeColumn.TextBoxColumn;
        }

        /// <summary>
        /// Заполнить структуру таблицы Сектора и диапазоны РР
        /// </summary>
        public void FillStructTableRangeRR()
        {
            tableRangeRR = new TableColumn[COUNT_COLUMN_RANGE_RR];

            // заголовки полей
            tableRangeRR[0].fields = tableFieldHeader.id;
            tableRangeRR[1].fields = tableFieldHeader.Fmin;
            tableRangeRR[2].fields = tableFieldHeader.Fmax;
            tableRangeRR[3].fields = tableFieldHeader.angleMin;
            tableRangeRR[4].fields = tableFieldHeader.angleMax;
            tableRangeRR[5].fields = tableFieldHeader.mode;

            // имена полей
            tableRangeRR[0].nameField = tableFieldName.id;
            tableRangeRR[1].nameField = tableFieldName.Fmin;
            tableRangeRR[2].nameField = tableFieldName.Fmax;
            tableRangeRR[3].nameField = tableFieldName.angleMin;
            tableRangeRR[4].nameField = tableFieldName.angleMax;
            tableRangeRR[5].nameField = tableFieldName.mode;

            // ширина столбцов
            tableRangeRR[0].widthField = 0;  // номер
            tableRangeRR[1].widthField = 75; // частота мин.
            tableRangeRR[2].widthField = 75; // частота макс. 
            tableRangeRR[3].widthField = 50; // угол мин.
            tableRangeRR[4].widthField = 50; // угол макс.
            tableRangeRR[5].widthField = 0;  // режим (1 - ведущая, 2 - ведомая)
            
            // видимый/невидимый столбец
            tableRangeRR[0].bVisible = false;
            for (int i = 1; i < COUNT_COLUMN_RANGE_RR - 1; i++)
                tableRangeRR[i].bVisible = true;
            tableRangeRR[5].bVisible = false;

            // изменение размера столбца
            tableRangeRR[0].resizable = DataGridViewTriState.False;
            for (int i = 1; i < COUNT_COLUMN_RANGE_RR - 1; i++)
                tableRangeRR[i].resizable = DataGridViewTriState.True;
            tableRangeRR[5].resizable = DataGridViewTriState.False;

            // тип столбцов
            for (int i = 0; i < COUNT_COLUMN_RANGE_RR; i++)
                tableRangeRR[i].typeColumn = TypeColumn.TextBoxColumn;
        }

        /// <summary>
        /// Заполнить структуру таблицы Сектора и диапазоны РП
        /// </summary>
        public void FillStructTableRangeRP()
        {
            tableRangeRP = new TableColumn[COUNT_COLUMN_RANGE_RP];

            // заголовки полей
            tableRangeRP[0].fields = tableFieldHeader.id;
            tableRangeRP[1].fields = tableFieldHeader.Fmin;
            tableRangeRP[2].fields = tableFieldHeader.Fmax;
            tableRangeRP[3].fields = tableFieldHeader.angleMin;
            tableRangeRP[4].fields = tableFieldHeader.angleMax;
            tableRangeRP[5].fields = tableFieldHeader.mode;

            // имена полей
            tableRangeRP[0].nameField = tableFieldName.id;
            tableRangeRP[1].nameField = tableFieldName.Fmin;
            tableRangeRP[2].nameField = tableFieldName.Fmax;
            tableRangeRP[3].nameField = tableFieldName.angleMin;
            tableRangeRP[4].nameField = tableFieldName.angleMax;
            tableRangeRP[5].nameField = tableFieldName.mode;

            // ширина столбцов
            tableRangeRP[0].widthField = 0;  // номер
            tableRangeRP[1].widthField = 75; // частота мин.
            tableRangeRP[2].widthField = 75; // частота макс. 
            tableRangeRP[3].widthField = 50; // угол мин.
            tableRangeRP[4].widthField = 50; // угол макс.
            tableRangeRP[5].widthField = 0;  // режим (1 - ведущая, 2 - ведомая)

            // видимый/невидимый столбец
            tableRangeRP[0].bVisible = false;
            for (int i = 1; i < COUNT_COLUMN_RANGE_RP - 1; i++)
                tableRangeRP[i].bVisible = true;
            tableRangeRP[5].bVisible = false;

            // изменение размера столбца
            tableRangeRP[0].resizable = DataGridViewTriState.False;
            for (int i = 1; i < COUNT_COLUMN_RANGE_RP - 1; i++)
                tableRangeRP[i].resizable = DataGridViewTriState.True;
            tableRangeRP[5].resizable = DataGridViewTriState.False;

            // тип столбцов
            for (int i = 0; i < COUNT_COLUMN_RANGE_RP; i++)
                tableRangeRP[i].typeColumn = TypeColumn.TextBoxColumn;
        }

        /// <summary>
        /// Заполнить структуру таблицы Запрещенные частоты
        /// </summary>
        public void FillStructTableFreqForbidden()
        {
            tableFreqForbidden = new TableColumn[COUNT_COLUMN_FREQ_FORBIDDEN];

            // заголовки полей
            tableFreqForbidden[0].fields = tableFieldHeader.id;
            tableFreqForbidden[1].fields = tableFieldHeader.Fmin;
            tableFreqForbidden[2].fields = tableFieldHeader.Fmax;
            tableFreqForbidden[3].fields = tableFieldHeader.mode;

            // имена полей
            tableFreqForbidden[0].nameField = tableFieldName.id;
            tableFreqForbidden[1].nameField = tableFieldName.Fmin;
            tableFreqForbidden[2].nameField = tableFieldName.Fmax;
            tableFreqForbidden[3].nameField = tableFieldName.mode;

            // ширина столбцов
            tableFreqForbidden[0].widthField = 0;  // номер
            tableFreqForbidden[1].widthField = 75; // частота мин.
            tableFreqForbidden[2].widthField = 75; // частота макс. 
            tableFreqForbidden[3].widthField = 0;  // режим (1 - ведущая, 2 - ведомая)

            // видимый/невидимый столбец
            tableFreqForbidden[0].bVisible = false;
            for (int i = 1; i < COUNT_COLUMN_FREQ_FORBIDDEN - 1; i++)
                tableFreqForbidden[i].bVisible = true;
            tableFreqForbidden[3].bVisible = false;

            // изменение размера столбца
            tableFreqForbidden[0].resizable = DataGridViewTriState.False;
            for (int i = 1; i < COUNT_COLUMN_FREQ_FORBIDDEN - 1; i++)
                tableFreqForbidden[i].resizable = DataGridViewTriState.True;
            tableFreqForbidden[3].resizable = DataGridViewTriState.False;

            // тип столбцов
            for (int i = 0; i < COUNT_COLUMN_FREQ_FORBIDDEN; i++)
                tableFreqForbidden[i].typeColumn = TypeColumn.TextBoxColumn;
        }

        /// <summary>
        /// Заполнить структуру таблицы Известные частоты
        /// </summary>
        public void FillStructTableFreqKnown()
        {
            tableFreqKnown = new TableColumn[COUNT_COLUMN_FREQ_KNOWN];

            // заголовки полей
            tableFreqKnown[0].fields = tableFieldHeader.id;
            tableFreqKnown[1].fields = tableFieldHeader.Fmin;
            tableFreqKnown[2].fields = tableFieldHeader.Fmax;
            tableFreqKnown[3].fields = tableFieldHeader.mode;

            // имена полей
            tableFreqKnown[0].nameField = tableFieldName.id;
            tableFreqKnown[1].nameField = tableFieldName.Fmin;
            tableFreqKnown[2].nameField = tableFieldName.Fmax;
            tableFreqKnown[3].nameField = tableFieldName.mode;

            // ширина столбцов
            tableFreqKnown[0].widthField = 0;  // номер
            tableFreqKnown[1].widthField = 75; // частота мин.
            tableFreqKnown[2].widthField = 75; // частота макс. 
            tableFreqKnown[3].widthField = 0;  // режим (1 - ведущая, 2 - ведомая)

            // видимый/невидимый столбец
            tableFreqKnown[0].bVisible = false;
            for (int i = 1; i < COUNT_COLUMN_FREQ_KNOWN - 1; i++)
                tableFreqKnown[i].bVisible = true;
            tableFreqKnown[3].bVisible = false;

            // изменение размера столбца
            tableFreqKnown[0].resizable = DataGridViewTriState.False;
            for (int i = 1; i < COUNT_COLUMN_FREQ_KNOWN - 1; i++)
                tableFreqKnown[i].resizable = DataGridViewTriState.True;
            tableFreqKnown[3].resizable = DataGridViewTriState.False;

            // тип столбцов
            for (int i = 0; i < COUNT_COLUMN_FREQ_KNOWN; i++)
                tableFreqKnown[i].typeColumn = TypeColumn.TextBoxColumn;
        }

        /// <summary>
        /// Заполнить структуру таблицы Важные чвстоты
        /// </summary>
        public void FillStructTableFreqImportant()
        {
            tableFreqImportant = new TableColumn[COUNT_COLUMN_FREQ_IMPORTANT];

            // заголовки полей
            tableFreqImportant[0].fields = tableFieldHeader.id;
            tableFreqImportant[1].fields = tableFieldHeader.Fmin;
            tableFreqImportant[2].fields = tableFieldHeader.Fmax;
            tableFreqImportant[3].fields = tableFieldHeader.mode;

            // имена полей
            tableFreqImportant[0].nameField = tableFieldName.id;
            tableFreqImportant[1].nameField = tableFieldName.Fmin;
            tableFreqImportant[2].nameField = tableFieldName.Fmax;
            tableFreqImportant[3].nameField = tableFieldName.mode;

            // ширина столбцов
            tableFreqImportant[0].widthField = 0;  // номер
            tableFreqImportant[1].widthField = 75; // частота мин.
            tableFreqImportant[2].widthField = 75; // частота макс. 
            tableFreqImportant[3].widthField = 0;  // режим (1 - ведущая, 2 - ведомая)

            // видимый/невидимый столбец
            tableFreqImportant[0].bVisible = false;
            for (int i = 1; i < COUNT_COLUMN_FREQ_IMPORTANT - 1; i++)
                tableFreqImportant[i].bVisible = true;
            tableFreqImportant[3].bVisible = false;

            // изменение размера столбца
            tableFreqImportant[0].resizable = DataGridViewTriState.False;
            for (int i = 1; i < COUNT_COLUMN_FREQ_IMPORTANT - 1; i++)
                tableFreqImportant[i].resizable = DataGridViewTriState.True;
            tableFreqImportant[3].resizable = DataGridViewTriState.False;

            // тип столбцов
            for (int i = 0; i < COUNT_COLUMN_FREQ_IMPORTANT; i++)
                tableFreqImportant[i].typeColumn = TypeColumn.TextBoxColumn;
        }

        /// <summary>
        /// Заполнить структуру таблицы microADSB Receiver
        /// </summary>
        public void FillStructTableADSBReceiver()
        {
            tableADSB_Receiver = new TableColumn[COUNT_COLUMN_ADSB_RECEIVER];

            // заголовки полей
            tableADSB_Receiver[0].fields = tableFieldHeader.id;
            tableADSB_Receiver[1].fields = tableFieldHeader.ICAO;
            tableADSB_Receiver[2].fields = tableFieldHeader.latitude;
            tableADSB_Receiver[3].fields = tableFieldHeader.longitude;
            tableADSB_Receiver[4].fields = tableFieldHeader.altitude;
            tableADSB_Receiver[5].fields = tableFieldHeader.datetime;

            // имена полей
            tableADSB_Receiver[0].nameField = tableFieldName.id;
            tableADSB_Receiver[1].nameField = tableFieldName.ICAO;
            tableADSB_Receiver[2].nameField = tableFieldName.latitude;
            tableADSB_Receiver[3].nameField = tableFieldName.longitude;
            tableADSB_Receiver[4].nameField = tableFieldName.altitude;
            tableADSB_Receiver[5].nameField = tableFieldName.datetime;

            // ширина столбцов
            tableADSB_Receiver[0].widthField = 0;   // номер
            tableADSB_Receiver[1].widthField = 75;  // воздушный объект, ICAO
            tableADSB_Receiver[2].widthField = 60;  // широта
            tableADSB_Receiver[3].widthField = 60;  // долгота
            tableADSB_Receiver[4].widthField = 65;  // высота
            tableADSB_Receiver[5].widthField = 125; // время

            // видимый/невидимый столбец
            tableADSB_Receiver[0].bVisible = false;
            for (int i = 1; i < COUNT_COLUMN_ADSB_RECEIVER; i++)
                tableADSB_Receiver[i].bVisible = true;

            // изменение размера столбца
            tableADSB_Receiver[0].resizable = DataGridViewTriState.False;
            for (int i = 1; i < COUNT_COLUMN_ADSB_RECEIVER; i++)
                tableADSB_Receiver[i].resizable = DataGridViewTriState.True;

            // тип столбцов
            for (int i = 0; i < COUNT_COLUMN_ADSB_RECEIVER; i++)
                tableADSB_Receiver[i].typeColumn = TypeColumn.TextBoxColumn;
        }
        public void CorrectCountRows()
        {

            //if (dgv.Height > dgv.RowCount * dgv.RowTemplate.Height)
            //{
            //    for (int i = 0; i < countRows; i--)
            //    {
            //        if (dgv.Rows[i].Cells[0].Value == null)
            //        {
            //            dgv.Rows.RemoveAt(i);
            //        }
            //    }

            //}
        }

        /// <summary>
        /// Если изменился размер таблиц
        /// </summary>
        private void dgv_Resize(object sender, EventArgs e)
        {
            if (fInitIRI_FRCh)
                SetInitialNumberOfRows(dgv, tableIRI_FRCh, NameTable.IRI_FRCh);

            if (fInitIRI_FRCh_CR)
                SetInitialNumberOfRows(dgv, tableIRI_FRCh_CR, NameTable.IRI_FRCh_CR);

            if (fInitIRI_FRCh_RP)
                SetInitialNumberOfRows(dgv, tableIRI_FRCh_RP, NameTable.IRI_FRCh_RP);

            if (fInitIRI_PPRCh)
                SetInitialNumberOfRows(dgv, tableIRI_PPRCh, NameTable.IRI_PPRCh);

            if (fInitIRI_PPRCh2)
                SetInitialNumberOfRows(dgv, tableIRI_PPRCh2, NameTable.IRI_PPRCh2);

            if (fInitIRI_PPRCh3)
                SetInitialNumberOfRows(dgv, tableIRI_PPRCh3, NameTable.IRI_PPRCh3);

            if (fInitIRI_PPRCh_RP)
                SetInitialNumberOfRows(dgv, tableIRI_PPRCh_RP, NameTable.IRI_PPRCh_RP);

            if (fInitIRI_PPRCh_RP2)
                SetInitialNumberOfRows(dgv, tableIRI_PPRCh_RP2, NameTable.IRI_PPRCh_RP2);

            if (fInitIRI_PPRCh_RP_Exclude)
                SetInitialNumberOfRows(dgv, tableIRI_PPRCh_RP_Exclude, NameTable.IRI_PPRCh_RP_EXCLUDE);

            if (fInitAR_ONE)
                SetInitialNumberOfRows(dgv, tableAR_ONE, NameTable.AR_ONE);

            if (fInitRangeRR)
                SetInitialNumberOfRows(dgv, tableRangeRR, NameTable.RANGE_RR);

            if (fInitRangeRP)
                SetInitialNumberOfRows(dgv, tableRangeRP, NameTable.RANGE_RP);

            if (fInitFreqForbidden)
                SetInitialNumberOfRows(dgv, tableFreqForbidden, NameTable.FREQ_FORBIDDEN);

            if (fInitFreqKnown)
                SetInitialNumberOfRows(dgv, tableFreqKnown, NameTable.FREQ_KNOWN);

            if (fInitFreqImportant)
                SetInitialNumberOfRows(dgv, tableFreqImportant, NameTable.FREQ_IMPORTANT);

            if (fInitADSBReceiver)
                SetInitialNumberOfRows(dgv, tableADSB_Receiver, NameTable.ADSB_RECEIVER);
        }

        public delegate void DGVKeyUpEventHandler(KeyEventArgs e);
        public static event DGVKeyUpEventHandler OnDGVKeyUp;
        private void dgv_KeyUp(object sender, KeyEventArgs e)
        {
            if (OnDGVKeyUp != null)
                OnDGVKeyUp(e);
        }
    }
}
