﻿using Protocols;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyDataGridView
{
    public struct Struct_IRI_FRCh_RP
    {
        public int  iID;           // номер записи
        public int  iFreq;         // частота
        public int  iQ;            // пеленг
        public byte bLetter;       // литера
        public short sU;           // порог
        public byte bPriority;     // приоритет
        public byte bModulation;   // модуляция
        public byte bDeviation;    // девиация
        public byte bManipulation; // манипуляция
        public byte bDuration;     // длительность
        public byte bMode;         // режим
    }

    public struct Struct_IRI_FRCh_CR
    {
        public int iID;        // номер записи
        public int iFreq;      // частота
        public int iQ1;        // пеленг 1
        public int iQ2;        // пеленг 2
        public int iLevel;     // уровень
        public int iDFreq;     // ширина полосы
        public int iCKO;       // СКО
        public double dLatitude;  // широта
        public double dLongitude; // долгота
        public string sView;   // вид
        public int iSP_RR;     // СП радиоразведки
        public int iSP_RP;     // СП радиоподавления
    }

    public struct Struct_IRI_PPRCh
    {
        public int iID;        // номер записи
        public int iFreqMin;   // частота минимальная
        public int iFreqMax;   // частота максимальная
        public int iStep;      // шаг
        public int iDFreq;     // ширина полосы
        public int iDuratImp;  // длительность импульсов
        public int iCountIRI;  // количество ИРИ
        public string sTime;   // время
        public int iSP_RR;     // СП радиоразведки
        public bool bCR;       // ЦР
        public Struct_IRI_PPRCh_Location[] sLocation;
        public Struct_IRI_PPRCh_Freq_Band[] sFreq_Band;
    }

    public struct Struct_IRI_PPRCh_Location
    {
        public int iID;        // номер записи
        public int iQ1;        // пеленг 1
        public int iQ2;        // пеленг 2
        public double dLatitude;  // широта
        public double dLongitude; // долгота
    }

    public struct Struct_IRI_PPRCh_Freq_Band
    {
        public int iID;        // номер записи
        public int iFreq;      // частота
        public int iDFreq;     // ширина полосы
    }

    public struct Struct_IRI_PPRCh_RP
    {
        public int iID;            // номер записи
        public int iFreqMin;       // частота минимальная
        public int iFreqMax;       // частота максимальная
        public short sU;           // порог
        public byte bModulation;   // модуляция
        public byte bDeviation;    // девиация
        public byte bManipulation; // манипуляция
        public byte bDuration;     // длительность
        public byte bMode;         // режим
    }

    public struct Struct_IRI_PPRCh_RP2
    {
        public int iID;        // номер записи
        public int iFreq;      // частота
        public byte bMode;         // режим
    }

    public struct Struct_IRI_PPRCh_RP_EXCLUDE
    {
        public int iID;        // номер записи
        public int iFreq;      // частота
        public int iDFreq;     // ширина полосы
        public byte bMode;         // режим
    }

    public struct Struct_AR_ONE
    {
        public int iID;           // номер записи
        public int iOnOff;        // вкл./выкл.
        public int iFreq;         // частота
        public int iU;            // уровень
        public string sTimeFirst; //
        public byte bPriority;    // приоритет
        public string Note;       // примечание
        public int iPause;        // пауза
        public int iAmpl;         // усилитель
        public int iAttenuator;   // аттенюатор
        public int iMode;         // режим
        public int iBW;           // полоса
        public int iHPF;          // ФВЧ
        public int iLPF;          // ФНЧ
    }

    public struct Struct_RangeRR
    {
        public int iID;           // номер записи
        public int iFreqMin;      // частота мин.
        public int iFreqMax;      // частота макс.
        public int iAngleMin;     // угол мин.
        public int iAngleMax;     // угол макс.
        public byte bMode;        // режим
    }

    public struct Struct_RangeRP
    {
        public int iID;           // номер записи
        public int iFreqMin;      // частота мин.
        public int iFreqMax;      // частота макс.
        public int iAngleMin;     // угол мин.
        public int iAngleMax;     // угол макс.
        public byte bMode;        // режим
    }

    public struct Struct_SpecialFrequencies
    {
        public int iID;           // номер записи
        public int iFreqMin;      // частота мин.
        public int iFreqMax;      // частота макс.
        public byte bMode;        // режим
    }

    public struct Struct_ADSB_Receiver
    {
        public int iID;           // номер записи
        public string sICAO;      // номер воздушного объекта
        public string sLatitude;  // широта
        public string sLongitude; // долгота
        public string sAltitude;  // высота
        public string sDatetime;  // дата, время
    }
}
