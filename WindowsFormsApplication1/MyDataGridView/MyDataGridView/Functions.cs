﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using USR_DLL;
using VariableDynamic;
using VariableStatic;

namespace MyDataGridView
{
    public class Functions
    {
        public bool fChangeRec = false;

        public static NumberFormatInfo format; 
       

        //public SortedList<string, string> Dictionary;
        VariableCommon variableCommon = new VariableCommon();
        VariableWork variableWork = new VariableWork();

        static Functions()
        { 
            //System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("ru-RU");
            //Char separator = System.Globalization.CultureInfo.CurrentCulture.NumberFormat.CurrencyDecimalSeparator[0];
            
            format = new NumberFormatInfo();
            format.NumberDecimalSeparator = ",";
            //format.NumberDecimalSeparator = separator.ToString();
            format.NumberGroupSeparator = " ";


           

            


        }

        /// <summary>
        /// представление частоты строкой с разделением на разряды
        /// </summary>
        /// <param name="iFreq"> частота, кГц </param>
        /// <returns></returns>
        public string FreqToStr(int iFreq)
        {
            string strFreqResult = "";
            string strFreq = Convert.ToString(iFreq);
            int iLength = strFreq.Length;

            if (strFreq == "0")
                return "";

            if (iLength == 8)
                strFreqResult = strFreq.Substring(0, 1) + " " + strFreq.Substring(1, 3) + " " + strFreq.Substring(iLength - 4, 3) + "," + strFreq.Substring(iLength - 1, 1);
            else
                strFreqResult = strFreq.Substring(0, iLength - 4) + " " + strFreq.Substring(iLength - 4, 3) + "," + strFreq.Substring(iLength - 1, 1);

            return strFreqResult;
        }

        /// <summary>
        /// представление значения частоты без пробелов
        /// </summary>
        /// <param name="inputString"> строковое значение частоты </param>
        /// <returns></returns>
        public string StrToFreq(string inputString)
        {
            inputString = inputString.Replace("  ", string.Empty);
            inputString = inputString.Trim().Replace(" ", string.Empty);

            return inputString;
        }

        /// <summary>
        /// представление длительности строкой с разделением на разряды
        /// </summary>
        /// <param name="iDuration"> длительность, мс</param>
        /// <returns></returns>
        public string DurationToStr(int iDuration)
        {
            string strDurationResult = "";
            string strDuration = Convert.ToString(iDuration);
            int iLength = strDuration.Length;

            if (iLength > 3)
            {
                switch (iLength % 3)
                {
                    case 0:
                        for (int i = 0; i < iLength; i = i + 3)
                        {
                            strDurationResult += strDuration.Substring(i, 3) + " ";
                        }
                        break;

                    case 1:
                        strDurationResult = strDuration.Substring(0, 1) + " ";

                        for (int i = 1; i < iLength; i = i + 3)
                        {
                            strDurationResult += strDuration.Substring(i, 3) + " ";
                        }
                        break;

                    case 2:
                        strDurationResult = strDuration.Substring(0, 2) + " ";

                        for (int i = 2; i < iLength; i = i + 3)
                        {
                            strDurationResult += strDuration.Substring(i, 3) + " ";
                        }
                        break;
                }
            }
            else strDurationResult = strDuration;

            return strDurationResult;
        }

        /// <summary>
        /// Проверка на возможность объединения диапазонов РР и РП
        /// </summary>
        /// <param name="FreqMin"></param>
        /// <param name="FreqMax"></param>
        /// <returns></returns>
        public bool JoinRangesRR_RP(DataGridView dgv ,int FreqMin, int FreqMax, int AngleMin, int AngleMax)
        {
            try
            {
                for (int i = 0; i < dgv.Rows.Count; i++)
                {
                    if (fChangeRec)
                    {
                        int ind = (int)dgv.Rows[dgv.SelectedRows[0].Index].Cells[0].Value - 1;

                        if (ind == i)
                        {
                            i++;
                            fChangeRec = false;
                        }
                    }

                    if (dgv.Rows[i].Cells[1].Value != null)
                    {
                        // значение частоты
                        int dgvFreqMin = (int)(Convert.ToDecimal(dgv.Rows[i].Cells[1].Value, format) * 10);
                        int dgvFreqMax = (int)(Convert.ToDecimal(dgv.Rows[i].Cells[2].Value, format) * 10);

                        int cmp1 = dgvFreqMin.CompareTo(FreqMin);
                        int cmp2 = dgvFreqMax.CompareTo(FreqMin);
                        int cmp3 = dgvFreqMin.CompareTo(FreqMax);
                        int cmp4 = dgvFreqMax.CompareTo(FreqMax);

                        // значения углов

                        int dgvAngleMin = (int)Convert.ToDecimal(dgv.Rows[i].Cells[3].Value, format);
                        int dgvAngleMax = (int)Convert.ToDecimal(dgv.Rows[i].Cells[4].Value, format);

                        // с = a.CompareTo(b), если результат сравнения
                        // < 0 --> a < b,
                        // = 0 --> a = b,
                        // > 0 --> a > b.
                        int cmp5 = dgvAngleMin.CompareTo(AngleMin);
                        int cmp6 = dgvAngleMax.CompareTo(AngleMin);
                        int cmp7 = dgvAngleMin.CompareTo(AngleMax);
                        int cmp8 = dgvAngleMax.CompareTo(AngleMax);

                        if (((cmp1 < 0) || (cmp1 == 0)) && (cmp3 < 0) && (cmp2 > 0) && ((cmp4 < 0) || (cmp4 == 0)))
                        {
                            for (int j = 0; j < dgv.Rows.Count; j++)
                            {
                                if (((int)Convert.ToDecimal(dgv.Rows[j].Cells[3].Value, format) == AngleMin) &&
                                ((int)Convert.ToDecimal(dgv.Rows[j].Cells[4].Value, format) == AngleMax) &&
                                    ((cmp1 == 0) && (cmp4 == 0)))
                                {
                                    MessageBox.Show(SMessages.mesSector + dgv.Rows[i].Cells[3].Value.ToString() + " - " + dgv.Rows[i].Cells[4].Value.ToString() + SMessages.mesAndRange + dgv.Rows[j].Cells[1].Value.ToString() + " - " + dgv.Rows[j].Cells[2].Value.ToString() + SMessages.mesContainSetSector,
                                            SMessages.mesMessage, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    return false;
                                }
                            }

                            // если значение угла мин. < значения угла макс. 
                            if (((cmp1 == 0) && (cmp4 == 0)) && (AngleMin < AngleMax))
                            {
                                for (int j = 0; j < dgv.Rows.Count; j++)
                                {
                                    if (((cmp5 < 0) || (cmp5 == 0)) && ((cmp8 > 0) || (cmp8 == 0)))
                                    {
                                        MessageBox.Show(SMessages.mesSector + dgv.Rows[i].Cells[3].Value.ToString() + " - " + dgv.Rows[i].Cells[4].Value.ToString() + SMessages.mesAndRange + dgv.Rows[j].Cells[1].Value.ToString() + " - " + dgv.Rows[j].Cells[2].Value.ToString() + SMessages.mesContainSetSector,
                                                    SMessages.mesMessage, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                        return false;
                                    }
                                }
                            }
                            // если значение угла мин. < значения угла макс. 
                            else if (((cmp1 == 0) && (cmp4 == 0)) && (AngleMin > AngleMax))
                            {
                                return true;
                            }

                            else
                            {
                                MessageBox.Show(SMessages.mesRange + dgv.Rows[i].Cells[1].Value.ToString() + " - " + dgv.Rows[i].Cells[2].Value.ToString() + SMessages.mesCrossingRange,
                                                SMessages.mesMessage, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                return false;
                            }

                        }

                        else if (((cmp1 > 0) || (cmp1 == 0)) && (cmp3 < 0) && (cmp2 > 0) && ((cmp4 > 0) || (cmp4 == 0)))
                        {
                            MessageBox.Show(SMessages.mesRange + dgv.Rows[i].Cells[1].Value.ToString() + " - " + dgv.Rows[i].Cells[2].Value.ToString() + SMessages.mesCrossingRange,
                                            SMessages.mesMessage, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return false;
                        }

                        else if (((cmp1 < 0) || (cmp1 == 0)) && (cmp3 < 0) && (cmp2 > 0) && ((cmp4 > 0) || (cmp4 == 0)))
                        {
                            MessageBox.Show(SMessages.mesRange + dgv.Rows[i].Cells[1].Value.ToString() + " - " + dgv.Rows[i].Cells[2].Value.ToString() + SMessages.mesCrossingRange,
                                            SMessages.mesMessage, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return false;
                        }

                        else if (((cmp1 > 0) || (cmp1 == 0)) && (cmp3 < 0) && (cmp2 > 0) && ((cmp4 < 0) || (cmp4 == 0)))
                        {
                            MessageBox.Show(SMessages.mesRange + dgv.Rows[i].Cells[1].Value.ToString() + " - " + dgv.Rows[i].Cells[2].Value.ToString() + SMessages.mesCrossingRange,
                                           SMessages.mesMessage, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return false;
                        }
                    }
                }
            }
            catch { }

            return true;
        }

        /// <summary>
        /// Проверка на равенство FreqMin и FreqMax (Специальные частоты)
        /// </summary>
        /// <param name="FreqMin"></param>
        /// <returns></returns>
        public bool OneSpecFreq(DataGridView dgv, int FreqMin)
        {
            int dgvFreqMin = 0;

            for (int i = 0; i < dgv.Rows.Count; i++)
            {
                if (dgv.Rows[i].Cells[1].Value != null && dgv.Rows[i].Cells[2].Value.ToString() == "————")
                {
                    // значение частоты
                    dgvFreqMin = (int)(Convert.ToDecimal(dgv.Rows[i].Cells[1].Value, format) * 10);
                    
                    if (FreqMin == dgvFreqMin)
                    {
                        MessageBox.Show(SMessages.mesFreq + FreqToStr(FreqMin) + SMessages.mesExist,
                                        SMessages.mesMessage, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return false;
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// Проверка на возможность объединения диапазонов Специальных частот
        /// </summary>
        /// <param name="FreqMin"></param>
        /// <param name="FreqMax"></param>
        /// <returns></returns>
        public bool JoinRangesSpecialFrequencies(DataGridView dgv, int FreqMin, int FreqMax)
        {
            int dgvFreqMin = 0;
            int dgvFreqMax = 0;

            for (int i = 0; i < dgv.Rows.Count; i++)
            {
                if (fChangeRec)
                {
                    int ind = (int)dgv.Rows[dgv.SelectedRows[0].Index].Cells[0].Value - 1;

                    if (ind == i)
                    {
                        i++;
                        fChangeRec = false;
                    }
                }

                if (FreqMin == FreqMax)
                    return true;

                if (dgv.Rows[i].Cells[1].Value != null && dgv.Rows[i].Cells[2].Value.ToString() != "————")
                {
                    // значение частоты
                    dgvFreqMin = (int)(Convert.ToDecimal(dgv.Rows[i].Cells[1].Value, format) * 10);
                    dgvFreqMax = (int)(Convert.ToDecimal(dgv.Rows[i].Cells[2].Value, format) * 10);
                  
                    int cmp1 = dgvFreqMin.CompareTo(FreqMin);
                    int cmp2 = dgvFreqMax.CompareTo(FreqMin);
                    int cmp3 = dgvFreqMin.CompareTo(FreqMax);
                    int cmp4 = dgvFreqMax.CompareTo(FreqMax);

                    if (((cmp1 < 0) || (cmp1 == 0)) && (cmp3 < 0) && (cmp2 > 0) && ((cmp4 < 0) || (cmp4 == 0)))
                    {
                        MessageBox.Show(SMessages.mesRange + dgv.Rows[i].Cells[1].Value.ToString() + " - " + dgv.Rows[i].Cells[2].Value.ToString() + SMessages.mesCrossingRange,
                                        SMessages.mesMessage, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return false;

                    }

                    else if (((cmp1 > 0) || (cmp1 == 0)) && (cmp3 < 0) && (cmp2 > 0) && ((cmp4 > 0) || (cmp4 == 0)))
                    {
                        MessageBox.Show(SMessages.mesRange + dgv.Rows[i].Cells[1].Value.ToString() + " - " + dgv.Rows[i].Cells[2].Value.ToString() + SMessages.mesCrossingRange,
                                        SMessages.mesMessage, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return false;
                    }

                    else if (((cmp1 < 0) || (cmp1 == 0)) && (cmp3 < 0) && (cmp2 > 0) && ((cmp4 > 0) || (cmp4 == 0)))
                    {
                        MessageBox.Show(SMessages.mesRange + dgv.Rows[i].Cells[1].Value.ToString() + " - " + dgv.Rows[i].Cells[2].Value.ToString() + SMessages.mesCrossingRange,
                                        SMessages.mesMessage, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return false;
                    }

                    else if (((cmp1 > 0) || (cmp1 == 0)) && (cmp3 < 0) && (cmp2 > 0) && ((cmp4 < 0) || (cmp4 == 0)))
                    {
                        MessageBox.Show(SMessages.mesRange + dgv.Rows[i].Cells[1].Value.ToString() + " - " + dgv.Rows[i].Cells[2].Value.ToString() + SMessages.mesCrossingRange,
                                        SMessages.mesMessage, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return false;
                    }
                }
            }
            return true;
        }

        /// <summary>
        /// Проверка на корректность ввода значений частоты
        /// </summary>
        /// <param name="FreqMin"></param>
        /// <param name="FreqMax"></param>
        /// <returns></returns>
        public bool CorrectFreqMinMax(int FreqMin, int FreqMax)
        {
            bool fCorrectFreq = true;

            if(FreqMin >= FreqMax)
            {
                MessageBox.Show(SMessages.mesValuesMaxMin, SMessages.mesMessage, MessageBoxButtons.OK, MessageBoxIcon.Information);

                fCorrectFreq = false;

                return fCorrectFreq;
            }

            return fCorrectFreq;
        }

        public int LastID(DataGridView dgv)
        {
            int id = 1;
            int iNum = 0;

            for (int i = 0; i < dgv.RowCount; i++)
            {
                if (dgv.Rows[i].Cells[0].Value != null)
                {
                    iNum++;
                }
            }

            if (dgv.Rows[0].Cells[0].Value == null)
                iNum = 1;

            id = Convert.ToInt32(dgv.Rows[iNum - 1].Cells[0].Value) + 1;

            return id;
        }

        public byte[] DefineLetter(int iFreqMin, int iFreqMax)
        {
            byte[] bMasLetters = null;

            byte iLetterMin = 0;
            byte iLetterMax = 0;

            // определить литеру для Fmin
            if (iFreqMin >= RangesLetters.FREQ_START_LETTER_1 & iFreqMin < RangesLetters.FREQ_START_LETTER_2)
                iLetterMin = 1;
            if (iFreqMin >= RangesLetters.FREQ_START_LETTER_2 & iFreqMin < RangesLetters.FREQ_START_LETTER_3)
                iLetterMin = 2;
            if (iFreqMin >= RangesLetters.FREQ_START_LETTER_3 & iFreqMin < RangesLetters.FREQ_START_LETTER_4)
                iLetterMin = 3;
            if (iFreqMin >= RangesLetters.FREQ_START_LETTER_4 & iFreqMin < RangesLetters.FREQ_START_LETTER_5)
                iLetterMin = 4;
            if (iFreqMin >= RangesLetters.FREQ_START_LETTER_5 & iFreqMin < RangesLetters.FREQ_START_LETTER_6)
                iLetterMin = 5;
            if (iFreqMin >= RangesLetters.FREQ_START_LETTER_6 & iFreqMin < RangesLetters.FREQ_START_LETTER_7)
                iLetterMin = 6;
            if (variableCommon.TypeStation == 0)
            {
                if (iFreqMin >= RangesLetters.FREQ_START_LETTER_7 & iFreqMin < RangesLetters.FREQ_STOP_LETTER_7)
                    iLetterMin = 7;
            }
            else //if (variableCommon.TypeStation == 1)
            {
                if (iFreqMin >= RangesLetters.FREQ_START_LETTER_7 & iFreqMin < RangesLetters.FREQ_START_LETTER_8)
                    iLetterMin = 7;
                if (iFreqMin >= RangesLetters.FREQ_START_LETTER_8 & iFreqMin < RangesLetters.FREQ_START_LETTER_9)
                    iLetterMin = 8;
                if (iFreqMin >= RangesLetters.FREQ_START_LETTER_9 & iFreqMin <= RangesLetters.FREQ_STOP_LETTER_9)
                    iLetterMin = 9;
            }

            // определить литеру для Fmax
            if (iFreqMax >= RangesLetters.FREQ_START_LETTER_1 & iFreqMax < RangesLetters.FREQ_START_LETTER_2)
                iLetterMax = 1;
            if (iFreqMax >= RangesLetters.FREQ_START_LETTER_2 & iFreqMax < RangesLetters.FREQ_START_LETTER_3)
                iLetterMax = 2;
            if (iFreqMax >= RangesLetters.FREQ_START_LETTER_3 & iFreqMax < RangesLetters.FREQ_START_LETTER_4)
                iLetterMax = 3;
            if (iFreqMax >= RangesLetters.FREQ_START_LETTER_4 & iFreqMax < RangesLetters.FREQ_START_LETTER_5)
                iLetterMax = 4;
            if (iFreqMax >= RangesLetters.FREQ_START_LETTER_5 & iFreqMax < RangesLetters.FREQ_START_LETTER_6)
                iLetterMax = 5;
            if (iFreqMax >= RangesLetters.FREQ_START_LETTER_6 & iFreqMax < RangesLetters.FREQ_START_LETTER_7)
                iLetterMax = 6;
            if (variableCommon.TypeStation == 0)
            {
                if (iFreqMax >= RangesLetters.FREQ_START_LETTER_7 & iFreqMax < RangesLetters.FREQ_STOP_LETTER_7)
                    iLetterMax = 7;
            }
            else// if (variableCommon.TypeStation == 1)
            {
                if (iFreqMax >= RangesLetters.FREQ_START_LETTER_7 & iFreqMax < RangesLetters.FREQ_START_LETTER_8)
                    iLetterMax = 7;
                if (iFreqMax >= RangesLetters.FREQ_START_LETTER_8 & iFreqMax < RangesLetters.FREQ_START_LETTER_9)
                    iLetterMax = 8;
                if (iFreqMax >= RangesLetters.FREQ_START_LETTER_9 & iFreqMax <= RangesLetters.FREQ_STOP_LETTER_9)
                    iLetterMax = 9;
            }

            if (iLetterMin != iLetterMax)
            {
                bMasLetters = new byte[2];
                bMasLetters[0] = iLetterMin;
                bMasLetters[1] = iLetterMax;
            }
            else if (iLetterMin == iLetterMax)
            {
                bMasLetters = new byte[1];
                bMasLetters[0] = iLetterMin;
            }

            return bMasLetters;
        }

        public byte[] DefineEPO(int iFreqMin, int iFreqMax)
        {
            byte[] bMasEPO = null;

            int iEPO = 300000;

            byte bEPOmin = 0;
            byte bEPOmax = 0;

            // определить полосу для Fmin
            bEPOmin = Convert.ToByte(iFreqMin / iEPO);

            // определить полосу для Fmax
            bEPOmax = Convert.ToByte(iFreqMax / iEPO);

            if (bEPOmin != bEPOmax)
            {
                bMasEPO = new byte[2];
                bMasEPO[0] = bEPOmin;
                bMasEPO[1] = bEPOmax;
            }
            else if (bEPOmin == bEPOmax)
            {
                bMasEPO = new byte[1];
                bMasEPO[0] = bEPOmin;
            }

            return bMasEPO;
        }

        /// <summary>
        /// Определение цвета для отрисовки ИРИ
        /// </summary>
        /// <param name="iFreq"></param>
        /// <returns></returns>
        public string ColorFromFreqRange(int iFreq)
        {
            string sColorRange = "";

            VariableColor variableColor = new VariableColor();

            if ((iFreq > 250000) && (iFreq <= 880000))
            {
                sColorRange = variableColor.Range30_88;
            }
            if ((iFreq > 880000) && (iFreq <= 1080000))
            {
                sColorRange = variableColor.Range88_108;
            }
            if ((iFreq > 1080000) && (iFreq <= 1700000))
            {
                sColorRange = variableColor.Range108_170;
            }
            if ((iFreq > 1700000) && (iFreq <= 2200000))
            {
                sColorRange = variableColor.Range170_220;
            }
            if ((iFreq > 2200000) && (iFreq <= 4000000))
            {
                sColorRange = variableColor.Range220_400;
            }
            if ((iFreq > 4000000) && (iFreq <= 5120000))
            {
                sColorRange = variableColor.Range400_512;
            }
            if ((iFreq > 5120000) && (iFreq <= 8600000))
            {
                sColorRange = variableColor.Range512_860;
            }
            if ((iFreq > 8600000) && (iFreq <= 12150000))
            {
                sColorRange = variableColor.Range860_1215;
            }
            if ((iFreq > 12150000) && (iFreq <= 15750000))
            {
                sColorRange = variableColor.Range1215_1575;
            }
            if ((iFreq > 15750000) && (iFreq <= 30250000))
            {
                sColorRange = variableColor.Range1575_3000;
            }

            return sColorRange;
        }

        /// <summary>
        /// Проверка ИРИ ФРЧ на принадлежность к Известным или Важным частотам
        /// </summary>
        /// <param name="Freq"> частота </param>
        /// <returns> цвет обзначения в таблице (синий - Известные, красный - Важные) </returns>
        public Color CheckSpecFreq(int Freq)
        {
            Color color = Color.Black;

            int lenKnown = variableWork.FrequencyRangeKnownOwn.Length;
            for (int i = 0; i < lenKnown; i++)
            {
                if (Freq >= variableWork.FrequencyRangeKnownOwn[i].StartFrequency && Freq <= variableWork.FrequencyRangeKnownOwn[i].EndFrequency)
                    color = Color.MediumBlue;
            }

            int lenImport = variableWork.FrequencyRangeImportantOwn.Length;
            for (int i = 0; i < lenImport; i++)
            {
                if (Freq >= variableWork.FrequencyRangeImportantOwn[i].StartFrequency && Freq <= variableWork.FrequencyRangeImportantOwn[i].EndFrequency)
                    color = Color.Red;
            }   
            
            return color;
        }

        /// <summary>
        /// Принадлежность ИРИ ППРЧ заданным диапазонам РП
        /// </summary>
        /// <param name="bTable"> 1 - ведущая, 2 - ведомая </param>
        /// <param name="iFreq"> частота </param>
        /// <returns> сообщение об ошибке (пустая строка - успешно) </returns>
        public string CheckRangesRP(byte bTable, int iFreqMin, int iFreqMax)
        {
            string sMessage = SMessages.mesFreqWithoutRangeRP;

            switch (bTable)
            {
                case 1:
                    {
                        int lenOwn = variableWork.RangeSectorSupprOwn.Length;
                        for (int i = 0; i < lenOwn; i++)
                        {
                            if ((iFreqMin >= variableWork.RangeSectorSupprOwn[i].StartFrequency && iFreqMin <= variableWork.RangeSectorSupprOwn[i].EndFrequency) &&
                                (iFreqMax >= variableWork.RangeSectorSupprOwn[i].StartFrequency && iFreqMax <= variableWork.RangeSectorSupprOwn[i].EndFrequency))
                                sMessage = "";
                        }
                    }
                    break;

                case 2:
                    {
                        int lenLinked = variableWork.RangeSectorSupprLinked.Length;
                        for (int i = 0; i < lenLinked; i++)
                        {
                            if ((iFreqMin >= variableWork.RangeSectorSupprLinked[i].StartFrequency && iFreqMin <= variableWork.RangeSectorSupprLinked[i].EndFrequency) &&
                                (iFreqMax >= variableWork.RangeSectorSupprLinked[i].StartFrequency && iFreqMax <= variableWork.RangeSectorSupprLinked[i].EndFrequency))
                                sMessage = "";
                        }
                    }
                    break;

                default:
                    break;
            }

            return sMessage;
        }

        /// <summary>
        /// Принадлежность ИРИ ППРЧ Запрещенным частотам
        /// </summary>
        /// <param name="bTable"> 1 - ведущая, 2 - ведомая </param>
        /// <param name="iFreqMin"> частота </param>
        /// <returns></returns>
        public string CheckForbiddenFreq(byte bTable, int iFreqMin, int iFreqMax)
        {
            string sMessage = "";

            switch (bTable)
            {
                case 1:
                    {
                        int lenOwn = variableWork.FrequencyRangeForbiddenOwn.Length;
                        for (int i = 0; i < lenOwn; i++)
                        {
                            if ((iFreqMin >= variableWork.FrequencyRangeForbiddenOwn[i].StartFrequency && iFreqMin <= variableWork.FrequencyRangeForbiddenOwn[i].EndFrequency) ||
                                (iFreqMax >= variableWork.FrequencyRangeForbiddenOwn[i].StartFrequency && iFreqMax <= variableWork.FrequencyRangeForbiddenOwn[i].EndFrequency))
                                sMessage = SMessages.mesFreqForbidNotAdd;
                        }
                    }
                    break;

                case 2:
                    {
                        int lenLinked = variableWork.FrequencyRangeForbiddenLinked.Length;
                        for (int i = 0; i < lenLinked; i++)
                        {
                            if ((iFreqMin >= variableWork.FrequencyRangeForbiddenLinked[i].StartFrequency && iFreqMin <= variableWork.FrequencyRangeForbiddenLinked[i].EndFrequency) ||
                                (iFreqMax >= variableWork.FrequencyRangeForbiddenLinked[i].StartFrequency && iFreqMax <= variableWork.FrequencyRangeForbiddenLinked[i].EndFrequency))
                                sMessage = SMessages.mesFreqForbidNotAdd;
                        }
                    }
                    break;

                default:
                    break;
            }

            return sMessage;
        }
    }
}
