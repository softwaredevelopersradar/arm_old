﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyDataGridView
{
    public class FuncDGV_IRI_PPRCh
    {
        Functions functions;
        DecoderParams decoderParams;

        Struct_IRI_PPRCh[] struct_IRI_PPRCh;
        Struct_IRI_PPRCh_Location[] struct_PPRCh_Location;
        Struct_IRI_PPRCh_Freq_Band[] struct_PPRCh_Freq_Band;

        public FuncDGV_IRI_PPRCh()
        {
            functions = new Functions();
            decoderParams = new DecoderParams();
        }

        /// <summary>
        /// Функция добавления записи в таблицу IRI_PPRCh в DGV
        /// </summary>
        /// <param name=""></param>
        public void AddRecord_IRI_PPRChToDGV(DataGridView dgv, NameTable nameTable, object obj)
        {
            struct_IRI_PPRCh = (Struct_IRI_PPRCh[])obj;

            for (var i = 0; i < struct_IRI_PPRCh.Length; i++)
            {
                //добавить строку в таблицу
                if (i >= dgv.RowCount)
                    dgv.Rows.Add(null, null, null, null, null, null, null, null, null, null);

                var row = dgv.Rows[i];
                row.Cells[0].Value = struct_IRI_PPRCh[i].iID;
                row.Cells[1].Value = functions.FreqToStr(struct_IRI_PPRCh[i].iFreqMin);
                row.Cells[2].Value = functions.FreqToStr(struct_IRI_PPRCh[i].iFreqMax);
                row.Cells[3].Value = (Convert.ToSingle(struct_IRI_PPRCh[i].iStep) / 10.0).ToString();
                row.Cells[4].Value = (Convert.ToSingle(struct_IRI_PPRCh[i].iDFreq) / 10.0).ToString();
                row.Cells[5].Value = struct_IRI_PPRCh[i].iDuratImp;
                row.Cells[6].Value = struct_IRI_PPRCh[i].iCountIRI;
                row.Cells[7].Value = struct_IRI_PPRCh[i].sTime;
                row.Cells[8].Value = struct_IRI_PPRCh[i].iSP_RR;
                row.Cells[9].Value = 1;
            }
        }

        /// <summary>
        /// Функция добавления записи в таблицу IRI_PPRCh_Location в DGV
        /// </summary>
        /// <param name=""></param>
        public void AddRecord_IRI_PPRCh_LocationToDGV(DataGridView dgv, NameTable nameTable, object obj)
        {
            struct_PPRCh_Location = (Struct_IRI_PPRCh_Location[])obj;

            for (var i = 0; i < struct_PPRCh_Location.Length; i++)
            {
                //добавить строку в таблицу
                if (i >= dgv.RowCount)
                    dgv.Rows.Add(null, null, null, null, null);

                var row = dgv.Rows[i];
                row.Cells[0].Value = struct_PPRCh_Location[i].iID;

                if (struct_PPRCh_Location[i].iQ1 == -1) { row.Cells[1].Value = "-"; }
                else { row.Cells[1].Value = (Convert.ToSingle(struct_PPRCh_Location[i].iQ1) / 10.0).ToString(); }
                if (struct_PPRCh_Location[i].iQ2 == -1) { row.Cells[2].Value = "-"; }
                else { row.Cells[2].Value = (Convert.ToSingle(struct_PPRCh_Location[i].iQ2) / 10.0).ToString(); }

                if (struct_PPRCh_Location[i].dLatitude == -1 && struct_PPRCh_Location[i].dLongitude == -1)
                {
                    row.Cells[3].Value = "-";
                    row.Cells[4].Value = "-";
                }
                else
                {
                    row.Cells[3].Value = struct_PPRCh_Location[i].dLatitude.ToString("0.000000");
                    row.Cells[4].Value = struct_PPRCh_Location[i].dLongitude.ToString("0.000000");
                }
            }
        }

        /// <summary>
        /// Функция добавления записи в таблицу IRI_PPRCh_Freq_Band в DGV
        /// </summary>
        /// <param name=""></param>
        public void AddRecord_IRI_PPRCh_Freq_BandToDGV(DataGridView dgv, NameTable nameTable, object obj)
        {
            struct_PPRCh_Freq_Band = (Struct_IRI_PPRCh_Freq_Band[])obj;

            for (var i = 0; i < struct_PPRCh_Freq_Band.Length; i++)
            {
                //добавить строку в таблицу
                if (i >= dgv.RowCount)
                    dgv.Rows.Add(null, null, null);

                var row = dgv.Rows[i];
                row.Cells[0].Value = struct_PPRCh_Freq_Band[i].iID;
                row.Cells[1].Value = functions.FreqToStr(struct_PPRCh_Freq_Band[i].iFreq);
                row.Cells[2].Value = (Convert.ToSingle(struct_PPRCh_Freq_Band[i].iDFreq) / 10.0).ToString();
            }
        }

       
    }
}
