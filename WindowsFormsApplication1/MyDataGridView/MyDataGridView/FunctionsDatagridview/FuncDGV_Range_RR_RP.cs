﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyDataGridView
{
    public class FuncDGV_Range_RR_RP
    {
        Functions functions;

        Struct_RangeRR struct_RangeRR;
        Struct_RangeRP struct_RangeRP;

        public FuncDGV_Range_RR_RP()
        {
            functions = new Functions();
        }

        /// <summary>
        /// Функция добавления записи в таблицы Сектороа и диапазоны РР и РП в DGV
        /// </summary>
        /// <param name=""></param>
        public void AddRecord_Range_RR_RPToDGV(DataGridView dgv, NameTable nameTable, object obj)
        {
            int iNum = 0;

            try
            {
                for (int i = 0; i < dgv.RowCount; i++)
                {
                    if (dgv.Rows[i].Cells[0].Value != null)
                        iNum++;
                }

                switch (nameTable)
                {
                    case NameTable.RANGE_RR:

                        struct_RangeRR = (Struct_RangeRR)obj;

                        iNum = struct_RangeRR.iID - 1;

                        //добавить строку в таблицу
                        if (iNum >= dgv.RowCount)
                            dgv.Rows.Add();

                        dgv.Rows[iNum].Cells[0].Value = struct_RangeRR.iID;
                        dgv.Rows[iNum].Cells[1].Value = functions.FreqToStr(struct_RangeRR.iFreqMin);
                        dgv.Rows[iNum].Cells[2].Value = functions.FreqToStr(struct_RangeRR.iFreqMax);
                        dgv.Rows[iNum].Cells[3].Value = (struct_RangeRR.iAngleMin / 10.0).ToString();
                        dgv.Rows[iNum].Cells[4].Value = (struct_RangeRR.iAngleMax / 10.0).ToString();
                        dgv.Rows[iNum].Cells[5].Value = struct_RangeRR.bMode;

                        break;

                    case NameTable.RANGE_RP:

                        struct_RangeRP = (Struct_RangeRP)obj;

                        iNum = struct_RangeRP.iID - 1;

                        //добавить строку в таблицу
                        if (iNum >= dgv.RowCount)
                            dgv.Rows.Add();

                        dgv.Rows[iNum].Cells[0].Value = struct_RangeRP.iID;
                        dgv.Rows[iNum].Cells[1].Value = functions.FreqToStr(struct_RangeRP.iFreqMin);
                        dgv.Rows[iNum].Cells[2].Value = functions.FreqToStr(struct_RangeRP.iFreqMax);
                        dgv.Rows[iNum].Cells[3].Value = (struct_RangeRP.iAngleMin / 10.0).ToString();
                        dgv.Rows[iNum].Cells[4].Value = (struct_RangeRP.iAngleMax / 10.0).ToString();
                        dgv.Rows[iNum].Cells[5].Value = struct_RangeRP.bMode;

                        break;

                    default:
                        break;
                }
            }
            catch { }
        }
    }
}
