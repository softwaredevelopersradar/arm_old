﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using USR_DLL;

namespace MyDataGridView
{
    public class FuncDGV_IRI_PPRCh_RP
    {
        TDistribFHSS_RP DistribFHSS_RP;
        TDistribFHSS_RPExclude DistribFHSS_RPExclude;

        Functions functions;
        DecoderParams decoderParams;

        public FuncDGV_IRI_PPRCh_RP()
        { 
            functions = new Functions();
            decoderParams = new DecoderParams();
        }

        /// <summary>
        /// Функция добавления записи в таблицу в DGV
        /// </summary>
        /// <param name=""></param>
        public void AddRecord_IRI_PPRCh_RPToDGV(DataGridView dgv, NameTable nameTable, object obj, Table table)
        {
            byte bTable = 0;
            if (table == Table.Own) bTable = 1;
            if (table == Table.Linked) bTable = 2;

            int iNum = 0;
            string sLetters = "";
            string sEPO = "";

            for (int i = 0; i < dgv.RowCount; i++)
            {
                if (dgv.Rows[i].Cells[0].Value != null)
                    iNum++;
            }

            DistribFHSS_RP = (TDistribFHSS_RP)obj;

            //добавить строку в таблицу
            if (iNum >= dgv.RowCount)
                dgv.Rows.Add(null, null, null, null, null, null, Properties.Resources.empty, null, null, null, null, null, null);

            if (DistribFHSS_RP.bLetter.Length == 1)
                sLetters = DistribFHSS_RP.bLetter[0].ToString();
            else if(DistribFHSS_RP.bLetter.Length == 2)
            {
                sLetters = DistribFHSS_RP.bLetter[0] + " - " + DistribFHSS_RP.bLetter[1];
            }

            byte[] bEPO = functions.DefineEPO(DistribFHSS_RP.iFreqMin, DistribFHSS_RP.iFreqMax);
            if (bEPO.Length == 1) { sEPO = bEPO[0].ToString(); }
            else if (bEPO.Length == 2) { sEPO = bEPO[0] + " - " + bEPO[1]; }

            string sParamsJam = decoderParams.ParamHindrance(DistribFHSS_RP.bModulation, DistribFHSS_RP.bDeviation, DistribFHSS_RP.bManipulation, 10/*DistribFHSS_RP.bDuration*/);

            dgv.Rows[iNum].Cells[0].Value = DistribFHSS_RP.iID;
            dgv.Rows[iNum].Cells[1].Value = functions.FreqToStr(DistribFHSS_RP.iFreqMin);
            dgv.Rows[iNum].Cells[2].Value = functions.FreqToStr(DistribFHSS_RP.iFreqMax);
            dgv.Rows[iNum].Cells[3].Value = sLetters;
            dgv.Rows[iNum].Cells[4].Value = DistribFHSS_RP.sLevel;
            dgv.Rows[iNum].Cells[5].Value = sParamsJam;

            dgv.Rows[iNum].Cells[7].Value = sEPO;
            dgv.Rows[iNum].Cells[8].Value = DistribFHSS_RP.bModulation;
            dgv.Rows[iNum].Cells[9].Value = DistribFHSS_RP.bDeviation;
            dgv.Rows[iNum].Cells[10].Value = DistribFHSS_RP.bManipulation;
            dgv.Rows[iNum].Cells[11].Value = DistribFHSS_RP.iDuration;
            dgv.Rows[iNum].Cells[12].Value = bTable;
        }

        public void AddRecord_IRI_PPRChFreq_RPToDGV(DataGridView dgv, int id, int FreqMin, int FreqMax, int Step, Table table)
        {
            byte bTable = 0;
            if (table == Table.Own) bTable = 1;
            if (table == Table.Linked) bTable = 2;


            int len = (FreqMax - FreqMin) / Step;

            for (var i = 0; i < len; i++)
            {
                //добавить строку в таблицу
                if (i >= dgv.RowCount)
                    dgv.Rows.Add();

                var row = dgv.Rows[i];
                row.Cells[0].Value = id;
                row.Cells[1].Value = functions.FreqToStr(FreqMin + (Step * i));
                row.Cells[2].Value = bTable;
            }
        }

        /// <summary>
        /// Функция добавления записи в таблицу PPRChExclude в DGV
        /// </summary>
        /// <param name=""></param>
        public void AddRecord_IRI_PPRCh_RPExcludeToDGV(DataGridView dgv, NameTable nameTable, object obj, Table table)
        {
            byte bTable = 0;
            if (table == Table.Own) bTable = 1;
            if (table == Table.Linked) bTable = 2;

            int iNum = 0;

            for (int i = 0; i < dgv.RowCount; i++)
            {
                if (dgv.Rows[i].Cells[0].Value != null)
                    iNum++;
            }

            DistribFHSS_RPExclude = (TDistribFHSS_RPExclude)obj;

            //добавить строку в таблицу
            if (iNum >= dgv.RowCount)
                dgv.Rows.Add(null, null, null, null);

            dgv.Rows[iNum].Cells[0].Value = DistribFHSS_RPExclude.iID;
            dgv.Rows[iNum].Cells[1].Value = functions.FreqToStr(DistribFHSS_RPExclude.iFreqExclude);
            dgv.Rows[iNum].Cells[2].Value = (Convert.ToSingle(DistribFHSS_RPExclude.iWidthExclude) / 10.0).ToString();
            dgv.Rows[iNum].Cells[3].Value = bTable;
        }
    }
}
