﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyDataGridView
{
    public class FuncDGV_IRI_FRCh
    {
        Functions functions;
        DecoderParams decoderParams;

        Struct_IRI_FRCh[] struct_IRI_FRCh;
        Struct_IRI_FRCh ChangeRecIRI_FRCh;

        public FuncDGV_IRI_FRCh()
        {
            functions = new Functions();
            decoderParams = new DecoderParams();
        }

        /// <summary>
        /// Функция добавления записи в таблицу в DGV
        /// </summary>
        /// <param name=""></param>
        public void AddRecord_IRI_FRChToDGV(DataGridView dgv, NameTable nameTable, object obj)
        {
            struct_IRI_FRCh = (Struct_IRI_FRCh[])obj;

            for (var i = 0; i < struct_IRI_FRCh.Length; i++)
            {
                // определить цвет
                Color color = functions.CheckSpecFreq(struct_IRI_FRCh[i].iFreq);

                //добавить строку в таблицу
                if (i >= dgv.RowCount)
                    dgv.Rows.Add(null, Properties.Resources.empty, null, null, null, null, null, null, null, null, null, null, null, null, null);

                var row = dgv.Rows[i];

                row.Cells[0].Value = struct_IRI_FRCh[i].iID;
                if (struct_IRI_FRCh[i].bKR == 0)
                    row.Cells[1].Value = Properties.Resources.empty;
                else
                    row.Cells[1].Value = Properties.Resources.green;

                row.Cells[2].Value = functions.FreqToStr(struct_IRI_FRCh[i].iFreq);
                row.Cells[2].Style.ForeColor = color;

                if (struct_IRI_FRCh[i].iQ1 == -1) 
                { 
                    row.Cells[3].Value = "-";
                    row.Cells[3].Style.ForeColor = color;
                }
                else 
                { 
                    row.Cells[3].Value = (Convert.ToSingle(struct_IRI_FRCh[i].iQ1) / 10.0).ToString();
                    row.Cells[3].Style.ForeColor = color;
                }

                if (struct_IRI_FRCh[i].iQ2 == -1)
                { 
                    row.Cells[4].Value = "-";
                    row.Cells[4].Style.ForeColor = color;
                }
                else 
                { 
                    row.Cells[4].Value = (Convert.ToSingle(struct_IRI_FRCh[i].iQ2) / 10.0).ToString();
                    row.Cells[4].Style.ForeColor = color;
                }

                row.Cells[5].Value = struct_IRI_FRCh[i].iLevel * (-1);
                row.Cells[5].Style.ForeColor = color;

                row.Cells[6].Value = (Convert.ToSingle(struct_IRI_FRCh[i].iDFreq) / 10.0).ToString();
                row.Cells[6].Style.ForeColor = color;

                row.Cells[7].Value = (Convert.ToSingle(struct_IRI_FRCh[i].iCKO) / 10).ToString();
                row.Cells[7].Style.ForeColor = color;

                if (struct_IRI_FRCh[i].dLatitude == -1 && struct_IRI_FRCh[i].dLongitude == -1) 
                {
                    row.Cells[8].Value = "-";
                    row.Cells[8].Style.ForeColor = color;
                    row.Cells[9].Value = "-";
                    row.Cells[9].Style.ForeColor = color;
                }
                else 
                {
                    row.Cells[8].Value = struct_IRI_FRCh[i].dLatitude.ToString("0.000000");
                    row.Cells[8].Style.ForeColor = color;
                    row.Cells[9].Value = struct_IRI_FRCh[i].dLongitude.ToString("0.000000");
                    row.Cells[9].Style.ForeColor = color;
                }

                row.Cells[10].Value = decoderParams.ViewModulation(struct_IRI_FRCh[i].bView);
                row.Cells[10].Style.ForeColor = color;

                row.Cells[11].Value = SMeaning.meaningOwn;
                row.Cells[11].Style.ForeColor = color;

                row.Cells[12].Value = struct_IRI_FRCh[i].sTime;
                row.Cells[12].Style.ForeColor = color;

                row.Cells[13].Value = functions.DurationToStr(struct_IRI_FRCh[i].iDurationSignal);
                row.Cells[13].Style.ForeColor = color;

                row.Cells[14].Value = struct_IRI_FRCh[i].iCountExits;
                row.Cells[14].Style.ForeColor = color;

                if (row.Selected)
                    row.DefaultCellStyle.SelectionForeColor = color;
            }
        }

        public List<DataGridViewRow> AddRecord_IRI_FRChToDGV2(DataGridView dgv, NameTable nameTable, object obj)
        {
            List<DataGridViewRow> lrs = new List<DataGridViewRow>();

            struct_IRI_FRCh = (Struct_IRI_FRCh[])obj;

            for (var i = 0; i < struct_IRI_FRCh.Length; i++)
            {
                // определить цвет
                Color color = functions.CheckSpecFreq(struct_IRI_FRCh[i].iFreq);

                DataGridViewRow row = (DataGridViewRow)dgv.Rows[dgv.Rows.Count - 1].Clone();

                row.Cells[0].Value = struct_IRI_FRCh[i].iID;
                if (struct_IRI_FRCh[i].bKR == 0)
                    row.Cells[1].Value = Properties.Resources.empty;
                else
                    row.Cells[1].Value = Properties.Resources.green;

                row.Cells[2].Value = functions.FreqToStr(struct_IRI_FRCh[i].iFreq);
                row.Cells[2].Style.ForeColor = color;

                if (struct_IRI_FRCh[i].iQ1 == -1)
                {
                    row.Cells[3].Value = "-";
                    row.Cells[3].Style.ForeColor = color;
                }
                else
                {
                    row.Cells[3].Value = (Convert.ToSingle(struct_IRI_FRCh[i].iQ1) / 10.0).ToString();
                    row.Cells[3].Style.ForeColor = color;
                }

                if (struct_IRI_FRCh[i].iQ2 == -1)
                {
                    row.Cells[4].Value = "-";
                    row.Cells[4].Style.ForeColor = color;
                }
                else
                {
                    row.Cells[4].Value = (Convert.ToSingle(struct_IRI_FRCh[i].iQ2) / 10.0).ToString();
                    row.Cells[4].Style.ForeColor = color;
                }

                row.Cells[5].Value = struct_IRI_FRCh[i].iLevel * (-1);
                row.Cells[5].Style.ForeColor = color;

                row.Cells[6].Value = (Convert.ToSingle(struct_IRI_FRCh[i].iDFreq) / 10.0).ToString();
                row.Cells[6].Style.ForeColor = color;

                row.Cells[7].Value = (Convert.ToSingle(struct_IRI_FRCh[i].iCKO) / 10).ToString();
                row.Cells[7].Style.ForeColor = color;

                if (struct_IRI_FRCh[i].dLatitude == -1 && struct_IRI_FRCh[i].dLongitude == -1)
                {
                    row.Cells[8].Value = "-";
                    row.Cells[8].Style.ForeColor = color;
                    row.Cells[9].Value = "-";
                    row.Cells[9].Style.ForeColor = color;
                }
                else
                {
                    row.Cells[8].Value = struct_IRI_FRCh[i].dLatitude.ToString("0.000000");
                    row.Cells[8].Style.ForeColor = color;
                    row.Cells[9].Value = struct_IRI_FRCh[i].dLongitude.ToString("0.000000");
                    row.Cells[9].Style.ForeColor = color;
                }

                row.Cells[10].Value = decoderParams.ViewModulation(struct_IRI_FRCh[i].bView);
                row.Cells[10].Style.ForeColor = color;

                row.Cells[11].Value = SMeaning.meaningOwn;
                row.Cells[11].Style.ForeColor = color;

                row.Cells[12].Value = struct_IRI_FRCh[i].sTime;
                row.Cells[12].Style.ForeColor = color;

                row.Cells[13].Value = functions.DurationToStr(struct_IRI_FRCh[i].iDurationSignal);
                row.Cells[13].Style.ForeColor = color;

                row.Cells[14].Value = struct_IRI_FRCh[i].iCountExits;
                row.Cells[14].Style.ForeColor = color;
                
                if (row.Selected)
                    row.DefaultCellStyle.SelectionForeColor = color;

                lrs.Add(row);
            }

            return lrs;

        }

        public DataGridViewRow NullRow(DataGridView dgv, NameTable nameTable, object obj)
        {
            var row = (DataGridViewRow)dgv.Rows[dgv.Rows.Count - 1].Clone();
            row.SetValues(null, Properties.Resources.empty, null, null, null, null, null, null, null, null, null, null, null, null, null);
            return row;
        }

        /// <summary>
        /// Функция изменения записи в таблице в DGV
        /// </summary>
        /// <param name=""></param>
        public void ChangeRecord_IRI_FRChToDGV(DataGridView dgv, NameTable nameTable, object obj, int iNum)
        {
            ChangeRecIRI_FRCh = (Struct_IRI_FRCh)obj;

            var row = dgv.Rows[iNum];
            row.Cells[0].Value = ChangeRecIRI_FRCh.iID;
            if (ChangeRecIRI_FRCh.bKR == 0)
                row.Cells[1].Value = Properties.Resources.empty;
            else
                row.Cells[1].Value = Properties.Resources.green;
            row.Cells[2].Value = functions.FreqToStr(ChangeRecIRI_FRCh.iFreq);
            if (ChangeRecIRI_FRCh.iQ1 == -1) { row.Cells[3].Value = "-"; }
            else { row.Cells[3].Value = Convert.ToSingle(ChangeRecIRI_FRCh.iQ1) / 10.0; }
            if (ChangeRecIRI_FRCh.iQ2 == -1) { row.Cells[4].Value = "-"; }
            else { row.Cells[4].Value = Convert.ToSingle(ChangeRecIRI_FRCh.iQ2) / 10.0; }
            row.Cells[5].Value = ChangeRecIRI_FRCh.iLevel * (-1);
            row.Cells[6].Value = Convert.ToSingle(ChangeRecIRI_FRCh.iDFreq) / 10.0;
            row.Cells[7].Value = Convert.ToSingle(ChangeRecIRI_FRCh.iCKO) / 10;
            if (ChangeRecIRI_FRCh.dLatitude == -1 && ChangeRecIRI_FRCh.dLongitude == -1)
            {
                row.Cells[8].Value = "-";
                row.Cells[9].Value = "-";
            }
            else
            {
                row.Cells[8].Value = ChangeRecIRI_FRCh.dLatitude.ToString("0.000000");
                row.Cells[9].Value = ChangeRecIRI_FRCh.dLongitude.ToString("0.000000");
            }
            row.Cells[10].Value = decoderParams.ViewModulation(ChangeRecIRI_FRCh.bView);
            row.Cells[11].Value = SMeaning.meaningOwn;
            row.Cells[12].Value = ChangeRecIRI_FRCh.sTime;
            row.Cells[13].Value = functions.DurationToStr(ChangeRecIRI_FRCh.iDurationSignal);
            row.Cells[14].Value = ChangeRecIRI_FRCh.iCountExits;
        }
    }
}
