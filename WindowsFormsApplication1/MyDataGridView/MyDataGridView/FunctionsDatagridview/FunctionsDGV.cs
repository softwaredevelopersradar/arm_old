﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using USR_DLL;

namespace MyDataGridView
{
    public class FunctionsDGV
    {
        public int iCurID = 0;
        public bool fCurID = false;

        public FunctionsDGV()
        {
        }

        /// <summary>
        /// Удалить одну запись из таблицы DataGridView
        /// </summary>
        /// <param name="myDGV"></param>
        public int DeleteOneRecordDGV(DataGridView dgv)
        {
            Int32 rowToDelete = dgv.Rows.GetFirstRow(DataGridViewElementStates.Selected);
            int iID = Convert.ToInt32(dgv.Rows[rowToDelete].Cells[0].Value);

            if (rowToDelete > -1)
            {
                dgv.Rows.RemoveAt(rowToDelete);
                fCurID = true;
            }

            return iID;
        }

        /// <summary>
        /// Удалить одну запись из таблицы DataGridView
        /// </summary>
        /// <param name="myDGV"></param>
        public int DeleteOneRecordDGV(DataGridView dgv, int id)
        {
            int iID = 0;
            for (int i = 0; i < dgv.RowCount; i++)
            {
                if (Convert.ToInt32(dgv.Rows[i].Cells[0].Value) == id)
                {
                    iID = Convert.ToInt32(dgv.Rows[i].Cells[0].Value);
                    dgv.Rows.RemoveAt(i);
                    return iID;
                }
            }
            return iID;
        }

        /// <summary>
        /// Удалить все записи из таблицы DataGridView
        /// </summary>
        /// <param name="myDGV"></param>
        public void DeleteAllRecordsDGV(DataGridView dgv)
        {
            try
            {
                dgv.Rows.Clear();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }

        /// <summary>
        /// Количество записей в таблице
        /// </summary>
        /// <param name="dgv"></param>
        /// <returns></returns>
        public int AmountRecordsDGV(DataGridView dgv)
        {
            int amount = 0;

            for (int i = 0; i < dgv.RowCount; i++)
            {
                if (dgv.Rows[i].Cells[0].Value != null)
                {
                    amount++;
                }
            }

            return amount;
        }

        public int AmountRecordsRangesDGV(DataGridView dgv)
        {
            int amount = 0;

            for (int i = 0; i < dgv.RowCount; i++)
            {
                if (dgv.Rows[i].Cells[0].Value != null && dgv.Rows[i].Cells[1].Value.ToString() != "————")
                {
                    amount++;
                }
            }

            return amount;
        }
    }
}
