﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyDataGridView
{
    public class FuncDGV_SpecialFrequencies
    {
        Functions functions;

        Struct_SpecialFrequencies struct_SpecialFrequencies;

        public FuncDGV_SpecialFrequencies()
        {
            functions = new Functions();
         
            struct_SpecialFrequencies = new Struct_SpecialFrequencies();
        }

        /// <summary>
        /// Функция добавления записи в таблицы Специальных частот в DGV
        /// </summary>
        /// <param name=""></param>
        public void AddRecord_SpecialFreqToDGV(DataGridView dgv, NameTable nameTable, object obj)
        {
            int iNum = 0;
            int iID = 0;
            int iTempNum = 0;

            if (dgv.RowCount == 0)
            {
                iID = 1;
            }
            else if (dgv.Rows[0].Cells[0].Value == null)
            {
                iID = 1;
            }
            else
            {
                for (int i = 0; i < dgv.RowCount; i++)
                {
                    if (dgv.Rows[i].Cells[0].Value != null)
                    {
                        iNum++;
                    }
                }
                iTempNum = iNum;

                iID = Convert.ToInt32(dgv.Rows[iNum - 1].Cells[0].Value) + 1;
            }

            switch (nameTable)
            {
                case NameTable.FREQ_FORBIDDEN:

                    struct_SpecialFrequencies = (Struct_SpecialFrequencies)obj;

                    iNum = struct_SpecialFrequencies.iID - 1;

                    //добавить строку в таблицу
                    if (iNum >= dgv.RowCount)
                        dgv.Rows.Add();

                    dgv.Rows[iNum].Cells[0].Value = struct_SpecialFrequencies.iID;
                    dgv.Rows[iNum].Cells[1].Value = functions.FreqToStr(struct_SpecialFrequencies.iFreqMin);

                    if (struct_SpecialFrequencies.iFreqMin == struct_SpecialFrequencies.iFreqMax)
                        dgv.Rows[iNum].Cells[2].Value = "————";
                    else dgv.Rows[iNum].Cells[2].Value = functions.FreqToStr(struct_SpecialFrequencies.iFreqMax);

                    dgv.Rows[iNum].Cells[3].Value = struct_SpecialFrequencies.bMode;

                    break;

                case NameTable.FREQ_KNOWN:

                    struct_SpecialFrequencies = (Struct_SpecialFrequencies)obj;

                    iNum = struct_SpecialFrequencies.iID - 1;

                    //добавить строку в таблицу
                    if (iNum >= dgv.RowCount)
                        dgv.Rows.Add();

                    dgv.Rows[iNum].Cells[0].Value = struct_SpecialFrequencies.iID;
                    dgv.Rows[iNum].Cells[1].Value = functions.FreqToStr(struct_SpecialFrequencies.iFreqMin);

                    if (struct_SpecialFrequencies.iFreqMin == struct_SpecialFrequencies.iFreqMax)
                        dgv.Rows[iNum].Cells[2].Value = "————";
                    else dgv.Rows[iNum].Cells[2].Value = functions.FreqToStr(struct_SpecialFrequencies.iFreqMax);

                    dgv.Rows[iNum].Cells[3].Value = struct_SpecialFrequencies.bMode;

                    break;

                case NameTable.FREQ_IMPORTANT:

                    struct_SpecialFrequencies = (Struct_SpecialFrequencies)obj;

                    iNum = struct_SpecialFrequencies.iID - 1;

                    //добавить строку в таблицу
                    if (iNum >= dgv.RowCount)
                        dgv.Rows.Add();

                    dgv.Rows[iNum].Cells[0].Value = struct_SpecialFrequencies.iID;
                    dgv.Rows[iNum].Cells[1].Value = functions.FreqToStr(struct_SpecialFrequencies.iFreqMin);

                    if (struct_SpecialFrequencies.iFreqMin == struct_SpecialFrequencies.iFreqMax)
                        dgv.Rows[iNum].Cells[2].Value = "————";
                    else dgv.Rows[iNum].Cells[2].Value = functions.FreqToStr(struct_SpecialFrequencies.iFreqMax);

                    dgv.Rows[iNum].Cells[3].Value = struct_SpecialFrequencies.bMode;

                    break;

                default:
                    break;
            }
        }
    }
}
