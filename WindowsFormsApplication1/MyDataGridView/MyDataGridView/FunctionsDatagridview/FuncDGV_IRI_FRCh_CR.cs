﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using USR_DLL;

namespace MyDataGridView
{
    public class FuncDGV_IRI_FRCh_CR
    {
        TDistribFWS DistribFWS;

        Functions functions;
        DecoderParams decoderParams;

        public FuncDGV_IRI_FRCh_CR()
        {
            functions = new Functions();
            decoderParams = new DecoderParams();
        }

        /// <summary>
        /// Функция добавления записи в таблицу в DGV
        /// </summary>
        /// <param name=""></param>
        public void AddRecord_IRI_FRCh_CRToDGV(DataGridView dgv, NameTable nameTable, object obj)
        {
            int iNum = 0;

            for (int i = 0; i < dgv.RowCount; i++)
            {
                if (dgv.Rows[i].Cells[0].Value != null)
                    iNum++;
            }

            DistribFWS = (TDistribFWS)obj;

            //добавить строку в таблицу
            if (iNum >= dgv.RowCount)
                dgv.Rows.Add();

            dgv.Rows[iNum].Cells[0].Value = DistribFWS.iID;
            dgv.Rows[iNum].Cells[1].Value = functions.FreqToStr(DistribFWS.iFreq);
            if (DistribFWS.sBearing1 == -1) { dgv.Rows[iNum].Cells[2].Value = "-"; }
            else { dgv.Rows[iNum].Cells[2].Value = (Convert.ToSingle(DistribFWS.sBearing1) / 10.0).ToString(); }
            if (DistribFWS.sBearing2 == -1) { dgv.Rows[iNum].Cells[3].Value = "-"; }
            else { dgv.Rows[iNum].Cells[3].Value = (Convert.ToSingle(DistribFWS.sBearing2) / 10.0).ToString(); }
            dgv.Rows[iNum].Cells[4].Value = DistribFWS.sLevel;// *(-1);
            dgv.Rows[iNum].Cells[5].Value = (Convert.ToSingle(DistribFWS.iDFreq) / 10.0).ToString();
            dgv.Rows[iNum].Cells[6].Value = (Convert.ToSingle(DistribFWS.iCKO) / 10.0).ToString();
            if (DistribFWS.dLatitude == -1 && DistribFWS.dLongitude == -1)
            { 
                dgv.Rows[iNum].Cells[7].Value = "-";
                dgv.Rows[iNum].Cells[8].Value = "-";
            }
            else
            {
                dgv.Rows[iNum].Cells[7].Value = Convert.ToDouble(DistribFWS.dLatitude, Functions.format).ToString("0.000000");
                dgv.Rows[iNum].Cells[8].Value = Convert.ToDouble(DistribFWS.dLongitude, Functions.format).ToString("0.000000");
            }

            dgv.Rows[iNum].Cells[9].Value = decoderParams.ViewModulation(DistribFWS.bView);

            switch (DistribFWS.iSP_RR)
            { 
                case 0:
                    dgv.Rows[iNum].Cells[10].Value = "";
                    break;

                case 1:
                    dgv.Rows[iNum].Cells[10].Value = SMeaning.meaningOwn;
                    break;

                case 2:
                    dgv.Rows[iNum].Cells[10].Value = SMeaning.meaningLinked;
                    break;

                default:
                    dgv.Rows[iNum].Cells[10].Value = "";
                    break;

            }
            //dgv.Rows[iNum].Cells[10].Value = DistribFWS.iSP_RR;

            switch (DistribFWS.iSP_RP)
            {
                case 0:
                    dgv.Rows[iNum].Cells[11].Value = "";
                    break;

                case 1:
                    dgv.Rows[iNum].Cells[11].Value = SMeaning.meaningOwn;
                    break;

                case 2:
                    dgv.Rows[iNum].Cells[11].Value = SMeaning.meaningLinked;
                    break;

                default:
                    dgv.Rows[iNum].Cells[11].Value = "";
                    break;

            }
            //dgv.Rows[iNum].Cells[11].Value = DistribFWS.iSP_RP;
        }


    }
}
