﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using USR_DLL;

namespace MyDataGridView
{
    public class FuncDGV_IRI_FRCh_RP
    {
        TSupprFWS SupprFWS;

        Functions functions;
        DecoderParams decoderParams;

        public FuncDGV_IRI_FRCh_RP()
        { 
            functions = new Functions();
            decoderParams = new DecoderParams();
        }

        /// <summary>
        /// Функция добавления записи в таблицу FRCh в DGV
        /// </summary>
        /// <param name=""></param>
        public void AddRecord_IRI_FRCh_RPToDGV(DataGridView dgv, NameTable nameTable, object obj, Table table)
        {
            byte bTable = 0;
            if (table == Table.Own) bTable = 1;
            if (table == Table.Linked) bTable = 2;

            int iNum = 0;

            for (int i = 0; i < dgv.RowCount; i++)
            {
                if (dgv.Rows[i].Cells[0].Value != null)
                    iNum++;
            }
            
            SupprFWS = (TSupprFWS)obj;

             //добавить строку в таблицу
            if (iNum >= dgv.RowCount)
                dgv.Rows.Add(null, null, null, null, null, null, null, Properties.Resources.empty, Properties.Resources.empty, Properties.Resources.empty, null, null, null, null, null);

            string sParamsJam = decoderParams.ParamHindrance(SupprFWS.bModulation, SupprFWS.bDeviation, SupprFWS.bManipulation, SupprFWS.bDuration);

            dgv.Rows[iNum].Cells[0].Value = SupprFWS.iID;
            dgv.Rows[iNum].Cells[1].Value = functions.FreqToStr(SupprFWS.iFreq);

            if (SupprFWS.sBearing == -1) { dgv.Rows[iNum].Cells[2].Value = "-"; }
            else { dgv.Rows[iNum].Cells[2].Value = (Convert.ToSingle(SupprFWS.sBearing) / 10.0).ToString(); }
            
            dgv.Rows[iNum].Cells[3].Value = SupprFWS.bLetter;
            dgv.Rows[iNum].Cells[4].Value = sParamsJam;
            dgv.Rows[iNum].Cells[5].Value = SupprFWS.sLevel;
            dgv.Rows[iNum].Cells[6].Value = SupprFWS.bPrior;

            dgv.Rows[iNum].Cells[10].Value = SupprFWS.bModulation;
            dgv.Rows[iNum].Cells[11].Value = SupprFWS.bDeviation;
            dgv.Rows[iNum].Cells[12].Value = SupprFWS.bManipulation;
            dgv.Rows[iNum].Cells[13].Value = SupprFWS.bDuration;
            dgv.Rows[iNum].Cells[14].Value = bTable;
        }
    }
}
