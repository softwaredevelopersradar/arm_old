﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyDataGridView
{
    public class FuncDGV_ADSB
    {
        Struct_ADSB_Receiver struct_ADSB_Receiver;

        public FuncDGV_ADSB()
        { 
        
        }

        /// <summary>
        /// Функция добавления записи ADSB в таблицу в DGV
        /// </summary>
        /// <param name=""></param>
        public void AddRecord_ADSBToDGV(DataGridView dgv, NameTable nameTable, object obj)
        {
            int iNum = 0;
            int iID = 0;
            int iTempNum = 0;

            if (dgv.RowCount == 0)
            {
                iID = 1;
            }
            else if (dgv.Rows[0].Cells[0].Value == null)
            {
                iID = 1;
            }
            else
            {
                for (int i = 0; i < dgv.RowCount; i++)
                {
                    if (dgv.Rows[i].Cells[0].Value != null)
                    {
                        iNum++;
                    }
                }
                iTempNum = iNum;

                iID = Convert.ToInt32(dgv.Rows[iNum - 1].Cells[0].Value) + 1;
            }

            
            bool fICAO = false;

            struct_ADSB_Receiver = (Struct_ADSB_Receiver)obj;

            iNum = struct_ADSB_Receiver.iID - 1;

            //добавить строку в таблицу
            if (iNum >= dgv.RowCount)
                dgv.Rows.Add();

            for (int i = 0; i < dgv.RowCount; i++)
            {
                if (dgv.Rows[i].Cells[1].Value != null)
                {
                    int cmp = dgv.Rows[i].Cells[1].Value.ToString().CompareTo(struct_ADSB_Receiver.sICAO);

                    if (cmp == 0)
                    {
                        fICAO = true;
                        iNum = i;
                    }
                }
            }

            if (fICAO)
            {
                if (struct_ADSB_Receiver.sLatitude != "")
                    dgv.Rows[iNum].Cells[2].Value = struct_ADSB_Receiver.sLatitude;
                if (struct_ADSB_Receiver.sLongitude != "")
                    dgv.Rows[iNum].Cells[3].Value = struct_ADSB_Receiver.sLongitude;
                if (struct_ADSB_Receiver.sAltitude != "")
                    dgv.Rows[iNum].Cells[4].Value = struct_ADSB_Receiver.sAltitude;
                dgv.Rows[iNum].Cells[5].Value = struct_ADSB_Receiver.sDatetime;
            }
            else
            {
                dgv.Rows[iNum].Cells[0].Value = struct_ADSB_Receiver.iID;
                dgv.Rows[iNum].Cells[1].Value = struct_ADSB_Receiver.sICAO;
                dgv.Rows[iNum].Cells[2].Value = struct_ADSB_Receiver.sLatitude;
                dgv.Rows[iNum].Cells[3].Value = struct_ADSB_Receiver.sLongitude;
                dgv.Rows[iNum].Cells[4].Value = struct_ADSB_Receiver.sAltitude;
                dgv.Rows[iNum].Cells[5].Value = struct_ADSB_Receiver.sDatetime;

            }
        }
    }
}
