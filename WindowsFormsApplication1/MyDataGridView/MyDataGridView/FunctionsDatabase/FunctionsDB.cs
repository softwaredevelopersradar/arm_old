﻿using Protocols;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using USR_DLL;
using VariableDynamic;



namespace MyDataGridView
{
    public class FunctionsDB
    {
        VariableWork variableWork;

        DecoderParams decoderParams;
        Functions functions;

        public FunctionsDB()
        {
            decoderParams = new DecoderParams();
            functions = new Functions();

            variableWork = new VariableWork();

        }

        public string ConnectionString()
        {
            string connectionString = "";

            if (System.IO.File.Exists(Application.StartupPath + "/DB/database.db"))
            //if (System.IO.File.Exists("E:/Work/Main/WindowsFormsApplication1/WindowsFormsApplication1/bin/Debug/DB/database.db"))
            {
                //connectionString = @"Data Source=E:/Work/Main/WindowsFormsApplication1/WindowsFormsApplication1/bin/Debug/DB/database.db";
                connectionString = @"Data Source=" + Application.StartupPath + "/DB/database.db";

            }
            else
            {
                MessageBox.Show(SMessageError.mesErrFileDBNotFound, SMessageError.mesErr, MessageBoxButtons.OK, MessageBoxIcon.Error);
                connectionString = "";

                return connectionString;
            }

            return connectionString;
        }

        /// <summary>
        /// Подсчет количества записей в таблице БД
        /// </summary>
        /// <param name="nameTable"> название таблицы </param>
        /// <returns> количество записей </returns>
        public int CountRecordsDB(NameTable nameTable, Table table)
        { 
            int countRec = 0;

            byte bTable = 0;
            if (table == Table.Own) bTable = 1;
            if (table == Table.Linked) bTable = 2;
            
            if (ConnectionString() != "")
            {
                System.Data.SQLite.SQLiteConnection sqLiteConnect;
                System.Data.SQLite.SQLiteCommand sqLiteCommand;

                sqLiteConnect = new System.Data.SQLite.SQLiteConnection(ConnectionString());

                try
                {
                    sqLiteConnect.Open();
                    if (bTable == 0)
                        sqLiteCommand = new System.Data.SQLite.SQLiteCommand("SELECT COUNT(*) FROM " + nameTable.ToString(), sqLiteConnect);
                    else
                        sqLiteCommand = new System.Data.SQLite.SQLiteCommand("SELECT COUNT(*) FROM " + nameTable.ToString() + " WHERE Mode=" + bTable, sqLiteConnect);


                    countRec = System.Convert.ToInt32(sqLiteCommand.ExecuteScalar());
                }
                catch (System.Data.DataException)
                {
                    return countRec;
                }
                finally
                {
                    sqLiteConnect.Close();
                }

                // удалить подключение к БД
                sqLiteConnect = null;
                sqLiteCommand = null;
            }

            return countRec;
        }

        /// <summary>
        /// Удалить одну запись из таблицы базы данных
        /// </summary>
        /// <param name="iNum"></param>
        public bool DeleteOneRecordDB(int iID, NameTable nameTable, Table table)
        {
            bool fDelete = false;

            byte bTable = 0;
            if (table == Table.Own) bTable = 1;
            if (table == Table.Linked) bTable = 2;

            if (ConnectionString() != "")
            {
                string name = nameTable.ToString();

                System.Data.SQLite.SQLiteConnection sqLiteConnect;
                System.Data.SQLite.SQLiteCommand sqLiteCommand;

                // подключиться к БД
                sqLiteConnect = new System.Data.SQLite.SQLiteConnection(ConnectionString());

                try
                {
                    // удалить запись с номером Num = iNum
                    sqLiteConnect.Open();

                    if (bTable == 0)
                        sqLiteCommand = new System.Data.SQLite.SQLiteCommand("DELETE FROM " + name + " WHERE ID=" + iID, sqLiteConnect);
                    else
                        sqLiteCommand = new System.Data.SQLite.SQLiteCommand("DELETE FROM " + name + " WHERE ID=" + iID + " AND Mode=" + bTable, sqLiteConnect);

                    sqLiteCommand.Parameters.AddWithValue("@iID", iID);
                    sqLiteCommand.ExecuteNonQuery();


                    fDelete = true;
                }
                catch (System.Data.DataException)
                {
                    MessageBox.Show(SMessageError.mesErrDeleteRecord, SMessageError.mesErr, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.ServiceNotification);
                    fDelete = false;

                    return fDelete;
                }
                finally
                {
                    sqLiteConnect.Close();
                }

                // обновить номера ID записей, следующих за удаленной
                if (iID > 0)
                {
                    try
                    {
                        sqLiteConnect.Open();

                        if (bTable == 0)
                        {
                            sqLiteCommand = new System.Data.SQLite.SQLiteCommand("UPDATE " + name + " SET ID =ID-1 WHERE ID > @iID", sqLiteConnect);
                        }
                        else
                        {
                            sqLiteCommand = new System.Data.SQLite.SQLiteCommand("UPDATE " + name + " SET ID =ID-1 WHERE ID > @iID AND Mode=" + bTable, sqLiteConnect);
                        }
                        sqLiteCommand.Parameters.AddWithValue("@iID", iID);
                        sqLiteCommand.ExecuteNonQuery();
                    }
                    catch (System.Data.DataException ex)
                    {
                        MessageBox.Show(ex.Message, SMessageError.mesErr, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.ServiceNotification);
                    }
                    finally
                    {
                        sqLiteConnect.Close();
                    }
                }

                // удалить подключение к БД
                sqLiteConnect = null;
                sqLiteCommand = null;
            }

            return fDelete;
        }
        
        /// <summary>
        /// Удалить все записи из таблицы базы данных
        /// </summary>
        /// <param name="nameTable"> Название таблицы</param>
        /// <param name="bTable"> Режим таблицы (0 - таблица (без поля режим: ведущая, ведомая), 1 - ведущая, 2 - ведомая)</param>
        public bool DeleteAllRecordsDB(NameTable nameTable, Table table)
        {
            bool fDeleteAll = false;

            byte bTable = 0;
            if (table == Table.Own) bTable = 1;
            if (table == Table.Linked) bTable = 2;

            if (ConnectionString() != "")
            {
                string name = nameTable.ToString();

                System.Data.SQLite.SQLiteConnection sqLiteConnect;
                System.Data.SQLite.SQLiteCommand sqLiteCommand;

                sqLiteConnect = new System.Data.SQLite.SQLiteConnection(ConnectionString());

                try
                {
                    sqLiteConnect.Open();

                    if (bTable == 0)
                    {
                        sqLiteCommand = new System.Data.SQLite.SQLiteCommand("DELETE FROM " + name +
                            "; DELETE FROM sqlite_sequence WHERE name = '" + name + "'", sqLiteConnect);

                        fDeleteAll = true;
                    }
                    else
                    {
                        sqLiteCommand = new System.Data.SQLite.SQLiteCommand("DELETE FROM " + name + " WHERE Mode=" + bTable +
                            "; DELETE FROM sqlite_sequence WHERE name = '" + name + "'", sqLiteConnect);

                        fDeleteAll = true;
                    }

                    //  Example
                    //  DELETE FROM IRI_FRCh_RP;   
                    //  DELETE FROM sqlite_sequence WHERE name = 'IRI_FRCh_RP' // обнулить счетчик ID

                    sqLiteCommand.ExecuteNonQuery();
                }
                catch (System.Data.DataException)
                {
                    MessageBox.Show(SMessageError.mesErrDeleteRecord, SMessageError.mesErr, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.ServiceNotification);
                    fDeleteAll = false;

                    return fDeleteAll;
                }
                finally
                {
                    sqLiteConnect.Close();
                }

                // удалить подключение к БД
                sqLiteConnect = null;
                sqLiteCommand = null;
            }

            return fDeleteAll;
        }
    }
}
