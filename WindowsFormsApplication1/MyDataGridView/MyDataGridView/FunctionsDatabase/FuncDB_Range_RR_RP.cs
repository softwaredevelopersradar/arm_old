﻿using Protocols;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using VariableDynamic;

namespace MyDataGridView
{
    public class FuncDB_Range_RR_RP
    {
        FunctionsDB functionsDB;
        VariableWork variableWork;

        Struct_RangeRR struct_RangeRR;
        Struct_RangeRP struct_RangeRP;

        public FuncDB_Range_RR_RP()
        {
            functionsDB = new FunctionsDB();
            variableWork = new VariableWork();
        }

        /// <summary>
        /// Функция добавления записи в таблицы Range_RR и Range_RP в БД
        /// </summary>
        /// <param name="myDGV"></param>
        /// <param name="bTable"></param>
        public bool AddRecord_Range_RR_RPToDB(NameTable nameTable, object obj)
        {
            bool fRead = false;

            if (functionsDB.ConnectionString() != "")
            {
                System.Data.SQLite.SQLiteConnection sqLiteConnect;
                System.Data.SQLite.SQLiteCommand sqLiteCommand;

                sqLiteConnect = new System.Data.SQLite.SQLiteConnection(functionsDB.ConnectionString());

                string query = "";

                try
                {
                    switch (nameTable)
                    {
                        case NameTable.RANGE_RR:

                            struct_RangeRR = (Struct_RangeRR)obj;

                            query = "INSERT INTO " + NameTable.RANGE_RR + " (ID, FreqMin, FreqMax, AngleMin, AngleMax, Mode)" +
                                " VALUES ('" + struct_RangeRR.iID + "', '" + struct_RangeRR.iFreqMin + "', '" + struct_RangeRR.iFreqMax + "', '" + struct_RangeRR.iAngleMin + "', '" + struct_RangeRR.iAngleMax +
                                "', '" + struct_RangeRR.bMode + "')";

                            break;

                        case NameTable.RANGE_RP:

                            struct_RangeRP = (Struct_RangeRP)obj;

                            query = "INSERT INTO " + NameTable.RANGE_RP + " (ID, FreqMin, FreqMax, AngleMin, AngleMax, Mode)" +
                                " VALUES ('" + struct_RangeRP.iID + "', '" + struct_RangeRP.iFreqMin + "', '" + struct_RangeRP.iFreqMax + "', '" + struct_RangeRP.iAngleMin + "', '" + struct_RangeRP.iAngleMax +
                                "', '" + struct_RangeRP.bMode + "')";

                            break;

                        default:
                            break;
                    }

                    sqLiteConnect.Open();
                    sqLiteCommand = new System.Data.SQLite.SQLiteCommand(query);
                    sqLiteCommand.Connection = sqLiteConnect;
                    sqLiteCommand.ExecuteNonQuery();
                    sqLiteConnect.Close();

                    fRead = true;
                }
                catch (System.Exception e)
                {
                    //MessageBox.Show(e.Message);
                    MessageBox.Show(SMessageError.mesErrAddRecord, SMessageError.mesErr, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.ServiceNotification);
                    fRead = false;

                    return fRead;
                }

                // удалить подключение к БД
                sqLiteConnect = null;
                sqLiteCommand = null;
            }
            return fRead;
        }

        /// <summary>
        /// Функция загрузки таблицы в dgv
        /// </summary>      
        public bool LoadTablesRangeRR_RPFromDB(NameTable nameTable, Table table)
        {
            bool fLoad = false;

            byte bTable = 0;
            if (table == Table.Own) bTable = 1;
            if (table == Table.Linked) bTable = 2;

            if (functionsDB.ConnectionString() != "")
            {
                System.Data.SQLite.SQLiteConnection sqLiteConnect;
                System.Data.SQLite.SQLiteCommand sqLiteCommand;
                System.Data.SQLite.SQLiteDataReader sqLiteRead;

                // подключиться к БД
                sqLiteConnect = new System.Data.SQLite.SQLiteConnection(functionsDB.ConnectionString());

                sqLiteConnect.Open();

                switch (nameTable)
                {
                    case NameTable.RANGE_RR:

                        try
                        {
                            sqLiteCommand = new System.Data.SQLite.SQLiteCommand("SELECT * FROM RANGE_RR WHERE Mode =" + Convert.ToString(bTable), sqLiteConnect);
                            sqLiteRead = sqLiteCommand.ExecuteReader();

                            List<RangeSector> lrs = new List<RangeSector>();
                            while (sqLiteRead.Read())
                            {
                                RangeSector temprs = new RangeSector(
                                    Convert.ToInt32(sqLiteRead["FreqMin"]),
                                    Convert.ToInt32(sqLiteRead["FreqMax"]),
                                    Convert.ToInt16(Convert.ToInt32(sqLiteRead["AngleMin"])),
                                    Convert.ToInt16(Convert.ToInt32(sqLiteRead["AngleMax"]))
                                    );

                                lrs.Add(temprs);
                            }

                            if (table == Table.Own)
                            {
                                variableWork.RangeSectorReconOwn = lrs.ToArray();
                            }
                            else
                            {
                                variableWork.RangeSectorReconLinked = lrs.ToArray();
                            }

                            fLoad = true;
                        }
                        catch (Exception)
                        {
                            string strError = "";
                            switch (table)
                            {
                                case Table.Own:
                                    strError = SMessageError.mesErrReadDataOwn;
                                    break;

                                case Table.Linked:
                                    strError = SMessageError.mesErrReadDataLinked;
                                    break;

                                default:
                                    break;
                            }
                            MessageBox.Show(strError, SMessageError.mesErr, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.ServiceNotification);
                            fLoad = false;

                            return fLoad;
                        }
                        finally
                        {
                            sqLiteConnect.Close();
                        }

                        break;

                    case NameTable.RANGE_RP:

                        try
                        {
                            sqLiteCommand = new System.Data.SQLite.SQLiteCommand("SELECT * FROM RANGE_RP WHERE Mode =" + Convert.ToString(bTable), sqLiteConnect);
                            sqLiteRead = sqLiteCommand.ExecuteReader();

                            List<RangeSector> lrs = new List<RangeSector>();
                            while (sqLiteRead.Read())
                            {
                                RangeSector temprs = new RangeSector(
                                    Convert.ToInt32(sqLiteRead["FreqMin"]),
                                    Convert.ToInt32(sqLiteRead["FreqMax"]),
                                    Convert.ToInt16(Convert.ToInt32(sqLiteRead["AngleMin"])),
                                    Convert.ToInt16(Convert.ToInt32(sqLiteRead["AngleMax"]))
                                    );

                                lrs.Add(temprs);
                            }

                            if (table == Table.Own)
                            {
                                variableWork.RangeSectorSupprOwn = lrs.ToArray();
                            }
                            else
                            {
                                variableWork.RangeSectorSupprLinked = lrs.ToArray();
                            }

                            fLoad = true;
                        }
                        catch (Exception)
                        {
                            string strError = "";
                            switch (table)
                            {
                                case Table.Own:
                                    strError = SMessageError.mesErrReadDataOwn;
                                    break;

                                case Table.Linked:
                                    strError = SMessageError.mesErrReadDataLinked;
                                    break;

                                default:
                                    break;
                            }
                            MessageBox.Show(strError, SMessageError.mesErr, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.ServiceNotification);
                            fLoad = false;

                            return fLoad;
                        }
                        finally
                        {
                            sqLiteConnect.Close();
                        }

                        break;
                }

                // удалить подключение к БД
                sqLiteConnect = null;
                sqLiteCommand = null;
                sqLiteRead = null;
            }

            return fLoad;
        }

        /// <summary>
        /// Добавить значения из таблиц Сектора и диапазоны РР и РП в переменные класса VariableWork
        /// </summary>
        /// <param name="nameTable"></param>
        /// <param name="bTable"></param>
        /// <returns></returns>
        public int AddRangesRR_RPToVariableWork(NameTable nameTable, Table table)
        {
            int count = 0;

            byte bTable = 0;
            if (table == Table.Own) bTable = 1;
            if (table == Table.Linked) bTable = 2;

            if (functionsDB.ConnectionString() != "")
            {
                System.Data.SQLite.SQLiteConnection sqLiteConnect;
                System.Data.SQLite.SQLiteCommand sqLiteCommand;
                System.Data.SQLite.SQLiteDataReader sqLiteRead;

                sqLiteConnect = new System.Data.SQLite.SQLiteConnection(functionsDB.ConnectionString());

                try
                {
                    sqLiteConnect.Open();

                    if (bTable == 0)
                    {
                        sqLiteCommand = new System.Data.SQLite.SQLiteCommand("SELECT * FROM " + nameTable.ToString(), sqLiteConnect);
                    }
                    else
                    {
                        sqLiteCommand = new System.Data.SQLite.SQLiteCommand("SELECT * FROM " + nameTable.ToString() + " WHERE Mode=" + bTable, sqLiteConnect);
                    }

                    sqLiteRead = sqLiteCommand.ExecuteReader();

                    List<RangeSector> lrs = new List<RangeSector>();
                    while (sqLiteRead.Read())
                    {
                        RangeSector temprs = new RangeSector(
                            Convert.ToInt32(sqLiteRead["FreqMin"]),
                            Convert.ToInt32(sqLiteRead["FreqMax"]),
                            Convert.ToInt16(Convert.ToInt32(sqLiteRead["AngleMin"])),
                            Convert.ToInt16(Convert.ToInt32(sqLiteRead["AngleMax"]))
                            );

                        lrs.Add(temprs);

                        count++;
                    }

                    if (nameTable == NameTable.RANGE_RR && table == Table.Own)
                    {
                        variableWork.RangeSectorReconOwn = lrs.ToArray();
                    }
                    else if (nameTable == NameTable.RANGE_RR && table == Table.Linked)
                    {
                        variableWork.RangeSectorReconLinked = lrs.ToArray();
                    }
                    else if (nameTable == NameTable.RANGE_RP && table == Table.Own)
                    {
                        variableWork.RangeSectorSupprOwn = lrs.ToArray();
                    }
                    else if (nameTable == NameTable.RANGE_RP && table == Table.Linked)
                    {
                        variableWork.RangeSectorSupprLinked = lrs.ToArray();
                    }
                }
                catch (System.Data.DataException)
                {
                    return count;
                }
                finally
                {
                    sqLiteConnect.Close();
                }

                // удалить подключение к БД
                sqLiteConnect = null;
                sqLiteCommand = null;
            }

            return count;
        }

        
    }
}
