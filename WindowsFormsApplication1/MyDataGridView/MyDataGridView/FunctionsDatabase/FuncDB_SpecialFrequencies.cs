﻿using Protocols;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using VariableDynamic;

namespace MyDataGridView
{
    public class FuncDB_SpecialFrequencies
    {
        FunctionsDB functionsDB;
        VariableWork variableWork;

        Struct_SpecialFrequencies struct_SpecialFrequencies;
        CurVal_SpecialFrequencies curVal_SpecialFrequencies;

        public FuncDB_SpecialFrequencies()
        {
            functionsDB = new FunctionsDB();
            variableWork = new VariableWork();
        }

        /// <summary>
        /// Функция добавления записи в таблицы FREQ_FORBIDDEN, FREQ_KNOWN, FREQ_IMPORTANT в БД
        /// </summary>
        /// <param name="myDGV"></param>
        /// <param name="bTable"></param>
        public bool AddRecord_SpecialFreqToDB(NameTable nameTable, object obj)
        {
            bool fRead = false;

            if (functionsDB.ConnectionString() != "")
            {
                System.Data.SQLite.SQLiteConnection sqLiteConnect;
                System.Data.SQLite.SQLiteCommand sqLiteCommand;

                sqLiteConnect = new System.Data.SQLite.SQLiteConnection(functionsDB.ConnectionString());

                string query = "";

                try
                {
                    switch (nameTable)
                    {
                        case NameTable.FREQ_FORBIDDEN:

                            struct_SpecialFrequencies = (Struct_SpecialFrequencies)obj;

                            query = "INSERT INTO " + NameTable.FREQ_FORBIDDEN + " (ID, FreqMin, FreqMax, Mode)" +
                                " VALUES ('" + struct_SpecialFrequencies.iID + "', '" + struct_SpecialFrequencies.iFreqMin + "', '" + struct_SpecialFrequencies.iFreqMax + "', '" + struct_SpecialFrequencies.bMode + "')";

                            break;

                        case NameTable.FREQ_KNOWN:

                            struct_SpecialFrequencies = (Struct_SpecialFrequencies)obj;

                            query = "INSERT INTO " + NameTable.FREQ_KNOWN + " (ID, FreqMin, FreqMax, Mode)" +
                                " VALUES ('" + struct_SpecialFrequencies.iID + "', '" + struct_SpecialFrequencies.iFreqMin + "', '" + struct_SpecialFrequencies.iFreqMax + "', '" + struct_SpecialFrequencies.bMode + "')";

                            break;

                        case NameTable.FREQ_IMPORTANT:

                            struct_SpecialFrequencies = (Struct_SpecialFrequencies)obj;

                            query = "INSERT INTO " + NameTable.FREQ_IMPORTANT + " (ID, FreqMin, FreqMax, Mode)" +
                                " VALUES ('" + struct_SpecialFrequencies.iID + "', '" + struct_SpecialFrequencies.iFreqMin + "', '" + struct_SpecialFrequencies.iFreqMax + "', '" + struct_SpecialFrequencies.bMode + "')";

                            break;

                        default:
                            break;
                    }

                    sqLiteConnect.Open();
                    sqLiteCommand = new System.Data.SQLite.SQLiteCommand(query);
                    sqLiteCommand.Connection = sqLiteConnect;
                    sqLiteCommand.ExecuteNonQuery();
                    sqLiteConnect.Close();

                    fRead = true;
                }
                catch (System.Exception e)
                {
                    MessageBox.Show(e.Message);
                    MessageBox.Show(SMessageError.mesErrAddRecord, SMessageError.mesErr, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.ServiceNotification);
                    fRead = false;

                    return fRead;
                }

                // удалить подключение к БД
                sqLiteConnect = null;
                sqLiteCommand = null;
            }

            return fRead;
        }

        /// <summary>
        /// Функция изменения записи в таблицах FREQ_FORBIDDEN, FREQ_KNOWN, FREQ_IMPORTANT в базе данных
        /// </summary>
        public bool ChangeRecord_SpecialFreqToDB(NameTable nameTable, object objTable, object objCurVal)
        {
            bool fChange = false;

            if (functionsDB.ConnectionString() != "")
            {
                System.Data.SQLite.SQLiteConnection sqLiteConnect;
                System.Data.SQLite.SQLiteCommand sqLiteCommand;

                sqLiteConnect = new System.Data.SQLite.SQLiteConnection(functionsDB.ConnectionString());

                string query = "";

                try
                {
                    switch (nameTable)
                    {
                        case NameTable.FREQ_FORBIDDEN:

                            struct_SpecialFrequencies = (Struct_SpecialFrequencies)objTable;
                            curVal_SpecialFrequencies = (CurVal_SpecialFrequencies)objCurVal;

                            query = string.Format("UPDATE " + NameTable.FREQ_FORBIDDEN +
                                " SET ID={0}, FreqMin={1}, FreqMax={2}, Mode={3} " +
                                "WHERE ID={4} AND FreqMin={5} AND FreqMax={6} AND Mode={7}",
                                struct_SpecialFrequencies.iID, struct_SpecialFrequencies.iFreqMin, struct_SpecialFrequencies.iFreqMax, struct_SpecialFrequencies.bMode,
                                curVal_SpecialFrequencies.IdDGV, curVal_SpecialFrequencies.FreqMinDGV, curVal_SpecialFrequencies.FreqMaxDGV, curVal_SpecialFrequencies.TableDGV);

                            break;

                        case NameTable.FREQ_KNOWN:

                            struct_SpecialFrequencies = (Struct_SpecialFrequencies)objTable;
                            curVal_SpecialFrequencies = (CurVal_SpecialFrequencies)objCurVal;

                            query = string.Format("UPDATE " + NameTable.FREQ_KNOWN +
                                " SET ID={0}, FreqMin={1}, FreqMax={2}, Mode={3} " +
                                "WHERE ID={4} AND FreqMin={5} AND FreqMax={6} AND Mode={7}",
                                struct_SpecialFrequencies.iID, struct_SpecialFrequencies.iFreqMin, struct_SpecialFrequencies.iFreqMax, struct_SpecialFrequencies.bMode,
                                curVal_SpecialFrequencies.IdDGV, curVal_SpecialFrequencies.FreqMinDGV, curVal_SpecialFrequencies.FreqMaxDGV, curVal_SpecialFrequencies.TableDGV);

                            break;

                        case NameTable.FREQ_IMPORTANT:

                            struct_SpecialFrequencies = (Struct_SpecialFrequencies)objTable;
                            curVal_SpecialFrequencies = (CurVal_SpecialFrequencies)objCurVal;

                            query = string.Format("UPDATE " + NameTable.FREQ_IMPORTANT +
                                " SET ID={0}, FreqMin={1}, FreqMax={2}, Mode={3} " +
                                "WHERE ID={4} AND FreqMin={5} AND FreqMax={6} AND Mode={7}",
                                struct_SpecialFrequencies.iID, struct_SpecialFrequencies.iFreqMin, struct_SpecialFrequencies.iFreqMax, struct_SpecialFrequencies.bMode,
                                curVal_SpecialFrequencies.IdDGV, curVal_SpecialFrequencies.FreqMinDGV, curVal_SpecialFrequencies.FreqMaxDGV, curVal_SpecialFrequencies.TableDGV);

                            break;

                        default:
                            break;
                    }

                    sqLiteConnect.Open();
                    sqLiteCommand = new System.Data.SQLite.SQLiteCommand(query);
                    sqLiteCommand.Connection = sqLiteConnect;
                    sqLiteCommand.ExecuteNonQuery();

                    fChange = true;
                }
                catch (Exception e)
                {
                    MessageBox.Show(SMessageError.mesErrChangeRecord, SMessageError.mesErr, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.ServiceNotification);
                    fChange = false;

                    return fChange;
                }
                finally
                {
                    sqLiteConnect.Close();
                }

                // удалить подключение к БД
                sqLiteConnect = null;
                sqLiteCommand = null;
            }

            return fChange;
        }

        /// <summary>
        /// Добавить значения из таблиц Специальные частоты в переменные класса VariableWork
        /// </summary>
        /// <param name="nameTable"></param>
        /// <param name="bTable"></param>
        /// <returns></returns>
        public int AddSpecialFrequenciesToVariableWork(NameTable nameTable, Table table)
        {
            int count = 0;

            byte bTable = 0;
            if (table == Table.Own) bTable = 1;
            if (table == Table.Linked) bTable = 2;

            if (functionsDB.ConnectionString() != "")
            {
                System.Data.SQLite.SQLiteConnection sqLiteConnect;
                System.Data.SQLite.SQLiteCommand sqLiteCommand;
                System.Data.SQLite.SQLiteDataReader sqLiteRead;

                sqLiteConnect = new System.Data.SQLite.SQLiteConnection(functionsDB.ConnectionString());

                try
                {
                    sqLiteConnect.Open();

                    if (bTable == 0)
                    {
                        sqLiteCommand = new System.Data.SQLite.SQLiteCommand("SELECT * FROM " + nameTable.ToString(), sqLiteConnect);
                    }
                    else
                    {
                        sqLiteCommand = new System.Data.SQLite.SQLiteCommand("SELECT * FROM " + nameTable.ToString() + " WHERE Mode=" + bTable, sqLiteConnect);
                    }

                    sqLiteRead = sqLiteCommand.ExecuteReader();

                    List<FrequencyRange> lfr = new List<FrequencyRange>();
                    while (sqLiteRead.Read())
                    {
                        FrequencyRange tempfr = new FrequencyRange(
                            Convert.ToInt32(sqLiteRead["FreqMin"]),
                            Convert.ToInt32(sqLiteRead["FreqMax"])
                            );

                        lfr.Add(tempfr);

                        count++;
                    }

                    if (nameTable == NameTable.FREQ_FORBIDDEN && table == Table.Own)
                    {
                        variableWork.FrequencyRangeForbiddenOwn = lfr.ToArray();
                    }
                    else if (nameTable == NameTable.FREQ_FORBIDDEN && table == Table.Linked)
                    {
                        variableWork.FrequencyRangeForbiddenLinked = lfr.ToArray();
                    }
                    else if (nameTable == NameTable.FREQ_KNOWN && table == Table.Own)
                    {
                        variableWork.FrequencyRangeKnownOwn = lfr.ToArray();
                    }
                    else if (nameTable == NameTable.FREQ_KNOWN && table == Table.Linked)
                    {
                        variableWork.FrequencyRangeKnownLinked = lfr.ToArray();
                    }
                    else if (nameTable == NameTable.FREQ_IMPORTANT && table == Table.Own)
                    {
                        variableWork.FrequencyRangeImportantOwn = lfr.ToArray();
                    }
                    else if (nameTable == NameTable.FREQ_IMPORTANT && table == Table.Linked)
                    {
                        variableWork.FrequencyRangeImportantLinked = lfr.ToArray();
                    }
                }
                catch (System.Data.DataException)
                {
                    return count;
                }
                finally
                {
                    sqLiteConnect.Close();
                }

                // удалить подключение к БД
                sqLiteConnect = null;
                sqLiteCommand = null;
            }

            return count;
        }

        /// <summary>
        /// Функция загрузки таблиц Специальных частот в dgv
        /// </summary>      
        public bool LoadTableSpecFreqFromDB(NameTable nameTable, Table table)
        {
            bool fLoad = false;

            byte bTable = 0;
            if (table == Table.Own) bTable = 1;
            if (table == Table.Linked) bTable = 2;

            if (functionsDB.ConnectionString() != "")
            {
                System.Data.SQLite.SQLiteConnection sqLiteConnect;
                System.Data.SQLite.SQLiteCommand sqLiteCommand;
                System.Data.SQLite.SQLiteDataReader sqLiteRead;

                // подключиться к БД
                sqLiteConnect = new System.Data.SQLite.SQLiteConnection(functionsDB.ConnectionString());

                sqLiteConnect.Open();

                switch (nameTable)
                {
                    case NameTable.FREQ_FORBIDDEN:

                        try
                        {
                            sqLiteCommand = new System.Data.SQLite.SQLiteCommand("SELECT * FROM FREQ_FORBIDDEN WHERE Mode =" + Convert.ToString(bTable), sqLiteConnect);
                            sqLiteRead = sqLiteCommand.ExecuteReader();

                            List<FrequencyRange> lfr = new List<FrequencyRange>();
                            while (sqLiteRead.Read())
                            {
                                FrequencyRange tempfr = new FrequencyRange(
                                    Convert.ToInt32(sqLiteRead["FreqMin"]),
                                    Convert.ToInt32(sqLiteRead["FreqMax"])
                                    );

                                lfr.Add(tempfr);
                            }

                            if (table == Table.Own)
                            {
                                variableWork.FrequencyRangeForbiddenOwn = lfr.ToArray();
                            }
                            else
                            {
                                variableWork.FrequencyRangeForbiddenLinked = lfr.ToArray();
                            }

                            fLoad = true;
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(SMessageError.mesErrReadDataTable, SMessageError.mesErr, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.ServiceNotification);
                            fLoad = false;

                            return fLoad;
                        }
                        finally
                        {
                            sqLiteConnect.Close();
                        }

                        break;

                    case NameTable.FREQ_KNOWN:

                        try
                        {
                            sqLiteCommand = new System.Data.SQLite.SQLiteCommand("SELECT * FROM FREQ_KNOWN WHERE Mode =" + Convert.ToString(bTable), sqLiteConnect);
                            sqLiteRead = sqLiteCommand.ExecuteReader();

                            List<FrequencyRange> lfr = new List<FrequencyRange>();
                            while (sqLiteRead.Read())
                            {
                                FrequencyRange tempfr = new FrequencyRange(
                                    Convert.ToInt32(sqLiteRead["FreqMin"]),
                                    Convert.ToInt32(sqLiteRead["FreqMax"])
                                    );

                                lfr.Add(tempfr);
                            }

                            if (table == Table.Own)
                            {
                                variableWork.FrequencyRangeKnownOwn = lfr.ToArray();
                            }
                            else
                            {
                                variableWork.FrequencyRangeKnownLinked = lfr.ToArray();
                            }

                            fLoad = true;
                        }
                        catch (Exception)
                        {
                            MessageBox.Show(SMessageError.mesErrReadDataTable, SMessageError.mesErr, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.ServiceNotification);
                            fLoad = false;

                            return fLoad;
                        }
                        finally
                        {
                            sqLiteConnect.Close();
                        }

                        break;

                    case NameTable.FREQ_IMPORTANT:

                        try
                        {
                            sqLiteCommand = new System.Data.SQLite.SQLiteCommand("SELECT * FROM FREQ_IMPORTANT WHERE Mode =" + Convert.ToString(bTable), sqLiteConnect);
                            sqLiteRead = sqLiteCommand.ExecuteReader();

                            List<FrequencyRange> lfr = new List<FrequencyRange>();
                            while (sqLiteRead.Read())
                            {
                                FrequencyRange tempfr = new FrequencyRange(
                                    Convert.ToInt32(sqLiteRead["FreqMin"]),
                                    Convert.ToInt32(sqLiteRead["FreqMax"])
                                    );

                                lfr.Add(tempfr);
                            }

                            if (table == Table.Own)
                            {
                                variableWork.FrequencyRangeImportantOwn = lfr.ToArray();
                            }
                            else
                            {
                                variableWork.FrequencyRangeImportantLinked = lfr.ToArray();
                            }

                            fLoad = true;
                        }
                        catch (Exception)
                        {
                            MessageBox.Show(SMessageError.mesErrReadDataTable, SMessageError.mesErr, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.ServiceNotification);
                            fLoad = false;

                            return fLoad;
                        }
                        finally
                        {
                            sqLiteConnect.Close();
                        }

                        break;
                }

                // удалить подключение к БД
                sqLiteConnect = null;
                sqLiteCommand = null;
                sqLiteRead = null;
            }

            return fLoad;
        }
    }
}
