﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using USR_DLL;
using VariableDynamic;

namespace MyDataGridView
{
    public class FuncDB_IRI_FRCh_RP
    {
        FunctionsDB functionsDB;
        VariableWork variableWork;
        TSupprFWS SupprFWS;
        CurVal_IRI_FRCh_RP curVal_IRI_FRCh_RP;

        public FuncDB_IRI_FRCh_RP()
        {
            functionsDB = new FunctionsDB();
            variableWork = new VariableWork();
        }

        /// <summary>
        /// Функция добавления записи в таблицу IRI_FRCh_RP в БД
        /// </summary>
        /// <param name="myDGV"></param>
        /// <param name="bTable"></param>
        public bool AddRecord_IRI_FRCh_RPToDB(NameTable nameTable, object obj, Table table)
        {
            bool fRead = false;

            byte bTable = 0;
            if (table == Table.Own) bTable = 1;
            if (table == Table.Linked) bTable = 2;

            if (functionsDB.ConnectionString() != "")
            {
                System.Data.SQLite.SQLiteConnection sqLiteConnect;
                System.Data.SQLite.SQLiteCommand sqLiteCommand;

                sqLiteConnect = new System.Data.SQLite.SQLiteConnection(functionsDB.ConnectionString());

                try
                {
                    SupprFWS = (TSupprFWS)obj;

                    string query = "INSERT INTO " + NameTable.IRI_FRCh_RP + " (ID, Freq, Q, Letter, U, Priority, Modulation, Deviation, Manipulation, Duration, Mode)" +
                        " VALUES ('" + SupprFWS.iID + "','" + SupprFWS.iFreq + "', '" + SupprFWS.sBearing + "', '" + SupprFWS.bLetter + "', '" + SupprFWS.sLevel +
                        "', '" + SupprFWS.bPrior + "', '" + SupprFWS.bModulation + "', '" + SupprFWS.bDeviation + "', '" + SupprFWS.bManipulation + "', '" + SupprFWS.bDuration + "', '" + bTable + "')";

                    sqLiteConnect.Open();
                    sqLiteCommand = new System.Data.SQLite.SQLiteCommand(query);
                    sqLiteCommand.Connection = sqLiteConnect;
                    sqLiteCommand.ExecuteNonQuery();
                    sqLiteConnect.Close();

                    fRead = true;
                }
                catch (System.Exception e)
                {
                    MessageBox.Show(e.Message);
                    MessageBox.Show(SMessageError.mesErrAddRecord, SMessageError.mesErr, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.ServiceNotification);
                    fRead = false;

                    return fRead;
                }

                // удалить подключение к БД
                sqLiteConnect = null;
                sqLiteCommand = null;
            }

            return fRead;
        }

        /// <summary>
        /// Функция загрузки таблицы IRI_FRCh_RP в variableWork
        /// </summary>      
        public bool LoadTableIRI_FRCh_RPFromDBToVW(NameTable nameTable, Table table)
        {
            bool fLoad = false;

            byte bTable = 0;
            if (table == Table.Own) bTable = 1;
            if (table == Table.Linked) bTable = 2;

            if (functionsDB.ConnectionString() != "")
            {
                System.Data.SQLite.SQLiteConnection sqLiteConnect;
                System.Data.SQLite.SQLiteCommand sqLiteCommand;
                System.Data.SQLite.SQLiteDataReader sqLiteRead;

                // подключиться к БД
                sqLiteConnect = new System.Data.SQLite.SQLiteConnection(functionsDB.ConnectionString());

                sqLiteConnect.Open();

                try
                {
                    sqLiteCommand = new System.Data.SQLite.SQLiteCommand("SELECT * FROM IRI_FRCh_RP WHERE Mode =" + Convert.ToString(bTable), sqLiteConnect);
                    sqLiteRead = sqLiteCommand.ExecuteReader();

                    List<TSupprFWS> lSupprFWS = new List<TSupprFWS>();
                    while (sqLiteRead.Read())
                    {
                        TSupprFWS temp = new TSupprFWS();
                        
                        temp.iID = Convert.ToInt32(sqLiteRead["ID"]);
                        temp.iFreq = Convert.ToInt32(sqLiteRead["Freq"]);
                        temp.sBearing = Convert.ToInt16(sqLiteRead["Q"]);
                        temp.bLetter = Convert.ToByte(sqLiteRead["Letter"]);
                        temp.sLevel = Convert.ToInt16(sqLiteRead["U"]);
                        temp.bPrior = Convert.ToByte(sqLiteRead["Priority"]);

                        temp.bModulation = Convert.ToByte(sqLiteRead["Modulation"]);
                        temp.bDeviation = Convert.ToByte(sqLiteRead["Deviation"]);
                        temp.bManipulation = Convert.ToByte(sqLiteRead["Manipulation"]);
                        temp.bDuration = Convert.ToByte(sqLiteRead["Duration"]);

                        lSupprFWS.Add(temp);
                    }

                    //IEnumerable<TSupprFWS> sortPriority = lSupprFWS.OrderByDescending(x => x.bPrior); // сортировка по приоритету
                    //IEnumerable<TSupprFWS> sortLetter = sortPriority.OrderBy(x => x.bLetter); // сортировка по литере

                    if (table == Table.Own)
                    {
                        variableWork.SupprFWS_Own = lSupprFWS.ToArray();// sortLetter.ToArray();
                    }
                    else
                    {
                        variableWork.SupprFWS_Linked = lSupprFWS.ToArray();// sortLetter.ToArray();
                    }

                    fLoad = true;
                }
                catch (Exception)
                {
                    string strError = "";
                    switch (table)
                    {
                        case Table.Own:
                            strError = SMessageError.mesErrReadDataOwn;
                            break;

                        case Table.Linked:
                            strError = SMessageError.mesErrReadDataLinked;
                            break;

                        default:
                            break;
                    }
                    MessageBox.Show(strError, SMessageError.mesErr, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.ServiceNotification);
                    fLoad = false;

                    return fLoad;
                }
                finally
                {
                    sqLiteConnect.Close();
                }

                // удалить подключение к БД
                sqLiteConnect = null;
                sqLiteCommand = null;
                sqLiteRead = null;
            }

            return fLoad;
        }

        /// <summary>
        /// Функция установки минимального значения ID
        /// </summary>      
        public void FindMinIDIRI_FRCh_RP(NameTable nameTable)
        {
            if (functionsDB.ConnectionString() != "")
            {
                System.Data.SQLite.SQLiteConnection sqLiteConnect;
                System.Data.SQLite.SQLiteCommand sqLiteCommand;
                System.Data.SQLite.SQLiteDataReader sqLiteRead;

                // подключиться к БД
                sqLiteConnect = new System.Data.SQLite.SQLiteConnection(functionsDB.ConnectionString());

                sqLiteConnect.Open();

                try
                {
                    sqLiteCommand = new System.Data.SQLite.SQLiteCommand("SELECT * FROM IRI_FRCh_RP", sqLiteConnect);
                    sqLiteRead = sqLiteCommand.ExecuteReader();

                    List<TSupprFWS> lSupprFWS = new List<TSupprFWS>();
                    while (sqLiteRead.Read())
                    {
                        TSupprFWS temp = new TSupprFWS();

                        temp.iID = Convert.ToInt32(sqLiteRead["ID"]);
                        temp.iFreq = Convert.ToInt32(sqLiteRead["Freq"]);
                        temp.sBearing = Convert.ToInt16(sqLiteRead["Q"]);
                        temp.bLetter = Convert.ToByte(sqLiteRead["Letter"]);
                        temp.sLevel = Convert.ToInt16(sqLiteRead["U"]);
                        temp.bPrior = Convert.ToByte(sqLiteRead["Priority"]);

                        temp.bModulation = Convert.ToByte(sqLiteRead["Modulation"]);
                        temp.bDeviation = Convert.ToByte(sqLiteRead["Deviation"]);
                        temp.bManipulation = Convert.ToByte(sqLiteRead["Manipulation"]);
                        temp.bDuration = Convert.ToByte(sqLiteRead["Duration"]);

                        lSupprFWS.Add(temp);
                    }

                    int minInd = lSupprFWS.Min(x => x.iID); // определить min ID
                    TSupprFWS.SetIdFWS(minInd);
                }
                catch 
                {
                }
                finally
                {
                    sqLiteConnect.Close();
                }

                // удалить подключение к БД
                sqLiteConnect = null;
                sqLiteCommand = null;
                sqLiteRead = null;
            }
        }
    }
}
