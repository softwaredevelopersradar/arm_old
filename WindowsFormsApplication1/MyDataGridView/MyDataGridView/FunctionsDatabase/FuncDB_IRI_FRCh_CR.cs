﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using USR_DLL;
using VariableDynamic;

namespace MyDataGridView
{
    public class FuncDB_IRI_FRCh_CR
    {
        Functions functions;
        FunctionsDB functionsDB;
        VariableWork variableWork;
        TDistribFWS DistribFWS;
        TDistribFWS current;

        public FuncDB_IRI_FRCh_CR()
        {
            functions = new Functions();
            functionsDB = new FunctionsDB();
            variableWork = new VariableWork();
        }

        /// <summary>
        /// Добавить значения из таблицы ИРИ ФРЧ на ЦР в переменные класса VariableWork
        /// </summary>
        /// <param name="nameTable"></param>
        /// <param name="bTable"></param>
        /// <returns></returns>
        public void AddIRI_FRCh_CRToVariableWork(List<TDistribFWS> lDistribFWS)
        {
            try
            {
                if (lDistribFWS != null)
                    variableWork.DistribFWS = lDistribFWS.ToArray();
                
            }
            catch { return; }
        }

        /// <summary>
        /// Функция загрузки таблицы IRI_FRCh_CR в dgv
        /// </summary>      
        public List<TDistribFWS> LoadTableIRI_FRCh_CRFromDB(NameTable nameTable)
        {
            List<TDistribFWS> lDistribFWS = new List<TDistribFWS>();

            if (functionsDB.ConnectionString() != "")
            {
                System.Data.SQLite.SQLiteConnection sqLiteConnect;
                System.Data.SQLite.SQLiteCommand sqLiteCommand;
                System.Data.SQLite.SQLiteDataReader sqLiteRead;

                // подключиться к БД
                sqLiteConnect = new System.Data.SQLite.SQLiteConnection(functionsDB.ConnectionString());

                sqLiteConnect.Open();
                
                try
                {
                    // выбрать частоты текущей литеры по уменьшению приоритета
                    sqLiteCommand = new System.Data.SQLite.SQLiteCommand("SELECT * FROM " + nameTable.ToString(), sqLiteConnect);
                    sqLiteRead = sqLiteCommand.ExecuteReader();

                   
                    while (sqLiteRead.Read())
                    {
                        TDistribFWS tempdistr = new TDistribFWS();
                        tempdistr.iID = Convert.ToInt32(sqLiteRead["ID"]);
                        tempdistr.iFreq = Convert.ToInt32(sqLiteRead["Freq"]);
                        tempdistr.sBearing1 = Convert.ToInt16(sqLiteRead["Q1"]);
                        tempdistr.sBearing2 = Convert.ToInt16(sqLiteRead["Q2"]);
                        tempdistr.sLevel = Convert.ToInt16(sqLiteRead["U"]);
                        tempdistr.iDFreq = Convert.ToInt32(sqLiteRead["dFreq"]);
                        tempdistr.iCKO = Convert.ToInt32(sqLiteRead["CKO"]);
                        tempdistr.dLatitude = Convert.ToDouble(sqLiteRead["Latitude"]);
                        tempdistr.dLongitude = Convert.ToDouble(sqLiteRead["Longitude"]);
                        tempdistr.bView = Convert.ToByte(sqLiteRead["View"]);
                        tempdistr.iSP_RR = Convert.ToInt32(sqLiteRead["SP_RR"]);
                        tempdistr.iSP_RP = Convert.ToInt32(sqLiteRead["SP_RP"]);

                        lDistribFWS.Add(tempdistr);
                    }

                    //variableWork.DistribFWS = ldistr.ToArray();
                }
                catch (Exception)
                {
                    //MessageBox.Show(ex.Message);
                    MessageBox.Show(SMessageError.mesErrReadDataTable, SMessageError.mesErr, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.ServiceNotification);
                    return null;
                }
                finally
                {
                    sqLiteConnect.Close();
                }

                // удалить подключение к БД
                sqLiteConnect = null;
                sqLiteCommand = null;
                sqLiteRead = null;
            }

            return lDistribFWS;
        }

        /// <summary>
        /// Функция добавления записи в таблицу IRI_FRCh_RP в БД
        /// </summary>
        /// <param name="myDGV"></param>
        /// <param name="bTable"></param>
        public bool AddRecord_IRI_FRCh_CRToDB(NameTable nameTable, object obj)
        {
            bool fRead = false;

            if (functionsDB.ConnectionString() != "")
            {
                System.Data.SQLite.SQLiteConnection sqLiteConnect;
                System.Data.SQLite.SQLiteCommand sqLiteCommand;

                sqLiteConnect = new System.Data.SQLite.SQLiteConnection(functionsDB.ConnectionString());

                try
                {
                    DistribFWS = (TDistribFWS)obj;

                    string query = "INSERT INTO " + nameTable.ToString() + " (ID, Freq, Q1, Q2, U, dFreq, CKO, Latitude, Longitude, View, SP_RR, SP_RP)" +
                        " VALUES ('" + DistribFWS.iID + "','" + DistribFWS.iFreq + "', '" + DistribFWS.sBearing1 + "', '" + DistribFWS.sBearing2 + "', '" + DistribFWS.sLevel + "', '" + DistribFWS.iDFreq +
                        "', '" + DistribFWS.iCKO + "', '" + DistribFWS.dLatitude.ToString("0.000000") + "', '" + DistribFWS.dLongitude.ToString("0.000000") + "', '" + DistribFWS.bView + "', '" + DistribFWS.iSP_RR + "', '" + DistribFWS.iSP_RP + "')";

                    sqLiteConnect.Open();
                    sqLiteCommand = new System.Data.SQLite.SQLiteCommand(query);
                    sqLiteCommand.Connection = sqLiteConnect;
                    sqLiteCommand.ExecuteNonQuery();
                    sqLiteConnect.Close();

                    fRead = true;
                }
                catch (System.Exception)
                {
                    //MessageBox.Show(e.Message);
                    MessageBox.Show(SMessageError.mesErrAddRecord, SMessageError.mesErr, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.ServiceNotification);
                    fRead = false;

                    return fRead;
                }

                // удалить подключение к БД
                sqLiteConnect = null;
                sqLiteCommand = null;
            }

            return fRead;
        }

        /// <summary>
        /// Удалить одну запись из таблицы базы данных
        /// </summary>
        /// <param name="iNum"></param>
        public bool DeleteOneRecord_IRI_FRCh_CRfromDB(int iID, NameTable nameTable)
        {
            bool fDelete = false;

            if (functionsDB.ConnectionString() != "")
            {
                string name = nameTable.ToString();

                System.Data.SQLite.SQLiteConnection sqLiteConnect;
                System.Data.SQLite.SQLiteCommand sqLiteCommand;

                // подключиться к БД
                sqLiteConnect = new System.Data.SQLite.SQLiteConnection(functionsDB.ConnectionString());

                try
                {
                    sqLiteConnect.Open();

                    sqLiteCommand = new System.Data.SQLite.SQLiteCommand("DELETE FROM " + name + " WHERE ID=" + iID, sqLiteConnect);

                    sqLiteCommand.ExecuteNonQuery();

                    fDelete = true;
                }
                catch (System.Data.DataException)
                {
                    MessageBox.Show(SMessageError.mesErrDeleteRecord, SMessageError.mesErr, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.ServiceNotification);
                    fDelete = false;

                    return fDelete;
                }
                finally
                {
                    sqLiteConnect.Close();
                }

                // удалить подключение к БД
                sqLiteConnect = null;
                sqLiteCommand = null;
            }

            return fDelete;
        }

        /// <summary>
        /// Функция изменения записи в таблице IRI_FRCh_RP в базе данных
        /// </summary>
        public bool ChangeRecord_IRI_FRCh_CRToDB(NameTable nameTable, object objTable, object objCurVal)
        {
            bool fChange = false;

            if (functionsDB.ConnectionString() != "")
            {
                System.Data.SQLite.SQLiteConnection sqLiteConnect;
                System.Data.SQLite.SQLiteCommand sqLiteCommand;

                sqLiteConnect = new System.Data.SQLite.SQLiteConnection(functionsDB.ConnectionString());

                try
                {
                    DistribFWS = (TDistribFWS)objTable;
                    current = (TDistribFWS)objCurVal;

                    string query = string.Format("UPDATE " + nameTable.ToString() +
                    " SET ID={0}, SP_RR={1}, SP_RP={2} WHERE ID={3} AND SP_RR={4} AND SP_RP={5}",
                    DistribFWS.iID, DistribFWS.iSP_RR, DistribFWS.iSP_RP, current.iID, current.iSP_RR, current.iSP_RP);

                    sqLiteConnect.Open();
                    sqLiteCommand = new System.Data.SQLite.SQLiteCommand(query);
                    sqLiteCommand.Connection = sqLiteConnect;
                    sqLiteCommand.ExecuteNonQuery();

                    fChange = true;
                }
                catch (Exception)
                {
                    MessageBox.Show(SMessageError.mesErrChangeRecord, SMessageError.mesErr, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.ServiceNotification);
                    fChange = false;

                    return fChange;
                }
                finally
                {
                    sqLiteConnect.Close();
                }

                // удалить подключение к БД
                sqLiteConnect = null;
                sqLiteCommand = null;
            }

            return fChange;
        }

        /// <summary>
        /// Функция установки минимального значения ID
        /// </summary>      
        public void FindMinIDIRI_FRCh_CR(NameTable nameTable)
        {
            if (functionsDB.ConnectionString() != "")
            {
                System.Data.SQLite.SQLiteConnection sqLiteConnect;
                System.Data.SQLite.SQLiteCommand sqLiteCommand;
                System.Data.SQLite.SQLiteDataReader sqLiteRead;

                // подключиться к БД
                sqLiteConnect = new System.Data.SQLite.SQLiteConnection(functionsDB.ConnectionString());

                sqLiteConnect.Open();

                try
                {
                    sqLiteCommand = new System.Data.SQLite.SQLiteCommand("SELECT * FROM " + nameTable.ToString(), sqLiteConnect);
                    sqLiteRead = sqLiteCommand.ExecuteReader();

                    List<TDistribFWS> lDistribFWS = new List<TDistribFWS>();
                    while (sqLiteRead.Read())
                    {
                        TDistribFWS tempdistr = new TDistribFWS();
                        tempdistr.iID = Convert.ToInt32(sqLiteRead["ID"]);
                        tempdistr.iFreq = Convert.ToInt32(sqLiteRead["Freq"]);
                        tempdistr.sBearing1 = Convert.ToInt16(sqLiteRead["Q1"]);
                        tempdistr.sBearing2 = Convert.ToInt16(sqLiteRead["Q2"]);
                        tempdistr.sLevel = Convert.ToInt16(sqLiteRead["U"]);
                        tempdistr.iDFreq = Convert.ToInt32(sqLiteRead["dFreq"]);
                        tempdistr.iCKO = Convert.ToInt32(sqLiteRead["CKO"]);
                        tempdistr.dLatitude = Convert.ToDouble(sqLiteRead["Latitude"]);
                        tempdistr.dLongitude = Convert.ToDouble(sqLiteRead["Longitude"]);
                        tempdistr.bView = Convert.ToByte(sqLiteRead["View"]);
                        tempdistr.iSP_RR = Convert.ToInt32(sqLiteRead["SP_RR"]);
                        tempdistr.iSP_RP = Convert.ToInt32(sqLiteRead["SP_RP"]);

                        lDistribFWS.Add(tempdistr);
                    }

                    int minInd = lDistribFWS.Min(x => x.iID); // определить min ID
                    if (minInd > 0) TDistribFWS.SetIdFWS(0);
                    else TDistribFWS.SetIdFWS(minInd);
                }
                catch
                {
                }
                finally
                {
                    sqLiteConnect.Close();
                }

                // удалить подключение к БД
                sqLiteConnect = null;
                sqLiteCommand = null;
                sqLiteRead = null;
            }
        }
    }
}
