﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using USR_DLL;
using VariableDynamic;

namespace MyDataGridView
{
    public class FuncDB_ADSB
    {
        FunctionsDB functionsDB;
        Struct_ADSB_Receiver struct_ADSB_Receiver;

        public FuncDB_ADSB()
        {
            functionsDB = new FunctionsDB();
        }

        /// <summary>
        /// Функция добавления записи в таблицу ADSB в БД
        /// </summary>
        /// <param name="myDGV"></param>
        /// <param name="bTable"></param>
        public bool AddRecordsADSBToDB(NameTable nameTable, object obj)
        {
            bool fRead = false;

            if (functionsDB.ConnectionString() != "")
            {
                System.Data.SQLite.SQLiteConnection sqLiteConnect;
                System.Data.SQLite.SQLiteCommand sqLiteCommand;

                sqLiteConnect = new System.Data.SQLite.SQLiteConnection(functionsDB.ConnectionString());

                string query = "";

                Struct_ADSB_Receiver[] struct_ADSB_Receiver = (Struct_ADSB_Receiver[])obj;

                try
                {
                    sqLiteConnect.Open();

                    for (int i = 0; i < struct_ADSB_Receiver.Length; i++)
                    {
                        query = "INSERT INTO " + nameTable.ToString() + " (ID, ICAO, Latitude, Longitude, Altitude, DateTime)" +
                            " VALUES ('" + struct_ADSB_Receiver[i].iID + "', '" + struct_ADSB_Receiver[i].sICAO + "', '" + struct_ADSB_Receiver[i].sLatitude + "', '" + struct_ADSB_Receiver[i].sLongitude + "', '" + struct_ADSB_Receiver[i].sAltitude + "', '" + struct_ADSB_Receiver[i].sDatetime + "')";

                        sqLiteCommand = new System.Data.SQLite.SQLiteCommand(query);
                        sqLiteCommand.Connection = sqLiteConnect;
                        sqLiteCommand.ExecuteNonQuery();
                    }

                    sqLiteConnect.Close();

                    fRead = true;
                }
                catch (System.Exception e)
                {
                    MessageBox.Show(SMessageError.mesErrAddRecord, SMessageError.mesErr, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.ServiceNotification);
                    fRead = false;

                    return fRead;
                }

                // удалить подключение к БД
                sqLiteConnect = null;
                sqLiteCommand = null;
            }

            return fRead;

            //bool fRead = false;

            //if (functionsDB.ConnectionString() != "")
            //{
            //    System.Data.SQLite.SQLiteConnection sqLiteConnect;
            //    System.Data.SQLite.SQLiteCommand sqLiteCommand;

            //    sqLiteConnect = new System.Data.SQLite.SQLiteConnection(functionsDB.ConnectionString());

            //    string query = "";

            //    struct_ADSB_Receiver = (Struct_ADSB_Receiver)obj;

            //    try
            //    {
            //        sqLiteConnect.Open();

            //        query = "INSERT INTO " + nameTable.ToString() + " (ID, ICAO, Latitude, Longitude, Altitude, DateTime)" +
            //            " VALUES ('" + struct_ADSB_Receiver.iID + "', '" + struct_ADSB_Receiver.sICAO + "', '" + struct_ADSB_Receiver.sLatitude + "', '" + struct_ADSB_Receiver.sLongitude + "', '" + struct_ADSB_Receiver.sAltitude + "', '" + struct_ADSB_Receiver.sDatetime + "')";

            //        sqLiteCommand = new System.Data.SQLite.SQLiteCommand(query);
            //        sqLiteCommand.Connection = sqLiteConnect;
            //        sqLiteCommand.ExecuteNonQuery();

            //        sqLiteConnect.Close();

            //        fRead = true;
            //    }
            //    catch (System.Exception e)
            //    {
            //        MessageBox.Show(SMessageError.mesErrAddRecord, SMessageError.mesErr, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.ServiceNotification);
            //        fRead = false;

            //        return fRead;
            //    }

            //    // удалить подключение к БД
            //    sqLiteConnect = null;
            //    sqLiteCommand = null;
            //}

            //return fRead;
        }

        /// <summary>
        /// Функция загрузки таблицы в dgv
        /// </summary>      
        public bool LoadTableADSBFromDB(DataGridView dgv, NameTable nameTable)
        {
            bool fLoad = false;

            if (functionsDB.ConnectionString() != "")
            {
                System.Data.SQLite.SQLiteConnection sqLiteConnect;
                System.Data.SQLite.SQLiteCommand sqLiteCommand;
                System.Data.SQLite.SQLiteDataReader sqLiteRead;

                int j = 0;

                // подключиться к БД
                sqLiteConnect = new System.Data.SQLite.SQLiteConnection(functionsDB.ConnectionString());

                sqLiteConnect.Open();

                try
                {
                    sqLiteCommand = new System.Data.SQLite.SQLiteCommand("SELECT * FROM ADSB_RECEIVER", sqLiteConnect);
                    sqLiteRead = sqLiteCommand.ExecuteReader();

                    while (sqLiteRead.Read())
                    {
                        // добавить строку в таблицу
                        if (j >= dgv.RowCount)
                            dgv.Rows.Add(null, null, null, null, null, null);

                        // отобразить данные в таблице
                        dgv.Rows[j].Cells[0].Value = Convert.ToString(sqLiteRead["ID"]);
                        dgv.Rows[j].Cells[1].Value = sqLiteRead["ICAO"];
                        dgv.Rows[j].Cells[2].Value = sqLiteRead["Latitude"];
                        dgv.Rows[j].Cells[3].Value = sqLiteRead["Longitude"];
                        dgv.Rows[j].Cells[4].Value = sqLiteRead["Altitude"];
                        dgv.Rows[j].Cells[5].Value = sqLiteRead["DateTime"];

                        j++;
                    }

                    fLoad = true;
                }
                catch (Exception)
                {
                    MessageBox.Show(SMessageError.mesErrReadDataTable, SMessageError.mesErr, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.ServiceNotification);
                    fLoad = false;

                    return fLoad;
                }
                finally
                {
                    sqLiteConnect.Close();
                }

                // удалить подключение к БД
                sqLiteConnect = null;
                sqLiteCommand = null;
                sqLiteRead = null;
            }

            return fLoad;
        }
    }
}
