﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using USR_DLL;
using VariableDynamic;

namespace MyDataGridView
{
    public class FuncDB_IRI_PPRCh
    {
        Functions functions;
        FunctionsDB functionsDB;
        VariableWork variableWork;
        TDistribFHSS[] DistribFHSS;

        public FuncDB_IRI_PPRCh()
        {
            functions = new Functions();
            functionsDB = new FunctionsDB();
            variableWork = new VariableWork();
        }

        /// <summary>
        /// Добавить значения из таблицы ИРИ ППРЧ в переменные класса VariableWork
        /// </summary>
        /// <param name="nameTable"></param>
        /// <param name="bTable"></param>
        /// <returns></returns>
        public void AddIRI_PPRChToVariableWork(List<TDistribFHSS> lDistribFHSS)
        {
            try
            {
                if (lDistribFHSS != null)
                    variableWork.DistribFHSS = lDistribFHSS.ToArray();
                
            }
            catch { return; }
        }

        /// <summary>
        /// Функция загрузки таблицы IRI_PPRCh в variableWork
        /// </summary>      
        public bool LoadTableIRI_PPRChFromDB(NameTable nameTable)
        {
            bool fLoad = false;

            if (functionsDB.ConnectionString() != "")
            {
                System.Data.SQLite.SQLiteConnection sqLiteConnect;
                System.Data.SQLite.SQLiteCommand sqLiteCommand, sqLiteCommandPPRCh2, sqLiteCommandPPRCh3;
                System.Data.SQLite.SQLiteDataReader sqLiteRead, sqLiteReadPPRCh2, sqLiteReadPPRCh3;

                // подключиться к БД
                sqLiteConnect = new System.Data.SQLite.SQLiteConnection(functionsDB.ConnectionString());
                // открыть БД
                sqLiteConnect.Open();

                try
                {
                    sqLiteCommand = new System.Data.SQLite.SQLiteCommand("SELECT * FROM " + nameTable.ToString(), sqLiteConnect);
                    sqLiteRead = sqLiteCommand.ExecuteReader();

                    List<TDistribFHSS> lDistribFHSS = new List<TDistribFHSS>();
                    
                    while (sqLiteRead.Read())
                    {
                        TDistribFHSS temp = new TDistribFHSS();
                        temp.iID = Convert.ToInt32(sqLiteRead["ID"]);
                        temp.iFreqMin = Convert.ToInt32(sqLiteRead["FreqMin"]);
                        temp.iFreqMax = Convert.ToInt32(sqLiteRead["FreqMax"]);
                        temp.iStep = Convert.ToInt32(sqLiteRead["Step"]);
                        temp.iDFreq = Convert.ToInt32(sqLiteRead["dFreq"]);
                        temp.iDuratImp = Convert.ToInt32(sqLiteRead["DuratImp"]);
                        temp.iCountIRI = Convert.ToInt32(sqLiteRead["CountIRI"]);
                        temp.sTime = Convert.ToString(sqLiteRead["Time"]);
                        temp.iSP_RR = Convert.ToByte(sqLiteRead["SP_RR"]);
                        temp.bCR = Convert.ToBoolean(sqLiteRead["CR"]);

                        sqLiteCommandPPRCh2 = new System.Data.SQLite.SQLiteCommand("SELECT * FROM IRI_PPRCh2 WHERE ID=" + temp.iID, sqLiteConnect);
                        sqLiteReadPPRCh2 = sqLiteCommandPPRCh2.ExecuteReader();

                        List<FHSS_Location> lFHSSLocation = new List<FHSS_Location>();
                        while (sqLiteReadPPRCh2.Read())
                        {
                            FHSS_Location FHSSLocation = new FHSS_Location();
                            FHSSLocation.iID = Convert.ToInt32(sqLiteReadPPRCh2["ID"]);
                            FHSSLocation.iQ1 = Convert.ToInt32(sqLiteReadPPRCh2["Q1"]);
                            FHSSLocation.iQ2 = Convert.ToInt32(sqLiteReadPPRCh2["Q2"]);
                            FHSSLocation.dLatitude = Convert.ToDouble(sqLiteReadPPRCh2["Latitude"]);
                            FHSSLocation.dLongitude = Convert.ToDouble(sqLiteReadPPRCh2["Longitude"]);

                            lFHSSLocation.Add(FHSSLocation);
                        }
                        temp.sLocation = lFHSSLocation.ToArray();

                        sqLiteCommandPPRCh3 = new System.Data.SQLite.SQLiteCommand("SELECT * FROM IRI_PPRCh3 WHERE ID= " + temp.iID, sqLiteConnect);
                        sqLiteReadPPRCh3 = sqLiteCommandPPRCh3.ExecuteReader();

                        List<FHSS_Freq_Band> lFHSSFreqBand = new List<FHSS_Freq_Band>();
                        while (sqLiteReadPPRCh3.Read())
                        {
                            FHSS_Freq_Band FHSSFreqBand = new FHSS_Freq_Band();
                            FHSSFreqBand.iID = Convert.ToInt32(sqLiteReadPPRCh3["ID"]);
                            FHSSFreqBand.iFreq = Convert.ToInt32(sqLiteReadPPRCh3["Freq"]);
                            FHSSFreqBand.iDFreq = Convert.ToInt32(sqLiteReadPPRCh3["dFreq"]);

                            lFHSSFreqBand.Add(FHSSFreqBand);
                        }
                        temp.sFreq_Band = lFHSSFreqBand.ToArray();

                        lDistribFHSS.Add(temp);
                    }

                    AddIRI_PPRChToVariableWork(lDistribFHSS);

                    fLoad = true;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(SMessageError.mesErrReadDataTable, SMessageError.mesErr, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.ServiceNotification);
                    fLoad = false;

                    return fLoad;
                }
                finally
                {
                    sqLiteConnect.Close();
                }

                // удалить подключение к БД
                sqLiteConnect = null;
                sqLiteCommand = null;
                sqLiteRead = null;
            }

            return fLoad;
        }

        /// <summary>
        /// Функция загрузки таблицы IRI_PPRCh2 в variableWork
        /// </summary>      
        private FHSS_Location[] LoadTableIRI_PPRCh_LocationFromDB(NameTable nameTable, int id)
        {
            List<FHSS_Location> lFHSSLocation = new List<FHSS_Location>();

            if (functionsDB.ConnectionString() != "")
            {
                System.Data.SQLite.SQLiteConnection sqLiteConnect;
                System.Data.SQLite.SQLiteCommand sqLiteCommand;
                System.Data.SQLite.SQLiteDataReader sqLiteRead;

                // подключиться к БД
                sqLiteConnect = new System.Data.SQLite.SQLiteConnection(functionsDB.ConnectionString());

                sqLiteConnect.Open();

                try
                {
                    sqLiteCommand = new System.Data.SQLite.SQLiteCommand("SELECT * FROM " + nameTable.ToString() + " WHERE ID= " + id, sqLiteConnect);
                    sqLiteRead = sqLiteCommand.ExecuteReader();

                    while (sqLiteRead.Read())
                    {
                        FHSS_Location FHSSLocation = new FHSS_Location();
                        FHSSLocation.iID = Convert.ToInt32(sqLiteRead["ID"]);
                        FHSSLocation.iQ1 = Convert.ToInt32(sqLiteRead["Q1"]);
                        FHSSLocation.iQ2 = Convert.ToInt32(sqLiteRead["Q2"]);
                        FHSSLocation.dLatitude = Convert.ToDouble(sqLiteRead["Latitude"]);
                        FHSSLocation.dLongitude = Convert.ToDouble(sqLiteRead["Longitude"]);

                        lFHSSLocation.Add(FHSSLocation);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(SMessageError.mesErrReadDataTable, SMessageError.mesErr, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.ServiceNotification);
                    //return FHSSLocation;
                }
                finally
                {
                    sqLiteConnect.Close();
                }

                // удалить подключение к БД
                sqLiteConnect = null;
                sqLiteCommand = null;
                sqLiteRead = null;
            }

            return lFHSSLocation.ToArray();
        }

        /// <summary>
        /// Функция загрузки таблицы IRI_PPRCh2 в variableWork
        /// </summary>      
        private FHSS_Freq_Band[] LoadTableIRI_PPRCh_FreqBandFromDB(NameTable nameTable, int id)
        {
            List<FHSS_Freq_Band> lFHSSFreqBand = new List<FHSS_Freq_Band>();

            if (functionsDB.ConnectionString() != "")
            {
                System.Data.SQLite.SQLiteConnection sqLiteConnect;
                System.Data.SQLite.SQLiteCommand sqLiteCommand;
                System.Data.SQLite.SQLiteDataReader sqLiteRead;

                // подключиться к БД
                sqLiteConnect = new System.Data.SQLite.SQLiteConnection(functionsDB.ConnectionString());

                sqLiteConnect.Open();

                try
                {
                    sqLiteCommand = new System.Data.SQLite.SQLiteCommand("SELECT * FROM " + nameTable.ToString() + " WHERE ID= " + id, sqLiteConnect);
                    sqLiteRead = sqLiteCommand.ExecuteReader();

                    while (sqLiteRead.Read())
                    {
                        FHSS_Freq_Band FHSSFreqBand = new FHSS_Freq_Band();
                        FHSSFreqBand.iID = Convert.ToInt32(sqLiteRead["ID"]);
                        FHSSFreqBand.iFreq = Convert.ToInt32(sqLiteRead["Freq"]);
                        FHSSFreqBand.iDFreq = Convert.ToInt32(sqLiteRead["dFreq"]);

                        lFHSSFreqBand.Add(FHSSFreqBand);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(SMessageError.mesErrReadDataTable, SMessageError.mesErr, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.ServiceNotification);
                }
                finally
                {
                    sqLiteConnect.Close();
                }

                // удалить подключение к БД
                sqLiteConnect = null;
                sqLiteCommand = null;
                sqLiteRead = null;
            }

            return lFHSSFreqBand.ToArray();
        }

        /// <summary>
        /// Функция добавления записи в таблицу IRI_PPRCh в БД
        /// </summary>
        /// <param name="myDGV"></param>
        /// <param name="bTable"></param>
        public bool AddRecords_IRI_PPRChToDB(NameTable nameTable, object obj)
        {
            bool fRead = false;

            if (functionsDB.ConnectionString() != "")
            {
                System.Data.SQLite.SQLiteConnection sqLiteConnect;
                System.Data.SQLite.SQLiteCommand sqLiteCommandPPRCh, sqLiteCommandPPRCh2, sqLiteCommandPPRCh3;

                sqLiteConnect = new System.Data.SQLite.SQLiteConnection(functionsDB.ConnectionString());

                try
                {
                    DistribFHSS = (TDistribFHSS[])obj;
                    
                    sqLiteConnect.Open();

                    for (int i = 0; i < DistribFHSS.Length; i++)
                    {
                        string queryPPRCh = "INSERT INTO " + nameTable.ToString() + " (ID, FreqMin, FreqMax, Step, dFreq, DuratImp, CountIRI, Time, SP_RR, CR)" +
                            " VALUES ('" + DistribFHSS[i].iID + "', '" + DistribFHSS[i].iFreqMin + "', '" + DistribFHSS[i].iFreqMax + "', '" + DistribFHSS[i].iStep + "', '" + DistribFHSS[i].iDFreq +
                            "', '" + DistribFHSS[i].iDuratImp + "', '" + DistribFHSS[i].iCountIRI + "', '" + DistribFHSS[i].sTime + "', '" + DistribFHSS[i].iSP_RR + "', '" + DistribFHSS[i].bCR + "')";


                        sqLiteCommandPPRCh = new System.Data.SQLite.SQLiteCommand(queryPPRCh);
                        sqLiteCommandPPRCh.Connection = sqLiteConnect;
                        sqLiteCommandPPRCh.ExecuteNonQuery();

                        for (int j = 0; j < DistribFHSS[i].sLocation.Length; j++)
                        {

                            string queryPPRCh2 = "INSERT INTO IRI_PPRCh2 (ID, Q1, Q2, Latitude, Longitude)" +
                           " VALUES ('" + DistribFHSS[i].sLocation[j].iID + "', '" + DistribFHSS[i].sLocation[j].iQ1 + "', '" + DistribFHSS[i].sLocation[j].iQ2 +
                           "', '" + DistribFHSS[i].sLocation[j].dLatitude.ToString("0.000000") + "', '" + DistribFHSS[i].sLocation[j].dLongitude.ToString("0.000000") + "')";

                            sqLiteCommandPPRCh2 = new System.Data.SQLite.SQLiteCommand(queryPPRCh2);
                            sqLiteCommandPPRCh2.Connection = sqLiteConnect;
                            sqLiteCommandPPRCh2.ExecuteNonQuery();
                        }

                        for (int j = 0; j < DistribFHSS[i].sFreq_Band.Length; j++)
                        {

                            string queryPPRCh3 = "INSERT INTO IRI_PPRCh3 (ID, Freq, dFreq)" +
                           " VALUES ('" + DistribFHSS[i].sFreq_Band[j].iID + "', '" + DistribFHSS[i].sFreq_Band[j].iFreq + "', '" + DistribFHSS[i].sFreq_Band[j].iDFreq + "')";

                            sqLiteCommandPPRCh3 = new System.Data.SQLite.SQLiteCommand(queryPPRCh3);
                            sqLiteCommandPPRCh3.Connection = sqLiteConnect;
                            sqLiteCommandPPRCh3.ExecuteNonQuery();
                        }
                    }

                    sqLiteConnect.Close();

                    fRead = true;
                }
                catch (System.Exception e)
                {
                    //MessageBox.Show(e.Message);
                    MessageBox.Show(SMessageError.mesErrAddRecord, SMessageError.mesErr, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.ServiceNotification);
                    fRead = false;

                    return fRead;
                }

                // удалить подключение к БД
                sqLiteConnect = null;
                sqLiteCommandPPRCh = null;
                sqLiteCommandPPRCh2 = null;
                sqLiteCommandPPRCh3 = null;
            }

            return fRead;
        }

        /// <summary>
        /// Удалить одну запись из таблицы базы данных
        /// </summary>
        /// <param name="iNum"></param>
        public bool DeleteRecordsPPRChDB(int iID, NameTable nameTable, Table table)
        {
            bool fDelete = false;

            byte bTable = 0;
            if (table == Table.Own) bTable = 1;
            if (table == Table.Linked) bTable = 2;

            if (functionsDB.ConnectionString() != "")
            {
                string name = nameTable.ToString();

                System.Data.SQLite.SQLiteConnection sqLiteConnect;
                System.Data.SQLite.SQLiteCommand sqLiteCommand;

                // подключиться к БД
                sqLiteConnect = new System.Data.SQLite.SQLiteConnection(functionsDB.ConnectionString());

                try
                {
                    // удалить запись с номером Num = iNum
                    sqLiteConnect.Open();

                    if (bTable == 0)
                        sqLiteCommand = new System.Data.SQLite.SQLiteCommand("DELETE FROM " + name + " WHERE ID=" + iID, sqLiteConnect);
                    else
                        sqLiteCommand = new System.Data.SQLite.SQLiteCommand("DELETE FROM " + name + " WHERE ID=" + iID + " AND Mode=" + bTable, sqLiteConnect);

                    sqLiteCommand.ExecuteNonQuery();

                    fDelete = true;
                }
                catch (System.Data.DataException)
                {
                    MessageBox.Show(SMessageError.mesErrDeleteRecord, SMessageError.mesErr, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.ServiceNotification);
                    fDelete = false;

                    return fDelete;
                }
                finally
                {
                    sqLiteConnect.Close();
                }
              
                // удалить подключение к БД
                sqLiteConnect = null;
                sqLiteCommand = null;
            }

            return fDelete;
        }
    }
}
