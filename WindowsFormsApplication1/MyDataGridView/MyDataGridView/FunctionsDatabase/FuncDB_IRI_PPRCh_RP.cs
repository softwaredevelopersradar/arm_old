﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using USR_DLL;
using VariableDynamic;

namespace MyDataGridView
{
    public class FuncDB_IRI_PPRCh_RP
    {
        Functions functions;
        FunctionsDB functionsDB;
        VariableWork variableWork;
        TDistribFHSS_RP DistribFHSS_RP;
        TDistribFHSS_RPExclude DistribFHSS_RPExclude;
        

        public FuncDB_IRI_PPRCh_RP()
        {
            functions = new Functions();
            functionsDB = new FunctionsDB();
            variableWork = new VariableWork();
        }

        /// <summary>
        /// Функция добавления записи в таблицу IRI_PPRCh_RP в БД
        /// </summary>
        /// <param name="myDGV"></param>
        /// <param name="bTable"></param>
        public bool AddRecord_IRI_PPRCh_RPToDB(NameTable nameTable, object obj, Table table)
        {
            bool fRead = false;

            byte bTable = 0;
            if (table == Table.Own) bTable = 1;
            if (table == Table.Linked) bTable = 2;

            if (functionsDB.ConnectionString() != "")
            {
                System.Data.SQLite.SQLiteConnection sqLiteConnect;
                System.Data.SQLite.SQLiteCommand sqLiteCommand;

                sqLiteConnect = new System.Data.SQLite.SQLiteConnection(functionsDB.ConnectionString());

                try
                {
                    DistribFHSS_RP = (TDistribFHSS_RP)obj;

                    string query = "INSERT INTO " + nameTable.ToString() + " (ID, Fmin, Fmax, U, Step, Modulation, Deviation, Manipulation, Duration, Mode)" +
                        " VALUES ('" + DistribFHSS_RP.iID + "', '" + DistribFHSS_RP.iFreqMin + "', '" + DistribFHSS_RP.iFreqMax + "', '" + DistribFHSS_RP.sLevel + "', '" + DistribFHSS_RP.iStep +
                        "', '" + DistribFHSS_RP.bModulation + "', '" + DistribFHSS_RP.bDeviation + "', '" + DistribFHSS_RP.bManipulation + "', '" + DistribFHSS_RP.iDuration + "', '" + bTable + "')";

                    sqLiteConnect.Open();
                    sqLiteCommand = new System.Data.SQLite.SQLiteCommand(query);
                    sqLiteCommand.Connection = sqLiteConnect;
                    sqLiteCommand.ExecuteNonQuery();
                    sqLiteConnect.Close();

                    fRead = true;
                }
                catch (System.Exception e)
                {
                    //MessageBox.Show(e.Message);
                    MessageBox.Show(SMessageError.mesErrAddRecord, SMessageError.mesErr, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.ServiceNotification);
                    fRead = false;

                    return fRead;
                }

                // удалить подключение к БД
                sqLiteConnect = null;
                sqLiteCommand = null;
            }

            return fRead;
        }

        /// <summary>
        /// Функция загрузки таблицы IRI_PPRCh_RP в dgv
        /// </summary>      
        public bool LoadTableIRI_PPRCh_RPFromDB(NameTable nameTable, Table table)
        {
            bool fLoad = false;

            byte bTable = 0;
            if (table == Table.Own) bTable = 1;
            if (table == Table.Linked) bTable = 2;

            if (functionsDB.ConnectionString() != "")
            {
                System.Data.SQLite.SQLiteConnection sqLiteConnect;
                System.Data.SQLite.SQLiteCommand sqLiteCommand;
                System.Data.SQLite.SQLiteDataReader sqLiteRead;

                int j = 0;

                // подключиться к БД
                sqLiteConnect = new System.Data.SQLite.SQLiteConnection(functionsDB.ConnectionString());

                sqLiteConnect.Open();

                try
                {
                    sqLiteCommand = new System.Data.SQLite.SQLiteCommand("SELECT * FROM " + nameTable.ToString() +" WHERE Mode =" + Convert.ToString(bTable), sqLiteConnect);
                    sqLiteRead = sqLiteCommand.ExecuteReader();

                    List<TDistribFHSS_RP> lDistribFHSS_RP = new List<TDistribFHSS_RP>();
                    while (sqLiteRead.Read())
                    {
                        TDistribFHSS_RP temp = new TDistribFHSS_RP();
                        temp.iID = Convert.ToInt32(sqLiteRead["ID"]);
                        temp.iFreqMin = Convert.ToInt32(sqLiteRead["Fmin"]);
                        temp.iFreqMax = Convert.ToInt32(sqLiteRead["Fmax"]);
                        temp.sLevel = Convert.ToInt16(sqLiteRead["U"]);
                        temp.iStep = Convert.ToInt32(sqLiteRead["Step"]);
                        temp.bLetter = functions.DefineLetter(temp.iFreqMin, temp.iFreqMax); 
                        temp.bModulation = Convert.ToByte(sqLiteRead["Modulation"]);
                        temp.bDeviation = Convert.ToByte(sqLiteRead["Deviation"]);
                        temp.bManipulation = Convert.ToByte(sqLiteRead["Manipulation"]);
                        temp.bDuration = 1;
                        temp.iDuration = Convert.ToInt32(sqLiteRead["Duration"]);
                        temp.bCodeFFT = 4;

                        lDistribFHSS_RP.Add(temp);
                    }

                    if (table == Table.Own)
                    {
                        variableWork.DistribFHSS_RPOwn = lDistribFHSS_RP.ToArray();
                    }
                    else
                    {
                        variableWork.DistribFHSS_RPLinked = lDistribFHSS_RP.ToArray();
                    }

                    fLoad = true;
                }
                catch (Exception ex)
                {
                    //MessageBox.Show(ex.Message);

                    string strError = "";
                    switch (table)
                    {
                        case Table.Own:
                            strError = SMessageError.mesErrReadDataOwn;
                            break;

                        case Table.Linked:
                            strError = SMessageError.mesErrReadDataLinked;
                            break;

                        default:
                            break;
                    }
                    MessageBox.Show(strError, SMessageError.mesErr, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.ServiceNotification);
                    fLoad = false;

                    return fLoad;
                }
                finally
                {
                    sqLiteConnect.Close();
                }

                // удалить подключение к БД
                sqLiteConnect = null;
                sqLiteCommand = null;
                sqLiteRead = null;
            }

            return fLoad;
        }

        public bool AddRecord_IRI_PPRCh_RPExcludeToDB(NameTable nameTable, object obj, Table table)
        {
            bool fRead = false;

            byte bTable = 0;
            if (table == Table.Own) bTable = 1;
            if (table == Table.Linked) bTable = 2;

            if (functionsDB.ConnectionString() != "")
            {
                System.Data.SQLite.SQLiteConnection sqLiteConnect;
                System.Data.SQLite.SQLiteCommand sqLiteCommand;

                sqLiteConnect = new System.Data.SQLite.SQLiteConnection(functionsDB.ConnectionString());

                try
                {
                    DistribFHSS_RPExclude = (TDistribFHSS_RPExclude)obj;

                    string query = "INSERT INTO " + nameTable.ToString() + " (ID, Freq, dFreq, Mode)" +
                        " VALUES ('" + DistribFHSS_RPExclude.iID + "', '" + DistribFHSS_RPExclude.iFreqExclude + "', '" + DistribFHSS_RPExclude.iWidthExclude + "', '" + bTable + "')";

                    sqLiteConnect.Open();
                    sqLiteCommand = new System.Data.SQLite.SQLiteCommand(query);
                    sqLiteCommand.Connection = sqLiteConnect;
                    sqLiteCommand.ExecuteNonQuery();
                    sqLiteConnect.Close();

                    fRead = true;
                }
                catch (System.Exception e)
                {
                    //MessageBox.Show(e.Message);
                    MessageBox.Show(SMessageError.mesErrAddRecord, SMessageError.mesErr, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.ServiceNotification);
                    fRead = false;

                    return fRead;
                }

                // удалить подключение к БД
                sqLiteConnect = null;
                sqLiteCommand = null;
            }

            return fRead;
        }

        /// <summary>
        /// Изменить значения ИРИ ФРЧ на РП в переменных класса VariableWork
        /// </summary>
        /// <param name="nameTable"></param>
        /// <param name="bTable"></param>
        /// <returns></returns>
        public void LoadIRI_PPRCh_RPExcludeFromDBToVW(NameTable nameTable, Table table)
        {
            byte bTable = 0;
            if (table == Table.Own) bTable = 1;
            if (table == Table.Linked) bTable = 2;

            if (functionsDB.ConnectionString() != "")
            {
                System.Data.SQLite.SQLiteConnection sqLiteConnect;
                System.Data.SQLite.SQLiteCommand sqLiteCommand;
                System.Data.SQLite.SQLiteDataReader sqLiteRead;

                sqLiteConnect = new System.Data.SQLite.SQLiteConnection(functionsDB.ConnectionString());

                try
                {
                    sqLiteConnect.Open();
                    sqLiteCommand = new System.Data.SQLite.SQLiteCommand("SELECT * FROM " + nameTable.ToString() + " WHERE Mode=" + bTable, sqLiteConnect);
                    sqLiteRead = sqLiteCommand.ExecuteReader();

                    List<TDistribFHSS_RPExclude> lDistribFHSS_RPExclude = new List<TDistribFHSS_RPExclude>();
                    while (sqLiteRead.Read())
                    {
                        TDistribFHSS_RPExclude temp = new TDistribFHSS_RPExclude();
                        temp.iID = Convert.ToInt32(sqLiteRead["ID"]);
                        //TDistribFHSS_RPExclude.SetIdFWS(temp.iID);
                        temp.iFreqExclude = Convert.ToInt32(sqLiteRead["Freq"]);
                        temp.iWidthExclude = Convert.ToInt32(sqLiteRead["dFreq"]);

                        lDistribFHSS_RPExclude.Add(temp);
                    }

                    if (nameTable == NameTable.IRI_PPRCh_RP_EXCLUDE && table == Table.Own)
                    {
                        variableWork.DistribFHSS_RPExcludeOwn = lDistribFHSS_RPExclude.ToArray();
                    }
                    else if (nameTable == NameTable.IRI_PPRCh_RP_EXCLUDE && table == Table.Linked)
                    {
                        variableWork.DistribFHSS_RPExcludeLinked = lDistribFHSS_RPExclude.ToArray();
                    }
                }
                catch (System.Data.DataException)
                {
                    MessageBox.Show(SMessageError.mesErrReadDataTable, SMessageError.mesErr, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.ServiceNotification);
                }
                finally
                {
                    sqLiteConnect.Close();
                }

                // удалить подключение к БД
                sqLiteConnect = null;
                sqLiteCommand = null;
            }
        }

        public bool ChangeIdenticalIRI_PPRCh_RPExclude(NameTable nameTable, object objTable, Table table)
        {
            bool fChange = false;

            byte bTable = 0;
            if (table == Table.Own) bTable = 1;
            if (table == Table.Linked) bTable = 2;

            DistribFHSS_RPExclude = (TDistribFHSS_RPExclude)objTable; // для добавления

            List<TDistribFHSS_RPExclude> lTemp = LoadIRI_PPRCh_RPExclude(NameTable.IRI_PPRCh_RP_EXCLUDE, table); // для проверки

            for (int i = 0; i < lTemp.Count(); i++)
            {
                if (lTemp[i].iFreqExclude == DistribFHSS_RPExclude.iFreqExclude)// && lTemp[i].iWidthExclude < DistribFHSS_RPExclude.iWidthExclude)
                {
                   if (functionsDB.ConnectionString() != "")
                    {
                        System.Data.SQLite.SQLiteConnection sqLiteConnect;
                        System.Data.SQLite.SQLiteCommand sqLiteCommand;

                        sqLiteConnect = new System.Data.SQLite.SQLiteConnection(functionsDB.ConnectionString());

                        try
                        {
                            string query = string.Format("UPDATE " + nameTable.ToString() +
                            " SET ID={0}, Freq={1}, dFreq={2}, Mode={3} " +
                            "WHERE ID={4} AND Freq={5} AND dFreq={6} AND Mode={7}",
                            DistribFHSS_RPExclude.iID, DistribFHSS_RPExclude.iFreqExclude, DistribFHSS_RPExclude.iWidthExclude, bTable,
                            lTemp[i].iID, lTemp[i].iFreqExclude, lTemp[i].iWidthExclude, bTable);

                            sqLiteConnect.Open();
                            sqLiteCommand = new System.Data.SQLite.SQLiteCommand(query);
                            sqLiteCommand.Connection = sqLiteConnect;
                            sqLiteCommand.ExecuteNonQuery();

                            fChange = true;
                        }
                        catch (Exception e)
                        {
                            MessageBox.Show(SMessageError.mesErrChangeRecord, SMessageError.mesErr, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.ServiceNotification);
                            fChange = false;

                            return fChange;
                        }
                        finally
                        {
                            sqLiteConnect.Close();
                        }

                        // удалить подключение к БД
                        sqLiteConnect = null;
                        sqLiteCommand = null;
                    }
                }
            }

            return fChange;
        }

        /// <summary>
        /// Загрузить данные из таблицы IRI_PPRCh_RPExclude для анализа при добавлении записи
        /// </summary>
        /// <param name="nameTable"></param>
        /// <param name="bTable"></param>
        /// <returns></returns>
        private List<TDistribFHSS_RPExclude> LoadIRI_PPRCh_RPExclude(NameTable nameTable, Table table)
        {
            byte bTable = 0;
            if (table == Table.Own) bTable = 1;
            if (table == Table.Linked) bTable = 2;

            List<TDistribFHSS_RPExclude> lDistribFHSS_RPExclude = new List<TDistribFHSS_RPExclude>();

            if (functionsDB.ConnectionString() != "")
            {
                System.Data.SQLite.SQLiteConnection sqLiteConnect;
                System.Data.SQLite.SQLiteCommand sqLiteCommand;
                System.Data.SQLite.SQLiteDataReader sqLiteRead;

                sqLiteConnect = new System.Data.SQLite.SQLiteConnection(functionsDB.ConnectionString());

                try
                {
                    sqLiteConnect.Open();
                    sqLiteCommand = new System.Data.SQLite.SQLiteCommand("SELECT * FROM " + nameTable.ToString() + " WHERE Mode=" + bTable, sqLiteConnect);
                    sqLiteRead = sqLiteCommand.ExecuteReader();

                    while (sqLiteRead.Read())
                    {
                        TDistribFHSS_RPExclude temp = new TDistribFHSS_RPExclude();
                        temp.iID = Convert.ToInt32(sqLiteRead["ID"]);
                        temp.iFreqExclude = Convert.ToInt32(sqLiteRead["Freq"]);
                        temp.iWidthExclude = Convert.ToInt32(sqLiteRead["dFreq"]);

                        lDistribFHSS_RPExclude.Add(temp);
                    }
                }
                catch (System.Data.DataException)
                {
                    MessageBox.Show(SMessageError.mesErrReadDataTable, SMessageError.mesErr, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.ServiceNotification);
                }
                finally
                {
                    sqLiteConnect.Close();
                }

                // удалить подключение к БД
                sqLiteConnect = null;
                sqLiteCommand = null;
            }

            return lDistribFHSS_RPExclude;
        }

        /// <summary>
        /// Удалить одну запись из таблицы базы данных
        /// </summary>
        /// <param name="iNum"></param>
        public bool DeleteOneRecord_IRI_PPRCh_RPExcludefromDB(int iID, NameTable nameTable, Table table)
        {
            bool fDelete = false;

            byte bTable = 0;
            if (table == Table.Own) bTable = 1;
            if (table == Table.Linked) bTable = 2;

            if (functionsDB.ConnectionString() != "")
            {
                string name = nameTable.ToString();

                System.Data.SQLite.SQLiteConnection sqLiteConnect;
                System.Data.SQLite.SQLiteCommand sqLiteCommand;

                // подключиться к БД
                sqLiteConnect = new System.Data.SQLite.SQLiteConnection(functionsDB.ConnectionString());

                try
                {
                    sqLiteConnect.Open();

                    if (bTable == 0)
                        sqLiteCommand = new System.Data.SQLite.SQLiteCommand("DELETE FROM " + name + " WHERE ID=" + iID, sqLiteConnect);
                    else
                        sqLiteCommand = new System.Data.SQLite.SQLiteCommand("DELETE FROM " + name + " WHERE ID=" + iID + " AND Mode=" + bTable, sqLiteConnect);

                    sqLiteCommand.ExecuteNonQuery();

                    fDelete = true;
                }
                catch (System.Data.DataException)
                {
                    MessageBox.Show(SMessageError.mesErrDeleteRecord, SMessageError.mesErr, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.ServiceNotification);
                    fDelete = false;

                    return fDelete;
                }
                finally
                {
                    sqLiteConnect.Close();
                }

                // удалить подключение к БД
                sqLiteConnect = null;
                sqLiteCommand = null;
            }

            return fDelete;
        }

        /// <summary>
        /// Изменить значения ИРИ ФРЧ на РП в переменных класса VariableWork
        /// </summary>
        /// <param name="nameTable"></param>
        /// <param name="bTable"></param>
        /// <returns></returns>
        public int ChangeIRI_PPRCh_RPExcludeToVariableWork(NameTable nameTable, Table table)
        {
            int count = 0;

            byte bTable = 0;
            if (table == Table.Own) bTable = 1;
            if (table == Table.Linked) bTable = 2;

            if (functionsDB.ConnectionString() != "")
            {
                System.Data.SQLite.SQLiteConnection sqLiteConnect;
                System.Data.SQLite.SQLiteCommand sqLiteCommand;
                System.Data.SQLite.SQLiteDataReader sqLiteRead;

                sqLiteConnect = new System.Data.SQLite.SQLiteConnection(functionsDB.ConnectionString());

                try
                {
                    sqLiteConnect.Open();

                    if (bTable == 0)
                    {
                        sqLiteCommand = new System.Data.SQLite.SQLiteCommand("SELECT * FROM " + nameTable.ToString(), sqLiteConnect);
                    }
                    else
                    {
                        sqLiteCommand = new System.Data.SQLite.SQLiteCommand("SELECT * FROM " + nameTable.ToString() + " WHERE Mode=" + bTable, sqLiteConnect);
                    }

                    sqLiteRead = sqLiteCommand.ExecuteReader();

                    List<TDistribFHSS_RPExclude> list = new List<TDistribFHSS_RPExclude>();
                    while (sqLiteRead.Read())
                    {
                        TDistribFHSS_RPExclude temp = new TDistribFHSS_RPExclude();
                        temp.iID = Convert.ToInt32(sqLiteRead["ID"]);
                        temp.iFreqExclude = Convert.ToInt32(sqLiteRead["Freq"]);
                        temp.iWidthExclude = Convert.ToInt16(sqLiteRead["dFreq"]);

                        list.Add(temp);
                    }

                    if (nameTable == NameTable.IRI_PPRCh_RP_EXCLUDE && table == Table.Own)
                    {
                        variableWork.DistribFHSS_RPExcludeOwn = list.ToArray();
                    }
                    else if (nameTable == NameTable.IRI_PPRCh_RP_EXCLUDE && table == Table.Linked)
                    {
                        variableWork.DistribFHSS_RPExcludeLinked = list.ToArray();
                    }
                }
                catch (System.Data.DataException)
                {
                    return count;
                }
                finally
                {
                    sqLiteConnect.Close();
                }

                // удалить подключение к БД
                sqLiteConnect = null;
                sqLiteCommand = null;
            }

            return count;
        }
    }
}
