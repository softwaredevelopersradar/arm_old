﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyDataGridView
{
    public class DecoderParams
    {
        // декодирование параметров помехи из кодов в строку
        public string ParamHindrance(byte bModulation, byte bDeviation, byte bManipulation, byte bDuration)
        {
            string strModulation = "";      // модуляция
            string strDeviation = "";       // девиация
            string strManipulation = "";    // манипуляция
            string strDuration = "";        // длительность

            string strHindrance = "";       // все параметры

            switch (bModulation)
            {
                case 1:
                    strModulation = SParams.paramChMSh + " ";// "ЧМШ ";

                    switch (bDeviation)
                    {
                        case 1:
                            strDeviation = "+/- 1,75 " + SMeaning.meaningkHz + "  ";// кГц  ";
                            break;

                        case 2:
                            strDeviation = "+/- 2,47 " + SMeaning.meaningkHz + "  ";//кГц  ";
                            break;

                        case 3:
                            strDeviation = "+/- 3,5 " + SMeaning.meaningkHz + "  ";//кГц  ";
                            break;

                        case 4:
                            strDeviation = "+/- 4,2 " + SMeaning.meaningkHz + "  ";//кГц  ";
                            break;

                        case 5:
                            strDeviation = "+/- 5 " + SMeaning.meaningkHz + "  ";//кГц  ";
                            break;

                        case 6:
                            strDeviation = "+/- 7,07 " + SMeaning.meaningkHz + "  ";//кГц  ";
                            break;

                        case 7:
                            strDeviation = "+/- 10 " + SMeaning.meaningkHz + "  ";//кГц  ";
                            break;

                        case 8:
                            strDeviation = "+/- 15,8 " + SMeaning.meaningkHz + "  ";//кГц  ";
                            break;

                        case 9:
                            strDeviation = "+/- 25 " + SMeaning.meaningkHz + "  ";//кГц  ";
                            break;

                        case 10:
                            strDeviation = "+/- 35,35 " + SMeaning.meaningkHz + "  ";//кГц  ";
                            break;

                        case 11:
                            strDeviation = "+/- 50 " + SMeaning.meaningkHz + "  ";//кГц  ";
                            break;

                        case 12:
                            strDeviation = "+/- 70 " + SMeaning.meaningkHz + "  ";//кГц  ";
                            break;

                        case 13:
                            strDeviation = "+/- 100 " + SMeaning.meaningkHz + "  ";//кГц  ";
                            break;

                        case 14:
                            strDeviation = "+/- 150 " + SMeaning.meaningkHz + "  ";//кГц  ";
                            break;

                        case 15:
                            strDeviation = "+/- 250 " + SMeaning.meaningkHz + "  ";//кГц  ";
                            break;

                        case 16:
                            strDeviation = "+/- 350 " + SMeaning.meaningkHz + "  ";//кГц  ";
                            break;

                        case 17:
                            strDeviation = "+/- 500 " + SMeaning.meaningkHz + "  ";//кГц  ";
                            break;

                        case 18:
                            strDeviation = "+/- 700 " + SMeaning.meaningkHz + "  ";//кГц  ";
                            break;

                        case 19:
                            strDeviation = "+/- 1000 " + SMeaning.meaningkHz + "  ";//кГц  ";
                            break;

                        case 20:
                            strDeviation = "+/- 10000 " + SMeaning.meaningkHz + "  ";//кГц  ";
                            break;

                        default:
                            break;
                    }
                    break;

                case 2:
                    strModulation = SParams.paramChM2 + " ";// "ЧМ-2 ";

                    switch (bDeviation)
                    {
                        case 1:
                            strDeviation = "+/- 2 " + SMeaning.meaningkHz + "  ";//кГц  ";
                            break;

                        case 2:
                            strDeviation = "+/- 4 " + SMeaning.meaningkHz + "  ";//кГц  ";
                            break;

                        case 3:
                            strDeviation = "+/- 6 " + SMeaning.meaningkHz + "  ";//кГц  ";
                            break;

                        case 4:
                            strDeviation = "+/- 8 " + SMeaning.meaningkHz + "  ";//кГц  ";
                            break;

                        case 5:
                            strDeviation = "+/- 10 " + SMeaning.meaningkHz + "  ";//кГц  ";
                            break;

                        case 6:
                            strDeviation = "+/- 12 " + SMeaning.meaningkHz + "  ";//кГц  ";
                            break;

                        case 7:
                            strDeviation = "+/- 14 " + SMeaning.meaningkHz + "  ";//кГц  ";
                            break;

                        case 8:
                            strDeviation = "+/- 16 " + SMeaning.meaningkHz + "  ";//кГц  ";
                            break;

                        case 9:
                            strDeviation = "+/- 18 " + SMeaning.meaningkHz + "  ";//кГц  ";
                            break;

                        case 10:
                            strDeviation = "+/- 20 " + SMeaning.meaningkHz + "  ";//кГц  ";
                            break;

                        case 11:
                            strDeviation = "+/- 22 " + SMeaning.meaningkHz + "  ";//кГц  ";
                            break;

                        case 12:
                            strDeviation = "+/- 24 " + SMeaning.meaningkHz + "  ";//кГц  ";
                            break;

                        default:
                            break;
                    }

                    switch (bManipulation)
                    {
                        //case 0:
                        //    strManipulation = "62,5 " + SMeaning.meaningmks + "  ";//мкс  ";
                        //    break;

                        //case 1:
                        //    strManipulation = "125 " + SMeaning.meaningmks + "  ";//мкс  ";
                        //    break;

                        //case 2:
                        //    strManipulation = "250 " + SMeaning.meaningmks + "  ";//мкс  ";
                        //    break;

                        //case 3:
                        //    strManipulation = "500 " + SMeaning.meaningmks + "  ";//мкс  ";
                        //    break;

                        //case 4:
                        //    strManipulation = "1 " + SMeaning.meaningms + "  ";//мс  ";
                        //    break;

                        //case 5:
                        //    strManipulation = "2 " + SMeaning.meaningms + "  ";//мс  ";
                        //    break;

                        case 1:
                            strManipulation = "62,5 " + SMeaning.meaningmks + "  ";//мкс  ";
                            break;

                        case 2:
                            strManipulation = "125 " + SMeaning.meaningmks + "  ";//мкс  ";
                            break;

                        case 3:
                            strManipulation = "250 " + SMeaning.meaningmks + "  ";//мкс  ";
                            break;

                        case 4:
                            strManipulation = "500 " + SMeaning.meaningmks + "  ";//мкс  ";
                            break;

                        case 5:
                            strManipulation = "1 " + SMeaning.meaningms + "  ";//мс  ";
                            break;

                        case 6:
                            strManipulation = "2 " + SMeaning.meaningms + "  ";//мс  ";
                            break;

                        default:
                            break;
                    }
                    break;

                case 3:
                    strModulation = SParams.paramChM4 + " ";// "ЧМ-4 ";

                    switch (bDeviation)
                    {
                        case 1:
                            strDeviation = "+/- 2 " + SMeaning.meaningkHz + "  ";//кГц  ";
                            break;

                        case 2:
                            strDeviation = "+/- 4 " + SMeaning.meaningkHz + "  ";//кГц  ";
                            break;

                        case 3:
                            strDeviation = "+/- 6 " + SMeaning.meaningkHz + "  ";//кГц  ";
                            break;

                        case 4:
                            strDeviation = "+/- 8 " + SMeaning.meaningkHz + "  ";//кГц  ";
                            break;

                        case 5:
                            strDeviation = "+/- 10 " + SMeaning.meaningkHz + "  ";//кГц  ";
                            break;

                        case 6:
                            strDeviation = "+/- 12 " + SMeaning.meaningkHz + "  ";//кГц  ";
                            break;

                        case 7:
                            strDeviation = "+/- 14 " + SMeaning.meaningkHz + "  ";//кГц  ";
                            break;

                        case 8:
                            strDeviation = "+/- 16 " + SMeaning.meaningkHz + "  ";//кГц  ";
                            break;

                        case 9:
                            strDeviation = "+/- 18 " + SMeaning.meaningkHz + "  ";//кГц  ";
                            break;

                        case 10:
                            strDeviation = "+/- 20 " + SMeaning.meaningkHz + "  ";//кГц  ";
                            break;

                        case 11:
                            strDeviation = "+/- 22 " + SMeaning.meaningkHz + "  ";//кГц  ";
                            break;

                        case 12:
                            strDeviation = "+/- 24 " + SMeaning.meaningkHz + "  ";//кГц  ";
                            break;

                        default:
                            break;
                    }

                    switch (bManipulation)
                    {
                        //case 0:
                        //    strManipulation = "62,5 " + SMeaning.meaningmks + "  ";//мкс  ";
                        //    break;

                        //case 1:
                        //    strManipulation = "125 " + SMeaning.meaningmks + "  ";//мкс  ";
                        //    break;

                        //case 2:
                        //    strManipulation = "250 " + SMeaning.meaningmks + "  ";//мкс  ";
                        //    break;

                        //case 3:
                        //    strManipulation = "500 " + SMeaning.meaningmks + "  ";//мкс  ";
                        //    break;

                        //case 4:
                        //    strManipulation = "1 " + SMeaning.meaningms + "  ";//мс  ";
                        //    break;

                        //case 5:
                        //    strManipulation = "2 " + SMeaning.meaningms + "  ";//мс  ";
                        //    break;


                        case 1:
                            strManipulation = "62,5 " + SMeaning.meaningmks + "  ";//мкс  ";
                            break;

                        case 2:
                            strManipulation = "125 " + SMeaning.meaningmks + "  ";//мкс  ";
                            break;

                        case 3:
                            strManipulation = "250 " + SMeaning.meaningmks + "  ";//мкс  ";
                            break;

                        case 4:
                            strManipulation = "500 " + SMeaning.meaningmks + "  ";//мкс  ";
                            break;

                        case 5:
                            strManipulation = "1 " + SMeaning.meaningms + "  ";//мс  ";
                            break;

                        case 6:
                            strManipulation = "2 " + SMeaning.meaningms + "  ";//мс  ";
                            break;

                        default:
                            break;
                    }
                    break;

                case 4:
                    strModulation = SParams.paramChM8 + " ";// "ЧМ-8 ";

                    switch (bDeviation)
                    {
                        case 1:
                            strDeviation = "+/- 2 " + SMeaning.meaningkHz + "  ";//кГц  ";
                            break;

                        case 2:
                            strDeviation = "+/- 4 " + SMeaning.meaningkHz + "  ";//кГц  ";
                            break;

                        case 3:
                            strDeviation = "+/- 6 " + SMeaning.meaningkHz + "  ";//кГц  ";
                            break;

                        case 4:
                            strDeviation = "+/- 8 " + SMeaning.meaningkHz + "  ";//кГц  ";
                            break;

                        case 5:
                            strDeviation = "+/- 10 " + SMeaning.meaningkHz + "  ";//кГц  ";
                            break;

                        case 6:
                            strDeviation = "+/- 12 " + SMeaning.meaningkHz + "  ";//кГц  ";
                            break;

                        case 7:
                            strDeviation = "+/- 14 " + SMeaning.meaningkHz + "  ";//кГц  ";
                            break;

                        case 8:
                            strDeviation = "+/- 16 " + SMeaning.meaningkHz + "  ";//кГц  ";
                            break;

                        case 9:
                            strDeviation = "+/- 18 " + SMeaning.meaningkHz + "  ";//кГц  ";
                            break;

                        case 10:
                            strDeviation = "+/- 20 " + SMeaning.meaningkHz + "  ";//кГц  ";
                            break;

                        case 11:
                            strDeviation = "+/- 22 " + SMeaning.meaningkHz + "  ";//кГц  ";
                            break;

                        case 12:
                            strDeviation = "+/- 24 " + SMeaning.meaningkHz + "  ";//кГц  ";
                            break;

                        default:
                            break;
                    }

                    switch (bManipulation)
                    {
                        //case 0:
                        //    strManipulation = "62,5 " + SMeaning.meaningmks + "  ";//мкс  ";
                        //    break;

                        //case 1:
                        //    strManipulation = "125 " + SMeaning.meaningmks + "  ";//мкс  ";
                        //    break;

                        //case 2:
                        //    strManipulation = "250 " + SMeaning.meaningmks + "  ";//мкс  ";
                        //    break;

                        //case 3:
                        //    strManipulation = "500 " + SMeaning.meaningmks + "  ";//мкс  ";
                        //    break;

                        //case 4:
                        //    strManipulation = "1 " + SMeaning.meaningms + "  ";//мс  ";
                        //    break;

                        //case 5:
                        //    strManipulation = "2 " + SMeaning.meaningms + "  ";//мс  ";
                        //    break;

                        case 1:
                            strManipulation = "62,5 " + SMeaning.meaningmks + "  ";//мкс  ";
                            break;

                        case 2:
                            strManipulation = "125 " + SMeaning.meaningmks + "  ";//мкс  ";
                            break;

                        case 3:
                            strManipulation = "250 " + SMeaning.meaningmks + "  ";//мкс  ";
                            break;

                        case 4:
                            strManipulation = "500 " + SMeaning.meaningmks + "  ";//мкс  ";
                            break;

                        case 5:
                            strManipulation = "1 " + SMeaning.meaningms + "  ";//мс  ";
                            break;

                        case 6:
                            strManipulation = "2 " + SMeaning.meaningms + "  ";//мс  ";
                            break;

                        default:
                            break;
                    }
                    break;

                case 5:
                    strModulation = SParams.paramFMn + " ";// "ФМн ";

                    switch (bManipulation)
                    {
                        case 10:
                            strManipulation = "0,08 " + SMeaning.meaningmks + "  ";//мкс  ";
                            break;

                        case 1:
                            strManipulation = "0,2 " + SMeaning.meaningmks + "  ";//мкс  ";
                            break;

                        case 2:
                            strManipulation = "0,5 " + SMeaning.meaningmks + "  ";//мкс  ";
                            break;

                        case 3:
                            strManipulation = "1 " + SMeaning.meaningmks + "  ";//мкс  ";
                            break;

                        case 4:
                            strManipulation = "5 " + SMeaning.meaningmks + "  ";//мкс  ";
                            break;

                        case 5:
                            strManipulation = "10 " + SMeaning.meaningmks + "  ";//мкс  ";
                            break;

                        case 6:
                            strManipulation = "20 " + SMeaning.meaningmks + "  ";//мкс  ";
                            break;

                        case 7:
                            strManipulation = "40 " + SMeaning.meaningmks + "  ";//мкс  ";
                            break;

                        case 8:
                            strManipulation = "80 " + SMeaning.meaningmks + "  ";//мкс  ";
                            break;

                        case 9:
                            strManipulation = "400 " + SMeaning.meaningmks + "  ";//мкс  ";
                            break;

                        default:
                            break;
                    }
                    break;

                case 6:
                    strModulation = SParams.paramFMn4 + " ";// "ФМн-4 ";

                    switch (bManipulation)
                    {
                        case 10:
                            strManipulation = "0,08 " + SMeaning.meaningmks + "  ";//мкс  ";
                            break;

                        case 1:
                            strManipulation = "0,2 " + SMeaning.meaningmks + "  ";//мкс  ";
                            break;

                        case 2:
                            strManipulation = "0,5 " + SMeaning.meaningmks + "  ";//мкс  ";
                            break;

                        case 3:
                            strManipulation = "1 " + SMeaning.meaningmks + "  ";//мкс  ";
                            break;

                        case 4:
                            strManipulation = "5 " + SMeaning.meaningmks + "  ";//мкс  ";
                            break;

                        case 5:
                            strManipulation = "10 " + SMeaning.meaningmks + "  ";//мкс  ";
                            break;

                        case 6:
                            strManipulation = "20 " + SMeaning.meaningmks + "  ";//мкс  ";
                            break;

                        case 7:
                            strManipulation = "40 " + SMeaning.meaningmks + "  ";//мкс  ";
                            break;

                        case 8:
                            strManipulation = "80 " + SMeaning.meaningmks + "  ";//мкс  ";
                            break;

                        case 9:
                            strManipulation = "400 " + SMeaning.meaningmks + "  ";//мкс  ";
                            break;

                        default:
                            break;
                    }
                    break;

                case 7:
                    strModulation = SParams.paramFMn8 + " ";// "ФМн-8 ";

                    switch (bManipulation)
                    {
                        case 10:
                            strManipulation = "0,08 " + SMeaning.meaningmks + "  ";//мкс  ";
                            break;

                        case 1:
                            strManipulation = "0,2 " + SMeaning.meaningmks + "  ";//мкс  ";
                            break;

                        case 2:
                            strManipulation = "0,5 " + SMeaning.meaningmks + "  ";//мкс  ";
                            break;

                        case 3:
                            strManipulation = "1 " + SMeaning.meaningmks + "  ";//мкс  ";
                            break;

                        case 4:
                            strManipulation = "5 " + SMeaning.meaningmks + "  ";//мкс  ";
                            break;

                        case 5:
                            strManipulation = "10 " + SMeaning.meaningmks + "  ";//мкс  ";
                            break;

                        case 6:
                            strManipulation = "20 " + SMeaning.meaningmks + "  ";//мкс  ";
                            break;

                        case 7:
                            strManipulation = "40 " + SMeaning.meaningmks + "  ";//мкс  ";
                            break;

                        case 8:
                            strManipulation = "80 " + SMeaning.meaningmks + "  ";//мкс  ";
                            break;

                        case 9:
                            strManipulation = "400 " + SMeaning.meaningmks + "  ";//мкс  ";
                            break;

                        default:
                            break;
                    }
                    break;

                case 8:
                    strModulation = SParams.paramZSh + " 2,5 " + SMeaning.meaningMHz + " 0,2 " + SMeaning.meaningmks;//мкс";// "ЗШ 2,5 МГц 0,2 мкс";
                    break;
            }

            if (bDuration < 99)
            {
                switch (bDuration)
                {
                    case 1:
                        strDuration = "0.5 " + SMeaning.meaningms + "  ";//мс";
                        break;

                    case 2:
                        strDuration = "1 " + SMeaning.meaningms + "  ";//мс";
                        break;

                    case 3:
                        strDuration = "1.5 " + SMeaning.meaningms + "  ";//мс";
                        break;

                    case 4:
                        strDuration = "2 " + SMeaning.meaningms + "  ";//мс";
                        break;

                    default:
                        strDuration = "";
                        break;
                }

                strHindrance = strModulation + " " + strDeviation + " " + strManipulation + " " + strDuration;
            }
            else
            {
                strHindrance = strModulation + " " + strDeviation + " " + strManipulation;// +" " + strDuration;
            }
            return strHindrance;
        }

        /// <summary>
        /// Вид модуляции (декодирование из кодов в строку)
        /// </summary>
        /// <param name="bModulation"></param>
        /// <returns></returns>
        public string ViewModulation(byte bModulation)
        {
            string sModulation = "";

            switch (bModulation)
            { 
                case 0:
                    sModulation = SMeaning.meaningShum;// "Шум"; //"ZeroImpulse";
                    break;

                case 1:
                    sModulation = SMeaning.meaningNes;//"Несущая"; //"Am2";
                    break;

                case 2:
                    sModulation = SMeaning.meaningAMn;//"АМн"; //"Am101";
                    break;

                case 3:
                    sModulation = SMeaning.meaningFMn;//"ФМн"; //"Fm2";
                    break;

                case 4:
                    sModulation = SMeaning.meaningChMn2;//"ЧМн2"; //"Fsk2"; 
                    break;

                case 5:
                    sModulation = SMeaning.meaningAMChM;//"АМ ЧМ"; //"AmFm";
                    break;

                case 6:
                    sModulation = SMeaning.meaningChMn4;//"ЧМн4"; //"Fsk4"; 
                    break;

                case 7:
                    sModulation = SMeaning.meaningChMn8;//"ЧМн8"; //"Fsk8"; 
                    break;

                case 8:
                    sModulation = SMeaning.meaningChM;//"ЧМ"; //"Fm"; 
                    break;

                case 9:
                    sModulation = SMeaning.meaningChMn;//"ЧМн"; //"Fsk"; 
                    break;

                case 10:
                    sModulation = SMeaning.meaningShPS;//"ШПС"; //"ShPS"; 
                    break;

                default:
                    sModulation = SMeaning.meaningUnkn;//"Неизв."; //"Unknown";
                    break;
            
            }

            return sModulation;
        }

        /// <summary>
        /// Вид модуляции (декодирование из строки в код)
        /// </summary>
        /// <param name="sModulation"></param>
        /// <returns></returns>
        public byte ViewModulation(string sModulation)
        {
            byte bModulation = 255;

            if (sModulation == SMeaning.meaningShum) bModulation = 0;
            if (sModulation == SMeaning.meaningNes) bModulation = 1;
            if (sModulation == SMeaning.meaningAMn) bModulation = 2;
            if (sModulation == SMeaning.meaningFMn) bModulation = 3;
            if (sModulation == SMeaning.meaningChMn2) bModulation = 4;
            if (sModulation == SMeaning.meaningAMChM) bModulation = 5;
            if (sModulation == SMeaning.meaningChMn4) bModulation = 6;
            if (sModulation == SMeaning.meaningChMn8) bModulation = 7;
            if (sModulation == SMeaning.meaningChM) bModulation = 8;
            if (sModulation == SMeaning.meaningChMn) bModulation = 9;
            if (sModulation == SMeaning.meaningShPS) bModulation = 10;
           

            switch (sModulation)
            {
                case  "Шум":         // SMeaning.meaningShum:
                    bModulation = 0; //"ZeroImpulse";
                    break;

                case "Несущая":
                    bModulation = 1; //"Am2";
                    break;

                case "АМн":
                    bModulation = 2; //"Am101";
                    break;

                case "ФМн":
                    bModulation = 3; //"Fm2";
                    break;

                case "ЧМн2":
                    bModulation = 4; //"Fsk2"; 
                    break;

                case "АМ ЧМ":
                    bModulation = 5; //"AmFm";
                    break;

                case "ЧМн4":
                    bModulation = 6; //"Fsk4"; 
                    break;

                case "ЧМн8":
                    bModulation = 7; //"Fsk8"; 
                    break;

                case "ЧМ":
                    bModulation = 8; //"Fm"; 
                    break;

                case "ЧМн":
                    bModulation = 9; //"Fsk"; 
                    break;

                case "ШПС":
                    bModulation = 10; //"ShPS"; 
                    break;

                default:
                    bModulation = 255; //"Unknown";
                    break;

            }

            return bModulation;
        }

        public byte[] SetParamHind(int iModeSignal, int iDevSignal, int iManSignal)
        {
            byte[] bResult = new byte[3];

            // для АМ ЧМ НБП ВБП АМ-2 несущ назначить ЧМШ
            if (iModeSignal == 1 | iModeSignal == 2 |
                iModeSignal == 4 | iModeSignal == 5 |
                iModeSignal == 7 | iModeSignal == 9 |
                iModeSignal == 8)
            {

                bResult[0] = 1;

                double dDevSignal = Convert.ToDouble(iDevSignal); 

                if (dDevSignal < 2.11)
                    bResult[1] = 1;
                if (dDevSignal >= 2.11 & dDevSignal < 2.95)
                    bResult[1] = 2;
                if (dDevSignal >= 2.95 & dDevSignal < 3.85)
                    bResult[1] = 3;
                if (dDevSignal >= 3.85 & dDevSignal < 4.6)
                    bResult[1] = 4;
                if (dDevSignal >= 4.6 & dDevSignal < 6.035)
                    bResult[1] = 5;
                if (dDevSignal >= 6.035 & dDevSignal < 8.535)
                    bResult[1] = 6;
                if (dDevSignal >= 8.535 & dDevSignal < 12.9)
                    bResult[1] = 7;
                if (dDevSignal >= 12.9 & dDevSignal < 20.4)
                    bResult[1] = 8;
                if (dDevSignal >= 20.4 & dDevSignal < 30.30)
                    bResult[1] = 9;
                if (dDevSignal >= 30.30 & dDevSignal < 42.67)
                    bResult[1] = 10;
                if (dDevSignal >= 42.67 & dDevSignal < 60)
                    bResult[1] = 11;
                if (dDevSignal >= 60 & dDevSignal < 85)
                    bResult[1] = 12;
                if (dDevSignal >= 85 & dDevSignal < 125)
                    bResult[1] = 13;
                if (dDevSignal >= 125 & dDevSignal < 200)
                    bResult[1] = 14;
                if (dDevSignal >= 200 & dDevSignal < 300)
                    bResult[1] = 15;
                if (dDevSignal >= 300 & dDevSignal < 425)
                    bResult[1] = 16;
                if (dDevSignal >= 425 & dDevSignal < 600)
                    bResult[1] = 17;
                if (dDevSignal >= 600 & dDevSignal < 850)
                    bResult[1] = 18;
                if (dDevSignal >= 850)
                    bResult[1] = 19;

                bResult[2] = 0;
            }


            // для ЧМ-2 несущ назначить ЧМ-2
            if (iModeSignal == 3)
            {
                bResult[0] = 2;

                if (iDevSignal < 3)
                    bResult[1] = 1;
                if (iDevSignal >= 3 & iDevSignal < 5)
                    bResult[1] = 2;
                if (iDevSignal >= 5 & iDevSignal < 7)
                    bResult[1] = 3;
                if (iDevSignal >= 7 & iDevSignal < 9)
                    bResult[1] = 4;
                if (iDevSignal >= 9 & iDevSignal < 11)
                    bResult[1] = 5;
                if (iDevSignal >= 11 & iDevSignal < 13)
                    bResult[1] = 6;
                if (iDevSignal >= 13 & iDevSignal < 15)
                    bResult[1] = 7;
                if (iDevSignal >= 15 & iDevSignal < 17)
                    bResult[1] = 8;
                if (iDevSignal >= 17 & iDevSignal < 19)
                    bResult[1] = 9;
                if (iDevSignal >= 19 & iDevSignal < 21)
                    bResult[1] = 10;
                if (iDevSignal >= 21 & iDevSignal < 23)
                    bResult[1] = 11;
                if (iDevSignal >= 23)
                    bResult[1] = 12;

                double dManSignal = 1 / (Convert.ToDouble(iManSignal));
                dManSignal = dManSignal / 1000000;

                if (dManSignal < 93.75)
                    bResult[2] = 1;
                if (dManSignal >= 93.75 & dManSignal < 187.5)
                    bResult[2] = 2;
                if (dManSignal >= 187.5 & dManSignal < 375)
                    bResult[2] = 3;
                if (dManSignal >= 375 & dManSignal < 750)
                    bResult[2] = 4;
                if (dManSignal >= 750 & dManSignal < 1500)
                    bResult[2] = 5;
                if (dManSignal >= 1500)
                    bResult[2] = 6;
            }

            // для ФМн-2 назначить Фмн-2
            if (iModeSignal == 10 | iModeSignal == 13 |
                iModeSignal == 14 | iModeSignal == 15 |
                iModeSignal == 16)
            {
                bResult[0] = 5;

                double dManSignal = 1 / (Convert.ToDouble(iManSignal));
                dManSignal = dManSignal / 1000000;


                if (dManSignal < 0.35)
                    bResult[2] = 1;
                if (dManSignal >= 0.35 & dManSignal < 0.75)
                    bResult[2] = 2;
                if (dManSignal >= 0.75 & dManSignal < 3)
                    bResult[2] = 3;
                if (dManSignal >= 3 & dManSignal < 7.5)
                    bResult[2] = 4;
                if (dManSignal >= 7.5 & dManSignal < 15)
                    bResult[2] = 5;
                if (dManSignal >= 15 & dManSignal < 30)
                    bResult[2] = 6;
                if (dManSignal >= 30 & dManSignal < 60)
                    bResult[2] = 7;
                if (dManSignal >= 60 & dManSignal < 240)
                    bResult[2] = 8;
                if (dManSignal >= 240)
                    bResult[2] = 9;

                bResult[1] = 0;
            }

            // для ФМн-4 назначить Фмн-4
            if (iModeSignal == 11)
            {
                bResult[0] = 6;

                double dManSignal = 1 / (Convert.ToDouble(iManSignal));
                dManSignal = dManSignal / 1000000;


                if (dManSignal < 0.35)
                    bResult[2] = 1;
                if (dManSignal >= 0.35 & dManSignal < 0.75)
                    bResult[2] = 2;
                if (dManSignal >= 0.75 & dManSignal < 3)
                    bResult[2] = 3;
                if (dManSignal >= 3 & dManSignal < 7.5)
                    bResult[2] = 4;
                if (dManSignal >= 7.5 & dManSignal < 15)
                    bResult[2] = 5;
                if (dManSignal >= 15 & dManSignal < 30)
                    bResult[2] = 6;
                if (dManSignal >= 30 & dManSignal < 60)
                    bResult[2] = 7;
                if (dManSignal >= 60 & dManSignal < 240)
                    bResult[2] = 8;
                if (dManSignal >= 240)
                    bResult[2] = 9;

                bResult[1] = 0;
            }

            // для ФМн-8 назначить Фмн-8
            if (iModeSignal == 12)
            {
                bResult[0] = 7;

                double dManSignal = 1 / (Convert.ToDouble(iManSignal));
                dManSignal = dManSignal / 1000000;


                if (dManSignal < 0.35)
                    bResult[2] = 1;
                if (dManSignal >= 0.35 & dManSignal < 0.75)
                    bResult[2] = 2;
                if (dManSignal >= 0.75 & dManSignal < 3)
                    bResult[2] = 3;
                if (dManSignal >= 3 & dManSignal < 7.5)
                    bResult[2] = 4;
                if (dManSignal >= 7.5 & dManSignal < 15)
                    bResult[2] = 5;
                if (dManSignal >= 15 & dManSignal < 30)
                    bResult[2] = 6;
                if (dManSignal >= 30 & dManSignal < 60)
                    bResult[2] = 7;
                if (dManSignal >= 60 & dManSignal < 240)
                    bResult[2] = 8;
                if (dManSignal >= 240)
                    bResult[2] = 9;

                bResult[1] = 0;
            }

            // для ШПС
            if (iModeSignal == 6)
            {
                bResult[0] = 8;
                bResult[1] = 0;
                bResult[2] = 0;
            }
            //------------------------------------------------------------

            return bResult;
        }
    }
}
