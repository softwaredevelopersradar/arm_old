﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyDataGridView
{
    public class CurVal_RangeRR_RP
    {
        int dgvID;
        int dgvFreqMin;
        int dgvFreqMax;
        int dgvAngleMin;
        int dgvAngleMax;
        byte dgvTable;

        // свойство номер строки 
        public int IdDGV
        {
            get { return dgvID; }
            set { dgvID = value; }
        }

        // свойство частота min
        public int FreqMinDGV
        {
            get { return dgvFreqMin; }
            set { dgvFreqMin = value; }
        }

        // свойство частота max
        public int FreqMaxDGV
        {
            get { return dgvFreqMax; }
            set { dgvFreqMax = value; }
        }

        // свойство угол min
        public int AngleMinDGV
        {
            get { return dgvAngleMin; }
            set { dgvAngleMin = value; }
        }

        // свойство угол max
        public int AngleMaxDGV
        {
            get { return dgvAngleMax; }
            set { dgvAngleMax = value; }
        }

        // свойство режим таблицы (ведущая, ведомая)
        public byte TableDGV
        {
            get { return dgvTable; }
            set { dgvTable = value; }
        }
    }
}
