﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyDataGridView
{
    public class CurVal_SpecialFrequencies
    {
        int dgvID;
        int dgvFreqMin;
        int dgvFreqMax;
        byte dgvTable;

        // свойство номер строки 
        public int IdDGV
        {
            get { return dgvID; }
            set { dgvID = value; }
        }

        // свойство частота min
        public int FreqMinDGV
        {
            get { return dgvFreqMin; }
            set { dgvFreqMin = value; }
        }

        // свойство частота max
        public int FreqMaxDGV
        {
            get { return dgvFreqMax; }
            set { dgvFreqMax = value; }
        }

        // свойство режим таблицы (ведущая, ведомая)
        public byte TableDGV
        {
            get { return dgvTable; }
            set { dgvTable = value; }
        }
    }
}
