﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyDataGridView
{
    public class CurVal_IRI_FRCh_RP
    {
        int dgvID;
        int dgvFreq;
        int dgvBearing;
        int dgvLetter;
        string dgvParamsNoise;
        int dgvLevel;
        int dgvPrior;
        byte dgvModulation;
        byte dgvDeviation;
        byte dgvManipulation;
        byte dgvDuration;
        byte dgvTable;

        // свойство номер строки 
        public int IdDGV
        {
            get { return dgvID; }
            set { dgvID = value; }
        }

        // свойство частота
        public int FreqDGV
        {
            get { return dgvFreq; }
            set { dgvFreq = value; }
        }

        // свойство пеленг
        public int BearingDGV
        {
            get { return dgvBearing; }
            set { dgvBearing = value; }
        }

        // свойство литера
        public int LetterDGV
        {
            get { return dgvLetter; }
            set { dgvLetter = value; }
        }

        // свойство параметры помехи
        public string ParamsNoiseDGV
        {
            get { return dgvParamsNoise; }
            set { dgvParamsNoise = value; }
        }

        // свойство уровень
        public int LevelDGV
        {
            get { return dgvLevel; }
            set { dgvLevel = value; }
        }

        // свойство приоритет
        public int PriorDGV
        {
            get { return dgvPrior; }
            set { dgvPrior = value; }
        }

        // свойство модуляция
        public byte ModulationDGV
        {
            get { return dgvModulation; }
            set { dgvModulation = value; }
        }

        // свойство девиация
        public byte DeviationDGV
        {
            get { return dgvDeviation; }
            set { dgvDeviation = value; }
        }

        // свойство манипуляция
        public byte ManipulationDGV
        {
            get { return dgvManipulation; }
            set { dgvManipulation = value; }
        }

        // свойство длительность
        public byte DurationDGV
        {
            get { return dgvDuration; }
            set { dgvDuration = value; }
        }

        // свойство режим таблицы (ведущая, ведомая)
        public byte TableDGV
        {
            get { return dgvTable; }
            set { dgvTable = value; }
        }

        int curIndexRP;
        // свойство текущий индекс строрки в таблице
        public int СurIndexRP
        {
            get { return curIndexRP; }
            set { curIndexRP = value; }
        }
    }
}
