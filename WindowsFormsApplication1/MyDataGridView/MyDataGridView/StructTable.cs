﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyDataGridView
{
    public enum NameTable { IRI_FRCh, IRI_FRCh_CR, IRI_FRCh_RP, IRI_PPRCh, IRI_PPRCh2, IRI_PPRCh3, IRI_PPRCh_RP, IRI_PPRCh_RP2, IRI_PPRCh_RP_EXCLUDE,
        AR_ONE, RANGE_RR, RANGE_RP, FREQ_FORBIDDEN, FREQ_KNOWN, FREQ_IMPORTANT, ADSB_RECEIVER, ARDV1, AR6000, DEFAULT };
    public enum TypeColumn { TextBoxColumn, ImageColumn, CheckBoxColumn, ComboBoxColumn, ButtonColumn, LinkColumn };

    public enum Table { Default = 0, Own = 1, Linked = 2 }

    #region Структура полей таблицы
    public struct TableColumn
    {
        public string fields;                  // поля таблицы
        public int widthField;                 // ширина полей таблицы
        public bool bVisible;                  // видимый/невидимый столбец
        public TypeColumn typeColumn;          // тип столбца
        public DataGridViewTriState resizable; // изменение размера столбца
        public string nameField;               // имя поля
    };
    #endregion

    public struct Struct_IRI_FRCh
    {
        public int iID;             // номер записи
        public int iFreq;           // частота
        public int iQ1;             // пеленг 1
        public int iQ2;             // пеленг 2
        public int iLevel;          // уровень
        public int iDFreq;          // ширина полосы
        public int iCKO;            // СКО
        public double dLatitude;    // широта
        public double dLongitude;   // долгота
        public byte bView;          // вид
        public string sSP;          // станция помех
        public string sTime;        // время
        public int iDurationSignal; // длительность сигнала в эфире
        public int iCountExits;     // количество выходов в эфир
        public byte bKR;            // индикатор появления (0 - есть, 1 - нет)
    }

    #region Названия полей в таблицах
    public struct TableFieldHeader //FieldsTable
    {
        public string empty;
        public string id;
        public string idFRCh;
        public string F;
        public string Q1;
        public string Q2;
        public string U;
        public string level;
        public string deltaF;
        public string CKO;
        public string latitude;
        public string longitude;
        public string altitude;
        public string view;
        public string SP;
        public string time;
        public string durationSignal;
        public string countExits;
        public string SP_RR;
        public string SP_RP;
        public string Q;
        public string litera;
        public string paramsNoise;
        public string priority;
        public string KR;
        public string RP;
        public string izl;
        public string Fmin;
        public string Fmax;
        public string step;
        public string durationImpulses;
        public string countIRI;
        public string CR;
        public string sound;
        public string OnOff;
        public string note;
        public string modulation;
        public string deviation;
        public string manipulation;
        public string duration;
        public string mode;
        public string angleMin;
        public string angleMax;
        public string pause;
        public string ampl;
        public string attenuator;
        public string BW;
        public string HPF;
        public string LPF;
        public string ICAO;
        public string datetime;
        public string epo;

        public void initTableFieldHeader()
        {
            this.empty = "";
            this.id = "№";
            this.idFRCh = "№ФРЧ";
            this.F = "Частота, кГц";
            this.Q1 = "Пел.1,°";
            this.Q2 = "Пел.2,°";
            this.U = "Порог, дБ";
            this.level = "Уровень, дБ";
            this.deltaF = "Шир. пол., кГц";
            this.CKO = "СКО";
            this.latitude = "Широта,°";
            this.longitude = "Долгота,°";
            this.view = "Вид";
            this.SP = "СП";
            this.time = "Время";
            this.durationSignal = "Длит., мс";
            this.countExits = "К вых.";
            this.SP_RR = "СП РР";
            this.SP_RP = "СП РП";
            this.Q = "Пел.,°";
            this.litera = "Лит.";
            this.paramsNoise = "Параметры помехи";
            this.priority = "Пр.";
            this.KR = "КР";
            this.RP = "РП";
            this.izl = "Изл.";
            this.Fmin = "Част. мин., кГц";
            this.Fmax = "Част. макс., кГц";
            this.angleMin = "Угол мин.,°";
            this.angleMax = "Угол макс.,°";
            this.step = "Шаг, кГц";
            this.durationImpulses = "Длит. имп., мс";// "Кол. частот";
            this.countIRI = "Кол. ИРИ";
            this.CR = "ЦР";
            this.sound = "Запись";
            this.OnOff = "Вкл.";
            this.note = "Прим.";
            this.modulation = "Модул.";
            this.deviation = "Дев.";
            this.manipulation = "Манип.";
            this.duration = "Длит.";
            this.mode = "Режим";
            this.pause = "Пауза";
            this.ampl = "Усил.";
            this.attenuator = "Аттен.";
            this.BW = "Пол.";
            this.HPF = "ФВЧ";
            this.LPF = "ФНЧ";
            this.altitude = "Высота, м";
            this.ICAO = "№ ICAO воздушного судна";
            this.datetime = "Дата, время";
            this.epo = "ЕПО";
        }
    }
#endregion
    
}
