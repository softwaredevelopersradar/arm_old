﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyDataGridView
{
    #region Имена полей в таблицах (для перевода)
    public struct STableFieldName
    {
        public string empty;
        public string id;
        public string idFRCh;
        public string F;
        public string Q1;
        public string Q2;
        public string U;
        public string level;
        public string deltaF;
        public string CKO;
        public string latitude;
        public string longitude;
        public string altitude;
        public string view;
        public string SP;
        public string time;
        public string durationSignal;
        public string countExits;
        public string SP_RR;
        public string SP_RP;
        public string Q;
        public string litera;
        public string paramsNoise;
        public string priority;
        public string KR;
        public string RP;
        public string izl;
        public string Fmin;
        public string Fmax;
        public string step;
        public string durationImpulses;
        public string countIRI;
        public string CR;
        public string sound;
        public string OnOff;
        public string note;
        public string modulation;
        public string deviation;
        public string manipulation;
        public string duration;
        public string mode;
        public string angleMin;
        public string angleMax;
        public string pause;
        public string ampl;
        public string attenuator;
        public string BW;
        public string HPF;
        public string LPF;
        public string ICAO;
        public string datetime;
        public string epo;

        public void initTableFieldName()
        {
            this.empty = "empty";
            this.id = "id";
            this.idFRCh = "idFRCh";
            this.F = "F";
            this.Q1 = "Q1";
            this.Q2 = "Q2";
            this.U = "U";
            this.level = "level";
            this.deltaF = "deltaF";
            this.CKO = "CKO";
            this.latitude = "latitude";
            this.longitude = "longitude";
            this.view = "view";
            this.SP = "SP";
            this.time = "time";
            this.durationSignal = "durationSignal";
            this.countExits = "countExits";
            this.SP_RR = "SP_RR";
            this.SP_RP = "SP_RP";
            this.Q = "Q";
            this.litera = "litera";
            this.paramsNoise = "paramsNoise";
            this.priority = "priority";
            this.KR = "KR";
            this.RP = "RP";
            this.izl = "izl";
            this.Fmin = "Fmin";
            this.Fmax = "Fmax";
            this.angleMin = "angleMin";
            this.angleMax = "angleMax";
            this.step = "step";
            this.durationImpulses = "durationImpulses";// "Длит. имп., мс";
            this.countIRI = "countIRI";
            this.CR = "CR";
            this.sound = "sound";
            this.OnOff = "OnOff";
            this.note = "note";
            this.modulation = "modulation";
            this.deviation = "deviation";
            this.manipulation = "manipulation";
            this.duration = "duration";
            this.mode = "mode";
            this.pause = "pause";
            this.ampl = "ampl";
            this.attenuator = "attenuator";
            this.BW = "BW";
            this.HPF = "HPF";
            this.LPF = "LPF";
            this.altitude = "altitude";
            this.ICAO = "ICAO";
            this.datetime = "datetime";
            this.epo = "epo";
        }
    }
    #endregion

    #region Сообщения об ошибках
    public struct SMessageError
    {
        public static string mesErr;
        public static string mesErrAddRecord;
        public static string mesErrReadDataTable;
        public static string mesErrDeleteRecord;
        public static string mesErrDeleteRecords;
        public static string mesErrChangeRecord;
        public static string mesErrReadDataOwn;
        public static string mesErrReadDataLinked;
        public static string mesErrFileDBNotFound;

        public static string mesErrIncorrSetFreq;
        public static string mesErrIncorrSetManip;
        public static string mesErrFreqWithoutRange;
        public static string mesErrIncorrSetPel;
        public static string mesErrPelWithoutRange;
        public static string mesErrIncorrSetPrior;
        public static string mesErrIncorrSetLevel;
       
        public static void initMessageError()
        {
            mesErr = "Ошибка!";
            mesErrAddRecord = "Ошибка добавления записи!";
            mesErrReadDataTable = "Ошибка чтения данных таблицы!";
            mesErrDeleteRecord = "Ошибка удаления записи!";
            mesErrDeleteRecords = "Ошибка удаления записей!";
            mesErrChangeRecord = "Ошибка изменения записи!";
            mesErrReadDataOwn = "Ошибка чтения данных таблицы ведущей СП!";
            mesErrReadDataLinked = "Ошибка чтения данных таблицы ведомой СП!";
            mesErrFileDBNotFound = "Файл базы данных database.db не найден!";

            mesErrIncorrSetFreq = "Неверно задана частота!";
            mesErrIncorrSetManip = "Для выбранной девиации значении манипуляции должно быть больше!";
            mesErrFreqWithoutRange = "Частота вне диапазона работы станции!";
            mesErrIncorrSetPel = "Неверно задан пеленг!";
            mesErrPelWithoutRange = "Пеленг вне диапазона!";
            mesErrIncorrSetPrior = "Неверно задан приоритет!";
            mesErrIncorrSetLevel =  "Неверно задан порог!";
        }
    }
    #endregion

    #region Значения
    public struct SMeaning
    {
        public static string meaningOwn;
        public static string meaningLinked;
        public static string meaningkHz;
        public static string meaningmks;
        public static string meaningms;
        public static string meaningMHz;

        public static string meaningShum;
        public static string meaningNes;
        public static string meaningAMn;
        public static string meaningFMn;
        public static string meaningChMn2;
        public static string meaningAMChM;
        public static string meaningChMn4;
        public static string meaningChMn8;
        public static string meaningChM;
        public static string meaningChMn;
        public static string meaningShPS; 
        public static string meaningUnkn;

        public static void initMeaning()
        {
            meaningOwn = "Ведущая";
            meaningLinked = "Ведомая";
            meaningkHz = "кГц";
            meaningmks = "мкс";
            meaningms = "мс";
            meaningMHz = "МГц";

            meaningShum = "Шум";
            meaningNes = "Несущая";
            meaningAMn = "АМн";
            meaningFMn = "ФМн";
            meaningChMn2 = "ЧМн2";
            meaningAMChM = "АМ ЧМ";
            meaningChMn4 = "ЧМн4";
            meaningChMn8 = "ЧМн8";
            meaningChM = "ЧМ";
            meaningChMn = "ЧМн";
            meaningShPS = "ШПС";
            meaningUnkn = "Неизв";
        }
    }
    #endregion

    #region Сообщения
    public struct SMessages
    {
        public static string mesMessage;
        public static string mesRange;
        public static string mesAndRange;
        public static string mesSector;
        public static string mesContainSetSector;
        public static string mesCrossingRange;
        public static string mesFreq;
        public static string mesExist;
        public static string mesValuesMaxMin;
        public static string mesFreqWithoutRangeRP;
        public static string mesFreqForbidNotAdd;

        public static void initMessages()
        {
            mesMessage = "Сообщение!";
            mesRange = "Диапазон ";
            mesAndRange = " и диапазон ";
            mesSector = "Сектор ";
            mesContainSetSector = " содержит значения заданного сектора!";
            mesCrossingRange = " пересекается с заданным диапазоном! Объедините диапазоны или разбейте их на несколько!";
            mesFreq = "Частота ";
            mesExist = " уже существует!";
            mesValuesMaxMin = "Значение поля 'Частота мин.' должно быть меньше значения поля 'Частота макс.'! Проверьте правильность ввода значений!";
            mesFreqWithoutRangeRP = "Частоты не принадлежат диапазону РП!";
            mesFreqForbidNotAdd = "Частоты не могут быть добавлены, так как запрещенные!";
        }
    }
    #endregion

    #region Параметры
    public struct SParams
    {
        public static string paramChMSh;
        public static string paramChM2;
        public static string paramChM4;
        public static string paramChM8;
        public static string paramFMn;
        public static string paramFMn4;
        public static string paramFMn8;
        public static string paramZSh;

        public static void initParams()
        {
            paramChMSh = "ЧМШ";
            paramChM2 = "ЧМ-2";
            paramChM4 = "ЧМ-4";
            paramChM8 = "ЧМ-8";
            paramFMn = "ФМн";
            paramFMn4 = "ФМн-4";
            paramFMn8 = "ФМн-8";
            paramZSh = "ЗШ";
        }
    }
    #endregion
}
