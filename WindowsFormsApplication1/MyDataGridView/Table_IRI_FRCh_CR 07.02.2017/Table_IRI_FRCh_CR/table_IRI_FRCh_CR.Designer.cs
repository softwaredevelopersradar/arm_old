﻿namespace Table_IRI_FRCh_CR
{
    partial class table_IRI_FRCh_CR
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbVedomCR = new System.Windows.Forms.TextBox();
            this.tbVedCR = new System.Windows.Forms.TextBox();
            this.tbAll = new System.Windows.Forms.TextBox();
            this.lVedomCR = new System.Windows.Forms.Label();
            this.lVedCR = new System.Windows.Forms.Label();
            this.lAll = new System.Windows.Forms.Label();
            this.bIRI_RP = new System.Windows.Forms.Button();
            this.bCR = new System.Windows.Forms.Button();
            this.bDelete = new System.Windows.Forms.Button();
            this.bClear = new System.Windows.Forms.Button();
            this.pLabels = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.bIRI_CR_RP = new System.Windows.Forms.Button();
            this.bRN = new System.Windows.Forms.Button();
            this.bUS = new System.Windows.Forms.Button();
            this.bRS = new System.Windows.Forms.Button();
            this.bClearCR = new System.Windows.Forms.Button();
            this.bClearRSUS = new System.Windows.Forms.Button();
            this.dgvIRI_FRCh_CR = new MyDataGridView.myDataGridView();
            this.pLabels.SuspendLayout();
            this.SuspendLayout();
            // 
            // tbVedomCR
            // 
            this.tbVedomCR.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.tbVedomCR.Location = new System.Drawing.Point(411, 5);
            this.tbVedomCR.Name = "tbVedomCR";
            this.tbVedomCR.ReadOnly = true;
            this.tbVedomCR.Size = new System.Drawing.Size(50, 20);
            this.tbVedomCR.TabIndex = 37;
            this.tbVedomCR.Text = "0";
            // 
            // tbVedCR
            // 
            this.tbVedCR.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.tbVedCR.Location = new System.Drawing.Point(255, 5);
            this.tbVedCR.Name = "tbVedCR";
            this.tbVedCR.ReadOnly = true;
            this.tbVedCR.Size = new System.Drawing.Size(50, 20);
            this.tbVedCR.TabIndex = 36;
            this.tbVedCR.Text = "0";
            // 
            // tbAll
            // 
            this.tbAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.tbAll.Location = new System.Drawing.Point(56, 5);
            this.tbAll.Name = "tbAll";
            this.tbAll.ReadOnly = true;
            this.tbAll.Size = new System.Drawing.Size(50, 20);
            this.tbAll.TabIndex = 35;
            this.tbAll.Text = "0";
            // 
            // lVedomCR
            // 
            this.lVedomCR.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lVedomCR.AutoSize = true;
            this.lVedomCR.ForeColor = System.Drawing.Color.Navy;
            this.lVedomCR.Location = new System.Drawing.Point(318, 12);
            this.lVedomCR.Name = "lVedomCR";
            this.lVedomCR.Size = new System.Drawing.Size(100, 13);
            this.lVedomCR.TabIndex = 33;
            this.lVedomCR.Text = "Ведомая (ЦР)........";
            // 
            // lVedCR
            // 
            this.lVedCR.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lVedCR.AutoSize = true;
            this.lVedCR.ForeColor = System.Drawing.Color.Navy;
            this.lVedCR.Location = new System.Drawing.Point(162, 12);
            this.lVedCR.Name = "lVedCR";
            this.lVedCR.Size = new System.Drawing.Size(100, 13);
            this.lVedCR.TabIndex = 32;
            this.lVedCR.Text = "Ведущая (ЦР)........";
            // 
            // lAll
            // 
            this.lAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lAll.AutoSize = true;
            this.lAll.ForeColor = System.Drawing.Color.Navy;
            this.lAll.Location = new System.Drawing.Point(3, 12);
            this.lAll.Name = "lAll";
            this.lAll.Size = new System.Drawing.Size(61, 13);
            this.lAll.TabIndex = 31;
            this.lAll.Text = "Всего........";
            // 
            // bIRI_RP
            // 
            this.bIRI_RP.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bIRI_RP.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.bIRI_RP.Location = new System.Drawing.Point(164, 310);
            this.bIRI_RP.Name = "bIRI_RP";
            this.bIRI_RP.Size = new System.Drawing.Size(75, 23);
            this.bIRI_RP.TabIndex = 30;
            this.bIRI_RP.Text = "ИРИ на РП";
            this.bIRI_RP.UseVisualStyleBackColor = true;
            this.bIRI_RP.Click += new System.EventHandler(this.bIRI_RP_Click);
            // 
            // bCR
            // 
            this.bCR.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bCR.ForeColor = System.Drawing.Color.Green;
            this.bCR.Location = new System.Drawing.Point(250, 310);
            this.bCR.Name = "bCR";
            this.bCR.Size = new System.Drawing.Size(122, 23);
            this.bCR.TabIndex = 29;
            this.bCR.Text = "Целераспределение";
            this.bCR.UseVisualStyleBackColor = true;
            this.bCR.Click += new System.EventHandler(this.bCR_Click);
            // 
            // bDelete
            // 
            this.bDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bDelete.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.bDelete.Location = new System.Drawing.Point(79, 310);
            this.bDelete.Name = "bDelete";
            this.bDelete.Size = new System.Drawing.Size(75, 23);
            this.bDelete.TabIndex = 28;
            this.bDelete.Text = "Удалить";
            this.bDelete.UseVisualStyleBackColor = true;
            this.bDelete.Click += new System.EventHandler(this.bDelete_Click);
            // 
            // bClear
            // 
            this.bClear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bClear.ForeColor = System.Drawing.Color.Red;
            this.bClear.Location = new System.Drawing.Point(3, 310);
            this.bClear.Name = "bClear";
            this.bClear.Size = new System.Drawing.Size(75, 23);
            this.bClear.TabIndex = 27;
            this.bClear.Text = "Очистить";
            this.bClear.UseVisualStyleBackColor = true;
            this.bClear.Click += new System.EventHandler(this.bClear_Click);
            // 
            // pLabels
            // 
            this.pLabels.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pLabels.BackColor = System.Drawing.SystemColors.Control;
            this.pLabels.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pLabels.Controls.Add(this.button1);
            this.pLabels.Controls.Add(this.tbAll);
            this.pLabels.Controls.Add(this.tbVedomCR);
            this.pLabels.Controls.Add(this.tbVedCR);
            this.pLabels.Controls.Add(this.lAll);
            this.pLabels.Controls.Add(this.lVedomCR);
            this.pLabels.Controls.Add(this.lVedCR);
            this.pLabels.Location = new System.Drawing.Point(0, 275);
            this.pLabels.Name = "pLabels";
            this.pLabels.Size = new System.Drawing.Size(798, 30);
            this.pLabels.TabIndex = 38;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(566, 2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(112, 23);
            this.button1.TabIndex = 38;
            this.button1.Text = "Запрос ИРИ ФРЧ";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // bIRI_CR_RP
            // 
            this.bIRI_CR_RP.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bIRI_CR_RP.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.bIRI_CR_RP.Location = new System.Drawing.Point(464, 310);
            this.bIRI_CR_RP.Name = "bIRI_CR_RP";
            this.bIRI_CR_RP.Size = new System.Drawing.Size(92, 23);
            this.bIRI_CR_RP.TabIndex = 39;
            this.bIRI_CR_RP.Text = "ИРИ ЦР на РП";
            this.bIRI_CR_RP.UseVisualStyleBackColor = true;
            this.bIRI_CR_RP.Click += new System.EventHandler(this.bIRI_CR_RP_Click);
            // 
            // bRN
            // 
            this.bRN.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bRN.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.bRN.Location = new System.Drawing.Point(761, 310);
            this.bRN.Name = "bRN";
            this.bRN.Size = new System.Drawing.Size(32, 23);
            this.bRN.TabIndex = 40;
            this.bRN.Text = "РН";
            this.bRN.UseVisualStyleBackColor = true;
            this.bRN.Visible = false;
            // 
            // bUS
            // 
            this.bUS.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bUS.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.bUS.Location = new System.Drawing.Point(600, 310);
            this.bUS.Name = "bUS";
            this.bUS.Size = new System.Drawing.Size(32, 23);
            this.bUS.TabIndex = 41;
            this.bUS.Text = "УС";
            this.bUS.UseVisualStyleBackColor = true;
            this.bUS.Click += new System.EventHandler(this.bUS_Click);
            // 
            // bRS
            // 
            this.bRS.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bRS.ForeColor = System.Drawing.Color.Purple;
            this.bRS.Location = new System.Drawing.Point(567, 310);
            this.bRS.Name = "bRS";
            this.bRS.Size = new System.Drawing.Size(32, 23);
            this.bRS.TabIndex = 42;
            this.bRS.Text = "РС";
            this.bRS.UseVisualStyleBackColor = true;
            this.bRS.Click += new System.EventHandler(this.bRS_Click);
            // 
            // bClearCR
            // 
            this.bClearCR.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bClearCR.ForeColor = System.Drawing.Color.Red;
            this.bClearCR.Location = new System.Drawing.Point(373, 310);
            this.bClearCR.Name = "bClearCR";
            this.bClearCR.Size = new System.Drawing.Size(90, 23);
            this.bClearCR.TabIndex = 43;
            this.bClearCR.Text = "Очистить ЦР";
            this.bClearCR.UseVisualStyleBackColor = true;
            this.bClearCR.Click += new System.EventHandler(this.bClearCR_Click);
            // 
            // bClearRSUS
            // 
            this.bClearRSUS.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bClearRSUS.ForeColor = System.Drawing.Color.Red;
            this.bClearRSUS.Location = new System.Drawing.Point(633, 310);
            this.bClearRSUS.Name = "bClearRSUS";
            this.bClearRSUS.Size = new System.Drawing.Size(112, 23);
            this.bClearRSUS.TabIndex = 44;
            this.bClearRSUS.Text = "Очистить РС/УС ";
            this.bClearRSUS.UseVisualStyleBackColor = true;
            this.bClearRSUS.Click += new System.EventHandler(this.bClearRSUS_Click);
            // 
            // dgvIRI_FRCh_CR
            // 
            this.dgvIRI_FRCh_CR.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvIRI_FRCh_CR.Location = new System.Drawing.Point(0, 0);
            this.dgvIRI_FRCh_CR.Name = "dgvIRI_FRCh_CR";
            this.dgvIRI_FRCh_CR.Size = new System.Drawing.Size(798, 275);
            this.dgvIRI_FRCh_CR.TabIndex = 0;
            // 
            // table_IRI_FRCh_CR
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.bClearRSUS);
            this.Controls.Add(this.bClearCR);
            this.Controls.Add(this.bRS);
            this.Controls.Add(this.bUS);
            this.Controls.Add(this.bRN);
            this.Controls.Add(this.bIRI_CR_RP);
            this.Controls.Add(this.pLabels);
            this.Controls.Add(this.bIRI_RP);
            this.Controls.Add(this.bCR);
            this.Controls.Add(this.bDelete);
            this.Controls.Add(this.bClear);
            this.Controls.Add(this.dgvIRI_FRCh_CR);
            this.Name = "table_IRI_FRCh_CR";
            this.Size = new System.Drawing.Size(798, 337);
            this.pLabels.ResumeLayout(false);
            this.pLabels.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lVedomCR;
        private System.Windows.Forms.Label lVedCR;
        private System.Windows.Forms.Label lAll;
        public MyDataGridView.myDataGridView dgvIRI_FRCh_CR;
        public System.Windows.Forms.TextBox tbVedomCR;
        public System.Windows.Forms.TextBox tbVedCR;
        public System.Windows.Forms.TextBox tbAll;
        public System.Windows.Forms.Button bIRI_RP;
        public System.Windows.Forms.Button bCR;
        public System.Windows.Forms.Button bDelete;
        public System.Windows.Forms.Button bClear;
        private System.Windows.Forms.Panel pLabels;
        public System.Windows.Forms.Button bIRI_CR_RP;
        public System.Windows.Forms.Button bRN;
        public System.Windows.Forms.Button bUS;
        public System.Windows.Forms.Button bRS;
        public System.Windows.Forms.Button bClearCR;
        public System.Windows.Forms.Button bClearRSUS;
        private System.Windows.Forms.Button button1;
    }
}
