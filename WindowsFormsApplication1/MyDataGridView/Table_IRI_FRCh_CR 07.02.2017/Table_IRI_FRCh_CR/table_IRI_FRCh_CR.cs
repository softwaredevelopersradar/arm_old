﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using MyDataGridView;
using System.Data.SQLite;
using VariableDynamic;
using USR_DLL;
using VariableStatic;

namespace Table_IRI_FRCh_CR
{
    public partial class table_IRI_FRCh_CR: UserControl
    {
        FunctionsDB functionsDB;
        FunctionsDGV functionsDGV;
        Functions functions;
        FunctionsTranslate functionsTranslate;

        InfoMessages infoMessage;

        FuncDB_IRI_FRCh_CR funcDB_IRI_FRCh_CR;
        FuncDGV_IRI_FRCh_CR funcDGV_IRI_FRCh_CR;
        FuncDB_IRI_FRCh_RP funcDB_IRI_FRCh_RP;
        FuncVW_IRI_FRCh_CR funcVW_IRI_FRCh_CR;

        VariableWork variableWork;
        VariableSuppression variableSuppression;
        VariableCommon variableCommon;
        FunctionsUser functionsUser;
        TDistribFWS DistribFWS;

        const Int16 constLevel = -130;

        public table_IRI_FRCh_CR()
        {
            InitializeComponent();

            functionsDB = new FunctionsDB();
            functionsDGV = new FunctionsDGV();
            functions = new Functions();
            functionsTranslate = new FunctionsTranslate();

            variableWork = new VariableWork();
            variableSuppression = new VariableSuppression();
            variableCommon = new VariableCommon();
            functionsUser = new FunctionsUser();
            DistribFWS = new TDistribFWS();

            funcDB_IRI_FRCh_CR = new FuncDB_IRI_FRCh_CR();
            funcDGV_IRI_FRCh_CR = new FuncDGV_IRI_FRCh_CR();
            funcDB_IRI_FRCh_RP = new FuncDB_IRI_FRCh_RP();
            funcVW_IRI_FRCh_CR = new FuncVW_IRI_FRCh_CR();

            infoMessage = new InfoMessages();

            dgvIRI_FRCh_CR.InitTableDB(dgvIRI_FRCh_CR.dgv, NameTable.IRI_FRCh_CR);

            ////////////////////////////////////////OTL
            //functionsDB.LoadTableFromDB(dgvIRI_FRCh_CR.dgv, NameTable.IRI_FRCh_CR, 0);
            ////////////////////////////////////////OTL

            VariableCommon.OnChangeCommonLanguage += new VariableCommon.ChangeCommonEventHandler(VariableCommon_OnChangeCommonLanguage);

            VariableWork.OnChangeDistribFWS += new VariableWork.ChangeVariableEventHandler(VariableWork_OnChangeDistribFWS);

            // 
            VariableWork.aWPtoBearingDSPprotocolNew.GetASPRadioSourcesUpdate += new AWPtoBearingDSPprotocolNew.GetASPRadioSourcesEventHandler(AWPtoBearingDSPprotocolNew_GetASPRadioSourcesUpdate);

            //
            VariableWork.aWPtoBearingDSPprotocolNew.PushASPRadioSourcesUpdate += new AWPtoBearingDSPprotocolNew.PushASPRadioSourcesEventHandler(AWPtoBearingDSPprotocolNew_PushASPRadioSourcesUpdate);

            VariableDynamic.VariableWork.OnChangeCheckIridiumInmarsat += new VariableWork.ChangeCheckIridiumInmarsatEventHandler(VariableWork_OnChangeCheckIridiumInmarsat);

            VariableWork.OnDelFRCh_CR += VariableWork_OnDelFRCh_CR;
        }

        void VariableWork_OnDelFRCh_CR(int iID_FRCh_CR)
        {
            try
            {
                funcVW_IRI_FRCh_CR.DeleteOneIRI_FRCh_CRToVariableWork(dgvIRI_FRCh_CR.dgv, variableWork, iID_FRCh_CR);
            }
            catch { }
        }

        private bool bCheckIridiumInmarsat = false;
        void VariableWork_OnChangeCheckIridiumInmarsat(int iCheckIridiumInmarsat)
        {
            try
            {
                if (iCheckIridiumInmarsat > 0) bCheckIridiumInmarsat = true;
                else bCheckIridiumInmarsat = false;
            }
            catch { }
        }

        private void AWPtoBearingDSPprotocolNew_PushASPRadioSourcesUpdate(Protocols.GetRadioSourcesResponse answer)
        {
            if (InvokeRequired)
            {
                Invoke((MethodInvoker)(() => AWPtoBearingDSPprotocolNew_PushASPRadioSourcesUpdate(answer)));
                return;
            }

            if (variableCommon.Role == 1)
            {
                List<TDistribFWS> tempVW = variableWork.DistribFWS.ToList();

                for (int i = 0; i < answer.RadioSources.Length; i++)
                {
                    TDistribFWS temp = new TDistribFWS();
                    temp.iID = answer.RadioSources[i].Id;
                    temp.iFreq = answer.RadioSources[i].Frequency;
                    temp.sBearing1 = answer.RadioSources[i].Direction;
                    temp.sBearing2 = answer.RadioSources[i].Direction2;
                    temp.sLevel = (short)((-1) * answer.RadioSources[i].Amplitude);
                    temp.iDFreq = answer.RadioSources[i].Bandwidth;
                    if (answer.RadioSources[i].StandardDeviation == 1800)
                        temp.iCKO = (int)(answer.RadioSources[i].StandardDeviation / 100);
                    else
                        temp.iCKO = (int)answer.RadioSources[i].StandardDeviation;
                    temp.dLatitude = answer.RadioSources[i].Latitude;
                    temp.dLongitude = answer.RadioSources[i].Longitude;
                    temp.bView = answer.RadioSources[i].Modulation;
                    temp.iSP_RR = 2;

                    tempVW.Add(temp);
                }

                variableWork.DistribFWS = tempVW.ToArray();
            }
        }

        private async void AWPtoBearingDSPprotocolNew_GetASPRadioSourcesUpdate(Protocols.DefaultMessage answer)
        {
            if (variableCommon.Role == 2)
            {
                int N = functionsDGV.AmountRecordsDGV(dgvIRI_FRCh_CR.dgv);

                var radioSources = new Protocols.RadioSource[N];

                for (int i = 0; i < N; i++)
                {
                    radioSources[i].Id = variableWork.DistribFWS[i].iID;
                    radioSources[i].Frequency = variableWork.DistribFWS[i].iFreq;
                    radioSources[i].Direction = variableWork.DistribFWS[i].sBearing1;
                    radioSources[i].Direction2 = variableWork.DistribFWS[i].sBearing2;
                    radioSources[i].Amplitude = (byte)((-1) * variableWork.DistribFWS[i].sLevel);
                    radioSources[i].Bandwidth = variableWork.DistribFWS[i].iDFreq;
                    radioSources[i].StandardDeviation = (short)variableWork.DistribFWS[i].iCKO;
                    radioSources[i].Latitude = variableWork.DistribFWS[i].dLatitude;
                    radioSources[i].Longitude = variableWork.DistribFWS[i].dLongitude;
                    radioSources[i].Modulation = variableWork.DistribFWS[i].bView;
                    radioSources[i].Time = new Protocols.DetectionTime();
                }

                var answerPush = await VariableDynamic.VariableWork.aWPtoBearingDSPprotocolNew.PushASPRadioSources(radioSources);

                bClear_Click(this, null);
            }
        }

        private void VariableCommon_OnChangeCommonLanguage()
        {
            VariableCommon variableCommon = new VariableCommon();
            switch (variableCommon.Language)
            {
                case 0:
                    functionsTranslate.SetLanguage("rus");
                    ChangeLanguage();
                    break;

                case 1:
                    functionsTranslate.SetLanguage("eng");
                    ChangeLanguage();
                    break;

                case 2:
                    functionsTranslate.SetLanguage("az");
                    ChangeLanguage();
                    break;

                default:
                    functionsTranslate.SetLanguage("rus");
                    ChangeLanguage();
                    break;
            }
        }

        /// <summary>
        /// Для инициализации языка при запуске приложения 
        /// </summary>
        /// <param name="bLanguage"></param>
        public void ChangeControlLanguage(byte bLanguage)
        {

            switch (bLanguage)
            {
                case 0:
                    functionsTranslate.SetLanguage("rus");
                    ChangeLanguage();
                    break;

                case 1:
                    functionsTranslate.SetLanguage("eng");
                    ChangeLanguage();
                    break;

                case 2:
                    functionsTranslate.SetLanguage("az");
                    ChangeLanguage();
                    break;

                default:
                    functionsTranslate.SetLanguage("rus");
                    ChangeLanguage();
                    break;
            }
        }

        /// <summary>
        /// Изменение языка интерфейса
        /// </summary>
        private void ChangeLanguage()
        {
            functionsTranslate.RenameButtons(functionsTranslate.Dictionary, this);
            functionsTranslate.RenameLabels(functionsTranslate.Dictionary, pLabels);
            functionsTranslate.RenameFieldsTables(functionsTranslate.Dictionary, dgvIRI_FRCh_CR.dgv);

            VariableWork_OnChangeDistribFWS();
        }

        void VariableWork_OnChangeDistribFWS()
        {
            if (variableWork.DistribFWS != null)
            {
                int len = variableWork.DistribFWS.Length;

                DistribFWS = new TDistribFWS();

                if (functionsDB.DeleteAllRecordsDB(NameTable.IRI_FRCh_CR, 0))
                {
                    if (dgvIRI_FRCh_CR.dgv.InvokeRequired)
                    {
                        Invoke((MethodInvoker)(() =>
                        {
                            functionsDGV.DeleteAllRecordsDGV(dgvIRI_FRCh_CR.dgv);
                            dgvIRI_FRCh_CR.SetInitialNumberOfRows(dgvIRI_FRCh_CR.dgv, dgvIRI_FRCh_CR.tableIRI_FRCh_CR, NameTable.IRI_FRCh_CR);
                        }));
                    }
                    else
                    {
                        functionsDGV.DeleteAllRecordsDGV(dgvIRI_FRCh_CR.dgv);
                        dgvIRI_FRCh_CR.SetInitialNumberOfRows(dgvIRI_FRCh_CR.dgv, dgvIRI_FRCh_CR.tableIRI_FRCh_CR, NameTable.IRI_FRCh_CR);
                    }
                }

                //functionsDGV.DeleteAllRecordsDGV(dgvIRI_FRCh_CR.dgv);
                //dgvIRI_FRCh_CR.SetInitialNumberOfRows(dgvIRI_FRCh_CR.dgv, dgvIRI_FRCh_CR.tableIRI_FRCh_CR, NameTable.IRI_FRCh_CR);

                int rpO = 0, rpL = 0;

                for (int i = 0; i < len; i++)
                {
                    DistribFWS.iID = variableWork.DistribFWS[i].iID;
                    DistribFWS.iFreq = variableWork.DistribFWS[i].iFreq;
                    DistribFWS.sBearing1 = variableWork.DistribFWS[i].sBearing1;
                    DistribFWS.sBearing2 = variableWork.DistribFWS[i].sBearing2;
                    DistribFWS.sLevel = variableWork.DistribFWS[i].sLevel;
                    DistribFWS.iDFreq = variableWork.DistribFWS[i].iDFreq;
                    DistribFWS.iCKO = variableWork.DistribFWS[i].iCKO;
                    DistribFWS.dLatitude = variableWork.DistribFWS[i].dLatitude;
                    DistribFWS.dLongitude = variableWork.DistribFWS[i].dLongitude;
                    DistribFWS.bView = variableWork.DistribFWS[i].bView;
                    DistribFWS.iSP_RR = variableWork.DistribFWS[i].iSP_RR;
                    DistribFWS.iSP_RP = variableWork.DistribFWS[i].iSP_RP;
                    if(DistribFWS.iSP_RP == 1) { rpO++; }
                    if(DistribFWS.iSP_RP == 2) { rpL++; }

                    if (funcDB_IRI_FRCh_CR.AddRecord_IRI_FRCh_CRToDB(NameTable.IRI_FRCh_CR, DistribFWS))
                    {
                        if (dgvIRI_FRCh_CR.dgv.InvokeRequired)
                        {
                            Invoke((MethodInvoker)(() => funcDGV_IRI_FRCh_CR.AddRecord_IRI_FRCh_CRToDGV(dgvIRI_FRCh_CR.dgv, NameTable.IRI_FRCh_CR, DistribFWS)));
                        }
                        else
                        {
                            funcDGV_IRI_FRCh_CR.AddRecord_IRI_FRCh_CRToDGV(dgvIRI_FRCh_CR.dgv, NameTable.IRI_FRCh_CR, DistribFWS);
                        }
                    }
                    //funcDGV_IRI_FRCh_CR.AddRecord_IRI_FRCh_CRToDGV(dgvIRI_FRCh_CR.dgv, NameTable.IRI_FRCh_CR, DistribFWS);
                }

                tbAll.Text = functionsDGV.AmountRecordsDGV(dgvIRI_FRCh_CR.dgv).ToString();
                tbVedCR.Text = rpO.ToString();
                tbVedomCR.Text = rpL.ToString();
            }
        }

        public void LoadTableIRI_FRCh_CRfromDB()
        {
            List<TDistribFWS> lDistribFWS = funcDB_IRI_FRCh_CR.LoadTableIRI_FRCh_CRFromDB(NameTable.IRI_FRCh_CR);
            funcDB_IRI_FRCh_CR.AddIRI_FRCh_CRToVariableWork(lDistribFWS);

            funcDB_IRI_FRCh_CR.FindMinIDIRI_FRCh_CR(NameTable.IRI_FRCh_CR);
        }

        /// <summary>
        /// Удалить все записи из таблицы
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void bClear_Click(object sender, EventArgs e)
        {
            int count = 0;

            for (int i = 0; i < dgvIRI_FRCh_CR.dgv.RowCount; i++)
            {
                if (dgvIRI_FRCh_CR.dgv.Rows[i].Cells[0].Value == null)
                    count++;
            }

            if (count == dgvIRI_FRCh_CR.dgv.RowCount)
                return;

            int[] SignalId = new int[variableWork.DistribFWS.Length];
            for (int i = 0; i < variableWork.DistribFWS.Length; i++ )
            {
                SignalId[i] = variableWork.DistribFWS[i].iID;
            }
            await VariableWork.aWPtoBearingDSPprotocolNew.StorageAction(0, 1, SignalId); // отправить запрос на очистку ИРИ ФРЧ на сервер

            funcVW_IRI_FRCh_CR.DeleteAllIRI_FRCh_CRToVariableWork(variableWork);

            //// удалить все записи из базы данных
            //if (functionsDB.DeleteAllRecordsDB(NameTable.IRI_FRCh_CR, 0))
            //{
            //    List<TDistribFWS> lDistribFWS = funcDB_IRI_FRCh_CR.LoadTableIRI_FRCh_CRFromDB(NameTable.IRI_FRCh_CR);
            //    funcDB_IRI_FRCh_CR.AddIRI_FRCh_CRToVariableWork(lDistribFWS);
            //}
        }

        private async void bDelete_Click(object sender, EventArgs e)
        {
            //int iID = 0;

            if (dgvIRI_FRCh_CR.dgv.Rows[dgvIRI_FRCh_CR.dgv.SelectedRows[0].Index].Cells[0].Value == null)
                return;

            int[] SignalId = new int[1];
            SignalId[0] = Convert.ToInt32(dgvIRI_FRCh_CR.dgv.Rows[dgvIRI_FRCh_CR.dgv.SelectedRows[0].Index].Cells[0].Value);
            await VariableWork.aWPtoBearingDSPprotocolNew.StorageAction(0, 1, SignalId); // отправить запрос на очистку ИРИ ФРЧ на сервер

            funcVW_IRI_FRCh_CR.DeleteOneIRI_FRCh_CRToVariableWork(dgvIRI_FRCh_CR.dgv, variableWork);

            //iID = Convert.ToInt32(dgvIRI_FRCh_CR.dgv.Rows[dgvIRI_FRCh_CR.dgv.SelectedRows[0].Index].Cells[0].Value);

            //// удалить запись из базы данных
            //if (funcDB_IRI_FRCh_CR.DeleteOneRecord_IRI_FRCh_CRfromDB(iID, NameTable.IRI_FRCh_CR))
            //{
            //    List<TDistribFWS> lDistribFWS = funcDB_IRI_FRCh_CR.LoadTableIRI_FRCh_CRFromDB(NameTable.IRI_FRCh_CR);
            //    funcDB_IRI_FRCh_CR.AddIRI_FRCh_CRToVariableWork(lDistribFWS);
            //}
        }

        private void bIRI_RP_Click(object sender, EventArgs e)
        {
            if (bCheckIridiumInmarsat)
            {
                MessageBox.Show("В диапазоне литеры 8 осуществляется подавление IRIDIUM (INMARSAT)!", SMessages.mesMessage, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            TSupprFWS SupprFWS = AddIRIToRP(dgvIRI_FRCh_CR.dgv);

            if (Convert.ToString(dgvIRI_FRCh_CR.dgv.Rows[dgvIRI_FRCh_CR.dgv.SelectedRows[0].Index].Cells[10].Value) == SMeaning.meaningOwn) // ведущая СП
            {
                infoMessage.MessageErr = functionsUser.ChangeIRI_FRCh_RPToVariableWork(SupprFWS, variableWork.SupprFWS_Own.ToList(), 1, -1);

                if (infoMessage.MessageErr != "")
                {
                    MessageBox.Show(infoMessage.MessageErr, SMessages.mesMessage, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    infoMessage.MessageErr = "";
                    return;
                }
            }
            else if (Convert.ToString(dgvIRI_FRCh_CR.dgv.Rows[dgvIRI_FRCh_CR.dgv.SelectedRows[0].Index].Cells[10].Value) == SMeaning.meaningLinked) // ведомая СП
            {
                infoMessage.MessageErr = functionsUser.ChangeIRI_FRCh_RPToVariableWork(AddIRIToRP(dgvIRI_FRCh_CR.dgv), variableWork.SupprFWS_Linked.ToList(), 2, -1);

                if (infoMessage.MessageErr != "")
                {
                    MessageBox.Show(infoMessage.MessageErr, SMessages.mesMessage, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    infoMessage.MessageErr = "";
                    return;
                }
            }
        }

        private TSupprFWS AddIRIToRP(DataGridView dgv)
        {
            TSupprFWS SupprFWS = new TSupprFWS();

            if (dgv.Rows[dgv.SelectedRows[0].Index].Cells[0].Value == null)
                return SupprFWS;
            else
            {
                SupprFWS.iID = Convert.ToInt32(dgv.Rows[dgv.SelectedRows[0].Index].Cells[0].Value);

                string sFreq = functions.StrToFreq(Convert.ToString(dgv.Rows[dgv.SelectedRows[0].Index].Cells[1].Value));
                SupprFWS.iFreq = Convert.ToInt32(Convert.ToDecimal(sFreq, Functions.format) * 10);

                string sQ1 = functions.StrToFreq(Convert.ToString(dgv.Rows[dgv.SelectedRows[0].Index].Cells[2].Value));
                if (sQ1 == "-") { SupprFWS.sBearing = -1; }
                else { SupprFWS.sBearing = Convert.ToInt16(Convert.ToDecimal(sQ1, Functions.format) * 10); }

                // определить литеру
                if (SupprFWS.iFreq >= RangesLetters.FREQ_START_LETTER_1 & SupprFWS.iFreq < RangesLetters.FREQ_START_LETTER_2)
                    SupprFWS.bLetter = 1;
                if (SupprFWS.iFreq >= RangesLetters.FREQ_START_LETTER_2 & SupprFWS.iFreq < RangesLetters.FREQ_START_LETTER_3)
                    SupprFWS.bLetter = 2;
                if (SupprFWS.iFreq >= RangesLetters.FREQ_START_LETTER_3 & SupprFWS.iFreq < RangesLetters.FREQ_START_LETTER_4)
                    SupprFWS.bLetter = 3;
                if (SupprFWS.iFreq >= RangesLetters.FREQ_START_LETTER_4 & SupprFWS.iFreq < RangesLetters.FREQ_START_LETTER_5)
                    SupprFWS.bLetter = 4;
                if (SupprFWS.iFreq >= RangesLetters.FREQ_START_LETTER_5 & SupprFWS.iFreq < RangesLetters.FREQ_START_LETTER_6)
                    SupprFWS.bLetter = 5;
                if (SupprFWS.iFreq >= RangesLetters.FREQ_START_LETTER_6 & SupprFWS.iFreq < RangesLetters.FREQ_START_LETTER_7)
                    SupprFWS.bLetter = 6;
                if (variableCommon.TypeStation == 0)
                {
                    if (SupprFWS.iFreq >= RangesLetters.FREQ_START_LETTER_7 & SupprFWS.iFreq < RangesLetters.FREQ_STOP_LETTER_7)
                        SupprFWS.bLetter = 7;
                }
                else if (variableCommon.TypeStation == 1)
                {
                    if (SupprFWS.iFreq >= RangesLetters.FREQ_START_LETTER_7 & SupprFWS.iFreq < RangesLetters.FREQ_START_LETTER_8)
                        SupprFWS.bLetter = 7;
                    if (SupprFWS.iFreq >= RangesLetters.FREQ_START_LETTER_8 & SupprFWS.iFreq < RangesLetters.FREQ_START_LETTER_9)
                        SupprFWS.bLetter = 8;
                    if (SupprFWS.iFreq >= RangesLetters.FREQ_START_LETTER_9 & SupprFWS.iFreq <= RangesLetters.FREQ_STOP_LETTER_9)
                        SupprFWS.bLetter = 9;
                }

                Int16 iLevel = (Int16)dgv.Rows[dgv.SelectedRows[0].Index].Cells[4].Value;
                iLevel = (Int16)(iLevel - 10);

                if (iLevel < constLevel) { SupprFWS.sLevel = (Int16)constLevel; }
                else SupprFWS.sLevel = iLevel;
             
                SupprFWS.bPrior = 2;
                SupprFWS.bManipulation = 0;
                SupprFWS.bModulation = 1;
                SupprFWS.bDeviation = 1;
                SupprFWS.bDuration = 1;

            }

            return SupprFWS;
        }

        private void bCR_Click(object sender, EventArgs e)
        {
            TJammer[] Jammer = new TJammer[2];

            // ведущая
            Jammer[0].iNumSP = 1;
            Jammer[0].dLat = variableWork.CoordsGNSS[0].Lat; // широта
            Jammer[0].dLon = variableWork.CoordsGNSS[0].Lon; // долгота
            Jammer[0].iCountIRI = variableSuppression.CountSourceLetter; // количество ИРИ в одной литере
            Jammer[0].lettersRP = new LettersRP();
            Jammer[0].lettersRP.bLet1 = variableSuppression.OwnLetter1;
            Jammer[0].lettersRP.bLet2 = variableSuppression.OwnLetter2;
            Jammer[0].lettersRP.bLet3 = variableSuppression.OwnLetter3;
            Jammer[0].lettersRP.bLet4 = variableSuppression.OwnLetter4;
            Jammer[0].lettersRP.bLet5 = variableSuppression.OwnLetter5;
            Jammer[0].lettersRP.bLet6 = variableSuppression.OwnLetter6;
            Jammer[0].lettersRP.bLet7 = variableSuppression.OwnLetter7;
            if (variableCommon.TypeStation == 1)
            {
                Jammer[0].lettersRP.bLet8 = variableSuppression.OwnLetter8;
                Jammer[0].lettersRP.bLet9 = variableSuppression.OwnLetter9;
            }
            Jammer[0].rangesRP = new Protocols.RangeSector[variableWork.RangeSectorSupprOwn.Length];
            for (int i = 0; i < variableWork.RangeSectorSupprOwn.Length; i++)
            {
                Jammer[0].rangesRP[i].StartFrequency = variableWork.RangeSectorSupprOwn[i].StartFrequency;
                Jammer[0].rangesRP[i].EndFrequency = variableWork.RangeSectorSupprOwn[i].EndFrequency;
                Jammer[0].rangesRP[i].StartDirection = variableWork.RangeSectorSupprOwn[i].StartDirection;
                Jammer[0].rangesRP[i].EndDirection = variableWork.RangeSectorSupprOwn[i].EndDirection;
            }

            // ведомая
            Jammer[1].iNumSP = 2;
            Jammer[1].dLat = variableWork.CoordsGNSS[0].Lat; // широта
            Jammer[1].dLon = variableWork.CoordsGNSS[0].Lon; // долгота
            Jammer[1].iCountIRI = variableSuppression.CountSourceLetter; // количество ИРИ в одной литере
            Jammer[1].lettersRP = new LettersRP();
            Jammer[1].lettersRP.bLet1 = variableSuppression.JammerLinkedLetter1;
            Jammer[1].lettersRP.bLet2 = variableSuppression.JammerLinkedLetter2;
            Jammer[1].lettersRP.bLet3 = variableSuppression.JammerLinkedLetter3;
            Jammer[1].lettersRP.bLet4 = variableSuppression.JammerLinkedLetter4;
            Jammer[1].lettersRP.bLet5 = variableSuppression.JammerLinkedLetter5;
            Jammer[1].lettersRP.bLet6 = variableSuppression.JammerLinkedLetter6;
            Jammer[1].lettersRP.bLet7 = variableSuppression.JammerLinkedLetter7;
            if (variableCommon.TypeStation == 1)
            {
                Jammer[1].lettersRP.bLet8 = variableSuppression.JammerLinkedLetter8;
                Jammer[1].lettersRP.bLet9 = variableSuppression.JammerLinkedLetter9;
            }
            Jammer[1].rangesRP = new Protocols.RangeSector[variableWork.RangeSectorSupprLinked.Length];
            for (int i = 0; i < variableWork.RangeSectorSupprLinked.Length; i++)
            {
                Jammer[1].rangesRP[i].StartFrequency = variableWork.RangeSectorSupprLinked[i].StartFrequency;
                Jammer[1].rangesRP[i].EndFrequency = variableWork.RangeSectorSupprLinked[i].EndFrequency;
                Jammer[1].rangesRP[i].StartDirection = variableWork.RangeSectorSupprLinked[i].StartDirection;
                Jammer[1].rangesRP[i].EndDirection = variableWork.RangeSectorSupprLinked[i].EndDirection;
            }

            TDistribFWS[] DistribFWS = variableWork.DistribFWS.ToArray();

            // Целераспределение
            functionsUser.Distribution(Jammer, ref DistribFWS);

            funcVW_IRI_FRCh_CR.DeleteAllIRI_FRCh_CRToVariableWork(variableWork);
            funcVW_IRI_FRCh_CR.AddIRI_FRCh_CRToVariableWork(variableWork, DistribFWS);

           // if (functionsDB.DeleteAllRecordsDB(NameTable.IRI_FRCh_CR, 0))
           // {
           //     for (int i = 0; i < DistribFWS.Count(); i++)
           //     {
           //         funcDB_IRI_FRCh_CR.AddRecord_IRI_FRCh_CRToDB(NameTable.IRI_FRCh_CR, DistribFWS[i]);
           //     }             
           // }

           //funcDB_IRI_FRCh_CR.AddIRI_FRCh_CRToVariableWork(DistribFWS.ToList());
        }

        private void bIRI_CR_RP_Click(object sender, EventArgs e)
        {
            if (dgvIRI_FRCh_CR.dgv.Rows[dgvIRI_FRCh_CR.dgv.SelectedRows[0].Index].Cells[0].Value == null) { return; }
            else
            {
                TSupprFWS[] SupprFWS = AddAllIRIToRP(dgvIRI_FRCh_CR.dgv);

                if (bCheckIridiumInmarsat)
                { 
                    List<TSupprFWS> lSupprFWS = SupprFWS.ToList();
                    for (int i = 0; i < lSupprFWS.Count; i++)
                    {
                        int ind = lSupprFWS.FindIndex(x => x.bLetter == 8);
                        if (ind != -1) lSupprFWS.RemoveAt(ind);
                    }

                    SupprFWS = lSupprFWS.ToArray();

                    MessageBox.Show("В диапазоне литеры 8 осуществляется подавление IRIDIUM (INMARSAT)!", SMessages.mesMessage, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

                for (int i = 0; i < SupprFWS.Length; i++)
                {
                    if (Convert.ToString(dgvIRI_FRCh_CR.dgv.Rows[i].Cells[11].Value) == SMeaning.meaningOwn) // ведущая СП
                    {
                        infoMessage.MessageErr = functionsUser.ChangeIRI_FRCh_RPToVariableWork(SupprFWS[i], variableWork.SupprFWS_Own.ToList(), 1, -1);

                        if (infoMessage.MessageErr != "")
                        {
                            MessageBox.Show(infoMessage.MessageErr, SMessages.mesMessage, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            infoMessage.MessageErr = "";
                            return;
                        }
                    }
                    else if (Convert.ToString(dgvIRI_FRCh_CR.dgv.Rows[i].Cells[11].Value) == SMeaning.meaningLinked) // ведомая СП
                    {
                        infoMessage.MessageErr = functionsUser.ChangeIRI_FRCh_RPToVariableWork(SupprFWS[i], variableWork.SupprFWS_Linked.ToList(), 2, -1);

                        if (infoMessage.MessageErr != "")
                        {
                            MessageBox.Show(infoMessage.MessageErr, SMessages.mesMessage, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            infoMessage.MessageErr = "";
                            return;
                        }
                    }
                }
            }
        }

        private TSupprFWS[] AddAllIRIToRP(DataGridView dgv)
        {
            int len = functionsDGV.AmountRecordsDGV(dgv);

            TSupprFWS[] SupprFWS = new TSupprFWS[len];

            for (int i = 0; i < len; i++)
            {
                SupprFWS[i].iID = Convert.ToInt32(dgv.Rows[i].Cells[0].Value);

                string sFreq = functions.StrToFreq(Convert.ToString(dgv.Rows[i].Cells[1].Value));
                SupprFWS[i].iFreq = Convert.ToInt32(Convert.ToDecimal(sFreq, Functions.format) * 10);

                string sQ1 = functions.StrToFreq(Convert.ToString(dgv.Rows[i].Cells[2].Value));
                if (sQ1 == "-") { SupprFWS[i].sBearing = -1; }
                else { SupprFWS[i].sBearing = Convert.ToInt16(Convert.ToDecimal(sQ1, Functions.format) * 10); }

                // определить литеру
                if (SupprFWS[i].iFreq >= RangesLetters.FREQ_START_LETTER_1 & SupprFWS[i].iFreq < RangesLetters.FREQ_START_LETTER_2)
                    SupprFWS[i].bLetter = 1;
                if (SupprFWS[i].iFreq >= RangesLetters.FREQ_START_LETTER_2 & SupprFWS[i].iFreq < RangesLetters.FREQ_START_LETTER_3)
                    SupprFWS[i].bLetter = 2;
                if (SupprFWS[i].iFreq >= RangesLetters.FREQ_START_LETTER_3 & SupprFWS[i].iFreq < RangesLetters.FREQ_START_LETTER_4)
                    SupprFWS[i].bLetter = 3;
                if (SupprFWS[i].iFreq >= RangesLetters.FREQ_START_LETTER_4 & SupprFWS[i].iFreq < RangesLetters.FREQ_START_LETTER_5)
                    SupprFWS[i].bLetter = 4;
                if (SupprFWS[i].iFreq >= RangesLetters.FREQ_START_LETTER_5 & SupprFWS[i].iFreq < RangesLetters.FREQ_START_LETTER_6)
                    SupprFWS[i].bLetter = 5;
                if (SupprFWS[i].iFreq >= RangesLetters.FREQ_START_LETTER_6 & SupprFWS[i].iFreq < RangesLetters.FREQ_START_LETTER_7)
                    SupprFWS[i].bLetter = 6;
                if (variableCommon.TypeStation == 0)
                {
                    if (SupprFWS[i].iFreq >= RangesLetters.FREQ_START_LETTER_7 & SupprFWS[i].iFreq < RangesLetters.FREQ_STOP_LETTER_7)
                        SupprFWS[i].bLetter = 7;
                }
                else if (variableCommon.TypeStation == 1)
                {
                    if (SupprFWS[i].iFreq >= RangesLetters.FREQ_START_LETTER_7 & SupprFWS[i].iFreq < RangesLetters.FREQ_START_LETTER_8)
                        SupprFWS[i].bLetter = 7;
                    if (SupprFWS[i].iFreq >= RangesLetters.FREQ_START_LETTER_8 & SupprFWS[i].iFreq < RangesLetters.FREQ_START_LETTER_9)
                        SupprFWS[i].bLetter = 8;
                    if (SupprFWS[i].iFreq >= RangesLetters.FREQ_START_LETTER_9 & SupprFWS[i].iFreq <= RangesLetters.FREQ_STOP_LETTER_9)
                        SupprFWS[i].bLetter = 9;
                }

                Int16 iLevel = (Int16)dgv.Rows[dgv.SelectedRows[0].Index].Cells[4].Value;

                iLevel = (Int16)(iLevel - 10);

                if (iLevel < constLevel) { SupprFWS[i].sLevel = (Int16)constLevel; }
               
                //if (iLevel >= -120 && iLevel <= -10) { SupprFWS[i].sLevel = Convert.ToInt16(iLevel); }
                //else
                //{
                //    SupprFWS[i].sLevel = (Int16)(iLevel - 10);
                //}

                SupprFWS[i].bPrior = 2;
                SupprFWS[i].bManipulation = 0;
                SupprFWS[i].bModulation = 1;
                SupprFWS[i].bDeviation = 1;
                SupprFWS[i].bDuration = 1;
            }
            return SupprFWS;
        }

        private void bClearCR_Click(object sender, EventArgs e)
        {
            tbVedCR.Text = "";
            tbVedomCR.Text = "";

            int len = variableWork.DistribFWS.Length;

            //TDistribFWS[] current = new TDistribFWS[len];
            //current = variableWork.DistribFWS.ToArray(); // для обновления столбцов таблицы

            TDistribFWS[] DistribFWS = variableWork.DistribFWS;//.ToArray();

            for(int i = 0; i <len; i++ )
            {
                DistribFWS[i].iSP_RR = 1; 
                DistribFWS[i].iSP_RP = 0; 
            }

            //for (int i = 0; i < len; i++)
            //{
            //    DistribFWS[i].iSP_RR = current[i].iSP_RR;
            //    DistribFWS[i].iSP_RP = current[i].iSP_RP;

            //    //funcDB_IRI_FRCh_CR.ChangeRecord_IRI_FRCh_CRToDB(NameTable.IRI_FRCh_CR, DistribFWS[i], current[i]);
            //}

            variableWork.DistribFWS = DistribFWS;
        }

        private void bRS_Click(object sender, EventArgs e)
        {
            ClearRS_US();

            TDistribFWS[] DistribFWS = variableWork.DistribFWS.ToArray();

            // Радиосети
            double dD = 500;
            List<SRNet> lSRNet = new List<SRNet>();

            functionsUser.Organization_RNet(DistribFWS, dD, 0, 0, ref lSRNet);

            Random randA = new Random();
            Random randR = new Random();     
            Random randG = new Random();      
            Random randB = new Random(); 
            Color color = new Color();

            for (int i = 0; i < lSRNet.Count; i++)
            {
                int a, r, g, b = new int();

                if (i % 2 == 0) // розовый
                {
                    a = randA.Next(66, 122); // яркость
                    r = randR.Next(140, 255); // красный
                    g = randG.Next(0, 4);   // красный
                    b = randB.Next(40, 130); // синий
                }
                else // фиолетовый
                {
                    a = randA.Next(30, 80); // яркость
                    r = randR.Next(70, 140); // красный
                    g = randG.Next(0, 0);   // красный
                    b = randB.Next(140, 255); // синий
                }

                color = Color.FromArgb(a, r, g, b);

                for (int j = 0; j < lSRNet[i].list_IRI_ID.Count; j++)
                {
                    int id = lSRNet[i].list_IRI_ID[j].ID;
                    for (int row = 0; row < dgvIRI_FRCh_CR.dgv.RowCount; row++)
                    {
                        int idRowDGV = Convert.ToInt32(dgvIRI_FRCh_CR.dgv.Rows[row].Cells[0].Value);
                        if (idRowDGV == id)
                        {
                            dgvIRI_FRCh_CR.dgv.Rows[row].Cells[1].Style.ForeColor = color;
                            dgvIRI_FRCh_CR.dgv.Rows[row].Cells[2].Style.ForeColor = color;
                            dgvIRI_FRCh_CR.dgv.Rows[row].Cells[3].Style.ForeColor = color;
                            dgvIRI_FRCh_CR.dgv.Rows[row].Cells[4].Style.ForeColor = color;
                            dgvIRI_FRCh_CR.dgv.Rows[row].Cells[5].Style.ForeColor = color;
                            dgvIRI_FRCh_CR.dgv.Rows[row].Cells[6].Style.ForeColor = color;
                            dgvIRI_FRCh_CR.dgv.Rows[row].Cells[7].Style.ForeColor = color;
                            dgvIRI_FRCh_CR.dgv.Rows[row].Cells[8].Style.ForeColor = color;
                            dgvIRI_FRCh_CR.dgv.Rows[row].Cells[9].Style.ForeColor = color;
                            dgvIRI_FRCh_CR.dgv.Rows[row].Cells[10].Style.ForeColor = color;
                            dgvIRI_FRCh_CR.dgv.Rows[row].Cells[11].Style.ForeColor = color;
                        }
                    }
                
                }
            }
        }

        private void bUS_Click(object sender, EventArgs e)
        {
            ClearRS_US();

            TDistribFWS[] DistribFWS = variableWork.DistribFWS.ToArray();

            // Радиосети
            double dD = 500;
            List<SRNet> lSRNet = new List<SRNet>();

            functionsUser.Organization_Communication(DistribFWS, dD, 0, ref lSRNet);

            Random randA = new Random();   
            Random randR = new Random();   
            Random randG = new Random();     
            Random randB = new Random(); 
            Color color = new Color();

            for (int i = 0; i < lSRNet.Count; i++)
            {
                int a, r, g, b = new int();

                if (i % 2 == 0) // желтый
                {
                    a = randA.Next(75, 120); // яркость
                    r = randR.Next(150, 255); // красный
                    g = randG.Next(80, 128);   // красный
                    b = randB.Next(0, 0); // синий
                }
                else // зеленый
                {
                    a = randA.Next(47, 90); // яркость
                    r = randR.Next(0, 0); // красный
                    g = randG.Next(100, 190);   // красный
                    b = randB.Next(0, 0); // синий
                }
             
                color = Color.FromArgb(a, r, g, b);

                for (int j = 0; j < lSRNet[i].list_IRI_ID.Count; j++)
                {
                    int id = lSRNet[i].list_IRI_ID[j].ID;
                    for (int row = 0; row < dgvIRI_FRCh_CR.dgv.RowCount; row++)
                    {
                        int idRowDGV = Convert.ToInt32(dgvIRI_FRCh_CR.dgv.Rows[row].Cells[0].Value);
                        if (idRowDGV == id)
                        {
                            dgvIRI_FRCh_CR.dgv.Rows[row].Cells[1].Style.ForeColor = color;
                            dgvIRI_FRCh_CR.dgv.Rows[row].Cells[2].Style.ForeColor = color;
                            dgvIRI_FRCh_CR.dgv.Rows[row].Cells[3].Style.ForeColor = color;
                            dgvIRI_FRCh_CR.dgv.Rows[row].Cells[4].Style.ForeColor = color;
                            dgvIRI_FRCh_CR.dgv.Rows[row].Cells[5].Style.ForeColor = color;
                            dgvIRI_FRCh_CR.dgv.Rows[row].Cells[6].Style.ForeColor = color;
                            dgvIRI_FRCh_CR.dgv.Rows[row].Cells[7].Style.ForeColor = color;
                            dgvIRI_FRCh_CR.dgv.Rows[row].Cells[8].Style.ForeColor = color;
                            dgvIRI_FRCh_CR.dgv.Rows[row].Cells[9].Style.ForeColor = color;
                            dgvIRI_FRCh_CR.dgv.Rows[row].Cells[10].Style.ForeColor = color;
                            dgvIRI_FRCh_CR.dgv.Rows[row].Cells[11].Style.ForeColor = color;
                        }
                    }
                }
            }
        }

        private void bClearRSUS_Click(object sender, EventArgs e)
        {
            ClearRS_US();
        }

        private void ClearRS_US()
        {
            try
            {
                for (int row = 0; row < dgvIRI_FRCh_CR.dgv.RowCount; row++)
                {
                    dgvIRI_FRCh_CR.dgv.Rows[row].Cells[1].Style.ForeColor = Color.Black;
                    dgvIRI_FRCh_CR.dgv.Rows[row].Cells[2].Style.ForeColor = Color.Black;
                    dgvIRI_FRCh_CR.dgv.Rows[row].Cells[3].Style.ForeColor = Color.Black;
                    dgvIRI_FRCh_CR.dgv.Rows[row].Cells[4].Style.ForeColor = Color.Black;
                    dgvIRI_FRCh_CR.dgv.Rows[row].Cells[5].Style.ForeColor = Color.Black;
                    dgvIRI_FRCh_CR.dgv.Rows[row].Cells[6].Style.ForeColor = Color.Black;
                    dgvIRI_FRCh_CR.dgv.Rows[row].Cells[7].Style.ForeColor = Color.Black;
                    dgvIRI_FRCh_CR.dgv.Rows[row].Cells[8].Style.ForeColor = Color.Black;
                    dgvIRI_FRCh_CR.dgv.Rows[row].Cells[9].Style.ForeColor = Color.Black;
                    dgvIRI_FRCh_CR.dgv.Rows[row].Cells[10].Style.ForeColor = Color.Black;
                    dgvIRI_FRCh_CR.dgv.Rows[row].Cells[11].Style.ForeColor = Color.Black;
                }
            }
            catch { }
        }

        private async void button1_Click(object sender, EventArgs e)
        {
            var answer = await VariableDynamic.VariableWork.aWPtoBearingDSPprotocolNew.GetASPRadioSources();
        }



    }
}
