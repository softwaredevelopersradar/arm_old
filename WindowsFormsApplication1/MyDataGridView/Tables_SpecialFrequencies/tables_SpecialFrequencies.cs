﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MyDataGridView;

using Protocols;
using VariableDynamic;
using System.Globalization;
using VariableStatic;
using USR_DLL;



namespace Tables_SpecialFrequencies
{
    public partial class tables_SpecialFrequencies: UserControl
    {
        FunctionsDB functionsDB;
        FunctionsDGV functionsDGV;
        FuncDB_SpecialFrequencies funcDB_SpecialFrequencies;
        FuncDGV_SpecialFrequencies funcDGV_SpecialFrequencies;

        CurVal_SpecialFrequencies curVal_SpecialFrequencies;
        Struct_SpecialFrequencies struct_SpecialFrequencies;

        Functions functions;
        FunctionsTranslate functionsTranslate;
        InitParams initParams;

        VariableWork variableWork;
        VariableCommon variableCommon;

        TFreqImportantKRPU[] FreqImportantKRPU;

        public int tcIndex = 0;

        private bool f_rbOwn = true;
        private bool f_rbMated = false;

        public tables_SpecialFrequencies()
        {
            InitializeComponent();

            functionsDB = new FunctionsDB();
            functionsDGV = new FunctionsDGV();
            funcDB_SpecialFrequencies = new FuncDB_SpecialFrequencies();
            funcDGV_SpecialFrequencies = new FuncDGV_SpecialFrequencies();

            curVal_SpecialFrequencies = new CurVal_SpecialFrequencies();
            struct_SpecialFrequencies = new Struct_SpecialFrequencies();

            functions = new Functions();
            functionsTranslate = new FunctionsTranslate();
            initParams = new InitParams();

            variableWork = new VariableWork();
            variableCommon = new VariableCommon();

            InitTablesSpecialFrequencies();

            //установить значение мин., макс. для поля
            initParams.Init3000_6000_RangeRR(variableCommon.TypeStation, nudFreqMin, nudFreqMax);
 
            SetViewWindowForRole();

            VariableCommon.OnChangeCommonLanguage += new VariableCommon.ChangeCommonEventHandler(VariableCommon_OnChangeCommonLanguage);
            VariableCommon.OnChangeCommonRole += new VariableCommon.ChangeCommonEventHandler(VariableCommon_OnChangeCommonRole);

            // Событие возникает при изменении значений запрещенных частот (ведущая)
            VariableWork.OnChangeFrequencyRangeForbiddenOwn += new VariableWork.ChangeRegimeEventHandler(VariableWork_OnChangeFrequencyRangeForbiddenOwn); 
            // Событие возникает при изменении значений запрещенных частот (ведомая)
            VariableWork.OnChangeFrequencyRangeForbiddenLinked += new VariableWork.ChangeRegimeEventHandler(VariableWork_OnChangeFrequencyRangeForbiddenLinked);
            // Событие возникает при изменении значений известных частот (ведущая)
            VariableWork.OnChangeFrequencyRangeKnownOwn += new VariableWork.ChangeRegimeEventHandler(VariableWork_OnChangeFrequencyRangeKnownOwn);
            // Событие возникает при изменении значений известных частот (ведомая)
            VariableWork.OnChangeFrequencyRangeKnownLinked += new VariableWork.ChangeRegimeEventHandler(VariableWork_OnChangeFrequencyRangeKnownLinked);
            // Событие возникает при изменении значений важных частот (ведущая)
            VariableWork.OnChangeFrequencyRangeImportantOwn += new VariableWork.ChangeRegimeEventHandler(VariableWork_OnChangeFrequencyRangeImportantOwn);
            // Событие возникает при изменении значений важных частот (ведомая)
            VariableWork.OnChangeFrequencyRangeImportantLinked +=new VariableWork.ChangeRegimeEventHandler(VariableWork_OnChangeFrequencyRangeImportantLinked);

            // События возникают при изменении всех значений специальных частот (для обновления всех таблиц в БД)
            VariableWork.OnChangeDBAllSpecForbiddenOwn += new VariableWork.UpdateAllSpecFreqEventHandler(VariableWork_OnChangeDBAllSpecForbiddenOwn);
            VariableWork.OnChangeDBAllSpecForbiddenLinked += new VariableWork.UpdateAllSpecFreqEventHandler(VariableWork_OnChangeDBAllSpecForbiddenLinked);
            VariableWork.OnChangeDBAllSpecKnownOwn += new VariableWork.UpdateAllSpecFreqEventHandler(VariableWork_OnChangeDBAllSpeKnownOwn);
            VariableWork.OnChangeDBAllSpecKnownLinked += new VariableWork.UpdateAllSpecFreqEventHandler(VariableWork_OnChangeDBAllSpecKnownLinked);
            VariableWork.OnChangeDBAllSpecImportantOwn += new VariableWork.UpdateAllSpecFreqEventHandler(VariableWork_OnChangeDBAllSpecImportantOwn);
            VariableWork.OnChangeDBAllSpecImportantLinked += new VariableWork.UpdateAllSpecFreqEventHandler(VariableWork_OnChangeDBAllSpecImportantLinked);

            // События возникают при добавлении значений специальных частот 
            VariableWork.OnAddRecSpecFreq += new VariableWork.AddRecSpecFreqEventHandler(VariableWork_OnAddRecSpecFreq);

            LoadTablesSpecialFrequenciesfromDB();

            // Событие возникает при выделении строки в dgv
            dgvFreqForbidden1.dgv.SelectionChanged += new EventHandler(dgv_SelectionChanged_FreqForbidden1);
            dgvFreqForbidden2.dgv.SelectionChanged += new EventHandler(dgv_SelectionChanged_FreqForbidden2);
            dgvFreqKnown1.dgv.SelectionChanged += new EventHandler(dgv_SelectionChanged_FreqKnown1);
            dgvFreqKnown2.dgv.SelectionChanged += new EventHandler(dgv_SelectionChanged_dgvFreqKnown2);
            dgvFreqImportant1.dgv.SelectionChanged += new EventHandler(dgv_SelectionChanged_FreqImportant1);
            dgvFreqImportant2.dgv.SelectionChanged += new EventHandler(dgv_SelectionChanged_FreqImportant2);
           
        }

        void VariableCommon_OnChangeCommonRole()
        {
            SetViewWindowForRole();
        }

        /// <summary>
        /// Установить вид окна в зависимости от роли (автономная, ведущая, ведомая)
        /// </summary>
        public void SetViewWindowForRole()
        {
            try
            {
                if (variableCommon.Role == 2)
                {
                    tableLayoutPanel1.ColumnStyles[1] = new ColumnStyle(SizeType.Absolute, 0);

                    dgvFreqForbidden1.Visible = true;
                    dgvFreqForbidden2.Visible = false;
                    dgvFreqImportant1.Visible = true;
                    dgvFreqImportant2.Visible = false;
                    dgvFreqKnown1.Visible = true;
                    dgvFreqKnown2.Visible = false;
                }
                else if (variableCommon.Role == 0 || variableCommon.Role == 1)
                {
                    tableLayoutPanel1.ColumnStyles[1] = new ColumnStyle(SizeType.Absolute, 200);

                    dgvFreqForbidden1.Visible = true;
                    dgvFreqForbidden2.Visible = true;
                    dgvFreqImportant1.Visible = true;
                    dgvFreqImportant2.Visible = true;
                    dgvFreqKnown1.Visible = true;
                    dgvFreqKnown2.Visible = true;
                }
            }
            catch { }
        }

        private void VariableCommon_OnChangeCommonLanguage()
        {
            VariableCommon variableCommon = new VariableCommon();
            switch (variableCommon.Language)
            {
                case 0:
                    functionsTranslate.SetLanguage("rus");
                    ChangeLanguage();
                    break;

                case 1:
                    functionsTranslate.SetLanguage("eng");
                    ChangeLanguage();
                    break;

                case 2:
                    functionsTranslate.SetLanguage("az");
                    ChangeLanguage();
                    break;

                default:
                    functionsTranslate.SetLanguage("rus");
                    ChangeLanguage();
                    break;
            }
        }

        /// <summary>
        /// Для инициализации языка при запуске приложения 
        /// </summary>
        /// <param name="bLanguage"></param>
        public void ChangeControlLanguage(byte bLanguage)
        {

            switch (bLanguage)
            {
                case 0:
                    functionsTranslate.SetLanguage("rus");
                    ChangeLanguage();
                    break;

                case 1:
                    functionsTranslate.SetLanguage("eng");
                    ChangeLanguage();
                    break;

                case 2:
                    functionsTranslate.SetLanguage("az");
                    ChangeLanguage();
                    break;

                default:
                    functionsTranslate.SetLanguage("rus");
                    ChangeLanguage();
                    break;
            }
        }

        /// <summary>
        /// Изменение языка интерфейса
        /// </summary>
        private void ChangeLanguage()
        {
            functionsTranslate.RenameFieldsTables(functionsTranslate.Dictionary, dgvFreqForbidden1.dgv);
            functionsTranslate.RenameFieldsTables(functionsTranslate.Dictionary, dgvFreqForbidden2.dgv);
            functionsTranslate.RenameFieldsTables(functionsTranslate.Dictionary, dgvFreqImportant1.dgv);
            functionsTranslate.RenameFieldsTables(functionsTranslate.Dictionary, dgvFreqImportant2.dgv);
            functionsTranslate.RenameFieldsTables(functionsTranslate.Dictionary, dgvFreqKnown1.dgv);
            functionsTranslate.RenameFieldsTables(functionsTranslate.Dictionary, dgvFreqKnown2.dgv);
            functionsTranslate.RenameRadioButtons(functionsTranslate.Dictionary, grbRadioButtons);
            functionsTranslate.RenameButtons(functionsTranslate.Dictionary, pButtons);
            functionsTranslate.RenameLabels(functionsTranslate.Dictionary, grbRange);
            functionsTranslate.RenameCheckBoxes(functionsTranslate.Dictionary, grbRange);
            functionsTranslate.RenameTabPages(functionsTranslate.Dictionary, tabControl);
        }

        /// <summary>
        /// Добавление специальных частот в таблицы откуда-то
        /// </summary>
        /// <param name="FreqMinkHzx10"> частота мин. </param>
        /// <param name="FreqMaxkHzx10"> частота макс. </param>
        /// <param name="TypeTable"> 1 - запрещенные, 2 - известные, 3 - важные </param>
        /// <param name="bTable"> 0 - ведущая + ведомая, 1 - ведущая, 2 - ведомая </param>
        void VariableWork_OnAddRecSpecFreq(int FreqMinkHzx10, int FreqMaxkHzx10, byte TypeTable, byte bTable)
        {
            switch (TypeTable)
            { 
                case 0:
                    break;

                case 1:
                   switch (bTable)
                    {
                        case 0:
                            AddRecord(dgvFreqForbidden1, dgvFreqForbidden1.tableFreqForbidden, NameTable.FREQ_FORBIDDEN, Table.Own, FreqMinkHzx10, FreqMaxkHzx10);
                            AddRecord(dgvFreqForbidden2, dgvFreqForbidden2.tableFreqForbidden, NameTable.FREQ_FORBIDDEN, Table.Linked, FreqMinkHzx10, FreqMaxkHzx10);
                            break;

                        case 1:
                            AddRecord(dgvFreqForbidden1, dgvFreqForbidden1.tableFreqForbidden, NameTable.FREQ_FORBIDDEN, Table.Own, FreqMinkHzx10, FreqMaxkHzx10);
                            break;

                        case 2:
                            AddRecord(dgvFreqForbidden2, dgvFreqForbidden2.tableFreqForbidden, NameTable.FREQ_FORBIDDEN, Table.Linked, FreqMinkHzx10, FreqMaxkHzx10);
                            break;
                    }
                    break;

                case 2:
                   switch (bTable)
                    {
                        case 0:
                            AddRecord(dgvFreqKnown1, dgvFreqKnown1.tableFreqKnown, NameTable.FREQ_KNOWN, Table.Own, FreqMinkHzx10, FreqMaxkHzx10);
                            AddRecord(dgvFreqKnown2, dgvFreqKnown2.tableFreqKnown, NameTable.FREQ_KNOWN, Table.Linked, FreqMinkHzx10, FreqMaxkHzx10);
                            break;

                        case 1:
                            AddRecord(dgvFreqKnown1, dgvFreqKnown1.tableFreqKnown, NameTable.FREQ_KNOWN, Table.Own, FreqMinkHzx10, FreqMaxkHzx10);
                            break;

                        case 2:
                            AddRecord(dgvFreqKnown2, dgvFreqKnown2.tableFreqKnown, NameTable.FREQ_KNOWN, Table.Linked, FreqMinkHzx10, FreqMaxkHzx10);
                            break;
                    }
                    break;

                case 3:
                    switch (bTable)
                    {
                        case 0:
                            AddRecord(dgvFreqImportant1, dgvFreqImportant1.tableFreqImportant, NameTable.FREQ_IMPORTANT, Table.Own, FreqMinkHzx10, FreqMaxkHzx10);
                            AddRecord(dgvFreqImportant2, dgvFreqImportant2.tableFreqImportant, NameTable.FREQ_IMPORTANT, Table.Linked, FreqMinkHzx10, FreqMaxkHzx10);
                            break;

                        case 1:
                            AddRecord(dgvFreqImportant1, dgvFreqImportant1.tableFreqImportant, NameTable.FREQ_IMPORTANT, Table.Own, FreqMinkHzx10, FreqMaxkHzx10);
                            break;

                        case 2:
                            AddRecord(dgvFreqImportant2, dgvFreqImportant2.tableFreqImportant, NameTable.FREQ_IMPORTANT, Table.Linked, FreqMinkHzx10, FreqMaxkHzx10);
                            break;
                    }
                    break;

                default:
                    return;
            }
            

        }

        void VariableWork_OnChangeDBAllSpecImportantLinked()
        {
            if (variableWork.FrequencyRangeImportantLinked != null)
            {
                int len = variableWork.FrequencyRangeImportantLinked.Length;

                struct_SpecialFrequencies = new Struct_SpecialFrequencies();

                functionsDB.DeleteAllRecordsDB(NameTable.FREQ_IMPORTANT, Table.Linked);

                for (int i = 0; i < len; i++)
                {
                    struct_SpecialFrequencies.iID = i + 1;
                    struct_SpecialFrequencies.iFreqMin = variableWork.FrequencyRangeImportantLinked[i].StartFrequency;
                    struct_SpecialFrequencies.iFreqMax = variableWork.FrequencyRangeImportantLinked[i].EndFrequency;
                    struct_SpecialFrequencies.bMode = 2;

                    funcDB_SpecialFrequencies.AddRecord_SpecialFreqToDB(NameTable.FREQ_IMPORTANT, struct_SpecialFrequencies);
                }
            }
        }

        void VariableWork_OnChangeDBAllSpecImportantOwn()
        {
            if (variableWork.FrequencyRangeImportantOwn != null)
            {
                int len = variableWork.FrequencyRangeImportantOwn.Length;

                struct_SpecialFrequencies = new Struct_SpecialFrequencies();

                functionsDB.DeleteAllRecordsDB(NameTable.FREQ_IMPORTANT, Table.Own);

                for (int i = 0; i < len; i++)
                {
                    struct_SpecialFrequencies.iID = i + 1;
                    struct_SpecialFrequencies.iFreqMin = variableWork.FrequencyRangeImportantOwn[i].StartFrequency;
                    struct_SpecialFrequencies.iFreqMax = variableWork.FrequencyRangeImportantOwn[i].EndFrequency;
                    struct_SpecialFrequencies.bMode = 1;

                    funcDB_SpecialFrequencies.AddRecord_SpecialFreqToDB(NameTable.FREQ_IMPORTANT, struct_SpecialFrequencies);
                }
            }
        }

        void VariableWork_OnChangeDBAllSpecKnownLinked()
        {
            if (variableWork.FrequencyRangeKnownLinked != null)
            {
                int len = variableWork.FrequencyRangeKnownLinked.Length;

                struct_SpecialFrequencies = new Struct_SpecialFrequencies();

                functionsDB.DeleteAllRecordsDB(NameTable.FREQ_KNOWN, Table.Linked);

                for (int i = 0; i < len; i++)
                {
                    struct_SpecialFrequencies.iID = i + 1;
                    struct_SpecialFrequencies.iFreqMin = variableWork.FrequencyRangeKnownLinked[i].StartFrequency;
                    struct_SpecialFrequencies.iFreqMax = variableWork.FrequencyRangeKnownLinked[i].EndFrequency;
                    struct_SpecialFrequencies.bMode = 2;

                    funcDB_SpecialFrequencies.AddRecord_SpecialFreqToDB(NameTable.FREQ_KNOWN, struct_SpecialFrequencies);
                }
            }
        }

        void VariableWork_OnChangeDBAllSpeKnownOwn()
        {
            if (variableWork.FrequencyRangeKnownOwn != null)
            {
                int len = variableWork.FrequencyRangeKnownOwn.Length;

                struct_SpecialFrequencies = new Struct_SpecialFrequencies();

                functionsDB.DeleteAllRecordsDB(NameTable.FREQ_KNOWN, Table.Own);

                for (int i = 0; i < len; i++)
                {
                    struct_SpecialFrequencies.iID = i + 1;
                    struct_SpecialFrequencies.iFreqMin = variableWork.FrequencyRangeKnownOwn[i].StartFrequency;
                    struct_SpecialFrequencies.iFreqMax = variableWork.FrequencyRangeKnownOwn[i].EndFrequency;
                    struct_SpecialFrequencies.bMode = 1;

                    funcDB_SpecialFrequencies.AddRecord_SpecialFreqToDB(NameTable.FREQ_KNOWN, struct_SpecialFrequencies);
                }
            }
        }

        void VariableWork_OnChangeDBAllSpecForbiddenLinked()
        {
            if (variableWork.FrequencyRangeForbiddenLinked != null)
            {
                int len = variableWork.FrequencyRangeForbiddenLinked.Length;

                struct_SpecialFrequencies = new Struct_SpecialFrequencies();

                functionsDB.DeleteAllRecordsDB(NameTable.FREQ_FORBIDDEN, Table.Linked);

                for (int i = 0; i < len; i++)
                {
                    struct_SpecialFrequencies.iID = i + 1;
                    struct_SpecialFrequencies.iFreqMin = variableWork.FrequencyRangeForbiddenLinked[i].StartFrequency;
                    struct_SpecialFrequencies.iFreqMax = variableWork.FrequencyRangeForbiddenLinked[i].EndFrequency;
                    struct_SpecialFrequencies.bMode = 2;

                    funcDB_SpecialFrequencies.AddRecord_SpecialFreqToDB(NameTable.FREQ_FORBIDDEN, struct_SpecialFrequencies);
                }
            }
        }

        void VariableWork_OnChangeDBAllSpecForbiddenOwn()
        {
            if (variableWork.FrequencyRangeForbiddenOwn != null)
            {
                int len = variableWork.FrequencyRangeForbiddenOwn.Length;

                struct_SpecialFrequencies = new Struct_SpecialFrequencies();

                functionsDB.DeleteAllRecordsDB(NameTable.FREQ_FORBIDDEN, Table.Own);

                for (int i = 0; i < len; i++)
                {
                    struct_SpecialFrequencies.iID = i + 1;
                    struct_SpecialFrequencies.iFreqMin = variableWork.FrequencyRangeForbiddenOwn[i].StartFrequency;
                    struct_SpecialFrequencies.iFreqMax = variableWork.FrequencyRangeForbiddenOwn[i].EndFrequency;
                    struct_SpecialFrequencies.bMode = 1;

                    funcDB_SpecialFrequencies.AddRecord_SpecialFreqToDB(NameTable.FREQ_FORBIDDEN, struct_SpecialFrequencies);
                }
            }
        }

        public void LoadTablesSpecialFrequenciesfromDB()
        {
            funcDB_SpecialFrequencies.LoadTableSpecFreqFromDB(NameTable.FREQ_FORBIDDEN, Table.Own);
            funcDB_SpecialFrequencies.LoadTableSpecFreqFromDB(NameTable.FREQ_FORBIDDEN, Table.Linked);

            funcDB_SpecialFrequencies.LoadTableSpecFreqFromDB(NameTable.FREQ_KNOWN, Table.Own);
            funcDB_SpecialFrequencies.LoadTableSpecFreqFromDB(NameTable.FREQ_KNOWN, Table.Linked);

            funcDB_SpecialFrequencies.LoadTableSpecFreqFromDB(NameTable.FREQ_IMPORTANT, Table.Own);
            funcDB_SpecialFrequencies.LoadTableSpecFreqFromDB(NameTable.FREQ_IMPORTANT, Table.Linked);

            VisibleTable();
        }

        private void VariableWork_OnChangeFrequencyRangeImportantLinked()
        {
            if (InvokeRequired)
            {
                BeginInvoke((MethodInvoker)(()
                      => VariableWork_OnChangeFrequencyRangeImportantLinked()));
                return;
            }

            if (variableWork.FrequencyRangeImportantLinked != null)
            {
                int len = variableWork.FrequencyRangeImportantLinked.Length;

                if (functionsDB.DeleteAllRecordsDB(NameTable.FREQ_IMPORTANT, Table.Linked))
                {
                    struct_SpecialFrequencies = new Struct_SpecialFrequencies();

                    functionsDGV.DeleteAllRecordsDGV(dgvFreqImportant2.dgv);

                    dgvFreqImportant2.SetInitialNumberOfRows(dgvFreqImportant2.dgv, dgvFreqImportant2.tableFreqImportant, NameTable.FREQ_IMPORTANT);

                    for (int i = 0; i < len; i++)
                    {
                        struct_SpecialFrequencies.iID = i + 1;
                        struct_SpecialFrequencies.iFreqMin = variableWork.FrequencyRangeImportantLinked[i].StartFrequency;
                        struct_SpecialFrequencies.iFreqMax = variableWork.FrequencyRangeImportantLinked[i].EndFrequency;
                        struct_SpecialFrequencies.bMode = 2;

                        if (funcDB_SpecialFrequencies.AddRecord_SpecialFreqToDB(NameTable.FREQ_IMPORTANT, struct_SpecialFrequencies))
                        {
                            funcDGV_SpecialFrequencies.AddRecord_SpecialFreqToDGV(dgvFreqImportant2.dgv, NameTable.FREQ_IMPORTANT, struct_SpecialFrequencies);
                        }
                    }
                }

                VisibleTable();
            }
        }

        private void VariableWork_OnChangeFrequencyRangeImportantOwn()
        {
            if (InvokeRequired)
            {
                BeginInvoke((MethodInvoker)(()
                      => VariableWork_OnChangeFrequencyRangeImportantOwn()));
                return;
            }

            if (variableWork.FrequencyRangeImportantOwn != null)
            {
                int len = variableWork.FrequencyRangeImportantOwn.Length;

                if (functionsDB.DeleteAllRecordsDB(NameTable.FREQ_IMPORTANT, Table.Own))
                {
                    struct_SpecialFrequencies = new Struct_SpecialFrequencies();

                    functionsDGV.DeleteAllRecordsDGV(dgvFreqImportant1.dgv);
                    dgvFreqImportant1.SetInitialNumberOfRows(dgvFreqImportant1.dgv, dgvFreqImportant1.tableFreqImportant, NameTable.FREQ_IMPORTANT);

                    for (int i = 0; i < len; i++)
                    {
                        struct_SpecialFrequencies.iID = i + 1;
                        struct_SpecialFrequencies.iFreqMin = variableWork.FrequencyRangeImportantOwn[i].StartFrequency;
                        struct_SpecialFrequencies.iFreqMax = variableWork.FrequencyRangeImportantOwn[i].EndFrequency;
                        struct_SpecialFrequencies.bMode = 1;

                        if (funcDB_SpecialFrequencies.AddRecord_SpecialFreqToDB(NameTable.FREQ_IMPORTANT, struct_SpecialFrequencies))
                        {
                            funcDGV_SpecialFrequencies.AddRecord_SpecialFreqToDGV(dgvFreqImportant1.dgv, NameTable.FREQ_IMPORTANT, struct_SpecialFrequencies);
                        }
                    }
                }

                VisibleTable();
            }
        }

        private void VariableWork_OnChangeFrequencyRangeKnownLinked()
        {
            if (InvokeRequired)
            {
                BeginInvoke((MethodInvoker)(()
                      => VariableWork_OnChangeFrequencyRangeKnownLinked()));
                return;
            }

            if (variableWork.FrequencyRangeKnownLinked != null)
            {
                int len = variableWork.FrequencyRangeKnownLinked.Length;

                if (functionsDB.DeleteAllRecordsDB(NameTable.FREQ_KNOWN, Table.Linked))
                {
                    struct_SpecialFrequencies = new Struct_SpecialFrequencies();

                    functionsDGV.DeleteAllRecordsDGV(dgvFreqKnown2.dgv);

                    dgvFreqKnown2.SetInitialNumberOfRows(dgvFreqKnown2.dgv, dgvFreqKnown2.tableFreqKnown, NameTable.FREQ_KNOWN);

                    for (int i = 0; i < len; i++)
                    {
                        struct_SpecialFrequencies.iID = i + 1;
                        struct_SpecialFrequencies.iFreqMin = variableWork.FrequencyRangeKnownLinked[i].StartFrequency;
                        struct_SpecialFrequencies.iFreqMax = variableWork.FrequencyRangeKnownLinked[i].EndFrequency;
                        struct_SpecialFrequencies.bMode = 2;

                        if (funcDB_SpecialFrequencies.AddRecord_SpecialFreqToDB(NameTable.FREQ_KNOWN, struct_SpecialFrequencies))
                        {
                            funcDGV_SpecialFrequencies.AddRecord_SpecialFreqToDGV(dgvFreqKnown2.dgv, NameTable.FREQ_KNOWN, struct_SpecialFrequencies);
                        }
                    }
                }

                VisibleTable();
            }
        }

        private void VariableWork_OnChangeFrequencyRangeKnownOwn()
        {
            if (InvokeRequired)
            {
                BeginInvoke((MethodInvoker)(()
                      => VariableWork_OnChangeFrequencyRangeKnownOwn()));
                return;
            }

            if (variableWork.FrequencyRangeKnownOwn != null)
            {
                int len = variableWork.FrequencyRangeKnownOwn.Length;

                if (functionsDB.DeleteAllRecordsDB(NameTable.FREQ_KNOWN, Table.Own))
                {
                    struct_SpecialFrequencies = new Struct_SpecialFrequencies();

                    functionsDGV.DeleteAllRecordsDGV(dgvFreqKnown1.dgv);

                    dgvFreqKnown1.SetInitialNumberOfRows(dgvFreqKnown1.dgv, dgvFreqKnown1.tableFreqKnown, NameTable.FREQ_KNOWN);

                    for (int i = 0; i < len; i++)
                    {
                        struct_SpecialFrequencies.iID = i + 1;
                        struct_SpecialFrequencies.iFreqMin = variableWork.FrequencyRangeKnownOwn[i].StartFrequency;
                        struct_SpecialFrequencies.iFreqMax = variableWork.FrequencyRangeKnownOwn[i].EndFrequency;
                        struct_SpecialFrequencies.bMode = 1;

                        if (funcDB_SpecialFrequencies.AddRecord_SpecialFreqToDB(NameTable.FREQ_KNOWN, struct_SpecialFrequencies))
                        {
                            funcDGV_SpecialFrequencies.AddRecord_SpecialFreqToDGV(dgvFreqKnown1.dgv, NameTable.FREQ_KNOWN, struct_SpecialFrequencies);
                        }
                    }
                }

                VisibleTable();
            }
        }

        private void VariableWork_OnChangeFrequencyRangeForbiddenLinked()
        {
            if (InvokeRequired)
            {
                BeginInvoke((MethodInvoker)(()
                      => VariableWork_OnChangeFrequencyRangeForbiddenLinked()));
                return;
            }

            if (variableWork.FrequencyRangeForbiddenLinked != null)
            {
                int len = variableWork.FrequencyRangeForbiddenLinked.Length;

                if (functionsDB.DeleteAllRecordsDB(NameTable.FREQ_FORBIDDEN, Table.Linked))
                {
                    struct_SpecialFrequencies = new Struct_SpecialFrequencies();

                    functionsDGV.DeleteAllRecordsDGV(dgvFreqForbidden2.dgv);

                    dgvFreqForbidden2.SetInitialNumberOfRows(dgvFreqForbidden2.dgv, dgvFreqForbidden2.tableFreqForbidden, NameTable.FREQ_FORBIDDEN);

                    for (int i = 0; i < len; i++)
                    {
                        struct_SpecialFrequencies.iID = i + 1;
                        struct_SpecialFrequencies.iFreqMin = variableWork.FrequencyRangeForbiddenLinked[i].StartFrequency;
                        struct_SpecialFrequencies.iFreqMax = variableWork.FrequencyRangeForbiddenLinked[i].EndFrequency;
                        struct_SpecialFrequencies.bMode = 2;

                        if (funcDB_SpecialFrequencies.AddRecord_SpecialFreqToDB(NameTable.FREQ_FORBIDDEN, struct_SpecialFrequencies))
                        {
                            funcDGV_SpecialFrequencies.AddRecord_SpecialFreqToDGV(dgvFreqForbidden2.dgv, NameTable.FREQ_FORBIDDEN, struct_SpecialFrequencies);
                        }
                    }
                }

                VisibleTable();
            }
        }

        private void VariableWork_OnChangeFrequencyRangeForbiddenOwn()
        {
            if (InvokeRequired)
            {
                BeginInvoke((MethodInvoker)(()
                      => VariableWork_OnChangeFrequencyRangeForbiddenOwn()));
                return;
            }

            if (variableWork.FrequencyRangeForbiddenOwn != null)
            {
                int len = variableWork.FrequencyRangeForbiddenOwn.Length;

                if (functionsDB.DeleteAllRecordsDB(NameTable.FREQ_FORBIDDEN, Table.Own))
                {
                    struct_SpecialFrequencies = new Struct_SpecialFrequencies();

                    functionsDGV.DeleteAllRecordsDGV(dgvFreqForbidden1.dgv);

                    dgvFreqForbidden1.SetInitialNumberOfRows(dgvFreqForbidden1.dgv, dgvFreqForbidden1.tableFreqForbidden, NameTable.FREQ_FORBIDDEN);

                    for (int i = 0; i < len; i++)
                    {
                        struct_SpecialFrequencies.iID = i + 1;
                        struct_SpecialFrequencies.iFreqMin = variableWork.FrequencyRangeForbiddenOwn[i].StartFrequency;
                        struct_SpecialFrequencies.iFreqMax = variableWork.FrequencyRangeForbiddenOwn[i].EndFrequency;
                        struct_SpecialFrequencies.bMode = 1;

                        if (funcDB_SpecialFrequencies.AddRecord_SpecialFreqToDB(NameTable.FREQ_FORBIDDEN, struct_SpecialFrequencies))
                        {
                            funcDGV_SpecialFrequencies.AddRecord_SpecialFreqToDGV(dgvFreqForbidden1.dgv, NameTable.FREQ_FORBIDDEN, struct_SpecialFrequencies);
                        }
                    }
                }

                VisibleTable();
            }
        }

        public void InitTablesSpecialFrequencies()
        {
            dgvFreqForbidden1.InitTableDB(dgvFreqForbidden1.dgv, NameTable.FREQ_FORBIDDEN);
            dgvFreqForbidden2.InitTableDB(dgvFreqForbidden2.dgv, NameTable.FREQ_FORBIDDEN);

            dgvFreqKnown1.InitTableDB(dgvFreqKnown1.dgv, NameTable.FREQ_KNOWN);
            dgvFreqKnown2.InitTableDB(dgvFreqKnown2.dgv, NameTable.FREQ_KNOWN);

            dgvFreqImportant1.InitTableDB(dgvFreqImportant1.dgv, NameTable.FREQ_IMPORTANT);
            dgvFreqImportant2.InitTableDB(dgvFreqImportant2.dgv, NameTable.FREQ_IMPORTANT);
        }

        private void dgv_SelectionChanged_FreqImportant2(object sender, EventArgs e)
        {
            SetCurParamToControls(dgvFreqImportant2.dgv);
        }

        private void dgv_SelectionChanged_FreqImportant1(object sender, EventArgs e)
        {
            SetCurParamToControls(dgvFreqImportant1.dgv);
        }

        private void dgv_SelectionChanged_dgvFreqKnown2(object sender, EventArgs e)
        {
            SetCurParamToControls(dgvFreqKnown2.dgv);
        }

        private void dgv_SelectionChanged_FreqKnown1(object sender, EventArgs e)
        {
            SetCurParamToControls(dgvFreqKnown1.dgv);
        }

        private void dgv_SelectionChanged_FreqForbidden2(object sender, EventArgs e)
        {
            SetCurParamToControls(dgvFreqForbidden2.dgv);
        }

        private void dgv_SelectionChanged_FreqForbidden1(object sender, EventArgs e)
        {
            SetCurParamToControls(dgvFreqForbidden1.dgv);
        }

        /// <summary>
        /// Определение выделенной строки текущей таблицы
        /// </summary>
        /// <param name="dgv"></param>
        private void SetCurParamToControls(DataGridView dgv)
        {
            if (dgv.SelectedRows.Count > 0)
            {
                if (dgv.Rows[dgv.SelectedRows[0].Index].Cells[0].Value != null)
                {
                    try
                    {
                        SetParams(dgv);
                    }
                    catch (System.Exception e)
                    {
                        MessageBox.Show(e.Message);
                        return;
                    }
                }
            }
        }

        /// <summary>
        /// Установить рараметры текущей строки в элементы редактирования
        /// </summary>
        /// <param name="dgv"></param>
        private void SetParams(DataGridView dgv)
        {
            decimal dFreqMin, dFreqMax;
            string sFreqMin, sFreqMax, sSubRes;

            if (dgv.RowCount != 0)
            {
                curVal_SpecialFrequencies = new CurVal_SpecialFrequencies();

                // ID строки
                curVal_SpecialFrequencies.IdDGV = Convert.ToInt32(dgv.Rows[dgv.SelectedRows[0].Index].Cells[0].Value);

                // значение частоты min
                dFreqMin = Convert.ToDecimal(dgv.Rows[dgv.SelectedRows[0].Index].Cells[1].Value, Functions.format);
                sSubRes = functions.FreqToStr((int)(dFreqMin * 10));
                sFreqMin = functions.StrToFreq(sSubRes);

                nudFreqMin.Value = Convert.ToDecimal(sFreqMin, Functions.format);
                curVal_SpecialFrequencies.FreqMinDGV = Convert.ToInt32(Convert.ToDecimal(dFreqMin, Functions.format) * 10);

                // значение частоты max
                if (dgv.Rows[dgv.SelectedRows[0].Index].Cells[2].Value.ToString() == "————")
                {
                    chbRange.Checked = false;
                    nudFreqMax.Value = nudFreqMin.Value;
                    curVal_SpecialFrequencies.FreqMaxDGV = Convert.ToInt32(Convert.ToDecimal(dFreqMin, Functions.format) * 10);
                }
                else
                {
                    chbRange.Checked = true;
                    dFreqMax = Convert.ToDecimal(dgv.Rows[dgv.SelectedRows[0].Index].Cells[2].Value, Functions.format);
                    sSubRes = functions.FreqToStr((int)(dFreqMax * 10));
                    sFreqMax = functions.StrToFreq(sSubRes);

                    nudFreqMax.Value = Convert.ToDecimal(sFreqMax, Functions.format);
                    curVal_SpecialFrequencies.FreqMaxDGV = Convert.ToInt32(Convert.ToDecimal(dFreqMax, Functions.format) * 10);
                }

                // режим (ведущая, ведомая)
                curVal_SpecialFrequencies.TableDGV = Convert.ToByte(dgv.Rows[dgv.SelectedRows[0].Index].Cells[3].Value);
            }
        }

        private void rbOwn_CheckedChanged(object sender, EventArgs e)
        {
            if (rbOwn.Checked)
            {
                f_rbOwn = true;
                f_rbMated = false;
            }
            else
            {
                f_rbOwn = false;
                f_rbMated = true;
            }

            VisibleTable();
        }

        private void tabControl_Selected(object sender, TabControlEventArgs e)
        {
            tcIndex = tabControl.SelectedIndex;

            if (tcIndex == 2) 
            { 
                bOne_KRPU.Visible = true;
                bAll_KRPU.Visible = true; 
            }
            else 
            {
                bOne_KRPU.Visible = false;
                bAll_KRPU.Visible = false; 
            }
         
            VisibleTable();
        }

        /// <summary>
        /// В зависимости от переключения radioButton и tabPage
        /// отображается таблица dgv
        /// </summary>
        private void VisibleTable()
        {
            if (tcIndex == 0 && f_rbOwn)
            {
                dgvFreqForbidden1.Visible = true;
                dgvFreqForbidden2.Visible = false;

                SetCurParamToControls(dgvFreqForbidden1.dgv);
            }
            else if (tcIndex == 0 && f_rbMated)
            {
                dgvFreqForbidden1.Visible = false;
                dgvFreqForbidden2.Visible = true;

                SetCurParamToControls(dgvFreqForbidden2.dgv);
            }
            else if (tcIndex == 1 && f_rbOwn)
            {
                dgvFreqKnown1.Visible = true;
                dgvFreqKnown2.Visible = false;

                SetCurParamToControls(dgvFreqKnown1.dgv);
            }
            else if (tcIndex == 1 && f_rbMated)
            {
                dgvFreqKnown1.Visible = false;
                dgvFreqKnown2.Visible = true;

                SetCurParamToControls(dgvFreqKnown2.dgv);
            }
            else if (tcIndex == 2 && f_rbOwn)
            {
                dgvFreqImportant1.Visible = true;
                dgvFreqImportant2.Visible = false;

                SetCurParamToControls(dgvFreqImportant1.dgv);
            }
            else if (tcIndex == 2 && f_rbMated)
            {
                dgvFreqImportant1.Visible = false;
                dgvFreqImportant2.Visible = true;

                SetCurParamToControls(dgvFreqImportant2.dgv);
            }
        }

        private void ClearRecords(myDataGridView myDGV, TableColumn[] tableColumn, NameTable nameTable, Table table)
        {
            int count = 0;

            for (int i = 0; i < myDGV.dgv.RowCount; i++)
            {
                if (myDGV.dgv.Rows[i].Cells[0].Value == null)
                    count++;
            }

            if (count == myDGV.dgv.RowCount)
                return;
           
            // удалить все записи из базы данных
            if (functionsDB.DeleteAllRecordsDB(nameTable, table))
            {
                funcDB_SpecialFrequencies.AddSpecialFrequenciesToVariableWork(nameTable, table);

                // добавить пустые строки при необходимости
                myDGV.SetInitialNumberOfRows(myDGV.dgv, tableColumn, nameTable);
            }
        }

        private void bClear_Click(object sender, EventArgs e)
        {
            // таблица Запрещенные частоты (ведущая)
            if (dgvFreqForbidden1.Visible)
            {
                if (f_rbOwn && tcIndex == 0)
                {
                    ClearRecords(dgvFreqForbidden1, dgvFreqForbidden1.tableFreqForbidden, NameTable.FREQ_FORBIDDEN, Table.Own);
                }
            }

            // таблица Запрещенные частоты (ведомая)
            if (dgvFreqForbidden2.Visible)
            {
                if (f_rbMated && tcIndex == 0)
                {
                    ClearRecords(dgvFreqForbidden2, dgvFreqForbidden2.tableFreqForbidden, NameTable.FREQ_FORBIDDEN, Table.Linked);
                }
            }

            // таблица Известные частоты (ведущая)
            if (dgvFreqKnown1.Visible)
            {
                if (f_rbOwn && tcIndex == 1)
                {
                    ClearRecords(dgvFreqKnown1, dgvFreqKnown1.tableFreqKnown, NameTable.FREQ_KNOWN, Table.Own);
                }
            }

            // таблица Известные частоты (ведомая)
            if (dgvFreqKnown2.Visible)
            {
                if (f_rbMated && tcIndex == 1)
                {
                    ClearRecords(dgvFreqKnown2, dgvFreqKnown2.tableFreqKnown, NameTable.FREQ_KNOWN, Table.Linked);
                }
            }

            // таблица Важные частоты (ведущая)
            if (dgvFreqImportant1.Visible)
            {
                if (f_rbOwn && tcIndex == 2)
                {
                    ClearRecords(dgvFreqImportant1, dgvFreqImportant1.tableFreqImportant, NameTable.FREQ_IMPORTANT, Table.Own);
                }
            }

            // таблица Важные частоты (ведомая)
            if (dgvFreqImportant2.Visible)
            {
                if (f_rbMated && tcIndex == 2)
                {
                    ClearRecords(dgvFreqImportant2, dgvFreqImportant2.tableFreqImportant, NameTable.FREQ_IMPORTANT, Table.Linked);
                }
            }
        }

        private void RemoveRecord(myDataGridView myDGV, TableColumn[] tableColumn, NameTable nameTable, Table table)
        {
            int iID = 0;

            if (myDGV.dgv.Rows[myDGV.dgv.SelectedRows[0].Index].Cells[0].Value == null)
                return;

            iID = Convert.ToInt32(myDGV.dgv.Rows[myDGV.dgv.SelectedRows[0].Index].Cells[0].Value);

            // удалить запись из базы данных
            if (functionsDB.DeleteOneRecordDB(iID, nameTable, table))
            {
                funcDB_SpecialFrequencies.AddSpecialFrequenciesToVariableWork(nameTable, table);

                // добавить пустые строки в dgv при необходимости
                myDGV.SetInitialNumberOfRows(myDGV.dgv, tableColumn, nameTable);
            }
        }

        private void bDelete_Click(object sender, EventArgs e)
        {
            // таблица Запрещенные частоты (ведущая)
            if (dgvFreqForbidden1.Visible)
            {
                if (f_rbOwn && tcIndex == 0)
                {
                    RemoveRecord(dgvFreqForbidden1, dgvFreqForbidden1.tableFreqForbidden, NameTable.FREQ_FORBIDDEN, Table.Own);
                }
            }

            // таблица Запрещенные частоты (ведомая)
            if (dgvFreqForbidden2.Visible)
            {
                if (f_rbMated && tcIndex == 0)
                {
                    RemoveRecord(dgvFreqForbidden2, dgvFreqForbidden2.tableFreqForbidden, NameTable.FREQ_FORBIDDEN, Table.Linked);
                }
            }

            // таблица Известные частоты (ведущая)
            if (dgvFreqKnown1.Visible)
            {
                if (f_rbOwn && tcIndex == 1)
                {
                    RemoveRecord(dgvFreqKnown1, dgvFreqKnown1.tableFreqKnown, NameTable.FREQ_KNOWN, Table.Own);
                }
            }

            // таблица Известные частоты (ведомая)
            if (dgvFreqKnown2.Visible)
            {
                if (f_rbMated && tcIndex == 1)
                {
                    RemoveRecord(dgvFreqKnown2, dgvFreqKnown2.tableFreqKnown, NameTable.FREQ_KNOWN, Table.Linked);
                }
            }

            // таблица Важные частоты (ведущая)
            if (dgvFreqImportant1.Visible)
            {
                if (f_rbOwn && tcIndex == 2)
                {
                    RemoveRecord(dgvFreqImportant1, dgvFreqImportant1.tableFreqImportant, NameTable.FREQ_IMPORTANT, Table.Own);
                }
            }

            // таблица Важные частоты (ведомая)
            if (dgvFreqImportant2.Visible)
            {
                if (f_rbMated && tcIndex == 2)
                {
                    RemoveRecord(dgvFreqImportant2, dgvFreqImportant2.tableFreqImportant, NameTable.FREQ_IMPORTANT, Table.Linked);
                }
            }
        }

        private void AddRecord(myDataGridView myDGV, TableColumn[] tableColumn, NameTable nameTable, Table table)
        {
            byte bTable = 0;
            if (table == Table.Own) bTable = 1;
            if (table == Table.Linked) bTable = 2;

            struct_SpecialFrequencies.iID = functions.LastID(myDGV.dgv);
            struct_SpecialFrequencies.iFreqMin = Convert.ToInt32(nudFreqMin.Value * 10);
            struct_SpecialFrequencies.iFreqMax = Convert.ToInt32(nudFreqMax.Value * 10);
            struct_SpecialFrequencies.bMode = bTable;

            if (!chbRange.Checked)
            {
                if (functions.OneSpecFreq(myDGV.dgv, struct_SpecialFrequencies.iFreqMin))
                {
                    if (funcDB_SpecialFrequencies.AddRecord_SpecialFreqToDB(nameTable, struct_SpecialFrequencies))
                    {
                        funcDB_SpecialFrequencies.AddSpecialFrequenciesToVariableWork(nameTable, table);

                        // добавить пустые строки в dgv при необходимости
                        myDGV.SetInitialNumberOfRows(myDGV.dgv, tableColumn, nameTable);
                    }
                }
            }
            else
            {
                if (functions.CorrectFreqMinMax(struct_SpecialFrequencies.iFreqMin, struct_SpecialFrequencies.iFreqMax))
                {
                    if (functions.JoinRangesSpecialFrequencies(myDGV.dgv, struct_SpecialFrequencies.iFreqMin, struct_SpecialFrequencies.iFreqMax))
                    {
                        if (funcDB_SpecialFrequencies.AddRecord_SpecialFreqToDB(nameTable, struct_SpecialFrequencies))
                        {
                            funcDB_SpecialFrequencies.AddSpecialFrequenciesToVariableWork(nameTable, table);

                            // добавить пустые строки в dgv при необходимости
                            myDGV.SetInitialNumberOfRows(myDGV.dgv, tableColumn, nameTable);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Для добавления записи в таблицы спец. частот от кого-то по событию
        /// </summary>
        /// <param name="myDGV"></param>
        /// <param name="tableColumn"></param>
        /// <param name="nameTable"></param>
        /// <param name="bTable"></param>
        /// <param name="iFreqMin"></param>
        /// <param name="iFreqMax"></param>
        private void AddRecord(myDataGridView myDGV, TableColumn[] tableColumn, NameTable nameTable, Table table, int iFreqMin, int iFreqMax)
        {
            byte bTable = 0;
            if (table == Table.Own) bTable = 1;
            if (table == Table.Linked) bTable = 2;

            struct_SpecialFrequencies.iID = functions.LastID(myDGV.dgv);
            struct_SpecialFrequencies.iFreqMin = iFreqMin;
            struct_SpecialFrequencies.iFreqMax = iFreqMax;
            struct_SpecialFrequencies.bMode = (byte)bTable;

            if (iFreqMin == iFreqMax)
            {
                if (functions.OneSpecFreq(myDGV.dgv, struct_SpecialFrequencies.iFreqMin))
                {
                    if (funcDB_SpecialFrequencies.AddRecord_SpecialFreqToDB(nameTable, struct_SpecialFrequencies))
                    {
                        funcDB_SpecialFrequencies.AddSpecialFrequenciesToVariableWork(nameTable, table);

                        // добавить пустые строки в dgv при необходимости
                        myDGV.SetInitialNumberOfRows(myDGV.dgv, tableColumn, nameTable);
                    }
                }
            }
            else
            {
                if (functions.CorrectFreqMinMax(struct_SpecialFrequencies.iFreqMin, struct_SpecialFrequencies.iFreqMax))
                {
                    if (functions.JoinRangesSpecialFrequencies(myDGV.dgv, struct_SpecialFrequencies.iFreqMin, struct_SpecialFrequencies.iFreqMax))
                    {
                        if (funcDB_SpecialFrequencies.AddRecord_SpecialFreqToDB(nameTable, struct_SpecialFrequencies))
                        {
                            funcDB_SpecialFrequencies.AddSpecialFrequenciesToVariableWork(nameTable, table);

                            // добавить пустые строки в dgv при необходимости
                            myDGV.SetInitialNumberOfRows(myDGV.dgv, tableColumn, nameTable);
                        }
                    }
                }
            }
        }

        private void bAdd_Click(object sender, EventArgs e)
        {
            functions.fChangeRec = false;

            // таблица Запрещенные частоты (ведущая)
            if (dgvFreqForbidden1.Visible)
            {
                if (f_rbOwn && tcIndex == 0)
                {
                    AddRecord(dgvFreqForbidden1, dgvFreqForbidden1.tableFreqForbidden, NameTable.FREQ_FORBIDDEN, Table.Own);
                }
            }

            // таблица Запрещенные частоты (ведомая)
            if (dgvFreqForbidden2.Visible)
            {
                if (f_rbMated && tcIndex == 0)
                {
                    AddRecord(dgvFreqForbidden2, dgvFreqForbidden2.tableFreqForbidden, NameTable.FREQ_FORBIDDEN, Table.Linked);
                }
            }

            // таблица Известные частоты (ведущая)
            if (dgvFreqKnown1.Visible)
            {
                if (f_rbOwn && tcIndex == 1)
                {
                    AddRecord(dgvFreqKnown1, dgvFreqKnown1.tableFreqKnown, NameTable.FREQ_KNOWN, Table.Own);
                }
            }

            // таблица Известные частоты (ведомая)
            if (dgvFreqKnown2.Visible)
            {
                if (f_rbMated && tcIndex == 1)
                {
                    AddRecord(dgvFreqKnown2, dgvFreqKnown2.tableFreqKnown, NameTable.FREQ_KNOWN, Table.Linked);
                }
            }

            // таблица Известные частоты (ведомая)
            if (dgvFreqImportant1.Visible)
            {
                if (f_rbOwn && tcIndex == 2)
                {
                    AddRecord(dgvFreqImportant1, dgvFreqImportant1.tableFreqImportant, NameTable.FREQ_IMPORTANT, Table.Own);
                }
            }

            // таблица Известные частоты (ведущая)
            if (dgvFreqImportant2.Visible)
            {
                if (f_rbMated && tcIndex == 2)
                {
                    AddRecord(dgvFreqImportant2, dgvFreqImportant2.tableFreqImportant, NameTable.FREQ_IMPORTANT, Table.Linked);
                }
            }
        }

        private void chbRange_CheckedChanged(object sender, EventArgs e)
        {
            if (!chbRange.Checked)
            {
                nudFreqMax.Enabled = false;
                nudFreqMax.Value = nudFreqMin.Value;

            }
            else nudFreqMax.Enabled = true;
        }

        private void nudFreqMin_ValueChanged(object sender, EventArgs e)
        {
            if (!chbRange.Checked)
                nudFreqMax.Value = nudFreqMin.Value;
        }

        private void ChangeRecord(myDataGridView myDGV, TableColumn[] tableColumn, NameTable nameTable, Table table)
        {
            byte bTable = 0;
            if (table == Table.Own) bTable = 1;
            if (table == Table.Linked) bTable = 2;

            if (myDGV.dgv.Rows[myDGV.dgv.SelectedRows[0].Index].Cells[0].Value == null)
                return;

            struct_SpecialFrequencies = new Struct_SpecialFrequencies();

            struct_SpecialFrequencies.iID = Convert.ToInt32(myDGV.dgv.Rows[myDGV.dgv.SelectedRows[0].Index].Cells[0].Value);
            struct_SpecialFrequencies.iFreqMin = Convert.ToInt32(nudFreqMin.Value * 10);
            struct_SpecialFrequencies.iFreqMax = Convert.ToInt32(nudFreqMax.Value * 10);
            struct_SpecialFrequencies.bMode = bTable;

            if (!chbRange.Checked)
            {
                if (functions.JoinRangesSpecialFrequencies(myDGV.dgv, struct_SpecialFrequencies.iFreqMin, struct_SpecialFrequencies.iFreqMax))
                {
                    if (funcDB_SpecialFrequencies.ChangeRecord_SpecialFreqToDB(nameTable, struct_SpecialFrequencies, curVal_SpecialFrequencies))
                    {
                        funcDB_SpecialFrequencies.AddSpecialFrequenciesToVariableWork(nameTable, table);

                        // добавить пустые строки в dgv при необходимости
                        myDGV.SetInitialNumberOfRows(myDGV.dgv, tableColumn, nameTable);
                    }
                }
            }
            else
            {
                if (functionsDGV.AmountRecordsRangesDGV(myDGV.dgv) > 1)
                {
                    if (functions.CorrectFreqMinMax(struct_SpecialFrequencies.iFreqMin, struct_SpecialFrequencies.iFreqMax))
                    {
                        if (functions.JoinRangesSpecialFrequencies(myDGV.dgv, struct_SpecialFrequencies.iFreqMin, struct_SpecialFrequencies.iFreqMax))
                        {
                            if (funcDB_SpecialFrequencies.ChangeRecord_SpecialFreqToDB(nameTable, struct_SpecialFrequencies, curVal_SpecialFrequencies))
                            {
                                funcDB_SpecialFrequencies.AddSpecialFrequenciesToVariableWork(nameTable, table);

                                // добавить пустые строки в dgv при необходимости
                                myDGV.SetInitialNumberOfRows(myDGV.dgv, tableColumn, nameTable);
                            }
                        }
                    }
                }
                else
                {
                    if (functions.CorrectFreqMinMax(struct_SpecialFrequencies.iFreqMin, struct_SpecialFrequencies.iFreqMax))
                    {
                        if (funcDB_SpecialFrequencies.ChangeRecord_SpecialFreqToDB(nameTable, struct_SpecialFrequencies, curVal_SpecialFrequencies))
                        {
                            funcDB_SpecialFrequencies.AddSpecialFrequenciesToVariableWork(nameTable, table);

                            // добавить пустые строки в dgv при необходимости
                            myDGV.SetInitialNumberOfRows(myDGV.dgv, tableColumn, nameTable);
                        }
                    }
                }
            }
        }

        private void bChange_Click(object sender, EventArgs e)
        {
            functions.fChangeRec = true;

            // таблица Запрещенные частоты (ведущая)
            if (dgvFreqForbidden1.Visible)
            {
                if (f_rbOwn && tcIndex == 0)
                {
                    ChangeRecord(dgvFreqForbidden1, dgvFreqForbidden1.tableFreqForbidden, NameTable.FREQ_FORBIDDEN, Table.Own);
                }
            }

            // таблица Запрещенные частоты (ведомая)
            if (dgvFreqForbidden2.Visible)
            {
                if (f_rbMated && tcIndex == 0)
                {
                    ChangeRecord(dgvFreqForbidden2, dgvFreqForbidden2.tableFreqForbidden, NameTable.FREQ_FORBIDDEN, Table.Linked);
                }
            }

            // таблица Известные частоты (ведущая)
            if (dgvFreqKnown1.Visible)
            {
                if (f_rbOwn && tcIndex == 1)
                {
                    ChangeRecord(dgvFreqKnown1, dgvFreqKnown1.tableFreqKnown, NameTable.FREQ_KNOWN, Table.Own);
                }
            }

            // таблица Известные частоты (ведомая)
            if (dgvFreqKnown2.Visible)
            {
                if (f_rbMated && tcIndex == 1)
                {
                    ChangeRecord(dgvFreqKnown2, dgvFreqKnown2.tableFreqKnown, NameTable.FREQ_KNOWN, Table.Linked);
                }
            }

            // таблица Важные частоты (ведущая)
            if (dgvFreqImportant1.Visible)
            {
                if (f_rbOwn && tcIndex == 2)
                {
                    ChangeRecord(dgvFreqImportant1, dgvFreqImportant1.tableFreqImportant, NameTable.FREQ_IMPORTANT, Table.Own);
                }
            }

            // таблица Важные частоты (ведущая)
            if (dgvFreqImportant2.Visible)
            {
                if (f_rbMated && tcIndex == 2)
                {
                    ChangeRecord(dgvFreqImportant2, dgvFreqImportant2.tableFreqImportant, NameTable.FREQ_IMPORTANT, Table.Linked);
                }
            }
        }

        /// <summary>
        /// Одну важную частоту на КРПУ
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bOne_KRPU_Click(object sender, EventArgs e)
        {
            // таблица Важные частоты (ведущая)
            if (dgvFreqImportant1.Visible)
            {
                if (dgvFreqImportant1.dgv.Rows[dgvFreqImportant1.dgv.SelectedRows[0].Index].Cells[0].Value == null) { return; }
                else variableWork.FreqImportantKRPU = AddFreqImportant(dgvFreqImportant1.dgv, 1);
            }

            // таблица Важные частоты (ведущая)
            if (dgvFreqImportant2.Visible)
            {
                if (dgvFreqImportant2.dgv.Rows[dgvFreqImportant2.dgv.SelectedRows[0].Index].Cells[0].Value == null) { return; }
                else variableWork.FreqImportantKRPU = AddFreqImportant(dgvFreqImportant2.dgv, 1);
            }
           
        }

        /// <summary>
        /// Добавить в массив Важные частоты для отправки на КРПУ
        /// </summary>
        /// <param name="dgv"> DataGridView </param>
        /// <param name="count"> количество частот </param>
        /// <returns> массив Важные частоты </returns>
        public TFreqImportantKRPU[] AddFreqImportant(DataGridView dgv, int count)
        {
            FreqImportantKRPU = new TFreqImportantKRPU[count];

            if (count == 1)
            {
                FreqImportantKRPU[0].iID = Convert.ToInt32(dgv.Rows[dgv.SelectedRows[0].Index].Cells[0].Value);
                decimal dFreqMin = Convert.ToDecimal(dgv.Rows[dgv.SelectedRows[0].Index].Cells[1].Value, Functions.format);
                FreqImportantKRPU[0].iFreqMin = Convert.ToInt64(Convert.ToDecimal(dFreqMin, Functions.format) * 1000);

                // значение частоты max
                if (dgv.Rows[dgv.SelectedRows[0].Index].Cells[2].Value.ToString() == "————")
                {
                    FreqImportantKRPU[0].iFreqMax = Convert.ToInt64(Convert.ToDecimal(dFreqMin, Functions.format) * 1000);
                }
                else
                {
                    decimal dFreqMax = Convert.ToDecimal(dgv.Rows[dgv.SelectedRows[0].Index].Cells[2].Value, Functions.format);
                    FreqImportantKRPU[0].iFreqMax = Convert.ToInt64(Convert.ToDecimal(dFreqMax, Functions.format) * 1000);
                }
            }
            else
            {
                for (int i = 0; i < count; i++)
                {
                    FreqImportantKRPU[i].iID = Convert.ToInt32(dgv.Rows[i].Cells[0].Value);
                    decimal dFreqMin = Convert.ToDecimal(dgv.Rows[i].Cells[1].Value, Functions.format);
                    FreqImportantKRPU[i].iFreqMin = Convert.ToInt64(Convert.ToDecimal(dFreqMin, Functions.format) * 1000);

                    // значение частоты max
                    if (dgv.Rows[i].Cells[2].Value.ToString() == "————")
                    {
                        FreqImportantKRPU[i].iFreqMax = Convert.ToInt64(Convert.ToDecimal(dFreqMin, Functions.format) * 1000);
                    }
                    else
                    {
                        decimal dFreqMax = Convert.ToDecimal(dgv.Rows[i].Cells[2].Value, Functions.format);
                        FreqImportantKRPU[i].iFreqMax = Convert.ToInt64(Convert.ToDecimal(dFreqMax, Functions.format) * 1000);
                    }
                }
            }

            return FreqImportantKRPU;
        }

        /// <summary>
        /// Все важные частоты на КРПУ
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bAll_KRPU_Click(object sender, EventArgs e)
        {
            // таблица Важные частоты (ведущая)
            if (dgvFreqImportant1.Visible)
            {
                if (variableWork.FrequencyRangeImportantOwn.Length != 0)
                    variableWork.FreqImportantKRPU = AddFreqImportant(dgvFreqImportant1.dgv, variableWork.FrequencyRangeImportantOwn.Length);
            }

            // таблица Важные частоты (ведущая)
            if (dgvFreqImportant2.Visible)
            {
                if (variableWork.FrequencyRangeImportantLinked.Length != 0)
                    variableWork.FreqImportantKRPU = AddFreqImportant(dgvFreqImportant2.dgv, variableWork.FrequencyRangeImportantLinked.Length);
            }
        }
    }
}
