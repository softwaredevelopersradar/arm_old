﻿namespace Tables_SpecialFrequencies
{
    partial class tables_SpecialFrequencies
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.pParam = new System.Windows.Forms.Panel();
            this.grbRadioButtons = new System.Windows.Forms.GroupBox();
            this.rbMated = new System.Windows.Forms.RadioButton();
            this.rbOwn = new System.Windows.Forms.RadioButton();
            this.pButtons = new System.Windows.Forms.Panel();
            this.bAll_KRPU = new System.Windows.Forms.Button();
            this.bOne_KRPU = new System.Windows.Forms.Button();
            this.bChange = new System.Windows.Forms.Button();
            this.bAdd = new System.Windows.Forms.Button();
            this.bClear = new System.Windows.Forms.Button();
            this.bDelete = new System.Windows.Forms.Button();
            this.grbRange = new System.Windows.Forms.GroupBox();
            this.nudFreqMax = new System.Windows.Forms.NumericUpDown();
            this.lFreqMax = new System.Windows.Forms.Label();
            this.nudFreqMin = new System.Windows.Forms.NumericUpDown();
            this.lFreqMin = new System.Windows.Forms.Label();
            this.chbRange = new System.Windows.Forms.CheckBox();
            this.pTables = new System.Windows.Forms.Panel();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tpForbidden = new System.Windows.Forms.TabPage();
            this.tpKnown = new System.Windows.Forms.TabPage();
            this.tpImportant = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.dgvFreqForbidden1 = new MyDataGridView.myDataGridView();
            this.dgvFreqForbidden2 = new MyDataGridView.myDataGridView();
            this.dgvFreqKnown2 = new MyDataGridView.myDataGridView();
            this.dgvFreqKnown1 = new MyDataGridView.myDataGridView();
            this.dgvFreqImportant2 = new MyDataGridView.myDataGridView();
            this.dgvFreqImportant1 = new MyDataGridView.myDataGridView();
            this.pParam.SuspendLayout();
            this.grbRadioButtons.SuspendLayout();
            this.pButtons.SuspendLayout();
            this.grbRange.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudFreqMax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudFreqMin)).BeginInit();
            this.pTables.SuspendLayout();
            this.tabControl.SuspendLayout();
            this.tpForbidden.SuspendLayout();
            this.tpKnown.SuspendLayout();
            this.tpImportant.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pParam
            // 
            this.pParam.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pParam.BackColor = System.Drawing.SystemColors.Control;
            this.pParam.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pParam.Controls.Add(this.grbRadioButtons);
            this.pParam.Controls.Add(this.pButtons);
            this.pParam.Controls.Add(this.grbRange);
            this.pParam.Location = new System.Drawing.Point(269, 3);
            this.pParam.Name = "pParam";
            this.pParam.Size = new System.Drawing.Size(194, 243);
            this.pParam.TabIndex = 18;
            // 
            // grbRadioButtons
            // 
            this.grbRadioButtons.Controls.Add(this.rbMated);
            this.grbRadioButtons.Controls.Add(this.rbOwn);
            this.grbRadioButtons.Location = new System.Drawing.Point(4, 2);
            this.grbRadioButtons.Name = "grbRadioButtons";
            this.grbRadioButtons.Size = new System.Drawing.Size(188, 28);
            this.grbRadioButtons.TabIndex = 24;
            this.grbRadioButtons.TabStop = false;
            // 
            // rbMated
            // 
            this.rbMated.AutoSize = true;
            this.rbMated.Location = new System.Drawing.Point(100, 9);
            this.rbMated.Name = "rbMated";
            this.rbMated.Size = new System.Drawing.Size(72, 17);
            this.rbMated.TabIndex = 14;
            this.rbMated.Text = "Ведомый";
            this.rbMated.UseVisualStyleBackColor = true;
            // 
            // rbOwn
            // 
            this.rbOwn.AutoSize = true;
            this.rbOwn.Checked = true;
            this.rbOwn.ForeColor = System.Drawing.Color.Red;
            this.rbOwn.Location = new System.Drawing.Point(10, 9);
            this.rbOwn.Name = "rbOwn";
            this.rbOwn.Size = new System.Drawing.Size(70, 17);
            this.rbOwn.TabIndex = 13;
            this.rbOwn.TabStop = true;
            this.rbOwn.Text = "Ведущий";
            this.rbOwn.UseVisualStyleBackColor = true;
            this.rbOwn.CheckedChanged += new System.EventHandler(this.rbOwn_CheckedChanged);
            // 
            // pButtons
            // 
            this.pButtons.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pButtons.Controls.Add(this.bAll_KRPU);
            this.pButtons.Controls.Add(this.bOne_KRPU);
            this.pButtons.Controls.Add(this.bChange);
            this.pButtons.Controls.Add(this.bAdd);
            this.pButtons.Controls.Add(this.bClear);
            this.pButtons.Controls.Add(this.bDelete);
            this.pButtons.Location = new System.Drawing.Point(-1, 114);
            this.pButtons.Name = "pButtons";
            this.pButtons.Size = new System.Drawing.Size(188, 124);
            this.pButtons.TabIndex = 16;
            // 
            // bAll_KRPU
            // 
            this.bAll_KRPU.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.bAll_KRPU.Location = new System.Drawing.Point(96, 3);
            this.bAll_KRPU.Name = "bAll_KRPU";
            this.bAll_KRPU.Size = new System.Drawing.Size(88, 23);
            this.bAll_KRPU.TabIndex = 33;
            this.bAll_KRPU.Text = "Все на КРПУ";
            this.bAll_KRPU.UseVisualStyleBackColor = true;
            this.bAll_KRPU.Visible = false;
            this.bAll_KRPU.Click += new System.EventHandler(this.bAll_KRPU_Click);
            // 
            // bOne_KRPU
            // 
            this.bOne_KRPU.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.bOne_KRPU.Location = new System.Drawing.Point(4, 3);
            this.bOne_KRPU.Name = "bOne_KRPU";
            this.bOne_KRPU.Size = new System.Drawing.Size(88, 23);
            this.bOne_KRPU.TabIndex = 32;
            this.bOne_KRPU.Text = "На КРПУ";
            this.bOne_KRPU.UseVisualStyleBackColor = true;
            this.bOne_KRPU.Visible = false;
            this.bOne_KRPU.Click += new System.EventHandler(this.bOne_KRPU_Click);
            // 
            // bChange
            // 
            this.bChange.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.bChange.ForeColor = System.Drawing.Color.Green;
            this.bChange.Location = new System.Drawing.Point(4, 99);
            this.bChange.Name = "bChange";
            this.bChange.Size = new System.Drawing.Size(88, 23);
            this.bChange.TabIndex = 15;
            this.bChange.Text = "Изменить";
            this.bChange.UseVisualStyleBackColor = true;
            this.bChange.Click += new System.EventHandler(this.bChange_Click);
            // 
            // bAdd
            // 
            this.bAdd.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.bAdd.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.bAdd.Location = new System.Drawing.Point(96, 99);
            this.bAdd.Name = "bAdd";
            this.bAdd.Size = new System.Drawing.Size(88, 23);
            this.bAdd.TabIndex = 14;
            this.bAdd.Text = "Добавить";
            this.bAdd.UseVisualStyleBackColor = true;
            this.bAdd.Click += new System.EventHandler(this.bAdd_Click);
            // 
            // bClear
            // 
            this.bClear.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.bClear.ForeColor = System.Drawing.Color.Red;
            this.bClear.Location = new System.Drawing.Point(4, 76);
            this.bClear.Name = "bClear";
            this.bClear.Size = new System.Drawing.Size(88, 23);
            this.bClear.TabIndex = 13;
            this.bClear.Text = "Очистить";
            this.bClear.UseVisualStyleBackColor = true;
            this.bClear.Click += new System.EventHandler(this.bClear_Click);
            // 
            // bDelete
            // 
            this.bDelete.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.bDelete.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.bDelete.Location = new System.Drawing.Point(96, 76);
            this.bDelete.Name = "bDelete";
            this.bDelete.Size = new System.Drawing.Size(88, 23);
            this.bDelete.TabIndex = 10;
            this.bDelete.Text = "Удалить";
            this.bDelete.UseVisualStyleBackColor = true;
            this.bDelete.Click += new System.EventHandler(this.bDelete_Click);
            // 
            // grbRange
            // 
            this.grbRange.Controls.Add(this.nudFreqMax);
            this.grbRange.Controls.Add(this.lFreqMax);
            this.grbRange.Controls.Add(this.nudFreqMin);
            this.grbRange.Controls.Add(this.lFreqMin);
            this.grbRange.Controls.Add(this.chbRange);
            this.grbRange.ForeColor = System.Drawing.Color.Blue;
            this.grbRange.Location = new System.Drawing.Point(4, 41);
            this.grbRange.Name = "grbRange";
            this.grbRange.Size = new System.Drawing.Size(188, 67);
            this.grbRange.TabIndex = 4;
            this.grbRange.TabStop = false;
            // 
            // nudFreqMax
            // 
            this.nudFreqMax.DecimalPlaces = 1;
            this.nudFreqMax.Location = new System.Drawing.Point(108, 39);
            this.nudFreqMax.Maximum = new decimal(new int[] {
            3000000,
            0,
            0,
            0});
            this.nudFreqMax.Minimum = new decimal(new int[] {
            30000,
            0,
            0,
            0});
            this.nudFreqMax.Name = "nudFreqMax";
            this.nudFreqMax.Size = new System.Drawing.Size(75, 20);
            this.nudFreqMax.TabIndex = 12;
            this.nudFreqMax.ThousandsSeparator = true;
            this.nudFreqMax.Value = new decimal(new int[] {
            30000,
            0,
            0,
            0});
            // 
            // lFreqMax
            // 
            this.lFreqMax.AutoSize = true;
            this.lFreqMax.ForeColor = System.Drawing.Color.Black;
            this.lFreqMax.Location = new System.Drawing.Point(2, 44);
            this.lFreqMax.Name = "lFreqMax";
            this.lFreqMax.Size = new System.Drawing.Size(123, 13);
            this.lFreqMax.TabIndex = 11;
            this.lFreqMax.Text = "Частота макс., кГц......";
            // 
            // nudFreqMin
            // 
            this.nudFreqMin.DecimalPlaces = 1;
            this.nudFreqMin.Location = new System.Drawing.Point(108, 17);
            this.nudFreqMin.Maximum = new decimal(new int[] {
            3000000,
            0,
            0,
            0});
            this.nudFreqMin.Minimum = new decimal(new int[] {
            30000,
            0,
            0,
            0});
            this.nudFreqMin.Name = "nudFreqMin";
            this.nudFreqMin.Size = new System.Drawing.Size(75, 20);
            this.nudFreqMin.TabIndex = 10;
            this.nudFreqMin.ThousandsSeparator = true;
            this.nudFreqMin.Value = new decimal(new int[] {
            30000,
            0,
            0,
            0});
            this.nudFreqMin.ValueChanged += new System.EventHandler(this.nudFreqMin_ValueChanged);
            // 
            // lFreqMin
            // 
            this.lFreqMin.AutoSize = true;
            this.lFreqMin.ForeColor = System.Drawing.Color.Black;
            this.lFreqMin.Location = new System.Drawing.Point(1, 23);
            this.lFreqMin.Name = "lFreqMin";
            this.lFreqMin.Size = new System.Drawing.Size(117, 13);
            this.lFreqMin.TabIndex = 0;
            this.lFreqMin.Text = "Частота мин., кГц......";
            // 
            // chbRange
            // 
            this.chbRange.AutoSize = true;
            this.chbRange.Checked = true;
            this.chbRange.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chbRange.Location = new System.Drawing.Point(10, 0);
            this.chbRange.Name = "chbRange";
            this.chbRange.Size = new System.Drawing.Size(77, 17);
            this.chbRange.TabIndex = 13;
            this.chbRange.Text = "Диапазон";
            this.chbRange.UseVisualStyleBackColor = true;
            this.chbRange.CheckedChanged += new System.EventHandler(this.chbRange_CheckedChanged);
            // 
            // pTables
            // 
            this.pTables.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pTables.Controls.Add(this.tabControl);
            this.pTables.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pTables.Location = new System.Drawing.Point(3, 3);
            this.pTables.Name = "pTables";
            this.pTables.Size = new System.Drawing.Size(260, 243);
            this.pTables.TabIndex = 19;
            // 
            // tabControl
            // 
            this.tabControl.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
            this.tabControl.Controls.Add(this.tpForbidden);
            this.tabControl.Controls.Add(this.tpKnown);
            this.tabControl.Controls.Add(this.tpImportant);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Location = new System.Drawing.Point(0, 0);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(258, 241);
            this.tabControl.TabIndex = 0;
            this.tabControl.Selected += new System.Windows.Forms.TabControlEventHandler(this.tabControl_Selected);
            // 
            // tpForbidden
            // 
            this.tpForbidden.Controls.Add(this.dgvFreqForbidden2);
            this.tpForbidden.Controls.Add(this.dgvFreqForbidden1);
            this.tpForbidden.Location = new System.Drawing.Point(4, 25);
            this.tpForbidden.Name = "tpForbidden";
            this.tpForbidden.Padding = new System.Windows.Forms.Padding(3);
            this.tpForbidden.Size = new System.Drawing.Size(250, 212);
            this.tpForbidden.TabIndex = 0;
            this.tpForbidden.Text = "Запрещенные";
            this.tpForbidden.UseVisualStyleBackColor = true;
            // 
            // tpKnown
            // 
            this.tpKnown.Controls.Add(this.dgvFreqKnown1);
            this.tpKnown.Controls.Add(this.dgvFreqKnown2);
            this.tpKnown.Location = new System.Drawing.Point(4, 25);
            this.tpKnown.Name = "tpKnown";
            this.tpKnown.Padding = new System.Windows.Forms.Padding(3);
            this.tpKnown.Size = new System.Drawing.Size(250, 212);
            this.tpKnown.TabIndex = 1;
            this.tpKnown.Text = "Известные";
            this.tpKnown.UseVisualStyleBackColor = true;
            // 
            // tpImportant
            // 
            this.tpImportant.Controls.Add(this.dgvFreqImportant1);
            this.tpImportant.Controls.Add(this.dgvFreqImportant2);
            this.tpImportant.Location = new System.Drawing.Point(4, 25);
            this.tpImportant.Name = "tpImportant";
            this.tpImportant.Padding = new System.Windows.Forms.Padding(3);
            this.tpImportant.Size = new System.Drawing.Size(250, 212);
            this.tpImportant.TabIndex = 3;
            this.tpImportant.Text = "Важные";
            this.tpImportant.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel1.Controls.Add(this.pTables, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.pParam, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(466, 249);
            this.tableLayoutPanel1.TabIndex = 20;
            // 
            // dgvFreqForbidden1
            // 
            this.dgvFreqForbidden1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvFreqForbidden1.Location = new System.Drawing.Point(3, 3);
            this.dgvFreqForbidden1.Name = "dgvFreqForbidden1";
            this.dgvFreqForbidden1.Size = new System.Drawing.Size(244, 206);
            this.dgvFreqForbidden1.TabIndex = 0;
            // 
            // dgvFreqForbidden2
            // 
            this.dgvFreqForbidden2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvFreqForbidden2.Location = new System.Drawing.Point(3, 3);
            this.dgvFreqForbidden2.Name = "dgvFreqForbidden2";
            this.dgvFreqForbidden2.Size = new System.Drawing.Size(244, 206);
            this.dgvFreqForbidden2.TabIndex = 1;
            // 
            // dgvFreqKnown2
            // 
            this.dgvFreqKnown2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvFreqKnown2.Location = new System.Drawing.Point(3, 3);
            this.dgvFreqKnown2.Name = "dgvFreqKnown2";
            this.dgvFreqKnown2.Size = new System.Drawing.Size(244, 206);
            this.dgvFreqKnown2.TabIndex = 0;
            // 
            // dgvFreqKnown1
            // 
            this.dgvFreqKnown1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvFreqKnown1.Location = new System.Drawing.Point(3, 3);
            this.dgvFreqKnown1.Name = "dgvFreqKnown1";
            this.dgvFreqKnown1.Size = new System.Drawing.Size(244, 206);
            this.dgvFreqKnown1.TabIndex = 1;
            // 
            // dgvFreqImportant2
            // 
            this.dgvFreqImportant2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvFreqImportant2.Location = new System.Drawing.Point(3, 3);
            this.dgvFreqImportant2.Name = "dgvFreqImportant2";
            this.dgvFreqImportant2.Size = new System.Drawing.Size(244, 206);
            this.dgvFreqImportant2.TabIndex = 0;
            // 
            // dgvFreqImportant1
            // 
            this.dgvFreqImportant1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvFreqImportant1.Location = new System.Drawing.Point(3, 3);
            this.dgvFreqImportant1.Name = "dgvFreqImportant1";
            this.dgvFreqImportant1.Size = new System.Drawing.Size(244, 206);
            this.dgvFreqImportant1.TabIndex = 1;
            // 
            // tables_SpecialFrequencies
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "tables_SpecialFrequencies";
            this.Size = new System.Drawing.Size(466, 249);
            this.pParam.ResumeLayout(false);
            this.grbRadioButtons.ResumeLayout(false);
            this.grbRadioButtons.PerformLayout();
            this.pButtons.ResumeLayout(false);
            this.grbRange.ResumeLayout(false);
            this.grbRange.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudFreqMax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudFreqMin)).EndInit();
            this.pTables.ResumeLayout(false);
            this.tabControl.ResumeLayout(false);
            this.tpForbidden.ResumeLayout(false);
            this.tpKnown.ResumeLayout(false);
            this.tpImportant.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pParam;
        private System.Windows.Forms.GroupBox grbRadioButtons;
        public System.Windows.Forms.RadioButton rbMated;
        public System.Windows.Forms.RadioButton rbOwn;
        private System.Windows.Forms.Panel pButtons;
        public System.Windows.Forms.Button bChange;
        public System.Windows.Forms.Button bAdd;
        public System.Windows.Forms.Button bClear;
        public System.Windows.Forms.Button bDelete;
        private System.Windows.Forms.GroupBox grbRange;
        private System.Windows.Forms.NumericUpDown nudFreqMax;
        private System.Windows.Forms.Label lFreqMax;
        private System.Windows.Forms.NumericUpDown nudFreqMin;
        private System.Windows.Forms.Label lFreqMin;
        private System.Windows.Forms.Panel pTables;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tpForbidden;
        private System.Windows.Forms.TabPage tpKnown;
        private System.Windows.Forms.TabPage tpImportant;
        public System.Windows.Forms.CheckBox chbRange;
        private System.Windows.Forms.Button bOne_KRPU;
        private System.Windows.Forms.Button bAll_KRPU;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private MyDataGridView.myDataGridView dgvFreqForbidden2;
        private MyDataGridView.myDataGridView dgvFreqForbidden1;
        private MyDataGridView.myDataGridView dgvFreqKnown1;
        private MyDataGridView.myDataGridView dgvFreqKnown2;
        private MyDataGridView.myDataGridView dgvFreqImportant1;
        private MyDataGridView.myDataGridView dgvFreqImportant2;
    }
}
