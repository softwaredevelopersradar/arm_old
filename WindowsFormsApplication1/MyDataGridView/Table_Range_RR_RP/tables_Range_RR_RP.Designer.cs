﻿namespace Table_Range_RR_RP
{
    partial class tables_Range_RR_RP
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tpRangeRR = new System.Windows.Forms.TabPage();
            this.dgvRangeRR_2 = new MyDataGridView.myDataGridView();
            this.dgvRangeRR_1 = new MyDataGridView.myDataGridView();
            this.tpRangeRP = new System.Windows.Forms.TabPage();
            this.dgvRangeRP_2 = new MyDataGridView.myDataGridView();
            this.dgvRangeRP_1 = new MyDataGridView.myDataGridView();
            this.pParam = new System.Windows.Forms.Panel();
            this.grbRadioButtons = new System.Windows.Forms.GroupBox();
            this.rbMated = new System.Windows.Forms.RadioButton();
            this.rbOwn = new System.Windows.Forms.RadioButton();
            this.pButtons = new System.Windows.Forms.Panel();
            this.bChange = new System.Windows.Forms.Button();
            this.bAdd = new System.Windows.Forms.Button();
            this.bClear = new System.Windows.Forms.Button();
            this.bDelete = new System.Windows.Forms.Button();
            this.grbSector = new System.Windows.Forms.GroupBox();
            this.nudAngleMax = new System.Windows.Forms.NumericUpDown();
            this.lAngleMax = new System.Windows.Forms.Label();
            this.nudAngleMin = new System.Windows.Forms.NumericUpDown();
            this.lAngleMin = new System.Windows.Forms.Label();
            this.grbRange = new System.Windows.Forms.GroupBox();
            this.nudFreqMax = new System.Windows.Forms.NumericUpDown();
            this.lFreqMax = new System.Windows.Forms.Label();
            this.nudFreqMin = new System.Windows.Forms.NumericUpDown();
            this.lFreqMin = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.pTables = new System.Windows.Forms.Panel();
            this.tabControl.SuspendLayout();
            this.tpRangeRR.SuspendLayout();
            this.tpRangeRP.SuspendLayout();
            this.pParam.SuspendLayout();
            this.grbRadioButtons.SuspendLayout();
            this.pButtons.SuspendLayout();
            this.grbSector.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudAngleMax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudAngleMin)).BeginInit();
            this.grbRange.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudFreqMax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudFreqMin)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.pTables.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl
            // 
            this.tabControl.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
            this.tabControl.Controls.Add(this.tpRangeRR);
            this.tabControl.Controls.Add(this.tpRangeRP);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Location = new System.Drawing.Point(0, 0);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(317, 240);
            this.tabControl.TabIndex = 0;
            this.tabControl.Selected += new System.Windows.Forms.TabControlEventHandler(this.tabControl_Selected);
            // 
            // tpRangeRR
            // 
            this.tpRangeRR.Controls.Add(this.dgvRangeRR_2);
            this.tpRangeRR.Controls.Add(this.dgvRangeRR_1);
            this.tpRangeRR.Location = new System.Drawing.Point(4, 25);
            this.tpRangeRR.Name = "tpRangeRR";
            this.tpRangeRR.Padding = new System.Windows.Forms.Padding(3);
            this.tpRangeRR.Size = new System.Drawing.Size(309, 211);
            this.tpRangeRR.TabIndex = 0;
            this.tpRangeRR.Text = "Сектора и диапазоны РР";
            this.tpRangeRR.UseVisualStyleBackColor = true;
            // 
            // dgvRangeRR_2
            // 
            this.dgvRangeRR_2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvRangeRR_2.Location = new System.Drawing.Point(3, 3);
            this.dgvRangeRR_2.Name = "dgvRangeRR_2";
            this.dgvRangeRR_2.Size = new System.Drawing.Size(303, 205);
            this.dgvRangeRR_2.TabIndex = 1;
            this.dgvRangeRR_2.Visible = false;
            // 
            // dgvRangeRR_1
            // 
            this.dgvRangeRR_1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvRangeRR_1.Location = new System.Drawing.Point(3, 3);
            this.dgvRangeRR_1.Name = "dgvRangeRR_1";
            this.dgvRangeRR_1.Size = new System.Drawing.Size(303, 205);
            this.dgvRangeRR_1.TabIndex = 0;
            // 
            // tpRangeRP
            // 
            this.tpRangeRP.Controls.Add(this.dgvRangeRP_2);
            this.tpRangeRP.Controls.Add(this.dgvRangeRP_1);
            this.tpRangeRP.Location = new System.Drawing.Point(4, 25);
            this.tpRangeRP.Name = "tpRangeRP";
            this.tpRangeRP.Padding = new System.Windows.Forms.Padding(3);
            this.tpRangeRP.Size = new System.Drawing.Size(311, 215);
            this.tpRangeRP.TabIndex = 1;
            this.tpRangeRP.Text = "Сектора и диапазоны РП";
            this.tpRangeRP.UseVisualStyleBackColor = true;
            // 
            // dgvRangeRP_2
            // 
            this.dgvRangeRP_2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvRangeRP_2.Location = new System.Drawing.Point(3, 3);
            this.dgvRangeRP_2.Name = "dgvRangeRP_2";
            this.dgvRangeRP_2.Size = new System.Drawing.Size(305, 209);
            this.dgvRangeRP_2.TabIndex = 1;
            this.dgvRangeRP_2.Visible = false;
            // 
            // dgvRangeRP_1
            // 
            this.dgvRangeRP_1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvRangeRP_1.Location = new System.Drawing.Point(3, 3);
            this.dgvRangeRP_1.Name = "dgvRangeRP_1";
            this.dgvRangeRP_1.Size = new System.Drawing.Size(305, 209);
            this.dgvRangeRP_1.TabIndex = 0;
            // 
            // pParam
            // 
            this.pParam.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pParam.BackColor = System.Drawing.SystemColors.Control;
            this.pParam.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pParam.Controls.Add(this.grbRadioButtons);
            this.pParam.Controls.Add(this.pButtons);
            this.pParam.Controls.Add(this.grbSector);
            this.pParam.Controls.Add(this.grbRange);
            this.pParam.Location = new System.Drawing.Point(328, 3);
            this.pParam.Name = "pParam";
            this.pParam.Size = new System.Drawing.Size(194, 242);
            this.pParam.TabIndex = 17;
            // 
            // grbRadioButtons
            // 
            this.grbRadioButtons.Controls.Add(this.rbMated);
            this.grbRadioButtons.Controls.Add(this.rbOwn);
            this.grbRadioButtons.Location = new System.Drawing.Point(1, 1);
            this.grbRadioButtons.Name = "grbRadioButtons";
            this.grbRadioButtons.Size = new System.Drawing.Size(189, 30);
            this.grbRadioButtons.TabIndex = 24;
            this.grbRadioButtons.TabStop = false;
            // 
            // rbMated
            // 
            this.rbMated.AutoSize = true;
            this.rbMated.Location = new System.Drawing.Point(103, 10);
            this.rbMated.Name = "rbMated";
            this.rbMated.Size = new System.Drawing.Size(72, 17);
            this.rbMated.TabIndex = 14;
            this.rbMated.Text = "Ведомый";
            this.rbMated.UseVisualStyleBackColor = true;
            // 
            // rbOwn
            // 
            this.rbOwn.AutoSize = true;
            this.rbOwn.Checked = true;
            this.rbOwn.ForeColor = System.Drawing.Color.Red;
            this.rbOwn.Location = new System.Drawing.Point(10, 10);
            this.rbOwn.Name = "rbOwn";
            this.rbOwn.Size = new System.Drawing.Size(70, 17);
            this.rbOwn.TabIndex = 13;
            this.rbOwn.TabStop = true;
            this.rbOwn.Text = "Ведущий";
            this.rbOwn.UseVisualStyleBackColor = true;
            this.rbOwn.CheckedChanged += new System.EventHandler(this.rb1_CheckedChanged);
            // 
            // pButtons
            // 
            this.pButtons.Controls.Add(this.bChange);
            this.pButtons.Controls.Add(this.bAdd);
            this.pButtons.Controls.Add(this.bClear);
            this.pButtons.Controls.Add(this.bDelete);
            this.pButtons.Location = new System.Drawing.Point(2, 186);
            this.pButtons.Name = "pButtons";
            this.pButtons.Size = new System.Drawing.Size(188, 52);
            this.pButtons.TabIndex = 16;
            // 
            // bChange
            // 
            this.bChange.ForeColor = System.Drawing.Color.Green;
            this.bChange.Location = new System.Drawing.Point(5, 26);
            this.bChange.Name = "bChange";
            this.bChange.Size = new System.Drawing.Size(83, 23);
            this.bChange.TabIndex = 15;
            this.bChange.Text = "Изменить";
            this.bChange.UseVisualStyleBackColor = true;
            this.bChange.Click += new System.EventHandler(this.bChange_Click);
            // 
            // bAdd
            // 
            this.bAdd.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.bAdd.Location = new System.Drawing.Point(103, 26);
            this.bAdd.Name = "bAdd";
            this.bAdd.Size = new System.Drawing.Size(80, 23);
            this.bAdd.TabIndex = 14;
            this.bAdd.Text = "Добавить";
            this.bAdd.UseVisualStyleBackColor = true;
            this.bAdd.Click += new System.EventHandler(this.bAdd_Click);
            // 
            // bClear
            // 
            this.bClear.ForeColor = System.Drawing.Color.Red;
            this.bClear.Location = new System.Drawing.Point(5, 3);
            this.bClear.Name = "bClear";
            this.bClear.Size = new System.Drawing.Size(83, 23);
            this.bClear.TabIndex = 13;
            this.bClear.Text = "Очистить";
            this.bClear.UseVisualStyleBackColor = true;
            this.bClear.Click += new System.EventHandler(this.bClear_Click);
            // 
            // bDelete
            // 
            this.bDelete.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.bDelete.Location = new System.Drawing.Point(103, 3);
            this.bDelete.Name = "bDelete";
            this.bDelete.Size = new System.Drawing.Size(80, 23);
            this.bDelete.TabIndex = 10;
            this.bDelete.Text = "Удалить";
            this.bDelete.UseVisualStyleBackColor = true;
            this.bDelete.Click += new System.EventHandler(this.bDelete_Click);
            // 
            // grbSector
            // 
            this.grbSector.Controls.Add(this.nudAngleMax);
            this.grbSector.Controls.Add(this.lAngleMax);
            this.grbSector.Controls.Add(this.nudAngleMin);
            this.grbSector.Controls.Add(this.lAngleMin);
            this.grbSector.ForeColor = System.Drawing.Color.Blue;
            this.grbSector.Location = new System.Drawing.Point(4, 105);
            this.grbSector.Name = "grbSector";
            this.grbSector.Size = new System.Drawing.Size(186, 61);
            this.grbSector.TabIndex = 15;
            this.grbSector.TabStop = false;
            this.grbSector.Text = "Сектор";
            // 
            // nudAngleMax
            // 
            this.nudAngleMax.Location = new System.Drawing.Point(108, 36);
            this.nudAngleMax.Maximum = new decimal(new int[] {
            360,
            0,
            0,
            0});
            this.nudAngleMax.Name = "nudAngleMax";
            this.nudAngleMax.Size = new System.Drawing.Size(74, 20);
            this.nudAngleMax.TabIndex = 12;
            this.nudAngleMax.Value = new decimal(new int[] {
            360,
            0,
            0,
            0});
            // 
            // lAngleMax
            // 
            this.lAngleMax.AutoSize = true;
            this.lAngleMax.ForeColor = System.Drawing.Color.Black;
            this.lAngleMax.Location = new System.Drawing.Point(2, 41);
            this.lAngleMax.Name = "lAngleMax";
            this.lAngleMax.Size = new System.Drawing.Size(131, 13);
            this.lAngleMax.TabIndex = 11;
            this.lAngleMax.Text = "Угол макс.,°....................";
            // 
            // nudAngleMin
            // 
            this.nudAngleMin.Location = new System.Drawing.Point(107, 15);
            this.nudAngleMin.Maximum = new decimal(new int[] {
            360,
            0,
            0,
            0});
            this.nudAngleMin.Name = "nudAngleMin";
            this.nudAngleMin.Size = new System.Drawing.Size(75, 20);
            this.nudAngleMin.TabIndex = 10;
            // 
            // lAngleMin
            // 
            this.lAngleMin.AutoSize = true;
            this.lAngleMin.ForeColor = System.Drawing.Color.Black;
            this.lAngleMin.Location = new System.Drawing.Point(2, 20);
            this.lAngleMin.Name = "lAngleMin";
            this.lAngleMin.Size = new System.Drawing.Size(128, 13);
            this.lAngleMin.TabIndex = 0;
            this.lAngleMin.Text = "Угол мин.,°.....................";
            // 
            // grbRange
            // 
            this.grbRange.Controls.Add(this.nudFreqMax);
            this.grbRange.Controls.Add(this.lFreqMax);
            this.grbRange.Controls.Add(this.nudFreqMin);
            this.grbRange.Controls.Add(this.lFreqMin);
            this.grbRange.ForeColor = System.Drawing.Color.Blue;
            this.grbRange.Location = new System.Drawing.Point(2, 35);
            this.grbRange.Name = "grbRange";
            this.grbRange.Size = new System.Drawing.Size(188, 61);
            this.grbRange.TabIndex = 4;
            this.grbRange.TabStop = false;
            this.grbRange.Text = "Диапазон";
            // 
            // nudFreqMax
            // 
            this.nudFreqMax.DecimalPlaces = 1;
            this.nudFreqMax.Location = new System.Drawing.Point(108, 36);
            this.nudFreqMax.Maximum = new decimal(new int[] {
            3000000,
            0,
            0,
            0});
            this.nudFreqMax.Minimum = new decimal(new int[] {
            30000,
            0,
            0,
            0});
            this.nudFreqMax.Name = "nudFreqMax";
            this.nudFreqMax.Size = new System.Drawing.Size(76, 20);
            this.nudFreqMax.TabIndex = 12;
            this.nudFreqMax.ThousandsSeparator = true;
            this.nudFreqMax.Value = new decimal(new int[] {
            30000,
            0,
            0,
            0});
            // 
            // lFreqMax
            // 
            this.lFreqMax.AutoSize = true;
            this.lFreqMax.ForeColor = System.Drawing.Color.Black;
            this.lFreqMax.Location = new System.Drawing.Point(2, 41);
            this.lFreqMax.Name = "lFreqMax";
            this.lFreqMax.Size = new System.Drawing.Size(123, 13);
            this.lFreqMax.TabIndex = 11;
            this.lFreqMax.Text = "Частота макс., кГц......";
            // 
            // nudFreqMin
            // 
            this.nudFreqMin.DecimalPlaces = 1;
            this.nudFreqMin.Location = new System.Drawing.Point(107, 14);
            this.nudFreqMin.Maximum = new decimal(new int[] {
            3000000,
            0,
            0,
            0});
            this.nudFreqMin.Minimum = new decimal(new int[] {
            30000,
            0,
            0,
            0});
            this.nudFreqMin.Name = "nudFreqMin";
            this.nudFreqMin.Size = new System.Drawing.Size(76, 20);
            this.nudFreqMin.TabIndex = 10;
            this.nudFreqMin.ThousandsSeparator = true;
            this.nudFreqMin.Value = new decimal(new int[] {
            30000,
            0,
            0,
            0});
            // 
            // lFreqMin
            // 
            this.lFreqMin.AutoSize = true;
            this.lFreqMin.ForeColor = System.Drawing.Color.Black;
            this.lFreqMin.Location = new System.Drawing.Point(2, 20);
            this.lFreqMin.Name = "lFreqMin";
            this.lFreqMin.Size = new System.Drawing.Size(117, 13);
            this.lFreqMin.TabIndex = 0;
            this.lFreqMin.Text = "Частота мин., кГц......";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel1.Controls.Add(this.pTables, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.pParam, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(525, 248);
            this.tableLayoutPanel1.TabIndex = 18;
            // 
            // pTables
            // 
            this.pTables.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pTables.Controls.Add(this.tabControl);
            this.pTables.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pTables.Location = new System.Drawing.Point(3, 3);
            this.pTables.Name = "pTables";
            this.pTables.Size = new System.Drawing.Size(319, 242);
            this.pTables.TabIndex = 2;
            // 
            // tables_Range_RR_RP
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "tables_Range_RR_RP";
            this.Size = new System.Drawing.Size(525, 248);
            this.tabControl.ResumeLayout(false);
            this.tpRangeRR.ResumeLayout(false);
            this.tpRangeRP.ResumeLayout(false);
            this.pParam.ResumeLayout(false);
            this.grbRadioButtons.ResumeLayout(false);
            this.grbRadioButtons.PerformLayout();
            this.pButtons.ResumeLayout(false);
            this.grbSector.ResumeLayout(false);
            this.grbSector.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudAngleMax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudAngleMin)).EndInit();
            this.grbRange.ResumeLayout(false);
            this.grbRange.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudFreqMax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudFreqMin)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.pTables.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl;
        public System.Windows.Forms.TabPage tpRangeRR;
        public System.Windows.Forms.TabPage tpRangeRP;
        private System.Windows.Forms.Panel pParam;
        private System.Windows.Forms.GroupBox grbSector;
        private System.Windows.Forms.NumericUpDown nudAngleMax;
        private System.Windows.Forms.Label lAngleMax;
        private System.Windows.Forms.NumericUpDown nudAngleMin;
        private System.Windows.Forms.Label lAngleMin;
        private System.Windows.Forms.GroupBox grbRange;
        private System.Windows.Forms.NumericUpDown nudFreqMax;
        private System.Windows.Forms.Label lFreqMax;
        private System.Windows.Forms.NumericUpDown nudFreqMin;
        private System.Windows.Forms.Label lFreqMin;
        private System.Windows.Forms.Panel pButtons;
        public System.Windows.Forms.Button bChange;
        public System.Windows.Forms.Button bAdd;
        public System.Windows.Forms.Button bClear;
        public System.Windows.Forms.Button bDelete;
        private System.Windows.Forms.GroupBox grbRadioButtons;
        public System.Windows.Forms.RadioButton rbMated;
        public System.Windows.Forms.RadioButton rbOwn;
        public MyDataGridView.myDataGridView dgvRangeRR_1;
        public MyDataGridView.myDataGridView dgvRangeRP_1;
        public MyDataGridView.myDataGridView dgvRangeRR_2;
        public MyDataGridView.myDataGridView dgvRangeRP_2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel pTables;
    }
}
