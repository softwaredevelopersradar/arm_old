﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MyDataGridView;

using Protocols;
using VariableDynamic;
using VariableStatic;

namespace Table_Range_RR_RP
{
    public partial class tables_Range_RR_RP : UserControl
    {
        FunctionsDB functionsDB;
        FunctionsDGV functionsDGV;
        FuncDB_Range_RR_RP funcDB_Range_RR_RP;
        FuncDGV_Range_RR_RP funcDGV_Range_RR_RP;
        FuncVW_Range_RR_RP funcVW_Range_RR_RP;

        CurVal_RangeRR_RP curVal_RangeRR_RP;
      
        Struct_RangeRR struct_RangeRR;
        Struct_RangeRP struct_RangeRP;

        Functions functions;
        FunctionsTranslate functionsTranslate;

        InitParams initParams;

        VariableWork variableWork;
        VariableCommon variableCommon;

        public int tcIndex = 0;

        private bool f_rbOwn = true;
        private bool f_rbMated = false;

        public tables_Range_RR_RP()
        {
            InitializeComponent();

            functionsDB = new FunctionsDB();
            functionsDGV = new FunctionsDGV();
            funcDB_Range_RR_RP = new FuncDB_Range_RR_RP();
            funcDGV_Range_RR_RP = new FuncDGV_Range_RR_RP();
            funcVW_Range_RR_RP = new FuncVW_Range_RR_RP();

            curVal_RangeRR_RP = new CurVal_RangeRR_RP();

            struct_RangeRR = new Struct_RangeRR();
            struct_RangeRP = new Struct_RangeRP();

            functions = new Functions();
            functionsTranslate = new FunctionsTranslate();
            initParams = new InitParams();

            variableWork = new VariableWork();
            variableCommon = new VariableCommon();

            InitTablesRR_RP();

            SetViewWindowForRole();

            VariableCommon.OnChangeCommonLanguage += new VariableCommon.ChangeCommonEventHandler(VariableCommon_OnChangeCommonLanguage);
            VariableCommon.OnChangeCommonRole += new VariableCommon.ChangeCommonEventHandler(VariableCommon_OnChangeCommonRole); 

            // Событие возникает при изменении значений секторов и диапазонов РР (ведущая)
            VariableWork.OnChangeRangeSectorReconOwn += new VariableWork.ChangeRegimeEventHandler(VariableWork_OnChangeRangeSectorReconOwn);
            // Событие возникает при изменении значений секторов и диапазонов РР (ведомая)
            VariableWork.OnChangeRangeSectorReconLinked += new VariableWork.ChangeRegimeEventHandler(VariableWork_OnChangeRangeSectorReconLinked);
            // Событие возникает при изменении значений секторов и диапазонов РП (ведущая)
            VariableWork.OnChangeRangeSectorSupprOwn += new VariableWork.ChangeRegimeEventHandler(VariableWork_OnChangeRangeSectorSupprOwn);
            // Событие возникает при изменении значений секторов и диапазонов РП (ведомая)
            VariableWork.OnChangeRangeSectorSupprLinked += new VariableWork.ChangeRegimeEventHandler(VariableWork_OnChangeRangeSectorSupprLinked);
           
            // Событие возникает при изменении всех значений секторов и диапазонов РП  (для обновления всей таблицы в БД (ведомая))
            //VariableWork.OnChangeDBAllRangeRPLinked += new VariableWork.UpdateAllRangeSectorEventHandler(VariableWork_OnChangeDBAllRangeRPLinked);
            // Событие возникает при изменении всех значений секторов и диапазонов РП (для обновления всей таблицы в БД (ведущая))
            //VariableWork.OnChangeDBAllRangeRPOwn += new VariableWork.UpdateAllRangeSectorEventHandler(VariableWork_OnChangeDBAllRangeRPOwn);
            // Событие возникает при изменении всех значений секторов и диапазонов РП  (для обновления всей таблицы в БД (ведомая))
            //VariableWork.OnChangeDBAllRangeRRLinked += new VariableWork.UpdateAllRangeSectorEventHandler(VariableWork_OnChangeDBAllRangeRRLinked);
            // Событие возникает при изменении всех значений секторов и диапазонов РП (для обновления всей таблицы в БД (ведущая))
            //VariableWork.OnChangeDBAllRangeRROwn += new VariableWork.UpdateAllRangeSectorEventHandler(VariableWork_OnChangeDBAllRangeRROwn); 
       
            // Событие возникает при выделении строки в dgv
            dgvRangeRR_1.dgv.SelectionChanged += new EventHandler(dgv_SelectionChanged_RR1);
            dgvRangeRR_2.dgv.SelectionChanged += new EventHandler(dgv_SelectionChanged_RR2);
            dgvRangeRP_1.dgv.SelectionChanged += new EventHandler(dgv_SelectionChanged_RP1);
            dgvRangeRP_2.dgv.SelectionChanged += new EventHandler(dgv_SelectionChanged_RP2);
        }

        private void VariableCommon_OnChangeCommonRole()
        {
            SetViewWindowForRole();
        }

        /// <summary>
        /// Установить вид окна в зависимости от роли (автономная, ведущая, ведомая)
        /// </summary>
        public void SetViewWindowForRole()
        {
            tcIndex = tabControl.TabIndex;
             try
                {
                    if (variableCommon.Role == 2) 
                    {
                        //tableLayoutPanel1.ColumnStyles[1] = new ColumnStyle(SizeType.Absolute, 0);

                        grbRadioButtons.Visible = false;

                        if (tcIndex == 0)
                        {
                            grbRange.Enabled = false;
                            grbSector.Enabled = true;
                            bAdd.Enabled = false;
                            bDelete.Enabled = false;
                            bClear.Enabled = false;
                            bChange.Enabled = true;

                        }
                        else if (tcIndex == 1)
                        {
                            grbRange.Enabled = true;
                            grbSector.Enabled = true;
                            //pButtons.Enabled = true;
                            bAdd.Enabled = true;
                            bDelete.Enabled = true;
                            bClear.Enabled = true;
                            bChange.Enabled = true;
                        }

                        dgvRangeRR_1.Visible = true;
                        dgvRangeRR_2.Visible = false;
                        dgvRangeRP_1.Visible = true;
                        dgvRangeRP_2.Visible = false;
                    }
                    else if (variableCommon.Role == 0 || variableCommon.Role == 1) 
                    {
                        //tableLayoutPanel1.ColumnStyles[1] = new ColumnStyle(SizeType.Absolute, 200);

                        grbRadioButtons.Visible = true;

                        if (tcIndex == 0)
                        {
                            grbRange.Enabled = true;
                            grbSector.Enabled = true;
                            //pButtons.Enabled = true;
                            bAdd.Enabled = true;
                            bDelete.Enabled = true;
                            bClear.Enabled = true;
                            bChange.Enabled = true;
                        }
                        else if (tcIndex == 1)
                        {
                            grbRange.Enabled = true;
                            grbSector.Enabled = true;
                            //pButtons.Enabled = true;
                            bAdd.Enabled = true;
                            bDelete.Enabled = true;
                            bClear.Enabled = true;
                            bChange.Enabled = true;
                        }

                        VisibleTable();

                        //dgvRangeRR_1.Visible = true;
                        //dgvRangeRR_2.Visible = true;
                        //dgvRangeRP_1.Visible = true;
                        //dgvRangeRP_2.Visible = true;
                    }
                }
                catch { }

        }

        private void VariableCommon_OnChangeCommonLanguage()
        {
            VariableCommon variableCommon = new VariableCommon();
            switch (variableCommon.Language)
            {
                case 0:
                    functionsTranslate.SetLanguage("rus");
                    ChangeLanguage();
                    break;

                case 1:
                    functionsTranslate.SetLanguage("eng");
                    ChangeLanguage();
                    break;

                case 2:
                    functionsTranslate.SetLanguage("az");
                    ChangeLanguage();
                    break;

                default:
                    functionsTranslate.SetLanguage("rus");
                    ChangeLanguage();
                    break;
            }
        }

        /// <summary>
        /// Для инициализации языка при запуске приложения 
        /// </summary>
        /// <param name="bLanguage"></param>
        public void ChangeControlLanguage(byte bLanguage)
        {

            switch (bLanguage)
            {
                case 0:
                    functionsTranslate.SetLanguage("rus");
                    ChangeLanguage();
                    break;

                case 1:
                    functionsTranslate.SetLanguage("eng");
                    ChangeLanguage();
                    break;

                case 2:
                    functionsTranslate.SetLanguage("az");
                    ChangeLanguage();
                    break;

                default:
                    functionsTranslate.SetLanguage("rus");
                    ChangeLanguage();
                    break;
            }
        }

        /// <summary>
        /// Изменение языка интерфейса
        /// </summary>
        private void ChangeLanguage()
        {
            functionsTranslate.RenameFieldsTables(functionsTranslate.Dictionary, dgvRangeRP_1.dgv);
            functionsTranslate.RenameFieldsTables(functionsTranslate.Dictionary, dgvRangeRP_2.dgv);
            functionsTranslate.RenameFieldsTables(functionsTranslate.Dictionary, dgvRangeRR_1.dgv);
            functionsTranslate.RenameFieldsTables(functionsTranslate.Dictionary, dgvRangeRR_2.dgv);
            functionsTranslate.RenameRadioButtons(functionsTranslate.Dictionary, grbRadioButtons);
            functionsTranslate.RenameButtons(functionsTranslate.Dictionary, pButtons);
            functionsTranslate.RenameLabels(functionsTranslate.Dictionary, grbRange);
            functionsTranslate.RenameLabels(functionsTranslate.Dictionary, grbSector);
            functionsTranslate.RenameGroupBoxes(functionsTranslate.Dictionary, pParam);
            functionsTranslate.RenameTabPages(functionsTranslate.Dictionary, tabControl);
        }

        /// <summary>
        /// Замена всей таблицы RangeRR в БД (ведущая)
        /// </summary>
        void VariableWork_OnChangeDBAllRangeRROwn()
        {
            if (variableWork.RangeSectorReconOwn != null)
            {
                int len = variableWork.RangeSectorReconOwn.Length;

                struct_RangeRR = new Struct_RangeRR();

                functionsDB.DeleteAllRecordsDB(NameTable.RANGE_RR, Table.Own);

                for (int i = 0; i < len; i++)
                {
                    struct_RangeRR.iID = i + 1;
                    struct_RangeRR.iFreqMin = variableWork.RangeSectorReconOwn[i].StartFrequency;
                    struct_RangeRR.iFreqMax = variableWork.RangeSectorReconOwn[i].EndFrequency;
                    struct_RangeRR.iAngleMin = variableWork.RangeSectorReconOwn[i].StartDirection / 10;
                    struct_RangeRR.iAngleMax = variableWork.RangeSectorReconOwn[i].EndDirection / 10;
                    struct_RangeRR.bMode = 1;

                    funcDB_Range_RR_RP.AddRecord_Range_RR_RPToDB(NameTable.RANGE_RR, struct_RangeRR);
                }
            }
        }

        /// <summary>
        /// Замена всей таблицы RangeRR в БД (ведомая)
        /// </summary>
        void VariableWork_OnChangeDBAllRangeRRLinked()
        {
            if (variableWork.RangeSectorReconLinked != null)
            {
                int len = variableWork.RangeSectorReconLinked.Length;

                struct_RangeRR = new Struct_RangeRR();

                functionsDB.DeleteAllRecordsDB(NameTable.RANGE_RR, Table.Linked);

                for (int i = 0; i < len; i++)
                {
                    struct_RangeRR.iID = i + 1;
                    struct_RangeRR.iFreqMin = variableWork.RangeSectorReconLinked[i].StartFrequency;
                    struct_RangeRR.iFreqMax = variableWork.RangeSectorReconLinked[i].EndFrequency;
                    struct_RangeRR.iAngleMin = variableWork.RangeSectorReconLinked[i].StartDirection / 10;
                    struct_RangeRR.iAngleMax = variableWork.RangeSectorReconLinked[i].EndDirection / 10;
                    struct_RangeRR.bMode = 2;

                    funcDB_Range_RR_RP.AddRecord_Range_RR_RPToDB(NameTable.RANGE_RR, struct_RangeRR);
                }
            }
        }

        /// <summary>
        /// Замена всей таблицы RangeRP в БД (ведущая)
        /// </summary>
        void VariableWork_OnChangeDBAllRangeRPOwn()
        {
            if (variableWork.RangeSectorSupprOwn != null)
            {
                int len = variableWork.RangeSectorSupprOwn.Length;

                struct_RangeRP = new Struct_RangeRP();

                functionsDB.DeleteAllRecordsDB(NameTable.RANGE_RP, Table.Own);

                for (int i = 0; i < len; i++)
                {
                    struct_RangeRP.iID = i + 1;
                    struct_RangeRP.iFreqMin = variableWork.RangeSectorSupprOwn[i].StartFrequency;
                    struct_RangeRP.iFreqMax = variableWork.RangeSectorSupprOwn[i].EndFrequency;
                    struct_RangeRP.iAngleMin = variableWork.RangeSectorSupprOwn[i].StartDirection / 10;
                    struct_RangeRP.iAngleMax = variableWork.RangeSectorSupprOwn[i].EndDirection / 10;
                    struct_RangeRP.bMode = 1;

                    funcDB_Range_RR_RP.AddRecord_Range_RR_RPToDB(NameTable.RANGE_RP, struct_RangeRP);
                }
            }
        }

        /// <summary>
        /// Замена всей таблицы RangeRP в БД (ведомая)
        /// </summary>
        void VariableWork_OnChangeDBAllRangeRPLinked()
        {
            if (variableWork.RangeSectorSupprLinked != null)
            {
                int len = variableWork.RangeSectorSupprLinked.Length;

                struct_RangeRP = new Struct_RangeRP();

                functionsDB.DeleteAllRecordsDB(NameTable.RANGE_RP, Table.Linked);

                for (int i = 0; i < len; i++)
                {
                    struct_RangeRP.iID = i + 1;
                    struct_RangeRP.iFreqMin = variableWork.RangeSectorSupprLinked[i].StartFrequency;
                    struct_RangeRP.iFreqMax = variableWork.RangeSectorSupprLinked[i].EndFrequency;
                    struct_RangeRP.iAngleMin = variableWork.RangeSectorSupprLinked[i].StartDirection / 10;
                    struct_RangeRP.iAngleMax = variableWork.RangeSectorSupprLinked[i].EndDirection / 10;
                    struct_RangeRP.bMode = 2;

                    funcDB_Range_RR_RP.AddRecord_Range_RR_RPToDB(NameTable.RANGE_RP, struct_RangeRP);
                }
            }
        }

        public void InitTablesRR_RP()
        {
            dgvRangeRR_1.InitTableDB(dgvRangeRR_1.dgv, NameTable.RANGE_RR);
            dgvRangeRR_2.InitTableDB(dgvRangeRR_2.dgv, NameTable.RANGE_RR);

            dgvRangeRP_1.InitTableDB(dgvRangeRP_1.dgv, NameTable.RANGE_RP);
            dgvRangeRP_2.InitTableDB(dgvRangeRP_2.dgv, NameTable.RANGE_RP);
        }

        private void dgv_SelectionChanged_RP2(object sender, EventArgs e)
        {
            SetCurParamToControls(dgvRangeRP_2.dgv);
        }

        private void dgv_SelectionChanged_RP1(object sender, EventArgs e)
        {
            SetCurParamToControls(dgvRangeRP_1.dgv);
        }

        private void dgv_SelectionChanged_RR2(object sender, EventArgs e)
        {
            SetCurParamToControls(dgvRangeRR_2.dgv);
        }

        private void dgv_SelectionChanged_RR1(object sender, EventArgs e)
        {
            SetCurParamToControls(dgvRangeRR_1.dgv);
        }

        async void VariableWork_OnChangeRangeSectorSupprLinked()
        {
            if (variableWork.RangeSectorSupprLinked != null)
            {
                int len = variableWork.RangeSectorSupprLinked.Length;

               
                if (functionsDB.DeleteAllRecordsDB(NameTable.RANGE_RP, Table.Linked))
                {
                    if (dgvRangeRP_2.dgv.InvokeRequired)
                    {
                        Invoke((MethodInvoker)(() =>
                        {
                            functionsDGV.DeleteAllRecordsDGV(dgvRangeRP_2.dgv);
                            dgvRangeRP_2.SetInitialNumberOfRows(dgvRangeRP_2.dgv, dgvRangeRP_2.tableRangeRP, NameTable.RANGE_RP);
                        }));
                    }
                    else
                    {
                        functionsDGV.DeleteAllRecordsDGV(dgvRangeRP_2.dgv);
                        dgvRangeRP_2.SetInitialNumberOfRows(dgvRangeRP_2.dgv, dgvRangeRP_2.tableRangeRP, NameTable.RANGE_RP);
                    }

                    for (int i = 0; i < len; i++)
                    {
                        struct_RangeRP = new Struct_RangeRP();

                        struct_RangeRP.iID = i + 1;
                        struct_RangeRP.iFreqMin = variableWork.RangeSectorSupprLinked[i].StartFrequency;
                        struct_RangeRP.iFreqMax = variableWork.RangeSectorSupprLinked[i].EndFrequency;
                        struct_RangeRP.iAngleMin = variableWork.RangeSectorSupprLinked[i].StartDirection;
                        struct_RangeRP.iAngleMax = variableWork.RangeSectorSupprLinked[i].EndDirection;
                        struct_RangeRP.bMode = 2;

                         if (funcDB_Range_RR_RP.AddRecord_Range_RR_RPToDB(NameTable.RANGE_RP, struct_RangeRP))
                        {
                            if (dgvRangeRP_2.dgv.InvokeRequired)
                            {
                                Invoke((MethodInvoker)(() => funcDGV_Range_RR_RP.AddRecord_Range_RR_RPToDGV(dgvRangeRP_2.dgv, NameTable.RANGE_RP, struct_RangeRP)));
                            }
                            else
                            {
                                funcDGV_Range_RR_RP.AddRecord_Range_RR_RPToDGV(dgvRangeRP_2.dgv, NameTable.RANGE_RP, struct_RangeRP);
                            }
                        }
                    }

                    VisibleTable();

                    var answer = await VariableDynamic.VariableWork.aWPtoBearingDSPprotocolNew.SetSectorsAndRanges(1, 1, variableWork.RangeSectorSupprLinked);
                }
            }
        }

        async void VariableWork_OnChangeRangeSectorSupprOwn()
        {
            if (variableWork.RangeSectorSupprOwn != null)
            {
                int len = variableWork.RangeSectorSupprOwn.Length;

                if (functionsDB.DeleteAllRecordsDB(NameTable.RANGE_RP, Table.Own))
                {
                    if (dgvRangeRP_1.dgv.InvokeRequired)
                    {
                        Invoke((MethodInvoker)(() =>
                        {
                            functionsDGV.DeleteAllRecordsDGV(dgvRangeRP_1.dgv);
                            dgvRangeRP_1.SetInitialNumberOfRows(dgvRangeRP_1.dgv, dgvRangeRP_1.tableRangeRP, NameTable.RANGE_RP);
                        }));
                    }
                    else
                    {
                        functionsDGV.DeleteAllRecordsDGV(dgvRangeRP_1.dgv);
                        dgvRangeRP_1.SetInitialNumberOfRows(dgvRangeRP_1.dgv, dgvRangeRP_1.tableRangeRP, NameTable.RANGE_RP);
                    }

                    for (int i = 0; i < len; i++)
                    {
                        struct_RangeRP = new Struct_RangeRP();

                        struct_RangeRP.iID = i + 1;
                        struct_RangeRP.iFreqMin = variableWork.RangeSectorSupprOwn[i].StartFrequency;
                        struct_RangeRP.iFreqMax = variableWork.RangeSectorSupprOwn[i].EndFrequency;
                        struct_RangeRP.iAngleMin = variableWork.RangeSectorSupprOwn[i].StartDirection;
                        struct_RangeRP.iAngleMax = variableWork.RangeSectorSupprOwn[i].EndDirection;
                        struct_RangeRP.bMode = 1;

                        if (funcDB_Range_RR_RP.AddRecord_Range_RR_RPToDB(NameTable.RANGE_RP, struct_RangeRP))
                        {
                            if (dgvRangeRP_1.dgv.InvokeRequired)
                            {
                                Invoke((MethodInvoker)(() => funcDGV_Range_RR_RP.AddRecord_Range_RR_RPToDGV(dgvRangeRP_1.dgv, NameTable.RANGE_RP, struct_RangeRP)));
                            }
                            else
                            {
                                funcDGV_Range_RR_RP.AddRecord_Range_RR_RPToDGV(dgvRangeRP_1.dgv, NameTable.RANGE_RP, struct_RangeRP);
                            }
                        }
                    }

                    VisibleTable();

                    var answer = await VariableDynamic.VariableWork.aWPtoBearingDSPprotocolNew.SetSectorsAndRanges(1, 0, variableWork.RangeSectorSupprOwn);
                }
            }
        }

        async void VariableWork_OnChangeRangeSectorReconLinked()
        {
            if (variableWork.RangeSectorReconLinked != null)
            {
                int len = variableWork.RangeSectorReconLinked.Length;


                if (functionsDB.DeleteAllRecordsDB(NameTable.RANGE_RR, Table.Linked))
                {
                    if (dgvRangeRR_2.dgv.InvokeRequired)
                    {
                        Invoke((MethodInvoker)(() =>
                        {
                            functionsDGV.DeleteAllRecordsDGV(dgvRangeRR_2.dgv);
                            dgvRangeRR_2.SetInitialNumberOfRows(dgvRangeRR_2.dgv, dgvRangeRR_2.tableRangeRR, NameTable.RANGE_RR);
                        }));
                    }
                    else
                    {
                        functionsDGV.DeleteAllRecordsDGV(dgvRangeRR_2.dgv);
                        dgvRangeRR_2.SetInitialNumberOfRows(dgvRangeRR_2.dgv, dgvRangeRR_2.tableRangeRR, NameTable.RANGE_RR);
                    }

                    for (int i = 0; i < len; i++)
                    {
                        struct_RangeRR = new Struct_RangeRR();

                        struct_RangeRR.iID = i + 1;
                        struct_RangeRR.iFreqMin = variableWork.RangeSectorReconLinked[i].StartFrequency;
                        struct_RangeRR.iFreqMax = variableWork.RangeSectorReconLinked[i].EndFrequency;
                        struct_RangeRR.iAngleMin = variableWork.RangeSectorReconLinked[i].StartDirection;
                        struct_RangeRR.iAngleMax = variableWork.RangeSectorReconLinked[i].EndDirection;
                        struct_RangeRR.bMode = 2;

                        if (funcDB_Range_RR_RP.AddRecord_Range_RR_RPToDB(NameTable.RANGE_RR, struct_RangeRR))
                        {
                            if (dgvRangeRR_2.dgv.InvokeRequired)
                            {
                                Invoke((MethodInvoker)(() => funcDGV_Range_RR_RP.AddRecord_Range_RR_RPToDGV(dgvRangeRR_2.dgv, NameTable.RANGE_RR, struct_RangeRR)));
                            }
                            else
                            {
                                funcDGV_Range_RR_RP.AddRecord_Range_RR_RPToDGV(dgvRangeRR_2.dgv, NameTable.RANGE_RR, struct_RangeRR);
                            }
                        }
                    }

                    VisibleTable();

                    var answer = await VariableDynamic.VariableWork.aWPtoBearingDSPprotocolNew.SetSectorsAndRanges(0, 1, variableWork.RangeSectorReconLinked);

                }
            }
        }

        async void VariableWork_OnChangeRangeSectorReconOwn()
        {
            if (variableWork.RangeSectorReconOwn != null)
            {
                int len = variableWork.RangeSectorReconOwn.Length;


                if (functionsDB.DeleteAllRecordsDB(NameTable.RANGE_RR, Table.Own))
                {
                    if (dgvRangeRR_1.dgv.InvokeRequired)
                    {
                        Invoke((MethodInvoker)(() =>
                        {
                            functionsDGV.DeleteAllRecordsDGV(dgvRangeRR_1.dgv);
                            dgvRangeRR_1.SetInitialNumberOfRows(dgvRangeRR_1.dgv, dgvRangeRR_1.tableRangeRR, NameTable.RANGE_RR);
                        }));
                    }
                    else
                    {
                        functionsDGV.DeleteAllRecordsDGV(dgvRangeRR_1.dgv);
                        dgvRangeRR_1.SetInitialNumberOfRows(dgvRangeRR_1.dgv, dgvRangeRR_1.tableRangeRR, NameTable.RANGE_RR);
                    }

                    for (int i = 0; i < len; i++)
                    {
                        struct_RangeRR = new Struct_RangeRR();

                        struct_RangeRR.iID = i + 1;
                        struct_RangeRR.iFreqMin = variableWork.RangeSectorReconOwn[i].StartFrequency;
                        struct_RangeRR.iFreqMax = variableWork.RangeSectorReconOwn[i].EndFrequency;
                        struct_RangeRR.iAngleMin = variableWork.RangeSectorReconOwn[i].StartDirection;
                        struct_RangeRR.iAngleMax = variableWork.RangeSectorReconOwn[i].EndDirection;
                        struct_RangeRR.bMode = 1;

                        if (funcDB_Range_RR_RP.AddRecord_Range_RR_RPToDB(NameTable.RANGE_RR, struct_RangeRR))
                        {
                            if (dgvRangeRR_1.dgv.InvokeRequired)
                            {
                                Invoke((MethodInvoker)(() => funcDGV_Range_RR_RP.AddRecord_Range_RR_RPToDGV(dgvRangeRR_1.dgv, NameTable.RANGE_RR, struct_RangeRR)));
                            }
                            else
                            {
                                funcDGV_Range_RR_RP.AddRecord_Range_RR_RPToDGV(dgvRangeRR_1.dgv, NameTable.RANGE_RR, struct_RangeRR);
                            }
                        }
                    }

                    VisibleTable();

                    var answer = await VariableDynamic.VariableWork.aWPtoBearingDSPprotocolNew.SetSectorsAndRanges(0, 0, variableWork.RangeSectorReconOwn);
                }
            }
        }

        public void LoadTablesRangeRR_RPfromDB()
        {
            funcDB_Range_RR_RP.LoadTablesRangeRR_RPFromDB(NameTable.RANGE_RR, Table.Own);
            funcDB_Range_RR_RP.LoadTablesRangeRR_RPFromDB(NameTable.RANGE_RR, Table.Linked);

            funcDB_Range_RR_RP.LoadTablesRangeRR_RPFromDB(NameTable.RANGE_RP, Table.Own);
            funcDB_Range_RR_RP.LoadTablesRangeRR_RPFromDB(NameTable.RANGE_RP, Table.Linked);

            VisibleTable();
        }

        /// <summary>
        /// индекс активной TabControl
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tabControl_Selected(object sender, TabControlEventArgs e)
        {
            tcIndex = tabControl.SelectedIndex;

            ////////////////////////////OTL
            VisibleTable();
            ////////////////////////////OTL

            if (variableCommon.Role == 2)
            {
                grbRadioButtons.Visible = false;

                if (tcIndex == 0)
                {
                    grbRange.Enabled = false;
                    grbSector.Enabled = true;
                    bAdd.Enabled = false;
                    bDelete.Enabled = false;
                    bClear.Enabled = false;
                    bChange.Enabled = true;
                }
                else if (tcIndex == 1)
                {
                    grbRange.Enabled = true;
                    grbSector.Enabled = true;
                    bAdd.Enabled = true;
                    bDelete.Enabled = true;
                    bClear.Enabled = true;
                    bChange.Enabled = true;
                }

                dgvRangeRR_1.Visible = true;
                dgvRangeRR_2.Visible = false;
                dgvRangeRP_1.Visible = true;
                dgvRangeRP_2.Visible = false;
            }
           
        }

        /// <summary>
        /// В зависимости от переключения radioButton и tabPage
        /// отображается таблица dgv
        /// </summary>
        private void VisibleTable()
        {
            if (tcIndex == 0 && f_rbOwn)
            {
                dgvRangeRR_1.Visible = true;
                dgvRangeRR_2.Visible = false;

                initParams.Init3000_6000_RangeRR(variableCommon.TypeStation, nudFreqMin, nudFreqMax);

                SetCurParamToControls(dgvRangeRR_1.dgv);
            }
            else if (tcIndex == 1 && f_rbOwn)
            {
                dgvRangeRP_1.Visible = true;
                dgvRangeRP_2.Visible = false;

                initParams.Init3000_6000_RangeRP(variableCommon.TypeStation, nudFreqMin, nudFreqMax);

                SetCurParamToControls(dgvRangeRP_1.dgv);
            }
            else if (tcIndex == 0 && f_rbMated)
            {
                dgvRangeRR_1.Visible = false;
                dgvRangeRR_2.Visible = true;

                initParams.Init3000_6000_RangeRR(variableCommon.TypeStation, nudFreqMin, nudFreqMax);

                SetCurParamToControls(dgvRangeRR_2.dgv);
            }
            else if (tcIndex == 1 && f_rbMated)
            {
                dgvRangeRP_1.Visible = false;
                dgvRangeRP_2.Visible = true;

                initParams.Init3000_6000_RangeRP(variableCommon.TypeStation, nudFreqMin, nudFreqMax);

                SetCurParamToControls(dgvRangeRP_2.dgv);
            }
        }

        /// <summary>
        /// Определение выделенной строки текущей таблицы
        /// </summary>
        /// <param name="dgv"></param>
        private void SetCurParamToControls(DataGridView dgv)
        {
            //if (dgv.Visible)
            //{
                if (dgv.SelectedRows.Count > 0)
                {
                    if (dgv.Rows[dgv.SelectedRows[0].Index].Cells[0].Value != null)
                    {
                        try
                        {
                            SetParams(dgv);
                        }
                        catch (System.Exception e)
                        {
                            MessageBox.Show(e.Message);
                            return;
                        }
                    }
                }
            //}
        }

        /// <summary>
        /// Установить рараметры текущей строки в элементы редактирования
        /// </summary>
        /// <param name="dgv"></param>
        private void SetParams(DataGridView dgv)
        {
            decimal dFreqMin = 0, dFreqMax;
            string sFreqMin, sFreqMax, sSubRes;

            if (dgv.RowCount != 0)
            {
                curVal_RangeRR_RP = new CurVal_RangeRR_RP();

                // ID строки
                curVal_RangeRR_RP.IdDGV = Convert.ToInt32(dgv.Rows[dgv.SelectedRows[0].Index].Cells[0].Value);
                
                // значение частоты min
                dFreqMin = Convert.ToDecimal(dgv.Rows[dgv.SelectedRows[0].Index].Cells[1].Value, Functions.format);
                sSubRes = functions.FreqToStr((int)(dFreqMin * 10));
                sFreqMin = functions.StrToFreq(sSubRes);

                nudFreqMin.Value = Convert.ToDecimal(sFreqMin, Functions.format);
                curVal_RangeRR_RP.FreqMinDGV = Convert.ToInt32(Convert.ToDecimal(dFreqMin, Functions.format) * 10);

                // значение частоты max
                dFreqMax = Convert.ToDecimal(dgv.Rows[dgv.SelectedRows[0].Index].Cells[2].Value, Functions.format);
                sSubRes = functions.FreqToStr((int)(dFreqMax * 10));
                sFreqMax = functions.StrToFreq(sSubRes);

                nudFreqMax.Value = Convert.ToDecimal(sFreqMax, Functions.format);
                curVal_RangeRR_RP.FreqMaxDGV = Convert.ToInt32(Convert.ToDecimal(dFreqMax, Functions.format) * 10);

                // значение угла min
                curVal_RangeRR_RP.AngleMinDGV = Convert.ToInt32(dgv.Rows[dgv.SelectedRows[0].Index].Cells[3].Value);
                nudAngleMin.Value = curVal_RangeRR_RP.AngleMinDGV;

                // значение угла max
                curVal_RangeRR_RP.AngleMaxDGV = Convert.ToInt32(dgv.Rows[dgv.SelectedRows[0].Index].Cells[4].Value);
                nudAngleMax.Value = curVal_RangeRR_RP.AngleMaxDGV;

                // режим (ведущая, ведомая)
                curVal_RangeRR_RP.TableDGV = Convert.ToByte(dgv.Rows[dgv.SelectedRows[0].Index].Cells[5].Value);
            }
        }

        private void rb1_CheckedChanged(object sender, EventArgs e)
        {
            if (rbOwn.Checked)
            {
                f_rbOwn = true;
                f_rbMated = false;
            }
            else
            {
                f_rbOwn = false;
                f_rbMated = true;
            }

            VisibleTable();
        }

        /// <summary>
        /// Добавить одну запись
        /// </summary>
        private void bAdd_Click(object sender, EventArgs e)
        {
            functions.fChangeRec = false;

            RangeSector rangeSector = new RangeSector();
 
            rangeSector.StartFrequency = Convert.ToInt32(nudFreqMin.Value * 10);
            rangeSector.EndFrequency = Convert.ToInt32(nudFreqMax.Value * 10);
            rangeSector.StartDirection = Convert.ToInt16(nudAngleMin.Value * 10);
            rangeSector.EndDirection = Convert.ToInt16(nudAngleMax.Value * 10);

            // таблица сектора и диапазоны РР (ведущая)
            if (dgvRangeRR_1.Visible)
            {
                if (f_rbOwn && tcIndex == 0)
                {
                    if (functions.CorrectFreqMinMax(rangeSector.StartFrequency, rangeSector.EndFrequency))
                    {
                        if (functions.JoinRangesRR_RP(dgvRangeRR_1.dgv, rangeSector.StartFrequency, rangeSector.EndFrequency, rangeSector.StartDirection, rangeSector.EndDirection/* struct_RangeRR.iAngleMax*/))
                        {
                            funcVW_Range_RR_RP.ChangeRange_RR_RPToVariableWork(rangeSector, variableWork.RangeSectorReconOwn.ToList(), Table.Own, -1, NameTable.RANGE_RR);
                        }

                        if (functions.JoinRangesRR_RP(dgvRangeRR_2.dgv, rangeSector.StartFrequency, rangeSector.EndFrequency, 0, 3600))
                        {
                            rangeSector.StartDirection = 0;
                            rangeSector.EndDirection = 3600;
                            funcVW_Range_RR_RP.ChangeRange_RR_RPToVariableWork(rangeSector, variableWork.RangeSectorReconLinked.ToList(), Table.Linked, -1, NameTable.RANGE_RR);
                        }
                    }
                }
            }

            // таблица сектора и диапазоны РР (ведомая)
            if (dgvRangeRR_2.Visible)
            {
                if (f_rbMated && tcIndex == 0)
                {
                    if (functions.CorrectFreqMinMax(rangeSector.StartFrequency, rangeSector.EndFrequency))
                    {
                        if (functions.JoinRangesRR_RP(dgvRangeRR_2.dgv, rangeSector.StartFrequency, rangeSector.EndFrequency, rangeSector.StartDirection, rangeSector.EndDirection/*.iAngleMax*/))
                        {
                            funcVW_Range_RR_RP.ChangeRange_RR_RPToVariableWork(rangeSector, variableWork.RangeSectorReconLinked.ToList(), Table.Linked, -1, NameTable.RANGE_RR);
                        }
                    }
                }
            }

            // таблица сектора и диапазоны РП (ведущая)
            if (dgvRangeRP_1.Visible)
            {
                if (f_rbOwn && tcIndex == 1)
                {
                    if (functions.CorrectFreqMinMax(rangeSector.StartFrequency, rangeSector.EndFrequency))
                    {
                        if (functions.JoinRangesRR_RP(dgvRangeRP_1.dgv, rangeSector.StartFrequency, rangeSector.EndFrequency, rangeSector.StartDirection, rangeSector.EndDirection/* struct_RangeRR.iAngleMax*/))
                        {
                            funcVW_Range_RR_RP.ChangeRange_RR_RPToVariableWork(rangeSector, variableWork.RangeSectorSupprOwn.ToList(), Table.Own, -1, NameTable.RANGE_RP);
                        }
                    }
                }
            }

            // таблица сектора и диапазоны РП (ведомая)
            if (dgvRangeRP_2.Visible)
            {
                if (f_rbMated && tcIndex == 1)
                {
                    if (functions.CorrectFreqMinMax(rangeSector.StartFrequency, rangeSector.EndFrequency))
                    {
                        if (functions.JoinRangesRR_RP(dgvRangeRP_2.dgv, rangeSector.StartFrequency, rangeSector.EndFrequency, rangeSector.StartDirection, rangeSector.EndDirection/*.iAngleMax*/))
                        {
                            funcVW_Range_RR_RP.ChangeRange_RR_RPToVariableWork(rangeSector, variableWork.RangeSectorSupprLinked.ToList(), Table.Linked, -1, NameTable.RANGE_RP);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Удалить одну запись
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bDelete_Click(object sender, EventArgs e)
        {
            // таблица сектора и диапазоны РР (ведущая)
            if (dgvRangeRR_1.Visible)
            {
                if (f_rbOwn && tcIndex == 0)
                {
                    funcVW_Range_RR_RP.DeleteOneRange_RR_RPToVariableWork(dgvRangeRR_1.dgv, variableWork, Table.Own, NameTable.RANGE_RR);
                }
            }

            // таблица сектора и диапазоны РР (ведомая)
            if (dgvRangeRR_2.Visible)
            {
                if (f_rbMated && tcIndex == 0)
                {
                    funcVW_Range_RR_RP.DeleteOneRange_RR_RPToVariableWork(dgvRangeRR_2.dgv, variableWork, Table.Linked, NameTable.RANGE_RR);
                }
            }

            // таблица сектора и диапазоны РП (ведущая)
            if (dgvRangeRP_1.Visible)
            {
                if (f_rbOwn && tcIndex == 1)
                {
                    funcVW_Range_RR_RP.DeleteOneRange_RR_RPToVariableWork(dgvRangeRP_1.dgv, variableWork, Table.Own, NameTable.RANGE_RP);
                }
            }

            // таблица сектора и диапазоны РП (ведомая)
            if (dgvRangeRP_2.Visible)
            {
                if (f_rbMated && tcIndex == 1)
                {
                    funcVW_Range_RR_RP.DeleteOneRange_RR_RPToVariableWork(dgvRangeRP_2.dgv, variableWork, Table.Linked, NameTable.RANGE_RP);
                }
            }
        }

        private void bClear_Click(object sender, EventArgs e)
        {
            // таблица сектора и диапазоны РР (ведущая)
            if (dgvRangeRR_1.Visible)
            {
                if (f_rbOwn && tcIndex == 0)
                {
                    funcVW_Range_RR_RP.DeleteAllRange_RR_RPToVariableWork(variableWork, Table.Own, NameTable.RANGE_RR);
                }
            }

            // таблица сектора и диапазоны РР (ведомая)
            if (dgvRangeRR_2.Visible)
            {
                if (f_rbMated && tcIndex == 0)
                {
                    funcVW_Range_RR_RP.DeleteAllRange_RR_RPToVariableWork(variableWork, Table.Linked, NameTable.RANGE_RR);
                }
            }

            // таблица сектора и диапазоны РП (ведущая)
            if (dgvRangeRP_1.Visible)
            {
                if (f_rbOwn && tcIndex == 1)
                {
                    funcVW_Range_RR_RP.DeleteAllRange_RR_RPToVariableWork(variableWork, Table.Own, NameTable.RANGE_RP);
                }
            }

            // таблица сектора и диапазоны РП (ведомая)
            if (dgvRangeRP_2.Visible)
            {
                if (f_rbMated && tcIndex == 1)
                {
                    funcVW_Range_RR_RP.DeleteAllRange_RR_RPToVariableWork(variableWork, Table.Linked, NameTable.RANGE_RP);
                }
            }
        }

        private void bChange_Click(object sender, EventArgs e)
        {
            functions.fChangeRec = true;

            RangeSector rangeSector = new RangeSector();

            rangeSector.StartFrequency = Convert.ToInt32(nudFreqMin.Value * 10);
            rangeSector.EndFrequency = Convert.ToInt32(nudFreqMax.Value * 10);
            rangeSector.StartDirection = Convert.ToInt16(nudAngleMin.Value * 10);
            rangeSector.EndDirection = Convert.ToInt16(nudAngleMax.Value * 10);

            // таблица сектора и диапазоны РР (ведущая)
            if (dgvRangeRR_1.Visible)
            {
                if (f_rbOwn && tcIndex == 0)
                {
                    decimal dFMin = Convert.ToDecimal(dgvRangeRR_1.dgv.Rows[dgvRangeRR_1.dgv.SelectedRows[0].Index].Cells[1].Value, Functions.format);
                    int iFMin = (int)(dFMin * 10);
                    decimal dFMax = Convert.ToDecimal(dgvRangeRR_1.dgv.Rows[dgvRangeRR_1.dgv.SelectedRows[0].Index].Cells[2].Value, Functions.format);
                    int iFMax = (int)(dFMax * 10);
                    Int16 iAMin = (Int16)(Convert.ToInt16(dgvRangeRR_1.dgv.Rows[dgvRangeRR_1.dgv.SelectedRows[0].Index].Cells[3].Value) * 10);
                    Int16 iAMax = (Int16)(Convert.ToInt16(dgvRangeRR_1.dgv.Rows[dgvRangeRR_1.dgv.SelectedRows[0].Index].Cells[4].Value) * 10);

                    var lRangeSector = variableWork.RangeSectorReconOwn.ToList();

                    int ind = lRangeSector.FindIndex(x => x.StartFrequency == iFMin && x.EndFrequency == iFMax && x.StartDirection == iAMin && x.EndDirection == iAMax);
                            
                    if (functionsDGV.AmountRecordsDGV(dgvRangeRR_1.dgv) > 1)
                    {
                        if (functions.CorrectFreqMinMax(rangeSector.StartFrequency, rangeSector.EndFrequency))
                        {
                            if (functions.JoinRangesRR_RP(dgvRangeRR_1.dgv, rangeSector.StartFrequency, rangeSector.EndFrequency, rangeSector.StartDirection, rangeSector.EndDirection))
                            {
                                if (ind != -1) 
                                {
                                    lRangeSector.RemoveAt(ind);

                                    funcVW_Range_RR_RP.ChangeRange_RR_RPToVariableWork(rangeSector, lRangeSector, Table.Own, ind, NameTable.RANGE_RR);

                                    dgvRangeRR_1.dgv.Rows[ind].Selected = true;
                                }
                                else return;
                            }
                        }
                    }
                    else 
                    {
                        if (functions.CorrectFreqMinMax(rangeSector.StartFrequency, rangeSector.EndFrequency))
                        {
                            if (ind != -1)
                            {
                                lRangeSector.RemoveAt(ind);

                                funcVW_Range_RR_RP.ChangeRange_RR_RPToVariableWork(rangeSector, lRangeSector, Table.Own, ind, NameTable.RANGE_RR);

                                dgvRangeRR_1.dgv.Rows[ind].Selected = true;
                            }
                            else return;
                        }
                    }
                }
            }

            // таблица сектора и диапазоны РР (ведомая)
            if (dgvRangeRR_2.Visible)
            {
                if (f_rbMated && tcIndex == 0)
                {
                    decimal dFMin = Convert.ToDecimal(dgvRangeRR_2.dgv.Rows[dgvRangeRR_2.dgv.SelectedRows[0].Index].Cells[1].Value, Functions.format);
                    int iFMin = (int)(dFMin * 10);
                    decimal dFMax = Convert.ToDecimal(dgvRangeRR_2.dgv.Rows[dgvRangeRR_2.dgv.SelectedRows[0].Index].Cells[2].Value, Functions.format);
                    int iFMax = (int)(dFMax * 10);
                    Int16 iAMin = (Int16)(Convert.ToInt16(dgvRangeRR_2.dgv.Rows[dgvRangeRR_2.dgv.SelectedRows[0].Index].Cells[3].Value) * 10);
                    Int16 iAMax = (Int16)(Convert.ToInt16(dgvRangeRR_2.dgv.Rows[dgvRangeRR_2.dgv.SelectedRows[0].Index].Cells[4].Value) * 10);

                    var lRangeSector = variableWork.RangeSectorReconLinked.ToList();

                    int ind = lRangeSector.FindIndex(x => x.StartFrequency == iFMin && x.EndFrequency == iFMax && x.StartDirection == iAMin && x.EndDirection == iAMax);

                    if (functionsDGV.AmountRecordsDGV(dgvRangeRR_2.dgv) > 1)
                    {
                        if (functions.CorrectFreqMinMax(rangeSector.StartFrequency, rangeSector.EndFrequency))
                        {
                            if (functions.JoinRangesRR_RP(dgvRangeRR_2.dgv, rangeSector.StartFrequency, rangeSector.EndFrequency, rangeSector.StartDirection, rangeSector.EndDirection))
                            {
                                if (ind != -1)
                                {
                                    lRangeSector.RemoveAt(ind);

                                    funcVW_Range_RR_RP.ChangeRange_RR_RPToVariableWork(rangeSector, lRangeSector, Table.Linked, ind, NameTable.RANGE_RR);

                                    dgvRangeRR_2.dgv.Rows[ind].Selected = true;
                                }
                                else return;
                            }
                        }
                    }
                    else
                    {
                        if (functions.CorrectFreqMinMax(rangeSector.StartFrequency, rangeSector.EndFrequency))
                        {
                            if (ind != -1)
                            {
                                lRangeSector.RemoveAt(ind);

                                funcVW_Range_RR_RP.ChangeRange_RR_RPToVariableWork(rangeSector, lRangeSector, Table.Linked, ind, NameTable.RANGE_RR);

                                dgvRangeRR_2.dgv.Rows[ind].Selected = true;
                            }
                            else return;
                        }
                    
                    }
                }
            }

            // таблица сектора и диапазоны РП (ведущая)
            if (dgvRangeRP_1.Visible)
            {
                if (f_rbOwn && tcIndex == 1)
                {
                    decimal dFMin = Convert.ToDecimal(dgvRangeRP_1.dgv.Rows[dgvRangeRP_1.dgv.SelectedRows[0].Index].Cells[1].Value, Functions.format);
                    int iFMin = (int)(dFMin * 10);
                    decimal dFMax = Convert.ToDecimal(dgvRangeRP_1.dgv.Rows[dgvRangeRP_1.dgv.SelectedRows[0].Index].Cells[2].Value, Functions.format);
                    int iFMax = (int)(dFMax * 10);
                    Int16 iAMin = (Int16)(Convert.ToInt16(dgvRangeRP_1.dgv.Rows[dgvRangeRP_1.dgv.SelectedRows[0].Index].Cells[3].Value) * 10);
                    Int16 iAMAx = (Int16)(Convert.ToInt16(dgvRangeRP_1.dgv.Rows[dgvRangeRP_1.dgv.SelectedRows[0].Index].Cells[4].Value) * 10);

                    var lRangeSector = variableWork.RangeSectorSupprOwn.ToList();

                    int ind = lRangeSector.FindIndex(x => x.StartFrequency == iFMin && x.EndFrequency == iFMax && x.StartDirection == iAMin && x.EndDirection == iAMAx);

                    if (functionsDGV.AmountRecordsDGV(dgvRangeRP_1.dgv) > 1)
                    {
                        if (functions.CorrectFreqMinMax(rangeSector.StartFrequency, rangeSector.EndFrequency))
                        {
                            if (functions.JoinRangesRR_RP(dgvRangeRP_1.dgv, rangeSector.StartFrequency, rangeSector.EndFrequency, rangeSector.StartDirection, rangeSector.EndDirection))
                            {
                                if (ind != -1)
                                {
                                    lRangeSector.RemoveAt(ind);

                                    funcVW_Range_RR_RP.ChangeRange_RR_RPToVariableWork(rangeSector, lRangeSector, Table.Own, ind, NameTable.RANGE_RP);

                                    dgvRangeRP_1.dgv.Rows[ind].Selected = true;
                                }
                                else return;
                            }
                        }
                    }
                    else
                    {
                        if (functions.CorrectFreqMinMax(rangeSector.StartFrequency, rangeSector.EndFrequency))
                        {
                            if (ind != -1)
                            {
                                lRangeSector.RemoveAt(ind);

                                funcVW_Range_RR_RP.ChangeRange_RR_RPToVariableWork(rangeSector, lRangeSector, Table.Own, ind, NameTable.RANGE_RP);

                                dgvRangeRP_1.dgv.Rows[ind].Selected = true;
                            }
                            else return;
                        }
                    }
                }
            }

            // таблица сектора и диапазоны РП (ведомая)
            if (dgvRangeRP_2.Visible)
            {
                if (f_rbMated && tcIndex == 1)
                {
                    decimal dFMin = Convert.ToDecimal(dgvRangeRP_2.dgv.Rows[dgvRangeRP_2.dgv.SelectedRows[0].Index].Cells[1].Value, Functions.format);
                    int iFMin = (int)(dFMin * 10);
                    decimal dFMax = Convert.ToDecimal(dgvRangeRP_2.dgv.Rows[dgvRangeRP_2.dgv.SelectedRows[0].Index].Cells[2].Value, Functions.format);
                    int iFMax = (int)(dFMax * 10);
                    Int16 iAMin = (Int16)(Convert.ToInt16(dgvRangeRP_2.dgv.Rows[dgvRangeRP_2.dgv.SelectedRows[0].Index].Cells[3].Value) * 10);
                    Int16 iAMAx = (Int16)(Convert.ToInt16(dgvRangeRP_2.dgv.Rows[dgvRangeRP_2.dgv.SelectedRows[0].Index].Cells[4].Value) * 10);

                    var lRangeSector = variableWork.RangeSectorSupprLinked.ToList();

                    int ind = lRangeSector.FindIndex(x => x.StartFrequency == iFMin && x.EndFrequency == iFMax && x.StartDirection == iAMin && x.EndDirection == iAMAx);
                   
                    if (functionsDGV.AmountRecordsDGV(dgvRangeRP_2.dgv) > 1)
                    {
                        if (functions.CorrectFreqMinMax(rangeSector.StartFrequency, rangeSector.EndFrequency))
                        {
                            if (functions.JoinRangesRR_RP(dgvRangeRP_2.dgv, rangeSector.StartFrequency, rangeSector.EndFrequency, rangeSector.StartDirection, rangeSector.EndDirection))
                            {
                                if (ind != -1)
                                {
                                    lRangeSector.RemoveAt(ind);

                                    funcVW_Range_RR_RP.ChangeRange_RR_RPToVariableWork(rangeSector, lRangeSector, Table.Linked, ind, NameTable.RANGE_RP);

                                    dgvRangeRP_2.dgv.Rows[ind].Selected = true;
                                }
                                else return;
                            }
                        }
                    }
                    else
                    {
                        if (functions.CorrectFreqMinMax(rangeSector.StartFrequency, rangeSector.EndFrequency))
                        {
                            if (ind != -1)
                            {
                                lRangeSector.RemoveAt(ind);

                                funcVW_Range_RR_RP.ChangeRange_RR_RPToVariableWork(rangeSector, lRangeSector, Table.Linked, ind, NameTable.RANGE_RP);

                                dgvRangeRP_2.dgv.Rows[ind].Selected = true;
                            }
                            else return;
                        }
                    }
                }
            }
        }
    }
}
