﻿namespace PanoramaLibrary
{
    partial class PanoramaControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PanoramaControl));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton3 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton4 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton5 = new System.Windows.Forms.ToolStripButton();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.label15 = new System.Windows.Forms.Label();
            this.buttonTD = new System.Windows.Forms.Button();
            this.buttonAC = new System.Windows.Forms.Button();
            this.buttonRS = new System.Windows.Forms.Button();
            this.waveformGraph1 = new NationalInstruments.UI.WindowsForms.WaveformGraph();
            this.xCursor = new NationalInstruments.UI.XYCursor();
            this.waveformPlot1 = new NationalInstruments.UI.WaveformPlot();
            this.xAxes = new NationalInstruments.UI.XAxis();
            this.yAxes = new NationalInstruments.UI.YAxis();
            this.porogCursor = new NationalInstruments.UI.XYCursor();
            this.SpectrxyCursor = new NationalInstruments.UI.XYCursor();
            this.buttonExDF = new System.Windows.Forms.CheckBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.intensityGraph1 = new NationalInstruments.UI.WindowsForms.IntensityGraph();
            this.colorScale2 = new NationalInstruments.UI.ColorScale();
            this.intensityCursor1 = new NationalInstruments.UI.IntensityCursor();
            this.intensityPlot2 = new NationalInstruments.UI.IntensityPlot();
            this.intensityXAxis3 = new NationalInstruments.UI.IntensityXAxis();
            this.intensityYAxis2 = new NationalInstruments.UI.IntensityYAxis();
            this.intensityXAxis4 = new NationalInstruments.UI.IntensityXAxis();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.gbutton0 = new System.Windows.Forms.Button();
            this.gbutton7_2 = new System.Windows.Forms.Button();
            this.gbutton9 = new System.Windows.Forms.Button();
            this.glabel9 = new System.Windows.Forms.Label();
            this.gbutton8 = new System.Windows.Forms.Button();
            this.glabel8 = new System.Windows.Forms.Label();
            this.gbutton7_1 = new System.Windows.Forms.Button();
            this.glabel7 = new System.Windows.Forms.Label();
            this.gbutton6 = new System.Windows.Forms.Button();
            this.glabel6 = new System.Windows.Forms.Label();
            this.gbutton5 = new System.Windows.Forms.Button();
            this.glabel5 = new System.Windows.Forms.Label();
            this.gbutton4 = new System.Windows.Forms.Button();
            this.glabel4 = new System.Windows.Forms.Label();
            this.gbutton3 = new System.Windows.Forms.Button();
            this.glabel3 = new System.Windows.Forms.Label();
            this.gbutton2 = new System.Windows.Forms.Button();
            this.glabel2 = new System.Windows.Forms.Label();
            this.gbutton1 = new System.Windows.Forms.Button();
            this.glabel1 = new System.Windows.Forms.Label();
            this.scatterGraph1 = new NationalInstruments.UI.WindowsForms.ScatterGraph();
            this.xyPointAnnotation0 = new NationalInstruments.UI.XYPointAnnotation();
            this.xAxis2 = new NationalInstruments.UI.XAxis();
            this.yAxis2 = new NationalInstruments.UI.YAxis();
            this.xyPointAnnotation45 = new NationalInstruments.UI.XYPointAnnotation();
            this.xyPointAnnotation90 = new NationalInstruments.UI.XYPointAnnotation();
            this.xyPointAnnotation135 = new NationalInstruments.UI.XYPointAnnotation();
            this.xyPointAnnotation180 = new NationalInstruments.UI.XYPointAnnotation();
            this.xyPointAnnotation225 = new NationalInstruments.UI.XYPointAnnotation();
            this.xyPointAnnotation270 = new NationalInstruments.UI.XYPointAnnotation();
            this.xyPointAnnotation315 = new NationalInstruments.UI.XYPointAnnotation();
            this.roundxyCursor = new NationalInstruments.UI.XYCursor();
            this.rScatterPlot1 = new NationalInstruments.UI.ScatterPlot();
            this.rScatterPlot2 = new NationalInstruments.UI.ScatterPlot();
            this.rxyPlot = new NationalInstruments.UI.ScatterPlot();
            this.rScatterPlot3 = new NationalInstruments.UI.ScatterPlot();
            this.rScatterPlot4 = new NationalInstruments.UI.ScatterPlot();
            this.rScatterPlot5 = new NationalInstruments.UI.ScatterPlot();
            this.rScatterPlot6 = new NationalInstruments.UI.ScatterPlot();
            this.rScatterPlot7 = new NationalInstruments.UI.ScatterPlot();
            this.rScatterPlot8 = new NationalInstruments.UI.ScatterPlot();
            this.Plabel = new System.Windows.Forms.Label();
            this.SDlabel = new System.Windows.Forms.Label();
            this.Qlabel = new System.Windows.Forms.Label();
            this.Flabel = new System.Windows.Forms.Label();
            this.xyDataScatterGraph = new NationalInstruments.UI.WindowsForms.ScatterGraph();
            this.xyCursor1 = new NationalInstruments.UI.XYCursor();
            this.xyPlot = new NationalInstruments.UI.ScatterPlot();
            this.xAxis1 = new NationalInstruments.UI.XAxis();
            this.xyPlotYAxis = new NationalInstruments.UI.YAxis();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.EDFtimer = new System.Windows.Forms.Timer(this.components);
            this.intensityTimer = new System.Windows.Forms.Timer(this.components);
            this.scatterGraph2 = new NationalInstruments.UI.WindowsForms.ScatterGraph();
            this.xyrPointAnnotation0 = new NationalInstruments.UI.XYPointAnnotation();
            this.xAxis3 = new NationalInstruments.UI.XAxis();
            this.yAxis1 = new NationalInstruments.UI.YAxis();
            this.xyrPointAnnotation45 = new NationalInstruments.UI.XYPointAnnotation();
            this.xyrPointAnnotation90 = new NationalInstruments.UI.XYPointAnnotation();
            this.xyrPointAnnotation135 = new NationalInstruments.UI.XYPointAnnotation();
            this.xyrPointAnnotation180 = new NationalInstruments.UI.XYPointAnnotation();
            this.xyrPointAnnotation225 = new NationalInstruments.UI.XYPointAnnotation();
            this.xyrPointAnnotation270 = new NationalInstruments.UI.XYPointAnnotation();
            this.xyrPointAnnotation315 = new NationalInstruments.UI.XYPointAnnotation();
            this.r2ScatterPlot1 = new NationalInstruments.UI.ScatterPlot();
            this.r2ScatterPlot2 = new NationalInstruments.UI.ScatterPlot();
            this.r2ScatterPlot3 = new NationalInstruments.UI.ScatterPlot();
            this.r2ScatterPlot4 = new NationalInstruments.UI.ScatterPlot();
            this.CofAvPh = new System.Windows.Forms.Label();
            this.CofAvPl = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.labelLeft = new System.Windows.Forms.Label();
            this.labelRight = new System.Windows.Forms.Label();
            this.ScanSpeedTimer = new System.Windows.Forms.Timer(this.components);
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.FtextBox = new System.Windows.Forms.TextBox();
            this.QtextBox = new System.Windows.Forms.TextBox();
            this.SDtextBox = new System.Windows.Forms.TextBox();
            this.PtextBox = new System.Windows.Forms.TextBox();
            this.shapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.PllineShape = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.PhlineShape = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.PlineShape = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.SDlineShape = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.QlineShape = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.FlineShape = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.numericUpDownPh = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownPl = new System.Windows.Forms.NumericUpDown();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.button34 = new System.Windows.Forms.Button();
            this.label94 = new System.Windows.Forms.Label();
            this.label104 = new System.Windows.Forms.Label();
            this.label95 = new System.Windows.Forms.Label();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.waveformGraph1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xCursor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.porogCursor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SpectrxyCursor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intensityGraph1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intensityCursor1)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.scatterGraph1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.roundxyCursor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xyDataScatterGraph)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xyCursor1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.scatterGraph2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownPh)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownPl)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.AutoSize = false;
            this.toolStrip1.CanOverflow = false;
            this.toolStrip1.GripMargin = new System.Windows.Forms.Padding(0);
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton1,
            this.toolStripSeparator1,
            this.toolStripButton2,
            this.toolStripSeparator2,
            this.toolStripButton3,
            this.toolStripSeparator3,
            this.toolStripButton4,
            this.toolStripSeparator4,
            this.toolStripButton5});
            this.toolStrip1.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.Flow;
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Padding = new System.Windows.Forms.Padding(0);
            this.toolStrip1.Size = new System.Drawing.Size(500, 23);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.TabStop = true;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.CheckOnClick = true;
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(36, 19);
            this.toolStripButton1.Text = "ЧАП";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 23);
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.CheckOnClick = true;
            this.toolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton2.Image")));
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(77, 19);
            this.toolStripButton2.Text = "ЧАП+ЧПлП";
            this.toolStripButton2.Click += new System.EventHandler(this.toolStripButton2_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 23);
            // 
            // toolStripButton3
            // 
            this.toolStripButton3.CheckOnClick = true;
            this.toolStripButton3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton3.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton3.Image")));
            this.toolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton3.Name = "toolStripButton3";
            this.toolStripButton3.Size = new System.Drawing.Size(74, 19);
            this.toolStripButton3.Text = "ЧАП+лимб";
            this.toolStripButton3.Click += new System.EventHandler(this.toolStripButton3_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 23);
            // 
            // toolStripButton4
            // 
            this.toolStripButton4.CheckOnClick = true;
            this.toolStripButton4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton4.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton4.Image")));
            this.toolStripButton4.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton4.Name = "toolStripButton4";
            this.toolStripButton4.Size = new System.Drawing.Size(50, 19);
            this.toolStripButton4.Text = "ЧПлПк";
            this.toolStripButton4.Click += new System.EventHandler(this.toolStripButton4_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 23);
            // 
            // toolStripButton5
            // 
            this.toolStripButton5.CheckOnClick = true;
            this.toolStripButton5.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton5.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton5.Image")));
            this.toolStripButton5.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton5.Name = "toolStripButton5";
            this.toolStripButton5.Size = new System.Drawing.Size(68, 19);
            this.toolStripButton5.Text = "ЧАП+ЧВП";
            this.toolStripButton5.Click += new System.EventHandler(this.toolStripButton5_Click);
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.numericUpDown1.Location = new System.Drawing.Point(455, 26);
            this.numericUpDown1.Maximum = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.numericUpDown1.Minimum = new decimal(new int[] {
            120,
            0,
            0,
            -2147483648});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(44, 20);
            this.numericUpDown1.TabIndex = 5;
            this.numericUpDown1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericUpDown1.Value = new decimal(new int[] {
            80,
            0,
            0,
            -2147483648});
            this.numericUpDown1.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // label15
            // 
            this.label15.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(386, 28);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(66, 13);
            this.label15.TabIndex = 90;
            this.label15.Text = "Порог, дБ =";
            // 
            // buttonTD
            // 
            this.buttonTD.ForeColor = System.Drawing.Color.Green;
            this.buttonTD.Location = new System.Drawing.Point(240, 25);
            this.buttonTD.Margin = new System.Windows.Forms.Padding(0);
            this.buttonTD.Name = "buttonTD";
            this.buttonTD.Size = new System.Drawing.Size(75, 23);
            this.buttonTD.TabIndex = 2;
            this.buttonTD.Text = "ИРИ на ЦР";
            this.buttonTD.UseVisualStyleBackColor = true;
            this.buttonTD.Visible = false;
            this.buttonTD.Click += new System.EventHandler(this.buttonTD_Click);
            // 
            // buttonAC
            // 
            this.buttonAC.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.buttonAC.Location = new System.Drawing.Point(75, 25);
            this.buttonAC.Margin = new System.Windows.Forms.Padding(0);
            this.buttonAC.Name = "buttonAC";
            this.buttonAC.Size = new System.Drawing.Size(90, 23);
            this.buttonAC.TabIndex = 3;
            this.buttonAC.Text = "ИРИ на КРПУ";
            this.buttonAC.UseVisualStyleBackColor = true;
            this.buttonAC.Click += new System.EventHandler(this.buttonAC_Click);
            // 
            // buttonRS
            // 
            this.buttonRS.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.buttonRS.Location = new System.Drawing.Point(0, 25);
            this.buttonRS.Margin = new System.Windows.Forms.Padding(0);
            this.buttonRS.Name = "buttonRS";
            this.buttonRS.Size = new System.Drawing.Size(75, 23);
            this.buttonRS.TabIndex = 1;
            this.buttonRS.Text = "ИРИ на РП";
            this.buttonRS.UseVisualStyleBackColor = true;
            this.buttonRS.Click += new System.EventHandler(this.buttonRS_Click);
            // 
            // waveformGraph1
            // 
            this.waveformGraph1.Border = NationalInstruments.UI.Border.None;
            this.waveformGraph1.Cursors.AddRange(new NationalInstruments.UI.XYCursor[] {
            this.xCursor,
            this.porogCursor,
            this.SpectrxyCursor});
            this.waveformGraph1.InteractionMode = ((NationalInstruments.UI.GraphInteractionModes)((((NationalInstruments.UI.GraphInteractionModes.ZoomX | NationalInstruments.UI.GraphInteractionModes.PanX) 
            | NationalInstruments.UI.GraphInteractionModes.DragCursor) 
            | NationalInstruments.UI.GraphInteractionModes.DragAnnotationCaption)));
            this.waveformGraph1.Location = new System.Drawing.Point(0, 50);
            this.waveformGraph1.Margin = new System.Windows.Forms.Padding(0);
            this.waveformGraph1.Name = "waveformGraph1";
            this.waveformGraph1.PlotAreaBorder = NationalInstruments.UI.Border.None;
            this.waveformGraph1.Plots.AddRange(new NationalInstruments.UI.WaveformPlot[] {
            this.waveformPlot1});
            this.waveformGraph1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.waveformGraph1.Size = new System.Drawing.Size(500, 150);
            this.waveformGraph1.TabIndex = 6;
            this.waveformGraph1.UseColorGenerator = true;
            this.waveformGraph1.XAxes.AddRange(new NationalInstruments.UI.XAxis[] {
            this.xAxes});
            this.waveformGraph1.YAxes.AddRange(new NationalInstruments.UI.YAxis[] {
            this.yAxes});
            this.waveformGraph1.ZoomAnimation = false;
            this.waveformGraph1.XAxisRangeChanged += new NationalInstruments.UI.XAxisEventHandler(this.waveformGraph1_XAxisRangeChanged);
            this.waveformGraph1.AfterDrawPlot += new NationalInstruments.UI.AfterDrawXYPlotEventHandler(this.waveformGraph1_AfterDrawPlot);
            this.waveformGraph1.AfterMoveCursor += new NationalInstruments.UI.AfterMoveXYCursorEventHandler(this.waveformGraph1_AfterMoveCursor);
            this.waveformGraph1.PlotAreaBoundsChanged += new System.EventHandler(this.waveformGraph1_PlotAreaBoundsChanged);
            this.waveformGraph1.PlotAreaMouseMove += new System.Windows.Forms.MouseEventHandler(this.waveformGraph1_PlotAreaMouseMove);
            this.waveformGraph1.PlotAreaMouseWheel += new System.Windows.Forms.MouseEventHandler(this.waveformGraph1_PlotAreaMouseWheel);
            this.waveformGraph1.Zoom += new NationalInstruments.UI.ActionEventHandler(this.waveformGraph1_Zoom);
            this.waveformGraph1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.waveformGraph1_KeyDown);
            this.waveformGraph1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.waveformGraph1_MouseClick);
            this.waveformGraph1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.waveformGraph1_MouseDoubleClick);
            this.waveformGraph1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.waveformGraph1_MouseDown);
            this.waveformGraph1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.waveformGraph1_MouseUp);
            this.waveformGraph1.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.waveformGraph1_PreviewKeyDown);
            // 
            // xCursor
            // 
            this.xCursor.HorizontalCrosshairMode = NationalInstruments.UI.CursorCrosshairMode.None;
            this.xCursor.Plot = this.waveformPlot1;
            this.xCursor.PointSize = new System.Drawing.Size(0, 0);
            this.xCursor.PointStyle = NationalInstruments.UI.PointStyle.None;
            this.xCursor.SnapMode = NationalInstruments.UI.CursorSnapMode.Floating;
            this.xCursor.XPosition = 25D;
            this.xCursor.YPosition = -120D;
            // 
            // waveformPlot1
            // 
            this.waveformPlot1.AntiAliased = true;
            this.waveformPlot1.XAxis = this.xAxes;
            this.waveformPlot1.YAxis = this.yAxes;
            // 
            // xAxes
            // 
            this.xAxes.BaseLineColor = System.Drawing.Color.DarkRed;
            this.xAxes.Caption = "Частота, МГц";
            this.xAxes.CaptionBackColor = System.Drawing.Color.Transparent;
            this.xAxes.CaptionFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.xAxes.InteractionMode = NationalInstruments.UI.ScaleInteractionMode.None;
            this.xAxes.MajorDivisions.GridColor = System.Drawing.Color.SlateGray;
            this.xAxes.MajorDivisions.GridLineStyle = NationalInstruments.UI.LineStyle.Dot;
            this.xAxes.MajorDivisions.GridVisible = true;
            this.xAxes.MajorDivisions.LabelBackColor = System.Drawing.Color.Transparent;
            this.xAxes.MinorDivisions.GridColor = System.Drawing.Color.SlateGray;
            this.xAxes.MinorDivisions.GridLineStyle = NationalInstruments.UI.LineStyle.Dot;
            this.xAxes.MinorDivisions.GridVisible = true;
            this.xAxes.MinorDivisions.Interval = 5D;
            this.xAxes.MinorDivisions.TickVisible = true;
            this.xAxes.Mode = NationalInstruments.UI.AxisMode.Fixed;
            this.xAxes.Range = new NationalInstruments.UI.Range(25D, 3000D);
            // 
            // yAxes
            // 
            this.yAxes.Caption = "Уровень, дБ";
            this.yAxes.CaptionBackColor = System.Drawing.Color.Transparent;
            this.yAxes.CaptionFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.yAxes.MajorDivisions.GridColor = System.Drawing.Color.SlateGray;
            this.yAxes.MajorDivisions.GridLineStyle = NationalInstruments.UI.LineStyle.Dot;
            this.yAxes.MajorDivisions.GridVisible = true;
            this.yAxes.MinorDivisions.GridColor = System.Drawing.Color.SlateGray;
            this.yAxes.MinorDivisions.GridLineStyle = NationalInstruments.UI.LineStyle.Dot;
            this.yAxes.MinorDivisions.GridVisible = true;
            this.yAxes.MinorDivisions.TickVisible = true;
            this.yAxes.Mode = NationalInstruments.UI.AxisMode.Fixed;
            this.yAxes.Range = new NationalInstruments.UI.Range(-120D, 0D);
            // 
            // porogCursor
            // 
            this.porogCursor.Color = System.Drawing.Color.DarkMagenta;
            this.porogCursor.LabelAlignment = NationalInstruments.UI.PointAlignment.TopRight;
            this.porogCursor.LabelBackColor = System.Drawing.Color.Transparent;
            this.porogCursor.LabelDisplay = NationalInstruments.UI.XYCursorLabelDisplay.ShowY;
            this.porogCursor.LabelForeColor = System.Drawing.Color.DarkMagenta;
            this.porogCursor.Plot = this.waveformPlot1;
            this.porogCursor.PointSize = new System.Drawing.Size(0, 0);
            this.porogCursor.PointStyle = NationalInstruments.UI.PointStyle.None;
            this.porogCursor.SnapMode = NationalInstruments.UI.CursorSnapMode.Floating;
            this.porogCursor.VerticalCrosshairMode = NationalInstruments.UI.CursorCrosshairMode.None;
            this.porogCursor.XPosition = 25D;
            this.porogCursor.YPosition = -80D;
            // 
            // SpectrxyCursor
            // 
            this.SpectrxyCursor.Color = System.Drawing.Color.White;
            this.SpectrxyCursor.LabelBackColor = System.Drawing.Color.Transparent;
            this.SpectrxyCursor.Plot = this.waveformPlot1;
            this.SpectrxyCursor.PointSize = new System.Drawing.Size(0, 0);
            this.SpectrxyCursor.SnapMode = NationalInstruments.UI.CursorSnapMode.Fixed;
            this.SpectrxyCursor.XPosition = 25D;
            this.SpectrxyCursor.YPosition = -120D;
            // 
            // buttonExDF
            // 
            this.buttonExDF.Appearance = System.Windows.Forms.Appearance.Button;
            this.buttonExDF.Location = new System.Drawing.Point(165, 25);
            this.buttonExDF.Margin = new System.Windows.Forms.Padding(0);
            this.buttonExDF.Name = "buttonExDF";
            this.buttonExDF.Size = new System.Drawing.Size(75, 23);
            this.buttonExDF.TabIndex = 4;
            this.buttonExDF.Text = "Пл. ИРИ";
            this.buttonExDF.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.buttonExDF.UseVisualStyleBackColor = true;
            this.buttonExDF.CheckedChanged += new System.EventHandler(this.buttonExDF_CheckedChanged);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox1.Location = new System.Drawing.Point(37, 200);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(446, 20);
            this.pictureBox1.TabIndex = 100;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseClick);
            // 
            // intensityGraph1
            // 
            this.intensityGraph1.Border = NationalInstruments.UI.Border.None;
            this.intensityGraph1.ColorScales.AddRange(new NationalInstruments.UI.ColorScale[] {
            this.colorScale2});
            this.intensityGraph1.Cursors.AddRange(new NationalInstruments.UI.IntensityCursor[] {
            this.intensityCursor1});
            this.intensityGraph1.Location = new System.Drawing.Point(0, 220);
            this.intensityGraph1.Margin = new System.Windows.Forms.Padding(0);
            this.intensityGraph1.Name = "intensityGraph1";
            this.intensityGraph1.PlotAreaBorder = NationalInstruments.UI.Border.None;
            this.intensityGraph1.Plots.AddRange(new NationalInstruments.UI.IntensityPlot[] {
            this.intensityPlot2});
            this.intensityGraph1.Size = new System.Drawing.Size(500, 150);
            this.intensityGraph1.TabIndex = 11;
            this.intensityGraph1.XAxes.AddRange(new NationalInstruments.UI.IntensityXAxis[] {
            this.intensityXAxis3,
            this.intensityXAxis4});
            this.intensityGraph1.YAxes.AddRange(new NationalInstruments.UI.IntensityYAxis[] {
            this.intensityYAxis2});
            this.intensityGraph1.ZoomAnimation = false;
            this.intensityGraph1.AfterDrawPlot += new NationalInstruments.UI.AfterDrawIntensityPlotEventHandler(this.intensityGraph1_AfterDrawPlot);
            this.intensityGraph1.XAxisRangeChanged += new NationalInstruments.UI.IntensityXAxisEventHandler(this.intensityGraph1_XAxisRangeChanged);
            this.intensityGraph1.PlotAreaBoundsChanged += new System.EventHandler(this.intensityGraph1_PlotAreaBoundsChanged);
            this.intensityGraph1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.intensityGraph1_MouseClick);
            // 
            // colorScale2
            // 
            this.colorScale2.ColorMap.AddRange(new NationalInstruments.UI.ColorMapEntry[] {
            new NationalInstruments.UI.ColorMapEntry(-120D, System.Drawing.Color.Black),
            new NationalInstruments.UI.ColorMapEntry(-100D, System.Drawing.Color.Blue),
            new NationalInstruments.UI.ColorMapEntry(-70D, System.Drawing.Color.Lime)});
            this.colorScale2.HighColor = System.Drawing.Color.Red;
            this.colorScale2.Range = new NationalInstruments.UI.Range(-120D, 0D);
            // 
            // intensityCursor1
            // 
            this.intensityCursor1.Color = System.Drawing.Color.White;
            this.intensityCursor1.HorizontalCrosshairMode = NationalInstruments.UI.CursorCrosshairMode.None;
            this.intensityCursor1.LabelBackColor = System.Drawing.Color.Transparent;
            this.intensityCursor1.LabelDisplay = NationalInstruments.UI.IntensityCursorLabelDisplay.ShowX;
            this.intensityCursor1.Plot = this.intensityPlot2;
            this.intensityCursor1.PointSize = new System.Drawing.Size(0, 0);
            this.intensityCursor1.SnapMode = NationalInstruments.UI.CursorSnapMode.Fixed;
            this.intensityCursor1.XPosition = 25D;
            this.intensityCursor1.YPosition = 0D;
            // 
            // intensityPlot2
            // 
            this.intensityPlot2.ColorScale = this.colorScale2;
            this.intensityPlot2.HistoryCapacityX = 1;
            this.intensityPlot2.HistoryCapacityY = 1;
            this.intensityPlot2.XAxis = this.intensityXAxis3;
            this.intensityPlot2.YAxis = this.intensityYAxis2;
            // 
            // intensityXAxis3
            // 
            this.intensityXAxis3.InteractionMode = NationalInstruments.UI.ScaleInteractionMode.None;
            this.intensityXAxis3.MajorDivisions.LabelFormat = new NationalInstruments.UI.FormatString(NationalInstruments.UI.FormatStringMode.Numeric, "G6");
            this.intensityXAxis3.Mode = NationalInstruments.UI.IntensityAxisMode.Fixed;
            this.intensityXAxis3.Range = new NationalInstruments.UI.Range(0D, 974808D);
            this.intensityXAxis3.Visible = false;
            // 
            // intensityYAxis2
            // 
            this.intensityYAxis2.Caption = "Время, с";
            this.intensityYAxis2.CaptionBackColor = System.Drawing.Color.Transparent;
            this.intensityYAxis2.EditRangeDateTimeFormatMode = NationalInstruments.UI.DateTimeFormatMode.CreateLongTimeMode();
            this.intensityYAxis2.Mode = NationalInstruments.UI.IntensityAxisMode.Fixed;
            this.intensityYAxis2.Range = new NationalInstruments.UI.Range(0D, 60D);
            // 
            // intensityXAxis4
            // 
            this.intensityXAxis4.Caption = "Частота, МГц";
            this.intensityXAxis4.CaptionBackColor = System.Drawing.Color.Transparent;
            this.intensityXAxis4.MinorDivisions.TickVisible = true;
            this.intensityXAxis4.Range = new NationalInstruments.UI.Range(25D, 3000D);
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.gbutton0);
            this.groupBox2.Controls.Add(this.gbutton7_2);
            this.groupBox2.Controls.Add(this.gbutton9);
            this.groupBox2.Controls.Add(this.glabel9);
            this.groupBox2.Controls.Add(this.gbutton8);
            this.groupBox2.Controls.Add(this.glabel8);
            this.groupBox2.Controls.Add(this.gbutton7_1);
            this.groupBox2.Controls.Add(this.glabel7);
            this.groupBox2.Controls.Add(this.gbutton6);
            this.groupBox2.Controls.Add(this.glabel6);
            this.groupBox2.Controls.Add(this.gbutton5);
            this.groupBox2.Controls.Add(this.glabel5);
            this.groupBox2.Controls.Add(this.gbutton4);
            this.groupBox2.Controls.Add(this.glabel4);
            this.groupBox2.Controls.Add(this.gbutton3);
            this.groupBox2.Controls.Add(this.glabel3);
            this.groupBox2.Controls.Add(this.gbutton2);
            this.groupBox2.Controls.Add(this.glabel2);
            this.groupBox2.Controls.Add(this.gbutton1);
            this.groupBox2.Controls.Add(this.glabel1);
            this.groupBox2.Location = new System.Drawing.Point(343, 50);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(0);
            this.groupBox2.Size = new System.Drawing.Size(157, 280);
            this.groupBox2.TabIndex = 31;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Цвета панорамы";
            // 
            // gbutton0
            // 
            this.gbutton0.Location = new System.Drawing.Point(46, 23);
            this.gbutton0.Name = "gbutton0";
            this.gbutton0.Size = new System.Drawing.Size(20, 23);
            this.gbutton0.TabIndex = 120;
            this.gbutton0.Text = "!";
            this.gbutton0.UseVisualStyleBackColor = true;
            this.gbutton0.Click += new System.EventHandler(this.gbutton0_Click);
            // 
            // gbutton7_2
            // 
            this.gbutton7_2.Location = new System.Drawing.Point(115, 198);
            this.gbutton7_2.Name = "gbutton7_2";
            this.gbutton7_2.Size = new System.Drawing.Size(35, 23);
            this.gbutton7_2.TabIndex = 107;
            this.gbutton7_2.UseVisualStyleBackColor = true;
            this.gbutton7_2.Click += new System.EventHandler(this.gbutton7_2_Click);
            // 
            // gbutton9
            // 
            this.gbutton9.Location = new System.Drawing.Point(76, 255);
            this.gbutton9.Name = "gbutton9";
            this.gbutton9.Size = new System.Drawing.Size(75, 23);
            this.gbutton9.TabIndex = 109;
            this.gbutton9.UseVisualStyleBackColor = true;
            this.gbutton9.Click += new System.EventHandler(this.gbutton9_Click);
            // 
            // glabel9
            // 
            this.glabel9.AutoSize = true;
            this.glabel9.Location = new System.Drawing.Point(4, 254);
            this.glabel9.Name = "glabel9";
            this.glabel9.Size = new System.Drawing.Size(51, 13);
            this.glabel9.TabIndex = 119;
            this.glabel9.Text = "Подписи";
            // 
            // gbutton8
            // 
            this.gbutton8.Location = new System.Drawing.Point(87, 225);
            this.gbutton8.Name = "gbutton8";
            this.gbutton8.Size = new System.Drawing.Size(63, 23);
            this.gbutton8.TabIndex = 108;
            this.gbutton8.UseVisualStyleBackColor = true;
            this.gbutton8.Click += new System.EventHandler(this.gbutton8_Click);
            // 
            // glabel8
            // 
            this.glabel8.AutoSize = true;
            this.glabel8.Location = new System.Drawing.Point(3, 229);
            this.glabel8.Name = "glabel8";
            this.glabel8.Size = new System.Drawing.Size(75, 13);
            this.glabel8.TabIndex = 118;
            this.glabel8.Text = "Внешний фон";
            // 
            // gbutton7_1
            // 
            this.gbutton7_1.Location = new System.Drawing.Point(75, 198);
            this.gbutton7_1.Name = "gbutton7_1";
            this.gbutton7_1.Size = new System.Drawing.Size(35, 23);
            this.gbutton7_1.TabIndex = 106;
            this.gbutton7_1.UseVisualStyleBackColor = true;
            this.gbutton7_1.Click += new System.EventHandler(this.gbutton7_1_Click);
            // 
            // glabel7
            // 
            this.glabel7.AutoSize = true;
            this.glabel7.Location = new System.Drawing.Point(3, 200);
            this.glabel7.Name = "glabel7";
            this.glabel7.Size = new System.Drawing.Size(63, 13);
            this.glabel7.TabIndex = 117;
            this.glabel7.Text = "График ЧВ";
            // 
            // gbutton6
            // 
            this.gbutton6.Location = new System.Drawing.Point(75, 168);
            this.gbutton6.Name = "gbutton6";
            this.gbutton6.Size = new System.Drawing.Size(75, 23);
            this.gbutton6.TabIndex = 105;
            this.gbutton6.UseVisualStyleBackColor = true;
            this.gbutton6.Click += new System.EventHandler(this.gbutton6_Click);
            // 
            // glabel6
            // 
            this.glabel6.AutoSize = true;
            this.glabel6.Location = new System.Drawing.Point(3, 168);
            this.glabel6.Name = "glabel6";
            this.glabel6.Size = new System.Drawing.Size(64, 13);
            this.glabel6.TabIndex = 116;
            this.glabel6.Text = "График ЧП";
            // 
            // gbutton5
            // 
            this.gbutton5.Location = new System.Drawing.Point(75, 138);
            this.gbutton5.Name = "gbutton5";
            this.gbutton5.Size = new System.Drawing.Size(75, 23);
            this.gbutton5.TabIndex = 104;
            this.gbutton5.UseVisualStyleBackColor = true;
            this.gbutton5.Click += new System.EventHandler(this.gbutton5_Click);
            // 
            // glabel5
            // 
            this.glabel5.AutoSize = true;
            this.glabel5.Location = new System.Drawing.Point(1, 139);
            this.glabel5.Name = "glabel5";
            this.glabel5.Size = new System.Drawing.Size(57, 13);
            this.glabel5.TabIndex = 115;
            this.glabel5.Text = "Грфик ЧА";
            // 
            // gbutton4
            // 
            this.gbutton4.Location = new System.Drawing.Point(75, 108);
            this.gbutton4.Name = "gbutton4";
            this.gbutton4.Size = new System.Drawing.Size(75, 23);
            this.gbutton4.TabIndex = 103;
            this.gbutton4.UseVisualStyleBackColor = true;
            this.gbutton4.Click += new System.EventHandler(this.gbutton4_Click);
            // 
            // glabel4
            // 
            this.glabel4.AutoSize = true;
            this.glabel4.Location = new System.Drawing.Point(1, 110);
            this.glabel4.Name = "glabel4";
            this.glabel4.Size = new System.Drawing.Size(37, 13);
            this.glabel4.TabIndex = 114;
            this.glabel4.Text = "Сетка";
            // 
            // gbutton3
            // 
            this.gbutton3.Location = new System.Drawing.Point(75, 78);
            this.gbutton3.Name = "gbutton3";
            this.gbutton3.Size = new System.Drawing.Size(75, 23);
            this.gbutton3.TabIndex = 102;
            this.gbutton3.UseVisualStyleBackColor = true;
            this.gbutton3.Click += new System.EventHandler(this.gbutton3_Click);
            // 
            // glabel3
            // 
            this.glabel3.AutoSize = true;
            this.glabel3.Location = new System.Drawing.Point(3, 81);
            this.glabel3.Name = "glabel3";
            this.glabel3.Size = new System.Drawing.Size(43, 13);
            this.glabel3.TabIndex = 113;
            this.glabel3.Text = "Курсор";
            // 
            // gbutton2
            // 
            this.gbutton2.Location = new System.Drawing.Point(75, 48);
            this.gbutton2.Name = "gbutton2";
            this.gbutton2.Size = new System.Drawing.Size(75, 23);
            this.gbutton2.TabIndex = 101;
            this.gbutton2.UseVisualStyleBackColor = true;
            this.gbutton2.Click += new System.EventHandler(this.gbutton2_Click);
            // 
            // glabel2
            // 
            this.glabel2.AutoSize = true;
            this.glabel2.Location = new System.Drawing.Point(3, 52);
            this.glabel2.Name = "glabel2";
            this.glabel2.Size = new System.Drawing.Size(38, 13);
            this.glabel2.TabIndex = 112;
            this.glabel2.Text = "Порог";
            // 
            // gbutton1
            // 
            this.gbutton1.Location = new System.Drawing.Point(75, 20);
            this.gbutton1.Name = "gbutton1";
            this.gbutton1.Size = new System.Drawing.Size(75, 23);
            this.gbutton1.TabIndex = 100;
            this.gbutton1.UseVisualStyleBackColor = true;
            this.gbutton1.Click += new System.EventHandler(this.gbutton1_Click);
            // 
            // glabel1
            // 
            this.glabel1.AutoSize = true;
            this.glabel1.Location = new System.Drawing.Point(3, 23);
            this.glabel1.Name = "glabel1";
            this.glabel1.Size = new System.Drawing.Size(30, 13);
            this.glabel1.TabIndex = 111;
            this.glabel1.Text = "Фон";
            // 
            // scatterGraph1
            // 
            this.scatterGraph1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.scatterGraph1.Annotations.AddRange(new NationalInstruments.UI.XYAnnotation[] {
            this.xyPointAnnotation0,
            this.xyPointAnnotation45,
            this.xyPointAnnotation90,
            this.xyPointAnnotation135,
            this.xyPointAnnotation180,
            this.xyPointAnnotation225,
            this.xyPointAnnotation270,
            this.xyPointAnnotation315});
            this.scatterGraph1.Cursors.AddRange(new NationalInstruments.UI.XYCursor[] {
            this.roundxyCursor});
            this.scatterGraph1.InteractionMode = NationalInstruments.UI.GraphInteractionModes.None;
            this.scatterGraph1.Location = new System.Drawing.Point(0, 50);
            this.scatterGraph1.Margin = new System.Windows.Forms.Padding(0);
            this.scatterGraph1.Name = "scatterGraph1";
            this.scatterGraph1.Plots.AddRange(new NationalInstruments.UI.ScatterPlot[] {
            this.rScatterPlot1,
            this.rScatterPlot2,
            this.rxyPlot,
            this.rScatterPlot3,
            this.rScatterPlot4,
            this.rScatterPlot5,
            this.rScatterPlot6,
            this.rScatterPlot7,
            this.rScatterPlot8});
            this.scatterGraph1.Size = new System.Drawing.Size(300, 290);
            this.scatterGraph1.TabIndex = 10;
            this.scatterGraph1.UseColorGenerator = true;
            this.scatterGraph1.XAxes.AddRange(new NationalInstruments.UI.XAxis[] {
            this.xAxis2});
            this.scatterGraph1.YAxes.AddRange(new NationalInstruments.UI.YAxis[] {
            this.yAxis2});
            this.scatterGraph1.AfterDrawPlot += new NationalInstruments.UI.AfterDrawXYPlotEventHandler(this.scatterGraph1_AfterDrawPlot);
            this.scatterGraph1.PlotAreaBoundsChanged += new System.EventHandler(this.scatterGraph1_PlotAreaBoundsChanged);
            this.scatterGraph1.PlotAreaMouseMove += new System.Windows.Forms.MouseEventHandler(this.scatterGraph1_PlotAreaMouseMove);
            this.scatterGraph1.PlotAreaMouseWheel += new System.Windows.Forms.MouseEventHandler(this.scatterGraph1_PlotAreaMouseWheel);
            this.scatterGraph1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.scatterGraph1_MouseClick);
            this.scatterGraph1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.scatterGraph1_MouseDown);
            this.scatterGraph1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.scatterGraph1_MouseUp);
            // 
            // xyPointAnnotation0
            // 
            this.xyPointAnnotation0.ArrowVisible = false;
            this.xyPointAnnotation0.Caption = "0";
            this.xyPointAnnotation0.CaptionAlignment = new NationalInstruments.UI.AnnotationCaptionAlignment(NationalInstruments.UI.BoundsAlignment.None, 0F, 0F);
            this.xyPointAnnotation0.ShapeVisible = false;
            this.xyPointAnnotation0.XAxis = this.xAxis2;
            this.xyPointAnnotation0.XPosition = 0D;
            this.xyPointAnnotation0.YAxis = this.yAxis2;
            this.xyPointAnnotation0.YPosition = 11D;
            // 
            // xAxis2
            // 
            this.xAxis2.Mode = NationalInstruments.UI.AxisMode.Fixed;
            this.xAxis2.Range = new NationalInstruments.UI.Range(-12D, 12D);
            this.xAxis2.Visible = false;
            // 
            // yAxis2
            // 
            this.yAxis2.Mode = NationalInstruments.UI.AxisMode.Fixed;
            this.yAxis2.Range = new NationalInstruments.UI.Range(-12D, 12D);
            this.yAxis2.Visible = false;
            // 
            // xyPointAnnotation45
            // 
            this.xyPointAnnotation45.ArrowHeadSize = new System.Drawing.Size(1, 1);
            this.xyPointAnnotation45.ArrowTailSize = new System.Drawing.Size(1, 1);
            this.xyPointAnnotation45.ArrowVisible = false;
            this.xyPointAnnotation45.Caption = "45";
            this.xyPointAnnotation45.CaptionAlignment = new NationalInstruments.UI.AnnotationCaptionAlignment(NationalInstruments.UI.BoundsAlignment.None, 0F, 0F);
            this.xyPointAnnotation45.ShapeVisible = false;
            this.xyPointAnnotation45.XAxis = this.xAxis2;
            this.xyPointAnnotation45.XPosition = 7D;
            this.xyPointAnnotation45.YAxis = this.yAxis2;
            this.xyPointAnnotation45.YPosition = 7D;
            // 
            // xyPointAnnotation90
            // 
            this.xyPointAnnotation90.ArrowVisible = false;
            this.xyPointAnnotation90.Caption = "90";
            this.xyPointAnnotation90.CaptionAlignment = new NationalInstruments.UI.AnnotationCaptionAlignment(NationalInstruments.UI.BoundsAlignment.None, 0F, 0F);
            this.xyPointAnnotation90.ShapeVisible = false;
            this.xyPointAnnotation90.XAxis = this.xAxis2;
            this.xyPointAnnotation90.XPosition = 11D;
            this.xyPointAnnotation90.YAxis = this.yAxis2;
            this.xyPointAnnotation90.YPosition = 0D;
            // 
            // xyPointAnnotation135
            // 
            this.xyPointAnnotation135.ArrowVisible = false;
            this.xyPointAnnotation135.Caption = "135";
            this.xyPointAnnotation135.CaptionAlignment = new NationalInstruments.UI.AnnotationCaptionAlignment(NationalInstruments.UI.BoundsAlignment.None, 0F, 0F);
            this.xyPointAnnotation135.ShapeVisible = false;
            this.xyPointAnnotation135.XAxis = this.xAxis2;
            this.xyPointAnnotation135.XPosition = 10D;
            this.xyPointAnnotation135.YAxis = this.yAxis2;
            this.xyPointAnnotation135.YPosition = -10D;
            // 
            // xyPointAnnotation180
            // 
            this.xyPointAnnotation180.ArrowVisible = false;
            this.xyPointAnnotation180.Caption = "180";
            this.xyPointAnnotation180.CaptionAlignment = new NationalInstruments.UI.AnnotationCaptionAlignment(NationalInstruments.UI.BoundsAlignment.None, 0F, 0F);
            this.xyPointAnnotation180.ShapeVisible = false;
            this.xyPointAnnotation180.XAxis = this.xAxis2;
            this.xyPointAnnotation180.XPosition = 0D;
            this.xyPointAnnotation180.YAxis = this.yAxis2;
            this.xyPointAnnotation180.YPosition = -10D;
            // 
            // xyPointAnnotation225
            // 
            this.xyPointAnnotation225.ArrowVisible = false;
            this.xyPointAnnotation225.Caption = "225";
            this.xyPointAnnotation225.CaptionAlignment = new NationalInstruments.UI.AnnotationCaptionAlignment(NationalInstruments.UI.BoundsAlignment.None, 0F, 0F);
            this.xyPointAnnotation225.ShapeVisible = false;
            this.xyPointAnnotation225.XAxis = this.xAxis2;
            this.xyPointAnnotation225.XPosition = -10D;
            this.xyPointAnnotation225.YAxis = this.yAxis2;
            this.xyPointAnnotation225.YPosition = -10D;
            // 
            // xyPointAnnotation270
            // 
            this.xyPointAnnotation270.ArrowVisible = false;
            this.xyPointAnnotation270.Caption = "270";
            this.xyPointAnnotation270.CaptionAlignment = new NationalInstruments.UI.AnnotationCaptionAlignment(NationalInstruments.UI.BoundsAlignment.None, 0F, 0F);
            this.xyPointAnnotation270.ShapeVisible = false;
            this.xyPointAnnotation270.XAxis = this.xAxis2;
            this.xyPointAnnotation270.XPosition = -10D;
            this.xyPointAnnotation270.YAxis = this.yAxis2;
            this.xyPointAnnotation270.YPosition = 0D;
            // 
            // xyPointAnnotation315
            // 
            this.xyPointAnnotation315.ArrowVisible = false;
            this.xyPointAnnotation315.Caption = "315";
            this.xyPointAnnotation315.CaptionAlignment = new NationalInstruments.UI.AnnotationCaptionAlignment(NationalInstruments.UI.BoundsAlignment.None, 0F, 0F);
            this.xyPointAnnotation315.ShapeVisible = false;
            this.xyPointAnnotation315.XAxis = this.xAxis2;
            this.xyPointAnnotation315.XPosition = -9.6D;
            this.xyPointAnnotation315.YAxis = this.yAxis2;
            this.xyPointAnnotation315.YPosition = 9.6000000000000014D;
            // 
            // roundxyCursor
            // 
            this.roundxyCursor.Color = System.Drawing.Color.White;
            this.roundxyCursor.LabelBackColor = System.Drawing.Color.Transparent;
            this.roundxyCursor.Plot = this.rScatterPlot1;
            this.roundxyCursor.PointSize = new System.Drawing.Size(0, 0);
            this.roundxyCursor.SnapMode = NationalInstruments.UI.CursorSnapMode.Fixed;
            this.roundxyCursor.XPosition = 0D;
            this.roundxyCursor.YPosition = 0D;
            // 
            // rScatterPlot1
            // 
            this.rScatterPlot1.HistoryCapacity = 250;
            this.rScatterPlot1.XAxis = this.xAxis2;
            this.rScatterPlot1.YAxis = this.yAxis2;
            // 
            // rScatterPlot2
            // 
            this.rScatterPlot2.LineColor = System.Drawing.Color.Lime;
            this.rScatterPlot2.LineColorPrecedence = NationalInstruments.UI.ColorPrecedence.UserDefinedColor;
            this.rScatterPlot2.XAxis = this.xAxis2;
            this.rScatterPlot2.YAxis = this.yAxis2;
            // 
            // rxyPlot
            // 
            this.rxyPlot.HistoryCapacity = 100000;
            this.rxyPlot.LineColor = System.Drawing.Color.Lime;
            this.rxyPlot.LineColorPrecedence = NationalInstruments.UI.ColorPrecedence.UserDefinedColor;
            this.rxyPlot.LineStyle = NationalInstruments.UI.LineStyle.None;
            this.rxyPlot.PointColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(104)))), ((int)(((byte)(0)))));
            this.rxyPlot.PointSize = new System.Drawing.Size(6, 6);
            this.rxyPlot.PointStyle = NationalInstruments.UI.PointStyle.SolidCircle;
            this.rxyPlot.XAxis = this.xAxis2;
            this.rxyPlot.YAxis = this.yAxis2;
            // 
            // rScatterPlot3
            // 
            this.rScatterPlot3.LineColor = System.Drawing.Color.Lime;
            this.rScatterPlot3.LineColorPrecedence = NationalInstruments.UI.ColorPrecedence.UserDefinedColor;
            this.rScatterPlot3.XAxis = this.xAxis2;
            this.rScatterPlot3.YAxis = this.yAxis2;
            // 
            // rScatterPlot4
            // 
            this.rScatterPlot4.LineColor = System.Drawing.Color.Lime;
            this.rScatterPlot4.LineColorPrecedence = NationalInstruments.UI.ColorPrecedence.UserDefinedColor;
            this.rScatterPlot4.XAxis = this.xAxis2;
            this.rScatterPlot4.YAxis = this.yAxis2;
            // 
            // rScatterPlot5
            // 
            this.rScatterPlot5.LineColor = System.Drawing.Color.Lime;
            this.rScatterPlot5.LineColorPrecedence = NationalInstruments.UI.ColorPrecedence.UserDefinedColor;
            this.rScatterPlot5.LineStyle = NationalInstruments.UI.LineStyle.Dash;
            this.rScatterPlot5.XAxis = this.xAxis2;
            this.rScatterPlot5.YAxis = this.yAxis2;
            // 
            // rScatterPlot6
            // 
            this.rScatterPlot6.LineColor = System.Drawing.Color.Lime;
            this.rScatterPlot6.LineColorPrecedence = NationalInstruments.UI.ColorPrecedence.UserDefinedColor;
            this.rScatterPlot6.LineStyle = NationalInstruments.UI.LineStyle.Dash;
            this.rScatterPlot6.XAxis = this.xAxis2;
            this.rScatterPlot6.YAxis = this.yAxis2;
            // 
            // rScatterPlot7
            // 
            this.rScatterPlot7.LineColor = System.Drawing.Color.Lime;
            this.rScatterPlot7.LineColorPrecedence = NationalInstruments.UI.ColorPrecedence.UserDefinedColor;
            this.rScatterPlot7.XAxis = this.xAxis2;
            this.rScatterPlot7.YAxis = this.yAxis2;
            // 
            // rScatterPlot8
            // 
            this.rScatterPlot8.LineColor = System.Drawing.Color.Lime;
            this.rScatterPlot8.LineColorPrecedence = NationalInstruments.UI.ColorPrecedence.UserDefinedColor;
            this.rScatterPlot8.XAxis = this.xAxis2;
            this.rScatterPlot8.YAxis = this.yAxis2;
            // 
            // Plabel
            // 
            this.Plabel.AutoSize = true;
            this.Plabel.Location = new System.Drawing.Point(173, 285);
            this.Plabel.Name = "Plabel";
            this.Plabel.Size = new System.Drawing.Size(24, 13);
            this.Plabel.TabIndex = 23;
            this.Plabel.Text = "% =";
            // 
            // SDlabel
            // 
            this.SDlabel.AutoSize = true;
            this.SDlabel.Location = new System.Drawing.Point(173, 265);
            this.SDlabel.Name = "SDlabel";
            this.SDlabel.Size = new System.Drawing.Size(31, 13);
            this.SDlabel.TabIndex = 22;
            this.SDlabel.Text = "SD =";
            // 
            // Qlabel
            // 
            this.Qlabel.AutoSize = true;
            this.Qlabel.Location = new System.Drawing.Point(173, 245);
            this.Qlabel.Name = "Qlabel";
            this.Qlabel.Size = new System.Drawing.Size(24, 13);
            this.Qlabel.TabIndex = 21;
            this.Qlabel.Text = "Q =";
            // 
            // Flabel
            // 
            this.Flabel.AutoSize = true;
            this.Flabel.Location = new System.Drawing.Point(173, 225);
            this.Flabel.Name = "Flabel";
            this.Flabel.Size = new System.Drawing.Size(22, 13);
            this.Flabel.TabIndex = 20;
            this.Flabel.Text = "F =";
            // 
            // xyDataScatterGraph
            // 
            this.xyDataScatterGraph.Border = NationalInstruments.UI.Border.None;
            this.xyDataScatterGraph.CaptionVisible = false;
            this.xyDataScatterGraph.Cursors.AddRange(new NationalInstruments.UI.XYCursor[] {
            this.xyCursor1});
            this.xyDataScatterGraph.Location = new System.Drawing.Point(0, 220);
            this.xyDataScatterGraph.Margin = new System.Windows.Forms.Padding(0);
            this.xyDataScatterGraph.Name = "xyDataScatterGraph";
            this.xyDataScatterGraph.PlotAreaBorder = NationalInstruments.UI.Border.None;
            this.xyDataScatterGraph.Plots.AddRange(new NationalInstruments.UI.ScatterPlot[] {
            this.xyPlot});
            this.xyDataScatterGraph.Size = new System.Drawing.Size(500, 150);
            this.xyDataScatterGraph.TabIndex = 8;
            this.xyDataScatterGraph.XAxes.AddRange(new NationalInstruments.UI.XAxis[] {
            this.xAxis1});
            this.xyDataScatterGraph.YAxes.AddRange(new NationalInstruments.UI.YAxis[] {
            this.xyPlotYAxis});
            this.xyDataScatterGraph.ZoomAnimation = false;
            this.xyDataScatterGraph.AfterDrawPlot += new NationalInstruments.UI.AfterDrawXYPlotEventHandler(this.xyDataScatterGraph_AfterDrawPlot);
            this.xyDataScatterGraph.MouseClick += new System.Windows.Forms.MouseEventHandler(this.xyDataScatterGraph_MouseClick);
            this.xyDataScatterGraph.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.xyDataScatterGraph_PreviewKeyDown);
            // 
            // xyCursor1
            // 
            this.xyCursor1.Color = System.Drawing.Color.White;
            this.xyCursor1.LabelBackColor = System.Drawing.Color.Transparent;
            this.xyCursor1.LabelVisible = true;
            this.xyCursor1.Plot = this.xyPlot;
            this.xyCursor1.PointSize = new System.Drawing.Size(0, 0);
            this.xyCursor1.SnapMode = NationalInstruments.UI.CursorSnapMode.Fixed;
            this.xyCursor1.XPosition = 0D;
            this.xyCursor1.YPosition = 0D;
            // 
            // xyPlot
            // 
            this.xyPlot.AntiAliased = true;
            this.xyPlot.HistoryCapacity = 100000;
            this.xyPlot.LineStyle = NationalInstruments.UI.LineStyle.None;
            this.xyPlot.PointColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(104)))), ((int)(((byte)(0)))));
            this.xyPlot.PointSize = new System.Drawing.Size(6, 6);
            this.xyPlot.PointStyle = NationalInstruments.UI.PointStyle.SolidCircle;
            this.xyPlot.XAxis = this.xAxis1;
            this.xyPlot.YAxis = this.xyPlotYAxis;
            // 
            // xAxis1
            // 
            this.xAxis1.Caption = "Частота, МГц";
            this.xAxis1.CaptionBackColor = System.Drawing.Color.Transparent;
            this.xAxis1.MinorDivisions.TickVisible = true;
            this.xAxis1.Mode = NationalInstruments.UI.AxisMode.Fixed;
            this.xAxis1.Range = new NationalInstruments.UI.Range(25D, 3000D);
            // 
            // xyPlotYAxis
            // 
            this.xyPlotYAxis.Caption = "Пеленг, °";
            this.xyPlotYAxis.Mode = NationalInstruments.UI.AxisMode.Fixed;
            this.xyPlotYAxis.Range = new NationalInstruments.UI.Range(0D, 359D);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Black;
            this.label9.ForeColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(40, 64);
            this.label9.Margin = new System.Windows.Forms.Padding(0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(52, 13);
            this.label9.TabIndex = 12;
            this.label9.Text = "Частота:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Black;
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(162, 64);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(54, 13);
            this.label10.TabIndex = 13;
            this.label10.Text = "Уровень:";
            // 
            // button3
            // 
            this.button3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button3.BackColor = System.Drawing.Color.Black;
            this.button3.FlatAppearance.BorderSize = 0;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.ForeColor = System.Drawing.Color.Lime;
            this.button3.Location = new System.Drawing.Point(450, 62);
            this.button3.Margin = new System.Windows.Forms.Padding(0);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(32, 32);
            this.button3.TabIndex = 7;
            this.button3.Text = "1:1";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // EDFtimer
            // 
            this.EDFtimer.Interval = 500;
            this.EDFtimer.Tick += new System.EventHandler(this.EDFtimer_Tick);
            // 
            // intensityTimer
            // 
            this.intensityTimer.Interval = 1000;
            this.intensityTimer.Tick += new System.EventHandler(this.intensityTimer_Tick);
            // 
            // scatterGraph2
            // 
            this.scatterGraph2.Annotations.AddRange(new NationalInstruments.UI.XYAnnotation[] {
            this.xyrPointAnnotation0,
            this.xyrPointAnnotation45,
            this.xyrPointAnnotation90,
            this.xyrPointAnnotation135,
            this.xyrPointAnnotation180,
            this.xyrPointAnnotation225,
            this.xyrPointAnnotation270,
            this.xyrPointAnnotation315});
            this.scatterGraph2.Location = new System.Drawing.Point(37, 225);
            this.scatterGraph2.Margin = new System.Windows.Forms.Padding(0);
            this.scatterGraph2.Name = "scatterGraph2";
            this.scatterGraph2.Plots.AddRange(new NationalInstruments.UI.ScatterPlot[] {
            this.r2ScatterPlot1,
            this.r2ScatterPlot2,
            this.r2ScatterPlot3,
            this.r2ScatterPlot4});
            this.scatterGraph2.Size = new System.Drawing.Size(133, 133);
            this.scatterGraph2.TabIndex = 101;
            this.scatterGraph2.UseColorGenerator = true;
            this.scatterGraph2.XAxes.AddRange(new NationalInstruments.UI.XAxis[] {
            this.xAxis3});
            this.scatterGraph2.YAxes.AddRange(new NationalInstruments.UI.YAxis[] {
            this.yAxis1});
            // 
            // xyrPointAnnotation0
            // 
            this.xyrPointAnnotation0.ArrowVisible = false;
            this.xyrPointAnnotation0.Caption = "0";
            this.xyrPointAnnotation0.CaptionAlignment = new NationalInstruments.UI.AnnotationCaptionAlignment(NationalInstruments.UI.BoundsAlignment.None, 0F, 0F);
            this.xyrPointAnnotation0.InteractionMode = NationalInstruments.UI.AnnotationInteractionMode.None;
            this.xyrPointAnnotation0.ShapeVisible = false;
            this.xyrPointAnnotation0.XAxis = this.xAxis3;
            this.xyrPointAnnotation0.XPosition = 0D;
            this.xyrPointAnnotation0.YAxis = this.yAxis1;
            this.xyrPointAnnotation0.YPosition = 11D;
            // 
            // xAxis3
            // 
            this.xAxis3.Mode = NationalInstruments.UI.AxisMode.Fixed;
            this.xAxis3.Range = new NationalInstruments.UI.Range(-12D, 12D);
            this.xAxis3.Visible = false;
            // 
            // yAxis1
            // 
            this.yAxis1.Mode = NationalInstruments.UI.AxisMode.Fixed;
            this.yAxis1.Range = new NationalInstruments.UI.Range(-12D, 12D);
            this.yAxis1.Visible = false;
            // 
            // xyrPointAnnotation45
            // 
            this.xyrPointAnnotation45.ArrowHeadSize = new System.Drawing.Size(1, 1);
            this.xyrPointAnnotation45.ArrowTailSize = new System.Drawing.Size(1, 1);
            this.xyrPointAnnotation45.ArrowVisible = false;
            this.xyrPointAnnotation45.Caption = "45";
            this.xyrPointAnnotation45.CaptionAlignment = new NationalInstruments.UI.AnnotationCaptionAlignment(NationalInstruments.UI.BoundsAlignment.None, 0F, 0F);
            this.xyrPointAnnotation45.InteractionMode = NationalInstruments.UI.AnnotationInteractionMode.None;
            this.xyrPointAnnotation45.ShapeVisible = false;
            this.xyrPointAnnotation45.XAxis = this.xAxis3;
            this.xyrPointAnnotation45.XPosition = 7D;
            this.xyrPointAnnotation45.YAxis = this.yAxis1;
            this.xyrPointAnnotation45.YPosition = 7D;
            // 
            // xyrPointAnnotation90
            // 
            this.xyrPointAnnotation90.ArrowVisible = false;
            this.xyrPointAnnotation90.Caption = "90";
            this.xyrPointAnnotation90.CaptionAlignment = new NationalInstruments.UI.AnnotationCaptionAlignment(NationalInstruments.UI.BoundsAlignment.None, 0F, 0F);
            this.xyrPointAnnotation90.InteractionMode = NationalInstruments.UI.AnnotationInteractionMode.None;
            this.xyrPointAnnotation90.ShapeVisible = false;
            this.xyrPointAnnotation90.XAxis = this.xAxis3;
            this.xyrPointAnnotation90.XPosition = 11D;
            this.xyrPointAnnotation90.YAxis = this.yAxis1;
            this.xyrPointAnnotation90.YPosition = 0D;
            // 
            // xyrPointAnnotation135
            // 
            this.xyrPointAnnotation135.ArrowVisible = false;
            this.xyrPointAnnotation135.Caption = "135";
            this.xyrPointAnnotation135.CaptionAlignment = new NationalInstruments.UI.AnnotationCaptionAlignment(NationalInstruments.UI.BoundsAlignment.None, 0F, 0F);
            this.xyrPointAnnotation135.InteractionMode = NationalInstruments.UI.AnnotationInteractionMode.None;
            this.xyrPointAnnotation135.ShapeVisible = false;
            this.xyrPointAnnotation135.XAxis = this.xAxis3;
            this.xyrPointAnnotation135.XPosition = 10D;
            this.xyrPointAnnotation135.YAxis = this.yAxis1;
            this.xyrPointAnnotation135.YPosition = -10D;
            // 
            // xyrPointAnnotation180
            // 
            this.xyrPointAnnotation180.ArrowVisible = false;
            this.xyrPointAnnotation180.Caption = "180";
            this.xyrPointAnnotation180.CaptionAlignment = new NationalInstruments.UI.AnnotationCaptionAlignment(NationalInstruments.UI.BoundsAlignment.None, 0F, 0F);
            this.xyrPointAnnotation180.InteractionMode = NationalInstruments.UI.AnnotationInteractionMode.None;
            this.xyrPointAnnotation180.ShapeVisible = false;
            this.xyrPointAnnotation180.XAxis = this.xAxis3;
            this.xyrPointAnnotation180.XPosition = 0D;
            this.xyrPointAnnotation180.YAxis = this.yAxis1;
            this.xyrPointAnnotation180.YPosition = -10D;
            // 
            // xyrPointAnnotation225
            // 
            this.xyrPointAnnotation225.ArrowVisible = false;
            this.xyrPointAnnotation225.Caption = "225";
            this.xyrPointAnnotation225.CaptionAlignment = new NationalInstruments.UI.AnnotationCaptionAlignment(NationalInstruments.UI.BoundsAlignment.None, 0F, 0F);
            this.xyrPointAnnotation225.InteractionMode = NationalInstruments.UI.AnnotationInteractionMode.None;
            this.xyrPointAnnotation225.ShapeVisible = false;
            this.xyrPointAnnotation225.XAxis = this.xAxis3;
            this.xyrPointAnnotation225.XPosition = -10D;
            this.xyrPointAnnotation225.YAxis = this.yAxis1;
            this.xyrPointAnnotation225.YPosition = -10D;
            // 
            // xyrPointAnnotation270
            // 
            this.xyrPointAnnotation270.ArrowVisible = false;
            this.xyrPointAnnotation270.Caption = "270";
            this.xyrPointAnnotation270.CaptionAlignment = new NationalInstruments.UI.AnnotationCaptionAlignment(NationalInstruments.UI.BoundsAlignment.None, 0F, 0F);
            this.xyrPointAnnotation270.InteractionMode = NationalInstruments.UI.AnnotationInteractionMode.None;
            this.xyrPointAnnotation270.ShapeVisible = false;
            this.xyrPointAnnotation270.XAxis = this.xAxis3;
            this.xyrPointAnnotation270.XPosition = -10D;
            this.xyrPointAnnotation270.YAxis = this.yAxis1;
            this.xyrPointAnnotation270.YPosition = 0D;
            // 
            // xyrPointAnnotation315
            // 
            this.xyrPointAnnotation315.ArrowVisible = false;
            this.xyrPointAnnotation315.Caption = "315";
            this.xyrPointAnnotation315.CaptionAlignment = new NationalInstruments.UI.AnnotationCaptionAlignment(NationalInstruments.UI.BoundsAlignment.None, 0F, 0F);
            this.xyrPointAnnotation315.InteractionMode = NationalInstruments.UI.AnnotationInteractionMode.None;
            this.xyrPointAnnotation315.ShapeVisible = false;
            this.xyrPointAnnotation315.XAxis = this.xAxis3;
            this.xyrPointAnnotation315.XPosition = -9.6D;
            this.xyrPointAnnotation315.YAxis = this.yAxis1;
            this.xyrPointAnnotation315.YPosition = 9.6000000000000014D;
            // 
            // r2ScatterPlot1
            // 
            this.r2ScatterPlot1.AntiAliased = true;
            this.r2ScatterPlot1.FillToBaseColor = System.Drawing.SystemColors.ScrollBar;
            this.r2ScatterPlot1.HistoryCapacity = 10000;
            this.r2ScatterPlot1.LineColor = System.Drawing.Color.Lime;
            this.r2ScatterPlot1.LineColorPrecedence = NationalInstruments.UI.ColorPrecedence.UserDefinedColor;
            this.r2ScatterPlot1.SmoothUpdates = true;
            this.r2ScatterPlot1.XAxis = this.xAxis3;
            this.r2ScatterPlot1.YAxis = this.yAxis1;
            // 
            // r2ScatterPlot2
            // 
            this.r2ScatterPlot2.AntiAliased = true;
            this.r2ScatterPlot2.FillToBaseColor = System.Drawing.Color.Gray;
            this.r2ScatterPlot2.LineColor = System.Drawing.Color.Gray;
            this.r2ScatterPlot2.LineColorPrecedence = NationalInstruments.UI.ColorPrecedence.UserDefinedColor;
            this.r2ScatterPlot2.SmoothUpdates = true;
            this.r2ScatterPlot2.XAxis = this.xAxis3;
            this.r2ScatterPlot2.YAxis = this.yAxis1;
            // 
            // r2ScatterPlot3
            // 
            this.r2ScatterPlot3.AntiAliased = true;
            this.r2ScatterPlot3.FillToBaseColor = System.Drawing.Color.Gray;
            this.r2ScatterPlot3.LineColor = System.Drawing.Color.Gray;
            this.r2ScatterPlot3.LineColorPrecedence = NationalInstruments.UI.ColorPrecedence.UserDefinedColor;
            this.r2ScatterPlot3.SmoothUpdates = true;
            this.r2ScatterPlot3.XAxis = this.xAxis3;
            this.r2ScatterPlot3.YAxis = this.yAxis1;
            // 
            // r2ScatterPlot4
            // 
            this.r2ScatterPlot4.AntiAliased = true;
            this.r2ScatterPlot4.LineColor = System.Drawing.Color.Cyan;
            this.r2ScatterPlot4.LineColorPrecedence = NationalInstruments.UI.ColorPrecedence.UserDefinedColor;
            this.r2ScatterPlot4.SmoothUpdates = true;
            this.r2ScatterPlot4.XAxis = this.xAxis3;
            this.r2ScatterPlot4.YAxis = this.yAxis1;
            // 
            // CofAvPh
            // 
            this.CofAvPh.AutoSize = true;
            this.CofAvPh.Location = new System.Drawing.Point(173, 305);
            this.CofAvPh.Name = "CofAvPh";
            this.CofAvPh.Size = new System.Drawing.Size(58, 13);
            this.CofAvPh.TabIndex = 102;
            this.CofAvPh.Text = "CofAvPh =";
            // 
            // CofAvPl
            // 
            this.CofAvPl.AutoSize = true;
            this.CofAvPl.Location = new System.Drawing.Point(173, 325);
            this.CofAvPl.Name = "CofAvPl";
            this.CofAvPl.Size = new System.Drawing.Size(54, 13);
            this.CofAvPl.TabIndex = 103;
            this.CofAvPl.Text = "CofAvPl =";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Black;
            this.label11.ForeColor = System.Drawing.Color.White;
            this.label11.Location = new System.Drawing.Point(52, 138);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(22, 13);
            this.label11.TabIndex = 106;
            this.label11.Text = "v =";
            this.label11.Visible = false;
            // 
            // labelLeft
            // 
            this.labelLeft.BackColor = System.Drawing.Color.Black;
            this.labelLeft.ForeColor = System.Drawing.Color.Lime;
            this.labelLeft.Location = new System.Drawing.Point(57, 121);
            this.labelLeft.Margin = new System.Windows.Forms.Padding(0);
            this.labelLeft.Name = "labelLeft";
            this.labelLeft.Size = new System.Drawing.Size(10, 15);
            this.labelLeft.TabIndex = 107;
            this.labelLeft.Text = "<";
            this.labelLeft.Click += new System.EventHandler(this.labelLeft_Click);
            // 
            // labelRight
            // 
            this.labelRight.BackColor = System.Drawing.Color.Black;
            this.labelRight.ForeColor = System.Drawing.Color.Lime;
            this.labelRight.Location = new System.Drawing.Point(67, 121);
            this.labelRight.Margin = new System.Windows.Forms.Padding(0);
            this.labelRight.Name = "labelRight";
            this.labelRight.Size = new System.Drawing.Size(10, 15);
            this.labelRight.TabIndex = 108;
            this.labelRight.Text = ">";
            this.labelRight.Click += new System.EventHandler(this.labelRight_Click);
            // 
            // ScanSpeedTimer
            // 
            this.ScanSpeedTimer.Interval = 1000;
            this.ScanSpeedTimer.Tick += new System.EventHandler(this.ScanSpeedTimer_Tick);
            // 
            // FtextBox
            // 
            this.FtextBox.Location = new System.Drawing.Point(235, 220);
            this.FtextBox.Name = "FtextBox";
            this.FtextBox.ReadOnly = true;
            this.FtextBox.Size = new System.Drawing.Size(60, 20);
            this.FtextBox.TabIndex = 109;
            // 
            // QtextBox
            // 
            this.QtextBox.Location = new System.Drawing.Point(235, 242);
            this.QtextBox.Name = "QtextBox";
            this.QtextBox.ReadOnly = true;
            this.QtextBox.Size = new System.Drawing.Size(60, 20);
            this.QtextBox.TabIndex = 110;
            // 
            // SDtextBox
            // 
            this.SDtextBox.Location = new System.Drawing.Point(235, 263);
            this.SDtextBox.Name = "SDtextBox";
            this.SDtextBox.ReadOnly = true;
            this.SDtextBox.Size = new System.Drawing.Size(60, 20);
            this.SDtextBox.TabIndex = 111;
            // 
            // PtextBox
            // 
            this.PtextBox.Location = new System.Drawing.Point(235, 283);
            this.PtextBox.Name = "PtextBox";
            this.PtextBox.ReadOnly = true;
            this.PtextBox.Size = new System.Drawing.Size(60, 20);
            this.PtextBox.TabIndex = 112;
            // 
            // shapeContainer1
            // 
            this.shapeContainer1.Location = new System.Drawing.Point(0, 0);
            this.shapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer1.Name = "shapeContainer1";
            this.shapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.PllineShape,
            this.PhlineShape,
            this.PlineShape,
            this.SDlineShape,
            this.QlineShape,
            this.FlineShape});
            this.shapeContainer1.Size = new System.Drawing.Size(500, 370);
            this.shapeContainer1.TabIndex = 113;
            this.shapeContainer1.TabStop = false;
            // 
            // PllineShape
            // 
            this.PllineShape.Enabled = false;
            this.PllineShape.Name = "PllineShape";
            this.PllineShape.X1 = 213;
            this.PllineShape.X2 = 288;
            this.PllineShape.Y1 = 332;
            this.PllineShape.Y2 = 332;
            // 
            // PhlineShape
            // 
            this.PhlineShape.Enabled = false;
            this.PhlineShape.Name = "PhlineShape";
            this.PhlineShape.X1 = 213;
            this.PhlineShape.X2 = 288;
            this.PhlineShape.Y1 = 313;
            this.PhlineShape.Y2 = 313;
            // 
            // PlineShape
            // 
            this.PlineShape.Enabled = false;
            this.PlineShape.Name = "PlineShape";
            this.PlineShape.X1 = 191;
            this.PlineShape.X2 = 266;
            this.PlineShape.Y1 = 294;
            this.PlineShape.Y2 = 294;
            // 
            // SDlineShape
            // 
            this.SDlineShape.Enabled = false;
            this.SDlineShape.Name = "SDlineShape";
            this.SDlineShape.X1 = 197;
            this.SDlineShape.X2 = 272;
            this.SDlineShape.Y1 = 275;
            this.SDlineShape.Y2 = 275;
            // 
            // QlineShape
            // 
            this.QlineShape.Enabled = false;
            this.QlineShape.Name = "QlineShape";
            this.QlineShape.X1 = 190;
            this.QlineShape.X2 = 265;
            this.QlineShape.Y1 = 255;
            this.QlineShape.Y2 = 255;
            // 
            // FlineShape
            // 
            this.FlineShape.Enabled = false;
            this.FlineShape.Name = "FlineShape";
            this.FlineShape.X1 = 187;
            this.FlineShape.X2 = 262;
            this.FlineShape.Y1 = 236;
            this.FlineShape.Y2 = 236;
            // 
            // numericUpDownPh
            // 
            this.numericUpDownPh.Location = new System.Drawing.Point(235, 304);
            this.numericUpDownPh.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numericUpDownPh.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownPh.Name = "numericUpDownPh";
            this.numericUpDownPh.Size = new System.Drawing.Size(60, 20);
            this.numericUpDownPh.TabIndex = 114;
            this.numericUpDownPh.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.numericUpDownPh.ValueChanged += new System.EventHandler(this.numericUpDownPh_ValueChanged);
            this.numericUpDownPh.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.numericUpDownPh_KeyPress);
            // 
            // numericUpDownPl
            // 
            this.numericUpDownPl.Location = new System.Drawing.Point(235, 323);
            this.numericUpDownPl.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numericUpDownPl.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownPl.Name = "numericUpDownPl";
            this.numericUpDownPl.Size = new System.Drawing.Size(60, 20);
            this.numericUpDownPl.TabIndex = 115;
            this.numericUpDownPl.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.numericUpDownPl.ValueChanged += new System.EventHandler(this.numericUpDownPl_ValueChanged);
            this.numericUpDownPl.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.numericUpDownPl_KeyPress);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.toolStripMenuItem2,
            this.toolStripMenuItem3});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(181, 70);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(180, 22);
            this.toolStripMenuItem1.Text = "toolStripMenuItem1";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(180, 22);
            this.toolStripMenuItem2.Text = "toolStripMenuItem2";
            this.toolStripMenuItem2.Click += new System.EventHandler(this.toolStripMenuItem2_Click);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(180, 22);
            this.toolStripMenuItem3.Text = "toolStripMenuItem3";
            this.toolStripMenuItem3.Click += new System.EventHandler(this.toolStripMenuItem3_Click);
            // 
            // button34
            // 
            this.button34.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button34.BackColor = System.Drawing.Color.Black;
            this.button34.FlatAppearance.BorderSize = 0;
            this.button34.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button34.ForeColor = System.Drawing.Color.Lime;
            this.button34.Location = new System.Drawing.Point(450, 94);
            this.button34.Margin = new System.Windows.Forms.Padding(0);
            this.button34.Name = "button34";
            this.button34.Size = new System.Drawing.Size(32, 32);
            this.button34.TabIndex = 116;
            this.button34.Text = "1:1";
            this.button34.UseVisualStyleBackColor = false;
            this.button34.Click += new System.EventHandler(this.button34_Click);
            // 
            // label94
            // 
            this.label94.AutoSize = true;
            this.label94.BackColor = System.Drawing.Color.Black;
            this.label94.ForeColor = System.Drawing.Color.White;
            this.label94.Location = new System.Drawing.Point(40, 84);
            this.label94.Margin = new System.Windows.Forms.Padding(0);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(52, 13);
            this.label94.TabIndex = 117;
            this.label94.Text = "Частота:";
            // 
            // label104
            // 
            this.label104.AutoSize = true;
            this.label104.BackColor = System.Drawing.Color.Black;
            this.label104.ForeColor = System.Drawing.Color.White;
            this.label104.Location = new System.Drawing.Point(40, 104);
            this.label104.Margin = new System.Windows.Forms.Padding(0);
            this.label104.Name = "label104";
            this.label104.Size = new System.Drawing.Size(47, 13);
            this.label104.TabIndex = 118;
            this.label104.Text = "Пеленг:";
            // 
            // label95
            // 
            this.label95.AutoSize = true;
            this.label95.BackColor = System.Drawing.Color.Black;
            this.label95.ForeColor = System.Drawing.Color.White;
            this.label95.Location = new System.Drawing.Point(40, 220);
            this.label95.Margin = new System.Windows.Forms.Padding(0);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(52, 13);
            this.label95.TabIndex = 119;
            this.label95.Text = "Частота:";
            // 
            // PanoramaControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label95);
            this.Controls.Add(this.label104);
            this.Controls.Add(this.label94);
            this.Controls.Add(this.button34);
            this.Controls.Add(this.numericUpDownPl);
            this.Controls.Add(this.numericUpDownPh);
            this.Controls.Add(this.PtextBox);
            this.Controls.Add(this.SDtextBox);
            this.Controls.Add(this.QtextBox);
            this.Controls.Add(this.FtextBox);
            this.Controls.Add(this.labelRight);
            this.Controls.Add(this.labelLeft);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.CofAvPl);
            this.Controls.Add(this.CofAvPh);
            this.Controls.Add(this.scatterGraph2);
            this.Controls.Add(this.Plabel);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.SDlabel);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.Qlabel);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.Flabel);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.buttonExDF);
            this.Controls.Add(this.numericUpDown1);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.buttonTD);
            this.Controls.Add(this.buttonAC);
            this.Controls.Add(this.buttonRS);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.waveformGraph1);
            this.Controls.Add(this.scatterGraph1);
            this.Controls.Add(this.intensityGraph1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.shapeContainer1);
            this.Controls.Add(this.xyDataScatterGraph);
            this.Name = "PanoramaControl";
            this.Size = new System.Drawing.Size(500, 370);
            this.Load += new System.EventHandler(this.PanoramaControl_Load);
            this.ControlRemoved += new System.Windows.Forms.ControlEventHandler(this.PanoramaControl_ControlRemoved);
            this.Resize += new System.EventHandler(this.PanoramaControl_Resize);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.waveformGraph1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xCursor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.porogCursor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SpectrxyCursor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intensityGraph1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intensityCursor1)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.scatterGraph1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.roundxyCursor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xyDataScatterGraph)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xyCursor1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.scatterGraph2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownPh)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownPl)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton toolStripButton3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton toolStripButton4;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripButton toolStripButton5;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Button buttonTD;
        private System.Windows.Forms.Button buttonAC;
        private System.Windows.Forms.Button buttonRS;
        private NationalInstruments.UI.WindowsForms.WaveformGraph waveformGraph1;
        private NationalInstruments.UI.XYCursor xCursor;
        private NationalInstruments.UI.WaveformPlot waveformPlot1;
        private NationalInstruments.UI.YAxis yAxes;
        private NationalInstruments.UI.XYCursor porogCursor;
        private NationalInstruments.UI.XYCursor SpectrxyCursor;
        private System.Windows.Forms.CheckBox buttonExDF;
        private System.Windows.Forms.PictureBox pictureBox1;
        private NationalInstruments.UI.WindowsForms.IntensityGraph intensityGraph1;
        private NationalInstruments.UI.ColorScale colorScale2;
        private NationalInstruments.UI.IntensityPlot intensityPlot2;
        private NationalInstruments.UI.IntensityXAxis intensityXAxis3;
        private NationalInstruments.UI.IntensityYAxis intensityYAxis2;
        private NationalInstruments.UI.IntensityXAxis intensityXAxis4;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button gbutton7_2;
        private System.Windows.Forms.Button gbutton9;
        private System.Windows.Forms.Label glabel9;
        private System.Windows.Forms.Button gbutton8;
        private System.Windows.Forms.Label glabel8;
        private System.Windows.Forms.Button gbutton7_1;
        private System.Windows.Forms.Label glabel7;
        private System.Windows.Forms.Button gbutton6;
        private System.Windows.Forms.Label glabel6;
        private System.Windows.Forms.Button gbutton5;
        private System.Windows.Forms.Label glabel5;
        private System.Windows.Forms.Button gbutton4;
        private System.Windows.Forms.Label glabel4;
        private System.Windows.Forms.Button gbutton3;
        private System.Windows.Forms.Label glabel3;
        private System.Windows.Forms.Button gbutton2;
        private System.Windows.Forms.Label glabel2;
        private System.Windows.Forms.Button gbutton1;
        private System.Windows.Forms.Label glabel1;
        private NationalInstruments.UI.WindowsForms.ScatterGraph scatterGraph1;
        private NationalInstruments.UI.XYPointAnnotation xyPointAnnotation0;
        private NationalInstruments.UI.XAxis xAxis2;
        private NationalInstruments.UI.YAxis yAxis2;
        private NationalInstruments.UI.XYPointAnnotation xyPointAnnotation45;
        private NationalInstruments.UI.XYPointAnnotation xyPointAnnotation90;
        private NationalInstruments.UI.XYPointAnnotation xyPointAnnotation135;
        private NationalInstruments.UI.XYPointAnnotation xyPointAnnotation180;
        private NationalInstruments.UI.XYPointAnnotation xyPointAnnotation225;
        private NationalInstruments.UI.XYPointAnnotation xyPointAnnotation270;
        private NationalInstruments.UI.XYPointAnnotation xyPointAnnotation315;
        private NationalInstruments.UI.XYCursor roundxyCursor;
        private NationalInstruments.UI.ScatterPlot rScatterPlot1;
        private NationalInstruments.UI.ScatterPlot rScatterPlot2;
        private NationalInstruments.UI.ScatterPlot rxyPlot;
        private NationalInstruments.UI.ScatterPlot rScatterPlot3;
        private NationalInstruments.UI.ScatterPlot rScatterPlot4;
        private NationalInstruments.UI.ScatterPlot rScatterPlot5;
        private NationalInstruments.UI.ScatterPlot rScatterPlot6;
        private NationalInstruments.UI.ScatterPlot rScatterPlot7;
        private NationalInstruments.UI.ScatterPlot rScatterPlot8;
        private System.Windows.Forms.Label Plabel;
        private System.Windows.Forms.Label SDlabel;
        private System.Windows.Forms.Label Qlabel;
        private System.Windows.Forms.Label Flabel;
        private NationalInstruments.UI.WindowsForms.ScatterGraph xyDataScatterGraph;
        private NationalInstruments.UI.XYCursor xyCursor1;
        private NationalInstruments.UI.ScatterPlot xyPlot;
        private NationalInstruments.UI.XAxis xAxis1;
        private NationalInstruments.UI.YAxis xyPlotYAxis;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Timer EDFtimer;
        private System.Windows.Forms.Timer intensityTimer;
        private NationalInstruments.UI.IntensityCursor intensityCursor1;
        private NationalInstruments.UI.WindowsForms.ScatterGraph scatterGraph2;
        private NationalInstruments.UI.XYPointAnnotation xyrPointAnnotation0;
        private NationalInstruments.UI.XAxis xAxis3;
        private NationalInstruments.UI.YAxis yAxis1;
        private NationalInstruments.UI.XYPointAnnotation xyrPointAnnotation45;
        private NationalInstruments.UI.XYPointAnnotation xyrPointAnnotation90;
        private NationalInstruments.UI.XYPointAnnotation xyrPointAnnotation135;
        private NationalInstruments.UI.XYPointAnnotation xyrPointAnnotation180;
        private NationalInstruments.UI.XYPointAnnotation xyrPointAnnotation225;
        private NationalInstruments.UI.XYPointAnnotation xyrPointAnnotation270;
        private NationalInstruments.UI.XYPointAnnotation xyrPointAnnotation315;
        private NationalInstruments.UI.ScatterPlot r2ScatterPlot1;
        private NationalInstruments.UI.ScatterPlot r2ScatterPlot2;
        private NationalInstruments.UI.ScatterPlot r2ScatterPlot3;
        private NationalInstruments.UI.ScatterPlot r2ScatterPlot4;
        private System.Windows.Forms.Button gbutton0;
        private System.Windows.Forms.Label CofAvPh;
        private System.Windows.Forms.Label CofAvPl;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label labelLeft;
        private System.Windows.Forms.Label labelRight;
        private System.Windows.Forms.Timer ScanSpeedTimer;
        private NationalInstruments.UI.XAxis xAxes;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.TextBox FtextBox;
        private System.Windows.Forms.TextBox QtextBox;
        private System.Windows.Forms.TextBox SDtextBox;
        private System.Windows.Forms.TextBox PtextBox;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer1;
        private Microsoft.VisualBasic.PowerPacks.LineShape PllineShape;
        private Microsoft.VisualBasic.PowerPacks.LineShape PhlineShape;
        private Microsoft.VisualBasic.PowerPacks.LineShape PlineShape;
        private Microsoft.VisualBasic.PowerPacks.LineShape SDlineShape;
        private Microsoft.VisualBasic.PowerPacks.LineShape QlineShape;
        private Microsoft.VisualBasic.PowerPacks.LineShape FlineShape;
        private System.Windows.Forms.NumericUpDown numericUpDownPh;
        private System.Windows.Forms.NumericUpDown numericUpDownPl;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.Button button34;
        private System.Windows.Forms.Label label94;
        private System.Windows.Forms.Label label104;
        private System.Windows.Forms.Label label95;
    }
}
