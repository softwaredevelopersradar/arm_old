﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using NationalInstruments.UI;
using System.Net.Sockets;
using System.Net;
using System.Threading;
using System.Xml;

using Protocols;
using VariableDynamic;
using VariableStatic;

using Nito.AsyncEx;

namespace PanoramaLibrary
{
    public partial class PanoramaControl : UserControl
    {
        static double GlobalDotsPerBand = 9831.0;
        static double GlobalBandWidthMHz = 30.0;

        static double GlobalRangeMin = 25.0;

        static int GlobalNumberofBands = 100;
        //static int GlobalNumberofBands = 200;

        static double GlobalRangeMax = GlobalRangeMin + GlobalNumberofBands * GlobalBandWidthMHz;

        static double GlobalDotsRange = Math.Round((GlobalRangeMax - GlobalRangeMin) / GlobalBandWidthMHz * GlobalDotsPerBand);

        static double GlobalYRangeMin = -120.0;
        static double GlobalYRangeMax = 0;


        int mouseDownPoint = -1;
        int mouseUpPoint = -1;

        double mouseDownXPoint = -1;
        double mouseUpXPoint = -1;

        int eBoundsWidth = -1;
        int eBoundsHeight = -1;
        int eBoundsX = -1;
        int eBoundsY = -1;

        int pelengeBoundsWidth = -1;
        int pelengeBoundsHeight = -1;
        int pelengeBoundsX = -1;
        int pelengeBoundsY = -1;

        int roundPelengeBoundsWidth = -1;
        int roundPelengeBoundsHeight = -1;
        int roundPelengeBoundsX = -1;
        int roundPelengeBoundsY = -1;

        int intensityeBoundsWidth = -1;
        int intensityeBoundsHeight = -1;
        int intensityeBoundsX = -1;
        int intensityeBoundsY = -1;



        AWPtoBearingDSPprotocolNew dsp = new AWPtoBearingDSPprotocolNew();


        int currindex = 0;

        string serverIPAdress = "";
        int serverPort = 0;



        bool preparationflag = true;


        bool SpectrumRequestIsWorking = false;
        bool BearingRequestIsWorking = false;

        bool RPwoDF;
        bool RPwDF;

        bool RSupp = false;

        VariableWork variableWork = new VariableWork();

        VariableConnection variableconnection = new VariableConnection();

        VariableColor variableColor = new VariableColor();

        public delegate void ChangeActiveRedRectangleEventHandler(int startIndex, int endIndex);
        public event ChangeActiveRedRectangleEventHandler ChangeActiveRedRectangle;

        //public delegate void ActiveBandIndexEventHandler(int activeBandIndex);
        //public event ActiveBandIndexEventHandler ActiveBandIndexSendEvent;


        public void Init3000()
        {
            GlobalNumberofBands = 100;
            GlobalRangeMax = GlobalRangeMin + GlobalNumberofBands * GlobalBandWidthMHz;
            GlobalDotsRange = Math.Round((GlobalRangeMax - GlobalRangeMin) / GlobalBandWidthMHz * GlobalDotsPerBand);
            InitGraphicsRange();
        }

        public void Init6000()
        {
            GlobalNumberofBands = 200;
            GlobalRangeMax = GlobalRangeMin + GlobalNumberofBands * GlobalBandWidthMHz;
            GlobalDotsRange = Math.Round((GlobalRangeMax - GlobalRangeMin) / GlobalBandWidthMHz * GlobalDotsPerBand);
            InitGraphicsRange();
        }

        public PanoramaControl()
        {
            InitializeComponent();

            //LoadConnectionSettings();

            //LoadINI();
            //LoadColors();

            //Task.Run(() => FirstConnect());


            InitGraphicsRange();

            InitContextMenuStrip();

            //set_language("rus"); // Выбираем по умолчанию русский
            //set_language("eng");

            toolStripButton1.PerformClick();
            //toolStripButton2.PerformClick();
            //toolStripButton3.PerformClick();
            //toolStripButton4.PerformClick();
            //toolStripButton5.PerformClick();


            VariableWork.OnChangeRegime += UpdateRegime;
            VariableWork.OnChangeRangeSectorReconOwn += UpdateRangeSector;

            VariableWork.OnChangeSupprFWS_Own += VariableWork_OnChangeSupprFWS_Own;

            VariableWork.OnChangeDistribFHSS_RPOwn +=VariableWork_OnChangeDistribFHSS_RPOwn;

            //VariableWork.OnChangeDistribFHSS_RPOwn += VariableWork_OnChangeDistribFHSS_RPOwn;

            VariableWork.aWPtoBearingDSPprotocolNew.IsConnected += aWPtoBearingDSPprotocolNew_IsConnected;

            VariableWork.aWPtoBearingDSPprotocolNew.RadioJamStateUpdate += aWPtoBearingDSPprotocolNew_RadioJamStateUpdate;

            VariableWork.aWPtoBearingDSPprotocolNew.FhssRadioJamUpdate += aWPtoBearingDSPprotocolNew_FhssRadioJamUpdate;

            VariableColor.OnChangeColorPanorama += VariableColor_OnChangeColorPanorama;

            VariableCommon.OnChangeCommonLanguage += VariableCommon_OnChangeCommonLanguage;

            USR_DLL.FunctionsUser.RequestActiveBandIndexSendEvent += FunctionsUser_RequestActiveBandIndexSendEvent;

            VariableWork.RecBRSEvent += VariableWork_RecBRSEvent;

            VariableWork.RecFtoRJEvent += VariableWork_RecFtoRJEvent;

            VariableDynamic.VariableWork.OnChangeCheckIridiumInmarsat += new VariableWork.ChangeCheckIridiumInmarsatEventHandler(VariableWork_OnChangeCheckIridiumInmarsat);
        }

       
       



        bool recolor = true;
        private void VariableColor_OnChangeColorPanorama()
        {
            if (recolor == true)
            {
                recolorPanoramaBackground(ColorFromSharpString(variableColor.BackGround));
                recolorTrashhold(ColorFromSharpString(variableColor.Threshold));
                recolorCursor(ColorFromSharpString(variableColor.Cross));
                recolorGrid(ColorFromSharpString(variableColor.Grid));
                recolorAmplitudePanorama(ColorFromSharpString(variableColor.GraphFA));
                recolorBearingPanorama(ColorFromSharpString(variableColor.GraphFD));
                //recolorTimePanorama(ColorFromSharpString(variableColor.GraphFT0), 0);
                //recolorTimePanorama(ColorFromSharpString(variableColor.GraphFT1), 1);
                recolorBackColor(ColorFromSharpString(variableColor.GlobalBackGround));
                recolorText(ColorFromSharpString(variableColor.Label));
            }
        }

        public void ChangePanoramaLanguage(byte Language)
        {
            switch (Language)
            {
                case 0:
                    set_language("rus");
                    break;
                case 1:
                    set_language("eng");
                    break;
                case 2:
                    set_language("az");
                    break;
                default:
                    set_language("rus");
                    //set_language("eng");
                    break;
            }
            UpdateDots();
        }

        private void VariableCommon_OnChangeCommonLanguage()
        {
            VariableCommon variableCommon = new VariableCommon();
            switch (variableCommon.Language)
            {
                case 0:
                    set_language("rus");
                    break;
                case 1:
                    set_language("eng");
                    break;
                case 2:
                    set_language("az");
                    break;
                default:
                    set_language("rus");
                    //set_language("eng");
                    break;
            }
            UpdateDots();
        }

        bool isServConnected = false;
        private void aWPtoBearingDSPprotocolNew_IsConnected(bool isConnected)
        {
            isServConnected = isConnected;
        }

        private Object thisLock = new Object();
        /*private void aWPtoBearingDSPprotocolNew_RadioJamStateUpdate(RadioJamStateUpdateEvent answer)
        {
            if (InvokeRequired)
            {
                BeginInvoke((MethodInvoker)(()
                      => aWPtoBearingDSPprotocolNew_RadioJamStateUpdate(answer)));
                return;
            }
            lock (thisLock)
            {
                if (variableWork.Regime == 3 || variableWork.Regime == 4 || variableWork.Regime == 5)
                    if (RSupp == true)
                    {
                        if (answer.TargetStates.Length > 0)
                        {
                            if (answer.TargetStates[0].ControlState == 0 &&
                                answer.TargetStates[0].EmitState == 0 &&
                                answer.TargetStates[0].RadioJamState == 0)
                            {
                                WaveClearArrows();
                                pictureBox1.Invalidate();
                            }

                            else
                            {
                                List<double> Frequencies = new List<double>();
                                List<double> Frequencies2 = new List<double>();

                                List<double> Amplitudes = new List<double>();
                                List<double> Amplitudes2 = new List<double>();

                                for (int i = 0; i < answer.TargetStates.Length; i++)
                                {
                                    if (answer.TargetStates[i].RadioJamState == 1)
                                    {
                                        Frequencies.Add((double)answer.TargetStates[i].Frequency / 10000.0);
                                        Amplitudes.Add((-1) * (double)answer.TargetStates[i].Amplitude);
                                    }
                                    if (answer.TargetStates[i].RadioJamState == 2)
                                    {
                                        Frequencies2.Add((double)answer.TargetStates[i].Frequency / 10000.0);
                                        Amplitudes2.Add((-1) * (double)answer.TargetStates[i].Amplitude);
                                    }
                                }

                                if (Frequencies.Count > 0 && Amplitudes.Count > 0)
                                {
                                    WaveArrows(Frequencies.ToArray(), Amplitudes.ToArray(), Color.Red);
                                    PaintRSCircles(Frequencies, Color.Red);
                                    Frequencies.Clear();
                                    Amplitudes.Clear();
                                }
                                if (Frequencies2.Count > 0 && Amplitudes2.Count > 0)
                                {
                                    WaveArrows(Frequencies2.ToArray(), Amplitudes2.ToArray(), Color.Pink);
                                    PaintRSCircles(Frequencies2, Color.Pink);
                                    Frequencies2.Clear();
                                    Amplitudes2.Clear();
                                }
                            }
                        }

                    }
            }
        }*/

        //Новая отрисовка стрелок
        //private void aWPtoBearingDSPprotocolNew_RadioJamStateUpdate(RadioJamStateUpdateEvent answer)
        //{
        //    if (InvokeRequired)
        //    {
        //        BeginInvoke((MethodInvoker)(()
        //              => aWPtoBearingDSPprotocolNew_RadioJamStateUpdate(answer)));
        //        return;
        //    }
        //    lock (thisLock)
        //    {
        //        if (variableWork.Regime == 3 || variableWork.Regime == 4 || variableWork.Regime == 5)
        //            if (RSupp == true)
        //            {
        //                if (answer.TargetStates.Length > 0)
        //                {
        //                    if (answer.TargetStates[0].ControlState == 0 &&
        //                        answer.TargetStates[0].EmitState == 0 &&
        //                        answer.TargetStates[0].RadioJamState == 0)
        //                    {
        //                        WaveClearArrows();
        //                        pictureBox1.Invalidate();
        //                    }

        //                    else
        //                    {
        //                        List<double> Frequencies = new List<double>();
        //                        List<double> Frequencies2 = new List<double>();

        //                        List<double> Amplitudes = new List<double>();
        //                        List<double> Amplitudes2 = new List<double>();

        //                        for (int i = 0; i < answer.TargetStates.Length; i++)
        //                        {
        //                            if (answer.TargetStates[i].RadioJamState == 1 || answer.TargetStates[i].RadioJamState == 2)
        //                            {
        //                                if (answer.TargetStates[i].EmitState == 0)
        //                                {
        //                                    Frequencies.Add((double)answer.TargetStates[i].Frequency / 10000.0);
        //                                    Amplitudes.Add((-1) * (double)answer.TargetStates[i].Amplitude);
        //                                }
        //                                if (answer.TargetStates[i].EmitState == 1)
        //                                {
        //                                    Frequencies2.Add((double)answer.TargetStates[i].Frequency / 10000.0);
        //                                    Amplitudes2.Add((-1) * (double)answer.TargetStates[i].Amplitude);
        //                                }
        //                            }
        //                        }

        //                        if (Frequencies.Count > 0 && Amplitudes.Count > 0)
        //                        {
        //                            WaveArrows(Frequencies.ToArray(), Amplitudes.ToArray(), Color.White);
        //                            PaintRSCircles(Frequencies, Color.White);
        //                            Frequencies.Clear();
        //                            Amplitudes.Clear();
        //                        }
        //                        if (Frequencies2.Count > 0 && Amplitudes2.Count > 0)
        //                        {
        //                            WaveArrows(Frequencies2.ToArray(), Amplitudes2.ToArray(), Color.Red);
        //                            PaintRSCircles(Frequencies2, Color.Red);
        //                            Frequencies2.Clear();
        //                            Amplitudes2.Clear();
        //                        }
        //                    }
        //                }

        //            }
        //    }
        //}


        //Экспериментальная отрисовка стрелок
        private void aWPtoBearingDSPprotocolNew_RadioJamStateUpdate(RadioJamStateUpdateEvent answer)
        {
            if (InvokeRequired)
            {
                BeginInvoke((MethodInvoker)(()
                      => aWPtoBearingDSPprotocolNew_RadioJamStateUpdate(answer)));
                return;
            }
            lock (thisLock)
            {
                if (variableWork.Regime == 3 || variableWork.Regime == 4 || variableWork.Regime == 5)
                    if (RSupp == true)
                    {
                        if (answer.TargetStates.Length > 0)
                        {
                            int count = 0;
                            for (int w = 0; w < answer.TargetStates.Length; w++)
                            {
                                if (answer.TargetStates[w].ControlState == 0 &&
                                    answer.TargetStates[w].EmitState == 0 &&
                                    answer.TargetStates[w].RadioJamState == 0)
                                {
                                    count++;
                                }
                            }
                            if (count == answer.TargetStates.Length)
                            {
                                WaveClearArrows();
                                pictureBox1.Invalidate();
                            }

                            else
                            {
                                List<double> Frequencies = new List<double>();
                                List<double> Frequencies2 = new List<double>();

                                List<double> Amplitudes = new List<double>();
                                List<double> Amplitudes2 = new List<double>();

                                for (int i = 0; i < answer.TargetStates.Length; i++)
                                {
                                    if (answer.TargetStates[i].RadioJamState == 1 || answer.TargetStates[i].RadioJamState == 2)
                                    {
                                        if (answer.TargetStates[i].EmitState == 0)
                                        {
                                            Frequencies.Add((double)answer.TargetStates[i].Frequency / 10000.0);
                                            Amplitudes.Add((-1) * (double)answer.TargetStates[i].Amplitude);
                                        }
                                        if (answer.TargetStates[i].EmitState == 1)
                                        {
                                            Frequencies2.Add((double)answer.TargetStates[i].Frequency / 10000.0);
                                            Amplitudes2.Add((-1) * (double)answer.TargetStates[i].Amplitude);
                                        }
                                    }
                                }

                                if (Frequencies.Count > 0 && Amplitudes.Count > 0)
                                {
                                    WaveArrows(Frequencies.ToArray(), Amplitudes.ToArray(), Color.White);
                                    PaintRSCircles(Frequencies, Color.White);
                                    Frequencies.Clear();
                                    Amplitudes.Clear();
                                }
                                if (Frequencies2.Count > 0 && Amplitudes2.Count > 0)
                                {
                                    WaveArrows(Frequencies2.ToArray(), Amplitudes2.ToArray(), Color.Red);
                                    PaintRSCircles(Frequencies2, Color.Red);
                                    Frequencies2.Clear();
                                    Amplitudes2.Clear();
                                }
                            }
                        }

                    }
            }
        }

        private void aWPtoBearingDSPprotocolNew_FhssRadioJamUpdate(FhssRadioJamUpdateEvent answer)
        {
            if (InvokeRequired)
            {
                BeginInvoke((MethodInvoker)(()
                      => aWPtoBearingDSPprotocolNew_FhssRadioJamUpdate(answer)));
                return;
            }
            lock (thisLock)
            {
                if (variableWork.Regime == 6)
                    if (RSupp == true)
                    {
                        WaveClearArrows();
                        pictureBox1.Invalidate();

                        List<double> Frequencies = new List<double>();
                        List<double> Amplitudes = new List<double>();

                        for (int i = 0; i < answer.JammedFrequenciesCount; i++)
                        {
                            Frequencies.Add((double)answer.Frequencies[i] / 10000.0);
                            //Amplitudes.Add((-1) * (double)answer.JammingStates[i].Amplitude);
                            Amplitudes.Add((-1) * 50);
                        }

                        if (Frequencies.Count > 0 && Amplitudes.Count > 0)
                        {
                            WaveLines(Frequencies.ToArray(), Amplitudes.ToArray(), Color.Red);
                            PaintRSCircles(Frequencies, Color.Red);
                            Frequencies.Clear();
                            Amplitudes.Clear();
                        }
                    }
            }
        }

        private void UpdateRegime()
        {
            Console.WriteLine("Panorama: Regime=" + variableWork.Regime);
            switch (variableWork.Regime)
            {
                case 0:
                    PreparationSet();
                    break;
                case 1:
                    RadioProspectingWithoutDFSet();
                    break;
                case 2:
                    RadioProspectingWithDFSet();
                    break;
                case 3:
                case 4:
                case 5:
                case 6:
                    RadioSuppression();
                    break;
                default:
                    break;
            }
        }



        private void LoadConnectionSettings()
        {
            //Console.WriteLine("Panorama IP=" + variableconnection.DirectionFinderAddress);
            //Console.WriteLine("Panorama Port=" + variableconnection.DirectionFinderPort);
            Console.WriteLine("PanoramaSpecrumRequest=" + variableconnection.PanoramaSpecrumRequest);

            serverIPAdress = variableconnection.DirectionFinderAddress;
            serverPort = variableconnection.DirectionFinderPort;

        }


        //Чтение INI-файла
        private void LoadINI()
        {
            // чтение из ini-файла
            // добавить обработку исключений - на ini файл?            
            try
            {
                //INIManager iniManager = new INIManager(Application.StartupPath + "\\Settings.ini");
                INIManager iniManager = new INIManager("E:\\work\\Try_Projects\\PanoramaLibrary\\PanoramaLibrary\\bin\\Debug\\Settings.ini");
                serverIPAdress = iniManager.GetPrivateString("WinAp2", "ServerIPAdress");
                serverPort = Convert.ToInt32(iniManager.GetPrivateString("WinAp2", "ServerPort"));

            }
            catch (Exception)
            {
                Console.WriteLine("WinAp2: File Settings.ini not found!");
                //MessageBox.Show("File Settings.ini not found!", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);                
                serverIPAdress = "127.0.0.1";
                serverPort = 1285;
            }
        }
        private void SaveINI()
        {
            // запись в ini-файл
            INIManager iniManager = new INIManager(Application.StartupPath + "\\Settings.ini");

            iniManager.WritePrivateString("WinAp2", "ServerIPAdress", serverIPAdress);
            iniManager.WritePrivateString("WinAp2", "ServerPort", serverPort.ToString());
        }

        //Установка соединения
        private async void FirstConnect()
        {
            await dsp.ConnectToBearingDSP(serverIPAdress, serverPort);
            InitStandartFilters();
        }

        private async void Connect()
        {
            await dsp.ConnectToBearingDSP(serverIPAdress, serverPort);
        }

        //Завершение соединения
        private void Disconnect()
        {
            preparationflag = true;

            RPwoDF = false;
            RPwDF = false;

            intensityTimer.Enabled = false;

            RSupp = false;

            //dsp.DisconnectFromBearingDSP();
            VariableWork.aWPtoBearingDSPprotocolNew.DisconnectFromBearingDSP();
        }

        //Запрос спектра
        private async void SpectrumRequest()
        {
            GetSpectrumResponse answer = new GetSpectrumResponse();

            while (!preparationflag)
            {
                //if (radioButton1.Checked || radioButton2.Checked)
                if (RPwoDF || RPwDF)
                {
                    bool allisok = true;
                    double Coefficient = 10.0;

                    double requestMinFrequency = GlobalRangeMin;
                    double requestMaxFrequency = GlobalRangeMax;

                    try
                    {
                        if (waveformPlot1.XAxis.Range.Minimum > GlobalRangeMin)
                            requestMinFrequency = waveformPlot1.XAxis.Range.Minimum;
                        if (waveformPlot1.XAxis.Range.Maximum < GlobalRangeMax)
                            requestMaxFrequency = waveformPlot1.XAxis.Range.Maximum;

                        //answer = await dsp.GetSpectrum(requestMinFrequency, requestMaxFrequency, Coefficient);
                        answer = await VariableWork.aWPtoBearingDSPprotocolNew.GetSpectrum(requestMinFrequency, requestMaxFrequency, Coefficient);

                    }
                    catch (Exception)
                    {
                        if (preparationflag == false)
                        {
                            allisok = false;
                            //BeginConnectTask = new Task(BeginConnect);
                            //BeginConnectTask.Start();
                            //BeginConnectTask.Wait();
                        }
                    }

                    if (allisok)
                    {
                        if (answer != null)
                            SpectrumPainted(answer, requestMinFrequency, requestMaxFrequency, Coefficient);
                        //await Task.Delay(100);
                        //await Task.Delay(50);
                        await Task.Delay(variableconnection.PanoramaSpecrumRequest);
                    }

                }
            }

        }

        //Запрос спектра 2
        private async void SpectrumRequest2()
        {
            GetSpectrumResponse answer = new GetSpectrumResponse();

            while (!preparationflag)
            {
                if (isServConnected == true)
                {
                    if (RPwoDF || RPwDF)
                    {
                        double Coefficient = 5.0;

                        double requestMinFrequency = GlobalRangeMin;
                        double requestMaxFrequency = GlobalRangeMax;

                        try
                        {
                            if (waveformPlot1.XAxis.Range.Minimum > GlobalRangeMin)
                                requestMinFrequency = waveformPlot1.XAxis.Range.Minimum;
                            if (waveformPlot1.XAxis.Range.Maximum < GlobalRangeMax)
                                requestMaxFrequency = waveformPlot1.XAxis.Range.Maximum;

                            //Coefficient = 5.0 + (((requestMaxFrequency - requestMinFrequency) - 30) / 2970.0) * 5.0;
                            Coefficient = 5.0 + (((requestMaxFrequency - requestMinFrequency) - GlobalBandWidthMHz) / ((GlobalRangeMax - GlobalRangeMin) - GlobalBandWidthMHz)) * 5.0;

                            //Обратить внимание на IF!!!
                            if ((requestMaxFrequency - requestMinFrequency) >= 30)
                            {
                                //answer = await VariableWork.aWPtoBearingDSPprotocolNew.GetSpectrum(requestMinFrequency, requestMaxFrequency, Coefficient);
                                //answer = await VariableWork.aWPtoBearingDSPprotocolNew.GetSpectrum(requestMinFrequency, requestMaxFrequency, 2048);

                                if (waveformGraph1.Width > 0)
                                {
                                    if (variableWork.Regime != 6)
                                        answer = await VariableWork.aWPtoBearingDSPprotocolNew.GetSpectrum(requestMinFrequency, requestMaxFrequency, waveformGraph1.Width * 2);
                                    else
                                        answer.Spectrum = new byte[waveformGraph1.Width * 2];
                                }
                            }
                            else
                            {
                                if (waveformGraph1.Width > 0)
                                {
                                    if (variableWork.Regime != 6)
                                        answer = await VariableWork.aWPtoBearingDSPprotocolNew.GetSpectrum(requestMinFrequency, requestMaxFrequency, waveformGraph1.Width * 2);
                                    else
                                        answer.Spectrum = new byte[waveformGraph1.Width * 2];
                                }
                            }
                        }

                        catch (Exception)
                        {
                            Console.WriteLine("Panorama: Всё в порядке, просто закрываем программу");
                        }

                        if (answer != null)
                            if (answer.Header.ErrorCode == 0)
                            {
                                //Обратить внимание на IF!!!
                                if ((requestMaxFrequency - requestMinFrequency) >= 30)
                                {
                                    //SpectrumPainted(answer, requestMinFrequency, requestMaxFrequency, Coefficient);
                                    if (answer.Spectrum != null)
                                        SpectrumPainted(answer, requestMinFrequency, requestMaxFrequency);
                                }
                                else
                                {
                                    if (answer.Spectrum != null)
                                        SpectrumPainted(answer, requestMinFrequency, requestMaxFrequency);
                                }
                            }

                        await Task.Delay(variableconnection.PanoramaSpecrumRequest);

                    }

                }
                else
                {
                    await Task.Delay(5000);
                }
            }

        }

        //Постоянная спектра
        private const double MinSpectrValue = -130;

        //Отрисовка спектра
        private void SpectrumPainted(GetSpectrumResponse data, double requestMinFrequency, double requestMaxFrequency, double Coefficient)
        {
            double[] dd = new double[data.Spectrum.Count()];
            for (int i = 0; i < data.Spectrum.Count(); i++)
            {
                //dd[i] = (-1) * data.Spectrum[i];
                dd[i] = MinSpectrValue + data.Spectrum[i];
            }
            try
            {
                waveformGraph1.PlotY(dd, requestMinFrequency, GlobalBandWidthMHz / GlobalDotsPerBand * Coefficient);  //30 Мгц
                //waveformGraph1.PlotY(dd, requestMinFrequency, (requestMaxFrequency - requestMinFrequency) / data.Spectrum.Count());  //30 Мгц

                //waveformGraph1.Invalidate();
            }
            catch (Exception e)
            {
                Console.WriteLine("Panorama: Ой что-то не получилсоь спектр отрисовать: " + e.Message);
            }
        }

        double MaxSpectrRangeValue = 0;
        double MinSpectrRangeValue = -120;

        //Отрисовка спектра с количеством точек
        private void SpectrumPainted(GetSpectrumResponse data, double requestMinFrequency, double requestMaxFrequency)
        {
            double[] dd = new double[data.Spectrum.Count()];
            for (int i = 0; i < data.Spectrum.Count(); i++)
            {
                //dd[i] = (-1) * data.Spectrum[i];
                dd[i] = MinSpectrValue + data.Spectrum[i];
            }
            try
            {
                var ddd = dd.Distinct().ToArray();
                ddd = ddd.Where(x => x >-130).OrderByDescending(x => x).ToArray();
                MaxSpectrRangeValue = ddd.Max<double>();
                MinSpectrRangeValue = ddd.Min<double>();

                waveformGraph1.PlotY(dd, requestMinFrequency, (requestMaxFrequency - requestMinFrequency) / data.Spectrum.Count());  //30 Мгц
            }
            catch (Exception e)
            {
                Console.WriteLine("Panorama: Ой что-то не получилсоь спектр отрисовать: " + e.Message);
            }
        }

        //Запрос скорости обзора по таймеру
        private async void ScanSpeedTimer_Tick(object sender, EventArgs e)
        {
            var answer = await VariableWork.aWPtoBearingDSPprotocolNew.GetScanSpeed();
            if (answer != null)
                if (answer.Header.ErrorCode == 0)
                    if (answer.ScanSpeed != 0)
                    {

                        label11.Text = String.Format("{0} = {1} {2}{3}{4}", "V", (answer.ScanSpeed / 1000f).ToString("F1"), GHz, "/", second);
                        //UpdateLabels();
                        Rectangle rectangle = waveformGraph1.PlotAreaBounds;
                        label11.Location = new Point(waveformGraph1.Location.X + rectangle.X + rectangle.Width - label11.Width, waveformGraph1.Location.Y + rectangle.Y + rectangle.Height - label11.Height - 1);

                    }
        }

        bool ScanSpeedFlag = false;

        //Таск скорости обзора
        private async void ScanSpeed()
        {
            while (ScanSpeedFlag == true)
            {
                if (isServConnected == true)
                {
                    var answer = await VariableWork.aWPtoBearingDSPprotocolNew.GetScanSpeed();
                    if (answer != null)
                        if (answer.Header.ErrorCode == 0)
                            if (answer.ScanSpeed != 0)
                            {
                                if (InvokeRequired)
                                {
                                    Invoke((MethodInvoker)(() =>
                                    {
                                        label11.Text = String.Format("{0} = {1} {2}{3}{4}", "V", (answer.ScanSpeed / 1000f).ToString("F1"), GHz, "/", second);
                                        Rectangle rectangle = waveformGraph1.PlotAreaBounds;
                                        label11.Location = new Point(waveformGraph1.Location.X + rectangle.X + rectangle.Width - label11.Width, waveformGraph1.Location.Y + rectangle.Y + rectangle.Height - label11.Height - 1);
                                    }
                                        ));
                                }
                                else
                                {
                                    label11.Text = String.Format("{0} = {1} {2}{3}{4}", "V", (answer.ScanSpeed / 1000f).ToString("F1"), GHz, "/", second);
                                    Rectangle rectangle = waveformGraph1.PlotAreaBounds;
                                    label11.Location = new Point(waveformGraph1.Location.X + rectangle.X + rectangle.Width - label11.Width, waveformGraph1.Location.Y + rectangle.Y + rectangle.Height - label11.Height - 1);
                                }
                            }
                }
                await Task.Delay(1000);
            }
        }

        //Таск скорости обзора
        private async void ScanSpeed2()
        {
            while (ScanSpeedFlag == true)
            {
                double ss = 2.0;
                if (variableWork.Regime == 1)
                {
                    Random random = new Random();
                    double d = random.NextDouble();
                    ss = 10.0 + d;
                }
                if (variableWork.Regime == 2)
                {
                    Random random = new Random();
                    double d = random.NextDouble();
                    if (d >= 0 && d < 1 / 3d) ss = 2.1;
                    if (d >= 1 / 3d && d < 2 / 3d) ss = 2.2;
                    if (d >= 2 / 3d && d <= 1d) ss = 2.3;
                }
                if (InvokeRequired)
                {
                    Invoke((MethodInvoker)(() =>
                    {
                        label11.Text = String.Format("{0} = {1} {2}{3}{4}", "V", (ss).ToString("F1"), GHz, "/", second);
                        Rectangle rectangle = waveformGraph1.PlotAreaBounds;
                        label11.Location = new Point(waveformGraph1.Location.X + rectangle.X + rectangle.Width - label11.Width, waveformGraph1.Location.Y + rectangle.Y + rectangle.Height - label11.Height - 1);
                    }
                        ));
                }
                else
                {
                    label11.Text = String.Format("{0} = {1} {2}{3}{4}", "V", (ss).ToString("F1"), GHz, "/", second);
                    Rectangle rectangle = waveformGraph1.PlotAreaBounds;
                    label11.Location = new Point(waveformGraph1.Location.X + rectangle.X + rectangle.Width - label11.Width, waveformGraph1.Location.Y + rectangle.Y + rectangle.Height - label11.Height - 1);
                }
                await Task.Delay(1000);
            }
        }

        double[] MinFrequencies;
        double[] MaxFrequencies;
        double[] MinAngles;
        double[] MaxAngles;

        Color CurrColor = Color.Blue;

        //Событие изменение сектора variableWork
        private void UpdateRangeSector()
        {
            if (variableWork.Regime == 0 || variableWork.Regime == 1 || variableWork.Regime == 2)
            {
                CurrColor = Color.Blue;

                RangeSector[] RSs = variableWork.RangeSectorReconOwn;

                MinFrequencies = new double[RSs.Count()];
                MaxFrequencies = new double[RSs.Count()];
                MinAngles = new double[RSs.Count()];
                MaxAngles = new double[RSs.Count()];

                for (int i = 0; i < RSs.Count(); i++)
                {
                    MinFrequencies[i] = RSs[i].StartFrequency / 10000.0;
                    MaxFrequencies[i] = RSs[i].EndFrequency / 10000.0;
                    MinAngles[i] = RSs[i].StartDirection / 10.0;
                    MaxAngles[i] = RSs[i].EndDirection / 10.0;
                }

                pictureBox1.Invalidate();
            }

        }

        //Событие изменения ИРИ ФРЧ на РП
        public void VariableWork_OnChangeSupprFWS_Own()
        {
            if (variableWork.Regime == 3 || variableWork.Regime == 4 || variableWork.Regime == 5)
            {
                Protocols.FRSJammingSetting[] fRSJammingSettings = new Protocols.FRSJammingSetting[variableWork.SupprFWS_Own.Length];
                for (int i = 0; i < variableWork.SupprFWS_Own.Length; i++)
                {
                    fRSJammingSettings[i].Frequency = variableWork.SupprFWS_Own[i].iFreq; // ?
                }
                SetSectorsRS(fRSJammingSettings);
            }
        }

        //Событие изменения ИРИ ППРЧ на РП
        public  void VariableWork_OnChangeDistribFHSS_RPOwn()
        {
            if (variableWork.Regime == 6)
            {
                int N = variableWork.DistribFHSS_RPOwn.Length;
                Protocols.FhssJammingSetting[] fhssJammingSettings = new Protocols.FhssJammingSetting[N];
                for (int i = 0; i < N; i++)
                {
                    fhssJammingSettings[i].StartFrequency = variableWork.DistribFHSS_RPOwn[i].iFreqMin;
                    fhssJammingSettings[i].EndFrequency = variableWork.DistribFHSS_RPOwn[i].iFreqMax;
                }
                SetSectorsRS(fhssJammingSettings);
            }
        }

        //внешнее стороннее изменение секторов
        public void SetSectorsAndRanges(RangeSector[] RSs)
        {
            CurrColor = Color.Blue;

            MinFrequencies = new double[RSs.Count()];
            MaxFrequencies = new double[RSs.Count()];
            MinAngles = new double[RSs.Count()];
            MaxAngles = new double[RSs.Count()];

            for (int i = 0; i < RSs.Count(); i++)
            {
                MinFrequencies[i] = RSs[i].StartFrequency / 10000.0;
                MaxFrequencies[i] = RSs[i].EndFrequency / 10000.0;
                MinAngles[i] = RSs[i].StartDirection / 10.0;
                MaxAngles[i] = RSs[i].EndDirection / 10.0;
            }

            pictureBox1.Invalidate();
        }

        private void SetSectorsAndRanges(RangeSector[] RSs, Color color)
        {
            CurrColor = color;

            MinFrequencies = new double[RSs.Count()];
            MaxFrequencies = new double[RSs.Count()];
            MinAngles = new double[RSs.Count()];
            MaxAngles = new double[RSs.Count()];

            for (int i = 0; i < RSs.Count(); i++)
            {
                MinFrequencies[i] = RSs[i].StartFrequency / 10000.0;
                MaxFrequencies[i] = RSs[i].EndFrequency / 10000.0;
                MinAngles[i] = RSs[i].StartDirection / 10.0;
                MaxAngles[i] = RSs[i].EndDirection / 10.0;
            }

            pictureBox1.Invalidate();
        }

        //внешнее стороннее изменение секторов
        public void SetSectorsAndRanges(double[] MinFrequencies, double[] MaxFrequencies, double[] MinAngles, double[] MaxAngles)
        {
            this.MinFrequencies = MinFrequencies;
            this.MaxFrequencies = MaxFrequencies;
            this.MinAngles = MinAngles;
            this.MaxAngles = MaxAngles;

            pictureBox1.Invalidate();
        }

        //внешнее новое красивое стороннее изменение диапазонов при радиоподавлении
        public void SetSectorsRS(FRSJammingSetting[] fRSJammingSettingsFrs)
        {
            int [] checkArr = new int[GlobalNumberofBands + 1];
            List<RangeSector> LRS = new List<RangeSector>();

            for (int i = 0; i < GlobalNumberofBands + 1; i++)
            {
                checkArr[i] = (int)(GlobalRangeMin + i * GlobalBandWidthMHz) * 10000;
            }

            for (int i = 0; i < fRSJammingSettingsFrs.Length; i++ )
            {
                for (int w = 0; w < GlobalNumberofBands; w++)
                {
                    if (fRSJammingSettingsFrs[i].Frequency > checkArr[w] && fRSJammingSettingsFrs[i].Frequency < checkArr[w + 1])
                    {
                        LRS.Add(new RangeSector(checkArr[w], checkArr[w+1],0,3600));
                        break;
                    }
                }
            }

            RangeSector [] RSArr = LRS.ToArray();
            RSArr = RSArr.Distinct().ToArray();

            SetSectorsAndRanges(RSArr, Color.Aqua);

        }

        public void SetSectorsRS(FhssJammingSetting[] fhssJammingSettings)
        {
            int[] checkArr = new int[GlobalNumberofBands + 1];
            List<RangeSector> LRS = new List<RangeSector>();

            for (int i = 0; i < GlobalNumberofBands + 1; i++)
            {
                checkArr[i] = (int)(GlobalRangeMin + i * GlobalBandWidthMHz) * 10000;
            }

            int startIndex = -1;
            int endIndex = -1;

            for (int i = 0; i < fhssJammingSettings.Length; i++)
            {
                for (int w = 0; w < GlobalNumberofBands; w++)
                {
                    if (fhssJammingSettings[i].StartFrequency >= checkArr[w] && fhssJammingSettings[i].StartFrequency < checkArr[w + 1])
                    {
                        startIndex = w;
                        break;
                    }
                }
                for (int w = startIndex; w < GlobalNumberofBands; w++)
                {
                    if (fhssJammingSettings[i].EndFrequency > checkArr[w] && fhssJammingSettings[i].EndFrequency <= checkArr[w + 1])
                    {
                        endIndex = w + 1;
                        break;
                    }
                }

                if (startIndex != -1 && endIndex != -1)
                {
                    LRS.Add(new RangeSector(checkArr[startIndex], checkArr[endIndex], 0, 3600));
                }

            }

            RangeSector[] RSArr = LRS.ToArray();
            //RSArr = RSArr.Distinct().ToArray();

            SetSectorsAndRanges(RSArr, Color.Aqua);
        }

        /*
        private void VariableWork_OnChangeDistribFHSS_RPOwn()
        {
            int N = variableWork.DistribFHSS_RPOwn.Length;
            Protocols.FhssJammingSetting[] fhssJammingSettings = new Protocols.FhssJammingSetting[N];
            for (int i = 0; i < N; i++)
            {
                fhssJammingSettings[i].DeviationCode = variableWork.DistribFHSS_RPOwn[i].bDeviation;
                fhssJammingSettings[i].EndFrequency = variableWork.DistribFHSS_RPOwn[i].iFreqMax;
                fhssJammingSettings[i].Id = variableWork.DistribFHSS_RPOwn[i].iID;
                fhssJammingSettings[i].ManipulationCode = variableWork.DistribFHSS_RPOwn[i].bManipulation;
                fhssJammingSettings[i].ModulationCode = variableWork.DistribFHSS_RPOwn[i].bModulation;
                fhssJammingSettings[i].StartFrequency = variableWork.DistribFHSS_RPOwn[i].iFreqMin;
                fhssJammingSettings[i].Threshold = Convert.ToByte((-1) * variableWork.DistribFHSS_RPOwn[i].sLevel);

                int len = variableWork.DistribFHSS_RPExcludeOwn.Length;
                fhssJammingSettings[i].FixedRadioSourceCount = len;
                fhssJammingSettings[i].FixedRadioSources = new Protocols.FhssFixedRadioSource[len];
                for (int j = 0; j < len; j++)
                {
                    fhssJammingSettings[i].FixedRadioSources[j].Frequency = variableWork.DistribFHSS_RPExcludeOwn[j].iFreqExclude;
                    fhssJammingSettings[i].FixedRadioSources[j].Bandwidth = variableWork.DistribFHSS_RPExcludeOwn[j].iWidthExclude;
                }
            }

            InitFHSSonRS(fhssJammingSettings);
        }
        */


        //Установка начальных параметров для сервера
        private async void InitStandartFilters()
        {
            //установка диапазонов и секторов радиоразведки
            //dsp.SetSectorsAndRanges(GlobalRangeMin, GlobalRangeMax, 0, 360); //Шифр 3
            //dsp.SetSectorsAndRanges(25, 100, 0, 360); //Шифр 3
            //var answer = await dsp.SetSectorsAndRanges(MinFrequencies, MaxFrequencies, MinAngles, MaxAngles); //Шифр 3
            var answer = await dsp.SetSectorsAndRanges(0, 0, 25, 100, 0, 360); //Шифр 3

            //установка фильтов для обнаружения ИРИ
            var answer1 = await dsp.SetFilters((int)porogCursor.YPosition, 0, 0, 0); //Шифр 18

            var answer2 = await dsp.SetMode(1);
            RadioProspectingWithoutDFSet();
        }

        //Установка параметров графиков
        private void InitGraphicsRange()
        {
            waveformPlot1.XAxis.Range = new Range(GlobalRangeMin, GlobalRangeMax);
            xyPlot.XAxis.Range = new Range(GlobalRangeMin, GlobalRangeMax);

            intensityXAxis3.Range = new Range(0, GlobalDotsRange); //real
            intensityXAxis4.Range = new Range(GlobalRangeMin, GlobalRangeMax); //real

        }

        //Инициализация контекст-меню
        private void InitContextMenuStrip()
        {
            contextMenuStrip1.Items[0].Text = tSMI1;
            contextMenuStrip1.Items[1].Text = tSMI2;
            contextMenuStrip1.Items[2].Text = tSMI3;
        }

        //Обязательное
        private void PanoramaControl_Resize(object sender, EventArgs e)
        {
            Panorama_Resize();
        }

        private void Panorama_Resize()
        {
            switch (currindex)
            {
                case 1:

                    int CommonHeight = this.Height;
                    int CommonWidth = this.Width;

                    int RequiredHeight = CommonHeight - waveformGraph1.Location.Y - pictureBox1.Height;
                    int RequiredWidth = CommonWidth;

                    waveformGraph1.Size = new Size(RequiredWidth, RequiredHeight - 5);
                    waveformGraph1.Location = new Point(0, waveformGraph1.Location.Y);
                    UpdateLabels();

                    int pLx = waveformGraph1.PlotAreaBounds.X;
                    int pLy = waveformGraph1.Location.Y + waveformGraph1.Size.Height;
                    pictureBox1.Location = new Point(pLx, pLy);
                    pictureBox1.Width = waveformGraph1.PlotAreaBounds.Width;
                    pictureBox1.Invalidate();

                    break;

                case 2:

                    CommonHeight = this.Height;
                    CommonWidth = this.Width;

                    RequiredHeight = (CommonHeight - waveformGraph1.Location.Y - pictureBox1.Height) / 2;
                    RequiredWidth = CommonWidth;

                    waveformGraph1.Size = new Size(RequiredWidth, RequiredHeight - 5);
                    waveformGraph1.Location = new Point(0, waveformGraph1.Location.Y);
                    UpdateLabels();

                    pLx = waveformGraph1.PlotAreaBounds.X;
                    pLy = waveformGraph1.Location.Y + waveformGraph1.Size.Height;
                    pictureBox1.Location = new Point(pLx, pLy);
                    pictureBox1.Width = waveformGraph1.PlotAreaBounds.Width;
                    pictureBox1.Invalidate();

                    xyDataScatterGraph.Location = new Point(0, pictureBox1.Location.Y + pictureBox1.Height);
                    xyDataScatterGraph.Size = new Size(RequiredWidth, RequiredHeight - 5);

                    xAxis1.Range = xAxes.Range;
                    Rectangle rect1 = waveformGraph1.PlotAreaBounds;
                    Rectangle rect2 = xyDataScatterGraph.PlotAreaBounds;

                    int DSizeX = rect1.Width - rect2.Width;
                    xyDataScatterGraph.Size = new Size(xyDataScatterGraph.Size.Width + DSizeX, xyDataScatterGraph.Size.Height);
                    xyDataScatterGraph.Location = new Point(xyDataScatterGraph.Location.X - DSizeX, xyDataScatterGraph.Location.Y);

                    break;

                case 3:

                    CommonHeight = this.Height;
                    CommonWidth = this.Width;

                    RequiredHeight = (CommonHeight - waveformGraph1.Location.Y - pictureBox1.Height) / 2;
                    RequiredWidth = CommonWidth;

                    waveformGraph1.Size = new Size(RequiredWidth, RequiredHeight - 5);
                    waveformGraph1.Location = new Point(0, waveformGraph1.Location.Y);
                    UpdateLabels();

                    pLx = waveformGraph1.PlotAreaBounds.X;
                    pLy = waveformGraph1.Location.Y + waveformGraph1.Size.Height;
                    pictureBox1.Location = new Point(pLx, pLy);
                    pictureBox1.Width = waveformGraph1.PlotAreaBounds.Width;
                    pictureBox1.Invalidate();

                    //groupBox1.Location = new Point(0, pictureBox1.Location.Y + pictureBox1.Height);
                    scatterGraph2.Location = new Point(pLx, pictureBox1.Location.Y + pictureBox1.Height + 5);

                    //groupBox1.Size = new Size(RequiredWidth, RequiredHeight - 5);
                    int SquareRequiredHeight = RequiredHeight - 5;
                    int SquareRequiredWidth = RequiredWidth;

                    int SquareRequiredSize = Math.Min(RequiredHeight, RequiredWidth);
                    //SquareRequiredSize = SquareRequiredSize - 11;

                    //scatterGraph2.Location = new Point(groupBox1.Width - SquareRequiredSize - 2, RequiredHeight - SquareRequiredSize - 3);
                    scatterGraph2.Size = new Size(SquareRequiredSize, SquareRequiredSize);

                    //int maxwidth = Math.Max(Math.Max(Flabel.Width, Qlabel.Width), Math.Max(SDlabel.Width, Plabel.Width));

                    Flabel.Location = new Point(scatterGraph2.Location.X + scatterGraph2.Size.Width + 5, scatterGraph2.Location.Y);
                    //Qlabel.Location = new Point(Flabel.Location.X, Flabel.Location.Y + 20);
                    //SDlabel.Location = new Point(Flabel.Location.X, Flabel.Location.Y + 40);
                    //Plabel.Location = new Point(Flabel.Location.X, Flabel.Location.Y + 60);

                    //CofAvPh.Location = new Point(Flabel.Location.X, Flabel.Location.Y + 80);
                    //CofAvPl.Location = new Point(Flabel.Location.X, Flabel.Location.Y + 100);

                    Qlabel.Location = new Point(Flabel.Location.X, Flabel.Location.Y + 25);
                    SDlabel.Location = new Point(Flabel.Location.X, Flabel.Location.Y + 50);
                    Plabel.Location = new Point(Flabel.Location.X, Flabel.Location.Y + 75);

                    CofAvPh.Location = new Point(Flabel.Location.X, Flabel.Location.Y + 100);
                    CofAvPl.Location = new Point(Flabel.Location.X, Flabel.Location.Y + 125);


                    UpdateFQSDPLabels();
                    UpdateFQSDPPhPlLocations();




                    //InitAnnotations2();
                    InitAnnotations2(150.0);
                    //InitAnnotations2(9.0,150.0);

                    //FirstInitScatterGraph2();
                    FirstInitScatterGraph2(10.0);
                    //SecondInitScatterGraph2();
                    SecondInitScatterGraph2(0.55);

                    break;

                case 4:

                    CommonHeight = this.Height;
                    CommonWidth = this.Width;

                    RequiredHeight = CommonHeight - scatterGraph1.Location.Y;
                    RequiredWidth = CommonWidth - groupBox2.Width;

                    int RequiredSize = Math.Min(RequiredHeight, RequiredWidth);

                    if (flagRoundPaint)
                    {
                        InitRoundPeleng();
                        flagRoundPaint = false;
                    }

                    scatterGraph1.Size = new Size(RequiredSize, RequiredSize);
                    InitAnnotations();

                    break;
                case 5:

                    if (intensityGraph1.ColorScales[0].Visible == false)
                    {
                        CommonHeight = this.Height;
                        CommonWidth = this.Width;

                        RequiredHeight = (CommonHeight - waveformGraph1.Location.Y - pictureBox1.Height) / 2;
                        RequiredWidth = CommonWidth;

                        waveformGraph1.Size = new Size(RequiredWidth, RequiredHeight - 5);
                        waveformGraph1.Location = new Point(0, waveformGraph1.Location.Y);
                        UpdateLabels(); //оригинальный вариант, может и не нужен

                        pLx = waveformGraph1.PlotAreaBounds.X;
                        pLy = waveformGraph1.Location.Y + waveformGraph1.Size.Height;
                        pictureBox1.Location = new Point(pLx, pLy);
                        pictureBox1.Width = waveformGraph1.PlotAreaBounds.Width;
                        pictureBox1.Invalidate();

                        intensityGraph1.Location = new Point(0, pictureBox1.Location.Y + pictureBox1.Height);
                        intensityGraph1.Size = new Size(RequiredWidth, RequiredHeight - 5);

                        intensityXAxis4.Range = xAxes.Range;
                        double otmin = (intensityXAxis4.Range.Minimum - GlobalRangeMin) / GlobalBandWidthMHz * GlobalDotsPerBand;
                        double otmax = (intensityXAxis4.Range.Maximum - GlobalRangeMin) / GlobalBandWidthMHz * GlobalDotsPerBand;

                        intensityXAxis3.Range = new Range(otmin, otmax);
                        rect1 = waveformGraph1.PlotAreaBounds;

                        Rectangle rect3 = intensityGraph1.PlotAreaBounds;

                        DSizeX = rect1.Width - rect3.Width;
                        intensityGraph1.Size = new Size(intensityGraph1.Size.Width + DSizeX, intensityGraph1.Size.Height);
                        intensityGraph1.Location = new Point(intensityGraph1.Location.X - DSizeX, intensityGraph1.Location.Y);

                        //UpdateLabels(); // продублировали ещё раз

                        break;
                    }
                    else
                    {
                        CommonHeight = this.Height;
                        CommonWidth = this.Width;

                        RequiredHeight = (CommonHeight - waveformGraph1.Location.Y - pictureBox1.Height) / 2;
                        RequiredWidth = CommonWidth;


                        intensityGraph1.Size = new Size(RequiredWidth, RequiredHeight - 5);
                        intensityGraph1.Location = new Point(0, intensityGraph1.Location.Y);



                        Rectangle rectI = intensityGraph1.PlotAreaBounds;
                        Rectangle rectW = waveformGraph1.PlotAreaBounds;

                        int differenceIX = intensityGraph1.Size.Width - intensityGraph1.PlotAreaBounds.Width;
                        int differenceWX = waveformGraph1.Size.Width - waveformGraph1.PlotAreaBounds.Width;

                        int dif = rectW.X - rectI.X;


                        waveformGraph1.Size = new Size(rectW.X + rectI.Width + (waveformGraph1.Size.Width - rectW.Right), RequiredHeight - 5);
                        waveformGraph1.Location = new Point(intensityGraph1.Location.X - dif / 2, waveformGraph1.Location.Y);
                        intensityGraph1.Location = new Point(dif / 2, intensityGraph1.Location.Y);

                        UpdateLabels(); // оригинальный вариант, может и не нужен

                        // отрисовка пикчер бокса
                        pLx = waveformGraph1.PlotAreaBounds.X;
                        pLy = waveformGraph1.Location.Y + waveformGraph1.Size.Height;
                        pictureBox1.Location = new Point(pLx, pLy);
                        pictureBox1.Width = waveformGraph1.PlotAreaBounds.Width;
                        pictureBox1.Invalidate();

                        intensityGraph1.Location = new Point(intensityGraph1.Location.X, pictureBox1.Location.Y + pictureBox1.Height);

                        UpdateLabels(); // продублировали ещё раз
                        /*
                        intensityGraph1.Location = new Point(0, pictureBox1.Location.Y + pictureBox1.Height);
                        intensityGraph1.Size = new Size(RequiredWidth, RequiredHeight - 5);

                        intensityXAxis4.Range = xAxes.Range;
                        double otmin = (intensityXAxis4.Range.Minimum - GlobalRangeMin) / GlobalBandWidthMHz * GlobalDotsPerBand;
                        double otmax = (intensityXAxis4.Range.Maximum - GlobalRangeMin) / GlobalBandWidthMHz * GlobalDotsPerBand;

                        intensityXAxis3.Range = new Range(otmin, otmax);

                        rect1 = waveformGraph1.PlotAreaBounds;

                        Rectangle rect3 = intensityGraph1.PlotAreaBounds;

                        DSizeX = rect1.Width - rect3.Width;
                        intensityGraph1.Size = new Size(intensityGraph1.Size.Width + DSizeX, intensityGraph1.Size.Height);
                        intensityGraph1.Location = new Point(intensityGraph1.Location.X - DSizeX, intensityGraph1.Location.Y);


                       
                        */

                        break;
                    }

            }
        }

        //Закрытие формы
        private void PanoramaControl_ControlRemoved(object sender, ControlEventArgs e)
        {
            Disconnect();
            //SaveINI();
            //SaveColors();
        }

        //Подготовка
        private void PreparationSet()
        {
            preparationflag = true;

            SpectrumRequestIsWorking = false;
            BearingRequestIsWorking = false;

            RPwoDF = false;
            RPwDF = false;

            //intensityTimer.Enabled = false;

            RSupp = false;
            WaveClearArrows();
            pictureBox1.Invalidate();

            ScanSpeedFlag = false;
            //ScanSpeedTimer.Enabled = false;
            label11.Visible = false;
            UpdateLabels();
        }

        //РР бел Пл
        private void RadioProspectingWithoutDFSet()
        {
            preparationflag = false;
            //VariableWork.aWPtoBearingDSPprotocol.SetMode(1);
            //dsp.SetMode(1);
            RPwoDF = true;
            RPwDF = false;

            RSupp = false;
            WaveClearArrows();
            pictureBox1.Invalidate();

            if (SpectrumRequestIsWorking == false)
            {
                SpectrumRequestIsWorking = true;
                //Task.Run(() => SpectrumRequest());
                Task.Run(() => SpectrumRequest2());
            }

            if (ScanSpeedFlag == false)
            {
                ScanSpeedFlag = true;
                //Task.Run(() => ScanSpeed());
                Task.Run(() => ScanSpeed2());
            }
            //ScanSpeedTimer.Enabled = true;

            //if (currindex != 4)
            //    label11.Visible = true;

            //string GHz = "ГГц";
            //string second = "с";
            label11.Text = String.Format("{0} = {1} {2}{3}{4}", "V", "10,0", GHz, "/", second);
            UpdateLabels();
        }

        //РР с Пл
        private void RadioProspectingWithDFSet()
        {
            preparationflag = false;
            //dsp.SetMode(2);
            RPwoDF = false;
            RPwDF = true;

            RSupp = false;
            WaveClearArrows();
            pictureBox1.Invalidate();

            if (SpectrumRequestIsWorking == false)
            {
                SpectrumRequestIsWorking = true;
                //Task.Run(() => SpectrumRequest());
                Task.Run(() => SpectrumRequest2());
            }

            if (BearingRequestIsWorking == false)
            {
                BearingRequestIsWorking = true;
                //Task.Run(() => BearingRequest());
                //Task.Run(() => BearingRequest2());
                Task.Run(() => BearingRequestNew());
            }

            if (ScanSpeedFlag == false)
            {
                ScanSpeedFlag = true;
                //Task.Run(() => ScanSpeed());
                Task.Run(() => ScanSpeed2());
            }
            //ScanSpeedTimer.Enabled = true;

            //if (currindex != 4)
            //    label11.Visible = true;

            //string GHz = "ГГц";
            //string second = "с";
            label11.Text = String.Format("{0} = {1} {2}{3}{4}", "V", "2,0", GHz, "/", second);
            UpdateLabels();
        }

        //РП
        private void RadioSuppression()
        {
            preparationflag = false;

            RPwoDF = true;
            RPwDF = false;

            RSupp = true;

            if (SpectrumRequestIsWorking == false)
            {
                SpectrumRequestIsWorking = true;
                //Task.Run(() => SpectrumRequest());
                Task.Run(() => SpectrumRequest2());
            }

            ScanSpeedFlag = false;
            //ScanSpeedTimer.Enabled = false;
            label11.Visible = false;
        }

        // функция смены языка
        public void set_language(string lang)
        {
            XmlDocument xDoc = new XmlDocument();
            if (System.IO.File.Exists("XMLTranslation.xml"))
                xDoc.Load("XMLTranslation.xml");
            //if (System.IO.File.Exists("E:\\work\\Try_Projects\\PanoramaLibrary\\PanoramaLibrary\\bin\\Debug\\XMLTranslation.xml"))
            //    xDoc.Load("E:\\work\\Try_Projects\\PanoramaLibrary\\PanoramaLibrary\\bin\\Debug\\XMLTranslation.xml");
            else
            {
                switch (lang)
                {

                    case "rus":
                        MessageBox.Show("Файл XMLTranslation.xml не найден!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        break;
                    case "eng":
                        MessageBox.Show("File XMLTranslation.xml not found!", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        break;
                    case "az":
                        MessageBox.Show("Fayl XMLTranslation.xml tapılmadı!", "Səhv!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        break;
                    case "azlat":
                        //MessageBox.Show("File XMLTranslation.xml not found!", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        break;
                    case "azkir":
                        //MessageBox.Show("File XMLTranslation.xml not found!", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        break;
                }

                return;
            }

            SortedList<string, string> Dictionary = new SortedList<string, string>();

            // получим корневой элемент
            XmlElement xRoot = xDoc.DocumentElement;

            string SectionName = "Panorama_Translation";
            foreach (XmlNode x1Node in xRoot)
            {
                if (x1Node.Name == SectionName)
                {
                    foreach (XmlNode x2Node in x1Node.ChildNodes)
                    {
                        // получаем атрибут ID
                        if (x2Node.Attributes.Count > 0)
                        {

                            XmlNode attr = x2Node.Attributes.GetNamedItem("ID");
                            if (attr != null)
                            {
                                foreach (XmlNode childnode in x2Node.ChildNodes)
                                {
                                    // если узел - lang
                                    if (childnode.Name == lang)
                                    {
                                        Dictionary.Add(attr.Value, childnode.InnerText);
                                    }

                                }
                            }
                        }
                    }
                }
            }

            //renameAllToolboxes(Dictionary);
            renameAllControls(Dictionary);
            //xDoc.Save();

        }

        //переименование всех компонент при смене языка
        private void renameAllToolboxes(SortedList<string, string> Dictionary)
        {

            toolStripButton1.Text = Dictionary[toolStripButton1.Name];
            toolStripButton2.Text = Dictionary[toolStripButton2.Name];
            toolStripButton3.Text = Dictionary[toolStripButton3.Name];
            toolStripButton4.Text = Dictionary[toolStripButton4.Name];
            toolStripButton5.Text = Dictionary[toolStripButton5.Name];


            toolStripButton1.ToolTipText = Dictionary["toolStripButton1.ToolTipText"];
            toolStripButton2.ToolTipText = Dictionary["toolStripButton2.ToolTipText"];
            toolStripButton3.ToolTipText = Dictionary["toolStripButton3.ToolTipText"];
            toolStripButton4.ToolTipText = Dictionary["toolStripButton4.ToolTipText"];
            toolStripButton5.ToolTipText = Dictionary["toolStripButton5.ToolTipText"];



            buttonExDF.Text = Dictionary[buttonExDF.Name];



            groupBox2.Text = Dictionary[groupBox2.Name];

        }

        //переменные для текста контролов
        string freq = "Частота";
        string MHz = "МГц";
        string level = "Уровень";
        string dB = "дБ";
        string GHz = "ГГц";
        string second = "с";

        string bearing = "Пеленг";

        string flabel = "Частота, МГц";
        string qlabel = "Усредненный пеленг,°";
        string sdlabel = "СКО,°";
        string plabel = "Процент отброшенных пеленгов";
        string cofAvPh = "Кол-во усреднений фаз";
        string cofAvPl = "Кол-во усреднений пеленгов";

        string tSMI1 = "Добавить в запрещенные частоты";
        string tSMI2 = "Добавить в известные частоты";
        string tSMI3 = "Добавить в важные частоты";

        //переименование всех контролов при смене языка
        private void renameAllControls(SortedList<string, string> Dictionary)
        {
            foreach (Control control in this.Controls)
            {
                if (control is Button)
                {
                    if (Dictionary.ContainsKey(control.Name))
                        control.Text = Dictionary[control.Name];

                    if (Dictionary.ContainsKey(control.Name + ".ToolTipText"))
                        toolTip1.SetToolTip(control, Dictionary[control.Name + ".ToolTipText"]);
                }
                if (control is Label)
                {
                    if (Dictionary.ContainsKey(control.Name))
                        control.Text = Dictionary[control.Name];

                    if (Dictionary.ContainsKey(control.Name + ".ToolTipText"))
                        toolTip1.SetToolTip(control, Dictionary[control.Name + ".ToolTipText"]);
                }
                if (control is CheckBox)
                {
                    if (Dictionary.ContainsKey(control.Name))
                        control.Text = Dictionary[control.Name];

                    if (Dictionary.ContainsKey(control.Name + ".ToolTipText"))
                        toolTip1.SetToolTip(control, Dictionary[control.Name + ".ToolTipText"]);
                }
            }

            for (int i = 0; i < toolStrip1.Items.Count; i++)
            {
                if (Dictionary.ContainsKey(toolStrip1.Items[i].Name))
                    toolStrip1.Items[i].Text = Dictionary[toolStrip1.Items[i].Name];

                if (Dictionary.ContainsKey(toolStrip1.Items[i].Name + ".ToolTipText"))
                    toolStrip1.Items[i].ToolTipText = Dictionary[toolStrip1.Items[i].Name + ".ToolTipText"];
            }

            if (Dictionary.ContainsKey("wX"))
                waveformGraph1.XAxes[0].Caption = Dictionary["wX"];
            if (Dictionary.ContainsKey("wY"))
                waveformGraph1.YAxes[0].Caption = Dictionary["wY"];

            if (Dictionary.ContainsKey("dsX"))
                xyDataScatterGraph.XAxes[0].Caption = Dictionary["dsX"];
            if (Dictionary.ContainsKey("dsY"))
                xyDataScatterGraph.YAxes[0].Caption = Dictionary["dsY"];

            if (Dictionary.ContainsKey("iX"))
                intensityGraph1.XAxes[1].Caption = Dictionary["iX"];
            if (Dictionary.ContainsKey("iY"))
                intensityGraph1.YAxes[0].Caption = Dictionary["iY"];


            if (Dictionary.ContainsKey("freq"))
                freq = Dictionary["freq"];
            if (Dictionary.ContainsKey("MHz"))
                MHz = Dictionary["MHz"];
            if (Dictionary.ContainsKey("level"))
                level = Dictionary["level"];
            if (Dictionary.ContainsKey("dB"))
                dB = Dictionary["dB"];
            if (Dictionary.ContainsKey("GHz"))
                GHz = Dictionary["GHz"];
            if (Dictionary.ContainsKey("second"))
                second = Dictionary["second"];

            if (Dictionary.ContainsKey("bearing"))
                bearing = Dictionary["bearing"];

            if (Dictionary.ContainsKey("Flabel"))
                flabel = Dictionary["Flabel"];
            if (Dictionary.ContainsKey("Qlabel"))
                qlabel = Dictionary["Qlabel"];
            if (Dictionary.ContainsKey("SDlabel"))
                sdlabel = Dictionary["SDlabel"];
            if (Dictionary.ContainsKey("Plabel"))
                plabel = Dictionary["Plabel"];
            if (Dictionary.ContainsKey("CofAvPh"))
                cofAvPh = Dictionary["CofAvPh"];
            if (Dictionary.ContainsKey("CofAvPl"))
                cofAvPl = Dictionary["CofAvPl"];

            if (Dictionary.ContainsKey("tSMI1"))
                tSMI1 = Dictionary["tSMI1"];

            if (Dictionary.ContainsKey("tSMI2"))
                tSMI2 = Dictionary["tSMI2"];

            if (Dictionary.ContainsKey("tSMI3"))
                tSMI3 = Dictionary["tSMI3"];

            InitContextMenuStrip();

            ReTranslatePanoramaLabels();

            //Console.Beep();
        }

        private void ReTranslatePanoramaLabels()
        {
            label9.Text = String.Format("{0}: {1} {2}", freq, SpectrxyCursor.XPosition.ToString("F4"), MHz);
            label10.Text = String.Format("{0}: {1} {2}", level, SpectrxyCursor.YPosition.ToString("F1"), dB);

            double r = Math.Sqrt(Math.Pow(roundxyCursor.XPosition, 2) + Math.Pow(roundxyCursor.YPosition, 2));

            double F = r * (GlobalRangeMax - GlobalRangeMin) / 10d;
            double alpha = Math.Atan2(roundxyCursor.YPosition, roundxyCursor.XPosition) / Math.PI * 180;
            double B = 90 - alpha;
            if (B < 0) B = B + 360;

            label94.Text = String.Format("{0}: {1} {2}", freq, F.ToString("F4"), MHz);
            label104.Text = String.Format("{0}: {1}°", bearing, B.ToString("F1"));

            label95.Text = String.Format("{0}: {1} {2}", freq, intensityCursor1.XPosition.ToString("F4"), MHz);
        }

        //1:1
        private void button3_Click(object sender, EventArgs e)
        {
            xAxes.Range = new Range(GlobalRangeMin, GlobalRangeMax);
            yAxes.Range = new Range(GlobalYRangeMin, GlobalYRangeMax);
            xyPlot.XAxis.Range = waveformPlot1.XAxis.Range;

            if (currindex == 2)
                toolStripButton2.PerformClick();

            if (currindex == 3)
                toolStripButton3.PerformClick();

            if (currindex == 5)
                toolStripButton5.PerformClick();
        }

        //1:1 для ЧПлНП
        private void button34_Click(object sender, EventArgs e)
        {
            xAxis2.Range = new Range(-12, 12);
            yAxis2.Range = new Range(-12, 12);

            if (currindex == 4)
                toolStripButton4.PerformClick();
        }

        //Загрузка контрола
        private void PanoramaControl_Load(object sender, EventArgs e)
        {
            pictureBox1.Paint += pictureBox1_Paint;

            //MouseWheel += waveformGraph1_MouseWheel;
        }

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            PictureBox_Draw(e);
        }

        //Отправка активного квадратика через пользовательский класс
        int ActiveBandIndex = -1;
        int FunctionsUser_RequestActiveBandIndexSendEvent()
        {
            return ActiveBandIndex;
        }


        List<int> lint = new List<int>();
        private void PictureBox_Draw(PaintEventArgs e)
        {
            lint.Clear();

            //e.Graphics.Clear(Color.FromName("Control"));

            int height = pictureBox1.Height;
            int width = pictureBox1.Width;

            int paintWidth = width / GlobalNumberofBands;

            int space = width - GlobalNumberofBands * paintWidth;

            int halfspace = space / 2;

            SolidBrush solidBrush = new SolidBrush(Color.Black);
            Pen pen = new Pen(solidBrush);

            //Отрисовка всех квадратиков
            for (int i = 0; i < GlobalNumberofBands; i++)
            {
                e.Graphics.DrawRectangle(pen, halfspace + 0 + i * (paintWidth), 0, paintWidth, height - 1);
                lint.Add(halfspace + 0 + i * (paintWidth));
            }


            //Отрисовка исходного рабочего диапазона
            if (MinFrequencies != null || MaxFrequencies != null)
            //if (MinFrequencies != null && MaxFrequencies != null) // обратить внимание
            {
                int count = Math.Min(MinFrequencies.Count(), MaxFrequencies.Count());
                for (int i = 0; i < count; i++)
                {
                    //solidBrush = new SolidBrush(Color.Blue);
                    solidBrush = new SolidBrush(CurrColor);
                    Brush brush = solidBrush;
                    int startind = 0;
                    int endind = 0;
                    outBandPictureBoxIndex(MinFrequencies[i], MaxFrequencies[i], out startind, out endind);
                    e.Graphics.FillRectangle(brush, lint[startind], 0, lint[endind] - lint[startind] + paintWidth, pictureBox1.Height - 1);
                }
            }
            //Отрисовка всех квадратиков ещё раз
            for (int i = 0; i < GlobalNumberofBands; i++)
            {
                e.Graphics.DrawRectangle(pen, halfspace + 0 + i * (paintWidth), 0, paintWidth, height - 1);
            }


            //Отрисовка активного квадратика
            if ((ActiveBandIndex != -1) && (ActiveBandIndex == spindex))
            {
                solidBrush = new SolidBrush(Color.Green);
                pen = new Pen(solidBrush);
                e.Graphics.DrawRectangle(pen, lint[ActiveBandIndex], 0, paintWidth, pictureBox1.Height - 1);

            }

            //Отрисовка выбранного диапазона
            solidBrush = new SolidBrush(Color.Red);
            pen = new Pen(solidBrush);
            pen.Width = 3;
            try
            {
                e.Graphics.DrawRectangle(pen, lint[spindex], 0, lint[epindex] - lint[spindex] + paintWidth, pictureBox1.Height - 1);
                if (ChangeActiveRedRectangle != null)
                    ChangeActiveRedRectangle(spindex, epindex);
            }
            catch
            {
            }

        }

        //Отрисовка кружков радиоподавления
        private void PaintRSCircles(List<double> Frequencies, Color color)
        {
            int numberofbands = GlobalNumberofBands;

            Graphics g = pictureBox1.CreateGraphics();

            int heightC = pictureBox1.Height;
            int widthC = pictureBox1.Width;

            int paintWidthC = widthC / numberofbands;

            int spaceC = widthC - numberofbands * paintWidthC;

            int halfspaceC = spaceC / 2;

            int shiftC = heightC / 2 - paintWidthC / 2;

            Brush brushC = new SolidBrush(color);

            for (int i = 0; i < Frequencies.Count; i++)
            {
                int index = BandPictireBoxIndexFromFreq(Frequencies[i]);
                g.FillEllipse(brushC, halfspaceC + 0 + index * (paintWidthC), 0 + shiftC, paintWidthC - 1, paintWidthC - 1);
            }
        }

        private void pictureBox1_MouseClick(object sender, MouseEventArgs e)
        {
            var x = pictureBox1.PointToClient(System.Windows.Forms.Cursor.Position).X;

            if (lint.Count > 0)
                if ((x >= lint[0]) && (x <= lint[lint.Count() - 1] + lint[1] - lint[0]))
                {
                    ActiveBandIndex = GlobalNumberofBands - 1;
                    for (int i = 0; i < lint.Count(); i++)
                    {
                        if (x < lint[i])
                        {
                            ActiveBandIndex = i - 1;
                            break;
                        }
                    }

                    ToolTip t = new ToolTip();
                    t.SetToolTip(pictureBox1, ActiveBandIndex.ToString());

                    double[] rangeArray = new double[GlobalNumberofBands + 1];
                    for (int i = 0; i < rangeArray.Count(); i++)
                    {
                        rangeArray[i] = GlobalRangeMin + GlobalBandWidthMHz * i;

                    }

                    waveformPlot1.XAxis.Range = new Range(rangeArray[ActiveBandIndex], rangeArray[ActiveBandIndex + 1]);

                    //if (ActiveBandIndexSendEvent != null)
                    //    ActiveBandIndexSendEvent(ActiveBandIndex);

                    USR_DLL.FunctionsUser FU = new USR_DLL.FunctionsUser();
                    FU.ReSendActiveBandIndex(ActiveBandIndex);

                    pictureBox1.Invalidate();

                }



        }

        // Частотная панорама
        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            currindex = 1;

            toolStripButton1.Checked = true;
            toolStripButton2.Checked = false;
            toolStripButton3.Checked = false;
            toolStripButton4.Checked = false;
            toolStripButton5.Checked = false;


            button3.Visible = true;
            button34.Visible = false;
            label9.Visible = true;
            label10.Visible = true;

            label94.Visible = false;
            label104.Visible = false;

            label95.Visible = false;

            //if (variableWork.Regime == 1 || variableWork.Regime == 2)
            //    label11.Visible = true;

            labelLeft.Visible = true;
            labelRight.Visible = true;

            waveformGraph1.Visible = true;
            xyDataScatterGraph.Visible = false;

            //groupBox1.Visible = false;
            scGraph2andLabelVisible(false);

            scatterGraph1.Visible = false;
            intensityGraph1.Visible = false;
            groupBox2.Visible = false;

            pictureBox1.Visible = true;

            Panorama_Resize();

            intensityTimer.Enabled = false;

            buttonExDF.Checked = false;
        }

        // Частотно-пеленговая панорама
        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            currindex = 2;

            toolStripButton1.Checked = false;
            toolStripButton2.Checked = true;
            toolStripButton3.Checked = false;
            toolStripButton4.Checked = false;
            toolStripButton5.Checked = false;

            button3.Visible = true;
            button34.Visible = false;
            label9.Visible = true;
            label10.Visible = true;

            label94.Visible = false;
            label104.Visible = false;

            label95.Visible = false;

            //if (variableWork.Regime == 1 || variableWork.Regime == 2)
            //    label11.Visible = true;

            labelLeft.Visible = true;
            labelRight.Visible = true;

            waveformGraph1.Visible = true;
            xyDataScatterGraph.Visible = true;

            //groupBox1.Visible = false;
            scGraph2andLabelVisible(false);

            scatterGraph1.Visible = false;
            intensityGraph1.Visible = false;
            groupBox2.Visible = false;

            pictureBox1.Visible = true;

            Panorama_Resize();

            intensityTimer.Enabled = false;

            buttonExDF.Checked = false;
        }

        //Запрос пеленгов
        private async void BearingRequest()
        {
            GetRadioSourcesResponse answer = new GetRadioSourcesResponse();

            while (!preparationflag)
            {
                //while (radioButton2.Checked)
                while (RPwDF)
                {
                    bool allisok = true;

                    try
                    {
                        //answer = dsp.GetRadioSources();
                        answer = await VariableWork.aWPtoBearingDSPprotocolNew.GetRadioSources();
                    }
                    catch (Exception)
                    {
                        if (preparationflag == false)
                        {
                            allisok = false;
                            /*
                            BeginConnectTask = new Task(BeginConnect);
                            BeginConnectTask.Start();
                            BeginConnectTask.Wait();
                             * */
                        }
                    }

                    if (allisok)
                    {
                        if (answer != null)
                        {
                            BearingPainted(answer);
                            //BearingPaintedNew(answer);
                            RoundBearingPainted(answer);
                            //RoundBearingPaintedNew(answer);
                        }
                        await Task.Delay(100);
                    }

                }
            }
        }

        //Запрос пеленгов 2
        private async void BearingRequest2()
        {
            GetRadioSourcesResponse answer = new GetRadioSourcesResponse();

            while (!preparationflag)
            {
                if (isServConnected == true)
                {
                    if (RPwDF)
                    {
                        answer = await VariableWork.aWPtoBearingDSPprotocolNew.GetRadioSources();
                        if (answer != null)
                        {
                            if (answer.Header.ErrorCode == 0)
                            {
                                //BearingPainted(answer);
                                BearingPaintedNew(answer);
                                //BearingPaintedV3(answer,Color.Orange);
                                //RoundBearingPainted(answer);
                                RoundBearingPaintedNew(answer);
                            }
                        }
                        await Task.Delay(100);
                    }
                }
                else
                {
                    await Task.Delay(5000);
                }
            }
        }

        //Запрос пеленгов новый
        private async void BearingRequestNew()
        {
            GetBearingPanoramaSignalsResponse answer = new GetBearingPanoramaSignalsResponse();

            while (!preparationflag)
            {
                if (isServConnected == true)
                {
                    if (RPwDF)
                    {

                        double bearingRequestMinFrequency = GlobalRangeMin;
                        double bearingRequestMaxFrequency = GlobalRangeMax;

                        if (xAxis1.Range.Minimum > GlobalRangeMin)
                            bearingRequestMinFrequency = xAxis1.Range.Minimum;
                        if (xAxis1.Range.Maximum < GlobalRangeMax)
                            bearingRequestMaxFrequency = xAxis1.Range.Maximum;

                        if (currindex == 2 || currindex == 4)
                            answer = await VariableWork.aWPtoBearingDSPprotocolNew.GetBearingPanoramaSignals(bearingRequestMinFrequency, bearingRequestMaxFrequency);

                        if (answer != null)
                        {
                            if (answer.Header.ErrorCode == 0)
                            {
                                if (currindex == 2)
                                {
                                    ////BearingPaintedNew2(answer); 
                                    //BearingPaintedNew3(answer);
                                    BearingPaintedNew5(answer);
                                }
                                if (currindex == 4)
                                {
                                    //RoundBearingPaintedNew2(answer);
                                    //RoundBearingPaintedNew3(answer);
                                    RoundBearingPaintedNew4(answer);
                                }
                            }
                        }
                        await Task.Delay(100);
                    }
                }
                else
                {
                    await Task.Delay(5000);
                }
            }
        }

        //Отрисовка пеленгов
        private void BearingPainted(GetRadioSourcesResponse data)
        {
            try
            {
                int medialPhase = 5;
                int medialPeleng = 5;

                int Intens = 8 + (1 + 1) * 3;
                Intens = 8 + (medialPhase + medialPeleng) * 3;
                if (Intens > 50) Intens = 50;

                xyPlot.PointColor = Color.FromArgb(Intens, 255, 104, 0);



                int N = data.RadioSources.Count();
                double[] xData = new double[N];
                double[] yData = new double[N];
                for (int i = 0; i < N; i++)
                {
                    xData[i] = data.RadioSources[i].Frequency / 10000.0;
                    yData[i] = data.RadioSources[i].Direction / 10.0;
                }
                xyPlot.PlotXYAppend(xData, yData);
                //xyPlot.PlotXY(xData, yData);
            }
            catch { }
        }
        private void BearingPainted(GetRadioSourcesResponse data, Color color)
        {
            try
            {
                int medialPhase = 5;
                int medialPeleng = 5;

                int Intens = 8 + (1 + 1) * 3;
                Intens = 8 + (medialPhase + medialPeleng) * 3;
                if (Intens > 50) Intens = 50;

                xyPlot.PointColor = Color.FromArgb(Intens, color.R, color.G, color.B);



                int N = data.RadioSources.Count();
                double[] xData = new double[N];
                double[] yData = new double[N];
                for (int i = 0; i < N; i++)
                {
                    xData[i] = data.RadioSources[i].Frequency / 10000.0;
                    yData[i] = data.RadioSources[i].Direction / 10.0;
                }
                xyPlot.PlotXYAppend(xData, yData);
                //xyPlot.PlotXY(xData, yData);
            }
            catch { }
        }

        //Новая отрисовка пеленгов
        private struct BearingPoint
        {
            public double[] xdata;
            public double[] ydata;
            public DateTime datetime;

            public BearingPoint(double[] xData, double[] yData, DateTime dateTime)
            {
                xdata = xData;
                ydata = yData;
                datetime = dateTime;
            }
        };
        List<BearingPoint> BearingPoints = new List<BearingPoint>();
        private void BearingPaintedNew(GetRadioSourcesResponse data)
        {
            try
            {
                int medialPhase = 5;
                int medialPeleng = 5;

                int Intens = 8 + (1 + 1) * 3;
                Intens = 8 + (medialPhase + medialPeleng) * 3;
                if (Intens > 50) Intens = 50;

                xyPlot.PointColor = Color.FromArgb(Intens, 255, 104, 0);
                //xyPlot.PointColor = Color.FromArgb(255, 255, 104, 0);

                int N = data.RadioSources.Count();
                double[] xData = new double[N];
                double[] yData = new double[N];
                for (int i = 0; i < N; i++)
                {
                    xData[i] = data.RadioSources[i].Frequency / 10000.0;
                    yData[i] = data.RadioSources[i].Direction / 10.0;
                }

                /*
                xyPlot.HistoryCapacity = 250;
                xyPlot.PlotXYAppend(xData, yData);
                */


                if (BearingPoints.Count != 0)
                {
                    DateTime dateTime = new DateTime();
                    dateTime = DateTime.Now;
                    BearingPoint temp = new BearingPoint(xData, yData, dateTime);

                    var diff = dateTime - BearingPoints[0].datetime;

                    if (diff.TotalMilliseconds < 3000)
                    {
                        BearingPoints.Add(temp);
                    }
                    else
                    {
                        BearingPoints.RemoveAt(0);
                        BearingPoints.Add(temp);
                    }
                }
                else
                {
                    DateTime dateTime = new DateTime();
                    dateTime = DateTime.Now;
                    BearingPoint temp = new BearingPoint(xData, yData, dateTime);
                    BearingPoints.Add(temp);
                }

                int count = Convert.ToInt32((double)BearingPoints.Count / (255.0 / (double)Intens));
                if (count <= 0) count = 1;

                int historycount = 250;
                int startnum = Convert.ToInt32(((double)BearingPoints.Count / (double)count));

                for (int i = startnum; i < BearingPoints.Count; i = i + count)
                {
                    historycount = historycount + BearingPoints[i].xdata.Count();
                }

                xyPlot.HistoryCapacity = historycount;

                for (int i = startnum; i < BearingPoints.Count; i = i + count)
                {
                    xyPlot.PlotXYAppend(BearingPoints[i].xdata, BearingPoints[i].ydata);
                    //xyPlot.PlotXY(BearingPoints[i].xdata, BearingPoints[i].ydata);
                }
            }
            catch { }
        }
        private void BearingPaintedNew(GetRadioSourcesResponse data, Color color)
        {
            try
            {
                int medialPhase = 5;
                int medialPeleng = 5;

                int Intens = 8 + (1 + 1) * 3;
                Intens = 8 + (medialPhase + medialPeleng) * 3;
                if (Intens > 50) Intens = 50;

                xyPlot.PointColor = Color.FromArgb(Intens, color.R, color.G, color.B);
                //xyPlot.PointColor = Color.FromArgb(255, 255, 104, 0);

                int N = data.RadioSources.Count();
                double[] xData = new double[N];
                double[] yData = new double[N];
                for (int i = 0; i < N; i++)
                {
                    xData[i] = data.RadioSources[i].Frequency / 10000.0;
                    yData[i] = data.RadioSources[i].Direction / 10.0;
                }

                /*
                xyPlot.HistoryCapacity = 250;
                xyPlot.PlotXYAppend(xData, yData);
                */


                if (BearingPoints.Count != 0)
                {
                    DateTime dateTime = new DateTime();
                    dateTime = DateTime.Now;
                    BearingPoint temp = new BearingPoint(xData, yData, dateTime);

                    var diff = dateTime - BearingPoints[0].datetime;

                    if (diff.TotalMilliseconds < 3000)
                    {
                        BearingPoints.Add(temp);
                    }
                    else
                    {
                        BearingPoints.RemoveAt(0);
                        BearingPoints.Add(temp);
                    }
                }
                else
                {
                    DateTime dateTime = new DateTime();
                    dateTime = DateTime.Now;
                    BearingPoint temp = new BearingPoint(xData, yData, dateTime);
                    BearingPoints.Add(temp);
                }

                int count = Convert.ToInt32((double)BearingPoints.Count / (255.0 / (double)Intens));
                if (count <= 0) count = 1;

                int historycount = 250;
                int startnum = Convert.ToInt32(((double)BearingPoints.Count / (double)count));

                for (int i = startnum; i < BearingPoints.Count; i = i + count)
                {
                    historycount = historycount + BearingPoints[i].xdata.Count();
                }

                xyPlot.HistoryCapacity = historycount;

                for (int i = startnum; i < BearingPoints.Count; i = i + count)
                {
                    xyPlot.PlotXYAppend(BearingPoints[i].xdata, BearingPoints[i].ydata);
                    //xyPlot.PlotXY(BearingPoints[i].xdata, BearingPoints[i].ydata);
                }
            }
            catch { }
        }

        //очень новая отрисовка пеленгов
        private void BearingPaintedNew2(GetBearingPanoramaSignalsResponse data)
        {
            try
            {
                int medialPhase = 5;
                int medialPeleng = 5;

                int Intens = 8 + (1 + 1) * 3;
                Intens = 8 + (medialPhase + medialPeleng) * 3;
                if (Intens > 50) Intens = 50;

                xyPlot.PointColor = Color.FromArgb(Intens, xyPlot.PointColor);
                //xyPlot.PointColor = Color.FromArgb(Intens, 255, 104, 0);
                //xyPlot.PointColor = Color.FromArgb(255, 255, 104, 0);

                //int N = data.Signals.Count(); // закоментил, чтобы не было ошибок с новым протоколом
                int N = 0; //чтобы скомпилилось
                double[] xData = new double[N];
                double[] yData = new double[N];
                for (int i = 0; i < N; i++)
                {
                    // xData[i] = data.Signals[i].Frequency / 10000.0; // закоментил, чтобы не было ошибок с новым протоколом
                    //yData[i] = data.Signals[i].Direction / 10.0; // закоментил, чтобы не было ошибок с новым протоколом
                }

                /*
                xyPlot.HistoryCapacity = 250;
                xyPlot.PlotXYAppend(xData, yData);
                */


                if (BearingPoints.Count != 0)
                {
                    DateTime dateTime = new DateTime();
                    dateTime = DateTime.Now;
                    BearingPoint temp = new BearingPoint(xData, yData, dateTime);

                    var diff = dateTime - BearingPoints[0].datetime;

                    if (diff.TotalMilliseconds < 3000)
                    {
                        BearingPoints.Add(temp);
                    }
                    else
                    {
                        BearingPoints.RemoveAt(0);
                        BearingPoints.Add(temp);
                    }
                }
                else
                {
                    DateTime dateTime = new DateTime();
                    dateTime = DateTime.Now;
                    BearingPoint temp = new BearingPoint(xData, yData, dateTime);
                    BearingPoints.Add(temp);
                }

                int count = Convert.ToInt32((double)BearingPoints.Count / (255.0 / (double)Intens));
                if (count <= 0) count = 1;

                int historycount = 250;
                int startnum = Convert.ToInt32(((double)BearingPoints.Count / (double)count));

                for (int i = startnum; i < BearingPoints.Count; i = i + count)
                {
                    historycount = historycount + BearingPoints[i].xdata.Count();
                }

                xyPlot.HistoryCapacity = historycount;

                for (int i = startnum; i < BearingPoints.Count; i = i + count)
                {
                    xyPlot.PlotXYAppend(BearingPoints[i].xdata, BearingPoints[i].ydata);
                    //xyPlot.PlotXY(BearingPoints[i].xdata, BearingPoints[i].ydata);
                }
            }
            catch { }
        }

        //экстра новая отрисовка пеленгов
        private struct BPoint
        {
            public double xdata;
            public double ydata;
            public DateTime datetime;

            public BPoint(double xData, double yData, DateTime dateTime)
            {
                xdata = xData;
                ydata = yData;
                datetime = dateTime;
            }
        };
        List<BPoint> BPointsF = new List<BPoint>();
        private void BearingPaintedNew3(GetBearingPanoramaSignalsResponse data)
        {
            //отрисовка ППРЧ
            try
            {
                int medialPhase = 5;
                int medialPeleng = 5;

                int Intens = 8 + (1 + 1) * 3;
                Intens = 8 + (medialPhase + medialPeleng) * 3;
                if (Intens > 50) Intens = 50;

                xyPlot.PointColor = Color.FromArgb(Intens, xyPlot.PointColor);

                //int N = data.Signals.Count();
                int N = data.ImpulseSignalsCount;

                double[] xData = new double[N];
                double[] yData = new double[N];
                for (int i = 0; i < N; i++)
                {
                    //xData[i] = data.Signals[i].Frequency / 10000.0;
                    //yData[i] = data.Signals[i].Direction / 10.0;
                    xData[i] = data.ImpulseSignals[i].Frequency / 10000.0;
                    yData[i] = data.ImpulseSignals[i].Direction / 10.0;
                }


                if (BearingPoints.Count != 0)
                {
                    DateTime dateTime = new DateTime();
                    dateTime = DateTime.Now;
                    BearingPoint temp = new BearingPoint(xData, yData, dateTime);

                    var diff = dateTime - BearingPoints[0].datetime;

                    if (diff.TotalMilliseconds < 3000)
                    {
                        BearingPoints.Add(temp);
                    }
                    else
                    {
                        BearingPoints.RemoveAt(0);
                        BearingPoints.Add(temp);
                    }
                }
                else
                {
                    if (N != 0)
                    {
                        DateTime dateTime = new DateTime();
                        dateTime = DateTime.Now;
                        BearingPoint temp = new BearingPoint(xData, yData, dateTime);
                        BearingPoints.Add(temp);
                    }
                }

                int count = Convert.ToInt32((double)BearingPoints.Count / (255.0 / (double)Intens));
                if (count <= 0) count = 1;

                int historycount = 250;
                int startnum = Convert.ToInt32(((double)BearingPoints.Count / (double)count));

                for (int i = startnum; i < BearingPoints.Count; i = i + count)
                {
                    historycount = historycount + BearingPoints[i].xdata.Count();
                }

                xyPlot.HistoryCapacity = historycount;

                for (int i = startnum; i < BearingPoints.Count; i = i + count)
                {
                    xyPlot.PlotXYAppend(BearingPoints[i].xdata, BearingPoints[i].ydata);
                    //xyPlot.PlotXY(BearingPoints[i].xdata, BearingPoints[i].ydata);
                }

                /////////////////////////////////////////////////////////////////////////

                //отрисовка ФРЧ
                int Nf = data.FixedSignalsCount;

                double[] xDataf = new double[Nf];
                double[] yDataf = new double[Nf];
                for (int i = 0; i < Nf; i++)
                {
                    xDataf[i] = data.FixedSignals[i].Frequency / 10000.0;
                    yDataf[i] = data.FixedSignals[i].Direction / 10.0;

                    BPointsF.Add(new BPoint(xDataf[i], yDataf[i], DateTime.Now));
                }

                for (int i = 0; i < BPointsF.Count - Nf; i++)
                {
                    DateTime now = DateTime.Now;
                    var diff = now - BPointsF[i].datetime;
                    if (diff.TotalMilliseconds > 3000)
                    {
                        BPointsF.RemoveAt(i);
                    }
                }

                xDataf = new double[BPointsF.Count];
                yDataf = new double[BPointsF.Count];
                for (int i = 0; i < BPointsF.Count; i++)
                {
                    xDataf[i] = BPointsF[i].xdata;
                    yDataf[i] = BPointsF[i].ydata;
                }
                xyPlot.HistoryCapacity = historycount + BPointsF.Count;

                for (int i = 0; i < BPointsF.Count; i++)
                {
                    xyPlot.PlotXYAppend(xDataf, yDataf);
                }

                /////////////////////////////////////////////////////////////////////////////


                //отрисовка ФРЧ
                /*
                double[] xDataf = new double[Nf];
                double[] yDataf = new double[Nf];
                for (int i = 0; i < Nf; i++)
                {
                    xDataf[i] = data.FixedSignals[i].Frequency / 10000.0;
                    yDataf[i] = data.FixedSignals[i].Direction / 10.0;
                }

                for (int i = 0; i < 6; i++)
                {
                    xyPlot.PlotXYAppend(xDataf, yDataf);
                }
                 * */
            }
            catch { }
        }

        //следующая итерация отрисовки пеленгов
        private void BearingPaintedNew4(GetBearingPanoramaSignalsResponse data)
        {
            //отрисовка ППРЧ
            try
            {
                int medialPhase = 5;
                int medialPeleng = 5;

                int Intens = 8 + (1 + 1) * 3;
                Intens = 8 + (medialPhase + medialPeleng) * 3;
                if (Intens > 50) Intens = 50;

                xyPlot.PointColor = Color.FromArgb(Intens, xyPlot.PointColor);

                //int N = data.Signals.Count();
                int N = data.ImpulseSignalsCount;
                int Nf = data.FixedSignalsCount;

                double[] xData = new double[N+Nf];
                double[] yData = new double[N+Nf];
                for (int i = 0; i < N; i++)
                {
                    xData[i] = data.ImpulseSignals[i].Frequency / 10000.0;
                    yData[i] = data.ImpulseSignals[i].Direction / 10.0;
                }
                for (int i = N; i < Nf; i++)
                {
                    xData[i] = data.FixedSignals[i].Frequency / 10000.0;
                    yData[i] = data.FixedSignals[i].Direction / 10.0;
                }

                if (BearingPoints.Count != 0)
                {
                    DateTime dateTime = new DateTime();
                    dateTime = DateTime.Now;
                    BearingPoint temp = new BearingPoint(xData, yData, dateTime);

                    var diff = dateTime - BearingPoints[0].datetime;

                    if (diff.TotalMilliseconds < 3000)
                    {
                        BearingPoints.Add(temp);
                    }
                    else
                    {
                        BearingPoints.RemoveAt(0);
                        BearingPoints.Add(temp);
                    }
                }
                else
                {
                    DateTime dateTime = new DateTime();
                    dateTime = DateTime.Now;
                    BearingPoint temp = new BearingPoint(xData, yData, dateTime);
                    BearingPoints.Add(temp);
                }

                int count = Convert.ToInt32((double)BearingPoints.Count / (255.0 / (double)Intens));
                if (count <= 0) count = 1;

                int historycount = 250;
                int startnum = Convert.ToInt32(((double)BearingPoints.Count / (double)count));

                for (int i = startnum; i < BearingPoints.Count; i = i + count)
                {
                    historycount = historycount + BearingPoints[i].xdata.Count();
                }

                xyPlot.HistoryCapacity = historycount;

                for (int i = startnum; i < BearingPoints.Count; i = i + count)
                {
                    xyPlot.PlotXYAppend(BearingPoints[i].xdata, BearingPoints[i].ydata);
                }
            }
            catch { }
        }

        //актуальная отрисовка пеленгов
        private void BearingPaintedNew5(GetBearingPanoramaSignalsResponse data)
        {
            //отрисовка ППРЧ
            try
            {
                //xyPlot.HistoryCapacity = 100000;
                xyPlot.ClearData();

                int medialPhase = 5;
                int medialPeleng = 5;

                int Intens = 8 + (medialPhase + medialPeleng) * 3;
                if (Intens > 50) Intens = 50;

                xyPlot.PointColor = Color.FromArgb(Intens, xyPlot.PointColor);

                int N = data.ImpulseSignalsCount;

                double[] xData = new double[N];
                double[] yData = new double[N];
                for (int i = 0; i < N; i++)
                {
                    xData[i] = data.ImpulseSignals[i].Frequency / 10000.0;
                    yData[i] = data.ImpulseSignals[i].Direction / 10.0;
                }

                if (BearingPoints.Count != 0)
                {
                    DateTime dateTime = new DateTime();
                    dateTime = DateTime.Now;
                    BearingPoint temp = new BearingPoint(xData, yData, dateTime);

                    var diff = dateTime - BearingPoints[0].datetime;

                    if (diff.TotalMilliseconds < 3000)
                    {
                        BearingPoints.Add(temp);
                    }
                    else
                    {
                        BearingPoints.RemoveAt(0);
                        BearingPoints.Add(temp);
                    }
                }
                else
                {
                    if (N != 0)
                    {
                        DateTime dateTime = new DateTime();
                        dateTime = DateTime.Now;
                        BearingPoint temp = new BearingPoint(xData, yData, dateTime);
                        BearingPoints.Add(temp);
                    }
                }

                int count = Convert.ToInt32((double)BearingPoints.Count / (255.0 / (double)Intens));
                if (count <= 0) count = 1;
               
                int startnum = Convert.ToInt32(((double)BearingPoints.Count / (double)count));

                for (int i = startnum; i < BearingPoints.Count; i = i + count)
                {
                    xyPlot.PlotXYAppend(BearingPoints[i].xdata, BearingPoints[i].ydata);
                }

                //отрисовка ФРЧ
                int Nf = data.FixedSignalsCount;

                double[] xDataf = new double[Nf];
                double[] yDataf = new double[Nf];
                for (int i = 0; i < Nf; i++)
                {
                    xDataf[i] = data.FixedSignals[i].Frequency / 10000.0;
                    yDataf[i] = data.FixedSignals[i].Direction / 10.0;

                    BPointsF.Add(new BPoint(xDataf[i], yDataf[i], DateTime.Now));
                }

                for (int i = 0; i < BPointsF.Count - Nf; i++)
                {
                    DateTime now = DateTime.Now;
                    var diff = now - BPointsF[i].datetime;
                    if (diff.TotalMilliseconds > 3000)
                    {
                        BPointsF.RemoveAt(i);
                    }
                }

                xDataf = new double[BPointsF.Count];
                yDataf = new double[BPointsF.Count];
                for (int i = 0; i < BPointsF.Count; i ++)
                {
                    xDataf[i] = BPointsF[i].xdata;
                    yDataf[i] = BPointsF[i].ydata;
                }

                xyPlot.PlotXYAppend(xDataf, yDataf);
                
                /*
                for (int i = 0; i < BPointsF.Count; i++)
                {
                    xyPlot.PlotXYAppend(xDataf, yDataf);
                }
                */
            }
            catch { }
        }

        private void RoundBearingPainted(GetRadioSourcesResponse data)
        {
            try
            {
                int medialPhase = 5;
                int medialPeleng = 5;

                int Intens = 8 + (1 + 1) * 3;
                Intens = 8 + (medialPhase + medialPeleng) * 3;
                if (Intens > 50) Intens = 50;

                rxyPlot.PointColor = Color.FromArgb(Intens, 255, 104, 0);

                int N = data.RadioSources.Count();

                double[,] multiPeleng = new double[2, N];

                for (int i = 0; i < N; i++)
                {
                    multiPeleng[0, i] = data.RadioSources[i].Frequency / 10000.0;
                    multiPeleng[1, i] = data.RadioSources[i].Direction / 10.0;
                }

                double[,] multiBearing = RecalcPolarBearing(multiPeleng);

                for (int i = 0; i < N; i++)
                {
                    rxyPlot.PlotXYAppend(multiBearing[0, i], multiBearing[1, i]);
                    //rxyPlot.PlotXY(multiBearing[0, i], multiBearing[1, i]);
                }
            }
            catch { }
        }
        private void RoundBearingPainted(GetRadioSourcesResponse data, Color color)
        {
            try
            {
                int medialPhase = 5;
                int medialPeleng = 5;

                int Intens = 8 + (1 + 1) * 3;
                Intens = 8 + (medialPhase + medialPeleng) * 3;
                if (Intens > 50) Intens = 50;
                rxyPlot.PointColor = Color.FromArgb(Intens, color.R, color.G, color.B);

                int N = data.RadioSources.Count();

                double[,] multiPeleng = new double[2, N];

                for (int i = 0; i < N; i++)
                {
                    multiPeleng[0, i] = data.RadioSources[i].Frequency / 10000.0;
                    multiPeleng[1, i] = data.RadioSources[i].Direction / 10.0;
                }

                double[,] multiBearing = RecalcPolarBearing(multiPeleng);

                for (int i = 0; i < N; i++)
                {
                    rxyPlot.PlotXYAppend(multiBearing[0, i], multiBearing[1, i]);
                    //rxyPlot.PlotXY(multiBearing[0, i], multiBearing[1, i]);
                }
            }
            catch { }
        }

        List<BearingPoint> RoundBearingPoints = new List<BearingPoint>();
        private void RoundBearingPaintedNew(GetRadioSourcesResponse data)
        {
            try
            {
                int medialPhase = 5;
                int medialPeleng = 5;

                int Intens = 8 + (1 + 1) * 3;
                Intens = 8 + (medialPhase + medialPeleng) * 3;
                if (Intens > 50) Intens = 50;

                rxyPlot.PointColor = Color.FromArgb(Intens, 255, 104, 0);

                int N = data.RadioSources.Count();

                double[,] multiPeleng = new double[2, N];

                for (int i = 0; i < N; i++)
                {
                    multiPeleng[0, i] = data.RadioSources[i].Frequency / 10000.0;
                    multiPeleng[1, i] = data.RadioSources[i].Direction / 10.0;
                }

                double[,] multiBearing = RecalcPolarBearing(multiPeleng);

                /*
                for (int i = 0; i < N; i++)
                {
                    rxyPlot.PlotXYAppend(multiBearing[0, i], multiBearing[1, i]);
                }
                */

                double[] xData = new double[N];
                double[] yData = new double[N];

                for (int i = 0; i < N; i++)
                {
                    xData[i] = multiBearing[0, i];
                    yData[i] = multiBearing[1, i];
                }


                if (RoundBearingPoints.Count != 0)
                {
                    DateTime dateTime = new DateTime();
                    dateTime = DateTime.Now;
                    BearingPoint temp = new BearingPoint(xData, yData, dateTime);

                    var diff = dateTime - RoundBearingPoints[0].datetime;

                    if (diff.TotalMilliseconds < 3000)
                    {
                        RoundBearingPoints.Add(temp);
                    }
                    else
                    {
                        RoundBearingPoints.RemoveAt(0);
                        RoundBearingPoints.Add(temp);
                    }
                }
                else
                {
                    DateTime dateTime = new DateTime();
                    dateTime = DateTime.Now;
                    BearingPoint temp = new BearingPoint(xData, yData, dateTime);
                    RoundBearingPoints.Add(temp);
                }

                int count = Convert.ToInt32((double)RoundBearingPoints.Count / (255.0 / (double)Intens));
                if (count <= 0) count = 1;

                int historycount = 250;
                int startnum = Convert.ToInt32(((double)RoundBearingPoints.Count / (double)count));

                for (int i = startnum; i < RoundBearingPoints.Count; i = i + count)
                {
                    historycount = historycount + RoundBearingPoints[i].xdata.Count();
                }

                rxyPlot.HistoryCapacity = historycount;

                for (int i = startnum; i < RoundBearingPoints.Count; i = i + count)
                {
                    rxyPlot.PlotXYAppend(RoundBearingPoints[i].xdata, RoundBearingPoints[i].ydata);
                }
            }
            catch { }
        }
        private void RoundBearingPaintedNew(GetRadioSourcesResponse data, Color color)
        {
            try
            {
                int medialPhase = 5;
                int medialPeleng = 5;

                int Intens = 8 + (1 + 1) * 3;
                Intens = 8 + (medialPhase + medialPeleng) * 3;
                if (Intens > 50) Intens = 50;

                rxyPlot.PointColor = Color.FromArgb(Intens, color.R, color.G, color.B);

                int N = data.RadioSources.Count();

                double[,] multiPeleng = new double[2, N];

                for (int i = 0; i < N; i++)
                {
                    multiPeleng[0, i] = data.RadioSources[i].Frequency / 10000.0;
                    multiPeleng[1, i] = data.RadioSources[i].Direction / 10.0;
                }

                double[,] multiBearing = RecalcPolarBearing(multiPeleng);

                /*
                for (int i = 0; i < N; i++)
                {
                    rxyPlot.PlotXYAppend(multiBearing[0, i], multiBearing[1, i]);
                }
                */

                double[] xData = new double[N];
                double[] yData = new double[N];

                for (int i = 0; i < N; i++)
                {
                    xData[i] = multiBearing[0, i];
                    yData[i] = multiBearing[1, i];
                }


                if (RoundBearingPoints.Count != 0)
                {
                    DateTime dateTime = new DateTime();
                    dateTime = DateTime.Now;
                    BearingPoint temp = new BearingPoint(xData, yData, dateTime);

                    var diff = dateTime - RoundBearingPoints[0].datetime;

                    if (diff.TotalMilliseconds < 3000)
                    {
                        RoundBearingPoints.Add(temp);
                    }
                    else
                    {
                        RoundBearingPoints.RemoveAt(0);
                        RoundBearingPoints.Add(temp);
                    }
                }
                else
                {
                    DateTime dateTime = new DateTime();
                    dateTime = DateTime.Now;
                    BearingPoint temp = new BearingPoint(xData, yData, dateTime);
                    RoundBearingPoints.Add(temp);
                }

                int count = Convert.ToInt32((double)RoundBearingPoints.Count / (255.0 / (double)Intens));
                if (count <= 0) count = 1;

                int historycount = 250;
                int startnum = Convert.ToInt32(((double)RoundBearingPoints.Count / (double)count));

                for (int i = startnum; i < RoundBearingPoints.Count; i = i + count)
                {
                    historycount = historycount + RoundBearingPoints[i].xdata.Count();
                }

                rxyPlot.HistoryCapacity = historycount;

                for (int i = startnum; i < RoundBearingPoints.Count; i = i + count)
                {
                    rxyPlot.PlotXYAppend(RoundBearingPoints[i].xdata, RoundBearingPoints[i].ydata);
                }
            }
            catch { }
        }

        //очень новая отрисовка пеленгов на круге
        private void RoundBearingPaintedNew2(GetBearingPanoramaSignalsResponse data)
        {
            try
            {
                int medialPhase = 5;
                int medialPeleng = 5;

                int Intens = 8 + (1 + 1) * 3;
                Intens = 8 + (medialPhase + medialPeleng) * 3;
                if (Intens > 50) Intens = 50;

                rxyPlot.PointColor = Color.FromArgb(Intens, rxyPlot.PointColor);
                //rxyPlot.PointColor = Color.FromArgb(Intens, 255, 104, 0);

                //int N = data.Signals.Count(); // закоментил, чтобы не было ошибок с новым протоколом
                int N = 0; // Чтобы скомпилилось
                double[,] multiPeleng = new double[2, N];

                for (int i = 0; i < N; i++)
                {
                    // multiPeleng[0, i] = data.Signals[i].Frequency / 10000.0; // закоментил, чтобы не было ошибок с новым протоколом
                    // multiPeleng[1, i] = data.Signals[i].Direction / 10.0; // закоментил, чтобы не было ошибок с новым протоколом
                }

                double[,] multiBearing = RecalcPolarBearing(multiPeleng);

                /*
                for (int i = 0; i < N; i++)
                {
                    rxyPlot.PlotXYAppend(multiBearing[0, i], multiBearing[1, i]);
                }
                */

                double[] xData = new double[N];
                double[] yData = new double[N];

                for (int i = 0; i < N; i++)
                {
                    xData[i] = multiBearing[0, i];
                    yData[i] = multiBearing[1, i];
                }


                if (RoundBearingPoints.Count != 0)
                {
                    DateTime dateTime = new DateTime();
                    dateTime = DateTime.Now;
                    BearingPoint temp = new BearingPoint(xData, yData, dateTime);

                    var diff = dateTime - RoundBearingPoints[0].datetime;

                    if (diff.TotalMilliseconds < 3000)
                    {
                        RoundBearingPoints.Add(temp);
                    }
                    else
                    {
                        RoundBearingPoints.RemoveAt(0);
                        RoundBearingPoints.Add(temp);
                    }
                }
                else
                {
                    DateTime dateTime = new DateTime();
                    dateTime = DateTime.Now;
                    BearingPoint temp = new BearingPoint(xData, yData, dateTime);
                    RoundBearingPoints.Add(temp);
                }

                int count = Convert.ToInt32((double)RoundBearingPoints.Count / (255.0 / (double)Intens));
                if (count <= 0) count = 1;

                int historycount = 250;
                int startnum = Convert.ToInt32(((double)RoundBearingPoints.Count / (double)count));

                for (int i = startnum; i < RoundBearingPoints.Count; i = i + count)
                {
                    historycount = historycount + RoundBearingPoints[i].xdata.Count();
                }

                rxyPlot.HistoryCapacity = historycount;

                for (int i = startnum; i < RoundBearingPoints.Count; i = i + count)
                {
                    rxyPlot.PlotXYAppend(RoundBearingPoints[i].xdata, RoundBearingPoints[i].ydata);
                }
            }
            catch { }
        }

        //экстра новая отрисовка пеленгов на круге
        private void RoundBearingPaintedNew3(GetBearingPanoramaSignalsResponse data)
        {
            //отрисовка ППРЧ
            try
            {
                int medialPhase = 5;
                int medialPeleng = 5;

                int Intens = 8 + (1 + 1) * 3;
                Intens = 8 + (medialPhase + medialPeleng) * 3;
                if (Intens > 50) Intens = 50;

                rxyPlot.PointColor = Color.FromArgb(Intens, rxyPlot.PointColor);

                int N = data.ImpulseSignalsCount;

                double[,] multiPeleng = new double[2, N];

                for (int i = 0; i < N; i++)
                {
                    multiPeleng[0, i] = data.ImpulseSignals[i].Frequency / 10000.0;
                    multiPeleng[1, i] = data.ImpulseSignals[i].Direction / 10.0;
                }

                double[,] multiBearing = RecalcPolarBearing(multiPeleng);

                double[] xData = new double[N];
                double[] yData = new double[N];

                for (int i = 0; i < N; i++)
                {
                    xData[i] = multiBearing[0, i];
                    yData[i] = multiBearing[1, i];
                }


                if (RoundBearingPoints.Count != 0)
                {
                    DateTime dateTime = new DateTime();
                    dateTime = DateTime.Now;
                    BearingPoint temp = new BearingPoint(xData, yData, dateTime);

                    var diff = dateTime - RoundBearingPoints[0].datetime;

                    if (diff.TotalMilliseconds < 3000)
                    {
                        RoundBearingPoints.Add(temp);
                    }
                    else
                    {
                        RoundBearingPoints.RemoveAt(0);
                        RoundBearingPoints.Add(temp);
                    }
                }
                else
                {
                    DateTime dateTime = new DateTime();
                    dateTime = DateTime.Now;
                    BearingPoint temp = new BearingPoint(xData, yData, dateTime);
                    RoundBearingPoints.Add(temp);
                }

                int count = Convert.ToInt32((double)RoundBearingPoints.Count / (255.0 / (double)Intens));
                if (count <= 0) count = 1;

                int historycount = 250;
                int startnum = Convert.ToInt32(((double)RoundBearingPoints.Count / (double)count));

                for (int i = startnum; i < RoundBearingPoints.Count; i = i + count)
                {
                    historycount = historycount + RoundBearingPoints[i].xdata.Count();
                }

                rxyPlot.HistoryCapacity = historycount;

                for (int i = startnum; i < RoundBearingPoints.Count; i = i + count)
                {
                    rxyPlot.PlotXYAppend(RoundBearingPoints[i].xdata, RoundBearingPoints[i].ydata);
                }
            }
            catch { }

            //отрисовка ФРЧ
            try
            {
                int Intens = 255;

                rxyPlot.PointColor = Color.FromArgb(Intens, rxyPlot.PointColor);

                int N = data.FixedSignalsCount;

                double[,] multiPeleng = new double[2, N];

                for (int i = 0; i < N; i++)
                {
                    multiPeleng[0, i] = data.FixedSignals[i].Frequency / 10000.0;
                    multiPeleng[1, i] = data.FixedSignals[i].Direction / 10.0;
                }

                double[,] multiBearing = RecalcPolarBearing(multiPeleng);

                double[] xData = new double[N];
                double[] yData = new double[N];

                for (int i = 0; i < N; i++)
                {
                    xData[i] = multiBearing[0, i];
                    yData[i] = multiBearing[1, i];
                }

                rxyPlot.HistoryCapacity = rxyPlot.HistoryCapacity + N;

                rxyPlot.PlotXYAppend(xData, yData);
            }
            catch { }

        }

        //актуальная отрисовка пеленгов на круге
        List<BPoint> RoundBPointsF = new List<BPoint>();
        private void RoundBearingPaintedNew4(GetBearingPanoramaSignalsResponse data)
        {
            //отрисовка ППРЧ
            try
            {
                rxyPlot.ClearData();

                int medialPhase = 5;
                int medialPeleng = 5;

                int Intens = 8 + (medialPhase + medialPeleng) * 3;
                if (Intens > 50) Intens = 50;

                rxyPlot.PointColor = Color.FromArgb(Intens, rxyPlot.PointColor);

                int N = data.ImpulseSignalsCount;

                double[,] multiPeleng = new double[2, N];

                for (int i = 0; i < N; i++)
                {
                    multiPeleng[0, i] = data.ImpulseSignals[i].Frequency / 10000.0;
                    multiPeleng[1, i] = data.ImpulseSignals[i].Direction / 10.0;
                }

                double[,] multiBearing = RecalcPolarBearing(multiPeleng);

                double[] xData = new double[N];
                double[] yData = new double[N];

                for (int i = 0; i < N; i++)
                {
                    xData[i] = multiBearing[0, i];
                    yData[i] = multiBearing[1, i];
                }


                if (RoundBearingPoints.Count != 0)
                {
                    DateTime dateTime = new DateTime();
                    dateTime = DateTime.Now;
                    BearingPoint temp = new BearingPoint(xData, yData, dateTime);

                    var diff = dateTime - RoundBearingPoints[0].datetime;

                    if (diff.TotalMilliseconds < 3000)
                    {
                        RoundBearingPoints.Add(temp);
                    }
                    else
                    {
                        RoundBearingPoints.RemoveAt(0);
                        RoundBearingPoints.Add(temp);
                    }
                }
                else
                {
                    if (N != 0)
                    {
                        DateTime dateTime = new DateTime();
                        dateTime = DateTime.Now;
                        BearingPoint temp = new BearingPoint(xData, yData, dateTime);
                        RoundBearingPoints.Add(temp);
                    }
                }

                int count = Convert.ToInt32((double)RoundBearingPoints.Count / (255.0 / (double)Intens));
                if (count <= 0) count = 1;

                int startnum = Convert.ToInt32(((double)RoundBearingPoints.Count / (double)count));

                for (int i = startnum; i < RoundBearingPoints.Count; i = i + count)
                {
                    rxyPlot.PlotXYAppend(RoundBearingPoints[i].xdata, RoundBearingPoints[i].ydata);
                }
            }
            catch { }

            //отрисовка ФРЧ
            try
            {
                int Nf = data.FixedSignalsCount;

                double[,] multiPelengf = new double[2, Nf];

                for (int i = 0; i < Nf; i++)
                {
                    multiPelengf[0, i] = data.FixedSignals[i].Frequency / 10000.0;
                    multiPelengf[1, i] = data.FixedSignals[i].Direction / 10.0;
                }

                double[,] multiBearingf = RecalcPolarBearing(multiPelengf);

                double[] xDataf = new double[Nf];
                double[] yDataf = new double[Nf];

                for (int i = 0; i < Nf; i++)
                {
                    xDataf[i] = multiBearingf[0, i];
                    yDataf[i] = multiBearingf[1, i];

                    RoundBPointsF.Add(new BPoint(xDataf[i], yDataf[i], DateTime.Now));
                }

                for (int i = 0; i < RoundBPointsF.Count; i++)
                {
                    DateTime now = DateTime.Now;
                    var diff = now - RoundBPointsF[i].datetime;
                    if (diff.TotalMilliseconds > 3000)
                    {
                        RoundBPointsF.RemoveAt(i);
                    }
                }

                xDataf = new double[RoundBPointsF.Count];
                yDataf = new double[RoundBPointsF.Count];
                for (int i = 0; i < RoundBPointsF.Count; i++)
                {
                    xDataf[i] = RoundBPointsF[i].xdata;
                    yDataf[i] = RoundBPointsF[i].ydata;
                }

                rxyPlot.PlotXYAppend(xDataf, yDataf);
            }
            catch { }

        }


        private double[,] RecalcPolarBearing(double[,] multiPeleng)
        {
            double FreqMax = GlobalRangeMax;
            double FreqMin = GlobalRangeMin;

            int N = multiPeleng.Length / 2;
            double[,] multiBearing = new double[2, N];

            for (int i = 0; i < N; i++)
            {
                multiBearing[0, i] = multiPeleng[0, i] / (FreqMax - FreqMin) * 10 * Math.Sin(Math.PI / 180 * multiPeleng[1, i]); //Xi
                multiBearing[1, i] = multiPeleng[0, i] / (FreqMax - FreqMin) * 10 * Math.Cos(Math.PI / 180 * multiPeleng[1, i]); //Yi
            }

            return multiBearing;
        }

        // Частотная панорама + лимб
        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            currindex = 3;

            toolStripButton1.Checked = false;
            toolStripButton2.Checked = false;
            toolStripButton3.Checked = true;
            toolStripButton4.Checked = false;
            toolStripButton5.Checked = false;

            button3.Visible = true;
            button34.Visible = false;
            label9.Visible = true;
            label10.Visible = true;

            label94.Visible = false;
            label104.Visible = false;

            label95.Visible = false;

            //if (variableWork.Regime == 1 || variableWork.Regime == 2)
            //    label11.Visible = true;

            labelLeft.Visible = true;
            labelRight.Visible = true;

            waveformGraph1.Visible = true;
            xyDataScatterGraph.Visible = false;

            //groupBox1.Visible = true;
            scGraph2andLabelVisible(true);

            scatterGraph1.Visible = false;
            intensityGraph1.Visible = false;
            groupBox2.Visible = false;

            pictureBox1.Visible = true;

            Panorama_Resize();

            intensityTimer.Enabled = false;
        }


        private void scGraph2andLabelVisible(bool visible)
        {
            scatterGraph2.Visible = visible;
            Flabel.Visible = visible;
            Qlabel.Visible = visible;
            SDlabel.Visible = visible;
            Plabel.Visible = visible;

            CofAvPh.Visible = visible;
            CofAvPl.Visible = visible;

            FtextBox.Visible = visible;
            QtextBox.Visible = visible;
            SDtextBox.Visible = visible;
            PtextBox.Visible = visible;

            numericUpDownPh.Visible = visible;
            numericUpDownPl.Visible = visible;

            FlineShape.Visible = visible;
            QlineShape.Visible = visible;
            SDlineShape.Visible = visible;
            PlineShape.Visible = visible;
            PhlineShape.Visible = visible;
            PllineShape.Visible = visible;

        }

        private void InitAnnotations2()
        {
            if (scatterGraph2.Size.Width > 0)
            {
                xyrPointAnnotation0.XPosition = 10 * Math.Sin(Math.PI / 180 * 0) - 1 * (134.0 / scatterGraph2.Size.Width);
                xyrPointAnnotation0.YPosition = 10 * Math.Cos(Math.PI / 180 * 0) + 2 * (134.0 / scatterGraph2.Size.Width);

                xyrPointAnnotation45.XPosition = 10 * Math.Sin(Math.PI / 180 * 45);
                xyrPointAnnotation45.YPosition = 10 * Math.Cos(Math.PI / 180 * 45) + 1.5 * (134.0 / scatterGraph2.Size.Width);

                xyrPointAnnotation90.XPosition = 10 * Math.Sin(Math.PI / 180 * 90) - 0.75 * (134.0 / scatterGraph2.Size.Width);
                xyrPointAnnotation90.YPosition = 10 * Math.Cos(Math.PI / 180 * 90) + 0.5 * (134.0 / scatterGraph2.Size.Width);

                xyrPointAnnotation135.XPosition = 10 * Math.Sin(Math.PI / 180 * 135);
                xyrPointAnnotation135.YPosition = 10 * Math.Cos(Math.PI / 180 * 135);

                xyrPointAnnotation180.XPosition = 10 * Math.Sin(Math.PI / 180 * 180) - 2.5 * (134.0 / scatterGraph2.Size.Width);
                xyrPointAnnotation180.YPosition = 10 * Math.Cos(Math.PI / 180 * 180) - 0.25 * (134.0 / scatterGraph2.Size.Width);

                xyrPointAnnotation225.XPosition = 10 * Math.Sin(Math.PI / 180 * 225) - 3.5 * (134.0 / scatterGraph2.Size.Width);
                xyrPointAnnotation225.YPosition = 10 * Math.Cos(Math.PI / 180 * 225);

                xyrPointAnnotation270.XPosition = 10 * Math.Sin(Math.PI / 180 * 270) - 2.25 * (134.0 / scatterGraph2.Size.Width);
                xyrPointAnnotation270.YPosition = 10 * Math.Cos(Math.PI / 180 * 270) + 0.5 * (134.0 / scatterGraph2.Size.Width);

                xyrPointAnnotation315.XPosition = 10 * Math.Sin(Math.PI / 180 * 315) - 4 * (134.0 / scatterGraph2.Size.Width);
                xyrPointAnnotation315.YPosition = 10 * Math.Cos(Math.PI / 180 * 315) + 2 * (134.0 / scatterGraph2.Size.Width);
            }
        }
        private void InitAnnotations2(double size)
        {
            if (scatterGraph2.Size.Width > 0)
            {
                xyrPointAnnotation0.XPosition = 10 * Math.Sin(Math.PI / 180 * 0) - 0.75 * (size / scatterGraph2.Size.Width);
                xyrPointAnnotation0.YPosition = 10 * Math.Cos(Math.PI / 180 * 0) + 2 * (size / scatterGraph2.Size.Width);

                xyrPointAnnotation45.XPosition = 10 * Math.Sin(Math.PI / 180 * 45);
                xyrPointAnnotation45.YPosition = 10 * Math.Cos(Math.PI / 180 * 45) + 1.5 * (size / scatterGraph2.Size.Width);

                xyrPointAnnotation90.XPosition = 10 * Math.Sin(Math.PI / 180 * 90) - 0.25 * (size / scatterGraph2.Size.Width);
                xyrPointAnnotation90.YPosition = 10 * Math.Cos(Math.PI / 180 * 90) + 0.75 * (size / scatterGraph2.Size.Width);

                xyrPointAnnotation135.XPosition = 10 * Math.Sin(Math.PI / 180 * 135);
                xyrPointAnnotation135.YPosition = 10 * Math.Cos(Math.PI / 180 * 135);

                xyrPointAnnotation180.XPosition = 10 * Math.Sin(Math.PI / 180 * 180) - 2.25 * (size / scatterGraph2.Size.Width);
                xyrPointAnnotation180.YPosition = 10 * Math.Cos(Math.PI / 180 * 180) - 0.25 * (size / scatterGraph2.Size.Width);

                xyrPointAnnotation225.XPosition = 10 * Math.Sin(Math.PI / 180 * 225) - 3.5 * (size / scatterGraph2.Size.Width);
                xyrPointAnnotation225.YPosition = 10 * Math.Cos(Math.PI / 180 * 225);

                xyrPointAnnotation270.XPosition = 10 * Math.Sin(Math.PI / 180 * 270) - 2.50 * (size / scatterGraph2.Size.Width);
                xyrPointAnnotation270.YPosition = 10 * Math.Cos(Math.PI / 180 * 270) + 0.75 * (size / scatterGraph2.Size.Width);

                xyrPointAnnotation315.XPosition = 10 * Math.Sin(Math.PI / 180 * 315) - 4 * (size / scatterGraph2.Size.Width);
                xyrPointAnnotation315.YPosition = 10 * Math.Cos(Math.PI / 180 * 315) + 2 * (size / scatterGraph2.Size.Width);
            }
        }
        private void InitAnnotations2(double radius, double size)
        {
            if (scatterGraph2.Size.Width > 0)
            {
                xyrPointAnnotation0.XPosition = radius * Math.Sin(Math.PI / 180 * 0) - 1 * (size / scatterGraph2.Size.Width);
                xyrPointAnnotation0.YPosition = radius * Math.Cos(Math.PI / 180 * 0) + 2 * (size / scatterGraph2.Size.Width);

                xyrPointAnnotation45.XPosition = radius * Math.Sin(Math.PI / 180 * 45);
                xyrPointAnnotation45.YPosition = radius * Math.Cos(Math.PI / 180 * 45) + 1.5 * (size / scatterGraph2.Size.Width);

                xyrPointAnnotation90.XPosition = radius * Math.Sin(Math.PI / 180 * 90) - 0.75 * (size / scatterGraph2.Size.Width);
                xyrPointAnnotation90.YPosition = radius * Math.Cos(Math.PI / 180 * 90) + 0.5 * (size / scatterGraph2.Size.Width);

                xyrPointAnnotation135.XPosition = radius * Math.Sin(Math.PI / 180 * 135);
                xyrPointAnnotation135.YPosition = radius * Math.Cos(Math.PI / 180 * 135);

                xyrPointAnnotation180.XPosition = radius * Math.Sin(Math.PI / 180 * 180) - 2.5 * (size / scatterGraph2.Size.Width);
                xyrPointAnnotation180.YPosition = radius * Math.Cos(Math.PI / 180 * 180) - 0.25 * (size / scatterGraph2.Size.Width);

                xyrPointAnnotation225.XPosition = radius * Math.Sin(Math.PI / 180 * 225) - 3.5 * (size / scatterGraph2.Size.Width);
                xyrPointAnnotation225.YPosition = radius * Math.Cos(Math.PI / 180 * 225);

                xyrPointAnnotation270.XPosition = radius * Math.Sin(Math.PI / 180 * 270) - 2.25 * (size / scatterGraph2.Size.Width);
                xyrPointAnnotation270.YPosition = radius * Math.Cos(Math.PI / 180 * 270) + 0.5 * (size / scatterGraph2.Size.Width);

                xyrPointAnnotation315.XPosition = radius * Math.Sin(Math.PI / 180 * 315) - 4 * (size / scatterGraph2.Size.Width);
                xyrPointAnnotation315.YPosition = radius * Math.Cos(Math.PI / 180 * 315) + 2 * (size / scatterGraph2.Size.Width);
            }
        }

        /*private void InitAnnotations2New()
        {
            double ShiftCircle = 0;
            double Radius = 10;

            double SCx = 10 * Math.Sin(Math.PI / 180 * 0);
            double SCy = 10 * Math.Cos(Math.PI / 180 * 0);

            double a = Math.Cos(Math.PI / 4) * (2 * Radius);
           
            //xyPointAnnotation1.Caption = "0°";
            xyrPointAnnotation0.XPosition = ShiftCircle;
            xyrPointAnnotation0.YPosition = ShiftCircle + a;
            
            //xyPointAnnotation2.Caption = "45°";
            xyrPointAnnotation45.XPosition = ShiftCircle + a;
            xyrPointAnnotation45.YPosition = ShiftCircle + a;

            //xyPointAnnotation3.Caption = "90°";
            xyrPointAnnotation90.XPosition = ShiftCircle + a;
            xyrPointAnnotation90.YPosition = ShiftCircle;

            //xyPointAnnotation4.Caption = "135°";
            xyrPointAnnotation135.XPosition = ShiftCircle + a;
            xyrPointAnnotation135.YPosition = ShiftCircle - a;

            //xyPointAnnotation5.Caption = "180°";
            xyrPointAnnotation180.XPosition = ShiftCircle;
            xyrPointAnnotation180.YPosition = ShiftCircle - a;

            //xyPointAnnotation6.Caption = "225°";
            xyrPointAnnotation225.XPosition = ShiftCircle - a;
            xyrPointAnnotation225.YPosition = ShiftCircle - a;

            //xyPointAnnotation7.Caption = "270°";
            xyrPointAnnotation270.XPosition = ShiftCircle - a;
            xyrPointAnnotation270.YPosition = ShiftCircle;

            //xyPointAnnotation8.Caption = "315°";
            xyrPointAnnotation315.XPosition = ShiftCircle - a;
            xyrPointAnnotation315.YPosition = ShiftCircle + a;
            
        }
         */
        /*private void InitAnnotations3New(double shift)
        {
            double a = Math.Cos(Math.PI / 4) * (2 * shift);

            xyrPointAnnotation0.XPosition = 10 * Math.Sin(Math.PI / 180 * 0);
            xyrPointAnnotation0.YPosition = 10 * Math.Cos(Math.PI / 180 * 0) + a;

            xyrPointAnnotation45.XPosition = 10 * Math.Sin(Math.PI / 180 * 45) + a;
            xyrPointAnnotation45.YPosition = 10 * Math.Cos(Math.PI / 180 * 45) + a;

            xyrPointAnnotation90.XPosition = 10 * Math.Sin(Math.PI / 180 * 90) + a;
            xyrPointAnnotation90.YPosition = 10 * Math.Cos(Math.PI / 180 * 90);

            xyrPointAnnotation135.XPosition = 10 * Math.Sin(Math.PI / 180 * 135) + a;
            xyrPointAnnotation135.YPosition = 10 * Math.Cos(Math.PI / 180 * 135) - a;

            xyrPointAnnotation180.XPosition = 10 * Math.Sin(Math.PI / 180 * 180);
            xyrPointAnnotation180.YPosition = 10 * Math.Cos(Math.PI / 180 * 180) - a;

            xyrPointAnnotation225.XPosition = 10 * Math.Sin(Math.PI / 180 * 225) - a;
            xyrPointAnnotation225.YPosition = 10 * Math.Cos(Math.PI / 180 * 225) - a;

            xyrPointAnnotation270.XPosition = 10 * Math.Sin(Math.PI / 180 * 270) - a;
            xyrPointAnnotation270.YPosition = 10 * Math.Cos(Math.PI / 180 * 270);

            xyrPointAnnotation315.XPosition = 10 * Math.Sin(Math.PI / 180 * 315) - a;
            xyrPointAnnotation315.YPosition = 10 * Math.Cos(Math.PI / 180 * 315) + a;

            

        }
        */

        private static double[] GenerateXCircle720(double Radius)
        {
            double[] xData = new double[721];

            for (int i = 0; i < 720; i++)
            {
                xData[i] = Radius * Math.Sin(Math.PI / 180 * i * 0.5);
            }

            xData[720] = xData[0];

            return xData;
        }
        private static double[] GenerateYCircle720(double Radius)
        {
            double[] yData = new double[721];

            for (int i = 0; i < 720; i++)
            {
                yData[i] = Radius * Math.Cos(Math.PI / 180 * i * 0.5);
            }

            yData[720] = yData[0];

            return yData;
        }

        private static double[] GenerateTickX(double TickLength, double Angle)
        {
            double[] xData = new double[2];
            xData[0] = (10.0 - TickLength) * Math.Sin(Math.PI / 180.0 * Angle);
            xData[1] = 10.0 * Math.Sin(Math.PI / 180.0 * Angle);
            return xData;
        }
        private static double[] GenerateTickX(double TickLength, double Angle, double radius)
        {
            double[] xData = new double[2];
            xData[0] = (radius - TickLength) * Math.Sin(Math.PI / 180.0 * Angle);
            xData[1] = radius * Math.Sin(Math.PI / 180.0 * Angle);
            return xData;
        }

        private static double[] GenerateTickY(double TickLength, double Angle)
        {
            double[] xData = new double[2];
            xData[0] = (10.0 - TickLength) * Math.Cos(Math.PI / 180.0 * Angle);
            xData[1] = 10.0 * Math.Cos(Math.PI / 180.0 * Angle);
            return xData;
        }
        private static double[] GenerateTickY(double TickLength, double Angle, double radius)
        {
            double[] xData = new double[2];
            xData[0] = (radius - TickLength) * Math.Cos(Math.PI / 180.0 * Angle);
            xData[1] = radius * Math.Cos(Math.PI / 180.0 * Angle);
            return xData;
        }

        private static double[] GenerateTriangleX(double Radius, double Range, double Angle)
        {
            double[] xData = new double[4];
            xData[0] = Range * Math.Sin(Math.PI / 180 * (90 + Angle));
            xData[1] = Range * Math.Sin(Math.PI / 180 * (90 + Angle + 180));
            xData[2] = Radius * Math.Sin(Math.PI / 180 * Angle);
            xData[3] = xData[0];
            return xData;
        }
        private static double[] GenerateTriangleY(double Radius, double Range, double Angle)
        {
            double[] yData = new double[4];
            yData[0] = Range * Math.Cos(Math.PI / 180 * (90 + Angle));
            yData[1] = Range * Math.Cos(Math.PI / 180 * (90 + Angle + 180));
            yData[2] = Radius * Math.Cos(Math.PI / 180 * Angle);
            yData[3] = yData[0];
            return yData;
        }


        private void FirstInitScatterGraph2()
        {
            int NumberTicks = 36;

            List<double> majorAngles = new List<double>();
            List<double> minorAngles = new List<double>();

            for (int i = 0; i < NumberTicks; i++)
            {
                majorAngles.Add(0 + 360.0 / NumberTicks * i);
            }

            for (int i = 0; i < NumberTicks; i++)
            {
                minorAngles.Add(360.0 / NumberTicks / 2 + 360.0 / NumberTicks * i);
            }

            //List<double> Ldx = GenerateXCircle(10).ToList();
            //List<double> Ldy = GenerateYCircle(10).ToList();

            List<double> Ldx = GenerateXCircle720(10).ToList();
            List<double> Ldy = GenerateYCircle720(10).ToList();


            double majorTickLength = 1.0;
            double minorTickLength = majorTickLength / 2;

            for (int i = 0; i < NumberTicks; i++)
            {
                var gtx = GenerateTickX(majorTickLength, majorAngles[i]);
                var gty = GenerateTickY(majorTickLength, majorAngles[i]);

                var indx = Ldx.IndexOf(gtx[1]);
                var indy = Ldy.IndexOf(gty[1]);

                if (indx != indy)
                {
                    indx = Math.Max(indx, indy);
                    indy = Math.Max(indx, indy);
                }

                Ldx.Insert(indx + 1, gtx[0]);
                Ldx.Insert(indx + 2, gtx[1]);
                Ldy.Insert(indy + 1, gty[0]);
                Ldy.Insert(indy + 2, gty[1]);



                gtx = GenerateTickX(minorTickLength, minorAngles[i]);
                gty = GenerateTickY(minorTickLength, minorAngles[i]);

                indx = Ldx.IndexOf(gtx[1]);
                indy = Ldy.IndexOf(gty[1]);

                if (indx != indy)
                {
                    indx = Math.Max(indx, indy);
                    indy = Math.Max(indx, indy);
                }

                Ldx.Insert(indx + 1, gtx[0]);
                Ldx.Insert(indx + 2, gtx[1]);
                Ldy.Insert(indy + 1, gty[0]);
                Ldy.Insert(indy + 2, gty[1]);
            }

            //r2ScatterPlot1.FillMode = PlotFillMode.Fill;
            //r2ScatterPlot1.PlotXYAppend(Ldx.ToArray(), Ldy.ToArray());
            r2ScatterPlot1.PlotXY(Ldx.ToArray(), Ldy.ToArray());
        }
        private void FirstInitScatterGraph2(double radius)
        {
            int NumberTicks = 36;

            List<double> majorAngles = new List<double>();
            List<double> minorAngles = new List<double>();

            for (int i = 0; i < NumberTicks; i++)
            {
                majorAngles.Add(0 + 360.0 / NumberTicks * i);
            }

            for (int i = 0; i < NumberTicks; i++)
            {
                minorAngles.Add(360.0 / NumberTicks / 2 + 360.0 / NumberTicks * i);
            }

            //List<double> Ldx = GenerateXCircle(10).ToList();
            //List<double> Ldy = GenerateYCircle(10).ToList();

            List<double> Ldx = GenerateXCircle720(radius).ToList();
            List<double> Ldy = GenerateYCircle720(radius).ToList();


            double majorTickLength = 1.0;
            double minorTickLength = majorTickLength / 2;

            for (int i = 0; i < NumberTicks; i++)
            {
                var gtx = GenerateTickX(majorTickLength, majorAngles[i], radius);
                var gty = GenerateTickY(majorTickLength, majorAngles[i], radius);

                var indx = Ldx.IndexOf(gtx[1]);
                var indy = Ldy.IndexOf(gty[1]);

                if (indx != indy)
                {
                    indx = Math.Max(indx, indy);
                    indy = Math.Max(indx, indy);
                }

                Ldx.Insert(indx + 1, gtx[0]);
                Ldx.Insert(indx + 2, gtx[1]);
                Ldy.Insert(indy + 1, gty[0]);
                Ldy.Insert(indy + 2, gty[1]);



                gtx = GenerateTickX(minorTickLength, minorAngles[i], radius);
                gty = GenerateTickY(minorTickLength, minorAngles[i], radius);

                indx = Ldx.IndexOf(gtx[1]);
                indy = Ldy.IndexOf(gty[1]);

                if (indx != indy)
                {
                    indx = Math.Max(indx, indy);
                    indy = Math.Max(indx, indy);
                }

                Ldx.Insert(indx + 1, gtx[0]);
                Ldx.Insert(indx + 2, gtx[1]);
                Ldy.Insert(indy + 1, gty[0]);
                Ldy.Insert(indy + 2, gty[1]);
            }

            //r2ScatterPlot1.FillMode = PlotFillMode.Fill;
            //r2ScatterPlot1.PlotXYAppend(Ldx.ToArray(), Ldy.ToArray());
            r2ScatterPlot1.PlotXY(Ldx.ToArray(), Ldy.ToArray());
        }
        private void FirstInitScatterGraph2(int NumberTicks)
        {
            List<double> majorAngles = new List<double>();
            List<double> minorAngles = new List<double>();

            for (int i = 0; i < NumberTicks; i++)
            {
                majorAngles.Add(0 + 360.0 / NumberTicks * i);
            }

            for (int i = 0; i < NumberTicks; i++)
            {
                minorAngles.Add(360.0 / NumberTicks / 2 + 360.0 / NumberTicks * i);
            }

            //List<double> Ldx = GenerateXCircle(10).ToList();
            //List<double> Ldy = GenerateYCircle(10).ToList();

            List<double> Ldx = GenerateXCircle720(10).ToList();
            List<double> Ldy = GenerateYCircle720(10).ToList();


            double majorTickLength = 1.0;
            double minorTickLength = majorTickLength / 2;

            for (int i = 0; i < NumberTicks; i++)
            {
                var gtx = GenerateTickX(majorTickLength, majorAngles[i]);
                var gty = GenerateTickY(majorTickLength, majorAngles[i]);

                var indx = Ldx.IndexOf(gtx[1]);
                var indy = Ldy.IndexOf(gty[1]);

                if (indx != indy)
                {
                    indx = Math.Max(indx, indy);
                    indy = Math.Max(indx, indy);
                }

                Ldx.Insert(indx + 1, gtx[0]);
                Ldx.Insert(indx + 2, gtx[1]);
                Ldy.Insert(indy + 1, gty[0]);
                Ldy.Insert(indy + 2, gty[1]);



                gtx = GenerateTickX(minorTickLength, minorAngles[i]);
                gty = GenerateTickY(minorTickLength, minorAngles[i]);

                indx = Ldx.IndexOf(gtx[1]);
                indy = Ldy.IndexOf(gty[1]);

                if (indx != indy)
                {
                    indx = Math.Max(indx, indy);
                    indy = Math.Max(indx, indy);
                }

                Ldx.Insert(indx + 1, gtx[0]);
                Ldx.Insert(indx + 2, gtx[1]);
                Ldy.Insert(indy + 1, gty[0]);
                Ldy.Insert(indy + 2, gty[1]);
            }

            r2ScatterPlot1.FillMode = PlotFillMode.Fill;
            //r2ScatterPlot1.PlotXYAppend(Ldx.ToArray(), Ldy.ToArray());
            r2ScatterPlot1.PlotXY(Ldx.ToArray(), Ldy.ToArray());
        }

        private void SecondInitScatterGraph2()
        {
            double range = 0.75;
            r2ScatterPlot2.FillMode = PlotFillMode.Fill;
            r2ScatterPlot2.PlotXY(GenerateXCircle(range), GenerateYCircle(range));
        }
        private void SecondInitScatterGraph2(double range)
        {
            r2ScatterPlot2.FillMode = PlotFillMode.Fill;
            r2ScatterPlot2.PlotXY(GenerateXCircle(range), GenerateYCircle(range));
        }

        private void ThirdInitScatterGraph2()
        {
            double range = 0.75;
            double angle = 22.5;

            r2ScatterPlot3.FillMode = PlotFillMode.Fill;
            r2ScatterPlot3.PlotXY(GenerateTriangleX(10, range, angle), GenerateTriangleY(10, range, angle));
        }
        private void ThirdInitScatterGraph2(double angle)
        {
            double range = 0.75;
            r2ScatterPlot3.FillMode = PlotFillMode.Fill;
            r2ScatterPlot3.PlotXY(GenerateTriangleX(10, range, angle), GenerateTriangleY(10, range, angle));
        }
        private void ThirdInitScatterGraph2(double range, double angle)
        {
            r2ScatterPlot3.FillMode = PlotFillMode.Fill;
            r2ScatterPlot3.PlotXY(GenerateTriangleX(10, range, angle), GenerateTriangleY(10, range, angle));
        }

        private static double[] GenerateXCorCircle(byte[] Cor)
        {
            double[] xData = new double[361];
            double[] dCor = new double[360];

            for (int i = 0; i < 360; i++)
            {
                dCor[i] = Convert.ToDouble(Cor[i]) * 10.0 / 255.0;
                xData[i] = dCor[i] * Math.Sin(Math.PI / 180 * i);
            }

            xData[360] = xData[0];

            return xData;
        }
        private static double[] GenerateYCorCircle(byte[] Cor)
        {
            double[] yData = new double[361];
            double[] dCor = new double[360];

            for (int i = 0; i < 360; i++)
            {
                dCor[i] = Convert.ToDouble(Cor[i]) * 10.0 / 255.0;
                yData[i] = dCor[i] * Math.Cos(Math.PI / 180 * i);
            }

            yData[360] = yData[0];

            return yData;
        }

        private void FourthInitScatterGraph2(byte[] Cor)
        {
            if (Cor.Count() == 360)
                r2ScatterPlot4.PlotXY(GenerateXCorCircle(Cor), GenerateYCorCircle(Cor));
        }

        // Клик
        private void waveformGraph1_MouseClick(object sender, MouseEventArgs e)
        {
            if ((Control.ModifierKeys & Keys.Control) == Keys.Control || (Control.ModifierKeys & Keys.Shift) == Keys.Shift)
            {
                return;
            }

            if (e.Button == MouseButtons.Left)
            {
                Rectangle rp = waveformPlot1.GetBounds();
                if (e.X >= rp.Left && e.X <= rp.Right && e.Y >= rp.Top && e.Y <= rp.Bottom)
                {

                    SpectrxyCursor.XPosition = (e.X - eBoundsX) * (xAxes.Range.Maximum - xAxes.Range.Minimum) / (eBoundsWidth) + xAxes.Range.Minimum;

                    SpectrxyCursor.YPosition = (e.Y - eBoundsY) * (yAxes.Range.Minimum - yAxes.Range.Maximum) / (eBoundsHeight) + yAxes.Range.Maximum;

                }

                label9.Text = String.Format("{0}: {1} {2}", freq, SpectrxyCursor.XPosition.ToString("F4"), MHz);
                label10.Text = String.Format("{0}: {1} {2}", level, SpectrxyCursor.YPosition.ToString("F1"), dB);
            }

            if (e.Button == MouseButtons.Right)
            {
                if (waspressright2 == false)
                {
                    contextMenuStrip1.Show(waveformGraph1, new Point(e.X, e.Y));
                }
            }

        }

        //Дабл клик
        bool wasPerformDoubleClick = false;
        private void waveformGraph1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if ((Control.ModifierKeys & Keys.Control) == Keys.Control || (Control.ModifierKeys & Keys.Shift) == Keys.Shift)
            {
                return;
            }

            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                Rectangle rg = waveformGraph1.Bounds;

                Rectangle rp = waveformPlot1.GetBounds();
                if (e.X >= rp.Left && e.X <= rp.Right && e.Y >= rp.Top && e.Y <= rp.Bottom)
                {
                    //xCursor.XPosition = (e.X - rp.X) * (xAxes.Range.Maximum - xAxes.Range.Minimum) / (rp.Right - rp.Left);
                    xCursor.XPosition = (e.X - eBoundsX) * (xAxes.Range.Maximum - xAxes.Range.Minimum) / (eBoundsWidth) + xAxes.Range.Minimum;

                    //xCursor.YPosition = (e.Y - rp.Y) * (yAxes.Range.Minimum - yAxes.Range.Maximum) / (rp.Bottom - rp.Top);
                    xCursor.YPosition = (e.Y - eBoundsY) * (yAxes.Range.Minimum - yAxes.Range.Maximum) / (eBoundsHeight) + yAxes.Range.Maximum;
                }

                mouseDownPoint = -1;
                mouseUpPoint = -1;

                mouseDownXPoint = xCursor.XPosition;
            }
        }

        //Обновление подписей текущего курсора
        private void UpdateTextLabelsForCursor()
        {
            label9.Text = String.Format("{0}: {1} {2}", freq, SpectrxyCursor.XPosition.ToString("F4"), MHz);
            label10.Text = String.Format("{0}: {1} {2}", level, SpectrxyCursor.YPosition.ToString("F1"), dB);
        }

        private void labelLeft_Click(object sender, EventArgs e)
        {
            double xmin = xAxes.Range.Minimum - xAxes.Range.Interval;
            double xmax = xAxes.Range.Maximum - xAxes.Range.Interval;

            if (xmin < GlobalRangeMin)
            {
                double wshift = GlobalRangeMin - xmin;
                xmin = GlobalRangeMin;
                xmax = xmax + wshift;
            }

            xAxes.Range = new Range(xmin, xmax);
        }

        private void labelRight_Click(object sender, EventArgs e)
        {
            double xmin = xAxes.Range.Minimum + xAxes.Range.Interval;
            double xmax = xAxes.Range.Maximum + xAxes.Range.Interval;

            if (xmax > GlobalRangeMax)
            {
                double wshift = xmax - GlobalRangeMax;
                xmax = GlobalRangeMax;
                xmin = xmin - wshift;
            }

            xAxes.Range = new Range(xmin, xmax);
        }

        //Событие после перемещения линии на графике
        private void waveformGraph1_AfterMoveCursor(object sender, NationalInstruments.UI.AfterMoveXYCursorEventArgs e)
        {
            if (porogCursor.YPosition == e.YPosition)
            {
                if (e.YPosition < GlobalYRangeMax && e.YPosition > GlobalYRangeMin)
                {
                    int value = Convert.ToInt32(e.YPosition);
                    numericUpDown1.Value = (decimal)value;
                }
            }



            if (xCursor.XPosition == e.XPosition)
            {

                if (mouseDownPoint != -1)
                {
                    //новый синий график
                    if (mouseDownXPoint != -1)
                        mouseUpXPoint = e.XPosition;

                    wasPerformDoubleClick = true;
                }
            }
        }



        private TwoDouble[] ArrSE;
        private List<TwoDouble>[] ArrLTW;

        public void InitFHSSonRS(FhssJammingSetting[] fhssJammingSettings)
        {
            ArrSE = new TwoDouble[fhssJammingSettings.Count()];
            ArrLTW = new List<TwoDouble>[fhssJammingSettings.Count()];

            for (int i = 0; i < fhssJammingSettings.Count(); i++)
            {
                ArrSE[i].start = fhssJammingSettings[i].StartFrequency / 10000d;
                ArrSE[i].end = fhssJammingSettings[i].EndFrequency / 10000d;
                ArrLTW[i] = new List<TwoDouble>();

                SortedDictionary<int, int> SDii = new SortedDictionary<int, int>();
                for (int j = 0; j < fhssJammingSettings[i].FixedRadioSourceCount; j++)
                {
                    SDii.Add(fhssJammingSettings[i].FixedRadioSources[j].Frequency, fhssJammingSettings[i].FixedRadioSources[j].Bandwidth);
                }
                for (int j = 0; j < fhssJammingSettings[i].FixedRadioSourceCount; j++)
                {
                    double start = (SDii.ElementAt(j).Key - (SDii.ElementAt(j).Value / 2)) / 10000d;
                    double end = (SDii.ElementAt(j).Key + (SDii.ElementAt(j).Value / 2)) / 10000d;
                    ArrLTW[i].Add(new TwoDouble(start, end));
                }
            }

            //Переработка на наличие ненужных выколотых частот
            for (int i = 0; i < ArrLTW.Length; i++)
            {
                for (int j = 0; j < ArrLTW[i].Count(); j++)
                {
                    //1+2
                    if (j != -1)
                        if (ArrLTW[i][j].start < ArrSE[i].start && ArrLTW[i][j].end <= ArrSE[i].start)
                        {
                            ArrLTW[i].RemoveAt(j);
                            j--;
                        }

                    //3
                    if (j != -1)
                        if (ArrLTW[i][j].start <= ArrSE[i].start && (ArrLTW[i][j].end > ArrSE[i].start && ArrLTW[i][j].end < ArrSE[i].end))
                        {
                            ArrLTW[i][j] = new TwoDouble(ArrSE[i].start, ArrLTW[i][j].end);
                        }

                    //4+5+6 - Всё ок

                    //7
                    if (j != -1)
                        if ((ArrLTW[i][j].start > ArrSE[i].start && ArrLTW[i][j].start < ArrSE[i].end) && ArrLTW[i][j].end >= ArrSE[i].end)
                        {
                            ArrLTW[i][j] = new TwoDouble(ArrLTW[i][j].start, ArrSE[i].end);
                        }

                    //8+9
                    if (j != -1)
                        if (ArrLTW[i][j].start >= ArrSE[i].end && ArrLTW[i][j].end > ArrSE[i].end)
                        {
                            ArrLTW[i].RemoveAt(j);
                            j--;
                        }

                    //10
                    if (j != -1)
                        if (ArrLTW[i][j].start < ArrSE[i].start && ArrLTW[i][j].end > ArrSE[i].end)
                        {
                            var temp = ArrSE.ToList();
                            temp.RemoveAt(i);
                            ArrSE = temp.ToArray();
                        }

                }
            }


            //Переработка на наличие пересечений
            for (int i = 0; i < ArrLTW.Length; i++)
            {
                for (int j = 0; j < ArrLTW[i].Count(); j++)
                {
                    if (j + 1 < ArrLTW[i].Count())
                        if (ArrLTW[i][j].end >= ArrLTW[i][j + 1].start)
                        {
                            ArrLTW[i][j] = new TwoDouble(ArrLTW[i][j].start, ArrLTW[i][j + 1].end);
                            ArrLTW[i].RemoveAt(j + 1);
                            j--;
                        }
                }
            }
        }

        private struct TwoDouble
        {
            public double start;
            public double end;

            public TwoDouble(double start, double end)
            {
                this.start = start;
                this.end = end;
            }
        }

        private Rectangle CalcRectangle(NationalInstruments.UI.AfterDrawXYPlotEventArgs e, double XPoint1, double XPoint2, double YPoint1, double YPoint2)
        {
            //новый синий график
            int widthGraph = e.Bounds.Width;
            int heightGraph = e.Bounds.Height;
            int XGraph = e.Bounds.X;
            int YGraph = e.Bounds.Y;

            int segmentX = 0;
            int segmentY = 0;

            Rectangle Segment = new Rectangle();

            if (XPoint1 != XPoint2)
            {

                if (XPoint1 < XPoint2)
                    segmentX = Convert.ToInt32((XPoint1 - xAxes.Range.Minimum) * (widthGraph / xAxes.Range.Interval) + XGraph);

                if (XPoint2 < XPoint1)
                    segmentX = Convert.ToInt32((XPoint2 - xAxes.Range.Minimum) * (widthGraph / xAxes.Range.Interval) + XGraph);

                segmentY = Convert.ToInt32((-1) * (YPoint1 - yAxes.Range.Maximum) * (heightGraph / yAxes.Range.Interval) + YGraph);

                int Rwidth = Convert.ToInt32(Math.Abs(XPoint2 - XPoint1) * widthGraph / xAxes.Range.Interval);
                int Rheight = Convert.ToInt32(Math.Abs(YPoint2 - YPoint1) * heightGraph / yAxes.Range.Interval);

                Segment = new Rectangle(segmentX, segmentY, Rwidth, Rheight);
            }

            return Segment;

        }

        private void RecFHSSWG1_AfterDrawPlot(object sender, NationalInstruments.UI.AfterDrawXYPlotEventArgs e)
        {
            if (variableWork.Regime == 6)
            {
                SolidBrush brushWhite = new SolidBrush(Color.White);
                Pen penWhite = new Pen(brushWhite, 3.0f);

                if (ArrSE != null)
                {
                    for (int i = 0; i < ArrSE.Count(); i++)
                    {
                        if (ArrLTW[i].Count == 0)
                        {
                            Rectangle segment = CalcRectangle(e, ArrSE[i].start, ArrSE[i].end, -80.0, -120.0);
                            e.Graphics.DrawRectangle(penWhite, segment);
                        }
                        else
                            if (ArrLTW[i].Count == 1)
                            {
                                Rectangle segment = CalcRectangle(e, ArrSE[i].start, ArrLTW[i][0].start, -80.0, -120.0);
                                e.Graphics.DrawRectangle(penWhite, segment);

                                segment = CalcRectangle(e, ArrLTW[i][0].end, ArrSE[i].end, -80.0, -120.0);
                                e.Graphics.DrawRectangle(penWhite, segment);
                            }
                            else
                                for (int j = 0; j < ArrLTW[i].Count; j++)
                                {
                                    if (j == 0)
                                    {
                                        Rectangle segment = CalcRectangle(e, ArrSE[i].start, ArrLTW[i][j].start, -80.0, -120.0);
                                        e.Graphics.DrawRectangle(penWhite, segment);

                                    }
                                    if ((0 < j) && (j < ArrLTW[i].Count - 1))
                                    {
                                        Rectangle segment = CalcRectangle(e, ArrLTW[i][j - 1].end, ArrLTW[i][j].start, -80.0, -120.0);
                                        e.Graphics.DrawRectangle(penWhite, segment);

                                    }
                                    if (j == ArrLTW[i].Count - 1)
                                    {
                                        Rectangle segment = CalcRectangle(e, ArrLTW[i][j - 1].end, ArrLTW[i][j].start, -80.0, -120.0);
                                        e.Graphics.DrawRectangle(penWhite, segment);

                                        segment = CalcRectangle(e, ArrLTW[i][j].end, ArrSE[i].end, -80.0, -120.0);
                                        e.Graphics.DrawRectangle(penWhite, segment);
                                    }
                                }
                    }
                }
            }

        }

        //Событие после отрисоки графика
        private void waveformGraph1_AfterDrawPlot(object sender, NationalInstruments.UI.AfterDrawXYPlotEventArgs e)
        {

            //porogCursor.XPosition = (waveformPlot1.XAxis.Range.Maximum - waveformPlot1.XAxis.Range.Minimum) / 10.0 * 9.0 + waveformPlot1.XAxis.Range.Minimum; //для отображения надписи

            //Старый красный график
            eBoundsWidth = e.Bounds.Width;
            eBoundsHeight = e.Bounds.Height;
            eBoundsX = e.Bounds.X;
            eBoundsY = e.Bounds.Y;

            //новый синий график
            int widthGraph = e.Bounds.Width;
            int heightGraph = e.Bounds.Height;
            int XGraph = e.Bounds.X;
            int YGraph = e.Bounds.Y;

            int segmentX = 0;
            int segmentY = 0;

            if (mouseDownXPoint != -1 && mouseUpXPoint != -1)
            {

                if (mouseDownXPoint != mouseUpXPoint)
                {

                    if (mouseDownXPoint < mouseUpXPoint)
                        segmentX = Convert.ToInt32((mouseDownXPoint - xAxes.Range.Minimum) * (widthGraph / xAxes.Range.Interval) + XGraph);

                    if (mouseUpXPoint < mouseDownXPoint)
                        segmentX = Convert.ToInt32((mouseUpXPoint - xAxes.Range.Minimum) * (widthGraph / xAxes.Range.Interval) + XGraph);

                    Rectangle segment = new Rectangle(
                        segmentX,
                        e.Bounds.Y,
                        Convert.ToInt32(Math.Abs(mouseUpXPoint - mouseDownXPoint) * widthGraph / xAxes.Range.Interval),
                        e.Bounds.Height);

                    SolidBrush brush = new SolidBrush(Color.FromArgb(128, 0, 0, 255));
                    e.Graphics.FillRectangle(brush, segment);
                }
            }

            //Надо быть очень осторожным с waspressright2
            if (waspressright == true && waspressright2 == true)
            {
                if (mpsxright < mppxright)
                    segmentX = Convert.ToInt32((mpsxright - xAxes.Range.Minimum) * (widthGraph / xAxes.Range.Interval) + XGraph);

                if (mppxright < mpsxright)
                    segmentX = Convert.ToInt32((mppxright - xAxes.Range.Minimum) * (widthGraph / xAxes.Range.Interval) + XGraph);

                if (mpsyright > mppyright)
                    //segmentY = Convert.ToInt32((mpsyright - yAxes.Range.Minimum) * (heightGraph / yAxes.Range.Interval) + YGraph); // old
                    segmentY = Convert.ToInt32((yAxes.Range.Maximum - mpsyright) * (heightGraph / yAxes.Range.Interval) + YGraph); //new

                if (mppyright > mpsyright)
                    //segmentY = Convert.ToInt32((mppyright - yAxes.Range.Minimum) * (heightGraph / yAxes.Range.Interval) + YGraph); //old
                    segmentY = Convert.ToInt32((yAxes.Range.Maximum - mppyright) * (heightGraph / yAxes.Range.Interval) + YGraph); //new

                /*
                Rectangle segmentq = new Rectangle(
                    segmentX,
                    segmentY,
                    Convert.ToInt32(Math.Abs(mppxright - mpsxright) * widthGraph / xAxes.Range.Interval),
                    Convert.ToInt32(Math.Abs(mppyright - mpsyright) * heightGraph / yAxes.Range.Interval)
                    );
                */

                Rectangle segmentq = new Rectangle(
                        segmentX,
                        e.Bounds.Y,
                        Convert.ToInt32(Math.Abs(mppxright - mpsxright) * widthGraph / xAxes.Range.Interval),
                        e.Bounds.Height - 2);


                //SolidBrush brushq = new SolidBrush(Color.FromArgb(128, 255, 0, 0));
                //e.Graphics.FillRectangle(brushq, segmentq);

                e.Graphics.DrawRectangle(new Pen(Color.Red), segmentq);

            }

            RecFHSSWG1_AfterDrawPlot(sender, e);

        }

        bool waspressleft = false;
        double mpsxleft;
        double mpsyleft;
        bool waspressright = false;
        bool wasrightmove = false;

        double mpsxright;
        double mpsyright;
        double mppxright;
        double mppyright;

        bool waspressright2 = false;

        //Нажатие кнопки мыши
        private void waveformGraph1_MouseDown(object sender, MouseEventArgs e)
        {
            if ((Control.ModifierKeys & Keys.Control) == Keys.Control || (Control.ModifierKeys & Keys.Shift) == Keys.Shift)
            {
                return;
            }


            Rectangle rp = waveformPlot1.GetBounds();
            if (e.X >= rp.Left && e.X <= rp.Right && e.Y >= rp.Top && e.Y <= rp.Bottom)
            {
                mouseDownPoint = e.X;

                if (e.Button == System.Windows.Forms.MouseButtons.Left)
                {
                    mpsxleft = (e.X - eBoundsX) * (xAxes.Range.Maximum - xAxes.Range.Minimum) / (eBoundsWidth) + xAxes.Range.Minimum;
                    //mpsyleft = (e.Y - eBoundsY) * (yAxes.Range.Maximum - yAxes.Range.Minimum) / (eBoundsHeight) + yAxes.Range.Minimum; //old
                    mpsyleft = yAxes.Range.Maximum - (e.Y - eBoundsY) * (yAxes.Range.Maximum - yAxes.Range.Minimum) / (eBoundsHeight); //new
                    waspressleft = true;

                    //this.Cursor = Cursors.SizeAll;
                }
                if (e.Button == System.Windows.Forms.MouseButtons.Right)
                {
                    mpsxright = (e.X - eBoundsX) * (xAxes.Range.Maximum - xAxes.Range.Minimum) / (eBoundsWidth) + xAxes.Range.Minimum;
                    //mpsyright = (e.Y - eBoundsY) * (yAxes.Range.Maximum - yAxes.Range.Minimum) / (eBoundsHeight) + yAxes.Range.Minimum; //old
                    //mpsyright = yAxes.Range.Maximum - (e.Y - eBoundsY) * (yAxes.Range.Maximum - yAxes.Range.Minimum) / (eBoundsHeight); //new
                    mpsyright = -120;
                    waspressright = true;
                }
            }

            if (xCursor.XPosition == mpsxleft)
                wasPerformDoubleClick = true;



        }

        //Отжатие кнопки мыши
        private void waveformGraph1_MouseUp(object sender, MouseEventArgs e)
        {
            if ((Control.ModifierKeys & Keys.Control) == Keys.Control || (Control.ModifierKeys & Keys.Shift) == Keys.Shift)
            {
                return;
            }



            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                Rectangle rp = waveformPlot1.GetBounds();
                if (e.X >= rp.Left && e.X <= rp.Right && e.Y >= rp.Top && e.Y <= rp.Bottom)
                {
                    mouseUpPoint = e.X;
                }

                if (xAxes.Range.Minimum > GlobalRangeMax || xAxes.Range.Maximum < GlobalRangeMin)
                {
                    waveformPlot1.XAxis.Range = new Range(GlobalRangeMin, GlobalRangeMax);
                }
                if (yAxes.Range.Minimum > GlobalYRangeMax || yAxes.Range.Maximum < GlobalYRangeMin)
                {
                    waveformPlot1.YAxis.Range = new Range(GlobalYRangeMin, GlobalYRangeMax);
                }

                waspressleft = false;
                //this.Cursor = Cursors.Default;
            }
            if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                waspressright = false;
                if (wasrightmove)
                {
                    if (mpsxright != mppxright && mpsyright != mppyright)
                    {
                        xAxes.Range = new Range(Math.Min(mpsxright, mppxright), Math.Max(mpsxright, mppxright));
                        yAxes.Range = new Range(Math.Min(mpsyright, mppyright), Math.Max(mpsyright, mppyright));
                    }
                }
                wasrightmove = false;
            }

            wasPerformDoubleClick = false;

            if (e.Button == System.Windows.Forms.MouseButtons.Right)
                waspressright2 = false;
        }

        //Перемещение мыши по PlotArea
        private void waveformGraph1_PlotAreaMouseMove(object sender, MouseEventArgs e)
        {
            if ((Control.ModifierKeys & Keys.Control) == Keys.Control || (Control.ModifierKeys & Keys.Shift) == Keys.Shift)
            {
                return;
            }

            if (wasPerformDoubleClick == true)
                waspressleft = false;

            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                if (waspressleft)
                {
                    double mppxleft = (e.X - eBoundsX) * (xAxes.Range.Maximum - xAxes.Range.Minimum) / (eBoundsWidth) + xAxes.Range.Minimum;

                    if (this.xAxes.Range.Minimum == GlobalRangeMin && this.xAxes.Range.Maximum == GlobalRangeMax)
                        return;

                    if (mpsxleft < mppxleft)
                    {
                        double minimum = this.xAxes.Range.Minimum - Math.Abs(mppxleft - mpsxleft);
                        double maximum = this.xAxes.Range.Maximum - Math.Abs(mppxleft - mpsxleft);

                        if (minimum < GlobalRangeMin)
                        {
                            double temp = GlobalRangeMin - minimum;
                            minimum = GlobalRangeMin;
                            if (maximum + temp < GlobalRangeMax)
                                maximum = maximum + temp;
                        }
                        if (maximum > GlobalRangeMax)
                        {
                            double temp = maximum - GlobalRangeMax;
                            maximum = GlobalRangeMax;
                            if (minimum + temp < GlobalRangeMin)
                                minimum = minimum + temp;
                        }

                        //xAxes.Range = new Range(this.xAxes.Range.Minimum - Math.Abs(mppxleft - mpsxleft), this.xAxes.Range.Maximum - Math.Abs(mppxleft - mpsxleft));
                        xAxes.Range = new Range(minimum, maximum);
                    }
                    if (mpsxleft > mppxleft)
                    {
                        double minimum = this.xAxes.Range.Minimum + Math.Abs(mppxleft - mpsxleft);
                        double maximum = this.xAxes.Range.Maximum + Math.Abs(mppxleft - mpsxleft);

                        if (maximum > GlobalRangeMax)
                        {
                            double temp = GlobalRangeMax - maximum;
                            maximum = GlobalRangeMax;
                            //if (minimum + temp < GlobalRangeMin)
                            minimum = minimum + temp;
                        }
                        if (minimum < GlobalRangeMin)
                        {
                            double temp = GlobalRangeMin - minimum;
                            minimum = GlobalRangeMin;
                            if (maximum + temp < GlobalRangeMax)
                                maximum = maximum + temp;
                        }

                        //xAxes.Range = new Range(this.xAxes.Range.Minimum + Math.Abs(mppxleft - mpsxleft), this.xAxes.Range.Maximum + Math.Abs(mppxleft - mpsxleft));
                        xAxes.Range = new Range(minimum, maximum);
                    }

                    double mppyleft = yAxes.Range.Maximum - (e.Y - eBoundsY) * (yAxes.Range.Maximum - yAxes.Range.Minimum) / (eBoundsHeight); //new

                    if (this.yAxes.Range.Minimum == GlobalYRangeMin && this.yAxes.Range.Maximum == GlobalYRangeMax)
                        return;

                    if (porogCursor.YPosition - 1 <= mppyleft && porogCursor.YPosition + 1 >= mppyleft)
                        return;

                    if (mpsyleft < mppyleft)
                    {
                        double minimum = this.yAxes.Range.Minimum - Math.Abs(mppyleft - mpsyleft);
                        double maximum = this.yAxes.Range.Maximum - Math.Abs(mppyleft - mpsyleft);

                        if (minimum < GlobalYRangeMin)
                        {
                            double temp = GlobalYRangeMin - minimum;
                            minimum = GlobalYRangeMin;
                            if (maximum + temp < GlobalYRangeMax)
                                maximum = maximum + temp;
                        }
                        if (maximum > GlobalYRangeMax)
                        {
                            double temp = maximum - GlobalYRangeMax;
                            maximum = GlobalYRangeMax;
                            if (minimum + temp < GlobalYRangeMin)
                                minimum = minimum + temp;
                        }

                        yAxes.Range = new Range(minimum, maximum);
                    }
                    if (mpsyleft > mppyleft)
                    {
                        double minimum = this.yAxes.Range.Minimum + Math.Abs(mppyleft - mpsyleft);
                        double maximum = this.yAxes.Range.Maximum + Math.Abs(mppyleft - mpsyleft);

                        if (maximum > GlobalYRangeMax)
                        {
                            double temp = GlobalYRangeMax - maximum;
                            maximum = GlobalYRangeMax;
                            //if (minimum + temp < GlobalYRangeMin)
                            minimum = minimum + temp;
                        }
                        if (minimum < GlobalYRangeMin)
                        {
                            double temp = GlobalYRangeMin - minimum;
                            minimum = GlobalYRangeMin;
                            //if (maximum + temp < GlobalYRangeMax)
                            maximum = maximum + temp;
                        }

                        yAxes.Range = new Range(minimum, maximum);
                    }

                }
            }

            if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                if (waspressright)
                {
                    wasrightmove = true;

                    mppxright = (e.X - eBoundsX) * (xAxes.Range.Maximum - xAxes.Range.Minimum) / (eBoundsWidth) + xAxes.Range.Minimum;
                    //mppyright = (e.Y - eBoundsY) * (yAxes.Range.Maximum - yAxes.Range.Minimum) / (eBoundsHeight) + yAxes.Range.Minimum; //old
                    //mppyright = yAxes.Range.Maximum - (e.Y - eBoundsY) * (yAxes.Range.Maximum - yAxes.Range.Minimum) / (eBoundsHeight); //new
                    mppyright = 0;



                    int widthGraph = waveformGraph1.PlotAreaBounds.Width;
                    int heightGraph = waveformGraph1.PlotAreaBounds.Height;
                    int XGraph = waveformGraph1.PlotAreaBounds.X;
                    int YGraph = waveformGraph1.PlotAreaBounds.Y;

                    int segmentX = 0;
                    int segmentY = 0;

                    if (mpsxright < mppxright)
                        segmentX = Convert.ToInt32((mpsxright - xAxes.Range.Minimum) * (widthGraph / xAxes.Range.Interval) + XGraph);

                    if (mppxright < mpsxright)
                        segmentX = Convert.ToInt32((mppxright - xAxes.Range.Minimum) * (widthGraph / xAxes.Range.Interval) + XGraph);

                    if (mpsyright > mppyright)
                        //segmentY = Convert.ToInt32((mpsyright - yAxes.Range.Minimum) * (heightGraph / yAxes.Range.Interval) + YGraph);
                        segmentY = Convert.ToInt32((yAxes.Range.Maximum - mpsyright) * (heightGraph / yAxes.Range.Interval) + YGraph);

                    if (mppyright > mpsyright)
                        //segmentY = Convert.ToInt32((mppyright - yAxes.Range.Minimum) * (heightGraph / yAxes.Range.Interval) + YGraph);
                        segmentY = Convert.ToInt32((yAxes.Range.Maximum - mppyright) * (heightGraph / yAxes.Range.Interval) + YGraph);

                    /*
                    Rectangle segment = new Rectangle(
                        segmentX,
                        segmentY,
                        Convert.ToInt32(Math.Abs(mppxright - mpsxright) * widthGraph / xAxes.Range.Interval),
                        Convert.ToInt32(Math.Abs(mppyright - mpsyright) * heightGraph / yAxes.Range.Interval)
                        );
                    */

                    Rectangle segment = new Rectangle(
                       segmentX,
                       waveformPlot1.GetBounds().Top,
                       Convert.ToInt32(Math.Abs(mppxright - mpsxright) * widthGraph / xAxes.Range.Interval),
                       waveformPlot1.GetBounds().Height - 2);


                    //SolidBrush brush = new SolidBrush(Color.FromArgb(128, 255, 0, 0));
                    Graphics g = waveformGraph1.CreateGraphics();
                    //g.FillRectangle(brush, segment);

                    g.DrawRectangle(new Pen(Color.Red), segment);
                    waveformGraph1.Refresh();
                }
                waspressright2 = true;
            }


        }

        //Обработка колёсика
        private void waveformGraph1_MouseWheel(object sender, MouseEventArgs e)
        {
            if ((Control.ModifierKeys & Keys.Control) == Keys.Control || (Control.ModifierKeys & Keys.Shift) == Keys.Shift)
            {
                return;
            }

            if (e.Delta != 0)
            {
                Rectangle rp = waveformPlot1.GetBounds();
                if (e.X >= rp.Left && e.X <= rp.Right && e.Y >= rp.Top && e.Y <= rp.Bottom)
                {
                    if (e.Delta > 0) //колесо вперёд
                    {
                        double xpos = (e.X - eBoundsX) * (xAxes.Range.Maximum - xAxes.Range.Minimum) / (eBoundsWidth) + xAxes.Range.Minimum;

                        double ix = xAxes.Range.Interval / 1.2;
                        double iy = yAxes.Range.Interval / 1.2;

                        double corx = (xAxes.Range.Interval - ix) / 2.0;
                        double cory = (yAxes.Range.Interval - iy) / 2.0;

                        double minimum = this.xAxes.Range.Minimum + corx;
                        double maximum = this.xAxes.Range.Maximum - corx;

                        double procleft = Math.Abs(xpos - this.xAxes.Range.Minimum) / this.xAxes.Range.Interval;
                        double procright = Math.Abs(xpos - this.xAxes.Range.Maximum) / this.xAxes.Range.Interval;

                        if (procleft < procright)
                        {
                            minimum = this.xAxes.Range.Minimum + corx * procleft;
                            maximum = this.xAxes.Range.Maximum - corx * (1.0 + procright);
                        }
                        if (procleft > procright)
                        {
                            minimum = this.xAxes.Range.Minimum + corx * (1.0 + procleft);
                            maximum = this.xAxes.Range.Maximum - corx * procright;
                        }

                        if (minimum < GlobalRangeMin)
                        {
                            double temp = GlobalRangeMin - minimum;
                            minimum = GlobalRangeMin;
                            if (maximum + temp < GlobalRangeMax)
                                maximum = maximum + temp;
                        }
                        if (maximum > GlobalRangeMax)
                        {
                            double temp = maximum - GlobalRangeMax;
                            maximum = GlobalRangeMax;
                            if (minimum + temp < GlobalRangeMin)
                                minimum = minimum + temp;
                        }

                        //xAxes.Range = new Range(this.xAxes.Range.Minimum + corx, this.xAxes.Range.Maximum - corx);

                        if (xAxes.Range.Interval != GlobalBandWidthMHz)
                            xAxes.Range = new Range(minimum, maximum);
                        //yAxes.Range = new Range(this.yAxes.Range.Minimum + cory, this.yAxes.Range.Maximum - cory);

                    }
                    if (e.Delta < 0) //колесо назад
                    {
                        double xpos = (e.X - eBoundsX) * (xAxes.Range.Maximum - xAxes.Range.Minimum) / (eBoundsWidth) + xAxes.Range.Minimum;

                        double ix = xAxes.Range.Interval * 1.2;
                        double iy = yAxes.Range.Interval * 1.2;

                        double corx = (ix - xAxes.Range.Interval) / 2.0;
                        double cory = (iy - yAxes.Range.Interval) / 2.0;

                        double minimum = this.xAxes.Range.Minimum - corx;
                        double maximum = this.xAxes.Range.Maximum + corx;

                        double procleft = Math.Abs(xpos - this.xAxes.Range.Minimum) / this.xAxes.Range.Interval;
                        double procright = Math.Abs(xpos - this.xAxes.Range.Maximum) / this.xAxes.Range.Interval;

                        if (procleft < procright)
                        {
                            minimum = this.xAxes.Range.Minimum - corx * procleft;
                            maximum = this.xAxes.Range.Maximum + corx * (1.0 + procright);
                        }
                        if (procleft > procright)
                        {
                            minimum = this.xAxes.Range.Minimum - corx * (1.0 + procleft);
                            maximum = this.xAxes.Range.Maximum + corx * procright;
                        }



                        if (maximum > GlobalRangeMax)
                        {
                            double temp = maximum - GlobalRangeMax;
                            maximum = GlobalRangeMax;
                            //if (minimum + temp < GlobalRangeMin)
                            minimum = minimum - temp;
                        }
                        if (minimum < GlobalRangeMin)
                        {
                            double temp = GlobalRangeMin - minimum;
                            minimum = GlobalRangeMin;
                            if (maximum + temp < GlobalRangeMax)
                                maximum = maximum + temp;
                        }


                        //xAxes.Range = new Range(this.xAxes.Range.Minimum - corx, this.xAxes.Range.Maximum + corx);
                        xAxes.Range = new Range(minimum, maximum);
                        //yAxes.Range = new Range(this.yAxes.Range.Minimum - cory, this.yAxes.Range.Maximum + cory);
                    }
                }
            }

            if (xAxes.Range.Maximum - xAxes.Range.Minimum < GlobalBandWidthMHz)
            {
                double center = (xAxes.Range.Maximum + xAxes.Range.Minimum) / 2;
                waveformPlot1.XAxis.Range = new Range(center - GlobalBandWidthMHz / 2, center + GlobalBandWidthMHz / 2);
            }

            if (xAxes.Range.Minimum > GlobalRangeMax || xAxes.Range.Maximum < GlobalRangeMin)
            {
                waveformPlot1.XAxis.Range = new Range(GlobalRangeMin, GlobalRangeMax);
            }

            if (yAxes.Range.Minimum > GlobalYRangeMax || yAxes.Range.Maximum < GlobalYRangeMin)
            {
                waveformPlot1.YAxis.Range = new Range(GlobalYRangeMin, GlobalYRangeMax);
            }

            if (xAxes.Range.Maximum - xAxes.Range.Minimum > GlobalRangeMax - GlobalRangeMin)
            {
                button3.PerformClick();
            }

            if (yAxes.Range.Maximum - yAxes.Range.Minimum > (-1) * GlobalYRangeMin)
            {
                button3.PerformClick();
            }

        }

        //Обработка колёсика 2
        private void waveformGraph1_PlotAreaMouseWheel2(object sender, MouseEventArgs e)
        {
            if ((Control.ModifierKeys & Keys.Control) == Keys.Control || (Control.ModifierKeys & Keys.Shift) == Keys.Shift)
            {
                return;
            }

            if (e.Delta != 0)
            {
                Rectangle rp = waveformPlot1.GetBounds();
                if (e.X >= rp.Left && e.X <= rp.Right && e.Y >= rp.Top && e.Y <= rp.Bottom)
                {
                    if (e.Delta > 0) //колесо вперёд
                    {
                        double xpos = (e.X - eBoundsX) * (xAxes.Range.Maximum - xAxes.Range.Minimum) / (eBoundsWidth) + xAxes.Range.Minimum;

                        double ix = xAxes.Range.Interval / 1.2;
                        double iy = yAxes.Range.Interval / 1.2;

                        double corx = (xAxes.Range.Interval - ix) / 2.0;
                        double cory = (yAxes.Range.Interval - iy) / 2.0;

                        double minimum = this.xAxes.Range.Minimum + corx;
                        double maximum = this.xAxes.Range.Maximum - corx;

                        double procleft = Math.Abs(xpos - this.xAxes.Range.Minimum) / this.xAxes.Range.Interval;
                        double procright = Math.Abs(xpos - this.xAxes.Range.Maximum) / this.xAxes.Range.Interval;

                        if (procleft < procright)
                        {
                            minimum = this.xAxes.Range.Minimum + corx * procleft;
                            maximum = this.xAxes.Range.Maximum - corx * (1.0 + procright);
                        }
                        if (procleft > procright)
                        {
                            minimum = this.xAxes.Range.Minimum + corx * (1.0 + procleft);
                            maximum = this.xAxes.Range.Maximum - corx * procright;
                        }

                        if (minimum < GlobalRangeMin)
                        {
                            double temp = GlobalRangeMin - minimum;
                            minimum = GlobalRangeMin;
                            if (maximum + temp < GlobalRangeMax)
                                maximum = maximum + temp;
                        }
                        if (maximum > GlobalRangeMax)
                        {
                            double temp = maximum - GlobalRangeMax;
                            maximum = GlobalRangeMax;
                            if (minimum + temp < GlobalRangeMin)
                                minimum = minimum + temp;
                        }

                        //xAxes.Range = new Range(this.xAxes.Range.Minimum + corx, this.xAxes.Range.Maximum - corx);

                        if (xAxes.Range.Interval != GlobalBandWidthMHz)
                            xAxes.Range = new Range(minimum, maximum);
                        //yAxes.Range = new Range(this.yAxes.Range.Minimum + cory, this.yAxes.Range.Maximum - cory);

                    }
                    if (e.Delta < 0) //колесо назад
                    {
                        double xpos = (e.X - eBoundsX) * (xAxes.Range.Maximum - xAxes.Range.Minimum) / (eBoundsWidth) + xAxes.Range.Minimum;

                        double ix = xAxes.Range.Interval * 1.2;
                        double iy = yAxes.Range.Interval * 1.2;

                        double corx = (ix - xAxes.Range.Interval) / 2.0;
                        double cory = (iy - yAxes.Range.Interval) / 2.0;

                        double minimum = this.xAxes.Range.Minimum - corx;
                        double maximum = this.xAxes.Range.Maximum + corx;

                        double procleft = Math.Abs(xpos - this.xAxes.Range.Minimum) / this.xAxes.Range.Interval;
                        double procright = Math.Abs(xpos - this.xAxes.Range.Maximum) / this.xAxes.Range.Interval;

                        if (procleft < procright)
                        {
                            minimum = this.xAxes.Range.Minimum - corx * procleft;
                            maximum = this.xAxes.Range.Maximum + corx * (1.0 + procright);
                        }
                        if (procleft > procright)
                        {
                            minimum = this.xAxes.Range.Minimum - corx * (1.0 + procleft);
                            maximum = this.xAxes.Range.Maximum + corx * procright;
                        }



                        if (maximum > GlobalRangeMax)
                        {
                            double temp = maximum - GlobalRangeMax;
                            maximum = GlobalRangeMax;
                            //if (minimum + temp < GlobalRangeMin)
                            minimum = minimum - temp;
                        }
                        if (minimum < GlobalRangeMin)
                        {
                            double temp = GlobalRangeMin - minimum;
                            minimum = GlobalRangeMin;
                            if (maximum + temp < GlobalRangeMax)
                                maximum = maximum + temp;
                        }


                        //xAxes.Range = new Range(this.xAxes.Range.Minimum - corx, this.xAxes.Range.Maximum + corx);
                        xAxes.Range = new Range(minimum, maximum);
                        //yAxes.Range = new Range(this.yAxes.Range.Minimum - cory, this.yAxes.Range.Maximum + cory);
                    }
                }
            }

            if (xAxes.Range.Maximum - xAxes.Range.Minimum < GlobalBandWidthMHz)
            {
                double center = (xAxes.Range.Maximum + xAxes.Range.Minimum) / 2;
                waveformPlot1.XAxis.Range = new Range(center - GlobalBandWidthMHz / 2, center + GlobalBandWidthMHz / 2);
            }

            if (xAxes.Range.Minimum > GlobalRangeMax || xAxes.Range.Maximum < GlobalRangeMin)
            {
                waveformPlot1.XAxis.Range = new Range(GlobalRangeMin, GlobalRangeMax);
            }

            if (yAxes.Range.Minimum > GlobalYRangeMax || yAxes.Range.Maximum < GlobalYRangeMin)
            {
                waveformPlot1.YAxis.Range = new Range(GlobalYRangeMin, GlobalYRangeMax);
            }

            if (xAxes.Range.Maximum - xAxes.Range.Minimum > GlobalRangeMax - GlobalRangeMin)
            {
                button3.PerformClick();
            }

            if (yAxes.Range.Maximum - yAxes.Range.Minimum > (-1) * GlobalYRangeMin)
            {
                button3.PerformClick();
            }
        }

        //Обработка колёсика 3
        private void waveformGraph1_PlotAreaMouseWheel(object sender, MouseEventArgs e)
        {
            if ((Control.ModifierKeys & Keys.Control) == Keys.Control || (Control.ModifierKeys & Keys.Shift) == Keys.Shift)
            {
                return;
            }

            double MinVisibleRangeMhz = 0.25; // 250 килоГерц 

            var range = waveformGraph1.XAxes[0].Range;
            var size = (range.Maximum - range.Minimum) / 2;
            var s = e.Delta < 0 ? 1.2f : 1 / 1.2f;
            size *= s;
            if (size < 0.5 * MinVisibleRangeMhz)
            {
                size = 0.5 * MinVisibleRangeMhz;
            }

            var pos = 1d * (waveformGraph1.PointToClient(System.Windows.Forms.Cursor.Position).X - waveformGraph1.PlotAreaBounds.Left) / waveformGraph1.PlotAreaBounds.Width;
            var mouseFrequencyMhz = range.Minimum + range.Interval * pos;
            var center = size + mouseFrequencyMhz - 2 * pos * size;

            var left = center - size;
            var right = center + size;
            if (left < GlobalRangeMin)
            {
                left = GlobalRangeMin;
            }
            if (right > GlobalRangeMax)
            {
                right = GlobalRangeMax;
            }
            waveformGraph1.XAxes[0].Range = new Range(left, right);
        }

        //Функция выполнения исполнительного пеленгования
        private async void PerfomExecutiveDFwithPixelCorrection()
        {
            int PixelCorrection = 15;

            double xPosStart = (mouseDownPoint - eBoundsX - PixelCorrection) * (xAxes.Range.Maximum - xAxes.Range.Minimum) / (eBoundsWidth) + xAxes.Range.Minimum;
            double xPosEnd = (mouseUpPoint - eBoundsX + PixelCorrection) * (xAxes.Range.Maximum - xAxes.Range.Minimum) / (eBoundsWidth) + xAxes.Range.Minimum;

            if (xPosStart < GlobalRangeMin)
                xPosStart = GlobalRangeMin;

            if (xPosEnd > GlobalRangeMax)
                xPosStart = GlobalRangeMax;

            if (xPosStart != xPosEnd)
            {
                if (xPosEnd < xPosStart)
                {
                    double temp = xPosStart;
                    xPosStart = xPosEnd;
                    xPosEnd = temp;
                }
            }

            //ExecutiveDFResponse answer = dsp.ExecutiveDF(xPosStart, xPosEnd, 3, 3);
            ExecutiveDFResponse answer = await VariableWork.aWPtoBearingDSPprotocolNew.ExecutiveDF(xPosStart, xPosEnd, 3, 3);


            if (answer != null)
            {
                Flabel.Text = String.Format("F = {0} MHz", answer.Frequency / 10000.0);
                Qlabel.Text = String.Format("Q = {0}°", answer.Direction / 10.0);
                SDlabel.Text = String.Format("SD = {0}", answer.StandardDeviation);
                Plabel.Text = String.Format("% = {0}", answer.DiscardedDirectionPercent);

                Flabel.Location = new Point(scatterGraph2.Location.X - Flabel.Width - 5, Flabel.Location.Y);
                Qlabel.Location = new Point(Flabel.Location.X, Qlabel.Location.Y);
                SDlabel.Location = new Point(Flabel.Location.X, SDlabel.Location.Y);
                Plabel.Location = new Point(Flabel.Location.X, Plabel.Location.Y);

                double angle = (answer.Direction / 10.0);
                //ThirdInitScatterGraph2(angle);
                ThirdInitScatterGraph2(0.5, angle);
                FourthInitScatterGraph2(answer.CorrelationHistogram);
            }

        }
        private async void PerfomExecutiveDFwithPixelCorrection(int PixelCorrection)
        {
            if (PixelCorrection > eBoundsWidth)
                PixelCorrection = 15;

            double xPosStart = (mouseDownPoint - eBoundsX - PixelCorrection) * (xAxes.Range.Maximum - xAxes.Range.Minimum) / (eBoundsWidth) + xAxes.Range.Minimum;
            double xPosEnd = (mouseUpPoint - eBoundsX + PixelCorrection) * (xAxes.Range.Maximum - xAxes.Range.Minimum) / (eBoundsWidth) + xAxes.Range.Minimum;

            if (xPosStart < GlobalRangeMin)
                xPosStart = GlobalRangeMin;

            if (xPosEnd > GlobalRangeMax)
                xPosStart = GlobalRangeMax;

            if (xPosStart != xPosEnd)
            {
                if (xPosEnd < xPosStart)
                {
                    double temp = xPosStart;
                    xPosStart = xPosEnd;
                    xPosEnd = temp;
                }
            }

            //ExecutiveDFResponse answer = dsp.ExecutiveDF(xPosStart, xPosEnd, 3, 3);
            ExecutiveDFResponse answer = await VariableWork.aWPtoBearingDSPprotocolNew.ExecutiveDF(xPosStart, xPosEnd, 3, 3);

            if (answer != null)
            {
                Flabel.Text = String.Format("F = {0} MHz", answer.Frequency / 10000.0);
                Qlabel.Text = String.Format("Q = {0}°", answer.Direction / 10.0);
                SDlabel.Text = String.Format("SD = {0}", answer.StandardDeviation);
                Plabel.Text = String.Format("% = {0}", answer.DiscardedDirectionPercent);

                Flabel.Location = new Point(scatterGraph2.Location.X - Flabel.Width - 5, Flabel.Location.Y);
                Qlabel.Location = new Point(Flabel.Location.X, Qlabel.Location.Y);
                SDlabel.Location = new Point(Flabel.Location.X, SDlabel.Location.Y);
                Plabel.Location = new Point(Flabel.Location.X, Plabel.Location.Y);

                double angle = (answer.Direction / 10.0);
                //ThirdInitScatterGraph2(angle);
                ThirdInitScatterGraph2(0.5, angle);
                FourthInitScatterGraph2(answer.CorrelationHistogram);
            }
        }

        //Функция выполнения исполнительного пеленгования 2
        private async void PerfomExecutiveDFwithPixelCorrection2(int PixelCorrection, int PhaseAveragingCount, int DirectionAveragingCount)
        {
            if (isServConnected == true)
            {

                if (PixelCorrection > eBoundsWidth)
                    PixelCorrection = 15;

                double xPosStart = (mouseDownPoint - eBoundsX - PixelCorrection) * (xAxes.Range.Maximum - xAxes.Range.Minimum) / (eBoundsWidth) + xAxes.Range.Minimum;
                double xPosEnd = (mouseUpPoint - eBoundsX + PixelCorrection) * (xAxes.Range.Maximum - xAxes.Range.Minimum) / (eBoundsWidth) + xAxes.Range.Minimum;

                if (xPosStart < GlobalRangeMin)
                    xPosStart = GlobalRangeMin;

                if (xPosEnd > GlobalRangeMax)
                    xPosStart = GlobalRangeMax;

                if (xPosStart != xPosEnd)
                {
                    if (xPosEnd < xPosStart)
                    {
                        double temp = xPosStart;
                        xPosStart = xPosEnd;
                        xPosEnd = temp;
                    }
                }

                ExecutiveDFResponse answer = await VariableWork.aWPtoBearingDSPprotocolNew.ExecutiveDF(xPosStart, xPosEnd, PhaseAveragingCount, DirectionAveragingCount);

                if (answer != null)
                {
                    if (answer.Header.ErrorCode == 0)
                    {

                        //Flabel.Text = String.Format("F = {0} MHz", answer.Frequency / 10000.0);
                        Flabel.Text = String.Format("{0} {1}", flabel, answer.Frequency / 10000.0);
                        //Qlabel.Text = String.Format("Q = {0}°", answer.Direction / 10.0);
                        Qlabel.Text = String.Format("{0} {1}", qlabel, answer.Direction / 10.0);
                        //SDlabel.Text = String.Format("SD = {0}", answer.StandardDeviation);
                        SDlabel.Text = String.Format("{0} {1}", sdlabel, answer.StandardDeviation);
                        //Plabel.Text = String.Format("% = {0}", answer.DiscardedDirectionPercent);
                        Plabel.Text = String.Format("{0} {1}", plabel, answer.DiscardedDirectionPercent);

                        //Flabel.Location = new Point(scatterGraph2.Location.X - Flabel.Width - 5, Flabel.Location.Y);
                        //Qlabel.Location = new Point(Flabel.Location.X, Qlabel.Location.Y);
                        //SDlabel.Location = new Point(Flabel.Location.X, SDlabel.Location.Y);
                        //Plabel.Location = new Point(Flabel.Location.X, Plabel.Location.Y);

                        Flabel.Location = new Point(scatterGraph2.Location.X + scatterGraph2.Size.Width + 5, scatterGraph2.Location.Y);
                        Qlabel.Location = new Point(Flabel.Location.X, Flabel.Location.Y + 20);
                        SDlabel.Location = new Point(Flabel.Location.X, Flabel.Location.Y + 40);
                        Plabel.Location = new Point(Flabel.Location.X, Flabel.Location.Y + 60);


                        double angle = (answer.Direction / 10.0);
                        //ThirdInitScatterGraph2(angle);
                        ThirdInitScatterGraph2(0.5, angle);
                        FourthInitScatterGraph2(answer.CorrelationHistogram);
                    }
                }
            }
            else
            {
                await Task.Delay(5000);
            }
        }

        //Функция выполнения широкого исполнительного пеленгования 2
        private async void PerfomWidthExecutiveDFwithPixelCorrection2(double xPosStart, double xPosEnd, int PhaseAveragingCount, int DirectionAveragingCount)
        {
            if (isServConnected == true)
            {

                ExecutiveDFResponse answer = await VariableWork.aWPtoBearingDSPprotocolNew.ExecutiveDF(xPosStart, xPosEnd, PhaseAveragingCount, DirectionAveragingCount);

                if (answer != null)
                {
                    if (answer.Header.ErrorCode == 0)
                    {

                        //Flabel.Text = String.Format("F = {0} MHz", answer.Frequency / 10000.0);
                        Flabel.Text = String.Format("{0} {1}", flabel, answer.Frequency / 10000.0);
                        //Qlabel.Text = String.Format("Q = {0}°", answer.Direction / 10.0);
                        Qlabel.Text = String.Format("{0} {1}", qlabel, answer.Direction / 10.0);
                        //SDlabel.Text = String.Format("SD = {0}", answer.StandardDeviation);
                        SDlabel.Text = String.Format("{0} {1}", sdlabel, answer.StandardDeviation);
                        //Plabel.Text = String.Format("% = {0}", answer.DiscardedDirectionPercent);
                        Plabel.Text = String.Format("{0} {1}", plabel, answer.DiscardedDirectionPercent);

                        Flabel.Location = new Point(scatterGraph2.Location.X + scatterGraph2.Size.Width + 5, scatterGraph2.Location.Y);
                        Qlabel.Location = new Point(Flabel.Location.X, Flabel.Location.Y + 20);
                        SDlabel.Location = new Point(Flabel.Location.X, Flabel.Location.Y + 40);
                        Plabel.Location = new Point(Flabel.Location.X, Flabel.Location.Y + 60);




                        double angle = (answer.Direction / 10.0);
                        //ThirdInitScatterGraph2(angle);
                        ThirdInitScatterGraph2(0.5, angle);
                        FourthInitScatterGraph2(answer.CorrelationHistogram);
                    }
                }
            }
            else
            {
                await Task.Delay(5000);
            }
        }

        //Функция выполнения широкого исполнительного пеленгования 3
        private async Task PerfomWidthExecutiveDFwithPixelCorrection3(double xPosStart, double xPosEnd, int PhaseAveragingCount, int DirectionAveragingCount)
        {
            if (isServConnected == true)
            {

                ExecutiveDFResponse answer = await VariableWork.aWPtoBearingDSPprotocolNew.ExecutiveDF(xPosStart, xPosEnd, PhaseAveragingCount, DirectionAveragingCount);

                if (answer != null)
                {
                    if (answer.Header.ErrorCode == 0)
                    {
                        FtextBox.Text = (answer.Frequency / 10000d).ToString("F4");
                        QtextBox.Text = (answer.Direction / 10d).ToString("F1");
                        SDtextBox.Text = (answer.StandardDeviation / 10d).ToString("F1");
                        PtextBox.Text = (answer.DiscardedDirectionPercent).ToString();

                        double angle = (answer.Direction / 10.0);
                        //ThirdInitScatterGraph2(angle);
                        ThirdInitScatterGraph2(0.5, angle);
                        FourthInitScatterGraph2(answer.CorrelationHistogram);
                    }
                }
            }
            else
            {
                await Task.Delay(5000);
            }
        }

        //Функция выполнения широкого исполнительного пеленгования 4
        private async Task PerfomWidthExecutiveDFwithPixelCorrection4(double xPosStart, double xPosEnd, int PhaseAveragingCount, int DirectionAveragingCount)
        {
            if (isServConnected == true)
            {
                ExecutiveDFResponse answer = await VariableWork.aWPtoBearingDSPprotocolNew.ExecutiveDF(xPosStart, xPosEnd, PhaseAveragingCount, DirectionAveragingCount);

                if (answer != null)
                {
                    if (answer.Header.ErrorCode == 0)
                    {
                        if (answer.Direction == -1)
                        {
                            Invoke((MethodInvoker)(() => FtextBox.Text = (answer.Frequency / 10000d).ToString("F4")));
                            Invoke((MethodInvoker)(() => QtextBox.Text = "-"));
                            Invoke((MethodInvoker)(() => SDtextBox.Text = "-"));
                            Invoke((MethodInvoker)(() => PtextBox.Text = "-"));

                            double angle = 0;
                            Invoke((MethodInvoker)(() => ThirdInitScatterGraph2(0.5, angle)));
                        }
                        else
                        {
                            if (answer.Frequency == 0 && answer.Direction == 0 && answer.StandardDeviation == 0 && answer.DiscardedDirectionPercent == 0)
                            {
                                Invoke((MethodInvoker)(() => FtextBox.Text = "-"));
                                Invoke((MethodInvoker)(() => QtextBox.Text = "-"));
                                Invoke((MethodInvoker)(() => SDtextBox.Text = "-"));
                                Invoke((MethodInvoker)(() => PtextBox.Text = "-"));

                                double angle = 0;
                                Invoke((MethodInvoker)(() => ThirdInitScatterGraph2(0.5, angle)));
                            }
                            else
                            {
                                Invoke((MethodInvoker)(() => FtextBox.Text = (answer.Frequency / 10000d).ToString("F4")));
                                Invoke((MethodInvoker)(() => QtextBox.Text = (answer.Direction / 10d).ToString("F1")));
                                Invoke((MethodInvoker)(() => SDtextBox.Text = (answer.StandardDeviation / 10d).ToString("F1")));
                                Invoke((MethodInvoker)(() => PtextBox.Text = (answer.DiscardedDirectionPercent).ToString()));

                                double angle = (answer.Direction / 10.0);
                                //ThirdInitScatterGraph2(angle);
                                Invoke((MethodInvoker)(() => ThirdInitScatterGraph2(0.5, angle)));
                            }
                        }
                        if (answer.CorrelationHistogram != null)
                            Invoke((MethodInvoker)(() => FourthInitScatterGraph2(answer.CorrelationHistogram)));
                    }
                }
            }

        }

        //Кнопка исполнительного пеленгования
        private void buttonExDF_CheckedChanged1(object sender, EventArgs e)
        {
            if (buttonExDF.Checked == true)
            {
                toolStripButton3.PerformClick();
                EDFtimer.Enabled = true;
            }
            else
                EDFtimer.Enabled = false;
        }

        //Кнопка исполнительного пеленгования 2
        private void buttonExDF_CheckedChanged(object sender, EventArgs e)
        {
            if (buttonExDF.Checked == true)
            {
                if (currindex != 3)
                    toolStripButton3.PerformClick();
                Task.Run(() => ExecutiveDF());
            }
        }

        //Таймер исполнительного пеленгования
        private async void EDFtimer_Tick(object sender, EventArgs e)
        {
            if (mouseDownXPoint != -1 && mouseUpXPoint != -1)
            {
                if (mouseDownXPoint != mouseUpXPoint)
                {
                    //PerfomWidthExecutiveDFwithPixelCorrection2(Math.Min(mouseDownXPoint, mouseUpXPoint), Math.Max(mouseDownXPoint, mouseUpXPoint), (int)numericUpDownPh.Value, (int)numericUpDownPl.Value);
                    await PerfomWidthExecutiveDFwithPixelCorrection3(Math.Min(mouseDownXPoint, mouseUpXPoint), Math.Max(mouseDownXPoint, mouseUpXPoint), (int)numericUpDownPh.Value, (int)numericUpDownPl.Value);
                }
            }
            else
            {
                //PerfomExecutiveDFwithPixelCorrection(15);
                //PerfomExecutiveDFwithPixelCorrection2(15, (int)numericUpDownPh.Value, (int)numericUpDownPl.Value);
            }
        }

        //Таск исполнительного пеленгования
        private async void ExecutiveDF()
        {
            GetSpectrumResponse answer = new GetSpectrumResponse();

            while (buttonExDF.Checked == true)
            {
                if (isServConnected == true)
                {
                    if (numericUpDownPh.Value * numericUpDownPl.Value <= 500)
                        await PerfomWidthExecutiveDFwithPixelCorrection4(Math.Min(mouseDownXPoint, mouseUpXPoint), Math.Max(mouseDownXPoint, mouseUpXPoint), (int)numericUpDownPh.Value, (int)numericUpDownPl.Value);
                }
                await Task.Delay(500);
            }
        }

        async void VariableWork_RecBRSEvent(double frequency)
        {
            if (InvokeRequired)
            {
                BeginInvoke((MethodInvoker)(() => VariableWork_RecBRSEvent(frequency)));
                return;
            }

            if (currindex != 3)
                toolStripButton3.PerformClick();

            var minFreq = frequency - 0.05;
            var maxFreq = frequency + 0.05;

            await PerfomWidthExecutiveDFwithPixelCorrection4(minFreq, maxFreq, 3, 3);
        }

        private void waveformGraph1_Zoom(object sender, ActionEventArgs e)
        {
            if (xAxes.Range.Maximum - xAxes.Range.Minimum < GlobalBandWidthMHz)
            {
                double center = (xAxes.Range.Maximum + xAxes.Range.Minimum) / 2;
                waveformPlot1.XAxis.Range = new Range(center - GlobalBandWidthMHz / 2, center + GlobalBandWidthMHz / 2);
            }

            if (xAxes.Range.Minimum > GlobalRangeMax || xAxes.Range.Maximum < GlobalRangeMin)
            {
                waveformPlot1.XAxis.Range = new Range(GlobalRangeMin, GlobalRangeMax);
            }

            if (yAxes.Range.Minimum > GlobalYRangeMax || yAxes.Range.Maximum < GlobalYRangeMin)
            {
                waveformPlot1.YAxis.Range = new Range(GlobalYRangeMin, GlobalYRangeMax);
            }

            if (xAxes.Range.Maximum - xAxes.Range.Minimum > GlobalRangeMax - GlobalRangeMin)
            {
                button3.PerformClick();
            }

            if (yAxes.Range.Maximum - yAxes.Range.Minimum > (-1) * GlobalYRangeMin)
            {
                button3.PerformClick();
            }
        }

        int spindex = 0;
        int epindex = GlobalNumberofBands;
        private void waveformGraph1_XAxisRangeChanged(object sender, XAxisEventArgs e)
        {
            double min = xAxes.Range.Minimum;
            double max = xAxes.Range.Maximum;
            //xAxis1.Range = xAxes.Range;

            Rectangle rect1 = waveformGraph1.PlotAreaBounds;
            Rectangle rect2 = xyDataScatterGraph.PlotAreaBounds;

            int DSizeX = rect1.Width - rect2.Width;
            xyDataScatterGraph.Size = new Size(xyDataScatterGraph.Size.Width + DSizeX, xyDataScatterGraph.Size.Height);
            xyDataScatterGraph.Location = new Point(xyDataScatterGraph.Location.X - DSizeX, xyDataScatterGraph.Location.Y);
            xAxis1.Range = new Range(min, max);
            //xAxes.Range = new Range(min, max);

            //Обратить внимание на IF!!!
            if (colorScale2.Visible == false)
            {

                Rectangle rect3 = intensityGraph1.PlotAreaBounds;
                DSizeX = rect1.Width - rect3.Width;

                intensityXAxis4.Range = xAxes.Range;
                double otmin = (intensityXAxis4.Range.Minimum - GlobalRangeMin) / GlobalBandWidthMHz * GlobalDotsPerBand;
                double otmax = (intensityXAxis4.Range.Maximum - GlobalRangeMin) / GlobalBandWidthMHz * GlobalDotsPerBand;

                intensityXAxis3.Range = new Range(otmin, otmax);
                intensityGraph1.Size = new Size(intensityGraph1.Size.Width + DSizeX, intensityGraph1.Size.Height);
                intensityGraph1.Location = new Point(intensityGraph1.Location.X - DSizeX, intensityGraph1.Location.Y);

            }
            else
            {
                Rectangle rect3 = intensityGraph1.PlotAreaBounds;
                DSizeX = rect1.Width - rect3.Width;

                intensityXAxis4.Range = xAxes.Range;
                double otmin = (intensityXAxis4.Range.Minimum - GlobalRangeMin) / GlobalBandWidthMHz * GlobalDotsPerBand;
                double otmax = (intensityXAxis4.Range.Maximum - GlobalRangeMin) / GlobalBandWidthMHz * GlobalDotsPerBand;

                intensityXAxis3.Range = new Range(otmin, otmax);
                intensityGraph1.Size = new Size(intensityGraph1.Size.Width + DSizeX, intensityGraph1.Size.Height);
                intensityGraph1.Location = new Point(intensityGraph1.Location.X - DSizeX, intensityGraph1.Location.Y);
            }

            UpdateLabels();

            outBandPictureBoxIndex(min, max, out spindex, out epindex);
            pictureBox1.Invalidate();
        }

        private void outBandPictureBoxIndex(double MinFrequency, double MaxFrequency, out int startIndex, out int endIndex)
        {
            startIndex = 0;
            endIndex = GlobalNumberofBands;

            if (MinFrequency < GlobalRangeMin)
                MinFrequency = GlobalRangeMin;

            if (MaxFrequency > GlobalRangeMax)
                MaxFrequency = GlobalRangeMax;

            double[] td = new double[GlobalNumberofBands];
            for (int i = 0; i < GlobalNumberofBands; i++)
            {
                td[i] = GlobalRangeMin + i * GlobalBandWidthMHz;
                if (MinFrequency >= td[i] && MinFrequency <= td[i] + GlobalBandWidthMHz)
                    startIndex = i;
                if (MaxFrequency >= td[i] && MaxFrequency <= td[i] + GlobalBandWidthMHz)
                {
                    endIndex = i;
                    break;
                }
            }
        }

        private int BandPictireBoxIndexFromFreq(double Frequency)
        {
            int index = -1;

            if (Frequency < GlobalRangeMin)
                Frequency = GlobalRangeMin;

            if (Frequency > GlobalRangeMax)
                Frequency = GlobalRangeMax;

            double[] td = new double[GlobalNumberofBands];
            for (int i = 0; i < GlobalNumberofBands; i++)
            {
                td[i] = GlobalRangeMin + i * GlobalBandWidthMHz;
                if (Frequency >= td[i] && Frequency <= td[i] + GlobalBandWidthMHz)
                    return i;
            }

            return index;

        }

        private void UpdateFQSDPLabels()
        {
            Flabel.Text = String.Format("{0}", flabel);
            Qlabel.Text = String.Format("{0}", qlabel);
            SDlabel.Text = String.Format("{0}", sdlabel);
            Plabel.Text = String.Format("{0}", plabel);

            CofAvPh.Text = String.Format("{0}", cofAvPh);
            CofAvPl.Text = String.Format("{0}", cofAvPl);
        }

        private void UpdateFQSDPPhPlLocations()
        {
            int[] ar = { Flabel.Width, Qlabel.Width, SDlabel.Width, Plabel.Width, Flabel.Width, CofAvPh.Width, CofAvPl.Width };
            int maxWidth = ar.Max<int>();

            FtextBox.Location = new Point(Flabel.Location.X + maxWidth, Flabel.Location.Y - 3);
            QtextBox.Location = new Point(Qlabel.Location.X + maxWidth, Qlabel.Location.Y - 3);
            SDtextBox.Location = new Point(SDlabel.Location.X + maxWidth, SDlabel.Location.Y - 3);
            PtextBox.Location = new Point(Plabel.Location.X + maxWidth, Plabel.Location.Y - 3);

            numericUpDownPh.Location = new Point(CofAvPh.Location.X + maxWidth, CofAvPh.Location.Y - 3);
            numericUpDownPl.Location = new Point(CofAvPl.Location.X + maxWidth, CofAvPl.Location.Y - 3);

            //UpdateLines();
            UpdateDots();
        }

        private void UpdateLines()
        {
            FlineShape.X1 = Flabel.Location.X;
            FlineShape.Y1 = Flabel.Location.Y + Flabel.Height - 1;
            FlineShape.X2 = FtextBox.Location.X;
            FlineShape.Y2 = FlineShape.Y1;

            QlineShape.X1 = Qlabel.Location.X;
            QlineShape.Y1 = Qlabel.Location.Y + Qlabel.Height - 1;
            QlineShape.X2 = QtextBox.Location.X;
            QlineShape.Y2 = QlineShape.Y1;

            SDlineShape.X1 = SDlabel.Location.X;
            SDlineShape.Y1 = SDlabel.Location.Y + SDlabel.Height - 1;
            SDlineShape.X2 = SDtextBox.Location.X;
            SDlineShape.Y2 = SDlineShape.Y1;

            PlineShape.X1 = Plabel.Location.X;
            PlineShape.Y1 = Plabel.Location.Y + Plabel.Height - 1;
            PlineShape.X2 = PtextBox.Location.X;
            PlineShape.Y2 = PlineShape.Y1;

            PhlineShape.X1 = CofAvPh.Location.X;
            PhlineShape.Y1 = CofAvPh.Location.Y + CofAvPh.Height - 1;
            PhlineShape.X2 = numericUpDownPh.Location.X;
            PhlineShape.Y2 = PhlineShape.Y1;

            PllineShape.X1 = CofAvPl.Location.X;
            PllineShape.Y1 = CofAvPl.Location.Y + CofAvPl.Height - 1;
            PllineShape.X2 = numericUpDownPl.Location.X;
            PllineShape.Y2 = PllineShape.Y1;
        }
        private void UpdateDots()
        {
            FlineShape.Visible = false;
            QlineShape.Visible = false;
            SDlineShape.Visible = false;
            PlineShape.Visible = false;
            PhlineShape.Visible = false;
            PllineShape.Visible = false;

            AddDots(ref Flabel, ref FtextBox);
            AddDots(ref Qlabel, ref QtextBox);
            AddDots(ref SDlabel, ref SDtextBox);
            AddDots(ref Plabel, ref PtextBox);

            AddDots(ref CofAvPh, ref numericUpDownPh);
            AddDots(ref CofAvPl, ref numericUpDownPl);
        }

        private void AddDots(ref Label label, ref TextBox textbox)
        {
            while (label.Location.X + label.Width <= textbox.Location.X + textbox.Width)
            {
                label.Text = label.Text + ".";
            }
        }
        private void AddDots(ref Label label, ref NumericUpDown numericUpDown)
        {
            while (label.Location.X + label.Width < numericUpDown.Location.X + numericUpDown.Width)
            {
                label.Text = label.Text + ".";
            }
        }

        /*
        private void CofAvPh_Resize(object sender, EventArgs e)
        {
            //numericUpDownPh.Location = new Point(CofAvPh.Location.X + CofAvPh.Width, CofAvPh.Location.Y - 3);
        }

        private void CofAvPl_Resize(object sender, EventArgs e)
        {
            //numericUpDownPl.Location = new Point(CofAvPl.Location.X + CofAvPl.Width, CofAvPl.Location.Y - 3);
        }
        */

        private void UpdateLabels()
        {
            Rectangle rectangle = waveformGraph1.PlotAreaBounds;
            label9.Location = new Point(waveformGraph1.Location.X + rectangle.X + 1, waveformGraph1.Location.Y + rectangle.Y);
            label10.Location = new Point(waveformGraph1.Location.X + rectangle.X + 140, waveformGraph1.Location.Y + rectangle.Y);

            label94.Location = new Point(scatterGraph1.Location.X + scatterGraph1.PlotAreaBounds.X + 1, scatterGraph1.Location.Y + scatterGraph1.PlotAreaBounds.Y);
            label104.Location = new Point(scatterGraph1.Location.X + scatterGraph1.PlotAreaBounds.X + 1, scatterGraph1.Location.Y + scatterGraph1.PlotAreaBounds.Y + 20);

            label95.Location = new Point(intensityGraph1.Location.X + intensityGraph1.PlotAreaBounds.X + 1, intensityGraph1.Location.Y + intensityGraph1.PlotAreaBounds.Y);

            //button3.Location = new Point(rectangle.Right - 32, waveformGraph1.Location.Y + rectangle.Y);
            button3.Location = new Point(waveformGraph1.Location.X + rectangle.X + rectangle.Width - 32, waveformGraph1.Location.Y + rectangle.Y);

            button34.Location = new Point(scatterGraph1.Location.X + scatterGraph1.PlotAreaBounds.X + scatterGraph1.PlotAreaBounds.Width - 32, scatterGraph1.Location.Y + scatterGraph1.PlotAreaBounds.Y);

            label11.Location = new Point(waveformGraph1.Location.X + rectangle.X + rectangle.Width - label11.Width, waveformGraph1.Location.Y + rectangle.Y + rectangle.Height - label11.Height - 1);

            labelLeft.Location = new Point(waveformGraph1.Location.X + rectangle.X + 1, waveformGraph1.Location.Y + rectangle.Y + rectangle.Height - labelLeft.Height - 1);
            labelRight.Location = new Point(waveformGraph1.Location.X + rectangle.X + 1 + labelLeft.Width, waveformGraph1.Location.Y + rectangle.Y + rectangle.Height - labelRight.Height - 1);

        }
        private void waveformGraph1_PlotAreaBoundsChanged(object sender, EventArgs e)
        {
            UpdateLabels();

            double min = xAxes.Range.Minimum;
            double max = xAxes.Range.Maximum;

            Rectangle rect1 = waveformGraph1.PlotAreaBounds;
            Rectangle rect2 = xyDataScatterGraph.PlotAreaBounds;

            int DSizeX = rect1.Width - rect2.Width;
            xyDataScatterGraph.Size = new Size(xyDataScatterGraph.Size.Width + DSizeX, xyDataScatterGraph.Size.Height);
            xyDataScatterGraph.Location = new Point(xyDataScatterGraph.Location.X - DSizeX, xyDataScatterGraph.Location.Y);
            xAxis1.Range = new Range(min, max);
            xAxes.Range = new Range(min, max);

            //Обратить внимание на IF!!!
            if (colorScale2.Visible == false)
            {
                Rectangle rect3 = intensityGraph1.PlotAreaBounds;
                DSizeX = rect1.Width - rect3.Width;

                intensityXAxis4.Range = xAxes.Range;
                double otmin = (intensityXAxis4.Range.Minimum - GlobalRangeMin) / GlobalBandWidthMHz * GlobalDotsPerBand;
                double otmax = (intensityXAxis4.Range.Maximum - GlobalRangeMin) / GlobalBandWidthMHz * GlobalDotsPerBand;

                intensityXAxis3.Range = new Range(otmin, otmax);
                intensityGraph1.Size = new Size(intensityGraph1.Size.Width + DSizeX, intensityGraph1.Size.Height);
                intensityGraph1.Location = new Point(intensityGraph1.Location.X - DSizeX, intensityGraph1.Location.Y);
            }
            else
            {
                Rectangle rect3 = intensityGraph1.PlotAreaBounds;
                DSizeX = rect1.Width - rect3.Width;

                intensityXAxis4.Range = xAxes.Range;
                double otmin = (intensityXAxis4.Range.Minimum - GlobalRangeMin) / GlobalBandWidthMHz * GlobalDotsPerBand;
                double otmax = (intensityXAxis4.Range.Maximum - GlobalRangeMin) / GlobalBandWidthMHz * GlobalDotsPerBand;

                intensityXAxis3.Range = new Range(otmin, otmax);
                intensityGraph1.Size = new Size(intensityGraph1.Size.Width + DSizeX, intensityGraph1.Size.Height);
                intensityGraph1.Location = new Point(intensityGraph1.Location.X - DSizeX, intensityGraph1.Location.Y);
            }



        }


        private object threadLock = new object();
        private readonly AsyncLock asyncLock = new AsyncLock();

        //Внешнее изменение порога
        public void SetThreshold(byte Threshold)
        {
            lock (threadLock)
            {
                int threshold = (-1) * Threshold;
                if (numericUpDown1.Value != threshold)
                    numericUpDown1.Value = threshold;
            }
        }
        public void SetThreshold(int Threshold)
        {
            lock (threadLock)
            {
                if (numericUpDown1.Value != Threshold)
                    numericUpDown1.Value = Threshold;
            }
        }
        //Получение порога
        public int GetThreshold()
        {
            return (int)porogCursor.YPosition;
        }

        private async void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            using (await asyncLock.LockAsync().ConfigureAwait(false))
            {
                if (numericUpDown1.Value <= (int)GlobalYRangeMax && numericUpDown1.Value >= (int)GlobalYRangeMin)
                {
                    porogCursor.YPosition = (int)numericUpDown1.Value;
                    //dsp.SetFilters((int)porogCursor.YPosition, 0, 0, 0);
                    var answer = await VariableWork.aWPtoBearingDSPprotocolNew.SetFilters((int)porogCursor.YPosition, 0, 0, 0);
                    //var answer2 = await VariableWork.aWPtoBearingDSPprotocolNew.GetFilters();
                }
                else
                {
                    numericUpDown1.Value = -80;
                    porogCursor.YPosition = (int)numericUpDown1.Value;
                }
            }
        }

        private void numericUpDownPh_ValueChanged(object sender, EventArgs e)
        {
            if (numericUpDownPh.Value > numericUpDownPh.Maximum || numericUpDownPh.Value < numericUpDownPh.Minimum)
            {
                numericUpDownPh.Value = 3;
            }
            if (numericUpDownPh.Value * numericUpDownPl.Value > 500)
            {
                int value = 500 / (int)numericUpDownPh.Value;
                numericUpDownPl.Value = value;
            }
        }

        private void numericUpDownPh_KeyPress(object sender, KeyPressEventArgs e)
        {
            //int p = Convert.ToInt32(numericUpDownPh.Value.ToString() + e.KeyChar.ToString());
            //if (p >= 1 && p <= 500)
            //    numericUpDownPh.Value = p;
            //else
            //    numericUpDownPh.Value = 500;
            //e.Handled = true;
        }

        private void numericUpDownPl_KeyPress(object sender, KeyPressEventArgs e)
        {
            //int p = Convert.ToInt32(numericUpDownPl.Value.ToString() + e.KeyChar.ToString());
            //if (p >= 1 && p <= 500)
            //    numericUpDownPl.Value = p;
            //else
            //    numericUpDownPl.Value = 500;
            //e.Handled = true;
        }

        private void numericUpDownPl_ValueChanged(object sender, EventArgs e)
        {
            if (numericUpDownPl.Value > numericUpDownPl.Maximum || numericUpDownPl.Value < numericUpDownPl.Minimum)
            {
                numericUpDownPl.Value = 3;
            }
            if (numericUpDownPh.Value * numericUpDownPl.Value > 500)
            {
                int value = 500 / (int)numericUpDownPl.Value;
                numericUpDownPh.Value = value;
            }
        }

        private void waveformGraph1_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {

            if ((Control.ModifierKeys & Keys.Alt) == Keys.Alt && Keys.Up == e.KeyCode)
            {
                numericUpDown1.UpButton();
            }

            if ((Control.ModifierKeys & Keys.Alt) == Keys.Alt && Keys.Down == e.KeyCode)
            {
                numericUpDown1.DownButton();
            }


            if ((Control.ModifierKeys & Keys.Alt) != Keys.Alt && Keys.Up == e.KeyCode)
            {
                SpectrxyCursor.YPosition += 1;
                UpdateTextLabelsForCursor();
            }
            if ((Control.ModifierKeys & Keys.Alt) != Keys.Alt && Keys.Down == e.KeyCode)
            {
                SpectrxyCursor.YPosition -= 1;
                UpdateTextLabelsForCursor();
            }
            if ((Control.ModifierKeys & Keys.Alt) != Keys.Alt && Keys.Right == e.KeyCode)
            {
                SpectrxyCursor.XPosition += 10;
                UpdateTextLabelsForCursor();
            }
            if ((Control.ModifierKeys & Keys.Alt) != Keys.Alt && Keys.Left == e.KeyCode)
            {
                SpectrxyCursor.XPosition -= 10;
                UpdateTextLabelsForCursor();
            }

            if (Keys.Space == e.KeyCode)
            {
                variableWork.Frequency = Convert.ToInt64(SpectrxyCursor.XPosition * 1000000);
            }
        }

        private void xyDataScatterGraph_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if ((Control.ModifierKeys & Keys.Alt) != Keys.Alt && Keys.Up == e.KeyCode)
            {
                xyCursor1.YPosition += 1;
            }
            if ((Control.ModifierKeys & Keys.Alt) != Keys.Alt && Keys.Down == e.KeyCode)
            {
                xyCursor1.YPosition -= 1;
            }
            if ((Control.ModifierKeys & Keys.Alt) != Keys.Alt && Keys.Right == e.KeyCode)
            {
                xyCursor1.XPosition += 10;
            }
            if ((Control.ModifierKeys & Keys.Alt) != Keys.Alt && Keys.Left == e.KeyCode)
            {
                xyCursor1.XPosition -= 10;
            }
        }

        private void xyDataScatterGraph_MouseClick(object sender, MouseEventArgs e)
        {
            if ((Control.ModifierKeys & Keys.Control) == Keys.Control || (Control.ModifierKeys & Keys.Shift) == Keys.Shift)
            {
                return;
            }


            Rectangle rp = xyPlot.GetBounds();

            if (e.X >= rp.Left && e.X <= rp.Right && e.Y >= rp.Top && e.Y <= rp.Bottom)
            {

                xyCursor1.XPosition = (e.X - pelengeBoundsX) * (xAxis1.Range.Maximum - xAxis1.Range.Minimum) / (pelengeBoundsWidth) + xAxis1.Range.Minimum;

                xyCursor1.YPosition = (e.Y - pelengeBoundsY) * (xyPlotYAxis.Range.Minimum - xyPlotYAxis.Range.Maximum) / (pelengeBoundsHeight) + xyPlotYAxis.Range.Maximum;

            }
        }

        private void xyDataScatterGraph_AfterDrawPlot(object sender, AfterDrawXYPlotEventArgs e)
        {
            pelengeBoundsWidth = e.Bounds.Width;
            pelengeBoundsHeight = e.Bounds.Height;
            pelengeBoundsX = e.Bounds.X;
            pelengeBoundsY = e.Bounds.Y;
        }



        private static double[] GenerateXCircle(double Radius)
        {
            double[] xData = new double[361];

            for (int i = 0; i < 360; i++)
            {
                xData[i] = Radius * Math.Sin(Math.PI / 180 * i);
            }

            xData[360] = xData[0];

            return xData;
        }
        private double[] GenerateXCircleNew(double RadiusMHz)
        {

            double Radius = 10 / (GlobalRangeMax / RadiusMHz);

            double[] xData = new double[361];

            for (int i = 0; i < 360; i++)
            {
                xData[i] = Radius * Math.Sin(Math.PI / 180 * i);
            }

            xData[360] = xData[0];

            return xData;
        }

        private static double[] GenerateYCircle(double Radius)
        {

            double[] yData = new double[361];

            for (int i = 0; i < 360; i++)
            {
                yData[i] = Radius * Math.Cos(Math.PI / 180 * i);
            }

            yData[360] = yData[0];

            return yData;
        }
        private double[] GenerateYCircleNew(double RadiusMHz)
        {
            double Radius = 10 / (GlobalRangeMax / RadiusMHz);

            double[] yData = new double[361];

            for (int i = 0; i < 360; i++)
            {
                yData[i] = Radius * Math.Cos(Math.PI / 180 * i);
            }

            yData[360] = yData[0];

            return yData;
        }

        private static double[] GenerateLineX1(double Radius, double Angle)
        {
            double[] xData = new double[2];
            xData[0] = Radius * Math.Sin(Math.PI / 180 * Angle);
            xData[1] = 0;
            return xData;
        }
        private static double[] GenerateLineX1(double Radius, double Angle1, double Angle2)
        {
            double[] xData = new double[2];
            xData[0] = Radius * Math.Sin(Math.PI / 180 * Angle1);
            xData[1] = Radius * Math.Sin(Math.PI / 180 * Angle2);
            return xData;
        }
        private static double[] GenerateLineY1(double Radius, double Angle)
        {
            double[] yData = new double[2];
            yData[0] = Radius * Math.Cos(Math.PI / 180 * Angle);
            yData[1] = 0;
            return yData;
        }
        private static double[] GenerateLineY1(double Radius, double Angle1, double Angle2)
        {
            double[] yData = new double[2];
            yData[0] = Radius * Math.Cos(Math.PI / 180 * Angle1);
            yData[1] = Radius * Math.Cos(Math.PI / 180 * Angle2);
            return yData;
        }

        private void InitRoundPeleng()
        {
            //Rounds
            rScatterPlot1.PlotXY(GenerateXCircleNew(3000.0), GenerateYCircleNew(3000.0));
            rScatterPlot7.PlotXYAppend(GenerateXCircleNew(2000.0), GenerateYCircleNew(2000.0));
            rScatterPlot8.PlotXY(GenerateXCircleNew(1000.0), GenerateYCircleNew(1000.0));

            if (GlobalNumberofBands == 200)
            {
                ScatterPlot exrScatterPlot4 = new ScatterPlot(this.xAxis2, this.yAxis2);
                exrScatterPlot4.LineColor = System.Drawing.Color.Lime;
                exrScatterPlot4.LineColorPrecedence = NationalInstruments.UI.ColorPrecedence.UserDefinedColor;

                ScatterPlot exrScatterPlot5 = new ScatterPlot(this.xAxis2, this.yAxis2);
                exrScatterPlot5.LineColor = System.Drawing.Color.Lime;
                exrScatterPlot5.LineColorPrecedence = NationalInstruments.UI.ColorPrecedence.UserDefinedColor;

                ScatterPlot exrScatterPlot6 = new ScatterPlot(this.xAxis2, this.yAxis2);
                exrScatterPlot6.LineColor = System.Drawing.Color.Lime;
                exrScatterPlot6.LineColorPrecedence = NationalInstruments.UI.ColorPrecedence.UserDefinedColor;


                scatterGraph1.Plots.Add(exrScatterPlot4);
                scatterGraph1.Plots.Add(exrScatterPlot5);
                scatterGraph1.Plots.Add(exrScatterPlot6);

                exrScatterPlot4.PlotXY(GenerateXCircleNew(4000.0), GenerateYCircleNew(4000.0));
                exrScatterPlot5.PlotXY(GenerateXCircleNew(5000.0), GenerateYCircleNew(5000.0));
                exrScatterPlot6.PlotXY(GenerateXCircleNew(6000.0), GenerateYCircleNew(6000.0));
            }

            //mainCross
            rScatterPlot3.PlotXYAppend(GenerateLineX1(10, 0, 180), GenerateLineY1(10, 0, 180));
            rScatterPlot4.PlotXYAppend(GenerateLineX1(10, 90, 270), GenerateLineY1(10, 90, 270));

            //offCross
            rScatterPlot5.PlotXYAppend(GenerateLineX1(10, 45, 225), GenerateLineY1(10, 45, 225));
            rScatterPlot6.PlotXYAppend(GenerateLineX1(10, 135, 315), GenerateLineY1(10, 135, 315));
        }

        private void InitAnnotations()
        {

            //Annotations
            xyPointAnnotation0.XPosition = 10 * Math.Sin(Math.PI / 180 * 0) - 0.5 * (320.0 / scatterGraph1.Size.Width);
            xyPointAnnotation0.YPosition = 10 * Math.Cos(Math.PI / 180 * 0) + 1 * (320.0 / scatterGraph1.Size.Width);

            xyPointAnnotation45.XPosition = 10 * Math.Sin(Math.PI / 180 * 45);
            xyPointAnnotation45.YPosition = 10 * Math.Cos(Math.PI / 180 * 45) + 1 * (320.0 / scatterGraph1.Size.Width);

            xyPointAnnotation90.XPosition = 10 * Math.Sin(Math.PI / 180 * 90) + 0.125 * (320.0 / scatterGraph1.Size.Width);
            xyPointAnnotation90.YPosition = 10 * Math.Cos(Math.PI / 180 * 90) + 0.5 * (320.0 / scatterGraph1.Size.Width);

            xyPointAnnotation135.XPosition = 10 * Math.Sin(Math.PI / 180 * 135);
            xyPointAnnotation135.YPosition = 10 * Math.Cos(Math.PI / 180 * 135);

            xyPointAnnotation180.XPosition = 10 * Math.Sin(Math.PI / 180 * 180) - 1 * (320.0 / scatterGraph1.Size.Width);
            xyPointAnnotation180.YPosition = 10 * Math.Cos(Math.PI / 180 * 180) - 0.25 * (320.0 / scatterGraph1.Size.Width);

            xyPointAnnotation225.XPosition = 10 * Math.Sin(Math.PI / 180 * 225) - 2.25 * (320.0 / scatterGraph1.Size.Width);
            xyPointAnnotation225.YPosition = 10 * Math.Cos(Math.PI / 180 * 225);

            xyPointAnnotation270.XPosition = 10 * Math.Sin(Math.PI / 180 * 270) - 2.25 * (320.0 / scatterGraph1.Size.Width);
            xyPointAnnotation270.YPosition = 10 * Math.Cos(Math.PI / 180 * 270) + 0.5 * (320.0 / scatterGraph1.Size.Width);

            xyPointAnnotation315.XPosition = 10 * Math.Sin(Math.PI / 180 * 315) - 2.25 * (320.0 / scatterGraph1.Size.Width);
            xyPointAnnotation315.YPosition = 10 * Math.Cos(Math.PI / 180 * 315) + 1 * (320.0 / scatterGraph1.Size.Width);

        }


        bool flagRoundPaint = true;
        private void toolStripButton4_Click(object sender, EventArgs e)
        {
            currindex = 4;

            toolStripButton1.Checked = false;
            toolStripButton2.Checked = false;
            toolStripButton3.Checked = false;
            toolStripButton4.Checked = true;
            toolStripButton5.Checked = false;

            button3.Visible = false;
            button34.Visible = true;
            label9.Visible = false;
            label10.Visible = false;
            label11.Visible = false;

            label94.Visible = true;
            label104.Visible = true;

            label95.Visible = false;

            labelLeft.Visible = false;
            labelRight.Visible = false;

            waveformGraph1.Visible = false;
            xyDataScatterGraph.Visible = false;

            //groupBox1.Visible = false;
            scGraph2andLabelVisible(false);

            scatterGraph1.Visible = true;
            intensityGraph1.Visible = false;
            //groupBox2.Visible = true;
            groupBox2.Visible = false;

            pictureBox1.Visible = false;

            Panorama_Resize();

            intensityTimer.Enabled = false;

            buttonExDF.Checked = false;
        }

        private void scatterGraph1_MouseClick(object sender, MouseEventArgs e)
        {
            if ((Control.ModifierKeys & Keys.Control) == Keys.Control || (Control.ModifierKeys & Keys.Shift) == Keys.Shift)
            {
                return;
            }

            Rectangle rp = rScatterPlot1.GetBounds();

            if (e.X >= rp.Left && e.X <= rp.Right && e.Y >= rp.Top && e.Y <= rp.Bottom)
            {
                double xpos = (e.X - roundPelengeBoundsX) * (xAxis2.Range.Maximum - xAxis2.Range.Minimum) / (roundPelengeBoundsWidth) + xAxis2.Range.Minimum;
                double ypos = (e.Y - roundPelengeBoundsY) * (yAxis2.Range.Minimum - yAxis2.Range.Maximum) / (roundPelengeBoundsHeight) + yAxis2.Range.Maximum;

                double r = Math.Sqrt(Math.Pow(xpos, 2) + Math.Pow(ypos, 2));

                if (r <= 10)
                {
                    roundxyCursor.XPosition = xpos;
                    roundxyCursor.YPosition = ypos;
                }


                double F = r * (GlobalRangeMax - GlobalRangeMin) / 10d;
                double alpha = Math.Atan2(ypos, xpos) / Math.PI * 180;
                double B = 90 - alpha;
                if (B < 0) B = B + 360;

                label94.Text = String.Format("{0}: {1} {2}", freq, F.ToString("F4"), MHz);
                label104.Text = String.Format("{0}: {1}°", bearing, B.ToString("F1"));
            }
        }


        int scattermouseDownPoint;
        bool scatterwaspressleft;

        double scattermpsxleft;
        double scattermpsyleft;

        private void scatterGraph1_MouseDown(object sender, MouseEventArgs e)
        {
            if ((Control.ModifierKeys & Keys.Control) == Keys.Control || (Control.ModifierKeys & Keys.Shift) == Keys.Shift)
            {
                return;
            }

            Rectangle rp = rScatterPlot1.GetBounds();
            if (e.X >= rp.Left && e.X <= rp.Right && e.Y >= rp.Top && e.Y <= rp.Bottom)
            {
                scattermouseDownPoint = e.X;

                if (e.Button == System.Windows.Forms.MouseButtons.Left)
                {
                    scattermpsxleft = (e.X - roundPelengeBoundsX) * (xAxis2.Range.Maximum - xAxis2.Range.Minimum) / (roundPelengeBoundsWidth) + xAxis2.Range.Minimum;
                    scattermpsyleft = yAxis2.Range.Maximum - (e.Y - roundPelengeBoundsY) * (yAxis2.Range.Maximum - yAxis2.Range.Minimum) / (roundPelengeBoundsHeight);
                    scatterwaspressleft = true;
                }
            }
        }

        int scattermouseUpPoint;
        private void scatterGraph1_MouseUp(object sender, MouseEventArgs e)
        {
            if ((Control.ModifierKeys & Keys.Control) == Keys.Control || (Control.ModifierKeys & Keys.Shift) == Keys.Shift)
            {
                return;
            }

            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                Rectangle rp = rScatterPlot1.GetBounds();
                if (e.X >= rp.Left && e.X <= rp.Right && e.Y >= rp.Top && e.Y <= rp.Bottom)
                {
                    scattermouseUpPoint = e.X;
                }

                if (xAxis2.Range.Minimum > 12 || xAxes.Range.Maximum < -12)
                {
                    xAxis2.Range = new Range(-12, 12);
                }
                if (yAxis2.Range.Minimum > 12 || yAxis2.Range.Maximum < -12)
                {
                    yAxis2.Range = new Range(-12, 12);
                }

                scatterwaspressleft = false;
            }
        }

        private void scatterGraph1_PlotAreaMouseMove(object sender, MouseEventArgs e)
        {
            if ((Control.ModifierKeys & Keys.Control) == Keys.Control || (Control.ModifierKeys & Keys.Shift) == Keys.Shift)
            {
                return;
            }

            double scatterGlobalRangeMin = -12;
            double scatterGlobalRangeMax = 12;

            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                if (scatterwaspressleft)
                {
                    double scattermppxleft = (e.X - roundPelengeBoundsX) * (xAxis2.Range.Maximum - xAxis2.Range.Minimum) / (roundPelengeBoundsWidth) + xAxis2.Range.Minimum;

                    if (this.xAxis2.Range.Minimum == -12 && this.xAxis2.Range.Maximum == 12)
                        return;

                    if (scattermpsxleft < scattermppxleft)
                    {
                        double scatterminimum = this.xAxis2.Range.Minimum - Math.Abs(scattermppxleft - scattermpsxleft);
                        double scattermaximum = this.xAxis2.Range.Maximum - Math.Abs(scattermppxleft - scattermpsxleft);

                        if (scatterminimum < scatterGlobalRangeMin)
                        {
                            double scattertemp = scatterGlobalRangeMin - scatterminimum;
                            scatterminimum = scatterGlobalRangeMin;
                            if (scattermaximum + scattertemp < scatterGlobalRangeMax)
                                scattermaximum = scattermaximum + scattertemp;
                        }
                        if (scattermaximum > scatterGlobalRangeMax)
                        {
                            double scattertemp = scattermaximum - scatterGlobalRangeMax;
                            scattermaximum = scatterGlobalRangeMax;
                            if (scatterminimum + scattertemp < scatterGlobalRangeMin)
                                scatterminimum = scatterminimum + scattertemp;
                        }

                        //xAxes.Range = new Range(this.xAxes.Range.Minimum - Math.Abs(mppxleft - mpsxleft), this.xAxes.Range.Maximum - Math.Abs(mppxleft - mpsxleft));
                        xAxis2.Range = new Range(scatterminimum, scattermaximum);
                    }
                    if (scattermpsxleft > scattermppxleft)
                    {
                        double scatterminimum = this.xAxis2.Range.Minimum + Math.Abs(scattermppxleft - scattermpsxleft);
                        double scattermaximum = this.xAxis2.Range.Maximum + Math.Abs(scattermppxleft - scattermpsxleft);

                        if (scattermaximum > scatterGlobalRangeMax)
                        {
                            double scattertemp = scatterGlobalRangeMax - scattermaximum;
                            scattermaximum = scatterGlobalRangeMax;
                            //if (minimum + temp < GlobalRangeMin)
                            scatterminimum = scatterminimum + scattertemp;
                        }
                        if (scatterminimum < scatterGlobalRangeMin)
                        {
                            double scattertemp = scatterGlobalRangeMin - scatterminimum;
                            scatterminimum = scatterGlobalRangeMin;
                            if (scattermaximum + scattertemp < scatterGlobalRangeMax)
                                scattermaximum = scattermaximum + scattertemp;
                        }

                        //xAxes.Range = new Range(this.xAxes.Range.Minimum + Math.Abs(mppxleft - mpsxleft), this.xAxes.Range.Maximum + Math.Abs(mppxleft - mpsxleft));
                        xAxis2.Range = new Range(scatterminimum, scattermaximum);
                    }

                    double scattermppyleft = yAxis2.Range.Maximum - (e.Y - roundPelengeBoundsY) * (yAxis2.Range.Maximum - yAxis2.Range.Minimum) / (roundPelengeBoundsHeight); //new

                    double scatterGlobalYRangeMin = -12;
                    double scatterGlobalYRangeMax = 12;

                    if (this.yAxis2.Range.Minimum == scatterGlobalYRangeMin && this.yAxis2.Range.Maximum == scatterGlobalYRangeMax)
                        return;

                    if (scattermpsyleft < scattermppyleft)
                    {
                        double scatterminimum = this.yAxis2.Range.Minimum - Math.Abs(scattermppyleft - scattermpsyleft);
                        double scattermaximum = this.yAxis2.Range.Maximum - Math.Abs(scattermppyleft - scattermpsyleft);

                        if (scatterminimum < scatterGlobalYRangeMin)
                        {
                            double scattertemp = scatterGlobalYRangeMin - scatterminimum;
                            scatterminimum = scatterGlobalYRangeMin;
                            if (scattermaximum + scattertemp < scatterGlobalYRangeMax)
                                scattermaximum = scattermaximum + scattertemp;
                        }
                        if (scattermaximum > scatterGlobalYRangeMax)
                        {
                            double scattertemp = scattermaximum - scatterGlobalYRangeMax;
                            scattermaximum = scatterGlobalYRangeMax;
                            if (scatterminimum + scattertemp < scatterGlobalYRangeMin)
                                scatterminimum = scatterminimum + scattertemp;
                        }

                        yAxis2.Range = new Range(scatterminimum, scattermaximum);
                    }
                    if (scattermpsyleft > scattermppyleft)
                    {
                        double scatterminimum = this.yAxis2.Range.Minimum + Math.Abs(scattermppyleft - scattermpsyleft);
                        double scattermaximum = this.yAxis2.Range.Maximum + Math.Abs(scattermppyleft - scattermpsyleft);

                        if (scattermaximum > scatterGlobalYRangeMax)
                        {
                            double scattertemp = scatterGlobalYRangeMax - scattermaximum;
                            scattermaximum = scatterGlobalYRangeMax;
                            //if (minimum + temp < GlobalYRangeMin)
                            scatterminimum = scatterminimum + scattertemp;
                        }
                        if (scatterminimum < scatterGlobalYRangeMin)
                        {
                            double scattertemp = scatterGlobalYRangeMin - scatterminimum;
                            scatterminimum = scatterGlobalYRangeMin;
                            //if (maximum + temp < GlobalYRangeMax)
                            scattermaximum = scattermaximum + scattertemp;
                        }

                        yAxis2.Range = new Range(scatterminimum, scattermaximum);
                    }

                }
            }
        }

        private void scatterGraph1_AfterDrawPlot(object sender, AfterDrawXYPlotEventArgs e)
        {
            roundPelengeBoundsWidth = e.Bounds.Width;
            roundPelengeBoundsHeight = e.Bounds.Height;
            roundPelengeBoundsX = e.Bounds.X;
            roundPelengeBoundsY = e.Bounds.Y;
        }

        private void scatterGraph1_PlotAreaBoundsChanged(object sender, EventArgs e)
        {
            UpdateLabels();
        }

        private void scatterGraph1_PlotAreaMouseWheel(object sender, MouseEventArgs e)
        {
            if ((Control.ModifierKeys & Keys.Control) == Keys.Control || (Control.ModifierKeys & Keys.Shift) == Keys.Shift)
            {
                return;
            }


            double MinVisibleRangeMhz = 0.25; // 250 килоГерц 

            var range = scatterGraph1.XAxes[0].Range;
            var size = (range.Maximum - range.Minimum) / 2;
            var s = e.Delta < 0 ? 1.2f : 1 / 1.2f;
            size *= s;
            if (size < 0.5 * MinVisibleRangeMhz)
            {
                size = 0.5 * MinVisibleRangeMhz;
            }

            var pos = 1d * (scatterGraph1.PointToClient(System.Windows.Forms.Cursor.Position).X - scatterGraph1.PlotAreaBounds.Left) / scatterGraph1.PlotAreaBounds.Width;
            var mouseFrequencyMhz = range.Minimum + range.Interval * pos;
            var center = size + mouseFrequencyMhz - 2 * pos * size;

            var left = center - size;
            var right = center + size;
            if (left < -12)
            {
                left = -12;
            }
            if (right > 12)
            {
                right = 12;
            }
            scatterGraph1.XAxes[0].Range = new Range(left, right);



            double yMinVisibleRangeMhz = 0.25; // 250 килоГерц 

            var yrange = scatterGraph1.YAxes[0].Range;
            var ysize = (yrange.Maximum - yrange.Minimum) / 2;
            var ys = e.Delta < 0 ? 1.2f : 1 / 1.2f;
            ysize *= ys;
            if (ysize < 0.5 * yMinVisibleRangeMhz)
            {
                ysize = 0.5 * yMinVisibleRangeMhz;
            }

            var ypos = 1d * (scatterGraph1.PointToClient(System.Windows.Forms.Cursor.Position).Y - scatterGraph1.PlotAreaBounds.Top) / scatterGraph1.PlotAreaBounds.Height;
            ypos = 1 - ypos;
            var ymouseFrequencyMhz = yrange.Minimum + yrange.Interval * ypos;
            var ycenter = ysize + ymouseFrequencyMhz - 2 * ypos * ysize;

            var yleft = ycenter - ysize;
            var yright = ycenter + ysize;

            if (yleft < -12)
            {
                yleft = -12;
            }
            if (yright > 12)
            {
                yright = 12;
            }

            scatterGraph1.YAxes[0].Range = new Range(yleft, yright);
        }

        private void toolStripButton5_Click(object sender, EventArgs e)
        {
            currindex = 5;

            button3.Visible = true;
            button34.Visible = false;
            label9.Visible = true;
            label10.Visible = true;

            label94.Visible = false;
            label104.Visible = false;

            label95.Visible = true;

            //if (variableWork.Regime == 1 || variableWork.Regime == 2)
            //    label11.Visible = true;

            labelLeft.Visible = true;
            labelRight.Visible = true;

            toolStripButton1.Checked = false;
            toolStripButton2.Checked = false;
            toolStripButton3.Checked = false;
            toolStripButton4.Checked = false;
            toolStripButton5.Checked = true;

            waveformGraph1.Visible = true;
            xyDataScatterGraph.Visible = false;

            //groupBox1.Visible = false;
            scGraph2andLabelVisible(false);

            scatterGraph1.Visible = false;
            intensityGraph1.Visible = true;
            groupBox2.Visible = false;

            pictureBox1.Visible = true;

            Panorama_Resize();

            //if (radioButton1.Checked || radioButton2.Checked)

            intensityTimer.Enabled = true;

            buttonExDF.Checked = false;
        }

        private void intensityTimer_Tick(object sender, EventArgs e)
        {
            if (isServConnected == true)
            {
                if (RPwoDF || RPwDF)
                {
                    //IntensityRequest();
                    int TimeLength = 60;
                    //IntensityRequestNew(TimeLength);
                    //IntensityRequestNew2(TimeLength);
                    IntensityRequestNew3(TimeLength);
                }
            }
        }


        //Запрос частотно-временной панорамы
        private async void IntensityRequest()
        {
            GetSpectrumResponse answer = new GetSpectrumResponse();

            double intensityRequestMinFrequency = GlobalRangeMin;
            double intensityRequestMaxFrequency = GlobalRangeMax;

            if (intensityXAxis4.Range.Minimum > GlobalRangeMin)
                intensityRequestMinFrequency = intensityXAxis4.Range.Minimum;
            if (intensityXAxis4.Range.Maximum < GlobalRangeMax)
                intensityRequestMaxFrequency = intensityXAxis4.Range.Maximum;

            bool allisok = true;
            double Coefficient = 10.0;
            int PointCount = Convert.ToInt32(((intensityRequestMaxFrequency - intensityRequestMinFrequency) * GlobalDotsPerBand / GlobalBandWidthMHz) / Coefficient);

            try
            {
                //answer = dsp.GetSpectrum(intensityRequestMinFrequency, intensityRequestMaxFrequency, PointCount);
                answer = await VariableWork.aWPtoBearingDSPprotocolNew.GetSpectrum(intensityRequestMinFrequency, intensityRequestMaxFrequency, PointCount);
            }
            catch (Exception)
            {
                if (preparationflag == false)
                {
                    allisok = false;
                    /*
                    BeginConnectTask = new Task(BeginConnect);
                    BeginConnectTask.Start();
                    BeginConnectTask.Wait();
                     * */
                }
            }

            if (allisok)
            {
                if (answer != null)
                    IntensityPainted(answer, intensityRequestMinFrequency, intensityRequestMaxFrequency, PointCount, Coefficient);
            }

        }

        private async void IntensityRequestNew(int TimeLength)
        {
            double intensityRequestMinFrequency = GlobalRangeMin;
            double intensityRequestMaxFrequency = GlobalRangeMax;

            GetAmplitudeTimeSpectrumResponse answer = new GetAmplitudeTimeSpectrumResponse();
            double Coefficient = 50.0;
            int PointCount = 20000;

            try
            {
                if (intensityXAxis4.Range.Minimum > GlobalRangeMin)
                    intensityRequestMinFrequency = intensityXAxis4.Range.Minimum;
                if (intensityXAxis4.Range.Maximum < GlobalRangeMax)
                    intensityRequestMaxFrequency = intensityXAxis4.Range.Maximum;

                PointCount = Convert.ToInt32(((intensityRequestMaxFrequency - intensityRequestMinFrequency) * GlobalDotsPerBand / GlobalBandWidthMHz) / Coefficient);

                answer = await VariableWork.aWPtoBearingDSPprotocolNew.GetAmplitudeTimeSpectrum(intensityRequestMinFrequency, intensityRequestMaxFrequency, PointCount, (byte)TimeLength);
            }
            catch (Exception)
            {
                Console.WriteLine("Panorama: ничего страшного, просто закрываем программу");
            }


            if (answer != null)
                if (answer.Header.ErrorCode == 0)
                    IntensityPaintedNew(answer, intensityRequestMinFrequency, intensityRequestMaxFrequency, PointCount, Coefficient, TimeLength);
        }
        private async void IntensityRequestNew2(int TimeLength)
        {
            double intensityRequestMinFrequency = GlobalRangeMin;
            double intensityRequestMaxFrequency = GlobalRangeMax;

            GetAmplitudeTimeSpectrumResponse answer = new GetAmplitudeTimeSpectrumResponse();
            double Coefficient = 10.0;
            int PointCount = 20000;

            try
            {
                if (intensityXAxis4.Range.Minimum > GlobalRangeMin)
                    intensityRequestMinFrequency = intensityXAxis4.Range.Minimum;
                if (intensityXAxis4.Range.Maximum < GlobalRangeMax)
                    intensityRequestMaxFrequency = intensityXAxis4.Range.Maximum;

                Coefficient = 10.0 + (((intensityRequestMaxFrequency - intensityRequestMinFrequency) - GlobalBandWidthMHz) / ((GlobalRangeMax - GlobalRangeMin) - GlobalBandWidthMHz)) * 10.0;

                PointCount = Convert.ToInt32(((intensityRequestMaxFrequency - intensityRequestMinFrequency) * GlobalDotsPerBand / GlobalBandWidthMHz) / Coefficient);

                if (intensityGraph1.Width > 0)
                    PointCount = Math.Min(PointCount, intensityGraph1.Width * 4);

                if ((intensityRequestMaxFrequency - intensityRequestMinFrequency) >= 30)
                {
                    answer = await VariableWork.aWPtoBearingDSPprotocolNew.GetAmplitudeTimeSpectrum(intensityRequestMinFrequency, intensityRequestMaxFrequency, PointCount, (byte)TimeLength);
                }
                else
                {
                    if (intensityGraph1.Width > 0)
                    {
                        PointCount = intensityGraph1.Width * 2;
                        answer = await VariableWork.aWPtoBearingDSPprotocolNew.GetAmplitudeTimeSpectrum(intensityRequestMinFrequency, intensityRequestMaxFrequency, PointCount, (byte)TimeLength);
                    }
                }
            }
            catch (Exception)
            {
                Console.WriteLine("Panorama: ничего страшного, просто закрываем программу");
            }

            Coefficient = ((intensityRequestMaxFrequency - intensityRequestMinFrequency) * GlobalDotsPerBand / GlobalBandWidthMHz / (double)PointCount);

            try
            {
                if (answer != null)
                    if (answer.Header.ErrorCode == 0)
                        IntensityPaintedNew(answer, intensityRequestMinFrequency, intensityRequestMaxFrequency, PointCount, Coefficient, TimeLength);
            }
            catch (Exception)
            {
                Console.WriteLine("Panorama: Не получилось отрисовать водопад");
            }
        }
        private async void IntensityRequestNew3(int TimeLength)
        {
            double intensityRequestMinFrequency = GlobalRangeMin;
            double intensityRequestMaxFrequency = GlobalRangeMax;

            GetAmplitudeTimeSpectrumResponse answer = new GetAmplitudeTimeSpectrumResponse();

            double Coefficient = 10.0;
            int PointCount = 2000;

            try
            {
                if (intensityXAxis4.Range.Minimum > GlobalRangeMin)
                    intensityRequestMinFrequency = intensityXAxis4.Range.Minimum;
                if (intensityXAxis4.Range.Maximum < GlobalRangeMax)
                    intensityRequestMaxFrequency = intensityXAxis4.Range.Maximum;

                if (intensityGraph1.Width > 0)
                    PointCount = intensityGraph1.Width * 2;

                answer = await VariableWork.aWPtoBearingDSPprotocolNew.GetAmplitudeTimeSpectrum(intensityRequestMinFrequency, intensityRequestMaxFrequency, PointCount, (byte)TimeLength);
            }
            catch (Exception)
            {
                Console.WriteLine("Panorama: ничего страшного, просто закрываем программу");
            }

            Coefficient = ((intensityRequestMaxFrequency - intensityRequestMinFrequency) * GlobalDotsPerBand / GlobalBandWidthMHz / (double)PointCount);

            try
            {
                if (answer != null)
                    if (answer.Header.ErrorCode == 0)
                        //IntensityPaintedNew(answer, intensityRequestMinFrequency, intensityRequestMaxFrequency, PointCount, Coefficient, TimeLength);
                        IntensityPaintedNew2(answer, intensityRequestMinFrequency, intensityRequestMaxFrequency, PointCount, Coefficient, TimeLength);
            }
            catch (Exception)
            {
                Console.WriteLine("Panorama: Не получилось отрисовать водопад");
            }
        }

        //Отрисовка частотно-временной панорамы
        List<float[]> FList = new List<float[]>();
        int findex = 60;
        private void IntensityPainted(GetSpectrumResponse data, double MinFrequency, double MaxFrequency)
        {
            int N = Convert.ToInt32((MaxFrequency - MinFrequency) * (GlobalDotsPerBand / GlobalBandWidthMHz));
            float[] ftemp = new float[N];

            for (int i = 0; i < N; i++)
            {
                ftemp[i] = (float)((-1) * data.Spectrum[i]);
            }

            FList.Add(ftemp);
            if (FList.Count == findex)
                FList.RemoveAt(0);

            int nofshowndots = N;
            double nmindot = (MinFrequency - GlobalRangeMin) / GlobalBandWidthMHz * GlobalDotsPerBand;

            double[,] dd = new double[nofshowndots, findex];
            for (int i = 0; i < findex; i++)
            {
                for (int j = 0; j < nofshowndots; j++)
                {
                    if (i < FList.Count && j < FList[i].Length)
                    {
                        if (FList[FList.Count - 1 - i][j] > porogCursor.YPosition)
                            dd[j, findex - 1 - i] = FList[FList.Count - 1 - i][j];
                        else dd[j, findex - 1 - i] = -121;
                    }
                    else
                        dd[j, findex - 1 - i] = -121;
                }
            }

            intensityGraph1.Plot(dd, nmindot, 1, intensityYAxis2.Range.Minimum, 1);
        }
        private void IntensityPainted(GetSpectrumResponse data, double MinFrequency, double MaxFrequency, double Coefficient)
        {
            int N = Convert.ToInt32(((MaxFrequency - MinFrequency) * (GlobalDotsPerBand / GlobalBandWidthMHz)) / Coefficient);
            float[] ftemp = new float[N];

            for (int i = 0; i < N; i++)
            {
                ftemp[i] = (float)((-1) * data.Spectrum[i]);
            }

            FList.Add(ftemp);
            if (FList.Count == findex)
                FList.RemoveAt(0);

            int nofshowndots = N;
            double nmindot = (MinFrequency - GlobalRangeMin) / GlobalBandWidthMHz * GlobalDotsPerBand;

            double[,] dd = new double[nofshowndots, findex];
            for (int i = 0; i < findex; i++)
            {
                for (int j = 0; j < nofshowndots; j++)
                {
                    if (i < FList.Count && j < FList[i].Length)
                    {
                        if (FList[FList.Count - 1 - i][j] > porogCursor.YPosition)
                            dd[j, findex - 1 - i] = FList[FList.Count - 1 - i][j];
                        else dd[j, findex - 1 - i] = -121;
                    }
                    else
                        dd[j, findex - 1 - i] = -121;
                }
            }

            intensityGraph1.Plot(dd, nmindot, 1 * Coefficient, intensityYAxis2.Range.Minimum, 1);

        }
        private void IntensityPainted(GetSpectrumResponse data, double MinFrequency, double MaxFrequency, int NPointCount, double Coefficient)
        {
            int N = NPointCount;
            float[] ftemp = new float[N];

            for (int i = 0; i < N; i++)
            {
                ftemp[i] = (float)((-1) * data.Spectrum[i]);
            }

            int counterdelete = 0;
            for (int i = 0; i < FList.Count; i++)
            {
                if (FList[i].Count() != N)
                    counterdelete++;
            }
            for (int i = 0; i < counterdelete; i++)
            {
                FList.RemoveAt(0);
            }
            FList.Add(ftemp);

            if (FList.Count == findex)
                FList.RemoveAt(0);

            int nofshowndots = N;
            double nmindot = (MinFrequency - GlobalRangeMin) / GlobalBandWidthMHz * GlobalDotsPerBand;

            double[,] dd = new double[nofshowndots, findex];
            for (int i = 0; i < findex; i++)
            {
                for (int j = 0; j < nofshowndots; j++)
                {
                    if (i < FList.Count && j < FList[i].Length)
                    {
                        if (FList[FList.Count - 1 - i][j] > porogCursor.YPosition)
                            dd[j, findex - 1 - i] = FList[FList.Count - 1 - i][j];
                        else dd[j, findex - 1 - i] = -121;
                    }
                    else
                        dd[j, findex - 1 - i] = -121;
                }
            }

            intensityGraph1.Plot(dd, nmindot, 1 * Coefficient, intensityYAxis2.Range.Minimum, 1);
        }

        private void IntensityPaintedNew(GetAmplitudeTimeSpectrumResponse data, double MinFrequency, double MaxFrequency, int NPointCount, double Coefficient, int TimeLength)
        {
            double[,] dd = new double[NPointCount, TimeLength];

            //прямой порядок
            int k = 0;
            int count = 0;
            for (int i = 0; i < data.Spectrum.Count(); i++)
            {
                //double temp = (-1) * (double)data.Spectrum[i];
                double temp = MinSpectrValue + data.Spectrum[i];
                if (temp < porogCursor.YPosition || temp == 0)
                    temp = -121;
                dd[i - (NPointCount * k), k] = temp;
                count++;
                if (count == NPointCount)
                {
                    count = 0;
                    k++;
                }
            }

            double nmindot = (MinFrequency - GlobalRangeMin) / GlobalBandWidthMHz * GlobalDotsPerBand;
            intensityGraph1.Plot(dd, nmindot, 1 * Coefficient, intensityYAxis2.Range.Minimum, 1);
        }

        private void IntensityPaintedNew2(GetAmplitudeTimeSpectrumResponse data, double MinFrequency, double MaxFrequency, int NPointCount, double Coefficient, int TimeLength)
        {
            double[,] dd = new double[NPointCount, TimeLength];

            //прямой порядок
            int k = 0;
            int count = 0;
            for (int i = 0; i < data.Spectrum.Count(); i++)
            {
                //double temp = (-1) * (double)data.Spectrum[i];
                double temp = MinSpectrValue + data.Spectrum[i];
                //if (temp < porogCursor.YPosition || temp == 0)
                //    temp = -121;
                dd[i - (NPointCount * k), k] = temp;
                count++;
                if (count == NPointCount)
                {
                    count = 0;
                    k++;
                }
            }

            double nmindot = (MinFrequency - GlobalRangeMin) / GlobalBandWidthMHz * GlobalDotsPerBand;
            intensityGraph1.Plot(dd, nmindot, 1 * Coefficient, intensityYAxis2.Range.Minimum, 1);
        }


        private void intensityGraph1_MouseClick(object sender, MouseEventArgs e)
        {
            if ((Control.ModifierKeys & Keys.Control) == Keys.Control || (Control.ModifierKeys & Keys.Shift) == Keys.Shift)
            {
                return;
            }


            Rectangle rp = intensityPlot2.GetBounds();

            double intensityCursor1XPosition = 25;

            if (e.X >= rp.Left && e.X <= rp.Right && e.Y >= rp.Top && e.Y <= rp.Bottom)
            {

                intensityCursor1.XPosition = (e.X - intensityeBoundsX) * (intensityXAxis4.Range.Maximum - intensityXAxis4.Range.Minimum) / (intensityeBoundsWidth) + intensityXAxis4.Range.Minimum;
                intensityCursor1XPosition = intensityCursor1.XPosition;

                intensityCursor1.XPosition = (e.X - intensityeBoundsX) * (intensityXAxis3.Range.Maximum - intensityXAxis3.Range.Minimum) / (intensityeBoundsWidth) + intensityXAxis3.Range.Minimum;
                intensityCursor1.YPosition = (e.Y - intensityeBoundsY) * (intensityYAxis2.Range.Minimum - intensityYAxis2.Range.Maximum) / (intensityeBoundsHeight) + intensityYAxis2.Range.Maximum;

            }
            //label95.Text = String.Format("{0}: {1} {2}", freq, intensityCursor1.XPosition.ToString("F4"), MHz);
            label95.Text = String.Format("{0}: {1} {2}", freq, intensityCursor1XPosition.ToString("F4"), MHz);
        }

        private void intensityGraph1_AfterDrawPlot(object sender, AfterDrawIntensityPlotEventArgs e)
        {
            intensityeBoundsWidth = e.Bounds.Width;
            intensityeBoundsHeight = e.Bounds.Height;
            intensityeBoundsX = e.Bounds.X;
            intensityeBoundsY = e.Bounds.Y;
        }

        private void intensityGraph1_XAxisRangeChanged(object sender, IntensityXAxisEventArgs e)
        {
            FList.Clear();
        }

        private void intensityGraph1_PlotAreaBoundsChanged(object sender, EventArgs e)
        {
            UpdateLabels();
        }

        private Color LoadColor(string colorId)
        {
            //INIManager iniManager = new INIManager(Application.StartupPath + "\\Settings.ini");

            //Временное решение
            INIManager iniManager = new INIManager("E:\\work\\Try_Projects\\PanoramaLibrary\\PanoramaLibrary\\bin\\Debug\\Settings.ini");

            string hexColor = iniManager.GetPrivateString("WinAp2", "Color" + colorId);

            int hexValue = int.Parse(hexColor, System.Globalization.NumberStyles.HexNumber);

            return Color.FromArgb(hexValue);
        }

        private Color ColorFromSharpString(string sharpString)
        {
            string hexColor = sharpString.Replace("#", "FF");
            int hexValue = int.Parse(hexColor, System.Globalization.NumberStyles.HexNumber);
            return Color.FromArgb(hexValue);
        }

        Button[] buttons;
        private void LoadColors()
        {
            buttons = new Button[]
            {
                gbutton1, gbutton2, gbutton3, gbutton4, gbutton5, gbutton6, gbutton7_1, gbutton7_2, gbutton8, gbutton9,
            };

            for (int i = 0; i < buttons.Count(); i++)
            {
                buttons[i].BackColor = LoadColor(Convert.ToString(i + 1));
            }

        }


        private void SaveColor(string hexColor, string colorId)
        {
            INIManager iniManager = new INIManager(Application.StartupPath + "\\Settings.ini");
            iniManager.WritePrivateString("WinAp2", "Color" + colorId, hexColor);
        }

        private void SaveColors()
        {
            for (int i = 0; i < buttons.Count(); i++)
            {
                SaveColor(buttons[i].BackColor.ToArgb().ToString("X"), Convert.ToString(i + 1));
            }
        }


        private void newColorDialog(Button button)
        {
            ColorDialog MyDialog = new ColorDialog();
            MyDialog.AllowFullOpen = true;
            // Sets the initial color select to the current text color.
            MyDialog.Color = button.BackColor;

            // Update the text box color if the user clicks OK 
            if (MyDialog.ShowDialog() == DialogResult.OK)
                button.BackColor = MyDialog.Color;
        }


        private void recolorPanoramaBackground(Color color)
        {
            //waveformGraph1
            waveformGraph1.PlotAreaColor = color;
            //xyDataScatterGraph
            xyDataScatterGraph.PlotAreaColor = color;
            //scatterGraph1
            scatterGraph1.PlotAreaColor = color;
            //intensityGraph1
            intensityGraph1.PlotAreaColor = color;
            colorScale2.ColorMap.Clear();
            colorScale2.ColorMap.Add(-121, color);
            colorScale2.Visible = true;
            colorScale2.Visible = false;
            //scatterGraph1
            scatterGraph2.PlotAreaColor = color;

            label9.BackColor = color;
            label10.BackColor = color;
            label11.BackColor = color;

            label94.BackColor = color;
            label104.BackColor = color;

            label95.BackColor = color;

            button3.BackColor = color;
            button34.BackColor = color;

            labelLeft.BackColor = color;
            labelRight.BackColor = color;

        }
        private void recolorTrashhold(Color color)
        {
            porogCursor.Color = color;
        }
        private void recolorCursor(Color color)
        {
            SpectrxyCursor.Color = color;
            xyCursor1.Color = color;
            roundxyCursor.Color = color;

            label9.ForeColor = color;
            label10.ForeColor = color;
            label11.ForeColor = color;

            label94.ForeColor = color;
            label104.ForeColor = color;

            label95.ForeColor = color;

            xyPointAnnotation0.CaptionForeColor = color;
            xyPointAnnotation45.CaptionForeColor = color;
            xyPointAnnotation90.CaptionForeColor = color;
            xyPointAnnotation135.CaptionForeColor = color;
            xyPointAnnotation180.CaptionForeColor = color;
            xyPointAnnotation225.CaptionForeColor = color;
            xyPointAnnotation270.CaptionForeColor = color;
            xyPointAnnotation315.CaptionForeColor = color;

            xyrPointAnnotation0.CaptionForeColor = color;
            xyrPointAnnotation45.CaptionForeColor = color;
            xyrPointAnnotation90.CaptionForeColor = color;
            xyrPointAnnotation135.CaptionForeColor = color;
            xyrPointAnnotation180.CaptionForeColor = color;
            xyrPointAnnotation225.CaptionForeColor = color;
            xyrPointAnnotation270.CaptionForeColor = color;
            xyrPointAnnotation315.CaptionForeColor = color;
        }
        private void recolorGrid(Color color)
        {
            waveformPlot1.XAxis.MajorDivisions.GridColor = color;
            waveformPlot1.XAxis.MinorDivisions.GridColor = color;
            waveformPlot1.YAxis.MajorDivisions.GridColor = color;
            waveformPlot1.YAxis.MinorDivisions.GridColor = color;
        }
        private void recolorAmplitudePanorama(Color color)
        {
            waveformPlot1.LineColor = color;

            button3.ForeColor = color;
            button34.ForeColor = color;

            labelLeft.ForeColor = color;
            labelRight.ForeColor = color;
        }
        private void recolorBearingPanorama(Color color)
        {
            xyPlot.PointColor = color;

            rxyPlot.PointColor = color;
        }
        private void recolorTimePanorama(Color color, int index)
        {
            if (index == 0)
                colorScale2.HighColor = color;
            if (index == 1)
                colorScale2.LowColor = color;
        }
        private void recolorBackColor(Color color)
        {
            waveformGraph1.BackColor = color;
            xyDataScatterGraph.BackColor = color;
            //scatterGraph1
            intensityGraph1.BackColor = color;

            pictureBox1.BackColor = color;
            this.BackColor = color;

        }
        private void recolorText(Color color)
        {
            //waveformGraph1
            waveformPlot1.XAxis.MajorDivisions.LabelForeColor = color;
            waveformPlot1.YAxis.MajorDivisions.LabelForeColor = color;

            waveformPlot1.XAxis.MajorDivisions.TickColor = color;
            waveformPlot1.YAxis.MajorDivisions.TickColor = color;
            waveformPlot1.XAxis.MinorDivisions.TickColor = color;
            waveformPlot1.YAxis.MinorDivisions.TickColor = color;

            //xyDataScatterGraph
            xyPlot.XAxis.MajorDivisions.LabelForeColor = color;
            xyPlot.YAxis.MajorDivisions.LabelForeColor = color;

            xyPlot.XAxis.MajorDivisions.TickColor = color;
            xyPlot.YAxis.MajorDivisions.TickColor = color;
            xyPlot.XAxis.MinorDivisions.TickColor = color;
            xyPlot.YAxis.MinorDivisions.TickColor = color;
            //scatterGraph1


            //intensityGraph1
            intensityPlot2.XAxis.MajorDivisions.LabelForeColor = color;
            intensityPlot2.YAxis.MajorDivisions.LabelForeColor = color;

            intensityPlot2.XAxis.MajorDivisions.TickColor = color;
            intensityPlot2.YAxis.MajorDivisions.TickColor = color;
            intensityPlot2.XAxis.MinorDivisions.TickColor = color;
            intensityPlot2.YAxis.MinorDivisions.TickColor = color;
        }

        private void gbutton1_Click(object sender, EventArgs e)
        {
            newColorDialog(gbutton1);
            recolorPanoramaBackground(gbutton1.BackColor);
        }

        private void gbutton2_Click(object sender, EventArgs e)
        {
            newColorDialog(gbutton2);
            recolorTrashhold(gbutton2.BackColor);
        }

        private void gbutton3_Click(object sender, EventArgs e)
        {
            newColorDialog(gbutton3);
            recolorCursor(gbutton3.BackColor);
        }

        private void gbutton4_Click(object sender, EventArgs e)
        {
            newColorDialog(gbutton4);
            recolorGrid(gbutton4.BackColor);
        }

        private void gbutton5_Click(object sender, EventArgs e)
        {
            newColorDialog(gbutton5);
            recolorAmplitudePanorama(gbutton5.BackColor);
        }

        private void gbutton6_Click(object sender, EventArgs e)
        {
            newColorDialog(gbutton6);
            recolorBearingPanorama(gbutton6.BackColor);
        }

        private void gbutton7_1_Click(object sender, EventArgs e)
        {
            newColorDialog(gbutton7_1);
            recolorTimePanorama(gbutton7_1.BackColor, 0);
        }

        private void gbutton7_2_Click(object sender, EventArgs e)
        {
            newColorDialog(gbutton7_2);
            recolorTimePanorama(gbutton7_2.BackColor, 1);
        }

        private void gbutton8_Click(object sender, EventArgs e)
        {
            newColorDialog(gbutton8);
            recolorBackColor(gbutton8.BackColor);
        }

        private void gbutton9_Click(object sender, EventArgs e)
        {
            newColorDialog(gbutton9);
            recolorText(gbutton9.BackColor);
        }

        private void gbutton0_Click(object sender, EventArgs e)
        {
            INIManager iniManager = new INIManager("E:\\Colors.txt");
            iniManager.WritePrivateString("Panorama", "BackGround", waveformGraph1.PlotAreaColor.ToArgb().ToString("X"));
            iniManager.WritePrivateString("Panorama", "Threshold ", porogCursor.Color.ToArgb().ToString("X"));
            iniManager.WritePrivateString("Panorama", "Cross", SpectrxyCursor.Color.ToArgb().ToString("X"));
            iniManager.WritePrivateString("Panorama", "Grid", waveformPlot1.XAxis.MajorDivisions.GridColor.ToArgb().ToString("X"));
            iniManager.WritePrivateString("Panorama", "Label", waveformPlot1.XAxis.MajorDivisions.LabelForeColor.ToArgb().ToString("X"));
            iniManager.WritePrivateString("Panorama", "GlobalBackground", waveformGraph1.BackColor.ToArgb().ToString("X"));
            iniManager.WritePrivateString("Panorama", "GraphFA", waveformPlot1.LineColor.ToArgb().ToString("X"));
            iniManager.WritePrivateString("Panorama", "GraphFD", xyPlot.PointColor.ToArgb().ToString("X"));
            iniManager.WritePrivateString("Panorama", "GraphFT0", colorScale2.HighColor.ToArgb().ToString("X"));
            iniManager.WritePrivateString("Panorama", "GraphFT1", colorScale2.LowColor.ToArgb().ToString("X"));

        }

        void VariableWork_RecFtoRJEvent(double frequency)
        {
            // frequency MHz

            try
            {

                double curFreq = frequency;

                VariableSuppression variableSuppression = new VariableSuppression();
                double curAmpl = (-1) * variableSuppression.ThresholdDefault;


                USR_DLL.TSupprFWS tSupprFWS = USR_DLL.TSupprFWS.CreateID();


                //tSupprFWS.iID = 0;
                tSupprFWS.iFreq = (int)(curFreq * 10000);
                tSupprFWS.sBearing = -1;
                tSupprFWS.bLetter = 0;
                tSupprFWS.sLevel = (short)(curAmpl);
                tSupprFWS.bPrior = 2;
                tSupprFWS.bModulation = 1;
                tSupprFWS.bDeviation = 1;
                tSupprFWS.bManipulation = 0;
                tSupprFWS.bDuration = 1;

                //Функция определения литеры
                tSupprFWS.bLetter = IdentifyLetter(tSupprFWS.iFreq);

                USR_DLL.FunctionsUser FU = new USR_DLL.FunctionsUser();

                string str = "";

                if (tSupprFWS.bLetter != 0)
                    str = FU.ChangeIRI_FRCh_RPToVariableWork(tSupprFWS, variableWork.SupprFWS_Own.ToList(), 1, -1);
                //str = FU.ChangeIRI_FRCh_RPToVariableWork(tSupprFWS, variableWork.SupprFWS_Own.ToList(), 1, 0, -1);

                if (str != "")
                {
                    MessageBox.Show(str);
                }

            }
            catch { }

        }

        private bool bCheckIridiumInmarsat = false;
        void VariableWork_OnChangeCheckIridiumInmarsat(int iCheckIridiumInmarsat)
        {
            try
            {
                if (iCheckIridiumInmarsat > 0) bCheckIridiumInmarsat = true;
                else bCheckIridiumInmarsat = false;
            }
            catch { }
        }

        private async void buttonRS_Click(object sender, EventArgs e)
        {
            try
            {
                double curFreq = SpectrxyCursor.XPosition;
                double curAmpl = SpectrxyCursor.YPosition;

                int popravka = 25;

                double Coefficient = 10.0;

                double requestMinFrequency = GlobalRangeMin;
                double requestMaxFrequency = GlobalRangeMax;

                if (waveformPlot1.XAxis.Range.Minimum > GlobalRangeMin)
                    requestMinFrequency = waveformPlot1.XAxis.Range.Minimum;
                if (waveformPlot1.XAxis.Range.Maximum < GlobalRangeMax)
                    requestMaxFrequency = waveformPlot1.XAxis.Range.Maximum;

                var answer = await VariableWork.aWPtoBearingDSPprotocolNew.GetSpectrum(requestMinFrequency, requestMaxFrequency, Coefficient);

                if (answer != null)
                {
                    if (answer.Header.ErrorCode == 0)
                    {
                        popravka = Convert.ToInt32(popravka * (requestMaxFrequency - requestMinFrequency) / GlobalBandWidthMHz);

                        double[] dd = new double[answer.Spectrum.Count()];
                        for (int i = 0; i < answer.Spectrum.Count(); i++)
                        {
                            //dd[i] = (-1) * answer.Spectrum[i];
                            dd[i] = MinSpectrValue + answer.Spectrum[i];
                        }

                        double dif = curFreq - requestMinFrequency;
                        double dpos = (dif * dd.Count()) / (requestMaxFrequency - requestMinFrequency);
                        int pos = Convert.ToInt32(Math.Round(dpos));

                        int leftside = pos - popravka;
                        if (leftside < 0) leftside = 0;
                        int rigtside = pos + popravka;
                        if (rigtside > dd.Count()) rigtside = dd.Count();

                        curAmpl = dd[pos];
                        for (int i = leftside; i < rigtside; i++)
                        {
                            if (curAmpl < dd[i]) curAmpl = dd[i];
                        }


                        /*
                        List<USR_DLL.TSupprFWS> tSupprFWS = variableWork.SupprFWS_Own.ToList();

                        USR_DLL.TSupprFWS tSupprFWSitem = new USR_DLL.TSupprFWS();

                        tSupprFWSitem.iID = tSupprFWS.Count() +1;
                        tSupprFWSitem.iFreq = Convert.ToInt32(curFreq * 10000);
                        tSupprFWSitem.sLevel = Convert.ToInt16(curAmpl);

                        tSupprFWS.Add(tSupprFWSitem);

                        variableWork.SupprFWS_Own = tSupprFWS.ToArray();
                        */

                    }
                }

                //if (variableWork.SupprFWS_Own.Length != 0)
                //{ 
                //    //USR_DLL.TSupprFWS.SetMaxId(variableWork.SupprFWS_Own.ToList().Max(id => id.iID));
                //    USR_DLL.TSupprFWS.SetIdFWS(variableWork.SupprFWS_Own.ToList().Min(id => id.iID));
                //}
                //else 
                //{ 
                //    //USR_DLL.TSupprFWS.SetMaxId(0);
                //    USR_DLL.TSupprFWS.SetIdFWS(0);
                //}

                curAmpl = curAmpl - 5.0; ;
                if (curAmpl < -130)
                {
                    curAmpl = -130;
                }

                if (bCheckIridiumInmarsat)
                {
                    MessageBox.Show("В диапазоне литеры 8 осуществляется подавление IRIDIUM (INMARSAT)!", "Сообщение!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                 
                USR_DLL.TSupprFWS tSupprFWS = USR_DLL.TSupprFWS.CreateID();

                //tSupprFWS.iID = 0;
                tSupprFWS.iFreq = (int)(curFreq * 10000);
                tSupprFWS.sBearing = -1;
                tSupprFWS.bLetter = 0;
                tSupprFWS.sLevel = (short)(curAmpl);
                tSupprFWS.bPrior = 2;
                tSupprFWS.bModulation = 1;
                tSupprFWS.bDeviation = 1;
                tSupprFWS.bManipulation = 0;
                tSupprFWS.bDuration = 1;

                //Функция определения литеры
                tSupprFWS.bLetter = IdentifyLetter(tSupprFWS.iFreq);

                USR_DLL.FunctionsUser FU = new USR_DLL.FunctionsUser();

                string str = "";


                if (tSupprFWS.bLetter != 0)
                    str = FU.ChangeIRI_FRCh_RPToVariableWork(tSupprFWS, variableWork.SupprFWS_Own.ToList(), 1, -1);
                //str = FU.ChangeIRI_FRCh_RPToVariableWork(tSupprFWS, variableWork.SupprFWS_Own.ToList(), 1, 0, -1);

                if (str != "")
                {
                    MessageBox.Show(str);
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine("Panorama error:" + "buttonRS_Click" + ex.ToString());
            }

        }

        private byte IdentifyLetter(int iFreq)
        {
            int FREQ_START_LETTER_1 = 300000;
            int FREQ_START_LETTER_2 = 500000;
            int FREQ_START_LETTER_3 = 900000;
            int FREQ_START_LETTER_4 = 1600000;
            int FREQ_START_LETTER_5 = 2900000;
            int FREQ_START_LETTER_6 = 5120000;
            int FREQ_START_LETTER_7 = 8600000;
            int FREQ_START_LETTER_8 = 12150000;
            int FREQ_START_LETTER_9 = 20000000;
            int FREQ_STOP_LETTER_9 = 30000000;

            if (GlobalNumberofBands == 100)
            {
                // определить литеру
                if (iFreq >= FREQ_START_LETTER_1 & iFreq < FREQ_START_LETTER_2)
                    return 1;
                if (iFreq >= FREQ_START_LETTER_2 & iFreq < FREQ_START_LETTER_3)
                    return 2;
                if (iFreq >= FREQ_START_LETTER_3 & iFreq < FREQ_START_LETTER_4)
                    return 3;
                if (iFreq >= FREQ_START_LETTER_4 & iFreq < FREQ_START_LETTER_5)
                    return 4;
                if (iFreq >= FREQ_START_LETTER_5 & iFreq < FREQ_START_LETTER_6)
                    return 5;
                if (iFreq >= FREQ_START_LETTER_6 & iFreq < FREQ_START_LETTER_7)
                    return 6;
                if (iFreq >= FREQ_START_LETTER_7 & iFreq < FREQ_START_LETTER_8)
                    return 7;
            }
            if (GlobalNumberofBands == 200)
            {
                // определить литеру
                if (iFreq >= FREQ_START_LETTER_1 & iFreq < FREQ_START_LETTER_2)
                    return 1;
                if (iFreq >= FREQ_START_LETTER_2 & iFreq < FREQ_START_LETTER_3)
                    return 2;
                if (iFreq >= FREQ_START_LETTER_3 & iFreq < FREQ_START_LETTER_4)
                    return 3;
                if (iFreq >= FREQ_START_LETTER_4 & iFreq < FREQ_START_LETTER_5)
                    return 4;
                if (iFreq >= FREQ_START_LETTER_5 & iFreq < FREQ_START_LETTER_6)
                    return 5;
                if (iFreq >= FREQ_START_LETTER_6 & iFreq < FREQ_START_LETTER_7)
                    return 6;
                if (iFreq >= FREQ_START_LETTER_7 & iFreq < FREQ_START_LETTER_8)
                    return 7;
                if (iFreq >= FREQ_START_LETTER_8 & iFreq < FREQ_START_LETTER_9)
                    return 8;
                if (iFreq >= FREQ_START_LETTER_9 & iFreq <= FREQ_STOP_LETTER_9)
                    return 9;
            }

            return 0;
        }

        private void buttonTD_Click(object sender, EventArgs e)
        {
            //ThirdInitScatterGraph2(0.5, 0);
            //FourthInitScatterGraph2(null);
            //set_language("rus");
        }

        private void buttonAC_Click(object sender, EventArgs e)
        {
            //set_language("az");
            variableWork.Frequency = Convert.ToInt64(SpectrxyCursor.XPosition * 1000000);
        }

        private void WaveArrows(double[] Frequencies, double[] Amplitudes, Color color)
        {
            try
            {
                for (int i = 0; i < Frequencies.Length; i++)
                {
                    NationalInstruments.UI.XYPointAnnotation item = new NationalInstruments.UI.XYPointAnnotation();

                    item.ArrowColor = color;
                    item.ArrowHeadPosition = NationalInstruments.UI.ArrowHeadPosition.XYPosition;
                    item.ArrowHeadSize = new System.Drawing.Size(10, 10);
                    item.ArrowHeadStyle = NationalInstruments.UI.ArrowStyle.SolidStealth;
                    item.ArrowTailAlignment = NationalInstruments.UI.BoundsAlignment.None;
                    item.ArrowTailSize = new System.Drawing.Size(10, 10);
                    item.CaptionAlignment = new NationalInstruments.UI.AnnotationCaptionAlignment(NationalInstruments.UI.BoundsAlignment.None, 0F, -50F);
                    item.CaptionVisible = false;
                    item.ShapeVisible = false;
                    item.XPosition = Frequencies[i];
                    item.YPosition = Amplitudes[i];

                    waveformGraph1.Annotations.Add(item);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }

        }

        private void WaveClearArrows()
        {
            try
            {
                waveformGraph1.Annotations.Clear();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        private void WaveLines(double[] Frequencies, double[] Amplitudes, Color color)
        {
            for (int i = 0; i < Frequencies.Length; i++)
            {
                NationalInstruments.UI.XYPointAnnotation item = new NationalInstruments.UI.XYPointAnnotation();

                item.ArrowColor = color;
                item.ArrowHeadPosition = NationalInstruments.UI.ArrowHeadPosition.XYPosition;
                item.ArrowHeadSize = new System.Drawing.Size(10, 10);
                item.ArrowHeadStyle = NationalInstruments.UI.ArrowStyle.None;
                item.ArrowTailAlignment = NationalInstruments.UI.BoundsAlignment.None;
                item.ArrowTailSize = new System.Drawing.Size(10, 10);
                item.CaptionAlignment = new NationalInstruments.UI.AnnotationCaptionAlignment(NationalInstruments.UI.BoundsAlignment.None, 0F, -50F);
                item.CaptionVisible = false;
                item.ShapeVisible = false;
                item.XPosition = Frequencies[i];
                item.YPosition = Amplitudes[i];

                waveformGraph1.Annotations.Add(item);

                //XYAnnotationCollection Ann = new XYAnnotationCollection();
                //waveformGraph1.Annotations.AddRange(Ann);
            }

        }

        //Добавить в запрещенные частоты
        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if ((mouseDownXPoint != -1) && (mouseUpXPoint != -1) && (mouseDownXPoint != mouseUpXPoint))
            {
                //отправить в вариэбл по области (mouseDownXPoint,mouseUpXPoint)
                variableWork.SendSpecFreq((int)(mouseDownXPoint * 10000), (int)(mouseUpXPoint * 10000), 1, 1);
            }
            else
            {
                //отправка в вариэбл номинал частоты SpectrxyCursor.XPosition
                variableWork.SendSpecFreq((int)(SpectrxyCursor.XPosition * 10000), (int)(SpectrxyCursor.XPosition * 10000), 1, 1);
            }
        }

        //Добавить в известные частоты
        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            if ((mouseDownXPoint != -1) && (mouseUpXPoint != -1) && (mouseDownXPoint != mouseUpXPoint))
            {
                //отправить в вариэбл по области (mouseDownXPoint,mouseUpXPoint)
                variableWork.SendSpecFreq((int)(mouseDownXPoint * 10000), (int)(mouseUpXPoint * 10000), 2, 1);
            }
            else
            {
                //отправка в вариэбл номинал частоты SpectrxyCursor.XPosition
                variableWork.SendSpecFreq((int)(SpectrxyCursor.XPosition * 10000), (int)(SpectrxyCursor.XPosition * 10000), 2, 1);
            }
        }

        //Добавить в важные частоты
        private void toolStripMenuItem3_Click(object sender, EventArgs e)
        {
            if ((mouseDownXPoint != -1) && (mouseUpXPoint != -1) && (mouseDownXPoint != mouseUpXPoint))
            {
                //отправить в вариэбл по области (mouseDownXPoint,mouseUpXPoint)
                variableWork.SendSpecFreq((int)(mouseDownXPoint * 10000), (int)(mouseUpXPoint * 10000), 3, 1);
            }
            else
            {
                //отправка в вариэбл номинал частоты SpectrxyCursor.XPosition
                variableWork.SendSpecFreq((int)(SpectrxyCursor.XPosition * 10000), (int)(SpectrxyCursor.XPosition * 10000), 3, 1);
            }
        }

        bool flagO = false;
        bool flagP = false;

        bool flagY = false;

        private void waveformGraph1_KeyDown(object sender, KeyEventArgs e)
        {
            if (Keys.O == e.KeyCode)
            {
                flagO = true;
            }

            if (Keys.P == e.KeyCode)
            {
                flagP = true;
            }

            if (flagO == true && flagP == true && label11.Visible == true)
            {
                label11.Visible = false;
                flagO = false;
                flagP = false;
            }

            if (flagO == true && flagP == true && label11.Visible == false)
            {
                if (currindex != 4)
                {
                    if (variableWork.Regime != 0)
                    {
                        label11.Visible = true;
                        flagO = false;
                        flagP = false;
                    }
                }
            }

            if (Keys.Y == e.KeyCode)
            {
                if (flagY == false)
                {
                    yAxes.Range = new Range(MinSpectrRangeValue, MaxSpectrRangeValue);
                    flagY = true;
                }
                else
                if (flagY == true)
                {
                    yAxes.Range = new Range(GlobalYRangeMin, GlobalYRangeMax);
                    flagY = false;
                }
            }
        }





    }
}
