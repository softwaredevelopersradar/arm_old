﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using VariableDynamic;
using VariableStatic;
using GrozaBerezinaDLL;
using System.Drawing;
using System.IO.Ports;
using System.Net;
using FuncVLC;
using CntPC;
using System.Runtime.InteropServices;
using Microsoft.VisualBasic;
using USR_DLL;
using StructPC;

namespace WndProject
{
    partial class MainWnd
    {
        ///Обработка связи с Пунктом Управления Березина

        [DllImport("kernel32.dll", EntryPoint = "SetSystemTime", SetLastError = true)]
        public static extern bool SetSystemTime(ref SYSTEMTIME time);
        [DllImport("kernel32.dll", EntryPoint = "GetSystemTime", SetLastError = true)]
        public static extern bool GetSystemTime(ref SYSTEMTIME time);


        [StructLayout(LayoutKind.Sequential)]
        public struct SYSTEMTIME
        {
            public short wYear;
            public short wMonth;
            public short wDayOfWeek;
            public short wDay;
            public short wHour;
            public short wMinute;
            public short wSecond;
            public short wMilliseconds;
        }
        #region Groza
        static public IExGRZ_BRZ ClientPu;
        static public FuncVLC.StateConnection stateIndicateConnection = null;
        Color clConnect = Color.Green;
        Color clDisconnect = Color.Red;

        public StateIndicate stateIndicateReadPU = null;
        public StateIndicate stateIndicateWritePU = null;
        public StateIndicate stateIndicateReadHS = null;
        public StateIndicate stateIndicateWriteHS = null;
        Image imgReadByte = Properties.Resources.green;
        Image imgWriteByte = Properties.Resources.red;
        Image imgWaitByte = Properties.Resources.gray;
        short TIME_INDICATE = 300;
        const Int32 FreqMin_RR_Bel = 250000;
        const Int32 FreqMax_RR_Bel = 30250000;
        const Int32 FreqMin_RP_Bel = 300000;
        const Int32 FreqMax_RP_Bel = 12150000;

        const Int32 FreqMin_RR_BRZ = 250000;
        const Int32 FreqMax_RR_BRZ = 60250000;
        const Int32 FreqMin_RP_BRZ = 300000;
        const Int32 FreqMax_RP_BRZ = 30000000;


        #region диапазоны литер
        public struct RangesLetters
        {
            public static int FREQ_START_LETTER_1 = 300000;
            public static int FREQ_START_LETTER_2 = 500000;
            public static int FREQ_START_LETTER_3 = 900000;
            public static int FREQ_START_LETTER_4 = 1600000;
            public static int FREQ_START_LETTER_5 = 2900000;
            public static int FREQ_START_LETTER_6 = 5120000;
            public static int FREQ_START_LETTER_7 = 8600000;
            public static int FREQ_START_LETTER_8 = 10000000;
            public static int FREQ_START_LETTER_9 = 20000000;
            public static int FREQ_STOP_LETTER_9 = 30000000;
        }
        #endregion

        private void ReadByte(object sender, byte[] bData)
        {
            if (sender == MainWnd.ClientPu)
            {
                stateIndicateReadPU.SetIndicateOn();
            }
        }

        private void WriteByte(object sender, byte[] bData)
        {
            if (sender == MainWnd.ClientPu)
                stateIndicateWritePU.SetIndicateOn();
        }
        private void Connect(object sender)
        {
            if (sender == ClientPu)
                stateIndicateConnection.SetConnectOn();

        }
        private void Disconnect(object sender)
        {
            if (sender == ClientPu)
                stateIndicateConnection.SetConnectOff();
            ClientPu = null;

        }
        public Parity PortParity(VariableConnection.StructPc structParamsPort)
        {
            Parity parity = new Parity();
            switch (structParamsPort.Parity)
            {
                case "None":
                    parity = Parity.None;
                    break;
                case "Odd":
                    parity = Parity.Odd;
                    break;
                case "Even":
                    parity = Parity.Even;
                    break;
                case "Mark":
                    parity = Parity.Mark;
                    break;
                case "Space":
                    parity = Parity.Space;
                    break;
                default:
                    break;
            }
            return parity;
        }

        public StopBits PortStopBits(VariableConnection.StructPc structParamsPort)
        {
            StopBits stopBits = new StopBits();
            switch (structParamsPort.StopBits)
            {
                case "None":
                    stopBits = StopBits.None;
                    break;
                case "One":
                    stopBits = StopBits.One;
                    break;
                case "Two":
                    stopBits = StopBits.Two;
                    break;
                case "OnePointFive":
                    stopBits = StopBits.OnePointFive;
                    break;
                default:
                    break;
            }
            return stopBits;
        }


        private void btPU_Click(object sender, EventArgs e)
        {

            switch (variableCommon.TypeStation)
            {
                case 0:
                case 2:
                    if (ClientPu_Bel != null)
                    {
                        ClientPu_Bel.ClosePort();
                        btPU.BackColor = Color.Red;
                        ClientPu_Bel = null;
                    }
                    else
                    {
                        InitIExGRZ_Bel();
                    }
                    break;
                case 1:
                    if (ClientPu != null)
                    {
                        ClientPu.Disconnect();
                        ClientPu = null;
                        btPU.BackColor = Color.Red;
                    }
                    else
                    {
                        InitIExGRZ_BRZ();
                    }
                    break;
            }
            
        }
        private void InitIExGRZ_BRZ()
        {

            messPU.sText = new List<string>();
            messPU.sTime = new List<string>();
            if (variableCommon.TypeStation == 0 || variableCommon.TypeStation == 2)
                variableConnection.PostControlType = 1;
            else
                if (variableConnection.PostControlType == 1)
                    variableConnection.PostControlType = 2;
            MainWnd.stateIndicateConnection = new FuncVLC.StateConnection(btPU, clConnect, clDisconnect);
            var postControlStract = variableConnection.PostControlStruct;

            ClientPu = new IExGRZ_BRZ(Convert.ToByte(variableCommon.OwnAddressPC));
            stateIndicateReadPU = new StateIndicate(pbReadPU, imgReadByte, imgWaitByte, TIME_INDICATE);
            stateIndicateWritePU = new StateIndicate(pbWritePU, imgWriteByte, imgWaitByte, TIME_INDICATE);
            MainWnd.ClientPu.OnConnect += new GrozaBerezinaDLL.IExGRZ_BRZ.ConnectEventHandler(Connect);
            MainWnd.ClientPu.OnDisconnect += new GrozaBerezinaDLL.IExGRZ_BRZ.ConnectEventHandler(Disconnect);
            MainWnd.ClientPu.OnReadByte += new GrozaBerezinaDLL.IExGRZ_BRZ.ByteEventHandler(ReadByte);
            MainWnd.ClientPu.OnWriteByte += new GrozaBerezinaDLL.IExGRZ_BRZ.ByteEventHandler(WriteByte);

            MainWnd.ClientPu.OnRequestSynchTime += new IExGRZ_BRZ.RequestSynchTimeEventHandler(Receive_RequestSynchTime); //Синхронизация времени

            MainWnd.ClientPu.OnRequestRegime += new IExGRZ_BRZ.RequestRegimeEventHandler(Receive_RequestRegime); //Устаноква Режима работы

            MainWnd.ClientPu.OnRequestRangeSpec += new IExGRZ_BRZ.RequestRangeSpecEventHandler(Receive_RequestRangeSpec); //Специальные частоты

            MainWnd.ClientPu.OnRequestSupprFWS += new IExGRZ_BRZ.RequestSupprFWSEventHandler(Receive_RequestSupprFWS); //Назначение ФРЧ на РП

            MainWnd.ClientPu.OnRequestState += new IExGRZ_BRZ.RequestStateEventHandler(Receive_RequestState); //Запрос состояния

            MainWnd.ClientPu.OnRequestRangeSector += new IExGRZ_BRZ.RequestRangeSectorEventHandler(Receive_RequestRangeSector); //Сектора и диапазоны

            if (objChangeMessage.IsLoaded)
                MainWnd.ClientPu.OnReceiveTextCmd += new GrozaBerezinaDLL.IExGRZ_BRZ.CmdTextEventHandler(Receive_TextCmd);
            else
                MainWnd.ClientPu.OnReceiveTextCmd += ClientPu_OnReceiveTextCmd;
            MainWnd.ClientPu.OnReceiveConfirmTextCmd += new GrozaBerezinaDLL.IExGRZ_BRZ.ConfirmTextEventHandler(Receive_ConfirmTextCmd);

            MainWnd.ClientPu.OnSendTextCmd += new GrozaBerezinaDLL.IExGRZ_BRZ.CmdTextEventHandler(Send_TextCmd);

            MainWnd.ClientPu.OnRequestReconFWS += ClientPu_OnRequestReconFWS; //Запрос ИРИ ФРЧ
            MainWnd.ClientPu.OnRequestReconFHSS += ClientPu_OnRequestReconFHSS; //Запрос ИРИ ППРЧ 
            MainWnd.ClientPu.OnRequestSupprFHSS += ClientPu_OnRequestSupprFHSS; ////Назначение ППРЧ на РП 
            MainWnd.ClientPu.OnRequestCoord += ClientPu_OnRequestCoord; // Запрос координат
            MainWnd.ClientPu.OnRequestExecBear += ClientPu_OnRequestExecBear; // Запрос на исполнительное пеленгование
            MainWnd.ClientPu.OnRequestSimulBear += ClientPu_OnRequestSimulBear; // Запрос на квазиодновременное пеленгование

            MainWnd.ClientPu.OnRequestStateSupprFHSS += ClientPu_OnRequestStateSupprFHSS;//Квитанция ири ППРЧ РП
            MainWnd.ClientPu.OnRequestStateSupprFWS += ClientPu_OnRequestStateSupprFWS;//Квитанция ири ФРЧ РП

            //подписка на обновления данных о состоянии ФРЧ на РП
            VariableWork.aWPtoBearingDSPprotocolNew.RadioJamStateUpdate += aWPtoBearingDSPprotocolNew_RadioJamStateUpdate;
            //подписка на обновления данных о состоянии ППРЧ на РП
            VariableWork.aWPtoBearingDSPprotocolNew.FhssRadioJamUpdate += aWPtoBearingDSPprotocolNew_FhssRadioJamUpdate;

            stateIndicateReadPU.SetIndicateOn();
            stateIndicateWritePU.SetIndicateOn();
            bool ans = false;
            if (postControlStract.Address != null) { ans = ClientPu.Connect(postControlStract.Address, postControlStract.Port); }
            else
            {
                if (postControlStract.Com != null)
                {
                    try
                    {
                        ClientPu.Connect(postControlStract.Com, postControlStract.BaudRate, 
                            PortParity(postControlStract), postControlStract.DataBits, PortStopBits(postControlStract));                        
                    }
                    catch { }
                }
            }
            if (ans)
            {
                btPU.BackColor = Color.Green;
            }
        }



        StructTypeGR_BRZ.TResSupprFWS[] StateSuppFWS; // Cостояние ФРЧ на РП
        void aWPtoBearingDSPprotocolNew_RadioJamStateUpdate(Protocols.RadioJamStateUpdateEvent answer)
        {
            if (answer == null)
                return;
            if (answer.Header.ErrorCode != 0)
                return;
            StateSuppFWS = new StructTypeGR_BRZ.TResSupprFWS[answer.TargetStates.Length];
            for (int i = 0; i < answer.TargetStates.Length; i++)
            {
                StateSuppFWS[i].iID = answer.TargetStates[i].Id;
                StateSuppFWS[i].iFreq = answer.TargetStates[i].Frequency;
                StateSuppFWS[i].bResCtrl = answer.TargetStates[i].ControlState;
                StateSuppFWS[i].bResSuppr = answer.TargetStates[i].RadioJamState;
            }
        }

        StructTypeGR_BRZ.TResSupprFHSS[] StateSuppFHSS; // Cостояние ППРЧ на РП
        void aWPtoBearingDSPprotocolNew_FhssRadioJamUpdate(Protocols.FhssRadioJamUpdateEvent answer)
        {
            if (answer == null)
                return;
            if (answer.Header.ErrorCode != 0)
                return;
            StateSuppFHSS = new StructTypeGR_BRZ.TResSupprFHSS[answer.JammingStates.Length];
            for (int i = 0; i < answer.JammingStates.Length; i++)
            {
                StateSuppFHSS[i].iID = answer.JammingStates[i].Id;
                StateSuppFHSS[i].iFreqMin = answer.Frequencies.Min();
                StateSuppFHSS[i].iFreqMax = answer.Frequencies.Max();
                StateSuppFHSS[i].bResCtrl = 1;
                StateSuppFHSS[i].bResSuppr = Convert.ToByte(answer.JammingStates[i].IsJammed);
            }
            
        }

        //Квитанция ири ФРЧ РП
        void ClientPu_OnRequestStateSupprFWS(object sender)
        {
            try
            {
                if (StateSuppFWS == null)
                    MainWnd.ClientPu.SendStateSupprFWS(1, null);
                else
                    MainWnd.ClientPu.SendStateSupprFWS(0, StateSuppFWS);
            }
            catch
            { MainWnd.ClientPu.SendStateSupprFWS(1, null); }
        }

        //Квитанция ири ППРЧ РП
        void ClientPu_OnRequestStateSupprFHSS(object sender)
        {
            try
            {
                if (StateSuppFHSS == null)
                    MainWnd.ClientPu.SendStateSupprFHSS(1, null);
                else
                    MainWnd.ClientPu.SendStateSupprFHSS(0, StateSuppFHSS);
            }
            catch
            { MainWnd.ClientPu.SendStateSupprFHSS(1, null); }
        }

        //Запрос координат
        void ClientPu_OnRequestCoord(object sender)
        {
            byte ErrorCode = 0;
            StructTypeGR_BRZ.TCoord tCoord = new StructTypeGR_BRZ.TCoord();
            try
            {          
                ConvertCoord convertCoord = new ConvertCoord();
                CoordDegMinSec coordDegMinSec = new CoordDegMinSec();
                coordDegMinSec = convertCoord.DegToDegMinSec(Math.Abs(variableWork.CoordsGNSS[0].Lat), Math.Abs(variableWork.CoordsGNSS[0].Lon));
                tCoord.bDegreeLatitude = (byte)coordDegMinSec.Lat[0].LatDeg;
                tCoord.bDegreeLongitude = (byte)coordDegMinSec.Lon[0].LonDeg;
                tCoord.bMinuteLatitude = (byte)coordDegMinSec.Lat[0].LatMin;
                tCoord.bMinuteLongitude = (byte)coordDegMinSec.Lon[0].LonMin;
                tCoord.bSecondLatitude = (byte)coordDegMinSec.Lat[0].LatSec;
                tCoord.bSecondLongitude = (byte)coordDegMinSec.Lon[0].LonSec;
                tCoord.bSignLatitude = variableWork.CoordsGNSS[0].signLat;
                tCoord.bSignLongitude = variableWork.CoordsGNSS[0].signLon;
            }
            catch { ErrorCode = 1; }
            MainWnd.ClientPu.SendCoord(ErrorCode, tCoord);
        }

        //Запись в структуру пришедших текстовых сообщений(Если форма "Сообщения" уже загружена - запить будет происходить прямо в элемент формы)
        void ClientPu_OnReceiveTextCmd(object sender, string strText)
        {
            try
            {
                messPU.sTime.Add(System.DateTime.Now.ToString("hh:mm:ss"));
                messPU.sText.Add(strText);
            }
            catch { }
        }

        //Поиск попадания в Литеру
        byte LetterFind(int iFreq)
        {
            byte bLetter = 10;
            if (iFreq >= RangesLetters.FREQ_START_LETTER_1 & iFreq < RangesLetters.FREQ_START_LETTER_2)
                bLetter = 1;
            if (iFreq >= RangesLetters.FREQ_START_LETTER_2 & iFreq < RangesLetters.FREQ_START_LETTER_3)
                bLetter = 2;
            if (iFreq >= RangesLetters.FREQ_START_LETTER_3 & iFreq < RangesLetters.FREQ_START_LETTER_4)
                bLetter = 3;
            if (iFreq >= RangesLetters.FREQ_START_LETTER_4 & iFreq < RangesLetters.FREQ_START_LETTER_5)
                bLetter = 4;
            if (iFreq >= RangesLetters.FREQ_START_LETTER_5 & iFreq < RangesLetters.FREQ_START_LETTER_6)
                bLetter = 5;
            if (iFreq >= RangesLetters.FREQ_START_LETTER_6 & iFreq < RangesLetters.FREQ_START_LETTER_7)
                bLetter = 6;
            if (iFreq >= RangesLetters.FREQ_START_LETTER_7 & iFreq < RangesLetters.FREQ_START_LETTER_8)
                bLetter = 7;
            if (iFreq >= RangesLetters.FREQ_START_LETTER_8 & iFreq < RangesLetters.FREQ_START_LETTER_9)
                bLetter = 8;
            if (iFreq >= RangesLetters.FREQ_START_LETTER_9 & iFreq <= RangesLetters.FREQ_STOP_LETTER_9)
                bLetter = 9;
            return bLetter;
        }
        
        //ИРИ ППРЧ на РП
        void ClientPu_OnRequestSupprFHSS(object sender, int iDuration, StructTypeGR_BRZ.TSupprFHSS[] tSupprFHSS)
        {
            byte ErrorCode = 0;
            try
            {
                Invoke((MethodInvoker)(() => (new VariableSuppression()).TimeRadiatFHSS = (short)iDuration));
                List<TDistribFHSS_RP> tempMass = new List<TDistribFHSS_RP>();
                TDistribFHSS_RP temp = new TDistribFHSS_RP();
                for (int i = 0; i < tSupprFHSS.Length; i++)
                {
                    if (tSupprFHSS[i].iFreqMin < FreqMin_RP_BRZ || tSupprFHSS[i].iFreqMax > FreqMax_RP_BRZ)
                    {
                        ErrorCode = 1;
                    }
                    else
                    {
                        temp.bDeviation = tSupprFHSS[i].bDeviation;
                        temp.iFreqMin = tSupprFHSS[i].iFreqMin;
                        temp.iFreqMax = tSupprFHSS[i].iFreqMax;
                        temp.bModulation = tSupprFHSS[i].bModulation;
                        temp.bManipulation = tSupprFHSS[i].bManipulation;
                        temp.bDuration = 1;
                        temp.iID = tSupprFHSS[i].iID;
                        temp.bCodeFFT = tSupprFHSS[i].bCodeFFT;
                        temp.bLetter = new byte[2];
                        temp.bLetter[0] = LetterFind(tSupprFHSS[i].iFreqMin);
                        temp.bLetter[1] = LetterFind(tSupprFHSS[i].iFreqMax);
                        temp.sLevel = -60;
                        temp.iStep = 100;
                        tempMass.Add(temp);
                    }
                }
                    Invoke((MethodInvoker)(() => variableWork.DistribFHSS_RPOwn = tempMass.ToArray()));
            }
            catch (SystemException)
            { ErrorCode = 2; }

            if (MainWnd.ClientPu != null)
                MainWnd.ClientPu.SendSupprFHSS(ErrorCode);
        }

        //ИРИ ППРЧ
        async void ClientPu_OnRequestReconFHSS(object sender)
        {
            StructTypeGR_BRZ.TReconFHSS[] tReconFHSS = new StructTypeGR_BRZ.TReconFHSS[variableWork.DistribFHSS.Length];
            for (int i = 0; i < variableWork.DistribFHSS.Length; i++)
            {
                tReconFHSS[i].bBandWidth = (byte)variableWork.DistribFHSS[i].iDFreq;
                tReconFHSS[i].bLevelLinked = 0; // Еще спросить у Лины 
                tReconFHSS[i].bLevelOwn = 0; // Еще спросить у Лины  
                tReconFHSS[i].bModulation = 0; // Сросить у Иры
                tReconFHSS[i].bSignAudio = 0;
                tReconFHSS[i].iFreqMin = variableWork.DistribFHSS[i].iFreqMin;
                tReconFHSS[i].iFreqMax = variableWork.DistribFHSS[i].iFreqMax;
                tReconFHSS[i].iID = variableWork.DistribFHSS[i].iID;

                tReconFHSS[i].sBearingLinked = (short)variableWork.DistribFHSS[i].sLocation[0].iQ1;
                tReconFHSS[i].sBearingOwn = (short)variableWork.DistribFHSS[i].sLocation[0].iQ2;
                ConvertCoord convertCoord = new ConvertCoord();

                CoordDegMinSec coordDegMinSec = new CoordDegMinSec();
                coordDegMinSec = convertCoord.DegToDegMinSec(Math.Abs(variableWork.DistribFHSS[i].sLocation[0].dLatitude), Math.Abs(variableWork.DistribFHSS[i].sLocation[0].dLongitude));
                tReconFHSS[i].tCoord.bDegreeLatitude = (byte)coordDegMinSec.Lat[0].LatDeg;
                tReconFHSS[i].tCoord.bDegreeLongitude = (byte)coordDegMinSec.Lon[0].LonDeg;
                tReconFHSS[i].tCoord.bMinuteLatitude = (byte)coordDegMinSec.Lat[0].LatMin;
                tReconFHSS[i].tCoord.bMinuteLongitude = (byte)coordDegMinSec.Lon[0].LonMin;
                tReconFHSS[i].tCoord.bSecondLatitude = (byte)coordDegMinSec.Lat[0].LatSec;
                tReconFHSS[i].tCoord.bSecondLongitude = (byte)coordDegMinSec.Lon[0].LonSec;
                tReconFHSS[i].tCoord.bSignLongitude = 0;
                tReconFHSS[i].tCoord.bSignLatitude = 0;
                if (variableWork.DistribFHSS[i].sLocation[0].dLongitude < 0)
                    tReconFHSS[i].tCoord.bSignLongitude = 1;
                if (variableWork.DistribFHSS[i].sLocation[0].dLatitude < 0)
                    tReconFHSS[i].tCoord.bSignLatitude = 1;
                DateTime dTime = DateTime.Now;
                tReconFHSS[i].bMinute = (byte)dTime.Minute;
                tReconFHSS[i].bSecond = (byte)dTime.Second;
                tReconFHSS[i].sStep = (short)variableWork.DistribFHSS[i].iStep;
                tReconFHSS[i].iDuration = variableWork.DistribFHSS[i].iDuratImp;
            }
            VariablePU varPU = new VariableStatic.VariablePU();
            if (variableConnection.PostControlType == 2)
            {
                if (tReconFHSS.Length > varPU.NumPacket)
                {
                    do
                    {
                        StructTypeGR_BRZ.TReconFHSS[] Buff = new StructTypeGR_BRZ.TReconFHSS[varPU.NumPacket];
                        Array.Copy(tReconFHSS, Buff, varPU.NumPacket);
                        Array.Reverse(tReconFHSS);
                        Array.Resize(ref tReconFHSS, tReconFHSS.Length - varPU.NumPacket);
                        Array.Reverse(tReconFHSS);
                        MainWnd.ClientPu.SendReconFHSS(0, Buff);
                        await Task.Run(async () =>
                        {
                            await Task.Delay(varPU.TimePeriod);
                        });
                    }
                    while (tReconFHSS.Length > varPU.NumPacket);
                }
                if (tReconFHSS.Length > 0)
                    MainWnd.ClientPu.SendReconFHSS(0, tReconFHSS);
            }
            else
            {
                MainWnd.ClientPu.SendReconFHSS(0, tReconFHSS);
            }
            if (Convert.ToBoolean(variableSuppression.AutoClearTable) == true)
            {
                Invoke((MethodInvoker)(() =>
                {
                    List<TDistribFHSS> lSupprFHSS = variableWork.DistribFHSS.ToList();
                    lSupprFHSS.Clear();
                    variableWork.DistribFHSS = lSupprFHSS.ToArray();
                }));
            }
           // MainWnd.ClientPu.SendReconFHSS(0, tReconFHSS);
        }

        //Запрос ИРИ ФРЧ
        async void ClientPu_OnRequestReconFWS(object sender)
        {            //Забрать данные из таблици ИРи ФРЧ на ЦР        // Передать их ПУ, отчистить табл ИРИ ФРЧ на ЦР
           StructTypeGR_BRZ.TReconFWS[] tReconFWSAuto = new StructTypeGR_BRZ.TReconFWS[variableWork.DistribFWS.Length];
            for (int i = 0; i < variableWork.DistribFWS.Length; i++)
            {
                tReconFWSAuto[i].bBandWidth = (byte)variableWork.DistribFWS[i].iDFreq;
                tReconFWSAuto[i].bLevelLinked = (byte)(variableWork.DistribFWS[i].sLevel*-1);
                tReconFWSAuto[i].bLevelOwn = (byte)(variableWork.DistribFWS[i].sLevel * -1);
                tReconFWSAuto[i].bModulation = variableWork.DistribFWS[i].bView; // ViewModulation(variableWork.DistribFWS[i].sView);
                tReconFWSAuto[i].bSignAudio = 0;
                tReconFWSAuto[i].iFreq = variableWork.DistribFWS[i].iFreq;
                tReconFWSAuto[i].iID = variableWork.DistribFWS[i].iID;
                tReconFWSAuto[i].sBearingLinked = variableWork.DistribFWS[i].sBearing2;
                tReconFWSAuto[i].sBearingOwn = variableWork.DistribFWS[i].sBearing1;
                ConvertCoord convertCoord = new ConvertCoord(); 
                
                CoordDegMinSec coordDegMinSec = new CoordDegMinSec();

                coordDegMinSec = convertCoord.DegToDegMinSec(variableWork.DistribFWS[i].dLatitude, variableWork.DistribFWS[i].dLongitude);
                tReconFWSAuto[i].tCoord.bDegreeLatitude = (byte)coordDegMinSec.Lat[0].LatDeg;
                tReconFWSAuto[i].tCoord.bDegreeLongitude = (byte)coordDegMinSec.Lon[0].LonDeg;
                tReconFWSAuto[i].tCoord.bMinuteLatitude = (byte)coordDegMinSec.Lat[0].LatMin;
                tReconFWSAuto[i].tCoord.bMinuteLongitude = (byte)coordDegMinSec.Lon[0].LonMin;
                tReconFWSAuto[i].tCoord.bSecondLatitude = (byte)coordDegMinSec.Lat[0].LatSec;
                tReconFWSAuto[i].tCoord.bSecondLongitude = (byte)coordDegMinSec.Lon[0].LonSec;
                tReconFWSAuto[i].tCoord.bSignLongitude = 0;
                tReconFWSAuto[i].tCoord.bSignLatitude = 0;
                if (variableWork.DistribFWS[i].dLongitude < 0)
                    tReconFWSAuto[i].tCoord.bSignLongitude = 1;
                if (variableWork.DistribFWS[i].dLatitude < 0)
                    tReconFWSAuto[i].tCoord.bSignLatitude = 1;
                DateTime dTime = DateTime.Now;
                tReconFWSAuto[i].tTime.bHour = (byte)dTime.Hour;
                tReconFWSAuto[i].tTime.bMinute = (byte)dTime.Minute;
                tReconFWSAuto[i].tTime.bSecond = (byte)dTime.Second;
            }
            VariablePU varPU = new VariableStatic.VariablePU();
            if (variableConnection.PostControlType == 2)
            {
                if (tReconFWSAuto.Length > varPU.NumPacket)
                {
                    do
                    {
                        StructTypeGR_BRZ.TReconFWS[] Buff = new StructTypeGR_BRZ.TReconFWS[varPU.NumPacket];
                        Array.Copy(tReconFWSAuto, Buff, varPU.NumPacket);
                        Array.Reverse(tReconFWSAuto);
                        Array.Resize(ref tReconFWSAuto, tReconFWSAuto.Length - varPU.NumPacket);
                        Array.Reverse(tReconFWSAuto);
                        MainWnd.ClientPu.SendReconFWS(0, Buff);
                        await Task.Run(async () =>
                        {
                            await Task.Delay(varPU.TimePeriod);
                        });
                    }
                    while (tReconFWSAuto.Length > varPU.NumPacket);
                }
                if (tReconFWSAuto.Length > 0)
                    MainWnd.ClientPu.SendReconFWS(0, tReconFWSAuto);
            }
            else
            {
                MainWnd.ClientPu.SendReconFWS(0, tReconFWSAuto);
            }
            if (Convert.ToBoolean(variableSuppression.AutoClearTable) == true)
            {
                Invoke((MethodInvoker)(() =>
                {
                    List<TDistribFWS> lSupprFWS = variableWork.DistribFWS.ToList();
                    lSupprFWS.Clear();
                    variableWork.DistribFWS = lSupprFWS.ToArray();
                }));
            }

        }
       

        /// Вид модуляции (декодирование из кодов в строку)
        /*
        public byte ViewModulation(string sModulation)
        {
            byte bModulation = 255;

            switch (sModulation)
            {
                case "Шум":
                    bModulation = +; //"ZeroImpulse";
                    break;

                case "Несущая":
                    bModulation = 1; //"Am2";
                    break;

                case  "АМн":
                    bModulation = 2; //"Am101";
                    break;

                case "ФМн":
                    bModulation = 3; //"Fm2";
                    break;

                case "ЧМн2":
                    bModulation = 4; //"Fsk2"; 
                    break;

                case "АМ ЧМ":
                    bModulation = 5; //"AmFm";
                    break;

                case "ЧМн4":
                    bModulation = 6; //"Fsk4"; 
                    break;

                case "ЧМн8":
                    bModulation = 7; //"Fsk8"; 
                    break;

                case "ЧМ":
                    bModulation = 8; //"Fm"; 
                    break;

                case "ЧМн":
                    bModulation = 9; //"Fsk"; 
                    break;

                case "ШПС":
                    bModulation = 10; //"ShPS"; 
                    break;

                default:
                    bModulation = 255; //"Unknown";
                    break;

            }

            return bModulation;
        }
        */
        
        //Текстовое сообщение получено
        private void Receive_TextCmd(object sender, string strText)
        {
            try
            {
                string strName = "";
                strName = "  ";// CMD_Text; 
                if (objChangeMessage.rtbHistoryPC.InvokeRequired)
                {
                    objChangeMessage.rtbHistoryPC.Invoke((MethodInvoker)(delegate()
                    {
                        objChangeMessage.rtbHistoryPC.SelectionAlignment = HorizontalAlignment.Right;
                        objChangeMessage.showLogJammer.ShowMessage(objChangeMessage.rtbHistoryPC, 0, System.DateTime.Now.ToString("hh:mm:ss"), strName, " " + strText);
                        objChangeMessage.showLogJammer.ShowMessage(objChangeMessage.rtbHistoryPC, 0, "", "", ""); //"\n");
                    }));

                }
                else
                {
                    objChangeMessage.rtbHistoryPC.SelectionAlignment = HorizontalAlignment.Right;
                    objChangeMessage.showLogJammer.ShowMessage(objChangeMessage.rtbHistoryPC, 0, System.DateTime.Now.ToString("hh:mm:ss"), strName, " " + strText);
                    objChangeMessage.showLogJammer.ShowMessage(objChangeMessage.rtbHistoryPC, 0, "", "", ""); //"\n");
                }

            }
            catch (SystemException)
            { }

            if (MainWnd.ClientPu != null)
                MainWnd.ClientPu.SendConfirmText(0);

        }

        //Отправка текстового сообщения на ПУ
        private void Send_TextCmd(object sender, string strText)
        {
            try
            {
                if (objChangeMessage.rtbHistoryPC.InvokeRequired)
                {
                    objChangeMessage.rtbHistoryPC.Invoke((MethodInvoker)(delegate()
                    {
                        objChangeMessage.rtbHistoryPC.SelectionAlignment = HorizontalAlignment.Left;
                        objChangeMessage.showLogJammer.ShowMessage(objChangeMessage.rtbHistoryPC, 1, System.DateTime.Now.ToString("hh:mm:ss"), "", " " + strText);
                        objChangeMessage.showLogJammer.ShowMessage(objChangeMessage.rtbHistoryPC, 1, "", "", ""); //"\n");
                    }));

                }
                else
                {
                    objChangeMessage.rtbHistoryPC.SelectionAlignment = HorizontalAlignment.Left;
                    objChangeMessage.showLogJammer.ShowMessage(objChangeMessage.rtbHistoryPC, 1, System.DateTime.Now.ToString("hh:mm:ss"), "", " " + strText);
                    objChangeMessage.showLogJammer.ShowMessage(objChangeMessage.rtbHistoryPC, 1, "", "", ""); //"\n");
                }
            }
            catch (SystemException)
            { }
        }

        //Подтверждение получения текстового сообщения от ПУ 
        private void Receive_ConfirmTextCmd(object sender, byte bCodeError)
        {
            /*try
            {
                string strName = CMD_ConfirmText + "\n"; //+" ( Адр " + bAddressSend.ToString() + ")\n";"";                   
                string strDetail = "Код ошибки  " + bCodeError.ToString();

                if (rtbHistoryPC.InvokeRequired)
                {
                    rtbHistoryPC.Invoke((MethodInvoker)(delegate()
                    {
                        rtbHistoryPC.SelectionAlignment = HorizontalAlignment.Right;
                        showLogJammer.ShowMessage(rtbHistoryPC, 0, System.DateTime.Now.ToString("hh:mm:ss"), strName, strDetail);
                        showLogJammer.ShowMessage(rtbHistoryPC, 0, "", "", "\n");
                    }));

                }
                else
                {
                    rtbHistoryPC.SelectionAlignment = HorizontalAlignment.Right;
                    showLogJammer.ShowMessage(rtbHistoryPC, 0, System.DateTime.Now.ToString("hh:mm:ss"), strName, strDetail);
                    showLogJammer.ShowMessage(rtbHistoryPC, 0, "", "", "\n");
                }
            }
            catch (SystemException)
            { }*/
        }

        //Синхронизация времени
        private void Receive_RequestSynchTime(object sender, StructTypeGR_BRZ.TTimeMy tTime)
        {
            try
            {
                SYSTEMTIME time = new SYSTEMTIME();
                DateTime DateUtc = DateTime.UtcNow;
                short utcHour = (short)(DateTime.Now.Hour - DateTime.UtcNow.Hour);
                short utcMin = (short)(DateTime.Now.Minute - DateTime.UtcNow.Minute);
                //получаем текущее время
                GetSystemTime(ref time);

                time.wHour = (short)(tTime.bHour - utcHour);
                time.wMinute = (short)(tTime.bMinute - utcMin);
                if (time.wMinute < 0)
                {
                    time.wMinute = (short)(60 + time.wMinute);
                    time.wHour--;
                }
                if (time.wMinute >= 60)
                {
                    time.wMinute = (short)(time.wMinute - 60);
                    time.wHour++;
                }
                if (time.wHour < 0) { time.wHour = (short)(24 + time.wHour); }
                if (time.wHour > 23) { time.wHour = (short)(time.wHour - 24); }
                time.wSecond = tTime.bSecond;

                //DateAndTime.TimeOfDay = DateTime.Now.AddSeconds(3000).AddMilliseconds(80);
                //устанавливаем новые значения
                if (!SetSystemTime(ref time))
                {
                    //получаем текущее время
                    GetSystemTime(ref time);
                    tTime.bHour = (byte)(time.wHour + utcHour);
                    tTime.bMinute = (byte)(time.wMinute + utcMin);
                    tTime.bSecond = (byte)utcHour;// time.wSecond;
                    
                    if (ClientPu != null)
                        ClientPu.SendSynchTime(1, tTime);
                }
                else
                {
                    if (ClientPu != null)
                        ClientPu.SendSynchTime(0, tTime); 
                }

            }
            catch (SystemException)
            { }                
            }    
        byte RegimeToPU(byte bRegime)
        {
            switch (bRegime)
            {
                case 0:                   
                    break;
                case 1: case 2:
                    bRegime = 1;
                    break;
                case 3: case 4: case 5: case 6:
                    bRegime = 2;
                    break;
            }
            return bRegime;
        }
        byte RegimeToARM(byte bRegime)
        {
            switch (bRegime)
            {
                case 0:
                    break;
                case 1:
                    bRegime = 2;
                    break;
                case 2:
                    bRegime = (byte)(3 + variableSuppression.RegimeRadioSuppr);
                    break;
            }
            return bRegime;
        }

        /////Изменение режима
        private async void Receive_RequestRegime(object sender, byte bRegime)
        {
            byte ErrorCode = 0;
            int Regim = RegimeToARM(bRegime);
            switch (Regim)
            {
                case 2:

                    //Установка режима работы с эфира
                    var answer002 = await VariableWork.aWPtoBearingDSPprotocolNew.SetReceiversChannel(0);

                    //установка курсового угла
                    var answer02 = await VariableWork.aWPtoBearingDSPprotocolNew.SetDirectionCorrection(variableIntellegence.RelativeBearing, true);

                    //установка диапазонов и секторов радиоразведки
                    var answer = await VariableWork.aWPtoBearingDSPprotocolNew.SetSectorsAndRanges(0, 0, variableWork.RangeSectorReconOwn); //Шифр 3

                    //установка порога для панорамы
                    panoramaControl1.SetSectorsAndRanges(variableWork.RangeSectorReconOwn);

                    //установка фильтов для обнаружения ИРИ
                    answer = await VariableWork.aWPtoBearingDSPprotocolNew.SetFilters(panoramaControl1.GetThreshold(), 0, 0, 0); //Шифр 18
                    break;
                case 3:

                    //Установка режима работы с эфира
                    var answer003 = await VariableWork.aWPtoBearingDSPprotocolNew.SetReceiversChannel(0);

                    //установка курсового угла
                    var answer03 = await VariableWork.aWPtoBearingDSPprotocolNew.SetDirectionCorrection(variableIntellegence.RelativeBearing, true);


                    //установка диапазонов и секторов радиоподавления
                    var answerSSaR = await VariableWork.aWPtoBearingDSPprotocolNew.SetSectorsAndRanges(1, 0, variableWork.RangeSectorSupprOwn); //Шифр 3
                    
                    //установка синих квадратиков для панорамы
                    panoramaControl1.SetSectorsAndRanges(variableWork.RangeSectorSupprOwn);

                    //Установка параметров радиоподавлениЯ для ФРЧ, Шифр 32
                    var answerSettingsFrs = await VariableWork.aWPtoBearingDSPprotocolNew.SetFrsRadioJamSettings(variableSuppression.TimeRadiatFWS, variableSuppression.Circle * 1000, variableSuppression.ChannelUse);

                    Protocols.FRSJammingSetting[] fRSJammingSettingsFrs = new Protocols.FRSJammingSetting[variableWork.SupprFWS_Own.Length];
                    for (int i = 0; i < variableWork.SupprFWS_Own.Length; i++)
                    {
                        fRSJammingSettingsFrs[i].DeviationCode = variableWork.SupprFWS_Own[i].bDeviation;
                        fRSJammingSettingsFrs[i].DurationCode = variableWork.SupprFWS_Own[i].bDuration;
                        fRSJammingSettingsFrs[i].Frequency = variableWork.SupprFWS_Own[i].iFreq; // ?
                        fRSJammingSettingsFrs[i].Id = variableWork.SupprFWS_Own[i].iID;
                        fRSJammingSettingsFrs[i].Liter = variableWork.SupprFWS_Own[i].bLetter;
                        fRSJammingSettingsFrs[i].ManipulationCode = variableWork.SupprFWS_Own[i].bManipulation;
                        fRSJammingSettingsFrs[i].ModulationCode = variableWork.SupprFWS_Own[i].bModulation;
                        fRSJammingSettingsFrs[i].Priority = variableWork.SupprFWS_Own[i].bPrior;
                        fRSJammingSettingsFrs[i].Threshold = Convert.ToByte((-1) * variableWork.SupprFWS_Own[i].sLevel);

                    }

                    //Установка ИРИ ФРЧ для РП, Шифр 6
                    //1.2.11
                    var answerFreqFrs = await VariableWork.aWPtoBearingDSPprotocolNew.SetFRSJamming((byte)0,fRSJammingSettingsFrs);

                    break;
            }
            var answerMode = await VariableDynamic.VariableWork.aWPtoBearingDSPprotocolNew.SetMode(Regim);
            try
            {
                if (answerMode.Header.ErrorCode != 0)
                    ErrorCode = 1;
                else
                    Invoke((MethodInvoker)(() => variableWork.Regime = Convert.ToByte(Regim)));
            }
            catch
            {
                ErrorCode = 1;
            }

            if (MainWnd.ClientPu != null)
            {
                MainWnd.ClientPu.SendRegime(ErrorCode);
            }
        }
        //Специальные частоты
        private void Receive_RequestRangeSpec(object sender, byte bType, StructTypeGR_BRZ.TRangeSpec[] tRangeSpec)
        {
            byte ErrorCode = 0;
            try
            {
               /* if (variableCommon.Role == 2) //Ведомая
                {
                    switch (bType)
                    {
                        case 0: // Запрещенные частоты
                            List<Protocols.FrequencyRange> tempForbLink = new List<Protocols.FrequencyRange>();
                            Protocols.FrequencyRange tempFrL = new Protocols.FrequencyRange();
                            for (int i = 0; i < tRangeSpec.Length; i++)
                            {
                                if (tRangeSpec[i].iFregMin < FreqMin_RP_BRZ || tRangeSpec[i].iFregMax > FreqMax_RP_BRZ) // Проверка попадания в допустимый диапазон РП
                                {
                                    ErrorCode = 1;
                                }
                                else
                                {
                                    tempFrL.StartFrequency = tRangeSpec[i].iFregMin;
                                    tempFrL.EndFrequency = tRangeSpec[i].iFregMax;
                                    tempForbLink.Add(tempFrL);
                                }
                            }
                            Invoke((MethodInvoker)(() => variableWork.UpdateAllSpecForbiddenLinked(tempForbLink.ToArray())));
                            break;

                        case 1: //Известные частоты
                            List<Protocols.FrequencyRange> tempKnowLink = new List<Protocols.FrequencyRange>();
                            Protocols.FrequencyRange tempKnL = new Protocols.FrequencyRange();
                            for (int i = 0; i < tRangeSpec.Length; i++)
                            {
                                if (tRangeSpec[i].iFregMin < FreqMin_RP_BRZ || tRangeSpec[i].iFregMax > FreqMax_RP_BRZ) // Проверка попадания в допустимый диапазон РП
                                {
                                    ErrorCode = 1;
                                }
                                else
                                {
                                    tempKnL.StartFrequency = tRangeSpec[i].iFregMin;
                                    tempKnL.EndFrequency = tRangeSpec[i].iFregMax;
                                    tempKnowLink.Add(tempKnL);
                                }
                            }
                            Invoke((MethodInvoker)(() => variableWork.UpdateAllSpecKnownLinked(tempKnowLink.ToArray())));
                            break;

                        case 2: //Важные частоты 
                            List<Protocols.FrequencyRange> tempImpLink = new List<Protocols.FrequencyRange>();
                            Protocols.FrequencyRange tempImL = new Protocols.FrequencyRange();
                            for (int i = 0; i < tRangeSpec.Length; i++)
                            {
                                if (tRangeSpec[i].iFregMin < FreqMin_RP_BRZ || tRangeSpec[i].iFregMax > FreqMax_RP_BRZ) // Проверка попадания в допустимый диапазон РП
                                {
                                    ErrorCode = 1;
                                }
                                else
                                {
                                    tempImL.StartFrequency = tRangeSpec[i].iFregMin;
                                    tempImL.EndFrequency = tRangeSpec[i].iFregMax;
                                    tempImpLink.Add(tempImL);
                                }
                            }
                            Invoke((MethodInvoker)(() => variableWork.UpdateAllSpecImportantLinked(tempImpLink.ToArray())));
                            break;
                    }
                }
                else //Ведущая и Автономная 
                    */ 
                {
                    switch (bType)
                    {
                        case 0: // Запрещенные частоты
                            List<Protocols.FrequencyRange> tempForbOwn = new List<Protocols.FrequencyRange>();
                        Protocols.FrequencyRange tempFrO = new Protocols.FrequencyRange();
                            for (int i = 0; i < tRangeSpec.Length; i++)
                            {
                                if (tRangeSpec[i].iFregMin < FreqMin_RP_BRZ || tRangeSpec[i].iFregMax > FreqMax_RP_BRZ) // Проверка попадания в допустимый диапазон РП
                                {
                                    ErrorCode = 1;
                                }
                                else
                                {
                                    tempFrO.StartFrequency = tRangeSpec[i].iFregMin;
                                    tempFrO.EndFrequency = tRangeSpec[i].iFregMax;
                                    tempForbOwn.Add(tempFrO);
                                }
                            }
                            Invoke((MethodInvoker)(() => variableWork.UpdateAllSpecForbiddenOwn(tempForbOwn.ToArray())));
                            break;
                    
                        case 1: //Известные частоты
                        
                            List<Protocols.FrequencyRange> tempKnowOwn = new List<Protocols.FrequencyRange>();
                        Protocols.FrequencyRange tempKnO = new Protocols.FrequencyRange();
                            for (int i = 0; i < tRangeSpec.Length; i++)
                            {
                                if (tRangeSpec[i].iFregMin < FreqMin_RP_BRZ || tRangeSpec[i].iFregMax > FreqMax_RP_BRZ) // Проверка попадания в допустимый диапазон РП
                                {
                                    ErrorCode = 1;
                                }
                                else
                                {
                                    tempKnO.StartFrequency = tRangeSpec[i].iFregMin;
                                    tempKnO.EndFrequency = tRangeSpec[i].iFregMax;
                                    tempKnowOwn.Add(tempKnO);
                                }
                            }
                            Invoke((MethodInvoker)(() => variableWork.UpdateAllSpecKnownOwn(tempKnowOwn.ToArray())));
                            break;

                        case 2: //Важные частоты 
                            List<Protocols.FrequencyRange> tempImpOwn = new List<Protocols.FrequencyRange>();
                            Protocols.FrequencyRange tempImO = new Protocols.FrequencyRange();
                            for (int i = 0; i < tRangeSpec.Length; i++)
                            {
                                if (tRangeSpec[i].iFregMin < FreqMin_RP_BRZ || tRangeSpec[i].iFregMax > FreqMax_RP_BRZ) // Проверка попадания в допустимый диапазон РП
                                {
                                    ErrorCode = 1;
                                }
                                else
                                {
                                    tempImO.StartFrequency = tRangeSpec[i].iFregMin;
                                    tempImO.EndFrequency = tRangeSpec[i].iFregMax;
                                    tempImpOwn.Add(tempImO);
                                }
                            }
                            Invoke((MethodInvoker)(() => variableWork.UpdateAllSpecImportantOwn(tempImpOwn.ToArray())));
                            break;
                    }
                }

            }
            catch(Exception e)
            {
                MessageBox.Show(e.Message);
                ErrorCode = 2; }

            if (MainWnd.ClientPu != null)
                MainWnd.ClientPu.SendRangeSpec(ErrorCode, bType);
            
        }
        //Назначение ФРЧ на РП
        private void Receive_RequestSupprFWS(object sender, int iDuration, StructTypeGR_BRZ.TSupprFWS[] tSupprFWS)
        {
            byte ErrorCode = 0;
            try
            {
                Invoke((MethodInvoker)(() => (new VariableSuppression()).TimeRadiatFWS = (short)iDuration));
                List<TSupprFWS> tempMass = new List<TSupprFWS>();
                //TSupprFWS temp = new TSupprFWS();
                    for (int i = 0; i < tSupprFWS.Length; i++)
                    {
                        if (tSupprFWS[i].iFreq < FreqMin_RP_BRZ || tSupprFWS[i].iFreq > FreqMax_RP_BRZ)
                        {
                            ErrorCode = 1;
                        }
                        else
                        {
                            USR_DLL.TSupprFWS temp = USR_DLL.TSupprFWS.CreateID();
                            temp.bDeviation = tSupprFWS[i].bDeviation;
                            temp.iFreq = tSupprFWS[i].iFreq;
                            temp.bModulation = tSupprFWS[i].bModulation;
                            temp.bManipulation = tSupprFWS[i].bManipulation;
                            temp.bPrior = tSupprFWS[i].bPrioritet;
                            temp.bDuration = 1;
                            //temp.iID = tSupprFWS[i].iID;
                            temp.sBearing = tSupprFWS[i].sBearing;
                            temp.bLetter = LetterFind(tSupprFWS[i].iFreq);
                            temp.sLevel = (short)(tSupprFWS[i].bThreshold * -1);
                            tempMass.Add(temp);
                        }
                    }

                   /* if (variableCommon.Role == 2) //Ведомая
                    {
                        Invoke((MethodInvoker)(() => variableWork.SupprFWS_Linked = tempMass.ToArray()));
                    }
                    else//Ведущая и автономная
                    {*/
                        Invoke((MethodInvoker)(() => variableWork.SupprFWS_Own = tempMass.ToArray()));
                    //}
            }
            catch (SystemException)
            { ErrorCode = 2; }

            if (MainWnd.ClientPu != null)
                MainWnd.ClientPu.SendSupprFWS(ErrorCode);

            
        }
        //Запрос состояния
        private void Receive_RequestState(object sender)
        {
            try
            {
                byte bCodeError = 0;

                byte bRegime = RegimeToPU( variableWork.Regime);

                short sNum = (new VariableCommon()).OwnNumber;

                byte bType = variableCommon.TypeStation;
                
                byte bRole = (new VariableCommon()).Role;                

                byte[] bLetter = (new VariableSuppression()).OwnLetters;

                if (MainWnd.ClientPu != null)
                    MainWnd.ClientPu.SendState(bCodeError, bRegime, sNum, bType, bRole, bLetter);
            }
            catch
            {

            }
        }
        //Сектора и диапазоны
        private void Receive_RequestRangeSector(object sender, byte bType, StructTypeGR_BRZ.TRangeSector[] tRangeSector)
        {
            byte ErrorCode = 0;
            try
            {
                switch (bType)
                {
                    case 0:  // Радиоразведка                        
                        List<Protocols.RangeSector> RtempMass = new List<Protocols.RangeSector>();
                        Protocols.RangeSector Rtemp = new Protocols.RangeSector();
                        for (int i = 0; i < tRangeSector.Length; i++)
                        {
                            if (tRangeSector[i].iFregMin < FreqMin_RR_BRZ || tRangeSector[i].iFregMax > FreqMax_RR_BRZ)
                            {
                                ErrorCode = 1;
                            }
                            else
                            {
                                Rtemp.StartFrequency = tRangeSector[i].iFregMin;
                                Rtemp.EndFrequency = tRangeSector[i].iFregMax;
                                Rtemp.StartDirection = (short)(tRangeSector[i].sAngleMin * 10);
                                Rtemp.EndDirection = (short)(tRangeSector[i].sAngleMax * 10);
                                RtempMass.Add(Rtemp);
                            }
                        }
                            Invoke((MethodInvoker)(() => variableWork.UpdateAllRangeSectorReconOwn(RtempMass.ToArray())));
                            //Invoke((MethodInvoker)(() => variableWork.RangeSectorReconOwn = RtempMass.ToArray()));

                            break;

                    case 1: // Радиоподавление
                        List<Protocols.RangeSector> tempMass = new List<Protocols.RangeSector>();
                        Protocols.RangeSector temp = new Protocols.RangeSector();
                        for (int i = 0; i < tRangeSector.Length; i++)
                        {
                            if (tRangeSector[i].iFregMin < FreqMin_RP_BRZ || tRangeSector[i].iFregMax > FreqMax_RP_BRZ)
                            {
                                ErrorCode = 1;
                            }
                            else
                            {
                                temp.StartFrequency = tRangeSector[i].iFregMin;
                                temp.EndFrequency = tRangeSector[i].iFregMax;
                                temp.StartDirection = (short)(tRangeSector[i].sAngleMin * 10);
                                temp.EndDirection = (short)(tRangeSector[i].sAngleMax * 10);
                                tempMass.Add(temp);
                            }
                        }
                        Invoke((MethodInvoker)(() => variableWork.UpdateAllRangeSectorSupprOwn(tempMass.ToArray())));;
                            //Invoke((MethodInvoker)(() => variableWork.RangeSectorSupprOwn = tempMass.ToArray()));

                            break;
                }
            }
            catch (SystemException)
            { ErrorCode = 2; }

            if (MainWnd.ClientPu != null)
                MainWnd.ClientPu.SendRangeSector(ErrorCode, bType);

        }
        //Запрос на исполнительное пеленгование
        async void ClientPu_OnRequestExecBear(object sender, int iID, int iFreq)
        {
            byte ErrorCode = 0;
            double iFmin = 0;
            double iFmax = 0;
            Protocols.ExecutiveDFResponse answer;
            try
            {
                iFmin = (iFreq - 250)/10000;
                iFmax = (iFreq + 250)/10000;
                answer = await VariableDynamic.VariableWork.aWPtoBearingDSPprotocolNew.ExecutiveDF(iFmin, iFmax, 3, 3);
                MainWnd.ClientPu.SendExecBear(ErrorCode, iID, answer.Frequency, (short)(answer.Direction == -1 ? (361) : Math.Round(answer.Direction/10.0d)));
            }
            catch
            {
                ErrorCode = 1;
                MainWnd.ClientPu.SendExecBear(ErrorCode, 0, 0, 0);
            }
        }

        //Запрос на квазиодновременное пеленгование
        async void ClientPu_OnRequestSimulBear(object sender, int iID, int iFreq)
        {
            byte ErrorCode = 0;
            int iFmin = 0;
            int iFmax = 0;
            Protocols.QuasiSimultaneouslyDFResponse answer;
            try
            {
                iFmin = iFreq - 250;
                iFmax = iFreq + 250;
                answer = await VariableWork.aWPtoBearingDSPprotocolNew.QuasiSimultaneouslyDFX10(iFmin, iFmax, 3, 3);
                MainWnd.ClientPu.SendSimulBear(ErrorCode, iID, answer.Source.Frequency, (short)(answer.Source.Direction == -1 ? (361) : Math.Round(answer.Source.Direction/ 10.0d)),
                    (short)(answer.Source.Direction2 == -1 ? (361) : Math.Round(answer.Source.Direction2 / 10.0d))); 
            }
            catch
            {
                ErrorCode = 1;
                MainWnd.ClientPu.SendSimulBear(ErrorCode, 0, 0, 0, 0);
            }
            
        }

        #endregion

        #region R934
        /////////////////Обработка связи с Пунктом Управления БЕРЕЗА\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

        public const byte REQUEST = 1;
        public const byte SYNCHRONIZE = 2;
        public const byte TEXT_MESSAGE = 3;
        public const byte REGIME_WORK = 4;
        public const byte RANGE_FORBID = 5;
        public const byte RANGE_RECON = 6;
        public const byte RANGE_SUPPRESS = 7;
        public const byte SUPPRESS_FWS = 8;
        public const byte SUPPRESS_FHSS = 9;
        public const byte REQUEST_BEARING = 10;
        public const byte RECEPTION = 11;
        public const byte COORDINATES = 12;
        public const byte DATA_FWS_PART_ONE = 13;
        public const byte DATA_FWS_PART_TWO = 18;
        public const byte DATA_FHSS_PART_ONE = 14;
        public const byte DATA_FHSS_PART_TWO = 19;
        public const byte CONTROL_SIGNAL = 15;
        public const byte EXECUTE_BEARING = 16;
        public const byte SIMULTAN_BEARING = 17;
        public const byte TEST = 20;

        public const byte NumLetters = 7;
        byte bSighF_FRCH = 0;
        byte bSighF_PPRCH = 1;
        byte bCode_PPRCH_RP = 0x06;
        static public ExInPC ClientPu_Bel;
        byte CountStateSupressFreq = 10;

        private void ReadByte(byte[] bData)
        {
            stateIndicateReadPU.SetIndicateOn();
        }

        private void WriteByte(byte[] bData)
        {
            stateIndicateWritePU.SetIndicateOn();
        }
        private void Connect()
        {
            stateIndicateConnection.SetConnectOn();

        }
        private void Disconnect()
        {
            stateIndicateConnection.SetConnectOff();
            ClientPu_Bel = null;

        }
        void InitChanMess()
        {

            objChangeMessage = new ChangeMessage();
            if (objChangeMessage.InvokeRequired)
            {
                objChangeMessage.Invoke((MethodInvoker)(delegate()
                {
                    objChangeMessage.Visible = false;
                    objChangeMessage.Show();
                    objChangeMessage.Close();
                }));

            }
            else
            {
                objChangeMessage.Visible = false;
                objChangeMessage.Show();
                objChangeMessage.Close();
            }
        }
        private void InitIExGRZ_Bel()
        {
            messPU.sText = new List<string>();
            messPU.sTime = new List<string>();
            ClientPu_Bel = new ExInPC(0, (byte)variableCommon.OwnAddressPC);
            MainWnd.stateIndicateConnection = new FuncVLC.StateConnection(btPU, clConnect, clDisconnect);
            var postControlStract = variableConnection.PostControlStruct;

            ClientPu_Bel = new ExInPC(0, Convert.ToByte(variableCommon.OwnAddressPC));
            stateIndicateReadPU = new StateIndicate(pbReadPU, imgReadByte, imgWaitByte, TIME_INDICATE);
            stateIndicateWritePU = new StateIndicate(pbWritePU, imgWriteByte, imgWaitByte, TIME_INDICATE);

            MainWnd.ClientPu_Bel.OnConnectPort += new ExInPC.ConnectEventHandler(Connect);
            MainWnd.ClientPu_Bel.OnDisconnectPort += new ExInPC.ConnectEventHandler(Disconnect);
            MainWnd.ClientPu_Bel.OnReadByte += new ExInPC.ByteEventHandler(ReadByte);
            MainWnd.ClientPu_Bel.OnWriteByte += new ExInPC.ByteEventHandler(WriteByte);

            MainWnd.ClientPu_Bel.OnReceive_SYNCHRONIZE += new ExInPC.CmdEventHandler(Receive_RequestSynchTime_Bel); //Синхронизaаия времени
            MainWnd.ClientPu_Bel.OnSend_SYNCHRONIZE += new ExInPC.CmdEventHandler(Send_SynchTime);

            MainWnd.ClientPu_Bel.OnReceive_REGIME_WORK += new ExInPC.CmdEventHandler(Receive_RequestRegime); // Назначение режима работы
            MainWnd.ClientPu_Bel.OnReceive_RANGE_FORBID += new ExInPC.CmdEventHandler(Receive_RequestRangeSpec); // Запрещенные частоты (диапазоны) от ПУ АКПП/ на ПУ АКПП
            MainWnd.ClientPu_Bel.OnReceive_RANGE_RECON += new ExInPC.CmdEventHandler(Receive_RequestReconRangeSector); // Сектора и диапазоны РР от ПУ АКПП
            MainWnd.ClientPu_Bel.OnReceive_RANGE_SUPPRESS += new ExInPC.CmdEventHandler(Receive_RequestSupressRangeSector);  // Сектора и диапазоны РП от ПУ АКПП
            MainWnd.ClientPu_Bel.OnReceive_SUPPRESS_FWS += new ExInPC.CmdEventHandler(Receive_RequestSupprFWS); // ФРЧ на подавление от ПУ АКПП
            if (objChangeMessage.IsLoaded)
                MainWnd.ClientPu_Bel.OnReceive_TEXT_MESSAGE += Receive_TextCmd;
            else
                MainWnd.ClientPu_Bel.OnReceive_TEXT_MESSAGE += ClientPu_OnReceive_TEXT_MESSAGE; 
            MainWnd.ClientPu_Bel.OnSend_TEXT_MESSAGE += new ExInPC.CmdEventHandler(Send_TextCmd);
            MainWnd.ClientPu_Bel.OnReceive_REQUEST += ClientPu_OnReceive_REQUEST; // запрос - есть вопросы
            MainWnd.ClientPu_Bel.OnReceive_SUPPRESS_FHSS += ClientPu_OnReceive_SUPPRESS_FHSS; // ППРЧ на РП - есть вопрос
            MainWnd.ClientPu_Bel.OnReceive_REQUEST_BEARING += ClientPu_OnReceive_REQUEST_BEARING;//Запрос пеленгования
            MainWnd.ClientPu_Bel.OnReceive_TEST += ClientPu_OnReceive_TEST;

            
            //подписка на обновления данных о состоянии ФРЧ на РП
            VariableWork.aWPtoBearingDSPprotocolNew.RadioJamStateUpdate += RadioJamStateUpdate;
            //подписка на обновления данных о состоянии ППРЧ на РП
            VariableWork.aWPtoBearingDSPprotocolNew.FhssRadioJamUpdate += FhssRadioJamUpdate;

            stateIndicateReadPU.SetIndicateOn();
            stateIndicateWritePU.SetIndicateOn();
            if (postControlStract.Com != null)
            {
                try
                {
                    ClientPu_Bel.OpenPort(postControlStract.Com);
                }
                catch { }
            }
        }

        byte[] GetIndexLetters(Protocols.RadioJamTargetState[] mass, byte letter)
        {
            List<byte> indexesLetter = new List<byte>();
            for (byte i = 0; i < mass.Length; i++)
            {
                if (letter == LetterFind(mass[i].Frequency))
                    indexesLetter.Add(i);
            }
            return indexesLetter.ToArray();
        }


        TPCStateSupressFreq[] StateSuppFWS_Bel; // Cостояние ФРЧ на РП
        void RadioJamStateUpdate(Protocols.RadioJamStateUpdateEvent answer)
        {
            if (answer == null)
                return;
            if (answer.Header.ErrorCode != 0)
                return;
            try
            {
                List<TPCStateSupressFreq> ListTemp = new List<TPCStateSupressFreq>();
                TPCStateSupressFreq temp = new TPCStateSupressFreq();
                for (byte i = 0; i < NumLetters; i++)
                {
                    byte[] indexesLetter = GetIndexLetters(answer.TargetStates, (byte)(i + 1));
                    if (indexesLetter.Length != 0)
                    {
                        temp = new TPCStateSupressFreq();
                        //temp.bStateWork = new byte[CountStateSupressFreq];
                        //temp.bStateLongTerm = new byte[CountStateSupressFreq];
                        //temp.bStateSupress = new byte[CountStateSupressFreq];
                        temp.Init(CountStateSupressFreq);
                        temp.bSighF = bSighF_FRCH;
                        temp.bStateFHSS = 0;
                        temp.bTempSubRange = (byte)(i + 1);

                        for (int j = 0; j < indexesLetter.Length; j++)
                        {
                            temp.bStateWork[j] = answer.TargetStates[indexesLetter[j]].EmitState;
                            temp.bStateLongTerm[j] = answer.TargetStates[indexesLetter[j]].ControlState;
                            temp.bStateSupress[j] = answer.TargetStates[indexesLetter[j]].RadioJamState;
                        }
                    ListTemp.Add(temp);
                    }
                }
                StateSuppFWS_Bel = ListTemp.ToArray();
            }
            catch { StateSuppFWS_Bel = null; }
        }

        TPCStateSupressFreq StateSuppFHSS_Bel; // Cостояние ППРЧ на РП
        void FhssRadioJamUpdate(Protocols.FhssRadioJamUpdateEvent answer)
        {
            if (answer == null)
                return;
            if (answer.Header.ErrorCode != 0)
                return;
            try
            {
                StateSuppFHSS_Bel = new TPCStateSupressFreq();
               // StateSuppFHSS_Bel.bStateLongTerm = new byte[CountStateSupressFreq];
                //StateSuppFHSS_Bel.bStateSupress = new byte[CountStateSupressFreq];
               // StateSuppFHSS_Bel.bStateWork = new byte[CountStateSupressFreq];
                StateSuppFHSS_Bel.Init(CountStateSupressFreq);
                StateSuppFHSS_Bel.bSighF = bSighF_PPRCH;
                StateSuppFHSS_Bel.bStateFHSS = Convert.ToByte(answer.JammingStates[0].IsJammed);
                StateSuppFHSS_Bel.bTempSubRange = 255;

            }
            catch { StateSuppFHSS_Bel = new TPCStateSupressFreq(); }
        }


        void ClientPu_OnReceive_TEST(object obj)
        {
            TPCTestChannel tpcTestChannel = new TPCTestChannel();

            tpcTestChannel.Init();
            tpcTestChannel.bSignDir = 1;
            tpcTestChannel.bRegimeMain = RegimeToPU(variableWork.Regime);
            tpcTestChannel.bLetterMain = variableSuppression.OwnLetters;
            tpcTestChannel.bLetterAdd = variableSuppression.LinkedLetters;
            MainWnd.ClientPu_Bel.Send_Test(tpcTestChannel);
        }
        //Назначение ППРЧ на РП
        void ClientPu_OnReceive_SUPPRESS_FHSS(object obj)
        {
            byte ErrorCode = 0;
            TPCSupressFHSS tpcFHSS = (TPCSupressFHSS)obj;
            try
            {
                List<TDistribFHSS_RP> tempMass = variableWork.DistribFHSS_RPOwn.ToList();
                TDistribFHSS_RP temp = new TDistribFHSS_RP();
                switch (tpcFHSS.bAction)
                {
                    case 0: //Добавить
                        temp.bDeviation = tpcFHSS.bCodeDeviation;
                        temp.bDuration = 1;
                        temp.bManipulation = tpcFHSS.bCodeManipulation;
                        temp.bModulation = tpcFHSS.bCodeModulation;
                        temp.iFreqMin = tpcFHSS.iFreqBegin;
                        temp.iFreqMax = tpcFHSS.iFreqEnd;
                        temp.iID = tpcFHSS.bTempRow;
                        temp.iStep = tpcFHSS.bCodeStep;
                        temp.sLevel = -60;
                        temp.bLetter[0] = LetterFind(tpcFHSS.iFreqBegin);
                        temp.bLetter[1] = LetterFind(tpcFHSS.iFreqEnd);
                        temp.bCodeFFT = 0;
                        tempMass.Insert(tpcFHSS.bTempRow - 1, temp);
                        break;
                    case 1: //Заменить
                        temp.bDeviation = tpcFHSS.bCodeDeviation;
                        temp.bDuration = 1;
                        temp.bManipulation = tpcFHSS.bCodeManipulation;
                        temp.bModulation = tpcFHSS.bCodeModulation;
                        temp.iFreqMin = tpcFHSS.iFreqBegin;
                        temp.iFreqMax = tpcFHSS.iFreqEnd;
                        temp.iID = tpcFHSS.bTempRow;
                        temp.iStep = tpcFHSS.bCodeStep;
                        temp.sLevel = -60;
                        temp.bLetter[0] = LetterFind(tpcFHSS.iFreqBegin);
                        temp.bLetter[1] = LetterFind(tpcFHSS.iFreqEnd);
                        temp.bCodeFFT = 0;
                        tempMass.Insert(tpcFHSS.bTempRow - 1, temp);
                        break;
                    case 2: //Удалить
                        tempMass.RemoveAt(tpcFHSS.bTempRow - 1);
                        break;
                    case 4: //Очистить
                        tempMass.Clear();
                        break;

                    default: //Ошибка
                        ErrorCode = 2;
                        break;
                }
                Invoke((MethodInvoker)(() => variableWork.DistribFHSS_RPOwn = tempMass.ToArray()));
            }
            catch (SystemException)
            {
                ErrorCode = 2;
            }

            TPCAnsReception tpcRexeption = new TPCAnsReception();
            tpcRexeption.bCmd = RECEPTION;
            tpcRexeption.bCodeError = ErrorCode;
            tpcRexeption.bTempPack = tpcFHSS.bTempRow;
            MainWnd.ClientPu_Bel.Send_Receipt(tpcRexeption);
        }

         async void ClientPu_OnReceive_REQUEST_BEARING(object obj)
        {
            TPCRequestBearing tpcBearing = (TPCRequestBearing)obj;
            byte ErrorCode = 0;
            switch (tpcBearing.bSign)
            {
                case 0: //Квазиодновременное пеленгование
            int iFmin = 0;
            int iFmax = 0;
            Protocols.QuasiSimultaneouslyDFResponse answer;
            try
            {
                iFmin = tpcBearing.iFreq - 250;
                iFmax = tpcBearing.iFreq + 250;
                answer = await VariableWork.aWPtoBearingDSPprotocolNew.QuasiSimultaneouslyDFX10(iFmin, iFmax, 3, 3);
                TPCSimultanBearing tpcSimul = new TPCSimultanBearing();
                tpcSimul.wBearMain = (ushort)(answer.Source.Direction == -1 ? (361) : Math.Round(answer.Source.Direction / 10.0d));
                tpcSimul.wBearAdd = (ushort)(answer.Source.Direction2 == -1 ? (361) : Math.Round(answer.Source.Direction2 / 10.0d));
               
            }
            catch
            {
                ErrorCode = 1;
                MainWnd.ClientPu.SendSimulBear(ErrorCode, 0, 0, 0, 0);
            }
                    break;
                case 1: //Исполнительное пеленгование 
                    break;
                default: //Error
                    ErrorCode = 2;
                    break;
            }

            throw new NotImplementedException();
        }

        void ClientPu_OnReceive_TEXT_MESSAGE(object obj)
        {
            byte ErrorCode = 0;
            TPCTextMessage tpcTextMessage = (TPCTextMessage)obj;
            try
            {
                ResizeArray(ref tpcTextMessage.bText);
                BigTextCmd += Encoding.Default.GetString(tpcTextMessage.bText);
                if (tpcTextMessage.bTempPack == tpcTextMessage.bCountPack)
                {
                    tpcTextMessage.strText = BigTextCmd;
                    messPU.sTime.Add(System.DateTime.Now.ToString("hh:mm:ss"));
                    messPU.sText.Add(tpcTextMessage.strText);
                    BigTextCmd = "";
                }
            }
            catch
            {
                ErrorCode = 2;
            }
            TPCAnsReception tpcReception = new TPCAnsReception();
            tpcReception.bCmd = RECEPTION;
            tpcReception.bCodeError = ErrorCode;
            tpcReception.bTempPack = tpcTextMessage.bTempPack;
            MainWnd.ClientPu_Bel.Send_Receipt(tpcReception);
        }
        string BigTextCmd = "";

        void ResizeArray(ref byte[] mass)
        {
            Array.Resize(ref mass, 9);
            while (mass[mass.Length - 1] == 0)
            {
                Array.Resize(ref mass, mass.Length - 1);
            }
        }

        private void Receive_TextCmd(object obj)
        {
            byte ErrorCode = 0;
            TPCTextMessage tpcTextMessage = (TPCTextMessage)obj;
            try
            {
                string strName = "";
                strName = "  ";
                ResizeArray(ref tpcTextMessage.bText);
                BigTextCmd += Encoding.Default.GetString(tpcTextMessage.bText);
                if (tpcTextMessage.bTempPack == tpcTextMessage.bCountPack)
                {
                    tpcTextMessage.strText = BigTextCmd; if (objChangeMessage.rtbHistoryPC.InvokeRequired)
                    {
                        objChangeMessage.rtbHistoryPC.Invoke((MethodInvoker)(delegate()
                        {
                            objChangeMessage.rtbHistoryPC.SelectionAlignment = HorizontalAlignment.Right;
                            objChangeMessage.showLogJammer.ShowMessage(objChangeMessage.rtbHistoryPC, 0, System.DateTime.Now.ToString("hh:mm:ss"), strName, " " + tpcTextMessage.strText);
                            objChangeMessage.showLogJammer.ShowMessage(objChangeMessage.rtbHistoryPC, 0, "", "", ""); //"\n");
                        }));

                    }
                    else
                    {
                        objChangeMessage.rtbHistoryPC.SelectionAlignment = HorizontalAlignment.Right;
                        objChangeMessage.showLogJammer.ShowMessage(objChangeMessage.rtbHistoryPC, 0, System.DateTime.Now.ToString("hh:mm:ss"), strName, " " + tpcTextMessage.strText);
                        objChangeMessage.showLogJammer.ShowMessage(objChangeMessage.rtbHistoryPC, 0, "", "", ""); //"\n");
                    }
                    BigTextCmd = "";
                }
            }
            catch (SystemException)
            { ErrorCode = 2; }

            TPCAnsReception tpcReception = new TPCAnsReception();
            tpcReception.bCmd = RECEPTION;
            tpcReception.bCodeError = ErrorCode;
            tpcReception.bTempPack = tpcTextMessage.bTempPack;
            MainWnd.ClientPu_Bel.Send_Receipt(tpcReception);

        }

        private void Send_TextCmd(object obj)
        {
            try
            {
                TPCTextMessage tpcTextMessage = (TPCTextMessage)obj;
                if (objChangeMessage.rtbHistoryPC.InvokeRequired)
                {
                    objChangeMessage.rtbHistoryPC.Invoke((MethodInvoker)(delegate()
                    {
                        objChangeMessage.rtbHistoryPC.SelectionAlignment = HorizontalAlignment.Left;
                        objChangeMessage.showLogJammer.ShowMessage(objChangeMessage.rtbHistoryPC, 1, System.DateTime.Now.ToString("hh:mm:ss"), "", " " + tpcTextMessage.strText);
                        objChangeMessage.showLogJammer.ShowMessage(objChangeMessage.rtbHistoryPC, 1, "", "", ""); //"\n");
                    }));

                }
                else
                {
                    objChangeMessage.rtbHistoryPC.SelectionAlignment = HorizontalAlignment.Left;
                    objChangeMessage.showLogJammer.ShowMessage(objChangeMessage.rtbHistoryPC, 1, System.DateTime.Now.ToString("hh:mm:ss"), "", " " + tpcTextMessage.strText);
                    objChangeMessage.showLogJammer.ShowMessage(objChangeMessage.rtbHistoryPC, 1, "", "", ""); //"\n");
                }
            }
            catch (SystemException)
            {
            }
        }

        //Синхронизация времени
        private void Receive_RequestSynchTime_Bel(object obj)
        {
            try
            {
                TPCSynchronize tTime = (TPCSynchronize)obj;
                SYSTEMTIME time = new SYSTEMTIME();
                DateTime DateUtc = DateTime.UtcNow;
                short utcHour = (short)(DateTime.Now.Hour - DateTime.UtcNow.Hour);
                short utcMin = (short)(DateTime.Now.Minute - DateTime.UtcNow.Minute);
                //получаем текущее время
                GetSystemTime(ref time);

                time.wHour = (short)(tTime.bHour - utcHour);
                time.wMinute = (short)(tTime.bMin - utcMin);
                if (time.wMinute < 0)
                {
                    time.wMinute = (short)(60 + time.wMinute);
                    time.wHour--;
                }
                if (time.wMinute >= 60)
                {
                    time.wMinute = (short)(time.wMinute - 60);
                    time.wHour++;
                }
                if (time.wHour < 0) { time.wHour = (short)(24 + time.wHour); }
                if (time.wHour > 23) { time.wHour = (short)(time.wHour - 24); }
                time.wSecond = tTime.bSec;

                //DateAndTime.TimeOfDay = DateTime.Now.AddSeconds(3000).AddMilliseconds(80);
                //устанавливаем новые значения
                if (!SetSystemTime(ref time))
                {
                    //получаем текущее время
                    GetSystemTime(ref time);
                    tTime.bHour = (byte)(time.wHour + utcHour);
                    tTime.bMin = (byte)(time.wMinute + utcMin);
                    tTime.bSec = (byte)utcHour;// time.wSecond;

                    if (ClientPu_Bel != null)
                        ClientPu_Bel.Send_Synchronize(tTime);
                }
                else
                {
                    if (ClientPu_Bel != null)
                        ClientPu_Bel.Send_Synchronize(tTime);
                }

            }
            catch (SystemException)
            { }
        }

        private void Send_SynchTime(object obj)
        {
            try
            {
                /*string strName = "";
                if (chbName.Checked)
                    strName = CMD_SynchTime + "\n"; //+" ( Адр " + bAddressSend.ToString() + ")\n";

                string strDetail = "";

                if (chbDetail.Checked == true)
                {
                    strDetail = "Код ошибки  " + bCodeError.ToString() + "\n";
                    strDetail += "Время " + tTime.bHour.ToString() + ":" + tTime.bMinute.ToString() + ":" + tTime.bSecond.ToString() + "\n";
                }

                if (chbName.Checked)
                    showLogJammer.ShowMessage(rtbLogJammer, 1, System.DateTime.Now.ToString("hh:mm:ss"), strName, strDetail);*/
            }
            catch (SystemException)
            { }
        }

        /////Изменение режима
        private async void Receive_RequestRegime(object obj)
        {
            TPCRegimeWork tpcRegimeWork = (TPCRegimeWork)obj;
            byte ErrorCode = 0;
            byte Regim = RegimeToARM(tpcRegimeWork.bRegime);
            switch (Regim)
            {
                case 2:

                    //Установка режима работы с эфира
                    var answer002 = await VariableWork.aWPtoBearingDSPprotocolNew.SetReceiversChannel(0);

                    //установка курсового угла
                    var answer02 = await VariableWork.aWPtoBearingDSPprotocolNew.SetDirectionCorrection(variableIntellegence.RelativeBearing, true);

                    //установка диапазонов и секторов радиоразведки
                    var answer = await VariableWork.aWPtoBearingDSPprotocolNew.SetSectorsAndRanges(0, 0, variableWork.RangeSectorReconOwn); //Шифр 3

                    //установка порога для панорамы
                    panoramaControl1.SetSectorsAndRanges(variableWork.RangeSectorReconOwn);

                    //установка фильтов для обнаружения ИРИ
                    answer = await VariableWork.aWPtoBearingDSPprotocolNew.SetFilters(panoramaControl1.GetThreshold(), 0, 0, 0); //Шифр 18
                    break;
                case 3:

                    //Установка режима работы с эфира
                    var answer003 = await VariableWork.aWPtoBearingDSPprotocolNew.SetReceiversChannel(0);

                    //установка курсового угла
                    var answer03 = await VariableWork.aWPtoBearingDSPprotocolNew.SetDirectionCorrection(variableIntellegence.RelativeBearing, true);

                    //установка диапазонов и секторов радиоподавления
                    var answerSSaR = await VariableWork.aWPtoBearingDSPprotocolNew.SetSectorsAndRanges(1, 0, variableWork.RangeSectorSupprOwn); //Шифр 3

                    //установка синих квадратиков для панорамы
                    panoramaControl1.SetSectorsAndRanges(variableWork.RangeSectorSupprOwn);

                    //Установка параметров радиоподавлениЯ для ФРЧ, Шифр 32
                    var answerSettingsFrs = await VariableWork.aWPtoBearingDSPprotocolNew.SetFrsRadioJamSettings(variableSuppression.TimeRadiatFWS, variableSuppression.Circle * 1000, variableSuppression.ChannelUse);

                    Protocols.FRSJammingSetting[] fRSJammingSettingsFrs = new Protocols.FRSJammingSetting[variableWork.SupprFWS_Own.Length];
                    for (int i = 0; i < variableWork.SupprFWS_Own.Length; i++)
                    {
                        fRSJammingSettingsFrs[i].DeviationCode = variableWork.SupprFWS_Own[i].bDeviation;
                        fRSJammingSettingsFrs[i].DurationCode = variableWork.SupprFWS_Own[i].bDuration;
                        fRSJammingSettingsFrs[i].Frequency = variableWork.SupprFWS_Own[i].iFreq; // ?
                        fRSJammingSettingsFrs[i].Id = variableWork.SupprFWS_Own[i].iID;
                        fRSJammingSettingsFrs[i].Liter = variableWork.SupprFWS_Own[i].bLetter;
                        fRSJammingSettingsFrs[i].ManipulationCode = variableWork.SupprFWS_Own[i].bManipulation;
                        fRSJammingSettingsFrs[i].ModulationCode = variableWork.SupprFWS_Own[i].bModulation;
                        fRSJammingSettingsFrs[i].Priority = variableWork.SupprFWS_Own[i].bPrior;
                        fRSJammingSettingsFrs[i].Threshold = Convert.ToByte((-1) * variableWork.SupprFWS_Own[i].sLevel);

                    }

                    //Установка ИРИ ФРЧ для РП, Шифр 6
                    //1.2.11
                    var answerFreqFrs = await VariableWork.aWPtoBearingDSPprotocolNew.SetFRSJamming((byte)0,fRSJammingSettingsFrs);

                    break;
            }
            var answerMode = await VariableDynamic.VariableWork.aWPtoBearingDSPprotocolNew.SetMode(Regim);
            if (answerMode.Header.ErrorCode != 0)
                ErrorCode = 1;
            else
                Invoke((MethodInvoker)(() => variableWork.Regime = Regim));
            TPCAnsReception tpcAnsReception = new TPCAnsReception();
            tpcAnsReception.bCodeError = ErrorCode;
            tpcAnsReception.bCmd = RECEPTION;
            tpcAnsReception.bTempPack = 1;
            MainWnd.ClientPu_Bel.Send_Receipt(tpcAnsReception);
        }
        //Специальные частоты
        private void Receive_RequestRangeSpec(object sender)
        {
            byte ErrorCode = 0;
            TPCForbidRangeFreq tRangeSpec = (TPCForbidRangeFreq)sender;
            try
            {
                // Запрещенные частоты
                List<Protocols.FrequencyRange> tempForbOwn = variableWork.FrequencyRangeForbiddenOwn.ToList();
                Protocols.FrequencyRange tempFrL = new Protocols.FrequencyRange();
                switch (tRangeSpec.bAction)
                {
                    case 0: //Добавить
                        if (tempForbOwn.Count == tRangeSpec.bTempRow)
                        {
                            tempForbOwn = new List<Protocols.FrequencyRange>();
                        }
                        if (tRangeSpec.iFreqBegin < FreqMin_RP_Bel || tRangeSpec.iFreqEnd > FreqMax_RP_Bel)
                        {
                            ErrorCode = 2;
                            break;
                        }
                        tempFrL.StartFrequency = tRangeSpec.iFreqBegin;
                        tempFrL.EndFrequency = tRangeSpec.iFreqEnd;
                        tempForbOwn.Add(tempFrL);
                        break;
                    case 1: //Заменить
                        if (tRangeSpec.iFreqBegin < FreqMin_RP_Bel || tRangeSpec.iFreqEnd > FreqMax_RP_Bel)
                        {
                            ErrorCode = 2;
                            break;
                        }
                        tempFrL.StartFrequency = tRangeSpec.iFreqBegin;
                        tempFrL.EndFrequency = tRangeSpec.iFreqEnd;
                        tempForbOwn[tRangeSpec.bTempRow - 1] = tempFrL;
                        break;
                    case 2: //Удалить
                        tempForbOwn.RemoveAt(tRangeSpec.bTempRow - 1);
                        break;
                    case 4: //Очистить
                        tempForbOwn.Clear();
                        break;
                    default: //Ошибка
                        ErrorCode = 2;
                        break;
                }
                Invoke((MethodInvoker)(() => variableWork.FrequencyRangeForbiddenOwn = tempForbOwn.ToArray()));
            }
            catch (Exception)
            {
                ErrorCode = 2;
            }
            TPCAnsReception tpcRexeption = new TPCAnsReception();
            tpcRexeption.bCmd = RECEPTION;
            tpcRexeption.bCodeError = ErrorCode;
            tpcRexeption.bTempPack = tRangeSpec.bTempRow;
            MainWnd.ClientPu_Bel.Send_Receipt(tpcRexeption);
        }

        //Назначение ФРЧ на РП
        private void Receive_RequestSupprFWS(object obj)
        {
            byte ErrorCode = 0;
            TPCSupressFWS tpcSuppFWS = (TPCSupressFWS)obj;

            try
            {
                List<TSupprFWS> tempMass = variableWork.SupprFWS_Own.ToList();
                TSupprFWS temp = new TSupprFWS();
                switch (tpcSuppFWS.bAction)
                {
                    case 0: //Добавить
                        temp.bDeviation = tpcSuppFWS.bCodeDeviation;
                        temp.bDuration = 1;
                        temp.bManipulation = tpcSuppFWS.bCodeManipulation;
                        temp.bModulation = tpcSuppFWS.bCodeModulation;
                        temp.bPrior = tpcSuppFWS.bPrior;
                        temp.iFreq = tpcSuppFWS.iFreq;
                        temp.iID = tpcSuppFWS.bTempRow;
                        temp.sBearing = -1;
                        temp.sLevel = -60;
                        temp.bLetter = LetterFind(tpcSuppFWS.iFreq);
                        if (temp.bLetter < 8) { tempMass.Insert(tpcSuppFWS.bTempRow - 1, temp); }
                        else { ErrorCode = 2; }
                        break;
                    case 1: //Заменить
                        temp.bDeviation = tpcSuppFWS.bCodeDeviation;
                        temp.bDuration = 1;
                        temp.bManipulation = tpcSuppFWS.bCodeManipulation;
                        temp.bModulation = tpcSuppFWS.bCodeModulation;
                        temp.bPrior = tpcSuppFWS.bPrior;
                        temp.iFreq = tpcSuppFWS.iFreq;
                        temp.iID = tpcSuppFWS.bTempRow;
                        temp.sBearing = -1;
                        temp.sLevel = -60;
                        temp.bLetter = LetterFind(tpcSuppFWS.iFreq);
                        if (temp.bLetter < 8) { tempMass[tpcSuppFWS.bTempRow - 1] = temp; }
                        else { ErrorCode = 2; }
                        break;
                    case 2: //Удалить
                        tempMass.RemoveAt(tpcSuppFWS.bTempRow - 1);
                        break;
                    case 4: //Очистить
                        tempMass.Clear();
                        break;

                    default: //Ошибка
                        ErrorCode = 2;
                        break;
                }
                Invoke((MethodInvoker)(() => variableWork.SupprFWS_Own = tempMass.ToArray()));
            }
            catch (SystemException)
            {
                ErrorCode = 2;
            }

            TPCAnsReception tpcRexeption = new TPCAnsReception();
            tpcRexeption.bCmd = RECEPTION;
            tpcRexeption.bCodeError = ErrorCode;
            tpcRexeption.bTempPack = tpcSuppFWS.bTempRow;
            MainWnd.ClientPu_Bel.Send_Receipt(tpcRexeption);

        }

        //Сектора и диапазоны РР
        private void Receive_RequestReconRangeSector(object sender)
        {
            byte ErrorCode = 0;
            TPCReconSectorRange tRangeSector = (TPCReconSectorRange)sender;
            try
            {
                // Радиоразведка                        
                List<Protocols.RangeSector> RtempMass = variableWork.RangeSectorReconOwn.ToList();
                Protocols.RangeSector Rtemp = new Protocols.RangeSector();
                switch (tRangeSector.bAction)
                {
                    case 0: //Добавить
                        if (tRangeSector.iFreqBegin < FreqMin_RR_Bel || tRangeSector.iFreqEnd > FreqMax_RR_Bel)
                        {
                            ErrorCode = 2;
                            break;
                        }
                        Rtemp.StartFrequency = tRangeSector.iFreqBegin;
                        Rtemp.EndFrequency = tRangeSector.iFreqEnd;
                        Rtemp.StartDirection = (short)(10 * tRangeSector.wAngleBegin);
                        Rtemp.EndDirection = (short)(10 * tRangeSector.wAngleEnd);

                        RtempMass.Insert(tRangeSector.bTempRow - 1, Rtemp);
                        break;
                    case 1: //Заменить
                        if (tRangeSector.iFreqBegin < FreqMin_RR_Bel || tRangeSector.iFreqEnd > FreqMax_RR_Bel)
                        {
                            ErrorCode = 2;
                            break;
                        }
                        Rtemp.StartFrequency = tRangeSector.iFreqBegin;
                        Rtemp.EndFrequency = tRangeSector.iFreqEnd;
                        Rtemp.StartDirection = (short)(10 * tRangeSector.wAngleBegin);
                        Rtemp.EndDirection = (short)(10 * tRangeSector.wAngleEnd);
                        RtempMass[tRangeSector.bTempRow - 1] = Rtemp;
                        break;
                    case 2: //Удалить
                        RtempMass.RemoveAt(tRangeSector.bTempRow - 1);
                        break;
                    case 4: //Очистить
                        RtempMass.Clear();
                        break;

                    default: //Ошибка
                        ErrorCode = 2;
                        break;
                }
                Invoke((MethodInvoker)(() => variableWork.UpdateAllRangeSectorReconOwn(RtempMass.ToArray())));
            }
            catch (SystemException)
            {
                ErrorCode = 2;
            }
            TPCAnsReception tpcRexeption = new TPCAnsReception();
            tpcRexeption.bCmd = RECEPTION;
            tpcRexeption.bCodeError = ErrorCode;
            tpcRexeption.bTempPack = tRangeSector.bTempRow;
            MainWnd.ClientPu_Bel.Send_Receipt(tpcRexeption);
        }

        //Сектора и диапазоны РП
        private void Receive_RequestSupressRangeSector(object sender)
        {
            byte ErrorCode = 0;
            TPCSupressSectorRange tRangeSector = (TPCSupressSectorRange)sender;
            try
            {
                // Радиоподавление
                List<Protocols.RangeSector> RtempMass = variableWork.RangeSectorSupprOwn.ToList();
                Protocols.RangeSector Rtemp = new Protocols.RangeSector();
                switch (tRangeSector.bAction)
                {
                    case 0: //Добавить
                        if (tRangeSector.iFreqBegin < FreqMin_RP_Bel || tRangeSector.iFreqEnd > FreqMax_RP_Bel)
                        {
                            ErrorCode = 2;
                            break;
                        }
                        Rtemp.StartFrequency = tRangeSector.iFreqBegin;
                        Rtemp.EndFrequency = tRangeSector.iFreqEnd;
                        Rtemp.StartDirection = (short)(10 * tRangeSector.wAngleBegin);
                        Rtemp.EndDirection = (short)(10 * tRangeSector.wAngleEnd);

                        RtempMass.Insert(tRangeSector.bTempRow - 1, Rtemp);
                        break;
                    case 1: //Заменить
                        if (tRangeSector.iFreqBegin < FreqMin_RP_Bel || tRangeSector.iFreqEnd > FreqMax_RP_Bel)
                        {
                            ErrorCode = 2;
                            break;
                        }
                        Rtemp.StartFrequency = tRangeSector.iFreqBegin;
                        Rtemp.EndFrequency = tRangeSector.iFreqEnd;
                        Rtemp.StartDirection = (short)(10 * tRangeSector.wAngleBegin);
                        Rtemp.EndDirection = (short)(10 * tRangeSector.wAngleEnd);
                        RtempMass[tRangeSector.bTempRow - 1] = Rtemp;
                        break;
                    case 2: //Удалить
                        RtempMass.RemoveAt(tRangeSector.bTempRow - 1);
                        break;
                    case 4: //Очистить
                        RtempMass.Clear();
                        break;

                    default: //Ошибка
                        ErrorCode = 2;
                        break;
                }
                Invoke((MethodInvoker)(() => variableWork.UpdateAllRangeSectorSupprOwn(RtempMass.ToArray())));
            }
            catch (SystemException)
            {
                ErrorCode = 2;
            }
            TPCAnsReception tpcRexeption = new TPCAnsReception();
            tpcRexeption.bCmd = RECEPTION;
            tpcRexeption.bCodeError = ErrorCode;
            tpcRexeption.bTempPack = tRangeSector.bTempRow;
            MainWnd.ClientPu_Bel.Send_Receipt(tpcRexeption);
        }

        //Запрос
        void ClientPu_OnReceive_REQUEST(object obj)
        {
            byte ErrorCode = 0;
            TPCRequestData tpcRequestData = (TPCRequestData)obj;
            try
            {
                ConvertCoord convertCoord = new ConvertCoord();
                CoordDegMinSec coordDegMinSec = new CoordDegMinSec();

                DateTime dTime;
                switch (tpcRequestData.bSign)
                {
                    case 0: //Координаты
                        TPCCoordJammer tpcCoord = new TPCCoordJammer();
                        try
                        {
                            coordDegMinSec = convertCoord.DegToDegMinSec(variableWork.CoordsGNSS[0].Lat, variableWork.CoordsGNSS[0].Lon);
                            tpcCoord.bLatDegree = (byte)coordDegMinSec.Lat[0].LatDeg;
                            tpcCoord.bLongDegree = (byte)coordDegMinSec.Lon[0].LonDeg;
                            tpcCoord.bLatMinute = (byte)coordDegMinSec.Lat[0].LatMin;
                            tpcCoord.bLongMinute = (byte)coordDegMinSec.Lon[0].LonMin;
                            tpcCoord.bLatSecond = (byte)coordDegMinSec.Lat[0].LatSec;
                            tpcCoord.bLongSecond = (byte)coordDegMinSec.Lon[0].LonSec;
                            tpcCoord.bSignLat = variableWork.CoordsGNSS[0].signLat;
                            tpcCoord.bSignLong = variableWork.CoordsGNSS[0].signLon;
                            tpcCoord.usHeight = (ushort)variableWork.CoordsGNSS[0].Alt;
                        }
                        catch
                        {
                            tpcCoord.bLongDegree = 0;
                            tpcCoord.bLatMinute = 0;
                            tpcCoord.bLongMinute = 0;
                            tpcCoord.bLatSecond = 0;
                            tpcCoord.bLongSecond = 0;
                            tpcCoord.bSignLat = 0; 
                            tpcCoord.bSignLong = 0;
                            tpcCoord.usHeight = 0;
                        }
                        MainWnd.ClientPu_Bel.Send_Coordinates(tpcCoord);
                        break;

                    case 1://ИРИ ФРЧ
                        //Берем из таблицы ИРИ ФРЧ на ЦР
                        TPCDataFWS tpcDataFWS = new TPCDataFWS();
                        for (int i = 0; i < variableWork.DistribFWS.Length; i++)
                        {
                            tpcDataFWS.bTempSource = (byte)(i + 1);
                            tpcDataFWS.iFreq = variableWork.DistribFWS[i].iFreq;

                            tpcDataFWS.bCodeWidth = TypeBandWIfth(variableWork.DistribFWS[i].iDFreq);
                            tpcDataFWS.bCodeType = variableWork.DistribFWS[i].bView; // ViewModulation(variableWork.DistribFWS[i].sView);
                            coordDegMinSec = convertCoord.DegToDegMinSec(Math.Abs(variableWork.DistribFWS[i].
                                dLatitude), Math.Abs(variableWork.DistribFWS[i].dLongitude));
                            tpcDataFWS.bLatDegree = (byte)coordDegMinSec.Lat[0].LatDeg;
                            tpcDataFWS.bLongDegree = (byte)coordDegMinSec.Lon[0].LonDeg;
                            tpcDataFWS.bLatMinute = (byte)coordDegMinSec.Lat[0].LatMin;
                            tpcDataFWS.bLongMinute = (byte)coordDegMinSec.Lon[0].LonMin;
                            tpcDataFWS.bLatSecond = (byte)coordDegMinSec.Lat[0].LatSec;
                            tpcDataFWS.bLongSecond = (byte)coordDegMinSec.Lon[0].LonSec;
                            ///Лина
                            tpcDataFWS.bSignLat = 0;
                            tpcDataFWS.bSignLong = 0;

                            if (variableWork.DistribFWS[i].dLatitude < 0)
                                tpcDataFWS.bSignLat = 1;
                            if (variableWork.DistribFWS[i].dLongitude < 0)
                                tpcDataFWS.bSignLong = 1;


                            tpcDataFWS.bCodeRate = 0; /// СПросить у Иры о скорости ТЛГ
                            ///////
                            dTime = DateTime.Now;
                            tpcDataFWS.bHour = (byte)dTime.Hour;
                            tpcDataFWS.bMin = (byte)dTime.Minute;
                            tpcDataFWS.bSec = (byte)dTime.Second;
                            tpcDataFWS.wBearMain = variableWork.DistribFWS[i].sBearing1 == -1 ?
                                ((ushort)361) : ((ushort)variableWork.DistribFWS[i].sBearing1);
                            tpcDataFWS.wBearAdd = variableWork.DistribFWS[i].sBearing2 == -1 ?
                                ((ushort)361) : ((ushort)variableWork.DistribFWS[i].sBearing2);
                            MainWnd.ClientPu_Bel.Send_DataFWS(tpcDataFWS);
                        }
                        break;

                    case 2: //ИРИ ППРЧ
                        TPCDataFHSS tcpDataFHSS = new TPCDataFHSS();
                        for (int i = 0; i < variableWork.DistribFHSS.Length; i++)
                        {
                            tcpDataFHSS.bCodeStep = TypeStep(variableWork.DistribFHSS[i].iStep);
                            tcpDataFHSS.iFreqBegin = variableWork.DistribFHSS[i].iFreqMin;
                            tcpDataFHSS.iFreqEnd = variableWork.DistribFHSS[i].iFreqMax;
                            tcpDataFHSS.bTempSource = (byte)(i + 1);

                            dTime = DateTime.Now;
                            tcpDataFHSS.bHour = (byte)dTime.Hour;
                            tcpDataFHSS.bMin = (byte)dTime.Minute;
                            tcpDataFHSS.bSec = (byte)dTime.Second;
                            //Лина
                            tcpDataFHSS.bSignLat = 0;
                            tcpDataFHSS.bSignLong = 0;
                            tcpDataFHSS.wBearMain = variableWork.DistribFHSS[i].sLocation[0].iQ1 == -1 ?
                                ((ushort)361) : ((ushort)variableWork.DistribFHSS[i].sLocation[0].iQ1); // Надо уточнить у Лины
                            tcpDataFHSS.wBearAdd = variableWork.DistribFHSS[i].sLocation[0].iQ2 == -1 ?
                                ((ushort)361) : ((ushort)variableWork.DistribFHSS[i].sLocation[0].iQ2); // Надо уточнить у Лины
                            coordDegMinSec = convertCoord.DegToDegMinSec(Math.Abs(variableWork.DistribFHSS[i].sLocation[0]
                                .dLatitude), Math.Abs(variableWork.DistribFHSS[i].sLocation[0].dLongitude));
                            tcpDataFHSS.bLatDegree = (byte)coordDegMinSec.Lat[0].LatDeg;
                            tcpDataFHSS.bLongDegree = (byte)coordDegMinSec.Lon[0].LonDeg;
                            tcpDataFHSS.bLatMinute = (byte)coordDegMinSec.Lat[0].LatMin;
                            tcpDataFHSS.bLongMinute = (byte)coordDegMinSec.Lon[0].LonMin;
                            tcpDataFHSS.bLatSecond = (byte)coordDegMinSec.Lat[0].LatSec;
                            tcpDataFHSS.bLongSecond = (byte)coordDegMinSec.Lon[0].LonSec;
                            tcpDataFHSS.bCodeWidth = TypeBandWIfth(variableWork.DistribFHSS[i].iDFreq);

                            tcpDataFHSS.bSignLat = 0;
                            tcpDataFHSS.bSignLong = 0;

                            if (variableWork.DistribFHSS[i].sLocation[0].dLatitude < 0)
                                tcpDataFHSS.bSignLat = 1;
                            if (variableWork.DistribFHSS[i].sLocation[0].dLongitude < 0)
                                tcpDataFHSS.bSignLong = 1;
                            //ИРА
                            tcpDataFHSS.bDuration = (byte)variableWork.DistribFHSS[i].iDuratImp;
                            tcpDataFHSS.bGroupDuration = (byte)variableIntellegence.TimeMonitor;
                            tcpDataFHSS.bCodeType = 0;
                            MainWnd.ClientPu_Bel.Send_DataFHSS(tcpDataFHSS);
                        }
                        break;

                    case 3://Квитанция о подавляемых ИРИ
                        if (variableWork.Regime == bCode_PPRCH_RP)
                        {
                            try
                            {
                                MainWnd.ClientPu_Bel.Send_ControlSignal(StateSuppFHSS_Bel);
                            }
                            catch { MainWnd.ClientPu_Bel.Send_ControlSignal(new TPCStateSupressFreq()); }
                        }
                        else
                        {
                            try
                            {
                                for (int j = 0; j < StateSuppFWS_Bel.Length; j++)
                                {
                                    MainWnd.ClientPu_Bel.Send_ControlSignal(StateSuppFWS_Bel[j]);
                                }
                            }
                            catch { MainWnd.ClientPu_Bel.Send_ControlSignal(new TPCStateSupressFreq()); }

                        }
                        break;

                    default://Ошибка
                        break;
                }

            }
            catch (SystemException)
            {
                ErrorCode = 2;
            }
            TPCAnsReception tpcRexeption = new TPCAnsReception();
            tpcRexeption.bCmd = RECEPTION;
            tpcRexeption.bCodeError = ErrorCode;
            tpcRexeption.bTempPack = 0;
            MainWnd.ClientPu_Bel.Send_Receipt(tpcRexeption);
        }

        byte TypeBandWIfth(int BandWifdth)
        {
            if (BandWifdth <= 50)
                return 0;
            if (BandWifdth > 50 && BandWifdth <= 125)
                return 1;
            if (BandWifdth > 125 && BandWifdth <= 250)
                return 2;
            if (BandWifdth < 250 && BandWifdth <= 1250)
                return 3;
            if (BandWifdth > 1250 && BandWifdth <= 30000)
                return 4;
            if (BandWifdth > 30000 && BandWifdth <= 35000)
                return 5;
            if (BandWifdth > 35000 && BandWifdth <= 83300)
                return 6;
            else
                return 7;
        }
        byte TypeRate(int iRate)
        {
            if (iRate <= 10)
                return 1;
            if (iRate > 10 && iRate <= 20)
                return 2;
            if (iRate > 20 && iRate <= 50)
                return 3;
            if (iRate > 50 && iRate <= 100)
                return 4;
            if (iRate > 100 && iRate <= 200)
                return 5;
            if (iRate > 200 && iRate <= 500)
                return 6;
            if (iRate > 500 && iRate <= 1000)
                return 7;
            if (iRate > 1000 && iRate <= 2000)
                return 8;
            if (iRate > 2000 && iRate <= 5000)
                return 9;
            if (iRate > 5000 && iRate <= 10000)
                return 10;
            if (iRate > 10000 && iRate <= 20000)
                return 11;
            if (iRate > 20000 && iRate <= 50000)
                return 12;
            if (iRate > 50000 && iRate <= 100000)
                return 13;
            if (iRate > 100000 && iRate <= 200000)
                return 14;
            if (iRate > 200000 && iRate <= 500000)
                return 15;
            if (iRate > 500000 && iRate <= 1000000)
                return 16;
            if (iRate > 1000000 && iRate <= 2000000)
                return 17;
            if (iRate > 2000000 && iRate <= 5000000)
                return 18;
            else
                return 19;
        }
        byte TypeStep(int iStep)
        {
            if (iStep <= 50)
                return 1;
            if (iStep > 50 && iStep <= 125)
                return 2;
            if (iStep > 125 && iStep <= 250)
                return 3;
            if (iStep > 250 && iStep <= 1250)
                return 4;
            if (iStep > 1250 && iStep <= 30000)
                return 5;
            else
                return 6;
        }

        #endregion


    }
}
