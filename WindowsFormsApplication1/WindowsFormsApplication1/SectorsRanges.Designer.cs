﻿namespace WndProject
{
    partial class SectorsRanges
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SectorsRanges));
            this.tables_Range_RR_RP = new Table_Range_RR_RP.tables_Range_RR_RP();
            this.SuspendLayout();
            // 
            // tables_Range_RR_RP
            // 
            this.tables_Range_RR_RP.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tables_Range_RR_RP.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tables_Range_RR_RP.Location = new System.Drawing.Point(0, 0);
            this.tables_Range_RR_RP.Name = "tables_Range_RR_RP";
            this.tables_Range_RR_RP.Size = new System.Drawing.Size(548, 248);
            this.tables_Range_RR_RP.TabIndex = 0;
            // 
            // SectorsRanges
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(548, 248);
            this.Controls.Add(this.tables_Range_RR_RP);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SectorsRanges";
            this.Text = "Сектора и диапазоны РР и РП";
            this.TopMost = true;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SectorsRanges_FormClosing);
            this.Load += new System.EventHandler(this.SectorsRanges_Load);
            this.ResumeLayout(false);

        }

        #endregion

        public Table_Range_RR_RP.tables_Range_RR_RP tables_Range_RR_RP;

    }
}