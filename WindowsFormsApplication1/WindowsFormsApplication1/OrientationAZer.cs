﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using System.Globalization;
using System.IO;
using System.Runtime.InteropServices;
using System.Xml;
using System.Xml.Linq;
using LibNMEAParser;
using LibNMEAParser.Sentence;
using DeviceLib;
using DeviceLib.DataSource;
using DeviceLib.MessageSplitting;
using DeviceLib.HeaderSplitting;
using DeviceLib.Listenning;
using System.IO.Ports;
using DLL_Compass;
using NationalInstruments.UI;
using NationalInstruments;
using System.Threading;
using ClassLibraryPeleng;
using USR_DLL;
using VariableDynamic;
using VariableStatic;
using ARD_DLL;

namespace WndProject
{
    public partial class OrientationAZer : Form
    {

        TCoordsGNSS[] CoordsGNSS;
        VariableWork variableWork;
        VariableCommon variableCommon = new VariableCommon();
        TDirectionAntennas DirectionAntennas;
        //-----------Для пересчета координат------------------------------------------
        // DATUM
        // ГОСТ 51794_2008
        public double dX_Coord_datum = 25;
        public double dY_Coord_datum = -141;
        public double dZ_Coord_datum = -80;

        public double deltaLatWGS84;
        public double deltaLonWGS84;

        public double dLatSK42Geo;
        public double dLonSK42Geo;

        public double dLatSK42Pr;
        public double dLonSK42Pr;
        //---------------------------------------------------------------------------
        // Для сравнения значений координат
        public double tempLat = -1;
        public double tempLon = -1;
        //---------------------------------------------------****--------------------

        class Colors
        {
            public Color LPA1_3;
            public Color LPA2_4;
            public Color LPA;
            public Color SP;
            public Color PU;

        }
        int[] col = new int[4];
        class Mestopolozhenie
        {
            public string ARD_rrs_comN = "Com3";
            public bool ARD_IsOpen = false;
            public short ARD1_azimuth;//текущий от севера
            public short ARD1_need_azimuth;// желаемый, от севера
            public short ARD1_Radant;//текущий в АПУ
            public byte ARD1_adress = 0x01;
            public bool ARD1_IsOpen = false;

            public short ARD2_azimuth;
            public short ARD2_need_azimuth;
            public short ARD2_Radant;
            public byte ARD2_adress = 0x02;

            public short ARD3_azimuth;
            public short ARD3_need_azimuth;
            public short ARD3_Radant;
            public string ARD_lpa_comN = "Com3";
            public byte ARD3_adress = 0x03;

            public double compass1_kren;
            public int compass1_azimuth;
            public double compass1_tangazh;
            public double compass1_correction;
            public int compass1_final;
            public string compas1_comN = "COM1";
            public bool compass1_IsOpen = false;

            public double compass2_kren;
            public int compass2_azimuth;
            public double compass2_tangazh;
            public double compass2_correction;
            public int compass2_final;
            public string compas2_comN = "Com5";
            public bool compass2_IsOpen = false;

            public int LPA_1_3 = 270;
            public int LPA_2_4 = 270;

            public Double dLatitude;
            public Double dLongitude;
            public int sputnic_count;
            public string date_time;

        }

        #region DLL
        [DllImport("kernel32")]
        private static extern int GetPrivateProfileString(string section, string key, string def, StringBuilder retVal, int size, string filePath);

        #endregion
        public delegate void ByteEventHandler1(int ugol);
        public delegate void ByteEventHandler();
        public event ByteEventHandler OnOpenFormOrient;
        public event ByteEventHandler OnCloseFormOrient;
        public event ByteEventHandler1 OnChangeKursovojUgol;
        
        
        private bool isOpenned;
        public bool fBearing = false;

        Mestopolozhenie mestopolozhenie;
        public int hmap;
        public Graphics graph;
        public double pLat, gLat;
        public double pLon, gLon;
        public List<PointDouble> listStations;
        public List<PointDouble> listBearing;
        Random rand;
        public MessageTranslate messageTranslate;

        Peleng peleng;
        ConvertCoord convertCoord;
        ParametersPort parametersPort;
        StructParamsPort structParamsPort;
        SetParameters setParameters; // dll LibNMEAParser
        GetMessage getMessage; // dll LibNMEAParser
        SerialPort serial_port;
        GPSEncoder gpsEncoder;
        DeviceHolder<string> m_gps_device_holder;
        PassiveListener<string> gps_all_message_listener;
        Compass Compas1 = new Compass();
        Compass Compas2 = new Compass();
        ComARD_APU ARD_RRS = new ComARD_APU();
        ComARD_APU ARD_LPA1 = new ComARD_APU();
        int Led = 0;
        int TimePause = 300;
        System.Threading.Timer timer;
        System.Threading.Timer tmWriteByteARD;
        System.Threading.Timer tmReadByteARD;
        System.Threading.Timer tmWriteByteSP;
        System.Threading.Timer tmReadByteSP;
        System.Threading.Timer tmWriteBytePU;
        System.Threading.Timer tmReadBytePU;
        int TimePause_compass1 = 10;
        int TimePause_compass2 = 10;
        Colors colors = new Colors();
        int CountDataFromKmp1 = 0;
        int CountDataFromKmp2 = 0;
        int Compass1Count = 0;
        int Compass2Count = 0;

        public OrientationAZer()
        {
            InitializeComponent();
            InitItems();
            mestopolozhenie = new Mestopolozhenie();
            if (zapolnenie() != 1) { MessageBox.Show("Не удалось загрузить сохраненные данные из INI-файла", "Ошибка"); };
            cbSatSyst.SelectedIndex = 1;
            cbCoordinateSystem.SelectedIndex = 0;
            try
            {
                InitConnectionARD_RRS();
                InitConnectionARD_LPA();
            }
            catch (Exception) { /*MessageBox.Show("Не удалось подключиться к СОМ-порту");*/ }
            Compas1.OnMessage += new Compass.MessageEventHandler(Compas_OnMSG1);
            Compas2.OnMessage += new Compass.MessageEventHandler(Compas_OnMSG2);
            listStations = new List<PointDouble>();
            listBearing = new List<PointDouble>();
            messageTranslate = new MessageTranslate();
            messageTranslate.MesTranslate();
            rand = new Random();
            serial_port = new SerialPort();
            getMessage = new GetMessage();
            peleng = new Peleng(null);
            convertCoord = new ConvertCoord();
            parametersPort = new ParametersPort();
            structParamsPort = new StructParamsPort();
            setParameters = new SetParameters();
            parametersPort.SetPortParams(ref structParamsPort);
            gpsEncoder = new GPSEncoder();
            m_gps_device_holder = new DeviceHolder<string>(new EndLineMessageSplitter(), new EmptyGPSHeaderSplitter());
           // setParameters.Position += new SetParameters.PositionEventHandler(setParameters_Position);
            setParameters.Location += new SetParameters.LocationEventHandler(setParameters_Location);
            setParameters.DatetimeUTC += new SetParameters.DateTimeUTCEventHandler(setParameters_DatetimeUTC);
            setParameters.DatetimeLocal += new SetParameters.DateTimeLocalEventHandler(setParameters_DatetimeLocal);
            setParameters.SatelliteCount += new SetParameters.SatelliteCountEventHandler(setParameters_SatelliteCount);
            setParameters.HeightAntenna += new SetParameters.HeightAntennaEventHandler(setParameters_HeightAntenna);
            m_gps_device_holder.onConnectionStatusChanged += new EventHandler<bool>(M_gps_device_onConnectionStatusChanged);
            gps_all_message_listener = new PassiveListener<string> { ID = string.Empty };
            gps_all_message_listener.onDataArrived += new EventHandler<IEnumerable<byte>>(Gps_all_message_listener_onDataArrived);
            gps_all_message_listener.Start();
            m_gps_device_holder.Subscribe(gps_all_message_listener);
            m_gps_device_holder.SendRequest(gpsEncoder.QueryModeCompatibility(GPSEncoder.ModeCompatibility.ALL_SYST));

            //Событие смены языка
            VariableStatic.VariableCommon.OnChangeCommonLanguage += new VariableStatic.VariableCommon.ChangeCommonEventHandler(VariableCommon_OnChangeCommonLanguage);
        

            try
            {
                ARD_LPA1.SendGetAngle(mestopolozhenie.ARD3_adress);
            }
            catch { }
            try
            {
                ARD_RRS.SendGetAngle(mestopolozhenie.ARD2_adress);
            }
            catch { }
            try
            {
                ARD_RRS.SendGetAngle(mestopolozhenie.ARD1_adress);
            }
            catch { }
            calculate_angle_to_north_Paint(mestopolozhenie);
            timerLedReadBytes.Enabled = true;
            variableWork = new VariableWork();
            if (OnOpenFormOrient != null) OnOpenFormOrient();
            try
            {
                bConnectKmp1_Click(this, null);
                bConnectKmp2_Click(this, null);
                bConnectGPS_Click(this, null);
            }
            catch { }
        }


        private void ChangeKursovoiUgol(int ugol)
        {
            MessageBox.Show(ugol.ToString());
        }
        private void Enabling_LPA(bool flag)
        {
            if (flag)
            {
                nudLPA2.Enabled = true;
                bSetLPA2.Enabled = true;
                bStopLPA2.Enabled = true;
                bTransportLPA.Enabled = true;
                ARD_LPA1.SendGetAngle(mestopolozhenie.ARD3_adress);
                ledReadLPA.Value = true;
                timer = new System.Threading.Timer(TimeStepLedReadLPA, null, TimePause, 0);

            }
            else
            {
                nudLPA2.Enabled = false;
                bSetLPA2.Enabled = false;
                bStopLPA2.Enabled = false;
                bTransportLPA.Enabled = false;
            }
        }
        private void Enabling_RRS(bool flag)
        {
            try
            {
                if (flag)
                {
                    nudSP2.Enabled = true;
                    bSetSP2.Enabled = true;
                    bStopSP2.Enabled = true;
                    bTransportRRS1.Enabled = true;
                    ARD_RRS.SendGetAngle(mestopolozhenie.ARD1_adress);
                    ledReadSP.Value = true;
                    timer = new System.Threading.Timer(TimeStepLedReadSP, null, TimePause, 0);
                    nudPU.Enabled = true;
                    bSetPU.Enabled = true;
                    bStopPU.Enabled = true;
                    bTransportRRS2.Enabled = true;
                    ARD_RRS.SendGetAngle(mestopolozhenie.ARD2_adress);
                    ledReadPU.Value = true;
                    timer = new System.Threading.Timer(TimeStepLedReadPU, null, TimePause, 0);
                }
                else
                {
                    nudSP2.Enabled = false;
                    bSetSP2.Enabled = false;
                    bStopSP2.Enabled = false;
                    bTransportRRS1.Enabled = false;
                    nudPU.Enabled = false;
                    bSetPU.Enabled = false;
                    bStopPU.Enabled = false;
                    bTransportRRS2.Enabled = false;
                }
            }
            catch { }
        }
        void ShowConnectLPA()
        {
            if (bConnectLPA.InvokeRequired)
            {
                bConnectLPA.Invoke((MethodInvoker)(delegate()
                {
                    bConnectLPA.BackColor = Color.Green;
                    Enabling_LPA(true);
                }));
            }
            else
            {
                bConnectLPA.BackColor = Color.Green;
                Enabling_LPA(true);
            }
        }
        void ShowConnectRRS()
        {
            if (bConnectRRS.InvokeRequired)
            {
                bConnectRRS.Invoke((MethodInvoker)(delegate()
                {
                    bConnectRRS.BackColor = Color.Green;
                    Enabling_RRS(true);
                }));
            }
            else
            {
                bConnectRRS.BackColor = Color.Green;
                Enabling_RRS(true);
            }
            if (bConnectRRS2.InvokeRequired)
            {
                bConnectRRS2.Invoke((MethodInvoker)(delegate()
                {
                    bConnectRRS2.BackColor = Color.Green;
                    Enabling_RRS(true);
                }));
            }
            else
            {
                bConnectRRS2.BackColor = Color.Green;
                Enabling_RRS(true);
            }
        }
        void ShowDisconnectLPA()
        {
            if (bConnectLPA.InvokeRequired)
            {
                bConnectLPA.Invoke((MethodInvoker)(delegate()
                {
                    bConnectLPA.BackColor = Color.Red;
                    Enabling_LPA(false);
                }));
            }
            else
            {
                bConnectLPA.BackColor = Color.Red;
                Enabling_LPA(false);
            }
        }
        void ShowDisconnectRRS()
        {
            if (bConnectRRS.InvokeRequired)
            {
                bConnectRRS.Invoke((MethodInvoker)(delegate()
                {
                    bConnectRRS.BackColor = Color.Red;
                    Enabling_RRS(false);
                }));
            }
            else
            {
                bConnectRRS.BackColor = Color.Red;
                Enabling_RRS(false);
            }
            if (bConnectRRS2.InvokeRequired)
            {
                bConnectRRS2.Invoke((MethodInvoker)(delegate()
                {
                    bConnectRRS2.BackColor = Color.Red;
                    Enabling_RRS(false);
                }));
            }
            else
            {
                bConnectRRS2.BackColor = Color.Red;
                Enabling_RRS(false);
            }
        }


        private void Gps_all_message_listener_onDataArrived(object sender, IEnumerable<byte> e)
        {
            if (InvokeRequired)
            {
                BeginInvoke((MethodInvoker)(() => Gps_all_message_listener_onDataArrived(sender, e)));
                return;
            }

            var text = Encoding.ASCII.GetString(e.ToArray());
            try
            {
                getMessage.parseMessage(text, setParameters);
                ledReadGPS1.Value = true;
                timer = new System.Threading.Timer(TimeStepLedReadGPS, null, TimePause, 0);
            }
            catch (Exception) { }
        }
        private void M_gps_device_onConnectionStatusChanged(object sender, bool e)
        {
            Console.WriteLine("GPS connected: {0}", e);

            if (e)
            {
                bConnectGPS.BackColor = Color.Green;
            }
            else
            {
                bConnectGPS.BackColor = Color.Red;
            }
        }

        private void setParameters_HeightAntenna(string code, double heightAntenna)
        {
            if (InvokeRequired)
            {
                BeginInvoke((MethodInvoker)(() => setParameters_HeightAntenna(code, heightAntenna)));
                return;
            }

            tbAlt.Text = heightAntenna.ToString();
        }

        private void setParameters_SatelliteCount(string code, int satelliteCount)
        {
            if (InvokeRequired)
            {
                BeginInvoke((MethodInvoker)(() => setParameters_SatelliteCount(code, satelliteCount)));
                return;
            }

            tbCountSat.Text = satelliteCount.ToString();
        }
        private void setParameters_DatetimeLocal(string code, DateTime dateTime)
        {
            if (InvokeRequired)
            {
                BeginInvoke((MethodInvoker)(() => setParameters_DatetimeLocal(code, dateTime)));
                return;
            }

            string[] dt = dateTime.ToString().Split(' ');

            tbDate.Text = dt[0];

            //tbTime.Text = dt[1];
        }
        private void setParameters_DatetimeUTC(string code, DateTime dateTime)
        {
            if (InvokeRequired)
            {
                BeginInvoke((MethodInvoker)(() => setParameters_DatetimeUTC(code, dateTime)));
                return;
            }

            string[] dt = dateTime.ToString().Split(' ');

            tbTime.Text = dt[1];
        }

        private void CoordinateSystem(double latitude, double longitude)
        {
            if (latitude == 0.0 && longitude == 0.0)
            {
                tbLat.Text = "";
                tbLon.Text = "";
                return;
            }

            // WGS84(эллипсоид)->элл.Красовского ***************************************************
            // Перевод координат WGS84(эллипсоид) -> эллипсоид Красовского(Пулково-42)
            // WGS84(широта,долгота)из Панорамы пересчитываем в эллипсод Красовского моей функцией
            // Входные параметры -> град,km
            // Перевод в рад - внутри функции

            // dLong ..............................................................................
            // Расчет приращения по долготе при преобразованиях координат WGS84<->SK42
            // (преобразования Молоденского), угл.сек
            peleng.f_dLong
                (
                // Входные параметры (град,км)
                    latitude,   // широта
                    longitude,  // долгота
                    0,          // высота

                    // DATUM,m
                    dX_Coord_datum,
                    dY_Coord_datum,
                    dZ_Coord_datum,

                    ref deltaLonWGS84   // приращение по долготе, угл.сек
                );
            // .................................................................... dLong

            // dLat .....................................................................
            // Расчет приращения по широте при преобразованиях координат WGS84<->SK42
            // (преобразования Молоденского), угл.сек
            peleng.f_dLat
                (
                // Входные параметры (град,км)
                    latitude,   // широта
                    longitude,  // долгота
                    0,          // высота

                    // DATUM,m
                    dX_Coord_datum,
                    dY_Coord_datum,
                    dZ_Coord_datum,

                    ref deltaLatWGS84        // приращение по долготе, угл.сек
                );

            // ..................................................................... dLat

            // Lat,Long .................................................................
            // Преобразования широты и долготы при пересчете WGS84->SK42
            peleng.f_WGS84_SK42_Lat_Long
                   (
                // Входные параметры (град,км)
                       latitude,   // широта
                       longitude,  // долгота
                       0,          // высота

                       deltaLatWGS84,       // приращение по долготе, угл.сек
                       deltaLonWGS84,       // приращение по долготе, угл.сек

                       // Выходные параметры (grad)
                       ref dLatSK42Geo,   // широта
                       ref dLonSK42Geo    // долгота
                   );

            // SK42(элл.)->Крюгер *****************************************************************
            // Преобразование геодезических координат (широта, долгота, высота) 
            // эллипсоида Красовского (СК42) в плоские прямоугольные координаты в
            // проекции Гаусса-Крюгера
            // Lat_Coord_Vyx_8442, Long_Coord_Vyx_8442 -> координаты эллипсоида Красовского,
            // получили моей функцией пересчета из панарамовских координат WGS84

            // Входные параметры -> !!!grad
            peleng.f_SK42_Krug
                   (
                // Входные параметры (!!! grad)
                // !!! эллипсоид Красовского
                       dLatSK42Geo,   // широта
                       dLonSK42Geo,  // долгота

                       // Выходные параметры (km)
                       ref dLatSK42Pr,
                       ref dLonSK42Pr
                   );

            if (rbWGS84.Checked)
            {
                switch (cbCoordinateSystem.SelectedIndex)
                {
                    case 0: // WGS 84 ( гео ), градусы
                        {
                            tbLat.Text = latitude.ToString("0.000000") + "°";
                            tbLon.Text = longitude.ToString("0.000000") + "°";

                            lLAt.Text = "Широта";
                            lLon.Text = "Долгота";
                        }
                        break;

                    case 1: // WGS 84 ( гео ), градусы, минуты
                        {
                            CoordDegMin coordDegMin = new CoordDegMin();

                            coordDegMin = convertCoord.DegToDegMin(latitude, longitude);

                            tbLat.Text = coordDegMin.Lat[0].LatDeg.ToString() + "°" + coordDegMin.Lat[0].LatMin.ToString("0.000000") + "'";
                            tbLon.Text = coordDegMin.Lon[0].LonDeg.ToString() + "°" + coordDegMin.Lon[0].LonMin.ToString("0.000000") + "'";

                            lLAt.Text = "Широта";
                            lLon.Text = "Долгота";
                        }
                        break;

                    case 2: // WGS 84 ( гео ), градусы, минуты, секунды
                        {
                            CoordDegMinSec coordDegMinSec = new CoordDegMinSec();

                            coordDegMinSec = convertCoord.DegToDegMinSec(latitude, longitude);

                            tbLat.Text = coordDegMinSec.Lat[0].LatDeg.ToString() + "°" + coordDegMinSec.Lat[0].LatMin.ToString() + "'" + coordDegMinSec.Lat[0].LatSec.ToString("0.000000") + "''";
                            tbLon.Text = coordDegMinSec.Lon[0].LonDeg.ToString() + "°" + coordDegMinSec.Lon[0].LonMin.ToString() + "'" + coordDegMinSec.Lon[0].LonSec.ToString("0.000000") + "''";

                            lLAt.Text = "Широта";
                            lLon.Text = "Долгота";
                        }
                        break;

                    default:
                        break;
                }
            }
            else if(rbCK42.Checked)
            {
                switch (cbCoordinateSystem.SelectedIndex)
                {
                    case 0: // CK 42 ( гео ) Эллипсоид Красовского, градусы
                        {
                            tbLat.Text = dLatSK42Geo.ToString("0.000000") + "°";
                            tbLon.Text = dLonSK42Geo.ToString("0.000000") + "°";

                            lLAt.Text = "Широта";
                            lLon.Text = "Долгота";
                        }
                        break;

                    case 1: // CK 42 ( гео ) Эллипсоид Красовского, градусы, минуты
                        {
                            CoordDegMin coordDegMin = new CoordDegMin();

                            coordDegMin = convertCoord.DegToDegMin(dLatSK42Geo, dLonSK42Geo);

                            tbLat.Text = coordDegMin.Lat[0].LatDeg.ToString() + "°" + coordDegMin.Lat[0].LatMin.ToString("0.000000") + "'";
                            tbLon.Text = coordDegMin.Lon[0].LonDeg.ToString() + "°" + coordDegMin.Lon[0].LonMin.ToString("0.000000") + "'";

                            lLAt.Text = "Широта";
                            lLon.Text = "Долгота";
                        }
                        break;

                    case 2: // CK 42 ( гео ) Эллипсоид Красовского, градусы, минуты, секунды
                        {
                            CoordDegMinSec coordDegMinSec = new CoordDegMinSec();

                            coordDegMinSec = convertCoord.DegToDegMinSec(dLatSK42Geo, dLonSK42Geo);

                            tbLat.Text = coordDegMinSec.Lat[0].LatDeg.ToString() + "°" + coordDegMinSec.Lat[0].LatMin.ToString() + "'" + coordDegMinSec.Lat[0].LatSec.ToString("0.000000") + "''";
                            tbLon.Text = coordDegMinSec.Lon[0].LonDeg.ToString() + "°" + coordDegMinSec.Lon[0].LonMin.ToString() + "'" + coordDegMinSec.Lon[0].LonSec.ToString("0.000000") + "''";

                            lLAt.Text = "Широта";
                            lLon.Text = "Долгота";
                        }
                        break;

                    case 3: // CK 42 ( прям ) Гаусса - Крюгера, метры
                    {
                        tbLat.Text = (dLatSK42Pr * 1000).ToString("0.000000");
                        tbLon.Text = (dLonSK42Pr * 1000).ToString("0.000000");

                        lLAt.Text = "X, м";
                        lLon.Text = "Y, м";
                    }
                    break;

                    default:
                        break;
                    
                }
            }
        }


        private async void setParameters_Location(string code, char signLat, double latitude, char signLon, double longitude, double altitude)
        {
            if (InvokeRequired)
            {
                BeginInvoke((MethodInvoker)(() => setParameters_Location(code, signLat, latitude, signLon, longitude, altitude)));
                return;
            }

          

            if (variableCommon.Operator == 1)
            {
                CoordsGNSS = new TCoordsGNSS[1];

                if (signLat == 'S')
                {
                    latitude = latitude * (-1);
                    CoordsGNSS[0].signLat = 1;
                }

                if (signLat == 'N')
                    CoordsGNSS[0].signLat = 0;

                if (signLon == 'W')
                {
                    longitude = longitude * (-1);
                    CoordsGNSS[0].signLon = 1;
                }

                if (signLon == 'E')
                    CoordsGNSS[0].signLon = 0;

                if ((tempLat != latitude) || (tempLon != longitude))
                {
                    CoordsGNSS[0].Lat = latitude;
                    CoordsGNSS[0].Lon = longitude;
                    CoordsGNSS[0].Alt = altitude;

                    variableWork.CoordsGNSS = CoordsGNSS;
                    //1.2.11
                    //var answer = await VariableWork.aWPtoBearingDSPprotocolNew.SetStationLocation(0,latitude, longitude, Convert.ToInt16(altitude), Convert.ToBoolean(variableCommon.UseGNSS));
                    var answer = await VariableWork.aWPtoBearingDSPprotocolNew.SetStationLocation(0, latitude, longitude, Convert.ToInt16(altitude), false);

                    tempLat = latitude;
                    tempLon = longitude;
                 }
            }

            CoordinateSystem(latitude, longitude);
        }


        //private async void setParameters_Position(string code, char signLat, double latitude, char signLon, double longitude)
        //{
        //    if (InvokeRequired)
        //    {
        //        BeginInvoke((MethodInvoker)(() => setParameters_Position(code, signLat, latitude, signLon, longitude)));
        //        return;
        //    }

        //    variableCommon = new VariableCommon();

        //    if (variableCommon.Operator == 1)
        //    {
        //        CoordsGNSS = new TCoordsGNSS();

        //        if (signLat == 'S')
        //        {
        //            latitude = latitude * (-1);
        //            CoordsGNSS.signLat = 1;
        //        }

        //        if (signLat == 'N')
        //            CoordsGNSS.signLat = 0;

        //        if (signLon == 'W')
        //        {
        //            longitude = longitude * (-1);
        //            CoordsGNSS.signLon = 0;
        //        }

        //        if (signLon == 'E')
        //            CoordsGNSS.signLon = 1;

        //        CoordsGNSS.Lat = latitude;
        //        CoordsGNSS.Lon = longitude;

        //        variableWork.CoordsGNSS = CoordsGNSS;

        //        //var answer = await VariableWork.aWPtoBearingDSPprotocolNew.SetStationLocation(latitude, longitude);
        //        var answer = await VariableWork.aWPtoBearingDSPprotocolNew.SetStationLocation(latitude, longitude, Convert.ToInt16(altitude), true);
        //    }

        //    CoordinateSystem(latitude, longitude);
        //}

        private void bTranslation_Click(object sender, EventArgs e)
        {


        }
        private void bConnectGPS_Click(object sender, EventArgs e)
        {
            if (bConnectGPS.BackColor == Color.Red && !m_gps_device_holder.IsConnected)
            {
                try
                {
                    PortGPSConnect();
                }
                catch { }
                switch (cbSatSyst.SelectedIndex)
                {
                    case 0:
                        if (m_gps_device_holder != null)
                            m_gps_device_holder.SendRequest(gpsEncoder.QueryNavigationSystem(GPSEncoder.NavigationSystemType.GP));
                        break;

                    case 1:
                        if (m_gps_device_holder != null)
                            m_gps_device_holder.SendRequest(gpsEncoder.QueryNavigationSystem(GPSEncoder.NavigationSystemType.GN));
                        break;

                    case 2:
                        if (m_gps_device_holder != null)
                            m_gps_device_holder.SendRequest(gpsEncoder.QueryNavigationSystem(GPSEncoder.NavigationSystemType.GL));
                        break;
                }

            }
            else
            {
                m_gps_device_holder.Close();
            }
        }
        public void PortGPSConnect()
        {
            Parity parity = new Parity();
            switch (structParamsPort.Parity)
            {
                case "None":
                    parity = Parity.None;
                    break;
                case "Odd":
                    parity = Parity.Odd;
                    break;
                case "Even":
                    parity = Parity.Even;
                    break;
                case "Mark":
                    parity = Parity.Mark;
                    break;
                case "Space":
                    parity = Parity.Space;
                    break;
                default:
                    break;
            }
            StopBits stopBits = new StopBits();
            switch (structParamsPort.StopBits)
            {
                case "None":
                    stopBits = StopBits.None;
                    break;
                case "One":
                    stopBits = StopBits.One;
                    break;
                case "Two":
                    stopBits = StopBits.Two;
                    break;
                case "OnePointFive":
                    stopBits = StopBits.OnePointFive;
                    break;
                default:
                    break;
            }
            //creating serial port for device
            serial_port = null;
            serial_port = new SerialPort(structParamsPort.PortName, structParamsPort.BaudRate, parity, structParamsPort.DataBits, stopBits);
            var serial_port_data_source = new SerialPortDataSource(serial_port) { ReadTimeout = 1000 };
            try
            {
                m_gps_device_holder.Open(serial_port_data_source);
                bConnectGPS.BackColor = Color.Green;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                bConnectGPS.BackColor = Color.Red;
                //Console.WriteLine(ex.Message);
            }
        }
        private void timerLedReadBytes_Tick(object sender, EventArgs e)
        {

        }
        int zapolnenie()
        {
            try
            {
                mestopolozhenie.ARD1_Radant = (Int16)Math.Round(iniRW.read_ARD1_Radant());
                mestopolozhenie.ARD_rrs_comN = iniRW.read_ARD_RRS_ComN();
                byte.TryParse(iniRW.read_ARD_RRS1_address(), out mestopolozhenie.ARD1_adress);
                byte.TryParse(iniRW.read_ARD_RRS2_address(), out mestopolozhenie.ARD2_adress);
                mestopolozhenie.ARD_lpa_comN = iniRW.read_ARD_LPA_ComN();
                byte.TryParse(iniRW.read_ARD_LPA_address(), out mestopolozhenie.ARD3_adress);
                mestopolozhenie.ARD2_Radant = (Int16)Math.Round(iniRW.read_ARD2_Radant());
                mestopolozhenie.ARD3_Radant = (Int16)Math.Round(iniRW.read_ARD3_Radant());
                mestopolozhenie.compass1_azimuth = (int)Math.Round(iniRW.read_compass1_azimuth());
                mestopolozhenie.compass1_correction = iniRW.read_compass1_correction();
                mestopolozhenie.compass1_kren = iniRW.read_compass1_kren();
                mestopolozhenie.compass1_tangazh = iniRW.read_compass1_tangazh();
                mestopolozhenie.compas1_comN = iniRW.read_Compass1_ComN();
                mestopolozhenie.compass2_azimuth = (int)Math.Round(iniRW.read_compass2_azimuth());
                mestopolozhenie.compass2_correction = iniRW.read_compass2_correction();
                mestopolozhenie.compass2_kren = iniRW.read_compass2_kren();
                mestopolozhenie.compass2_tangazh = iniRW.read_compass2_tangazh();
                mestopolozhenie.compas2_comN = iniRW.read_Compass2_ComN();
                //mestopolozhenie.sputnic_count = iniRW.get_amount();
                //calculate_angle_to_north_Paint(mestopolozhenie);
                col = iniRW.get_color_lpa();
                colors.LPA = Color.FromArgb(col[0], col[1], col[2], col[3]);
                col = iniRW.get_color_lpa1_3();
                colors.LPA1_3 = Color.FromArgb(col[0], col[1], col[2], col[3]);
                col = iniRW.get_color_lpa2_4();
                colors.LPA2_4 = Color.FromArgb(col[0], col[1], col[2], col[3]);
                col = iniRW.get_color_SP();
                colors.SP = Color.FromArgb(col[0], col[1], col[2], col[3]);
                col = iniRW.get_color_PU();
                colors.PU = Color.FromArgb(col[0], col[1], col[2], col[3]);
                change_colors();
                return 1;
            }
            catch (Exception ) { return 0; }
        }
        void change_colors()
        {
            lSP2.ForeColor = colors.SP;
            lLPA1_3.ForeColor = colors.LPA1_3;
            lLPA2_4.ForeColor = colors.LPA2_4;
            lPU.ForeColor = colors.PU;
            lLPA2.ForeColor = colors.LPA;
            complexPlotLPA.LineColor = colors.LPA;
            complexPlotLPA1_3.LineColor = colors.LPA1_3;
            complexPlotLPA2_4.LineColor = colors.LPA2_4;
            complexPlotPU.LineColor = colors.PU;
            complexPlotSP.LineColor = colors.SP;
        }
        int sohranenie_to_ini()
        {
            try
            {
                iniRW.write_ARD1_Radant(mestopolozhenie.ARD1_Radant);
                iniRW.write_ARD1_need_azimuth(mestopolozhenie.ARD1_need_azimuth);
                iniRW.write_ARD2_Radant(mestopolozhenie.ARD2_Radant);
                iniRW.write_ARD2_need_azimuth(mestopolozhenie.ARD2_need_azimuth);
                iniRW.write_ARD3_Radant(mestopolozhenie.ARD3_Radant);
                iniRW.write_ARD3_need_azimuth(mestopolozhenie.ARD3_need_azimuth);
                iniRW.write_compass1_azimuth(mestopolozhenie.compass1_azimuth);
                iniRW.write_compass2_azimuth(mestopolozhenie.compass2_azimuth);
                iniRW.write_Color_lpa(new int[] { colors.LPA.A, colors.LPA.R, colors.LPA.G, colors.LPA.B });
                iniRW.write_Color_lpa1_3(new int[] { colors.LPA1_3.A, colors.LPA1_3.R, colors.LPA1_3.G, colors.LPA1_3.B });
                iniRW.write_Color_lpa2_4(new int[] { colors.LPA2_4.A, colors.LPA2_4.R, colors.LPA2_4.G, colors.LPA2_4.B });
                iniRW.write_Color_SP(new int[] { colors.SP.A, colors.SP.R, colors.SP.G, colors.SP.B });
                iniRW.write_Color_PU(new int[] { colors.PU.A, colors.PU.R, colors.PU.G, colors.PU.B });

                return 1;
            }
            catch (Exception) { return 0; }
        }
        
        async void calculate_angle_to_north_Paint(Mestopolozhenie mestopolozhenie)
        {
            if (InvokeRequired)
            {
                BeginInvoke((MethodInvoker)(() => calculate_angle_to_north_Paint(mestopolozhenie)));
                return;
            }
            //mestopolozhenie.compass1_final=(3*mestopolozhenie.compass1_final/4)+int.Parse(tbK1Azimuth.Text.ToString())/4;
            short dd = (Int16)mestopolozhenie.compass1_final;

            mestopolozhenie.compass1_final = (int)Math.Round(mestopolozhenie.compass1_azimuth - mestopolozhenie.compass1_correction);
            mestopolozhenie.compass2_final = (int)Math.Round(mestopolozhenie.compass2_azimuth - mestopolozhenie.compass2_correction);
            mestopolozhenie.ARD1_azimuth = (Int16)(mestopolozhenie.ARD1_Radant + dd + mestopolozhenie.ARD2_Radant);
            mestopolozhenie.ARD2_azimuth = (Int16)(mestopolozhenie.ARD2_Radant + mestopolozhenie.compass1_final);
            mestopolozhenie.ARD3_azimuth = (Int16)(mestopolozhenie.ARD3_Radant + mestopolozhenie.compass1_final);

            if (mestopolozhenie.ARD1_Radant > 359.99) { mestopolozhenie.ARD1_Radant -= 360; }
            if (mestopolozhenie.ARD2_Radant > 359.99) { mestopolozhenie.ARD2_Radant -= 360; }
            if (mestopolozhenie.ARD3_Radant > 359.99) { mestopolozhenie.ARD3_Radant -= 360; }
            if (mestopolozhenie.ARD1_azimuth > 359.99) { mestopolozhenie.ARD1_azimuth -= 360; }
            if (mestopolozhenie.ARD2_azimuth > 359.99) { mestopolozhenie.ARD2_azimuth -= 360; }
            if (mestopolozhenie.ARD3_azimuth > 359.99) { mestopolozhenie.ARD3_azimuth -= 360; }
            if (mestopolozhenie.compass1_final > 359.99) { mestopolozhenie.compass1_final -= 360; }
            if (mestopolozhenie.compass2_final > 359.99) { mestopolozhenie.compass2_final -= 360; }
            if (mestopolozhenie.ARD1_Radant < 0) { mestopolozhenie.ARD1_Radant += 360; }
            if (mestopolozhenie.ARD2_Radant < 0) { mestopolozhenie.ARD2_Radant += 360; }
            if (mestopolozhenie.ARD3_Radant < 0) { mestopolozhenie.ARD3_Radant += 360; }
            if (mestopolozhenie.ARD1_azimuth > 359.99) { mestopolozhenie.ARD1_azimuth -= 360; }
            if (mestopolozhenie.ARD2_azimuth > 359.99) { mestopolozhenie.ARD2_azimuth -= 360; }
            if (mestopolozhenie.ARD3_azimuth > 359.99) { mestopolozhenie.ARD3_azimuth -= 360; }
            if (mestopolozhenie.ARD3_Radant < 0) { mestopolozhenie.ARD3_Radant += 360; }
            if (mestopolozhenie.compass1_final < 0) { mestopolozhenie.compass1_final += 360; }
            if (mestopolozhenie.compass2_final < 0) { mestopolozhenie.compass2_final += 360; }

            ShowDirection(complexPlotLPA, mestopolozhenie.ARD3_azimuth);
            ShowDirection(complexPlotPU, mestopolozhenie.ARD2_azimuth);
            ShowDirection(complexPlotSP, mestopolozhenie.ARD1_azimuth);
            tbK1Azimuth.Text = mestopolozhenie.compass1_final.ToString();
            tbK2Azimuth.Text = mestopolozhenie.compass2_final.ToString();
            tbSP2.Text = mestopolozhenie.ARD1_azimuth.ToString();
            tbPU.Text = mestopolozhenie.ARD2_azimuth.ToString();
            tbLPA2.Text = mestopolozhenie.ARD3_azimuth.ToString();
            if (OnChangeKursovojUgol != null) OnChangeKursovojUgol(mestopolozhenie.compass1_final);

            //-------------------------------------------------------------------
            // записать данные в VariableWork для отображения на карте
            //-------------------------------------------------------------------
            AddDirectionsAntennasToVW();
            //DirectionAntennas = new TDirectionAntennas();
            //DirectionAntennas.ARD1 = mestopolozhenie.ARD1_azimuth;
            //DirectionAntennas.ARD2 = mestopolozhenie.ARD2_azimuth;
            //DirectionAntennas.ARD3 = mestopolozhenie.ARD3_azimuth;
            //DirectionAntennas.compass = mestopolozhenie.compass1_final;
            //DirectionAntennas.LPA13 = 270;
            //DirectionAntennas.LPA24 = 270;
            //variableWork.DirectionAntennas = DirectionAntennas;

            //var answer = await VariableWork.aWPtoBearingDSPprotocolNew.SetAntennaDirections((short)DirectionAntennas.ARD1,
            //    (short)DirectionAntennas.ARD2,
            //    (short)DirectionAntennas.ARD3,
            //    (short)DirectionAntennas.compass,
            //    (short)DirectionAntennas.LPA13,
            //    (short)DirectionAntennas.LPA24);
            //-------------------------------------------------------------------
        }

        /// <summary>
        /// записать данные в VariableWork для отображения на карте
        /// </summary>
        public async void AddDirectionsAntennasToVW()
        {
            try
            {
                DirectionAntennas = new TDirectionAntennas();
                DirectionAntennas.ARD1 = mestopolozhenie.ARD1_azimuth;
                DirectionAntennas.ARD2 = mestopolozhenie.ARD2_azimuth;
                DirectionAntennas.ARD3 = mestopolozhenie.ARD3_azimuth;
                DirectionAntennas.compass = mestopolozhenie.compass1_final;
                DirectionAntennas.LPA13 = mestopolozhenie.LPA_1_3;
                DirectionAntennas.LPA24 = mestopolozhenie.LPA_2_4;
                variableWork.DirectionAntennas = DirectionAntennas;

                var answer = await VariableWork.aWPtoBearingDSPprotocolNew.SetAntennaDirections((short)DirectionAntennas.ARD1,
                    (short)DirectionAntennas.ARD2,
                    (short)DirectionAntennas.ARD3,
                    (short)DirectionAntennas.compass,
                    (short)DirectionAntennas.LPA13,
                    (short)DirectionAntennas.LPA24);
            }
            catch { }
        }

        private void nudLPA_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                double angle;
                double.TryParse(nudLPA2.Value.ToString(), out angle);
                mestopolozhenie.ARD3_need_azimuth = (Int16)(Math.Round(angle) - mestopolozhenie.compass1_final);
                if (mestopolozhenie.ARD3_need_azimuth < 0) mestopolozhenie.ARD3_need_azimuth += 360;
                if ((mestopolozhenie.ARD3_need_azimuth < 190) || (mestopolozhenie.ARD3_need_azimuth > 350)) return;
                ARD_LPA1.SendSetAngle(mestopolozhenie.ARD3_adress, 1, mestopolozhenie.ARD3_need_azimuth, 0);
            }
            catch (Exception) {  /*MessageBox.Show("Не  могу повернуть");*/  }
            ledReadLPA.Value = true;
            timer = new System.Threading.Timer(TimeStepLedReadLPA, null, TimePause, 0);


        }
        private void nudSP_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if ((nudSP2.Value < 5) || (nudSP2.Value > 355)) return;
                double angl;
                double.TryParse(nudSP2.Value.ToString(), out angl);
                mestopolozhenie.ARD1_need_azimuth = (Int16)(Math.Round(angl));//- mestopolozhenie.ARD2_azimuth;//-mestopolozhenie.compass1_final
                angl = mestopolozhenie.ARD1_need_azimuth - mestopolozhenie.ARD2_azimuth;
                if (angl < 0) angl += 360;
                ARD_RRS.SendSetAngle(mestopolozhenie.ARD1_adress, 1, (Int16)angl, 0);
            }
            catch (Exception) { /*MessageBox.Show("Не  могу повернуть");*/ }
            ledReadSP.Value = true;
            timer = new System.Threading.Timer(TimeStepLedReadSP, null, TimePause, 0);
        }
        private void nudPU_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if ((nudPU.Value < 5) || (nudPU.Value > 355)) return;
                double angl;
                double.TryParse(nudPU.Value.ToString(), out angl);
                mestopolozhenie.ARD2_need_azimuth = (Int16)(Math.Round(angl) - mestopolozhenie.compass1_final);
                if (mestopolozhenie.ARD2_need_azimuth < 0) mestopolozhenie.ARD2_need_azimuth += 360;
                ARD_RRS.SendSetAngle(mestopolozhenie.ARD2_adress, 1, mestopolozhenie.ARD2_need_azimuth, 0);
            }
            catch (Exception) {  /*MessageBox.Show("Не  могу повернуть");*/  }
            ledReadPU.Value = true;
            timer = new System.Threading.Timer(TimeStepLedReadPU, null, TimePause, 0);


        }
        private void nudLPA1_3_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                int.TryParse(nudLPA1_3.Value.ToString(), out mestopolozhenie.LPA_1_3);
                ShowDirection(complexPlotLPA1_3, mestopolozhenie.LPA_1_3);

                AddDirectionsAntennasToVW();
            }
            catch (Exception) { }
        }
        private void nudLPA2_4_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                int.TryParse(nudLPA2_4.Value.ToString(), out mestopolozhenie.LPA_2_4);
                ShowDirection(complexPlotLPA2_4, mestopolozhenie.LPA_2_4);

                AddDirectionsAntennasToVW();
            }
            catch (Exception) { }

        }

        private void ShowInfoTb(TextBox Tb, Double dValue)
        {
            if (Tb.InvokeRequired)
            {
                Tb.Invoke((MethodInvoker)(delegate()
                {
                    Tb.Text = dValue.ToString();

                }));

            }

            else
            {
                Tb.Text = dValue.ToString();
            }
        }
        private void ShowDirection(ComplexPlot complexPlot, double dAngle)
        {

            double dShiftAngleR = ((double)(dAngle) * Math.PI) / 180;


            complexPlot.PlotComplex(GeneratePlotData(dShiftAngleR, 4));
        }
        private static ComplexDouble[] GeneratePlotData(Double dAngle, Double dRadius)
        {
            ComplexDouble[] complexData = new ComplexDouble[2];

            complexData[0].Real = 0.3 * dRadius * Math.Sin(dAngle);
            complexData[0].Imaginary = 0.3 * dRadius * Math.Cos(dAngle);

            complexData[1].Imaginary = dRadius * Math.Cos(dAngle);
            complexData[1].Real = dRadius * Math.Sin(dAngle);

            return complexData;
        }
        // complexGraphSphere.PlotAreaImage = (rotateImage(bmp2, alpha));
        private Bitmap rotateImage(Bitmap input, float angle)
        {
            Bitmap result = new Bitmap(input.Width, input.Height);
            Graphics g = Graphics.FromImage(result);
            g.TranslateTransform((float)input.Width / 2, (float)input.Height / 2);
            g.RotateTransform(angle);
            g.TranslateTransform(-(float)input.Width / 2, -(float)input.Height / 2);
            g.DrawImage(input, new Point(0, 0));
            return result;
        }
        private void bSetSP_Click(object sender, EventArgs e)
        {
            try
            {
                double angl;
                double.TryParse(nudSP2.Value.ToString(), out angl);
                mestopolozhenie.ARD1_need_azimuth = (Int16)(Math.Round(angl) - mestopolozhenie.ARD2_azimuth);//-mestopolozhenie.compass1_final
                angl = mestopolozhenie.ARD1_need_azimuth;
                if (angl < 0) angl += 360;
                if (((Int16)angl > -1) && ((Int16)angl < 355))
                    ARD_RRS.SendSetAngle(mestopolozhenie.ARD1_adress, 1, (Int16)angl, 0);
            }
            catch (Exception) { /*MessageBox.Show("Не  могу повернуть");*/  }
        }
        private void bSetPU_Click(object sender, EventArgs e)
        {
            try
            {
                double angl;
                double.TryParse(nudPU.Value.ToString(), out angl);
                mestopolozhenie.ARD2_need_azimuth = (Int16)(Math.Round(angl) - mestopolozhenie.compass1_final);
                if (mestopolozhenie.ARD2_need_azimuth < 0) mestopolozhenie.ARD2_need_azimuth += 360;
                if ((mestopolozhenie.ARD2_need_azimuth > -1) && (mestopolozhenie.ARD2_need_azimuth < 355))
                    ARD_RRS.SendSetAngle(mestopolozhenie.ARD2_adress, 1, mestopolozhenie.ARD2_need_azimuth, 0);
            }
            catch (Exception) {  /*MessageBox.Show("Не  могу повернуть");*/  }
        }
        private void bSetLPA_Click(object sender, EventArgs e)
        {
            try
            {
                double angl;
                double.TryParse(nudLPA2.Value.ToString(), out angl);
                mestopolozhenie.ARD3_need_azimuth = (Int16)(Math.Round(angl) - mestopolozhenie.compass1_final);
                if (mestopolozhenie.ARD3_need_azimuth < 0) mestopolozhenie.ARD3_need_azimuth += 360;
                if (mestopolozhenie.ARD3_need_azimuth < 0) mestopolozhenie.ARD3_need_azimuth += 360;

                if ((mestopolozhenie.ARD3_need_azimuth > 190) && (mestopolozhenie.ARD3_need_azimuth < 350))
                    ARD_LPA1.SendSetAngle(mestopolozhenie.ARD3_adress, 1, mestopolozhenie.ARD3_need_azimuth, 0);
            }
            catch (Exception) {  /*MessageBox.Show("Не  могу повернуть");*/  }
        }
        private void bStopLPA_Click(object sender, EventArgs e)
        {
            try
            {
                ARD_LPA1.SendStop(mestopolozhenie.ARD3_adress, 0x00);
            }
            catch (Exception) { }


        }
        private void bStopPU_Click(object sender, EventArgs e)
        {
            try
            {
                ARD_RRS.SendStop(mestopolozhenie.ARD2_adress, 0x00);
            }
            catch (Exception) { }//ARDPU.ClosePort(); mestopolozhenie.ARD2_IsOpen = false; bConnectPU.BackColor = Color.Red; ledReadPU.Value = false; ledWritePU.Value = false; }
            //ledWritePU.Value = true;
            //timer = new System.Threading.Timer(TimeStepLedWritePU, null, TimePause, 0);
        }
        private void bStopSP_Click(object sender, EventArgs e)
        {
            try
            {
                ARD_RRS.SendStop(mestopolozhenie.ARD1_adress, 0x00); ;
            }
            catch (Exception) { }// ARDSP.ClosePort(); mestopolozhenie.ARD3_IsOpen = false; bConnectSP.BackColor = Color.Red; ledReadSP.Value = false; ledWriteSP.Value = false; }
            //ledWriteSP.Value = true;
            //timer = new System.Threading.Timer(TimeStepLedWriteSP, null, TimePause, 0);
        }

        private void InitConnectionARD_LPA()
        {
            try
            {
                if (ARD_LPA1 != null)
                    ARD_LPA1 = null;
                ARD_LPA1 = new ComARD_APU();
                ARD_LPA1.OnGetAngleCmd += new ComARD_APU.GetAngleEventHandler(OnGetAngleReceivedLPA);
                ARD_LPA1.OnSetAngleCmd += new ComARD_APU.SetAngleEventHandler(SetAngleReceivedLPA);
                ARD_LPA1.OnConnectPort += new ComARD_APU.ConnectEventHandler(ShowConnectLPA);
                ARD_LPA1.OnDisconnectPort += new ComARD_APU.ConnectEventHandler(ShowDisconnectLPA);
                //ARD_PU.OnReadByte += new ComARD_APU.ByteEventHandler(ShowReadBytePU);
                //ARD_PU.OnWriteByte += new ComARD_APU.ByteEventHandler(ShowWriteBytePU);
                ARD_LPA1.OpenPort(mestopolozhenie.ARD_lpa_comN, 19200, Parity.None, 8, StopBits.One);
                Thread.Sleep(100);
                //ARD_PU.SendGetAngle();

            }
            catch (Exception)
            {
                ARD_LPA1.OnGetAngleCmd -= new ComARD_APU.GetAngleEventHandler(OnGetAngleReceivedLPA);
                ARD_LPA1.OnSetAngleCmd -= new ComARD_APU.SetAngleEventHandler(SetAngleReceivedLPA);
                ARD_LPA1.OnConnectPort -= new ComARD_APU.ConnectEventHandler(ShowConnectLPA);
                ARD_LPA1.OnDisconnectPort -= new ComARD_APU.ConnectEventHandler(ShowDisconnectLPA);
            }
        }
        private void InitConnectionARD_RRS()
        {
            try
            {
                if (ARD_RRS != null)
                    ARD_RRS = null;
                ARD_RRS = new ComARD_APU();
                ARD_RRS.OnGetAngleCmd += new ComARD_APU.GetAngleEventHandler(OnGetAngleReceivedSPs);
                ARD_RRS.OnSetAngleCmd += new ComARD_APU.SetAngleEventHandler(SetAngleReceivedSPs);
                ARD_RRS.OnConnectPort += new ComARD_APU.ConnectEventHandler(ShowConnectRRS);
                ARD_RRS.OnDisconnectPort += new ComARD_APU.ConnectEventHandler(ShowDisconnectRRS);
                //ARD_RRS.OnReadByte += new ComARD_APU.ByteEventHandler(ShowReadByteSPs);
                //ARD_RRS.OnWriteByte += new ComARD_APU.ByteEventHandler(ShowWriteByteSPs);
                ARD_RRS.OpenPort(mestopolozhenie.ARD_rrs_comN, 19200, Parity.None, 8, StopBits.One);
                //Thread.Sleep(100);
                //ARD_SP.SendGetAngle();
            }
            catch (Exception)
            {
                ARD_RRS.OnGetAngleCmd -= new ComARD_APU.GetAngleEventHandler(OnGetAngleReceivedSPs);
                ARD_RRS.OnSetAngleCmd -= new ComARD_APU.SetAngleEventHandler(SetAngleReceivedSPs);
                ARD_RRS.OnConnectPort -= new ComARD_APU.ConnectEventHandler(ShowConnectRRS);
                ARD_RRS.OnDisconnectPort -= new ComARD_APU.ConnectEventHandler(ShowDisconnectRRS);
            }
        }

        private void OnGetAngleReceivedSPs(object Sender, byte bAddress, short sAngle, byte bStateMotor, byte bCodeError)
        {
            if (bCodeError != 0) { /*MessageBox.Show("Adress: " + bAddress.ToString() + ". Err: " + bCodeError.ToString());*/ return; }
            if (bAddress == 0x01)//rrs1
            {
                try
                {
                    if (InvokeRequired)
                    {
                        BeginInvoke((MethodInvoker)(() => OnGetAngleReceivedSPs(Sender, bAddress, sAngle, bStateMotor, bCodeError)));
                        return;
                    }
                    mestopolozhenie.ARD1_Radant = (Int16)(sAngle / 10);
                    calculate_angle_to_north_Paint(mestopolozhenie);
                }
                catch (Exception) { ARD_RRS.ClosePort(); }
            }
            if (bAddress == 0x02)//rrs2
            {
                try
                {
                    if (InvokeRequired)
                    {
                        BeginInvoke((MethodInvoker)(() => OnGetAngleReceivedSPs(Sender, bAddress, sAngle, bStateMotor, bCodeError)));
                        return;
                    }
                    mestopolozhenie.ARD2_Radant = (Int16)(sAngle / 10);
                    calculate_angle_to_north_Paint(mestopolozhenie);
                }
                catch (Exception) { ARD_RRS.ClosePort(); }
            }
        }
        private void OnGetAngleReceivedLPA(object Sender, byte bAddress, short sAngle, byte bStateMotor, byte bCodeError)
        {
            if (bCodeError != 0) { /*MessageBox.Show("Adress: " + bAddress.ToString() + ". Err: " + bCodeError.ToString()); */return; }
            try
            {
                if (InvokeRequired)
                {
                    BeginInvoke((MethodInvoker)(() => OnGetAngleReceivedLPA(Sender, bAddress, sAngle, bStateMotor, bCodeError)));
                    return;
                }
                mestopolozhenie.ARD3_Radant = (Int16)(sAngle / 10);
                calculate_angle_to_north_Paint(mestopolozhenie);
            }
            catch (Exception) { ARD_LPA1.ClosePort(); }
        }

        private void SetAngleReceivedLPA(byte bAddress, byte bCodeError)
        {
            try
            {
                if (bCodeError != 0) { MessageBox.Show("Adress: " + bAddress.ToString() + ". Err: " + bCodeError.ToString()); }
                //else ustanovilos'
            }
            catch (Exception) { }
        }
        private void SetAngleReceivedSPs(byte bAddress, byte bCodeError)
        {
            try
            {
                if (bCodeError != 0) { MessageBox.Show("Adress: " + bAddress.ToString() + ". Err: " + bCodeError.ToString()); }
            }
            catch (Exception) { }
        }


        private void ShowReadByteARD(byte[] bData)
        {
            try
            {
                if (ledReadLPA.InvokeRequired)
                {
                    ledReadLPA.Invoke((MethodInvoker)(delegate()
                    {
                        ledReadLPA.Value = true;
                        tmReadByteARD = new System.Threading.Timer(TimeReadByteARD, null, TimePause, 0);
                    }));

                }
                else
                {
                    ledReadLPA.Value = true;
                    tmReadByteARD = new System.Threading.Timer(TimeReadByteARD, null, TimePause, 0);
                }
            }
            catch (SystemException)
            { }

        }
        private void ShowReadByteSP(byte[] bData)
        {
            try
            {
                if (ledReadSP.InvokeRequired)
                {
                    ledReadSP.Invoke((MethodInvoker)(delegate()
                    {
                        ledReadSP.Value = true;
                        tmReadByteSP = new System.Threading.Timer(TimeReadByteSP, null, TimePause, 0);
                    }));

                }
                else
                {
                    ledReadSP.Value = true;
                    tmReadByteSP = new System.Threading.Timer(TimeReadByteSP, null, TimePause, 0);
                }
            }
            catch (SystemException)
            { }
        }
        private void ShowReadBytePU(byte[] bData)
        {
            try
            {
                if (ledReadPU.InvokeRequired)
                {
                    ledReadPU.Invoke((MethodInvoker)(delegate()
                    {
                        ledReadPU.Value = true;
                        tmReadBytePU = new System.Threading.Timer(TimeReadBytePU, null, TimePause, 0);
                    }));

                }
                else
                {
                    ledReadPU.Value = true;
                    tmReadBytePU = new System.Threading.Timer(TimeReadBytePU, null, TimePause, 0);
                }
            }
            catch (SystemException)
            { }
        }
        private void ShowWriteByteARD(byte[] bData)
        {
            if (ledWriteLPA.InvokeRequired)
            {
                ledWriteLPA.Invoke((MethodInvoker)(delegate()
                {
                    ledWriteLPA.Value = true;
                    tmWriteByteARD = new System.Threading.Timer(TimeWriteByteARD, null, TimePause, 0);
                }));

            }
            else
            {
                ledWriteLPA.Value = true;
                tmWriteByteARD = new System.Threading.Timer(TimeWriteByteARD, null, TimePause, 0);
            }
        }
        private void ShowWriteBytePU(byte[] bData)
        {
            if (ledWritePU.InvokeRequired)
            {
                ledWritePU.Invoke((MethodInvoker)(delegate()
                {
                    ledWritePU.Value = true;
                    tmWriteBytePU = new System.Threading.Timer(TimeWriteBytePU, null, TimePause, 0);
                }));

            }
            else
            {
                ledWritePU.Value = true;
                tmWriteBytePU = new System.Threading.Timer(TimeWriteBytePU, null, TimePause, 0);
            }
        }
        private void ShowWriteByteSP(byte[] bData)
        {
            if (ledWriteSP.InvokeRequired)
            {
                ledWriteSP.Invoke((MethodInvoker)(delegate()
                {
                    ledWriteSP.Value = true;
                    tmWriteByteSP = new System.Threading.Timer(TimeWriteByteSP, null, TimePause, 0);
                }));

            }
            else
            {
                ledWriteSP.Value = true;
                tmWriteByteSP = new System.Threading.Timer(TimeWriteByteSP, null, TimePause, 0);
            }
        }
        private void TimeWriteByteARD(object o)
        {
            if (ledWriteLPA.InvokeRequired)
            {
                ledWriteLPA.Invoke((MethodInvoker)(delegate()
                {
                    ledWriteLPA.Value = false;
                    tmWriteByteARD.Dispose();
                }));
            }
            else
            {
                ledWriteLPA.Value = false;
                tmWriteByteARD.Dispose();
            }
        }
        private void TimeWriteBytePU(object o)
        {
            if (ledWritePU.InvokeRequired)
            {
                ledWritePU.Invoke((MethodInvoker)(delegate()
                {
                    ledWritePU.Value = false;
                    tmWriteBytePU.Dispose();
                }));
            }
            else
            {
                ledWritePU.Value = false;
                tmWriteBytePU.Dispose();
            }
        }
        private void TimeWriteByteSP(object o)
        {
            if (ledWriteSP.InvokeRequired)
            {
                ledWriteSP.Invoke((MethodInvoker)(delegate()
                {
                    ledWriteSP.Value = false;
                    tmWriteByteSP.Dispose();
                }));
            }
            else
            {
                ledWriteSP.Value = false;
                tmWriteByteSP.Dispose();
            }
        }
        private void TimeReadByteARD(object o)
        {
            if (ledReadLPA.InvokeRequired)
            {
                ledReadLPA.Invoke((MethodInvoker)(delegate()
                {
                    ledReadLPA.Value = false;
                    tmReadByteARD.Dispose();
                }));
            }
            else
            {
                ledReadLPA.Value = false;
                tmReadByteARD.Dispose();
            }
        }
        private void TimeReadBytePU(object o)
        {
            if (ledReadPU.InvokeRequired)
            {
                ledReadPU.Invoke((MethodInvoker)(delegate()
                {
                    ledReadPU.Value = false;
                    tmReadBytePU.Dispose();
                }));
            }
            else
            {
                ledReadPU.Value = false;
                tmReadBytePU.Dispose();
            }
        }
        private void TimeReadByteSP(object o)
        {
            if (ledReadSP.InvokeRequired)
            {
                ledReadSP.Invoke((MethodInvoker)(delegate()
                {
                    ledReadSP.Value = false;
                    tmReadByteSP.Dispose();
                }));
            }
            else
            {
                ledReadSP.Value = false;
                tmReadByteSP.Dispose();
            }
        }

        private void bConnectLPA_Click(object sender, EventArgs e)
        {
            if (bConnectLPA.BackColor == Color.Red)
            {
                InitConnectionARD_LPA();
            }
            else
                if (ARD_LPA1 != null)
                {
                    ARD_LPA1.SendStop(mestopolozhenie.ARD3_adress, 0);
                    ARD_LPA1.ClosePort();
                    Enabling_LPA(false);
                }
        }
        private void bConnectRRS_Click(object sender, EventArgs e)
        {
            try
            {
                if (bConnectRRS.BackColor == Color.Red)
                {
                    InitConnectionARD_RRS();
                }
                else
                    if (ARD_RRS != null)
                    {
                        ARD_RRS.SendStop(mestopolozhenie.ARD1_adress, 0);
                        ARD_RRS.SendStop(mestopolozhenie.ARD2_adress, 0);
                        Thread.Sleep(10);
                        ARD_RRS.ClosePort();
                        Enabling_RRS(false);
                    }
            }
            catch (Exception) { }
        }
        public void bConnectKmp1_Click(object sender, EventArgs e)
        {
            CountDataFromKmp1 = 0;
            if (InvokeRequired)
            {
                BeginInvoke((MethodInvoker)(() => bConnectKmp1_Click(sender, e)));
                return;
            }
            if (mestopolozhenie.compass1_IsOpen == false)
            {
                try
                {
                    Compas1.OpenPort(mestopolozhenie.compas1_comN, 9600, Parity.None, 8, StopBits.One, TimePause_compass1);
                    mestopolozhenie.compass1_IsOpen = true;
                    bConnectKmp1.BackColor = Color.Green;
                    tbK1Azimuth.ReadOnly = true;
                    Compass1Count = 0;
                }
                catch (Exception)
                {
                    tbK1Azimuth.ReadOnly = false; bConnectKmp1.BackColor = Color.Red; ledReadKmp1.Value = false; ledWriteKmp1.Value = false; mestopolozhenie.compass1_IsOpen = false;
                }
            }
            else
            {
                try
                {
                    Compas1.ClosePort();
                    mestopolozhenie.compass1_IsOpen = false;
                    tbK1Azimuth.ReadOnly = false;
                    bConnectKmp1.BackColor = Color.Red; ledReadKmp1.Value = false; ledWriteKmp1.Value = false;
                }
                catch (Exception) { tbK1Azimuth.ReadOnly = false; bConnectKmp1.BackColor = Color.Red; ledReadKmp1.Value = false; ledWriteKmp1.Value = false; mestopolozhenie.compass1_IsOpen = false; }
            }
        }
        public void bConnectKmp2_Click(object sender, EventArgs e)
        {
            CountDataFromKmp2 = 0;
            if (InvokeRequired)
            {
                BeginInvoke((MethodInvoker)(() => bConnectKmp2_Click(sender, e)));
                return;
            }
            if (mestopolozhenie.compass2_IsOpen == false)
            {
                try
                {
                    Compas2.OpenPort(mestopolozhenie.compas2_comN, 9600, Parity.None, 8, StopBits.One, TimePause_compass2);
                    mestopolozhenie.compass2_IsOpen = true;
                    bConnectKmp2.BackColor = Color.Green;
                    Compass2Count = 0;
                }
                catch (Exception) { bConnectKmp2.BackColor = Color.Red; ledReadKmp2.Value = false; ledWriteKmp2.Value = false; }
            }
            else
            {
                try
                {
                    Compas2.ClosePort();
                    mestopolozhenie.compass2_IsOpen = false;
                    bConnectKmp2.BackColor = Color.Red; ledReadKmp2.Value = false; ledWriteKmp2.Value = false;
                }
                catch { bConnectKmp2.BackColor = Color.Red; ledReadKmp2.Value = false; ledWriteKmp2.Value = false; }
            }
        }
        private void Compas_OnMSG1(int type)
        {
            try
            {
                Compass1Count++;
                switch (type)
                {
                    case 0: break; //MessageBox.Show("Усё добра прайшло(компас 1)"); break;
                    case 1: break; //MessageBox.Show("Ошибка при открытии порта(компас 1)"); break;
                    case 2: break; //MessageBox.Show("Ошибка при закрытии порта(компас 1)"); break;
                    case 3: break; //MessageBox.Show("Ошибка записи данных(компас 1)"); break;
                    case 4:
                        {
                            Compas1.ClosePort();
                            mestopolozhenie.compass1_IsOpen = false;
                            if (bConnectKmp1.InvokeRequired)
                            {
                                bConnectKmp1.Invoke((MethodInvoker)(delegate()
                                {
                                    bConnectKmp1.BackColor = Color.Red;
                                }));
                            }
                            else
                            {
                                bConnectKmp1.BackColor = Color.Red;
                            }
                            //ledReadKmp1.Value = false; ledWriteKmp1.Value = false;
                            return;
                            break; //MessageBox.Show("Ошибка чтения данных(компас 1)");
                        }
                    case 5: break; //MessageBox.Show("Ошибка декодирования ответа(компас 1)"); break;
                    case 6:
                        try
                        {
                            mestopolozhenie.compass1_azimuth = (int)Compas1._angles.head;

                            CountDataFromKmp1++;
                            if (CountDataFromKmp1 > 100) CountDataFromKmp1 = 0;
                            if (CountDataFromKmp1 - CountDataFromKmp2 > 3) { bConnectKmp2.PerformClick(); bConnectKmp2.PerformClick(); CountDataFromKmp1 = 0; CountDataFromKmp2 = 0; }
                            if (CountDataFromKmp2 - CountDataFromKmp2 > 1) { bConnectKmp1.PerformClick(); bConnectKmp1.PerformClick(); CountDataFromKmp1 = 0; CountDataFromKmp2 = 0; }

                        }
                        catch { }
                        calculate_angle_to_north_Paint(mestopolozhenie);
                        break; //MessageBox.Show("Получены значения углов(компас 1)"); break;
                    case 7: break; //MessageBox.Show("Успех при вызове функции SetDeclination(компас 1)"); break;
                    case 8: break; //MessageBox.Show("Ошибка при установке магнитного склонения(компас 1)"); break;
                    case 9: break; //MessageBox.Show("Получено значение отклонения компаса функцией ReadDeclination(компас 1)"); break;
                    case 10: break; //MessageBox.Show("Начата калибровка компаса(компас 1)"); break;
                    case 11: break; //MessageBox.Show("Получена точка калибровки(компас 1)"); break;
                    case 12: break; //MessageBox.Show("Получена последняя точка калибровки(компас 1)"); break;
                    case 13: break; //MessageBox.Show("Ошибка при старте калибровки(компас 1)"); break;
                    case 14: break; //MessageBox.Show("Неверный код при попытке остановить калибровку(компас 1)"); break;
                    case 15: break; //MessageBox.Show("результаты калибровки после функции StopCalibritaion(компас 1)"); break;
                    case 16: break; //MessageBox.Show("Ошибка при остановке калибровки(компас 1)"); break;
                    case 17: break; //MessageBox.Show("Успех при сохранении калибровки(компас 1)"); break;
                    case 18: break; //MessageBox.Show("Не удалось сохранить калибровку(компас 1)"); break;
                    case 19: break; //MessageBox.Show("Успех при вызове функции SetBaudRate(компас 1)"); break;
                    case 20: break; //MessageBox.Show("Не удалось установить BaudRate(компас 1)"); break;
                    case 21: break; //MessageBox.Show("Успех при вызове функции SetAngleOutputMode(компас 1)"); break;
                    case 22: break; //MessageBox.Show("Не удалось установить режим(компас 1)"); break;
                    case 23: break; //MessageBox.Show("Успех при вызове функции SetAngleMountingMode(компас 1)"); break;
                    case 24: break; //MessageBox.Show("Не удалось установить поляризацию компаса(компас 1)"); break;
                    case 25: break; //MessageBox.Show("Успех при вызове функции GetAngleMountingMode(компас 1)"); break;
                    case 26: break; //MessageBox.Show("Не удалось запросить поляризацию(компас 1)"); break;
                    case 27: break; //MessageBox.Show("Успех при вызове функции GetAngleOutputMode(компас 1)"); break;
                    case 28: break; //MessageBox.Show("Не удалось установить режим(компас 1)"); break;
                    default: break; //MessageBox.Show("Неопознанная ошибка(компас 1)"); break;
                }
                ledReadKmp1.Value = true;
                timer = new System.Threading.Timer(TimeStepLedReadKmp1, null, TimePause, 0);
                if (Compass1Count == 5)
                    bConnectKmp1_Click(this, null);
            }
            catch { }
        }
        private void Compas_OnMSG2(int type)
        {
            try
            {
                Compass2Count++;
                switch (type)
                {
                    case 0: break; //MessageBox.Show("Усё добра прайшло(компас 2)"); break;
                    case 1: break; //MessageBox.Show("Ошибка при открытии порта(компас 2)"); break;
                    case 2: break; //MessageBox.Show("Ошибка при закрытии порта(компас 2)"); break;
                    case 3: break; //MessageBox.Show("Ошибка записи данных(компас 2)"); break;
                    case 4:
                        {
                            Compas2.ClosePort();
                            mestopolozhenie.compass2_IsOpen = false;
                            if (bConnectKmp2.InvokeRequired)
                            {
                                bConnectKmp2.Invoke((MethodInvoker)(delegate()
                                {
                                    bConnectKmp2.BackColor = Color.Red;
                                }));
                            }
                            else
                            {
                                bConnectKmp2.BackColor = Color.Red;
                            }
                            //MessageBox.Show("Ошибка чтения данных(компас 2)"); 
                            return;
                            break;
                        }
                    case 5: break; //MessageBox.Show("Ошибка декодирования ответа(компас 2)"); break;
                    case 6: try
                        {
                            mestopolozhenie.compass2_azimuth = (int)Compas2._angles.head;
                        
                            //int.Parse(Compas1._angles.head.ToString());                    
                            //if (tbK2Azimuth.InvokeRequired)
                            //{
                            //    tbK2Azimuth.Invoke((MethodInvoker)(delegate()
                            //    {
                            //        tbK2Azimuth.Text = mestopolozhenie.compass2_final.ToString();
                            //    }));
                            //}
                            //else
                            //{
                            //    tbK2Azimuth.Text = mestopolozhenie.compass2_final.ToString();
                            //}
                            CountDataFromKmp2++;
                        }
                        catch { }
                        if (CountDataFromKmp2 > 100) CountDataFromKmp2 = 0;
                        if (CountDataFromKmp1 - CountDataFromKmp2 > 3) { bConnectKmp2.PerformClick(); bConnectKmp2.PerformClick(); CountDataFromKmp1 = 0; CountDataFromKmp2 = 0; }
                        if (CountDataFromKmp2 - CountDataFromKmp2 > 1) { bConnectKmp1.PerformClick(); bConnectKmp1.PerformClick(); CountDataFromKmp1 = 0; CountDataFromKmp2 = 0; }

                        calculate_angle_to_north_Paint(mestopolozhenie);
                        break; //MessageBox.Show("Получены значения углов(компас 2)"); break;
                    case 7: break; //MessageBox.Show("Успех при вызове функции SetDeclination(компас 2)"); break;
                    case 8: break; //MessageBox.Show("Ошибка при установке магнитного склонения(компас 2)"); break;
                    case 9: break; //MessageBox.Show("Получено значение отклонения компаса функцией ReadDeclination(компас 2)"); break;
                    case 10: break; //MessageBox.Show("Начата калибровка компаса(компас 2)"); break;
                    case 11: break; //MessageBox.Show("Получена точка калибровки(компас 2)"); break;
                    case 12: break; //MessageBox.Show("Получена последняя точка калибровки(компас 2)"); break;
                    case 13: break; //MessageBox.Show("Ошибка при старте калибровки(компас 2)"); break;
                    case 14: break; //MessageBox.Show("Неверный код при попытке остановить калибровку(компас 2)"); break;
                    case 15: break; //MessageBox.Show("результаты калибровки после функции StopCalibritaion(компас 2)"); break;
                    case 16: break; //MessageBox.Show("Ошибка при остановке калибровки(компас 2)"); break;
                    case 17: break; //MessageBox.Show("Успех при сохранении калибровки(компас 2)"); break;
                    case 18: break; //MessageBox.Show("Не удалось сохранить калибровку(компас 2)"); break;
                    case 19: break; //MessageBox.Show("Успех при вызове функции SetBaudRate(компас 1)"); break;
                    case 20: break; //MessageBox.Show("Не удалось установить BaudRate(компас 2)"); break;
                    case 21: break; //MessageBox.Show("Успех при вызове функции SetAngleOutputMode(компас 2)"); break;
                    case 22: break; //MessageBox.Show("Не удалось установить режим(компас 2)"); break;
                    case 23: break; //MessageBox.Show("Успех при вызове функции SetAngleMountingMode(компас 2)"); break;
                    case 24: break; //MessageBox.Show("Не удалось установить поляризацию компаса(компас 2)"); break;
                    case 25: break; //MessageBox.Show("Успех при вызове функции GetAngleMountingMode(компас 2)"); break;
                    case 26: break; //MessageBox.Show("Не удалось запросить поляризацию(компас 2)"); break;
                    case 27: break; //MessageBox.Show("Успех при вызове функции GetAngleOutputMode(компас 2)"); break;
                    case 28: break; //MessageBox.Show("Не удалось установить режим(компас 2)"); break;
                    default: break; //MessageBox.Show("Неопознанная ошибка(компас 2)"); break;
                }
                ledReadKmp2.Value = true;
                timer = new System.Threading.Timer(TimeStepLedReadKmp2, null, TimePause, 0);
                if (Compass1Count == 10)
                    bConnectKmp2_Click(this, null);
            }
            catch { }
        }
        private void TimeStepLedReadKmp1(object o)
        {
            if (ledReadKmp1.InvokeRequired)
            {
                ledReadKmp1.Invoke((MethodInvoker)(delegate()
                {
                    ledReadKmp1.Value = false;
                    timer.Dispose();
                }));

            }
            else
            {
                ledReadKmp1.Value = false;
                timer.Dispose();
            }

        }
        private void TimeStepLedWriteKmp1(object o)
        {
            if (ledWriteKmp1.InvokeRequired)
            {
                ledWriteKmp1.Invoke((MethodInvoker)(delegate()
                {
                    ledWriteKmp1.Value = false;
                    timer.Dispose();
                }));

            }
            else
            {
                ledWriteKmp1.Value = false;
                timer.Dispose();
            }

        }
        private void TimeStepLedReadKmp2(object o)
        {
            if (ledReadKmp2.InvokeRequired)
            {
                ledReadKmp2.Invoke((MethodInvoker)(delegate()
                {
                    ledReadKmp2.Value = false;
                    timer.Dispose();
                }));

            }
            else
            {
                ledReadKmp2.Value = false;
                timer.Dispose();
            }

        }
        private void TimeStepLedWriteKmp2(object o)
        {
            if (ledWriteKmp2.InvokeRequired)
            {
                ledWriteKmp2.Invoke((MethodInvoker)(delegate()
                {
                    ledWriteKmp2.Value = false;
                    timer.Dispose();
                }));

            }
            else
            {
                ledWriteKmp2.Value = false;
                timer.Dispose();
            }
        }
        private void TimeStepLedReadLPA(object o)
        {
            if (ledReadLPA.InvokeRequired)
            {
                ledReadLPA.Invoke((MethodInvoker)(delegate()
                {
                    ledReadLPA.Value = false;
                    timer.Dispose();
                }));
            }
            else
            {
                ledReadLPA.Value = false;
                timer.Dispose();
            }
        }
        private void TimeStepLedWriteLPA(object o)
        {
            if (ledWriteLPA.InvokeRequired)
            {
                ledWriteLPA.Invoke((MethodInvoker)(delegate()
                {
                    ledWriteLPA.Value = false;
                    timer.Dispose();
                }));
            }
            else
            {
                ledWriteLPA.Value = false;
                timer.Dispose();
            }
        }
        private void TimeStepLedReadSP(object o)
        {
            if (ledReadSP.InvokeRequired)
            {
                ledReadSP.Invoke((MethodInvoker)(delegate()
                {
                    ledReadSP.Value = false;
                    timer.Dispose();
                }));
            }
            else
            {
                ledReadSP.Value = false;
                timer.Dispose();
            }
        }
        private void TimeStepLedWriteSP(object o)
        {
            if (ledWriteSP.InvokeRequired)
            {
                ledWriteSP.Invoke((MethodInvoker)(delegate()
                {
                    ledWriteSP.Value = false;
                    timer.Dispose();
                }));
            }
            else
            {
                ledWriteSP.Value = false;
                timer.Dispose();
            }
        }
        private void TimeStepLedReadPU(object o)
        {
            if (ledReadPU.InvokeRequired)
            {
                ledReadPU.Invoke((MethodInvoker)(delegate()
                {
                    ledReadPU.Value = false;
                    timer.Dispose();
                }));
            }
            else
            {
                ledReadPU.Value = false;
                timer.Dispose();
            }
        }
        private void TimeStepLedWritePU(object o)
        {
            if (ledWritePU.InvokeRequired)
            {
                ledWritePU.Invoke((MethodInvoker)(delegate()
                {
                    ledWritePU.Value = false;
                    timer.Dispose();
                }));
            }
            else
            {
                ledWritePU.Value = false;
                timer.Dispose();
            }
        }
        private void TimeStepLedReadGPS(object o)
        {
            if (ledReadGPS1.InvokeRequired)
            {
                ledReadGPS1.Invoke((MethodInvoker)(delegate()
                {
                    ledReadGPS1.Value = false;
                    timer.Dispose();
                }));
            }
            else
            {
                ledReadGPS1.Value = false;
                timer.Dispose();
            }
        }
        private void TimeStepLedWriteGPS(object o)
        {
            if (ledWriteGPS.InvokeRequired)
            {
                ledWriteGPS.Invoke((MethodInvoker)(delegate()
                {
                    ledWriteGPS.Value = false;
                    timer.Dispose();
                }));
            }
            else
            {
                ledWriteGPS.Value = false;
                timer.Dispose();
            }
        }
        private void lLPA2_DoubleClick(object sender, EventArgs e)
        {
            if (colorDialog1.ShowDialog() == DialogResult.OK)
            {
                colors.LPA = colorDialog1.Color;
                change_colors();
            }
        }
        private void lPU_DoubleClick(object sender, EventArgs e)
        {
            if (colorDialog1.ShowDialog() == DialogResult.OK)
            {
                colors.PU = colorDialog1.Color;
                change_colors();
            }
        }
        private void lSP2_DoubleClick(object sender, EventArgs e)
        {
            if (colorDialog1.ShowDialog() == DialogResult.OK)
            {
                colors.SP = colorDialog1.Color;
                change_colors();
            }
        }
        private void lLPA1_3_DoubleClick(object sender, EventArgs e)
        {
            if (colorDialog1.ShowDialog() == DialogResult.OK)
            {
                colors.LPA1_3 = colorDialog1.Color;
                change_colors();
            }
        }
        private void lLPA2_4_DoubleClick(object sender, EventArgs e)
        {
            if (colorDialog1.ShowDialog() == DialogResult.OK)
            {
                colors.LPA2_4 = colorDialog1.Color;
                change_colors();
            }
        }

        private void WndProject_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (OnCloseFormOrient != null) OnCloseFormOrient();
            e.Cancel = true;
            Hide();
            
        }

        private void ClearTextBoxGNSS()
        {
            tbDate.Text = "";
            tbTime.Text = "";
            tbLat.Text = "";
            tbLon.Text = "";
            tbAlt.Text = "";
            tbCountSat.Text = "";
        }

        private void cbSatSyst_SelectedIndexChanged(object sender, EventArgs e)
        {
            ClearTextBoxGNSS();

            switch (cbSatSyst.SelectedIndex)
            {
                case 0: // GPS
                    if (m_gps_device_holder != null)
                        m_gps_device_holder.SendRequest(gpsEncoder.QueryNavigationSystem(GPSEncoder.NavigationSystemType.GP));
                    break;

                case 1: // GLONASS+GPS
                    if (m_gps_device_holder != null)
                        m_gps_device_holder.SendRequest(gpsEncoder.QueryNavigationSystem(GPSEncoder.NavigationSystemType.GN));
                    break;

                case 2: // GLONASS
                    if (m_gps_device_holder != null)
                        m_gps_device_holder.SendRequest(gpsEncoder.QueryNavigationSystem(GPSEncoder.NavigationSystemType.GL));
                    break;
            }
        }
        private void tbK1Azimuth_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= '0') && (e.KeyChar <= '9') || (e.KeyChar == '\b') || (e.KeyChar == '-'))  // цифра, Backspace
            {
                return;
            }
            e.Handled = true;
        }
        private void tbK1Azimuth_Validated(object sender, EventArgs e)
        {
            int ugol = 0;
            int.TryParse(tbK1Azimuth.Text.ToString(), out  ugol);
            if (ugol > 359) tbK1Azimuth.Text = "359";
            if (ugol < 0) tbK1Azimuth.Text = "0";

        }
        private void bTransportRRS1_Click(object sender, EventArgs e)
        {
            ARD_RRS.SendSetAngle(mestopolozhenie.ARD1_adress, 1, 0x00, 0);
        }
        private void bTransportRRS2_Click(object sender, EventArgs e)
        {
            ARD_RRS.SendSetAngle(mestopolozhenie.ARD2_adress, 1, 0x00, 0);
        }
        private void bTransportLPA_Click(object sender, EventArgs e)
        {
            ARD_RRS.SendSetAngle(mestopolozhenie.ARD3_adress, 1, 0x90, 0);
        }

        private void bSPLeft_Click(object sender, EventArgs e)
        {
            ARD_RRS.SendStepRotate(mestopolozhenie.ARD1_adress, 0, 1, 1);
        }

        private void bPULeft_Click(object sender, EventArgs e)
        {
            ARD_RRS.SendStepRotate(mestopolozhenie.ARD2_adress, 0, 1, 1);
        }

        private void bLPALeft_Click(object sender, EventArgs e)
        {
            ARD_LPA1.SendStepRotate(mestopolozhenie.ARD3_adress, 0, 1, 1);

        }

        private void bSPRight_Click(object sender, EventArgs e)
        {
            ARD_RRS.SendStepRotate(mestopolozhenie.ARD1_adress, 1, 1, 1);
        }

        private void bPURight_Click(object sender, EventArgs e)
        {
            ARD_RRS.SendStepRotate(mestopolozhenie.ARD2_adress, 1, 1, 1);
        }

        private void bLPARight_Click(object sender, EventArgs e)
        {
            ARD_LPA1.SendStepRotate(mestopolozhenie.ARD3_adress, 1, 1, 1);

        }

        private void InitItems()
        {
            try
            {
                if (rbWGS84.Checked)
                {
                    cbCoordinateSystem.Items.Clear();
                    cbCoordinateSystem.Items.Add("DD.DDDDDD");
                    cbCoordinateSystem.Items.Add("DD MM.MMMMMM");
                    cbCoordinateSystem.Items.Add("DD MM SS.SSSSSS");

                    //cbCoordinateSystem.Items.Clear();
                    //cbCoordinateSystem.Items[0] = ("DD.DDDDDD");
                    //cbCoordinateSystem.Items. = ("DD MM.MMMMMM");
                    //cbCoordinateSystem.Items.Add("DD MM SS.SSSSSS");

                    cbCoordinateSystem.SelectedIndex = 0;

                    ClearTextBoxGNSS();
                }
                else if (rbCK42.Checked)
                {
                    cbCoordinateSystem.Items.Clear();
                    cbCoordinateSystem.Items.Add("DD.DDDDDD");
                    cbCoordinateSystem.Items.Add("DD MM.MMMMMM");
                    cbCoordinateSystem.Items.Add("DD MM SS.SSSSSS");
                    //if (cbCoordinateSystem.Items[3] != null)
                    cbCoordinateSystem.Items.Add("Метры");

                    cbCoordinateSystem.SelectedIndex = 0;

                    ClearTextBoxGNSS();
                }
            }
            catch { }
        }

        private void rbWGS84_CheckedChanged(object sender, EventArgs e)
        {
            InitItems();


        }

        private void rbCK42_CheckedChanged(object sender, EventArgs e)
        {
            InitItems();
            if (rbCK42.Checked == true)
            {
                //this.variableCommon = variableCommon;
                if (variableCommon.Language.Equals(0))
                {
                    cbCoordinateSystem.Items[3] = "Метры";
                }
                if (variableCommon.Language.Equals(1))
                {
                    cbCoordinateSystem.Items[3] = "Metre";
                }
                if (variableCommon.Language.Equals(2))
                {
                    cbCoordinateSystem.Items[3] = "Ölçü";
                }
            }
        }

        private void bConnectRRS2_Click(object sender, EventArgs e)
        {
            bConnectRRS.PerformClick();
        }

        private void bSetLPA2_Click(object sender, EventArgs e)
        {

        }

        private void bStopLPA2_Click(object sender, EventArgs e)
        {

        }

        private void bSetSP2_Click(object sender, EventArgs e)
        {
            try
            {
                double angl;
                double.TryParse(nudSP2.Value.ToString(), out angl);
                mestopolozhenie.ARD1_need_azimuth = (Int16)(Math.Round(angl) - mestopolozhenie.ARD2_azimuth);//-mestopolozhenie.compass1_final
                angl = mestopolozhenie.ARD1_need_azimuth;
                if (angl < 0) angl += 360;
                if (((Int16)angl > -1) && ((Int16)angl < 355))
                    ARD_RRS.SendSetAngle(mestopolozhenie.ARD1_adress, 1, (Int16)angl, 0);
            }
            catch (Exception) { /*MessageBox.Show("Не  могу повернуть");*/  }
        }
        private void bStopSP2_Click(object sender, EventArgs e)
        {
            try
            {
                ARD_RRS.SendStop(mestopolozhenie.ARD1_adress, 0x00); ;
            }
            catch (Exception) { }// ARDSP.ClosePort(); mestopolozhenie.ARD3_IsOpen = false; bConnectSP.BackColor = Color.Red; ledReadSP.Value = false; ledWriteSP.Value = false; }
            //ledWriteSP.Value = true;
            //timer = new System.Threading.Timer(TimeStepLedWriteSP, null, TimePause, 0);
        }

        private void nudSP2_ValueChanged(object sender, EventArgs e)
        {

        }


        private void nudLPA2_ValueChanged(object sender, EventArgs e)
        {

        }

        private void OrientationAZer_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                if (OnCloseFormOrient != null) OnCloseFormOrient();
                e.Cancel = true;
                Hide();

            }
            catch { } 
            try
            {
                ARD_LPA1.ClosePort();
                mestopolozhenie.ARD1_IsOpen = false;
                ARD_RRS.ClosePort();
                mestopolozhenie.ARD_IsOpen = false;
                Compas1.ClosePort();
                mestopolozhenie.compass1_IsOpen = false;
                Compas2.ClosePort();
                mestopolozhenie.compass2_IsOpen = false;
                bConnectKmp1.BackColor = Color.Red;
                bConnectKmp2.BackColor = Color.Red;
                bConnectLPA.BackColor = Color.Red;
                bConnectRRS.BackColor = Color.Red;
                bConnectRRS2.BackColor = Color.Red;
            }
            catch (Exception) { /*MessageBox.Show("Не удалось закрыть СОМ-порт"); */}

        }

        private void OrientationAZer_Load(object sender, EventArgs e)
        {
            ReTranslate();
        }

        private void ReTranslate()
        {

            if (variableCommon.Language.Equals(0))
            {
                this.Text = "Местоположение и направление антенн";

                bSetLPA2.Text	="Установить";
                bSetPU.Text	="Установить";
                bSetSP2.Text	="Установить";
                bStopLPA2.Text	="Остановить";
                bStopPU.Text	="Остановить";
                bStopSP2.Text	="Остановить";
                bTransportLPA.Text	="АПУ ЛПА";
                bTransportRRS1.Text	="АПУ СП";
                bTransportRRS2.Text	="АПУ ПУ";
                cbSatSyst.Items[0]	    ="GPS";
                cbSatSyst.Items[1]	="GPS/ГЛОНАСС";
                cbSatSyst.Items[2] = "ГЛОНАСС";
                groupBox1.Text	="Компас РР";
                groupBox2.Text	="Компас ПА";
                groupBox3.Text	="ПУс";
                groupBox4.Text	="Местоположение";
                groupBox5.Text	="Спутниковая система";
                groupBox6.Text	="Система координат";
                groupBox7.Text	="Транспортное положение";
                label1.Text	="Высота, м";
                label11.Text	="ЛПА";
                label16.Text	="ПУ";
                label17.Text	="СП";
                label18.Text	="КмпРР";
                label19.Text	="КмпПА";
                lCountSat.Text	="Кол. спутн.";
                lDate.Text	="Дата";
                lGPS.Text	="GNSS";
                lK1Azimuth.Text	="Азимут, °";
                lK2Azimuth.Text	="Азимут, °";
                lLAt.Text	="Широта";
                lLon.Text	="Долгота";
                lLPA1_3.Text	="ЛПА (1,3)";
                lLPA2.Text	="ЛПА";
                lLPA2_4.Text	="ЛПА (2,4)";
                lPU.Text	="ПУ";
                lSP2.Text	="СП";
                lTime.Text	="Время";
                rbCK42.Text	="СК 42";
                rbWGS84.Text = "WGS 84";
            }
            if (variableCommon.Language.Equals(1))
            {
                //this.Text = "Aircrafts";
            }
            if (variableCommon.Language.Equals(2))
            {
                this.Text = "Antenanın yerləşməsi və istiqaməti";

                bSetLPA2.Text = "Quraşdırmaq";
                bSetPU.Text = "Quraşdırmaq";
                bSetSP2.Text = "Quraşdırmaq";
                bStopLPA2.Text = "Dayandırmaq";
                bStopPU.Text = "Dayandırmaq";
                bStopSP2.Text = "Dayandırmaq";
                bTransportLPA.Text = "DFQ LPA";
                bTransportRRS1.Text = "DFQ MS";
                bTransportRRS2.Text = "DFQ İM";
                cbSatSyst.Items[0] = "GPS";
                cbSatSyst.Items[1] = "GPS/GLONASS";
                cbSatSyst.Items[2] = "GLONASS";
                groupBox1.Text = "RK kompası";
                groupBox2.Text = "ÇA kompası";
                groupBox3.Text = "FQ";
                groupBox4.Text = "Coğrafi mövqey";
                groupBox5.Text = "Peyk sistemi";
                groupBox6.Text = "Koordinatlar sistemi";
                groupBox7.Text = "Yürüş vəziyyəti";
                label1.Text = "Hündürlük, m";
                label11.Text = "LPA";
                label16.Text = "İM";
                label17.Text = "MS";
                label18.Text = "RKkomp.";
                label19.Text = "ÇAkomp.";
                lCountSat.Text = "Peyklərin sayı";
                lDate.Text = "Tarix";
                lGPS.Text = "GNSS";
                lK1Azimuth.Text = "Azimut, °";
                lK2Azimuth.Text = "Azimut, °";
                lLAt.Text = "Enlik";
                lLon.Text = "Uzaqlıq";
                lLPA1_3.Text = "LPA (1,3)";
                lLPA2.Text = "LPA";
                lLPA2_4.Text = "LPA (2,4)";
                lPU.Text = "İM";
                lSP2.Text = "MS";
                lTime.Text = "Vaxt";
                rbCK42.Text = "SK 42";
                rbWGS84.Text = "WGS 84";
            }
        }

        void VariableCommon_OnChangeCommonLanguage()
        {
            ReTranslate();
        }
        
    }
}


