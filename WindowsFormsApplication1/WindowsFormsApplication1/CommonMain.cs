﻿using System;
using System.Windows.Forms;
using VariableDynamic;
using VariableStatic;
namespace WndProject
{
    partial class MainWnd
    {
        string strAddressLanguage = "";
        string strJammerLanguage = "";
        string strRoleLanguage = "";
        string strOperatorLanguage = "";

       

        VariableConnection variableConnection = new VariableConnection();
        VariableCommon variableCommon = new VariableCommon();

        VariableWork variableWork = new VariableWork();
        VariableIntellegence variableIntellegence = new VariableIntellegence();
        VariableSuppression variableSuppression = new VariableSuppression();

        private void InitStartParamCommon()
        {
            InitStringLabelLanguage(variableCommon.Language);

            InitStringRoleLanguage(variableCommon.Language, variableCommon.Role);
            //InitChanMess();
            ShowVariableStatic();
            //InitIExGRZ_BRZ();
            //initServerHS();
            //InitASP();
            InitTypeStation(variableCommon.TypeStation);
 
        }

        private void InitTypeStation(byte TypeStation)
        {
            switch(TypeStation)
            {
                case 0:
                case 2:
                    panel31.Visible = false;
                    break;
                case 1:
                    panel31.Visible = true;
                    break;
            }
        }

        private void InitStringLabelLanguage(byte bLanguage)
        {
            switch (bLanguage)
            {
                case 0:
                    strAddressLanguage = "Адрес ";
                    strJammerLanguage = "СП ";
                    strOperatorLanguage = "АРМ ";

                    break;

                case 1:
                    strAddressLanguage = "Address ";
                    strJammerLanguage = "JMR ";
                    strOperatorLanguage = "AWP ";
                    break;

                case 2:
                    strAddressLanguage = "Ünvan ";
                    strJammerLanguage = "MS ";
                    strOperatorLanguage = "AIY ";
                    break;

                default:
                    break;

            }
 
        }

        private void InitStringRoleLanguage(byte bLanguage, byte bRole)
        {
            switch (bLanguage)
            {
                case 0:
                    switch (bRole)
                    {
                        case 0:
                            strRoleLanguage = "Автономная";

                            break;

                        case 1:
                            strRoleLanguage = "Ведущая";
                            break;

                        case 2:
                            strRoleLanguage = "Ведомая";
                            break;

                        default:
                            strRoleLanguage = "";
                            break;

                    }
                    break;

                case 1:
                    switch (bRole)
                    {
                        case 0:
                            strRoleLanguage = "Independent";
                            break;

                        case 1:
                            strRoleLanguage = "Leading";
                            break;

                        case 2:
                            strRoleLanguage = "Slave";
                            break;

                        default:
                            strRoleLanguage = "";
                            break;

                    }
                    break;

                case 2:
                    switch (bRole)
                    {
                        case 0:
                            strRoleLanguage = "Avtonom";
                            break;

                        case 1:
                            strRoleLanguage = "İdarə edən";
                            break;

                        case 2:
                            strRoleLanguage = "İdarə olunan";
                            break;

                        default:
                            strRoleLanguage = "";
                            break;

                    }
                    break;

                default:
                    break;

            }

        }

        

        private void ShowVariableStatic()
        {
            ShowInfoLabel(lOwnNumber, strJammerLanguage + variableCommon.OwnNumber.ToString());
            ShowInfoLabel(lOwnRole, strRoleLanguage);
            ShowInfoLabel(lOwnAddressPC, strAddressLanguage + variableCommon.OwnAddressPC.ToString());
            ShowInfoLabel(lOperator, strOperatorLanguage + (variableCommon.Operator + 1).ToString());

        }


        private void ShowInfoLabel(Label label, string strInfo)
        {
            if (label.InvokeRequired)
            {
                label.Invoke((MethodInvoker)(delegate()
                {
                    label.Text = strInfo;
                }));

            }
            else
            {
                label.Text = strInfo;
            }

        }
        void ChangeOperatorForComponents()
        {
            try
            {
                Invoke((MethodInvoker)(() => button15.Visible = variableCommon.butCompass));
                Invoke((MethodInvoker)(() => button17.Visible = variableCommon.butFPS));
                Invoke((MethodInvoker)(() => buttonConnectFPS.Enabled = variableCommon.butFPS));
                Invoke((MethodInvoker)(() => bModeS.Visible = variableCommon.bModeS));
                Invoke((MethodInvoker)(() => FogNut.Visible = variableCommon.FogNut));
                Invoke((MethodInvoker)(() => pADSB.Visible = variableCommon.pADSB));
                Invoke((MethodInvoker)(() => bMap.Visible = variableCommon.Map));
                Invoke((MethodInvoker)(() => butServerAsp.Enabled = variableCommon.EnabledAsp));
                Invoke((MethodInvoker)(() => butClientAsp.Enabled = variableCommon.EnabledAsp));
                //bMap
            }
            catch { }
        }

        void ChangeOperator()
        {
            try
            {
                ShowInfoLabel(lOperator, strOperatorLanguage + (variableCommon.Operator + 1).ToString());
                ChangeOwnRole();
            }
            catch { }
        }

        void ChangeOwnRole()
        {
            try
            {
                InitStringRoleLanguage(variableCommon.Language, variableCommon.Role);
                ShowInfoLabel(lOwnRole, strRoleLanguage);

                Invoke((MethodInvoker)(() => bRegimeStop.Enabled = variableCommon.bRegimeStop));
                Invoke((MethodInvoker)(() => bRegimeRecon.Enabled = variableCommon.bRegimeRecon));
                Invoke((MethodInvoker)(() => bRegimeSuppr.Enabled = variableCommon.bRegimeSuppr));
                Invoke((MethodInvoker)(() => butClientAsp.Visible = variableCommon.butClientAsp));
                Invoke((MethodInvoker)(() => pASP.Visible = variableCommon.pASP));
                Invoke((MethodInvoker)(() => pPU.Visible = variableCommon.pPU));
                if (variableCommon.pPU == false)
                    ChangeTypeConnectionPC();
                ChangeTypeConnectionASP();
            }
            catch { }
        }

        void ChangeAddressPC()
        {
            ShowInfoLabel(lOwnAddressPC, strAddressLanguage + variableCommon.OwnAddressPC.ToString());
        }

        void ChangeOwnNumber()
        {
            ShowInfoLabel(lOwnNumber, strJammerLanguage + variableCommon.OwnNumber.ToString()); 
        }

        private void SetLatLonToCoordsGNSS()
        {
            try
            {
                USR_DLL.TCoordsGNSS tempOwn = new USR_DLL.TCoordsGNSS();
                tempOwn.Lat = Convert.ToDouble(variableCommon.OwnLatitude);
                tempOwn.Lon = Convert.ToDouble(variableCommon.OwnLongitude);

                if (tempOwn.Lat >= 0)
                    tempOwn.signLat = 0;
                else tempOwn.signLat = 1;

                if (tempOwn.Lon >= 0)
                    tempOwn.signLon = 0;
                else tempOwn.signLon = 1;

                variableWork.CoordsGNSS[0] = tempOwn;
            }
            catch { }

            try
            {
                USR_DLL.TCoordsGNSS tempLinked = new USR_DLL.TCoordsGNSS();
                tempLinked.Lat = Convert.ToDouble(variableCommon.JammerLinkedLatitude);
                tempLinked.Lon = Convert.ToDouble(variableCommon.JammerLinkedLongitude);

                if (tempLinked.Lat >= 0)
                    tempLinked.signLat = 0;
                else tempLinked.signLat = 1;

                if (tempLinked.Lon >= 0)
                    tempLinked.signLon = 0;
                else tempLinked.signLon = 1;

                variableWork.CoordsGNSS[1] = tempLinked;
            }
            catch { }

        }

        async void ChangeTypeConnectionASP()
        {
            if (butServerAsp.BackColor == System.Drawing.Color.Green)
            {
                disconnectIsNeeded = false;
                var answer = await VariableWork.aWPtoBearingDSPprotocolNew.DisconnectFromMasterSlaveStation();
                if (answer != null)
                    if (answer.Header.ErrorCode == 0)
                    {
                        butServerAsp.BackColor = System.Drawing.Color.Red;
                        VariableWork.aWPtoBearingDSPprotocolNew.MasterSlaveSendTextMessageUpdate -= aWPtoBearingDSPprotocolNew_MasterSlaveSendTextMessageUpdate;
                    }
            }
            if (variableConnection.ASPType == 3)
                Invoke((MethodInvoker)(() => butClientAsp.Visible = false));
            else
                Invoke((MethodInvoker)(() => butClientAsp.Visible = (variableCommon.Role == 1) ? true : false));
        }

        void NameStation()
        {
            switch (variableCommon.TypeStation)
            {
                case 0:
                case 2:
                    this.Text = (new VariablePU()).Bel; //"Р-934УМ";
                    break;
                case 1:
                    this.Text = (new VariablePU()).Azer; //"Гроза-6 Апп1";
                    break;
                default:
                    this.Text = "Error!";
                    break;
                /*
            case 2:
                this.Text = "Гроза-6 Апп2";
                break;
            case 3:
                this.Text = "Пурга";
                break;
            case 4:
                this.Text = "Р-325Б";
                break;
            case 5:
                this.Text = "Гриф";
                break;
            case 6:
                this.Text = "Журавль";
                break;
                */
            }
        }
        void ChangeTypeConnectionPC()
        {
            switch (variableCommon.TypeStation)
            {
                case 0:
                case 2:
                    if (ClientPu_Bel != null)
                    {
                        ClientPu_Bel.ClosePort();
                        btPU.BackColor = System.Drawing.Color.Red;
                        ClientPu_Bel = null;
                    }
                    break;
                case 1:
                    if (ClientPu != null)
                    {
                        ClientPu.Disconnect();
                        ClientPu = null;
                        btPU.BackColor = System.Drawing.Color.Red;
                    }
                    break;
            }
        }

        void ChangeTypeConnectionHS()
        {
            if (ServerHS != null)
            {
                ServerHS.DestroyServer();
                ServerHS = null;
            }
        }


        void ChangeStringLabelLanguage()
        {
            InitStringLabelLanguage(variableCommon.Language);
            ChangeAddressPC();
            ChangeOwnNumber();
            ChangeOwnRole();
            ChangeOperator();

        }
    }
}