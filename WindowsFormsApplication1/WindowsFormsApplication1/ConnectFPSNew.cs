﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using NoiseShaper;
using System.Windows.Forms;
//using VariableDynamic;

namespace WndProject
{
    partial class MainWnd
    {
        FormForNoiseShaper formForNoiseShaper;
        private void button17_Click(object sender, EventArgs e)
        {
            if (formForNoiseShaper == null || formForNoiseShaper.IsDisposed)
            {
                formForNoiseShaper = new FormForNoiseShaper();
                formForNoiseShaper.Show();
            }
            else
            {
                formForNoiseShaper.Show();
            }
        }
        private void ClickToConnectFPS(object sender, EventArgs e)
        {
            try
            {
                if (formForNoiseShaper == null || formForNoiseShaper.IsDisposed)
                {
                    formForNoiseShaper = new FormForNoiseShaper();
                    VariableDynamic.VariableWork.OnChangeRegime += ReactOnChangeMode;
                }
                if (buttonConnectFPS.BackColor == Color.Red)
                {
                    if (variableWork.Regime == 0)
                    {
                        if (formForNoiseShaper.ConnectFromMainForm())
                            buttonConnectFPS.BackColor = Color.Green;
                    }
                    else
                        MessageBox.Show("Подключение к ФПС возможно только в режиме Подготовка");
                }
                else
                {
                    buttonConnectFPS.BackColor = Color.Red;
                    formForNoiseShaper.DisconnectFromMainForm();
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Не удается подключиться к ФПС");
            }
        }
        private void ReactOnChangeMode()
        {
            if (variableWork.Regime == 1 && buttonConnectFPS.BackColor == Color.Green)
            {
                buttonConnectFPS.BackColor = Color.Red;
                formForNoiseShaper.DisconnectFromMainForm();
            }
        }
    }
}
