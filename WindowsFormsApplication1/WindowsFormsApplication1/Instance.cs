using System.Diagnostics;
using System.IO;
using System.Reflection;

namespace WndProject
{
    public static class Instance
    {
        public static bool HasRunningCopy
        {
            get { return RunningInstance() != null; }
        }

        public static Process RunningInstance()
        {
            var current = Process.GetCurrentProcess();
            var processes = Process.GetProcessesByName(current.ProcessName);

            foreach (var process in processes)
            {
                //Ignore current process
                if (process.Id != current.Id)
                {
                    // checking whether this process is running from the current file
                    if (Path.GetFullPath(Assembly.GetExecutingAssembly().Location) == Path.GetFullPath(current.MainModule.FileName))
                    {
                        return process;
                    }
                }
            }
            return null;
        }


    }
}
