﻿namespace WndProject
{
    partial class SpecialFrequencies
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SpecialFrequencies));
            this.tables_SpecialFrequencies = new Tables_SpecialFrequencies.tables_SpecialFrequencies();
            this.SuspendLayout();
            // 
            // tables_SpecialFrequencies
            // 
            this.tables_SpecialFrequencies.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tables_SpecialFrequencies.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tables_SpecialFrequencies.Location = new System.Drawing.Point(0, 0);
            this.tables_SpecialFrequencies.Name = "tables_SpecialFrequencies";
            this.tables_SpecialFrequencies.Size = new System.Drawing.Size(494, 259);
            this.tables_SpecialFrequencies.TabIndex = 0;
            // 
            // SpecialFrequencies
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(494, 259);
            this.Controls.Add(this.tables_SpecialFrequencies);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SpecialFrequencies";
            this.Text = "Специальные частоты";
            this.TopMost = true;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SpecialFrequencies_FormClosing);
            this.Load += new System.EventHandler(this.SpecialFrequencies_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private Tables_SpecialFrequencies.tables_SpecialFrequencies tables_SpecialFrequencies;
    }
}