﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VariableDynamic;

namespace WndProject
{
    public partial class MainWnd
    {
        private void SetViewWindowForRole()
        {
            table_IRI_FRCh_RP.SetViewWindowForRole();
            table_IRI_PPRCh_RP.SetViewWindowForRole();
        }

        private void LoadTablesFromDB()
        {
            table_IRI_FRCh_CR.LoadTableIRI_FRCh_CRfromDB();
            table_IRI_FRCh_RP.LoadTableIRI_FRCh_RPfromDB();
            table_IRI_PPRCh.LoadTableIRI_PPRChfromDB();
            table_IRI_PPRCh_RP.LoadTableIRI_PPRCh_RPfromDB();

            table_IRI_FRCh_RP.InitParamTranslateFRChRP();
            table_IRI_PPRCh_RP.InitParamTranslatePPRChRP();
        }

        private void ChangeControlsLanguage()
        {
            table_IRI_FRCh.ChangeControlLanguage(variableCommon.Language);
            table_IRI_FRCh_CR.ChangeControlLanguage(variableCommon.Language);
            table_IRI_FRCh_RP.ChangeControlLanguage(variableCommon.Language);
            table_IRI_PPRCh.ChangeControlLanguage(variableCommon.Language);
            table_IRI_PPRCh_RP.ChangeControlLanguage(variableCommon.Language);
        }

        private void ChangeMessagesLanguage()
        {
            myDGV.ChangeMessageLanguage(variableCommon.Language);
        }

    }
}