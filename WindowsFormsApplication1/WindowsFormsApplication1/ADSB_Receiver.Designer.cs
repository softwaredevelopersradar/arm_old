﻿namespace WndProject
{
    partial class ADSB_Receiver
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ADSB_Receiver));
            this.table_ADSB_Receiver = new Table_ADSB_Receiver.table_ADSB_Receiver();
            this.SuspendLayout();
            // 
            // table_ADSB_Receiver
            // 
            this.table_ADSB_Receiver.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.table_ADSB_Receiver.Location = new System.Drawing.Point(2, 2);
            this.table_ADSB_Receiver.Name = "table_ADSB_Receiver";
            this.table_ADSB_Receiver.Size = new System.Drawing.Size(404, 607);
            this.table_ADSB_Receiver.TabIndex = 88;
            // 
            // ADSB_Receiver
            // 
            this.ClientSize = new System.Drawing.Size(408, 612);
            this.Controls.Add(this.table_ADSB_Receiver);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ADSB_Receiver";
            this.Text = "Воздушные суда";
            this.TopMost = true;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ADSB_Receiver_FormClosing);
            this.Load += new System.EventHandler(this.ADSB_Receiver_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private Table_ADSB_Receiver.table_ADSB_Receiver table_ADSB_Receiver;

       
    }
}