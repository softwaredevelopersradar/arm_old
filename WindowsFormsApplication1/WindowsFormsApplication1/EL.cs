﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using NoiseShaper;
using USR_DLL;
using VariableDynamic;
using VariableStatic;

namespace WndProject
{
    public partial class EL : Form
    {
        //событие на закрытие
        public delegate void WithoutParamsEventHandler();//делегат обработки события получения кодограммы
        public event WithoutParamsEventHandler CloseForm;// событие получения команды
        public virtual void OnWithoutParamsEvent(WithoutParamsEventHandler SomeEvent)
        {
            if (SomeEvent != null)// если кому-то надо, т.е. я не приписываю эти функции
                SomeEvent();//Raise the event
        }
        Dictionary<byte, string> Cmds = new Dictionary<byte, string>
        {
            {1,"cостояние литер(ы)"},
            {2, "тип нагрузки"},
            {5, "параметры ИРИ ФРЧ и включ излучение"},
            {6, "напряжение литер(ы)"},
            {7, "мощность литер(ы)"},
            {8, "ток литер(ы)"},
            {9, "температура литер(ы)"},
            {10, "выключить излучение"},
            {11, "установить параметры РП для ИРИ ППРЧ"},
            {12, "выход из режима РП-ППРЧ"},
            {13, "состояние ФПС"},
            {14, "установить параметры ИРИ ППРЧ для измерения длительности"},
            {15, "сброс ошибок"},
            {16, "Запрос готовности приема речеподобной помехи для ИРИ ФРЧ"},
            {17, "Установить параметры речеподобной помехи для ИРИ ФРЧ и включить"},
            {18, "Установить параметры РП для ИРИ ППРЧ (н.с.)"}
        };
        Dictionary<byte, string> FPSStates = new Dictionary<byte, string>
        {
            {0, "П"},
            {1, "ФРЧ"},
            {2, "ППРЧ"},
            {3, "РчП"},
            {4, "Изм. дли-ти ППРЧ"}
        };
        Dictionary<string, Tuple<Font, Color>> AllStylesStr = new Dictionary<string, Tuple<Font, Color>> // словарь для шрифтов
        {
            {"SendArray", Tuple.Create<Font, Color>(new Font("Times New Roman", 9, FontStyle.Italic), Color.Brown)},
            {"ReceiveArray", Tuple.Create<Font, Color>(new Font("Times New Roman", 9, FontStyle.Italic), Color.DarkBlue)},
            {"SendCmd", Tuple.Create<Font, Color>(new Font("Times New Roman", 9, FontStyle.Italic), Color.Coral)},
            {"ReceiveCmd", Tuple.Create<Font, Color>(new Font("Times New Roman", 9, FontStyle.Italic), Color.Blue)},
            {"OrdinaryText", Tuple.Create<Font, Color>(new Font("Times New Roman", 9), Color.Black)},
            {"MessageForUser", Tuple.Create<Font, Color>(new Font("Times New Roman", 9), Color.CadetBlue)}
       };
        VariableStatic.VariableCommon varCommon = new VariableStatic.VariableCommon();
        //для ФПРЧ
        TDurationParamFWS dpfwsAll;
        DecordingFWS dfws;
        VariableConnection vcForNSnotStatic;
        private static Semaphore sForRepository;
        //Расшифровки ошибок
        int iCountErrorBytes;
        DecodingAllTypesError DecodAllError;
        bool bHex;
        public EL()
        {
            InitializeComponent();
            this.FormBorderStyle =FormBorderStyle. Fixed3D;//зафиксировали
            vcForNSnotStatic=new VariableConnection();
            iCountErrorBytes = vcForNSnotStatic.CountErrBytes == 1 ? 2 : 3;// проверить
            DecodAllError = new DecodingAllTypesError(iCountErrorBytes);
            bHex = false;
            dfws = new DecordingFWS();
            sForRepository = new Semaphore(1, 1);
            //Событие смены языка
            VariableStatic.VariableCommon.OnChangeCommonLanguage += new VariableStatic.VariableCommon.ChangeCommonEventHandler(VariableCommon_OnChangeCommonLanguage);
        }
        //общая функция
        void CutePrintStr(Tuple<Font, Color> StyleStr, String str, bool NewLine = true)
        {
            rtbExchangeLog.SelectionFont = StyleStr.Item1;
            rtbExchangeLog.SelectionColor = StyleStr.Item2;
            rtbExchangeLog.AppendText(NewLine ? str + "\r\n" : str);
        }
        //
        void PrintForArrays(byte[] bArray, bool IsSend = true)
        {
            if (bHex)
            {
                CutePrintStr(IsSend ? AllStylesStr["SendArray"] : AllStylesStr["ReceiveArray"], IsSend ? "Запрос в виде массиса" : "Ответ на запрос в виде массиса");
                String ForPrint = BitConverter.ToString(bArray).Replace('-', ' ') + "\r\n";
                CutePrintStr(AllStylesStr["OrdinaryText"], ForPrint);
            }
        }
        public void ReactOnSendArray(byte[] bArray)
        {
            PrintForArrays(bArray);
        }
        public void ReactOnReceiveArray(byte[] bArray)
        {
            if (InvokeRequired)
            {
                Invoke((MethodInvoker)(() => ReactOnReceiveArray(bArray)));
                return;
            }
            PrintForArrays(bArray, false);
        }
        void PrintAboutSendCmd(String RequestStr, String TranscriptStr)
        {
            CutePrintStr(AllStylesStr["OrdinaryText"], "Запрос ", false);
            CutePrintStr(AllStylesStr["SendCmd"], RequestStr);
            if (TranscriptStr != null)
                CutePrintStr(AllStylesStr["OrdinaryText"], TranscriptStr);
        }
        public void ReactOnSendCmd(byte bCode, object obj)
        {
            //очищение RichTextBox, если в нем осталось мало памяти
            if (rtbExchangeLog.MaxLength < 500)
                rtbExchangeLog.Clear();
            switch (bCode)
            {
                case 1://состояние литеры
                    PrintAboutSendCmd((byte)obj == 0 ? "состояние всех литер\r\n" : "состояние литеры " + (byte)obj + "\r\n", null);
                    break;
                case 2:
                    TTypeLoad tlForSend = (TTypeLoad)obj;
                    PrintAboutSendCmd("установить тип нагрузки",
                        (tlForSend.bLetter == 0 ? "установить на все литеры" : "установить на литеру " + tlForSend.bLetter) +
                        (tlForSend.bType == 0 ? " тип нагрузки - эквивалент\r\n" : " тип нагрузки - антенна\r\n"));
                    break;
                case 5:
                    TDurationParamFWS dpfwsForSend = (TDurationParamFWS)obj;
                    String ForTranscriptStr = "Длительность излучения - " + dpfwsForSend.iDuration + ", колчество ИРИ - " + dpfwsForSend.bCount;
                    for (int i = 0; i < dpfwsForSend.bCount; i++)// расшифровка
                        ForTranscriptStr += dfws.Description(dpfwsForSend.SourceSupress[i].bModulation, dpfwsForSend.SourceSupress[i].bDeviation,
                            dpfwsForSend.SourceSupress[i].bManipulation, dpfwsForSend.SourceSupress[i].bDuration);
                    PrintAboutSendCmd("параметры ИРИ ФРЧ и включ излучение", ForTranscriptStr + "\r\n");
                    break;
                case 6:
                    PrintAboutSendCmd((byte)obj == 0 ? "напряжение всех литер\r\n" : "напряжение литеры " + (byte)obj + "\r\n", null);
                    break;
                case 7:
                    PrintAboutSendCmd((byte)obj == 0 ? "мощность всех литер\r\n" : "мощность литеры " + (byte)obj + "\r\n", null);
                    break;
                case 8:
                    PrintAboutSendCmd((byte)obj == 0 ? "ток всех литер\r\n" : "ток литеры " + (byte)obj + "\r\n", null);
                    break;
                case 9:
                    PrintAboutSendCmd((byte)obj == 0 ? "температура всех литер\r\n" : "температура литеры " + (byte)obj + "\r\n", null);
                    break;
                case 10:
                    PrintAboutSendCmd("выключить излучение\r\n", null);
                    break;
                case 11:
                    TDurationParamFHSS dpfhssForSend = (TDurationParamFHSS)obj;
                    ForTranscriptStr = "Частота минимальная - " + dpfhssForSend.iFreqMin + "\r\nДлительность излучения - " + dpfhssForSend.iDuration +
                        "\r\nКод разрешения БПФ - " + dpfhssForSend.bCodeFFT + "\r\nКод модуляции - " + dpfhssForSend.bModulation + "\r\nКод девиации - " + dpfhssForSend.bDeviation +
                        "\r\nКод манипуляции - " + dpfhssForSend.bManipulation + "\r\n";
                    PrintAboutSendCmd("установить параметры РП для ИРИ ППРЧ", ForTranscriptStr);
                    break;
                case 12:
                case 13:
                case 15:
                    PrintAboutSendCmd(Cmds[bCode], null);
                    break;
                case 14:
                    DurationMeasurement durationMeasurement = (DurationMeasurement)obj;
                    ForTranscriptStr = "Минимальная частота - " + durationMeasurement.iFreqMin + ", длительность излучения - " + durationMeasurement.iRadiationDuration
                        + ", длительность цикла к-и - " + durationMeasurement.iCycleDuration;
                    PrintAboutSendCmd("установить параметры ИРИ ППРЧ lkz bpvthtybz lkbntkmyjcnb", ForTranscriptStr);
                    break;
                case 18:
                    TDurationParamFHSSForMoreOneNetworks dpfhssmon = (TDurationParamFHSSForMoreOneNetworks)obj;
                    ForTranscriptStr = "Длительность - " + dpfhssmon.iDuration;
                    for (int i = 0; i < dpfhssmon.bCount; i++)
                        ForTranscriptStr +="Частота - " + dpfhssmon.AllParamFHSS[i].iFreq + "\r\nкод БПФ - " + dpfhssmon.AllParamFHSS[i].bCodeFFT + 
                            "\r\nКод модуляции - " + dpfhssmon.AllParamFHSS[i].bModulation + "\r\nКод девиации - " + dpfhssmon.AllParamFHSS[i].bDeviation +
                            "\r\nКод манипуляции - " + dpfhssmon.AllParamFHSS[i].bManipulation + "\r\n";
                    PrintAboutSendCmd("установить параметры РП для ИРИ ППРЧ (н.с.)", ForTranscriptStr);
                    break;
                default:
                    break;
            }
        }
        public void ReactOnReceiveCmd(byte bCode, object obj)
        {
            if (InvokeRequired)
            {
                Invoke((MethodInvoker)(() => ReactOnReceiveCmd(bCode, obj)));
                return;
            }
            TCodeErrorInform FromReceiveCEI = (TCodeErrorInform)obj;
            sForRepository.WaitOne();
            CutePrintStr(AllStylesStr["OrdinaryText"], "Ответ на запрос ", false);
            CutePrintStr(AllStylesStr["ReceiveCmd"], Cmds[bCode]);
            CutePrintStr(AllStylesStr["OrdinaryText"],"Значение кода ошибки - " + DecodAllError.DecodingError(FromReceiveCEI.bCodeError));
            switch (bCode)
            {
                case 13:
                    CutePrintStr(AllStylesStr["OrdinaryText"], "Состояние ФПС - " + FPSStates[FromReceiveCEI.bInform[0]]);
                    break;
                case 2:
                    CutePrintStr(AllStylesStr["OrdinaryText"], "Текущий тип нагрузки - " + (FromReceiveCEI.bInform[0] ==0 ? "эквивалент" : "антенна"));
                    break;
                default:
                    break;
            }
            sForRepository.Release();
        }
        private void bClearRTB_Click(object sender, EventArgs e)
        { 
            rtbExchangeLog.Clear();
        }
        private void cbHex_CheckedChanged(object sender, EventArgs e)
        {
            bHex = cbHex.Checked ? true : false;
        }
        
        public void MessageForUser(String message)
        {
            if (InvokeRequired)
            {
                Invoke((MethodInvoker)(() => MessageForUser(message)));
                return;
            }
            CutePrintStr(AllStylesStr["MessageForUser"], message + "\r\n");
        }
        private void EL_FormClosing(object sender, FormClosingEventArgs e)
        {
            OnWithoutParamsEvent(CloseForm);
        }

        private void rtbExchangeLog_TextChanged(object sender, EventArgs e)
        {
            rtbExchangeLog.SelectionStart = rtbExchangeLog.Text.Length;
            rtbExchangeLog.ScrollToCaret();
        }

        private void EL_Load(object sender, EventArgs e)
        {
            ReTranslate();
        }

        private void ReTranslate()
        {
            if (varCommon.Language.Equals(0))
            {
                this.Text = "Журнал обмена";
                bClearRTB.Text = "Очистить";
            }
            if (varCommon.Language.Equals(1))
            {
                //this.Text = "Журнал обмена";
                //bClearRTB.Text = "Очистить";
            }
            if (varCommon.Language.Equals(2))
            {
                this.Text = "Mübadilə jurnalı";
                bClearRTB.Text = "Təmizləmək";
            }
        }

        void VariableCommon_OnChangeCommonLanguage()
        {
            ReTranslate();
        }

    }
}
