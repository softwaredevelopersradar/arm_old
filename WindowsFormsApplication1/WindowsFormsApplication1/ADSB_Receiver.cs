﻿using MyDataGridView;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Table_ADSB_Receiver;
using VariableStatic;

namespace WndProject
{
    public partial class ADSB_Receiver : Form
    {
        short TIME_INDICATE = 100;
        StateIndicate stateIndicateRead = null;

        VariableCommon variableCommon;

        private MainWnd main;

        public ADSB_Receiver(MainWnd f)
        {
            InitializeComponent();

            variableCommon = new VariableCommon();
            main = f;
        }

        public void InitEventADSB()
        {
            stateIndicateRead = new StateIndicate(main.pbReadModeS, Properties.Resources.green, Properties.Resources.gray, TIME_INDICATE);

            table_ADSB_Receiver.LoadTableADSB_Receiver();

            table_ADSB_Receiver.ChangeControlLanguage(variableCommon.Language);

            table_ADSB_Receiver.IndReadADSB += new Table_ADSB_Receiver.table_ADSB_Receiver.IndReadADSBEventHandler(table_ADSB_Receiver_IndReadADSB);
            table_ADSB_Receiver.bOpenEditorDB.Click += new EventHandler(bOpenEditorDB_Click);
            table_ADSB_Receiver.bCloseEditorDB.Click += new EventHandler(bCloseEditorDB_Click);

            //Событие смены языка
            VariableStatic.VariableCommon.OnChangeCommonLanguage += new VariableStatic.VariableCommon.ChangeCommonEventHandler(VariableCommon_OnChangeCommonLanguage);
        }


        void bCloseEditorDB_Click(object sender, EventArgs e)
        {
            main.bConnectModeS.Enabled = true;
        }

        void bOpenEditorDB_Click(object sender, EventArgs e)
        {
            main.bConnectModeS.Enabled = false;
        }

        void table_ADSB_Receiver_IndReadADSB()
        {
            table_ADSB_Receiver.IndicateReadDataADSB(stateIndicateRead);
        }

        private void ADSB_Receiver_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            Hide();
        }

        public void ConnectADSB()
        {
            //try
            //{
            table_ADSB_Receiver.ConnectADSB();
            //}
            //catch(Exception ex)
            //{
            //    MessageBox.Show(ex.Message, SMessageError.mesErr, MessageBoxButtons.OK, MessageBoxIcon.Error);
            //}
        }

        public void CloseADSB()
        {
            table_ADSB_Receiver.CloseADSB();
            table_ADSB_Receiver.DeleteRecords();
        }

        private void ADSB_Receiver_Load(object sender, EventArgs e)
        {
            ReTranslate();
        }

        private void ReTranslate()
        {
            if (variableCommon.Language.Equals(0))
            {
                this.Text = "Воздушные суда";
            }
            if (variableCommon.Language.Equals(1))
            {
                this.Text = "Aircrafts";
            }
            if (variableCommon.Language.Equals(2))
            {
                this.Text = "Hva gəmisi";
            }
        }

        void VariableCommon_OnChangeCommonLanguage()
        {
            ReTranslate();
        }

    }
}
