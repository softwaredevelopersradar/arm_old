﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WndProject
{
    public partial class ChangeMessage : Form
    {
        static bool bIsLoaded = false;
        public const string CMD_ConfirmText = "  Текстовое сообщение (подтверждение)";

        public FuncVLC.ShowLog showLogJammer = null;
        VariableStatic.VariableCommon varCommon = new VariableStatic.VariableCommon();
        public ChangeMessage()
        {
            InitializeComponent();
            if (varCommon.TypeStation == 0|| varCommon.TypeStation == 2)
                tbpHistoryHS.Parent = null;
            if (!varCommon.pPU)
                tbpHistoryPC.Parent = null;
            if (!varCommon.pASP)
                tbpHistoryJammer.Parent = null;
            VariableStatic.VariableCommon.OnChangeCommonRole += ChangeRole;
            EventPU();

            //Событие смены языка
            VariableStatic.VariableCommon.OnChangeCommonLanguage += new VariableStatic.VariableCommon.ChangeCommonEventHandler(VariableCommon_OnChangeCommonLanguage);
        }

        void ChangeRole()
        {
            if (!varCommon.pASP)
                tbpHistoryJammer.Parent = null;
            else if (tbpHistoryJammer.Parent == null)
            {
                tbpHistoryJammer = new System.Windows.Forms.TabPage();
                tbpHistoryJammer.SuspendLayout();
                tbcHistoryMessage.Controls.Add(this.tbpHistoryJammer);
                this.tbpHistoryJammer.BackColor = System.Drawing.Color.Gainsboro;
                this.tbpHistoryJammer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
                this.tbpHistoryJammer.Controls.Add(this.rtbHistoryJammer);
                this.tbpHistoryJammer.Location = new System.Drawing.Point(4, 22);
                this.tbpHistoryJammer.Name = "tbpHistoryJammer";
                this.tbpHistoryJammer.Padding = new System.Windows.Forms.Padding(3);
                this.tbpHistoryJammer.Size = new System.Drawing.Size(296, 228);
                this.tbpHistoryJammer.TabIndex = 0;
                this.tbpHistoryJammer.Text = "Станция помех";
                ///
                //// rtbHistoryJammer
                //// 
                this.rtbHistoryJammer.BackColor = System.Drawing.Color.Tan;
                this.rtbHistoryJammer.Location = new System.Drawing.Point(-1, -1);
                this.rtbHistoryJammer.Name = "rtbHistoryJammer";
                this.rtbHistoryJammer.ReadOnly = true;
                this.rtbHistoryJammer.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
                this.rtbHistoryJammer.Size = new System.Drawing.Size(296, 228);
                this.rtbHistoryJammer.TabIndex = 0;
                this.rtbHistoryJammer.Text = "";
                // 
            }
            if (!varCommon.pPU)
                tbpHistoryPC.Parent = null;
            else if (tbpHistoryPC.Parent == null)
            {
                tbpHistoryPC = new System.Windows.Forms.TabPage();
                tbpHistoryPC.SuspendLayout();
                tbcHistoryMessage.Controls.Add(this.tbpHistoryPC);
                this.tbpHistoryPC.BackColor = System.Drawing.Color.Gainsboro;
                this.tbpHistoryPC.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
                this.tbpHistoryPC.Controls.Add(this.rtbHistoryPC);
                this.tbpHistoryPC.Location = new System.Drawing.Point(4, 22);
                this.tbpHistoryPC.Name = "tbpHistoryPC";
                this.tbpHistoryPC.Padding = new System.Windows.Forms.Padding(3);
                this.tbpHistoryPC.Size = new System.Drawing.Size(296, 228);
                this.tbpHistoryPC.TabIndex = 0;
                this.tbpHistoryPC.Text = "Пункт управления";
                // 
                // rtbHistoryPC
                // 
                this.rtbHistoryPC.BackColor = System.Drawing.Color.Tan;
                this.rtbHistoryPC.Location = new System.Drawing.Point(-1, -1);
                this.rtbHistoryPC.Name = "rtbHistoryPC";
                this.rtbHistoryPC.ReadOnly = true;
                this.rtbHistoryPC.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
                this.rtbHistoryPC.Size = new System.Drawing.Size(295, 225);
                this.rtbHistoryPC.TabIndex = 1;
                this.rtbHistoryPC.Text = "";
            }
        }
        private void ChangeMessage_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            Hide();    
        }

        private void ShowSendTextPC(string strText)
        {
            if (rtbHistoryPC.InvokeRequired)
            {
                rtbHistoryPC.Invoke((MethodInvoker)(delegate()
                {
                    rtbHistoryPC.AppendText("\n");
                    rtbHistoryPC.SelectionColor = Color.Blue;
                    rtbHistoryPC.AppendText(System.DateTime.Now.ToString("HH:mm:ss") + "  "); 
                    rtbHistoryPC.SelectionColor = Color.Black;
                    rtbHistoryPC.AppendText(strText);
                    rtbHistoryPC.SelectionStart = rtbHistoryPC.TextLength;
                    rtbHistoryPC.ScrollToCaret();   
                }));

            }
            else
            {
                rtbHistoryPC.AppendText("\n");
                rtbHistoryPC.SelectionColor = Color.Blue;
                rtbHistoryPC.AppendText(System.DateTime.Now.ToString("HH:mm:ss") + "  ");
                rtbHistoryPC.SelectionColor = Color.Black;
                rtbHistoryPC.AppendText(strText);
                rtbHistoryPC.SelectionStart = rtbHistoryPC.TextLength;
                rtbHistoryPC.ScrollToCaret();   
            }                     
        }

        private void ShowReadTextPC(string strText)
        {
            if (rtbHistoryPC.InvokeRequired)
            {
                rtbHistoryPC.Invoke((MethodInvoker)(delegate()
                {
                    rtbHistoryPC.AppendText("\n");
                    rtbHistoryPC.SelectionColor = Color.Red;
                    rtbHistoryPC.Text = System.DateTime.Now.ToString("HH:mm:ss") + "  "; //.AppendText(System.DateTime.Now.ToString("HH:mm:ss") + "  ");
                    rtbHistoryPC.SelectionColor = Color.Black;
                    rtbHistoryPC.Text = strText;//.AppendText(strText);
                    rtbHistoryPC.SelectionStart = rtbHistoryPC.TextLength;
                    rtbHistoryPC.ScrollToCaret();
                }));

            }
            else
            {
                rtbHistoryPC.AppendText("\n");
                rtbHistoryPC.SelectionColor = Color.Red;
                rtbHistoryPC.Text = System.DateTime.Now.ToString("HH:mm:ss") + "  "; //.AppendText(System.DateTime.Now.ToString("HH:mm:ss") + "  ");
                rtbHistoryPC.SelectionColor = Color.Black;
                rtbHistoryPC.Text = strText;//.AppendText(strText);
                rtbHistoryPC.SelectionStart = rtbHistoryPC.TextLength;
                rtbHistoryPC.ScrollToCaret();
            }
        }

        private void ShowSendTextJammer(string strText)
        {
            rtbHistoryJammer.AppendText("\n");
            rtbHistoryJammer.SelectionColor = Color.Blue;
            rtbHistoryJammer.Text = System.DateTime.Now.ToString("HH:mm:ss") + "  "; //.AppendText(System.DateTime.Now.ToString("HH:mm:ss") + "  ");
            rtbHistoryJammer.SelectionColor = Color.Black;
            rtbHistoryJammer.Text = strText;//.AppendText(strText);
            rtbHistoryJammer.SelectionStart = rtbHistoryJammer.TextLength;
            rtbHistoryJammer.ScrollToCaret();
        }

        private void ShowReadTextJammer(string strText)
        {
            if (this.InvokeRequired)
            {
            }
            else
            {
                rtbHistoryJammer.AppendText("\n");
                rtbHistoryJammer.SelectionColor = Color.Red;
                rtbHistoryJammer.Text = System.DateTime.Now.ToString("HH:mm:ss") + "  "; //.AppendText(System.DateTime.Now.ToString("HH:mm:ss") + "  ");
                rtbHistoryJammer.SelectionColor = Color.Black;
                rtbHistoryJammer.Text = strText;//.AppendText(strText);
                rtbHistoryJammer.SelectionStart = rtbHistoryJammer.TextLength;
                rtbHistoryJammer.ScrollToCaret();
            }
        }

        private void bClearHistory_Click(object sender, EventArgs e)
        {
            switch (tbcHistoryMessage.SelectedTab.Name)//.SelectedIndex)
            {
                case "tbpHistoryPC"://0: // ПУ
                    rtbHistoryPC.Clear();                    
                    break;

                case "tbpHistoryJammer"://1: //Станция помех 
                    rtbHistoryJammer.Clear();                   
                    break;

                case "tbpHistoryHS"://2: // АПП
                    rtbHistoryHS.Clear();                   
                    break;

                default:
                    break;
            }
        }

        private async void bSend_Click(object sender, EventArgs e)
        {
            string strText = "";
            strText = tbTextMessage.Text;
            switch (tbcHistoryMessage.SelectedTab.Name)//.SelectedIndex)
            {
                case "tbpHistoryPC"://0: // ПУ
                    if (MainWnd.ClientPu != null)
                        MainWnd.ClientPu.SendText(strText);


                    if (MainWnd.ClientPu_Bel != null)
                    {
                        MainWnd.ClientPu_Bel.Send_TextMessage(strText);
                        ShowSendTextPC(strText);
                    }

                    tbTextMessage.Clear();
                    break;

                case "tbpHistoryJammer"://1: //Станция помех 
                    //ShowSendTextJammer(strText);
                    try
                    {
                       var answer =  await VariableDynamic.VariableWork.aWPtoBearingDSPprotocolNew.SendTextMasterSlaveMessage(strText);
                      //System.Threading.Tasks.Task.Run(async () => await VariableDynamic.VariableWork.aWPtoBearingDSPprotocolNew.SendTextMasterSlaveMessage(strText)).GetAwaiter().GetResult();
                       if (answer.Header.ErrorCode != 0)
                           return;
                        rtbHistoryJammer.SelectionAlignment = HorizontalAlignment.Left;
                        showLogJammer.ShowMessage(rtbHistoryJammer, 1, System.DateTime.Now.ToString("hh:mm:ss"), "", " " + strText);
                        showLogJammer.ShowMessage(rtbHistoryJammer, 1, "", "", "");// "\n");
                        tbTextMessage.Clear();
                    }
                    catch { }
                    break;
                case "tbpHistoryHS"://2: // АПП
                    if (MainWnd.ServerHS != null)
                    {
                        MainWnd.ServerHS.SendText(strText);
                        rtbHistoryHS.SelectionAlignment = HorizontalAlignment.Left;
                        showLogJammer.ShowMessage(rtbHistoryHS, 1, System.DateTime.Now.ToString("hh:mm:ss"), "", " " + strText);
                        showLogJammer.ShowMessage(rtbHistoryHS, 1, "", "", "");// "\n");
                        tbTextMessage.Clear();
                    }
                    break;
                default:
                    break;
            }

            
        }
        void ShowReadByte(object sender, byte[] bByte)
        {
            string strText = "";
            for (int i = 0; i < bByte.Length; i++)
            {
                strText += bByte[i] + " ";
            }
                ShowSendTextPC(strText);
        }

        void ShowWriteByte(object sender, string strText)
        {
            ShowSendTextPC(strText);
        }
        void ShowConfirmText(object sender,  byte bCodeError)
        {
            ShowSendTextPC(bCodeError.ToString());
        }
        

        public void EventPU()
        {
            showLogJammer = new FuncVLC.ShowLog();

        }
        private void ReadByte(object sender, byte[] bData)
        {
            if (sender == MainWnd.ClientPu || sender == MainWnd.ClientPu_Bel)
            {;
                //MainWnd.stateIndicateRead.SetIndicateOn();

                if (rtbHistoryPC.InvokeRequired)
                {
                    rtbHistoryPC.Invoke((MethodInvoker)(delegate()
                    {
                        rtbHistoryPC.SelectionAlignment = HorizontalAlignment.Right;
                        showLogJammer.ShowMessage(rtbHistoryPC, 0, showLogJammer.DecodeHexByte(bData), "", "");
                        showLogJammer.ShowMessage(rtbHistoryPC, 0, "", "", ""); // "\n");
                    }));

                }
                else
                {
                    rtbHistoryPC.SelectionAlignment = HorizontalAlignment.Right;
                    showLogJammer.ShowMessage(rtbHistoryPC, 0, showLogJammer.DecodeHexByte(bData), "", "");
                    showLogJammer.ShowMessage(rtbHistoryPC, 0, "", "", ""); //"\n");
                }
            }
            

        }

        private void WriteByte(object sender, byte[] bData)
        {
            if (sender == MainWnd.ClientPu || sender == MainWnd.ClientPu_Bel)
                //stateIndicateWrite.SetIndicateOn();

                if (rtbHistoryPC.InvokeRequired)
                {
                    rtbHistoryPC.Invoke((MethodInvoker)(delegate()
                    {
                        rtbHistoryPC.SelectionAlignment = HorizontalAlignment.Left;
                        showLogJammer.ShowMessage(rtbHistoryPC, 1, showLogJammer.DecodeHexByte(bData), "", "");
                        showLogJammer.ShowMessage(rtbHistoryPC, 1, "", "", ""); //"\n");
                    }));

                }
                else
                {
                    rtbHistoryPC.SelectionAlignment = HorizontalAlignment.Left;
                    showLogJammer.ShowMessage(rtbHistoryPC, 1, showLogJammer.DecodeHexByte(bData), "", "");
                    showLogJammer.ShowMessage(rtbHistoryPC, 1, "", "", ""); //"\n");
                }
            
        }
        private void Receive_TextCmd(object sender, string strText)
        {
            try
            {
                string strName = "";
                strName = "  ";// CMD_Text; 
                if (rtbHistoryPC.InvokeRequired)
                {
                    rtbHistoryPC.Invoke((MethodInvoker)(delegate()
                    {
                        rtbHistoryPC.SelectionAlignment = HorizontalAlignment.Right;
                        showLogJammer.ShowMessage(rtbHistoryPC, 0, System.DateTime.Now.ToString("hh:mm:ss"), strName, " " + strText);
                        showLogJammer.ShowMessage(rtbHistoryPC, 0, "", "", ""); //"\n");
                    }));

                }
                else
                {
                    rtbHistoryPC.SelectionAlignment = HorizontalAlignment.Right;
                    showLogJammer.ShowMessage(rtbHistoryPC, 0, System.DateTime.Now.ToString("hh:mm:ss"), strName, " " + strText);
                    showLogJammer.ShowMessage(rtbHistoryPC, 0, "", "",""); // "\n");
                }

            }
            catch (SystemException)
            { }

        }
        private void Send_TextCmd(object sender, string strText)
        {
            try
            {
                if (rtbHistoryPC.InvokeRequired)
                {
                    rtbHistoryPC.Invoke((MethodInvoker)(delegate()
                    {
                        rtbHistoryPC.SelectionAlignment = HorizontalAlignment.Left;
                        showLogJammer.ShowMessage(rtbHistoryPC, 1, System.DateTime.Now.ToString("hh:mm:ss"), "", " " + strText);
                        showLogJammer.ShowMessage(rtbHistoryPC, 1, "", "", ""); // "\n");
                    }));

                }
                else
                {
                    rtbHistoryPC.SelectionAlignment = HorizontalAlignment.Left;
                    showLogJammer.ShowMessage(rtbHistoryPC, 1, System.DateTime.Now.ToString("hh:mm:ss"), "", " " + strText);
                    showLogJammer.ShowMessage(rtbHistoryPC, 1, "", "", ""); // "\n");
                }
            }
            catch (SystemException)
            { }
        }
        public bool IsLoaded
        {
            get { return bIsLoaded; }
        }
        private void ChangeMessage_Load(object sender, EventArgs e)
        {
            bIsLoaded = true;
            ReTranslate();
        }

        private void ReTranslate()
        {
            if (varCommon.Language.Equals(0))
            {
                try
                {
                    this.Text = "Сообщения";
                    bSend.Text = "Отправить";
                    bClearHistory.Text = "Очистить историю";
                    for (int i = 0; i < tbcHistoryMessage.Controls.Count; i++)
                    {
                        switch (tbcHistoryMessage.Controls[i].Name)
                        {
                            case "tbpHistoryPC":// ПУ
                                tbcHistoryMessage.Controls[i].Text = "Пункт управления";
                                break;

                            case "tbpHistoryJammer": //Станция помех 
                                tbcHistoryMessage.Controls[i].Text = "Станция помех";
                                break;

                            case "tbpHistoryHS": // АПП
                                tbcHistoryMessage.Controls[i].Text = "Апп станция";
                                break;
                            default:
                                break;
                        }
                    }
                }
                catch { }
            }
            if (varCommon.Language.Equals(1))
            {
                //this.Text = "Settings";
            }
            if (varCommon.Language.Equals(2))
            {
                this.Text = "Məlumat";
                bSend.Text = "Göndərmək";
                bClearHistory.Text = "Tarixcəni sil";
                tbTextMessage.Text = "Введите текст";

                for (int i = 0; i < tbcHistoryMessage.Controls.Count; i++)
                {
                    switch (tbcHistoryMessage.Controls[i].Name)
                    {
                        case "tbpHistoryPC"://0: // ПУ
                            tbcHistoryMessage.Controls[i].Text = "İdarəetmə məntəqəsi";
                            break;

                        case "tbpHistoryJammer"://1: //Станция помех 
                            tbcHistoryMessage.Controls[i].Text = "Maneə stansiyası";
                            break;

                        case "tbpHistoryHS"://2: // АПП
                            tbcHistoryMessage.Controls[i].Text = "Ap stansiyası";
                            break;
                        default:
                            break;
                    }
                }
            }
        }

        void VariableCommon_OnChangeCommonLanguage()
        {
            ReTranslate();
        }

        private void tbTextMessage_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                tbTextMessage.Text = (tbTextMessage.Text.Split(new char[]{'\n', '\r'}))[0];
                bSend_Click(null, new EventArgs());
            }
        }
        
    }
}
