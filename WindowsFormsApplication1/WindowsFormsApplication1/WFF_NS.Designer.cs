﻿namespace WndProject
{
    partial class FormForNoiseShaper
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormForNoiseShaper));
            this.dataGridViewForLetter = new System.Windows.Forms.DataGridView();
            this.dgvPowerLetter = new System.Windows.Forms.DataGridView();
            this.dgvCurrentLetter = new System.Windows.Forms.DataGridView();
            this.dgvVoltageLetter = new System.Windows.Forms.DataGridView();
            this.dgvTempLetter = new System.Windows.Forms.DataGridView();
            this.bConnect = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.bStateButton = new System.Windows.Forms.Button();
            this.bReset = new System.Windows.Forms.Button();
            this.bCurrent = new System.Windows.Forms.Button();
            this.bVoltage = new System.Windows.Forms.Button();
            this.bTemp = new System.Windows.Forms.Button();
            this.bPowerLetter = new System.Windows.Forms.Button();
            this.bExitFWS = new System.Windows.Forms.Button();
            this.bFHSS = new System.Windows.Forms.Button();
            this.bFWS = new System.Windows.Forms.Button();
            this.bExitFHSS = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.bForEL = new System.Windows.Forms.Button();
            this.tlpForButtons = new System.Windows.Forms.TableLayoutPanel();
            this.bFHSSForDur = new System.Windows.Forms.Button();
            this.bFPSState = new System.Windows.Forms.Button();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.cbForLetterChosing = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.tbFWSTime = new System.Windows.Forms.TextBox();
            this.lFPSState = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewForLetter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPowerLetter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCurrentLetter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVoltageLetter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTempLetter)).BeginInit();
            this.tlpForButtons.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridViewForLetter
            // 
            this.dataGridViewForLetter.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            resources.ApplyResources(this.dataGridViewForLetter, "dataGridViewForLetter");
            this.dataGridViewForLetter.Name = "dataGridViewForLetter";
            this.dataGridViewForLetter.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewForLetter_CellContentClick);
            // 
            // dgvPowerLetter
            // 
            this.dgvPowerLetter.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            resources.ApplyResources(this.dgvPowerLetter, "dgvPowerLetter");
            this.dgvPowerLetter.Name = "dgvPowerLetter";
            this.dgvPowerLetter.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvPowerLetter_CellContentClick);
            // 
            // dgvCurrentLetter
            // 
            this.dgvCurrentLetter.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            resources.ApplyResources(this.dgvCurrentLetter, "dgvCurrentLetter");
            this.dgvCurrentLetter.Name = "dgvCurrentLetter";
            this.dgvCurrentLetter.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvCurrentLetter_CellContentClick);
            // 
            // dgvVoltageLetter
            // 
            this.dgvVoltageLetter.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            resources.ApplyResources(this.dgvVoltageLetter, "dgvVoltageLetter");
            this.dgvVoltageLetter.Name = "dgvVoltageLetter";
            this.dgvVoltageLetter.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvVoltageLetter_CellContentClick);
            // 
            // dgvTempLetter
            // 
            this.dgvTempLetter.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            resources.ApplyResources(this.dgvTempLetter, "dgvTempLetter");
            this.dgvTempLetter.Name = "dgvTempLetter";
            this.dgvTempLetter.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvTempLetter_CellContentClick);
            // 
            // bConnect
            // 
            resources.ApplyResources(this.bConnect, "bConnect");
            this.bConnect.Name = "bConnect";
            this.bConnect.UseVisualStyleBackColor = true;
            this.bConnect.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            resources.ApplyResources(this.button2, "button2");
            this.button2.Name = "button2";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            resources.ApplyResources(this.button4, "button4");
            this.button4.Name = "button4";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // button6
            // 
            resources.ApplyResources(this.button6, "button6");
            this.button6.Name = "button6";
            this.button6.UseVisualStyleBackColor = true;
            // 
            // bStateButton
            // 
            resources.ApplyResources(this.bStateButton, "bStateButton");
            this.bStateButton.Name = "bStateButton";
            this.bStateButton.UseVisualStyleBackColor = true;
            this.bStateButton.Click += new System.EventHandler(this.bStateButton_Click);
            // 
            // bReset
            // 
            resources.ApplyResources(this.bReset, "bReset");
            this.bReset.Name = "bReset";
            this.bReset.UseVisualStyleBackColor = true;
            this.bReset.Click += new System.EventHandler(this.button2_Click);
            // 
            // bCurrent
            // 
            resources.ApplyResources(this.bCurrent, "bCurrent");
            this.bCurrent.Name = "bCurrent";
            this.bCurrent.UseVisualStyleBackColor = true;
            this.bCurrent.Click += new System.EventHandler(this.bCurrent_Click);
            // 
            // bVoltage
            // 
            resources.ApplyResources(this.bVoltage, "bVoltage");
            this.bVoltage.Name = "bVoltage";
            this.bVoltage.UseVisualStyleBackColor = true;
            this.bVoltage.Click += new System.EventHandler(this.button4_Click);
            // 
            // bTemp
            // 
            resources.ApplyResources(this.bTemp, "bTemp");
            this.bTemp.Name = "bTemp";
            this.bTemp.UseVisualStyleBackColor = true;
            this.bTemp.Click += new System.EventHandler(this.bTemp_Click);
            // 
            // bPowerLetter
            // 
            resources.ApplyResources(this.bPowerLetter, "bPowerLetter");
            this.bPowerLetter.Name = "bPowerLetter";
            this.bPowerLetter.UseVisualStyleBackColor = true;
            this.bPowerLetter.Click += new System.EventHandler(this.button6_Click);
            // 
            // bExitFWS
            // 
            resources.ApplyResources(this.bExitFWS, "bExitFWS");
            this.bExitFWS.Name = "bExitFWS";
            this.bExitFWS.UseVisualStyleBackColor = true;
            this.bExitFWS.Click += new System.EventHandler(this.button5_Click);
            // 
            // bFHSS
            // 
            resources.ApplyResources(this.bFHSS, "bFHSS");
            this.bFHSS.Name = "bFHSS";
            this.bFHSS.UseVisualStyleBackColor = true;
            this.bFHSS.Click += new System.EventHandler(this.button7_Click);
            // 
            // bFWS
            // 
            resources.ApplyResources(this.bFWS, "bFWS");
            this.bFWS.Name = "bFWS";
            this.bFWS.UseVisualStyleBackColor = true;
            this.bFWS.Click += new System.EventHandler(this.button8_Click);
            // 
            // bExitFHSS
            // 
            resources.ApplyResources(this.bExitFHSS, "bExitFHSS");
            this.bExitFHSS.Name = "bExitFHSS";
            this.bExitFHSS.UseVisualStyleBackColor = true;
            this.bExitFHSS.Click += new System.EventHandler(this.button9_Click);
            // 
            // button10
            // 
            resources.ApplyResources(this.button10, "button10");
            this.button10.Name = "button10";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // button12
            // 
            resources.ApplyResources(this.button12, "button12");
            this.button12.Name = "button12";
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Click += new System.EventHandler(this.button12_Click);
            // 
            // bForEL
            // 
            resources.ApplyResources(this.bForEL, "bForEL");
            this.bForEL.Name = "bForEL";
            this.bForEL.UseVisualStyleBackColor = true;
            this.bForEL.Click += new System.EventHandler(this.bForEL_Click);
            // 
            // tlpForButtons
            // 
            resources.ApplyResources(this.tlpForButtons, "tlpForButtons");
            this.tlpForButtons.Controls.Add(this.bFHSSForDur, 2, 5);
            this.tlpForButtons.Controls.Add(this.bFPSState, 0, 5);
            this.tlpForButtons.Controls.Add(this.bStateButton, 0, 0);
            this.tlpForButtons.Controls.Add(this.bReset, 2, 1);
            this.tlpForButtons.Controls.Add(this.bExitFWS, 2, 3);
            this.tlpForButtons.Controls.Add(this.bPowerLetter, 1, 0);
            this.tlpForButtons.Controls.Add(this.bCurrent, 2, 0);
            this.tlpForButtons.Controls.Add(this.bFWS, 0, 3);
            this.tlpForButtons.Controls.Add(this.bVoltage, 0, 1);
            this.tlpForButtons.Controls.Add(this.bTemp, 1, 1);
            this.tlpForButtons.Controls.Add(this.button12, 0, 4);
            this.tlpForButtons.Controls.Add(this.button10, 1, 4);
            this.tlpForButtons.Controls.Add(this.tableLayoutPanel2, 2, 4);
            this.tlpForButtons.Controls.Add(this.bForEL, 0, 2);
            this.tlpForButtons.Controls.Add(this.bFHSS, 1, 2);
            this.tlpForButtons.Controls.Add(this.bExitFHSS, 2, 2);
            this.tlpForButtons.Controls.Add(this.tableLayoutPanel1, 1, 3);
            this.tlpForButtons.Controls.Add(this.lFPSState, 1, 5);
            this.tlpForButtons.Name = "tlpForButtons";
            // 
            // bFHSSForDur
            // 
            resources.ApplyResources(this.bFHSSForDur, "bFHSSForDur");
            this.bFHSSForDur.Name = "bFHSSForDur";
            this.bFHSSForDur.UseVisualStyleBackColor = true;
            this.bFHSSForDur.Click += new System.EventHandler(this.bFHSSForDur_Click);
            // 
            // bFPSState
            // 
            resources.ApplyResources(this.bFPSState, "bFPSState");
            this.bFPSState.Name = "bFPSState";
            this.bFPSState.UseVisualStyleBackColor = true;
            this.bFPSState.Click += new System.EventHandler(this.bFPSState_Click);
            // 
            // tableLayoutPanel2
            // 
            resources.ApplyResources(this.tableLayoutPanel2, "tableLayoutPanel2");
            this.tableLayoutPanel2.Controls.Add(this.cbForLetterChosing, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.label2, 0, 0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            // 
            // cbForLetterChosing
            // 
            resources.ApplyResources(this.cbForLetterChosing, "cbForLetterChosing");
            this.cbForLetterChosing.FormattingEnabled = true;
            this.cbForLetterChosing.Name = "cbForLetterChosing";
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // tableLayoutPanel1
            // 
            resources.ApplyResources(this.tableLayoutPanel1, "tableLayoutPanel1");
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tbFWSTime, 0, 1);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // tbFWSTime
            // 
            resources.ApplyResources(this.tbFWSTime, "tbFWSTime");
            this.tbFWSTime.Name = "tbFWSTime";
            this.tbFWSTime.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbFWSTime_KeyPress);
            this.tbFWSTime.KeyUp += new System.Windows.Forms.KeyEventHandler(this.tbFWSTime_KeyUp);
            // 
            // lFPSState
            // 
            resources.ApplyResources(this.lFPSState, "lFPSState");
            this.lFPSState.Name = "lFPSState";
            // 
            // FormForNoiseShaper
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tlpForButtons);
            this.Controls.Add(this.dgvTempLetter);
            this.Controls.Add(this.dgvVoltageLetter);
            this.Controls.Add(this.dgvCurrentLetter);
            this.Controls.Add(this.dgvPowerLetter);
            this.Controls.Add(this.dataGridViewForLetter);
            this.Controls.Add(this.bConnect);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormForNoiseShaper";
            this.TopMost = true;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormForNoiseShaper_FormClosing);
            this.Load += new System.EventHandler(this.FormForNoiseShaper_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewForLetter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPowerLetter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCurrentLetter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVoltageLetter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTempLetter)).EndInit();
            this.tlpForButtons.ResumeLayout(false);
            this.tlpForButtons.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridViewForLetter;
        private System.Windows.Forms.DataGridView dgvPowerLetter;
        private System.Windows.Forms.DataGridView dgvCurrentLetter;
        private System.Windows.Forms.DataGridView dgvVoltageLetter;
        private System.Windows.Forms.DataGridView dgvTempLetter;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button bConnect;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Button bStateButton;
        private System.Windows.Forms.Button bReset;
        private System.Windows.Forms.Button bCurrent;
        private System.Windows.Forms.Button bVoltage;
        private System.Windows.Forms.Button bTemp;
        private System.Windows.Forms.Button bPowerLetter;
        private System.Windows.Forms.Button bExitFWS;
        private System.Windows.Forms.Button bFHSS;
        private System.Windows.Forms.Button bFWS;
        private System.Windows.Forms.Button bExitFHSS;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button bForEL;
        private System.Windows.Forms.TableLayoutPanel tlpForButtons;
        private System.Windows.Forms.ComboBox cbForLetterChosing;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbFWSTime;
        private System.Windows.Forms.Button bFPSState;
        private System.Windows.Forms.Label lFPSState;
        private System.Windows.Forms.Button bFHSSForDur;



    }
}

