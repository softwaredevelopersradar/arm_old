﻿
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WndProject
{
    class iniRW
    {
        [DllImport("kernel32")]
        private static extern long WritePrivateProfileString(string section, string key, string val, string filePath);
        [DllImport("kernel32")]
        private static extern long WritePrivateProfileSection(string section, string val, string filePath);
        [DllImport("kernel32")]
        private static extern int GetPrivateProfileString(string section, string key, string def, StringBuilder retVal, int size, string filePath);


        public static int get_amount()
        {
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("WinAP3_General", "amount", "0", temp, 255, Application.StartupPath + "\\INI\\Connection.ini");
            return int.Parse(temp.ToString());
        }

        public static void write_amount(int amnt)
        {
            WritePrivateProfileString("WinAP3_General", "amount", amnt.ToString(), Application.StartupPath + "\\INI\\Connection.ini");
        }

        public static string read_ARD_RRS1_address()
        {
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("ARD_COMs_param", "Address_RRS1", "0", temp, 255, Application.StartupPath + "\\INI\\Connection.ini");
            return temp.ToString();
        }
        public static string read_ARD_RRS2_address()
        {
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("ARD_COMs_param", "Address_RRS2", "0", temp, 255, Application.StartupPath + "\\INI\\Connection.ini");
            return temp.ToString();
        }
        public static string read_ARD_LPA_address()
        {
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("ARD_COMs_param", "Address_LPA", "0", temp, 255, Application.StartupPath + "\\INI\\Connection.ini");
            return temp.ToString();
        }
        public static string read_ARD_RRS_ComN()
        {
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("ARD_COMs_param", "Com_RRS", "0", temp, 255, Application.StartupPath + "\\INI\\Connection.ini");
            return temp.ToString();
        }
        public static string read_ARD_LPA_ComN()
        {
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("ARD_COMs_param", "Com_LPA", "0", temp, 255, Application.StartupPath + "\\INI\\Connection.ini");
            return temp.ToString();
        }
        public static void write_ARD1_azimuth(double value)
        {
            WritePrivateProfileString("ARD1", "azimuth", value.ToString(), Application.StartupPath + "\\INI\\Connection.ini");
        }
        public static void write_ARD1_need_azimuth(double value)
        {
            WritePrivateProfileString("ARD1", "need_azimuth", value.ToString(), Application.StartupPath + "\\INI\\Connection.ini");
        }
        public static void write_ARD1_Radant(double value)
        {
            WritePrivateProfileString("ARD1", "Radant", value.ToString(), Application.StartupPath + "\\INI\\Connection.ini");
        }

        public static void write_ARD2_azimuth(double value)
        {
            WritePrivateProfileString("ARD2", "azimuth", value.ToString(), Application.StartupPath + "\\INI\\Connection.ini");
        }
        public static void write_ARD2_need_azimuth(double value)
        {
            WritePrivateProfileString("ARD2", "need_azimuth", value.ToString(), Application.StartupPath + "\\INI\\Connection.ini");
        }
        public static void write_ARD2_Radant(double value)
        {
            WritePrivateProfileString("ARD2", "Radant", value.ToString(), Application.StartupPath + "\\INI\\Connection.ini");
        }

        public static void write_ARD3_azimuth(double value)
        {
            WritePrivateProfileString("ARD3", "azimuth", value.ToString(), Application.StartupPath + "\\INI\\Connection.ini");
        }
        public static void write_ARD3_need_azimuth(double value)
        {
            WritePrivateProfileString("ARD3", "need_azimuth", value.ToString(), Application.StartupPath + "\\INI\\Connection.ini");
        }
        public static void write_ARD3_Radant(double value)
        {
            WritePrivateProfileString("ARD3", "Radant", value.ToString(), Application.StartupPath + "\\INI\\Connection.ini");
        }

        public static void write_compass1_kren(double value)
        {
            WritePrivateProfileString("compass1", "kren", value.ToString(), Application.StartupPath + "\\INI\\Connection.ini");
        }
        public static void write_compass1_azimuth(double value)
        {
            WritePrivateProfileString("compass1", "azimuth", value.ToString(), Application.StartupPath + "\\INI\\Connection.ini");
        }
        public static void write_compass1_tangazh(double value)
        {
            WritePrivateProfileString("compass1", "tangazh", value.ToString(), Application.StartupPath + "\\INI\\Connection.ini");
        }
        public static void write_compass1_correction(double value)
        {
            WritePrivateProfileString("compass1", "correction", value.ToString(), Application.StartupPath + "\\INI\\Connection.ini");
        }
        public static void write_compass1_declination(double value)
        {
            WritePrivateProfileString("compass1", "declination", value.ToString(), Application.StartupPath + "\\INI\\Connection.ini");
        }
        public static void write_compass1_final(double value)
        {
            WritePrivateProfileString("compass1", "final", value.ToString(), Application.StartupPath + "\\INI\\Connection.ini");
        }

        public static void write_compass2_kren(double value)
        {
            WritePrivateProfileString("compass2", "kren", value.ToString(), Application.StartupPath + "\\INI\\Connection.ini");
        }
        public static void write_compass2_azimuth(double value)
        {
            WritePrivateProfileString("compass2", "azimuth", value.ToString(), Application.StartupPath + "\\INI\\Connection.ini");
        }
        public static void write_compass2_tangazh(double value)
        {
            WritePrivateProfileString("compass2", "tangazh", value.ToString(), Application.StartupPath + "\\INI\\Connection.ini");
        }
        public static void write_compass2_correction(double value)
        {
            WritePrivateProfileString("compass2", "correction", value.ToString(), Application.StartupPath + "\\INI\\Connection.ini");
        }
        public static void write_compass2_declination(double value)
        {
            WritePrivateProfileString("compass2", "declination", value.ToString(), Application.StartupPath + "\\INI\\Connection.ini");
        }
        public static void write_compass2_final(double value)
        {
            WritePrivateProfileString("compass2", "final", value.ToString(), Application.StartupPath + "\\INI\\Connection.ini");
        }

        public static double read_ARD1_azimuth()
        {
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("ARD1", "azimuth", "0", temp, 255, Application.StartupPath + "\\INI\\Connection.ini");
            return double.Parse(temp.ToString());
        }
        public static double read_ARD1_need_azimuth()
        {
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("ARD1", "need_azimuth", "0", temp, 255, Application.StartupPath + "\\INI\\Connection.ini");
            return double.Parse(temp.ToString());
        }
        public static double read_ARD1_Radant()
        {
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("ARD1", "Radant", "0", temp, 255, Application.StartupPath + "\\INI\\Connection.ini");
            return double.Parse(temp.ToString());
        }
        public static string read_ARD1_ComN()
        {
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("ARD1", "ComN", "0", temp, 255, Application.StartupPath + "\\INI\\Connection.ini");
            return temp.ToString();
        }

        public static double read_ARD2_azimuth()
        {
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("ARD2", "azimuth", "0", temp, 255, Application.StartupPath + "\\INI\\Connection.ini");
            return double.Parse(temp.ToString());
        }
        public static double read_ARD2_need_azimuth()
        {
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("ARD2", "need_azimuth", "0", temp, 255, Application.StartupPath + "\\INI\\Connection.ini");
            return double.Parse(temp.ToString());
        }
        public static double read_ARD2_Radant()
        {
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("ARD2", "Radant", "0", temp, 255, Application.StartupPath + "\\INI\\Connection.ini");
            return double.Parse(temp.ToString());
        }
        public static double read_ARD2_correction_RRS()
        {
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("ARD2", "RRSCorrection", "0", temp, 255, Application.StartupPath + "\\INI\\Connection.ini");
            return double.Parse(temp.ToString());
        }
        public static string read_ARD2_ComN()
        {
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("ARD2", "ComN", "0", temp, 255, Application.StartupPath + "\\INI\\Connection.ini");
            return temp.ToString();
        }

        public static double read_ARD3_azimuth()
        {
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("ARD3", "azimuth", "0", temp, 255, Application.StartupPath + "\\INI\\Connection.ini");
            return double.Parse(temp.ToString());
        }
        public static double read_ARD3_need_azimuth()
        {
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("ARD3", "need_azimuth", "0", temp, 255, Application.StartupPath + "\\INI\\Connection.ini");
            return double.Parse(temp.ToString());
        }
        public static double read_ARD3_Radant()
        {
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("ARD3", "Radant", "0", temp, 255, Application.StartupPath + "\\INI\\Connection.ini");
            return double.Parse(temp.ToString());
        }
        public static double read_ARD3_correction_LPA()
        {
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("ARD3", "LPACorrection ", "0", temp, 255, Application.StartupPath + "\\INI\\Connection.ini");
            return double.Parse(temp.ToString());
        }
        public static string read_ARD3_ComN()
        {
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("ARD3", "ComN", "0", temp, 255, Application.StartupPath + "\\INI\\Connection.ini");
            return temp.ToString();
        }
        public static double read_compass1_kren()
        {
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("compass1", "kren", "0", temp, 255, Application.StartupPath + "\\INI\\Connection.ini");
            return double.Parse(temp.ToString());
        }
        public static double read_compass1_azimuth()
        {
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("compass1", "azimuth", "0", temp, 255, Application.StartupPath + "\\INI\\Connection.ini");
            return double.Parse(temp.ToString());
        }
        public static double read_compass1_tangazh()
        {
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("compass1", "tangazh", "0", temp, 255, Application.StartupPath + "\\INI\\Connection.ini");
            return double.Parse(temp.ToString());
        }
        public static double read_compass1_correction()
        {
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("compass1", "correction", "0", temp, 255, Application.StartupPath + "\\INI\\Connection.ini");
            return double.Parse(temp.ToString());
        }
        public static double read_compass1_declination()
        {
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("compass1", "declination", "0", temp, 255, Application.StartupPath + "\\INI\\Connection.ini");
            return double.Parse(temp.ToString());
        }
        public static double read_compass1_final()
        {
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("compass1", "final", "0", temp, 255, Application.StartupPath + "\\INI\\Connection.ini");
            return double.Parse(temp.ToString());
        }
        public static string read_Compass1_ComN()
        {
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("compass1", "ComN", "0", temp, 255, Application.StartupPath + "\\INI\\Connection.ini");
            return temp.ToString();
        }

        public static double read_compass2_kren()
        {
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("compass2", "kren", "0", temp, 255, Application.StartupPath + "\\INI\\Connection.ini");
            return double.Parse(temp.ToString());
        }
        public static double read_compass2_azimuth()
        {
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("compass2", "azimuth", "0", temp, 255, Application.StartupPath + "\\INI\\Connection.ini");
            return double.Parse(temp.ToString());
        }
        public static double read_compass2_tangazh()
        {
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("compass2", "tangazh", "0", temp, 255, Application.StartupPath + "\\INI\\Connection.ini");
            return double.Parse(temp.ToString());
        }
        public static double read_compass2_correction()
        {
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("compass2", "correction", "0", temp, 255, Application.StartupPath + "\\INI\\Connection.ini");
            return double.Parse(temp.ToString());
        }
        public static double read_compass2_declination()
        {
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("compass2", "declination", "0", temp, 255, Application.StartupPath + "\\INI\\Connection.ini");
            return double.Parse(temp.ToString());
        }
        public static double read_compass2_final()
        {
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("compass2", "final", "0", temp, 255, Application.StartupPath + "\\INI\\Connection.ini");
            return double.Parse(temp.ToString());
        }
        public static string read_Compass2_ComN()
        {
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("compass2", "ComN", "0", temp, 255, Application.StartupPath + "\\INI\\Connection.ini");
            return temp.ToString();
        }

        /// <summary> 
        /// Получить настройки СОМ-порта 
        /// </summary>
        /// <returns> </returns>
        public static string get_ParametersPort()
        {
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("ParametersPortGPS", "ParametersGPS", "", temp, 255, Application.StartupPath + "\\INI\\Connection.ini");
            return temp.ToString();
        }

        /// <summary>
        /// Сохранить настройки COM-порта 
        /// </summary>
        /// <param name="ParamPort"></param>
        public static void write_ParametersPort(string ParamPort)
        {
            WritePrivateProfileString("ParametersPortGPS", "ParametersGPS", ParamPort, Application.StartupPath + "\\INI\\Connection.ini");
        }
        public static int[] get_color_lpa()
        {
            int[] colorr = new int[4];
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("Colors_LPA", "A", "", temp, 255, Application.StartupPath + "\\INI\\Color.ini");
            int.TryParse(temp.ToString(), out colorr[0]);
            GetPrivateProfileString("Colors_LPA", "R", "", temp, 255, Application.StartupPath + "\\INI\\Color.ini");
            int.TryParse(temp.ToString(), out colorr[1]);
            GetPrivateProfileString("Colors_LPA", "G", "", temp, 255, Application.StartupPath + "\\INI\\Color.ini");
            int.TryParse(temp.ToString(), out colorr[2]);
            GetPrivateProfileString("Colors_LPA", "B", "", temp, 255, Application.StartupPath + "\\INI\\Color.ini");
            int.TryParse(temp.ToString(), out colorr[3]);
            return colorr;
        }
        public static int[] get_color_lpa1_3()
        {
            int[] colorr = new int[4];
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("Colors_LPA1_3", "A", "", temp, 255, Application.StartupPath + "\\INI\\Color.ini");
            int.TryParse(temp.ToString(), out colorr[0]);
            GetPrivateProfileString("Colors_LPA1_3", "R", "", temp, 255, Application.StartupPath + "\\INI\\Color.ini");
            int.TryParse(temp.ToString(), out colorr[1]);
            GetPrivateProfileString("Colors_LPA1_3", "G", "", temp, 255, Application.StartupPath + "\\INI\\Color.ini");
            int.TryParse(temp.ToString(), out colorr[2]);
            GetPrivateProfileString("Colors_LPA1_3", "B", "", temp, 255, Application.StartupPath + "\\INI\\Color.ini");
            int.TryParse(temp.ToString(), out colorr[3]);
            return colorr;
        }
        public static int[] get_color_lpa2_4()
        {
            int[] colorr = new int[4];
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("Colors_LPA2_4", "A", "", temp, 255, Application.StartupPath + "\\INI\\Color.ini");
            int.TryParse(temp.ToString(), out colorr[0]);
            GetPrivateProfileString("Colors_LPA2_4", "R", "", temp, 255, Application.StartupPath + "\\INI\\Color.ini");
            int.TryParse(temp.ToString(), out colorr[1]);
            GetPrivateProfileString("Colors_LPA2_4", "G", "", temp, 255, Application.StartupPath + "\\INI\\Color.ini");
            int.TryParse(temp.ToString(), out colorr[2]);
            GetPrivateProfileString("Colors_LPA2_4", "B", "", temp, 255, Application.StartupPath + "\\INI\\Color.ini");
            int.TryParse(temp.ToString(), out colorr[3]);
            return colorr;
        }
        public static int[] get_color_PU()
        {
            int[] colorr = new int[4];
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("Colors_PU", "A", "", temp, 255, Application.StartupPath + "\\INI\\Color.ini");
            int.TryParse(temp.ToString(), out colorr[0]);
            GetPrivateProfileString("Colors_PU", "R", "", temp, 255, Application.StartupPath + "\\INI\\Color.ini");
            int.TryParse(temp.ToString(), out colorr[1]);
            GetPrivateProfileString("Colors_PU", "G", "", temp, 255, Application.StartupPath + "\\INI\\Color.ini");
            int.TryParse(temp.ToString(), out colorr[2]);
            GetPrivateProfileString("Colors_PU", "B", "", temp, 255, Application.StartupPath + "\\INI\\Color.ini");
            int.TryParse(temp.ToString(), out colorr[3]);
            return colorr;
        }
        public static int[] get_color_SP()
        {
            int[] colorr = new int[4];
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("Colors_SP", "A", "", temp, 255, Application.StartupPath + "\\INI\\Color.ini");
            int.TryParse(temp.ToString(), out colorr[0]);
            GetPrivateProfileString("Colors_SP", "R", "", temp, 255, Application.StartupPath + "\\INI\\Color.ini");
            int.TryParse(temp.ToString(), out colorr[1]);
            GetPrivateProfileString("Colors_SP", "G", "", temp, 255, Application.StartupPath + "\\INI\\Color.ini");
            int.TryParse(temp.ToString(), out colorr[2]);
            GetPrivateProfileString("Colors_SP", "B", "", temp, 255, Application.StartupPath + "\\INI\\Color.ini");
            int.TryParse(temp.ToString(), out colorr[3]);
            return colorr;
        }
        public static void write_Color_lpa(int[] Colorr)
        {
            WritePrivateProfileString("Colors_LPA", "A", Colorr[0].ToString(), Application.StartupPath + "\\INI\\Color.ini");
            WritePrivateProfileString("Colors_LPA", "R", Colorr[1].ToString(), Application.StartupPath + "\\INI\\Color.ini");
            WritePrivateProfileString("Colors_LPA", "G", Colorr[2].ToString(), Application.StartupPath + "\\INI\\Color.ini");
            WritePrivateProfileString("Colors_LPA", "B", Colorr[3].ToString(), Application.StartupPath + "\\INI\\Color.ini");
        }
        public static void write_Color_lpa1_3(int[] Colorr)
        {
            WritePrivateProfileString("Colors_LPA1_3", "A", Colorr[0].ToString(), Application.StartupPath + "\\INI\\Color.ini");
            WritePrivateProfileString("Colors_LPA1_3", "R", Colorr[1].ToString(), Application.StartupPath + "\\INI\\Color.ini");
            WritePrivateProfileString("Colors_LPA1_3", "G", Colorr[2].ToString(), Application.StartupPath + "\\INI\\Color.ini");
            WritePrivateProfileString("Colors_LPA1_3", "B", Colorr[3].ToString(), Application.StartupPath + "\\INI\\Color.ini");
        }
        public static void write_Color_lpa2_4(int[] Colorr)
        {
            WritePrivateProfileString("Colors_LPA2_4", "A", Colorr[0].ToString(), Application.StartupPath + "\\INI\\Color.ini");
            WritePrivateProfileString("Colors_LPA2_4", "R", Colorr[1].ToString(), Application.StartupPath + "\\INI\\Color.ini");
            WritePrivateProfileString("Colors_LPA2_4", "G", Colorr[2].ToString(), Application.StartupPath + "\\INI\\Color.ini");
            WritePrivateProfileString("Colors_LPA2_4", "B", Colorr[3].ToString(), Application.StartupPath + "\\INI\\Color.ini");
        }
        public static void write_Color_PU(int[] Colorr)
        {
            WritePrivateProfileString("Colors_PU", "A", Colorr[0].ToString(), Application.StartupPath + "\\INI\\Color.ini");
            WritePrivateProfileString("Colors_PU", "R", Colorr[1].ToString(), Application.StartupPath + "\\INI\\Color.ini");
            WritePrivateProfileString("Colors_PU", "G", Colorr[2].ToString(), Application.StartupPath + "\\INI\\Color.ini");
            WritePrivateProfileString("Colors_PU", "B", Colorr[3].ToString(), Application.StartupPath + "\\INI\\Color.ini");
        }
        public static void write_Color_SP(int[] Colorr)
        {
            WritePrivateProfileString("Colors_SP", "A", Colorr[0].ToString(), Application.StartupPath + "\\INI\\Color.ini");
            WritePrivateProfileString("Colors_SP", "R", Colorr[1].ToString(), Application.StartupPath + "\\INI\\Color.ini");
            WritePrivateProfileString("Colors_SP", "G", Colorr[2].ToString(), Application.StartupPath + "\\INI\\Color.ini");
            WritePrivateProfileString("Colors_SP", "B", Colorr[3].ToString(), Application.StartupPath + "\\INI\\Color.ini");
        }

    }
}

