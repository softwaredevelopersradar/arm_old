﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Ports;
using System.Threading;
using MyDataGridView;
using MathNet.Numerics.IntegralTransforms;
using MathNet.Numerics.Signals;
using MathNet.Numerics;
using NAudio;
using NAudio.Wave;
using NAudio.FileFormats;
using NAudio.CoreAudioApi;
using System.Numerics;
using System.Diagnostics;
using System.ServiceModel;

using VariableDynamic;
using Contract;
using VariableStatic;
using System.Globalization;

namespace WndProject
{
    public partial class MainWnd : Form
    {
        public WndProject OrientationBY;
        public OrientationAZer OrientationAZ;
        SectorsRanges sectorsRanges;
        SpecialFrequencies specialFrequencies;
        ADSB_Receiver adsbReceiver;
        FunctionsDB functionsDB;
        FuncDB_ADSB funcDB_ADSB;
        //FormForNoiseShaper formForNoiseShaper;
        Setting objSetting;
        ChangeMessage objChangeMessage;

        StateIndicate stateIndicateRead = null;
        StateIndicate stateIndicateWrite = null;

        Functions functions;
        myDataGridView myDGV;

        System.Diagnostics.Process ProcessMAP;
        System.Diagnostics.Process ProcessTechnicalAnalysis;
        System.Diagnostics.Process ProcessWavPlayer;

        Contract.IService service;

        bool blMapExist = false;
        bool blMapCheck = true;

        public int KursovojUgol;
        ComponentResourceManager resources = new ComponentResourceManager(typeof(Setting));

        public MainWnd()
        {
            InitializeComponent();
            ChangeAOR((int)variableCommon.TypeStation, (int)variableCommon.Operator);
            stateIndicateRead = new StateIndicate(pbReadFCh, Properties.Resources.green, Properties.Resources.gray, 50);
            stateIndicateWrite = new StateIndicate(pbWriteFCh, Properties.Resources.green, Properties.Resources.gray, 50);
            functionsDB = new FunctionsDB();
            funcDB_ADSB = new FuncDB_ADSB();
            sectorsRanges = new SectorsRanges();
            specialFrequencies = new SpecialFrequencies();
            adsbReceiver = new ADSB_Receiver(this);
            objChangeMessage = new ChangeMessage();
            adsbReceiver.InitEventADSB();

            functions = new Functions();
            myDGV = new myDataGridView();

            //InitConnectionArOne();
            //InitLabelParamArOne();
            InitEventControl();
            NameStation();

            VariableStatic.VariableCommon v = new VariableStatic.VariableCommon();

            if (v.TypeStation == 1)
                panoramaControl1.Init6000();

            panoramaControl1.ChangePanoramaLanguage(v.Language);
          
            InitStartParamCommon();
            ShowVariableStatic();
            SetLatLonToCoordsGNSS();
            
            
            ChangeControlsLanguage();
            ChangeMessagesLanguage();

            LoadTablesFromDB();
            SetViewWindowForRole();


            ProcessMAP = new System.Diagnostics.Process();
            ProcessMAP.StartInfo.FileName = Application.StartupPath + "\\Map\\GrozaMap.exe";
            //ProcessMAP.StartInfo.FileName = "..\\..\\..\\GrozaMap\\GrozaMap\\bin\\Debug\\GrozaMap.exe";

            ProcessTechnicalAnalysis = new Process();
            //ProcessTechnicalAnalysis.StartInfo.FileName = Application.StartupPath + "\\Application.exe";
            ProcessWavPlayer = new Process();
            //ProcessWavPlayer.StartInfo.FileName = Application.StartupPath + "\\Player\\Player.exe";

            objChangeMessage.Load += objChangeMessage_Load;

            VariableDynamic.VariableWork.aWPtoBearingDSPprotocolNew.IsConnected += new AWPtoBearingDSPprotocolNew.IsConnectedEventHandler(IsConnected);

            VariableDynamic.VariableWork.aWPtoBearingDSPprotocolNew.ShaperStateUpdate += aWPtoBearingDSPprotocolNew_ShaperStateUpdate;


            //Новые события для взаимодействия АРМ1-АРМ2

            //Подписка на событие от Сервера обновления специальных частот 
            //1.2.11
            VariableDynamic.VariableWork.aWPtoBearingDSPprotocolNew.SpecialFrequenciesMessageUpdate += aWPtoBearingDSPprotocolNew_SpecialFrequenciesMessageUpdate;

            //VariableDynamic.VariableWork.aWPtoBearingDSPprotocolNew.FiltersMessageUpdate += new AWPtoBearingDSPprotocolNew.FiltersMessageEventHandler(FiltersMessages);

            VariableDynamic.VariableWork.aWPtoBearingDSPprotocolNew.FiltersMessageUpdate += aWPtoBearingDSPprotocolNew_FiltersMessageUpdate;

            VariableDynamic.VariableWork.aWPtoBearingDSPprotocolNew.ModeMessageUpdate += aWPtoBearingDSPprotocolNew_ModeMessageUpdate;

            VariableDynamic.VariableWork.aWPtoBearingDSPprotocolNew.SectorsAndRangesMessageUpdate += aWPtoBearingDSPprotocolNew_SectorsAndRangesMessageUpdate;

            VariableDynamic.VariableWork.aWPtoBearingDSPprotocolNew.AttenuatorsMessageUpdate += aWPtoBearingDSPprotocolNew_AttenuatorsMessageUpdate;

            //1.2.11
            VariableDynamic.VariableWork.aWPtoBearingDSPprotocolNew.FrsJammingMessageUpdate += aWPtoBearingDSPprotocolNew_FrsJammingMessageUpdate;
            //1.2.11
            VariableDynamic.VariableWork.aWPtoBearingDSPprotocolNew.FhssJammingMessageUpdate += aWPtoBearingDSPprotocolNew_FhssJammingMessageUpdate;

            //Подписка на событие изменения состоянию Ведущая-Ведомая
            VariableDynamic.VariableWork.aWPtoBearingDSPprotocolNew.MasterSlaveStateChangedUpdate += aWPtoBearingDSPprotocolNew_MasterSlaveStateChangedUpdate;

            //Подписка на событие запроса настроек и режима для Ведущая-Ведомая
            VariableDynamic.VariableWork.aWPtoBearingDSPprotocolNew.GetSettingsUpdate += aWPtoBearingDSPprotocolNew_GetSettingsUpdate;

            VariableDynamic.VariableWork.aWPtoBearingDSPprotocolNew.IsRead += aWPtoBearingDSPprotocolNew_IsRead;
            VariableDynamic.VariableWork.aWPtoBearingDSPprotocolNew.IsWrite += aWPtoBearingDSPprotocolNew_IsWrite;

            //Подписка на событие изменения курсового угла
            VariableDynamic.VariableWork.aWPtoBearingDSPprotocolNew.DirectionCorrectionMessageUpdate += aWPtoBearingDSPprotocolNew_DirectionCorrectionMessageUpdate;

            //События VariableWork
            VariableDynamic.VariableWork.OnChangeRegime += VariableWork_OnChangeRegime;

            //Подписка на событие обновления секторов и диапазонов радиоразведки OWN
            VariableDynamic.VariableWork.OnChangeRangeSectorReconOwn += new VariableDynamic.VariableWork.ChangeRegimeEventHandler(VariableWork_OnChangeRangeSectorReconOwn);

            //Подписка на событие обновления секторов и диапазонов радиоразведки Linked
            VariableDynamic.VariableWork.OnChangeRangeSectorReconLinked += new VariableDynamic.VariableWork.ChangeRegimeEventHandler(VariableWork_OnChangeRangeSectorReconLinked);

            //Подписка на событие обновления ИРИ ФРЧ на РП Own
            VariableDynamic.VariableWork.OnChangeSupprFWS_Own += new VariableDynamic.VariableWork.ChangeVariableEventHandler(VariableWork_OnChangeSupprFWS_Own);

            //Подписка на событие обновления ИРИ ППРЧ на РП Own
            VariableDynamic.VariableWork.OnChangeDistribFHSS_RPOwn += new VariableWork.ChangeVariableEventHandler(VariableWork_OnChangeDistribFHSS_RPOwn);

            //Подписка на событие обновления Вырезанных частот ИРИ ППРЧ на РП Own
            VariableDynamic.VariableWork.OnChangeDistribFHSS_RPExcludeOwn += VariableWork_OnChangeDistribFHSS_RPExcludeOwn;

            //Подписка на событие обновления ИРИ ФРЧ на РП Linked
            VariableDynamic.VariableWork.OnChangeSupprFWS_Linked += new VariableDynamic.VariableWork.ChangeVariableEventHandler(VariableWork_OnChangeSupprFWS_Linked);

            //Подписка на событие обновления ИРИ ППРЧ на РП Linked
            VariableDynamic.VariableWork.OnChangeDistribFHSS_RPLinked += new VariableWork.ChangeVariableEventHandler(VariableWork_OnChangeDistribFHSS_RPLinked);

            //Подписка на событие обновления Вырезанных частот ИРИ ППРЧ на РП Linked
            VariableDynamic.VariableWork.OnChangeDistribFHSS_RPExcludeLinked += VariableWork_OnChangeDistribFHSS_RPExcludeLinked;

            //Подписка на событие обновления галочки Поиск ППРЧ
            VariableDynamic.VariableWork.aWPtoBearingDSPprotocolNew.SearchFhssMessageUpdate += aWPtoBearingDSPprotocolNew_SearchFhssMessageUpdate;

            //Подписка на событие обновления запрещенных частот для Own
            VariableDynamic.VariableWork.OnChangeFrequencyRangeForbiddenOwn += VariableWork_OnChangeFrequencyRangeForbiddenOwn;
            //Подписка на событие обновления известных частот для Own
            VariableDynamic.VariableWork.OnChangeFrequencyRangeKnownOwn += VariableWork_OnChangeFrequencyRangeKnownOwn;
            //Подписка на событие обновления важных частот для Own
            VariableDynamic.VariableWork.OnChangeFrequencyRangeImportantOwn += VariableWork_OnChangeFrequencyRangeImportantOwn;

            //Подписка на событие обновления запрещенных частот для Linked
            VariableDynamic.VariableWork.OnChangeFrequencyRangeForbiddenLinked += VariableWork_OnChangeFrequencyRangeForbiddenLinked;
            //Подписка на событие обновления известных частот для Linked
            VariableDynamic.VariableWork.OnChangeFrequencyRangeKnownLinked += VariableWork_OnChangeFrequencyRangeKnownLinked;
            //Подписка на событие обновления важных частот для Linked
            VariableDynamic.VariableWork.OnChangeFrequencyRangeImportantLinked += VariableWork_OnChangeFrequencyRangeImportantLinked;


            VariableDynamic.VariableWork.OnChangeDataADSBReceiver += new VariableDynamic.VariableWork.ChangeVariableADSBEventHandler(VariableWork_OnChangeDataADSBReceiver);
            VariableDynamic.VariableWork.OnChangeCoordsGNSS += new VariableWork.ChangeVariableCoordsGNSSEventHandler(VariableWork_OnChangeCoordsGNSS);
            VariableDynamic.VariableWork.OnChangeCoordsIRI += new VariableWork.ChangeVariableCoordsIRIEventHandler(VariableWork_OnChangeCoordsIRI);
            VariableDynamic.VariableWork.OnChangeCoordsIRI_PPRCh += new VariableWork.ChangeVariableCoordsIRI_PPRChEventHandler(VariableWork_OnChangeCoordsIRI_PPRCh);
            VariableDynamic.VariableWork.OnChangePelengsIRI_PPRCh += new VariableWork.ChangeVariablePelengsIRI_PPRChEventHandler(VariableWork_OnChangePelengsIRI_PPRCh);
            VariableDynamic.VariableWork.OnChangeDirectionAntennas += new VariableWork.ChangeVariableDirectionAntennasEventHandler(VariableWork_OnChangeDirectionAntennas);

            //Подписка на событие изменнения координат
            //1.2.11
            VariableWork.aWPtoBearingDSPprotocolNew.StationLocationMessageUpdate += new AWPtoBearingDSPprotocolNew.StationLocationMessageEventHandler(aWPtoBearingDSPprotocolNew_StationLocationMessageUpdate);

            //Подписка на событие изменнения направлений антенн
            //1.2.11
            VariableWork.aWPtoBearingDSPprotocolNew.AntennaDirectionsMessageUpdate += new AWPtoBearingDSPprotocolNew.AntennaDirectionsMessageEventHandler(aWPtoBearingDSPprotocolNew_AntennaDirectionsMessageUpdate);

            //Подписка на событие изменения/синхронизации времени
            //1.2.11
            VariableWork.aWPtoBearingDSPprotocolNew.SetTimeUpdate += new AWPtoBearingDSPprotocolNew.SetTimeEventHandler(aWPtoBearingDSPprotocolNew_SetTimeUpdate);


            //События VariableStatic
            VariableStatic.VariableIntellegence.OnChangeBearing += VariableIntellegence_OnChangeBearing;
            VariableStatic.VariableSuppression.OnChangeRegimeRadioSuppr += VariableSuppression_OnChangeRegimeRadioSuppr;

            VariableStatic.VariableIntellegence.OnChangeDetectionPPRCH += VariableIntellegence_OnChangeDetectionPPRCH;

            if (variableCommon.TypeStation == 0)//bel
            {
                VariableDynamic.VariableWork.OnChangeFrequency += new VariableDynamic.VariableWork.ChangeFrequencyEventHandler(VariableWork_OnChangeFrequencyARONE); //new VariableDynamic.VariableWork.OnChangeFrequency(ChangeFrequency);
                //Подписка на события от ARONE
                controlARONE.OnBAORGreen += controlARONE_OnBAORGreen;
                controlARONE.OnBAORRed += controlARONE_OnBAORRed;
                controlARONE.zagruzka_tablFromBD(NameTable.AR_ONE.ToString());
            }
            if (variableCommon.TypeStation == 1)//az
            {
                if (variableCommon.Operator == 0)
                {
                    VariableDynamic.VariableWork.OnChangeFrequency += new VariableDynamic.VariableWork.ChangeFrequencyEventHandler(VariableWork_OnChangeFrequencyAR6000); //new VariableDynamic.VariableWork.OnChangeFrequency(ChangeFrequency);

                    //Подписка на события от AOR6000
                    controlAR6000.OnBAORGreen += controlAR6000_OnBAORGreen;
                    controlAR6000.OnBAORRed += controlAR6000_OnBAORRed;
                    controlAR6000.zagruzka_tablFromBD(NameTable.AR6000.ToString());
                }
                if (variableCommon.Operator == 1)
                {
                    VariableDynamic.VariableWork.OnChangeFrequency += new VariableDynamic.VariableWork.ChangeFrequencyEventHandler(VariableWork_OnChangeFrequencyARDV1); //new VariableDynamic.VariableWork.OnChangeFrequency(ChangeFrequency);

                    //Подписка на события от ARDV1
                    controlARDV1.OnBAORGreen += controlARDV1_OnBAORGreen;
                    controlARDV1.OnBAORRed += controlARDV1_OnBAORRed;
                    controlARDV1.zagruzka_tablFromBD(NameTable.ARDV1.ToString());
                }
            }
            if (variableCommon.TypeStation == 2)//bel 6GGh
            {
                VariableDynamic.VariableWork.OnChangeFrequency += new VariableDynamic.VariableWork.ChangeFrequencyEventHandler(VariableWork_OnChangeFrequencyARONE); //new VariableDynamic.VariableWork.OnChangeFrequency(ChangeFrequency);
                //Подписка на события от ARONE
                controlARONE.OnBAORGreen += controlARONE_OnBAORGreen;
                controlARONE.OnBAORRed += controlARONE_OnBAORRed;
                controlARONE.zagruzka_tablFromBD(NameTable.AR_ONE.ToString());
            }

            VariableStatic.VariableCommon.OnChangeCommonUseGNSS +=  new VariableStatic.VariableCommon.ChangeCommonEventHandler(VariableCommon_OnChangeCommonUseGNSS); //

            //Собыьие смены языка
            VariableStatic.VariableCommon.OnChangeCommonLanguage += new VariableStatic.VariableCommon.ChangeCommonEventHandler(VariableCommon_OnChangeCommonLanguage);
            VariableCommon.OnChangeCommonLanguage += new VariableStatic.VariableCommon.ChangeCommonEventHandler(LanguageChooser);

            // WCF 
            //создаем клиент
            var scf = new ChannelFactory<Contract.IService>(new NetTcpBinding(), "net.tcp://localhost:8000");
            service = scf.CreateChannel();



            // .................................................................................................
            // WCF
            //создаем сервис
            var serviceMap = new ServiceImplementation();
            serviceMap.MapIsOpenUpdated += new EventHandler<bool>(serviceMap_MapIsOpenUpdated);
            serviceMap.MapCurrentCoordsUpdated += new EventHandler<USR_DLL.TCoordsGNSS[]>(serviceMap_MapCurrentCoordsUpdated);

            //стартуем сервер
            var svhMap = new ServiceHost(serviceMap);
            svhMap.AddServiceEndpoint(typeof(Contract.IService), new NetTcpBinding(), "net.tcp://localhost:8001");
            svhMap.Open();
            // .................................................................................................



            //Смена АРМа
            VariableStatic.VariableCommon.OnChangeCommonOperator += new VariableStatic.VariableCommon.ChangeCommonEventHandler(ChangeOperator);
            VariableStatic.VariableCommon.OnChangeCommonOperator += new VariableStatic.VariableCommon.ChangeCommonEventHandler(ChangeOperatorForComponents);
            VariableStatic.VariableCommon.OnChangeCommonRole += new VariableStatic.VariableCommon.ChangeCommonEventHandler(ChangeOwnRole);
            VariableStatic.VariableCommon.OnChangeCommonOwnAddressPC += new VariableStatic.VariableCommon.ChangeCommonEventHandler(ChangeAddressPC);
            VariableStatic.VariableCommon.OnChangeCommonOwnNumber += new VariableStatic.VariableCommon.ChangeCommonEventHandler(ChangeOwnNumber);

            //table_IRI_FRCh.dgvIRI_FRCh.dgv.SelectionChanged += OnDataGridViewSelectionChanged;

            ///Изменение типов соединения
            VariableStatic.VariableConnection.OnChangeTypeASP += ChangeTypeConnectionASP;
            VariableStatic.VariableConnection.OnChangeTypeHS += ChangeTypeConnectionHS;
            VariableStatic.VariableConnection.OnChangeTypePostControl += ChangeTypeConnectionPC;

            //AppDomain.CurrentDomain.FirstChanceException += CurrentDomain_FirstChanceException;
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;

            //Событие для отображения сообщений от АСП
            messASP.sText = new List<string>();
            messASP.sTime = new List<string>();
            if (objChangeMessage.IsLoaded)
                VariableWork.aWPtoBearingDSPprotocolNew.MasterSlaveSendTextMessageUpdate += aWPtoBearingDSPprotocolNew_MasterSlaveSendTextMessageUpdate;
            else
                VariableWork.aWPtoBearingDSPprotocolNew.MasterSlaveSendTextMessageUpdate += ClientASP_OnReceive_TEXT_MESSAGE;
        }

        void aWPtoBearingDSPprotocolNew_AntennaDirectionsMessageUpdate(Protocols.AntennaDirectionsMessage answer)
        {
            USR_DLL.TDirectionAntennas DirectionAntennas = new USR_DLL.TDirectionAntennas();
            DirectionAntennas.ARD1 = answer.antennaDirections.Ard1;
            DirectionAntennas.ARD2 = answer.antennaDirections.Ard2;
            DirectionAntennas.ARD3 = answer.antennaDirections.Ard3;
            DirectionAntennas.compass = answer.antennaDirections.Compass;
            DirectionAntennas.LPA13 = answer.antennaDirections.Lpa1_3;
            DirectionAntennas.LPA24 = answer.antennaDirections.Lpa2_4;

            variableWork.DirectionAntennas = DirectionAntennas;

            variableCommon.ARD1 = answer.antennaDirections.Ard1;
            variableCommon.ARD2 = answer.antennaDirections.Ard2;
            variableCommon.ARD3 = answer.antennaDirections.Ard3;
            variableCommon.Compass = answer.antennaDirections.Compass;
            variableCommon.LPA1_3 = answer.antennaDirections.Lpa1_3;
            variableCommon.LPA2_4 = answer.antennaDirections.Lpa2_4;

            SendDirectionAntennasToMap(variableWork.DirectionAntennas);
        }

       
        void aWPtoBearingDSPprotocolNew_SetTimeUpdate(Protocols.SetTimeRequest answer)
        {
            SYSTEMTIME time = new SYSTEMTIME();
            DateTime dt = DateTime.Now;
            time.wYear = (short)dt.Year;
            time.wMonth = (short)dt.Month;
            time.wDayOfWeek = (short)dt.DayOfWeek;
            time.wDay = (short)dt.Day;
            time.wHour = answer.Hour;
            time.wMinute = answer.Minute;
            time.wSecond = answer.Second;
            time.wMilliseconds = answer.Millisecond;
            var b = SetSystemTime(ref time);
        }


        void aWPtoBearingDSPprotocolNew_GetSettingsUpdate(Protocols.ModeMessage answer)
        {
            switch (answer.Mode)
            {
                case DspDataModel.Tasks.DspServerMode.Stop:
                   Invoke((MethodInvoker)(() => bRegimeStop_Click(this, null) ));
                    break;
                case DspDataModel.Tasks.DspServerMode.RadioIntelligence:
                case DspDataModel.Tasks.DspServerMode.RadioIntelligenceWithDf:
                     Invoke((MethodInvoker)(() =>bRegimeRecon_Click(this, null) ));
                    break;
                case DspDataModel.Tasks.DspServerMode.RadioJammingFrs:
                case DspDataModel.Tasks.DspServerMode.RadioJammingAfrs:
                case DspDataModel.Tasks.DspServerMode.RadioJammingFrsAuto:
                case DspDataModel.Tasks.DspServerMode.RadioJammingFhss:
                     Invoke((MethodInvoker)(() =>bRegimeSuppr_Click(this, null) ));
                    break;
            }
        }

        void aWPtoBearingDSPprotocolNew_SearchFhssMessageUpdate(Protocols.SearchFhssMessage answer)
        {
            variableIntellegence.DetectionPPRCH = answer.SearchFhss;
        }

        async void VariableIntellegence_OnChangeDetectionPPRCH()
        {
            var answerSetSearchFHSS = await VariableWork.aWPtoBearingDSPprotocolNew.SetSearchFHSS(variableIntellegence.DetectionPPRCH);
        }

        //Координаты Карта->АРМ
        async void serviceMap_MapCurrentCoordsUpdated(object sender, USR_DLL.TCoordsGNSS[] CoordsGNSS)
        {
            //variableWork.CoordsGNSS = CoordsGNSS;

            if (CoordsGNSS.Count() == 1 )
            {
                variableCommon.OwnLatitude = CoordsGNSS[0].Lat.ToString();
                variableCommon.OwnLongitude = CoordsGNSS[0].Lon.ToString();

                variableWork.CoordsGNSS[0].Lat = CoordsGNSS[0].Lat;
                variableWork.CoordsGNSS[0].Lon = CoordsGNSS[0].Lon;

                //1.2.11
                var answer = await VariableWork.aWPtoBearingDSPprotocolNew.SetStationLocation(0, CoordsGNSS[0].Lat, CoordsGNSS[0].Lon, Convert.ToInt16(CoordsGNSS[0].Alt), true);
            }
            if (CoordsGNSS.Count() == 2)
            {
                variableCommon.JammerLinkedLatitude = CoordsGNSS[1].Lat.ToString();
                variableCommon.JammerLinkedLongitude = CoordsGNSS[1].Lon.ToString();

                variableWork.CoordsGNSS[1].Lat = CoordsGNSS[1].Lat;
                variableWork.CoordsGNSS[1].Lon = CoordsGNSS[1].Lon;

                //1.2.11
                var answer = await VariableWork.aWPtoBearingDSPprotocolNew.SetStationLocation(1, CoordsGNSS[1].Lat, CoordsGNSS[1].Lon, Convert.ToInt16(CoordsGNSS[1].Alt), true);
            }
            if (CoordsGNSS.Count() == 3)
            {
                variableCommon.PCLatitude = CoordsGNSS[2].Lat.ToString();
                variableCommon.PCLongitude = CoordsGNSS[2].Lon.ToString();

                variableWork.CoordsGNSS[2].Lat = CoordsGNSS[2].Lat;
                variableWork.CoordsGNSS[2].Lon = CoordsGNSS[2].Lon;
            }
            //1.2.11
            //var answer = await VariableWork.aWPtoBearingDSPprotocolNew.SetStationLocation(0, CoordsGNSS[0].Lat, CoordsGNSS[0].Lon, Convert.ToInt16(CoordsGNSS[0].Alt), !(Convert.ToBoolean(variableCommon.UseGNSS)));
        }

        public void ThreadFunc()
        {
            Thread.Sleep(1000);
            //SendCheckGNSSToMap(variableCommon.UseGNSS);
            SendCheckGNSSToMap(1);
            Thread.Sleep(1000);
            SendCurrentCoordsToMap(variableWork.CoordsGNSS);
            Thread.Sleep(1000);

            AntennaDirectionsToMap();
        }

        private void AntennaDirectionsToMap()
        {
            USR_DLL.TDirectionAntennas DirectionAntennas = new USR_DLL.TDirectionAntennas();
            DirectionAntennas.ARD1 = variableCommon.ARD1;
            DirectionAntennas.ARD2 = variableCommon.ARD2;
            DirectionAntennas.ARD3 = variableCommon.ARD3;
            DirectionAntennas.compass = variableCommon.Compass;
            DirectionAntennas.LPA13 = variableCommon.LPA1_3;
            DirectionAntennas.LPA24 = variableCommon.LPA2_4;

            variableWork.DirectionAntennas = DirectionAntennas;

            SendDirectionAntennasToMap(variableWork.DirectionAntennas);
        }

        public void TaskFunc()
        {
            Task.Delay(1000);
            SendCheckGNSSToMap(variableCommon.UseGNSS);
            Task.Delay(1000);
            SendCurrentCoordsToMap(variableWork.CoordsGNSS);
        }

        void serviceMap_MapIsOpenUpdated(object sender, bool mapIsOpen)
        {
            blMapExist = mapIsOpen;
            //if (mapIsOpen == true && variableCommon.UseGNSS == 1)
            if (mapIsOpen == true)
            {
                Thread myThread = new Thread(new ThreadStart(ThreadFunc));
                myThread.Start(); // запускаем поток

                //Task task = new Task(TaskFunc);
                //task.Start();
                //task.Wait();
                //SendCheckGNSSToMap(variableCommon.UseGNSS);
                //SendCurrentCoordsToMap(variableWork.CoordsGNSS);
            }
        }

       

        void VariableCommon_OnChangeCommonUseGNSS()
        {
            //SendCheckGNSSToMap(variableCommon.UseGNSS);
            //SendCheckGNSSToMap(true);
        }

        //private object threadLock1 = new object();
        //void CurrentDomain_FirstChanceException(object sender, System.Runtime.ExceptionServices.FirstChanceExceptionEventArgs e)
        //{
        //    lock (threadLock1)
        //    {
        //        System.IO.StreamWriter sw = new System.IO.StreamWriter("LogError.txt", true, System.Text.Encoding.Default);
        //        sw.WriteLine(e.Exception.ToString());
        //        sw.WriteLine(e.Exception.StackTrace.ToString());
        //        sw.WriteLine();
        //        sw.Close();
        //    }
        //}

        private object threadLock2 = new object();
        void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            lock (threadLock2)
            {
                System.IO.StreamWriter sw = new System.IO.StreamWriter("LogError2.txt", true, System.Text.Encoding.Default);
                DateTime dt = DateTime.Now;
                sw.WriteLine(dt.Day + "." + dt.Month + "." + dt.Year + " " + dt.Hour + ":" + dt.Minute + ":" + dt.Second);
                sw.WriteLine(e.ExceptionObject.ToString());
                sw.WriteLine();
                sw.Close();
            }
        }

        //Обработка события смены языка
        private void VariableCommon_OnChangeCommonLanguage()
        {
            VariableStatic.VariableCommon variableCommon = new VariableStatic.VariableCommon();


            if (variableCommon.TypeStation == 1)//az
            {
                if (variableCommon.Operator == 0)
                {
                    switch (variableCommon.Language)
                    {
                        case 0:                       //rus
                            controlAR6000.LanguageChooser((int)variableCommon.Language);
                            break;

                        case 1:                      //eng

                            break;

                        case 2:                     //az
                            controlAR6000.LanguageChooser((int)variableCommon.Language);
                            break;

                        default:                    //По умолчанию rus
                            controlAR6000.LanguageChooser((int)variableCommon.Language);
                            break;
                    }

                }
                if (variableCommon.Operator == 1)
                {
                    switch (variableCommon.Language)
                    {
                        case 0:                       //rus
                            controlARDV1.LanguageChooser((int)variableCommon.Language);
                            break;

                        case 1:                      //eng

                            break;

                        case 2:                     //az
                            controlARDV1.LanguageChooser((int)variableCommon.Language);
                            break;

                        default:                    //По умолчанию rus
                            controlARDV1.LanguageChooser((int)variableCommon.Language);
                            break;
                    }
                }
            }
        }


        void aWPtoBearingDSPprotocolNew_StationLocationMessageUpdate(Protocols.StationLocationMessage answer)
        {
            if (InvokeRequired)
            {
                BeginInvoke((MethodInvoker)(()
                      => aWPtoBearingDSPprotocolNew_StationLocationMessageUpdate(answer)));
                return;
            }

            if (variableCommon.Operator == 0)
            {
                if (answer.Station == DspDataModel.TargetStation.Current)
                {
                    USR_DLL.TCoordsGNSS temp = new USR_DLL.TCoordsGNSS();
                    temp.Lat = answer.Latitude;
                    temp.Lon = answer.Longitude;

                    if (answer.Latitude >= 0)
                        temp.signLat = 0;
                    else temp.signLat = 1;

                    if (answer.Longitude >= 0)
                        temp.signLon = 0;
                    else temp.signLon = 1;

                    variableWork.CoordsGNSS[0] = temp;

                    //SendCheckGNSSToMap(variableCommon.UseGNSS);
                    if (variableCommon.UseGNSS == 1)
                    {
                        if (!(variableCommon.OwnLatitude == variableWork.CoordsGNSS[0].Lat.ToString()
                            && variableCommon.OwnLongitude == variableWork.CoordsGNSS[0].Lon.ToString()))
                        {

                            variableCommon.OwnLatitude = variableWork.CoordsGNSS[0].Lat.ToString();
                            variableCommon.OwnLongitude = variableWork.CoordsGNSS[0].Lon.ToString();

                            //отправить на карту
                            SendCurrentCoordsToMap(variableWork.CoordsGNSS);

                            //отправить на сервер
                            VariableDynamic.VariableWork.aWPtoBearingDSPprotocolNew.SetStationLocation(0, variableWork.CoordsGNSS[0].Lat, variableWork.CoordsGNSS[0].Lon, answer.Altitude, true);
                        }
                    }
                }
                else
                {
                    USR_DLL.TCoordsGNSS temp = new USR_DLL.TCoordsGNSS();

                    temp.Lat = answer.Latitude;
                    temp.Lon = answer.Longitude;

                    if (answer.Latitude >= 0)
                        temp.signLat = 0;
                    else temp.signLat = 1;

                    if (answer.Longitude >= 0)
                        temp.signLon = 0;
                    else temp.signLon = 1;

                    variableWork.CoordsGNSS[1] = temp;

                    variableCommon.JammerLinkedLatitude = variableWork.CoordsGNSS[1].Lat.ToString();
                    variableCommon.JammerLinkedLongitude = variableWork.CoordsGNSS[1].Lon.ToString();
                    variableCommon.JammerLinkedHeight = answer.Altitude.ToString();

                    //отправить на карту
                    SendCurrentCoordsToMap(variableWork.CoordsGNSS);

                    //отправить на сервер
                    VariableDynamic.VariableWork.aWPtoBearingDSPprotocolNew.SetStationLocation(1, variableWork.CoordsGNSS[1].Lat, variableWork.CoordsGNSS[1].Lon, answer.Altitude, true);
                }
            }
        }

        void VariableWork_OnChangeCoordsIRI(USR_DLL.TCoordsIRI[] coordsIRI)
        {
            SendCoordsIRIToMap(coordsIRI);
        }

        void VariableWork_OnChangeDirectionAntennas(USR_DLL.TDirectionAntennas directionAntennas)
        {
            SendDirectionAntennasToMap(directionAntennas);
        }

        //void VariableWork_OnChangeCoordsIRI_PPRCh(USR_DLL.TCoordsIRI_PPRCh[] coordsIRI_PPRCh)
        //{
        //    SendCoordsIRI_PPRChToMap(coordsIRI_PPRCh);
        //}
        void VariableWork_OnChangeCoordsIRI_PPRCh(List<USR_DLL.TCoordsIRI_PPRCh> coordsIRI_PPRCh)
        {
            SendCoordsIRI_PPRChToMap(coordsIRI_PPRCh);
        }

        void VariableWork_OnChangePelengsIRI_PPRCh(USR_DLL.TCoordsIRI_PPRCh[] pelengsIRI_PPRCh)
        {
            SendPelengsIRI_PPRChToMap(pelengsIRI_PPRCh);
        }

        void VariableWork_OnChangeFrequencyARONE(long iFreq)
        {
            try
            {
                if (bAOR.BackColor == Color.Red) return;
                if (controlARONE.radioButton2.Checked) return;
                if ((iFreq > 3300000000) || (iFreq < 30000)) { return; }

                string mesFRQ = "RF" + iFreq.ToString().PadLeft(10, '0');

                controlARONE.FreqSetToAOR(iFreq);

                //ComPort.Write(mesFRQ+"\r\n");
                //classLibrary_ARONE.SendToArone(mesFRQ);
                //if  (radioButton1.Checked)
                //    classLibrary_ARONE.FrequencySet(iFreq / 1000000d);
            }
            catch { }
        }
        void VariableWork_OnChangeFrequencyAR6000(long iFreq)
        {
            try
            {
                if (bAOR.BackColor == Color.Red) return;
                if (controlAR6000.radioButton2.Checked) return;
                if ((iFreq > 6000000000) || (iFreq < 30000)) { return; }
                controlAR6000.FreqSetToAOR(iFreq);


                //classLibrary_ARONE.FrequencySet(iFreq / 1000000);
            }
            catch { }

        }
        void VariableWork_OnChangeFrequencyARDV1(long iFreq)
        {
            try
            {

                if (bAOR.BackColor == Color.Red) return;
                if (controlAR6000.radioButton2.Checked) return;
                if ((iFreq > 1300000000) || (iFreq < 100000)) { return; }
                controlARDV1.FreqSetToAOR(iFreq);


                //if ((iFreq > 6000000000) || (iFreq < 30000)) { return; }

                //classLibrary_ARONE.FrequencySet(iFreq / 1000000);
            }
            catch { }

        }

        private void button15_Click(object sender, EventArgs e)                     //open Orientation1
        {
            if (variableCommon.TypeStation == 0)
            {
                if (OrientationBY == null || OrientationBY.IsDisposed)
                {
                    OrientationBY = new WndProject();
                    OrientationBY.Show();
                    try
                    {
                        OrientationBY.OnChangeKursovojUgol += new WndProject.ByteEventHandler1(GetKursovojUgol);
                    }
                    catch { }
                }
                else
                {
                    OrientationBY.Show();
                }
                OpenFormOrient();
            }
            if (variableCommon.TypeStation == 1)
            {
                if (OrientationAZ == null || OrientationAZ.IsDisposed)
                {
                    OrientationAZ = new OrientationAZer();
                    OrientationAZ.Show();
                    try
                    {
                        OrientationAZ.OnChangeKursovojUgol += new OrientationAZer.ByteEventHandler1(GetKursovojUgol);
                    }
                    catch { }
                }
                else
                {
                    OrientationAZ.Show();
                }
                OpenFormOrient();
            }
            if (variableCommon.TypeStation == 2)
            {
                if (OrientationBY == null || OrientationBY.IsDisposed)
                {
                    OrientationBY = new WndProject();
                    OrientationBY.Show();
                    try
                    {
                        OrientationBY.OnChangeKursovojUgol += new WndProject.ByteEventHandler1(GetKursovojUgol);
                    }
                    catch { }
                }
                else
                {
                    OrientationBY.Show();
                }
                OpenFormOrient();
            }

        }

      

        private void CloseOrientForm()
        {
            if (variableCommon.TypeStation == 0)
                try
                {
                    OrientationBY.OnChangeKursovojUgol -= new WndProject.ByteEventHandler1(GetKursovojUgol);
                }
                catch { }
            if (variableCommon.TypeStation == 1)
                try
                {
                    OrientationAZ.OnChangeKursovojUgol -= new OrientationAZer.ByteEventHandler1(GetKursovojUgol);
                }
                catch { }

            if (variableCommon.TypeStation == 2)
                try
                {
                    OrientationBY.OnChangeKursovojUgol -= new WndProject.ByteEventHandler1(GetKursovojUgol);
                }
                catch { }
        }

        private void OpenFormOrient()
        {
            if (variableCommon.TypeStation == 0)
                try
                {
                    OrientationBY.OnChangeKursovojUgol += new WndProject.ByteEventHandler1(GetKursovojUgol);
                }
                catch { }
            if (variableCommon.TypeStation == 1)
                try
                {
                    OrientationAZ.OnChangeKursovojUgol += new OrientationAZer.ByteEventHandler1(GetKursovojUgol);
                }
                catch { }

            if (variableCommon.TypeStation == 2)
                try
                {
                    OrientationBY.OnChangeKursovojUgol += new WndProject.ByteEventHandler1(GetKursovojUgol);
                }
                catch { }
        }

        int currentAngle = 0;
        private async void GetKursovojUgol(int Ugol)
        {
            if (variableCommon.Operator == 1)
            {
                //Ugol = Ugol - variableIntellegence.ZeroVibrAngle;
                //if (Ugol < 0) Ugol = Ugol + 360;
                Ugol = Ugol + variableIntellegence.ZeroVibrAngle;
                while (Ugol >= 360) Ugol = Ugol - 360;
                variableIntellegence.RelativeBearing = Ugol;
                if (currentAngle != Ugol)
                {
                    currentAngle = Ugol;
                    var answer = await VariableWork.aWPtoBearingDSPprotocolNew.SetDirectionCorrection(currentAngle, false);
                }
            }
        }

        void aWPtoBearingDSPprotocolNew_DirectionCorrectionMessageUpdate(Protocols.DirectionCorrectionMessage answer)
        {
            if (variableCommon.Operator == 0)
                if (variableIntellegence.AutoRelativeBearing == 1)
                {
                    variableIntellegence.RelativeBearing = answer.DirectionCorrection / 10;
                }
        }

        async void VariableWork_OnChangeCoordsGNSS(USR_DLL.TCoordsGNSS[] coordsGNSS)
        {
            //Console.Beep();

            //variableCommon.OwnLatitude = variableWork.CoordsGNSS[0].Lat.ToString();
            //variableCommon.OwnLongitude = variableWork.CoordsGNSS[0].Lon.ToString();

            //if (Convert.ToBoolean(variableCommon.UseGNSS))
            //    SendCurrentCoordsToMap(coordsGNSS);

            //var answer = await VariableWork.aWPtoBearingDSPprotocolNew.SetStationLocation(coordsGNSS[0].Lat, coordsGNSS[0].Lon, Convert.ToInt16(coordsGNSS[0].Alt ), Convert.ToBoolean(variableCommon.UseGNSS));
        }

        void aWPtoBearingDSPprotocolNew_ShaperStateUpdate(Protocols.ShaperStateUpdateEvent answer)
        {
            if (answer.IsShaperConnected)
            {
                buttonConnectFPS.BackColor = Color.Green;
            }
            else
            {
                buttonConnectFPS.BackColor = Color.Red;
            }

        }

        void aWPtoBearingDSPprotocolNew_IsWrite(bool isWrite)
        {
            if (isWrite)
            {
                stateIndicateWrite.SetIndicateOn();
            }
        }

        void aWPtoBearingDSPprotocolNew_IsRead(bool isRead)
        {
            if (isRead)
            {
                stateIndicateRead.SetIndicateOn();
            }
        }

        private void ChangeMainButtonsColor(byte mode)
        {
            switch (mode)
            {
                case 0:
                    {
                        bRegimeStop.Image = Properties.Resources.greenStop;
                        bRegimeStop.ForeColor = Color.Green;

                        bRegimeRecon.Image = Properties.Resources.grayRecon;
                        bRegimeRecon.ForeColor = Color.Black;

                        bRegimeSuppr.Image = Properties.Resources.graySuppr;
                        bRegimeSuppr.ForeColor = Color.Black;
                    }
                    break;

                case 1:
                case 2:
                    {
                        bRegimeStop.Image = Properties.Resources.grayStop;
                        bRegimeStop.ForeColor = Color.Black;

                        bRegimeRecon.Image = Properties.Resources.greenRecon;
                        bRegimeRecon.ForeColor = Color.Green;

                        bRegimeSuppr.Image = Properties.Resources.graySuppr;
                        bRegimeSuppr.ForeColor = Color.Black;
                    }
                    break;

                case 3:
                case 4:
                case 5:
                case 6:
                    {
                        bRegimeStop.Image = Properties.Resources.grayStop;
                        bRegimeStop.ForeColor = Color.Black;

                        bRegimeRecon.Image = Properties.Resources.grayRecon;
                        bRegimeRecon.ForeColor = Color.Black;

                        bRegimeSuppr.Image = Properties.Resources.greenSuppr;
                        bRegimeSuppr.ForeColor = Color.Green;
                    }
                    break;

                default:
                    break;

            }
        }

        private void ChangeEnabledMainButtonsColor(byte mode)
        {
            switch (mode)
            {
                case 0:
                    {
                        ChangeEnabledMainButtonColor(bRegimeStop, Properties.Resources.greenStop, Color.Green);
                        ChangeEnabledMainButtonColor(bRegimeRecon, Properties.Resources.grayRecon, Color.Black);
                        ChangeEnabledMainButtonColor(bRegimeSuppr, Properties.Resources.graySuppr, Color.Black);
                    }
                    break;

                case 1:
                case 2:
                    {
                        ChangeEnabledMainButtonColor(bRegimeStop, Properties.Resources.grayStop, Color.Black);
                        ChangeEnabledMainButtonColor(bRegimeRecon, Properties.Resources.greenRecon, Color.Green);
                        ChangeEnabledMainButtonColor(bRegimeSuppr, Properties.Resources.graySuppr, Color.Black);
                    }
                    break;

                case 3:
                case 4:
                case 5:
                case 6:
                    {
                        ChangeEnabledMainButtonColor(bRegimeStop, Properties.Resources.grayStop, Color.Black);
                        ChangeEnabledMainButtonColor(bRegimeRecon, Properties.Resources.grayRecon, Color.Black);
                        ChangeEnabledMainButtonColor(bRegimeSuppr, Properties.Resources.greenSuppr, Color.Green);
                    }
                    break;

                default:
                    break;

            }
        }

        private void ChangeEnabledMainButtonColor(Button button, Image image, Color color)
        {
            var g = button.CreateGraphics();

            Brush brush = new SolidBrush(Color.FromName("Control"));
            var rClear = button.DisplayRectangle;
            rClear = new Rectangle(rClear.X + 3, rClear.Y + 3, rClear.Width - 6, rClear.Height - 6);
            g.FillRectangle(brush, rClear);

            var rImage = new Rectangle(rClear.X, rClear.Y, rClear.Height, rClear.Height);
            if (image.Width < rImage.Width)
            {
                var dif = rImage.Width - image.Width;
                rImage = new Rectangle(rImage.X + dif, rImage.Y + dif, rImage.Width - dif * 2, rImage.Height - dif * 2);
            }
            g.DrawImage(image, rImage);

            var rString1 = new Rectangle(rClear.X + rClear.Height, rClear.Y, rClear.Width - rClear.Height, rClear.Height);
            var rString2 = rClear;

            var sf = new StringFormat
            {
                Alignment = StringAlignment.Far,
                //Alignment = StringAlignment.Near,
                //Alignment = StringAlignment.Center,
                LineAlignment = StringAlignment.Center
            };

            g.DrawString(button.Text, button.Font, new SolidBrush(color), rString1, sf);
        }

        void VariableWork_OnChangeRegime()
        {
            if (variableCommon.Operator == 0)
            {
                if (variableCommon.Role == 0 || variableCommon.Role == 1)
                    ChangeMainButtonsColor(variableWork.Regime);
                else
                    ChangeEnabledMainButtonsColor(variableWork.Regime);
            }
            if (variableCommon.Operator == 1)
            {
                ChangeEnabledMainButtonsColor(variableWork.Regime);
            }

            if (variableWork.Regime >= 1 && variableWork.Regime <= 2)
            {
                variableIntellegence.Bearing = (byte)(variableWork.Regime - 1);
                panoramaControl1.SetSectorsAndRanges(variableWork.RangeSectorReconOwn);
            }
            if (variableWork.Regime >= 3 && variableWork.Regime <= 5)
            {
                variableSuppression.RegimeRadioSuppr = (byte)(variableWork.Regime - 3);
                //panoramaControl1.SetSectorsAndRanges(variableWork.RangeSectorSupprOwn);
                VariableWork_OnChangeSupprFWS_Own();

            }
            if (variableWork.Regime == 6)
            {
                variableSuppression.RegimeRadioSuppr = (byte)(variableWork.Regime - 3);
               // panoramaControl1.SetSectorsAndRanges(variableWork.RangeSectorSupprOwn);
                VariableWork_OnChangeDistribFHSS_RPOwn();
            }
        }



        void OnDataGridViewSelectionChanged(object sender, EventArgs e)
        {
            try
            {
                var iri = table_IRI_FRCh.GetCurrentIRI();
                if (iri.iFreq == 0 || iri.iQ1 == -1)
                {
                    return;
                }
                USR_DLL.TDirections tDirections = new USR_DLL.TDirections();
                tDirections.iQ1 = iri.iQ1 / 10;
                if (iri.iQ2 != -1)
                {
                    tDirections.iQ2 = iri.iQ2 / 10;
                }
                else
                {
                    tDirections.iQ2 = iri.iQ2;
                }
                SendCurrentDirectionToMap(tDirections);
            }
            catch { }
        }

        //private void FiltersMessages(Protocols.FiltersMessage answer)
        //{
        //}

        void VariableWork_OnChangeDataADSBReceiver(USR_DLL.TDataADSBReceiver[] tDadaADSBReceiver)
        {
            //GrozaMap.MapForm.f_lll(tDadaADSBReceiver);

            if (blMapCheck == true)
            {
                blMapExist = false;

                string name = "GrozaMap";
                System.Diagnostics.Process[] pr2 = System.Diagnostics.Process.GetProcesses();

                bool blProc = false;
                int i = 0;
                while (i < pr2.Length)
                {
                    if (pr2[i].ProcessName == name)
                    {
                        blProc = true;
                        i = pr2.Length;

                        blMapExist = true;
                    }
                    i++;
                }

                blMapCheck = false;
            }

            if (blMapExist == true)
            {


                try
                {
                    service.AirPlane(tDadaADSBReceiver);
                }
                catch
                {
                    //создаем клиент
                    var scf = new ChannelFactory<Contract.IService>(new NetTcpBinding(), "net.tcp://localhost:8000");
                    service = scf.CreateChannel();

                    try
                    {
                        service.AirPlane(tDadaADSBReceiver);
                    }
                    catch
                    { }
                }
            }
        }

        private void SendCurrentDirectionToMap(USR_DLL.TDirections tDirections)
        {
            if (blMapExist)
                TrySendToMap(() => service.CurrentDirection(tDirections));
        }

        private void SendCurrentCoordsToMap(USR_DLL.TCoordsGNSS[] coordsGNSS)
        {
            if (blMapExist)
            {
                TrySendToMap(() => service.CurrentCoords(coordsGNSS));
            }
        }

        private void SendCoordsIRIToMap(USR_DLL.TCoordsIRI[] coordsIRI)
        {
            if (blMapExist)
                TrySendToMap(() => service.CoordsIRI(coordsIRI));
        }

        private void SendDirectionAntennasToMap(USR_DLL.TDirectionAntennas directionAntennas)
        {
            if (blMapExist)
                TrySendToMap(() => service.DirectionAntennas(directionAntennas));
        }

        private void SendCoordsIRI_PPRChToMap(List<USR_DLL.TCoordsIRI_PPRCh> coordsIRI_PPRCh)
        {
            if (blMapExist)
                TrySendToMap(() => service.CoordsIRI_PPRCh(coordsIRI_PPRCh));
        }

        private void SendPelengsIRI_PPRChToMap(USR_DLL.TCoordsIRI_PPRCh[] pelengsIRI_PPRCh)
        {
            if (blMapExist)
                TrySendToMap(() => service.PelengsIRI_PPRCh(pelengsIRI_PPRCh));
        }

        private void SendCheckGNSSToMap(byte bCheck)
        {
            if (blMapExist)
                TrySendToMap(() => service.CheckGNSS(bCheck));
        }

        private void TrySendToMap(Action action)
        {
            Task.Run(() =>
            {
                try
                {
                    action();
                }
                catch
                {
                    //создаем клиент
                    var scf = new ChannelFactory<Contract.IService>(new NetTcpBinding(), "net.tcp://localhost:8000");
                    service = scf.CreateChannel();

                    try
                    {
                        action();
                    }
                    catch
                    {
                    }
                }
            });
        }

        //Обработка события обновления ИРИ ФРЧ на РП Own
        void VariableWork_OnChangeSupprFWS_Own()
        {
            if (VariableDynamic.VariableWork.aWPtoBearingDSPprotocolNew != null)
            {
                try
                {
                    if (variableWork.Regime != 6)
                    {
                        Protocols.FRSJammingSetting[] fRSJammingSettings = new Protocols.FRSJammingSetting[variableWork.SupprFWS_Own.Length];
                        for (int i = 0; i < variableWork.SupprFWS_Own.Length; i++)
                        {
                            fRSJammingSettings[i].DeviationCode = variableWork.SupprFWS_Own[i].bDeviation;
                            fRSJammingSettings[i].DurationCode = variableWork.SupprFWS_Own[i].bDuration;
                            fRSJammingSettings[i].Frequency = variableWork.SupprFWS_Own[i].iFreq; // ?
                            fRSJammingSettings[i].Id = variableWork.SupprFWS_Own[i].iID;
                            fRSJammingSettings[i].Liter = variableWork.SupprFWS_Own[i].bLetter;
                            fRSJammingSettings[i].ManipulationCode = variableWork.SupprFWS_Own[i].bManipulation;
                            fRSJammingSettings[i].ModulationCode = variableWork.SupprFWS_Own[i].bModulation;
                            fRSJammingSettings[i].Priority = variableWork.SupprFWS_Own[i].bPrior;
                            fRSJammingSettings[i].Threshold = Convert.ToByte((-1) * variableWork.SupprFWS_Own[i].sLevel);
                            if (variableWork.SupprFWS_Own[i].sBearing == -1)
                            {
                                fRSJammingSettings[i].Direction = 0;
                            }
                            else
                            {
                                fRSJammingSettings[i].Direction = variableWork.SupprFWS_Own[i].sBearing;
                            }
                        }
                        //1.2.11
                        VariableDynamic.VariableWork.aWPtoBearingDSPprotocolNew.SetFRSJamming((byte)0,fRSJammingSettings);
                    }
                }
                catch (SystemException)
                { }
            }
        }

        //Обработка события обновления ИРИ ППРЧ на РП Own
        void VariableWork_OnChangeDistribFHSS_RPOwn()
        {
            if (VariableDynamic.VariableWork.aWPtoBearingDSPprotocolNew != null)
            {
                if (variableWork.Regime == 0 || variableWork.Regime == 1 || variableWork.Regime == 2)
                {
                    try
                    {
                        int Duration = 1000;
                        byte bCodeFFT = 4;
                        int N = variableWork.DistribFHSS_RPOwn.Length;

                        Protocols.FhssJammingSetting[] fhssJammingSettings = new Protocols.FhssJammingSetting[N];
                        for (int i = 0; i < N; i++)
                        {
                            fhssJammingSettings[i].DeviationCode = variableWork.DistribFHSS_RPOwn[i].bDeviation;
                            fhssJammingSettings[i].EndFrequency = variableWork.DistribFHSS_RPOwn[i].iFreqMax;
                            fhssJammingSettings[i].Id = variableWork.DistribFHSS_RPOwn[i].iID;
                            fhssJammingSettings[i].ManipulationCode = variableWork.DistribFHSS_RPOwn[i].bManipulation;
                            fhssJammingSettings[i].ModulationCode = variableWork.DistribFHSS_RPOwn[i].bModulation;
                            fhssJammingSettings[i].StartFrequency = variableWork.DistribFHSS_RPOwn[i].iFreqMin;
                            fhssJammingSettings[i].Threshold = Convert.ToByte((-1) * variableWork.DistribFHSS_RPOwn[i].sLevel);

                            Duration = variableWork.DistribFHSS_RPOwn[i].iDuration;
                            bCodeFFT = variableWork.DistribFHSS_RPOwn[i].bCodeFFT;

                            int len = variableWork.DistribFHSS_RPExcludeOwn.Length;
                            fhssJammingSettings[i].FixedRadioSourceCount = len;
                            fhssJammingSettings[i].FixedRadioSources = new Protocols.FhssFixedRadioSource[len];
                            for (int j = 0; j < len; j++)
                            {
                                fhssJammingSettings[i].FixedRadioSources[j].Frequency = variableWork.DistribFHSS_RPExcludeOwn[j].iFreqExclude;
                                fhssJammingSettings[i].FixedRadioSources[j].Bandwidth = variableWork.DistribFHSS_RPExcludeOwn[j].iWidthExclude;
                            }
                        }
                        //1.2.11
                        VariableWork.aWPtoBearingDSPprotocolNew.SetFhssJamming(0,Duration, bCodeFFT, fhssJammingSettings);
                    }
                    catch (Exception)
                    { }
                }
            }
        }

        //Обработка события обновления Вырезанных частот ИРИ ППРЧ на РП Own
        void VariableWork_OnChangeDistribFHSS_RPExcludeOwn()
        {
            //VariableWork_OnChangeDistribFHSS_RPOwn();
        }

        //Обработка события обновления ИРИ ФРЧ на РП Linked
        void VariableWork_OnChangeSupprFWS_Linked()
        {
            if (InvokeRequired)
            {
                BeginInvoke((MethodInvoker)(()
                      => VariableWork_OnChangeSupprFWS_Linked()));
                return;
            }

            //1.2.11
            if (VariableDynamic.VariableWork.aWPtoBearingDSPprotocolNew != null)
            {
                try
                {
                    if (variableWork.Regime != 6)
                    {
                        Protocols.FRSJammingSetting[] fRSJammingSettings = new Protocols.FRSJammingSetting[variableWork.SupprFWS_Linked.Length];
                        for (int i = 0; i < variableWork.SupprFWS_Linked.Length; i++)
                        {
                            fRSJammingSettings[i].DeviationCode = variableWork.SupprFWS_Linked[i].bDeviation;
                            fRSJammingSettings[i].DurationCode = variableWork.SupprFWS_Linked[i].bDuration;
                            fRSJammingSettings[i].Frequency = variableWork.SupprFWS_Linked[i].iFreq; // ?
                            fRSJammingSettings[i].Id = variableWork.SupprFWS_Linked[i].iID;
                            fRSJammingSettings[i].Liter = variableWork.SupprFWS_Linked[i].bLetter;
                            fRSJammingSettings[i].ManipulationCode = variableWork.SupprFWS_Linked[i].bManipulation;
                            fRSJammingSettings[i].ModulationCode = variableWork.SupprFWS_Linked[i].bModulation;
                            fRSJammingSettings[i].Priority = variableWork.SupprFWS_Linked[i].bPrior;
                            fRSJammingSettings[i].Threshold = Convert.ToByte((-1) * variableWork.SupprFWS_Linked[i].sLevel);
                            fRSJammingSettings[i].Direction =variableWork.SupprFWS_Linked[i].sBearing;
                        }
                        //1.2.11
                        VariableDynamic.VariableWork.aWPtoBearingDSPprotocolNew.SetFRSJamming((byte)1, fRSJammingSettings);
                    }
                }
                catch (SystemException)
                { }
            }
        }

        //Обработка события обновления ИРИ ППРЧ на РП Linked
        void VariableWork_OnChangeDistribFHSS_RPLinked()
        {
            //1.2.11
            if (VariableDynamic.VariableWork.aWPtoBearingDSPprotocolNew != null)
            {
                if (variableWork.Regime == 0 || variableWork.Regime == 1 || variableWork.Regime == 2)
                {
                    try
                    {
                        int Duration = 1000;
                        byte bCodeFFT = 4;
                        int N = variableWork.DistribFHSS_RPLinked.Length;

                        Protocols.FhssJammingSetting[] fhssJammingSettings = new Protocols.FhssJammingSetting[N];
                        for (int i = 0; i < N; i++)
                        {
                            fhssJammingSettings[i].DeviationCode = variableWork.DistribFHSS_RPLinked[i].bDeviation;
                            fhssJammingSettings[i].EndFrequency = variableWork.DistribFHSS_RPLinked[i].iFreqMax;
                            fhssJammingSettings[i].Id = variableWork.DistribFHSS_RPLinked[i].iID;
                            fhssJammingSettings[i].ManipulationCode = variableWork.DistribFHSS_RPLinked[i].bManipulation;
                            fhssJammingSettings[i].ModulationCode = variableWork.DistribFHSS_RPLinked[i].bModulation;
                            fhssJammingSettings[i].StartFrequency = variableWork.DistribFHSS_RPLinked[i].iFreqMin;
                            fhssJammingSettings[i].Threshold = Convert.ToByte((-1) * variableWork.DistribFHSS_RPLinked[i].sLevel);

                            Duration = variableWork.DistribFHSS_RPLinked[i].iDuration;
                            bCodeFFT = variableWork.DistribFHSS_RPLinked[i].bCodeFFT;

                            int len = variableWork.DistribFHSS_RPExcludeLinked.Length;
                            fhssJammingSettings[i].FixedRadioSourceCount = len;
                            fhssJammingSettings[i].FixedRadioSources = new Protocols.FhssFixedRadioSource[len];
                            for (int j = 0; j < len; j++)
                            {
                                fhssJammingSettings[i].FixedRadioSources[j].Frequency = variableWork.DistribFHSS_RPExcludeLinked[j].iFreqExclude;
                                fhssJammingSettings[i].FixedRadioSources[j].Bandwidth = variableWork.DistribFHSS_RPExcludeLinked[j].iWidthExclude;
                            }
                        }
                        //1.2.11
                        VariableWork.aWPtoBearingDSPprotocolNew.SetFhssJamming(1, Duration, bCodeFFT, fhssJammingSettings);
                    }
                    catch (Exception)
                    { }
                }
            }
        }

        //Обработка события обновления Вырезанных частот ИРИ ППРЧ на РП Linked
        void VariableWork_OnChangeDistribFHSS_RPExcludeLinked()
        {
            //VariableWork_OnChangeDistribFHSS_RPLinked();
        }

        //Обработка события обновления секторов и диапазонов радиоразведки OWN
        void VariableWork_OnChangeRangeSectorReconOwn()
        {
            VariableDynamic.VariableWork.aWPtoBearingDSPprotocolNew.SetSectorsAndRanges(0, 0, variableWork.RangeSectorReconOwn);
        }

        //Обработка события обновления секторов и диапазонов радиоразведки Linked
        void VariableWork_OnChangeRangeSectorReconLinked()
        {
            VariableDynamic.VariableWork.aWPtoBearingDSPprotocolNew.SetSectorsAndRanges(0, 1, variableWork.RangeSectorReconLinked);
        }

        //Обработка события обновления запрещенных частот OWN
        void VariableWork_OnChangeFrequencyRangeForbiddenOwn()
        {
            //1.2.11
            VariableDynamic.VariableWork.aWPtoBearingDSPprotocolNew.SetSpecialFrequencies((byte)0, DspDataModel.FrequencyType.Forbidden, variableWork.FrequencyRangeForbiddenOwn);
        }
        //Обработка события обновления известных частот OWN
        void VariableWork_OnChangeFrequencyRangeKnownOwn()
        {
            //1.2.11
            VariableDynamic.VariableWork.aWPtoBearingDSPprotocolNew.SetSpecialFrequencies((byte)0, DspDataModel.FrequencyType.Known, variableWork.FrequencyRangeKnownOwn);
        }
        //Обработка события обновления важных частот для OWN
        void VariableWork_OnChangeFrequencyRangeImportantOwn()
        {
            //1.2.11
            VariableDynamic.VariableWork.aWPtoBearingDSPprotocolNew.SetSpecialFrequencies((byte)0, DspDataModel.FrequencyType.Important, variableWork.FrequencyRangeImportantOwn);
        }

        //Обработка события обновления запрещенных частот Linked
        void VariableWork_OnChangeFrequencyRangeForbiddenLinked()
        {
            //1.2.11
            VariableDynamic.VariableWork.aWPtoBearingDSPprotocolNew.SetSpecialFrequencies(1, 0, variableWork.FrequencyRangeForbiddenLinked);
        }
        //Обработка события обновления известных частот Linked
        void VariableWork_OnChangeFrequencyRangeKnownLinked()
        {
            //1.2.11
            VariableDynamic.VariableWork.aWPtoBearingDSPprotocolNew.SetSpecialFrequencies(1, 1, variableWork.FrequencyRangeKnownLinked);
        }
        //Обработка события обновления важных частот для Linked
        void VariableWork_OnChangeFrequencyRangeImportantLinked()
        {
            //1.2.11
            VariableDynamic.VariableWork.aWPtoBearingDSPprotocolNew.SetSpecialFrequencies(1, 2, variableWork.FrequencyRangeImportantLinked);
        }



        //Обработка события по клику на галочку Пеленгование
        async void VariableIntellegence_OnChangeBearing()
        {
            if (variableWork.Regime == 1 || variableWork.Regime == 2)
            {
                var answer = await VariableDynamic.VariableWork.aWPtoBearingDSPprotocolNew.SetMode((int)variableIntellegence.Bearing + 1);
                if (answer != null)
                    if (answer.Header.ErrorCode == 0)
                    {
                        variableWork.Regime = Convert.ToByte(variableIntellegence.Bearing + 1);
                    }
            }
        }

        //Обработка события по изменению режима Радиоподавления
        void VariableSuppression_OnChangeRegimeRadioSuppr()
        {
            if (variableWork.Regime == 3 || variableWork.Regime == 4 || variableWork.Regime == 5 || variableWork.Regime == 6)
                bRegimeSuppr.PerformClick();
            /*
            if (variableWork.Regime == 3 || variableWork.Regime == 4 || variableWork.Regime == 5 || variableWork.Regime == 6)
            {
                var answer = await VariableDynamic.VariableWork.aWPtoBearingDSPprotocolNew.SetMode((int)variableSuppression.RegimeRadioSuppr + 3);
                if (answer != null)
                    if (answer.Header.ErrorCode == 0)
                    {
                        variableWork.Regime = Convert.ToByte(variableSuppression.RegimeRadioSuppr + 3);
                    }
            }
             * */
        }

        private void IsConnected(bool isConnected)
        {
            if (isConnected == true)
                bFreqChannel.BackColor = Color.Green;
            if (isConnected == false)
                bFreqChannel.BackColor = Color.Red;
        }

        //Обработка событий для взаимодействия АРМ1-АРМ2

        //Обработка события от Сервера обновления специальных частот 
        void aWPtoBearingDSPprotocolNew_SpecialFrequenciesMessageUpdate(Protocols.SpecialFrequenciesMessage answer)
        {
            if (InvokeRequired)
            {
                BeginInvoke((MethodInvoker)(()
                      => aWPtoBearingDSPprotocolNew_SpecialFrequenciesMessageUpdate(answer)));
                return;
            }

            //1.2.11
            switch (answer.FrequencyType)
            {
                case DspDataModel.FrequencyType.Forbidden:
                    if (answer.Station == DspDataModel.TargetStation.Current)
                    {
                        if (variableWork.FrequencyRangeForbiddenOwn != answer.Frequencies)
                            variableWork.FrequencyRangeForbiddenOwn = answer.Frequencies;
                    }
                    else
                    {
                        if (variableWork.FrequencyRangeForbiddenLinked != answer.Frequencies)
                            variableWork.FrequencyRangeForbiddenLinked = answer.Frequencies;
                    }
                    break;

                case DspDataModel.FrequencyType.Known:
                    if (answer.Station == DspDataModel.TargetStation.Current)
                    {
                        if (variableWork.FrequencyRangeKnownOwn != answer.Frequencies)
                            variableWork.FrequencyRangeKnownOwn = answer.Frequencies;
                    }
                    else
                    {
                        if (variableWork.FrequencyRangeKnownLinked != answer.Frequencies)
                            variableWork.FrequencyRangeKnownLinked = answer.Frequencies;
                    }
                    break;

                case DspDataModel.FrequencyType.Important:
                    if (answer.Station == DspDataModel.TargetStation.Current)
                    {
                        if (variableWork.FrequencyRangeImportantOwn != answer.Frequencies)
                            variableWork.FrequencyRangeImportantOwn = answer.Frequencies;
                    }
                    else
                    {
                        if (variableWork.FrequencyRangeImportantLinked != answer.Frequencies)
                            variableWork.FrequencyRangeImportantLinked = answer.Frequencies;
                    }
                    break;

                default:
                    break;
            }
        }

        private object threadLock = new object();
        void aWPtoBearingDSPprotocolNew_FiltersMessageUpdate(Protocols.FiltersMessage answer)
        {
            lock (threadLock)
            {
                Invoke((MethodInvoker)(() => panoramaControl1.SetThreshold(answer.Threshold)));
            }
        }

        void aWPtoBearingDSPprotocolNew_ModeMessageUpdate(Protocols.ModeMessage answer)
        {
            Invoke((MethodInvoker)(() => variableWork.Regime = (byte)answer.Mode));
        }

        void aWPtoBearingDSPprotocolNew_SectorsAndRangesMessageUpdate(Protocols.SectorsAndRangesMessage answer)
        {
            if (answer.Station == DspDataModel.TargetStation.Current)
            {
                if (answer.RangesType == DspDataModel.RangeType.Intelligence)
                {
                    Invoke((MethodInvoker)(() => variableWork.RangeSectorReconOwn = answer.RangeSectors));
                }
                if (answer.RangesType == DspDataModel.RangeType.RadioJamming)
                {
                    Invoke((MethodInvoker)(() => variableWork.RangeSectorSupprOwn = answer.RangeSectors));
                }
            }
            if (answer.Station == DspDataModel.TargetStation.Linked)
            {
                if (answer.RangesType == DspDataModel.RangeType.Intelligence)
                {
                    Invoke((MethodInvoker)(() => variableWork.RangeSectorReconLinked = answer.RangeSectors));
                }
                if (answer.RangesType == DspDataModel.RangeType.RadioJamming)
                {
                    Invoke((MethodInvoker)(() => variableWork.RangeSectorSupprLinked = answer.RangeSectors));
                }
            }
        }

        void aWPtoBearingDSPprotocolNew_AttenuatorsMessageUpdate(Protocols.AttenuatorsMessage answer)
        {

        }

        void aWPtoBearingDSPprotocolNew_FrsJammingMessageUpdate(Protocols.FrsJammingMessage answer)
        {
            //1.2.11
            USR_DLL.TSupprFWS[] tempSupprFWS = new USR_DLL.TSupprFWS[answer.Settings.Length];
            for (int i = 0; i < answer.Settings.Length; i++)
            {
                tempSupprFWS[i].bDeviation = answer.Settings[i].DeviationCode;
                tempSupprFWS[i].bDuration = answer.Settings[i].DurationCode;
                tempSupprFWS[i].iFreq = answer.Settings[i].Frequency;
                tempSupprFWS[i].iID = answer.Settings[i].Id;
                tempSupprFWS[i].bLetter = answer.Settings[i].Liter;
                tempSupprFWS[i].bManipulation = answer.Settings[i].ManipulationCode;
                tempSupprFWS[i].bModulation = answer.Settings[i].ModulationCode;
                tempSupprFWS[i].bPrior = answer.Settings[i].Priority;
                tempSupprFWS[i].sLevel = Convert.ToInt16((-1) * answer.Settings[i].Threshold);
                tempSupprFWS[i].sBearing = answer.Settings[i].Direction;
            }
            if (answer.Station == DspDataModel.TargetStation.Current)  variableWork.SupprFWS_Own = tempSupprFWS;
            else variableWork.SupprFWS_Linked = tempSupprFWS;
        }

        void aWPtoBearingDSPprotocolNew_FhssJammingMessageUpdate(Protocols.FhssJammingMessage answer)
        {
            //1.2.11
            if (answer.Station == DspDataModel.TargetStation.Current)
                panoramaControl1.InitFHSSonRS(answer.Settings);

            int N = answer.Settings.Length;
            USR_DLL.TDistribFHSS_RP[] temp = new USR_DLL.TDistribFHSS_RP[N];

            for (int i = 0; i < N; i++)
            {
                temp[i].bDeviation = answer.Settings[i].DeviationCode;
                temp[i].iFreqMax = answer.Settings[i].EndFrequency;
                temp[i].iID = answer.Settings[i].Id;
                temp[i].bManipulation = answer.Settings[i].ManipulationCode;
                temp[i].bModulation = answer.Settings[i].ModulationCode;
                temp[i].iFreqMin = answer.Settings[i].StartFrequency;
                temp[i].sLevel = (short)((-1) * answer.Settings[i].Threshold);
                temp[i].bLetter = functions.DefineLetter(answer.Settings[i].StartFrequency, answer.Settings[i].EndFrequency);
                temp[i].iStep = 100;
                temp[i].bCodeFFT = 4;
                temp[i].iDuration = 1000;
            }

            USR_DLL.TDistribFHSS_RPExclude[] temp1 = new USR_DLL.TDistribFHSS_RPExclude[0];;
            for (int i = 0; i < N; i++)
            {
                int M = answer.Settings[i].FixedRadioSourceCount;
                temp1 = new USR_DLL.TDistribFHSS_RPExclude[M];
                for (int j = 0; j < M; j++)
                {
                    temp1[j].iID = j;
                    temp1[j].iFreqExclude = answer.Settings[i].FixedRadioSources[j].Frequency;
                    temp1[j].iWidthExclude = answer.Settings[i].FixedRadioSources[j].Bandwidth;
                }
            }

            if (answer.Station == DspDataModel.TargetStation.Current)
            {
                variableWork.DistribFHSS_RPExcludeOwn = temp1;
                variableWork.DistribFHSS_RPOwn = temp;
                
            }
            else
            {
                variableWork.DistribFHSS_RPExcludeLinked = temp1;
                variableWork.DistribFHSS_RPLinked = temp;
            }
        }

        //1.2.11
        //MainCore
        private async void bFreqChannel_Click(object sender, EventArgs e)
        {
            bFreqChannel.Enabled = false;
            butServerAsp.Enabled = false;

            if (bFreqChannel.BackColor == Color.Green)
            {
                try
                {
                    VariableDynamic.VariableWork.aWPtoBearingDSPprotocolNew.DisconnectFromBearingDSP();
                    butServerAsp.BackColor = Color.Red;
                    butClientAsp.BackColor = Color.Red;
                }
                catch (Exception) 
                {
                    bFreqChannel.Enabled = true;
                    if (variableCommon.Operator == 0)
                        butServerAsp.Enabled = true;
                }
            }
            else
            {
                try
                {
                    await VariableDynamic.VariableWork.aWPtoBearingDSPprotocolNew.ConnectToBearingDSP(variableConnection.DirectionFinderAddress, variableConnection.DirectionFinderPort);

                    //Установка режима работы с эфира
                    var answer00 = await VariableWork.aWPtoBearingDSPprotocolNew.SetReceiversChannel(0);
                    //Console.WriteLine(answer00.Header.Code.ToString() + answer00.Header.ErrorCode.ToString());

                    //Запросы секторов и диапазонов
                    await InitSectorsAndRanges();

                    //Запрос фильтров
                    var answer2 = await VariableDynamic.VariableWork.aWPtoBearingDSPprotocolNew.GetFilters();
                    //Обработка
                    if (answer2 != null)
                        panoramaControl1.SetThreshold(answer2.Threshold);

                    //Запрос режима
                    var answer3 = await VariableDynamic.VariableWork.aWPtoBearingDSPprotocolNew.GetMode();

                    //Обработка
                    if (Convert.ToByte(answer3.Mode) == 3 || Convert.ToByte(answer3.Mode) == 4 || Convert.ToByte(answer3.Mode) == 5)
                    {
                        //1.2.11
                        var answerFreq = await VariableWork.aWPtoBearingDSPprotocolNew.GetFRSJamming(0);
                        //panoramaControl1.
                        variableSuppression.RegimeRadioSuppr = (byte)(Convert.ToByte(answer3.Mode) - 3);
                    }
                    if (Convert.ToByte(answer3.Mode) == 6)
                    {
                        //1.2.11
                        var answerFhss = await VariableWork.aWPtoBearingDSPprotocolNew.GetFhssJamming(0);
                        panoramaControl1.InitFHSSonRS(answerFhss.Settings);
                        variableSuppression.RegimeRadioSuppr = (byte)(Convert.ToByte(answer3.Mode) - 3);
                    }

                    if (answer3 != null)
                    {
                        variableWork.Regime = Convert.ToByte(answer3.Mode);
                    }

                    //Запрос для синхронизации ИРИ ФРЧ на РП Own
                    //1.2.11
                    var answerFRSJamming = await VariableDynamic.VariableWork.aWPtoBearingDSPprotocolNew.GetFRSJamming(0);
                    //Обработка
                    if (answerFRSJamming.Settings.Length == 0)
                    {
                        VariableWork_OnChangeSupprFWS_Own();
                    }
                    else
                    {
                        aWPtoBearingDSPprotocolNew_FrsJammingMessageUpdate(answerFRSJamming);
                    }

                    //Запрос для синхронизации ИРИ ФРЧ на РП Linked
                    //1.2.11
                    var answerFRSJammingLinked = await VariableDynamic.VariableWork.aWPtoBearingDSPprotocolNew.GetFRSJamming(1);
                    //Обработка
                    if (answerFRSJammingLinked.Settings.Length == 0)
                    {
                        VariableWork_OnChangeSupprFWS_Linked();
                    }
                    else
                    {
                        aWPtoBearingDSPprotocolNew_FrsJammingMessageUpdate(answerFRSJammingLinked);
                    }

                    //Запрос для синхронизации ИРИ ППРЧ на РП Own
                    //1.2.11
                    var answerFHSSJamming = await VariableDynamic.VariableWork.aWPtoBearingDSPprotocolNew.GetFhssJamming(0);
                    //Обработка
                    if (answerFHSSJamming.Settings.Length == 0)
                    {
                        VariableWork_OnChangeDistribFHSS_RPOwn();
                    }
                    else
                    {
                        aWPtoBearingDSPprotocolNew_FhssJammingMessageUpdate(answerFHSSJamming);
                    }

                    //Запрос для синхронизации ИРИ ППРЧ на РП Linked
                    //1.2.11
                    var answerFHSSJammingLinked = await VariableDynamic.VariableWork.aWPtoBearingDSPprotocolNew.GetFhssJamming(1);
                    //Обработка
                    if (answerFHSSJammingLinked.Settings.Length == 0)
                    {
                        VariableWork_OnChangeDistribFHSS_RPLinked();
                    }
                    else
                    {
                        aWPtoBearingDSPprotocolNew_FhssJammingMessageUpdate(answerFHSSJammingLinked);
                    }

                    //Запросы для синхронизации и обработки специальных частот
                    await InitSpecialFrequecies();
                   

                    //Запрос курсвого угла от сервера
                    var answerCourseAngle = await VariableWork.aWPtoBearingDSPprotocolNew.GetDirectionCorrection();
                    //Обработка
                    if (answerCourseAngle.DirectionCorrection != -1)
                    {
                        if (variableCommon.Operator == 0)
                            if (variableIntellegence.AutoRelativeBearing == 1)
                            {
                                variableIntellegence.RelativeBearing = answerCourseAngle.DirectionCorrection / 10;
                            }
                        if (variableCommon.Operator == 1)
                        {
                            variableIntellegence.RelativeBearing = answerCourseAngle.DirectionCorrection / 10;
                        }
                    }
                    else
                    {
                        var answerDirectionCorrection = await VariableWork.aWPtoBearingDSPprotocolNew.SetDirectionCorrection(variableIntellegence.RelativeBearing, true);
                    }

                    //Запрос координат
                    //await InitCoordsOld();
                    await InitCoordsNew();


                    //Запрос состояния поиска ППРЧ от сервера
                    var answerSearchFHSS = await VariableWork.aWPtoBearingDSPprotocolNew.GetSearchFHSS();
                    if (answerSearchFHSS != null)
                    {
                        if (answerSearchFHSS.SearchFhss == 255)
                        {
                            var answerSetSearchFHSS = await VariableWork.aWPtoBearingDSPprotocolNew.SetSearchFHSS(variableIntellegence.DetectionPPRCH);
                        }
                        else
                        {
                            variableIntellegence.DetectionPPRCH = answerSearchFHSS.SearchFhss;
                        }
                    }

                    //Запрос состояния АСП от сервера
                    var answerASP = await VariableWork.aWPtoBearingDSPprotocolNew.MasterSlaveState((byte)variableCommon.Role);
                    if (answerASP != null && answerASP.Header.ErrorCode == 0)
                    {
                        if (answerASP.Role == DspDataModel.LinkedStation.StationRole.Master)
                        {
                            if (answerASP.State == DspDataModel.LinkedStation.ConnectionState.Hosted)
                            {
                                butServerAsp.BackColor = Color.Green;
                            }
                            if (answerASP.State == DspDataModel.LinkedStation.ConnectionState.Connected)
                            {
                                butServerAsp.BackColor = Color.Green;
                                butClientAsp.BackColor = Color.Green;
                            }
                        }
                        if (answerASP.Role == DspDataModel.LinkedStation.StationRole.Slave)
                        {
                            if (answerASP.State == DspDataModel.LinkedStation.ConnectionState.Connected)
                            {
                                butServerAsp.BackColor = Color.Green;
                            }
                        }
                    }

                }
                catch (Exception) 
                {
                    bFreqChannel.Enabled = true;
                    if (variableCommon.Operator == 0)
                        butServerAsp.Enabled = true;
                }
            }
            bFreqChannel.Enabled = true;
            if (variableCommon.Operator == 0)
                butServerAsp.Enabled = true;
        }

        private async Task InitSectorsAndRanges()
        {
            //запрос РР для Own
            var answer1 = await VariableDynamic.VariableWork.aWPtoBearingDSPprotocolNew.GetSectorsAndRanges(0, 0);
            //Проверка и Обработка
            if (answer1 != null)
                if (answer1.RangeSectors.Count() != 0)
                {
                    variableWork.RangeSectorReconOwn = answer1.RangeSectors;
                }
                else
                {
                    var answerRR = await VariableDynamic.VariableWork.aWPtoBearingDSPprotocolNew.SetSectorsAndRanges(0, 0, variableWork.RangeSectorReconOwn);
                }
            //запрос РП для Own
            var answer2 = await VariableDynamic.VariableWork.aWPtoBearingDSPprotocolNew.GetSectorsAndRanges(1, 0);
            //Проверка и Обработка
            if (answer2 != null)
                if (answer2.RangeSectors.Count() != 0)
                {
                    variableWork.RangeSectorSupprOwn = answer2.RangeSectors;
                }
                else
                {
                    var answerRR = await VariableDynamic.VariableWork.aWPtoBearingDSPprotocolNew.SetSectorsAndRanges(1, 0, variableWork.RangeSectorSupprOwn);
                }
            //запрос РР для Linked
            var answer3 = await VariableDynamic.VariableWork.aWPtoBearingDSPprotocolNew.GetSectorsAndRanges(0, 1);
            //Проверка и Обработка
            if (answer3 != null)
                if (answer3.RangeSectors.Count() != 0)
                {
                    variableWork.RangeSectorReconLinked = answer3.RangeSectors;
                }
                else
                {
                    var answerRR = await VariableDynamic.VariableWork.aWPtoBearingDSPprotocolNew.SetSectorsAndRanges(0, 1, variableWork.RangeSectorReconLinked);
                }
            //запрос РП для Linked
            var answer4 = await VariableDynamic.VariableWork.aWPtoBearingDSPprotocolNew.GetSectorsAndRanges(1, 1);
            //Проверка и Обработка
            if (answer4 != null)
                if (answer4.RangeSectors.Count() != 0)
                {
                    variableWork.RangeSectorSupprLinked = answer4.RangeSectors;
                }
                else
                {
                    var answerRR = await VariableDynamic.VariableWork.aWPtoBearingDSPprotocolNew.SetSectorsAndRanges(1, 1, variableWork.RangeSectorSupprLinked);
                }
        }
        private async Task InitSpecialFrequecies()
        {
            //Запрос для запрещенны частот Own
            //1.2.11
            var answerForbidden = await VariableDynamic.VariableWork.aWPtoBearingDSPprotocolNew.GetSpecialFrequencies((byte)0, DspDataModel.FrequencyType.Forbidden);
            //Обработка
            if (answerForbidden.Frequencies.Length == 0)
            {
                VariableWork_OnChangeFrequencyRangeForbiddenOwn();
            }
            else
            {
                //1.2.11
                Protocols.SpecialFrequenciesMessage answer = new Protocols.SpecialFrequenciesMessage(answerForbidden.Header, answerForbidden.FrequencyType, answerForbidden.Station, answerForbidden.Frequencies);
                aWPtoBearingDSPprotocolNew_SpecialFrequenciesMessageUpdate(answer);
            }
            //Запрос для известных частот Own
            //1.2.11
            var answerKnown = await VariableDynamic.VariableWork.aWPtoBearingDSPprotocolNew.GetSpecialFrequencies((byte)0, DspDataModel.FrequencyType.Known);
            //Обработка
            if (answerKnown.Frequencies.Length == 0)
            {
                VariableWork_OnChangeFrequencyRangeKnownOwn();
            }
            else
            {
                //1.2.11
                Protocols.SpecialFrequenciesMessage answer = new Protocols.SpecialFrequenciesMessage(answerKnown.Header, answerKnown.FrequencyType, answerKnown.Station, answerKnown.Frequencies);
                aWPtoBearingDSPprotocolNew_SpecialFrequenciesMessageUpdate(answer);
            }
            //Запрос для важных частот Own
            //1.2.11
            var answerImportant = await VariableDynamic.VariableWork.aWPtoBearingDSPprotocolNew.GetSpecialFrequencies((byte)0, DspDataModel.FrequencyType.Important);
            //Обработка
            if (answerImportant.Frequencies.Length == 0)
            {
                VariableWork_OnChangeFrequencyRangeImportantOwn();
            }
            else
            {
                //1.2.11
                Protocols.SpecialFrequenciesMessage answer = new Protocols.SpecialFrequenciesMessage(answerImportant.Header, answerImportant.FrequencyType, answerImportant.Station, answerImportant.Frequencies);
                aWPtoBearingDSPprotocolNew_SpecialFrequenciesMessageUpdate(answer);
            }

            //Запрос для запрещенны частот Linked
            //1.2.11
            var answerForbiddenLinked = await VariableDynamic.VariableWork.aWPtoBearingDSPprotocolNew.GetSpecialFrequencies((byte)1, DspDataModel.FrequencyType.Forbidden);
            //Обработка
            if (answerForbiddenLinked.Frequencies.Length == 0)
            {
                VariableWork_OnChangeFrequencyRangeForbiddenLinked();
            }
            else
            {
                //1.2.11
                Protocols.SpecialFrequenciesMessage answer = new Protocols.SpecialFrequenciesMessage(answerForbiddenLinked.Header, answerForbiddenLinked.FrequencyType, answerForbiddenLinked.Station, answerForbiddenLinked.Frequencies);
                aWPtoBearingDSPprotocolNew_SpecialFrequenciesMessageUpdate(answer);
            }
            //Запрос для известных частот Linked
            //1.2.11
            var answerKnownLinked = await VariableDynamic.VariableWork.aWPtoBearingDSPprotocolNew.GetSpecialFrequencies((byte)1, DspDataModel.FrequencyType.Known);
            //Обработка
            if (answerKnownLinked.Frequencies.Length == 0)
            {
                VariableWork_OnChangeFrequencyRangeKnownLinked();
            }
            else
            {
                //1.2.11
                Protocols.SpecialFrequenciesMessage answer = new Protocols.SpecialFrequenciesMessage(answerKnownLinked.Header, answerKnownLinked.FrequencyType, answerKnownLinked.Station, answerKnownLinked.Frequencies);
                aWPtoBearingDSPprotocolNew_SpecialFrequenciesMessageUpdate(answer);
            }
            //Запрос для важных частот Linked
            //1.2.11
            var answerImportantLinked = await VariableDynamic.VariableWork.aWPtoBearingDSPprotocolNew.GetSpecialFrequencies((byte)1, DspDataModel.FrequencyType.Important);
            //Обработка
            if (answerImportantLinked.Frequencies.Length == 0)
            {
                VariableWork_OnChangeFrequencyRangeImportantLinked();
            }
            else
            {
                //1.2.11
                Protocols.SpecialFrequenciesMessage answer = new Protocols.SpecialFrequenciesMessage(answerImportantLinked.Header, answerImportantLinked.FrequencyType, answerImportantLinked.Station, answerImportantLinked.Frequencies);
                aWPtoBearingDSPprotocolNew_SpecialFrequenciesMessageUpdate(answer);
            }
        }
        private async Task InitCoordsOld()
        {
            //Запрос координат от сервера Own
            //1.2.11
            var answerGNSS = await VariableWork.aWPtoBearingDSPprotocolNew.GetStationLocation(0);
            if (answerGNSS != null)
            {
                if (answerGNSS.Latitude == -1 || answerGNSS.Latitude == -1)
                {
                    //1.2.11
                    var setGNSS = await VariableWork.aWPtoBearingDSPprotocolNew.SetStationLocation(0, variableWork.CoordsGNSS[0].Lat, variableWork.CoordsGNSS[0].Lon, (short)variableWork.CoordsGNSS[0].Alt, true);
                }
                else
                {
                    //1.2.11
                    SetCoordsToStructGNSS(answerGNSS);
                }
            }

            //Запрос координат от сервера Linked
            //1.2.11
            var answerGNSSLinked = await VariableWork.aWPtoBearingDSPprotocolNew.GetStationLocation(1);
            if (answerGNSSLinked != null)
            {
                if (answerGNSSLinked.Latitude == -1 || answerGNSSLinked.Latitude == -1)
                {
                    //1.2.11
                    var setGNSS = await VariableWork.aWPtoBearingDSPprotocolNew.SetStationLocation(1, variableWork.CoordsGNSS[0].Lat, variableWork.CoordsGNSS[0].Lon, (short)variableWork.CoordsGNSS[0].Alt, true);
                }
                else
                {
                    //1.2.11
                    SetCoordsToStructGNSS(answerGNSSLinked);
                }
            }
        }
        private async Task InitCoordsNew()
        {
            if (variableCommon.Operator == 0)
            {
                //Установка координат Own
                double OwnLatitude = 0; try { OwnLatitude = Convert.ToDouble(variableCommon.OwnLatitude); }
                catch (System.FormatException) { OwnLatitude = -1; }

                double OwnLongitude = 0; try { OwnLongitude = Convert.ToDouble(variableCommon.OwnLongitude); }
                catch (System.FormatException) { OwnLongitude = -1; }

                short OwnHeight = 0; try { OwnHeight = Convert.ToInt16(variableCommon.OwnHeight); }
                catch (System.FormatException) { OwnHeight = -1; }

                if (OwnLatitude != -1 && OwnLongitude != -1)
                {
                    //1.2.11
                    var setGNSS = await VariableWork.aWPtoBearingDSPprotocolNew.SetStationLocation(
                        0,
                        OwnLatitude,
                        OwnLongitude,
                        OwnHeight, 
                        true);
                }

                //Установка координат Linked
                double LinkedLatitude = 0; try { LinkedLatitude = Convert.ToDouble(variableCommon.JammerLinkedLatitude); }
                catch (System.FormatException) { LinkedLatitude = -1; }

                double LinkedLongitude = 0; try { LinkedLongitude = Convert.ToDouble(variableCommon.JammerLinkedLongitude); }
                catch (System.FormatException) { LinkedLongitude = -1; }

                short LinkedHeight = 0; try { LinkedHeight = Convert.ToInt16(variableCommon.JammerLinkedHeight); }
                catch (System.FormatException) { LinkedHeight = -1; }

                if (LinkedLatitude != -1 && LinkedLongitude != -1)
                {
                    //1.2.11
                    var setGNSS = await VariableWork.aWPtoBearingDSPprotocolNew.SetStationLocation(
                        1,
                        LinkedLatitude,
                        LinkedLongitude,
                        LinkedHeight,
                        true);
                }
            }
        }

        private void bSectorsRanges_Click(object sender, EventArgs e)
        {
            if (sectorsRanges == null || sectorsRanges.IsDisposed)
            {
                sectorsRanges = new SectorsRanges();
                sectorsRanges.Show();
            }
            else
            {
                sectorsRanges.Show();
            }

        }

        private void button25_Click(object sender, EventArgs e)
        {
            //zagruzka_tablFromBD();
        }


        private void bSpecialFrequencies_Click(object sender, EventArgs e)
        {
            if (specialFrequencies == null || specialFrequencies.IsDisposed)
            {
                specialFrequencies = new SpecialFrequencies();
                specialFrequencies.Show();
            }
            else
            {
                specialFrequencies.Show();
            }
        }

        /*private void button17_Click(object sender, EventArgs e)
        {
            if (formForNoiseShaper == null || formForNoiseShaper.IsDisposed)
            {
                formForNoiseShaper = new FormForNoiseShaper();
                formForNoiseShaper.Show();
            }
            else
            {
                formForNoiseShaper.Show();
            }
        }*/


        private void bModeS_Click(object sender, EventArgs e)
        {
            if (adsbReceiver == null || adsbReceiver.IsDisposed)
            {
                adsbReceiver = new ADSB_Receiver(this);
                adsbReceiver.Show();
            }
            else
            {
                adsbReceiver.Show();
            }
        }

        private void button11_Click(object sender, EventArgs e)
        {
            if (objSetting == null || objSetting.IsDisposed)
            {
                objSetting = new Setting();
                objSetting.Show();
            }
            else
                objSetting.Show();
        }




        private void FogNut_Click(object sender, EventArgs e)
        {
            Process[] simulRu = Process.GetProcessesByName("StrokeFog_");
            Process FogProc;
            if (Process.GetProcessesByName("StrokeFog_").Any())
            {
                for (int i = 0; i < simulRu.Length; i++)
                {
                    simulRu[i].CloseMainWindow();
                    simulRu[i].Kill();
                }
            }
            if (!Process.GetProcessesByName("StrokeFog_").Any())
            {

                FogProc = new Process();
                string path = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + @"\\Fog";
                FogProc.StartInfo.FileName = path + "\\StrokeFog_.exe";
                FogProc.Start();
            }
            else
            {
                System.Diagnostics.Process[] p =
                    System.Diagnostics.Process.GetProcessesByName("StrokeFog_");
                if (p.Length > 0)
                {
                    ShowWindow(p[0].MainWindowHandle, 10);
                    ShowWindow(p[0].MainWindowHandle, 5);
                    SetForegroundWindow(p[0].MainWindowHandle);
                }
            }
        }

        [System.Runtime.InteropServices.DllImport("user32.dll")]
        static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);
        [System.Runtime.InteropServices.DllImport("user32.dll")]
        public static extern bool SetForegroundWindow(IntPtr hWnd);

        private void MessBut_Click(object sender, EventArgs e)
        {
            if (objChangeMessage == null || objChangeMessage.IsDisposed)
            {
                objChangeMessage = new ChangeMessage();
                objChangeMessage.Show();
            }
            else
                objChangeMessage.Show();

        }

        private void bMap_Click(object sender, EventArgs e)
        {
            string name = "GrozaMap";
            System.Diagnostics.Process[] pr2 = System.Diagnostics.Process.GetProcesses();

            bool blProc = false;
            int i = 0;
            while (i < pr2.Length)
            {
                if (pr2[i].ProcessName == name)
                {
                    blProc = true;
                    i = pr2.Length;
                }
                i++;
            }

            //await bMapOldClick();
            //bMapNewClick();

            if (blProc == false)
            {
                ProcessMAP.Start();

                blMapExist = true;
            }
        }

        private async Task bMapOldClick()
        {
            if (variableCommon.Operator == 0)
            {
                //1.2.11
                var answer = await VariableWork.aWPtoBearingDSPprotocolNew.GetStationLocation(0);

                if (answer != null)
                {
                    if (answer.Header.ErrorCode == 0)
                    {
                        try
                        {
                            USR_DLL.TCoordsGNSS[] temp = new USR_DLL.TCoordsGNSS[1];
                            temp[0].Lat = answer.Latitude;
                            temp[0].Lon = answer.Longitude;

                            if (answer.Latitude >= 0)
                                temp[0].signLat = 0;
                            else temp[0].signLat = 1;

                            if (answer.Longitude >= 0)
                                temp[0].signLon = 1;
                            else temp[0].signLon = 0;

                            variableWork.CoordsGNSS = temp;
                        }
                        catch { }
                    }
                }
            }
        }
        private void bMapNewClick()
        {
            if (variableCommon.Operator == 0)
            {
                try
                {
                    USR_DLL.TCoordsGNSS[] temp = new USR_DLL.TCoordsGNSS[2];

                    temp[0].Lat = Convert.ToDouble(variableCommon.OwnLatitude);
                    temp[0].Lon = Convert.ToDouble(variableCommon.OwnLongitude);

                    temp[1].Lat = Convert.ToDouble(variableCommon.JammerLinkedLatitude);
                    temp[1].Lon = Convert.ToDouble(variableCommon.JammerLinkedLongitude);

                    variableWork.CoordsGNSS = temp;
                }
                catch { }
            }
        }

        private void SetCoordsToStructGNSS(Protocols.StationLocationMessage answer)
        {
            //1.2.11
            if (answer != null)
            {
                if (answer.Header.ErrorCode == 0)
                {
                    if (answer.Station == DspDataModel.TargetStation.Current)
                    {
                        try
                        {
                            USR_DLL.TCoordsGNSS[] temp = new USR_DLL.TCoordsGNSS[1];
                            temp[0].Lat = answer.Latitude;
                            temp[0].Lon = answer.Longitude;

                            if (answer.Latitude >= 0)
                                temp[0].signLat = 0;
                            else temp[0].signLat = 1;

                            if (answer.Longitude >= 0)
                                temp[0].signLon = 0;
                            else temp[0].signLon = 1;

                            variableWork.CoordsGNSS = temp;

                            variableCommon.OwnLatitude = variableWork.CoordsGNSS[0].Lat.ToString();
                            variableCommon.OwnLongitude = variableWork.CoordsGNSS[0].Lon.ToString();
                        }
                        catch { }
                    }
                    else
                    {

                    }
                }
            }

        }

        private void bTechnicalAnalysis_Click(object sender, EventArgs e)
        {
            string name = "Application";
            string nameAZ = "ApplicationAZ";
            System.Diagnostics.Process[] process = System.Diagnostics.Process.GetProcesses();

            bool blProc = false;
            int i = 0;

            while (i < process.Length)
            {
                if (process[i].ProcessName == name || process[i].ProcessName == nameAZ)
                {
                    blProc = true;
                    i = process.Length;
                }
                i++;
            }


            VariableStatic.VariableCommon variableCommon = new VariableStatic.VariableCommon();
            //rus
            if (variableCommon.Language == 0)
            {
                ProcessTechnicalAnalysis.StartInfo.FileName = Application.StartupPath + "\\TA 2.0 RU\\Application.exe";
            }
            //az
            if (variableCommon.Language == 2)
            {
                ProcessTechnicalAnalysis.StartInfo.FileName = Application.StartupPath + "\\TA 2.0 AZ\\ApplicationAZ.exe";
            }

            if (blProc == false)
            {
                ProcessTechnicalAnalysis.Start();
            }
        }

        private void AddDataADSBToDB()
        {
            if (variableWork.DataADSBReceiver != null)
            {
                functionsDB.DeleteAllRecordsDB(NameTable.ADSB_RECEIVER, Table.Default);
                Struct_ADSB_Receiver[] strADSB = new Struct_ADSB_Receiver[variableWork.DataADSBReceiver.Length];
                for (int i = 0; i < variableWork.DataADSBReceiver.Length; i++)
                {
                    strADSB[i].iID = i + 1;
                    strADSB[i].sICAO = variableWork.DataADSBReceiver[i].sICAO;
                    strADSB[i].sLatitude = variableWork.DataADSBReceiver[i].sLatitude;
                    strADSB[i].sLongitude = variableWork.DataADSBReceiver[i].sLongitude;
                    strADSB[i].sAltitude = variableWork.DataADSBReceiver[i].sAltitude;
                    strADSB[i].sDatetime = variableWork.DataADSBReceiver[i].sDatetime;
                }
                funcDB_ADSB.AddRecordsADSBToDB(NameTable.ADSB_RECEIVER, strADSB);
            }
        }

        private void bConnectModeS_Click(object sender, EventArgs e)
        {
            if (bConnectModeS.BackColor == Color.Red)
            {
                adsbReceiver.ConnectADSB();

                bConnectModeS.BackColor = Color.Green;
            }
            else
            {
                bConnectModeS.BackColor = Color.Red;
                
                AddDataADSBToDB();
               
                adsbReceiver.CloseADSB();
            }
        }

        private void MainWnd_Load(object sender, EventArgs e)
        {
            ChangeOwnRole();
            ChangeOperatorForComponents();
            //Отправка события для перевода приёмников
            VariableCommon_OnChangeCommonLanguage();
            //Вызов функции Арсения для перевода
            LanguageChooser();
        }

        private void MainWnd_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (ServerHS != null)
            {
                ServerHS.DestroyServer();
                ServerHS = null;
            }
            switch (variableCommon.TypeStation)
            {
                case 0:
                case 2:
                    if (ClientPu_Bel != null)
                    {
                        ClientPu_Bel.ClosePort();
                        ClientPu_Bel = null;
                    }
                    break;
                case 1:
                    if (ClientPu != null)
                    {
                        ClientPu.Disconnect();
                        ClientPu = null;
                    }
                    break;               
                
            }
            btPU.BackColor = Color.Red;
            if (bAOR.BackColor == Color.Green) bAOR_Click(sender, e);

            controlAR6000.DisposeWave();
            controlARDV1.DisposeWave();
            controlARONE.DisposeWave();

            controlAR6000.AR6000aor.AudioGainSet(controlAR6000.tbAudioGain.Value);
            controlARDV1.radio_ARDV1.AudioGainSet(controlARDV1.tbAudioGain.Value);

            AddDataADSBToDB();
        }

        private void bAOR_Click(object sender, EventArgs e)
        {
            if (variableCommon.TypeStation == 0)//bel 
            {
                controlARONE.bAOR_Click(sender, e);
                bAOR.BackColor = controlARONE.bAOR.BackColor;
            }
            if (variableCommon.TypeStation == 1)//az
            {

                if (variableCommon.Operator == 0)
                {
                    controlAR6000.bAOR_Click(sender, e);
                    bAOR.BackColor = controlAR6000.bAOR.BackColor;
                }
                if (variableCommon.Operator == 1)
                {
                    controlARDV1.bAOR_Click(sender, e);
                    bAOR.BackColor = controlARDV1.bAOR.BackColor;
                }
            }

            if (variableCommon.TypeStation == 2)//bel 
            {
                controlARONE.bAOR_Click(sender, e);
                bAOR.BackColor = controlARONE.bAOR.BackColor;
            }
        }

        public void ChangeAOR(int TypeStantion, int armN)
        {
            if (variableCommon.Operator == 0) controlAR6000.BringToFront();

            if (variableCommon.TypeStation == 0)//bel 
            {
                controlARONE.BringToFront();
            }
            if (variableCommon.TypeStation == 1)//az
            {

                if (variableCommon.Operator == 0)
                {
                    controlAR6000.BringToFront();
                }
                if (variableCommon.Operator == 1)
                {
                    controlARDV1.BringToFront();
                }
            }

            if (variableCommon.TypeStation == 2)//bel 6 GGh
            {
                controlARONE.BringToFront();
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            controlARONE.AddFrqToTablARONE(563800000, 1);
            controlARONE.AddFrqToTablARONE(345600000, 0);
        }

        void controlARONE_OnBAORRed()
        {
            bAOR.BackColor = Color.Red;
        }

        void controlARONE_OnBAORGreen()
        {
            bAOR.BackColor = Color.Green;
        }

        void controlAR6000_OnBAORRed()
        {
            bAOR.BackColor = Color.Red;
        }

        void controlAR6000_OnBAORGreen()
        {
            bAOR.BackColor = Color.Green;
        }

        void controlARDV1_OnBAORRed()
        {
            bAOR.BackColor = Color.Red;
        }

        void controlARDV1_OnBAORGreen()
        {
            bAOR.BackColor = Color.Green;
        }


        private void LanguageChooser()
        {
            var cont = this.Controls;
            ChangeStringLabelLanguage();

            if (variableCommon.Language.Equals(0))
            {
                ChangeLanguageToRu(cont);
                //this.Text = "Гроза";
            }
            if (variableCommon.Language.Equals(1))
            {
                ChangeLanguageToEng(cont);
                //this.Text = "GROZA";
            }
            if (variableCommon.Language.Equals(2))
            {
                ChangeLanguageToAzer(cont);
                //this.Text = "GROZA";
            }
        }

        //private void ChangeLanguageToRu(System.Windows.Forms.Control.ControlCollection cont)
        //{
        //    string lang = "ru-RU";
        //    foreach (System.Windows.Forms.Control cc in cont)
        //    {
                
        //        //}
        //        if (!cc.Equals(lOwnNumber) && !cc.Equals(lOwnAddressPC) && !cc.Equals(lOwnRole) && !cc.Equals(lOperator)
        //            && cc is Label || cc is Button || cc is RadioButton || cc is CheckBox || cc is GroupBox || cc is ComboBox || cc is TabPage)
        //        {
        //            resources.ApplyResources(cc, cc.Name, new CultureInfo(lang));
        //            toolTip.SetToolTip(cc, resources.GetString(cc.Name + ".ToolTip", new CultureInfo(lang)));
        //        }
        //        if (cc is Panel || cc is GroupBox || cc is ListBox || cc is TabControl)
        //        {
        //            ChangeLanguageToRu(cc.Controls);
        //        }
        //    }
        //}

        //private void ChangeLanguageToAzer(System.Windows.Forms.Control.ControlCollection cont)
        //{
        //    string lang = "az-Latn";

        //    foreach (System.Windows.Forms.Control cc in cont)
        //    {
                
        //        if (!cc.Equals(lOwnNumber) && !cc.Equals(lOwnAddressPC) && !cc.Equals(lOwnRole) && !cc.Equals(lOperator)
        //             && cc is Label || cc is Button || cc is RadioButton || cc is CheckBox || cc is GroupBox || cc is ComboBox || cc is TabPage)
        //        {
        //            resources.ApplyResources(cc, cc.Name, new CultureInfo(lang));
        //            toolTip.SetToolTip(cc, resources.GetString(cc.Name + ".ToolTip", new CultureInfo(lang)));
        //        }
        //        if (cc is Panel || cc is GroupBox || cc is ListBox || cc is TabControl)
        //        {
        //            ChangeLanguageToAzer(cc.Controls);
        //        }



        //    }
        //}

        private void ChangeLanguageToRu(System.Windows.Forms.Control.ControlCollection cont)
        {
            string lang = "ru-RU";
    
            toolTip.SetToolTip(bMap, "Карта");
            toolTip.SetToolTip(bModeS, "Воздушные суда" );
            //toolTip.SetToolTip(bRegimeRecon, "Радиоразведка");
            //toolTip.SetToolTip(bRegimeStop, "Подготовка");
            //toolTip.SetToolTip(bRegimeSuppr, "Радиоподавление");

            toolTip.SetToolTip(bSectorsRanges, "Сектора и диапазоны РР и РП");
            toolTip.SetToolTip(bSpecialFrequencies, "Специальные частоты");
            toolTip.SetToolTip(bTechnicalAnalysis, "Технический анализ");
            toolTip.SetToolTip(button11, "Установки");
            toolTip.SetToolTip(button15, "Местоположение и направление антенн");

            toolTip.SetToolTip(button17, "Параметры ФПС и УМ");
            toolTip.SetToolTip(FogNut, "Туман");
            toolTip.SetToolTip(MessBut, "Сообщения");


            bRegimeRecon.Text = "Радиоразведка";

            bRegimeStop.Text = "Подготовка";

            bRegimeSuppr.Text = "Радиоподавление";
            label1.Text = "АСП";
                label15.Text	= "Апп";
                label2.Text = "	ПУ";
                label3.Text = "ФПС";
                label7.Text	= "КРПУ";
                lADSB.Text	="ADSB";
                lFreqChannel.Text	="ОП"; 
                //lOperator.Text	="АРМ";
                //lOwnAddressPC.Text	="Адрес";
                //lOwnNumber.Text	= "СП 313";   
                //lOwnRole.Text	="Ведущая";
                tpIRI_FRCh.Text	= "ИРИ ФРЧ";
                tpIRI_FRCh_CR.Text	="ИРИ ФРЧ на ЦР";
                tpIRI_FRCh_RP.Text	="ИРИ ФРЧ на РП";
                tpIRI_PPRCh.Text	="ИРИ ППРЧ";
                tpIRI_PPRCh_CR.Text = "ИРИ ППРЧ на РП";

           
        }

        private void ChangeLanguageToEng(System.Windows.Forms.Control.ControlCollection cont)
        {


        }

        private void ChangeLanguageToAzer(System.Windows.Forms.Control.ControlCollection cont)
        {
            toolTip.SetToolTip(bMap, "Xəritə");
            toolTip.SetToolTip(bModeS, "Hava gəmisi");
            //toolTip.SetToolTip(bRegimeRecon, "Radiokəşfiyyat");
            //toolTip.SetToolTip(bRegimeStop, "Hazırlıq");
            //toolTip.SetToolTip(bRegimeSuppr, "Radiosusdurma");

            toolTip.SetToolTip(bSectorsRanges, "Radiokəşfiyyat və radiosusdurmanın sektor və dipozonları");
            toolTip.SetToolTip(bSpecialFrequencies, "Xüsusu texliklər");
            toolTip.SetToolTip(bTechnicalAnalysis, "Texniki analiz");
            toolTip.SetToolTip(button11, "Quraşdırma");
            toolTip.SetToolTip(button15, "Antenanın yerləşməsi və istiqaməti");

            toolTip.SetToolTip(button17, "Maneə siqnallarını formalaşdıran parametrlər və güc gücləndiriciləri");
            toolTip.SetToolTip(FogNut, "Duman");
            toolTip.SetToolTip(MessBut, "Məlumat");


            bRegimeRecon.Text	="Radiokəşfiyyat";
	
            bRegimeStop.Text	="Hazırlıq";

            bRegimeSuppr.Text = "Radiosusdurma";

            label1.Text = "AMS";
            label15.Text = "Ap";
            label2.Text = "	İM";
            label3.Text = "MSF";
            label7.Text = "YRQQ";
            lADSB.Text = "ADSB";
            lFreqChannel.Text = "AP";
            //lOperator.Text = "AMS";
            //lOwnAddressPC.Text = "Ünvan";
            //lOwnNumber.Text = "İM 313";
            //lOwnRole.Text = "İdarəedən";
            tpIRI_FRCh.Text = "RŞM FRT";
            tpIRI_FRCh_CR.Text = "RŞM FRT görə HB";
            tpIRI_FRCh_RP.Text = "RŞM FRT görə Rs";
            tpIRI_PPRCh.Text = "RŞM İTTK";
            tpIRI_PPRCh_CR.Text = "RŞM İTTK-nə Rs";
        }

        private void pStateServer_Paint(object sender, PaintEventArgs e)
        {

        }

        
        private void tcTables_SelectedIndexChanged(object sender, EventArgs e)
        {
            variableWork.ChangeTabControlTables = tcTables.SelectedIndex;
        }




     
    }
}
