﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using VariableStatic;

namespace WndProject
{
    public partial class SpecialFrequencies : Form
    {
        VariableCommon variableCommon;

        public SpecialFrequencies()
        {
            InitializeComponent();

            variableCommon = new VariableCommon();

            tables_SpecialFrequencies.LoadTablesSpecialFrequenciesfromDB();
            tables_SpecialFrequencies.ChangeControlLanguage(variableCommon.Language);

            //Событие смены языка
            VariableStatic.VariableCommon.OnChangeCommonLanguage += new VariableStatic.VariableCommon.ChangeCommonEventHandler(VariableCommon_OnChangeCommonLanguage);
        }

        private void SpecialFrequencies_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            Hide();
        }

        private void SpecialFrequencies_Load(object sender, EventArgs e)
        {
            ReTranslate();
        }

        private void ReTranslate()
        {
            if (variableCommon.Language.Equals(0))
            {
                this.Text = "Специальные частоты";
            }
            if (variableCommon.Language.Equals(1))
            {
                this.Text = "Aircrafts";
            }
            if (variableCommon.Language.Equals(2))
            {
                this.Text = "Xüsusu texliklər";
            }
        }

        void VariableCommon_OnChangeCommonLanguage()
        {
            ReTranslate();
        }
    }
}
