﻿
//общие настройки вынести для всех таблиц
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Collections;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using USR_DLL;
using VariableDynamic;
using VariableStatic;
using NoiseShaper;


namespace WndProject
{
    public partial class FormForNoiseShaper : Form
    {

        //
        public delegate void WrongLenghtOfCMDHandler(String Message);
        public event WrongLenghtOfCMDHandler WrongLenghtOfCMD;
        protected virtual void OnWrongLenghtOfCMD(String Message)
        {
            if (WrongLenghtOfCMD != null)
                WrongLenghtOfCMD(Message);//Raise the event
        }
        //
        private byte bAmountOfLetter;
        private static String[] NumberFoLetter = new String[] {"I","II","III","IV","V","VI","VII","VIII","IX" };
        private static String[] NamesForStatesOfLetter = new String[] { "TEMP", "UCR", "ICR", "PCR", "CVS", "EVS", "LOAD", "POW" };//!!! подумать о названиях и править последовательность
        private const byte bAMBL = 7;//bAmountOfMeaningBitsfLetter - поменяли с 7 на 8 после изменения протокола
        private int iPositionLoad = Array.IndexOf(NamesForStatesOfLetter,"LOAD");
        static String sNCL = "Лит";//sNCL =NameForColumnsWithLetters
        private static byte bWCSL = 45;//bWCSL =bWightOfCellsOfStateLetter
        private static byte bWCNL = 35;//bWCNL =bWightOfCellsOfNumberLetter
        private static byte bWOC = 60;//bWOC =WightOfOtherColumns
        static String[] sInquiry = new String[] { "cостояние литер(ы)", "тип нагрузки","","", "параметры ИРИ ФРЧ и включ излучение",
            "напряжение литер(ы)","мощность литер(ы)","ток литер(ы)",
            "температура литер(ы)","выключить излучение","установить параметры РП для ИРИ ППРЧ","выход из режима РП-ППРЧ",
            "","","сброс ошибок","Запрос готовности приема речеподобной помехи для ИРИ ФРЧ", "Установить параметры речеподобной помехи для ИРИ ФРЧ и включить",
            "Установить параметры РП для ИРИ ППРЧ (н.с.)"};
        String[] sForLetterChoosing = new String[10] {"Все","1","2","3","4","5","6","7","8","9" };
        //для ФПРЧ
        TSupprFWS[] sfwsALL;
        TDurationParamFWS dpfwsAll;
        DecordingFWS dfws;
        VariableWork vwForNS;
        VariableCommon vcForNS;
        VariableConnection vcForNSnotStatic;
        //Расшифровки ошибок
        TranscriptOfErrors transcriptOfErrors;
        private Image imgTpansparent; 
        private Image imgMinus;       // минус            
        private Image imgPlus;     // плюс
        private Image imgAntenna;
        private Image imgEquivalent;
        int VoltageMin, VoltageMax, PowerMin, PowerMax, CurrentMin, CurrentMax, TempMin, TempMax;//максиминзначения изинишников
        public ModuleSHS KSH;//название придумать
        private static Semaphore sForRepository;
        bool bHex;
        //для длительности и усилительской фигни
        VariableSuppression ForFWS;
        EL elks;
        int iForLossTimer;//для поиска потерь кодограмм по таймеру, мс.
        int iForTableOfState;
        //
        int iCountErrorBytes;
        //
        Dictionary<byte, string> FPSStates = new Dictionary<byte, string>
        {
            {0, "П"},
            {1, "ФРЧ"},
            {2, "ППРЧ"},
            {3, "РчП"},
            {4, "Изм. дли-ти ППРЧ"}
        };
        public FormForNoiseShaper()
        {
            vcForNS = new VariableCommon();
            bAmountOfLetter=Convert.ToByte(vcForNS.TypeStation == 0 ? 7 : 9);
            vcForNSnotStatic = new VariableConnection();
            InitializeComponent();
            bConnect.Visible = false;
            InitConst();
            CreateParametrsForAllTable(ref dataGridViewForLetter);
            CreateParametrsForAllTable(ref dgvVoltageLetter);
            CreateParametrsForAllTable(ref dgvPowerLetter);
            CreateParametrsForAllTable(ref dgvCurrentLetter);
            CreateParametrsForAllTable(ref dgvTempLetter);
            InitTableState(ref dataGridViewForLetter);
            InitTableParamLetter(ref dgvVoltageLetter, "Напр,В");
            InitTableParamLetter(ref dgvPowerLetter, "Мощн,Вт");
            InitTableParamChannel(ref dgvCurrentLetter, "Ток,А");
            InitTableParamChannel(ref dgvTempLetter, "Темп,гр");
            InitParam();;
            bFHSS.Enabled = true;
            bExitFHSS.Enabled = false;
            for (int i = 0; i <= bAmountOfLetter; i++)
                cbForLetterChosing.Items.Add(sForLetterChoosing[i]);
            cbForLetterChosing.SelectedItem = sForLetterChoosing[0];
            iCountErrorBytes = vcForNSnotStatic.CountErrBytes;
            KSH = new ModuleSHS(iCountErrorBytes);
            KSH.InitConst(4, 2);//адреса
            KSH.UpdateState += new ModuleSHS.UpdateEventHandler(UpdateStateDetail);
            KSH.UpdateVoltage += new ModuleSHS.UpdateEventHandler(UpdateVoltage);
            KSH.UpdatePower += new ModuleSHS.UpdateEventHandler(UpdatePower);
            KSH.UpdateCurrent += new ModuleSHS.UpdateEventHandler(UpdateCurrent);
            KSH.UpdateTemp += new ModuleSHS.UpdateEventHandler(UpdateTemp);
            //события для коннекта, дисконнекта, приема и получения ответов
            KSH.ConnectNet += ForConnect;
            KSH.DisconnectNet += ForDisconnect;
            KSH.ReceiveCmd += ReactOnReceive;
            elks = new EL();
            elks.CloseForm+= FunctionForExchangeLog;
            KSH.LostAnyCmd += ReactOnLostCmd;//утеряна кодограмма
            vwForNS = new VariableWork();
            dfws = new DecordingFWS();
            transcriptOfErrors = new TranscriptOfErrors();
            sForRepository = new Semaphore(1, 1);
            bHex = false;
            //richTextBox1.Text = Properties.Settings.Default.textbox;
            ForFWS = new VariableSuppression();//спросить у тани
            VoltageMin = ForFWS.VoltageMin;
            VoltageMax = ForFWS.VoltageMax;
            PowerMin = ForFWS.PowerMin;
            PowerMax = ForFWS.PowerMax;
            CurrentMin = ForFWS.CurrentMin;
            CurrentMax = ForFWS.CurrentMax;
            TempMin = ForFWS.TempMin;
            TempMax = ForFWS.TempMax;
            //для таймера, используемого для поиска потерянных кодограмм
            iForLossTimer=2000;//мс 500 !!!
            timer1.Interval = iForLossTimer;
            timer1.Tick += new EventHandler(timer_TickForLost);//выполнить ф-ию по истечении времени
            //для таймера, используемого для таблицы состояний литер
            iForTableOfState = 300000;//5 мин =300000 мс
            //Событие смены языка
            VariableStatic.VariableCommon.OnChangeCommonLanguage += new VariableStatic.VariableCommon.ChangeCommonEventHandler(VariableCommon_OnChangeCommonLanguage);
        }
        //по завершении счетчика будет выполняться команда
        void timer_TickForLost(object sender, EventArgs e)
        {
            TurnOffTimer();
            ReactOnLostCmd();//оповещение, что кодограмма потеряна
            KSH.ChangingAfterCatchLoss(); 
        }
        //
        void ClearState(object state)
        {
            int iNumberOfLetter = Convert.ToInt32(state);
            if (iNumberOfLetter == 0)
                for (int i = 0; i < bAmountOfLetter; i++)
                    for (int j = 0; j < bAMBL; j++)
                        dataGridViewForLetter.Rows[i].Cells[j + 1].Value = imgTpansparent;
            else
                for (int j = 0; j < bAMBL; j++)
                    dataGridViewForLetter.Rows[iNumberOfLetter-1].Cells[j + 1].Value = imgTpansparent;
        }
        void ClearColumnVoltage(object state)
        {
            int iNumberOfLetter = Convert.ToInt32(state);
            if (iNumberOfLetter == 0)
                for (int i = 0; i < bAmountOfLetter; i++)
                {
                    dgvVoltageLetter.Rows[i].Cells[1].Value = "";
                    dgvVoltageLetter.Rows[i].Cells[1].Style.BackColor = Color.Gainsboro;
                }
            else
            {
                dgvVoltageLetter.Rows[iNumberOfLetter - 1].Cells[1].Value = "";
                dgvVoltageLetter.Rows[iNumberOfLetter - 1].Cells[1].Style.BackColor = Color.Gainsboro;
            }
        }
        void ClearColumnPower(object state)
        {
            int iNumberOfLetter = Convert.ToInt32(state);
            if (iNumberOfLetter == 0)
                for (int i = 0; i < bAmountOfLetter; i++)
                {
                    dgvPowerLetter.Rows[i].Cells[1].Value = "";
                    dgvPowerLetter.Rows[i].Cells[1].Style.BackColor = Color.Gainsboro;
                }
            else
            {
                dgvPowerLetter.Rows[iNumberOfLetter - 1].Cells[1].Value = "";
                dgvPowerLetter.Rows[iNumberOfLetter - 1].Cells[1].Style.BackColor = Color.Gainsboro;
            }
        }
        void ClearColumnsCurrent(object state)
        {
            int iNumberOfLetter = Convert.ToInt32(state);
            if (iNumberOfLetter == 0)
                for (int i1 = 0; i1 < bAmountOfLetter; i1++)
                    for (int i2 = 0; i2 < 4; i2++)
                    {
                        dgvCurrentLetter.Rows[i1].Cells[i2 + 1].Value = "";
                        dgvCurrentLetter.Rows[i1].Cells[i2 + 1].Style.BackColor = Color.Gainsboro;
                    }
            else
                for (int i2 = 0; i2 < 4; i2++)
                {
                    dgvCurrentLetter.Rows[iNumberOfLetter - 1].Cells[i2 + 1].Value = "";
                    dgvCurrentLetter.Rows[iNumberOfLetter - 1].Cells[i2 + 1].Style.BackColor = Color.Gainsboro;
                }
        }
        void ClearColumnsTemp(object state)
        {
            int iNumberOfLetter = Convert.ToInt32(state);
            if (iNumberOfLetter == 0)
                for (int i1 = 0; i1 < bAmountOfLetter; i1++)
                    for (int i2 = 0; i2 < 4; i2++)
                    {
                        dgvTempLetter.Rows[i1].Cells[i2 + 1].Value = "";
                        dgvTempLetter.Rows[i1].Cells[i2 + 1].Style.BackColor = Color.Gainsboro;
                    }
            else
                for (int i2 = 0; i2 < 4; i2++)
                {
                    dgvTempLetter.Rows[iNumberOfLetter - 1].Cells[i2 + 1].Value = "";
                    dgvTempLetter.Rows[iNumberOfLetter - 1].Cells[i2 + 1].Style.BackColor = Color.Gainsboro;
                }
        }
        //
        //включить таймер
        void EnableTimer() { timer1.Enabled = true; }
        void TurnOffTimer() { timer1.Enabled = false; }
        //
        void FunctionForExchangeLog()
        {
            KSH.SendCmd -= elks.ReactOnSendCmd;
            KSH.ReceiveCmd -= elks.ReactOnReceiveCmd;
            KSH.SendByte -= elks.ReactOnSendArray;
            KSH.ReceiveByte -= elks.ReactOnReceiveArray;
            WrongLenghtOfCMD -= elks.MessageForUser;
            bForEL.Enabled = true;
        }
        private void ForConnect()
        {
            bConnect.Text = "Отключить";
        }
        private void ForDisconnect()
        {
            bConnect.Text = "Подключить";
        }
       
        private void ReactOnReceive(byte bCode, object obj)
        {
            TurnOffTimer();//при получении обнуляем таймер
            if (bCode == 13)
            {
                TCodeErrorInform FromReceiveCEI = (TCodeErrorInform)obj;
                lFPSState.Text = "Состю ФПС - "+FPSStates[FromReceiveCEI.bInform[0]];
            }
        }
        //потеря cmd !!!смотреть, что там
        private void ReactOnLostCmd()
        {
            //DialogResult result = MessageBox.Show("Утеряна кодограмма");
        }
        //конец блока для коннекта и тра-та-та
        private void InitConst()
        {
            imgTpansparent = Properties.Resources.Transparent;
            imgMinus = Properties.Resources.Minus;
            imgPlus = Properties.Resources.Plus;
            imgAntenna = Properties.Resources.ant;
            imgEquivalent = Properties.Resources.eqv;
        }
        private void InitParam()//расположения объектов
        {
            dataGridViewForLetter.Top = 0;
            dataGridViewForLetter.Left = 0;
            dgvPowerLetter.Top = 0;
            dgvPowerLetter.Left = dataGridViewForLetter.Width + 2;
            dgvVoltageLetter.Top = dataGridViewForLetter.Height + 2;
            dgvVoltageLetter.Left = dataGridViewForLetter.Width + 2;
            dgvCurrentLetter.Top = 0;
            dgvCurrentLetter.Left = dgvPowerLetter.Left + dgvPowerLetter.Width + 2;
            dgvTempLetter.Top = dataGridViewForLetter.Height + 2;
            dgvTempLetter.Left = dgvPowerLetter.Left + dgvPowerLetter.Width + 2;
            this.ClientSize = new System.Drawing.Size(dataGridViewForLetter.Width + dgvVoltageLetter.Width + dgvCurrentLetter.Width + 6, dataGridViewForLetter.Height + dgvVoltageLetter.Height + 6);
            tlpForButtons.Top = dgvTempLetter.Top;
            tlpForButtons.Left = 0; 
            tlpForButtons.Height = dgvVoltageLetter.Height;
            tlpForButtons.Width = dataGridViewForLetter.Width;
        }
        private void CreateParametrsForAllTable(ref DataGridView dataGridView)
        {
            DataGridViewCellStyle columnHeaderStyle = new DataGridViewCellStyle();
            columnHeaderStyle.BackColor = Color.LightGray;
            columnHeaderStyle.Font = new Font("TimesNewRoman", 8, FontStyle.Regular);
            columnHeaderStyle.ForeColor = Color.Blue;
            dataGridView.ColumnHeadersDefaultCellStyle = columnHeaderStyle;
            dataGridView.ColumnHeadersVisible = true;
            dataGridView.EnableHeadersVisualStyles = false;
            dataGridView.AllowUserToAddRows = false;
            dataGridView.AllowUserToDeleteRows = false;
            dataGridView.AllowUserToResizeRows = false;
            dataGridView.AllowUserToResizeColumns = false;
            dataGridView.RowHeadersBorderStyle = DataGridViewHeaderBorderStyle.Raised;
            dataGridView.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.Raised;
            dataGridView.RowHeadersVisible = false;
            dataGridView.BackgroundColor = Color.Gainsboro;
            dataGridView.ScrollBars = ScrollBars.Vertical;
            dataGridView.CellBorderStyle = DataGridViewCellBorderStyle.Sunken;
            dataGridView.MultiSelect = false;
            dataGridView.DefaultCellStyle.SelectionBackColor = Color.LightSteelBlue;// цвет выделенной ячейки
            dataGridView.DefaultCellStyle.SelectionForeColor = Color.Black;// цвет текста в выдленной ячейке
            dataGridView.RowTemplate.DefaultCellStyle.BackColor = Color.Gainsboro;// цвет ячейки
            dataGridView.SelectionMode = DataGridViewSelectionMode.FullRowSelect;// стиль выделения - строка
            dataGridView.RowTemplate.Height = 20;//задание высоты строки
        }
        private void InitTableState(ref DataGridView dataGridView)//"создание" объектов
        {
            //столбец кнопок
            DataGridViewButtonColumn columnButtonDGV = new DataGridViewButtonColumn();
            columnButtonDGV.CellTemplate = new DataGridViewButtonCell();
            columnButtonDGV.SortMode = DataGridViewColumnSortMode.NotSortable;
            columnButtonDGV.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            columnButtonDGV.Name = sNCL;
            columnButtonDGV.Width = bWCNL;
            // image- столбцы таблицы
            DataGridViewImageColumn[] columnImageDGV = new DataGridViewImageColumn[bAMBL];//создаем массив столбцов с картинками для всех значащих байтов для литер
            for (int i = 0; i < bAMBL; i++)//настраиваем все столбцы
            {
                columnImageDGV[i] = new DataGridViewImageColumn();
                columnImageDGV[i].CellTemplate = new DataGridViewImageCell();
                columnImageDGV[i].SortMode = DataGridViewColumnSortMode.NotSortable;
                columnImageDGV[i].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                columnImageDGV[i].Width = bWCSL;//широта столбца
                columnImageDGV[i].Name = NamesForStatesOfLetter[i];//название столбца
            }
            dataGridView.Columns.Insert(0, columnButtonDGV);//вставка столбца кнопок для каждой из литеры
            for (int i = 0; i < bAMBL; i++)//встабка столбцов состояний литер
                dataGridView.Columns.Insert(i+1, columnImageDGV[i]);
            // добавить строки для каждой из литер
            for (int i = 0; i < bAmountOfLetter;i++ )
                if(bAMBL==8)
                    dataGridView.Rows.Add(NumberFoLetter[i], imgTpansparent, imgTpansparent, imgTpansparent, imgTpansparent, imgTpansparent, imgTpansparent, imgTpansparent, imgTpansparent);
                else if (bAMBL == 7)
                    dataGridView.Rows.Add(NumberFoLetter[i], imgTpansparent, imgTpansparent, imgTpansparent, imgTpansparent, imgTpansparent, imgTpansparent, imgTpansparent);
            //сделать широту таблицы равной суммарной широте всех ячеек
            dataGridView.Width = bWCNL + bWCSL * bAMBL;
            // определить высоту таблицы !!!
            dataGridView.Height = 0;
            dataGridView.Height += (dataGridView.RowTemplate.DividerHeight + dataGridView.RowTemplate.Height) * dataGridView.RowCount + dataGridView.ColumnHeadersHeight;
        }
        //править в одну ф-ию
        private void InitTableParamLetter(ref DataGridView dataGridView, string strName)
        {
            //столбец с кнопками
            DataGridViewButtonColumn columnButtonDGV = new DataGridViewButtonColumn();
            columnButtonDGV.CellTemplate = new DataGridViewButtonCell();
            columnButtonDGV.SortMode = DataGridViewColumnSortMode.NotSortable;
            columnButtonDGV.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            columnButtonDGV.Width = bWCSL;//ширина столбца
            columnButtonDGV.Name = sNCL;//имя столбца
            //столбец с текстом
            DataGridViewTextBoxColumn columnTextDGV = new DataGridViewTextBoxColumn();
            columnTextDGV = new DataGridViewTextBoxColumn();
            columnTextDGV.CellTemplate = new DataGridViewTextBoxCell();
            columnTextDGV.SortMode = DataGridViewColumnSortMode.NotSortable;
            columnTextDGV.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            columnTextDGV.Name = strName;
            columnTextDGV.Width = bWOC;
            // добавить столбцы в таблицу
            dataGridView.Columns.Insert(0, columnButtonDGV);
            dataGridView.Columns.Insert(1, columnTextDGV);
            for (int i = 0; i < bAmountOfLetter; i++)//заполнение строк
                    dataGridView.Rows.Add(NumberFoLetter[i], "");
            // определить ширину таблицы
            dataGridView.Width = bWCSL+bWOC;//!!! подумать
            // определить высоту таблицы
            dataGridView.Height = 0;
            dataGridView.Height += (dataGridView.RowTemplate.DividerHeight + dataGridView.RowTemplate.Height) * dataGridView.RowCount + dataGridView.ColumnHeadersHeight;
        }
        private void InitTableParamChannel(ref DataGridView dataGridView, string strName)
        {
            DataGridViewButtonColumn columnButtonDGV = new DataGridViewButtonColumn();//столбец кнопок
            columnButtonDGV.CellTemplate = new DataGridViewButtonCell();
            columnButtonDGV.SortMode = DataGridViewColumnSortMode.NotSortable;
            columnButtonDGV.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            columnButtonDGV.Name = sNCL;
            columnButtonDGV.Width = bWCNL;
            DataGridViewTextBoxColumn[] columnTextDGV = new DataGridViewTextBoxColumn[4];
            for (int i = 0; i < 4; i++)//границу продумать !!!
            {
                columnTextDGV[i] = new DataGridViewTextBoxColumn();
                columnTextDGV[i].CellTemplate = new DataGridViewTextBoxCell();
                columnTextDGV[i].SortMode = DataGridViewColumnSortMode.NotSortable;
                columnTextDGV[i].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                columnTextDGV[i].Name = Convert.ToString(i + 1) + " " + strName;
                columnTextDGV[i].Width = 65;
            }
            // добавить столбцы в таблицу
            dataGridView.Columns.Insert(0, columnButtonDGV);
            for (int i = 0; i < 4; i++)//индексы
                dataGridView.Columns.Insert(i+1, columnTextDGV[i]);
            for (int i = 0; i < bAmountOfLetter; i++)//добавить строки
                dataGridView.Rows.Add(NumberFoLetter[i], "", "", "", "");
            // определить ширину таблицы
            dataGridView.Width = bWCSL + bWOC * 4+10;
            // определить высоту таблицы
            dataGridView.Height = 0;
            dataGridView.Height += (dataGridView.RowTemplate.DividerHeight + dataGridView.RowTemplate.Height) * dataGridView.RowCount + dataGridView.ColumnHeadersHeight;
        }
        //очищение таблицы
        private void ClearAnyTable(ref DataGridView dataGridView, byte bLetter)
        {
            //определение заполнителя: пустая строка или рисунок в зависимости от типа заполняемых ячеек
            object Filling;
            if (dataGridView.Rows[1].Cells[1] is DataGridViewTextBoxCell)
                Filling = "";
            else
                Filling = imgTpansparent;
            if (bLetter == 0)//если для всех литер,  то очистить все
            {
                for (int i1 = 0; i1 < bAmountOfLetter; i1++)
                    for (int i2 = 1; i2 < dataGridView.ColumnCount; i2++)//все столбцы без учета левого столбца кнопок !!!
                        dataGridView.Rows[i1].Cells[i2].Value = Filling;
            }
            else
            {
                for (int i2 = 1; i2 < dataGridView.ColumnCount; i2++)//все столбцы без учета левого столбца кнопок
                    dataGridView.Rows[bLetter - 1].Cells[i2].Value = Filling;
            }
        }
        //
        private void UpdateStateDetail(byte bLetter, TCodeErrorInform tCodeErrorInform)//обновление состояния  //править
        {
            BitArray btaTemp;
            if (tCodeErrorInform.bInform.Length == 1)
            {
                btaTemp = new BitArray(new byte[] { tCodeErrorInform.bInform[0] });//переводим в биты
                for (int i = 0; i < bAMBL; i++)// по всем данным по литере
                {
                    if (i != iPositionLoad)
                        dataGridViewForLetter.Rows[bLetter - 1].Cells[i + 1].Value = (btaTemp[i] ? imgMinus : imgPlus);
                    else
                        dataGridViewForLetter.Rows[bLetter - 1].Cells[i + 1].Value = (btaTemp[i] ? imgAntenna : imgEquivalent);
                }
                //
                TimerCallback timeCB = new TimerCallback(ClearState);
                System.Threading.Timer time = new System.Threading.Timer(timeCB, bLetter, iForTableOfState, -1);
                //
            }
            else if (tCodeErrorInform.bInform.Length == bAmountOfLetter)
            {
                for (int l = 0; l < bAmountOfLetter; l++)
                {
                    btaTemp = new BitArray(new byte[] { tCodeErrorInform.bInform[l] });
                    for (int i = 0; i < bAMBL; i++)
                    {
                        if (i != iPositionLoad)// ...
                            dataGridViewForLetter.Rows[l].Cells[i + 1].Value = (btaTemp[i] ? imgMinus : imgPlus);
                        else
                            dataGridViewForLetter.Rows[l].Cells[i + 1].Value = (btaTemp[i] ? imgAntenna : imgEquivalent);
                    }
                }
                TimerCallback timeCB = new TimerCallback(ClearState);
                System.Threading.Timer time = new System.Threading.Timer(timeCB, bLetter, iForTableOfState, -1);
            }
            else if(tCodeErrorInform.bInform.Length!=0)
                OnWrongLenghtOfCMD("неверный тип ответной кодограммы на запрос состояния литер/ы");
        }
        private void UpdateVoltage(byte bLetter, TCodeErrorInform tCodeErrorInform)
        {
            if (tCodeErrorInform.bInform.Length == 2)
            {
                dgvVoltageLetter.Rows[bLetter - 1].Cells[1].Value =
                        String.Concat(Convert.ToString(tCodeErrorInform.bInform[0]), "/", Convert.ToString(tCodeErrorInform.bInform[1]));//на напряжение 2 байта приходит
                if ((tCodeErrorInform.bInform[0] < VoltageMin) || (tCodeErrorInform.bInform[0] > VoltageMax)
                    || (tCodeErrorInform.bInform[1] < VoltageMin) || (tCodeErrorInform.bInform[1] > VoltageMax))
                {
                    dgvVoltageLetter.Rows[bLetter - 1].Cells[1].Style.BackColor = Color.Red;
                    if (dgvVoltageLetter.SelectedRows.Count > 0)
                        if (dgvVoltageLetter.SelectedRows[0].Index == bLetter - 1)
                            dgvVoltageLetter.SelectedCells[0].Selected = false;
                }
                TimerCallback timeCB = new TimerCallback(ClearColumnVoltage);
                System.Threading.Timer time = new System.Threading.Timer(timeCB, bLetter, iForTableOfState, -1);
            }
            else if (tCodeErrorInform.bInform.Length == bAmountOfLetter * 2)
            {
                for (int i = 0; i < bAmountOfLetter; i++)
                    {
                        dgvVoltageLetter.Rows[i].Cells[1].Value =
                            String.Concat(Convert.ToString(tCodeErrorInform.bInform[i*2]), "/", Convert.ToString(tCodeErrorInform.bInform[i*2+1]));//аналогично
                        if ((tCodeErrorInform.bInform[i * 2] < VoltageMin) || (tCodeErrorInform.bInform[i * 2] > VoltageMax)
                            || (tCodeErrorInform.bInform[i * 2 + 1] < VoltageMin) || (tCodeErrorInform.bInform[i * 2 + 1] > VoltageMax))
                        {
                            dgvVoltageLetter.Rows[i].Cells[1].Style.BackColor = Color.Red;
                            if (dgvVoltageLetter.SelectedRows.Count >0)
                                if (dgvVoltageLetter.SelectedRows[0].Index == i)
                                    dgvVoltageLetter.SelectedCells[0].Selected = false;
                        }
                    }
                TimerCallback timeCB = new TimerCallback(ClearColumnVoltage);
                System.Threading.Timer time = new System.Threading.Timer(timeCB, 0, iForTableOfState, -1);
            }
            else if(tCodeErrorInform.bInform.Length !=0)
                OnWrongLenghtOfCMD("неверный тип ответной кодограммы на запрос напряжения литер/ы");
        }
        private void UpdatePower(byte bLetter, TCodeErrorInform tCodeErrorInform)
        {
            if (tCodeErrorInform.bInform.Length == 1)
            {
                dgvPowerLetter.Rows[bLetter - 1].Cells[1].Value = Convert.ToString(tCodeErrorInform.bInform[0] * 10);
                if ((tCodeErrorInform.bInform[0] * 10 < PowerMin) || (tCodeErrorInform.bInform[0] * 10 > PowerMax))
                {
                    dgvPowerLetter.Rows[bLetter - 1].Cells[1].Style.BackColor = Color.Red;
                    if (dgvPowerLetter.SelectedRows[0].Index == bLetter - 1)
                        dgvPowerLetter.SelectedCells[0].Selected = false;
                }
                TimerCallback timeCB = new TimerCallback(ClearColumnPower);
                System.Threading.Timer time = new System.Threading.Timer(timeCB, bLetter, iForTableOfState, -1);
            }
            else if (tCodeErrorInform.bInform.Length == bAmountOfLetter)
            {
                for (int i = 0; i < bAmountOfLetter; i++)
                {
                    dgvPowerLetter.Rows[i].Cells[1].Value = Convert.ToString(tCodeErrorInform.bInform[i] * 10);
                    if ((tCodeErrorInform.bInform[i] * 10 < PowerMin) || (tCodeErrorInform.bInform[i] * 10 > PowerMax))
                    {
                        dgvPowerLetter.Rows[i].Cells[1].Style.BackColor = Color.Red;
                        if (dgvPowerLetter.SelectedRows.Count > 0)
                            if (dgvPowerLetter.SelectedRows[0].Index == i)
                                dgvPowerLetter.SelectedCells[0].Selected = false;
                    }
                }
                TimerCallback timeCB = new TimerCallback(ClearColumnPower);
                System.Threading.Timer time = new System.Threading.Timer(timeCB, 0, iForTableOfState, -1);
            }
            else if(tCodeErrorInform.bInform.Length !=0)
                OnWrongLenghtOfCMD("неверный тип ответной кодограммы на запрос мощности литер/ы");
        }
        private void UpdateCurrent(byte bLetter, TCodeErrorInform tCodeErrorInform)
        {
            if (tCodeErrorInform.bInform.Length == 4)
            {
                for (int i = 0; i < 4; i++)
                {
                    dgvCurrentLetter.Rows[bLetter - 1].Cells[i + 1].Value = Convert.ToString(tCodeErrorInform.bInform[i]);
                    if (tCodeErrorInform.bInform[i] < CurrentMin || tCodeErrorInform.bInform[i] > CurrentMax)
                    {
                        dgvCurrentLetter.Rows[bLetter - 1].Cells[i + 1].Style.BackColor = Color.Red;
                        if (dgvCurrentLetter.SelectedRows.Count > 0)
                            if (dgvCurrentLetter.SelectedRows[0].Index == bLetter - 1)
                                dgvCurrentLetter.SelectedRows[0].Selected = false;
                    }
                }
                TimerCallback timeCB = new TimerCallback(ClearColumnsCurrent);
                System.Threading.Timer time = new System.Threading.Timer(timeCB, bLetter, iForTableOfState, -1);
            }
            else if (tCodeErrorInform.bInform.Length == bAmountOfLetter * 4)
            {
                for (int i1 = 0; i1 < bAmountOfLetter; i1++)
                    for (int i2 = 0; i2 < 4; i2++)
                    {
                        dgvCurrentLetter.Rows[i1].Cells[i2 + 1].Value = Convert.ToString(tCodeErrorInform.bInform[i1 * 4 + i2]);
                        if (tCodeErrorInform.bInform[i1 * 4 + i2] < CurrentMin || tCodeErrorInform.bInform[i1 * 4 + i2] > CurrentMax)
                        {
                            dgvCurrentLetter.Rows[i1].Cells[i2 + 1].Style.BackColor = Color.Red;
                            if (dgvCurrentLetter.SelectedRows.Count > 0)
                                if (dgvCurrentLetter.SelectedRows[0].Index == i1)//??
                                    dgvCurrentLetter.SelectedRows[0].Selected = false;
                        }
                    }
                TimerCallback timeCB = new TimerCallback(ClearColumnsCurrent);
                System.Threading.Timer  time = new System.Threading.Timer(timeCB, 0, iForTableOfState, -1);
            }
            else if(tCodeErrorInform.bInform.Length !=0)
                OnWrongLenghtOfCMD("неверный тип ответной кодограммы на запрос тока литер/ы");
        }
        private void UpdateTemp(byte bLetter, TCodeErrorInform tCodeErrorInform)//проверять
        {
            if (tCodeErrorInform.bInform.Length == 4)
            {
                for (int i = 0; i < 4; i++)
                {
                    BitArray btaTemp = new BitArray(new byte[] { tCodeErrorInform.bInform[i] });
                    int iSign = Convert.ToInt32(Math.Pow(-1, Convert.ToByte(btaTemp[7])));//проверить
                    btaTemp[7] = false;//оставляем чисто +
                    byte[] bTemp = new byte[1];
                    btaTemp.CopyTo(bTemp, 0);
                    dgvTempLetter.Rows[bLetter - 1].Cells[i + 1].Value = Convert.ToString(bTemp[0] * iSign);
                    if ((bTemp[0] * iSign) < TempMin || (bTemp[0] * iSign) > TempMax)
                    {
                        dgvTempLetter.Rows[bLetter - 1].Cells[i + 1].Style.BackColor = Color.Red;
                        if (dgvTempLetter.SelectedRows.Count > 0)
                            if (dgvTempLetter.SelectedRows[0].Index == bLetter - 1)
                                dgvTempLetter.SelectedRows[0].Selected = false;
                    }
                }
                TimerCallback timeCB = new TimerCallback(ClearColumnsTemp);
                System.Threading.Timer time = new System.Threading.Timer(timeCB, bLetter, iForTableOfState, -1);
            }
            else if (tCodeErrorInform.bInform.Length == bAmountOfLetter * 4)
            {
                for (int i1 = 0; i1 < bAmountOfLetter; i1++)
                    for (int i2 = 0; i2 < 4; i2++)
                    {
                        BitArray btaTemp = new BitArray(new byte[] { tCodeErrorInform.bInform[i1 * 4 + i2] });
                        int iSign = Convert.ToInt32(Math.Pow(-1, Convert.ToByte(btaTemp[7])));//проверить
                        btaTemp[7] = false;//оставляем чисто +
                        byte[] bTemp = new byte[1];
                        btaTemp.CopyTo(bTemp, 0);
                        dgvTempLetter.Rows[i1].Cells[i2 + 1].Value = Convert.ToString(bTemp[0] * iSign);
                        if (bTemp[0] * iSign < TempMin || bTemp[0] * iSign > TempMax)
                        {
                            dgvTempLetter.Rows[i1].Cells[i2 + 1].Style.BackColor = Color.Red;
                            if (dgvTempLetter.SelectedRows.Count > 0)
                                if (dgvTempLetter.SelectedRows[0].Index == i1)//??
                                    dgvTempLetter.SelectedRows[0].Selected = false;
                        }
                    }
                TimerCallback timeCB = new TimerCallback(ClearColumnsTemp);
                System.Threading.Timer time = new System.Threading.Timer(timeCB, 0, iForTableOfState, -1);
            }
            else if(tCodeErrorInform.bInform.Length != 0)
                OnWrongLenghtOfCMD("неверный тип ответной кодограммы на запрос температуры литер/ы");
        }
        private void bStateButton_Click(object sender, EventArgs e)
        {
            if (vwForNS.Regime != 0)
            {
                MessageBox.Show("Взаимодействие оператора с ФПС через данное приложение возможно только в режиме Подготовка");
                return;
            }
            if (KSH.SendRequestState(0))//посылаем запрос + !!! обработать true и false
                EnableTimer();
        }
        private void bCurrent_Click(object sender, EventArgs e)
        {
            if (vwForNS.Regime != 0)
            {
                MessageBox.Show("Взаимодействие оператора с ФПС через данное приложение возможно только в режиме Подготовка");
                return;
            }
            //создается переменная с номером литеры
            if (KSH.SendRequestCurrent(0))//посылаем запрос 
               timer1.Start();
        }
        private void bTemp_Click(object sender, EventArgs e)
        {
            if (vwForNS.Regime != 0)
            {
                MessageBox.Show("Взаимодействие оператора с ФПС через данное приложение возможно только в режиме Подготовка");
                return;
            }
            if (KSH.SendRequestTemp(0))
                EnableTimer();
        }
        private void button2_Click(object sender, EventArgs e)
        {
            if (vwForNS.Regime != 0)
            {
                MessageBox.Show("Взаимодействие оператора с ФПС через данное приложение возможно только в режиме Подготовка");
                return;
            }
            if (KSH.SendReset())  // отправить перезагрузку  
                EnableTimer();
        }
        private void button4_Click(object sender, EventArgs e)
        {
            if (vwForNS.Regime != 0)
            {
                MessageBox.Show("Взаимодействие оператора с ФПС через данное приложение возможно только в режиме Подготовка");
                return;
            }
            if (KSH.SendRequestVoltage(0))
                EnableTimer();
        }
        private void button6_Click(object sender, EventArgs e)
        {
            if (vwForNS.Regime != 0)
            {
                MessageBox.Show("Взаимодействие оператора с ФПС через данное приложение возможно только в режиме Подготовка");
                return;
            }
            if (KSH.SendRequestPower(0))
                EnableTimer();
        }
        private void button3_Click(object sender, EventArgs e)
        {
            if (vwForNS.Regime != 0)
            {
                MessageBox.Show("Взаимодействие оператора с ФПС через данное приложение возможно только в режиме Подготовка");
                return;
            }
            if (bConnect.Text == "Подключить")
            {
                bool bAttempt = KSH.ConnectClient("192.168.0.114", "192.168.0.102", 9114, 9102);//сначало чужие данные, потом свои ??
                if (!bAttempt)
                    MessageBox.Show("Unseccessfull attemp to connect");
            }
            else
                KSH.DisconnectClient();
        }
        private void button10_Click(object sender, EventArgs e)//эквивалент
        {
            if (vwForNS.Regime != 0)
            {
                MessageBox.Show("Взаимодействие оператора с ФПС через данное приложение возможно только в режиме Подготовка");
                return;
            }
            try
            {
                TTypeLoad ForSENDCMD = new TTypeLoad();
                ForSENDCMD.bType = 0;
                if (cbForLetterChosing.SelectedItem == sForLetterChoosing[0])
                    ForSENDCMD.bLetter = 0;
                else
                    ForSENDCMD.bLetter = Convert.ToByte(cbForLetterChosing.SelectedItem);
                if (KSH.SendTypeLoad(ForSENDCMD))
                    EnableTimer();
            }
            catch (Exception ex)
            {
            }
        }
        private void button5_Click(object sender, EventArgs e)//выключить излучение
        {
            if (vwForNS.Regime != 0)
            {
                MessageBox.Show("Взаимодействие оператора с ФПС через данное приложение возможно только в режиме Подготовка");
                return;
            }
            if (KSH.SendRadiatOff())
                EnableTimer();
        }
        private void button8_Click(object sender, EventArgs e)//Установить параметры РП для ИРИ ФРЧ и включить излучение заданной длительности
        {
            if (vwForNS.Regime != 0)
            {
                MessageBox.Show("Взаимодействие оператора с ФПС через данное приложение возможно только в режиме Подготовка");
                return;
            }
            //берем сразу же из структуры
            if (tbFWSTime.Text != "")
            {
                sfwsALL = vwForNS.SupprFWS_Own;
                dpfwsAll.bCount = Convert.ToByte(sfwsALL.Length);
                dpfwsAll.SourceSupress = new TOneSourceSupress[dpfwsAll.bCount];
                dpfwsAll.iDuration = Convert.ToInt32(tbFWSTime.Text);// длительности из текстового поля
                for (int i = 0; i < sfwsALL.Length; i++)
                {
                    dpfwsAll.SourceSupress[i].uiFreq = Convert.ToUInt32(sfwsALL[i].iFreq);
                    dpfwsAll.SourceSupress[i].bDeviation = sfwsALL[i].bDeviation;
                    dpfwsAll.SourceSupress[i].bDuration = sfwsALL[i].bDuration;
                    dpfwsAll.SourceSupress[i].bManipulation = sfwsALL[i].bManipulation;
                    dpfwsAll.SourceSupress[i].bModulation = sfwsALL[i].bModulation;
                }
                if (KSH.SendDurationParamFWS(dpfwsAll))//5 поменяно
                    EnableTimer();
            }
            else
                MessageBox.Show("Введите длительность радиоподавления ФРЧ");
        }
        private void button9_Click(object sender, EventArgs e)
        {
            if (vwForNS.Regime != 0)
            {
                MessageBox.Show("Взаимодействие оператора с ФПС через данное приложение возможно только в режиме Подготовка");
                return;
            }
            if (KSH.SendStopFHSS())
            {
                EnableTimer();
                bFHSS.Enabled = true;
                bExitFHSS.Enabled = false;
            }
        }
        private void button7_Click(object sender, EventArgs e)//Установить параметры РП для ИРИ ППРЧ (одна сеть) 
        {
            if (vwForNS.Regime != 0)
            {
                MessageBox.Show("Взаимодействие оператора с ФПС через данное приложение возможно только в режиме Подготовка");
                return;
            }
            TDurationParamFHSS ForSENDCMD = new TDurationParamFHSS();
            //проверить
            ForSENDCMD.iDuration = ForFWS.TimeRadiatFHSS;//длительность
            ForSENDCMD.iFreqMin = vwForNS.DistribFHSS_RPOwn[0].iFreqMin;
            ForSENDCMD.bCodeFFT = vwForNS.DistribFHSS_RPOwn[0].bCodeFFT;
            ForSENDCMD.bDeviation = vwForNS.DistribFHSS_RPOwn[0].bDeviation;
            ForSENDCMD.bManipulation = vwForNS.DistribFHSS_RPOwn[0].bManipulation;
            ForSENDCMD.bModulation = vwForNS.DistribFHSS_RPOwn[0].bModulation;
            //
            if (KSH.SendParamFHSS(ForSENDCMD))
            {
                EnableTimer();
                bFHSS.Enabled = false;
                bExitFHSS.Enabled = true;
            }
        }
        private void dataGridViewForLetter_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (vwForNS.Regime != 0)
            {
                MessageBox.Show("Взаимодействие оператора с ФПС через данное приложение возможно только в режиме Подготовка");
                return;
            }
            ClearAnyTable(ref dataGridViewForLetter, Convert.ToByte(e.RowIndex + 1));
            if (KSH.SendRequestState(Convert.ToByte(e.RowIndex + 1)))//посылаем запрос
                EnableTimer();
        }
        private void dgvPowerLetter_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (vwForNS.Regime != 0)
            {
                MessageBox.Show("Взаимодействие оператора с ФПС через данное приложение возможно только в режиме Подготовка");
                return;
            }
            ClearAnyTable(ref dgvPowerLetter, Convert.ToByte(e.RowIndex + 1));
            if (KSH.SendRequestPower(Convert.ToByte(e.RowIndex + 1)))
                EnableTimer();
        }
        private void dgvCurrentLetter_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (vwForNS.Regime != 0)
            {
                MessageBox.Show("Взаимодействие оператора с ФПС через данное приложение возможно только в режиме Подготовка");
                return;
            }
            ClearAnyTable(ref dgvCurrentLetter, Convert.ToByte(e.RowIndex + 1));
            if (KSH.SendRequestCurrent(Convert.ToByte(e.RowIndex + 1)))//посылаем запрос
                EnableTimer();
        }
        private void dgvVoltageLetter_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (vwForNS.Regime != 0)
            {
                MessageBox.Show("Взаимодействие оператора с ФПС через данное приложение возможно только в режиме Подготовка");
                return;
            }
            ClearAnyTable(ref dgvVoltageLetter, Convert.ToByte(e.RowIndex + 1));
            if (KSH.SendRequestVoltage(Convert.ToByte(e.RowIndex + 1)))
                EnableTimer();
        }
        private void dgvTempLetter_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (vwForNS.Regime != 0)
            {
                MessageBox.Show("Взаимодействие оператора с ФПС через данное приложение возможно только в режиме Подготовка");
                return;
            }
            ClearAnyTable(ref dgvTempLetter, Convert.ToByte(e.RowIndex + 1));
            if (KSH.SendRequestTemp(Convert.ToByte(e.RowIndex + 1)))
                EnableTimer();
        }
        private void button12_Click(object sender, EventArgs e)
        {
            if (vwForNS.Regime != 0)
            {
                MessageBox.Show("Взаимодействие оператора с ФПС через данное приложение возможно только в режиме Подготовка");
                return;
            }
            try
            {
                TTypeLoad ForSENDCMD = new TTypeLoad();
                ForSENDCMD.bType = 1;
                if (cbForLetterChosing.SelectedItem == sForLetterChoosing[0])
                    ForSENDCMD.bLetter = 0;
                else
                    ForSENDCMD.bLetter = Convert.ToByte(cbForLetterChosing.SelectedItem);
                if (KSH.SendTypeLoad(ForSENDCMD))
                    EnableTimer();
            }
            catch (Exception ex)
            {
            }
        }
        private void bFPSState_Click(object sender, EventArgs e)//состояние фпс
        {
            if (KSH.SendFPSState())
                EnableTimer();
        }
        private void bFHSSForDur_Click(object sender, EventArgs e)// установить параметры ИРИ ППРЧ для измерения длительности излучения
        {
            if (vwForNS.Regime != 0)
            {
                MessageBox.Show("Взаимодействие оператора с ФПС через данное приложение возможно только в режиме Подготовка");
                return;
            }
            try
            {
                DurationMeasurement durationMeasurement = new DurationMeasurement();
                durationMeasurement.iFreqMin = vwForNS.DistribFHSS_RPOwn[0].iFreqMin;// проверить
                durationMeasurement.iRadiationDuration = Convert.ToInt32(tbFWSTime.Text);// -//-
                durationMeasurement.iCycleDuration = vwForNS.DistribFHSS_RPOwn[0].iDuration;// -//-
                if (KSH.SendFHSSParamDur(durationMeasurement))
                    EnableTimer();
            }
            catch (Exception ex)
            {
            }
        }
        private void bForEL_Click(object sender, EventArgs e)
        {
            if (elks == null || elks.IsDisposed)
                elks = new EL();
            elks.Show();
                KSH.SendCmd += elks.ReactOnSendCmd;
                KSH.ReceiveCmd += elks.ReactOnReceiveCmd;
                KSH.SendByte += elks.ReactOnSendArray;
                KSH.ReceiveByte += elks.ReactOnReceiveArray;
                WrongLenghtOfCMD += elks.MessageForUser;
                elks.CloseForm += FunctionForExchangeLog;
                bForEL.Enabled = false;
        }

        public bool ConnectFromMainForm()
        {
            return KSH.ConnectClient(vcForNSnotStatic.AddressFPS, vcForNSnotStatic.AddressARM, vcForNSnotStatic.PortFPS, vcForNSnotStatic.PortARM);
        }
        public void DisconnectFromMainForm()
        {
            KSH.DisconnectClient();
        }

        private void FormForNoiseShaper_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            Hide();
        }

        private void tbFWSTime_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= '0') && (e.KeyChar <= '9') || e.KeyChar == '\b' || e.ToString() == "" || e.KeyChar == '\r')
            {
                // цифра
                return;
            }
            else
            {
                MessageBox.Show("Недопустимый символ ввода");//оставлять ли
                // остальные символы запрещены
                e.Handled = true;
            }
        }

        private void tbFWSTime_KeyUp(object sender, KeyEventArgs e)
        {
            if (Convert.ToInt32(tbFWSTime.Text)>2000)
            {
                tbFWSTime.Text = "2000";
                MessageBox.Show("Превышено максимально возможное значение длительности излучения");
            }
        }

        private void FormForNoiseShaper_Load(object sender, EventArgs e)
        {
            ReTranslate();
        }

        private void ReTranslate()
        {
            if (vcForNS.Language.Equals(0))
            {
                this.Text = "Параметры ФПС и УМ";

                bConnect.Text = "Подключить";
                bCurrent.Text = "Ток";
                bExitFHSS.Text = "Вых. ППРЧ";
                bExitFWS.Text = "Выкл. изл.";
                bFHSS.Text = "ППРЧ на РП";
                bForEL.Text = "Журнал обмена";
                bFWS.Text = "ФРЧ на РП";
                bPowerLetter.Text = "Мощность";
                bReset.Text = "Сброс аварии";
                bStateButton.Text = "Сост. лит.";
                bTemp.Text = "Температура";
                button10.Text = "Эквивалент";
                button12.Text = "Антенна";
                button2.Text = "Сброс УМ";
                button4.Text = "Напряжение";
                button6.Text = "Мощность";
                bVoltage.Text = "Напряжение";
                label1.Text = "Время изл., мс";
                label2.Text = "Литера";

                dataGridViewForLetter.Columns[0].HeaderText = "Лит";
                dgvPowerLetter.Columns[0].HeaderText = "Лит";
                dgvPowerLetter.Columns[1].HeaderText = "Мощн,Вт";
                dgvCurrentLetter.Columns[0].HeaderText = "Lit";
                dgvCurrentLetter.Columns[1].HeaderText = "1 Ток,А";
                dgvCurrentLetter.Columns[2].HeaderText = "2 Ток,А";
                dgvCurrentLetter.Columns[3].HeaderText = "3 Ток,А";
                dgvCurrentLetter.Columns[4].HeaderText = "4 Ток,А";

                dgvVoltageLetter.Columns[0].HeaderText = "Лит";
                dgvVoltageLetter.Columns[1].HeaderText = "Напр,В";

                dgvTempLetter.Columns[0].HeaderText = "Лит";
                dgvTempLetter.Columns[1].HeaderText = "1 Темп,гр";
                dgvTempLetter.Columns[2].HeaderText = "2 Темп,гр";
                dgvTempLetter.Columns[3].HeaderText = "3 Темп,гр";
                dgvTempLetter.Columns[4].HeaderText = "4 Темп,гр";

                cbForLetterChosing.Items[0] = "Все";


            }
            if (vcForNS.Language.Equals(1))
            {
                //this.Text = "Settings";
            }
            if (vcForNS.Language.Equals(2))
            {
                this.Text = "Maneə siqnallarını formalaşdıran parametrlər və güc gücləndiriciləri";

                bConnect.Text = "Bağlayın";
                bCurrent.Text = "Cərəyan";
                bExitFHSS.Text = "Çıxış İTTD ";
                bExitFWS.Text = "Şüa. sön.";
                bFHSS.Text = "İTTK-nə Rs";
                bForEL.Text = "Mübadilə jurnalı";
                bFWS.Text = "FRT görə Rs";
                bPowerLetter.Text = "Güc";
                bReset.Text = "Nasazlığın silinməsi";
                bStateButton.Text = "Vəziyyət Liter";
                bTemp.Text = "Tempratur";
                button10.Text = "Ekvivalent";
                button12.Text = "Antena";
                button2.Text = "silinməsi GG";
                button4.Text = "Gərginlik";
                button6.Text = "Güc";
                bVoltage.Text = "Gərginlik";
                label1.Text = "Şüalanma vaxtı";
                label2.Text = "Liter";

                dataGridViewForLetter.Columns[0].HeaderText = "Lit";
                dgvPowerLetter.Columns[0].HeaderText = "Lit";
                dgvPowerLetter.Columns[1].HeaderText = "Güc,Vt";
                dgvCurrentLetter.Columns[0].HeaderText = "Lit";
                dgvCurrentLetter.Columns[1].HeaderText = "1 Cərə.A";
                dgvCurrentLetter.Columns[2].HeaderText = "2 Cərə.A";
                dgvCurrentLetter.Columns[3].HeaderText = "3 Cərə.A";
                dgvCurrentLetter.Columns[4].HeaderText = "4 Cərə.A";

                dgvVoltageLetter.Columns[0].HeaderText = "Lit";
                dgvVoltageLetter.Columns[1].HeaderText = "Gərg, V";

                dgvTempLetter.Columns[0].HeaderText = "Lit";
                dgvTempLetter.Columns[1].HeaderText = "1 Temp.D";
                dgvTempLetter.Columns[2].HeaderText = "2 Temp.D";
                dgvTempLetter.Columns[3].HeaderText = "3 Temp.D";
                dgvTempLetter.Columns[4].HeaderText = "4 Temp.D";

                cbForLetterChosing.Items[0] = "Hammısı";
            }
        }

        void VariableCommon_OnChangeCommonLanguage()
        {
            ReTranslate();
        }

    }
}
