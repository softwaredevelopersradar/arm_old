﻿using MathNet.Numerics.IntegralTransforms;
using NAudio.Wave;
using System;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;
using System.Numerics;
using System.Collections.Generic;
using System.IO.Ports;
using System.Globalization;
using MyDataGridView;
using System.Data.SQLite;
using System.Runtime.InteropServices;
using System.Text;
using System.IO;

namespace WndProject
{
    public struct Struct_ARONE
    {
        public byte bPriority;
        public int iAmpl;
        public int iAttenuator;
        public int iBW;
        public Int64 iFreq;
        public int iHPF;
        public int iID;
        public int iLPF;
        public int iMode;
        public int iOnOff;
        public int iPause;
        public int iU;
        public string Note;
        public string sTimeFirst;
    }
    partial class MainWnd
    {
    }
}



