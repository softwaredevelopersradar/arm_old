﻿namespace WndProject
{
    partial class Setting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Setting));
            this.tbSetting = new System.Windows.Forms.TabControl();
            this.tbGeneral = new System.Windows.Forms.TabPage();
            this.InfoGroupBox = new System.Windows.Forms.GroupBox();
            this.UseGNSSBox = new System.Windows.Forms.CheckBox();
            this.labelGNSS = new System.Windows.Forms.Label();
            this.OwnAddressPCBox = new System.Windows.Forms.NumericUpDown();
            this.OwnNumberBox = new System.Windows.Forms.MaskedTextBox();
            this.JammerLinkedAddressPCBox = new System.Windows.Forms.NumericUpDown();
            this.JamLinkAddPcLabel = new System.Windows.Forms.Label();
            this.JammerLinkedNumberBox = new System.Windows.Forms.NumericUpDown();
            this.JamLinkNumLabel = new System.Windows.Forms.Label();
            this.OwnAddrPcLabel = new System.Windows.Forms.Label();
            this.LanguageBox = new System.Windows.Forms.ComboBox();
            this.LanguageLabel = new System.Windows.Forms.Label();
            this.RoleBox = new System.Windows.Forms.ComboBox();
            this.RoleLabel = new System.Windows.Forms.Label();
            this.OwnNumLabel = new System.Windows.Forms.Label();
            this.PostControlCallSignBox = new System.Windows.Forms.TextBox();
            this.PostConLabel = new System.Windows.Forms.Label();
            this.JammerLinkedCallSignBox = new System.Windows.Forms.TextBox();
            this.JamLinkLabel = new System.Windows.Forms.Label();
            this.OwnCallSignBox = new System.Windows.Forms.TextBox();
            this.OwnLabel = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.TableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.TotalGainLevel = new System.Windows.Forms.Label();
            this.BandNumberLabel = new System.Windows.Forms.Label();
            this.AttenuatorTank = new NationalInstruments.UI.WindowsForms.Tank();
            this.AmplifierLabel = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.RelativeBearingBox = new System.Windows.Forms.NumericUpDown();
            this.RelativeBearingLabel = new System.Windows.Forms.Label();
            this.DetectionPPRCHBox = new System.Windows.Forms.CheckBox();
            this.label12 = new System.Windows.Forms.Label();
            this.AveragingBox = new System.Windows.Forms.NumericUpDown();
            this.BearingBox = new System.Windows.Forms.CheckBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.AutoRelativeBearingBox = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.TimeMonitorBox = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabLetterOwn = new System.Windows.Forms.TabPage();
            this.OwnLetter9Box = new System.Windows.Forms.CheckBox();
            this.OwnLetter8Box = new System.Windows.Forms.CheckBox();
            this.OwnLetter7Box = new System.Windows.Forms.CheckBox();
            this.OwnLetter6Box = new System.Windows.Forms.CheckBox();
            this.OwnLetter5Box = new System.Windows.Forms.CheckBox();
            this.OwnLetter4Box = new System.Windows.Forms.CheckBox();
            this.OwnLetter3Box = new System.Windows.Forms.CheckBox();
            this.OwnLetter2Box = new System.Windows.Forms.CheckBox();
            this.OwnLetter1Box = new System.Windows.Forms.CheckBox();
            this.tabLetterMated = new System.Windows.Forms.TabPage();
            this.JammerLinkedLetter9Box = new System.Windows.Forms.CheckBox();
            this.JammerLinkedLetter8Box = new System.Windows.Forms.CheckBox();
            this.JammerLinkedLetter7Box = new System.Windows.Forms.CheckBox();
            this.JammerLinkedLetter6Box = new System.Windows.Forms.CheckBox();
            this.JammerLinkedLetter5Box = new System.Windows.Forms.CheckBox();
            this.JammerLinkedLetter4Box = new System.Windows.Forms.CheckBox();
            this.JammerLinkedLetter3Box = new System.Windows.Forms.CheckBox();
            this.JammerLinkedLetter2Box = new System.Windows.Forms.CheckBox();
            this.JammerLinkedLetter1Box = new System.Windows.Forms.CheckBox();
            this.GeneralTab = new System.Windows.Forms.TabPage();
            this.AutoClearTableBox = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.CountSourceLetterBox = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.CountJammerLinkedLetterBox = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.CountOwnLetterBox = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.tbRP = new System.Windows.Forms.TabPage();
            this.tbControlRP = new System.Windows.Forms.TabControl();
            this.tbFRCH = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.UPBox = new System.Windows.Forms.NumericUpDown();
            this.label14 = new System.Windows.Forms.Label();
            this.PriorHandleBox = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.ThresholdDefaultBox = new System.Windows.Forms.NumericUpDown();
            this.ThresholdDefaultLabel = new System.Windows.Forms.Label();
            this.TimeRadiatFWSBox = new System.Windows.Forms.NumericUpDown();
            this.TimeRadiatFWSLabel = new System.Windows.Forms.Label();
            this.ChannelUseBox = new System.Windows.Forms.NumericUpDown();
            this.ChannelUseLatter = new System.Windows.Forms.Label();
            this.CircleBox = new System.Windows.Forms.NumericUpDown();
            this.CircleLabel = new System.Windows.Forms.Label();
            this.tbAPRCH = new System.Windows.Forms.TabPage();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.SearchBandBox = new System.Windows.Forms.NumericUpDown();
            this.label17 = new System.Windows.Forms.Label();
            this.SearchArcBox = new System.Windows.Forms.NumericUpDown();
            this.label18 = new System.Windows.Forms.Label();
            this.SearchTimeBox = new System.Windows.Forms.NumericUpDown();
            this.label19 = new System.Windows.Forms.Label();
            this.tbPPRCH = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.CodeFFTBox = new System.Windows.Forms.ComboBox();
            this.CodeFftLabel = new System.Windows.Forms.Label();
            this.TimeRadiatFHSSBox = new System.Windows.Forms.NumericUpDown();
            this.TimeRadiatFHSSLabel = new System.Windows.Forms.Label();
            this.RegimeRadioSupprBox = new System.Windows.Forms.ComboBox();
            this.ChoiceModeRSlabel = new System.Windows.Forms.Label();
            this.tbColor = new System.Windows.Forms.TabPage();
            this.grbColorObject = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.ButRange1575_3000 = new System.Windows.Forms.Button();
            this.ButRange1215_1575 = new System.Windows.Forms.Button();
            this.ButRange860_1215 = new System.Windows.Forms.Button();
            this.ButRange400_512 = new System.Windows.Forms.Button();
            this.ButRange512_860 = new System.Windows.Forms.Button();
            this.ButRange220_400 = new System.Windows.Forms.Button();
            this.ButRange170_220 = new System.Windows.Forms.Button();
            this.ButRange108_170 = new System.Windows.Forms.Button();
            this.ButRange88_108 = new System.Windows.Forms.Button();
            this.ButRange30_88 = new System.Windows.Forms.Button();
            this.Range1575 = new System.Windows.Forms.Label();
            this.Range1215 = new System.Windows.Forms.Label();
            this.Range860 = new System.Windows.Forms.Label();
            this.Range512 = new System.Windows.Forms.Label();
            this.Range400 = new System.Windows.Forms.Label();
            this.Range220 = new System.Windows.Forms.Label();
            this.Range170 = new System.Windows.Forms.Label();
            this.Range108 = new System.Windows.Forms.Label();
            this.Range88 = new System.Windows.Forms.Label();
            this.LablRange3000 = new System.Windows.Forms.Label();
            this.LablRange6000 = new System.Windows.Forms.Label();
            this.grbColorPanorama = new System.Windows.Forms.GroupBox();
            this.ButGraphFT1 = new System.Windows.Forms.Button();
            this.LabGraphFT1 = new System.Windows.Forms.Label();
            this.ButGraphFT0 = new System.Windows.Forms.Button();
            this.LabGraphFT0 = new System.Windows.Forms.Label();
            this.ButGrid = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.ButGraphFT = new System.Windows.Forms.Button();
            this.ButGraphFD = new System.Windows.Forms.Button();
            this.ButGraphFA = new System.Windows.Forms.Button();
            this.ButGlobalBackGround = new System.Windows.Forms.Button();
            this.ButLabel = new System.Windows.Forms.Button();
            this.ButCross = new System.Windows.Forms.Button();
            this.ButThreshold = new System.Windows.Forms.Button();
            this.ButBackGround = new System.Windows.Forms.Button();
            this.LabGraphFT = new System.Windows.Forms.Label();
            this.LabGraphFD = new System.Windows.Forms.Label();
            this.LabGraphFA = new System.Windows.Forms.Label();
            this.LabGlobalBackGround = new System.Windows.Forms.Label();
            this.LabLabel = new System.Windows.Forms.Label();
            this.labCross = new System.Windows.Forms.Label();
            this.LabThreshold = new System.Windows.Forms.Label();
            this.LabBackGround = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.groupBoxHS = new System.Windows.Forms.GroupBox();
            this.ButHS2 = new System.Windows.Forms.RadioButton();
            this.ButHS1 = new System.Windows.Forms.RadioButton();
            this.groupBoxASP = new System.Windows.Forms.GroupBox();
            this.ButASP4 = new System.Windows.Forms.RadioButton();
            this.ButASP3 = new System.Windows.Forms.RadioButton();
            this.ButASP2 = new System.Windows.Forms.RadioButton();
            this.ButASP1 = new System.Windows.Forms.RadioButton();
            this.groupBoxPU = new System.Windows.Forms.GroupBox();
            this.ButPU4 = new System.Windows.Forms.RadioButton();
            this.ButPU3 = new System.Windows.Forms.RadioButton();
            this.ButPU2 = new System.Windows.Forms.RadioButton();
            this.ButPU1 = new System.Windows.Forms.RadioButton();
            this.colorDialog = new System.Windows.Forms.ColorDialog();
            this.tbSetting.SuspendLayout();
            this.tbGeneral.SuspendLayout();
            this.InfoGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.OwnAddressPCBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.JammerLinkedAddressPCBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.JammerLinkedNumberBox)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.TableLayoutPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AttenuatorTank)).BeginInit();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RelativeBearingBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AveragingBox)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TimeMonitorBox)).BeginInit();
            this.tabPage1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabLetterOwn.SuspendLayout();
            this.tabLetterMated.SuspendLayout();
            this.GeneralTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CountSourceLetterBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CountJammerLinkedLetterBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CountOwnLetterBox)).BeginInit();
            this.tbRP.SuspendLayout();
            this.tbControlRP.SuspendLayout();
            this.tbFRCH.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.UPBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PriorHandleBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ThresholdDefaultBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TimeRadiatFWSBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChannelUseBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CircleBox)).BeginInit();
            this.tbAPRCH.SuspendLayout();
            this.groupBox9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SearchBandBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SearchArcBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SearchTimeBox)).BeginInit();
            this.tbPPRCH.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TimeRadiatFHSSBox)).BeginInit();
            this.tbColor.SuspendLayout();
            this.grbColorObject.SuspendLayout();
            this.grbColorPanorama.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.groupBoxHS.SuspendLayout();
            this.groupBoxASP.SuspendLayout();
            this.groupBoxPU.SuspendLayout();
            this.SuspendLayout();
            // 
            // tbSetting
            // 
            this.tbSetting.Controls.Add(this.tbGeneral);
            this.tbSetting.Controls.Add(this.tabPage2);
            this.tbSetting.Controls.Add(this.tabPage1);
            this.tbSetting.Controls.Add(this.tbRP);
            this.tbSetting.Controls.Add(this.tbColor);
            this.tbSetting.Controls.Add(this.tabPage3);
            resources.ApplyResources(this.tbSetting, "tbSetting");
            this.tbSetting.Name = "tbSetting";
            this.tbSetting.SelectedIndex = 0;
            // 
            // tbGeneral
            // 
            this.tbGeneral.BackColor = System.Drawing.SystemColors.Control;
            this.tbGeneral.Controls.Add(this.InfoGroupBox);
            resources.ApplyResources(this.tbGeneral, "tbGeneral");
            this.tbGeneral.Name = "tbGeneral";
            // 
            // InfoGroupBox
            // 
            this.InfoGroupBox.Controls.Add(this.UseGNSSBox);
            this.InfoGroupBox.Controls.Add(this.labelGNSS);
            this.InfoGroupBox.Controls.Add(this.OwnAddressPCBox);
            this.InfoGroupBox.Controls.Add(this.OwnNumberBox);
            this.InfoGroupBox.Controls.Add(this.JammerLinkedAddressPCBox);
            this.InfoGroupBox.Controls.Add(this.JamLinkAddPcLabel);
            this.InfoGroupBox.Controls.Add(this.JammerLinkedNumberBox);
            this.InfoGroupBox.Controls.Add(this.JamLinkNumLabel);
            this.InfoGroupBox.Controls.Add(this.OwnAddrPcLabel);
            this.InfoGroupBox.Controls.Add(this.LanguageBox);
            this.InfoGroupBox.Controls.Add(this.LanguageLabel);
            this.InfoGroupBox.Controls.Add(this.RoleBox);
            this.InfoGroupBox.Controls.Add(this.RoleLabel);
            this.InfoGroupBox.Controls.Add(this.OwnNumLabel);
            this.InfoGroupBox.Controls.Add(this.PostControlCallSignBox);
            this.InfoGroupBox.Controls.Add(this.PostConLabel);
            this.InfoGroupBox.Controls.Add(this.JammerLinkedCallSignBox);
            this.InfoGroupBox.Controls.Add(this.JamLinkLabel);
            this.InfoGroupBox.Controls.Add(this.OwnCallSignBox);
            this.InfoGroupBox.Controls.Add(this.OwnLabel);
            resources.ApplyResources(this.InfoGroupBox, "InfoGroupBox");
            this.InfoGroupBox.Name = "InfoGroupBox";
            this.InfoGroupBox.TabStop = false;
            // 
            // UseGNSSBox
            // 
            resources.ApplyResources(this.UseGNSSBox, "UseGNSSBox");
            this.UseGNSSBox.Name = "UseGNSSBox";
            this.UseGNSSBox.UseVisualStyleBackColor = true;
            this.UseGNSSBox.CheckedChanged += new System.EventHandler(this.ChangeValueCommon);
            // 
            // labelGNSS
            // 
            resources.ApplyResources(this.labelGNSS, "labelGNSS");
            this.labelGNSS.Name = "labelGNSS";
            // 
            // OwnAddressPCBox
            // 
            resources.ApplyResources(this.OwnAddressPCBox, "OwnAddressPCBox");
            this.OwnAddressPCBox.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.OwnAddressPCBox.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.OwnAddressPCBox.Name = "OwnAddressPCBox";
            this.OwnAddressPCBox.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.OwnAddressPCBox.ValueChanged += new System.EventHandler(this.ChangeValueCommon);
            this.OwnAddressPCBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.KeyUpCommon);
            // 
            // OwnNumberBox
            // 
            resources.ApplyResources(this.OwnNumberBox, "OwnNumberBox");
            this.OwnNumberBox.Name = "OwnNumberBox";
            this.OwnNumberBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.KeyUpCommon);
            // 
            // JammerLinkedAddressPCBox
            // 
            resources.ApplyResources(this.JammerLinkedAddressPCBox, "JammerLinkedAddressPCBox");
            this.JammerLinkedAddressPCBox.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.JammerLinkedAddressPCBox.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.JammerLinkedAddressPCBox.Name = "JammerLinkedAddressPCBox";
            this.JammerLinkedAddressPCBox.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.JammerLinkedAddressPCBox.ValueChanged += new System.EventHandler(this.ChangeValueCommon);
            this.JammerLinkedAddressPCBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.KeyUpCommon);
            // 
            // JamLinkAddPcLabel
            // 
            resources.ApplyResources(this.JamLinkAddPcLabel, "JamLinkAddPcLabel");
            this.JamLinkAddPcLabel.Name = "JamLinkAddPcLabel";
            // 
            // JammerLinkedNumberBox
            // 
            resources.ApplyResources(this.JammerLinkedNumberBox, "JammerLinkedNumberBox");
            this.JammerLinkedNumberBox.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.JammerLinkedNumberBox.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.JammerLinkedNumberBox.Name = "JammerLinkedNumberBox";
            this.JammerLinkedNumberBox.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.JammerLinkedNumberBox.ValueChanged += new System.EventHandler(this.ChangeValueCommon);
            this.JammerLinkedNumberBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.KeyUpCommon);
            // 
            // JamLinkNumLabel
            // 
            resources.ApplyResources(this.JamLinkNumLabel, "JamLinkNumLabel");
            this.JamLinkNumLabel.Name = "JamLinkNumLabel";
            // 
            // OwnAddrPcLabel
            // 
            resources.ApplyResources(this.OwnAddrPcLabel, "OwnAddrPcLabel");
            this.OwnAddrPcLabel.Name = "OwnAddrPcLabel";
            // 
            // LanguageBox
            // 
            this.LanguageBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.LanguageBox.FormattingEnabled = true;
            this.LanguageBox.Items.AddRange(new object[] {
            resources.GetString("LanguageBox.Items"),
            resources.GetString("LanguageBox.Items1")});
            resources.ApplyResources(this.LanguageBox, "LanguageBox");
            this.LanguageBox.Name = "LanguageBox";
            this.LanguageBox.SelectedIndexChanged += new System.EventHandler(this.ChangeValueCommon);
            // 
            // LanguageLabel
            // 
            resources.ApplyResources(this.LanguageLabel, "LanguageLabel");
            this.LanguageLabel.Name = "LanguageLabel";
            // 
            // RoleBox
            // 
            this.RoleBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.RoleBox.FormattingEnabled = true;
            this.RoleBox.Items.AddRange(new object[] {
            resources.GetString("RoleBox.Items"),
            resources.GetString("RoleBox.Items1"),
            resources.GetString("RoleBox.Items2")});
            resources.ApplyResources(this.RoleBox, "RoleBox");
            this.RoleBox.Name = "RoleBox";
            this.RoleBox.SelectedIndexChanged += new System.EventHandler(this.ChangeValueCommon);
            // 
            // RoleLabel
            // 
            resources.ApplyResources(this.RoleLabel, "RoleLabel");
            this.RoleLabel.Name = "RoleLabel";
            // 
            // OwnNumLabel
            // 
            resources.ApplyResources(this.OwnNumLabel, "OwnNumLabel");
            this.OwnNumLabel.Name = "OwnNumLabel";
            // 
            // PostControlCallSignBox
            // 
            resources.ApplyResources(this.PostControlCallSignBox, "PostControlCallSignBox");
            this.PostControlCallSignBox.Name = "PostControlCallSignBox";
            this.PostControlCallSignBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.KeyUpCommon);
            this.PostControlCallSignBox.Leave += new System.EventHandler(this.LeaveCommon);
            // 
            // PostConLabel
            // 
            resources.ApplyResources(this.PostConLabel, "PostConLabel");
            this.PostConLabel.Name = "PostConLabel";
            // 
            // JammerLinkedCallSignBox
            // 
            resources.ApplyResources(this.JammerLinkedCallSignBox, "JammerLinkedCallSignBox");
            this.JammerLinkedCallSignBox.Name = "JammerLinkedCallSignBox";
            this.JammerLinkedCallSignBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.KeyUpCommon);
            this.JammerLinkedCallSignBox.Leave += new System.EventHandler(this.LeaveCommon);
            // 
            // JamLinkLabel
            // 
            resources.ApplyResources(this.JamLinkLabel, "JamLinkLabel");
            this.JamLinkLabel.Name = "JamLinkLabel";
            // 
            // OwnCallSignBox
            // 
            resources.ApplyResources(this.OwnCallSignBox, "OwnCallSignBox");
            this.OwnCallSignBox.Name = "OwnCallSignBox";
            this.OwnCallSignBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.KeyUpCommon);
            this.OwnCallSignBox.Leave += new System.EventHandler(this.LeaveCommon);
            // 
            // OwnLabel
            // 
            resources.ApplyResources(this.OwnLabel, "OwnLabel");
            this.OwnLabel.Name = "OwnLabel";
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage2.Controls.Add(this.groupBox8);
            this.tabPage2.Controls.Add(this.groupBox4);
            this.tabPage2.Controls.Add(this.groupBox1);
            resources.ApplyResources(this.tabPage2, "tabPage2");
            this.tabPage2.Name = "tabPage2";
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.TableLayoutPanel);
            resources.ApplyResources(this.groupBox8, "groupBox8");
            this.groupBox8.ForeColor = System.Drawing.Color.Blue;
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.TabStop = false;
            // 
            // TableLayoutPanel
            // 
            resources.ApplyResources(this.TableLayoutPanel, "TableLayoutPanel");
            this.TableLayoutPanel.Controls.Add(this.TotalGainLevel, 0, 3);
            this.TableLayoutPanel.Controls.Add(this.BandNumberLabel, 0, 0);
            this.TableLayoutPanel.Controls.Add(this.AttenuatorTank, 0, 1);
            this.TableLayoutPanel.Controls.Add(this.AmplifierLabel, 0, 2);
            this.TableLayoutPanel.Name = "TableLayoutPanel";
            // 
            // TotalGainLevel
            // 
            resources.ApplyResources(this.TotalGainLevel, "TotalGainLevel");
            this.TotalGainLevel.ForeColor = System.Drawing.SystemColors.ControlText;
            this.TotalGainLevel.Name = "TotalGainLevel";
            // 
            // BandNumberLabel
            // 
            resources.ApplyResources(this.BandNumberLabel, "BandNumberLabel");
            this.BandNumberLabel.ForeColor = System.Drawing.SystemColors.ControlText;
            this.BandNumberLabel.Name = "BandNumberLabel";
            // 
            // AttenuatorTank
            // 
            this.AttenuatorTank.FillColor = System.Drawing.SystemColors.Highlight;
            this.AttenuatorTank.InteractionMode = ((NationalInstruments.UI.LinearNumericPointerInteractionModes)((NationalInstruments.UI.LinearNumericPointerInteractionModes.DragPointer | NationalInstruments.UI.LinearNumericPointerInteractionModes.SnapPointer)));
            this.AttenuatorTank.InteractionMouseCursors.Drag = System.Windows.Forms.Cursors.SizeWE;
            this.AttenuatorTank.InteractionMouseCursors.Dragging = System.Windows.Forms.Cursors.SizeWE;
            this.AttenuatorTank.InteractionMouseCursors.Snap = System.Windows.Forms.Cursors.Default;
            this.AttenuatorTank.InteractionMouseCursors.Snapping = System.Windows.Forms.Cursors.Default;
            resources.ApplyResources(this.AttenuatorTank, "AttenuatorTank");
            this.AttenuatorTank.Name = "AttenuatorTank";
            this.AttenuatorTank.OutOfRangeMode = NationalInstruments.UI.NumericOutOfRangeMode.CoerceToRange;
            this.AttenuatorTank.Range = new NationalInstruments.UI.Range(0D, 40D);
            this.AttenuatorTank.ScaleBaseLineColor = System.Drawing.SystemColors.ButtonHighlight;
            this.AttenuatorTank.ScalePosition = NationalInstruments.UI.NumericScalePosition.Bottom;
            this.AttenuatorTank.TankStyle = NationalInstruments.UI.TankStyle.Flat;
            this.AttenuatorTank.ToolTipFormat = new NationalInstruments.UI.FormatString(NationalInstruments.UI.FormatStringMode.Numeric, "F1");
            this.AttenuatorTank.DoubleClick += new System.EventHandler(this.AttenuatorTank_DoubleClick);
            this.AttenuatorTank.MouseUp += new System.Windows.Forms.MouseEventHandler(this.OnAttenuatorTankValueChanged);
            // 
            // AmplifierLabel
            // 
            resources.ApplyResources(this.AmplifierLabel, "AmplifierLabel");
            this.AmplifierLabel.ForeColor = System.Drawing.SystemColors.ControlText;
            this.AmplifierLabel.Name = "AmplifierLabel";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.RelativeBearingBox);
            this.groupBox4.Controls.Add(this.RelativeBearingLabel);
            this.groupBox4.Controls.Add(this.DetectionPPRCHBox);
            this.groupBox4.Controls.Add(this.label12);
            this.groupBox4.Controls.Add(this.AveragingBox);
            this.groupBox4.Controls.Add(this.BearingBox);
            this.groupBox4.Controls.Add(this.label11);
            this.groupBox4.Controls.Add(this.label9);
            this.groupBox4.Controls.Add(this.AutoRelativeBearingBox);
            resources.ApplyResources(this.groupBox4, "groupBox4");
            this.groupBox4.ForeColor = System.Drawing.Color.Blue;
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.TabStop = false;
            // 
            // RelativeBearingBox
            // 
            resources.ApplyResources(this.RelativeBearingBox, "RelativeBearingBox");
            this.RelativeBearingBox.Maximum = new decimal(new int[] {
            360,
            0,
            0,
            0});
            this.RelativeBearingBox.Name = "RelativeBearingBox";
            this.RelativeBearingBox.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.RelativeBearingBox.ValueChanged += new System.EventHandler(this.ChangeValueIntellegence);
            this.RelativeBearingBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.KeyUpIntellegence);
            // 
            // RelativeBearingLabel
            // 
            resources.ApplyResources(this.RelativeBearingLabel, "RelativeBearingLabel");
            this.RelativeBearingLabel.ForeColor = System.Drawing.Color.Black;
            this.RelativeBearingLabel.Name = "RelativeBearingLabel";
            // 
            // DetectionPPRCHBox
            // 
            resources.ApplyResources(this.DetectionPPRCHBox, "DetectionPPRCHBox");
            this.DetectionPPRCHBox.Name = "DetectionPPRCHBox";
            this.DetectionPPRCHBox.UseVisualStyleBackColor = true;
            this.DetectionPPRCHBox.CheckedChanged += new System.EventHandler(this.ChangeValueIntellegence);
            // 
            // label12
            // 
            resources.ApplyResources(this.label12, "label12");
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Name = "label12";
            // 
            // AveragingBox
            // 
            resources.ApplyResources(this.AveragingBox, "AveragingBox");
            this.AveragingBox.Name = "AveragingBox";
            this.AveragingBox.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.AveragingBox.ValueChanged += new System.EventHandler(this.ChangeValueIntellegence);
            this.AveragingBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.KeyUpIntellegence);
            // 
            // BearingBox
            // 
            resources.ApplyResources(this.BearingBox, "BearingBox");
            this.BearingBox.Name = "BearingBox";
            this.BearingBox.UseVisualStyleBackColor = true;
            this.BearingBox.CheckedChanged += new System.EventHandler(this.ChangeValueIntellegence);
            // 
            // label11
            // 
            resources.ApplyResources(this.label11, "label11");
            this.label11.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label11.Name = "label11";
            // 
            // label9
            // 
            resources.ApplyResources(this.label9, "label9");
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Name = "label9";
            // 
            // AutoRelativeBearingBox
            // 
            resources.ApplyResources(this.AutoRelativeBearingBox, "AutoRelativeBearingBox");
            this.AutoRelativeBearingBox.Name = "AutoRelativeBearingBox";
            this.AutoRelativeBearingBox.UseVisualStyleBackColor = true;
            this.AutoRelativeBearingBox.CheckedChanged += new System.EventHandler(this.ChangeValueIntellegence);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.TimeMonitorBox);
            this.groupBox1.Controls.Add(this.label7);
            resources.ApplyResources(this.groupBox1, "groupBox1");
            this.groupBox1.ForeColor = System.Drawing.Color.Blue;
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.TabStop = false;
            // 
            // TimeMonitorBox
            // 
            resources.ApplyResources(this.TimeMonitorBox, "TimeMonitorBox");
            this.TimeMonitorBox.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.TimeMonitorBox.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.TimeMonitorBox.Name = "TimeMonitorBox";
            this.TimeMonitorBox.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.TimeMonitorBox.ValueChanged += new System.EventHandler(this.ChangeValueIntellegence);
            this.TimeMonitorBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.KeyUpIntellegence);
            // 
            // label7
            // 
            resources.ApplyResources(this.label7, "label7");
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Name = "label7";
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage1.Controls.Add(this.tabControl1);
            resources.ApplyResources(this.tabPage1, "tabPage1");
            this.tabPage1.Name = "tabPage1";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabLetterOwn);
            this.tabControl1.Controls.Add(this.tabLetterMated);
            this.tabControl1.Controls.Add(this.GeneralTab);
            resources.ApplyResources(this.tabControl1, "tabControl1");
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            // 
            // tabLetterOwn
            // 
            this.tabLetterOwn.BackColor = System.Drawing.SystemColors.Control;
            this.tabLetterOwn.Controls.Add(this.OwnLetter9Box);
            this.tabLetterOwn.Controls.Add(this.OwnLetter8Box);
            this.tabLetterOwn.Controls.Add(this.OwnLetter7Box);
            this.tabLetterOwn.Controls.Add(this.OwnLetter6Box);
            this.tabLetterOwn.Controls.Add(this.OwnLetter5Box);
            this.tabLetterOwn.Controls.Add(this.OwnLetter4Box);
            this.tabLetterOwn.Controls.Add(this.OwnLetter3Box);
            this.tabLetterOwn.Controls.Add(this.OwnLetter2Box);
            this.tabLetterOwn.Controls.Add(this.OwnLetter1Box);
            resources.ApplyResources(this.tabLetterOwn, "tabLetterOwn");
            this.tabLetterOwn.Name = "tabLetterOwn";
            // 
            // OwnLetter9Box
            // 
            resources.ApplyResources(this.OwnLetter9Box, "OwnLetter9Box");
            this.OwnLetter9Box.Name = "OwnLetter9Box";
            this.OwnLetter9Box.UseVisualStyleBackColor = true;
            this.OwnLetter9Box.CheckedChanged += new System.EventHandler(this.SelectOwnLetter);
            this.OwnLetter9Box.Click += new System.EventHandler(this.OwnLetterClick);
            // 
            // OwnLetter8Box
            // 
            resources.ApplyResources(this.OwnLetter8Box, "OwnLetter8Box");
            this.OwnLetter8Box.Name = "OwnLetter8Box";
            this.OwnLetter8Box.UseVisualStyleBackColor = true;
            this.OwnLetter8Box.CheckedChanged += new System.EventHandler(this.SelectOwnLetter);
            this.OwnLetter8Box.Click += new System.EventHandler(this.OwnLetterClick);
            // 
            // OwnLetter7Box
            // 
            resources.ApplyResources(this.OwnLetter7Box, "OwnLetter7Box");
            this.OwnLetter7Box.Name = "OwnLetter7Box";
            this.OwnLetter7Box.UseVisualStyleBackColor = true;
            this.OwnLetter7Box.CheckedChanged += new System.EventHandler(this.SelectOwnLetter);
            this.OwnLetter7Box.Click += new System.EventHandler(this.OwnLetterClick);
            // 
            // OwnLetter6Box
            // 
            resources.ApplyResources(this.OwnLetter6Box, "OwnLetter6Box");
            this.OwnLetter6Box.Name = "OwnLetter6Box";
            this.OwnLetter6Box.UseVisualStyleBackColor = true;
            this.OwnLetter6Box.CheckedChanged += new System.EventHandler(this.SelectOwnLetter);
            this.OwnLetter6Box.Click += new System.EventHandler(this.OwnLetterClick);
            // 
            // OwnLetter5Box
            // 
            resources.ApplyResources(this.OwnLetter5Box, "OwnLetter5Box");
            this.OwnLetter5Box.Name = "OwnLetter5Box";
            this.OwnLetter5Box.UseVisualStyleBackColor = true;
            this.OwnLetter5Box.CheckedChanged += new System.EventHandler(this.SelectOwnLetter);
            this.OwnLetter5Box.Click += new System.EventHandler(this.OwnLetterClick);
            // 
            // OwnLetter4Box
            // 
            resources.ApplyResources(this.OwnLetter4Box, "OwnLetter4Box");
            this.OwnLetter4Box.Name = "OwnLetter4Box";
            this.OwnLetter4Box.UseVisualStyleBackColor = true;
            this.OwnLetter4Box.CheckedChanged += new System.EventHandler(this.SelectOwnLetter);
            this.OwnLetter4Box.Click += new System.EventHandler(this.OwnLetterClick);
            // 
            // OwnLetter3Box
            // 
            resources.ApplyResources(this.OwnLetter3Box, "OwnLetter3Box");
            this.OwnLetter3Box.Name = "OwnLetter3Box";
            this.OwnLetter3Box.UseVisualStyleBackColor = true;
            this.OwnLetter3Box.CheckedChanged += new System.EventHandler(this.SelectOwnLetter);
            this.OwnLetter3Box.Click += new System.EventHandler(this.OwnLetterClick);
            // 
            // OwnLetter2Box
            // 
            resources.ApplyResources(this.OwnLetter2Box, "OwnLetter2Box");
            this.OwnLetter2Box.Name = "OwnLetter2Box";
            this.OwnLetter2Box.UseVisualStyleBackColor = true;
            this.OwnLetter2Box.CheckedChanged += new System.EventHandler(this.SelectOwnLetter);
            this.OwnLetter2Box.Click += new System.EventHandler(this.OwnLetterClick);
            // 
            // OwnLetter1Box
            // 
            resources.ApplyResources(this.OwnLetter1Box, "OwnLetter1Box");
            this.OwnLetter1Box.Name = "OwnLetter1Box";
            this.OwnLetter1Box.UseVisualStyleBackColor = true;
            this.OwnLetter1Box.CheckedChanged += new System.EventHandler(this.SelectOwnLetter);
            this.OwnLetter1Box.Click += new System.EventHandler(this.OwnLetterClick);
            // 
            // tabLetterMated
            // 
            this.tabLetterMated.BackColor = System.Drawing.SystemColors.Control;
            this.tabLetterMated.Controls.Add(this.JammerLinkedLetter9Box);
            this.tabLetterMated.Controls.Add(this.JammerLinkedLetter8Box);
            this.tabLetterMated.Controls.Add(this.JammerLinkedLetter7Box);
            this.tabLetterMated.Controls.Add(this.JammerLinkedLetter6Box);
            this.tabLetterMated.Controls.Add(this.JammerLinkedLetter5Box);
            this.tabLetterMated.Controls.Add(this.JammerLinkedLetter4Box);
            this.tabLetterMated.Controls.Add(this.JammerLinkedLetter3Box);
            this.tabLetterMated.Controls.Add(this.JammerLinkedLetter2Box);
            this.tabLetterMated.Controls.Add(this.JammerLinkedLetter1Box);
            resources.ApplyResources(this.tabLetterMated, "tabLetterMated");
            this.tabLetterMated.Name = "tabLetterMated";
            // 
            // JammerLinkedLetter9Box
            // 
            resources.ApplyResources(this.JammerLinkedLetter9Box, "JammerLinkedLetter9Box");
            this.JammerLinkedLetter9Box.Name = "JammerLinkedLetter9Box";
            this.JammerLinkedLetter9Box.UseVisualStyleBackColor = true;
            this.JammerLinkedLetter9Box.CheckedChanged += new System.EventHandler(this.SelectJammerLinkedLetter);
            this.JammerLinkedLetter9Box.Click += new System.EventHandler(this.JammerLinkedClick);
            // 
            // JammerLinkedLetter8Box
            // 
            resources.ApplyResources(this.JammerLinkedLetter8Box, "JammerLinkedLetter8Box");
            this.JammerLinkedLetter8Box.Name = "JammerLinkedLetter8Box";
            this.JammerLinkedLetter8Box.UseVisualStyleBackColor = true;
            this.JammerLinkedLetter8Box.CheckedChanged += new System.EventHandler(this.SelectJammerLinkedLetter);
            this.JammerLinkedLetter8Box.Click += new System.EventHandler(this.JammerLinkedClick);
            // 
            // JammerLinkedLetter7Box
            // 
            resources.ApplyResources(this.JammerLinkedLetter7Box, "JammerLinkedLetter7Box");
            this.JammerLinkedLetter7Box.Name = "JammerLinkedLetter7Box";
            this.JammerLinkedLetter7Box.UseVisualStyleBackColor = true;
            this.JammerLinkedLetter7Box.CheckedChanged += new System.EventHandler(this.SelectJammerLinkedLetter);
            this.JammerLinkedLetter7Box.Click += new System.EventHandler(this.JammerLinkedClick);
            // 
            // JammerLinkedLetter6Box
            // 
            resources.ApplyResources(this.JammerLinkedLetter6Box, "JammerLinkedLetter6Box");
            this.JammerLinkedLetter6Box.Name = "JammerLinkedLetter6Box";
            this.JammerLinkedLetter6Box.UseVisualStyleBackColor = true;
            this.JammerLinkedLetter6Box.CheckedChanged += new System.EventHandler(this.SelectJammerLinkedLetter);
            this.JammerLinkedLetter6Box.Click += new System.EventHandler(this.JammerLinkedClick);
            // 
            // JammerLinkedLetter5Box
            // 
            resources.ApplyResources(this.JammerLinkedLetter5Box, "JammerLinkedLetter5Box");
            this.JammerLinkedLetter5Box.Name = "JammerLinkedLetter5Box";
            this.JammerLinkedLetter5Box.UseVisualStyleBackColor = true;
            this.JammerLinkedLetter5Box.CheckedChanged += new System.EventHandler(this.SelectJammerLinkedLetter);
            this.JammerLinkedLetter5Box.Click += new System.EventHandler(this.JammerLinkedClick);
            // 
            // JammerLinkedLetter4Box
            // 
            resources.ApplyResources(this.JammerLinkedLetter4Box, "JammerLinkedLetter4Box");
            this.JammerLinkedLetter4Box.Name = "JammerLinkedLetter4Box";
            this.JammerLinkedLetter4Box.UseVisualStyleBackColor = true;
            this.JammerLinkedLetter4Box.CheckedChanged += new System.EventHandler(this.SelectJammerLinkedLetter);
            this.JammerLinkedLetter4Box.Click += new System.EventHandler(this.JammerLinkedClick);
            // 
            // JammerLinkedLetter3Box
            // 
            resources.ApplyResources(this.JammerLinkedLetter3Box, "JammerLinkedLetter3Box");
            this.JammerLinkedLetter3Box.Name = "JammerLinkedLetter3Box";
            this.JammerLinkedLetter3Box.UseVisualStyleBackColor = true;
            this.JammerLinkedLetter3Box.CheckedChanged += new System.EventHandler(this.SelectJammerLinkedLetter);
            this.JammerLinkedLetter3Box.Click += new System.EventHandler(this.JammerLinkedClick);
            // 
            // JammerLinkedLetter2Box
            // 
            resources.ApplyResources(this.JammerLinkedLetter2Box, "JammerLinkedLetter2Box");
            this.JammerLinkedLetter2Box.Name = "JammerLinkedLetter2Box";
            this.JammerLinkedLetter2Box.UseVisualStyleBackColor = true;
            this.JammerLinkedLetter2Box.CheckedChanged += new System.EventHandler(this.SelectJammerLinkedLetter);
            this.JammerLinkedLetter2Box.Click += new System.EventHandler(this.JammerLinkedClick);
            // 
            // JammerLinkedLetter1Box
            // 
            resources.ApplyResources(this.JammerLinkedLetter1Box, "JammerLinkedLetter1Box");
            this.JammerLinkedLetter1Box.Name = "JammerLinkedLetter1Box";
            this.JammerLinkedLetter1Box.UseVisualStyleBackColor = true;
            this.JammerLinkedLetter1Box.CheckedChanged += new System.EventHandler(this.SelectJammerLinkedLetter);
            this.JammerLinkedLetter1Box.Click += new System.EventHandler(this.JammerLinkedClick);
            // 
            // GeneralTab
            // 
            this.GeneralTab.BackColor = System.Drawing.SystemColors.Control;
            this.GeneralTab.Controls.Add(this.AutoClearTableBox);
            this.GeneralTab.Controls.Add(this.label1);
            this.GeneralTab.Controls.Add(this.CountSourceLetterBox);
            this.GeneralTab.Controls.Add(this.label4);
            this.GeneralTab.Controls.Add(this.CountJammerLinkedLetterBox);
            this.GeneralTab.Controls.Add(this.label5);
            this.GeneralTab.Controls.Add(this.CountOwnLetterBox);
            this.GeneralTab.Controls.Add(this.label6);
            resources.ApplyResources(this.GeneralTab, "GeneralTab");
            this.GeneralTab.Name = "GeneralTab";
            // 
            // AutoClearTableBox
            // 
            resources.ApplyResources(this.AutoClearTableBox, "AutoClearTableBox");
            this.AutoClearTableBox.Name = "AutoClearTableBox";
            this.AutoClearTableBox.UseVisualStyleBackColor = true;
            this.AutoClearTableBox.CheckedChanged += new System.EventHandler(this.ChangeValueSuppression);
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // CountSourceLetterBox
            // 
            resources.ApplyResources(this.CountSourceLetterBox, "CountSourceLetterBox");
            this.CountSourceLetterBox.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.CountSourceLetterBox.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.CountSourceLetterBox.Name = "CountSourceLetterBox";
            this.CountSourceLetterBox.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.CountSourceLetterBox.ValueChanged += new System.EventHandler(this.ChangeValueSuppression);
            this.CountSourceLetterBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.KeyUpSuppression);
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.Name = "label4";
            // 
            // CountJammerLinkedLetterBox
            // 
            resources.ApplyResources(this.CountJammerLinkedLetterBox, "CountJammerLinkedLetterBox");
            this.CountJammerLinkedLetterBox.Maximum = new decimal(new int[] {
            9,
            0,
            0,
            0});
            this.CountJammerLinkedLetterBox.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.CountJammerLinkedLetterBox.Name = "CountJammerLinkedLetterBox";
            this.CountJammerLinkedLetterBox.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.CountJammerLinkedLetterBox.ValueChanged += new System.EventHandler(this.ChangeValueSuppression);
            this.CountJammerLinkedLetterBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.KeyUpSuppression);
            // 
            // label5
            // 
            resources.ApplyResources(this.label5, "label5");
            this.label5.Name = "label5";
            // 
            // CountOwnLetterBox
            // 
            resources.ApplyResources(this.CountOwnLetterBox, "CountOwnLetterBox");
            this.CountOwnLetterBox.Maximum = new decimal(new int[] {
            9,
            0,
            0,
            0});
            this.CountOwnLetterBox.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.CountOwnLetterBox.Name = "CountOwnLetterBox";
            this.CountOwnLetterBox.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.CountOwnLetterBox.ValueChanged += new System.EventHandler(this.ChangeValueSuppression);
            this.CountOwnLetterBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.KeyUpSuppression);
            // 
            // label6
            // 
            resources.ApplyResources(this.label6, "label6");
            this.label6.Name = "label6";
            // 
            // tbRP
            // 
            this.tbRP.BackColor = System.Drawing.SystemColors.Control;
            this.tbRP.Controls.Add(this.tbControlRP);
            this.tbRP.Controls.Add(this.RegimeRadioSupprBox);
            this.tbRP.Controls.Add(this.ChoiceModeRSlabel);
            resources.ApplyResources(this.tbRP, "tbRP");
            this.tbRP.Name = "tbRP";
            // 
            // tbControlRP
            // 
            this.tbControlRP.Controls.Add(this.tbFRCH);
            this.tbControlRP.Controls.Add(this.tbAPRCH);
            this.tbControlRP.Controls.Add(this.tbPPRCH);
            resources.ApplyResources(this.tbControlRP, "tbControlRP");
            this.tbControlRP.Name = "tbControlRP";
            this.tbControlRP.SelectedIndex = 0;
            // 
            // tbFRCH
            // 
            this.tbFRCH.BackColor = System.Drawing.SystemColors.Control;
            this.tbFRCH.Controls.Add(this.groupBox3);
            resources.ApplyResources(this.tbFRCH, "tbFRCH");
            this.tbFRCH.Name = "tbFRCH";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.UPBox);
            this.groupBox3.Controls.Add(this.label14);
            this.groupBox3.Controls.Add(this.PriorHandleBox);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.ThresholdDefaultBox);
            this.groupBox3.Controls.Add(this.ThresholdDefaultLabel);
            this.groupBox3.Controls.Add(this.TimeRadiatFWSBox);
            this.groupBox3.Controls.Add(this.TimeRadiatFWSLabel);
            this.groupBox3.Controls.Add(this.ChannelUseBox);
            this.groupBox3.Controls.Add(this.ChannelUseLatter);
            this.groupBox3.Controls.Add(this.CircleBox);
            this.groupBox3.Controls.Add(this.CircleLabel);
            this.groupBox3.ForeColor = System.Drawing.Color.Blue;
            resources.ApplyResources(this.groupBox3, "groupBox3");
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.TabStop = false;
            // 
            // UPBox
            // 
            this.UPBox.DecimalPlaces = 1;
            this.UPBox.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            resources.ApplyResources(this.UPBox, "UPBox");
            this.UPBox.Maximum = new decimal(new int[] {
            6,
            0,
            0,
            0});
            this.UPBox.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.UPBox.Name = "UPBox";
            this.UPBox.Value = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.UPBox.ValueChanged += new System.EventHandler(this.ChangeValueSuppression);
            this.UPBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.KeyUpSuppression);
            // 
            // label14
            // 
            resources.ApplyResources(this.label14, "label14");
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Name = "label14";
            // 
            // PriorHandleBox
            // 
            resources.ApplyResources(this.PriorHandleBox, "PriorHandleBox");
            this.PriorHandleBox.Maximum = new decimal(new int[] {
            9,
            0,
            0,
            0});
            this.PriorHandleBox.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.PriorHandleBox.Name = "PriorHandleBox";
            this.PriorHandleBox.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.PriorHandleBox.ValueChanged += new System.EventHandler(this.ChangeValueSuppression);
            this.PriorHandleBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.KeyUpSuppression);
            // 
            // label8
            // 
            resources.ApplyResources(this.label8, "label8");
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Name = "label8";
            // 
            // ThresholdDefaultBox
            // 
            resources.ApplyResources(this.ThresholdDefaultBox, "ThresholdDefaultBox");
            this.ThresholdDefaultBox.Maximum = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.ThresholdDefaultBox.Minimum = new decimal(new int[] {
            140,
            0,
            0,
            -2147483648});
            this.ThresholdDefaultBox.Name = "ThresholdDefaultBox";
            this.ThresholdDefaultBox.ValueChanged += new System.EventHandler(this.ChangeValueSuppression);
            this.ThresholdDefaultBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.KeyUpSuppression);
            // 
            // ThresholdDefaultLabel
            // 
            resources.ApplyResources(this.ThresholdDefaultLabel, "ThresholdDefaultLabel");
            this.ThresholdDefaultLabel.ForeColor = System.Drawing.Color.Black;
            this.ThresholdDefaultLabel.Name = "ThresholdDefaultLabel";
            // 
            // TimeRadiatFWSBox
            // 
            resources.ApplyResources(this.TimeRadiatFWSBox, "TimeRadiatFWSBox");
            this.TimeRadiatFWSBox.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.TimeRadiatFWSBox.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.TimeRadiatFWSBox.Name = "TimeRadiatFWSBox";
            this.TimeRadiatFWSBox.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.TimeRadiatFWSBox.ValueChanged += new System.EventHandler(this.ChangeValueSuppression);
            this.TimeRadiatFWSBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.KeyUpSuppression);
            // 
            // TimeRadiatFWSLabel
            // 
            resources.ApplyResources(this.TimeRadiatFWSLabel, "TimeRadiatFWSLabel");
            this.TimeRadiatFWSLabel.ForeColor = System.Drawing.Color.Black;
            this.TimeRadiatFWSLabel.Name = "TimeRadiatFWSLabel";
            // 
            // ChannelUseBox
            // 
            resources.ApplyResources(this.ChannelUseBox, "ChannelUseBox");
            this.ChannelUseBox.Maximum = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.ChannelUseBox.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.ChannelUseBox.Name = "ChannelUseBox";
            this.ChannelUseBox.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.ChannelUseBox.ValueChanged += new System.EventHandler(this.ChangeValueSuppression);
            this.ChannelUseBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.KeyUpSuppression);
            // 
            // ChannelUseLatter
            // 
            resources.ApplyResources(this.ChannelUseLatter, "ChannelUseLatter");
            this.ChannelUseLatter.ForeColor = System.Drawing.Color.Black;
            this.ChannelUseLatter.Name = "ChannelUseLatter";
            // 
            // CircleBox
            // 
            resources.ApplyResources(this.CircleBox, "CircleBox");
            this.CircleBox.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.CircleBox.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.CircleBox.Name = "CircleBox";
            this.CircleBox.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.CircleBox.ValueChanged += new System.EventHandler(this.ChangeValueSuppression);
            this.CircleBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.KeyUpSuppression);
            // 
            // CircleLabel
            // 
            resources.ApplyResources(this.CircleLabel, "CircleLabel");
            this.CircleLabel.ForeColor = System.Drawing.Color.Black;
            this.CircleLabel.Name = "CircleLabel";
            // 
            // tbAPRCH
            // 
            this.tbAPRCH.BackColor = System.Drawing.SystemColors.Control;
            this.tbAPRCH.Controls.Add(this.groupBox9);
            resources.ApplyResources(this.tbAPRCH, "tbAPRCH");
            this.tbAPRCH.Name = "tbAPRCH";
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.SearchBandBox);
            this.groupBox9.Controls.Add(this.label17);
            this.groupBox9.Controls.Add(this.SearchArcBox);
            this.groupBox9.Controls.Add(this.label18);
            this.groupBox9.Controls.Add(this.SearchTimeBox);
            this.groupBox9.Controls.Add(this.label19);
            this.groupBox9.ForeColor = System.Drawing.Color.Blue;
            resources.ApplyResources(this.groupBox9, "groupBox9");
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.TabStop = false;
            // 
            // SearchBandBox
            // 
            resources.ApplyResources(this.SearchBandBox, "SearchBandBox");
            this.SearchBandBox.Maximum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.SearchBandBox.Name = "SearchBandBox";
            this.SearchBandBox.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.SearchBandBox.ValueChanged += new System.EventHandler(this.ChangeValueSuppression);
            this.SearchBandBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.KeyUpSuppression);
            // 
            // label17
            // 
            resources.ApplyResources(this.label17, "label17");
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Name = "label17";
            // 
            // SearchArcBox
            // 
            resources.ApplyResources(this.SearchArcBox, "SearchArcBox");
            this.SearchArcBox.Maximum = new decimal(new int[] {
            180,
            0,
            0,
            0});
            this.SearchArcBox.Name = "SearchArcBox";
            this.SearchArcBox.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.SearchArcBox.ValueChanged += new System.EventHandler(this.ChangeValueSuppression);
            this.SearchArcBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.KeyUpSuppression);
            // 
            // label18
            // 
            resources.ApplyResources(this.label18, "label18");
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Name = "label18";
            // 
            // SearchTimeBox
            // 
            resources.ApplyResources(this.SearchTimeBox, "SearchTimeBox");
            this.SearchTimeBox.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.SearchTimeBox.Name = "SearchTimeBox";
            this.SearchTimeBox.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.SearchTimeBox.ValueChanged += new System.EventHandler(this.ChangeValueSuppression);
            this.SearchTimeBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.KeyUpSuppression);
            // 
            // label19
            // 
            resources.ApplyResources(this.label19, "label19");
            this.label19.ForeColor = System.Drawing.Color.Black;
            this.label19.Name = "label19";
            // 
            // tbPPRCH
            // 
            this.tbPPRCH.BackColor = System.Drawing.SystemColors.Control;
            this.tbPPRCH.Controls.Add(this.groupBox2);
            resources.ApplyResources(this.tbPPRCH, "tbPPRCH");
            this.tbPPRCH.Name = "tbPPRCH";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.CodeFFTBox);
            this.groupBox2.Controls.Add(this.CodeFftLabel);
            this.groupBox2.Controls.Add(this.TimeRadiatFHSSBox);
            this.groupBox2.Controls.Add(this.TimeRadiatFHSSLabel);
            this.groupBox2.ForeColor = System.Drawing.Color.Blue;
            resources.ApplyResources(this.groupBox2, "groupBox2");
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.TabStop = false;
            // 
            // CodeFFTBox
            // 
            this.CodeFFTBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CodeFFTBox.FormattingEnabled = true;
            this.CodeFFTBox.Items.AddRange(new object[] {
            resources.GetString("CodeFFTBox.Items"),
            resources.GetString("CodeFFTBox.Items1"),
            resources.GetString("CodeFFTBox.Items2")});
            resources.ApplyResources(this.CodeFFTBox, "CodeFFTBox");
            this.CodeFFTBox.Name = "CodeFFTBox";
            this.CodeFFTBox.SelectedIndexChanged += new System.EventHandler(this.ChangeValueSuppression);
            // 
            // CodeFftLabel
            // 
            resources.ApplyResources(this.CodeFftLabel, "CodeFftLabel");
            this.CodeFftLabel.ForeColor = System.Drawing.Color.Black;
            this.CodeFftLabel.Name = "CodeFftLabel";
            // 
            // TimeRadiatFHSSBox
            // 
            resources.ApplyResources(this.TimeRadiatFHSSBox, "TimeRadiatFHSSBox");
            this.TimeRadiatFHSSBox.Maximum = new decimal(new int[] {
            6000,
            0,
            0,
            0});
            this.TimeRadiatFHSSBox.Name = "TimeRadiatFHSSBox";
            this.TimeRadiatFHSSBox.ValueChanged += new System.EventHandler(this.ChangeValueSuppression);
            this.TimeRadiatFHSSBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.KeyUpSuppression);
            // 
            // TimeRadiatFHSSLabel
            // 
            resources.ApplyResources(this.TimeRadiatFHSSLabel, "TimeRadiatFHSSLabel");
            this.TimeRadiatFHSSLabel.ForeColor = System.Drawing.Color.Black;
            this.TimeRadiatFHSSLabel.Name = "TimeRadiatFHSSLabel";
            // 
            // RegimeRadioSupprBox
            // 
            this.RegimeRadioSupprBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.RegimeRadioSupprBox.FormattingEnabled = true;
            this.RegimeRadioSupprBox.Items.AddRange(new object[] {
            resources.GetString("RegimeRadioSupprBox.Items"),
            resources.GetString("RegimeRadioSupprBox.Items1"),
            resources.GetString("RegimeRadioSupprBox.Items2"),
            resources.GetString("RegimeRadioSupprBox.Items3")});
            resources.ApplyResources(this.RegimeRadioSupprBox, "RegimeRadioSupprBox");
            this.RegimeRadioSupprBox.Name = "RegimeRadioSupprBox";
            this.RegimeRadioSupprBox.SelectedIndexChanged += new System.EventHandler(this.ChangeValueSuppression);
            // 
            // ChoiceModeRSlabel
            // 
            resources.ApplyResources(this.ChoiceModeRSlabel, "ChoiceModeRSlabel");
            this.ChoiceModeRSlabel.Name = "ChoiceModeRSlabel";
            // 
            // tbColor
            // 
            this.tbColor.BackColor = System.Drawing.SystemColors.Control;
            this.tbColor.Controls.Add(this.grbColorObject);
            this.tbColor.Controls.Add(this.grbColorPanorama);
            resources.ApplyResources(this.tbColor, "tbColor");
            this.tbColor.Name = "tbColor";
            // 
            // grbColorObject
            // 
            this.grbColorObject.Controls.Add(this.label3);
            this.grbColorObject.Controls.Add(this.ButRange1575_3000);
            this.grbColorObject.Controls.Add(this.ButRange1215_1575);
            this.grbColorObject.Controls.Add(this.ButRange860_1215);
            this.grbColorObject.Controls.Add(this.ButRange400_512);
            this.grbColorObject.Controls.Add(this.ButRange512_860);
            this.grbColorObject.Controls.Add(this.ButRange220_400);
            this.grbColorObject.Controls.Add(this.ButRange170_220);
            this.grbColorObject.Controls.Add(this.ButRange108_170);
            this.grbColorObject.Controls.Add(this.ButRange88_108);
            this.grbColorObject.Controls.Add(this.ButRange30_88);
            this.grbColorObject.Controls.Add(this.Range1575);
            this.grbColorObject.Controls.Add(this.Range1215);
            this.grbColorObject.Controls.Add(this.Range860);
            this.grbColorObject.Controls.Add(this.Range512);
            this.grbColorObject.Controls.Add(this.Range400);
            this.grbColorObject.Controls.Add(this.Range220);
            this.grbColorObject.Controls.Add(this.Range170);
            this.grbColorObject.Controls.Add(this.Range108);
            this.grbColorObject.Controls.Add(this.Range88);
            this.grbColorObject.Controls.Add(this.LablRange3000);
            this.grbColorObject.Controls.Add(this.LablRange6000);
            this.grbColorObject.ForeColor = System.Drawing.Color.Blue;
            resources.ApplyResources(this.grbColorObject, "grbColorObject");
            this.grbColorObject.Name = "grbColorObject";
            this.grbColorObject.TabStop = false;
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label3.Name = "label3";
            // 
            // ButRange1575_3000
            // 
            resources.ApplyResources(this.ButRange1575_3000, "ButRange1575_3000");
            this.ButRange1575_3000.Name = "ButRange1575_3000";
            this.ButRange1575_3000.UseVisualStyleBackColor = true;
            this.ButRange1575_3000.Click += new System.EventHandler(this.ChangeColor);
            // 
            // ButRange1215_1575
            // 
            resources.ApplyResources(this.ButRange1215_1575, "ButRange1215_1575");
            this.ButRange1215_1575.Name = "ButRange1215_1575";
            this.ButRange1215_1575.UseVisualStyleBackColor = true;
            this.ButRange1215_1575.Click += new System.EventHandler(this.ChangeColor);
            // 
            // ButRange860_1215
            // 
            resources.ApplyResources(this.ButRange860_1215, "ButRange860_1215");
            this.ButRange860_1215.Name = "ButRange860_1215";
            this.ButRange860_1215.UseVisualStyleBackColor = true;
            this.ButRange860_1215.Click += new System.EventHandler(this.ChangeColor);
            // 
            // ButRange400_512
            // 
            resources.ApplyResources(this.ButRange400_512, "ButRange400_512");
            this.ButRange400_512.Name = "ButRange400_512";
            this.ButRange400_512.UseVisualStyleBackColor = true;
            this.ButRange400_512.Click += new System.EventHandler(this.ChangeColor);
            // 
            // ButRange512_860
            // 
            resources.ApplyResources(this.ButRange512_860, "ButRange512_860");
            this.ButRange512_860.Name = "ButRange512_860";
            this.ButRange512_860.UseVisualStyleBackColor = true;
            this.ButRange512_860.Click += new System.EventHandler(this.ChangeColor);
            // 
            // ButRange220_400
            // 
            resources.ApplyResources(this.ButRange220_400, "ButRange220_400");
            this.ButRange220_400.Name = "ButRange220_400";
            this.ButRange220_400.UseVisualStyleBackColor = true;
            this.ButRange220_400.Click += new System.EventHandler(this.ChangeColor);
            // 
            // ButRange170_220
            // 
            resources.ApplyResources(this.ButRange170_220, "ButRange170_220");
            this.ButRange170_220.Name = "ButRange170_220";
            this.ButRange170_220.UseVisualStyleBackColor = true;
            this.ButRange170_220.Click += new System.EventHandler(this.ChangeColor);
            // 
            // ButRange108_170
            // 
            resources.ApplyResources(this.ButRange108_170, "ButRange108_170");
            this.ButRange108_170.Name = "ButRange108_170";
            this.ButRange108_170.UseVisualStyleBackColor = true;
            this.ButRange108_170.Click += new System.EventHandler(this.ChangeColor);
            // 
            // ButRange88_108
            // 
            resources.ApplyResources(this.ButRange88_108, "ButRange88_108");
            this.ButRange88_108.Name = "ButRange88_108";
            this.ButRange88_108.UseVisualStyleBackColor = true;
            this.ButRange88_108.Click += new System.EventHandler(this.ChangeColor);
            // 
            // ButRange30_88
            // 
            resources.ApplyResources(this.ButRange30_88, "ButRange30_88");
            this.ButRange30_88.Name = "ButRange30_88";
            this.ButRange30_88.UseVisualStyleBackColor = true;
            this.ButRange30_88.Click += new System.EventHandler(this.ChangeColor);
            // 
            // Range1575
            // 
            resources.ApplyResources(this.Range1575, "Range1575");
            this.Range1575.ForeColor = System.Drawing.Color.Black;
            this.Range1575.Name = "Range1575";
            // 
            // Range1215
            // 
            resources.ApplyResources(this.Range1215, "Range1215");
            this.Range1215.ForeColor = System.Drawing.Color.Black;
            this.Range1215.Name = "Range1215";
            // 
            // Range860
            // 
            resources.ApplyResources(this.Range860, "Range860");
            this.Range860.ForeColor = System.Drawing.Color.Black;
            this.Range860.Name = "Range860";
            // 
            // Range512
            // 
            resources.ApplyResources(this.Range512, "Range512");
            this.Range512.ForeColor = System.Drawing.Color.Black;
            this.Range512.Name = "Range512";
            // 
            // Range400
            // 
            resources.ApplyResources(this.Range400, "Range400");
            this.Range400.ForeColor = System.Drawing.Color.Black;
            this.Range400.Name = "Range400";
            // 
            // Range220
            // 
            resources.ApplyResources(this.Range220, "Range220");
            this.Range220.ForeColor = System.Drawing.Color.Black;
            this.Range220.Name = "Range220";
            // 
            // Range170
            // 
            resources.ApplyResources(this.Range170, "Range170");
            this.Range170.ForeColor = System.Drawing.Color.Black;
            this.Range170.Name = "Range170";
            // 
            // Range108
            // 
            resources.ApplyResources(this.Range108, "Range108");
            this.Range108.ForeColor = System.Drawing.Color.Black;
            this.Range108.Name = "Range108";
            // 
            // Range88
            // 
            resources.ApplyResources(this.Range88, "Range88");
            this.Range88.ForeColor = System.Drawing.Color.Black;
            this.Range88.Name = "Range88";
            // 
            // LablRange3000
            // 
            resources.ApplyResources(this.LablRange3000, "LablRange3000");
            this.LablRange3000.ForeColor = System.Drawing.Color.Black;
            this.LablRange3000.Name = "LablRange3000";
            // 
            // LablRange6000
            // 
            resources.ApplyResources(this.LablRange6000, "LablRange6000");
            this.LablRange6000.ForeColor = System.Drawing.Color.Black;
            this.LablRange6000.Name = "LablRange6000";
            // 
            // grbColorPanorama
            // 
            this.grbColorPanorama.BackColor = System.Drawing.SystemColors.Control;
            this.grbColorPanorama.Controls.Add(this.ButGraphFT1);
            this.grbColorPanorama.Controls.Add(this.LabGraphFT1);
            this.grbColorPanorama.Controls.Add(this.ButGraphFT0);
            this.grbColorPanorama.Controls.Add(this.LabGraphFT0);
            this.grbColorPanorama.Controls.Add(this.ButGrid);
            this.grbColorPanorama.Controls.Add(this.label2);
            this.grbColorPanorama.Controls.Add(this.ButGraphFT);
            this.grbColorPanorama.Controls.Add(this.ButGraphFD);
            this.grbColorPanorama.Controls.Add(this.ButGraphFA);
            this.grbColorPanorama.Controls.Add(this.ButGlobalBackGround);
            this.grbColorPanorama.Controls.Add(this.ButLabel);
            this.grbColorPanorama.Controls.Add(this.ButCross);
            this.grbColorPanorama.Controls.Add(this.ButThreshold);
            this.grbColorPanorama.Controls.Add(this.ButBackGround);
            this.grbColorPanorama.Controls.Add(this.LabGraphFT);
            this.grbColorPanorama.Controls.Add(this.LabGraphFD);
            this.grbColorPanorama.Controls.Add(this.LabGraphFA);
            this.grbColorPanorama.Controls.Add(this.LabGlobalBackGround);
            this.grbColorPanorama.Controls.Add(this.LabLabel);
            this.grbColorPanorama.Controls.Add(this.labCross);
            this.grbColorPanorama.Controls.Add(this.LabThreshold);
            this.grbColorPanorama.Controls.Add(this.LabBackGround);
            this.grbColorPanorama.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.grbColorPanorama.ForeColor = System.Drawing.Color.Blue;
            resources.ApplyResources(this.grbColorPanorama, "grbColorPanorama");
            this.grbColorPanorama.Name = "grbColorPanorama";
            this.grbColorPanorama.TabStop = false;
            // 
            // ButGraphFT1
            // 
            resources.ApplyResources(this.ButGraphFT1, "ButGraphFT1");
            this.ButGraphFT1.Name = "ButGraphFT1";
            this.ButGraphFT1.UseVisualStyleBackColor = true;
            // 
            // LabGraphFT1
            // 
            resources.ApplyResources(this.LabGraphFT1, "LabGraphFT1");
            this.LabGraphFT1.ForeColor = System.Drawing.Color.Black;
            this.LabGraphFT1.Name = "LabGraphFT1";
            // 
            // ButGraphFT0
            // 
            resources.ApplyResources(this.ButGraphFT0, "ButGraphFT0");
            this.ButGraphFT0.Name = "ButGraphFT0";
            this.ButGraphFT0.UseVisualStyleBackColor = true;
            // 
            // LabGraphFT0
            // 
            resources.ApplyResources(this.LabGraphFT0, "LabGraphFT0");
            this.LabGraphFT0.ForeColor = System.Drawing.Color.Black;
            this.LabGraphFT0.Name = "LabGraphFT0";
            // 
            // ButGrid
            // 
            resources.ApplyResources(this.ButGrid, "ButGrid");
            this.ButGrid.Name = "ButGrid";
            this.ButGrid.UseVisualStyleBackColor = true;
            this.ButGrid.Click += new System.EventHandler(this.ChangeColor);
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Name = "label2";
            // 
            // ButGraphFT
            // 
            resources.ApplyResources(this.ButGraphFT, "ButGraphFT");
            this.ButGraphFT.Name = "ButGraphFT";
            this.ButGraphFT.UseVisualStyleBackColor = true;
            this.ButGraphFT.Click += new System.EventHandler(this.ChangeColor);
            // 
            // ButGraphFD
            // 
            resources.ApplyResources(this.ButGraphFD, "ButGraphFD");
            this.ButGraphFD.Name = "ButGraphFD";
            this.ButGraphFD.UseVisualStyleBackColor = true;
            this.ButGraphFD.Click += new System.EventHandler(this.ChangeColor);
            // 
            // ButGraphFA
            // 
            resources.ApplyResources(this.ButGraphFA, "ButGraphFA");
            this.ButGraphFA.Name = "ButGraphFA";
            this.ButGraphFA.UseVisualStyleBackColor = true;
            this.ButGraphFA.Click += new System.EventHandler(this.ChangeColor);
            // 
            // ButGlobalBackGround
            // 
            resources.ApplyResources(this.ButGlobalBackGround, "ButGlobalBackGround");
            this.ButGlobalBackGround.Name = "ButGlobalBackGround";
            this.ButGlobalBackGround.UseVisualStyleBackColor = true;
            this.ButGlobalBackGround.Click += new System.EventHandler(this.ChangeColor);
            // 
            // ButLabel
            // 
            resources.ApplyResources(this.ButLabel, "ButLabel");
            this.ButLabel.Name = "ButLabel";
            this.ButLabel.UseVisualStyleBackColor = true;
            this.ButLabel.Click += new System.EventHandler(this.ChangeColor);
            // 
            // ButCross
            // 
            resources.ApplyResources(this.ButCross, "ButCross");
            this.ButCross.Name = "ButCross";
            this.ButCross.UseVisualStyleBackColor = true;
            this.ButCross.Click += new System.EventHandler(this.ChangeColor);
            // 
            // ButThreshold
            // 
            resources.ApplyResources(this.ButThreshold, "ButThreshold");
            this.ButThreshold.Name = "ButThreshold";
            this.ButThreshold.UseVisualStyleBackColor = true;
            this.ButThreshold.Click += new System.EventHandler(this.ChangeColor);
            // 
            // ButBackGround
            // 
            resources.ApplyResources(this.ButBackGround, "ButBackGround");
            this.ButBackGround.Name = "ButBackGround";
            this.ButBackGround.UseVisualStyleBackColor = true;
            this.ButBackGround.Click += new System.EventHandler(this.ChangeColor);
            // 
            // LabGraphFT
            // 
            resources.ApplyResources(this.LabGraphFT, "LabGraphFT");
            this.LabGraphFT.ForeColor = System.Drawing.Color.Black;
            this.LabGraphFT.Name = "LabGraphFT";
            // 
            // LabGraphFD
            // 
            resources.ApplyResources(this.LabGraphFD, "LabGraphFD");
            this.LabGraphFD.ForeColor = System.Drawing.Color.Black;
            this.LabGraphFD.Name = "LabGraphFD";
            // 
            // LabGraphFA
            // 
            resources.ApplyResources(this.LabGraphFA, "LabGraphFA");
            this.LabGraphFA.ForeColor = System.Drawing.Color.Black;
            this.LabGraphFA.Name = "LabGraphFA";
            // 
            // LabGlobalBackGround
            // 
            resources.ApplyResources(this.LabGlobalBackGround, "LabGlobalBackGround");
            this.LabGlobalBackGround.ForeColor = System.Drawing.Color.Black;
            this.LabGlobalBackGround.Name = "LabGlobalBackGround";
            // 
            // LabLabel
            // 
            resources.ApplyResources(this.LabLabel, "LabLabel");
            this.LabLabel.ForeColor = System.Drawing.Color.Black;
            this.LabLabel.Name = "LabLabel";
            // 
            // labCross
            // 
            resources.ApplyResources(this.labCross, "labCross");
            this.labCross.ForeColor = System.Drawing.Color.Black;
            this.labCross.Name = "labCross";
            // 
            // LabThreshold
            // 
            resources.ApplyResources(this.LabThreshold, "LabThreshold");
            this.LabThreshold.ForeColor = System.Drawing.Color.Black;
            this.LabThreshold.Name = "LabThreshold";
            // 
            // LabBackGround
            // 
            resources.ApplyResources(this.LabBackGround, "LabBackGround");
            this.LabBackGround.ForeColor = System.Drawing.Color.Black;
            this.LabBackGround.Name = "LabBackGround";
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage3.Controls.Add(this.groupBoxHS);
            this.tabPage3.Controls.Add(this.groupBoxASP);
            this.tabPage3.Controls.Add(this.groupBoxPU);
            resources.ApplyResources(this.tabPage3, "tabPage3");
            this.tabPage3.Name = "tabPage3";
            // 
            // groupBoxHS
            // 
            this.groupBoxHS.Controls.Add(this.ButHS2);
            this.groupBoxHS.Controls.Add(this.ButHS1);
            this.groupBoxHS.ForeColor = System.Drawing.Color.Blue;
            resources.ApplyResources(this.groupBoxHS, "groupBoxHS");
            this.groupBoxHS.Name = "groupBoxHS";
            this.groupBoxHS.TabStop = false;
            // 
            // ButHS2
            // 
            resources.ApplyResources(this.ButHS2, "ButHS2");
            this.ButHS2.ForeColor = System.Drawing.Color.Black;
            this.ButHS2.Name = "ButHS2";
            this.ButHS2.TabStop = true;
            this.ButHS2.UseVisualStyleBackColor = true;
            this.ButHS2.CheckedChanged += new System.EventHandler(this.ButHS_CheckedChanged);
            // 
            // ButHS1
            // 
            resources.ApplyResources(this.ButHS1, "ButHS1");
            this.ButHS1.ForeColor = System.Drawing.Color.Black;
            this.ButHS1.Name = "ButHS1";
            this.ButHS1.TabStop = true;
            this.ButHS1.UseVisualStyleBackColor = true;
            this.ButHS1.CheckedChanged += new System.EventHandler(this.ButHS_CheckedChanged);
            // 
            // groupBoxASP
            // 
            this.groupBoxASP.Controls.Add(this.ButASP4);
            this.groupBoxASP.Controls.Add(this.ButASP3);
            this.groupBoxASP.Controls.Add(this.ButASP2);
            this.groupBoxASP.Controls.Add(this.ButASP1);
            this.groupBoxASP.ForeColor = System.Drawing.Color.Blue;
            resources.ApplyResources(this.groupBoxASP, "groupBoxASP");
            this.groupBoxASP.Name = "groupBoxASP";
            this.groupBoxASP.TabStop = false;
            // 
            // ButASP4
            // 
            resources.ApplyResources(this.ButASP4, "ButASP4");
            this.ButASP4.ForeColor = System.Drawing.Color.Black;
            this.ButASP4.Name = "ButASP4";
            this.ButASP4.TabStop = true;
            this.ButASP4.UseVisualStyleBackColor = true;
            this.ButASP4.CheckedChanged += new System.EventHandler(this.ButASP_CheckedChanged);
            // 
            // ButASP3
            // 
            resources.ApplyResources(this.ButASP3, "ButASP3");
            this.ButASP3.ForeColor = System.Drawing.Color.Black;
            this.ButASP3.Name = "ButASP3";
            this.ButASP3.TabStop = true;
            this.ButASP3.UseVisualStyleBackColor = true;
            this.ButASP3.CheckedChanged += new System.EventHandler(this.ButASP_CheckedChanged);
            // 
            // ButASP2
            // 
            resources.ApplyResources(this.ButASP2, "ButASP2");
            this.ButASP2.ForeColor = System.Drawing.Color.Black;
            this.ButASP2.Name = "ButASP2";
            this.ButASP2.TabStop = true;
            this.ButASP2.UseVisualStyleBackColor = true;
            this.ButASP2.CheckedChanged += new System.EventHandler(this.ButASP_CheckedChanged);
            // 
            // ButASP1
            // 
            resources.ApplyResources(this.ButASP1, "ButASP1");
            this.ButASP1.ForeColor = System.Drawing.Color.Black;
            this.ButASP1.Name = "ButASP1";
            this.ButASP1.TabStop = true;
            this.ButASP1.UseVisualStyleBackColor = true;
            this.ButASP1.CheckedChanged += new System.EventHandler(this.ButASP_CheckedChanged);
            // 
            // groupBoxPU
            // 
            this.groupBoxPU.Controls.Add(this.ButPU4);
            this.groupBoxPU.Controls.Add(this.ButPU3);
            this.groupBoxPU.Controls.Add(this.ButPU2);
            this.groupBoxPU.Controls.Add(this.ButPU1);
            this.groupBoxPU.ForeColor = System.Drawing.Color.Blue;
            resources.ApplyResources(this.groupBoxPU, "groupBoxPU");
            this.groupBoxPU.Name = "groupBoxPU";
            this.groupBoxPU.TabStop = false;
            // 
            // ButPU4
            // 
            resources.ApplyResources(this.ButPU4, "ButPU4");
            this.ButPU4.ForeColor = System.Drawing.Color.Black;
            this.ButPU4.Name = "ButPU4";
            this.ButPU4.TabStop = true;
            this.ButPU4.UseVisualStyleBackColor = true;
            this.ButPU4.CheckedChanged += new System.EventHandler(this.ButPU_CheckedChanged);
            // 
            // ButPU3
            // 
            resources.ApplyResources(this.ButPU3, "ButPU3");
            this.ButPU3.ForeColor = System.Drawing.Color.Black;
            this.ButPU3.Name = "ButPU3";
            this.ButPU3.TabStop = true;
            this.ButPU3.UseVisualStyleBackColor = true;
            this.ButPU3.CheckedChanged += new System.EventHandler(this.ButPU_CheckedChanged);
            // 
            // ButPU2
            // 
            resources.ApplyResources(this.ButPU2, "ButPU2");
            this.ButPU2.ForeColor = System.Drawing.Color.Black;
            this.ButPU2.Name = "ButPU2";
            this.ButPU2.TabStop = true;
            this.ButPU2.UseVisualStyleBackColor = true;
            this.ButPU2.CheckedChanged += new System.EventHandler(this.ButPU_CheckedChanged);
            // 
            // ButPU1
            // 
            resources.ApplyResources(this.ButPU1, "ButPU1");
            this.ButPU1.ForeColor = System.Drawing.Color.Black;
            this.ButPU1.Name = "ButPU1";
            this.ButPU1.TabStop = true;
            this.ButPU1.UseVisualStyleBackColor = true;
            this.ButPU1.CheckedChanged += new System.EventHandler(this.ButPU_CheckedChanged);
            // 
            // Setting
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.Controls.Add(this.tbSetting);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Setting";
            this.TopMost = true;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Setting_FormClosing);
            this.Load += new System.EventHandler(this.Setting_Load);
            this.tbSetting.ResumeLayout(false);
            this.tbGeneral.ResumeLayout(false);
            this.InfoGroupBox.ResumeLayout(false);
            this.InfoGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.OwnAddressPCBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.JammerLinkedAddressPCBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.JammerLinkedNumberBox)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.groupBox8.ResumeLayout(false);
            this.TableLayoutPanel.ResumeLayout(false);
            this.TableLayoutPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AttenuatorTank)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RelativeBearingBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AveragingBox)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TimeMonitorBox)).EndInit();
            this.tabPage1.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabLetterOwn.ResumeLayout(false);
            this.tabLetterOwn.PerformLayout();
            this.tabLetterMated.ResumeLayout(false);
            this.tabLetterMated.PerformLayout();
            this.GeneralTab.ResumeLayout(false);
            this.GeneralTab.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CountSourceLetterBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CountJammerLinkedLetterBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CountOwnLetterBox)).EndInit();
            this.tbRP.ResumeLayout(false);
            this.tbRP.PerformLayout();
            this.tbControlRP.ResumeLayout(false);
            this.tbFRCH.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.UPBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PriorHandleBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ThresholdDefaultBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TimeRadiatFWSBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChannelUseBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CircleBox)).EndInit();
            this.tbAPRCH.ResumeLayout(false);
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SearchBandBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SearchArcBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SearchTimeBox)).EndInit();
            this.tbPPRCH.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TimeRadiatFHSSBox)).EndInit();
            this.tbColor.ResumeLayout(false);
            this.grbColorObject.ResumeLayout(false);
            this.grbColorObject.PerformLayout();
            this.grbColorPanorama.ResumeLayout(false);
            this.grbColorPanorama.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.groupBoxHS.ResumeLayout(false);
            this.groupBoxHS.PerformLayout();
            this.groupBoxASP.ResumeLayout(false);
            this.groupBoxASP.PerformLayout();
            this.groupBoxPU.ResumeLayout(false);
            this.groupBoxPU.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tbSetting;
        private System.Windows.Forms.TabPage tbColor;
        private System.Windows.Forms.GroupBox grbColorObject;
        private System.Windows.Forms.GroupBox grbColorPanorama;
        private System.Windows.Forms.Label LabGraphFT;
        private System.Windows.Forms.Label LabGraphFD;
        private System.Windows.Forms.Label LabGraphFA;
        private System.Windows.Forms.Label LabGlobalBackGround;
        private System.Windows.Forms.Label LabLabel;
        private System.Windows.Forms.Label labCross;
        private System.Windows.Forms.Label LabThreshold;
        private System.Windows.Forms.Label LabBackGround;
        private System.Windows.Forms.TabPage tbGeneral;
        private System.Windows.Forms.ColorDialog colorDialog;
        private System.Windows.Forms.Button ButRange1575_3000;
        private System.Windows.Forms.Button ButRange1215_1575;
        private System.Windows.Forms.Button ButRange860_1215;
        private System.Windows.Forms.Button ButRange400_512;
        private System.Windows.Forms.Button ButRange512_860;
        private System.Windows.Forms.Button ButRange220_400;
        private System.Windows.Forms.Button ButRange170_220;
        private System.Windows.Forms.Button ButRange108_170;
        private System.Windows.Forms.Button ButRange88_108;
        private System.Windows.Forms.Button ButRange30_88;
        private System.Windows.Forms.Label LablRange3000;
        private System.Windows.Forms.Label Range1575;
        private System.Windows.Forms.Label Range1215;
        private System.Windows.Forms.Label Range860;
        private System.Windows.Forms.Label Range512;
        private System.Windows.Forms.Label Range400;
        private System.Windows.Forms.Label Range220;
        private System.Windows.Forms.Label Range170;
        private System.Windows.Forms.Label Range108;
        private System.Windows.Forms.Label Range88;
        private System.Windows.Forms.Button ButGraphFT;
        private System.Windows.Forms.Button ButGraphFD;
        private System.Windows.Forms.Button ButGraphFA;
        private System.Windows.Forms.Button ButGlobalBackGround;
        private System.Windows.Forms.Button ButLabel;
        private System.Windows.Forms.Button ButCross;
        private System.Windows.Forms.Button ButThreshold;
        private System.Windows.Forms.Button ButBackGround;
        private System.Windows.Forms.Button ButGrid;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox InfoGroupBox;
        private System.Windows.Forms.MaskedTextBox OwnNumberBox;
        private System.Windows.Forms.NumericUpDown JammerLinkedAddressPCBox;
        private System.Windows.Forms.Label JamLinkAddPcLabel;
        private System.Windows.Forms.NumericUpDown JammerLinkedNumberBox;
        private System.Windows.Forms.Label JamLinkNumLabel;
        private System.Windows.Forms.NumericUpDown OwnAddressPCBox;
        private System.Windows.Forms.Label OwnAddrPcLabel;
        private System.Windows.Forms.ComboBox LanguageBox;
        private System.Windows.Forms.Label LanguageLabel;
        private System.Windows.Forms.ComboBox RoleBox;
        private System.Windows.Forms.Label RoleLabel;
        private System.Windows.Forms.Label OwnNumLabel;
        private System.Windows.Forms.TextBox PostControlCallSignBox;
        private System.Windows.Forms.Label PostConLabel;
        private System.Windows.Forms.TextBox JammerLinkedCallSignBox;
        private System.Windows.Forms.Label JamLinkLabel;
        private System.Windows.Forms.TextBox OwnCallSignBox;
        private System.Windows.Forms.Label OwnLabel;
        private System.Windows.Forms.TabPage tbRP;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.NumericUpDown TimeRadiatFWSBox;
        private System.Windows.Forms.Label TimeRadiatFWSLabel;
        private System.Windows.Forms.NumericUpDown ChannelUseBox;
        private System.Windows.Forms.Label ChannelUseLatter;
        private System.Windows.Forms.NumericUpDown CircleBox;
        private System.Windows.Forms.Label CircleLabel;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.NumericUpDown TimeRadiatFHSSBox;
        private System.Windows.Forms.Label TimeRadiatFHSSLabel;
        private System.Windows.Forms.NumericUpDown ThresholdDefaultBox;
        private System.Windows.Forms.Label ThresholdDefaultLabel;
        private System.Windows.Forms.ComboBox CodeFFTBox;
        private System.Windows.Forms.Label CodeFftLabel;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabLetterOwn;
        private System.Windows.Forms.CheckBox OwnLetter7Box;
        private System.Windows.Forms.CheckBox OwnLetter6Box;
        private System.Windows.Forms.CheckBox OwnLetter5Box;
        private System.Windows.Forms.CheckBox OwnLetter4Box;
        private System.Windows.Forms.CheckBox OwnLetter3Box;
        private System.Windows.Forms.CheckBox OwnLetter2Box;
        private System.Windows.Forms.CheckBox OwnLetter1Box;
        private System.Windows.Forms.TabPage tabLetterMated;
        private System.Windows.Forms.CheckBox JammerLinkedLetter7Box;
        private System.Windows.Forms.CheckBox JammerLinkedLetter6Box;
        private System.Windows.Forms.CheckBox JammerLinkedLetter5Box;
        private System.Windows.Forms.CheckBox JammerLinkedLetter4Box;
        private System.Windows.Forms.CheckBox JammerLinkedLetter3Box;
        private System.Windows.Forms.CheckBox JammerLinkedLetter2Box;
        private System.Windows.Forms.CheckBox JammerLinkedLetter1Box;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TabPage GeneralTab;
        private System.Windows.Forms.NumericUpDown CountSourceLetterBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown CountJammerLinkedLetterBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown CountOwnLetterBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.NumericUpDown TimeMonitorBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown PriorHandleBox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.CheckBox BearingBox;
        private System.Windows.Forms.Button ButGraphFT1;
        private System.Windows.Forms.Label LabGraphFT1;
        private System.Windows.Forms.Button ButGraphFT0;
        private System.Windows.Forms.Label LabGraphFT0;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.GroupBox groupBoxHS;
        private System.Windows.Forms.RadioButton ButHS2;
        private System.Windows.Forms.RadioButton ButHS1;
        private System.Windows.Forms.GroupBox groupBoxASP;
        private System.Windows.Forms.RadioButton ButASP3;
        private System.Windows.Forms.RadioButton ButASP2;
        private System.Windows.Forms.RadioButton ButASP1;
        private System.Windows.Forms.GroupBox groupBoxPU;
        private System.Windows.Forms.RadioButton ButPU4;
        private System.Windows.Forms.RadioButton ButPU3;
        private System.Windows.Forms.RadioButton ButPU2;
        private System.Windows.Forms.RadioButton ButPU1;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.NumericUpDown AveragingBox;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox RegimeRadioSupprBox;
        private System.Windows.Forms.Label ChoiceModeRSlabel;
        private System.Windows.Forms.TableLayoutPanel TableLayoutPanel;
        internal System.Windows.Forms.Label TotalGainLevel;
        internal System.Windows.Forms.Label BandNumberLabel;
        internal NationalInstruments.UI.WindowsForms.Tank AttenuatorTank;
        internal System.Windows.Forms.Label AmplifierLabel;
        private System.Windows.Forms.NumericUpDown RelativeBearingBox;
        private System.Windows.Forms.CheckBox AutoRelativeBearingBox;
        private System.Windows.Forms.Label RelativeBearingLabel;
        private System.Windows.Forms.CheckBox DetectionPPRCHBox;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.NumericUpDown UPBox;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TabControl tbControlRP;
        private System.Windows.Forms.TabPage tbFRCH;
        private System.Windows.Forms.TabPage tbPPRCH;
        private System.Windows.Forms.TabPage tbAPRCH;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.NumericUpDown SearchBandBox;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.NumericUpDown SearchArcBox;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.NumericUpDown SearchTimeBox;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.CheckBox OwnLetter9Box;
        private System.Windows.Forms.CheckBox OwnLetter8Box;
        private System.Windows.Forms.CheckBox JammerLinkedLetter9Box;
        private System.Windows.Forms.CheckBox JammerLinkedLetter8Box;
        private System.Windows.Forms.CheckBox AutoClearTableBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox UseGNSSBox;
        private System.Windows.Forms.Label labelGNSS;
        private System.Windows.Forms.Label LablRange6000;
        private System.Windows.Forms.RadioButton ButASP4;
    }
}

