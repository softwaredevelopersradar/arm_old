﻿namespace WndProject
{
    partial class EL
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rtbExchangeLog = new System.Windows.Forms.RichTextBox();
            this.cbHex = new System.Windows.Forms.CheckBox();
            this.bClearRTB = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // rtbExchangeLog
            // 
            this.rtbExchangeLog.Location = new System.Drawing.Point(13, 44);
            this.rtbExchangeLog.Name = "rtbExchangeLog";
            this.rtbExchangeLog.Size = new System.Drawing.Size(468, 473);
            this.rtbExchangeLog.TabIndex = 0;
            this.rtbExchangeLog.Text = "";
            this.rtbExchangeLog.TextChanged += new System.EventHandler(this.rtbExchangeLog_TextChanged);
            // 
            // cbHex
            // 
            this.cbHex.AutoSize = true;
            this.cbHex.Location = new System.Drawing.Point(13, 13);
            this.cbHex.Name = "cbHex";
            this.cbHex.Size = new System.Drawing.Size(45, 17);
            this.cbHex.TabIndex = 1;
            this.cbHex.Text = "Hex";
            this.cbHex.UseVisualStyleBackColor = true;
            this.cbHex.CheckedChanged += new System.EventHandler(this.cbHex_CheckedChanged);
            // 
            // bClearRTB
            // 
            this.bClearRTB.Location = new System.Drawing.Point(138, 7);
            this.bClearRTB.Name = "bClearRTB";
            this.bClearRTB.Size = new System.Drawing.Size(75, 23);
            this.bClearRTB.TabIndex = 2;
            this.bClearRTB.Text = "Очистить";
            this.bClearRTB.UseVisualStyleBackColor = true;
            this.bClearRTB.Click += new System.EventHandler(this.bClearRTB_Click);
            // 
            // EL
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(490, 521);
            this.Controls.Add(this.bClearRTB);
            this.Controls.Add(this.cbHex);
            this.Controls.Add(this.rtbExchangeLog);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "EL";
            this.Text = "Журнал обмена";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.EL_FormClosing);
            this.Load += new System.EventHandler(this.EL_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox rtbExchangeLog;
        private System.Windows.Forms.CheckBox cbHex;
        private System.Windows.Forms.Button bClearRTB;
    }
}

