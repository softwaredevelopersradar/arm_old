﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using CoopProtocolMCS1MCS2;
using NLog;


namespace  CoopProtocolMCS1MCS2
{
    //[StructLayout(LayoutKind.Sequential, Pack =1)]
    public struct Cmd
    {
        public PacketServiceData packetServiceData;
        public byte[] bData;
    }


    public struct PacketServiceData
    {
        public byte bAdressSender;
        public byte bAdressReceiver;
        public byte bCode;
        public byte bCounter;
        public ushort usLengthInform;
    }

    public struct StructSupprFWS
    {
        public int iFreq;
        public byte bModulation;
        public byte bDeviation;
        public byte bManipulation;
        public byte bDuration;
        public byte bPrioritet;
        public byte bThreshold;
        public ushort sBearing;
    }

    public class TCPServer
    {
        #region constants
        static byte LEN_HEAD = 6;
        private static int LENGTH_OF_INT = 4;

        const byte CONNECTION_REQUEST_CODE = 9;
        const byte TEXT_MESSAGE_CMD = 10;
        const byte MESSAGE_APPROVED_TEXT_CMD = 11;
        const byte EXECUTE_DF_CODE = 12;
        const byte FRIEQUENCIES_FOR_SUPPRESSION_CODE = 13;
        const byte COORD_REQUEST_CODE = 14;


        //ExecuteDF
        const byte VALUE_OF_MESSAGE_APPROVED_TEXT_CMD = 0;
        const byte VALUE_OF_MESSAGE_APPROVED_FRIEQUENCIES_FOR_SUPPRESSION_CODE = 0;
        const byte VALUE_OF_WRONG_MESSAGE_FRIEQUENCIES_FOR_SUPPRESSION_CODE = 1;


        #endregion


        #region variables
        private TcpListener tcpListener;
        private TcpClient tcpClient;
        private Thread thrRead;
        private Thread thrClient;
        private NetworkStream streamClient;

        private byte COUNTER_CMD = 0;
        private byte bAdrOwn = 0;
        private byte bAdrOpponent = 0;
        #endregion

        #region events
        public delegate void ConnectEventHandler();
        public event ConnectEventHandler OnCreate;
        public event ConnectEventHandler OnDestroy;
        public event ConnectEventHandler OnConnect;
        public event ConnectEventHandler OnDisconnect;

        public delegate void ByteEventHandler(byte[] bByte);
        public event ByteEventHandler OnReadByte;
        public event ByteEventHandler OnWriteByte;
        //public event EventHandler<byte[]> OnReadByte;

        public delegate void CmdTextEventHandler(string str);
        public event CmdTextEventHandler OnReceiveTextCmd;

        public delegate void CmdConfirmTextEventHandler(byte bCodeError);
        public event CmdConfirmTextEventHandler OnReceiveConfirmTextCmd;

        public delegate void ExecuteDFCmdEventHandler(int fr1, int fr2);
        /// <summary>
        /// necessary give a response with averageBearing and CKO
        /// </summary>
        public event ExecuteDFCmdEventHandler OnReceiveExecuteDFCmd;

        public delegate void CmdFriequenciesForSuppressionEventHandler(int iDuration, StructSupprFWS[] frForSuppr);
        public event CmdFriequenciesForSuppressionEventHandler OnReceiveFriequenciesForSuppressionCmd;

        public delegate void CmdCoordRequestEventHandler();
        public event CmdCoordRequestEventHandler OnReceiveCoordRequesCmd;

        #endregion

        // create server
        protected void Create()
        {
            if (OnCreate != null)
            {
                OnCreate();//Raise the event
            }
        }

        // destroy server
        protected void Destroy()
        {

            if (OnDestroy != null)
            {
                OnDestroy();
            }
        }

        // connect client
        protected void Connect()
        {
            if (OnConnect != null)
            {
                OnConnect();
            }
        }

        // disconnect client
        protected void Disconnect()
        {
            if (OnDisconnect != null)
            {
                OnDisconnect();
            }
        }

        // read array of byte
        protected void ReadByte(byte[] bByte)
        {
            if (OnReadByte != null)
            {
                OnReadByte(bByte);
            }
        }

        // write array of byte
        protected void WriteByte(byte[] bByte)
        {
            if (OnWriteByte != null)
            {
                OnWriteByte(bByte);
            }
        }

        // read text mesage
        protected virtual void ReceiveTextCmd(string str)
        {
            if (OnReceiveTextCmd != null)
            {
                OnReceiveTextCmd(str);
            }
        }

        // read confirm mesage of text mesage (0 - is all right)
        protected virtual void ReceiveConfirmTextCmd(byte bCodeError)
        {
            if (OnReceiveConfirmTextCmd != null)
            {
                OnReceiveConfirmTextCmd(bCodeError);
            }
        }

        // read executive direct finding (fr1 - first frequence, fr2 - second frequence)
        protected virtual void ReceiveExecuteDFCmd(int fr1, int fr2)
        {
            if (OnReceiveExecuteDFCmd != null)
            {
                OnReceiveExecuteDFCmd(fr1, fr2);
            }
        }

        // read Friequencies For Suppression (frForSuppr - array of packets)
        protected virtual void ReceiveFriequenciesForSuppressionCmd(int iDuration, StructSupprFWS[] frForSuppr)
        {
            if (OnReceiveFriequenciesForSuppressionCmd != null)
            {
                OnReceiveFriequenciesForSuppressionCmd(iDuration, frForSuppr);
            }
        }

        // read Coord of the station
        protected virtual void ReceiveCoordRequesCmd()
        {
            if (OnReceiveCoordRequesCmd != null)
            {
                OnReceiveCoordRequesCmd();
            }
        }


        // constructor
        public TCPServer(byte AdrOwn, byte AdrOpponent)
        {
            bAdrOwn = AdrOwn;
            bAdrOpponent = AdrOpponent;
        }

        // create server
        public void CreateServer(string strIP, int PortOperChannel)
        {
            tcpListener = null;

            try
            {
                tcpListener = new TcpListener(IPAddress.Parse(strIP), PortOperChannel);

                tcpListener.Start();

                thrClient = new Thread(new ThreadStart(ClientThreadFunc));
                thrClient.Start();

                Create();

            }
            catch (Exception ex)
            {
                logger.Error(ex, "   " + this.ToString() + ":  " + ex);
                Destroy();
            }

        }

        // destroy server
        public void DestroyServer()
        {
            if (tcpListener != null)
            {
                tcpListener.Stop();
            }

            if (streamClient != null)
            {
                streamClient.Close();
                streamClient = null;
            }

            if (thrClient != null)
            {
                thrClient.Abort();
                thrClient.Join(500);
                thrClient = null;
            }


            if (tcpClient != null)
            {
                tcpClient.Close();
                tcpClient = null;
            }

            if (thrRead != null)
            {
                thrRead.Abort();
                thrRead.Join(500);
                thrRead = null;
            }

            Destroy();
        }

        // thread for accept client (listen to)
        private void ClientThreadFunc()
        {
            try
            {
                while (true)
                {
                    var tcpClient = tcpListener.AcceptTcpClient();
                    if (streamClient != null)
                    {
                        streamClient.Close();
                        //streamClient.Dispose();
                    }
                    streamClient = tcpClient.GetStream();

                    if (thrRead != null)
                    {
                        thrRead.Abort();
                        thrRead.Join(500);
                        thrRead = null;
                    }

                    try
                    {

                        thrRead = new Thread(new ThreadStart(ReadData));
                        thrRead.IsBackground = true;
                        thrRead.Start();

                        Connect();
                    }
                    catch (System.Exception ex)
                    {
                        DestroyServer();
                        logger.Error(ex, "   " + this.ToString() + ":  " + ex);
                    }

                }
            }
            catch (System.Exception)
            {

                DestroyServer();
            }
        }

        // disconnect client (error read)
        private void DisconnectClient()
        {
            Disconnect();

            if (thrRead != null)
            {

                thrRead.Abort();
                thrRead.Join(500);
                thrRead = null;
            }

            if (tcpClient != null)
            {
                tcpClient.Close();
                tcpClient = null;
            }


        }

        private static Logger logger = LogManager.GetCurrentClassLogger();

        // Read data (byte) (thread)
        private void ReadData()
        {
            byte[] bRead = null;
            byte[] buffer = null;
            byte[] bData = null;
            Cmd cmd = new Cmd();

            int iReadLength = 0;
            int currSize = 0;
            while (true)
            {
                try
                {
                    Array.Resize(ref buffer, LEN_HEAD);
                    Array.Resize(ref bRead, 0);
                    currSize = 0;
                    iReadLength = streamClient.Read(buffer, 0, LEN_HEAD);
                    ushort realMessageLength = BitConverter.ToUInt16(buffer, 4);

                    Array.Resize(ref bRead, realMessageLength + LEN_HEAD);
                    Array.Resize(ref buffer, realMessageLength + LEN_HEAD);
                    //new
                    Array.Copy(buffer, 0, bRead, 0, LEN_HEAD);
                    currSize += iReadLength;

                    while (currSize < bRead.Length)
                    {
                        iReadLength = streamClient.Read(buffer, 0, bRead.Length - currSize);

                        Array.Copy(buffer, 0, bRead, currSize, iReadLength);
                        currSize += iReadLength;

                        if (iReadLength == 0)
                        {
                            Disconnect();
                        }
                    }

                    if (bRead.Length > 0)
                    {
                        ReadByte(bRead);
                        try
                        {
                            if (bRead.Length >= LEN_HEAD)
                            {

                                cmd.packetServiceData.bAdressSender = bRead[0];
                                cmd.packetServiceData.bAdressReceiver = bRead[1];
                                cmd.packetServiceData.bCode = bRead[2];
                                cmd.packetServiceData.bCounter = bRead[3];
                                cmd.packetServiceData.usLengthInform = BitConverter.ToUInt16(bRead, 4);

                                //if ()
                                Array.Resize(ref bData, bRead.Length - LEN_HEAD);
                                Array.Copy(bRead, LEN_HEAD, bData, 0, bRead.Length - LEN_HEAD);
                                cmd.bData = bData;

                                switch (cmd.packetServiceData.bCode)
                                {
                                    case TEXT_MESSAGE_CMD:

                                        string str = Encoding.GetEncoding(1251).GetString(cmd.bData);
                                        ReceiveTextCmd(str);

                                        SendConfirmText(VALUE_OF_MESSAGE_APPROVED_TEXT_CMD);
                                        break;

                                    case MESSAGE_APPROVED_TEXT_CMD:
                                        byte bCodeError = cmd.bData[0];
                                        ReceiveConfirmTextCmd(bCodeError);
                                        break;

                                    case EXECUTE_DF_CODE:
                                        if (cmd.bData.Length.Equals(8)) // 8 because intx2
                                        {
                                            int fr1 = BitConverter.ToInt32(cmd.bData, 0);
                                            int fr2 = BitConverter.ToInt32(cmd.bData, LENGTH_OF_INT);
                                            ReceiveExecuteDFCmd(fr1, fr2);
                                        }
                                        else
                                        {
                                            int testAverageBearing = -1;
                                            short testCKO = -1;
                                            SendAnsweringCmdWithRecivedBearing(testAverageBearing, testCKO);

                                        }

                                        break;

                                    case FRIEQUENCIES_FOR_SUPPRESSION_CODE:

                                        int iDuration = BitConverter.ToInt32(cmd.bData, 0);
                                        byte[] structArray = new byte[cmd.bData.Length - LENGTH_OF_INT];
                                        if ((structArray.Length % 12).Equals(0))
                                        {
                                            Array.Copy(cmd.bData, LENGTH_OF_INT, structArray, 0, structArray.Length);
                                            int index = structArray.Length / 12; // amount of structs

                                            StructSupprFWS[] frForSuppr = new StructSupprFWS[index];

                                            byte[] singleStruct = new byte[12];
                                            for (int i = 0; i < index; i++)
                                            {
                                                Array.Copy(structArray, i * 12, singleStruct, 0, 12);
                                                StructSupprFWS tSupprFWS = new StructSupprFWS();
                                                object obj = tSupprFWS;
                                                Serializer.ByteArrayToStructure(singleStruct, ref obj);
                                                tSupprFWS = (StructSupprFWS)obj;
                                                frForSuppr[i] = tSupprFWS;
                                            }


                                            ReceiveFriequenciesForSuppressionCmd(iDuration, frForSuppr);
                                            SendConfirmFriequenciesForSuppression(VALUE_OF_MESSAGE_APPROVED_FRIEQUENCIES_FOR_SUPPRESSION_CODE);
                                        }
                                        else
                                        {
                                            SendConfirmFriequenciesForSuppression(VALUE_OF_WRONG_MESSAGE_FRIEQUENCIES_FOR_SUPPRESSION_CODE);
                                        }


                                        break;

                                    case COORD_REQUEST_CODE:

                                        ReceiveCoordRequesCmd();
                                        break;

                                    default:
                                        break;
                                }
                            }

                        }
                        catch (System.Exception ex)
                        {
                            logger.Error(ex, "   " + this.ToString() + ":  " + ex);
                            //GlobalVar._LogFile.LogWrite("   " + this.ToString() + ":  " + ex.TargetSite + "   " + ex.Message);
                            //MessageBox.Show(ex.Message, "ModuleServerOK.Error blNewCmd", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.ServiceNotification);
                        }

                    }
                }

                catch (System.Exception ex)
                {
                    logger.Error(ex, "   " + this.ToString() + ":  " + ex);
                    Disconnect();
                    return;
                }
            }
        }

        public void SendConnectionRequest(byte bRegime)
        {
            try
            {

                Cmd packet = new Cmd();

                packet.bData = new byte[1];
                packet.bData[0] = bRegime;
                packet.packetServiceData.bAdressSender = bAdrOwn;
                packet.packetServiceData.bAdressReceiver = bAdrOpponent;
                packet.packetServiceData.bCode = CONNECTION_REQUEST_CODE;
                packet.packetServiceData.bCounter = COUNTER_CMD++;
                packet.packetServiceData.usLengthInform = Convert.ToByte(packet.bData.Length);
                // send struct to client
                SendData(packet);
            }
            catch (System.Exception ex)
            {
                logger.Error(ex, "   " + this.ToString() + ":  " + ex);

            }
        }

        public void SendText(string strText)
        {
            try
            {
                Cmd packet = new Cmd();
                packet.bData = null;
                packet.bData = Encoding.GetEncoding(1251).GetBytes(strText);
                packet.packetServiceData.bAdressSender = bAdrOwn;
                packet.packetServiceData.bAdressReceiver = bAdrOpponent;
                packet.packetServiceData.bCode = TEXT_MESSAGE_CMD;
                packet.packetServiceData.bCounter = COUNTER_CMD++;
                packet.packetServiceData.usLengthInform = Convert.ToByte(packet.bData.Length);
                SendData(packet);
            }
            catch (System.Exception ex)
            {
                logger.Error(ex, "   " + this.ToString() + ":  " + ex);
            }
        }

        public void SendConfirmText(byte bCodeError)
        {
            try
            {
                Cmd packet = new Cmd();
                packet.bData = new byte[1];
                packet.bData[0] = bCodeError;
                packet.packetServiceData.bAdressSender = bAdrOwn;
                packet.packetServiceData.bAdressReceiver = bAdrOpponent;
                packet.packetServiceData.bCode = MESSAGE_APPROVED_TEXT_CMD;
                packet.packetServiceData.bCounter = COUNTER_CMD++;
                packet.packetServiceData.usLengthInform = Convert.ToByte(packet.bData.Length);
                SendData(packet);
            }
            catch (System.Exception ex)
            {
                logger.Error(ex.Data);
            }

        }


        public void SendAnsweringCmdWithRecivedBearing(int averageBearing, short CKO)
        {
            try
            {
                Cmd packet = new Cmd();
                packet.bData = new byte[6];
                byte[] buffer1 = BitConverter.GetBytes(averageBearing);
                byte[] buffer2 = BitConverter.GetBytes(CKO);
                var buffer1Length = buffer1.Length;
                Array.Resize(ref buffer1, buffer1.Length + buffer2.Length);
                Array.Copy(buffer2, 0, buffer1, buffer1Length, buffer2.Length);
                packet.bData = buffer1;
                packet.packetServiceData.bAdressSender = bAdrOwn;
                packet.packetServiceData.bAdressReceiver = bAdrOpponent;
                packet.packetServiceData.bCode = EXECUTE_DF_CODE;
                packet.packetServiceData.bCounter = COUNTER_CMD++;
                packet.packetServiceData.usLengthInform = Convert.ToByte(packet.bData.Length);
                // send struct to client
                SendData(packet);
            }
            catch (System.Exception ex)
            {
                logger.Error(ex, "   " + this.ToString() + ":  " + ex);
            }
        }

        public void SendConfirmFriequenciesForSuppression(byte bCodeError)
        {
            try
            {
                Cmd packet = new Cmd();
                packet.bData = new byte[1];
                packet.bData[0] = bCodeError;
                packet.packetServiceData.bAdressSender = bAdrOwn;
                packet.packetServiceData.bAdressReceiver = bAdrOpponent;
                packet.packetServiceData.bCode = FRIEQUENCIES_FOR_SUPPRESSION_CODE;
                packet.packetServiceData.bCounter = COUNTER_CMD++;
                packet.packetServiceData.usLengthInform = Convert.ToByte(packet.bData.Length);
                SendData(packet);
            }
            catch (System.Exception ex)
            {
                logger.Error(ex, "   " + this.ToString() + ":  " + ex);
            }
        }


        public void SendAnsweringCmdWithCoord(double latitude, double longitude)
        {
            try
            {
                Cmd packet = new Cmd();
                packet.bData = new byte[16];
                byte[] buffer1 = BitConverter.GetBytes(latitude);
                byte[] buffer2 = BitConverter.GetBytes(longitude);
                var buffer1Length = buffer1.Length;
                Array.Resize(ref buffer1, buffer1.Length + buffer2.Length);
                Array.Copy(buffer2, 0, buffer1, buffer1Length, buffer2.Length);
                packet.bData = buffer1;
                packet.packetServiceData.bAdressSender = bAdrOwn;
                packet.packetServiceData.bAdressReceiver = bAdrOpponent;
                packet.packetServiceData.bCode = COORD_REQUEST_CODE;
                packet.packetServiceData.bCounter = COUNTER_CMD++;
                packet.packetServiceData.usLengthInform = Convert.ToByte(packet.bData.Length);
                // send struct to client
                SendData(packet);
            }
            catch (System.Exception ex)
            {
                logger.Error(ex, "   " + this.ToString() + ":  " + ex);
            }
        }


        // Write data (cmd)
        private void SendData(Cmd packetToSend)
        {
            byte[] bSend;
            // array of service part
            byte[] bHead = Serializer.StructToByteArray(packetToSend.packetServiceData);
            if (packetToSend.bData != null)
            {
                bSend = new byte[bHead.Length + packetToSend.bData.Length];
                Array.Copy(bHead, 0, bSend, 0, bHead.Length);
                Array.Copy(packetToSend.bData, 0, bSend, LEN_HEAD, packetToSend.bData.Length);
            }
            else
                bSend = new byte[bHead.Length];

            if (COUNTER_CMD == 255)
            {
                COUNTER_CMD = 0;
            }

            WriteData(bSend);

        }

        // Write data (byte)
        private bool WriteData(byte[] bData)
        {
            try
            {
                // if there is client
                if (streamClient != null)
                {
                    // write data
                    streamClient.Write(bData, 0, bData.Length);

                    // generate event
                    WriteByte(bData);
                }
            }
            catch (System.Exception ex)
            {
                logger.Error(ex, "   " + this.ToString() + ":  " + ex);
                //GlobalVar._LogFile.LogWrite("   " + this.ToString() + ":  " + ex.TargetSite + "   " + ex.Message);
                return false;
            }

            return true;
        }
    }


    public class Serializer
    {

        // function of serialization of structure to the bytes array
        public static byte[] StructToByteArray(object structure)
        {
            int sizeInBytes = System.Runtime.InteropServices.Marshal.SizeOf(structure.GetType());

            //sizeInBytes = System.Runtime.InteropServices.Marshal.SizeOf(structure);
            byte[] outArray = new byte[sizeInBytes];

            IntPtr ptr = System.Runtime.InteropServices.Marshal.AllocHGlobal(sizeInBytes);
            System.Runtime.InteropServices.Marshal.StructureToPtr(structure, ptr, false);
            System.Runtime.InteropServices.Marshal.Copy(ptr, outArray, 0, sizeInBytes);
            System.Runtime.InteropServices.Marshal.FreeHGlobal(ptr);

            return outArray;
        }

        // function of serialization of bytes array in structure
        public static void ByteArrayToStructure(byte[] bytes, ref object structure)
        {
            int sizeInBytes = System.Runtime.InteropServices.Marshal.SizeOf(structure);
            IntPtr ptr = System.Runtime.InteropServices.Marshal.AllocHGlobal(sizeInBytes);

            System.Runtime.InteropServices.Marshal.Copy(bytes, 0, ptr, sizeInBytes);
            structure = System.Runtime.InteropServices.Marshal.PtrToStructure(ptr, structure.GetType());

            System.Runtime.InteropServices.Marshal.FreeHGlobal(ptr);

        }

        public static string ByteArrayToString(byte[] bytes)
        {
            string hex = BitConverter.ToString(bytes);
            //return hex.Replace("-", "");
            return hex;
        }

        public static byte[] StringToByteArray(String hex)
        {
            int NumberChars = hex.Length;
            byte[] bytes = new byte[NumberChars / 2];
            for (int i = 0; i < NumberChars; i += 2)
                bytes[i / 2] = Convert.ToByte(hex.Substring(i, 2), 16);
            return bytes;
        }
    }
}
