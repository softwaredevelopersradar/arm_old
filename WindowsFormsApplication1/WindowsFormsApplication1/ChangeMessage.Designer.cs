﻿namespace WndProject
{
    partial class ChangeMessage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ChangeMessage));
            this.bSend = new System.Windows.Forms.Button();
            this.tbTextMessage = new System.Windows.Forms.TextBox();
            this.tbcHistoryMessage = new System.Windows.Forms.TabControl();
            this.tbpHistoryJammer = new System.Windows.Forms.TabPage();
            this.rtbHistoryJammer = new System.Windows.Forms.RichTextBox();
            this.tbpHistoryHS = new System.Windows.Forms.TabPage();
            this.rtbHistoryHS = new System.Windows.Forms.RichTextBox();
            this.tbpHistoryPC = new System.Windows.Forms.TabPage();
            this.rtbHistoryPC = new System.Windows.Forms.RichTextBox();
            this.bClearHistory = new System.Windows.Forms.Button();
            this.tbcHistoryMessage.SuspendLayout();
            this.tbpHistoryJammer.SuspendLayout();
            this.tbpHistoryHS.SuspendLayout();
            this.tbpHistoryPC.SuspendLayout();
            this.SuspendLayout();
            // 
            // bSend
            // 
            this.bSend.Location = new System.Drawing.Point(19, 330);
            this.bSend.Name = "bSend";
            this.bSend.Size = new System.Drawing.Size(117, 23);
            this.bSend.TabIndex = 4;
            this.bSend.Text = "Отправить";
            this.bSend.UseVisualStyleBackColor = true;
            this.bSend.Click += new System.EventHandler(this.bSend_Click);
            // 
            // tbTextMessage
            // 
            this.tbTextMessage.BackColor = System.Drawing.Color.White;
            this.tbTextMessage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbTextMessage.Location = new System.Drawing.Point(2, 253);
            this.tbTextMessage.Multiline = true;
            this.tbTextMessage.Name = "tbTextMessage";
            this.tbTextMessage.Size = new System.Drawing.Size(301, 71);
            this.tbTextMessage.TabIndex = 6;
            this.tbTextMessage.Text = "Введите текст";
            this.tbTextMessage.KeyUp += new System.Windows.Forms.KeyEventHandler(this.tbTextMessage_KeyUp);
            // 
            // tbcHistoryMessage
            // 
            this.tbcHistoryMessage.Controls.Add(this.tbpHistoryJammer);
            this.tbcHistoryMessage.Controls.Add(this.tbpHistoryHS);
            this.tbcHistoryMessage.Controls.Add(this.tbpHistoryPC);
            this.tbcHistoryMessage.Location = new System.Drawing.Point(2, 0);
            this.tbcHistoryMessage.Name = "tbcHistoryMessage";
            this.tbcHistoryMessage.SelectedIndex = 0;
            this.tbcHistoryMessage.Size = new System.Drawing.Size(304, 254);
            this.tbcHistoryMessage.TabIndex = 3;
            //// 
            //// tbpHistoryJammer
            //// 
            this.tbpHistoryJammer.BackColor = System.Drawing.Color.Gainsboro;
            this.tbpHistoryJammer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbpHistoryJammer.Controls.Add(this.rtbHistoryJammer);
            this.tbpHistoryJammer.Location = new System.Drawing.Point(4, 22);
            this.tbpHistoryJammer.Name = "tbpHistoryJammer";
            this.tbpHistoryJammer.Padding = new System.Windows.Forms.Padding(3);
            this.tbpHistoryJammer.Size = new System.Drawing.Size(296, 228);
            this.tbpHistoryJammer.TabIndex = 0;
            this.tbpHistoryJammer.Text = "Станция помех";
            //// 
            //// rtbHistoryJammer
            //// 
            this.rtbHistoryJammer.BackColor = System.Drawing.Color.Tan;
            this.rtbHistoryJammer.Location = new System.Drawing.Point(-1, -1);
            this.rtbHistoryJammer.Name = "rtbHistoryJammer";
            this.rtbHistoryJammer.ReadOnly = true;
            this.rtbHistoryJammer.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.rtbHistoryJammer.Size = new System.Drawing.Size(296, 228);
            this.rtbHistoryJammer.TabIndex = 0;
            this.rtbHistoryJammer.Text = "";
            // 
            // tbpHistoryHS
            // 
            this.tbpHistoryHS.Controls.Add(this.rtbHistoryHS);
            this.tbpHistoryHS.Location = new System.Drawing.Point(4, 22);
            this.tbpHistoryHS.Name = "tbpHistoryHS";
            this.tbpHistoryHS.Padding = new System.Windows.Forms.Padding(3);
            this.tbpHistoryHS.Size = new System.Drawing.Size(296, 228);
            this.tbpHistoryHS.TabIndex = 1;
            this.tbpHistoryHS.Text = "Апп станция";
            // 
            // rtbHistoryHS
            // 
            this.rtbHistoryHS.BackColor = System.Drawing.Color.Tan;
            this.rtbHistoryHS.Location = new System.Drawing.Point(-1, 0);
            this.rtbHistoryHS.Name = "rtbHistoryHS";
            this.rtbHistoryHS.ReadOnly = true;
            this.rtbHistoryHS.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.rtbHistoryHS.Size = new System.Drawing.Size(296, 228);
            this.rtbHistoryHS.TabIndex = 1;
            this.rtbHistoryHS.Text = "";
            // 
            // tbpHistoryPC
            // 
            this.tbpHistoryPC.BackColor = System.Drawing.Color.Gainsboro;
            this.tbpHistoryPC.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbpHistoryPC.Controls.Add(this.rtbHistoryPC);
            this.tbpHistoryPC.Location = new System.Drawing.Point(4, 22);
            this.tbpHistoryPC.Name = "tbpHistoryPC";
            this.tbpHistoryPC.Padding = new System.Windows.Forms.Padding(3);
            this.tbpHistoryPC.Size = new System.Drawing.Size(296, 228);
            this.tbpHistoryPC.TabIndex = 2;
            this.tbpHistoryPC.Text = "Пункт управления";
            // 
            // rtbHistoryPC
            // 
            this.rtbHistoryPC.BackColor = System.Drawing.Color.Tan;
            this.rtbHistoryPC.Location = new System.Drawing.Point(-1, -1);
            this.rtbHistoryPC.Name = "rtbHistoryPC";
            this.rtbHistoryPC.ReadOnly = true;
            this.rtbHistoryPC.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.rtbHistoryPC.Size = new System.Drawing.Size(295, 225);
            this.rtbHistoryPC.TabIndex = 2;
            this.rtbHistoryPC.Text = "";
            // 
            // bClearHistory
            // 
            this.bClearHistory.Location = new System.Drawing.Point(161, 330);
            this.bClearHistory.Name = "bClearHistory";
            this.bClearHistory.Size = new System.Drawing.Size(123, 23);
            this.bClearHistory.TabIndex = 5;
            this.bClearHistory.Text = "Очистить историю";
            this.bClearHistory.UseVisualStyleBackColor = true;
            this.bClearHistory.Click += new System.EventHandler(this.bClearHistory_Click);
            // 
            // ChangeMessage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(305, 360);
            this.Controls.Add(this.bSend);
            this.Controls.Add(this.tbTextMessage);
            this.Controls.Add(this.tbcHistoryMessage);
            this.Controls.Add(this.bClearHistory);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ChangeMessage";
            this.Text = "Сообщения";
            this.TopMost = true;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ChangeMessage_FormClosing);
            this.Load += new System.EventHandler(this.ChangeMessage_Load);
            this.tbcHistoryMessage.ResumeLayout(false);
            this.tbpHistoryJammer.ResumeLayout(false);
            this.tbpHistoryHS.ResumeLayout(false);
            this.tbpHistoryPC.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button bSend;
        private System.Windows.Forms.TextBox tbTextMessage;
        private System.Windows.Forms.TabControl tbcHistoryMessage;
        private System.Windows.Forms.TabPage tbpHistoryPC;
        public System.Windows.Forms.RichTextBox rtbHistoryPC;
        private System.Windows.Forms.TabPage tbpHistoryJammer;
        public System.Windows.Forms.RichTextBox rtbHistoryJammer;
        private System.Windows.Forms.Button bClearHistory;
        private System.Windows.Forms.TabPage tbpHistoryHS;
        public System.Windows.Forms.RichTextBox rtbHistoryHS;
    }
}