﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using VariableDynamic;
using VariableStatic;
using System.Drawing;
using System.IO.Ports;
using System.Net;
using FuncVLC;
using System.Runtime.InteropServices;
using Microsoft.VisualBasic;
using USR_DLL;

namespace WndProject
{
    partial class MainWnd
    {
        bool disconnectIsNeeded = false;

        async void butServerAsp_Click(object sender, EventArgs e)
        {
            if (butServerAsp.BackColor != Color.Green)
            {
                InitASP();
            }
            else
            {
                disconnectIsNeeded = false;
                var answer = await VariableWork.aWPtoBearingDSPprotocolNew.DisconnectFromMasterSlaveStation();
                if (answer != null)
                    if (answer.Header.ErrorCode == 0)
                    {
                        butServerAsp.BackColor = Color.Red;
                        VariableWork.aWPtoBearingDSPprotocolNew.MasterSlaveSendTextMessageUpdate -= aWPtoBearingDSPprotocolNew_MasterSlaveSendTextMessageUpdate;
                    }
            }
        }

        private async void InitASP()
        {
            messASP.sText = new List<string>();
            messASP.sTime = new List<string>();

            //VariableWork.aWPtoBearingDSPprotocolNew.MasterSlaveStateChangedUpdate += aWPtoBearingDSPprotocolNew_MasterSlaveStateChangedUpdate;

            var answer = await VariableWork.aWPtoBearingDSPprotocolNew.SetStationRole(variableCommon.Role, Convert.ToByte(variableCommon.OwnAddressPC), Convert.ToByte(variableCommon.JammerLinkedAddressPC));

            if (answer != null)
                if (answer.Header.ErrorCode == 0)
                {
                    var answer1 = await VariableWork.aWPtoBearingDSPprotocolNew.InitMasterSlave(variableConnection.ASPStruct.Address); // СОМ или (IP  Ведущая пишет свой, а ведомая IP ведущей)
                    if (answer1 != null)
                        if (answer1.Header.ErrorCode == 0)
                        {
                            butServerAsp.BackColor = Color.Green;
                            objChangeMessage.Load += objChangeMessage_Load_ForASP;
                            if (objChangeMessage.IsLoaded)
                                VariableWork.aWPtoBearingDSPprotocolNew.MasterSlaveSendTextMessageUpdate += aWPtoBearingDSPprotocolNew_MasterSlaveSendTextMessageUpdate;
                            else
                                VariableWork.aWPtoBearingDSPprotocolNew.MasterSlaveSendTextMessageUpdate += ClientASP_OnReceive_TEXT_MESSAGE;

                            if (variableConnection.ASPType == 3)
                                if (variableCommon.Role == 1)
                                {
                                    //запрос координат от сопряженной станции
                                    var answerLinkedCoords = await VariableWork.aWPtoBearingDSPprotocolNew.GetStationLocation(1);

                                    if (answerLinkedCoords != null)
                                        if (answerLinkedCoords.Header.ErrorCode == 0)
                                        {
                                            //Установка координат Linked
                                            USR_DLL.TCoordsGNSS temp = new USR_DLL.TCoordsGNSS();

                                            temp.Lat = answerLinkedCoords.Latitude;
                                            temp.Lon = answerLinkedCoords.Longitude;

                                            if (answerLinkedCoords.Latitude >= 0)
                                                temp.signLat = 0;
                                            else temp.signLat = 1;

                                            if (answerLinkedCoords.Longitude >= 0)
                                                temp.signLon = 0;
                                            else temp.signLon = 1;

                                            variableWork.CoordsGNSS[1] = temp;

                                            variableCommon.JammerLinkedLatitude = variableWork.CoordsGNSS[1].Lat.ToString();
                                            variableCommon.JammerLinkedLongitude = variableWork.CoordsGNSS[1].Lon.ToString();
                                            variableCommon.JammerLinkedHeight = answerLinkedCoords.Altitude.ToString();

                                            //отправить на карту
                                            SendCurrentCoordsToMap(variableWork.CoordsGNSS);
                                        }
                                }

                            disconnectIsNeeded = true;
                        }
                        else
                        {
                            butServerAsp.BackColor = Color.Red;
                        }

                }
        }

       
        void objChangeMessage_Load_ForASP(object sender, EventArgs e)
        {

        }
        void ClientASP_OnReceive_TEXT_MESSAGE(Protocols.SendTextMessage answer)
        {
            if (answer != null)
            {
                messASP.sTime.Add(indentifyARMStation(answer.Header.ReceiverAddress,answer.Header.SenderAddress));
                messASP.sTime.Add(System.DateTime.Now.ToString("hh:mm:ss"));
                messASP.sText.Add(answer.Text);
            }
        }

        private string typeStation = "Сопр:";
        private string operatorName = "AРМ";
        private string indentifyARMStation(byte receiverAddress, byte senderAddress)
        {
            if (receiverAddress==0 && senderAddress==1)
            {
                if (variableCommon.Operator == 0)
                {
                    return operatorName + " 2:";
                }
                else //if (variableCommon.Operator == 1)
                {
                    return operatorName + " 1:";
                }
            }
            else return typeStation;
        }


        void aWPtoBearingDSPprotocolNew_MasterSlaveSendTextMessageUpdate(Protocols.SendTextMessage answer)
        {
            if (answer != null)
            {
                try
                {
                    if (objChangeMessage.rtbHistoryJammer.InvokeRequired)
                    {
                        objChangeMessage.rtbHistoryJammer.Invoke((MethodInvoker)(delegate()
                        {
                            objChangeMessage.rtbHistoryJammer.SelectionAlignment = HorizontalAlignment.Right;
                            objChangeMessage.showLogJammer.ShowMessage(objChangeMessage.rtbHistoryJammer, 0, "", "", indentifyARMStation(answer.Header.ReceiverAddress, answer.Header.SenderAddress));
                            objChangeMessage.showLogJammer.ShowMessage(objChangeMessage.rtbHistoryJammer, 0, System.DateTime.Now.ToString("hh:mm:ss"), "", " " + answer.Text);
                            objChangeMessage.showLogJammer.ShowMessage(objChangeMessage.rtbHistoryJammer, 0, "", "", ""); //"\n");
                        }));

                    }
                    else
                    {
                        objChangeMessage.rtbHistoryJammer.SelectionAlignment = HorizontalAlignment.Left;
                        objChangeMessage.showLogJammer.ShowMessage(objChangeMessage.rtbHistoryJammer, 1, System.DateTime.Now.ToString("hh:mm:ss"), "", " " + answer.Text);
                        objChangeMessage.showLogJammer.ShowMessage(objChangeMessage.rtbHistoryJammer, 1, "", "", ""); //"\n");
                    }
                }
                catch (SystemException)
                { }
            }
        }

        void aWPtoBearingDSPprotocolNew_MasterSlaveStateChangedUpdate(Protocols.MasterSlaveStateChangedEvent answer)
        {
            //if (answer.IsConnected == true)
            if (variableCommon.Operator == 0)
            {
                if (answer.State == DspDataModel.LinkedStation.ConnectionState.Connected)
                {
                    butClientAsp.BackColor = Color.Green;
                }
                else
                {
                    if (variableCommon.Role == 1)
                    {
                        butClientAsp.BackColor = Color.Red;
                    }
                    if (variableCommon.Role == 2)
                    {
                        if (butServerAsp.BackColor == Color.Green)
                            if (disconnectIsNeeded)
                                Invoke((MethodInvoker)(() => butServerAsp.PerformClick()));
                    }
                }
            }
            if (variableCommon.Operator == 1)
            {
                if (answer.Role == DspDataModel.LinkedStation.StationRole.Master)
                {
                    if (answer.State == DspDataModel.LinkedStation.ConnectionState.Hosted)
                    {
                        butServerAsp.BackColor = Color.Green;
                        butClientAsp.BackColor = Color.Red;
                    }
                    if (answer.State == DspDataModel.LinkedStation.ConnectionState.Connected)
                    {
                        butServerAsp.BackColor = Color.Green;
                        butClientAsp.BackColor = Color.Green;
                    }
                    if (answer.State == DspDataModel.LinkedStation.ConnectionState.NoConnection)
                    {
                        butServerAsp.BackColor = Color.Red;
                        butClientAsp.BackColor = Color.Red;
                    }
                }
                if (answer.Role == DspDataModel.LinkedStation.StationRole.Slave)
                {
                    if (answer.State == DspDataModel.LinkedStation.ConnectionState.Connected)
                    {
                        butServerAsp.BackColor = Color.Green;
                    }
                    if (answer.State == DspDataModel.LinkedStation.ConnectionState.NoConnection)
                    {
                        butServerAsp.BackColor = Color.Red;
                    }
                }
            }
        }

    }
}
