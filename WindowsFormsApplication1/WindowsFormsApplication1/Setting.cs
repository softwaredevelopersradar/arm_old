﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using VariableStatic;

using Protocols;
using VariableDynamic;
using System.Globalization;

namespace WndProject
{
    public partial class Setting : Form
    {
        VariableColor variableColor = new VariableColor();
        VariableCommon variableCommon = new VariableCommon();
        VariableSuppression variableSuppression = new VariableSuppression();
        VariableIntellegence variableIntellegence = new VariableIntellegence();
        VariableConnection variableConnection = new VariableConnection();
        RadioButton[] buttonsPU, buttonHS, buttonASP;
        bool flagStart = true;
        ComponentResourceManager resources = new ComponentResourceManager(typeof(Setting));



        public Setting()
        {
            InitializeComponent();
            USR_DLL.FunctionsUser.ActiveBandIndexSendEvent += FunctionsUser_ActiveBandIndexSendEvent;
            USR_DLL.FunctionsUser FU = new USR_DLL.FunctionsUser();
            FU.RequestActiveBandIndex();
            LanguageChooser();
            VariableCommon.OnChangeCommonLanguage += new VariableStatic.VariableCommon.ChangeCommonEventHandler(LanguageChooser);
        }
       

        private void InitColor(object sender, EventArgs e)
        {
            System.Reflection.PropertyInfo[] properties = variableColor.GetType().GetProperties();
            foreach (Control value in grbColorObject.Controls)
            {

                string name = "";
                Button CurrentButton = value as Button;
                if (CurrentButton != null)
                {
                    name = (value as Button).Name;
                    name = name.Remove(0, 3);
                    foreach (System.Reflection.PropertyInfo fi in properties)
                        if (fi.Name == name)
                        {
                            string ColorStrin = fi.GetValue(variableColor) as String;
                            if (ColorStrin != "")
                            { (value as Button).BackColor = ConvertToColor(ColorStrin); }
                            break;
                        }
                }
            }
            foreach (Control value in grbColorPanorama.Controls)
            {

                string name = "";
                Button CurrentButton = value as Button;
                if (CurrentButton != null)
                {
                    name = (value as Button).Name;
                    name = name.Remove(0, 3);
                    foreach (System.Reflection.PropertyInfo fi in properties)
                        if (fi.Name == name)
                        {
                            string ColorStrin = fi.GetValue(variableColor) as String;
                            if (ColorStrin != "")
                            { (value as Button).BackColor = ConvertToColor(ColorStrin); }
                            break;
                        }
                }
            }
        }
        private static String HexConverter(System.Drawing.Color c)
        {
            String rtn = String.Empty;
            try
            {
                rtn = "#" + c.R.ToString("X2") + c.G.ToString("X2") + c.B.ToString("X2");
            }
            catch (Exception){ }

            return rtn;
        }
        private static System.Drawing.Color ConvertToColor(String s)
        {
            s = s.Remove(0, 1);
            int R;
            int G;
            int B;
            System.Drawing.Color rtn = System.Drawing.Color.Empty;
            try
            {
                R = Convert.ToByte(s.Substring(0, 2),16);
                G = Convert.ToByte(s.Substring(2, 2),16);
                B = Convert.ToByte(s.Substring(4, 2),16);
                rtn = Color.FromArgb(R, G, B);
            }
            catch (Exception)
            {
                //doing nothing
            }

            return rtn;
        }
        private void ChangeColor(object sender, EventArgs e)
        {
            colorDialog.Color = (sender as Button).BackColor;
            if (colorDialog.ShowDialog() == DialogResult.Cancel)
                return;
            (sender as Button).BackColor = colorDialog.Color;
            string ColorString = HexConverter((sender as Button).BackColor);
            string name = (sender as Button).Name;
            name = name.Remove(0, 3);


            System.Reflection.PropertyInfo[] properties = variableColor.GetType().GetProperties();
            foreach (System.Reflection.PropertyInfo fi in properties)
                if (fi.Name == name)
                {
                    fi.SetValue(variableColor, ColorString);
                    break;
                }
        }
        
        void variableEvent()
        {
            VariableIntellegence.OnChangeAutoRelativeBearing += VariableIntellegence_OnChangeAutoRelativeBearing;
            VariableIntellegence.OnChangeRelativeBearing += VariableIntellegence_OnChangeRelativeBearing;
            VariableCommon.OnChangeCommonRole += new VariableCommon.ChangeCommonEventHandler(ChangeRole);
            VariableSuppression.OnChangeRegimeRadioSuppr += VariableSuppression_OnChangeRegimeRadioSuppr;
            VariableSuppression.OnChangeTimeRadiatFWS += VariableSuppression_OnChangeTimeRadiatFWS;
        }


        void VariableSuppression_OnChangeTimeRadiatFWS()
        {
            Invoke((MethodInvoker)(() => TimeRadiatFWSBox.Value = variableSuppression.TimeRadiatFWS));
        }
        void VariableSuppression_OnChangeRegimeRadioSuppr()
        {
            Invoke((MethodInvoker)(() => RegimeRadioSupprBox.SelectedIndex = variableSuppression.RegimeRadioSuppr));
        }


        private void Setting_Load(object sender, EventArgs e)
        {
            SetView();
            variableEvent();
            InitColor(sender, e);
            InitCommon();
            InitSuppression();
            InitIntellegence(); 
            InitConnection();
        }

        void SetView()
        {
            switch (variableCommon.TypeStation)
            {
                case 0: //30Mhz
                case 2: 
                    LablRange3000.Visible = true;
                    LablRange6000.Visible = false;
                    groupBoxPU.Size = new System.Drawing.Size(245, 42);
                    groupBoxASP.Location = new Point(10, 60);
                    groupBoxASP.Size = new System.Drawing.Size(245, 70);
                    if (variableConnection.ASPType == 4)
                        variableConnection.ASPType = 1;
                    ButPU2.Visible = false;
                    ButPU3.Visible = false;
                    ButPU4.Visible = false;
                    variableConnection.PostControlType = 1;
                    groupBoxHS.Visible = false;
                    /*
                        LablRange3000.Text = "1575 - 3000 МГц........";
                        
                        */
                    break;
                case 1: //60MHz

                    LablRange3000.Visible = false;
                    LablRange6000.Visible = true;
                    groupBoxPU.Size = new Size(245, 88);
                    groupBoxASP.Location = new Point(10, 114);
                    groupBoxASP.Size = new System.Drawing.Size(245, 91);
                    groupBoxHS.Visible = true;
                    ButPU1.Visible = false;
                    ButPU2.Visible = true;
                    ButPU3.Visible = true;
                    ButPU4.Visible = true;
                    if (variableConnection.PostControlType == 1)
                        variableConnection.PostControlType = 2;
                    /*
                    LablRange3000.Text = "1575 - 6000 МГц........";
                     * */
                    break;
                
            }
        }

        private void InitConnection()
        {
            buttonsPU = new RadioButton[] { ButPU1, ButPU2, ButPU3, ButPU4 };
            buttonHS = new RadioButton[] { ButHS1, ButHS2 };
            buttonASP = new RadioButton[] { ButASP1, ButASP2, ButASP3, ButASP4 };
            try   
            { 
                buttonsPU[variableConnection.PostControlType - 1].Checked = true;
                buttonHS[variableConnection.HSType - 1].Checked = true;
                buttonASP[variableConnection.ASPType - 1].Checked = true;
            }
            catch { }
        }

        void ChangeRole()
        {
            switch (variableCommon.Role)
            {
                case 0://Автономная                    
                    Invoke((MethodInvoker)(() => BearingBox.Enabled = true));
                    Invoke((MethodInvoker)(() => RegimeRadioSupprBox.Enabled = true));
                    break;
                case 1://Ведущая
                    Invoke((MethodInvoker)(() => BearingBox.Enabled = true));
                    Invoke((MethodInvoker)(() => RegimeRadioSupprBox.Enabled = true));

                    break;
                case 2://Ведомая   
                    Invoke((MethodInvoker)(() => BearingBox.Enabled = false));
                    Invoke((MethodInvoker)(() => RegimeRadioSupprBox.Enabled = false));
                    break;
            }
        }
        
        private void InitCommon()
        {
            //VariableCommon.OnChangeCommonOperator += new VariableCommon.ChangeCommonEventHandler(ChangeOperator);
            OwnCallSignBox.Text = variableCommon.OwnCallSign;
            JammerLinkedCallSignBox.Text = variableCommon.JammerLinkedCallSign;
            PostControlCallSignBox.Text = variableCommon.PostControlCallSign;
            OwnNumberBox.Text = variableCommon.OwnNumber.ToString();
            RoleBox.SelectedIndex = variableCommon.Role;
            OwnAddressPCBox.Value = variableCommon.OwnAddressPC;
            JammerLinkedNumberBox.Value = variableCommon.JammerLinkedNumber;
            JammerLinkedAddressPCBox.Value = variableCommon.JammerLinkedAddressPC;
            LanguageBox.SelectedIndex = variableCommon.Language == 2 ? 1 : 0;
            UseGNSSBox.Checked = Convert.ToBoolean(variableCommon.UseGNSS);
            UseGNSSBox.Visible = variableCommon.VisGNSS;
            labelGNSS.Visible = variableCommon.VisGNSS;
            if (variableCommon.TypeStation == 0 || variableCommon.TypeStation == 2)
            {
                LanguageBox.SelectedIndex = 0;
                LanguageBox.Enabled = false;
            }
           // try { OperatorBox.SelectedIndex = variableCommon.Operator; }
           // catch { OperatorBox.SelectedIndex = 0; }
        }
        private void KeyUpCommon(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                ChangeValueCommon(sender, new EventArgs());
            }
        }
        private void ChangeValueCommon(object sender, EventArgs e)
        {
            string NameCommon;
            Type type = sender.GetType();
            System.Reflection.PropertyInfo[] properties = variableCommon.GetType().GetProperties();
            switch (type.Name)
            {
                case "TextBox":
                    NameCommon = (sender as TextBox).Name;
                    NameCommon = NameCommon.Substring(0, NameCommon.Length - 3);
                    foreach (System.Reflection.PropertyInfo fi in properties)
                        if (fi.Name == NameCommon)
                        {
                            fi.SetValue(variableCommon, (sender as TextBox).Text);
                            break;
                        }
                    break;

                case "MaskedTextBox":
                    NameCommon = (sender as MaskedTextBox).Name;
                    NameCommon = NameCommon.Substring(0, NameCommon.Length - 3);
                    foreach (System.Reflection.PropertyInfo fi in properties)
                        if (fi.Name == NameCommon)
                        {
                            if (Convert.ToInt32((sender as MaskedTextBox).Text) > 32767) { (sender as MaskedTextBox).Text = "32767"; }
                            fi.SetValue(variableCommon, Convert.ToInt16((sender as MaskedTextBox).Text));
                            break;
                        }
                    break;

                case "NumericUpDown":
                    NameCommon = (sender as NumericUpDown).Name;
                    NameCommon = NameCommon.Substring(0, NameCommon.Length - 3);
                    foreach (System.Reflection.PropertyInfo fi in properties)
                        if (fi.Name == NameCommon)
                        {
                            fi.SetValue(variableCommon, Convert.ToInt16((sender as NumericUpDown).Value));
                            break;
                        }
                    break;
                case "ComboBox":
                    NameCommon = (sender as ComboBox).Name;
                    NameCommon = NameCommon.Substring(0, NameCommon.Length - 3);
                    foreach (System.Reflection.PropertyInfo fi in properties)
                        if (fi.Name == NameCommon)
                        {
                            fi.SetValue(variableCommon, Convert.ToByte((sender as ComboBox).SelectedIndex));
                            break;
                        }
                    break;
                case "CheckBox":
                    NameCommon = (sender as CheckBox).Name;
                    NameCommon = NameCommon.Substring(0, NameCommon.Length - 3);
                    foreach (System.Reflection.PropertyInfo fi in properties)
                        if (fi.Name == NameCommon)
                        {
                            fi.SetValue(variableCommon, Convert.ToByte((sender as CheckBox).Checked));
                            break;
                        }
                    break;
            }
        }

        private void InitSuppression()
        { //RegimeRadioSuppr
            try
            {
                if (variableCommon.TypeStation == 0 ) // 7 Liter
                {
                    CountOwnLetterBox.Maximum = 7;
                    CountJammerLinkedLetterBox.Maximum = 7;
                }
                else
                {
                    CountOwnLetterBox.Maximum = 9;
                    CountJammerLinkedLetterBox.Maximum = 9;
                }
                TimeRadiatFWSBox.Value = variableSuppression.TimeRadiatFWS;
                ChannelUseBox.Value = variableSuppression.ChannelUse;
                ThresholdDefaultBox.Value = (-1) * variableSuppression.ThresholdDefault;
                CircleBox.Value = variableSuppression.Circle;
                TimeRadiatFHSSBox.Value = variableSuppression.TimeRadiatFHSS;
                CodeFFTBox.SelectedIndex = variableSuppression.CodeFFT;
                RegimeRadioSupprBox.SelectedIndex = variableSuppression.RegimeRadioSuppr;
                if (variableSuppression.CountOwnLetter > CountOwnLetterBox.Maximum)
                { variableSuppression.CountOwnLetter = (byte)CountOwnLetterBox.Maximum; }
                CountOwnLetterBox.Value = variableSuppression.CountOwnLetter;
                if (variableSuppression.CountOwnLetter > CountOwnLetterBox.Maximum)
                { variableSuppression.CountJammerLinkedLetter = (byte)CountJammerLinkedLetterBox.Maximum; }
                CountJammerLinkedLetterBox.Value = variableSuppression.CountJammerLinkedLetter;
                CountSourceLetterBox.Value = variableSuppression.CountSourceLetter;
                AutoClearTableBox.Checked = Convert.ToBoolean(variableSuppression.AutoClearTable);
                PriorHandleBox.Value = variableSuppression.PriorHandle;
                UPBox.Value = (decimal)(variableSuppression.UP / 10.0d);

                tbPPRCH.Parent = null;
                SearchArcBox.Value = variableSuppression.SearchArc;
                SearchTimeBox.Value = variableSuppression.SearchTime;
                SearchBandBox.Value = variableSuppression.SearchBand;
            }
            catch
            {
                TimeRadiatFWSBox.Value = 800;
                ChannelUseBox.Value = 0x04;
                ThresholdDefaultBox.Value = (-1) * 90;
                CircleBox.Value = 10;
                CountOwnLetterBox.Value = 5;
                CountJammerLinkedLetterBox.Value = 5;
                CountSourceLetterBox.Value = 10;
            }
            //TypeStation = 0; //0 - 3 ГГц; 1 - 6 ГГц
            if (variableCommon.TypeStation == 0) // 7 Liter
            {
                this.tabLetterOwn.Controls.Remove(OwnLetter8Box);
                this.tabLetterOwn.Controls.Remove(OwnLetter9Box);
                this.tabLetterMated.Controls.Remove(JammerLinkedLetter8Box);
                this.tabLetterMated.Controls.Remove(JammerLinkedLetter9Box);
            }
            string NameSuppression;
            System.Reflection.PropertyInfo[] properties = variableSuppression.GetType().GetProperties();
            foreach (Control item in this.tabLetterOwn.Controls.OfType<CheckBox>())
            {
                NameSuppression = (item as CheckBox).Name;
                NameSuppression = NameSuppression.Substring(0, NameSuppression.Length - 3);
                foreach (System.Reflection.PropertyInfo fi in properties)
                    if (fi.Name == NameSuppression)
                    {
                        (item as CheckBox).Checked = Convert.ToBoolean(fi.GetValue(variableSuppression));
                        int CountLetter = 0;
                        foreach (Control Citem in this.tabLetterOwn.Controls.OfType<CheckBox>())
                        {
                            CountLetter += Convert.ToInt16((Citem as CheckBox).Checked);
                        }
                        if (CountLetter > Convert.ToInt16(CountOwnLetterBox.Value))
                        {
                            flagStart = false;
                            (item as CheckBox).Checked = false;
                            flagStart = true;
                        }
                        break;
                    }

            }
            string NameJammerLinkedSuppression;
            foreach (Control item in this.tabLetterMated.Controls.OfType<CheckBox>())
            {
                NameJammerLinkedSuppression = (item as CheckBox).Name;
                NameJammerLinkedSuppression = NameJammerLinkedSuppression.Substring(0, NameJammerLinkedSuppression.Length - 3);
                foreach (System.Reflection.PropertyInfo fi in properties)
                    if (fi.Name == NameJammerLinkedSuppression)
                    {
                        (item as CheckBox).Checked = Convert.ToBoolean(fi.GetValue(variableSuppression));
                        int CountLetter = 0;
                        foreach (Control Citem in this.tabLetterMated.Controls.OfType<CheckBox>())
                        {
                            CountLetter += Convert.ToInt16((Citem as CheckBox).Checked);
                        }
                        if (CountLetter > Convert.ToInt16(CountJammerLinkedLetterBox.Value))
                        {
                            flagStart = false;
                            (item as CheckBox).Checked = false;
                            flagStart = true;
                        }
                        break;
                    }
            }
            flagStart = false;
        }
        private void ChangeValueSuppression(object sender, EventArgs e)
        {
            string NameSuppression;
            Type type = sender.GetType();
            System.Reflection.PropertyInfo[] properties = variableSuppression.GetType().GetProperties();
            switch (type.Name)
            {
                case "TextBox":
                    NameSuppression = (sender as TextBox).Name;
                    NameSuppression = NameSuppression.Substring(0, NameSuppression.Length - 3);
                    foreach (System.Reflection.PropertyInfo fi in properties)
                        if (fi.Name == NameSuppression)
                        {
                            fi.SetValue(variableSuppression, (sender as TextBox).Text);
                            break;
                        }
                    break;

                case "MaskedTextBox":
                    NameSuppression = (sender as MaskedTextBox).Name;
                    NameSuppression = NameSuppression.Substring(0, NameSuppression.Length - 3);
                    foreach (System.Reflection.PropertyInfo fi in properties)
                        if (fi.Name == NameSuppression)
                        {
                            fi.SetValue(variableSuppression, Convert.ToInt16((sender as MaskedTextBox).Text));
                            break;
                        }
                    break;

                case "NumericUpDown":
                    NameSuppression = (sender as NumericUpDown).Name;
                    NameSuppression = NameSuppression.Substring(0, NameSuppression.Length - 3);
                    foreach (System.Reflection.PropertyInfo fi in properties)
                        if (fi.Name == NameSuppression)
                        {
                            Int16 value = Convert.ToInt16((sender as NumericUpDown).Value);
                            if ((sender as NumericUpDown).Name == UPBox.Name)
                            {
                                value = Convert.ToInt16((sender as NumericUpDown).Value * 10);
                            }
                            if (value < 0)
                            { 
                                value = Convert.ToInt16(value * (-1));
                            }
                            try { fi.SetValue(variableSuppression, value); }
                            catch { fi.SetValue(variableSuppression, Convert.ToByte(value)); }
                            break;
                        }
                    break;
                case "ComboBox":
                    NameSuppression = (sender as ComboBox).Name;
                    NameSuppression = NameSuppression.Substring(0, NameSuppression.Length - 3);
                    foreach (System.Reflection.PropertyInfo fi in properties)
                        if (fi.Name == NameSuppression)
                        {
                            fi.SetValue(variableSuppression, Convert.ToByte((sender as ComboBox).SelectedIndex));
                            break;
                        }
                    break;
                case "CheckBox":
                    NameSuppression = (sender as CheckBox).Name;
                    NameSuppression = NameSuppression.Substring(0, NameSuppression.Length - 3);
                    foreach (System.Reflection.PropertyInfo fi in properties)
                        if (fi.Name == NameSuppression)
                        {
                            fi.SetValue(variableSuppression, Convert.ToByte((sender as CheckBox).Checked));
                            break;
                        }
                    break;
            }
        }
        private void KeyUpSuppression(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                ChangeValueSuppression(sender, new EventArgs());
            }
        }
        private void SelectOwnLetter(object sender, EventArgs e)
        {
            if (!flagStart)
            {
                string NameSuppression;
                System.Reflection.PropertyInfo[] properties = variableSuppression.GetType().GetProperties();
                foreach (Control item in this.tabLetterOwn.Controls.OfType<CheckBox>())
                {
                    NameSuppression = (item as CheckBox).Name;
                    NameSuppression = NameSuppression.Substring(0, NameSuppression.Length - 3);
                    foreach (System.Reflection.PropertyInfo fi in properties)
                        if (fi.Name == NameSuppression)
                        {
                            fi.SetValue(variableSuppression, Convert.ToByte((item as CheckBox).Checked));
                            break;
                        }
                }
            }
        }
        private void SelectJammerLinkedLetter(object sender, EventArgs e)
        {
            if (!flagStart)
            {
                string NameJammerLinkedSuppression;
                System.Reflection.PropertyInfo[] properties = variableSuppression.GetType().GetProperties();
                foreach (Control item in this.tabLetterMated.Controls.OfType<CheckBox>())
                {
                    NameJammerLinkedSuppression = (item as CheckBox).Name;
                    NameJammerLinkedSuppression = NameJammerLinkedSuppression.Substring(0, NameJammerLinkedSuppression.Length - 3);
                    foreach (System.Reflection.PropertyInfo fi in properties)
                        if (fi.Name == NameJammerLinkedSuppression)
                        {
                            fi.SetValue(variableSuppression, Convert.ToByte((item as CheckBox).Checked));
                            break;
                        }
                }
            }
        }
        private void JammerLinkedClick(object sender, EventArgs e)
        {
            /*foreach (Control item in this.tabLetterMated.Controls.OfType<CheckBox>())
            {
                if (item != sender)
                {
                    (item as CheckBox).Checked = false;
                }                
            }*/
            int CountLetter = 0;
            foreach (Control item in this.tabLetterMated.Controls.OfType<CheckBox>())
            {
                CountLetter += Convert.ToInt16((item as CheckBox).Checked);
            }
            if (CountLetter > Convert.ToInt16(CountJammerLinkedLetterBox.Value))
            {
                (sender as CheckBox).Checked = false;
            }
        }
        private void OwnLetterClick(object sender, EventArgs e)
        {
            /*foreach (Control item in this.tabLetterOwn.Controls.OfType<CheckBox>())
            {
                if (item != sender)
                {
                    (item as CheckBox).Checked = false;
                }
            }*/

            //TypeStation = 0; //0 - 3 ГГц; 1 - 6 ГГц

            int CountLetter = 0;
            foreach (Control item in this.tabLetterOwn.Controls.OfType<CheckBox>())
            {
                CountLetter += Convert.ToInt16((item as CheckBox).Checked);
            }
            if (CountLetter > Convert.ToInt16(CountOwnLetterBox.Value))
            {
                (sender as CheckBox).Checked = false;
            }
        }

        private void InitIntellegence()
        {
            TimeMonitorBox.Value = variableIntellegence.TimeMonitor;
            BearingBox.Checked = Convert.ToBoolean(variableIntellegence.Bearing);
            AveragingBox.Value = variableIntellegence.Averaging;
            DetectionPPRCHBox.Checked = Convert.ToBoolean(variableIntellegence.DetectionPPRCH);
            AutoRelativeBearingBox.Checked = Convert.ToBoolean(variableIntellegence.AutoRelativeBearing);
            if (variableCommon.Operator == 1)
            {
                AutoRelativeBearingBox.Checked = true;
                AutoRelativeBearingBox.Visible = false;
                Point NewLocationRelativBearing = AutoRelativeBearingBox.Location;
                NewLocationRelativBearing.X = NewLocationRelativBearing.X - 3;
                RelativeBearingLabel.Location = NewLocationRelativBearing;
            }
            RelativeBearingBox.Value = variableIntellegence.RelativeBearing;
            RelativeBearingBox.Enabled = !Convert.ToBoolean(variableIntellegence.AutoRelativeBearing);
        }

        async void VariableIntellegence_OnChangeRelativeBearing()
        {

            if (InvokeRequired)
            {
                BeginInvoke((MethodInvoker)(()
                      => VariableIntellegence_OnChangeRelativeBearing()));
                return;
            }

            RelativeBearingBox.Value = variableIntellegence.RelativeBearing;
            if (variableCommon.Operator == 0)
            {
                var answer = await VariableWork.aWPtoBearingDSPprotocolNew.SetDirectionCorrection(variableIntellegence.RelativeBearing, true);
            }

            /*
            try
            {
                Invoke((MethodInvoker)(async () =>
                {
                    RelativeBearingBox.Value = variableIntellegence.RelativeBearing;
                    if (variableCommon.Operator == 0)
                    {
                        var answer = await VariableWork.aWPtoBearingDSPprotocolNew.SetDirectionCorrection(variableIntellegence.RelativeBearing, true);
                    }
                }));
            }
            catch
            {

            }
             * */
        }

        void VariableIntellegence_OnChangeAutoRelativeBearing()
        {
            if (Convert.ToBoolean(variableIntellegence.AutoRelativeBearing) == true)
                VariableIntellegence.OnChangeRelativeBearing += VariableIntellegence_OnChangeRelativeBearing;
            else
                VariableIntellegence.OnChangeRelativeBearing -= VariableIntellegence_OnChangeRelativeBearing;

            Invoke((MethodInvoker)(() => RelativeBearingBox.Enabled = !Convert.ToBoolean(variableIntellegence.AutoRelativeBearing)));
        }

        private void KeyUpIntellegence(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                ChangeValueIntellegence(sender, new EventArgs());

                if (InvokeRequired)
                {
                    BeginInvoke((MethodInvoker)(()
                          => VariableIntellegence_OnChangeRelativeBearing()));
                    return;
                }
                else
                {
                    VariableIntellegence_OnChangeRelativeBearing();
                }
            }
        }

        private void ChangeValueIntellegence(object sender, EventArgs e)
        {
            string NameSuppression;
            Type type = sender.GetType();
            System.Reflection.PropertyInfo[] properties = variableIntellegence.GetType().GetProperties();

            try
            {
                NameSuppression = (sender as NumericUpDown).Name;
            }
            catch
            {
                NameSuppression = (sender as CheckBox).Name;
            }
            NameSuppression = NameSuppression.Substring(0, NameSuppression.Length - 3);
            foreach (System.Reflection.PropertyInfo fi in properties)
                if (fi.Name == NameSuppression)
                {
                    Int16 value;
                    try
                    {
                       value = Convert.ToInt16((sender as NumericUpDown).Value);
                    }
                    catch
                    {
                        value = Convert.ToInt16((sender as CheckBox).Checked);
                    }
                    try { fi.SetValue(variableIntellegence, value); }
                    catch { fi.SetValue(variableIntellegence, Convert.ToByte(value)); }
                    break;
                }
        }


        private void ButPU_CheckedChanged(object sender, EventArgs e)
        {
            if ((sender as RadioButton).Checked == true)
            {
                variableConnection.PostControlType = Convert.ToByte(Array.IndexOf(buttonsPU, (sender as RadioButton)) + 1);
            }
        }

        private void ButHS_CheckedChanged(object sender, EventArgs e)
        {
            if ((sender as RadioButton).Checked == true)
            {
                variableConnection.HSType = Convert.ToByte(Array.IndexOf(buttonHS, (sender as RadioButton)) + 1);
            }
        }

        private void ButASP_CheckedChanged(object sender, EventArgs e)
        {
            if ((sender as RadioButton).Checked == true)
            {
                variableConnection.ASPType = Convert.ToByte(Array.IndexOf(buttonASP, (sender as RadioButton)) + 1);
            }
        }

        void FunctionsUser_ActiveBandIndexSendEvent(int activeBandIndex)
        {
            if (activeBandIndex != -1)
                SetBandNumber(activeBandIndex);
        }

        private void AttenuatorTank_DoubleClick(object sender, EventArgs e)
        {
            AttenuatorTank.Value = 10.0f;
        }

        private void OnAttenuatorTankValueChanged(object sender, MouseEventArgs e)
        {
            UserEdit = true;
            SetAttenuatorValue((float)AttenuatorTank.Value);
            UserEdit = false;
        }

        public void SetEnabled(bool isEnabled)
        {
            Enabled = isEnabled;
            if (!isEnabled)
            {
                BandNumberLabel.Text = "Полоса";
                AmplifierLabel.Text = "Усилитель:";
                TotalGainLevel.Text = "Всего:";
            }
        }

        public float AmplifierLevel { get; private set; }

        public float AttenuatorLevel { get; private set; }

        public bool IsConstAttenuatorEnabled { get; private set; }

        public int BandNumber { get; private set; }

        bool UserEdit = false;

        public async void SetBandNumber(int bandNumber)
        {
            BandNumber = bandNumber;
            BandNumberLabel.Text = "Полоса " + bandNumber;

            var answer = await VariableWork.aWPtoBearingDSPprotocolNew.GetAmplifierValues(bandNumber);
            if (answer != null)
            {
                int level = answer.Settings[0];
                float flevel = level / 2.0f;
                SetAmplifierLevel(flevel);
            }

            var answer2 = await VariableWork.aWPtoBearingDSPprotocolNew.GetAttenuatorsValues(bandNumber);
            if (answer2 != null)
            {
               int level = answer2.Settings[0].AttenuatorValue;
               float flevel = level / 2.0f;
               bool isEnabled = Convert.ToBoolean(answer2.Settings[0].IsConstAttenuatorEnabled);

               SetAttenuatorValue(flevel);
               SetConstAttenuatorEnabled(isEnabled);
            }

        }

        public void SetAmplifierLevel(float level)
        {
            AmplifierLevel = level;
            AmplifierLabel.Text = "Усилитель: " + AmplifierLevel.ToString("F1") + " дБ";
            UpdateTotalGain();
        }

        public async void SetConstAttenuatorEnabled(bool isEnabled)
        {
            IsConstAttenuatorEnabled = isEnabled;
            if (isEnabled)
                AttenuatorTank.Value = AttenuatorTank.Value + 10;
            UpdateTotalGain();
            await SetAttenuatorRequestIfNeeded();
        }

        public async void SetAttenuatorValue(float level)
        {
            if (level < 10)
            {
                AttenuatorLevel = level;
                IsConstAttenuatorEnabled = false;
                AttenuatorTank.Value = level;
            }
            if (level == 10)
            {
                AttenuatorLevel = 0;
                IsConstAttenuatorEnabled = true;
                AttenuatorTank.Value = level;
            }
            if (level > 10)
            {
                AttenuatorLevel = level - 10;
                IsConstAttenuatorEnabled = true;
                AttenuatorTank.Value = level;
            }
            //AttenuatorTank.Value = AttenuatorLevel;
            UpdateTotalGain();
            await SetAttenuatorRequestIfNeeded();
        }

        private async Task SetAttenuatorRequestIfNeeded()
        {
            if (UserEdit)
            {
                try
                {
                    //await Client.SetAttenuatorsValue(BandNumber, AttenuatorLevel, IsConstAttenuatorEnabled);
                    var answer = await VariableWork.aWPtoBearingDSPprotocolNew.SetAttenuatorsValue(BandNumber, AttenuatorLevel, IsConstAttenuatorEnabled);
                }
                catch (Exception)
                {
                    // ignored
                }
            }
        }

        private void UpdateTotalGain()
        {
            var totalGain = AmplifierLevel - AttenuatorLevel;
            if (IsConstAttenuatorEnabled)
            {
                totalGain -= 10;
            }
            TotalGainLevel.Text = "Всего: " + totalGain.ToString("F1") + " дБ";
        }

        public void Update(int bandNumber, AttenuatorSetting attenuatorSetting, byte amplifier)
        {
            SetEnabled(true);
            SetBandNumber(bandNumber);
            SetAmplifierLevel(amplifier * 0.5f);
            SetAttenuatorValue(attenuatorSetting.AttenuatorValue * 0.5f);
            SetConstAttenuatorEnabled(attenuatorSetting.IsConstAttenuatorEnabled == 1);
        }

        private void NumericUpDownAveraging_ValueChanged(object sender, EventArgs e)
        {
            //Console.WriteLine(NumericUpDownAveraging.Value);
        }

        private void Setting_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            Hide();
        }

        private void LeaveCommon(object sender, EventArgs e)
        {
            ChangeValueCommon(sender, new EventArgs());
        }




        private void LanguageChooser()
        {
            var cont = this.Controls;

            if (variableCommon.Language.Equals(0))
            {
                ChangeLanguageToRu(cont);
                this.Text = "Установки";
            }
            if (variableCommon.Language.Equals(1))
            {
                ChangeLanguageToEng(cont);
                this.Text = "Settings";
            }
            if (variableCommon.Language.Equals(2))
            {

                ChangeLanguageToAzer(cont);
                this.Text = "Quraşdırma";

            }
        }
        private void ChangeLanguageToRu(System.Windows.Forms.Control.ControlCollection cont)
        {
            string lang = "ru-RU";
            foreach (System.Windows.Forms.Control cc in cont)
            {
                if (cc is ComboBox)
                {

                    for (int i = 0; i < (cc as ComboBox).Items.Count; i++)
                    {
                        if (i == 0)
                        {
                            string a = resources.GetString(cc.Name + ".Items", new CultureInfo(lang));
                            (cc as ComboBox).Items[i] = a;
                        }
                        else
                        {
                            string a = resources.GetString(cc.Name + ".Items" + i.ToString(), new CultureInfo(lang));
                            (cc as ComboBox).Items[i] = a;
                        }
                    }
                }
                //}
                if (cc is Label || cc is Button || cc is RadioButton || cc is CheckBox || cc is TabPage || cc is TextBox || cc is GroupBox || cc is NumericUpDown)
                {
                    resources.ApplyResources(cc, cc.Name, new CultureInfo(lang));
                    //toolTip.SetToolTip(cc, resources.GetString(cc.Name + ".ToolTip", new CultureInfo(lang)));
                }
                if (cc is Panel || cc is GroupBox || cc is ListBox || cc is TabControl || cc is DataGridView || cc is Splitter)
                {
                    ChangeLanguageToRu(cc.Controls);
                }
            }
        }

        private void ChangeLanguageToAzer(System.Windows.Forms.Control.ControlCollection cont)
        {
            string lang = "az-Latn";

            foreach (System.Windows.Forms.Control cc in cont)
            {
                if (cc is ComboBox)
                {

                    for (int i = 0; i < (cc as ComboBox).Items.Count; i++)
                    {
                        if (i == 0)
                        {
                            string a = resources.GetString(cc.Name + ".Items", new CultureInfo(lang));
                            (cc as ComboBox).Items[i] = a;
                        }
                        else
                        {
                            string a = resources.GetString(cc.Name + ".Items" + i.ToString(), new CultureInfo(lang));
                            (cc as ComboBox).Items[i] = a;
                        }
                    }
                }
                //}
                if (cc is Label || cc is Button || cc is RadioButton || cc is CheckBox || cc is TabPage || cc is TextBox || cc is GroupBox || cc is NumericUpDown)
                {
                    resources.ApplyResources(cc, cc.Name, new CultureInfo(lang));
                    //toolTip.SetToolTip(cc, resources.GetString(cc.Name + ".ToolTip", new CultureInfo(lang)));
                }
                if (cc is Panel || cc is GroupBox || cc is ListBox || cc is TabControl || cc is DataGridView || cc is Splitter)
                {
                    ChangeLanguageToAzer(cc.Controls);
                }



            }
        }

        private void ChangeLanguageToEng(System.Windows.Forms.Control.ControlCollection cont)
        {


        } 
    }
}
