﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using VariableDynamic;
using VariableStatic;
using System.Drawing;
using System.IO.Ports;
using System.Net;
using FuncVLC;
using CoopProtocolMCS1MCS2;
using USR_DLL;
namespace WndProject
{
    partial class MainWnd
    {

        struct MessCommon
        {
            public List<string> sTime;
            public List<string> sText;
        }
        MessCommon messPU = new MessCommon();
        MessCommon messHS = new MessCommon();
        MessCommon messASP = new MessCommon();


        static public TCPServer ServerHS;
        
        void initServerHS()
        {
            messHS.sText = new List<string>();
            messHS.sTime = new List<string>();
            ServerHS = new TCPServer(1, 2);
            stateIndicateReadHS = new StateIndicate(pbReadHS, imgReadByte, imgWaitByte, TIME_INDICATE);
            stateIndicateWriteHS = new StateIndicate(pbWriteHS, imgWriteByte, imgWaitByte, TIME_INDICATE);
            ServerHS.OnCreate += new TCPServer.ConnectEventHandler(CreateDerverHS);
            ServerHS.OnReadByte += new TCPServer.ByteEventHandler(ReadByteHS);
            ServerHS.OnWriteByte += new TCPServer.ByteEventHandler(WriteByteHS);
            ServerHS.OnConnect += new TCPServer.ConnectEventHandler(ConnectClientHS);
            ServerHS.OnDisconnect += new TCPServer.ConnectEventHandler(DisconnectClientHS);
            ServerHS.OnDestroy += new TCPServer.ConnectEventHandler(DestroyServerHS);
            if (objChangeMessage.IsLoaded)
                ServerHS.OnReceiveTextCmd += new CoopProtocolMCS1MCS2.TCPServer.CmdTextEventHandler(ReceiveTextCmdHS);
            else
                ServerHS.OnReceiveTextCmd += ServerHS_OnReceiveTextCmd;
            ServerHS.OnReceiveFriequenciesForSuppressionCmd += ServerHS_OnReceiveFriequenciesForSuppressionCmd;
            ServerHS.OnReceiveExecuteDFCmd += ServerHS_OnReceiveExecuteDFCmd;
            ServerHS.OnReceiveCoordRequesCmd += ServerHS_OnReceiveCoordRequesCmd;
            ServerHS.CreateServer(variableConnection.HSStruct.Addres, variableConnection.HSStruct.Port);            
            stateIndicateReadHS.SetIndicateOn();
            stateIndicateWriteHS.SetIndicateOn();
        }

        void ServerHS_OnReceiveCoordRequesCmd()
        {
            //широта 40,1917 долгота 49,0937
            ServerHS.SendAnsweringCmdWithCoord(variableWork.CoordsGNSS[0].Lat, variableWork.CoordsGNSS[0].Lon);
        }

        async void ServerHS_OnReceiveExecuteDFCmd(int fr1, int fr2)
        {
            var answer = await VariableDynamic.VariableWork.aWPtoBearingDSPprotocolNew.ExecutiveDfRequest(fr1, fr2, 3);
            if (answer != null)
                if (answer.Header.ErrorCode == 0)
                    ServerHS.SendAnsweringCmdWithRecivedBearing((int)Math.Round(answer.Direction / 10.0d), (short)Math.Round(answer.StandardDeviation / 10.0d));
                else
                    ServerHS.SendAnsweringCmdWithRecivedBearing(361, (short)Math.Round(answer.StandardDeviation / 10.0d));

            else
                ServerHS.SendAnsweringCmdWithRecivedBearing(361, 361);
        }

        byte LetterFindHS(int iFreq)
        {
            byte bLetter = 10;
            if (iFreq >= RangesLetters.FREQ_START_LETTER_1 & iFreq < RangesLetters.FREQ_START_LETTER_2)
                bLetter = 1;
            if (iFreq >= RangesLetters.FREQ_START_LETTER_2 & iFreq < RangesLetters.FREQ_START_LETTER_3)
                bLetter = 2;
            if (iFreq >= RangesLetters.FREQ_START_LETTER_3 & iFreq < RangesLetters.FREQ_START_LETTER_4)
                bLetter = 3;
            if (iFreq >= RangesLetters.FREQ_START_LETTER_4 & iFreq < RangesLetters.FREQ_START_LETTER_5)
                bLetter = 4;
            if (iFreq >= RangesLetters.FREQ_START_LETTER_5 & iFreq < RangesLetters.FREQ_START_LETTER_6)
                bLetter = 5;
            if (iFreq >= RangesLetters.FREQ_START_LETTER_6 & iFreq < RangesLetters.FREQ_START_LETTER_7)
                bLetter = 6;
            if (iFreq >= RangesLetters.FREQ_START_LETTER_7 & iFreq < RangesLetters.FREQ_START_LETTER_8)
                bLetter = 7;
            return bLetter;
        }
        void ServerHS_OnReceiveFriequenciesForSuppressionCmd(int iDuration, StructSupprFWS[] tSupprFWS)
        {
            try
            {
                Invoke((MethodInvoker)(() => (new VariableSuppression()).TimeRadiatFWS = (short)iDuration));
                List<TDistribFWS> tempMass = variableWork.DistribFWS.ToList();// new List<TDistribFWS>();
                //TDistribFWS temp = new TDistribFWS();
                for (int i = 0; i < tSupprFWS.Length; i++)
                {
                    USR_DLL.TDistribFWS temp = USR_DLL.TDistribFWS.CreateID();
                    //temp.bDeviation = tSupprFWS[i].bDeviation;
                    temp.iFreq = tSupprFWS[i].iFreq;
                    // temp.bModulation = tSupprFWS[i].bModulation;
                    //temp.bManipulation = tSupprFWS[i].bManipulation;
                    //temp.bPrior = tSupprFWS[i].bPrioritet;
                    //temp.bDuration = 1;
                    temp.sBearing1 = (short)tSupprFWS[i].sBearing;
                    temp.sBearing2 = -1;
                    temp.sLevel = (short)(tSupprFWS[i].bThreshold * (-1));
                    temp.bView = tSupprFWS[i].bModulation; // "Чм";
                    temp.iCKO = 0;
                    temp.iDFreq = 0;
                    temp.dLatitude = -1;
                    temp.dLongitude = -1;
                    //temp.bLetter =LetterFindHS( tSupprFWS[i].iFreq);
                    tempMass.Add(temp);
                }

                Invoke((MethodInvoker)(() => variableWork.DistribFWS = tempMass.ToArray()));
            }

            catch (SystemException)
            { }
            ServerHS.SendConfirmFriequenciesForSuppression(0);

            
        }

        void ServerHS_OnReceiveTextCmd(string str)
        {
            if (str != null)
            {
                messHS.sTime.Add(System.DateTime.Now.ToString("hh:mm:ss"));
                messHS.sText.Add(str);
            }
        }

        void objChangeMessage_Load(object sender, EventArgs e)
        {
            ///////////HS/////////////////////////////////////////
            if (ServerHS != null)
            {
                ServerHS.OnReceiveTextCmd -= ServerHS_OnReceiveTextCmd;
                objChangeMessage.Load -= objChangeMessage_Load;
                if (messHS.sText != null)
                {
                    for (int i = 0; i < messHS.sTime.Count; i++)
                    {
                        if (objChangeMessage.rtbHistoryHS.InvokeRequired)
                        {
                            objChangeMessage.rtbHistoryHS.Invoke((MethodInvoker)(delegate()
                            {
                                objChangeMessage.rtbHistoryHS.SelectionAlignment = HorizontalAlignment.Right;
                                objChangeMessage.showLogJammer.ShowMessage(objChangeMessage.rtbHistoryHS, 0, messHS.sTime[i], "", " " + messHS.sText[i]);
                                objChangeMessage.showLogJammer.ShowMessage(objChangeMessage.rtbHistoryHS, 0, "", "", ""); //"\n");
                            }));

                        }
                        else
                        {
                            objChangeMessage.rtbHistoryHS.SelectionAlignment = HorizontalAlignment.Right;
                            objChangeMessage.showLogJammer.ShowMessage(objChangeMessage.rtbHistoryHS, 0, messHS.sTime[i], "", " " + messHS.sText[i]);
                            objChangeMessage.showLogJammer.ShowMessage(objChangeMessage.rtbHistoryHS, 0, "", "", ""); //"\n");
                        }

                    }
                }
                ServerHS.OnReceiveTextCmd += new CoopProtocolMCS1MCS2.TCPServer.CmdTextEventHandler(ReceiveTextCmdHS);
            }
            ///////////////////////////////////////////////////////

            ///////////PU//////////////////////////////////////////            
            if (MainWnd.ClientPu != null)
            {
                MainWnd.ClientPu.OnReceiveTextCmd -= ClientPu_OnReceiveTextCmd;
                if (MainWnd.ClientPu != null)
                {
                    if (messPU.sText != null)
                    {
                        for (int i = 0; i < messPU.sTime.Count; i++)
                        {
                            if (objChangeMessage.rtbHistoryPC.InvokeRequired)
                            {
                                objChangeMessage.rtbHistoryPC.Invoke((MethodInvoker)(delegate()
                                {
                                    objChangeMessage.rtbHistoryPC.SelectionAlignment = HorizontalAlignment.Right;
                                    objChangeMessage.showLogJammer.ShowMessage(objChangeMessage.rtbHistoryPC, 0, messPU.sTime[i], "", " " + messPU.sText[i]);
                                    objChangeMessage.showLogJammer.ShowMessage(objChangeMessage.rtbHistoryPC, 0, "", "", ""); //"\n");
                                }));

                            }
                            else
                            {
                                objChangeMessage.rtbHistoryPC.SelectionAlignment = HorizontalAlignment.Right;
                                objChangeMessage.showLogJammer.ShowMessage(objChangeMessage.rtbHistoryPC, 0, messPU.sTime[i], "", " " + messPU.sText[i]);
                                objChangeMessage.showLogJammer.ShowMessage(objChangeMessage.rtbHistoryPC, 0, "", "", ""); //"\n");
                            }

                        }
                    }
                }
                MainWnd.ClientPu.OnReceiveTextCmd += new GrozaBerezinaDLL.IExGRZ_BRZ.CmdTextEventHandler(Receive_TextCmd);
            }
            if (MainWnd.ClientPu_Bel != null)
            {
                MainWnd.ClientPu_Bel.OnReceive_TEXT_MESSAGE += Receive_TextCmd;
                if (messPU.sText != null)
                {
                    for (int i = 0; i < messPU.sTime.Count; i++)
                    {
                        if (objChangeMessage.rtbHistoryPC.InvokeRequired)
                        {
                            objChangeMessage.rtbHistoryPC.Invoke((MethodInvoker)(delegate()
                            {
                                objChangeMessage.rtbHistoryPC.SelectionAlignment = HorizontalAlignment.Right;
                                objChangeMessage.showLogJammer.ShowMessage(objChangeMessage.rtbHistoryPC, 0, messPU.sTime[i], "", " " + messPU.sText[i]);
                                objChangeMessage.showLogJammer.ShowMessage(objChangeMessage.rtbHistoryPC, 0, "", "", ""); //"\n");
                            }));

                        }
                        else
                        {
                            objChangeMessage.rtbHistoryPC.SelectionAlignment = HorizontalAlignment.Right;
                            objChangeMessage.showLogJammer.ShowMessage(objChangeMessage.rtbHistoryPC, 0, messPU.sTime[i], "", " " + messPU.sText[i]);
                            objChangeMessage.showLogJammer.ShowMessage(objChangeMessage.rtbHistoryPC, 0, "", "", ""); //"\n");
                        }

                    }
                }
                MainWnd.ClientPu_Bel.OnReceive_TEXT_MESSAGE += ClientPu_OnReceive_TEXT_MESSAGE;
            }
            //////////////////////////////////////////////////////

            ////////ASP//////////////////////////////////////////
            VariableWork.aWPtoBearingDSPprotocolNew.MasterSlaveSendTextMessageUpdate -= ClientASP_OnReceive_TEXT_MESSAGE;
            if (messASP.sText != null)
            {
                for (int i = 0; i < messASP.sTime.Count; i++)
                {
                    if (objChangeMessage.rtbHistoryJammer.InvokeRequired)
                    {
                        objChangeMessage.rtbHistoryJammer.Invoke((MethodInvoker)(delegate()
                        {
                            objChangeMessage.rtbHistoryJammer.SelectionAlignment = HorizontalAlignment.Right;
                            objChangeMessage.showLogJammer.ShowMessage(objChangeMessage.rtbHistoryJammer, 0, messASP.sTime[i], "", " " + messASP.sText[i]);
                            objChangeMessage.showLogJammer.ShowMessage(objChangeMessage.rtbHistoryJammer, 0, "", "", ""); //"\n");
                        }));

                    }
                    else
                    {
                        objChangeMessage.rtbHistoryJammer.SelectionAlignment = HorizontalAlignment.Right;
                        objChangeMessage.showLogJammer.ShowMessage(objChangeMessage.rtbHistoryJammer, 0, messASP.sTime[i], "", " " + messASP.sText[i]);
                        objChangeMessage.showLogJammer.ShowMessage(objChangeMessage.rtbHistoryJammer, 0, "", "", ""); //"\n");
                    }

                }
            }
            VariableWork.aWPtoBearingDSPprotocolNew.MasterSlaveSendTextMessageUpdate += aWPtoBearingDSPprotocolNew_MasterSlaveSendTextMessageUpdate;
            ////////////////////////////////////////////////////
        }
        void CreateDerverHS()
        {
            butServerHS.BackColor = Color.Green; 
        }
        void ConnectClientHS()
        {
            butClientHS.BackColor = Color.Green;
        }
        void DisconnectClientHS()
        {
            butClientHS.BackColor = Color.Red;
        }
        void DestroyServerHS()
        {
            butServerHS.BackColor = Color.Red;
        }
        void ReadByteHS(byte[] mass)
        {
            stateIndicateReadHS.SetIndicateOn();
        }
        void WriteByteHS(byte[] mass)
        {
            stateIndicateWriteHS.SetIndicateOn();
        }
        void ConfirmTextClientHS(byte bCodeError)
        {
            string strName =ChangeMessage.CMD_ConfirmText + "\n"; //+" ( Адр " + bAddressSend.ToString() + ")\n";

            string strDetail = "Код ошибки  " + bCodeError.ToString() + "\n";
            if (objChangeMessage.rtbHistoryHS.InvokeRequired)
            {
                objChangeMessage.rtbHistoryHS.Invoke((MethodInvoker)(delegate()
                {
                    objChangeMessage.rtbHistoryHS.SelectionAlignment = HorizontalAlignment.Right;
                    objChangeMessage.showLogJammer.ShowMessage(objChangeMessage.rtbHistoryHS, 0, System.DateTime.Now.ToString("hh:mm:ss"), strName, strDetail);
                    //objChangeMessage.rtbHistoryHS.SelectionAlignment = HorizontalAlignment.Left;
                }));

            }
            else
            {
                objChangeMessage.rtbHistoryHS.SelectionAlignment = HorizontalAlignment.Right;
                objChangeMessage.showLogJammer.ShowMessage(objChangeMessage.rtbHistoryHS, 0, System.DateTime.Now.ToString("hh:mm:ss"), strName, strDetail);
                //objChangeMessage.rtbHistoryHS.SelectionAlignment = HorizontalAlignment.Left;
            }
        }

        void ReceiveTextCmdHS(string strText)
        {
            if (objChangeMessage.rtbHistoryHS.InvokeRequired)
            {
                objChangeMessage.rtbHistoryHS.Invoke((MethodInvoker)(delegate()
                {
                    objChangeMessage.rtbHistoryHS.SelectionAlignment = HorizontalAlignment.Right;
                    objChangeMessage.showLogJammer.ShowMessage(objChangeMessage.rtbHistoryHS, 0, System.DateTime.Now.ToString("hh:mm:ss"), "", "   " + strText); //+ "\n");
                    objChangeMessage.showLogJammer.ShowMessage(objChangeMessage.rtbHistoryHS, 1, "", "", "");
                   // objChangeMessage.rtbHistoryHS.SelectionAlignment = HorizontalAlignment.Left;
                }));

            }
            else
            {
                objChangeMessage.rtbHistoryHS.SelectionAlignment = HorizontalAlignment.Right;
                objChangeMessage.showLogJammer.ShowMessage(objChangeMessage.rtbHistoryHS, 0, System.DateTime.Now.ToString("hh:mm:ss"), "", "   " + strText); // + "\n");
                objChangeMessage.showLogJammer.ShowMessage(objChangeMessage.rtbHistoryHS, 1, "", "", "");
               // objChangeMessage.rtbHistoryHS.SelectionAlignment = HorizontalAlignment.Left;
            }
            ServerHS.SendConfirmText(0);
          /*  string strName = ChangeMessage.CMD_ConfirmText + "\n";
            string strDetail = "Код ошибки  " + (0).ToString() + "\n";
            if (objChangeMessage.rtbHistoryHS.InvokeRequired)
            {
                objChangeMessage.rtbHistoryHS.Invoke((MethodInvoker)(delegate()
                {
                    objChangeMessage.rtbHistoryHS.SelectionAlignment = HorizontalAlignment.Left;
                    objChangeMessage.showLogJammer.ShowMessage(objChangeMessage.rtbHistoryHS, 1, System.DateTime.Now.ToString("hh:mm:ss"), strName, strDetail);
                }));

            }
            else
            {
                objChangeMessage.rtbHistoryHS.SelectionAlignment = HorizontalAlignment.Left;
                objChangeMessage.showLogJammer.ShowMessage(objChangeMessage.rtbHistoryHS, 1, System.DateTime.Now.ToString("hh:mm:ss"), strName, strDetail);
            }
            */

        }



        private void ButHS_Click(object sender, EventArgs e)
        {
            if (ServerHS != null)
            {
                ServerHS.DestroyServer();
                ServerHS = null;
            }
            else
            {
                initServerHS();
            }
        }

        

    }
}
