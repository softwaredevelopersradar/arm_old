﻿using System;
using System.Drawing;
using VariableDynamic;
using VariableStatic;
namespace WndProject
{
    partial class MainWnd
    {
        private void InitEventControl()
        {
            //this.bRegimeStop.Click += new System.EventHandler(this.bRegimeStop_Click);
            //this.bRegimeRecon.Click += new System.EventHandler(this.bRegimeRecon_Click);
            //this.bRegimeSuppr.Click += new System.EventHandler(this.bRegimeSuppr_Click);
        }

        private async void bRegimeStop_Click(object sender, EventArgs e)
        {
            if (VariableWork.aWPtoBearingDSPprotocolNew != null)
            {
                try
                {
                    if (isEnableNeeded())
                        EnableMainButtons(false);

                    //Установка режима работы с эфира
                    var answer00 = await VariableWork.aWPtoBearingDSPprotocolNew.SetReceiversChannel(0);

                    var answer = await VariableWork.aWPtoBearingDSPprotocolNew.SetMode(0);

                    panoramaControl1.SetSectorsAndRanges(variableWork.RangeSectorReconOwn);

                    if (answer != null)
                        if (answer.Header.ErrorCode == 0)
                        {
                            variableWork.Regime = 0;

                            if (isEnableNeeded())
                                EnableMainButtons(true);
                        }
                }
                catch (SystemException)
                {
                    if (isEnableNeeded())
                        EnableMainButtons(true);
                }

                if (isEnableNeeded())
                    EnableMainButtons(true);

            }

        }

        private async void bRegimeRecon_Click(object sender, EventArgs e)
        {
            if (VariableWork.aWPtoBearingDSPprotocolNew != null)
            {
                try
                {
                    if (isEnableNeeded())
                        EnableMainButtons(false);

                    //Установка режима работы с эфира
                    var answer00 = await VariableWork.aWPtoBearingDSPprotocolNew.SetReceiversChannel(0);

                    //установка курсового угла
                    var answer0 = await VariableWork.aWPtoBearingDSPprotocolNew.SetDirectionCorrection(variableIntellegence.RelativeBearing, true);

                    //установка диапазонов и секторов радиоразведки
                    var answer = await VariableWork.aWPtoBearingDSPprotocolNew.SetSectorsAndRanges(0, 0, variableWork.RangeSectorReconOwn); //Шифр 3

                    panoramaControl1.SetSectorsAndRanges(variableWork.RangeSectorReconOwn);

                    //установка фильтов для обнаружения ИРИ
                    answer = await VariableWork.aWPtoBearingDSPprotocolNew.SetFilters(panoramaControl1.GetThreshold(), 0, 0, 0); //Шифр 18


                    if (Convert.ToBoolean((variableIntellegence.Bearing)) == false)
                        answer = await VariableWork.aWPtoBearingDSPprotocolNew.SetMode(1);
                    if (Convert.ToBoolean((variableIntellegence.Bearing)) == true)
                        answer = await VariableWork.aWPtoBearingDSPprotocolNew.SetMode(2);

                    if (answer != null)
                        if (answer.Header.ErrorCode == 0)
                        {
                            variableWork.Regime = Convert.ToByte(variableIntellegence.Bearing + 1);

                            if (isEnableNeeded())
                                EnableMainButtons(true);
                        }
                }
                catch (SystemException)
                {
                    if (isEnableNeeded())
                        EnableMainButtons(true);
                }
                if (isEnableNeeded())
                    EnableMainButtons(true);
            }

        }

        private async void bRegimeSuppr_Click(object sender, EventArgs e)
        {
            if (VariableWork.aWPtoBearingDSPprotocolNew != null)
            {
                try
                {
                    if (isEnableNeeded())
                        EnableMainButtons(false);

                    //Установка режима работы с эфира
                    var answer00 = await VariableWork.aWPtoBearingDSPprotocolNew.SetReceiversChannel(0);

                    //установка курсового угла
                    var answer0 = await VariableWork.aWPtoBearingDSPprotocolNew.SetDirectionCorrection(variableIntellegence.RelativeBearing, true);

                    //установка диапазонов и секторов радиоподавления
                    var answerSSaR = await VariableWork.aWPtoBearingDSPprotocolNew.SetSectorsAndRanges(1, 0, variableWork.RangeSectorSupprOwn); //Шифр 3
                    //panoramaControl1.SetSectorsAndRanges(variableWork.RangeSectorSupprOwn);

                    switch ((int)variableSuppression.RegimeRadioSuppr + 3)
                    {
                        case 3:

                            //Установка параметров радиоподавлениЯ для ФРЧ, Шифр 32
                            var answerSettingsFrs = await VariableWork.aWPtoBearingDSPprotocolNew.SetFrsRadioJamSettings(variableSuppression.TimeRadiatFWS, variableSuppression.Circle * 1000, variableSuppression.ChannelUse);

                            Protocols.FRSJammingSetting[] fRSJammingSettingsFrs = new Protocols.FRSJammingSetting[variableWork.SupprFWS_Own.Length];
                            for (int i = 0; i < variableWork.SupprFWS_Own.Length; i++)
                            {
                                fRSJammingSettingsFrs[i].DeviationCode = variableWork.SupprFWS_Own[i].bDeviation;
                                fRSJammingSettingsFrs[i].DurationCode = variableWork.SupprFWS_Own[i].bDuration;
                                fRSJammingSettingsFrs[i].Frequency = variableWork.SupprFWS_Own[i].iFreq; // ?
                                fRSJammingSettingsFrs[i].Id = variableWork.SupprFWS_Own[i].iID;
                                fRSJammingSettingsFrs[i].Liter = variableWork.SupprFWS_Own[i].bLetter;
                                fRSJammingSettingsFrs[i].ManipulationCode = variableWork.SupprFWS_Own[i].bManipulation;
                                fRSJammingSettingsFrs[i].ModulationCode = variableWork.SupprFWS_Own[i].bModulation;
                                fRSJammingSettingsFrs[i].Priority = variableWork.SupprFWS_Own[i].bPrior;
                                fRSJammingSettingsFrs[i].Threshold = Convert.ToByte((-1) * variableWork.SupprFWS_Own[i].sLevel);
                                fRSJammingSettingsFrs[i].Direction = variableWork.SupprFWS_Own[i].sBearing;
                            }

                            //Квадратики радиоподавления
                            panoramaControl1.SetSectorsRS(fRSJammingSettingsFrs);
                            //Установка ИРИ ФРЧ для РП, Шифр 6
                            //1.2.11
                            var answerFreqFrs = await VariableWork.aWPtoBearingDSPprotocolNew.SetFRSJamming((byte)0,fRSJammingSettingsFrs);
                            break;

                        case 4:

                            //Установка параметров радиоподавлениЯ для АПРЧ, Шифр 55
                            var answerSettings = await VariableWork.aWPtoBearingDSPprotocolNew.SetAfrsRadioJamSettings(
                                variableSuppression.TimeRadiatFWS,
                                variableSuppression.Circle * 1000,
                                variableSuppression.ChannelUse,
                                variableSuppression.SearchBand * 10000,
                                (short)(variableSuppression.SearchArc * 10),
                                variableSuppression.ThresholdDefault);

                            Protocols.FRSJammingSetting[] fRSJammingSettingsAfrs = new Protocols.FRSJammingSetting[variableWork.SupprFWS_Own.Length];
                            for (int i = 0; i < variableWork.SupprFWS_Own.Length; i++)
                            {
                                fRSJammingSettingsAfrs[i].DeviationCode = variableWork.SupprFWS_Own[i].bDeviation;
                                fRSJammingSettingsAfrs[i].DurationCode = variableWork.SupprFWS_Own[i].bDuration;
                                fRSJammingSettingsAfrs[i].Frequency = variableWork.SupprFWS_Own[i].iFreq; // ?
                                fRSJammingSettingsAfrs[i].Id = variableWork.SupprFWS_Own[i].iID;
                                fRSJammingSettingsAfrs[i].Liter = variableWork.SupprFWS_Own[i].bLetter;
                                fRSJammingSettingsAfrs[i].ManipulationCode = variableWork.SupprFWS_Own[i].bManipulation;
                                fRSJammingSettingsAfrs[i].ModulationCode = variableWork.SupprFWS_Own[i].bModulation;
                                fRSJammingSettingsAfrs[i].Priority = variableWork.SupprFWS_Own[i].bPrior;
                                fRSJammingSettingsAfrs[i].Threshold = Convert.ToByte((-1) * variableWork.SupprFWS_Own[i].sLevel);
                                fRSJammingSettingsAfrs[i].Direction = variableWork.SupprFWS_Own[i].sBearing;
                            }

                            //Квадратики радиоподавления
                            panoramaControl1.SetSectorsRS(fRSJammingSettingsAfrs);
                            //Установка ИРИ ФРЧ для РП, Шифр 6
                            //1.2.11
                            var answerFreqAfrs = await VariableWork.aWPtoBearingDSPprotocolNew.SetFRSJamming((byte)0,fRSJammingSettingsAfrs);
                            break;

                        case 5:

                            //Установка параметров радиоподавлениЯ для ФРЧ авто, Шифр 54
                            var answerSettingsFrsAuto = await VariableWork.aWPtoBearingDSPprotocolNew.SetFrsAutoRadioJamSettings(
                                variableSuppression.TimeRadiatFWS,
                                variableSuppression.Circle * 1000,
                                variableSuppression.ChannelUse,
                                variableSuppression.UP * 10,
                                variableSuppression.ThresholdDefault);

                            Protocols.FRSJammingSetting[] fRSJammingSettingsFrsAuto = new Protocols.FRSJammingSetting[variableWork.SupprFWS_Own.Length];
                            for (int i = 0; i < variableWork.SupprFWS_Own.Length; i++)
                            {
                                fRSJammingSettingsFrsAuto[i].DeviationCode = variableWork.SupprFWS_Own[i].bDeviation;
                                fRSJammingSettingsFrsAuto[i].DurationCode = variableWork.SupprFWS_Own[i].bDuration;
                                fRSJammingSettingsFrsAuto[i].Frequency = variableWork.SupprFWS_Own[i].iFreq; // ?
                                fRSJammingSettingsFrsAuto[i].Id = variableWork.SupprFWS_Own[i].iID;
                                fRSJammingSettingsFrsAuto[i].Liter = variableWork.SupprFWS_Own[i].bLetter;
                                fRSJammingSettingsFrsAuto[i].ManipulationCode = variableWork.SupprFWS_Own[i].bManipulation;
                                fRSJammingSettingsFrsAuto[i].ModulationCode = variableWork.SupprFWS_Own[i].bModulation;
                                fRSJammingSettingsFrsAuto[i].Priority = variableWork.SupprFWS_Own[i].bPrior;
                                fRSJammingSettingsFrsAuto[i].Threshold = Convert.ToByte((-1) * variableWork.SupprFWS_Own[i].sLevel);
                                fRSJammingSettingsFrsAuto[i].Direction = variableWork.SupprFWS_Own[i].sBearing;
                            }

                            //Квадратики радиоподавления
                            panoramaControl1.SetSectorsRS(fRSJammingSettingsFrsAuto);
                            //Установка ИРИ ФРЧ для РП, Шифр 6
                            //1.2.11
                            var answerFreqFrsAuto = await VariableWork.aWPtoBearingDSPprotocolNew.SetFRSJamming((byte)0,fRSJammingSettingsFrsAuto);
                            break;



                        case 6:

                            int Duration = 100;
                            byte bCodeFFT = 4;
                            int N = variableWork.DistribFHSS_RPOwn.Length;

                            Protocols.FhssJammingSetting[] fhssJammingSettings = new Protocols.FhssJammingSetting[N];
                            for (int i = 0; i < N; i++)
                            {
                                fhssJammingSettings[i].DeviationCode = variableWork.DistribFHSS_RPOwn[i].bDeviation;
                                fhssJammingSettings[i].EndFrequency = variableWork.DistribFHSS_RPOwn[i].iFreqMax;
                                fhssJammingSettings[i].Id = variableWork.DistribFHSS_RPOwn[i].iID;
                                fhssJammingSettings[i].ManipulationCode = variableWork.DistribFHSS_RPOwn[i].bManipulation;
                                fhssJammingSettings[i].ModulationCode = variableWork.DistribFHSS_RPOwn[i].bModulation;
                                fhssJammingSettings[i].StartFrequency = variableWork.DistribFHSS_RPOwn[i].iFreqMin;
                                fhssJammingSettings[i].Threshold = Convert.ToByte((-1) * variableWork.DistribFHSS_RPOwn[i].sLevel);

                                Duration = variableWork.DistribFHSS_RPOwn[i].iDuration / 10;
                                bCodeFFT = variableWork.DistribFHSS_RPOwn[i].bCodeFFT;

                                int len = variableWork.DistribFHSS_RPExcludeOwn.Length;
                                fhssJammingSettings[i].FixedRadioSourceCount = len;
                                fhssJammingSettings[i].FixedRadioSources = new Protocols.FhssFixedRadioSource[len];
                                for (int j = 0; j < len; j++)
                                {
                                    fhssJammingSettings[i].FixedRadioSources[j].Frequency = variableWork.DistribFHSS_RPExcludeOwn[j].iFreqExclude;
                                    fhssJammingSettings[i].FixedRadioSources[j].Bandwidth = variableWork.DistribFHSS_RPExcludeOwn[j].iWidthExclude;
                                }
                            }
                            //Квадратики радиоподавления
                            panoramaControl1.SetSectorsRS(fhssJammingSettings);
                            panoramaControl1.InitFHSSonRS(fhssJammingSettings);
                            //Установка ИРИ ППРЧ для РП, Шифр 7
                            //1.2.11
                            var answerFhss = await VariableWork.aWPtoBearingDSPprotocolNew.SetFhssJamming(0, Duration, bCodeFFT, fhssJammingSettings);
                            break;
                    }

                    var answer = await VariableWork.aWPtoBearingDSPprotocolNew.SetMode((int)variableSuppression.RegimeRadioSuppr + 3);

                    if (answer != null)
                        if (answer.Header.ErrorCode == 0)
                        {
                            variableWork.Regime = Convert.ToByte(variableSuppression.RegimeRadioSuppr + 3);

                            if (isEnableNeeded())
                                EnableMainButtons(true);
                        }
                }
                catch (SystemException)
                {
                    if (isEnableNeeded())
                        EnableMainButtons(true);
                }
                if (isEnableNeeded())
                    EnableMainButtons(true);

            }

        }

        private void EnableMainButtons(bool flag)
        {
            bRegimeStop.Enabled = flag;
            bRegimeRecon.Enabled = flag;
            bRegimeSuppr.Enabled = flag;
        }

        private bool isEnableNeeded()
        {
            VariableStatic.VariableCommon v = new VariableStatic.VariableCommon();

            if ((v.Operator == 0) && (v.Role ==1) )
            {
                return true;
            }

            return false;
        }


    }
}