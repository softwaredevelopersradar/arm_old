﻿using MyDataGridView;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using VariableStatic;



namespace WndProject
{
    public partial class SectorsRanges : Form
    {
        VariableCommon variableCommon;

        public SectorsRanges()
        {
            InitializeComponent();
           

            variableCommon = new VariableCommon();
           
            tables_Range_RR_RP.LoadTablesRangeRR_RPfromDB();
            tables_Range_RR_RP.ChangeControlLanguage(variableCommon.Language);

            //Событие смены языка
            VariableStatic.VariableCommon.OnChangeCommonLanguage += new VariableStatic.VariableCommon.ChangeCommonEventHandler(VariableCommon_OnChangeCommonLanguage);
        }

        private void SectorsRanges_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            Hide();
        }

        private void SectorsRanges_Load(object sender, EventArgs e)
        {
            ReTranslate();
        }

        private void ReTranslate()
        {
            if (variableCommon.Language.Equals(0))
            {
                this.Text = "Сектора и диапазоны РР и РП";
            }
            if (variableCommon.Language.Equals(1))
            {
                this.Text = "Aircrafts";
            }
            if (variableCommon.Language.Equals(2))
            {
                this.Text = "Radiokəşfiyyat və radiosusdurmanın sektor və dipozonları";
            }
        }

        void VariableCommon_OnChangeCommonLanguage()
        {
            ReTranslate();
        }
        
    }
}
