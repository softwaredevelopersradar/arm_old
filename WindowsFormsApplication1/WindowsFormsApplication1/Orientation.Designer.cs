﻿namespace WndProject
{
    partial class WndProject
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WndProject));
            this.complexGraph = new NationalInstruments.UI.WindowsForms.ComplexGraph();
            this.complexPlotLPA = new NationalInstruments.UI.ComplexPlot();
            this.complexXAxis1 = new NationalInstruments.UI.ComplexXAxis();
            this.complexYAxis1 = new NationalInstruments.UI.ComplexYAxis();
            this.complexPlotPU = new NationalInstruments.UI.ComplexPlot();
            this.complexPlotSP = new NationalInstruments.UI.ComplexPlot();
            this.complexPlotLPA1_3 = new NationalInstruments.UI.ComplexPlot();
            this.complexPlotLPA2_4 = new NationalInstruments.UI.ComplexPlot();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tbK1Azimuth = new System.Windows.Forms.TextBox();
            this.lK1Azimuth = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tbK2Azimuth = new System.Windows.Forms.TextBox();
            this.lK2Azimuth = new System.Windows.Forms.Label();
            this.pSP = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.bConnectSP = new System.Windows.Forms.Button();
            this.label17 = new System.Windows.Forms.Label();
            this.ledWriteSP = new NationalInstruments.UI.WindowsForms.Led();
            this.ledReadSP = new NationalInstruments.UI.WindowsForms.Led();
            this.pGPS = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.lGPS = new System.Windows.Forms.Label();
            this.bConnectGPS = new System.Windows.Forms.Button();
            this.ledWriteGPS = new NationalInstruments.UI.WindowsForms.Led();
            this.ledReadGPS1 = new NationalInstruments.UI.WindowsForms.Led();
            this.pState = new System.Windows.Forms.Panel();
            this.pKmpPA = new System.Windows.Forms.Panel();
            this.ledReadKmp2 = new NationalInstruments.UI.WindowsForms.Led();
            this.panel7 = new System.Windows.Forms.Panel();
            this.bConnectKmp2 = new System.Windows.Forms.Button();
            this.label19 = new System.Windows.Forms.Label();
            this.ledWriteKmp2 = new NationalInstruments.UI.WindowsForms.Led();
            this.pPU = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.bConnectPU = new System.Windows.Forms.Button();
            this.label16 = new System.Windows.Forms.Label();
            this.ledWritePU = new NationalInstruments.UI.WindowsForms.Led();
            this.ledReadPU = new NationalInstruments.UI.WindowsForms.Led();
            this.panel14 = new System.Windows.Forms.Panel();
            this.panel11 = new System.Windows.Forms.Panel();
            this.pKmpRR = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.bConnectKmp1 = new System.Windows.Forms.Button();
            this.label18 = new System.Windows.Forms.Label();
            this.ledWriteKmp1 = new NationalInstruments.UI.WindowsForms.Led();
            this.ledReadKmp1 = new NationalInstruments.UI.WindowsForms.Led();
            this.panel16 = new System.Windows.Forms.Panel();
            this.panel13 = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.panel10 = new System.Windows.Forms.Panel();
            this.pLPA = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.bConnectLPA = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.ledWriteLPA = new NationalInstruments.UI.WindowsForms.Led();
            this.ledReadLPA = new NationalInstruments.UI.WindowsForms.Led();
            this.nudLPA2_4 = new System.Windows.Forms.NumericUpDown();
            this.nudLPA1_3 = new System.Windows.Forms.NumericUpDown();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.tbLPA2 = new System.Windows.Forms.TextBox();
            this.bStopSP2 = new System.Windows.Forms.Button();
            this.lLPA2 = new System.Windows.Forms.Label();
            this.bStopPU = new System.Windows.Forms.Button();
            this.bSetSP2 = new System.Windows.Forms.Button();
            this.bSetPU = new System.Windows.Forms.Button();
            this.nudPU = new System.Windows.Forms.NumericUpDown();
            this.bStopLPA2 = new System.Windows.Forms.Button();
            this.tbPU = new System.Windows.Forms.TextBox();
            this.nudSP2 = new System.Windows.Forms.NumericUpDown();
            this.bSetLPA2 = new System.Windows.Forms.Button();
            this.lPU = new System.Windows.Forms.Label();
            this.lSP2 = new System.Windows.Forms.Label();
            this.nudLPA2 = new System.Windows.Forms.NumericUpDown();
            this.tbSP2 = new System.Windows.Forms.TextBox();
            this.lLPA1_3 = new System.Windows.Forms.Label();
            this.lLPA2_4 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.tbAlt = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.rbWGS84 = new System.Windows.Forms.RadioButton();
            this.rbCK42 = new System.Windows.Forms.RadioButton();
            this.cbCoordinateSystem = new System.Windows.Forms.ComboBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.cbSatSyst = new System.Windows.Forms.ComboBox();
            this.tbDate = new System.Windows.Forms.TextBox();
            this.lDate = new System.Windows.Forms.Label();
            this.tbTime = new System.Windows.Forms.TextBox();
            this.lTime = new System.Windows.Forms.Label();
            this.tbCountSat = new System.Windows.Forms.TextBox();
            this.tbLon = new System.Windows.Forms.TextBox();
            this.tbLat = new System.Windows.Forms.TextBox();
            this.lCountSat = new System.Windows.Forms.Label();
            this.lLon = new System.Windows.Forms.Label();
            this.lLAt = new System.Windows.Forms.Label();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.timerLedReadBytes = new System.Windows.Forms.Timer(this.components);
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.bTransportPU = new System.Windows.Forms.Button();
            this.bTransportLPA = new System.Windows.Forms.Button();
            this.bTransportSP = new System.Windows.Forms.Button();
            this.timerTest = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.complexGraph)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.pSP.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ledWriteSP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ledReadSP)).BeginInit();
            this.pGPS.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ledWriteGPS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ledReadGPS1)).BeginInit();
            this.pState.SuspendLayout();
            this.pKmpPA.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ledReadKmp2)).BeginInit();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ledWriteKmp2)).BeginInit();
            this.pPU.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ledWritePU)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ledReadPU)).BeginInit();
            this.pKmpRR.SuspendLayout();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ledWriteKmp1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ledReadKmp1)).BeginInit();
            this.pLPA.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ledWriteLPA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ledReadLPA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudLPA2_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudLPA1_3)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudPU)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSP2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudLPA2)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.SuspendLayout();
            // 
            // complexGraph
            // 
            this.complexGraph.BackgroundImage = global::WndProject.Properties.Resources.Compass21;
            this.complexGraph.Border = NationalInstruments.UI.Border.None;
            this.complexGraph.Location = new System.Drawing.Point(-1, 0);
            this.complexGraph.Name = "complexGraph";
            this.complexGraph.PlotAreaBorder = NationalInstruments.UI.Border.None;
            this.complexGraph.PlotAreaColor = System.Drawing.Color.Transparent;
            this.complexGraph.PlotAreaImageAlignment = NationalInstruments.UI.ImageAlignment.Center;
            this.complexGraph.Plots.AddRange(new NationalInstruments.UI.ComplexPlot[] {
            this.complexPlotLPA,
            this.complexPlotPU,
            this.complexPlotSP,
            this.complexPlotLPA1_3,
            this.complexPlotLPA2_4});
            this.complexGraph.Size = new System.Drawing.Size(340, 340);
            this.complexGraph.TabIndex = 57;
            this.complexGraph.UseColorGenerator = true;
            this.complexGraph.XAxes.AddRange(new NationalInstruments.UI.ComplexXAxis[] {
            this.complexXAxis1});
            this.complexGraph.YAxes.AddRange(new NationalInstruments.UI.ComplexYAxis[] {
            this.complexYAxis1});
            // 
            // complexPlotLPA
            // 
            this.complexPlotLPA.CanScaleXAxis = false;
            this.complexPlotLPA.CanScaleYAxis = false;
            this.complexPlotLPA.LineColor = System.Drawing.Color.Red;
            this.complexPlotLPA.LineColorPrecedence = NationalInstruments.UI.ColorPrecedence.UserDefinedColor;
            this.complexPlotLPA.LineWidth = 2F;
            this.complexPlotLPA.XAxis = this.complexXAxis1;
            this.complexPlotLPA.YAxis = this.complexYAxis1;
            // 
            // complexXAxis1
            // 
            this.complexXAxis1.Mode = NationalInstruments.UI.AxisMode.Fixed;
            this.complexXAxis1.OriginLineVisible = false;
            this.complexXAxis1.Visible = false;
            // 
            // complexYAxis1
            // 
            this.complexYAxis1.OriginLineVisible = false;
            this.complexYAxis1.Visible = false;
            // 
            // complexPlotPU
            // 
            this.complexPlotPU.CanScaleXAxis = false;
            this.complexPlotPU.CanScaleYAxis = false;
            this.complexPlotPU.LineColor = System.Drawing.Color.Cyan;
            this.complexPlotPU.LineColorPrecedence = NationalInstruments.UI.ColorPrecedence.UserDefinedColor;
            this.complexPlotPU.LineWidth = 2F;
            this.complexPlotPU.XAxis = this.complexXAxis1;
            this.complexPlotPU.YAxis = this.complexYAxis1;
            // 
            // complexPlotSP
            // 
            this.complexPlotSP.CanScaleXAxis = false;
            this.complexPlotSP.CanScaleYAxis = false;
            this.complexPlotSP.LineColor = System.Drawing.Color.Green;
            this.complexPlotSP.LineColorPrecedence = NationalInstruments.UI.ColorPrecedence.UserDefinedColor;
            this.complexPlotSP.LineWidth = 2F;
            this.complexPlotSP.XAxis = this.complexXAxis1;
            this.complexPlotSP.YAxis = this.complexYAxis1;
            // 
            // complexPlotLPA1_3
            // 
            this.complexPlotLPA1_3.CanScaleXAxis = false;
            this.complexPlotLPA1_3.CanScaleYAxis = false;
            this.complexPlotLPA1_3.LineColor = System.Drawing.Color.Blue;
            this.complexPlotLPA1_3.LineColorPrecedence = NationalInstruments.UI.ColorPrecedence.UserDefinedColor;
            this.complexPlotLPA1_3.LineWidth = 2F;
            this.complexPlotLPA1_3.XAxis = this.complexXAxis1;
            this.complexPlotLPA1_3.YAxis = this.complexYAxis1;
            // 
            // complexPlotLPA2_4
            // 
            this.complexPlotLPA2_4.CanScaleXAxis = false;
            this.complexPlotLPA2_4.CanScaleYAxis = false;
            this.complexPlotLPA2_4.LineColor = System.Drawing.Color.Olive;
            this.complexPlotLPA2_4.LineColorPrecedence = NationalInstruments.UI.ColorPrecedence.UserDefinedColor;
            this.complexPlotLPA2_4.LineWidth = 2F;
            this.complexPlotLPA2_4.XAxis = this.complexXAxis1;
            this.complexPlotLPA2_4.YAxis = this.complexYAxis1;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tbK1Azimuth);
            this.groupBox1.Controls.Add(this.lK1Azimuth);
            this.groupBox1.ForeColor = System.Drawing.Color.Blue;
            this.groupBox1.Location = new System.Drawing.Point(347, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(154, 37);
            this.groupBox1.TabIndex = 59;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Компас РР";
            // 
            // tbK1Azimuth
            // 
            this.tbK1Azimuth.Location = new System.Drawing.Point(68, 11);
            this.tbK1Azimuth.Name = "tbK1Azimuth";
            this.tbK1Azimuth.Size = new System.Drawing.Size(80, 20);
            this.tbK1Azimuth.TabIndex = 4;
            this.tbK1Azimuth.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbK1Azimuth_KeyPress);
            this.tbK1Azimuth.Validated += new System.EventHandler(this.tbK1Azimuth_Validated);
            // 
            // lK1Azimuth
            // 
            this.lK1Azimuth.AutoSize = true;
            this.lK1Azimuth.ForeColor = System.Drawing.Color.Black;
            this.lK1Azimuth.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lK1Azimuth.Location = new System.Drawing.Point(7, 18);
            this.lK1Azimuth.Name = "lK1Azimuth";
            this.lK1Azimuth.Size = new System.Drawing.Size(54, 13);
            this.lK1Azimuth.TabIndex = 0;
            this.lK1Azimuth.Text = "Азимут, °";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.tbK2Azimuth);
            this.groupBox2.Controls.Add(this.lK2Azimuth);
            this.groupBox2.ForeColor = System.Drawing.Color.Blue;
            this.groupBox2.Location = new System.Drawing.Point(523, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(155, 37);
            this.groupBox2.TabIndex = 60;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Компас ПА";
            // 
            // tbK2Azimuth
            // 
            this.tbK2Azimuth.Location = new System.Drawing.Point(69, 11);
            this.tbK2Azimuth.Name = "tbK2Azimuth";
            this.tbK2Azimuth.ReadOnly = true;
            this.tbK2Azimuth.Size = new System.Drawing.Size(80, 20);
            this.tbK2Azimuth.TabIndex = 6;
            // 
            // lK2Azimuth
            // 
            this.lK2Azimuth.AutoSize = true;
            this.lK2Azimuth.ForeColor = System.Drawing.Color.Black;
            this.lK2Azimuth.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lK2Azimuth.Location = new System.Drawing.Point(8, 18);
            this.lK2Azimuth.Name = "lK2Azimuth";
            this.lK2Azimuth.Size = new System.Drawing.Size(54, 13);
            this.lK2Azimuth.TabIndex = 6;
            this.lK2Azimuth.Text = "Азимут, °";
            // 
            // pSP
            // 
            this.pSP.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pSP.Controls.Add(this.panel5);
            this.pSP.Controls.Add(this.ledWriteSP);
            this.pSP.Controls.Add(this.ledReadSP);
            this.pSP.Location = new System.Drawing.Point(327, 347);
            this.pSP.Name = "pSP";
            this.pSP.Size = new System.Drawing.Size(110, 35);
            this.pSP.TabIndex = 63;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.bConnectSP);
            this.panel5.Controls.Add(this.label17);
            this.panel5.Location = new System.Drawing.Point(6, 5);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(54, 23);
            this.panel5.TabIndex = 47;
            // 
            // bConnectSP
            // 
            this.bConnectSP.BackColor = System.Drawing.Color.Red;
            this.bConnectSP.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.bConnectSP.Location = new System.Drawing.Point(1, 0);
            this.bConnectSP.Name = "bConnectSP";
            this.bConnectSP.Size = new System.Drawing.Size(23, 23);
            this.bConnectSP.TabIndex = 37;
            this.bConnectSP.UseVisualStyleBackColor = false;
            this.bConnectSP.Click += new System.EventHandler(this.bConnectSP_Click);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.label17.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label17.Location = new System.Drawing.Point(25, 4);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(24, 15);
            this.label17.TabIndex = 84;
            this.label17.Text = "СП";
            // 
            // ledWriteSP
            // 
            this.ledWriteSP.LedStyle = NationalInstruments.UI.LedStyle.Round3D;
            this.ledWriteSP.Location = new System.Drawing.Point(79, 7);
            this.ledWriteSP.Name = "ledWriteSP";
            this.ledWriteSP.OffColor = System.Drawing.Color.DarkGray;
            this.ledWriteSP.OnColor = System.Drawing.Color.Red;
            this.ledWriteSP.Size = new System.Drawing.Size(20, 20);
            this.ledWriteSP.TabIndex = 45;
            // 
            // ledReadSP
            // 
            this.ledReadSP.LedStyle = NationalInstruments.UI.LedStyle.Round3D;
            this.ledReadSP.Location = new System.Drawing.Point(63, 7);
            this.ledReadSP.Name = "ledReadSP";
            this.ledReadSP.OffColor = System.Drawing.Color.DarkGray;
            this.ledReadSP.OnColor = System.Drawing.Color.Green;
            this.ledReadSP.Size = new System.Drawing.Size(20, 20);
            this.ledReadSP.TabIndex = 46;
            // 
            // pGPS
            // 
            this.pGPS.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pGPS.Controls.Add(this.panel3);
            this.pGPS.Controls.Add(this.ledWriteGPS);
            this.pGPS.Controls.Add(this.ledReadGPS1);
            this.pGPS.Location = new System.Drawing.Point(-2, 347);
            this.pGPS.Name = "pGPS";
            this.pGPS.Size = new System.Drawing.Size(110, 35);
            this.pGPS.TabIndex = 58;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.lGPS);
            this.panel3.Controls.Add(this.bConnectGPS);
            this.panel3.Location = new System.Drawing.Point(3, 5);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(66, 23);
            this.panel3.TabIndex = 38;
            // 
            // lGPS
            // 
            this.lGPS.AutoSize = true;
            this.lGPS.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.lGPS.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lGPS.Location = new System.Drawing.Point(25, 4);
            this.lGPS.Name = "lGPS";
            this.lGPS.Size = new System.Drawing.Size(38, 15);
            this.lGPS.TabIndex = 84;
            this.lGPS.Text = "GNSS";
            // 
            // bConnectGPS
            // 
            this.bConnectGPS.BackColor = System.Drawing.Color.Red;
            this.bConnectGPS.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.bConnectGPS.Location = new System.Drawing.Point(1, 0);
            this.bConnectGPS.Name = "bConnectGPS";
            this.bConnectGPS.Size = new System.Drawing.Size(23, 23);
            this.bConnectGPS.TabIndex = 37;
            this.bConnectGPS.UseVisualStyleBackColor = false;
            this.bConnectGPS.Click += new System.EventHandler(this.bConnectGPS_Click);
            // 
            // ledWriteGPS
            // 
            this.ledWriteGPS.LedStyle = NationalInstruments.UI.LedStyle.Round3D;
            this.ledWriteGPS.Location = new System.Drawing.Point(82, 7);
            this.ledWriteGPS.Name = "ledWriteGPS";
            this.ledWriteGPS.OffColor = System.Drawing.Color.DarkGray;
            this.ledWriteGPS.OnColor = System.Drawing.Color.Green;
            this.ledWriteGPS.Size = new System.Drawing.Size(20, 20);
            this.ledWriteGPS.TabIndex = 37;
            this.ledWriteGPS.Visible = false;
            // 
            // ledReadGPS1
            // 
            this.ledReadGPS1.LedStyle = NationalInstruments.UI.LedStyle.Round3D;
            this.ledReadGPS1.Location = new System.Drawing.Point(68, 7);
            this.ledReadGPS1.Name = "ledReadGPS1";
            this.ledReadGPS1.OffColor = System.Drawing.Color.DarkGray;
            this.ledReadGPS1.OnColor = System.Drawing.Color.Green;
            this.ledReadGPS1.Size = new System.Drawing.Size(20, 20);
            this.ledReadGPS1.TabIndex = 38;
            // 
            // pState
            // 
            this.pState.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pState.Controls.Add(this.pKmpPA);
            this.pState.Controls.Add(this.pPU);
            this.pState.Controls.Add(this.panel14);
            this.pState.Controls.Add(this.panel11);
            this.pState.Controls.Add(this.pKmpRR);
            this.pState.Controls.Add(this.panel16);
            this.pState.Controls.Add(this.panel13);
            this.pState.Controls.Add(this.panel8);
            this.pState.Controls.Add(this.panel10);
            this.pState.Controls.Add(this.pLPA);
            this.pState.Location = new System.Drawing.Point(-2, 347);
            this.pState.Name = "pState";
            this.pState.Size = new System.Drawing.Size(680, 35);
            this.pState.TabIndex = 68;
            // 
            // pKmpPA
            // 
            this.pKmpPA.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pKmpPA.Controls.Add(this.ledReadKmp2);
            this.pKmpPA.Controls.Add(this.panel7);
            this.pKmpPA.Controls.Add(this.ledWriteKmp2);
            this.pKmpPA.Location = new System.Drawing.Point(553, -2);
            this.pKmpPA.Name = "pKmpPA";
            this.pKmpPA.Size = new System.Drawing.Size(125, 35);
            this.pKmpPA.TabIndex = 45;
            // 
            // ledReadKmp2
            // 
            this.ledReadKmp2.LedStyle = NationalInstruments.UI.LedStyle.Round3D;
            this.ledReadKmp2.Location = new System.Drawing.Point(83, 7);
            this.ledReadKmp2.Name = "ledReadKmp2";
            this.ledReadKmp2.OffColor = System.Drawing.Color.DarkGray;
            this.ledReadKmp2.OnColor = System.Drawing.Color.Green;
            this.ledReadKmp2.Size = new System.Drawing.Size(20, 20);
            this.ledReadKmp2.TabIndex = 52;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.bConnectKmp2);
            this.panel7.Controls.Add(this.label19);
            this.panel7.Location = new System.Drawing.Point(4, 4);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(76, 23);
            this.panel7.TabIndex = 53;
            // 
            // bConnectKmp2
            // 
            this.bConnectKmp2.BackColor = System.Drawing.Color.Red;
            this.bConnectKmp2.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.bConnectKmp2.Location = new System.Drawing.Point(1, 0);
            this.bConnectKmp2.Name = "bConnectKmp2";
            this.bConnectKmp2.Size = new System.Drawing.Size(23, 23);
            this.bConnectKmp2.TabIndex = 37;
            this.bConnectKmp2.UseVisualStyleBackColor = false;
            this.bConnectKmp2.Click += new System.EventHandler(this.bConnectKmp2_Click);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.label19.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label19.Location = new System.Drawing.Point(24, 5);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(50, 15);
            this.label19.TabIndex = 84;
            this.label19.Text = "КмпПА";
            // 
            // ledWriteKmp2
            // 
            this.ledWriteKmp2.LedStyle = NationalInstruments.UI.LedStyle.Round3D;
            this.ledWriteKmp2.Location = new System.Drawing.Point(98, 7);
            this.ledWriteKmp2.Name = "ledWriteKmp2";
            this.ledWriteKmp2.OffColor = System.Drawing.Color.DarkGray;
            this.ledWriteKmp2.OnColor = System.Drawing.Color.Green;
            this.ledWriteKmp2.Size = new System.Drawing.Size(20, 20);
            this.ledWriteKmp2.TabIndex = 51;
            this.ledWriteKmp2.Visible = false;
            // 
            // pPU
            // 
            this.pPU.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pPU.Controls.Add(this.panel4);
            this.pPU.Controls.Add(this.ledWritePU);
            this.pPU.Controls.Add(this.ledReadPU);
            this.pPU.Location = new System.Drawing.Point(218, -3);
            this.pPU.Name = "pPU";
            this.pPU.Size = new System.Drawing.Size(110, 35);
            this.pPU.TabIndex = 2;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.bConnectPU);
            this.panel4.Controls.Add(this.label16);
            this.panel4.Location = new System.Drawing.Point(6, 6);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(53, 23);
            this.panel4.TabIndex = 44;
            // 
            // bConnectPU
            // 
            this.bConnectPU.BackColor = System.Drawing.Color.Red;
            this.bConnectPU.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.bConnectPU.Location = new System.Drawing.Point(1, 0);
            this.bConnectPU.Name = "bConnectPU";
            this.bConnectPU.Size = new System.Drawing.Size(23, 23);
            this.bConnectPU.TabIndex = 37;
            this.bConnectPU.UseVisualStyleBackColor = false;
            this.bConnectPU.Click += new System.EventHandler(this.bConnectPU_Click);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.label16.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label16.Location = new System.Drawing.Point(25, 4);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(25, 15);
            this.label16.TabIndex = 84;
            this.label16.Text = "ПУ";
            // 
            // ledWritePU
            // 
            this.ledWritePU.LedStyle = NationalInstruments.UI.LedStyle.Round3D;
            this.ledWritePU.Location = new System.Drawing.Point(76, 8);
            this.ledWritePU.Name = "ledWritePU";
            this.ledWritePU.OffColor = System.Drawing.Color.DarkGray;
            this.ledWritePU.OnColor = System.Drawing.Color.Red;
            this.ledWritePU.Size = new System.Drawing.Size(20, 20);
            this.ledWritePU.TabIndex = 42;
            // 
            // ledReadPU
            // 
            this.ledReadPU.LedStyle = NationalInstruments.UI.LedStyle.Round3D;
            this.ledReadPU.Location = new System.Drawing.Point(62, 8);
            this.ledReadPU.Name = "ledReadPU";
            this.ledReadPU.OffColor = System.Drawing.Color.DarkGray;
            this.ledReadPU.OnColor = System.Drawing.Color.Green;
            this.ledReadPU.Size = new System.Drawing.Size(20, 20);
            this.ledReadPU.TabIndex = 43;
            // 
            // panel14
            // 
            this.panel14.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel14.Location = new System.Drawing.Point(454, 375);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(110, 35);
            this.panel14.TabIndex = 9;
            // 
            // panel11
            // 
            this.panel11.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel11.Location = new System.Drawing.Point(342, 380);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(110, 35);
            this.panel11.TabIndex = 6;
            // 
            // pKmpRR
            // 
            this.pKmpRR.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pKmpRR.Controls.Add(this.panel6);
            this.pKmpRR.Controls.Add(this.ledWriteKmp1);
            this.pKmpRR.Controls.Add(this.ledReadKmp1);
            this.pKmpRR.Location = new System.Drawing.Point(437, -2);
            this.pKmpRR.Name = "pKmpRR";
            this.pKmpRR.Size = new System.Drawing.Size(118, 35);
            this.pKmpRR.TabIndex = 10;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.bConnectKmp1);
            this.panel6.Controls.Add(this.label18);
            this.panel6.Location = new System.Drawing.Point(3, 5);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(69, 23);
            this.panel6.TabIndex = 50;
            // 
            // bConnectKmp1
            // 
            this.bConnectKmp1.BackColor = System.Drawing.Color.Red;
            this.bConnectKmp1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.bConnectKmp1.Location = new System.Drawing.Point(1, 0);
            this.bConnectKmp1.Name = "bConnectKmp1";
            this.bConnectKmp1.Size = new System.Drawing.Size(23, 23);
            this.bConnectKmp1.TabIndex = 37;
            this.bConnectKmp1.UseVisualStyleBackColor = false;
            this.bConnectKmp1.Click += new System.EventHandler(this.bConnectKmp1_Click);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.label18.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label18.Location = new System.Drawing.Point(23, 4);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(45, 15);
            this.label18.TabIndex = 84;
            this.label18.Text = "КмпРР";
            // 
            // ledWriteKmp1
            // 
            this.ledWriteKmp1.LedStyle = NationalInstruments.UI.LedStyle.Round3D;
            this.ledWriteKmp1.Location = new System.Drawing.Point(90, 8);
            this.ledWriteKmp1.Name = "ledWriteKmp1";
            this.ledWriteKmp1.OffColor = System.Drawing.Color.DarkGray;
            this.ledWriteKmp1.OnColor = System.Drawing.Color.Green;
            this.ledWriteKmp1.Size = new System.Drawing.Size(20, 20);
            this.ledWriteKmp1.TabIndex = 48;
            this.ledWriteKmp1.Visible = false;
            // 
            // ledReadKmp1
            // 
            this.ledReadKmp1.LedStyle = NationalInstruments.UI.LedStyle.Round3D;
            this.ledReadKmp1.Location = new System.Drawing.Point(73, 8);
            this.ledReadKmp1.Name = "ledReadKmp1";
            this.ledReadKmp1.OffColor = System.Drawing.Color.DarkGray;
            this.ledReadKmp1.OnColor = System.Drawing.Color.Green;
            this.ledReadKmp1.Size = new System.Drawing.Size(20, 20);
            this.ledReadKmp1.TabIndex = 49;
            // 
            // panel16
            // 
            this.panel16.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel16.Location = new System.Drawing.Point(673, 375);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(110, 35);
            this.panel16.TabIndex = 11;
            // 
            // panel13
            // 
            this.panel13.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel13.Location = new System.Drawing.Point(561, 380);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(110, 35);
            this.panel13.TabIndex = 8;
            // 
            // panel8
            // 
            this.panel8.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel8.Location = new System.Drawing.Point(226, 380);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(110, 35);
            this.panel8.TabIndex = 3;
            // 
            // panel10
            // 
            this.panel10.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel10.Location = new System.Drawing.Point(445, 380);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(110, 35);
            this.panel10.TabIndex = 5;
            // 
            // pLPA
            // 
            this.pLPA.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pLPA.Controls.Add(this.panel2);
            this.pLPA.Controls.Add(this.ledWriteLPA);
            this.pLPA.Controls.Add(this.ledReadLPA);
            this.pLPA.Location = new System.Drawing.Point(108, -3);
            this.pLPA.Name = "pLPA";
            this.pLPA.Size = new System.Drawing.Size(110, 35);
            this.pLPA.TabIndex = 1;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.bConnectLPA);
            this.panel2.Controls.Add(this.label11);
            this.panel2.Location = new System.Drawing.Point(4, 6);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(63, 23);
            this.panel2.TabIndex = 41;
            // 
            // bConnectLPA
            // 
            this.bConnectLPA.BackColor = System.Drawing.Color.Red;
            this.bConnectLPA.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.bConnectLPA.Location = new System.Drawing.Point(1, 0);
            this.bConnectLPA.Name = "bConnectLPA";
            this.bConnectLPA.Size = new System.Drawing.Size(23, 23);
            this.bConnectLPA.TabIndex = 37;
            this.bConnectLPA.UseVisualStyleBackColor = false;
            this.bConnectLPA.Click += new System.EventHandler(this.bConnectLPA_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.label11.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label11.Location = new System.Drawing.Point(25, 4);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(35, 15);
            this.label11.TabIndex = 84;
            this.label11.Text = "ЛПА";
            // 
            // ledWriteLPA
            // 
            this.ledWriteLPA.LedStyle = NationalInstruments.UI.LedStyle.Round3D;
            this.ledWriteLPA.Location = new System.Drawing.Point(83, 7);
            this.ledWriteLPA.Name = "ledWriteLPA";
            this.ledWriteLPA.OffColor = System.Drawing.Color.DarkGray;
            this.ledWriteLPA.OnColor = System.Drawing.Color.Red;
            this.ledWriteLPA.Size = new System.Drawing.Size(20, 20);
            this.ledWriteLPA.TabIndex = 39;
            // 
            // ledReadLPA
            // 
            this.ledReadLPA.LedStyle = NationalInstruments.UI.LedStyle.Round3D;
            this.ledReadLPA.Location = new System.Drawing.Point(69, 7);
            this.ledReadLPA.Name = "ledReadLPA";
            this.ledReadLPA.OffColor = System.Drawing.Color.DarkGray;
            this.ledReadLPA.OnColor = System.Drawing.Color.Green;
            this.ledReadLPA.Size = new System.Drawing.Size(20, 20);
            this.ledReadLPA.TabIndex = 40;
            // 
            // nudLPA2_4
            // 
            this.nudLPA2_4.ForeColor = System.Drawing.Color.Black;
            this.nudLPA2_4.Location = new System.Drawing.Point(578, 121);
            this.nudLPA2_4.Maximum = new decimal(new int[] {
            330,
            0,
            0,
            0});
            this.nudLPA2_4.Minimum = new decimal(new int[] {
            210,
            0,
            0,
            0});
            this.nudLPA2_4.Name = "nudLPA2_4";
            this.nudLPA2_4.Size = new System.Drawing.Size(58, 20);
            this.nudLPA2_4.TabIndex = 67;
            this.nudLPA2_4.Value = new decimal(new int[] {
            270,
            0,
            0,
            0});
            this.nudLPA2_4.ValueChanged += new System.EventHandler(this.nudLPA2_4_ValueChanged);
            // 
            // nudLPA1_3
            // 
            this.nudLPA1_3.ForeColor = System.Drawing.Color.Black;
            this.nudLPA1_3.Location = new System.Drawing.Point(422, 121);
            this.nudLPA1_3.Maximum = new decimal(new int[] {
            330,
            0,
            0,
            0});
            this.nudLPA1_3.Minimum = new decimal(new int[] {
            210,
            0,
            0,
            0});
            this.nudLPA1_3.Name = "nudLPA1_3";
            this.nudLPA1_3.Size = new System.Drawing.Size(58, 20);
            this.nudLPA1_3.TabIndex = 66;
            this.nudLPA1_3.Value = new decimal(new int[] {
            270,
            0,
            0,
            0});
            this.nudLPA1_3.ValueChanged += new System.EventHandler(this.nudLPA1_3_ValueChanged);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.tbLPA2);
            this.groupBox3.Controls.Add(this.bStopSP2);
            this.groupBox3.Controls.Add(this.lLPA2);
            this.groupBox3.Controls.Add(this.bStopPU);
            this.groupBox3.Controls.Add(this.bSetSP2);
            this.groupBox3.Controls.Add(this.bSetPU);
            this.groupBox3.Controls.Add(this.nudPU);
            this.groupBox3.Controls.Add(this.bStopLPA2);
            this.groupBox3.Controls.Add(this.tbPU);
            this.groupBox3.Controls.Add(this.nudSP2);
            this.groupBox3.Controls.Add(this.bSetLPA2);
            this.groupBox3.Controls.Add(this.lPU);
            this.groupBox3.Controls.Add(this.lSP2);
            this.groupBox3.Controls.Add(this.nudLPA2);
            this.groupBox3.Controls.Add(this.tbSP2);
            this.groupBox3.ForeColor = System.Drawing.Color.Blue;
            this.groupBox3.Location = new System.Drawing.Point(347, 37);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(331, 81);
            this.groupBox3.TabIndex = 61;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "ПУс";
            // 
            // tbLPA2
            // 
            this.tbLPA2.BackColor = System.Drawing.SystemColors.Control;
            this.tbLPA2.Enabled = false;
            this.tbLPA2.ForeColor = System.Drawing.Color.Black;
            this.tbLPA2.Location = new System.Drawing.Point(49, 57);
            this.tbLPA2.Name = "tbLPA2";
            this.tbLPA2.ReadOnly = true;
            this.tbLPA2.Size = new System.Drawing.Size(54, 20);
            this.tbLPA2.TabIndex = 9;
            // 
            // bStopSP2
            // 
            this.bStopSP2.ForeColor = System.Drawing.Color.Black;
            this.bStopSP2.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.bStopSP2.Location = new System.Drawing.Point(247, 8);
            this.bStopSP2.Name = "bStopSP2";
            this.bStopSP2.Size = new System.Drawing.Size(75, 23);
            this.bStopSP2.TabIndex = 20;
            this.bStopSP2.Text = "Остановить";
            this.bStopSP2.UseVisualStyleBackColor = true;
            this.bStopSP2.Click += new System.EventHandler(this.bStopSP_Click);
            // 
            // lLPA2
            // 
            this.lLPA2.AutoSize = true;
            this.lLPA2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.lLPA2.ForeColor = System.Drawing.Color.Red;
            this.lLPA2.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lLPA2.Location = new System.Drawing.Point(10, 62);
            this.lLPA2.Name = "lLPA2";
            this.lLPA2.Size = new System.Drawing.Size(33, 13);
            this.lLPA2.TabIndex = 6;
            this.lLPA2.Text = "ЛПА";
            this.lLPA2.DoubleClick += new System.EventHandler(this.lLPA_DoubleClick);
            // 
            // bStopPU
            // 
            this.bStopPU.ForeColor = System.Drawing.Color.Black;
            this.bStopPU.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.bStopPU.Location = new System.Drawing.Point(247, 32);
            this.bStopPU.Name = "bStopPU";
            this.bStopPU.Size = new System.Drawing.Size(75, 23);
            this.bStopPU.TabIndex = 18;
            this.bStopPU.Text = "Остановить";
            this.bStopPU.UseVisualStyleBackColor = true;
            this.bStopPU.Click += new System.EventHandler(this.bStopPU_Click);
            // 
            // bSetSP2
            // 
            this.bSetSP2.ForeColor = System.Drawing.Color.Black;
            this.bSetSP2.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.bSetSP2.Location = new System.Drawing.Point(168, 8);
            this.bSetSP2.Name = "bSetSP2";
            this.bSetSP2.Size = new System.Drawing.Size(75, 23);
            this.bSetSP2.TabIndex = 19;
            this.bSetSP2.Text = "Установить";
            this.bSetSP2.UseVisualStyleBackColor = true;
            this.bSetSP2.Click += new System.EventHandler(this.bSetSP_Click);
            // 
            // bSetPU
            // 
            this.bSetPU.ForeColor = System.Drawing.Color.Black;
            this.bSetPU.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.bSetPU.Location = new System.Drawing.Point(168, 32);
            this.bSetPU.Name = "bSetPU";
            this.bSetPU.Size = new System.Drawing.Size(75, 23);
            this.bSetPU.TabIndex = 17;
            this.bSetPU.Text = "Установить";
            this.bSetPU.UseVisualStyleBackColor = true;
            this.bSetPU.Click += new System.EventHandler(this.bSetPU_Click);
            // 
            // nudPU
            // 
            this.nudPU.ForeColor = System.Drawing.Color.Black;
            this.nudPU.Location = new System.Drawing.Point(104, 34);
            this.nudPU.Maximum = new decimal(new int[] {
            359,
            0,
            0,
            0});
            this.nudPU.Name = "nudPU";
            this.nudPU.Size = new System.Drawing.Size(58, 20);
            this.nudPU.TabIndex = 13;
            this.nudPU.ValueChanged += new System.EventHandler(this.nudPU_ValueChanged);
            // 
            // bStopLPA2
            // 
            this.bStopLPA2.ForeColor = System.Drawing.Color.Black;
            this.bStopLPA2.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.bStopLPA2.Location = new System.Drawing.Point(248, 56);
            this.bStopLPA2.Name = "bStopLPA2";
            this.bStopLPA2.Size = new System.Drawing.Size(75, 23);
            this.bStopLPA2.TabIndex = 16;
            this.bStopLPA2.Text = "Остановить";
            this.bStopLPA2.UseVisualStyleBackColor = true;
            this.bStopLPA2.Click += new System.EventHandler(this.bStopLPA_Click);
            // 
            // tbPU
            // 
            this.tbPU.Enabled = false;
            this.tbPU.ForeColor = System.Drawing.Color.Black;
            this.tbPU.Location = new System.Drawing.Point(48, 34);
            this.tbPU.Name = "tbPU";
            this.tbPU.ReadOnly = true;
            this.tbPU.Size = new System.Drawing.Size(54, 20);
            this.tbPU.TabIndex = 10;
            // 
            // nudSP2
            // 
            this.nudSP2.ForeColor = System.Drawing.Color.Black;
            this.nudSP2.Location = new System.Drawing.Point(104, 11);
            this.nudSP2.Maximum = new decimal(new int[] {
            359,
            0,
            0,
            0});
            this.nudSP2.Name = "nudSP2";
            this.nudSP2.Size = new System.Drawing.Size(58, 20);
            this.nudSP2.TabIndex = 14;
            this.nudSP2.ValueChanged += new System.EventHandler(this.nudSP_ValueChanged);
            // 
            // bSetLPA2
            // 
            this.bSetLPA2.ForeColor = System.Drawing.Color.Black;
            this.bSetLPA2.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.bSetLPA2.Location = new System.Drawing.Point(169, 56);
            this.bSetLPA2.Name = "bSetLPA2";
            this.bSetLPA2.Size = new System.Drawing.Size(75, 23);
            this.bSetLPA2.TabIndex = 15;
            this.bSetLPA2.Text = "Установить";
            this.bSetLPA2.UseVisualStyleBackColor = true;
            this.bSetLPA2.Click += new System.EventHandler(this.bSetLPA_Click);
            // 
            // lPU
            // 
            this.lPU.AutoSize = true;
            this.lPU.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.lPU.ForeColor = System.Drawing.Color.Cyan;
            this.lPU.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lPU.Location = new System.Drawing.Point(11, 39);
            this.lPU.Name = "lPU";
            this.lPU.Size = new System.Drawing.Size(25, 13);
            this.lPU.TabIndex = 7;
            this.lPU.Text = "ПУ";
            this.lPU.DoubleClick += new System.EventHandler(this.lPU_DoubleClick);
            // 
            // lSP2
            // 
            this.lSP2.AutoSize = true;
            this.lSP2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.lSP2.ForeColor = System.Drawing.Color.Green;
            this.lSP2.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lSP2.Location = new System.Drawing.Point(10, 15);
            this.lSP2.Name = "lSP2";
            this.lSP2.Size = new System.Drawing.Size(24, 13);
            this.lSP2.TabIndex = 8;
            this.lSP2.Text = "СП";
            this.lSP2.DoubleClick += new System.EventHandler(this.lSP_DoubleClick);
            // 
            // nudLPA2
            // 
            this.nudLPA2.ForeColor = System.Drawing.Color.Black;
            this.nudLPA2.Location = new System.Drawing.Point(105, 57);
            this.nudLPA2.Maximum = new decimal(new int[] {
            359,
            0,
            0,
            0});
            this.nudLPA2.Name = "nudLPA2";
            this.nudLPA2.Size = new System.Drawing.Size(58, 20);
            this.nudLPA2.TabIndex = 12;
            this.nudLPA2.ValueChanged += new System.EventHandler(this.nudLPA_ValueChanged);
            // 
            // tbSP2
            // 
            this.tbSP2.Enabled = false;
            this.tbSP2.ForeColor = System.Drawing.Color.Black;
            this.tbSP2.Location = new System.Drawing.Point(48, 11);
            this.tbSP2.Name = "tbSP2";
            this.tbSP2.ReadOnly = true;
            this.tbSP2.Size = new System.Drawing.Size(54, 20);
            this.tbSP2.TabIndex = 11;
            // 
            // lLPA1_3
            // 
            this.lLPA1_3.AutoSize = true;
            this.lLPA1_3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.lLPA1_3.ForeColor = System.Drawing.Color.Blue;
            this.lLPA1_3.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lLPA1_3.Location = new System.Drawing.Point(356, 126);
            this.lLPA1_3.Name = "lLPA1_3";
            this.lLPA1_3.Size = new System.Drawing.Size(63, 13);
            this.lLPA1_3.TabIndex = 64;
            this.lLPA1_3.Text = "ЛПА (1,3)";
            this.lLPA1_3.DoubleClick += new System.EventHandler(this.lLPA1_3_DoubleClick);
            // 
            // lLPA2_4
            // 
            this.lLPA2_4.AutoSize = true;
            this.lLPA2_4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.lLPA2_4.ForeColor = System.Drawing.Color.Olive;
            this.lLPA2_4.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lLPA2_4.Location = new System.Drawing.Point(512, 126);
            this.lLPA2_4.Name = "lLPA2_4";
            this.lLPA2_4.Size = new System.Drawing.Size(63, 13);
            this.lLPA2_4.TabIndex = 65;
            this.lLPA2_4.Text = "ЛПА (2,4)";
            this.lLPA2_4.DoubleClick += new System.EventHandler(this.lLPA2_4_DoubleClick);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.tbAlt);
            this.groupBox4.Controls.Add(this.label1);
            this.groupBox4.Controls.Add(this.groupBox6);
            this.groupBox4.Controls.Add(this.groupBox5);
            this.groupBox4.Controls.Add(this.tbDate);
            this.groupBox4.Controls.Add(this.lDate);
            this.groupBox4.Controls.Add(this.tbTime);
            this.groupBox4.Controls.Add(this.lTime);
            this.groupBox4.Controls.Add(this.tbCountSat);
            this.groupBox4.Controls.Add(this.tbLon);
            this.groupBox4.Controls.Add(this.tbLat);
            this.groupBox4.Controls.Add(this.lCountSat);
            this.groupBox4.Controls.Add(this.lLon);
            this.groupBox4.Controls.Add(this.lLAt);
            this.groupBox4.ForeColor = System.Drawing.Color.Blue;
            this.groupBox4.Location = new System.Drawing.Point(347, 186);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(331, 154);
            this.groupBox4.TabIndex = 62;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Местоположение";
            // 
            // tbAlt
            // 
            this.tbAlt.Location = new System.Drawing.Point(78, 104);
            this.tbAlt.Name = "tbAlt";
            this.tbAlt.ReadOnly = true;
            this.tbAlt.Size = new System.Drawing.Size(111, 20);
            this.tbAlt.TabIndex = 18;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label1.Location = new System.Drawing.Point(6, 111);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 13);
            this.label1.TabIndex = 17;
            this.label1.Text = "Высота, м";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.rbWGS84);
            this.groupBox6.Controls.Add(this.rbCK42);
            this.groupBox6.Controls.Add(this.cbCoordinateSystem);
            this.groupBox6.ForeColor = System.Drawing.Color.Blue;
            this.groupBox6.Location = new System.Drawing.Point(195, 71);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(130, 75);
            this.groupBox6.TabIndex = 16;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Система координат";
            // 
            // rbWGS84
            // 
            this.rbWGS84.AutoSize = true;
            this.rbWGS84.Checked = true;
            this.rbWGS84.ForeColor = System.Drawing.Color.Navy;
            this.rbWGS84.Location = new System.Drawing.Point(4, 19);
            this.rbWGS84.Name = "rbWGS84";
            this.rbWGS84.Size = new System.Drawing.Size(66, 17);
            this.rbWGS84.TabIndex = 16;
            this.rbWGS84.TabStop = true;
            this.rbWGS84.Text = "WGS 84";
            this.rbWGS84.UseVisualStyleBackColor = true;
            this.rbWGS84.CheckedChanged += new System.EventHandler(this.rbWGS84_CheckedChanged);
            // 
            // rbCK42
            // 
            this.rbCK42.AutoSize = true;
            this.rbCK42.ForeColor = System.Drawing.Color.Navy;
            this.rbCK42.Location = new System.Drawing.Point(75, 19);
            this.rbCK42.Name = "rbCK42";
            this.rbCK42.Size = new System.Drawing.Size(54, 17);
            this.rbCK42.TabIndex = 15;
            this.rbCK42.Text = "СК 42";
            this.rbCK42.UseVisualStyleBackColor = true;
            this.rbCK42.CheckedChanged += new System.EventHandler(this.rbCK42_CheckedChanged);
            // 
            // cbCoordinateSystem
            // 
            this.cbCoordinateSystem.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbCoordinateSystem.Font = new System.Drawing.Font("Arial Narrow", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cbCoordinateSystem.FormattingEnabled = true;
            this.cbCoordinateSystem.Location = new System.Drawing.Point(6, 44);
            this.cbCoordinateSystem.Name = "cbCoordinateSystem";
            this.cbCoordinateSystem.Size = new System.Drawing.Size(119, 23);
            this.cbCoordinateSystem.TabIndex = 14;
            // 
            // groupBox5
            // 
            this.groupBox5.BackColor = System.Drawing.SystemColors.Control;
            this.groupBox5.Controls.Add(this.cbSatSyst);
            this.groupBox5.ForeColor = System.Drawing.Color.Blue;
            this.groupBox5.Location = new System.Drawing.Point(193, 10);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(132, 53);
            this.groupBox5.TabIndex = 15;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Спутниковая система";
            // 
            // cbSatSyst
            // 
            this.cbSatSyst.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbSatSyst.FormattingEnabled = true;
            this.cbSatSyst.Items.AddRange(new object[] {
            "GPS",
            "GPS/ГЛОНАСС",
            "ГЛОНАСС"});
            this.cbSatSyst.Location = new System.Drawing.Point(5, 22);
            this.cbSatSyst.Name = "cbSatSyst";
            this.cbSatSyst.Size = new System.Drawing.Size(122, 21);
            this.cbSatSyst.TabIndex = 14;
            this.cbSatSyst.SelectedIndexChanged += new System.EventHandler(this.cbSatSyst_SelectedIndexChanged);
            // 
            // tbDate
            // 
            this.tbDate.Location = new System.Drawing.Point(77, 15);
            this.tbDate.Name = "tbDate";
            this.tbDate.ReadOnly = true;
            this.tbDate.Size = new System.Drawing.Size(111, 20);
            this.tbDate.TabIndex = 13;
            // 
            // lDate
            // 
            this.lDate.AutoSize = true;
            this.lDate.ForeColor = System.Drawing.Color.Black;
            this.lDate.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lDate.Location = new System.Drawing.Point(9, 22);
            this.lDate.Name = "lDate";
            this.lDate.Size = new System.Drawing.Size(33, 13);
            this.lDate.TabIndex = 12;
            this.lDate.Text = "Дата";
            // 
            // tbTime
            // 
            this.tbTime.Location = new System.Drawing.Point(77, 38);
            this.tbTime.Name = "tbTime";
            this.tbTime.ReadOnly = true;
            this.tbTime.Size = new System.Drawing.Size(111, 20);
            this.tbTime.TabIndex = 8;
            // 
            // lTime
            // 
            this.lTime.AutoSize = true;
            this.lTime.ForeColor = System.Drawing.Color.Black;
            this.lTime.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lTime.Location = new System.Drawing.Point(8, 45);
            this.lTime.Name = "lTime";
            this.lTime.Size = new System.Drawing.Size(40, 13);
            this.lTime.TabIndex = 3;
            this.lTime.Text = "Время";
            // 
            // tbCountSat
            // 
            this.tbCountSat.Location = new System.Drawing.Point(78, 126);
            this.tbCountSat.Name = "tbCountSat";
            this.tbCountSat.ReadOnly = true;
            this.tbCountSat.Size = new System.Drawing.Size(111, 20);
            this.tbCountSat.TabIndex = 7;
            // 
            // tbLon
            // 
            this.tbLon.Location = new System.Drawing.Point(78, 82);
            this.tbLon.Name = "tbLon";
            this.tbLon.ReadOnly = true;
            this.tbLon.Size = new System.Drawing.Size(111, 20);
            this.tbLon.TabIndex = 6;
            // 
            // tbLat
            // 
            this.tbLat.Location = new System.Drawing.Point(77, 60);
            this.tbLat.Name = "tbLat";
            this.tbLat.ReadOnly = true;
            this.tbLat.Size = new System.Drawing.Size(111, 20);
            this.tbLat.TabIndex = 5;
            // 
            // lCountSat
            // 
            this.lCountSat.AutoSize = true;
            this.lCountSat.ForeColor = System.Drawing.Color.Black;
            this.lCountSat.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lCountSat.Location = new System.Drawing.Point(6, 133);
            this.lCountSat.Name = "lCountSat";
            this.lCountSat.Size = new System.Drawing.Size(63, 13);
            this.lCountSat.TabIndex = 2;
            this.lCountSat.Text = "Кол. спутн.";
            // 
            // lLon
            // 
            this.lLon.AutoSize = true;
            this.lLon.ForeColor = System.Drawing.Color.Black;
            this.lLon.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lLon.Location = new System.Drawing.Point(6, 89);
            this.lLon.Name = "lLon";
            this.lLon.Size = new System.Drawing.Size(50, 13);
            this.lLon.TabIndex = 1;
            this.lLon.Text = "Долгота";
            // 
            // lLAt
            // 
            this.lLAt.AutoSize = true;
            this.lLAt.ForeColor = System.Drawing.Color.Black;
            this.lLAt.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lLAt.Location = new System.Drawing.Point(8, 67);
            this.lLAt.Name = "lLAt";
            this.lLAt.Size = new System.Drawing.Size(45, 13);
            this.lLAt.TabIndex = 0;
            this.lLAt.Text = "Широта";
            // 
            // timerLedReadBytes
            // 
            this.timerLedReadBytes.Interval = 300;
            this.timerLedReadBytes.Tick += new System.EventHandler(this.timerLedReadBytes_Tick);
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.bTransportPU);
            this.groupBox7.Controls.Add(this.bTransportLPA);
            this.groupBox7.Controls.Add(this.bTransportSP);
            this.groupBox7.ForeColor = System.Drawing.Color.Blue;
            this.groupBox7.Location = new System.Drawing.Point(347, 142);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(331, 43);
            this.groupBox7.TabIndex = 70;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Транспортное положение";
            // 
            // bTransportPU
            // 
            this.bTransportPU.Enabled = false;
            this.bTransportPU.ForeColor = System.Drawing.Color.Black;
            this.bTransportPU.Location = new System.Drawing.Point(130, 15);
            this.bTransportPU.Name = "bTransportPU";
            this.bTransportPU.Size = new System.Drawing.Size(75, 23);
            this.bTransportPU.TabIndex = 70;
            this.bTransportPU.Text = "АПУ ПУ";
            this.bTransportPU.UseVisualStyleBackColor = true;
            this.bTransportPU.Click += new System.EventHandler(this.bTransportPU_Click);
            // 
            // bTransportLPA
            // 
            this.bTransportLPA.Enabled = false;
            this.bTransportLPA.ForeColor = System.Drawing.Color.Black;
            this.bTransportLPA.Location = new System.Drawing.Point(247, 15);
            this.bTransportLPA.Name = "bTransportLPA";
            this.bTransportLPA.Size = new System.Drawing.Size(75, 23);
            this.bTransportLPA.TabIndex = 1;
            this.bTransportLPA.Text = "АПУ ЛПА";
            this.bTransportLPA.UseVisualStyleBackColor = true;
            this.bTransportLPA.Click += new System.EventHandler(this.bTransportLPA_Click);
            // 
            // bTransportSP
            // 
            this.bTransportSP.Enabled = false;
            this.bTransportSP.ForeColor = System.Drawing.Color.Black;
            this.bTransportSP.Location = new System.Drawing.Point(12, 15);
            this.bTransportSP.Name = "bTransportSP";
            this.bTransportSP.Size = new System.Drawing.Size(75, 23);
            this.bTransportSP.TabIndex = 0;
            this.bTransportSP.Text = "АПУ СП";
            this.bTransportSP.UseVisualStyleBackColor = true;
            this.bTransportSP.Click += new System.EventHandler(this.bTransportSP_Click);
            // 
            // timerTest
            // 
            this.timerTest.Enabled = true;
            this.timerTest.Interval = 3000;
            //this.timerTest.Tick += new System.EventHandler(this.timerTest_Tick);
            // 
            // WndProject
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(683, 383);
            this.Controls.Add(this.groupBox7);
            this.Controls.Add(this.complexGraph);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.pSP);
            this.Controls.Add(this.pGPS);
            this.Controls.Add(this.pState);
            this.Controls.Add(this.nudLPA2_4);
            this.Controls.Add(this.nudLPA1_3);
            this.Controls.Add(this.lLPA1_3);
            this.Controls.Add(this.lLPA2_4);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "WndProject";
            this.Text = "Местоположение и направление антенн";
            this.TopMost = true;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.WndProject_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.complexGraph)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.pSP.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ledWriteSP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ledReadSP)).EndInit();
            this.pGPS.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ledWriteGPS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ledReadGPS1)).EndInit();
            this.pState.ResumeLayout(false);
            this.pKmpPA.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ledReadKmp2)).EndInit();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ledWriteKmp2)).EndInit();
            this.pPU.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ledWritePU)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ledReadPU)).EndInit();
            this.pKmpRR.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ledWriteKmp1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ledReadKmp1)).EndInit();
            this.pLPA.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ledWriteLPA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ledReadLPA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudLPA2_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudLPA1_3)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudPU)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSP2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudLPA2)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private NationalInstruments.UI.WindowsForms.ComplexGraph complexGraph;
        private NationalInstruments.UI.ComplexPlot complexPlotLPA;
        private NationalInstruments.UI.ComplexXAxis complexXAxis1;
        private NationalInstruments.UI.ComplexYAxis complexYAxis1;
        private NationalInstruments.UI.ComplexPlot complexPlotPU;
        private NationalInstruments.UI.ComplexPlot complexPlotSP;
        private NationalInstruments.UI.ComplexPlot complexPlotLPA1_3;
        private NationalInstruments.UI.ComplexPlot complexPlotLPA2_4;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lK1Azimuth;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox tbK2Azimuth;
        private System.Windows.Forms.Label lK2Azimuth;
        private System.Windows.Forms.Panel pSP;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Button bConnectSP;
        private System.Windows.Forms.Label label17;
        private NationalInstruments.UI.WindowsForms.Led ledWriteSP;
        private NationalInstruments.UI.WindowsForms.Led ledReadSP;
        private System.Windows.Forms.Panel pGPS;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button bConnectGPS;
        private System.Windows.Forms.Label lGPS;
        private NationalInstruments.UI.WindowsForms.Led ledWriteGPS;
        private NationalInstruments.UI.WindowsForms.Led ledReadGPS1;
        private System.Windows.Forms.Panel pState;
        private System.Windows.Forms.Panel pKmpPA;
        private NationalInstruments.UI.WindowsForms.Led ledReadKmp2;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Button bConnectKmp2;
        private System.Windows.Forms.Label label19;
        private NationalInstruments.UI.WindowsForms.Led ledWriteKmp2;
        private System.Windows.Forms.Panel pPU;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button bConnectPU;
        private System.Windows.Forms.Label label16;
        private NationalInstruments.UI.WindowsForms.Led ledWritePU;
        private NationalInstruments.UI.WindowsForms.Led ledReadPU;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Panel pKmpRR;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Button bConnectKmp1;
        private System.Windows.Forms.Label label18;
        private NationalInstruments.UI.WindowsForms.Led ledWriteKmp1;
        private NationalInstruments.UI.WindowsForms.Led ledReadKmp1;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Panel pLPA;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button bConnectLPA;
        private System.Windows.Forms.Label label11;
        private NationalInstruments.UI.WindowsForms.Led ledWriteLPA;
        private NationalInstruments.UI.WindowsForms.Led ledReadLPA;
        private System.Windows.Forms.NumericUpDown nudLPA2_4;
        private System.Windows.Forms.NumericUpDown nudLPA1_3;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button bStopSP2;
        private System.Windows.Forms.Button bStopPU;
        private System.Windows.Forms.Button bSetSP2;
        private System.Windows.Forms.Button bSetPU;
        private System.Windows.Forms.Button bStopLPA2;
        private System.Windows.Forms.Button bSetLPA2;
        private System.Windows.Forms.NumericUpDown nudPU;
        private System.Windows.Forms.NumericUpDown nudLPA2;
        private System.Windows.Forms.TextBox tbPU;
        private System.Windows.Forms.NumericUpDown nudSP2;
        private System.Windows.Forms.TextBox tbLPA2;
        private System.Windows.Forms.Label lPU;
        private System.Windows.Forms.Label lLPA2;
        private System.Windows.Forms.Label lSP2;
        private System.Windows.Forms.TextBox tbSP2;
        private System.Windows.Forms.Label lLPA1_3;
        private System.Windows.Forms.Label lLPA2_4;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox tbAlt;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.ComboBox cbCoordinateSystem;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.ComboBox cbSatSyst;
        private System.Windows.Forms.TextBox tbDate;
        private System.Windows.Forms.Label lDate;
        private System.Windows.Forms.TextBox tbTime;
        private System.Windows.Forms.Label lTime;
        private System.Windows.Forms.TextBox tbCountSat;
        private System.Windows.Forms.TextBox tbLon;
        private System.Windows.Forms.TextBox tbLat;
        private System.Windows.Forms.Label lCountSat;
        private System.Windows.Forms.Label lLon;
        private System.Windows.Forms.Label lLAt;
        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.Windows.Forms.Timer timerLedReadBytes;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.TextBox tbK1Azimuth;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Button bTransportPU;
        private System.Windows.Forms.Button bTransportLPA;
        private System.Windows.Forms.Button bTransportSP;
        private System.Windows.Forms.RadioButton rbWGS84;
        private System.Windows.Forms.RadioButton rbCK42;
        private System.Windows.Forms.Timer timerTest;
    }
}