﻿namespace WndProject
{
    partial class MainWnd
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing && (components != null))
                {
                    components.Dispose();
                }
                base.Dispose(disposing);
            }
            catch { }
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWnd));
            this.pManage = new System.Windows.Forms.Panel();
            this.panel29 = new System.Windows.Forms.Panel();
            this.lOperator = new System.Windows.Forms.Label();
            this.panel28 = new System.Windows.Forms.Panel();
            this.lOwnRole = new System.Windows.Forms.Label();
            this.panel27 = new System.Windows.Forms.Panel();
            this.lOwnAddressPC = new System.Windows.Forms.Label();
            this.panel24 = new System.Windows.Forms.Panel();
            this.lOwnNumber = new System.Windows.Forms.Label();
            this.bRegimeStop = new System.Windows.Forms.Button();
            this.bRegimeRecon = new System.Windows.Forms.Button();
            this.bRegimeSuppr = new System.Windows.Forms.Button();
            this.pictureBox15 = new System.Windows.Forms.PictureBox();
            this.pTask = new System.Windows.Forms.Panel();
            this.bTechnicalAnalysis = new System.Windows.Forms.Button();
            this.bMap = new System.Windows.Forms.Button();
            this.MessBut = new System.Windows.Forms.Button();
            this.bModeS = new System.Windows.Forms.Button();
            this.FogNut = new System.Windows.Forms.Button();
            this.button17 = new System.Windows.Forms.Button();
            this.button15 = new System.Windows.Forms.Button();
            this.bSpecialFrequencies = new System.Windows.Forms.Button();
            this.bSectorsRanges = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.pRecDev = new System.Windows.Forms.Panel();
            this.controlARDV1 = new Control_AR_DV1.UserControl1();
            this.controlAR6000 = new ControlAR6000.controlAR6000();
            this.controlARONE = new ControlARONE.ControlARONE();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.pTable = new System.Windows.Forms.Panel();
            this.tcTables = new System.Windows.Forms.TabControl();
            this.tpIRI_FRCh = new System.Windows.Forms.TabPage();
            this.table_IRI_FRCh = new Table_IRI_FRCh.table_IRI_FRCh();
            this.tpIRI_FRCh_CR = new System.Windows.Forms.TabPage();
            this.table_IRI_FRCh_CR = new Table_IRI_FRCh_CR.table_IRI_FRCh_CR();
            this.tpIRI_FRCh_RP = new System.Windows.Forms.TabPage();
            this.table_IRI_FRCh_RP = new Table_IRI_FRCh_RP.table_IRI_FRCh_RP();
            this.tpIRI_PPRCh = new System.Windows.Forms.TabPage();
            this.table_IRI_PPRCh = new Table_IRI_PPRCh.table_IRI_PPRCh();
            this.tpIRI_PPRCh_CR = new System.Windows.Forms.TabPage();
            this.table_IRI_PPRCh_RP = new Table_IRI_PPRCh_RP.table_IRI_PPRCh_RP();
            this.splitter2 = new System.Windows.Forms.Splitter();
            this.dfd = new System.Windows.Forms.Panel();
            this.panoramaControl1 = new PanoramaLibrary.PanoramaControl();
            this.tmFlashFRQ = new System.Windows.Forms.Timer(this.components);
            this.tmValcoder = new System.Windows.Forms.Timer(this.components);
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.pStateServer = new System.Windows.Forms.Panel();
            this.pFreqChannel = new System.Windows.Forms.Panel();
            this.bFreqChannel = new System.Windows.Forms.Button();
            this.lFreqChannel = new System.Windows.Forms.Label();
            this.pIndicateFCh = new System.Windows.Forms.Panel();
            this.pbReadFCh = new System.Windows.Forms.PictureBox();
            this.pbWriteFCh = new System.Windows.Forms.PictureBox();
            this.pASP = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.butClientAsp = new System.Windows.Forms.Button();
            this.butServerAsp = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pState = new System.Windows.Forms.Panel();
            this.panel31 = new System.Windows.Forms.Panel();
            this.panel32 = new System.Windows.Forms.Panel();
            this.butClientHS = new System.Windows.Forms.Button();
            this.butServerHS = new System.Windows.Forms.Button();
            this.label15 = new System.Windows.Forms.Label();
            this.panel33 = new System.Windows.Forms.Panel();
            this.pbReadHS = new System.Windows.Forms.PictureBox();
            this.pbWriteHS = new System.Windows.Forms.PictureBox();
            this.pADSB = new System.Windows.Forms.Panel();
            this.panel25 = new System.Windows.Forms.Panel();
            this.lADSB = new System.Windows.Forms.Label();
            this.bConnectModeS = new System.Windows.Forms.Button();
            this.panel26 = new System.Windows.Forms.Panel();
            this.pictureBox16 = new System.Windows.Forms.PictureBox();
            this.pbReadModeS = new System.Windows.Forms.PictureBox();
            this.panel19 = new System.Windows.Forms.Panel();
            this.panel20 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.bAOR = new System.Windows.Forms.Button();
            this.panel21 = new System.Windows.Forms.Panel();
            this.pictureBox13 = new System.Windows.Forms.PictureBox();
            this.pictureBox14 = new System.Windows.Forms.PictureBox();
            this.panel7 = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.buttonConnectFPS = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.panel9 = new System.Windows.Forms.Panel();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pPU = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.btPU = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.pbReadPU = new System.Windows.Forms.PictureBox();
            this.pbWritePU = new System.Windows.Forms.PictureBox();
            this.tmBaraban = new System.Windows.Forms.Timer(this.components);
            this.tm500 = new System.Windows.Forms.Timer(this.components);
            this.pManage.SuspendLayout();
            this.panel29.SuspendLayout();
            this.panel28.SuspendLayout();
            this.panel27.SuspendLayout();
            this.panel24.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).BeginInit();
            this.pTask.SuspendLayout();
            this.pRecDev.SuspendLayout();
            this.pTable.SuspendLayout();
            this.tcTables.SuspendLayout();
            this.tpIRI_FRCh.SuspendLayout();
            this.tpIRI_FRCh_CR.SuspendLayout();
            this.tpIRI_FRCh_RP.SuspendLayout();
            this.tpIRI_PPRCh.SuspendLayout();
            this.tpIRI_PPRCh_CR.SuspendLayout();
            this.dfd.SuspendLayout();
            this.pStateServer.SuspendLayout();
            this.pFreqChannel.SuspendLayout();
            this.pIndicateFCh.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbReadFCh)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbWriteFCh)).BeginInit();
            this.pASP.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.pState.SuspendLayout();
            this.panel31.SuspendLayout();
            this.panel32.SuspendLayout();
            this.panel33.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbReadHS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbWriteHS)).BeginInit();
            this.pADSB.SuspendLayout();
            this.panel25.SuspendLayout();
            this.panel26.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbReadModeS)).BeginInit();
            this.panel19.SuspendLayout();
            this.panel20.SuspendLayout();
            this.panel21.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).BeginInit();
            this.panel7.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            this.pPU.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbReadPU)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbWritePU)).BeginInit();
            this.SuspendLayout();
            // 
            // pManage
            // 
            resources.ApplyResources(this.pManage, "pManage");
            this.pManage.BackColor = System.Drawing.Color.Gainsboro;
            this.pManage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pManage.Controls.Add(this.panel29);
            this.pManage.Controls.Add(this.panel28);
            this.pManage.Controls.Add(this.panel27);
            this.pManage.Controls.Add(this.panel24);
            this.pManage.Controls.Add(this.bRegimeStop);
            this.pManage.Controls.Add(this.bRegimeRecon);
            this.pManage.Controls.Add(this.bRegimeSuppr);
            this.pManage.Controls.Add(this.pictureBox15);
            this.pManage.Name = "pManage";
            this.toolTip.SetToolTip(this.pManage, resources.GetString("pManage.ToolTip"));
            // 
            // panel29
            // 
            resources.ApplyResources(this.panel29, "panel29");
            this.panel29.BackColor = System.Drawing.Color.Gainsboro;
            this.panel29.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel29.Controls.Add(this.lOperator);
            this.panel29.Name = "panel29";
            this.toolTip.SetToolTip(this.panel29, resources.GetString("panel29.ToolTip"));
            // 
            // lOperator
            // 
            resources.ApplyResources(this.lOperator, "lOperator");
            this.lOperator.ForeColor = System.Drawing.Color.Blue;
            this.lOperator.Name = "lOperator";
            this.toolTip.SetToolTip(this.lOperator, resources.GetString("lOperator.ToolTip"));
            // 
            // panel28
            // 
            resources.ApplyResources(this.panel28, "panel28");
            this.panel28.BackColor = System.Drawing.Color.Gainsboro;
            this.panel28.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel28.Controls.Add(this.lOwnRole);
            this.panel28.Name = "panel28";
            this.toolTip.SetToolTip(this.panel28, resources.GetString("panel28.ToolTip"));
            // 
            // lOwnRole
            // 
            resources.ApplyResources(this.lOwnRole, "lOwnRole");
            this.lOwnRole.ForeColor = System.Drawing.Color.Blue;
            this.lOwnRole.Name = "lOwnRole";
            this.toolTip.SetToolTip(this.lOwnRole, resources.GetString("lOwnRole.ToolTip"));
            // 
            // panel27
            // 
            resources.ApplyResources(this.panel27, "panel27");
            this.panel27.BackColor = System.Drawing.Color.Gainsboro;
            this.panel27.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel27.Controls.Add(this.lOwnAddressPC);
            this.panel27.Name = "panel27";
            this.toolTip.SetToolTip(this.panel27, resources.GetString("panel27.ToolTip"));
            // 
            // lOwnAddressPC
            // 
            resources.ApplyResources(this.lOwnAddressPC, "lOwnAddressPC");
            this.lOwnAddressPC.ForeColor = System.Drawing.Color.Blue;
            this.lOwnAddressPC.Name = "lOwnAddressPC";
            this.toolTip.SetToolTip(this.lOwnAddressPC, resources.GetString("lOwnAddressPC.ToolTip"));
            // 
            // panel24
            // 
            resources.ApplyResources(this.panel24, "panel24");
            this.panel24.BackColor = System.Drawing.Color.Gainsboro;
            this.panel24.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel24.Controls.Add(this.lOwnNumber);
            this.panel24.Name = "panel24";
            this.toolTip.SetToolTip(this.panel24, resources.GetString("panel24.ToolTip"));
            // 
            // lOwnNumber
            // 
            resources.ApplyResources(this.lOwnNumber, "lOwnNumber");
            this.lOwnNumber.ForeColor = System.Drawing.Color.Blue;
            this.lOwnNumber.Name = "lOwnNumber";
            this.toolTip.SetToolTip(this.lOwnNumber, resources.GetString("lOwnNumber.ToolTip"));
            // 
            // bRegimeStop
            // 
            resources.ApplyResources(this.bRegimeStop, "bRegimeStop");
            this.bRegimeStop.ForeColor = System.Drawing.Color.Green;
            this.bRegimeStop.Image = global::WndProject.Properties.Resources.greenStop;
            this.bRegimeStop.Name = "bRegimeStop";
            this.toolTip.SetToolTip(this.bRegimeStop, resources.GetString("bRegimeStop.ToolTip"));
            this.bRegimeStop.UseVisualStyleBackColor = true;
            this.bRegimeStop.Click += new System.EventHandler(this.bRegimeStop_Click);
            // 
            // bRegimeRecon
            // 
            resources.ApplyResources(this.bRegimeRecon, "bRegimeRecon");
            this.bRegimeRecon.Image = global::WndProject.Properties.Resources.grayRecon;
            this.bRegimeRecon.Name = "bRegimeRecon";
            this.toolTip.SetToolTip(this.bRegimeRecon, resources.GetString("bRegimeRecon.ToolTip"));
            this.bRegimeRecon.UseVisualStyleBackColor = true;
            this.bRegimeRecon.Click += new System.EventHandler(this.bRegimeRecon_Click);
            // 
            // bRegimeSuppr
            // 
            resources.ApplyResources(this.bRegimeSuppr, "bRegimeSuppr");
            this.bRegimeSuppr.Image = global::WndProject.Properties.Resources.graySuppr;
            this.bRegimeSuppr.Name = "bRegimeSuppr";
            this.toolTip.SetToolTip(this.bRegimeSuppr, resources.GetString("bRegimeSuppr.ToolTip"));
            this.bRegimeSuppr.UseVisualStyleBackColor = true;
            this.bRegimeSuppr.Click += new System.EventHandler(this.bRegimeSuppr_Click);
            // 
            // pictureBox15
            // 
            resources.ApplyResources(this.pictureBox15, "pictureBox15");
            this.pictureBox15.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox15.Image = global::WndProject.Properties.Resources.Radar;
            this.pictureBox15.Name = "pictureBox15";
            this.pictureBox15.TabStop = false;
            this.toolTip.SetToolTip(this.pictureBox15, resources.GetString("pictureBox15.ToolTip"));
            // 
            // pTask
            // 
            resources.ApplyResources(this.pTask, "pTask");
            this.pTask.BackColor = System.Drawing.Color.Gainsboro;
            this.pTask.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pTask.Controls.Add(this.bTechnicalAnalysis);
            this.pTask.Controls.Add(this.bMap);
            this.pTask.Controls.Add(this.MessBut);
            this.pTask.Controls.Add(this.bModeS);
            this.pTask.Controls.Add(this.FogNut);
            this.pTask.Controls.Add(this.button17);
            this.pTask.Controls.Add(this.button15);
            this.pTask.Controls.Add(this.bSpecialFrequencies);
            this.pTask.Controls.Add(this.bSectorsRanges);
            this.pTask.Controls.Add(this.button11);
            this.pTask.Name = "pTask";
            this.toolTip.SetToolTip(this.pTask, resources.GetString("pTask.ToolTip"));
            // 
            // bTechnicalAnalysis
            // 
            resources.ApplyResources(this.bTechnicalAnalysis, "bTechnicalAnalysis");
            this.bTechnicalAnalysis.BackColor = System.Drawing.Color.Transparent;
            this.bTechnicalAnalysis.BackgroundImage = global::WndProject.Properties.Resources.TehAnaliz;
            this.bTechnicalAnalysis.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.bTechnicalAnalysis.FlatAppearance.BorderSize = 0;
            this.bTechnicalAnalysis.Name = "bTechnicalAnalysis";
            this.toolTip.SetToolTip(this.bTechnicalAnalysis, resources.GetString("bTechnicalAnalysis.ToolTip"));
            this.bTechnicalAnalysis.UseVisualStyleBackColor = false;
            this.bTechnicalAnalysis.Click += new System.EventHandler(this.bTechnicalAnalysis_Click);
            // 
            // bMap
            // 
            resources.ApplyResources(this.bMap, "bMap");
            this.bMap.BackColor = System.Drawing.Color.Transparent;
            this.bMap.BackgroundImage = global::WndProject.Properties.Resources.Map;
            this.bMap.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.bMap.FlatAppearance.BorderSize = 0;
            this.bMap.Name = "bMap";
            this.toolTip.SetToolTip(this.bMap, resources.GetString("bMap.ToolTip"));
            this.bMap.UseVisualStyleBackColor = false;
            this.bMap.Click += new System.EventHandler(this.bMap_Click);
            // 
            // MessBut
            // 
            resources.ApplyResources(this.MessBut, "MessBut");
            this.MessBut.BackColor = System.Drawing.Color.Transparent;
            this.MessBut.BackgroundImage = global::WndProject.Properties.Resources.Message;
            this.MessBut.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.MessBut.FlatAppearance.BorderSize = 0;
            this.MessBut.Name = "MessBut";
            this.toolTip.SetToolTip(this.MessBut, resources.GetString("MessBut.ToolTip"));
            this.MessBut.UseVisualStyleBackColor = false;
            this.MessBut.Click += new System.EventHandler(this.MessBut_Click);
            // 
            // bModeS
            // 
            resources.ApplyResources(this.bModeS, "bModeS");
            this.bModeS.BackColor = System.Drawing.Color.Transparent;
            this.bModeS.BackgroundImage = global::WndProject.Properties.Resources.ModeS;
            this.bModeS.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.bModeS.FlatAppearance.BorderSize = 0;
            this.bModeS.Name = "bModeS";
            this.toolTip.SetToolTip(this.bModeS, resources.GetString("bModeS.ToolTip"));
            this.bModeS.UseVisualStyleBackColor = false;
            this.bModeS.Click += new System.EventHandler(this.bModeS_Click);
            // 
            // FogNut
            // 
            resources.ApplyResources(this.FogNut, "FogNut");
            this.FogNut.BackColor = System.Drawing.Color.Transparent;
            this.FogNut.BackgroundImage = global::WndProject.Properties.Resources.sputnik2;
            this.FogNut.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.FogNut.FlatAppearance.BorderSize = 0;
            this.FogNut.Name = "FogNut";
            this.toolTip.SetToolTip(this.FogNut, resources.GetString("FogNut.ToolTip"));
            this.FogNut.UseVisualStyleBackColor = false;
            this.FogNut.Click += new System.EventHandler(this.FogNut_Click);
            // 
            // button17
            // 
            resources.ApplyResources(this.button17, "button17");
            this.button17.BackColor = System.Drawing.Color.Transparent;
            this.button17.BackgroundImage = global::WndProject.Properties.Resources.WFF;
            this.button17.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.button17.FlatAppearance.BorderSize = 0;
            this.button17.Name = "button17";
            this.toolTip.SetToolTip(this.button17, resources.GetString("button17.ToolTip"));
            this.button17.UseVisualStyleBackColor = false;
            this.button17.Click += new System.EventHandler(this.button17_Click);
            // 
            // button15
            // 
            resources.ApplyResources(this.button15, "button15");
            this.button15.BackColor = System.Drawing.Color.Transparent;
            this.button15.BackgroundImage = global::WndProject.Properties.Resources.compas_GPS1;
            this.button15.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.button15.FlatAppearance.BorderSize = 0;
            this.button15.Name = "button15";
            this.toolTip.SetToolTip(this.button15, resources.GetString("button15.ToolTip"));
            this.button15.UseVisualStyleBackColor = false;
            this.button15.Click += new System.EventHandler(this.button15_Click);
            // 
            // bSpecialFrequencies
            // 
            resources.ApplyResources(this.bSpecialFrequencies, "bSpecialFrequencies");
            this.bSpecialFrequencies.BackColor = System.Drawing.Color.Transparent;
            this.bSpecialFrequencies.BackgroundImage = global::WndProject.Properties.Resources.SpecFreq;
            this.bSpecialFrequencies.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.bSpecialFrequencies.FlatAppearance.BorderSize = 0;
            this.bSpecialFrequencies.Name = "bSpecialFrequencies";
            this.toolTip.SetToolTip(this.bSpecialFrequencies, resources.GetString("bSpecialFrequencies.ToolTip"));
            this.bSpecialFrequencies.UseVisualStyleBackColor = false;
            this.bSpecialFrequencies.Click += new System.EventHandler(this.bSpecialFrequencies_Click);
            // 
            // bSectorsRanges
            // 
            resources.ApplyResources(this.bSectorsRanges, "bSectorsRanges");
            this.bSectorsRanges.BackColor = System.Drawing.Color.Transparent;
            this.bSectorsRanges.BackgroundImage = global::WndProject.Properties.Resources.SectorsRanges;
            this.bSectorsRanges.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.bSectorsRanges.FlatAppearance.BorderSize = 0;
            this.bSectorsRanges.Name = "bSectorsRanges";
            this.toolTip.SetToolTip(this.bSectorsRanges, resources.GetString("bSectorsRanges.ToolTip"));
            this.bSectorsRanges.UseVisualStyleBackColor = false;
            this.bSectorsRanges.Click += new System.EventHandler(this.bSectorsRanges_Click);
            // 
            // button11
            // 
            resources.ApplyResources(this.button11, "button11");
            this.button11.BackColor = System.Drawing.Color.Transparent;
            this.button11.BackgroundImage = global::WndProject.Properties.Resources.Setting;
            this.button11.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.button11.FlatAppearance.BorderSize = 0;
            this.button11.Name = "button11";
            this.toolTip.SetToolTip(this.button11, resources.GetString("button11.ToolTip"));
            this.button11.UseVisualStyleBackColor = false;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // pRecDev
            // 
            resources.ApplyResources(this.pRecDev, "pRecDev");
            this.pRecDev.BackColor = System.Drawing.SystemColors.Control;
            this.pRecDev.Controls.Add(this.controlARDV1);
            this.pRecDev.Controls.Add(this.controlAR6000);
            this.pRecDev.Controls.Add(this.controlARONE);
            this.pRecDev.Name = "pRecDev";
            this.toolTip.SetToolTip(this.pRecDev, resources.GetString("pRecDev.ToolTip"));
            // 
            // controlARDV1
            // 
            resources.ApplyResources(this.controlARDV1, "controlARDV1");
            this.controlARDV1.Name = "controlARDV1";
            this.toolTip.SetToolTip(this.controlARDV1, resources.GetString("controlARDV1.ToolTip"));
            // 
            // controlAR6000
            // 
            resources.ApplyResources(this.controlAR6000, "controlAR6000");
            this.controlAR6000.Name = "controlAR6000";
            this.toolTip.SetToolTip(this.controlAR6000, resources.GetString("controlAR6000.ToolTip"));
            // 
            // controlARONE
            // 
            resources.ApplyResources(this.controlARONE, "controlARONE");
            this.controlARONE.Name = "controlARONE";
            this.toolTip.SetToolTip(this.controlARONE, resources.GetString("controlARONE.ToolTip"));
            // 
            // splitter1
            // 
            resources.ApplyResources(this.splitter1, "splitter1");
            this.splitter1.BackColor = System.Drawing.SystemColors.ControlDark;
            this.splitter1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitter1.Name = "splitter1";
            this.splitter1.TabStop = false;
            this.toolTip.SetToolTip(this.splitter1, resources.GetString("splitter1.ToolTip"));
            // 
            // pTable
            // 
            resources.ApplyResources(this.pTable, "pTable");
            this.pTable.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.pTable.Controls.Add(this.tcTables);
            this.pTable.Name = "pTable";
            this.toolTip.SetToolTip(this.pTable, resources.GetString("pTable.ToolTip"));
            // 
            // tcTables
            // 
            resources.ApplyResources(this.tcTables, "tcTables");
            this.tcTables.Controls.Add(this.tpIRI_FRCh);
            this.tcTables.Controls.Add(this.tpIRI_FRCh_CR);
            this.tcTables.Controls.Add(this.tpIRI_FRCh_RP);
            this.tcTables.Controls.Add(this.tpIRI_PPRCh);
            this.tcTables.Controls.Add(this.tpIRI_PPRCh_CR);
            this.tcTables.Name = "tcTables";
            this.tcTables.SelectedIndex = 0;
            this.toolTip.SetToolTip(this.tcTables, resources.GetString("tcTables.ToolTip"));
            this.tcTables.SelectedIndexChanged += new System.EventHandler(this.tcTables_SelectedIndexChanged);
            // 
            // tpIRI_FRCh
            // 
            resources.ApplyResources(this.tpIRI_FRCh, "tpIRI_FRCh");
            this.tpIRI_FRCh.BackColor = System.Drawing.SystemColors.Control;
            this.tpIRI_FRCh.Controls.Add(this.table_IRI_FRCh);
            this.tpIRI_FRCh.Name = "tpIRI_FRCh";
            this.toolTip.SetToolTip(this.tpIRI_FRCh, resources.GetString("tpIRI_FRCh.ToolTip"));
            // 
            // table_IRI_FRCh
            // 
            resources.ApplyResources(this.table_IRI_FRCh, "table_IRI_FRCh");
            this.table_IRI_FRCh.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.table_IRI_FRCh.Name = "table_IRI_FRCh";
            this.toolTip.SetToolTip(this.table_IRI_FRCh, resources.GetString("table_IRI_FRCh.ToolTip"));
            // 
            // tpIRI_FRCh_CR
            // 
            resources.ApplyResources(this.tpIRI_FRCh_CR, "tpIRI_FRCh_CR");
            this.tpIRI_FRCh_CR.BackColor = System.Drawing.SystemColors.Control;
            this.tpIRI_FRCh_CR.Controls.Add(this.table_IRI_FRCh_CR);
            this.tpIRI_FRCh_CR.Name = "tpIRI_FRCh_CR";
            this.toolTip.SetToolTip(this.tpIRI_FRCh_CR, resources.GetString("tpIRI_FRCh_CR.ToolTip"));
            // 
            // table_IRI_FRCh_CR
            // 
            resources.ApplyResources(this.table_IRI_FRCh_CR, "table_IRI_FRCh_CR");
            this.table_IRI_FRCh_CR.BackColor = System.Drawing.SystemColors.Control;
            this.table_IRI_FRCh_CR.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.table_IRI_FRCh_CR.Name = "table_IRI_FRCh_CR";
            this.toolTip.SetToolTip(this.table_IRI_FRCh_CR, resources.GetString("table_IRI_FRCh_CR.ToolTip"));
            // 
            // tpIRI_FRCh_RP
            // 
            resources.ApplyResources(this.tpIRI_FRCh_RP, "tpIRI_FRCh_RP");
            this.tpIRI_FRCh_RP.BackColor = System.Drawing.SystemColors.Control;
            this.tpIRI_FRCh_RP.Controls.Add(this.table_IRI_FRCh_RP);
            this.tpIRI_FRCh_RP.Name = "tpIRI_FRCh_RP";
            this.toolTip.SetToolTip(this.tpIRI_FRCh_RP, resources.GetString("tpIRI_FRCh_RP.ToolTip"));
            // 
            // table_IRI_FRCh_RP
            // 
            resources.ApplyResources(this.table_IRI_FRCh_RP, "table_IRI_FRCh_RP");
            this.table_IRI_FRCh_RP.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.table_IRI_FRCh_RP.Name = "table_IRI_FRCh_RP";
            this.toolTip.SetToolTip(this.table_IRI_FRCh_RP, resources.GetString("table_IRI_FRCh_RP.ToolTip"));
            // 
            // tpIRI_PPRCh
            // 
            resources.ApplyResources(this.tpIRI_PPRCh, "tpIRI_PPRCh");
            this.tpIRI_PPRCh.BackColor = System.Drawing.SystemColors.Control;
            this.tpIRI_PPRCh.Controls.Add(this.table_IRI_PPRCh);
            this.tpIRI_PPRCh.Name = "tpIRI_PPRCh";
            this.toolTip.SetToolTip(this.tpIRI_PPRCh, resources.GetString("tpIRI_PPRCh.ToolTip"));
            // 
            // table_IRI_PPRCh
            // 
            resources.ApplyResources(this.table_IRI_PPRCh, "table_IRI_PPRCh");
            this.table_IRI_PPRCh.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.table_IRI_PPRCh.Name = "table_IRI_PPRCh";
            this.toolTip.SetToolTip(this.table_IRI_PPRCh, resources.GetString("table_IRI_PPRCh.ToolTip"));
            // 
            // tpIRI_PPRCh_CR
            // 
            resources.ApplyResources(this.tpIRI_PPRCh_CR, "tpIRI_PPRCh_CR");
            this.tpIRI_PPRCh_CR.BackColor = System.Drawing.SystemColors.Control;
            this.tpIRI_PPRCh_CR.Controls.Add(this.table_IRI_PPRCh_RP);
            this.tpIRI_PPRCh_CR.Name = "tpIRI_PPRCh_CR";
            this.toolTip.SetToolTip(this.tpIRI_PPRCh_CR, resources.GetString("tpIRI_PPRCh_CR.ToolTip"));
            // 
            // table_IRI_PPRCh_RP
            // 
            resources.ApplyResources(this.table_IRI_PPRCh_RP, "table_IRI_PPRCh_RP");
            this.table_IRI_PPRCh_RP.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.table_IRI_PPRCh_RP.Name = "table_IRI_PPRCh_RP";
            this.toolTip.SetToolTip(this.table_IRI_PPRCh_RP, resources.GetString("table_IRI_PPRCh_RP.ToolTip"));
            // 
            // splitter2
            // 
            resources.ApplyResources(this.splitter2, "splitter2");
            this.splitter2.BackColor = System.Drawing.SystemColors.ControlDark;
            this.splitter2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitter2.Name = "splitter2";
            this.splitter2.TabStop = false;
            this.toolTip.SetToolTip(this.splitter2, resources.GetString("splitter2.ToolTip"));
            // 
            // dfd
            // 
            resources.ApplyResources(this.dfd, "dfd");
            this.dfd.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.dfd.Controls.Add(this.panoramaControl1);
            this.dfd.Name = "dfd";
            this.toolTip.SetToolTip(this.dfd, resources.GetString("dfd.ToolTip"));
            // 
            // panoramaControl1
            // 
            resources.ApplyResources(this.panoramaControl1, "panoramaControl1");
            this.panoramaControl1.BackColor = System.Drawing.SystemColors.Control;
            this.panoramaControl1.Name = "panoramaControl1";
            this.toolTip.SetToolTip(this.panoramaControl1, resources.GetString("panoramaControl1.ToolTip"));
            // 
            // tmFlashFRQ
            // 
            this.tmFlashFRQ.Interval = 400;
            // 
            // tmValcoder
            // 
            this.tmValcoder.Interval = 5;
            // 
            // pStateServer
            // 
            resources.ApplyResources(this.pStateServer, "pStateServer");
            this.pStateServer.BackColor = System.Drawing.Color.Gainsboro;
            this.pStateServer.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pStateServer.Controls.Add(this.pFreqChannel);
            this.pStateServer.Controls.Add(this.pIndicateFCh);
            this.pStateServer.Name = "pStateServer";
            this.toolTip.SetToolTip(this.pStateServer, resources.GetString("pStateServer.ToolTip"));
            this.pStateServer.Paint += new System.Windows.Forms.PaintEventHandler(this.pStateServer_Paint);
            // 
            // pFreqChannel
            // 
            resources.ApplyResources(this.pFreqChannel, "pFreqChannel");
            this.pFreqChannel.Controls.Add(this.bFreqChannel);
            this.pFreqChannel.Controls.Add(this.lFreqChannel);
            this.pFreqChannel.Name = "pFreqChannel";
            this.toolTip.SetToolTip(this.pFreqChannel, resources.GetString("pFreqChannel.ToolTip"));
            // 
            // bFreqChannel
            // 
            resources.ApplyResources(this.bFreqChannel, "bFreqChannel");
            this.bFreqChannel.BackColor = System.Drawing.Color.Red;
            this.bFreqChannel.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.bFreqChannel.FlatAppearance.BorderSize = 0;
            this.bFreqChannel.Name = "bFreqChannel";
            this.toolTip.SetToolTip(this.bFreqChannel, resources.GetString("bFreqChannel.ToolTip"));
            this.bFreqChannel.UseVisualStyleBackColor = false;
            this.bFreqChannel.Click += new System.EventHandler(this.bFreqChannel_Click);
            // 
            // lFreqChannel
            // 
            resources.ApplyResources(this.lFreqChannel, "lFreqChannel");
            this.lFreqChannel.Name = "lFreqChannel";
            this.toolTip.SetToolTip(this.lFreqChannel, resources.GetString("lFreqChannel.ToolTip"));
            // 
            // pIndicateFCh
            // 
            resources.ApplyResources(this.pIndicateFCh, "pIndicateFCh");
            this.pIndicateFCh.Controls.Add(this.pbReadFCh);
            this.pIndicateFCh.Controls.Add(this.pbWriteFCh);
            this.pIndicateFCh.Name = "pIndicateFCh";
            this.toolTip.SetToolTip(this.pIndicateFCh, resources.GetString("pIndicateFCh.ToolTip"));
            // 
            // pbReadFCh
            // 
            resources.ApplyResources(this.pbReadFCh, "pbReadFCh");
            this.pbReadFCh.BackColor = System.Drawing.Color.Transparent;
            this.pbReadFCh.Name = "pbReadFCh";
            this.pbReadFCh.TabStop = false;
            this.toolTip.SetToolTip(this.pbReadFCh, resources.GetString("pbReadFCh.ToolTip"));
            // 
            // pbWriteFCh
            // 
            resources.ApplyResources(this.pbWriteFCh, "pbWriteFCh");
            this.pbWriteFCh.BackColor = System.Drawing.Color.Transparent;
            this.pbWriteFCh.Name = "pbWriteFCh";
            this.pbWriteFCh.TabStop = false;
            this.toolTip.SetToolTip(this.pbWriteFCh, resources.GetString("pbWriteFCh.ToolTip"));
            // 
            // pASP
            // 
            resources.ApplyResources(this.pASP, "pASP");
            this.pASP.BackColor = System.Drawing.Color.Gainsboro;
            this.pASP.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pASP.Controls.Add(this.panel2);
            this.pASP.Controls.Add(this.panel3);
            this.pASP.Name = "pASP";
            this.toolTip.SetToolTip(this.pASP, resources.GetString("pASP.ToolTip"));
            // 
            // panel2
            // 
            resources.ApplyResources(this.panel2, "panel2");
            this.panel2.Controls.Add(this.butClientAsp);
            this.panel2.Controls.Add(this.butServerAsp);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Name = "panel2";
            this.toolTip.SetToolTip(this.panel2, resources.GetString("panel2.ToolTip"));
            // 
            // butClientAsp
            // 
            resources.ApplyResources(this.butClientAsp, "butClientAsp");
            this.butClientAsp.BackColor = System.Drawing.Color.Red;
            this.butClientAsp.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.butClientAsp.FlatAppearance.BorderSize = 0;
            this.butClientAsp.Name = "butClientAsp";
            this.toolTip.SetToolTip(this.butClientAsp, resources.GetString("butClientAsp.ToolTip"));
            this.butClientAsp.UseVisualStyleBackColor = false;
            // 
            // butServerAsp
            // 
            resources.ApplyResources(this.butServerAsp, "butServerAsp");
            this.butServerAsp.BackColor = System.Drawing.Color.Red;
            this.butServerAsp.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.butServerAsp.FlatAppearance.BorderSize = 0;
            this.butServerAsp.Name = "butServerAsp";
            this.toolTip.SetToolTip(this.butServerAsp, resources.GetString("butServerAsp.ToolTip"));
            this.butServerAsp.UseVisualStyleBackColor = false;
            this.butServerAsp.Click += new System.EventHandler(this.butServerAsp_Click);
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            this.toolTip.SetToolTip(this.label1, resources.GetString("label1.ToolTip"));
            // 
            // panel3
            // 
            resources.ApplyResources(this.panel3, "panel3");
            this.panel3.Controls.Add(this.pictureBox1);
            this.panel3.Controls.Add(this.pictureBox2);
            this.panel3.Name = "panel3";
            this.toolTip.SetToolTip(this.panel3, resources.GetString("panel3.ToolTip"));
            // 
            // pictureBox1
            // 
            resources.ApplyResources(this.pictureBox1, "pictureBox1");
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.TabStop = false;
            this.toolTip.SetToolTip(this.pictureBox1, resources.GetString("pictureBox1.ToolTip"));
            // 
            // pictureBox2
            // 
            resources.ApplyResources(this.pictureBox2, "pictureBox2");
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.TabStop = false;
            this.toolTip.SetToolTip(this.pictureBox2, resources.GetString("pictureBox2.ToolTip"));
            // 
            // pState
            // 
            resources.ApplyResources(this.pState, "pState");
            this.pState.BackColor = System.Drawing.Color.Gainsboro;
            this.pState.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pState.Controls.Add(this.panel31);
            this.pState.Controls.Add(this.pADSB);
            this.pState.Controls.Add(this.panel19);
            this.pState.Controls.Add(this.panel7);
            this.pState.Controls.Add(this.pPU);
            this.pState.Controls.Add(this.pASP);
            this.pState.Controls.Add(this.pStateServer);
            this.pState.Name = "pState";
            this.toolTip.SetToolTip(this.pState, resources.GetString("pState.ToolTip"));
            // 
            // panel31
            // 
            resources.ApplyResources(this.panel31, "panel31");
            this.panel31.BackColor = System.Drawing.Color.Gainsboro;
            this.panel31.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel31.Controls.Add(this.panel32);
            this.panel31.Controls.Add(this.panel33);
            this.panel31.Name = "panel31";
            this.toolTip.SetToolTip(this.panel31, resources.GetString("panel31.ToolTip"));
            // 
            // panel32
            // 
            resources.ApplyResources(this.panel32, "panel32");
            this.panel32.Controls.Add(this.butClientHS);
            this.panel32.Controls.Add(this.butServerHS);
            this.panel32.Controls.Add(this.label15);
            this.panel32.Name = "panel32";
            this.toolTip.SetToolTip(this.panel32, resources.GetString("panel32.ToolTip"));
            // 
            // butClientHS
            // 
            resources.ApplyResources(this.butClientHS, "butClientHS");
            this.butClientHS.BackColor = System.Drawing.Color.Red;
            this.butClientHS.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.butClientHS.FlatAppearance.BorderSize = 0;
            this.butClientHS.Name = "butClientHS";
            this.toolTip.SetToolTip(this.butClientHS, resources.GetString("butClientHS.ToolTip"));
            this.butClientHS.UseVisualStyleBackColor = false;
            // 
            // butServerHS
            // 
            resources.ApplyResources(this.butServerHS, "butServerHS");
            this.butServerHS.BackColor = System.Drawing.Color.Red;
            this.butServerHS.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.butServerHS.FlatAppearance.BorderSize = 0;
            this.butServerHS.Name = "butServerHS";
            this.toolTip.SetToolTip(this.butServerHS, resources.GetString("butServerHS.ToolTip"));
            this.butServerHS.UseVisualStyleBackColor = false;
            this.butServerHS.Click += new System.EventHandler(this.ButHS_Click);
            // 
            // label15
            // 
            resources.ApplyResources(this.label15, "label15");
            this.label15.Name = "label15";
            this.toolTip.SetToolTip(this.label15, resources.GetString("label15.ToolTip"));
            // 
            // panel33
            // 
            resources.ApplyResources(this.panel33, "panel33");
            this.panel33.Controls.Add(this.pbReadHS);
            this.panel33.Controls.Add(this.pbWriteHS);
            this.panel33.Name = "panel33";
            this.toolTip.SetToolTip(this.panel33, resources.GetString("panel33.ToolTip"));
            // 
            // pbReadHS
            // 
            resources.ApplyResources(this.pbReadHS, "pbReadHS");
            this.pbReadHS.BackColor = System.Drawing.Color.Transparent;
            this.pbReadHS.Name = "pbReadHS";
            this.pbReadHS.TabStop = false;
            this.toolTip.SetToolTip(this.pbReadHS, resources.GetString("pbReadHS.ToolTip"));
            // 
            // pbWriteHS
            // 
            resources.ApplyResources(this.pbWriteHS, "pbWriteHS");
            this.pbWriteHS.BackColor = System.Drawing.Color.Transparent;
            this.pbWriteHS.Name = "pbWriteHS";
            this.pbWriteHS.TabStop = false;
            this.toolTip.SetToolTip(this.pbWriteHS, resources.GetString("pbWriteHS.ToolTip"));
            // 
            // pADSB
            // 
            resources.ApplyResources(this.pADSB, "pADSB");
            this.pADSB.BackColor = System.Drawing.Color.Gainsboro;
            this.pADSB.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pADSB.Controls.Add(this.panel25);
            this.pADSB.Controls.Add(this.panel26);
            this.pADSB.Name = "pADSB";
            this.toolTip.SetToolTip(this.pADSB, resources.GetString("pADSB.ToolTip"));
            // 
            // panel25
            // 
            resources.ApplyResources(this.panel25, "panel25");
            this.panel25.Controls.Add(this.lADSB);
            this.panel25.Controls.Add(this.bConnectModeS);
            this.panel25.Name = "panel25";
            this.toolTip.SetToolTip(this.panel25, resources.GetString("panel25.ToolTip"));
            // 
            // lADSB
            // 
            resources.ApplyResources(this.lADSB, "lADSB");
            this.lADSB.Name = "lADSB";
            this.toolTip.SetToolTip(this.lADSB, resources.GetString("lADSB.ToolTip"));
            // 
            // bConnectModeS
            // 
            resources.ApplyResources(this.bConnectModeS, "bConnectModeS");
            this.bConnectModeS.BackColor = System.Drawing.Color.Red;
            this.bConnectModeS.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.bConnectModeS.FlatAppearance.BorderSize = 0;
            this.bConnectModeS.Name = "bConnectModeS";
            this.toolTip.SetToolTip(this.bConnectModeS, resources.GetString("bConnectModeS.ToolTip"));
            this.bConnectModeS.UseVisualStyleBackColor = false;
            this.bConnectModeS.Click += new System.EventHandler(this.bConnectModeS_Click);
            // 
            // panel26
            // 
            resources.ApplyResources(this.panel26, "panel26");
            this.panel26.Controls.Add(this.pictureBox16);
            this.panel26.Controls.Add(this.pbReadModeS);
            this.panel26.Name = "panel26";
            this.toolTip.SetToolTip(this.panel26, resources.GetString("panel26.ToolTip"));
            // 
            // pictureBox16
            // 
            resources.ApplyResources(this.pictureBox16, "pictureBox16");
            this.pictureBox16.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox16.Name = "pictureBox16";
            this.pictureBox16.TabStop = false;
            this.toolTip.SetToolTip(this.pictureBox16, resources.GetString("pictureBox16.ToolTip"));
            // 
            // pbReadModeS
            // 
            resources.ApplyResources(this.pbReadModeS, "pbReadModeS");
            this.pbReadModeS.BackColor = System.Drawing.Color.Transparent;
            this.pbReadModeS.Name = "pbReadModeS";
            this.pbReadModeS.TabStop = false;
            this.toolTip.SetToolTip(this.pbReadModeS, resources.GetString("pbReadModeS.ToolTip"));
            // 
            // panel19
            // 
            resources.ApplyResources(this.panel19, "panel19");
            this.panel19.BackColor = System.Drawing.Color.Gainsboro;
            this.panel19.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel19.Controls.Add(this.panel20);
            this.panel19.Controls.Add(this.panel21);
            this.panel19.Name = "panel19";
            this.toolTip.SetToolTip(this.panel19, resources.GetString("panel19.ToolTip"));
            // 
            // panel20
            // 
            resources.ApplyResources(this.panel20, "panel20");
            this.panel20.Controls.Add(this.label7);
            this.panel20.Controls.Add(this.bAOR);
            this.panel20.Name = "panel20";
            this.toolTip.SetToolTip(this.panel20, resources.GetString("panel20.ToolTip"));
            // 
            // label7
            // 
            resources.ApplyResources(this.label7, "label7");
            this.label7.Name = "label7";
            this.toolTip.SetToolTip(this.label7, resources.GetString("label7.ToolTip"));
            // 
            // bAOR
            // 
            resources.ApplyResources(this.bAOR, "bAOR");
            this.bAOR.BackColor = System.Drawing.Color.Red;
            this.bAOR.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.bAOR.FlatAppearance.BorderSize = 0;
            this.bAOR.Name = "bAOR";
            this.toolTip.SetToolTip(this.bAOR, resources.GetString("bAOR.ToolTip"));
            this.bAOR.UseVisualStyleBackColor = false;
            this.bAOR.Click += new System.EventHandler(this.bAOR_Click);
            // 
            // panel21
            // 
            resources.ApplyResources(this.panel21, "panel21");
            this.panel21.Controls.Add(this.pictureBox13);
            this.panel21.Controls.Add(this.pictureBox14);
            this.panel21.Name = "panel21";
            this.toolTip.SetToolTip(this.panel21, resources.GetString("panel21.ToolTip"));
            // 
            // pictureBox13
            // 
            resources.ApplyResources(this.pictureBox13, "pictureBox13");
            this.pictureBox13.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox13.Name = "pictureBox13";
            this.pictureBox13.TabStop = false;
            this.toolTip.SetToolTip(this.pictureBox13, resources.GetString("pictureBox13.ToolTip"));
            // 
            // pictureBox14
            // 
            resources.ApplyResources(this.pictureBox14, "pictureBox14");
            this.pictureBox14.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox14.Name = "pictureBox14";
            this.pictureBox14.TabStop = false;
            this.toolTip.SetToolTip(this.pictureBox14, resources.GetString("pictureBox14.ToolTip"));
            // 
            // panel7
            // 
            resources.ApplyResources(this.panel7, "panel7");
            this.panel7.BackColor = System.Drawing.Color.Gainsboro;
            this.panel7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel7.Controls.Add(this.panel8);
            this.panel7.Controls.Add(this.panel9);
            this.panel7.Name = "panel7";
            this.toolTip.SetToolTip(this.panel7, resources.GetString("panel7.ToolTip"));
            // 
            // panel8
            // 
            resources.ApplyResources(this.panel8, "panel8");
            this.panel8.Controls.Add(this.buttonConnectFPS);
            this.panel8.Controls.Add(this.label3);
            this.panel8.Name = "panel8";
            this.toolTip.SetToolTip(this.panel8, resources.GetString("panel8.ToolTip"));
            // 
            // buttonConnectFPS
            // 
            resources.ApplyResources(this.buttonConnectFPS, "buttonConnectFPS");
            this.buttonConnectFPS.BackColor = System.Drawing.Color.Red;
            this.buttonConnectFPS.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.buttonConnectFPS.FlatAppearance.BorderSize = 0;
            this.buttonConnectFPS.Name = "buttonConnectFPS";
            this.toolTip.SetToolTip(this.buttonConnectFPS, resources.GetString("buttonConnectFPS.ToolTip"));
            this.buttonConnectFPS.UseVisualStyleBackColor = false;
            this.buttonConnectFPS.Click += new System.EventHandler(this.ClickToConnectFPS);
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            this.toolTip.SetToolTip(this.label3, resources.GetString("label3.ToolTip"));
            // 
            // panel9
            // 
            resources.ApplyResources(this.panel9, "panel9");
            this.panel9.Controls.Add(this.pictureBox5);
            this.panel9.Controls.Add(this.pictureBox6);
            this.panel9.Name = "panel9";
            this.toolTip.SetToolTip(this.panel9, resources.GetString("panel9.ToolTip"));
            // 
            // pictureBox5
            // 
            resources.ApplyResources(this.pictureBox5, "pictureBox5");
            this.pictureBox5.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.TabStop = false;
            this.toolTip.SetToolTip(this.pictureBox5, resources.GetString("pictureBox5.ToolTip"));
            // 
            // pictureBox6
            // 
            resources.ApplyResources(this.pictureBox6, "pictureBox6");
            this.pictureBox6.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.TabStop = false;
            this.toolTip.SetToolTip(this.pictureBox6, resources.GetString("pictureBox6.ToolTip"));
            // 
            // pPU
            // 
            resources.ApplyResources(this.pPU, "pPU");
            this.pPU.BackColor = System.Drawing.Color.Gainsboro;
            this.pPU.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pPU.Controls.Add(this.panel5);
            this.pPU.Controls.Add(this.panel6);
            this.pPU.Name = "pPU";
            this.toolTip.SetToolTip(this.pPU, resources.GetString("pPU.ToolTip"));
            // 
            // panel5
            // 
            resources.ApplyResources(this.panel5, "panel5");
            this.panel5.Controls.Add(this.btPU);
            this.panel5.Controls.Add(this.label2);
            this.panel5.Name = "panel5";
            this.toolTip.SetToolTip(this.panel5, resources.GetString("panel5.ToolTip"));
            // 
            // btPU
            // 
            resources.ApplyResources(this.btPU, "btPU");
            this.btPU.BackColor = System.Drawing.Color.Red;
            this.btPU.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btPU.FlatAppearance.BorderSize = 0;
            this.btPU.Name = "btPU";
            this.toolTip.SetToolTip(this.btPU, resources.GetString("btPU.ToolTip"));
            this.btPU.UseVisualStyleBackColor = false;
            this.btPU.Click += new System.EventHandler(this.btPU_Click);
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            this.toolTip.SetToolTip(this.label2, resources.GetString("label2.ToolTip"));
            // 
            // panel6
            // 
            resources.ApplyResources(this.panel6, "panel6");
            this.panel6.Controls.Add(this.pbReadPU);
            this.panel6.Controls.Add(this.pbWritePU);
            this.panel6.Name = "panel6";
            this.toolTip.SetToolTip(this.panel6, resources.GetString("panel6.ToolTip"));
            // 
            // pbReadPU
            // 
            resources.ApplyResources(this.pbReadPU, "pbReadPU");
            this.pbReadPU.BackColor = System.Drawing.Color.Transparent;
            this.pbReadPU.Name = "pbReadPU";
            this.pbReadPU.TabStop = false;
            this.toolTip.SetToolTip(this.pbReadPU, resources.GetString("pbReadPU.ToolTip"));
            // 
            // pbWritePU
            // 
            resources.ApplyResources(this.pbWritePU, "pbWritePU");
            this.pbWritePU.BackColor = System.Drawing.Color.Transparent;
            this.pbWritePU.Name = "pbWritePU";
            this.pbWritePU.TabStop = false;
            this.toolTip.SetToolTip(this.pbWritePU, resources.GetString("pbWritePU.ToolTip"));
            // 
            // tm500
            // 
            this.tm500.Enabled = true;
            this.tm500.Interval = 1000;
            // 
            // MainWnd
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.dfd);
            this.Controls.Add(this.splitter2);
            this.Controls.Add(this.pTable);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.pRecDev);
            this.Controls.Add(this.pTask);
            this.Controls.Add(this.pState);
            this.Controls.Add(this.pManage);
            this.Name = "MainWnd";
            this.toolTip.SetToolTip(this, resources.GetString("$this.ToolTip"));
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainWnd_FormClosing);
            this.Load += new System.EventHandler(this.MainWnd_Load);
            this.pManage.ResumeLayout(false);
            this.panel29.ResumeLayout(false);
            this.panel29.PerformLayout();
            this.panel28.ResumeLayout(false);
            this.panel28.PerformLayout();
            this.panel27.ResumeLayout(false);
            this.panel27.PerformLayout();
            this.panel24.ResumeLayout(false);
            this.panel24.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).EndInit();
            this.pTask.ResumeLayout(false);
            this.pRecDev.ResumeLayout(false);
            this.pTable.ResumeLayout(false);
            this.tcTables.ResumeLayout(false);
            this.tpIRI_FRCh.ResumeLayout(false);
            this.tpIRI_FRCh_CR.ResumeLayout(false);
            this.tpIRI_FRCh_RP.ResumeLayout(false);
            this.tpIRI_PPRCh.ResumeLayout(false);
            this.tpIRI_PPRCh_CR.ResumeLayout(false);
            this.dfd.ResumeLayout(false);
            this.pStateServer.ResumeLayout(false);
            this.pFreqChannel.ResumeLayout(false);
            this.pFreqChannel.PerformLayout();
            this.pIndicateFCh.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbReadFCh)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbWriteFCh)).EndInit();
            this.pASP.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.pState.ResumeLayout(false);
            this.panel31.ResumeLayout(false);
            this.panel32.ResumeLayout(false);
            this.panel32.PerformLayout();
            this.panel33.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbReadHS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbWriteHS)).EndInit();
            this.pADSB.ResumeLayout(false);
            this.panel25.ResumeLayout(false);
            this.panel25.PerformLayout();
            this.panel26.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbReadModeS)).EndInit();
            this.panel19.ResumeLayout(false);
            this.panel20.ResumeLayout(false);
            this.panel20.PerformLayout();
            this.panel21.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).EndInit();
            this.panel7.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.panel9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            this.pPU.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbReadPU)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbWritePU)).EndInit();
            this.ResumeLayout(false);

        }


        #endregion

        private System.Windows.Forms.Panel pManage;
        private System.Windows.Forms.Panel pTask;
        private System.Windows.Forms.Panel pRecDev;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.Panel pTable;
        private System.Windows.Forms.Splitter splitter2;
        private System.Windows.Forms.Panel dfd;
        private System.Windows.Forms.PictureBox pictureBox15;
        private System.Windows.Forms.Button bMap;
        private System.Windows.Forms.Button MessBut;
        private System.Windows.Forms.Button bModeS;
        private System.Windows.Forms.Button FogNut;
        private System.Windows.Forms.Button button17;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.Button bSpecialFrequencies;
        private System.Windows.Forms.Button bSectorsRanges;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button bRegimeStop;
        private System.Windows.Forms.Button bRegimeRecon;
        private System.Windows.Forms.Button bRegimeSuppr;
        private System.Windows.Forms.Label lOwnNumber;
        //private Table_IRI_FRCh_RP.table_IRI_FRCh_RP table_IRI_FRCh_RP1;
        private System.Windows.Forms.Timer tmFlashFRQ;
        private System.Windows.Forms.Timer tmValcoder;
        private System.Windows.Forms.Label lOperator;
        private System.Windows.Forms.Label lOwnAddressPC;
        private System.Windows.Forms.Label lOwnRole;
        private PanoramaLibrary.PanoramaControl panoramaControl1;
        private System.Windows.Forms.TabControl tcTables;
        private System.Windows.Forms.TabPage tpIRI_FRCh;
        private System.Windows.Forms.TabPage tpIRI_FRCh_CR;
        private System.Windows.Forms.TabPage tpIRI_FRCh_RP;
        private System.Windows.Forms.TabPage tpIRI_PPRCh;
        private System.Windows.Forms.TabPage tpIRI_PPRCh_CR;
        private Table_IRI_FRCh.table_IRI_FRCh table_IRI_FRCh;
        private Table_IRI_FRCh_CR.table_IRI_FRCh_CR table_IRI_FRCh_CR;
        private Table_IRI_FRCh_RP.table_IRI_FRCh_RP table_IRI_FRCh_RP;
        private Table_IRI_PPRCh.table_IRI_PPRCh table_IRI_PPRCh;
        private Table_IRI_PPRCh_RP.table_IRI_PPRCh_RP table_IRI_PPRCh_RP;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.Timer tmBaraban;
        private System.Windows.Forms.Timer tm500;
        private System.Windows.Forms.Panel panel29;
        private System.Windows.Forms.Panel panel28;
        private System.Windows.Forms.Panel panel27;
        private System.Windows.Forms.Panel panel24;
        private System.Windows.Forms.Button bTechnicalAnalysis;
        private System.Windows.Forms.Panel pStateServer;
        private System.Windows.Forms.Panel pFreqChannel;
        private System.Windows.Forms.Button bFreqChannel;
        private System.Windows.Forms.Label lFreqChannel;
        private System.Windows.Forms.Panel pIndicateFCh;
        private System.Windows.Forms.PictureBox pbReadFCh;
        private System.Windows.Forms.PictureBox pbWriteFCh;
        private System.Windows.Forms.Panel pASP;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button butClientAsp;
        private System.Windows.Forms.Button butServerAsp;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Panel pState;
        private System.Windows.Forms.Panel panel31;
        private System.Windows.Forms.Panel panel32;
        private System.Windows.Forms.Button butClientHS;
        private System.Windows.Forms.Button butServerHS;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Panel panel33;
        private System.Windows.Forms.PictureBox pbReadHS;
        private System.Windows.Forms.PictureBox pbWriteHS;
        private System.Windows.Forms.Panel pADSB;
        private System.Windows.Forms.Panel panel25;
        public System.Windows.Forms.Button bConnectModeS;
        private System.Windows.Forms.Label lADSB;
        private System.Windows.Forms.Panel panel26;
        private System.Windows.Forms.PictureBox pictureBox16;
        public System.Windows.Forms.PictureBox pbReadModeS;
        private System.Windows.Forms.Panel panel19;
        private System.Windows.Forms.Panel panel20;
        private System.Windows.Forms.Button bAOR;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel panel21;
        private System.Windows.Forms.PictureBox pictureBox13;
        private System.Windows.Forms.PictureBox pictureBox14;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Button buttonConnectFPS;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.Panel pPU;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Button btPU;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.PictureBox pbReadPU;
        private System.Windows.Forms.PictureBox pbWritePU;
        private Control_AR_DV1.UserControl1 controlARDV1;
        private ControlAR6000.controlAR6000 controlAR6000;
        private ControlARONE.ControlARONE controlARONE;





    }
}

