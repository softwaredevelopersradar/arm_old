﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NAudio;
using NAudio.Wave;
using NAudio.FileFormats;
using NAudio.CoreAudioApi;
using System.IO;
using System.IO.Ports;
using MathNet.Numerics.IntegralTransforms;
using MathNet.Numerics.Signals;
using System.Threading;

public class AR6000DLL
{
    public struct AOR
    {
        public string s;
        public Int64 frequency;
        public int Mode;
        public int Bandwidth;
        public int HighPassFilter;
        public int LowPassFilter;
        public int AutomaticGainControl;
        public int AudioGain;
        public int Attenuator;
        public int Amplifier;
        public int LevelSquelch;
        public int NoiseSquelch;
        public int AGC;
        public int AFGain;
        public int ManualGain;
        public int ManualRFGain;
        public int IFGain;
        public int SquelchSelect;
        public int SignalLevel;
        public int AutoSignalLevel;
        public int AutoBackLit;
        public int BackLit_OnOff;
        public int BackLitDimmer;
        public int LCDContrast;
        public int BeepLevel;
        public int SpeakerSelect;
        public int ExternalSpeacker;
        public int DelayTime;
        public int FreeScan;
        public int SignalMeterDisplay;
        public int DuplexMode;
        //public int DuplexFrequencymm;
        public int[] DuplexFrequency;
        public int DuplexFrequencyN;
        public int SignalLevelUnit_dBmV;
        public int SignalLevelUnit_dBm;
        // public int[] SearchDataSetting;
        //public int PassFrequency;
        public int[] PassFrequencyList;
        public int SelectMemory_OnOff;
        public int SelectMemoryList;
        // public int TransferCurrentSearchDataToVF0;
        // public int MemoryDataSetting;
        // public int[] MemoryDataList;
        // public int SelectPriorityChanel;
        // public int IFOutSelection;
        public int DEemphasis;
        public int BFOFreq;
    }

    public AOR ArOne = new AOR();
    private SerialPort _port;
    private Thread thrRead;
    byte ArOne_SignalLevel;

    public delegate void ByteEventHandler();
    public delegate void ByteEventHandler1(string data);
    public delegate void ConnectEventHandler();
    public event ByteEventHandler OnReadByte;
    //public event ByteEventHandler OnDecodedByte;
    public event ByteEventHandler OnWriteByte;
    public event ConnectEventHandler OnConnectPort;
    public event ConnectEventHandler OnDisconnectPort;
    public event ByteEventHandler1 OnDecodedError;
    public event ByteEventHandler OnDecodedFrq;
    public event ByteEventHandler OnDecodedMode;
    public event ByteEventHandler OnDecodedHPF;
    public event ByteEventHandler OnDecodedBW;
    public event ByteEventHandler OnDecodedLPF;
    public event ByteEventHandler OnDecodedAGC;
    public event ByteEventHandler OnDecodedAudioGain;
    public event ByteEventHandler OnDecodedDEEmphasis;
    //public event ByteEventHandler OnDecodedAMpl;
    public event ByteEventHandler OnDecodedATT;
    //public event ByteEventHandler OnDecodedBFOFreq;
    public event ByteEventHandler OnDecodedNoiseSquelch;
    public event ByteEventHandler OnDecodedLevelSquelch;
    //public event ByteEventHandler OnDecodedAFGain;
    public event ByteEventHandler OnDecodedManualGain;
    public event ByteEventHandler OnDecodedRFGain;
    //public event ByteEventHandler OnDecodedIFGain;
    //public event ByteEventHandler OnDecodedSquelchSelect;
    //public event ByteEventHandler OnDecodedAutoSignalLevel;
    //public event ByteEventHandler OnDecodedAutoBackLit;
    public event ByteEventHandler OnDecodedBackLit_OnOff;
    //public event ByteEventHandler OnDecodedBackLitDimmer;
    //public event ByteEventHandler OnDecodedLCDContrast;
    //public event ByteEventHandler OnDecodedBeepLevel;
    //public event ByteEventHandler OnDecodedSpeakerSelect;
    //public event ByteEventHandler OnDecodedExternalSpeacker;
    //public event ByteEventHandler OnDecodedFreeScan;
    //public event ByteEventHandler OnDecodedSignalMeterDisplay;
    //public event ByteEventHandler OnDecodedDuplexMode;
    //public event ByteEventHandler OnDecodedDuplexFrequency;
    //public event ByteEventHandler OnDecodedSignalLevelUnit_dBmV;
    //public event ByteEventHandler OnDecodedSignalLevelUnit_dBm;
    //public event ByteEventHandler OnDecodedSelectMemory_OnOff;
    //public event ByteEventHandler OnDecodedSelectMemoryList;
    public event ByteEventHandler OnDecodedSignalLevel;


    public bool SendToArone(string message)
    {
        try
        {
            message += "\x0D\x0A";
            _port.WriteLine(message);
            return true;
        }
        catch (Exception) { return false; }
    }
    public bool TurnON()
    {
        try
        {
            string message = "X\x0D\x0A";
            _port.WriteLine(message);
            Thread.Sleep(30);
            return true;
        }
        catch (Exception) { return false; }
    }
    public bool TurnOFF()
    {
        try
        {
            string message = "QP\x0D\x0A";
            _port.WriteLine(message);
            Thread.Sleep(40);
            return true;
        }
        catch (Exception) { return false; }
    }
    public bool AutoModeSet(double AuM)
    {
        try
        {
            if ((AuM > 1) || (AuM < 0)) return false;
            string message = "AU" + AuM + "\x0D\x0A";
            _port.WriteLine(message);
            Thread.Sleep(40);
            return true;
        }
        catch (Exception) { return false; }
    }
    public bool AutoModeGet()
    {
        try
        {
            _port.WriteLine("AU\x0D\x0A");
            Thread.Sleep(20);
            return true;
        }
        catch (Exception) { return false; }
    }
    public bool FrequencySet(double FrqMhz)
    {
        try
        {
            if ((FrqMhz < 0.09) || (FrqMhz > 6000)) return false;
            Int64 FrqHz = (Int64)((FrqMhz * 1000000) / 1);
            string frq = FrqHz.ToString().PadLeft(10, '0');
            string message = "RF" + frq + "\x0D\x0A";
            _port.WriteLine(message);
            Thread.Sleep(40);
            return true;
        }
        catch (Exception) { return false; }
    }
    public bool FrequencyGet()
    {
        try
        {
            _port.WriteLine("RF\x0D\x0A");
            Thread.Sleep(20);
            return true;
        }
        catch (Exception) { return false; }
    }
    public bool FrequencySet(string freq_MHz)//MHz
    {
        try
        {
            long frql;
            double frqd;
            if (!double.TryParse(freq_MHz, out frqd)) { frqd = 0; return false; }
            frqd = 1000000 * frqd;
            if ((frqd > 3300000000) || (frqd < 10000)) { return false; }
            frql = (long)frqd;
            freq_MHz = frql.ToString();
            freq_MHz = freq_MHz.PadLeft(10, '0');
            string message = "RF" + freq_MHz + "\x0D\x0A";
            _port.WriteLine(message);
            Thread.Sleep(40);
            return true;
        }
        catch (Exception) { return false; }
    }
    public bool ModeSet(int Mode)
    {
        try
        {
            if ((Mode > 35) || (Mode < 0)) return false;
            if ((Mode > 8) && (Mode < 21)) return false;
            string message = "MD" + Mode.ToString() + "\x0D\x0A";
            _port.WriteLine(message);
            Thread.Sleep(20);
            return true;
        }
        catch (Exception) { return false; }
    }
    public bool ModeGet()
    {
        try
        {
            _port.WriteLine("MD\x0D\x0A");
            Thread.Sleep(20);
            return true;
        }
        catch (Exception) { return false; }
    }
    public bool BandWidthSet(int BW)
    {
        try
        {
            if ((BW > 9) || (BW < 0)) return false;
            string message = "BW0" + BW.ToString() + "\x0D\x0A";
            _port.WriteLine(message);
            Thread.Sleep(20);
            return true;
        }
        catch (Exception) { return false; }
    }
    public bool BandwidthGet()
    {
        try
        {
            _port.WriteLine("BW\x0D\x0A");
            Thread.Sleep(20);
            return true;
        }
        catch (Exception) { return false; }
    }
    public bool HighPassFiltrSet(char HPF)
    {
        try
        {
            if ((HPF.ToString() != "0") && (HPF.ToString() != "1") && (HPF.ToString() != "2")) { return false; }
            string message = "HP" + HPF.ToString() + "\x0D\x0A";
            _port.WriteLine(message);
            Thread.Sleep(20);
            return true;
        }
        catch (Exception) { return false; }
    }
    public bool HighPassFiltrGet()
    {
        try
        {
            _port.WriteLine("HP\x0D\x0A");
            Thread.Sleep(20);
            return true;
        }
        catch (Exception) { return false; }
    }
    public bool LowPassFiltrSet(char LPF)
    {
        try
        {
            if ((LPF.ToString() != "0") && (LPF.ToString() != "1") && (LPF.ToString() != "2")) { return false; }
            string message = "LP" + LPF.ToString() + "\x0D\x0A";
            _port.WriteLine(message);
            Thread.Sleep(20);
            return true;
        }
        catch (Exception) { return false; }
    }
    public bool LowPassFiltrGet()
    {
        try
        {
            _port.WriteLine("LP\x0D\x0A");
            Thread.Sleep(20);
            return true;
        }
        catch (Exception) { return false; }
    }
    public bool AudioGainSet(int AG)
    {
        try
        {
            if ((AG>255)||(AG<0)) { return false; }
            string message = "VL" + AG.ToString().PadLeft(3,'0') + "\x0D\x0A";
            _port.WriteLine(message);
            Thread.Sleep(20);
            return true;
        }
        catch (Exception) { return false; }
    }
    public bool AudioGainGet()
    {
        try
        {
            _port.WriteLine("VL\x0D\x0A");
            Thread.Sleep(20);
            return true;
        }
        catch (Exception) { return false; }
    }
    public bool AutomaticGainControlSet(int AGC)
    {
        string message;
        try
        {
            if ((AGC.ToString() != "0") && (AGC.ToString() != "1") && (AGC.ToString() != "2") && (AGC.ToString() != "3")) { return false; }

            if (AGC == 3) 
                message = "ACF" + "\x0D\x0A";
            else
                message = "AC" + AGC.ToString() + "\x0D\x0A";
            _port.WriteLine(message);
            Thread.Sleep(20);
            return true;
        }
        catch (Exception) { return false; }
    }
    public bool AutomaticGainControlGet()
    {
        try
        {
            _port.WriteLine("AC\x0D\x0A");
            Thread.Sleep(20);
            return true;
        }
        catch (Exception) { return false; }
    }
    public bool DE_EmphasisSet(int DE_E)
    {
        try
        {
            if ((DE_E > 1) || (DE_E < 0)) return false;
            string message = "EN" + DE_E.ToString() + "\x0D\x0A";
            _port.WriteLine(message);
            Thread.Sleep(20);
            return true;
        }
        catch (Exception) { return false; }
    }
    public bool DE_EmphasisGet()
    {
        try
        {
            _port.WriteLine("EN\x0D\x0A");
            Thread.Sleep(20);
            return true;
        }
        catch (Exception) { return false; }
    }
    public bool AttenuatorSet(int Att)
    {
        try
        {
            if ((Att > 4) || (Att < 0)) return false;
            string message = "AT" + Att.ToString() + "\x0D\x0A";
            _port.WriteLine(message);
            Thread.Sleep(20);
            return true;
        }
        catch (Exception) { return false; }
    }
    public bool AttenuatorGet()
    {
        try
        {
            _port.WriteLine("AT\x0D\x0A");
            Thread.Sleep(20);
            return true;
        }
        catch (Exception) { return false; }
    }
    
    public bool LevelSQuelchSet(int LSQ)
    {
        try
        {
            if ((LSQ > 255) || (LSQ < 0)) return false;
            string message = "RQ" + LSQ.ToString().PadLeft(3, '0') + "\x0D\x0A";
            _port.WriteLine(message);
            Thread.Sleep(20);
            return true;
        }
        catch (Exception) { return false; }
    }
    public bool LevelSQuelchGet()
    {
        try
        {
            _port.WriteLine("RQ\x0D\x0A");
            Thread.Sleep(20);
            return true;
        }
        catch (Exception) { return false; }
    }
    public bool NoiseSQuelchSet(int LSQ)
    {
        try
        {
            if ((LSQ > 255) || (LSQ < 0)) return false;
            string message = "NQ" + LSQ.ToString().PadLeft(3, '0') + "\x0D\x0A";
            _port.WriteLine(message);
            Thread.Sleep(20);
            return true;
        }
        catch (Exception) { return false; }
    }
    public bool NioseSQGet()
    {
        try
        {
            _port.WriteLine("NQ\x0D\x0A");
            Thread.Sleep(20);
            return true;
        }
        catch (Exception) { return false; }
    }
    public bool NoiseSQuelchOnOffSet(int NSQ00)
    {
        try
        {
            if ((NSQ00 > 1) || (NSQ00 < 0)) return false;
            string message = "NE" + NSQ00.ToString() + "\x0D\x0A";
            _port.WriteLine(message);
            Thread.Sleep(20);
            return true;
        }
        catch (Exception) { return false; }
    }
    public bool NioseSQuelchOnOffGet()
    {
        try
        {
            _port.WriteLine("NE\x0D\x0A");
            Thread.Sleep(20);
            return true;
        }
        catch (Exception) { return false; }
    }
    //public bool AFGainSet(int AG)
    //{
    //    try
    //    {
    //        if ((AG > 255) || (AG < 0)) return false;
    //        string message = "AG" + AG.ToString().PadLeft(3, '0') + "\x0D\x0A";
    //        _port.WriteLine(message);
    //        Thread.Sleep(20);
    //        return true;
    //    }
    //    catch (Exception) { return false; }
    //}
    //public bool AFGainGet()
    //{
    //    try
    //    {
    //        _port.WriteLine("AG\x0D\x0A");
    //        Thread.Sleep(20);
    //        return true;
    //    }
    //    catch (Exception) { return false; }
    //}
    //public bool ManualGainSet(int MG, int AGC)//10.7MHz AGC
    //{
    //    try
    //    {
    //        if ((MG > 255) || (MG < 0) || (AGC != 0)) return false;
    //        string message = "MG" + MG.ToString().PadLeft(3, '0') + "\x0D\x0A";
    //        _port.WriteLine(message);
    //        Thread.Sleep(20);
    //        return true;
    //    }
    //    catch (Exception) { return false; }
    //}
    //public bool ManualGainGet()
    //{
    //    try
    //    {
    //        _port.WriteLine("MG\x0D\x0A");
    //        Thread.Sleep(20);
    //        return true;
    //    }
    //    catch (Exception) { return false; }
    //}
    public bool RFGainSet(int RFG)
    {
        try
        {
            if ((RFG > 110) || (RFG < 0)) return false;
            string message = "RG" + RFG.ToString().PadLeft(3, '0') + "\x0D\x0A";
            _port.WriteLine(message);
            Thread.Sleep(20);
            return true;
        }
        catch (Exception) { return false; }
    }
    public bool RFGainGet()
    {
        try
        {
            _port.WriteLine("RG\x0D\x0A");
            Thread.Sleep(20);
            return true;
        }
        catch (Exception) { return false; }
    }
    //public bool IFGainSet(int IFG)
    //{
    //    try
    //    {
    //        if ((IFG > 255) || (IFG < 0)) return false;
    //        string message = "IG" + IFG.ToString().PadLeft(3, '0') + "\x0D\x0A";
    //        _port.WriteLine(message);
    //        Thread.Sleep(20);
    //        return true;
    //    }
    //    catch (Exception) { return false; }
    //}
    //public bool IFGainGet()
    //{
    //    try
    //    {
    //        _port.WriteLine("IG\x0D\x0A");
    //        Thread.Sleep(20);
    //        return true;
    //    }
    //    catch (Exception) { return false; }
    //}
    //public bool SquelchSelectSet(int SS)
    //{
    //    try
    //    {
    //        if ((SS != 0) || (SS != 1)) return false;
    //        string message = "SQ" + SS.ToString().PadLeft(3, '0') + "\x0D\x0A";
    //        _port.WriteLine(message);
    //        Thread.Sleep(20);
    //        return true;
    //    }
    //    catch (Exception) { return false; }
    //}
    //public bool SquelchSelectGet()
    //{
    //    try
    //    {
    //        _port.WriteLine("SQ\x0D\x0A");
    //        Thread.Sleep(20);
    //        return true;
    //    }
    //    catch (Exception) { return false; }
    //}
    public bool SignalLevelGet()
    {
        try
        {
            _port.WriteLine("LMX\x0D\x0A");
            Thread.Sleep(20);
            return true;
        }
        catch (Exception) { return false; }
    }
    //public bool AutoSignalLevelSet(int ASL)
    //{
    //    try
    //    {
    //        if ((ASL > 1) || (ASL < 0)) return false;
    //        string message = "LC" + ASL.ToString() + "\x0D\x0A";
    //        _port.WriteLine(message);
    //        Thread.Sleep(20);
    //        return true;
    //    }
    //    catch (Exception) { return false; }
    //}
    //public bool AutoSignalLevelGet()
    //{
    //    try
    //    {
    //        _port.WriteLine("LC\x0D\x0A");
    //        Thread.Sleep(20);
    //        return true;
    //    }
    //    catch (Exception) { return false; }
    //}
    public bool AutoSignalLevelReoprtSet(int ASL)
    {
        try
        {
            if ((ASL > 6000) || (ASL < 0)) return false;
            string message = "LT" + ASL.ToString() + "\x0D\x0A";
            _port.WriteLine(message);
            Thread.Sleep(20);
            return true;
        }
        catch (Exception) { return false; }
    }
    //public bool AutoBackLitSet(int ABS)
    //{
    //    try
    //    {
    //        if ((ABS > 2) || (ABS < 0)) return false;
    //        string message = "LA" + ABS.ToString() + "\x0D\x0A";
    //        _port.WriteLine(message);
    //        Thread.Sleep(20);
    //        return true;
    //    }
    //    catch (Exception) { return false; }
    //}
    //public bool AutoBackLitGet()
    //{
    //    try
    //    {
    //        _port.WriteLine("LA\x0D\x0A");
    //        Thread.Sleep(20);
    //        return true;
    //    }
    //    catch (Exception) { return false; }
    //}
    public bool LCDBackLitSet(int ABS)
    {
        try
        {
            if ((ABS > 1) || (ABS < 0) ) return false;
            string message = "BL" + ABS.ToString() + "\x0D\x0A";
            _port.WriteLine(message);
            Thread.Sleep(20);
            return true;
        }
        catch (Exception) { return false; }
    }
    public bool LCDBackLitGet()
    {
        try
        {
            _port.WriteLine("BL\x0D\x0A");
            Thread.Sleep(20);
            return true;
        }
        catch (Exception) { return false; }
    }
    //public bool BackLitDimmerSet(int BD)
    //{
    //    try
    //    {
    //        if ((BD > 1) || (BD < 0)) return false;
    //        string message = "LD" + BD.ToString() + "\x0D\x0A";
    //        _port.WriteLine(message);
    //        Thread.Sleep(20);
    //        return true;
    //    }
    //    catch (Exception) { return false; }
    //}
    //public bool BackLitDimmerGet()
    //{
    //    try
    //    {
    //        _port.WriteLine("LD\x0D\x0A");
    //        Thread.Sleep(20);
    //        return true;
    //    }
    //    catch (Exception) { return false; }
    //}
    //public bool LCDContrastSet(int LCDC)
    //{
    //    try
    //    {
    //        if ((LCDC > 31) || (LCDC < 0)) return false;
    //        string message = "LV" + LCDC.ToString() + "\x0D\x0A";
    //        _port.WriteLine(message);
    //        Thread.Sleep(20);
    //        return true;
    //    }
    //    catch (Exception) { return false; }
    //}
    //public bool LCDContrastGet()
    //{
    //    try
    //    {
    //        _port.WriteLine("LV\x0D\x0A");
    //        Thread.Sleep(20);
    //        return true;
    //    }
    //    catch (Exception) { return false; }
    //}
    //public bool BeepLevelSet(int BLev)
    //{
    //    try
    //    {
    //        if ((BLev > 9) || (BLev < 0)) return false;
    //        string message = "BV" + BLev.ToString() + "\x0D\x0A";
    //        _port.WriteLine(message);
    //        Thread.Sleep(20);
    //        return true;
    //    }
    //    catch (Exception) { return false; }
    //}
    //public bool BeepLevelGet()
    //{
    //    try
    //    {
    //        _port.WriteLine("BV\x0D\x0A");
    //        Thread.Sleep(20);
    //        return true;
    //    }
    //    catch (Exception) { return false; }
    //}
    //public bool SpeackerSelectSet(int Speacker)
    //{
    //    try
    //    {
    //        if ((Speacker > 3) || (Speacker < 0)) return false;
    //        string message = "SO" + Speacker.ToString() + "\x0D\x0A";
    //        _port.WriteLine(message);
    //        Thread.Sleep(20);
    //        return true;
    //    }
    //    catch (Exception) { return false; }
    //}
    //public bool SpeackerSelectGet()
    //{
    //    try
    //    {
    //        _port.WriteLine("SO\x0D\x0A");
    //        Thread.Sleep(20);
    //        return true;
    //    }
    //    catch (Exception) { return false; }
    //}
    //public bool ExternalSpeackerSet(int ExtSpk)
    //{
    //    try
    //    {
    //        if ((ExtSpk > 1) || (ExtSpk < 0)) return false;
    //        string message = "PO" + ExtSpk.ToString() + "\x0D\x0A";
    //        _port.WriteLine(message);
    //        Thread.Sleep(20);
    //        return true;
    //    }
    //    catch (Exception) { return false; }
    //}
    //public bool ExternalSpeackerGet()
    //{
    //    try
    //    {
    //        _port.WriteLine("PO\x0D\x0A");
    //        Thread.Sleep(20);
    //        return true;
    //    }
    //    catch (Exception) { return false; }
    //}


    //public bool DelayTime_ScanDelaySet(double DT)
    //{
    //    try
    //    {
    //        if ((DT > 9.9) || (DT < 0)) return false;
    //        string DTStr = DT.ToString();
    //        if (DTStr.Length >= 2) DTStr = DTStr.Substring(0, 3); else DTStr = DTStr + ".0";
    //        string message = "DD" + DTStr + "\x0D\x0A";
    //        _port.WriteLine(message);
    //        Thread.Sleep(20);
    //        return true;
    //    }
    //    catch (Exception) { return false; }
    //}
    //public bool DelayTime_ScanDelayGet()
    //{
    //    try
    //    {
    //        _port.WriteLine("DD\x0D\x0A");
    //        Thread.Sleep(20);
    //        return true;
    //    }
    //    catch (Exception) { return false; }
    //}
    //public bool FreeScanSet(double FSTime)
    //{
    //    try
    //    {
    //        if ((FSTime > 9.9) || (FSTime < 0)) return false;
    //        string DTStr = FSTime.ToString();
    //        if (DTStr.Length >= 2) DTStr = DTStr.Substring(0, 3); else DTStr = DTStr + ".0";
    //        string message = "SP" + DTStr + "\x0D\x0A";
    //        _port.WriteLine(message);
    //        Thread.Sleep(20);
    //        return true;
    //    }
    //    catch (Exception) { return false; }
    //}
    //public bool FreeScanGet()
    //{
    //    try
    //    {
    //        _port.WriteLine("SP\x0D\x0A");
    //        Thread.Sleep(20);
    //        return true;
    //    }
    //    catch (Exception) { return false; }
    //}
    //public bool SignalMeterDisplaySet(int SMDMode)
    //{
    //    try
    //    {
    //        if ((SMDMode > 2) || (SMDMode < 0)) return false;
    //        string message = "SF" + SMDMode.ToString() + "\x0D\x0A";
    //        _port.WriteLine(message);
    //        Thread.Sleep(20);
    //        return true;
    //    }
    //    catch (Exception) { return false; }
    //}
    //public bool SignalMeterDisplayGet()
    //{
    //    try
    //    {
    //        _port.WriteLine("SF\x0D\x0A");
    //        Thread.Sleep(20);
    //        return true;
    //    }
    //    catch (Exception) { return false; }
    //}
    //public bool DuplexModeSet(int DM, string OfsetDirectionPlusOrMinus)
    //{
    //    try
    //    {
    //        if ((OfsetDirectionPlusOrMinus != "+") && (OfsetDirectionPlusOrMinus != "-")) return false;
    //        if ((DM > 47) || (DM < 0)) return false;
    //        string message = "OF" + DM.ToString() + "\x0D\x0A";
    //        _port.WriteLine(message);
    //        Thread.Sleep(20);
    //        return true;
    //    }
    //    catch (Exception) { return false; }
    //}
    //public bool DuplexModeGet()
    //{
    //    try
    //    {
    //        _port.WriteLine("OF\x0D\x0A");
    //        Thread.Sleep(20);
    //        return true;
    //    }
    //    catch (Exception) { return false; }
    //}
    //public bool DuplexFrequencySet(int num, int DplFreqHz)
    //{
    //    try
    //    {
    //        if ((num > 19) || (num < 1) || (DplFreqHz < 10001) || (DplFreqHz > 999999999)) return false;
    //        string DplFrqStr = DplFreqHz.ToString().PadLeft(10, '0');
    //        if (DplFrqStr.Substring(8, 2) != "00") return false;
    //        string message = "OL" + num.ToString().PadLeft('0') + " " + DplFreqHz.ToString() + "\x0D\x0A";
    //        _port.WriteLine(message);
    //        Thread.Sleep(20);
    //        return true;
    //    }
    //    catch (Exception) { return false; }
    //}
    //public bool DuplexFrequencyGet()
    //{
    //    try
    //    {
    //        _port.WriteLine("OL\x0D\x0A");
    //        Thread.Sleep(20);
    //        return true;
    //    }
    //    catch (Exception) { return false; }
    //}
    //public bool SignalLevelUnit_dBmV_Get()
    //{
    //    try
    //    {
    //        _port.WriteLine("LU\x0D\x0A");
    //        Thread.Sleep(20);
    //        return true;
    //    }
    //    catch (Exception) { return false; }
    //}
    //public bool SignalLevelUnit_dBm_Get()
    //{
    //    try
    //    {
    //        _port.WriteLine("LB\x0D\x0A");
    //        Thread.Sleep(20);
    //        return true;
    //    }
    //    catch (Exception) { return false; }
    //}
    //public bool SearchDataSettingSet_nowork(int BankNum)
    //{
    //    return false;
    //    //try
    //    //{
    //    //    if (( > 2) || ( < 0)) return false;
    //    //    string message = "" + .ToString() + "\x0D\x0A";
    //    //    port.WriteLine(message);
    //    //    Thread.Sleep(20);
    //    //    return true;
    //    //}
    //    //catch (Exception) { return false; }
    //}
    //public bool SearchDataListGet()
    //{
    //    try
    //    {
    //        _port.WriteLine("SR\x0D\x0A");
    //        Thread.Sleep(20);
    //        return true;
    //    }
    //    catch (Exception) { return false; }
    //}
    //public bool PassFrequencySet(int Frq)
    //{
    //    try
    //    {
    //        if ((Frq > 3300000000) || (Frq < 10000)) return false;
    //        string message = "PW" + Frq.ToString().PadLeft(10, '0') + "\x0D\x0A";
    //        _port.WriteLine(message);
    //        Thread.Sleep(20);
    //        return true;
    //    }
    //    catch (Exception) { return false; }
    //}
    //public bool PassFrequencyListGet(int BankNum)
    //{
    //    try
    //    {
    //        if ((BankNum > 40) || (BankNum < 1)) return false;
    //        _port.WriteLine("PR" + BankNum.ToString().PadLeft(2, '0') + "\x0D\x0A");
    //        Thread.Sleep(20);
    //        return true;
    //    }
    //    catch (Exception) { return false; }
    //}
    //public bool DeletePassFrequencySet(int BankNum, int PassChannel)
    //{
    //    try
    //    {
    //        if ((BankNum > 40) || (BankNum < 1) || (PassChannel > 49) || (PassChannel < 0)) return false;
    //        string message = "PD" + BankNum.ToString().PadLeft(2, '0') + PassChannel.ToString().PadLeft(2, '0') + "\x0D\x0A";
    //        _port.WriteLine(message);
    //        Thread.Sleep(20);
    //        return true;
    //    }
    //    catch (Exception) { return false; }
    //}
    //public bool DeletePassFrequencyAllChannelsInBankSet(int BankNum)
    //{
    //    try
    //    {
    //        if ((BankNum > 40) || (BankNum < 1)) return false;
    //        string message = "PD" + BankNum.ToString().PadLeft(2, '0') + "%%\x0D\x0A";
    //        _port.WriteLine(message);
    //        Thread.Sleep(20);
    //        return true;
    //    }
    //    catch (Exception) { return false; }
    //}
    //public bool DeleteSearchDataWithPassFrequencySet(int BankNum)
    //{
    //    try
    //    {
    //        if ((BankNum > 40) || (BankNum < 1)) return false;
    //        string message = "QS" + BankNum.ToString().PadLeft(2, '0') + "\x0D\x0A";
    //        _port.WriteLine(message);
    //        Thread.Sleep(20);
    //        return true;
    //    }
    //    catch (Exception) { return false; }
    //}
    //public bool DeleteSearchDataWithPassFrequencyAllBanksSet()
    //{
    //    try
    //    {
    //        string message = "QS%%\x0D\x0A";
    //        _port.WriteLine(message);
    //        Thread.Sleep(20);
    //        return true;
    //    }
    //    catch (Exception) { return false; }
    //}
    //public bool TransferCurrentSearchDataToVFOSet(int NumVFO)
    //{
    //    try
    //    {
    //        if ((NumVFO > 9) || (NumVFO < 0)) return false;
    //        string message = "SV" + NumVFO.ToString() + "\x0D\x0A";
    //        _port.WriteLine(message);
    //        Thread.Sleep(20);
    //        return true;
    //    }
    //    catch (Exception) { return false; }
    //}
    //public bool MemoryDataSettingSet(int BankNum, int MemoryChannel)
    //{
    //    try
    //    {
    //        if ((BankNum > 9) || (BankNum < 0) || (MemoryChannel > 99) || (MemoryChannel < 0)) return false;
    //        string message = "MX" + BankNum.ToString() + MemoryChannel.ToString().PadLeft(2, '0') + "\x0D\x0A";
    //        _port.WriteLine(message);
    //        Thread.Sleep(20);
    //        return true;
    //    }
    //    catch (Exception) { return false; }
    //}
    public long FrqStrMhzToLongHz(string FreqMHz)
    {
        try
        {
            long frql;
            double frqd;
            if (!double.TryParse(FreqMHz, out frqd)) { return 0; }//MessageBox.Show("Введите частоту в диапазоне \nот 0,01 до 3 300  МГц"); }
            frqd = 1000000 * frqd;
            frql = (long)frqd;
            return frql;
        }
        catch (Exception) { return -1; }
    }
    public void OpenPort(string portName)
    {
        if (_port == null)
            _port = new SerialPort();
        if (_port.IsOpen)
            ClosePort();
        try
        {
            _port.PortName = portName;
            _port.BaudRate = 115200;
            _port.Parity = Parity.None;
            _port.DataBits = 8;
            _port.StopBits = StopBits.One;
            _port.RtsEnable = true;
            _port.DtrEnable = true;
            _port.ReceivedBytesThreshold = 1000;
            _port.Open();
            if (thrRead != null)
            {
                thrRead.Abort();
                thrRead.Join(500);
                thrRead = null;
            }
            try
            {
                thrRead = new Thread(new ThreadStart(ReadExistingComPort));
                thrRead.IsBackground = true;
                thrRead.Start();
            }
            catch (System.Exception) { }
            ConnectPort();
        }
        catch (System.Exception ex)
        {
            ex.ToString();
            DisconnectPort();
        }
    }

    protected virtual void ConnectPort()
    {
        if (OnConnectPort != null)
        {
            OnConnectPort();//Raise the event
        }
    }
    protected virtual void DisconnectPort()
    {
        if (OnDisconnectPort != null)
        {
            OnDisconnectPort();//Raise the event
        }
    }
    public void ClosePort()
    {
        try
        {
            _port.DiscardInBuffer();
        }
        catch (System.Exception) { }
        try
        {
            _port.DiscardOutBuffer();
        }
        catch (System.Exception) { }
        try
        {

            _port.Close();
            if (thrRead != null)
            {
                thrRead.Abort();
                thrRead.Join(500);
                thrRead = null;
            }
            DisconnectPort();
        }
        catch (System.Exception) { }
    }
    protected virtual void ReadByte(byte[] bByte)
    {
        if (OnReadByte != null)
        {
            OnReadByte();//Raise the event
        }
    }
    protected virtual void WriteByte(byte[] bByte)
    {
        if (OnWriteByte != null)
        {
            OnWriteByte();//Raise the event
        }
    }
    double ArOne_frequency;
    private void ReadExistingComPort()
    {
        string comand = null;
        while (true)
        {
            try
            {
                comand = null;
                //string s = _port.ReadExisting();
                string s = _port.ReadLine();
                if (s.Length > 2)
                {

                    comand = s.Substring(0, 2);
                    if (s.Substring(3, 2) == "RF")
                    {
                        double.TryParse(s.Substring(5, 10), out ArOne_frequency);
                        Int64.TryParse((ArOne_frequency).ToString(), out ArOne.frequency);
                        if (OnDecodedFrq != null)
                        {
                            OnDecodedFrq();
                        }
                    }
                    switch (comand)
                    {
                        case "VL":
                            int.TryParse(s.Substring(2, 3), out ArOne.AudioGain);
                            if (OnDecodedAudioGain != null)
                            {
                                OnDecodedAudioGain();
                            }
                            break;
                        case "MD":
                            int.TryParse(s.Substring(2, 2), out ArOne.Mode);
                            if (OnDecodedMode != null)
                            {
                                OnDecodedMode();
                            }
                            break;
                        case "BW":
                            int.TryParse(s.Substring(2, 1), out ArOne.Bandwidth);
                            if (OnDecodedBW != null)
                            {
                                //OnDecodedByte(comand, ArOne.Bandwidth);
                                OnDecodedBW();
                            }
                            break;
                        case "HP": 
                            int.TryParse(s.Substring(2, 1), out ArOne.HighPassFilter);   
                            if (OnDecodedHPF != null)
                            {
                                OnDecodedHPF();
                            }
                            break;
                        case "LP":                        
                            int.TryParse(s.Substring(2, 1), out ArOne.LowPassFilter); 
                            if (OnDecodedLPF != null)
                            {
                                OnDecodedLPF();
                            }
                            break;
                        case "AC":
                                    string bbc = s.Substring(2, 1);
                                    if (bbc == "F") { ArOne.AGC =3; }
                                    else
                                    {
                                        int.TryParse(s.Substring(2, 1), out ArOne.AGC);
                                    }
                                    bbc = "";
                                    if (OnDecodedAGC != null)
                                    {
                                        OnDecodedAGC();
                                    }
                                    break;
                        case "EN":
                            int.TryParse(s.Substring(3, 1), out ArOne.DEemphasis);
                            if (OnDecodedDEEmphasis != null)
                            {
                                OnDecodedDEEmphasis();
                            }
                            break;
                        case "AT":
                            int.TryParse(s.Substring(2, 2), out ArOne.Attenuator);
                            if (OnDecodedATT != null)
                            {
                                OnDecodedATT();
                            }
                            break;
                        //case "AM":
                        //    int.TryParse(s.Substring(2, 1), out ArOne.Amplifier);
                        //    if (OnDecodedAMpl != null)
                        //    {
                        //        OnDecodedAMpl();
                        //    }
                        //    break;
                        //case "BF":
                        //    int.TryParse(s.Substring(3, 5), out ArOne.BFOFreq);
                        //    if (OnDecodedBFOFreq != null)
                        //    {
                        //        OnDecodedBFOFreq();
                        //    }
                        //    break;
                        case "RQ":
                            int.TryParse(s.Substring(2, 3), out ArOne.LevelSquelch);
                            if (OnDecodedLevelSquelch != null)
                            {
                                OnDecodedLevelSquelch();
                            }
                            break;
                        case "DB":
                            int.TryParse(s.Substring(2, 3), out ArOne.NoiseSquelch);
                            if (OnDecodedNoiseSquelch != null)
                            {
                                OnDecodedNoiseSquelch();
                            }
                            break;
                        //case "AG":
                        //    int.TryParse(s.Substring(3, 3), out ArOne.AFGain);
                        //    if (OnDecodedAFGain != null)
                        //    {
                        //        OnDecodedAFGain();
                        //    }
                        //    break;
                        //case "MG":
                        //    int.TryParse(s.Substring(3, 3), out ArOne.ManualGain);
                        //    if (OnDecodedManualGain != null)
                        //    {
                        //        OnDecodedManualGain();
                        //    }
                        //    break;
                        case "RG":          //agc must be manual
                            int.TryParse(s.Substring(3, 3), out ArOne.ManualRFGain);
                            if (OnDecodedManualGain != null)
                            {
                                OnDecodedRFGain();
                            }
                            break;
                        //case "IG":
                        //    int.TryParse(s.Substring(3, 3), out ArOne.IFGain);
                        //    if (OnDecodedIFGain != null)
                        //    {
                        //        OnDecodedIFGain();
                        //    }
                        //    break;
                        //case "SQ":
                        //    int.TryParse(s.Substring(2, 1), out ArOne.SquelchSelect);
                        //    if (OnDecodedSquelchSelect != null)
                        //    {
                        //        OnDecodedSquelchSelect();
                        //    }
                        //    break;
                        case "LM":
                            //if (s.Substring(3, 1) == "X")  /*  tut obrabotka zaprosa LMX */       //vsegda otvet LMnnn
                            //{
                                //ArOne.SignalLevel = int.Parse(s.Substring(2, 3), System.Globalization.NumberStyles.HexNumber);
                                ArOne.SignalLevel = int.Parse(s.Substring(2, 3));
                                if (OnDecodedSignalLevel != null)
                                {
                                    OnDecodedSignalLevel();
                                }
                                break;
                            //}
                            //else { /* tut obrabotka zaprosa LM*/break; }
                        //case "LC":
                        //    int.TryParse(s.Substring(2, 1), out ArOne.AutoSignalLevel);
                        //    if (OnDecodedAutoSignalLevel != null)
                        //    {
                        //        OnDecodedAutoSignalLevel();
                        //    }
                        //    break;
                        //case "LA":
                        //    int.TryParse(s.Substring(2, 1), out ArOne.AutoBackLit);
                        //    if (OnDecodedAutoBackLit != null)
                        //    {
                        //        OnDecodedAutoBackLit();
                        //    }
                        //    break;
                        case "BL":
                            int.TryParse(s.Substring(2, 1), out ArOne.BackLit_OnOff);
                            if (OnDecodedBackLit_OnOff != null)
                            {
                                OnDecodedBackLit_OnOff();
                            }
                            break;
                        //case "LD":
                        //    int.TryParse(s.Substring(2, 1), out ArOne.BackLitDimmer);
                        //    if (OnDecodedBackLitDimmer != null)
                        //    {
                        //        OnDecodedBackLitDimmer();
                        //    }
                        //    break;
                        //case "LV":
                        //    int.TryParse(s.Substring(2, 2), out ArOne.LCDContrast);
                        //    if (OnDecodedLCDContrast != null)
                        //    {
                        //        OnDecodedLCDContrast();
                        //    }
                        //    break;
                        //case "BV":
                        //    int.TryParse(s.Substring(2, 1), out ArOne.BeepLevel);
                        //    if (OnDecodedBeepLevel != null)
                        //    {
                        //        OnDecodedBeepLevel();
                        //    }
                        //    break;
                        //case "SO":
                        //    int.TryParse(s.Substring(2, 1), out ArOne.SpeakerSelect);
                        //    if (OnDecodedSpeakerSelect != null)
                        //    {
                        //        OnDecodedSpeakerSelect();
                        //    }
                        //    break;
                        //case "PO":
                        //    int.TryParse(s.Substring(2, 1), out ArOne.ExternalSpeacker);
                        //    if (OnDecodedExternalSpeacker != null)
                        //    {
                        //        OnDecodedExternalSpeacker();
                        //    }
                        //    break;
                        //case "DD": 
                        //int.TryParse(s.Substring(2, 3), out ArOne.DelayTime); break;
                        //if (OnDecodedByte != null)
                        //{
                        //    OnDecodedByte("Err ", ArOne.DelayTime);
                        //}
                        //case "SP":
                        //    int.TryParse(s.Substring(2, 3), out ArOne.FreeScan);
                        //    if (OnDecodedFreeScan != null)
                        //    {
                        //        OnDecodedFreeScan();
                        //    }
                        //    break;
                        //case "SF":
                        //    int.TryParse(s.Substring(2, 1), out ArOne.SignalMeterDisplay);
                        //    if (OnDecodedSignalMeterDisplay != null)
                        //    {
                        //        OnDecodedSignalMeterDisplay();
                        //    }
                        //    break;
                        //case "OF":
                        //    int.TryParse(s.Substring(2, 3), out ArOne.DuplexMode);
                        //    if (OnDecodedDuplexMode != null)
                        //    {
                        //        OnDecodedDuplexMode();
                        //    }
                        //    break;
                        //case "OL": //запрос: DplFrqN могло быть 0..47, а тут 1..19 ???????????
                        //    int.TryParse(s.Substring(2, 2), out  ArOne.DuplexFrequencyN);
                        //    int.TryParse(s.Substring(5, 10), out ArOne.DuplexFrequency[ArOne.DuplexFrequencyN]);
                        //    if (OnDecodedDuplexFrequency != null)
                        //    {
                        //        OnDecodedDuplexFrequency();
                        //    }
                        //    break;
                        //case "LU":
                        //    int.TryParse(s.Substring(2, 3), out ArOne.SignalLevelUnit_dBmV);
                        //    if (OnDecodedSignalLevelUnit_dBmV != null)
                        //    {
                        //        OnDecodedSignalLevelUnit_dBmV();
                        //    }
                        //    break;
                        //case "LB":
                        //    int.TryParse(s.Substring(3, 4), out ArOne.SignalLevelUnit_dBm);
                        //    if (OnDecodedSignalLevelUnit_dBm != null)
                        //    {
                        //        OnDecodedSignalLevelUnit_dBm();
                        //    }
                        //    break;
                        //case "SR": 
                        //int.TryParse(s.Substring(2, 1), out ArOne.Mode); 
                        //if (OnDecodedByte != null)
                        //{
                        //    OnDecodedByte(comand, ArOne.Mode);/
                        //}
                        //break;
                        //case "PR": 
                        //int.TryParse(s.Substring(5, 10), out ArOne.PassFrequencyList[int.Parse(s.Substring(2, 2))]); 
                        //if (OnDecodedByte != null)
                        //{
                        //    OnDecodedByte(comand, );
                        //}
                        //break;
                        //case "GA":
                        //    int.TryParse(s.Substring(2, 1), out ArOne.SelectMemory_OnOff);
                        //    if (OnDecodedSelectMemory_OnOff != null)
                        //    {
                        //        OnDecodedSelectMemory_OnOff();
                        //    }
                        //    break;
                        //case "GR":
                        //    int.TryParse(s.Substring(2, 2), out ArOne.SelectMemoryList);
                        //    if (OnDecodedSelectMemoryList != null)
                        //    {
                        //        OnDecodedSelectMemoryList();//Raise the event
                        //    }
                        //    break;
                    }
                }
            }

            catch (Exception)
            {
                if (OnDecodedError != null)
                {
                    OnDecodedError(ArOne.s);//Raise the event
                }
            }
            //if (OnDecodedByte != null)
            //{
            //    OnDecodedByte(1);//Raise the event
            //}

        }
    }


}