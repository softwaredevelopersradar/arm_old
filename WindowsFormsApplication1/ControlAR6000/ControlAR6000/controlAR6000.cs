﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Ports;
using System.Globalization;
using System.Threading;
using System.Runtime.InteropServices;
using MathNet.Numerics.IntegralTransforms;
using NAudio.Wave;
using System.Numerics;
using MyDataGridView;
using System.Data.SQLite;
using System.IO;
using System.Diagnostics;
using System.Reflection;
using System.Media;
using NAudio.Wave.SampleProviders;

namespace ControlAR6000
{
    public struct Struct_AR6000
    {
        public byte bPriority;
        public int iAttenuator;
        public int iBW;
        public Int64 iFreq;
        public int iHPF;
        public int iID;
        public int iLPF;
        public int iMode;
        public int iOnOff;
        public int iPause;
        public int iU;
        public string Note;
        public string sTimeFirst;
    }
    public partial class controlAR6000: UserControl
    //partial class MainWnd
    {

        //********** from main ***********//
        public AR6000DLL AR6000aor;
        string Com_ArOne = "COM6";
        string FrqToAOR = "";
        bool zapolneno = false;
        int spektorYMinPorog = -110;
        int spektorYMaxPorog = -40;
        int TimePause_Baraban = 300;
        double taimer_zapisi_max = 100;
        double taimer_zapisi = 0;
        FunctionsDB functionsDB;
        System.Diagnostics.Process ProcessWavPlayer;
        bool bMute = false;
        NumberFormatInfo nfi = new CultureInfo("en-US", false).NumberFormat;
        bool DotInFrq = false;
        int countafterdot = 0;
        static double Fs = 44100;               // Частота дискретизвции 
        static double T = 1.0 / Fs;             // Шаг дискретизации
        static double Fn = Fs / 2;              // Частота Найквиста
        static int N = (int)Fs / 5;                           //Длина сигнала (точек)
        WaveIn waveIn;// WaveIn - поток для записи
        WaveFileWriter writer;
        Complex[] sig1;
        int waterflow_count = 0;
        LinkedList<string> link = new LinkedList<string>();
        LinkedListNode<string> node;
        Complex max = 2400;                         //для корректировки уровня спектра
        int iAverage = 0;
        int val = 0;                            //для заолнения таблицы
        string sval = "";                            //для заолнения таблицы

        double[] SignalX;           //для точек графика БПФ 
        double[] SignalY;           //для точек графика БПФ 
        double[] PorogObnar;           //для точек порог обнаружения
        double[] SignalYAverage;           //для точек графика БПФ
        double[] SignalYMinPorog;   //для мин порога графика БПФ
        double[] SignalYMaxPorog;   //для макс порога графика БПФ
        double[] SignalYResized;
        int iNumDataMx = 0;
        int iNumDataSigma = 0;
        int count_baund = 0;
        int WaterflowTime = 50;             //коэффициент высота водопада
        double[,] intensity;
        public int resolutionX = 1000;
        public int resolutionY = 1000;
        public int delitel;
        double qwe = 0;                             //точка сигнала на графике
        double sig_sum = 0;                         //среднее за 5 отсчетов
        double K = N / 4;
        Task ThrIntensityGraphPain;
        int ThrIntensityGraphN;
        int ThrIntensityGraphNeedCount = 3;
        public bool closing = false;
        int Nwaterflow_count = 3;               // коэффициент пропуски водопада
        Struct_AR6000 StrAR6000 = new Struct_AR6000();
        PropertyDGV propertyDGV;
        int jsql = 0;
        SQLiteConnection sqLiteConnect;
        SQLiteCommand sqLiteCommand;
        SQLiteDataReader sqLiteRead;
        int[] IDBaraban0;       //обычный
        int[] IDBaraban1;       //важный
        int IDBaraban0Count = 0;
        int IDBaraban1Count = 0;
        bool BarabanPriorityWorking = false;
        int Baraban1Num = 0;
        int Baraban0Num = 0;
        int kol_previshenij_dlia_zapisi = 10;       //колво превышений за 1 набор БПФ
        int kol_previshenij_dlia_zapisi_time_count = 5; //колво превышений подряд
        int kol_previshenij_dlia_zapisi_time_count_i = 0;
        int kol_buf_ostanovka_zapisi = 300;                  //задержка конца записи
        int PorogZapisi = -120;

        System.Threading.Timer timer;
        System.Threading.Timer timerRead;
        int TimePause = 100;
        public float step_frq_MHz = 10f / 1000000f;
        Task TaskIntensityPaint;
        int ibaraban = 0;

        [DllImport("kernel32")]
        private static extern long WritePrivateProfileString(string section, string key, string val, string filePath);
        [DllImport("kernel32")]
        private static extern int GetPrivateProfileString(string section, string key, string def, StringBuilder retVal, int size, string filePath);

        public delegate void ByteEventHandler();
        public delegate void ByteEventHandlerInt(int qwe);
        public event ByteEventHandlerInt OnWrite;
        public event ByteEventHandlerInt OnRead;
        public event ByteEventHandler OnBAORRed;
        public event ByteEventHandler OnBAORGreen;

        ComponentResourceManager resources = new ComponentResourceManager(typeof(controlAR6000));
        private int NumberOfLanguage;
         string kHz = "кГц";
         string dBm = "дБм";
         string dB = "дБ";
         string UsilVKL = "Усил ВКЛ";
         string Attt = "Атт";
         string Avto = "Авто";
         string Vykl = "ВЫКЛ";
         string Gc = "Гц";
         string Obychnyj = "Обычный";
         string Vazhnyj = "Важный";
         string OshibkaChtenijaDannyhTablicy = "Ошибка чтения данных таблицы";
         string Oshibka = "Ошибка";
         //const string aroneVolumeChannelName2 = "Лин. вход (2- Realtek High Definition Audio)";
        //const string aroneVolumeChannelName2 = "Микрофон (Устройство с поддержкой High Definition Audio)";
         //const string aroneVolumeChannelName2 = "Лин. вход (Устройство с поддержкой High Definition Audio)";
         const string aroneVolumeChannelName2 = "Микрофон (Cirrus Logic CDB4207)";
        void VariableWork_OnAddFreqImportantKRPU(USR_DLL.TFreqImportantKRPU[] freqImportantKRPU)
        {
            try
            {
                for (int i = 0; i < freqImportantKRPU.Length; i++)
                {
                    if (freqImportantKRPU[i].iFreqMin == freqImportantKRPU[i].iFreqMax)
                        AddFrqToTablAR6000(freqImportantKRPU[i].iFreqMin, 1);
                }
            }
            catch { }
        }
        

        public controlAR6000()
        {
            InitializeComponent();
            functionsDB = new FunctionsDB();
            ProcessWavPlayer = new Process();
            //ProcessWavPlayer.StartInfo.FileName = Application.StartupPath + "\\Player\\Player.exe";
            InitConnectionArOne();
            InitLabelParamArOne();

            VariableDynamic.VariableWork.OnAddFreqImportantKRPU += new VariableDynamic.VariableWork.AddFreqImportantKRPUEventHandler(VariableWork_OnAddFreqImportantKRPU);

            
           // MuteArone(false);
          //  AR6000aor.AudioGainSet(60);// (tbAudioGain.Value);

            /*
            foreach (string line in File.ReadLines(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\INI\\Common.ini"))
            {
                if (line.Contains("Language "))
                {
                    string LanguageAsNumber = line.Split(new string[] { "=" }, StringSplitOptions.None).Last();
                    NumberOfLanguage = Int32.Parse(LanguageAsNumber);
                    break;
                }
            }
            LanguageChooser(NumberOfLanguage);
            */
        }

        public void FreqSetToAOR(Int64 f)
        {
            AR6000aor.FrequencySet(f/1000000d);
        }
        public static string read_AOR_ComN()
        {
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("AORAR6000", "ComN", "COM9", temp, 255, Application.StartupPath + "\\INI\\Connection.ini");
            return temp.ToString();
        }


        private void IzmenenieYazyka(int lang) 
        {
            switch (lang)
            {
                case 0:
                    kHz = "кГц";
                    dBm = "дБм";
                    dB = "дБ";
                    UsilVKL = "Усил ВКЛ";
                    Attt = "Атт";
                    Avto = "Авто";
                    Vykl = "ВЫКЛ";
                    Gc = "Гц";
                    Obychnyj = "Обычный";
                    Vazhnyj = "Важный";
                    OshibkaChtenijaDannyhTablicy = "Ошибка чтения данных таблицы";
                    Oshibka = "Ошибка";
                    break;
                case 1:
                    break;
                case 2:
                    kHz = "kHs";
                    dBm = "dBm";
                    dB = "dB";
                    UsilVKL = "Gücl. Qoş";
                    Attt = "Att";
                    Avto = "Avto";
                    Vykl = "Sön";
                     Gc = "Hs";
                     Obychnyj = "Adi ";
                     Vazhnyj = "Mühüm";
                     OshibkaChtenijaDannyhTablicy = "Cədvəl məlumatının oxunmasında";
                     Oshibka = "səhv";
                    break;
            }
        }
        private void InitConnectionArOne()
        {
            waveIn = new WaveIn();
            if (cbACP.Checked) cbACP.Checked = false;
            bAOR.BackColor = Color.Red;
            if (OnBAORRed != null)
                OnBAORRed();
            cbACP.Enabled = false;
            cbAverage.Enabled = false;
            cbRec.Enabled = false;
            cbARec.Enabled = false;
            cbIntensityPaint.Enabled = false;
            b1.Enabled = false;
            b2.Enabled = false;
            b3.Enabled = false;
            b4.Enabled = false;
            b5.Enabled = false;
            b6.Enabled = false;
            b7.Enabled = false;
            b8.Enabled = false;
            b9.Enabled = false;
            b0.Enabled = false;
            bENT.Enabled = false;
            bESC.Enabled = false;
            bDot.Enabled = false;
            groupBox4.Enabled = false;
            //comboBoxAtt.Enabled = false;
            //comboBoxBW.Enabled = false;
            //comboBoxHPF.Enabled = false;
            //comboBoxLPF.Enabled = false;
            //comboBoxMode.Enabled = false;
            //groupBox3.Enabled = false;
            bFrqMinus.Enabled = false;
            bFrqPlus.Enabled = false;
            cbFrqStep.Enabled = false;
            bIspPel.Enabled = false;
            bFRSonRS.Enabled = false;
            groupBox1.Enabled = false;
            cbSQL.Enabled = false;
            this.cbSQL.SelectedIndexChanged -= new EventHandler(this.cbSQL_SelectedIndexChanged);
            cbSQL.SelectedIndex = 0;
            cbFrqStep.SelectedIndex = 0;
            this.comboBoxAtt.SelectedIndexChanged -= new System.EventHandler(this.comboBoxAtt_SelectedIndexChanged);
            this.comboBoxBW.SelectedIndexChanged -= new System.EventHandler(this.comboBoxBW_SelectedIndexChanged);
            this.comboBoxMode.SelectedIndexChanged -= new System.EventHandler(this.comboBoxMode_SelectedIndexChanged);
            this.comboBoxHPF.SelectedIndexChanged -= new System.EventHandler(this.comboBoxHPF_SelectedIndexChanged);
            this.comboBoxLPF.SelectedIndexChanged -= new System.EventHandler(this.comboBoxLPF_SelectedIndexChanged);
            this.cbAGC.SelectedIndexChanged -= new System.EventHandler(this.cbAGC_SelectedIndexChanged);
            this.tmBaraban.Tick -= new System.EventHandler(this.tmBaraban_Tick);
            this.tbAudioGain.Scroll -= new System.EventHandler(this.tbAudioGain_Scroll);
            try
            {
                cbTablePriority.SelectedIndex = 0;
                comboBoxAtt.SelectedIndex = 0;
                comboBoxBW.SelectedIndex = 0;
                comboBoxHPF.SelectedIndex = 0;
                comboBoxLPF.SelectedIndex = 0;
                comboBoxMode.SelectedIndex = 0;
                cbAGC.SelectedIndex = 0;
            }
            catch { }
            try
            {
                if (AR6000aor != null) AR6000aor = null;
                if (AR6000aor != null)
                    AR6000aor = null;
                AR6000aor = new AR6000DLL();
                nfi.NumberGroupSeparator = " ";
                AR6000aor.OnConnectPort += new AR6000DLL.ConnectEventHandler(ConnectPort);
                AR6000aor.OnDisconnectPort += new AR6000DLL.ConnectEventHandler(DisconnextPort);
                AR6000aor.OnReadByte += new AR6000DLL.ByteEventHandler(ReadAOR);
                AR6000aor.OnWriteByte += new AR6000DLL.ByteEventHandler(WriteAOR);
                AR6000aor.OnDecodedBW += new AR6000DLL.ByteEventHandler(DecodedBW);
                AR6000aor.OnDecodedAGC += new AR6000DLL.ByteEventHandler(DecodedAC);
                AR6000aor.OnDecodedAudioGain += new AR6000DLL.ByteEventHandler(DecodedAudioGain);
                AR6000aor.OnDecodedATT += new AR6000DLL.ByteEventHandler(DecodedAtt);
                AR6000aor.OnDecodedError += new AR6000DLL.ByteEventHandler1(DecodedErr);
                AR6000aor.OnDecodedHPF += new AR6000DLL.ByteEventHandler(DecodedHPF);
                AR6000aor.OnDecodedLPF += new AR6000DLL.ByteEventHandler(DecodedLPF);
                AR6000aor.OnDecodedRFGain += new AR6000DLL.ByteEventHandler(DecodedRFGain);
                AR6000aor.OnDecodedLevelSquelch += new AR6000DLL.ByteEventHandler(DecodedLevelSquelch);
                AR6000aor.OnDecodedManualGain += new AR6000DLL.ByteEventHandler(DecodedManualGain);
                AR6000aor.OnDecodedMode += new AR6000DLL.ByteEventHandler(DecodedMode);
                AR6000aor.OnDecodedNoiseSquelch += new AR6000DLL.ByteEventHandler(DecodedNoiseSquelch);
                AR6000aor.OnDecodedFrq += new AR6000DLL.ByteEventHandler(DecodedFrq);
                AR6000aor.OnDecodedSignalLevel += new AR6000DLL.ByteEventHandler(DecodedSignalLevel);
                Com_ArOne = read_AOR_ComN();
                //AR6000aor.OpenPort(Com_ArOne);
                Thread.Sleep(300);
                FlashLedWriteARONE();
                AR6000aor.TurnON();
                Ask_Arone();
                int mod = AR6000aor.ArOne.Mode;
                sig1 = new Complex[44100];
                SignalX = new double[2205];
                SignalY = new double[2205];
                PorogObnar = new double[2205];
                SignalYAverage = new double[2205];
                SignalYMinPorog = new double[2205];
                SignalYMaxPorog = new double[2205];
                ConnectAudio();
                intensityGraph1Resize();
            }
            catch (Exception) { } 
            try
            {
                intensityPlot1.Plot(intensity);
            }
            catch { }
            propertyDGV = new PropertyDGV();
            propertyDGV.SetPropertyDGV(dgv);
            try
            {
                //zagruzka_tablFromBD();
            }
            catch { }
            int IDNum = 0;
            if(dgv.RowCount>0)
                if(dgv.Rows[0].Cells[2]!=null)
                    try
                    {
                        nudTableFRQ.Value = decimal.Parse(dgv.Rows[IDNum].Cells[2].Value.ToString());
                        nudTablePorogObnar.Value = int.Parse(dgv.Rows[IDNum].Cells[4].Value.ToString());
                        cbTablePriority.Text = dgv.Rows[IDNum].Cells[11].Value.ToString();
                        tbNote.Text = dgv.Rows[IDNum].Cells[12].Value.ToString();
                        //comboBoxAtt.Text = dgv.Rows[IDNum].Cells[6].Value.ToString();
                        nudPause.Value = int.Parse(dgv.Rows[IDNum].Cells[3].Value.ToString());
                        //comboBoxMode.Text = dgv.Rows[IDNum].Cells[7].Value.ToString();
                        //comboBoxBW.Text = dgv.Rows[IDNum].Cells[8].Value.ToString();
                        //comboBoxHPF.Text = dgv.Rows[IDNum].Cells[9].Value.ToString();
                        //comboBoxLPF.Text = dgv.Rows[IDNum].Cells[10].Value.ToString();
                    }
                    catch { }
            this.comboBoxAtt.SelectedIndexChanged += new System.EventHandler(this.comboBoxAtt_SelectedIndexChanged);
            this.comboBoxBW.SelectedIndexChanged += new System.EventHandler(this.comboBoxBW_SelectedIndexChanged);
            this.comboBoxMode.SelectedIndexChanged += new System.EventHandler(this.comboBoxMode_SelectedIndexChanged);
            this.comboBoxHPF.SelectedIndexChanged += new System.EventHandler(this.comboBoxHPF_SelectedIndexChanged);
            this.comboBoxLPF.SelectedIndexChanged += new System.EventHandler(this.comboBoxLPF_SelectedIndexChanged);
            this.cbSQL.SelectedIndexChanged += new EventHandler(this.cbSQL_SelectedIndexChanged);
            this.cbAGC.SelectedIndexChanged += new System.EventHandler(this.cbAGC_SelectedIndexChanged);
            this.tmBaraban.Tick += new System.EventHandler(this.tmBaraban_Tick);
            this.tbAudioGain.Scroll += new System.EventHandler(this.tbAudioGain_Scroll);
            var TaskIntensityPaint = Task.Run(() => intensityPlot1.Plot(intensity));
  
        }


        private void WriteAOR()
        {
        }

        private void ReadAOR()
        {
        }
        private void ConnectAudio()
        {
            try
            {
                waveIn.DeviceNumber = 0;//Дефолтное устройство для записи (если оно имеется)
                // waveIn.RecordingStopped += waveIn_RecordingStopped;//Прикрепляем обработчик завершения записи
                //waveIn.DataAvailable += waveIn_DataAvailable;
                waveIn.WaveFormat = new WaveFormat((int)Fs, 1); //Формат wav-файла - принимает параметры - частоту дискретизации и количество каналов(здесь mono)
                waveIn.StartRecording();
                //string outputFilename = String.Format("{0}{1}-{2}-{3}{4}", "C:\\ARM1\\Анализатор\\wav\\", textBox1.Text, DateTime.Now.ToString("dd.MM.yyyy"), DateTime.Now.ToString("hh.mm.ss"), ".wav");
                //"./" + textBox1.Text + "_" + DateTime.Now.ToString("yyyy:MM:dd-HH:mm") + "demo.wav";// 
                //writer = new WaveFileWriter(outputFilename, waveIn.WaveFormat);
                //WaveOut Waveout = new WaveOut();
            }
            catch (Exception ex)
            { MessageBox.Show(ex.Message); }
        }
        void Ask_Arone()
        {
            if (bAOR.BackColor == Color.Red) return;
            try
            {
                FlashLedWriteARONE();
                AR6000aor.AttenuatorGet();
                Thread.Sleep(5);
                FlashLedWriteARONE();
                AR6000aor.AutomaticGainControlGet();
                Thread.Sleep(5);
                FlashLedWriteARONE();
                AR6000aor.BandwidthGet();
                Thread.Sleep(5);
                FlashLedWriteARONE();
                Thread.Sleep(5);
                AR6000aor.FrequencyGet();
                Thread.Sleep(5);
                AR6000aor.AutomaticGainControlGet();
                FlashLedWriteARONE();
                AR6000aor.HighPassFiltrGet();
                Thread.Sleep(5);
                FlashLedWriteARONE();
                AR6000aor.LowPassFiltrGet();
                Thread.Sleep(5);
                FlashLedWriteARONE();
                AR6000aor.ModeGet();
                Thread.Sleep(5);
                FlashLedWriteARONE();
                AR6000aor.AudioGainGet();
                Thread.Sleep(50);
            }
            catch { }
        }
        private void InitLabelParamArOne()
        {
            lFrq.BackColor = pScreen.BackColor;
            lFrq.ForeColor = lFRQFromAOR.ForeColor;
        }
        private void DecodedAudioGain()
        {
            FlashLedReadARONE();
            return;
            this.tbAudioGain.Scroll -= new System.EventHandler(this.tbAudioGain_Scroll);
            tbAudioGain.Value = AR6000aor.ArOne.AudioGain;
            this.tbAudioGain.Scroll += new System.EventHandler(this.tbAudioGain_Scroll);

        }
        private void DecodedSquelchSelect()
        {
            FlashLedReadARONE();
        }
        private void DecodedSignalLevelUnit_dBmV()
        {
            FlashLedReadARONE();
        }
        private void DecodedSignalLevelUnit_dBm()
        {
            FlashLedReadARONE();
            if (lLevel.InvokeRequired)
            {
                lLevel.Invoke((MethodInvoker)(delegate()
                {
                    lLevel.Text = (AR6000aor.ArOne.SignalLevelUnit_dBm).ToString() + dBm;// " дБм";
                }));

            }
            else
            {
                lLevel.Text = (AR6000aor.ArOne.SignalLevelUnit_dBm).ToString() + dBm;//" дБм";
            }
        }
        private void DecodedSignalLevel()
        {
            FlashLedReadARONE();
            if (lLevel.InvokeRequired)
            {
                lLevel.Invoke((MethodInvoker)(delegate()
                {
                    lLevel.Text = "-" + (AR6000aor.ArOne.SignalLevel).ToString() + dB;//" дБ";
                }));

            }
            else
            {
                lLevel.Text = "-" + (AR6000aor.ArOne.SignalLevel).ToString() + dB;//" дБ";
            }
        }
        private void DecodedFrq()
        {
            FlashLedReadARONE();
            if (lFRQFromAOR.InvokeRequired)
            {
                lFRQFromAOR.Invoke((MethodInvoker)(delegate()
                {
                    double qwwe = AR6000aor.ArOne.frequency / 1000d;
                    //lFRQFromAOR.Text = qwwe.ToString("0.000"/*"N3", nfi*/) + " кГц";// ("{0:#,###,###.###}");             //ToString("F3");
                    lFRQFromAOR.Text = (AR6000aor.ArOne.frequency / 1000).ToString() + "," + (AR6000aor.ArOne.frequency % 1000).ToString("000") + kHz;// " кГц";// ("{0:#,###,###.###}");             //ToString("F3");
                }));

            }
            else
            {
                lFRQFromAOR.Text = (AR6000aor.ArOne.frequency / 1000).ToString() + "," + (AR6000aor.ArOne.frequency % 1000).ToString("000") + kHz;//" кГц";// ("{0:#,###,###.###}");             //ToString("F3");
            }
        }
        private void DecodedNoiseSquelch()
        {
            FlashLedReadARONE();
        }
        private void DecodedMode()
        {
            FlashLedReadARONE();
            if (lMode.InvokeRequired)
            {
                lMode.Invoke((MethodInvoker)(delegate()
                {
                    switch (AR6000aor.ArOne.Mode)
                    {
                        case 00: lMode.Text = "FM"; break;
                        case 01: lMode.Text = "FMST"; break;
                        case 02: lMode.Text = "AM"; break;
                        case 03: lMode.Text = "SAM"; break;
                        case 04: lMode.Text = "USB"; break;
                        case 05: lMode.Text = "LSB"; break;
                        case 06: lMode.Text = "CW"; break;
                        case 07: lMode.Text = "ISB"; break;
                        case 08: lMode.Text = "AIQ"; break;
                        case 21: lMode.Text = "WFM"; break;
                        case 22: lMode.Text = "WFM"; break;
                        case 23: lMode.Text = "FMST"; break;
                        case 24: lMode.Text = "NFM"; break;
                        case 25: lMode.Text = "SFM"; break;
                        case 26: lMode.Text = "WAM"; break;
                        case 27: lMode.Text = "AM"; break;
                        case 28: lMode.Text = "NAM"; break;
                        case 29: lMode.Text = "SAM"; break;
                        case 30: lMode.Text = "USB"; break;
                        case 31: lMode.Text = "LSB"; break;
                        case 32: lMode.Text = "CW1"; break;
                        case 33: lMode.Text = "CW2"; break;
                        case 34: lMode.Text = "ISB"; break;
                        case 35: lMode.Text = "AIQ"; break;
                    }
                    if (zapolneno == false) comboBoxMode.SelectedIndex = AR6000aor.ArOne.Mode;
                }));

            }
            else
            {
                switch (AR6000aor.ArOne.Mode)
                {
                    case 00: lMode.Text = "FM"; break;
                    case 01: lMode.Text = "FMST"; break;
                    case 02: lMode.Text = "AM"; break;
                    case 03: lMode.Text = "SAM"; break;
                    case 04: lMode.Text = "USB"; break;
                    case 05: lMode.Text = "LSB"; break;
                    case 06: lMode.Text = "CW"; break;
                    case 07: lMode.Text = "ISB"; break;
                    case 08: lMode.Text = "AIQ"; break;
                    case 21: lMode.Text = "WFM"; break;
                    case 22: lMode.Text = "WFM"; break;
                    case 23: lMode.Text = "FMST"; break;
                    case 24: lMode.Text = "NFM"; break;
                    case 25: lMode.Text = "SFM"; break;
                    case 26: lMode.Text = "WAM"; break;
                    case 27: lMode.Text = "AM"; break;
                    case 28: lMode.Text = "NAM"; break;
                    case 29: lMode.Text = "SAM"; break;
                    case 30: lMode.Text = "USB"; break;
                    case 31: lMode.Text = "LSB"; break;
                    case 32: lMode.Text = "CW1"; break;
                    case 33: lMode.Text = "CW2"; break;
                    case 34: lMode.Text = "ISB"; break;
                    case 35: lMode.Text = "AIQ"; break;
                }
                if (zapolneno == false) comboBoxMode.SelectedIndex = AR6000aor.ArOne.Mode;
            }
        }
        private void DecodedManualGain()
        {
            FlashLedReadARONE();
            if (trackBarManualGain.InvokeRequired)
            {
                trackBarManualGain.Invoke((MethodInvoker)(delegate()
                {
                    // trackBarManualGain.Value = AR6000aor.ArOne.ManualGain;
                }));

            }
            else
            {
                //trackBarManualGain.Value = AR6000aor.ArOne.ManualGain;
            }
        }
        private void DecodedLPF()
        {
            FlashLedReadARONE();
            if (comboBoxLPF.InvokeRequired)
            {
                comboBoxLPF.Invoke((MethodInvoker)(delegate()
                {
                    if (zapolneno == false) comboBoxLPF.SelectedIndex = AR6000aor.ArOne.LowPassFilter;
                }));
            }
            else
            {
                if (zapolneno == false) comboBoxLPF.SelectedIndex = AR6000aor.ArOne.LowPassFilter;
            }
        }
        private void DecodedLevelSquelch()
        {
            FlashLedReadARONE();
        }
        private void DecodedIFGain()
        {
            FlashLedReadARONE();
        }
        private void DecodedErr(string data)
        {
            FlashLedReadARONE();
            //MessageBox.Show("Err: " + data);
        }
        private void DecodedHPF()
        {
            FlashLedReadARONE();
            if (comboBoxHPF.InvokeRequired)
                comboBoxHPF.Invoke((MethodInvoker)(delegate()
                {
                    if (zapolneno == false) comboBoxHPF.SelectedIndex = AR6000aor.ArOne.HighPassFilter;
                }));
            else
            {
                if (zapolneno == false) comboBoxHPF.SelectedIndex = AR6000aor.ArOne.HighPassFilter;
            }
        }
        private void DecodedAutoSignalLevel()
        {
            FlashLedReadARONE();
        }
        private void DecodedAtt()
        {
            FlashLedReadARONE();
            if (comboBoxAtt.InvokeRequired)
            {
                comboBoxAtt.Invoke((MethodInvoker)(delegate()
                {
                    switch (AR6000aor.ArOne.Attenuator)
                    {
                        case 0: lat.Text = UsilVKL; break;// "Усил ВКЛ"; break;
                        case 1: lat.Text = Attt + " 0" + dB; break;// "Атт 0дБ"; break;
                        case 2: lat.Text = Attt + " -10" + dB; break;// "Атт -10дБ"; break;
                        case 3: lat.Text = Attt + " -20" + dB; break;//"Атт -20дБ"; break;
                        case 10: lat.Text = Attt +" "+ Avto; break;// "Атт Авто"; break;
                        case 11: lat.Text = Attt + " " + Avto; break;// "Атт Авто"; break;
                        case 12: lat.Text = Attt + " " + Avto; break; //"Атт Авто"; break;
                        case 13: lat.Text = Attt + " " + Avto; break; //"Атт Авто"; break;
                        case 14: lat.Text = Attt + " " + Avto; break; //"Атт Авто"; break;
                    }

                    if (zapolneno == false)
                        switch (AR6000aor.ArOne.Attenuator)
                        {
                            case 0: comboBoxAtt.SelectedIndex = 0; break;
                            case 1: comboBoxAtt.SelectedIndex = 1; break;
                            case 2: comboBoxAtt.SelectedIndex = 2; break;
                            case 3: comboBoxAtt.SelectedIndex = 3; break;
                            case 10: comboBoxAtt.SelectedIndex = 4; break;
                            case 11: comboBoxAtt.SelectedIndex = 4; break;
                            case 12: comboBoxAtt.SelectedIndex = 4; break;
                            case 13: comboBoxAtt.SelectedIndex = 4; break;
                            case 14: comboBoxAtt.SelectedIndex = 4; break;
                        }
                }));

            }
            else
            {
                switch (AR6000aor.ArOne.Attenuator)
                {
                    case 0: lat.Text = UsilVKL; break;// "Усил ВКЛ"; break;
                    case 1: lat.Text = Attt + " 0" + dB; break;// "Атт 0дБ"; break;
                    case 2: lat.Text = Attt + " -10" + dB; break;// "Атт -10дБ"; break;
                    case 3: lat.Text = Attt + " -20" + dB; break;//"Атт -20дБ"; break;
                    case 10: lat.Text = Attt + " " + Avto; break;// "Атт Авто"; break;
                    case 11: lat.Text = Attt + " " + Avto; break;// "Атт Авто"; break;
                    case 12: lat.Text = Attt + " " + Avto; break; //"Атт Авто"; break;
                    case 13: lat.Text = Attt + " " + Avto; break; //"Атт Авто"; break;
                    case 14: lat.Text = Attt + " " + Avto; break; //"Атт Авто"; break;
                }
                if (zapolneno == false)
                    switch (AR6000aor.ArOne.Attenuator)
                    {
                        case 0: comboBoxAtt.SelectedIndex = 0; break;
                        case 1: comboBoxAtt.SelectedIndex = 1; break;
                        case 2: comboBoxAtt.SelectedIndex = 2; break;
                        case 3: comboBoxAtt.SelectedIndex = 3; break;
                        case 10: comboBoxAtt.SelectedIndex = 4; break;
                        case 11: comboBoxAtt.SelectedIndex = 4; break;
                        case 12: comboBoxAtt.SelectedIndex = 4; break;
                        case 13: comboBoxAtt.SelectedIndex = 4; break;
                        case 14: comboBoxAtt.SelectedIndex = 4; break;
                    }
            }
        }
        private void DecodedAFGain()
        {
            FlashLedReadARONE();
            //if (.InvokeRequired)
            //{
            //    .Invoke((MethodInvoker)(delegate()
            //    {
            //        .Text = classLibrary_ARONE.ArOne..ToString();
            //    }));

            //}
            //else
            //{
            //    .Text = classLibrary_ARONE.ArOne..ToString();
            //}
        }
        private void DecodedRFGain()
        {
            FlashLedReadARONE();
            //    //if (trackBarRFGain.InvokeRequired)
            //    //{
            //    //    trackBarRFGain.Invoke((MethodInvoker)(delegate()
            //    //    {
            //    //        //trackBarRFGain.Value = classLibrary_ARONE.ArOne.RFGain;
            //    //    }));

            //    //}
            //    //else
            //    //{
            //    //    //trackBarRFGain.Value = classLibrary_ARONE.ArOne.RFGain;
            //    //}
        }
        private void DecodedAC() //AGC
        {
            FlashLedReadARONE();
            if (cbAGC.InvokeRequired)
            {
                cbAGC.Invoke((MethodInvoker)(delegate()
                {
                    if (zapolneno == false) cbAGC.SelectedIndex = AR6000aor.ArOne.AGC;
                }));
            }
            else
            {
                if (zapolneno == false) cbAGC.SelectedIndex = AR6000aor.ArOne.AGC;
            }
            zapolneno = true;
        }
        private void DecodedBW()
        {
            FlashLedReadARONE();
            if (lBw.InvokeRequired)
            {
                lBw.Invoke((MethodInvoker)(delegate()
                {
                    switch (AR6000aor.ArOne.Bandwidth)
                    {
                        case 0: lBw.Text = "0.2 " + kHz; break;// кГц"; break;
                        case 1: lBw.Text = "0.5 "+ kHz; break; //кГц"; break;
                        case 2: lBw.Text = "1 " + kHz; break; //кГц"; break;
                        case 3: lBw.Text = "3.0 " + kHz; break; //кГц"; break;
                        case 4: lBw.Text = "6.0 " + kHz; break; //кГц"; break;
                        case 5: lBw.Text = "15 " + kHz; break; //кГц"; break;
                        case 6: lBw.Text = "30 " + kHz; break; //кГц"; break;
                        case 7: lBw.Text = "100 " + kHz; break; //кГц"; break;
                        case 8: lBw.Text = "200 " + kHz; break; //кГц"; break;
                        case 9: lBw.Text = "300 " + kHz; break; //кГц"; break;
                    }
                    if (zapolneno == false) comboBoxBW.SelectedIndex = AR6000aor.ArOne.Bandwidth;
                }));

            }
            else
            {
                switch (AR6000aor.ArOne.Bandwidth)
                {
                    case 0: lBw.Text = "0.2 " + kHz; break; //кГц"; break;
                    case 1: lBw.Text = "0.5 " + kHz; break; //кГц"; break;
                    case 2: lBw.Text = "1 " + kHz; break; //кГц"; break;
                    case 3: lBw.Text = "3.0 " + kHz; break; //кГц"; break;
                    case 4: lBw.Text = "6.0 " + kHz; break; //кГц"; break;
                    case 5: lBw.Text = "15 " + kHz; break; //кГц"; break;
                    case 6: lBw.Text = "30 " + kHz; break; //кГц"; break;
                    case 7: lBw.Text = "100 " + kHz; break; //кГц"; break;
                    case 8: lBw.Text = "200 " + kHz; break; //кГц"; break;
                    case 9: lBw.Text = "300 " + kHz; break; //кГц"; break;
                }
                if (zapolneno == false) comboBoxBW.SelectedIndex = AR6000aor.ArOne.Bandwidth;
            }
        }
        private void WriteAOR(string qwe)
        {
            MessageBox.Show(qwe);
            /*  try
              {
                  if (ledAORWrite.InvokeRequired)
                  {
                      ledAORWrite.Invoke((MethodInvoker)(delegate()
                      {
                          ledAORWrite.Value = true;
                          tmWriteAOR = new System.Threading.Timer(TimeWriteAOR, null, TimePause, 0);
                      }));

                  }
                  else
                  {
                      ledAORWrite.Value = true;
                      tmWriteAOR = new System.Threading.Timer(TimeWriteAOR, null, TimePause, 0);
                  }
              }
              catch (SystemException)
              { }*/
        }
        private void ReadAOR(string qwe)
        {
            MessageBox.Show(qwe);
            /*   try
               {
                   if (ledAORRead.InvokeRequired)
                   {
                       ledAORRead.Invoke((MethodInvoker)(delegate()
                       {
                           ledAORRead.Value = true;
                           tmReadAOR = new System.Threading.Timer(TimeReadAOR, null, TimePause, 0);
                       }));

                   }
                   else
                   {
                       ledAORRead.Value = true;
                       tmReadAOR = new System.Threading.Timer(TimeReadAOR, null, TimePause, 0);
                   }
               }
               catch (SystemException)
               { }*/
        }
        private void ConnectPort()
        {
            ThrIntensityGraphPain = new Task(ThrWaterflowPaint);
            bAOR.BackColor = Color.Green;

            cbACP.Enabled = true;
            cbAverage.Enabled = true;
            cbRec.Enabled = true;
            cbARec.Enabled = true;
            cbIntensityPaint.Enabled = true;
            b1.Enabled = true;
            b2.Enabled = true;
            b3.Enabled = true;
            b4.Enabled = true;
            b5.Enabled = true;
            b6.Enabled = true;
            b7.Enabled = true;
            b8.Enabled = true;
            b9.Enabled = true;
            b0.Enabled = true;
            bDot.Enabled = true;
            bENT.Enabled = true;
            bESC.Enabled = true;
            comboBoxAtt.Enabled = true;
            comboBoxBW.Enabled = true;
            comboBoxHPF.Enabled = true;
            comboBoxLPF.Enabled = true;
            comboBoxMode.Enabled = true;
            cbSQL.Enabled = true;
            //groupBox3.Enabled = true;
            groupBox1.Enabled = true;
            groupBox4.Enabled = true;
            waveformGraph1.Enabled = true;
            intensityGraph1.Enabled = true;
            tbWaterflowMax.Enabled = true;
            tbWaterflowMin.Enabled = true;
            pScreen.Enabled = true;
            //bTableAdd.Enabled = true;
            //bTableChange.Enabled = true;
            //bTableClear.Enabled = true;
            //bTableDel.Enabled = true;
            //nudTableFRQ.Enabled = true;
            //nudTablePorogObnar.Enabled = true;
            //cbTablePriority.Enabled = true;
            //tbNote.Enabled = true;
            //nudPause.Enabled = true;
            //dgv.Enabled = true;
            if (!cbACP.Checked) cbACP.Checked = true;
            cbFrqStep.Enabled = true;
            bFrqMinus.Enabled = true;
            bFrqPlus.Enabled = true;
            groupBox1.Enabled = true;
            bIspPel.Enabled = true;
            bFRSonRS.Enabled = true;
            radioButton1.Enabled = true;
            radioButton2.Enabled = true;
            closing = false;
            bIspPel.Enabled = true;
            bFRSonRS.Enabled = true;
            ThrIntensityGraphPain.Start();
            if (OnBAORGreen != null)
                OnBAORGreen();

        }
        private void DisconnextPort()
        {
            if (cbACP.Checked) cbACP.Checked = false;
            closing = true;
            bAOR.BackColor = Color.Red;
            if (OnBAORRed != null)
                OnBAORRed();
            cbACP.Enabled = false;
            cbAverage.Enabled = false;
            cbRec.Enabled = false;
            cbARec.Enabled = false;
            cbIntensityPaint.Enabled = false;
            b1.Enabled = false;
            b2.Enabled = false;
            b3.Enabled = false;
            b4.Enabled = false;
            b5.Enabled = false;
            b6.Enabled = false;
            b7.Enabled = false;
            b8.Enabled = false;
            b9.Enabled = false;
            b0.Enabled = false;
            bDot.Enabled = false;
            bENT.Enabled = false;
            bESC.Enabled = false;
            //comboBoxAtt.Enabled = false;
            //comboBoxBW.Enabled = false;
            //comboBoxHPF.Enabled = false;
            //comboBoxLPF.Enabled = false;
            //comboBoxMode.Enabled = false;
            cbSQL.Enabled = false;
            //groupBox3.Enabled = false;
            groupBox4.Enabled = false;
            groupBox1.Enabled = false;
            waveformGraph1.Enabled = false;
            intensityGraph1.Enabled = false;
            tbWaterflowMax.Enabled = false;
            tbWaterflowMin.Enabled = false;
            pScreen.Enabled = false;
            //bTableAdd.Enabled = false;
            //bTableChange.Enabled = false;
            //bTableClear.Enabled = false;
            //bTableDel.Enabled = false;
            //nudTableFRQ.Enabled = false;
            //nudTablePorogObnar.Enabled = false;
            //cbTablePriority.Enabled = false;
            //tbNote.Enabled = false;
            //nudPause.Enabled = false;
            //dgv.Enabled = false;
            cbFrqStep.Enabled = false;
            bFrqMinus.Enabled = false;
            bFrqPlus.Enabled = false;
            groupBox1.Enabled = false;
            bIspPel.Enabled = false;
            bFRSonRS.Enabled = false;
            radioButton1.Enabled = false;
            radioButton2.Enabled = false;
            bIspPel.Enabled = false;
            bFRSonRS.Enabled = false;

        }
        private void comboBoxBW_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (zapolneno == false) return;
            FlashLedWriteARONE();
            switch (comboBoxBW.SelectedIndex)
            {
                case 0: AR6000aor.BandWidthSet(0); break;
                case 1: AR6000aor.BandWidthSet(1); break;
                case 2: AR6000aor.BandWidthSet(2); break;
                case 3: AR6000aor.BandWidthSet(3); break;
                case 4: AR6000aor.BandWidthSet(4); break;
                case 5: AR6000aor.BandWidthSet(5); break;
                case 6: AR6000aor.BandWidthSet(6); break;
                case 7: AR6000aor.BandWidthSet(7); break;
                case 8: AR6000aor.BandWidthSet(8); break;
                case 9: AR6000aor.BandWidthSet(9); break;
                default: break;
            }
            Thread.Sleep(20);
            AR6000aor.BandwidthGet();
        }
        private void comboBoxAtt_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (zapolneno == false) return;
            FlashLedWriteARONE();
            AR6000aor.AttenuatorSet(comboBoxAtt.SelectedIndex);
            Thread.Sleep(20);
            AR6000aor.AttenuatorGet();

        }
        private void comboBoxMode_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (zapolneno == false) return;
            FlashLedWriteARONE();
            switch (comboBoxMode.SelectedIndex)
            {
                case 0: AR6000aor.ModeSet(0); break;
                case 1: AR6000aor.ModeSet(1); break;
                case 2: AR6000aor.ModeSet(2); break;
                case 3: AR6000aor.ModeSet(3); break;
                case 4: AR6000aor.ModeSet(4); break;
                case 5: AR6000aor.ModeSet(5); break;
                case 6: AR6000aor.ModeSet(6); break;
                case 7: AR6000aor.ModeSet(7); break;
                case 8: AR6000aor.ModeSet(8); break;
                case 9: AR6000aor.ModeSet(21); break;
                case 10: AR6000aor.ModeSet(22); break;
                case 11: AR6000aor.ModeSet(23); break;
                case 12: AR6000aor.ModeSet(24); break;
                case 13: AR6000aor.ModeSet(25); break;
                case 14: AR6000aor.ModeSet(26); break;
                case 15: AR6000aor.ModeSet(27); break;
                case 16: AR6000aor.ModeSet(28); break;
                case 17: AR6000aor.ModeSet(29); break;
                case 18: AR6000aor.ModeSet(30); break;
                case 19: AR6000aor.ModeSet(31); break;
                case 20: AR6000aor.ModeSet(32); break;
                case 21: AR6000aor.ModeSet(33); break;
                case 22: AR6000aor.ModeSet(34); break;
                case 23: AR6000aor.ModeSet(35); break;
                default: MessageBox.Show("case not worked!"); break;
            }
            Thread.Sleep(20);
            AR6000aor.ModeGet();
            AR6000aor.BandwidthGet();

        }
        private void comboBoxLPF_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (zapolneno == false) return;
            FlashLedWriteARONE();
            switch (comboBoxLPF.SelectedIndex)
            {
                case 0: AR6000aor.LowPassFiltrSet('0'); break;
                case 1: AR6000aor.LowPassFiltrSet('1'); break;
                case 2: AR6000aor.LowPassFiltrSet('2'); break;
                default: MessageBox.Show("case not worked!"); break;
            }
            Thread.Sleep(20);
            AR6000aor.LowPassFiltrGet();
        }
        private void comboBoxHPF_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (zapolneno == false) return;
            FlashLedWriteARONE();
            switch (comboBoxHPF.SelectedIndex)
            {
                case 0: AR6000aor.HighPassFiltrSet('0'); break;
                case 1: AR6000aor.HighPassFiltrSet('1'); break;
                case 2: AR6000aor.HighPassFiltrSet('2'); break;
                default: MessageBox.Show("case not worked!"); break;
            }
            Thread.Sleep(20);
            AR6000aor.HighPassFiltrGet();
        }
        private void b1_Click(object sender, EventArgs e)
        {
            try
            {
                if (countafterdot > 2) return;
                if (!tmFlashFRQ.Enabled)
                    tmFlashFRQ.Start();
                lFRQFromAOR.Visible = false;
                lFrq.Visible = true;
                lFrq.ForeColor = Color.Green;
                if (FrqToAOR == "")
                {
                    FrqToAOR = "1";
                    lFrq.Text = FrqToAOR.ToString() + kHz; //" кГц";
                    return;
                }
                if ((DotInFrq) && (countafterdot == 0))
                {
                    FrqToAOR += "1";
                    countafterdot++;
                    lFrq.Text = FrqToAOR.ToString() + kHz; // " кГц";
                    return;
                }

                if (double.Parse(FrqToAOR) < 3299999)
                {
                    if (DotInFrq) countafterdot++;
                    FrqToAOR += "1";
                    if (double.Parse(FrqToAOR) > 3299999) FrqToAOR = FrqToAOR.Substring(0, FrqToAOR.Length - 1);
                }
                lFrq.Text = FrqToAOR.ToString() + "  kHz; //кГц";
            }
            catch (Exception) { }
        }
        private void b2_Click(object sender, EventArgs e)
        {
            try
            {
                if (countafterdot > 2) return;
                if (!tmFlashFRQ.Enabled)
                    tmFlashFRQ.Start();
                lFRQFromAOR.Visible = false;
                lFrq.Visible = true;
                lFrq.ForeColor = Color.Green;
                if (FrqToAOR == "")
                {
                    FrqToAOR = "2";
                    lFrq.Text = FrqToAOR.ToString() + kHz; //" кГц";
                    return;
                }
                if ((DotInFrq) && (countafterdot == 0))
                {
                    FrqToAOR += "2";
                    countafterdot++;
                    lFrq.Text = FrqToAOR.ToString() + kHz; //" кГц";
                    return;
                }

                if (double.Parse(FrqToAOR) < 3299999)
                {
                    if (DotInFrq) countafterdot++;
                    FrqToAOR += "2";
                    if (double.Parse(FrqToAOR) > 3299999) FrqToAOR = FrqToAOR.Substring(0, FrqToAOR.Length - 1);
                }
                lFrq.Text = FrqToAOR.ToString() + kHz; // " кГц";
            }
            catch (Exception) { }
        }
        private void b3_Click(object sender, EventArgs e)
        {
            try
            {
                if (countafterdot > 2) return;
                if (!tmFlashFRQ.Enabled)
                    tmFlashFRQ.Start();
                lFRQFromAOR.Visible = false;
                lFrq.Visible = true;
                lFrq.ForeColor = Color.Green;
                if (FrqToAOR == "")
                {
                    FrqToAOR = "3";
                    lFrq.Text = FrqToAOR.ToString() + kHz; //" кГц";
                    return;
                }
                if ((DotInFrq) && (countafterdot == 0))
                {
                    FrqToAOR += "3";
                    countafterdot++;
                    lFrq.Text = FrqToAOR.ToString() + kHz; //" кГц";
                    return;
                }

                if (double.Parse(FrqToAOR) < 3299999)
                {
                    if (DotInFrq) countafterdot++;
                    FrqToAOR += "3";
                    if (double.Parse(FrqToAOR) > 3299999) FrqToAOR = FrqToAOR.Substring(0, FrqToAOR.Length - 1);
                }
                lFrq.Text = FrqToAOR.ToString() + kHz; //" кГц";
            }
            catch (Exception) { }
        }
        private void b4_Click(object sender, EventArgs e)
        {
            try
            {
                if (countafterdot > 2) return;
                if (!tmFlashFRQ.Enabled)
                    tmFlashFRQ.Start();
                lFRQFromAOR.Visible = false;
                lFrq.Visible = true;
                lFrq.ForeColor = Color.Green;
                if (FrqToAOR == "")
                {
                    FrqToAOR = "4";
                    lFrq.Text = FrqToAOR.ToString() + kHz; //" кГц";
                    return;
                }
                if ((DotInFrq) && (countafterdot == 0))
                {
                    FrqToAOR += "4";
                    countafterdot++;
                    lFrq.Text = FrqToAOR.ToString() + kHz; //" кГц";
                    return;
                }

                if (double.Parse(FrqToAOR) < 3299999)
                {
                    if (DotInFrq) countafterdot++;
                    FrqToAOR += "4";
                    if (double.Parse(FrqToAOR) > 3299999) FrqToAOR = FrqToAOR.Substring(0, FrqToAOR.Length - 1);
                }
                lFrq.Text = FrqToAOR.ToString() + kHz; //" кГц";
            }
            catch (Exception) { }
        }
        private void b5_Click(object sender, EventArgs e)
        {
            try
            {
                if (countafterdot > 2) return;
                if (!tmFlashFRQ.Enabled)
                    tmFlashFRQ.Start();
                lFRQFromAOR.Visible = false;
                lFrq.Visible = true;
                lFrq.ForeColor = Color.Green;
                if (FrqToAOR == "")
                {
                    FrqToAOR = "5";
                    lFrq.Text = FrqToAOR.ToString() + kHz; //" кГц";
                    return;
                }
                if ((DotInFrq) && (countafterdot == 0))
                {
                    FrqToAOR += "5";
                    countafterdot++;
                    lFrq.Text = FrqToAOR.ToString() + kHz; //" кГц";
                    return;
                }

                if (double.Parse(FrqToAOR) < 3299999)
                {
                    if (DotInFrq) countafterdot++;
                    FrqToAOR += "5";
                    if (double.Parse(FrqToAOR) > 3299999) FrqToAOR = FrqToAOR.Substring(0, FrqToAOR.Length - 1);
                }
                lFrq.Text = FrqToAOR.ToString() + kHz; //" кГц";
            }
            catch (Exception) { }
        }
        private void b6_Click(object sender, EventArgs e)
        {
            try
            {
                if (countafterdot > 2) return;
                if (!tmFlashFRQ.Enabled)
                    tmFlashFRQ.Start();
                lFRQFromAOR.Visible = false;
                lFrq.Visible = true;
                lFrq.ForeColor = Color.Green;
                if (FrqToAOR == "")
                {
                    FrqToAOR = "6";
                    lFrq.Text = FrqToAOR.ToString() + kHz; //" кГц";
                    return;
                }
                if ((DotInFrq) && (countafterdot == 0))
                {
                    FrqToAOR += "6";
                    countafterdot++;
                    lFrq.Text = FrqToAOR.ToString() + kHz; //" кГц";
                    return;
                }

                if (double.Parse(FrqToAOR) < 3299999)
                {
                    if (DotInFrq) countafterdot++;
                    FrqToAOR += "6";
                    if (double.Parse(FrqToAOR) > 3299999) FrqToAOR = FrqToAOR.Substring(0, FrqToAOR.Length - 1);
                }
                lFrq.Text = FrqToAOR.ToString() + kHz; //" кГц";
            }
            catch (Exception) { }
        }
        private void b7_Click(object sender, EventArgs e)
        {
            try
            {
                if (countafterdot > 2) return;
                if (!tmFlashFRQ.Enabled)
                    tmFlashFRQ.Start();
                lFRQFromAOR.Visible = false;
                lFrq.Visible = true;
                lFrq.ForeColor = Color.Green;
                if (FrqToAOR == "")
                {
                    FrqToAOR = "7";
                    lFrq.Text = FrqToAOR.ToString() + kHz; //" кГц";
                    return;
                }
                if ((DotInFrq) && (countafterdot == 0))
                {
                    FrqToAOR += "7";
                    countafterdot++;
                    lFrq.Text = FrqToAOR.ToString() + kHz; //" кГц";
                    return;
                }

                if (double.Parse(FrqToAOR) < 3299999)
                {
                    if (DotInFrq) countafterdot++;
                    FrqToAOR += "7";
                    if (double.Parse(FrqToAOR) > 3299999) FrqToAOR = FrqToAOR.Substring(0, FrqToAOR.Length - 1);
                }
                lFrq.Text = FrqToAOR.ToString() + kHz; //" кГц";
            }
            catch (Exception) { }
        }
        private void b8_Click(object sender, EventArgs e)
        {
            try
            {
                if (countafterdot > 2) return;
                if (!tmFlashFRQ.Enabled)
                    tmFlashFRQ.Start();
                lFRQFromAOR.Visible = false;
                lFrq.Visible = true;
                lFrq.ForeColor = Color.Green;
                if (FrqToAOR == "")
                {
                    FrqToAOR = "8";
                    lFrq.Text = FrqToAOR.ToString() + kHz; // " кГц";
                    return;
                }
                if ((DotInFrq) && (countafterdot == 0))
                {
                    FrqToAOR += "8";
                    countafterdot++;
                    lFrq.Text = FrqToAOR.ToString() + kHz; // " кГц";
                    return;
                }

                if (double.Parse(FrqToAOR) < 3299999)
                {
                    if (DotInFrq) countafterdot++;
                    FrqToAOR += "8";
                    if (double.Parse(FrqToAOR) > 3299999) FrqToAOR = FrqToAOR.Substring(0, FrqToAOR.Length - 1);
                }
                lFrq.Text = FrqToAOR.ToString() + kHz; //" кГц";
            }
            catch (Exception) { }
        }
        private void b9_Click(object sender, EventArgs e)
        {
            if (countafterdot > 2) return;
            if (!tmFlashFRQ.Enabled)
                tmFlashFRQ.Start();
            lFRQFromAOR.Visible = false;
            lFrq.Visible = true;
            lFrq.ForeColor = Color.Green;
            if (FrqToAOR == "")
            {
                FrqToAOR = "9";
                lFrq.Text = FrqToAOR.ToString() + kHz; // " кГц";
                return;
            }
            if ((DotInFrq) && (countafterdot == 0))
            {
                FrqToAOR += "9";
                countafterdot++;
                lFrq.Text = FrqToAOR.ToString() + kHz; //" кГц";
                return;
            }

            if (double.Parse(FrqToAOR) < 3299999)
            {
                if (DotInFrq) countafterdot++;
                FrqToAOR += "9";
                if (double.Parse(FrqToAOR) > 3299999) FrqToAOR = FrqToAOR.Substring(0, FrqToAOR.Length - 1);
            }
            lFrq.Text = FrqToAOR.ToString() + kHz; //" кГц";
        }
        private void b0_Click(object sender, EventArgs e)
        {
            if (DotInFrq) countafterdot++;
            if (countafterdot > 3) return;
            if (!tmFlashFRQ.Enabled)
                tmFlashFRQ.Start();
            lFRQFromAOR.Visible = false;
            lFrq.Visible = true;
            lFrq.ForeColor = Color.Green;
            if (FrqToAOR == "")
            {
                FrqToAOR = "";
                lFrq.Text = "0 " + kHz; //кГц";
                return;
            }
            if (double.Parse(FrqToAOR) < 330000)
                FrqToAOR += "0";
            lFrq.Text = FrqToAOR.ToString() + kHz; //" кГц";
        }
        private void bDot_Click(object sender, EventArgs e)
        {
            if (DotInFrq == true) return;
            if (countafterdot > 3) return;
            DotInFrq = true;
            countafterdot = 0;
            if (!tmFlashFRQ.Enabled)
                tmFlashFRQ.Start();
            lFRQFromAOR.Visible = false;
            lFrq.Visible = true;
            lFrq.ForeColor = Color.Green;
            FrqToAOR += ",";
            lFrq.Text = FrqToAOR.ToString() + "0" + kHz; // кГц";

        }
        private void bESC_Click(object sender, EventArgs e)
        {
            DotInFrq = false;
            countafterdot = 0;
            if (tmFlashFRQ.Enabled)
                tmFlashFRQ.Stop();
            lFRQFromAOR.Visible = true;
            lFrq.Visible = false;
            FrqToAOR = "";
            lFrq.Text = FrqToAOR.ToString();
        }
        private void bENT_Click(object sender, EventArgs e)
        {
            double frqq = 0;
            if ((DotInFrq) && (countafterdot == 0))
            {
                DotInFrq = false;
                countafterdot = 0;
                if (tmFlashFRQ.Enabled)
                    tmFlashFRQ.Stop();
                lFRQFromAOR.Visible = true;
                lFrq.Visible = false;
                FlashLedWriteARONE();
                double.TryParse(FrqToAOR.Substring(0, FrqToAOR.Length - 1), out frqq);
                if ((frqq > 30) && (frqq < 3300000)) AR6000aor.FrequencySet(frqq / 1000);
                FrqToAOR = "";
                lFrq.Text = FrqToAOR.ToString();
                return;
            }


            DotInFrq = false;
            countafterdot = 0;
            if (tmFlashFRQ.Enabled)
                tmFlashFRQ.Stop();
            lFRQFromAOR.Visible = true;
            lFrq.Visible = false;
            FlashLedWriteARONE();
            double.TryParse(FrqToAOR, out frqq);
            if ((frqq > 30) && (frqq < 3300000)) AR6000aor.FrequencySet(frqq / 1000);
            FrqToAOR = "";
            lFrq.Text = FrqToAOR.ToString();
        }
        private void trackBarManualGain_Scroll(object sender, EventArgs e)
        {
            try
            {
                FlashLedWriteARONE();
                AR6000aor.RFGainSet(int.Parse(trackBarManualGain.Value.ToString()));
            }
            catch { }
        }
        //private void trackBarRFGain_Scroll(object sender, EventArgs e)
        //{
        //    //classLibrary_ARONE.RFGainSet(trackBarRFGain.Value);
        //}
        private void tmFlashFRQ_Tick(object sender, EventArgs e)
        {
            if (lFrq.ForeColor != Color.Green)
                lFrq.ForeColor = Color.Green;
            else
                lFrq.ForeColor = Color.Black;

        }
        int Poisk_chastoty_v_tabl(double freq, DataGridView DGV)
        {
            try
            {
                if (DGV.RowCount < 1) { return -1; }
                for (int i = 0; i < DGV.RowCount; i++)
                {
                    if (DGV.Rows[i].Cells[2].Value == null) return -1;
                    if (double.Parse(DGV.Rows[i].Cells[2].Value.ToString()) == freq) { return i; }
                    //if (DGV.Rows[i].Cells[2].Value.ToString().CompareTo(freq.ToString()) == 0) { return i; }
                }
                return -1;
            }
            catch
            {
                return -1;
            }
        }
        void SignalYResized_Clear()
        {
            try
            {
                for (int j = 0; j < SignalYResized.Length; j++)
                {
                    SignalYResized[j] = -150;
                }
            }
            catch (Exception) { }
        }
        void Intensity_Clear()
        {
            try
            {
                for (int j = 0; j < WaterflowTime; j++)
                {
                    for (int i = 0; i < intensity.Length / WaterflowTime; i++)
                    {
                        intensity[i, j] = -150;
                    }
                }
            }
            catch (Exception) { }
        }
        void waveIn_DataAvailable(object sender, WaveInEventArgs e)
        {
            if (bAOR.BackColor == Color.Red) return;
            //return;
            if (this.InvokeRequired)
            {
                this.BeginInvoke(new EventHandler<WaveInEventArgs>(waveIn_DataAvailable), sender, e);
            }
            else
            {
                if (writer != null) writer.WriteData(e.Buffer, 0, e.BytesRecorded);  //запись в файл
                byte[] buffer = e.Buffer;
                int bytesRecorded = e.BytesRecorded;
                Complex[] sig = new Complex[bytesRecorded / 2];
                for (int i = 0, j = 0; i < e.BytesRecorded; i += 2, j++)
                {
                    short sample = (short)((buffer[i + 1] << 8) | buffer[i + 0]);
                    sig[j] = sample / 32768f;
                    //link.AddLast(sig[j].Real.ToString());         //для осциллограммы
                    //if (link.Count > 10000)
                    //{
                    //    link.RemoveFirst();
                    //}
                }
                Transform.FourierForward(sig, FourierOptions.Matlab);

                foreach (Complex c in sig)
                {
                    if (max.Magnitude < c.Magnitude)
                    {
                        max = c;
                    }
                }

                for (int j = 0; j < bytesRecorded / 2; j++)
                {
                    sig1[j + iAverage * bytesRecorded] = sig[j];
                }
                iAverage++;
                if (iAverage == 5) { iAverage = 0; }

                CreateGraph2(sig, max);
            }
        }
        private void CreateGraph2(Complex[] sig, Complex max1)
        {
            int kol_previshenij = 0;
            try
            {
                if (InvokeRequired)
                {
                    Invoke((MethodInvoker)(() => CreateGraph2(sig, max1)));
                    return;
                }
                SignalYResized_Clear();
                qwe = 0;                             //точка сигнала на графике
                sig_sum = 0;                         //среднее за 5 отсчетов
                //double K = sig.Length / 2;
                for (int i = 0; i < K; i++)
                {
                    qwe = (Complex.Abs(sig[i]) * 1000000) / (N * 115 * max1.Magnitude);
                    qwe = 20 * Math.Log10(qwe);
                    SignalY[i] = qwe;
                    PorogObnar[i] = PorogZapisi;
                    if (iNumDataMx == count_baund) { iNumDataMx = 0; }
                    if (iNumDataSigma == count_baund) { iNumDataSigma = 0; }
                }

                for (int j = 0; j < sig.Length; j++)
                {
                    sig1[j + iAverage * sig.Length] = sig[j];
                }
                iAverage++;
                if (iAverage == 5) { iAverage = 0; }
                for (int i = 0; i < K; i++)
                {
                    sig_sum = ((
                        Complex.Abs(sig1[i + sig.Length * 0]) +
                        Complex.Abs(sig1[i + sig.Length * 1]) +
                        Complex.Abs(sig1[i + sig.Length * 2]) +
                        Complex.Abs(sig1[i + sig.Length * 3]) +
                        Complex.Abs(sig1[i + sig.Length * 4])) * 1000000) / (5 * N * 115 * max1.Magnitude);
                    sig_sum = 20 * Math.Log10(sig_sum);
                    SignalYAverage[i] = sig_sum;
                    //SignalYAverage[i] = Complex.FromPolarCoordinates(Math.Sqrt(K * K + sig_sum * sig_sum), Math.Atan2(sig_sum, K));
                    //if (i > 10 && i < 1000 && sig_sum > int.Parse(textBox3.Text)) { kol_previshenij++; }    // условие записи по порогу


                    if (i > 10 && i < 1000 && sig_sum > PorogZapisi) { kol_previshenij++; }    // условие записи по порогу
                }
                int ii = 0;
                for (int i = 0; i < K; i++)
                {
                    ii = i / delitel;
                    if (ii < SignalYResized.Length)
                        if (SignalYResized[ii] < SignalY[i]) SignalYResized[ii] = SignalY[i];
                }

                waterflow_count++;
                if (waterflow_count == Nwaterflow_count)
                {
                    waterflow_count = 0;
                }
                if (cbAverage.Checked) PlotAverage.PlotY(SignalYAverage, 0, Fn / K);
                PlotSignal.PlotY(SignalY, 0, Fn / K);
                PlotPorogObn.PlotY(PorogObnar, 0, Fn / K);
                //waveformPlot1.PlotY(SignalYResized,0, Fn / K);            //по ним рисуется водопад
                if (waterflow_count == 0)
                {
                    for (int i = 0; i < 2205 / delitel; i++)
                    {
                        for (int j = 0; j < WaterflowTime; j++)
                        {
                            if (j < WaterflowTime - 1) intensity[i, j] = intensity[i, j + 1];       // != 1я строчка
                            else
                            {
                                intensity[i, j] = SignalYResized[i]; //SignalY[i];//               // == 1я строчка

                                if (intensity[i, j] >= spektorYMaxPorog)
                                    intensity[i, j] = 0;
                                if (intensity[i, j] <= spektorYMinPorog)
                                    intensity[i, j] = -120;
                                if ((intensity[i, j] > spektorYMinPorog) & (intensity[i, j] < spektorYMaxPorog))
                                    intensity[i, j] = intensity[i, j] * (intensity[i, j] - spektorYMaxPorog) / (spektorYMinPorog - spektorYMaxPorog);
                            }
                        }
                    }
                    //if (cbIntensityPaint.Checked)
                        //intensityPlot1.Plot(intensity);

                    //if (cbIntensityPaint.Checked)
                    //{
                    //    if (TaskIntensityPaint == null)
                    //    {
                    //        TaskIntensityPaint = Task.Run(() => { intensityPlot1.Plot(intensity); });
                    //    }
                    //    else
                    //    {
                    //        if (TaskIntensityPaint.IsCompleted)
                    //        {
                    //            // ona zakonchena
                    //            TaskIntensityPaint = Task.Run(() => { intensityPlot1.Plot(intensity); });
                    //        }
                    //        else
                    //        {
                    //            // one eshe rabotaet
                    //        }
                    //    }
                    //}
                }


                //автоматическая запись *.wav
                if (cbARec.Checked)
                {
                    if (kol_previshenij_dlia_zapisi_time_count_i < kol_buf_ostanovka_zapisi)
                    {
                        if ((kol_previshenij > kol_previshenij_dlia_zapisi))
                        {
                            kol_previshenij_dlia_zapisi_time_count_i++;
                            if (kol_previshenij_dlia_zapisi_time_count_i > kol_previshenij_dlia_zapisi_time_count)
                            {
                                if (cbRec.Checked == false)
                                {
                                    // if(tmBaraban.Enabled)   tmBaraban.Stop();
                                    cbRec.Checked = true;                        //zapisyvat
                                }
                            }
                            else
                            {
                                //kol_previshenij_dlia_zapisi_time_count_i = 0;   //конец записи
                                // cbRec.Checked = false;
                                //if (tmBaraban.Enabled) tmBaraban.Start();
                            }
                        }

                        else
                        {
                            //if (kol_previshenij_dlia_zapisi_time_count_i > 5) { kol_previshenij_dlia_zapisi_time_count_i -= 5; }
                            //kol_buf_ostanovka_zapisi++;
                            // if (kol_buf_ostanovka_zapisi > 0)
                            // {
                            //   kol_buf_ostanovka_zapisi = 0;
                            //kol_previshenij_dlia_zapisi_time_count_i = 0;
                            cbRec.Checked = false;
                            //}
                        }
                    }
                    else
                    {
                        kol_previshenij_dlia_zapisi_time_count_i = 0;
                        cbRec.Checked = false;
                    }
                }


            }
            catch (Exception) { }
        }
        async void ThrWaterflowPaint()
        {
            while (!closing)
            {
                if (cbIntensityPaint.Checked)
                    try
                    {
                        await Task.Delay(100);
                        ThrIntensityGraphN++;
                        if (ThrIntensityGraphN > ThrIntensityGraphNeedCount)
                        {
                            this.intensityGraph1.BeginInvoke((MethodInvoker)(() => this.intensityPlot1.Plot(intensity)));
                            ThrIntensityGraphN = 0;
                            await Task.Delay(5);
                        }
                    }
                    catch { }
            }
        }
        private void tbTableTime_KeyPress(object sender, KeyPressEventArgs e)
        {
            //if (tbTableTime.Text.Length > 8)
            //{
            //    tbTableTime.Text = tbTableTime.Text.Substring(0, 8);
            //    return;
            //}

            //if ((e.KeyChar >= '0') && (e.KeyChar <= '9') || (e.KeyChar == '\b') || (e.KeyChar == ':')) // цифра, Backspace
            //{
            //    return;
            //}

            //// остальные символы запрещены
            //e.Handled = true;  
        }
        private void tbTableLevel_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= '0') && (e.KeyChar <= '9') || (e.KeyChar == '\b') || (e.KeyChar == '-')) // цифра, Backspace
            {
                return;
            }
            e.Handled = true;
        }
        private void tbTableFRQ_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= '0') && (e.KeyChar <= '9') || (e.KeyChar == '\b') || (e.KeyChar == ',')) // цифра, Backspace
            {
                return;
            }
            e.Handled = true;

        }

        private void nudTableFRQ_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= '0') && (e.KeyChar <= '9') || (e.KeyChar == '\b') || (e.KeyChar == ',')) // цифра, Backspace
            {
                return;
            }
            e.Handled = true;

        }
        private void nudTableLevel_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= '0') && (e.KeyChar <= '9') || (e.KeyChar == '\b') || (e.KeyChar == '-')) // цифра, Backspace
            {
                return;
            }
            e.Handled = true;

        }
        private void nudTableFRQ_KeyUp(object sender, KeyEventArgs e)
        {
            Int64 qwe;
            Int64.TryParse(nudTableFRQ.Text.ToString(), out qwe);
            if (qwe > 3300000) nudTableFRQ.Text = nudTableFRQ.Text.Substring(0, 7);
        }
        private void nudTableLevel_Validated(object sender, EventArgs e)
        {
            int qwe;
            int.TryParse(nudTablePorogObnar.Text.ToString(), out qwe);
            if ((Math.Abs(qwe)) > 120)
            {
                nudTablePorogObnar.Text = "-120";
                return;
            }
        }
        private void tm500_Tick(object sender, EventArgs e)
        {
            if (bAOR.BackColor != Color.Green) return;
            FlashLedWriteARONE();
            AR6000aor.SignalLevelGet();
        }
        private void textBoxFrq_Validated(object sender, EventArgs e)
        {

        }
        private void buttn8_Click(object sender, EventArgs e)
        {
            lFRQFromAOR.Text = (123456789.9).ToString("N", nfi);

        }
        private void intensityGraph1_Resize(object sender, EventArgs e)
        {
            intensityGraph1Resize();
        }
        private void intensityGraph1Resize()
        {
            resolutionX = intensityGraph1.Size.Width;
            resolutionY = intensityGraph1.Size.Height;

            delitel = 2205 / resolutionX;
            if (delitel < 2) delitel = 2;
            if (delitel > 10) delitel = 10;
            intensity = new double[2205 / delitel, WaterflowTime];
            SignalYResized = new double[2205 / delitel];
            SignalYResized_Clear();
            Intensity_Clear();
            intensityXAxis1.Range = new NationalInstruments.UI.Range(0, (int)(2205 / delitel));
        }
        private void cbAverage_Click(object sender, EventArgs e)
        {
            if (!cbAverage.Checked)
            {
                PlotAverage.ClearData();
                cbAverage.Image = Properties.Resources.average_blue;
            }
            else
            {
                cbAverage.Image = Properties.Resources.average_gray;
            }
        }
        private void cbACP_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (cbACP.Checked)
                {
                    cbACP.Image = Properties.Resources.stop;
                    waveIn.DataAvailable += waveIn_DataAvailable;   //Прикрепляем к событию DataAvailable обработчик, возникающий при наличии записываемых данных
                }
                else
                {
                    waveIn.DataAvailable -= waveIn_DataAvailable;
                    cbACP.Image = Properties.Resources.play;
                }
            }
            catch (Exception) { }
        }
        private void cbIntensityPaint_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (cbIntensityPaint.Checked)
                {
                    cbIntensityPaint.Image = Properties.Resources.stop;
                }
                else
                {
                    cbIntensityPaint.Image = Properties.Resources.play;
                }
            }
            catch (Exception) { }
        }
        //private void cbRec_CheckedChanged(object sender, EventArgs e)
        //{
        //    if (bAOR.BackColor != Color.Green) return;      //?=connected
        //    try
        //    {
        //        lREC.Visible = true;
        //        taimer_zapisi = 0;
        //        if (cbRec.Checked)
        //        {
        //            cbRec.Image = Properties.Resources.rec_red;
        //            string outputFilename = String.Format("{0}{1}-{2}-({3}-{4}-{5}){6}", "wav\\", classLibrary_ARONE.ArOne.frequency.ToString(), DateTime.Now.DayOfYear, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, ".wav");
        //            lREC.Text = "REC " + outputFilename.Substring(4, outputFilename.Length - 4);
        //            writer = new WaveFileWriter(outputFilename, waveIn.WaveFormat);
        //        }
        //        else
        //        {
        //            lREC.Visible = false;
        //            cbRec.Image = Properties.Resources.rec_grey;
        //            if (writer != null)
        //            {
        //                writer.Close();
        //                writer = null;
        //            }
        //        }
        //    }
        //    catch (Exception) { }

        //}
        private void cbRec_CheckedChanged(object sender, EventArgs e)
        {
            if (bAOR.BackColor != Color.Green) return;      //?=connected
            try
            {
                lREC.Visible = true;
                taimer_zapisi = 0;
                if (cbRec.Checked)
                {
                    cbRec.Image = Properties.Resources.rec_red;


                    string path = String.Format(Application.StartupPath + "\\wav\\" + DateTime.Now.Year.ToString("0000") + "-" + DateTime.Now.Month.ToString("00") + "-" + DateTime.Now.Day.ToString("00"));
                    if (!Directory.Exists(path))
                    {
                        DirectoryInfo di = Directory.CreateDirectory(path);
                    }

                    //string outputFilename = String.Format("{0}{1}-{2}-{3}{4}{5}-{6}:{7}:{8}{9}", "wav\\", DateTime.Now.Year.ToString("0000"), DateTime.Now.Month.ToString("00"), DateTime.Now.Day.ToString("00"), "\\", classLibrary_ARONE.ArOne.frequency.ToString(), DateTime.Now.Hour.ToString("00"), DateTime.Now.Minute.ToString("00"), DateTime.Now.Second.ToString("00"), ".wav");
                    string outputFilename = path + "\\" + AR6000aor.ArOne.frequency.ToString() + "_" + DateTime.Now.Hour.ToString("00") + "-" + DateTime.Now.Minute.ToString("00") + "-" + DateTime.Now.Second.ToString("00") + ".wav";


                    lREC.Text = "REC " + outputFilename.Substring(path.Length + 1, outputFilename.Length - path.Length - 5);
                    //outputFilename=outputFilename.Replace("\\","\");
                    writer = new WaveFileWriter(outputFilename, waveIn.WaveFormat);
                }//wav\\98900000-180-(17-38-33).wav
                //D:\\mywork\\Main\\WindowsFormsApplication1\\WindowsFormsApplication1\\bin\\Debug\\wav\\2017-06-29\\98900000-17:39:14.wav
                else
                {
                    lREC.Visible = false;
                    cbRec.Image = Properties.Resources.rec_grey;
                    if (writer != null)
                    {
                        writer.Close();
                        writer = null;
                    }
                }
            }
            catch (Exception) { }

        }
        private void cbARec_CheckedChanged(object sender, EventArgs e)
        {

            if (bAOR.BackColor != Color.Green) return;      //?=connected
            try
            {
                if (cbARec.Checked)
                {
                    cbARec.Image = Properties.Resources.a_rec_red;
                }
                else
                {
                    cbARec.Image = Properties.Resources.a_rec_grey;
                    cbRec.Checked = false;
                }
            }
            catch (Exception) { }
        }
        private void bTableDel_Click(object sender, EventArgs e)
        {
            if (dgv.CurrentRow == null) return;
            int ID = int.Parse(dgv.CurrentRow.Cells[0].Value.ToString());//= int.Parse(table_AR_ONE1.dgvAR_ONE.dgv.CurrentRow.Cells[0].Value.ToString());
            functionsDB.DeleteOneRecordDB(ID, NameTable.AR6000, 0);
            zagruzka_tablFromBD(NameTable.AR6000.ToString());
        }
        private void bTableAdd_Click(object sender, EventArgs e)
        {
            int val = Poisk_chastoty_v_tabl((double.Parse(nudTableFRQ.Value.ToString())), dgv);
            if (val == -1)
            {
                StrAR6000.iFreq = (int)(nudTableFRQ.Value * 100);
                StrAR6000.iU = (int)nudTablePorogObnar.Value;
                StrAR6000.bPriority = (byte)cbTablePriority.SelectedIndex;
                StrAR6000.iAttenuator = comboBoxAtt.SelectedIndex;
                StrAR6000.iBW = comboBoxBW.SelectedIndex;
                StrAR6000.iHPF = comboBoxHPF.SelectedIndex;
                StrAR6000.iLPF = comboBoxLPF.SelectedIndex;

                if (comboBoxMode.SelectedIndex < 9) StrAR6000.iMode = comboBoxMode.SelectedIndex;
                else StrAR6000.iMode = comboBoxMode.SelectedIndex + 12;     
       
                StrAR6000.iOnOff = 1;
                StrAR6000.iPause = (int)nudPause.Value;
                StrAR6000.Note = tbNote.Text.ToString();
                StrAR6000.sTimeFirst = DateTime.Now.Hour.ToString("00") + DateTime.Now.Minute.ToString("00") + DateTime.Now.Second.ToString("00");
                sqLiteConnect = new System.Data.SQLite.SQLiteConnection(functionsDB.ConnectionString());             // подключиться к БД      
                sqLiteConnect.Open();
                sqLiteCommand = new System.Data.SQLite.SQLiteCommand(
                    "INSERT INTO AR6000(OnOff,Freq, Pause, U, TimeFirst, Attenuator, Mode, BW, HPF, LPF, Priority, Note) VALUES (" + StrAR6000.iOnOff + "," + StrAR6000.iFreq + "," + StrAR6000.iPause + "," + StrAR6000.iU + ",'" + StrAR6000.sTimeFirst + "'," + StrAR6000.iAttenuator + "," + StrAR6000.iMode + "," + StrAR6000.iBW + "," + StrAR6000.iHPF + "," + StrAR6000.iLPF + "," + StrAR6000.bPriority + ",'" + StrAR6000.Note.ToString() + "')", sqLiteConnect);
                int qwee = sqLiteCommand.ExecuteNonQuery();
                sqLiteConnect.Close();
                zagruzka_tablFromBD(NameTable.AR6000.ToString());
            }
            else
            {
                dgv.FirstDisplayedScrollingRowIndex = val;
                dgv.Rows[val].Selected = true;
 
            }
        }

       
        private void bTableChange_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgv.CurrentCell.Value == null) return;
                if (UpdateSaveToBD(int.Parse(dgv.CurrentRow.Cells[0].Value.ToString())) != 1) { return; }//если не получилось, то делать {тут}
                zagruzka_tablFromBD(NameTable.AR6000.ToString());
            }
            catch (Exception) { }
        }

        private int UpdateSaveToBD(int IDn)
        {
            StrAR6000.iFreq = int.Parse(((double)nudTableFRQ.Value * 100).ToString());
            StrAR6000.iU = (int)nudTablePorogObnar.Value;
            StrAR6000.bPriority = (byte)cbTablePriority.SelectedIndex;
            StrAR6000.iAttenuator = comboBoxAtt.SelectedIndex;
            StrAR6000.iBW = comboBoxBW.SelectedIndex;
            StrAR6000.iHPF = comboBoxHPF.SelectedIndex;
            StrAR6000.iLPF = comboBoxLPF.SelectedIndex;
            //StrAR6000.iMode = comboBoxMode.SelectedIndex;
            if (comboBoxMode.SelectedIndex < 9) StrAR6000.iMode = comboBoxMode.SelectedIndex;
            else StrAR6000.iMode = comboBoxMode.SelectedIndex+12;            
            bool tru = true;
            if (dgv.CurrentRow.Cells[1].Value.ToString() == tru.ToString())
                StrAR6000.iOnOff = 1;
            else
                StrAR6000.iOnOff = 0;
            StrAR6000.iPause = (int)nudPause.Value;
            StrAR6000.Note = tbNote.Text.ToString();
            StrAR6000.sTimeFirst = DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString();
            sqLiteConnect = new System.Data.SQLite.SQLiteConnection(functionsDB.ConnectionString());             // подключиться к БД      
            sqLiteConnect.Open();
            sqLiteCommand = new System.Data.SQLite.SQLiteCommand(
                "UPDATE AR6000 SET OnOff=" + StrAR6000.iOnOff + ", Freq=" + StrAR6000.iFreq + ", Note='" + StrAR6000.Note + "', Pause=" + StrAR6000.iPause + ", U=" + StrAR6000.iU + ", TimeFirst='" + StrAR6000.sTimeFirst + "', Attenuator=" + StrAR6000.iAttenuator + ", Mode=" + StrAR6000.iMode + ", BW=" + StrAR6000.iBW + ", HPF=" + StrAR6000.iHPF + ", LPF=" + StrAR6000.iLPF + ", Priority=" + StrAR6000.bPriority + " WHERE ID=" + IDn, sqLiteConnect);
            int qwee = sqLiteCommand.ExecuteNonQuery();
            sqLiteConnect.Close();
            return 1;
        }

        private void bTableClear_Click(object sender, EventArgs e)
        {
            functionsDB.DeleteAllRecordsDB(NameTable.AR6000, 0);
            zagruzka_tablFromBD(NameTable.AR6000.ToString());
        }
        private void tmSignalLevel_Tick(object sender, EventArgs e)
        {
            FlashLedWriteARONE();
            AR6000aor.SignalLevelGet();
        }
        private void tbWaterflowMax_Scroll(object sender, EventArgs e)
        {
            spektorYMaxPorog = tbWaterflowMax.Value;
        }
        private void tbWaterflowMin_Scroll(object sender, EventArgs e)
        {
            spektorYMinPorog = tbWaterflowMin.Value;
        }
        private void tbWaterflowMin_ValueChanged(object sender, EventArgs e)
        {
            if (tbWaterflowMin.Value < tbWaterflowMax.Value)
            {
                spektorYMinPorog = tbWaterflowMin.Value;
            }
            else { tbWaterflowMin.Value = tbWaterflowMax.Value - 1; }
        }
        private void tbWaterflowMax_ValueChanged(object sender, EventArgs e)
        {
            if (tbWaterflowMax.Value > tbWaterflowMin.Value)
            {
                spektorYMaxPorog = tbWaterflowMax.Value;
            }
            else { tbWaterflowMax.Value = tbWaterflowMin.Value + 1; }

        }
        private void pRecDev_Resize(object sender, EventArgs e)
        {
            cbIntensityPaint.Location = new Point(2, 200 + intensityGraph1.Location.Y);
        }
        public void zagruzka_tablFromBD(string table)
        {
            try
            {
                dgv.Rows.Clear();
                jsql = 0;
                if (functionsDB.ConnectionString() != "")
                {
                    sqLiteConnect = new System.Data.SQLite.SQLiteConnection(functionsDB.ConnectionString());             // подключиться к БД      
                    sqLiteConnect.Open();
                    sqLiteCommand = new System.Data.SQLite.SQLiteCommand("SELECT * FROM "+table, sqLiteConnect);
                    sqLiteRead = sqLiteCommand.ExecuteReader();
                    while (sqLiteRead.Read())
                    {
                        if (jsql >= dgv.RowCount)
                            dgv.Rows.Add(null, false, null, null, null, null, null, null, null, null, null, null, null);
                        dgv.Rows[jsql].Cells[0].Value = Convert.ToString(sqLiteRead["ID"]);
                        if (Convert.ToBoolean(sqLiteRead["OnOff"]) == false)
                            dgv.Rows[jsql].Cells[1].Value = false;
                        else dgv.Rows[jsql].Cells[1].Value = true;
                        dgv.Rows[jsql].Cells[2].Value = String.Format("{0:# ### #00.000}", Convert.ToDouble(sqLiteRead["Freq"]) / 100);//(Convert.ToDouble(sqLiteRead["Freq"])/100).ToString();
                        dgv.Rows[jsql].Cells[3].Value = Convert.ToString(sqLiteRead["Pause"]);
                        dgv.Rows[jsql].Cells[4].Value = Convert.ToString(sqLiteRead["U"]);
                        dgv.Rows[jsql].Cells[5].Value = Convert.ToString(sqLiteRead["TimeFirst"]).Insert(2, ":").Insert(5, ":");

                        val = Convert.ToInt32(sqLiteRead["Attenuator"]);
                        switch (val)
                        {
                            case 0: dgv.Rows[jsql].Cells[6].Value = UsilVKL; break;// "Усил. ВКЛ"; break;
                            case 1: dgv.Rows[jsql].Cells[6].Value = "0"+dB;break;// дБ"; break;
                            case 2: dgv.Rows[jsql].Cells[6].Value = "10" + dB; break;//  дБ"; break;
                            case 3: dgv.Rows[jsql].Cells[6].Value = "20" + dB; break;//  дБ"; break;
                            case 4: dgv.Rows[jsql].Cells[6].Value = Avto; break;// "Авто"; break;
                        }
                        val = Convert.ToInt32(sqLiteRead["Mode"]);
                        switch (val)
                        {
                            case 00: dgv.Rows[jsql].Cells[7].Value = "FM"; break;
                            case 01: dgv.Rows[jsql].Cells[7].Value = "FMST"; break;
                            case 02: dgv.Rows[jsql].Cells[7].Value = "AM"; break;
                            case 03: dgv.Rows[jsql].Cells[7].Value = "SAM"; break;
                            case 04: dgv.Rows[jsql].Cells[7].Value = "USB"; break;
                            case 05: dgv.Rows[jsql].Cells[7].Value = "LSB"; break;
                            case 06: dgv.Rows[jsql].Cells[7].Value = "CW"; break;
                            case 07: dgv.Rows[jsql].Cells[7].Value = "ISB"; break;
                            case 08: dgv.Rows[jsql].Cells[7].Value = "AIQ"; break;
                            case 21: dgv.Rows[jsql].Cells[7].Value = "WFM(100k)"; break;
                            case 22: dgv.Rows[jsql].Cells[7].Value = "WFM(200k)"; break;
                            case 23: dgv.Rows[jsql].Cells[7].Value = "FMST(200k)"; break;
                            case 24: dgv.Rows[jsql].Cells[7].Value = "NFM(15k)"; break;
                            case 25: dgv.Rows[jsql].Cells[7].Value = "SFM(6k)"; break;
                            case 26: dgv.Rows[jsql].Cells[7].Value = "WAM(15k)"; break;
                            case 27: dgv.Rows[jsql].Cells[7].Value = "AM(6k)"; break;
                            case 28: dgv.Rows[jsql].Cells[7].Value = "NAM(3k)"; break;
                            case 29: dgv.Rows[jsql].Cells[7].Value = "SAM(6k)"; break;
                            case 30: dgv.Rows[jsql].Cells[7].Value = "USB(3k)"; break;
                            case 31: dgv.Rows[jsql].Cells[7].Value = "LSB(3k)"; break;
                            case 32: dgv.Rows[jsql].Cells[7].Value = "CW1(0.5k)"; break;
                            case 33: dgv.Rows[jsql].Cells[7].Value = "CW2(0.2k)"; break;
                            case 34: dgv.Rows[jsql].Cells[7].Value = "ISB(6k)"; break;
                            case 35: dgv.Rows[jsql].Cells[7].Value = "AIQ(15k)"; break;
                        }
                        val = Convert.ToInt32(sqLiteRead["BW"]);
                        switch (val)
                        {
                            case 0: dgv.Rows[jsql].Cells[8].Value = "0.2 "+kHz;break;// кГц"; break;
                            case 1: dgv.Rows[jsql].Cells[8].Value = "0.5 " + kHz; break;//кГц"; break;
                            case 2: dgv.Rows[jsql].Cells[8].Value = "1 " + kHz; break;//кГц"; break;
                            case 3: dgv.Rows[jsql].Cells[8].Value = "3.0 " + kHz; break;//кГц"; break;
                            case 4: dgv.Rows[jsql].Cells[8].Value = "6.0 " + kHz; break;//кГц"; break;
                            case 5: dgv.Rows[jsql].Cells[8].Value = "15 " + kHz; break;//кГц"; break;
                            case 6: dgv.Rows[jsql].Cells[8].Value = "30 " + kHz; break;//кГц"; break;
                            case 7: dgv.Rows[jsql].Cells[8].Value = "100 " + kHz; break;//кГц"; break;
                            case 8: dgv.Rows[jsql].Cells[8].Value = "200 " + kHz; break;//кГц"; break;
                            case 9: dgv.Rows[jsql].Cells[8].Value = "300 " + kHz; break;//кГц"; break;
                        }
                        sval = Convert.ToString(sqLiteRead["HPF"]);
                        switch (sval)
                        {
                            case "0": dgv.Rows[jsql].Cells[9].Value = "300 " +Gc; break;//Гц"; break;
                            case "1": dgv.Rows[jsql].Cells[9].Value = "600 "+Gc;break;//Гц"; break;
                            case "2": dgv.Rows[jsql].Cells[9].Value = Vykl; break;// "ВЫКЛ"; break;
                        }
                        sval = Convert.ToString(sqLiteRead["LPF"]);
                        switch (sval)
                        {
                            case "0": dgv.Rows[jsql].Cells[10].Value = "3 " + kHz; break;//КГц"; break;
                            case "1": dgv.Rows[jsql].Cells[10].Value = "6 " + kHz; break;//КГц"; break;
                            case "2": dgv.Rows[jsql].Cells[10].Value = Vykl; break;// "ВЫКЛ"; break;
                        }
                        val = Convert.ToInt32(sqLiteRead["Priority"]);
                        switch (val)
                        {
                            case 0:
                                dgv.Rows[jsql].Cells[11].Value = Obychnyj;break;//"Обычный";
                                break;
                            case 1:
                                dgv.Rows[jsql].Cells[11].Value = Vazhnyj;break;//"Важный";
                                break;
                        }
                        dgv.Rows[jsql].Cells[12].Value = Convert.ToString(sqLiteRead["Note"]);
                        jsql++;

                    }

                    //fLoad = true;
                }
            }
            catch (Exception)
            {
                switch (NumberOfLanguage)
                {
                    case 0:
                        MessageBox.Show("Ошибка чтения данных таблицы!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.ServiceNotification);
                        break;
                    case 1:
                        MessageBox.Show("Ошибка чтения данных таблицы!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.ServiceNotification);
                        break;
                    case 2:
                        MessageBox.Show(OshibkaChtenijaDannyhTablicy,Oshibka, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.ServiceNotification);
                        break;
                    default:
                        break;
                }
                
                //fLoad = false;

                //return fLoad;
            }
            finally
            {
                sqLiteConnect.Close();
                if (dgv.RowCount < 7) dgv.RowCount = 6;
            }
        }


        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                //IDBaraban0Count = 0;
                //IDBaraban1Count = 0;
                //if (radioButton2.Checked)
                //{
                //    IDBaraban0 = new int[dgv.RowCount];
                //    IDBaraban1 = new int[dgv.RowCount];
                //    ZapolnitPrioritety();
                //    if (BarabanPriorityWorking) tmBaraban.Interval = int.Parse(dgv.Rows[IDBaraban1[0]].Cells[3].Value.ToString());
                //    else tmBaraban.Interval = int.Parse(dgv.Rows[IDBaraban0[0]].Cells[3].Value.ToString());
                //    if (IDBaraban0Count + IDBaraban1Count < 1)
                //    {
                //        radioButton1.Checked = true;
                //        tmBaraban.Enabled = false;
                //        return;
                //    }
                //    tmBaraban.Enabled = true;
                //    cbARec.Enabled = true;
                //}
                //else
                //{
                //    tmBaraban.Enabled = false;
                //    BarabanPriorityWorking = false;
                //    cbARec.Enabled = false;
                //    Baraban0Num = 0;
                //    Baraban1Num = 0;
                //    IDBaraban0Count = 0;
                //    IDBaraban1Count = 0;
                //}

                IDBaraban0Count = 0;
                IDBaraban1Count = 0;
                if (radioButton2.Checked)
                {
                    if (dgv.RowCount < 2)
                    {
                        radioButton1.Checked = true;
                        tmBaraban.Enabled = false; 
                        return;
                    }
                    int IDNum = dgv.CurrentRow.Index;
                    if (dgv.Rows[IDNum].Cells[0].Value == null)
                    {
                        radioButton1.Checked = true;
                        tmBaraban.Enabled = false;
                        return;
                    }
                    IDBaraban0 = new int[dgv.RowCount];
                    IDBaraban1 = new int[dgv.RowCount];
                    int countPrior = ZapolnitPrioritety();
                    if (countPrior == -1)
                    {
                        radioButton1.Checked = true;
                        tmBaraban.Enabled = false;
                        return;
                    }
                    tmBaraban.Interval = 100;
                    //if (BarabanPriorityWorking) tmBaraban.Interval = int.Parse(dgv.Rows[IDBaraban1[0]].Cells[3].Value.ToString());
                    //else tmBaraban.Interval = int.Parse(dgv.Rows[IDBaraban0[0]].Cells[3].Value.ToString());

                    tmBaraban.Enabled = true;
                    //cbARec.Enabled = true;
                }
                else
                {
                    tmBaraban.Enabled = false;
                    BarabanPriorityWorking = false;
                    Baraban0Num = 0;
                    Baraban1Num = 0;
                    IDBaraban0Count = 0;
                    IDBaraban1Count = 0;
                }
            }
            catch { }
        }
        private int ZapolnitPrioritety()
        {
            for (int i = 0; i < dgv.RowCount; i++)
            {
                try
                {
                    if (dgv.Rows[i].Cells[11].Value != null)
                    {
                        string qwe = dgv.Rows[i].Cells[11].Value.ToString().Trim();
                        string qwee = dgv.Rows[i].Cells[1].Value.ToString();
                        if (dgv.Rows[i].Cells[11].Value.ToString() == "Обычный" && dgv.Rows[i].Cells[1].Value.ToString() == "True")
                        {
                            IDBaraban0[IDBaraban0Count] = i;
                            IDBaraban0Count++;
                        }
                        if (dgv.Rows[i].Cells[11].Value.ToString() == "Важный" && dgv.Rows[i].Cells[1].Value.ToString() == "True")
                        {
                            IDBaraban1[IDBaraban1Count] = i;
                            IDBaraban1Count++;
                            BarabanPriorityWorking = true;
                        }
                    }
                }
                catch { }
            }
            int countZP = IDBaraban0Count + IDBaraban1Count;
            if (IDBaraban0Count * IDBaraban1Count == 0) countZP = (-1) * countZP;
            return countZP;
        }
        private void tmBaraban_Tick(object sender, EventArgs e)
        {
            if ((cbRec.Checked) & (taimer_zapisi < taimer_zapisi_max) & (radioButton2.Checked)) { return; }//если идет запись, то ждать
            if ((cbRec.Checked) & (taimer_zapisi >= taimer_zapisi_max) & (radioButton2.Checked))
            { taimer_zapisi = 0;  cbRec.Checked = false; }

            if (((IDBaraban0Count == 0) && (IDBaraban1Count == 0)) || (!radioButton2.Checked))
            {
                tmBaraban.Enabled = false;                //Программная перестройка ВЫКЛ";
                radioButton1.Checked = true;
                return;
            }
            //if (BarabanPriorityWorking) //если приоритетные
            //{
            //    Perestroika(IDBaraban1[Baraban1Num]);
            //    Baraban1Num++;
            //    if (Baraban1Num >= IDBaraban1Count)
            //    {
            //        Baraban1Num = 0;
            //        BarabanPriorityWorking = false;
            //    }
            //}
            //else    //если обычные
            //{
            //    Perestroika(IDBaraban0[Baraban0Num]);
            //    Baraban0Num++;
            //    BarabanPriorityWorking = true;
            //    if (Baraban0Num >= IDBaraban0Count)
            //    {
            //        Baraban0Num = 0;
            //        //BarabanPriorityWorking = true;
            //    }
            //}
            if ((IDBaraban0Count != 0) && (IDBaraban1Count != 0))
            {
                try
                {
                    if (BarabanPriorityWorking) //если приоритетные
                    {
                        Perestroika(IDBaraban1[Baraban1Num]);
                        Baraban1Num++;
                        if (Baraban1Num >= IDBaraban1Count)
                        {
                            Baraban1Num = 0;
                            BarabanPriorityWorking = false;
                        }
                    }
                    else    //если обычные
                    {
                        Perestroika(IDBaraban0[Baraban0Num]);
                        Baraban0Num++;
                        BarabanPriorityWorking = true;
                        if (Baraban0Num >= IDBaraban0Count)
                        {
                            Baraban0Num = 0;
                            //BarabanPriorityWorking = true;
                        }
                    }
                }
                catch { }
            }                                    
            if ((IDBaraban0Count * IDBaraban1Count) < 1)        //если с одинаковым приоритетом все
            {
                if (BarabanPriorityWorking)
                {
                    try
                    {
                        Perestroika(IDBaraban1[Baraban1Num]);
                        Baraban1Num++;
                        if (Baraban1Num >= IDBaraban1Count)
                        {
                            Baraban1Num = 0;
                        }
                    }
                    catch { }
                }
                else
                {
                    try
                    {
                        Perestroika(IDBaraban0[Baraban0Num]);
                        Baraban0Num++;
                        if (Baraban0Num >= IDBaraban0Count)
                        {
                            Baraban0Num = 0;
                        }
                    }
                    catch { }
                }
            }
        }
        private async void Perestroika(int IDNum)
        {
            try
            {
                dgv.FirstDisplayedScrollingRowIndex = IDNum;
                dgv.Rows[IDNum].Selected = true;
                if (dgv.Rows[IDNum].Cells[2].Value == null) return;
                double d = double.Parse(dgv.Rows[IDNum].Cells[2].Value.ToString()) / 1000;
                FlashLedWriteARONE();
                AR6000aor.FrequencySet(double.Parse(dgv.Rows[IDNum].Cells[2].Value.ToString()) / 1000); //перевод в MHz
                AR6000aor.FrequencyGet();
                tmBaraban.Interval = int.Parse(dgv.Rows[IDNum].Cells[3].Value.ToString());
                nudTablePorogObnar.Value = int.Parse(dgv.Rows[IDNum].Cells[4].Value.ToString());
                cbTablePriority.Text = dgv.Rows[IDNum].Cells[11].Value.ToString();
                tbNote.Text = dgv.Rows[IDNum].Cells[12].Value.ToString();
                PorogZapisi = int.Parse(dgv.Rows[IDNum].Cells[4].Value.ToString());
                FlashLedWriteARONE();
                sval = dgv.Rows[IDNum].Cells[6].Value.ToString();


                if (sval == UsilVKL) AR6000aor.AttenuatorSet(0);
                if (sval == "0 " + dB) AR6000aor.AttenuatorSet(1);
                if (sval == "10 " + dB) AR6000aor.AttenuatorSet(2);
                if (sval == "20 " + dB) AR6000aor.AttenuatorSet(3);
                if (sval ==Avto) AR6000aor.AttenuatorSet(4);
                //switch (sval.ToString()) 
                //{
                //    case UsilVKL://"Усил. ВКЛ": 0
                //        AR6000aor.AttenuatorSet(0);
                //        break;
                //    case "0 "+dB:// дБ": 1
                //        AR6000aor.AttenuatorSet(1);
                //        break;
                //    case "10 " + dB://дБ": 2
                //        AR6000aor.AttenuatorSet(2);
                //        break;
                //    case "20 " + dB://дБ": 3
                //        AR6000aor.AttenuatorSet(3);
                //        break;
                //    case Avto://"Авто": 4
                //        AR6000aor.AttenuatorSet(4);
                //        break;
                //}
                FlashLedWriteARONE();
                AR6000aor.AttenuatorGet();
                switch (dgv.Rows[IDNum].Cells[7].Value.ToString())
                {
                    case "FM": AR6000aor.ModeSet(00); break;
                    case "FMST": AR6000aor.ModeSet(01); break;
                    case "AM": AR6000aor.ModeSet(02); break;
                    case "SAM": AR6000aor.ModeSet(03); break;
                    case "USB": AR6000aor.ModeSet(04); break;
                    case "LSB": AR6000aor.ModeSet(05); break;
                    case "CW": AR6000aor.ModeSet(06); break;
                    case "ISB": AR6000aor.ModeSet(07); break;
                    case "AIQ": AR6000aor.ModeSet(08); break;
                    case "WFM(100k)": AR6000aor.ModeSet(21); break;
                    case "WFM(200k)": AR6000aor.ModeSet(22); break;
                    case "FMST(200k)": AR6000aor.ModeSet(23); break;
                    case "NFM(15k)": AR6000aor.ModeSet(24); break;
                    case "SFM(6k)": AR6000aor.ModeSet(25); break;
                    case "WAM(15k)": AR6000aor.ModeSet(26); break;
                    case "AM(6k)": AR6000aor.ModeSet(27); break;
                    case "NAM(3k)": AR6000aor.ModeSet(28); break;
                    case "SAM(6k)": AR6000aor.ModeSet(29); break;
                    case "USB(3k)": AR6000aor.ModeSet(30); break;
                    case "LSB(3k)": AR6000aor.ModeSet(31); break;
                    case "CW1(0.5k)": AR6000aor.ModeSet(32); break;
                    case "CW2(0.2k)": AR6000aor.ModeSet(33); break;
                    case "ISB(6k)": AR6000aor.ModeSet(34); break;
                    case "AIQ(15k)": AR6000aor.ModeSet(35); break;
                }
                FlashLedWriteARONE();
                AR6000aor.ModeGet();
                sval = dgv.Rows[IDNum].Cells[8].Value.ToString();
                if (sval == "0.2 " + kHz) AR6000aor.BandWidthSet(0);
                if (sval == "0.5 " + kHz) AR6000aor.BandWidthSet(1);
                if (sval =="1 " + kHz ) AR6000aor.BandWidthSet(2);
                if (sval == "3.0 " + kHz) AR6000aor.BandWidthSet(3);
                if (sval =="6.0 " + kHz ) AR6000aor.BandWidthSet(4);
                if (sval =="15 " + kHz) AR6000aor.BandWidthSet(5);
                if (sval =="30 " + kHz ) AR6000aor.BandWidthSet(6);
                if (sval == "100 " + kHz) AR6000aor.BandWidthSet(7);
                if (sval == "200 " + kHz) AR6000aor.BandWidthSet(8);
                if (sval == "300 " + kHz) AR6000aor.BandWidthSet(9);
                //switch (sval)
                //{
                //    case "0.2 "+kHz: AR6000aor.BandWidthSet(0); break;
                //    case "0.5 " + kHz: AR6000aor.BandWidthSet(1); break;
                //    case "1 " + kHz: AR6000aor.BandWidthSet(2); break;
                //    case "3.0 " + kHz: AR6000aor.BandWidthSet(3); break;
                //    case "6.0 " + kHz: AR6000aor.BandWidthSet(4); break;
                //    case "15 " + kHz: AR6000aor.BandWidthSet(5); break;
                //    case "30 " + kHz: AR6000aor.BandWidthSet(6); break;
                //    case "100 " + kHz: AR6000aor.BandWidthSet(7); break;
                //    case "200 " + kHz: AR6000aor.BandWidthSet(8); break;
                //    case "300 " + kHz: AR6000aor.BandWidthSet(9); break;
                //}
                FlashLedWriteARONE();
                AR6000aor.BandwidthGet();
                sval = dgv.Rows[IDNum].Cells[9].Value.ToString();


                if (sval == "300 " + Gc) AR6000aor.HighPassFiltrSet('0');
                if (sval == "600 " + Gc) AR6000aor.HighPassFiltrSet('1');
                //switch (sval)
                //{
                //    case "300 " + Gc: AR6000aor.HighPassFiltrSet('0'); break;
                //    case "600 " + Gc: AR6000aor.HighPassFiltrSet('1'); break;
                //    case Vykl: AR6000aor.HighPassFiltrSet('2'); break;
                //}
                FlashLedWriteARONE();
                AR6000aor.HighPassFiltrGet();
                sval = dgv.Rows[IDNum].Cells[10].Value.ToString();

                if (sval == "3 " + kHz) AR6000aor.LowPassFiltrSet('0');
                if (sval == "6 " + kHz) AR6000aor.LowPassFiltrSet('1');
                //switch (sval)
                //{
                //    case "3 " + kHz: AR6000aor.LowPassFiltrSet('0'); break;
                //    case "6 " + kHz: AR6000aor.LowPassFiltrSet('1'); break;
                //    case Vykl: AR6000aor.LowPassFiltrSet('2'); break;
                //}
                AR6000aor.LowPassFiltrGet();
            }
            catch { }
        }
        private void dgv_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (radioButton2.Checked)
                radioButton1.Checked = true;
            Perestroika(dgv.CurrentRow.Index);
        }

        private void dgv_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {// НЕ! настраивает приемник, только отображает данные с датагрида в тоолбоксы на форме
                int IDNum = dgv.CurrentRow.Index;
                if (dgv.Rows[IDNum].Cells[0].Value == null) return;
                nudTableFRQ.Value = decimal.Parse(dgv.Rows[IDNum].Cells[2].Value.ToString());
                nudTablePorogObnar.Value = int.Parse(dgv.Rows[IDNum].Cells[4].Value.ToString());
                cbTablePriority.Text = dgv.Rows[IDNum].Cells[11].Value.ToString();
                tbNote.Text = dgv.Rows[IDNum].Cells[12].Value.ToString();
                this.comboBoxAtt.SelectedIndexChanged -= new System.EventHandler(this.comboBoxAtt_SelectedIndexChanged);
                this.comboBoxBW.SelectedIndexChanged -= new System.EventHandler(this.comboBoxBW_SelectedIndexChanged);
                this.comboBoxMode.SelectedIndexChanged -= new System.EventHandler(this.comboBoxMode_SelectedIndexChanged);
                this.comboBoxHPF.SelectedIndexChanged -= new System.EventHandler(this.comboBoxHPF_SelectedIndexChanged);
                this.comboBoxLPF.SelectedIndexChanged -= new System.EventHandler(this.comboBoxLPF_SelectedIndexChanged);
                this.tmBaraban.Tick -= new System.EventHandler(this.tmBaraban_Tick);
                comboBoxAtt.Text = dgv.Rows[IDNum].Cells[6].Value.ToString();
                nudPause.Value = int.Parse(dgv.Rows[IDNum].Cells[3].Value.ToString());
                comboBoxMode.Text = dgv.Rows[IDNum].Cells[7].Value.ToString();
                comboBoxBW.Text = dgv.Rows[IDNum].Cells[8].Value.ToString();
                comboBoxHPF.Text = dgv.Rows[IDNum].Cells[9].Value.ToString();
                comboBoxLPF.Text = dgv.Rows[IDNum].Cells[10].Value.ToString();
                this.comboBoxAtt.SelectedIndexChanged += new System.EventHandler(this.comboBoxAtt_SelectedIndexChanged);
                this.comboBoxBW.SelectedIndexChanged += new System.EventHandler(this.comboBoxBW_SelectedIndexChanged);
                this.comboBoxMode.SelectedIndexChanged += new System.EventHandler(this.comboBoxMode_SelectedIndexChanged);
                this.comboBoxHPF.SelectedIndexChanged += new System.EventHandler(this.comboBoxHPF_SelectedIndexChanged);
                this.comboBoxLPF.SelectedIndexChanged += new System.EventHandler(this.comboBoxLPF_SelectedIndexChanged);
                this.tmBaraban.Tick += new System.EventHandler(this.tmBaraban_Tick);
            }
            catch { }

        }


        private void dgv_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgv.CurrentCell.ColumnIndex == 1)
                if (radioButton2.Checked) radioButton1.Checked = true;
        }

        public void bAOR_Click(object sender, EventArgs e)
        {
            try
            {
                if (bAOR.BackColor == Color.Green)
                {
                    try
                    {
                        AR6000aor.ClosePort();
                    }
                    catch { }
                }
                else
                {
                    try
                    {
                        AR6000aor.OpenPort(Com_ArOne);
                        Ask_Arone();
                    }
                    catch { }
                }
            }
            catch (Exception) { }

        }

        private void changePorogZapisi(int qwe)
        {
            PorogZapisi = qwe;

        }
        private void nudTablePorogObnar_ValueChanged(object sender, EventArgs e)
        {
            changePorogZapisi((int)nudTablePorogObnar.Value);
        }

        async void LibInit()
        {
            var library = await MediaLibrary.MediaLibrary.CreateAsync("papka");
            library.Recorder.StartRecord(99500, 0);
            var result = library.Recorder.StopRecord();
            if (result.HasValue)
            {
                // записалось удачно
            }
            else
            {
                // все плохо
            }
            library.Player.Play(library.Records[0]);
            library.Player.Volume = 0.5f;
            library.Player.Playlist = new[] { library.Records[0], library.Records[1] };
        }
        private void tbNote_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (tbNote.Text.Length > 20)
            {
                tbNote.Text = tbNote.Text.Substring(0, 20);
            }
        }
        private void FlashLedWriteARONE()
        {
            try
            {
                pictureBox14.Image = Properties.Resources.red; ;
                timer = new System.Threading.Timer(TimeStepLedWriteArone, null, TimePause, 0);
                if (OnWrite != null)
                    OnWrite(1);
            }
            catch { }
        }

        private void TimeStepLedWriteArone(object state)
        {
            try
            {
                if (OnWrite != null)
                    OnWrite(0);
                if (pictureBox14.InvokeRequired)
                {
                    pictureBox14.Invoke((MethodInvoker)(delegate()
                    {
                        pictureBox14.Image = Properties.Resources.gray;
                        timer.Dispose();
                    }));
                }
                else
                {
                    pictureBox14.Image = Properties.Resources.gray;
                    timer.Dispose();
                }
            }
            catch { }
        }
        private void FlashLedReadARONE()
        {
            try
            {
                if (OnRead != null)
                    OnRead(1);
                pictureBox13.Image = Properties.Resources.green;
                timerRead = new System.Threading.Timer(TimeStepLedReadArone, null, TimePause, 0);
            }
            catch { }
        }
        private void TimeStepLedReadArone(object state)
        {
            try
            {
                if (OnRead != null)
                    OnRead(0);
                if (pictureBox13.InvokeRequired)
                {
                    pictureBox13.Invoke((MethodInvoker)(delegate()
                    {
                        pictureBox13.Image = Properties.Resources.gray;
                        timerRead.Dispose();
                    }));
                }
                else
                {
                    pictureBox13.Image = Properties.Resources.gray;
                    timerRead.Dispose();

                }
            }
            catch { }
        }

        private void dgv_Resize(object sender, EventArgs e)
        {
            if (dgv.Size.Width < 518)
            {
                dgv.Columns[6].Visible = false;
                dgv.Columns[8].Visible = false;
                dgv.Columns[9].Visible = false;
                dgv.Columns[10].Visible = false;
            }
            if ((dgv.Size.Width > 517) && (dgv.Size.Width < 574))
            {
                dgv.Columns[6].Visible = false;
                dgv.Columns[8].Visible = true;
                dgv.Columns[9].Visible = false;
                dgv.Columns[10].Visible = false;
            }
            if ((dgv.Size.Width > 573) && (dgv.Size.Width < 620))
            {
                dgv.Columns[6].Visible = true;
                dgv.Columns[8].Visible = false;
                dgv.Columns[9].Visible = true;
                dgv.Columns[10].Visible = false;
            }
            if ((dgv.Size.Width > 619) && (dgv.Size.Width < 665))
            {
                dgv.Columns[6].Visible = true;
                dgv.Columns[8].Visible = true;
                dgv.Columns[9].Visible = true;
                dgv.Columns[10].Visible = false;
            }
            if (dgv.Size.Width > 664)
            {
                dgv.Columns[6].Visible = true;
                dgv.Columns[8].Visible = true;
                dgv.Columns[9].Visible = true;
                dgv.Columns[10].Visible = true;
            }
        }
        private void bIspPel_Click(object sender, EventArgs e)
        {
            /*
            double frequency;
            if (double.TryParse(lFrq.Text, out frequency))
            {
                var minFreq = frequency - 0.05;
                var maxFreq = frequency + 0.05;
                var result = await VariableDynamic.VariableWork.aWPtoBearingDSPprotocolNew.ExecutiveDF(minFreq, maxFreq, 3, 3);
                if (result != null)
                {
                    lPel.Text = (result.Direction / 10).ToString();
                }
            }
            */
            
            double frequency = 0;

            /*
            System.IO.StreamWriter sw = new System.IO.StreamWriter("AR6000.txt", true, System.Text.Encoding.Default);
            sw.WriteLine("lFrq.Text: " + lFrq.Text);
            sw.WriteLine("lFRQFromAOR.Text: " + lFRQFromAOR.Text);
            sw.WriteLine();

            sw.Close();
            */


            try
            {
                string Frq2 = lFRQFromAOR.Text;
                Frq2 = Frq2.Substring(0, Frq2.Length - 3);
                Frq2 = Frq2.Replace(',', '.');

                if (double.TryParse(Frq2, out frequency))
                {
                    frequency = frequency / 1000d;
                    VariableDynamic.VariableWork VW = new VariableDynamic.VariableWork();
                    VW.RecBRSsend(frequency);
                }
            }
            catch { }

            try
            {
                string Frq2 = lFRQFromAOR.Text;
                Frq2 = Frq2.Substring(0, Frq2.Length - 3);

                if (double.TryParse(Frq2, out frequency))
                {
                    frequency = frequency / 1000d;
                    VariableDynamic.VariableWork VW = new VariableDynamic.VariableWork();
                    VW.RecBRSsend(frequency);
                }
            }
            catch { }

        }

        private void bFRSonRS_Click(object sender, EventArgs e)
        {
            double frequency = 0;

            try
            {
                string Frq2 = lFRQFromAOR.Text;
                Frq2 = Frq2.Substring(0, Frq2.Length - 3);
                Frq2 = Frq2.Replace(',', '.');

                if (double.TryParse(Frq2, out frequency))
                {
                    frequency = frequency / 1000d;
                    VariableDynamic.VariableWork VW = new VariableDynamic.VariableWork();
                    VW.RecFtoRJSsend(frequency);
                }
            }
            catch { }

            try
            {
                string Frq2 = lFRQFromAOR.Text;
                Frq2 = Frq2.Substring(0, Frq2.Length - 3);

                if (double.TryParse(Frq2, out frequency))
                {
                    frequency = frequency / 1000d;
                    VariableDynamic.VariableWork VW = new VariableDynamic.VariableWork();
                    VW.RecFtoRJSsend(frequency);
                }
            }
            catch { }
        }

        private void RefreshKatalogs()
        {
            try
            {
                listBox1.Items.Clear();
                string dirName = Application.StartupPath + "\\wav";

                string[] dirs = Directory.GetDirectories(dirName);
                foreach (string s in dirs)
                {
                    listBox1.Items.Add(s.Substring(dirName.Length + 1));
                }
                return;

            }
            catch (Exception) { }
        }

        private void RefreshKatalogWavs()
        {
            try
            {
                listBox2.Items.Clear();
                string dirName = Application.StartupPath + "\\wav\\" + listBox1.SelectedItem.ToString();
                if (Directory.Exists(dirName))
                {
                    string[] files = Directory.GetFiles(dirName);
                    foreach (string s in files)
                    {
                        listBox2.Items.Add(s.Substring(dirName.Length + 1, (s.Length - dirName.Length - 5)));
                    }
                }
            }
            catch (Exception) { }
        }
        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            RefreshKatalogWavs();
        }
        bool playing = false;
        //System.Threading.Tasks.Task library ;//= await MediaLibrary.MediaLibrary.CreateAsync(Application.StartupPath + "\\wav\\" + listBox1.SelectedItem.ToString());

        //----------------------------------------------------------------------------------------------------
      
        private WaveOutEvent outputDevice;
        private AudioFileReader audioFile;

        public void listBox2_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                string fileName = Application.StartupPath + "\\wav\\" + listBox1.SelectedItem.ToString() + "\\" + listBox2.SelectedItem.ToString() + ".wav";

                DisposeWave();

                outputDevice = new WaveOutEvent();
                audioFile = new AudioFileReader(fileName);
                outputDevice.Init(audioFile);
               
                outputDevice.Play();
                pictureBox18.Image = Properties.Resources.wavPlayer;  //equalizer ON

                Task.Run(async () =>
                {
                    while (true)
                    {
                        //Console.WriteLine(audioFile.CurrentTime.ToString());

                        if(audioFile.CurrentTime == audioFile.TotalTime)
                            pictureBox18.Image = Properties.Resources.wavPlayerGray;  //equalizer OFF

                        await Task.Delay(100);
                    }
                });
            }
            catch{}
            //catch (Exception) { MessageBox.Show(ex.Message); }
        }

        public void DisposeWave()
        {
            try
            {
                if (outputDevice != null)
                {
                    if (outputDevice.PlaybackState == NAudio.Wave.PlaybackState.Playing) outputDevice.Stop();
                    outputDevice.Dispose();
                    outputDevice = null;
                }
                if (audioFile != null)
                {
                    audioFile.Dispose();
                    audioFile = null;
                }
            }
            catch { }
            //catch (Exception) { MessageBox.Show(ex.Message); }
        }
        

        void output_PlaybackStopped(object sender, StoppedEventArgs e)
        {
            string s = e.Exception.ToString();
            //MessageBox.Show(s);
        }
        private void PlayBackStateEventt(object sender, SharpExtensions.EventArg<PlaybackState> e)
        {
            try
            {
                if (e.Data.ToString() == "Stopped")
                {
                    MuteArone(false);
                    bMute = true;
                    pictureBox18.Image = Properties.Resources.wavPlayerGray; playing = false;
                }         //equalizer OFF
                if (e.Data.ToString() == "Playing")
                {
                    MuteArone(true);
                    bMute = false;
                    pictureBox18.Image = Properties.Resources.wavPlayer; playing = true;
                }              //equalizer ON
                if (e.Data.ToString() == "Paused")
                {
                    MuteArone(true);
                    bMute = false;
                    pictureBox18.Image = Properties.Resources.wavPlayerGray; playing = false;
                }              //equalizer OFF
            }
            catch { }
        }
        //private void tabControl1_Selectingddddx(object sender, TabControlCancelEventArgs e)
        //{
        //    if (tabControl1.SelectedIndex == 1) RefreshKatalogs();
        //}
        private void listBox1_DoubleClick(object sender, EventArgs e)
        {
            RefreshKatalogs();
        }
        private void bFrqMinus_Click(object sender, EventArgs e)
        {
            double myfrq = double.Parse(AR6000aor.ArOne.frequency.ToString());
            myfrq = myfrq - step_frq_MHz;
            if (myfrq > 30000)
            {
                AR6000aor.FrequencySet(myfrq / 1000000);
            }
            else { };//MessageBox.Show("Не может быть меньше!"); }

        }
        private void bFrqPlus_Click(object sender, EventArgs e)
        {

            double myfrq = double.Parse(AR6000aor.ArOne.frequency.ToString());
            myfrq = myfrq + step_frq_MHz;
            if (myfrq < 3299999999)
            {
                AR6000aor.FrequencySet(myfrq / 1000000);
            }
            else { };//MessageBox.Show("Не может быть больше!"); }

        }
        private void cbFrqStep_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch ((int)cbFrqStep.SelectedIndex)
            {
                case 0: step_frq_MHz = 1f; break;// / 1000000f; break;
                case 1: step_frq_MHz = 10f; break;// / 1000000f; break;
                case 2: step_frq_MHz = 50f; break;// / 1000000f; break;
                case 3: step_frq_MHz = 100f; break;// / 1000000f; break;
                case 4: step_frq_MHz = 500f; break;// / 1000000f; break;
                case 5: step_frq_MHz = 1000f; break;// / 1000000f; break;
                case 6: step_frq_MHz = 5000f; break;// / 1000000f; break;
                case 7: step_frq_MHz = 10000f; break;// / 1000000f; break;
                case 8: step_frq_MHz = 50000f; break;// / 1000000f; break;
                case 9: step_frq_MHz = 100000f; break;// / 1000000f; break;
                case 10: step_frq_MHz = 500000f; break;// / 1000000f; break;
                case 11: step_frq_MHz = 1000000f; break;// / 1000000f; break;
                default: MessageBox.Show("case not worked!"); break;
            }

        }
        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            TimePause = (int)numericUpDown1.Value * 10;
        }

        private void bWavPlayer_Click(object sender, EventArgs e)
        {
            try
            {
                //string WavPath = "";
                //try
                //{
                //    if (listBox2.SelectedItem != null)
                //        WavPath = Application.StartupPath + "\\wav\\" + listBox1.SelectedItem.ToString() + "\\" + listBox2.SelectedItem.ToString() + ".wav";
                //    WritePrivateProfileString("Player", "PathIni", WavPath, Application.StartupPath + "\\Player\\player.ini");
                //}
                //catch { }

                string name = "Player";
                string nameAZ = "PlayerAZ";
                System.Diagnostics.Process[] process = System.Diagnostics.Process.GetProcesses();

                bool blProc = false;
                int i = 0;

                while (i < process.Length)
                {
                    if (process[i].ProcessName == name || process[i].ProcessName == nameAZ)
                    {
                        blProc = true;
                        i = process.Length;
                    }
                    i++;
                }

                VariableStatic.VariableCommon variableCommon = new VariableStatic.VariableCommon();

                string WavPath = "";
                try
                {
                    //rus
                    if (variableCommon.Language == 0)
                    {
                        if (listBox2.SelectedItem != null)
                        {
                            WavPath = Application.StartupPath + "\\wav\\" + listBox1.SelectedItem.ToString() + "\\" + listBox2.SelectedItem.ToString() + ".wav";
                            WritePrivateProfileString("Player", "PathIni", WavPath, Application.StartupPath + "\\Player RU\\Player.ini");
                        }
                       
                        ProcessWavPlayer.StartInfo.FileName = Application.StartupPath + "\\Player RU\\Player.exe";
                    }
                    //az
                    if (variableCommon.Language == 2)
                    {
                        if (listBox2.SelectedItem != null)
                        {
                            WavPath = Application.StartupPath + "\\wav\\" + listBox1.SelectedItem.ToString() + "\\" + listBox2.SelectedItem.ToString() + ".wav";
                            WritePrivateProfileString("Player", "PathIni", WavPath, Application.StartupPath + "\\Player AZ\\Player.ini");
                        }

                        ProcessWavPlayer.StartInfo.FileName = Application.StartupPath + "\\Player AZ\\PlayerAZ.exe";
                    }
                }
                catch { }

                if (blProc == false)
                {
                    ProcessWavPlayer.Start();
                }
            }
            catch (Exception) { }
        }
        private void cbAGC_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbAGC.SelectedIndex == 3)
            {
                trackBarManualGain.Enabled = true;
                AR6000aor.AutomaticGainControlSet(3);
            }
            else
            {
                trackBarManualGain.Enabled = false;
                AR6000aor.AutomaticGainControlSet(cbAGC.SelectedIndex);
            }
            Thread.Sleep(20);
            AR6000aor.AutomaticGainControlGet();
        }
        private void tbSQL_Scroll(object sender, EventArgs e)
        {
            if (cbSQL.SelectedIndex == 0)
            {
                AR6000aor.LevelSQuelchSet(tbSQL.Value);
                Thread.Sleep(20);
                AR6000aor.LevelSQuelchGet();
            }
            else
            {
                AR6000aor.NoiseSQuelchSet(tbSQL.Value);
                Thread.Sleep(20);
                AR6000aor.NioseSQGet();
            }
        }
        private void cbSQL_SelectedIndexChanged(object sender, EventArgs e)
        {
            AR6000aor.NoiseSQuelchOnOffSet(cbSQL.SelectedIndex);
            Thread.Sleep(20);
            AR6000aor.NioseSQuelchOnOffGet();
        }
        private void tabControl1_Selecting(object sender, TabControlCancelEventArgs e)
        {
            if (tabControl1.SelectedIndex == 1) RefreshKatalogs();
        }
        private void cbAverage_CheckedChanged(object sender, EventArgs e)
        {
            if (!cbAverage.Checked)
            {
                PlotAverage.ClearData();
                cbAverage.Image = Properties.Resources.average_gray;
            }
            else
            {
                cbAverage.Image = Properties.Resources.average_blue;

            }
        }
        public void MuteArone(bool mute)
        {
            var channels = AppVolumeLibrary.AppVolumeLibrary.EnumerateApplications().ToArray();
            if (channels.Any(c => c == aroneVolumeChannelName2))
            {
                AppVolumeLibrary.AppVolumeLibrary.SetApplicationMute(aroneVolumeChannelName2, mute);
            }
           
        }
        public void SetFRQ_MHz(double frqq)
        {
            AR6000aor.FrequencySet(frqq);
        }
        private void tbAudioGain_Scroll(object sender, EventArgs e)
        {
            AR6000aor.AudioGainSet(tbAudioGain.Value);
            Thread.Sleep(20);
            AR6000aor.AudioGainGet();
        }

        public void SetCheckBox(bool bCheck)
        {
            checkBox1.Checked = bCheck;

        
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            //if (checkBox1.Checked) AR6000aor.AudioGainSet(0);
            //else AR6000aor.AudioGainSet(60);// (tbAudioGain.Value);

            if (checkBox1.Checked) MuteArone(true);
            else MuteArone(false);
        }
        private void nudPause_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= '0') && (e.KeyChar <= '9') || (e.KeyChar == '\b')) // цифра, Backspace
            {
                return;
            }
            e.Handled = true;
        }
        private void nudPause_Validated(object sender, EventArgs e)
        {
            if (nudPause.Value > 30000) nudPause.Value = 30000;
        }
        private void bAddToTableFromAOR_Click(object sender, EventArgs e)
        {
            if (bAOR.BackColor == Color.Red) return;
            StrAR6000.iFreq =AR6000aor.ArOne.frequency / 10;
            if ((StrAR6000.iFreq < 30000) || (StrAR6000.iFreq > 6000000000)) return;
            StrAR6000.iU = (int)nudTablePorogObnar.Value;
            StrAR6000.bPriority = (byte)cbTablePriority.SelectedIndex;
            StrAR6000.iAttenuator = comboBoxAtt.SelectedIndex;
            StrAR6000.iBW = AR6000aor.ArOne.Bandwidth;
            StrAR6000.iHPF = AR6000aor.ArOne.HighPassFilter;
            StrAR6000.iLPF = AR6000aor.ArOne.LowPassFilter;
            StrAR6000.iMode = AR6000aor.ArOne.Mode;
            StrAR6000.iOnOff = 1;
            StrAR6000.iPause = (int)nudPause.Value;
            StrAR6000.Note = tbNote.Text.ToString();
            StrAR6000.sTimeFirst = DateTime.Now.Hour.ToString("00") + DateTime.Now.Minute.ToString("00") + DateTime.Now.Second.ToString("00");
            sqLiteConnect = new System.Data.SQLite.SQLiteConnection(functionsDB.ConnectionString());             // подключиться к БД      
            sqLiteConnect.Open();
            sqLiteCommand = new System.Data.SQLite.SQLiteCommand(
                "INSERT INTO AR6000(OnOff,Freq, Pause, U, TimeFirst,  Attenuator, Mode, BW, HPF, LPF, Priority, Note) VALUES (" + StrAR6000.iOnOff + "," + StrAR6000.iFreq + "," + StrAR6000.iPause + "," + StrAR6000.iU + "," + StrAR6000.sTimeFirst + "," + StrAR6000.iAttenuator + "," + StrAR6000.iMode + "," + StrAR6000.iBW + "," + StrAR6000.iHPF + "," + StrAR6000.iLPF + "," + StrAR6000.bPriority + ",'" + StrAR6000.Note.ToString() + "')", sqLiteConnect);
            int qwee = sqLiteCommand.ExecuteNonQuery();
            sqLiteConnect.Close();
            zagruzka_tablFromBD(NameTable.AR6000.ToString());
        }
        public void AddFrqToTablAR6000(Int64 Freq_Hz, byte Prority)
        {
            if ((Freq_Hz < 30000) || (Freq_Hz > 6000000000)) return;
            int qwe = Poisk_chastoty_v_tabl(nudTableFRQ.Value.ToString(), dgv);
            if (qwe != -1)
            {
                dgv.FirstDisplayedScrollingRowIndex = qwe;
                dgv.Rows[qwe].Selected = true;
                return;
            }
            StrAR6000.iFreq = (Freq_Hz);
            StrAR6000.iU = -50;
            StrAR6000.bPriority = (byte)Prority;
            StrAR6000.iAttenuator = 1;
            StrAR6000.iBW = 5;
            StrAR6000.iHPF = 4;
            StrAR6000.iLPF = 4;
            StrAR6000.iMode = 5;
            StrAR6000.iOnOff = 1;
            StrAR6000.iPause = 1000;
            StrAR6000.Note = "";
            StrAR6000.sTimeFirst = DateTime.Now.Hour.ToString("00") + DateTime.Now.Minute.ToString("00") + DateTime.Now.Second.ToString("00");
            sqLiteConnect = new System.Data.SQLite.SQLiteConnection(functionsDB.ConnectionString());             // подключиться к БД      
            sqLiteConnect.Open();
            sqLiteCommand = new System.Data.SQLite.SQLiteCommand(
               "INSERT INTO AR6000(OnOff,Freq, Pause, U, TimeFirst,  Attenuator, Mode, BW, HPF, LPF, Priority, Note) VALUES (" + StrAR6000.iOnOff + "," + StrAR6000.iFreq + "," + StrAR6000.iPause + "," + StrAR6000.iU + ",'" + StrAR6000.sTimeFirst + "," + StrAR6000.iAttenuator + "," + StrAR6000.iMode + "," + StrAR6000.iBW + "," + StrAR6000.iHPF + "," + StrAR6000.iLPF + "," + StrAR6000.bPriority + ",'" + StrAR6000.Note.ToString() + "')", sqLiteConnect);
            int qwee = sqLiteCommand.ExecuteNonQuery();
            sqLiteConnect.Close();
            zagruzka_tablFromBD(NameTable.AR6000.ToString());

        }
        int Poisk_chastoty_v_tabl(string freq, DataGridView DGV)
        {
            try
            {
                for (int i = 0; i < DGV.RowCount; i++)
                {
                    if (DGV.Rows[i].Cells[3].Value.ToString().CompareTo(freq.ToString()) == 0) { return i; }
                }
                return -1;
            }
            catch
            {
                return -1;
            }
        }
        public void LanguageChooser(int NumberOfLanguage)
        {
            var cont = this.Controls;
            this.NumberOfLanguage = NumberOfLanguage;
            InitTableAR6000();

            if (NumberOfLanguage.Equals(0))
            {
                ChangeLanguageToRu(cont);
                this.Text = "КРПУ AR6000 cлухового контроля";
                waveformGraph1.YAxes[0].Caption = "Уровень, дБм";
                waveformGraph1.XAxes[0].Caption = "Частота, Гц";

                string tempkHz = "кГц";
                if (lFRQFromAOR.Text.Length > 6)
                {
                    lFRQFromAOR.Text = lFRQFromAOR.Text.Remove(lFRQFromAOR.Text.Length - 3, 3);
                    lFRQFromAOR.Text = lFRQFromAOR.Text + tempkHz;
                }

            }
            if (NumberOfLanguage.Equals(1))
            {
                ChangeLanguageToEng(cont);
                this.Text = " ";
            }
            if (NumberOfLanguage.Equals(2))
            {
                ChangeLanguageToAzer(cont);
                this.Text = "YRQQ AR6000 eşitmə nəzarət";
                waveformGraph1.YAxes[0].Caption = "Səviyyə, dBm";
                waveformGraph1.XAxes[0].Caption = "Tezlik, Hs";

                string tempkHz = "kHs";
                if (lFRQFromAOR.Text.Length > 6)
                {
                    lFRQFromAOR.Text = lFRQFromAOR.Text.Remove(lFRQFromAOR.Text.Length - 3, 3);
                    lFRQFromAOR.Text = lFRQFromAOR.Text + tempkHz;
                }


            }
            //if (bAOR.BackColor == Color.Green) ConnectPort();
            //else DisconnextPort();
            
        }
        private void ChangeLanguageToRu(System.Windows.Forms.Control.ControlCollection cont)
        {
            string lang = "ru-RU";
            foreach (System.Windows.Forms.Control cc in cont)
            {
                if (cc is ComboBox)
                {

                    for (int i = 0; i < (cc as ComboBox).Items.Count; i++)
                    {
                        if (i == 0)
                        {
                            string a = resources.GetString(cc.Name + ".Items", new CultureInfo(lang));
                            (cc as ComboBox).Items[i] = a;
                        }
                        else
                        {
                            string a = resources.GetString(cc.Name + ".Items" + i.ToString(), new CultureInfo(lang));
                            (cc as ComboBox).Items[i] = a;
                        }
                    }
                }
                //}
                if (cc is Label || cc is Button || cc is RadioButton || cc is CheckBox || cc is GroupBox || cc is ComboBox || cc is TabPage || cc is TextBox)
              
                {
                    resources.ApplyResources(cc, cc.Name, new CultureInfo(lang));
                    //toolTip.SetToolTip(cc, resources.GetString(cc.Name + ".ToolTip", new CultureInfo(lang)));
                }
                if (cc is Panel || cc is GroupBox || cc is ListBox || cc is TabControl)
                {
                    ChangeLanguageToRu(cc.Controls);
                }
            }
        }
        private void ChangeLanguageToAzer(System.Windows.Forms.Control.ControlCollection cont)
        {
            string lang = "az-Latn";

            foreach (System.Windows.Forms.Control cc in cont)
            {
                if (cc is ComboBox)
                {

                    for (int i = 0; i < (cc as ComboBox).Items.Count; i++)
                    {
                        if (i == 0)
                        {
                            string a = resources.GetString(cc.Name + ".Items", new CultureInfo(lang));
                            (cc as ComboBox).Items[i] = a;
                        }
                        else
                        {
                            string a = resources.GetString(cc.Name + ".Items" + i.ToString(), new CultureInfo(lang));
                            (cc as ComboBox).Items[i] = a;
                        }
                    }
                }
                //}
                if (cc is Label || cc is Button || cc is RadioButton || cc is CheckBox || cc is GroupBox || cc is ComboBox || cc is TabPage || cc is TextBox)
              
                {
                    resources.ApplyResources(cc, cc.Name, new CultureInfo(lang));
                    //toolTip.SetToolTip(cc, resources.GetString(cc.Name + ".ToolTip", new CultureInfo(lang)));
                }
                if (cc is Panel || cc is GroupBox || cc is ListBox || cc is TabControl)
                {
                    ChangeLanguageToAzer(cc.Controls);
                }



            }
        }
        private void ChangeLanguageToEng(System.Windows.Forms.Control.ControlCollection cont)
        {


        
        }
        private void InitTableAR6000()
        {
          
            switch (NumberOfLanguage)
            {
                case 0:

                    dgv.Columns[0].HeaderText = "№";
                    dgv.Columns[1].HeaderText = "ВКЛ";
                    dgv.Columns[2].HeaderText = "Частота, кГц";
                    dgv.Columns[3].HeaderText = "Пауза, мс";
                    dgv.Columns[4].HeaderText = "Порог, дБм";
                    dgv.Columns[5].HeaderText = "Время";
                    dgv.Columns[6].HeaderText = "Атт";
                    dgv.Columns[7].HeaderText = "Режим";
                    dgv.Columns[8].HeaderText = "Полоса";
                    dgv.Columns[9].HeaderText = "ФВЧ";
                    dgv.Columns[10].HeaderText = "ФНЧ";
                    dgv.Columns[11].HeaderText = "Приор";
                    dgv.Columns[12].HeaderText = "Примечание";


                    break;
                case 1:

                    dgv.Columns[0].HeaderText = "";
                    dgv.Columns[1].HeaderText = "";
                    dgv.Columns[2].HeaderText = "";
                    dgv.Columns[3].HeaderText = "";
                    dgv.Columns[4].HeaderText = "";
                    dgv.Columns[5].HeaderText = "";
                    dgv.Columns[6].HeaderText = "";
                    dgv.Columns[7].HeaderText = "";
                    dgv.Columns[8].HeaderText = "";
                    dgv.Columns[9].HeaderText = "";
                    dgv.Columns[10].HeaderText = "";
                    dgv.Columns[11].HeaderText = "";
                    dgv.Columns[12].HeaderText = "";
                    dgv.Columns[13].HeaderText = "";

                    break;
                case 2:

                    dgv.Columns[0].HeaderText = "№";
                    dgv.Columns[1].HeaderText = "Qoş";
                    dgv.Columns[2].HeaderText = "Tezlik, KHs";
                    dgv.Columns[3].HeaderText = "Fasilə mKs	";
                    dgv.Columns[4].HeaderText = "Hədd, dBm";
                    dgv.Columns[5].HeaderText = "Vaxt";
                    dgv.Columns[6].HeaderText = "Att";
                    dgv.Columns[7].HeaderText = "Üsul";
                    dgv.Columns[8].HeaderText = "Zolaq";
                    dgv.Columns[9].HeaderText = "YTF";
                    dgv.Columns[10].HeaderText = "ATF";
                    dgv.Columns[11].HeaderText = "Üstünlük";
                    dgv.Columns[12].HeaderText = "Qeyd";

                    break;
                default:
                    break;
            }
        }
        private void button1_Click(object sender, EventArgs e)
        {
            ConnectPort();
        }

        private void bPause_Click(object sender, EventArgs e)
        {
            try
            {
                if (outputDevice != null)
                {
                    if (outputDevice.PlaybackState == NAudio.Wave.PlaybackState.Playing)
                    {
                        bPause.Image = Properties.Resources.Continue;
                        pictureBox18.Image = Properties.Resources.wavPlayerGray;

                        outputDevice.Pause();
                    }
                    else if (outputDevice.PlaybackState == NAudio.Wave.PlaybackState.Paused)
                    {
                        bPause.Image = Properties.Resources.Pause;
                        pictureBox18.Image = Properties.Resources.wavPlayer;  //equalizer ON

                        outputDevice.Play();
                       // --------------------------------------------------------------------------------

                    }
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }

        private void bStop_Click(object sender, EventArgs e)
        {
            try
            {
                pictureBox18.Image = Properties.Resources.wavPlayerGray;
                bPause.Image = Properties.Resources.Pause;

                DisposeWave();
            }
            catch { }
        }

    }
}