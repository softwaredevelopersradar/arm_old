﻿namespace ControlAR6000
{
    partial class controlAR6000
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(controlAR6000));
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.tbAudioGain = new System.Windows.Forms.TrackBar();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.cbSQL = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tbSQL = new System.Windows.Forms.TrackBar();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.label22 = new System.Windows.Forms.Label();
            this.cbFrqStep = new System.Windows.Forms.ComboBox();
            this.bFrqPlus = new System.Windows.Forms.Button();
            this.bFrqMinus = new System.Windows.Forms.Button();
            this.lPel = new System.Windows.Forms.Label();
            this.bIspPel = new System.Windows.Forms.Button();
            this.comboBoxMode = new System.Windows.Forms.ComboBox();
            this.comboBoxHPF = new System.Windows.Forms.ComboBox();
            this.comboBoxLPF = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.comboBoxBW = new System.Windows.Forms.ComboBox();
            this.b0 = new System.Windows.Forms.Button();
            this.comboBoxAtt = new System.Windows.Forms.ComboBox();
            this.b4 = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.b5 = new System.Windows.Forms.Button();
            this.b2 = new System.Windows.Forms.Button();
            this.label16 = new System.Windows.Forms.Label();
            this.b3 = new System.Windows.Forms.Button();
            this.bESC = new System.Windows.Forms.Button();
            this.b7 = new System.Windows.Forms.Button();
            this.b6 = new System.Windows.Forms.Button();
            this.label17 = new System.Windows.Forms.Label();
            this.b1 = new System.Windows.Forms.Button();
            this.b9 = new System.Windows.Forms.Button();
            this.b8 = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.cbAGC = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.trackBarManualGain = new System.Windows.Forms.TrackBar();
            this.pScreen = new System.Windows.Forms.Panel();
            this.lFRQFromAOR = new System.Windows.Forms.Label();
            this.lFrq = new System.Windows.Forms.Label();
            this.lBw = new System.Windows.Forms.Label();
            this.lat = new System.Windows.Forms.Label();
            this.lMode = new System.Windows.Forms.Label();
            this.lLevel = new System.Windows.Forms.Label();
            this.lAmplifier = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.panel22 = new System.Windows.Forms.Panel();
            this.panel23 = new System.Windows.Forms.Panel();
            this.lREC = new System.Windows.Forms.Label();
            this.cbARec = new System.Windows.Forms.CheckBox();
            this.cbRec = new System.Windows.Forms.CheckBox();
            this.cbACP = new System.Windows.Forms.CheckBox();
            this.cbAverage = new System.Windows.Forms.CheckBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.waveformGraph1 = new NationalInstruments.UI.WindowsForms.WaveformGraph();
            this.PlotSignal = new NationalInstruments.UI.WaveformPlot();
            this.xAxis1 = new NationalInstruments.UI.XAxis();
            this.yAxis1 = new NationalInstruments.UI.YAxis();
            this.PlotAverage = new NationalInstruments.UI.WaveformPlot();
            this.MinPorogPlot = new NationalInstruments.UI.WaveformPlot();
            this.MaxPorogPlot = new NationalInstruments.UI.WaveformPlot();
            this.PlotPorogObn = new NationalInstruments.UI.WaveformPlot();
            this.tbWaterflowMin = new System.Windows.Forms.TrackBar();
            this.tbWaterflowMax = new System.Windows.Forms.TrackBar();
            this.intensityGraph1 = new NationalInstruments.UI.WindowsForms.IntensityGraph();
            this.colorScale1 = new NationalInstruments.UI.ColorScale();
            this.intensityPlot1 = new NationalInstruments.UI.IntensityPlot();
            this.intensityXAxis1 = new NationalInstruments.UI.IntensityXAxis();
            this.intensityYAxis1 = new NationalInstruments.UI.IntensityYAxis();
            this.cbIntensityPaint = new System.Windows.Forms.CheckBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tpIRI_KRPU = new System.Windows.Forms.TabPage();
            this.bAddToTableFromAOR = new System.Windows.Forms.Button();
            this.nudTableFRQ = new System.Windows.Forms.NumericUpDown();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.Number = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OnOff = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Fequency = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Pause = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Treshold = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TimeFirst = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Att = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Mode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BW = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HPF = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LPF = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Priority = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Note = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tbNote = new System.Windows.Forms.TextBox();
            this.bTableChange = new System.Windows.Forms.Button();
            this.nudPause = new System.Windows.Forms.NumericUpDown();
            this.bTableAdd = new System.Windows.Forms.Button();
            this.label18 = new System.Windows.Forms.Label();
            this.bTableDel = new System.Windows.Forms.Button();
            this.cbTablePriority = new System.Windows.Forms.ComboBox();
            this.bTableClear = new System.Windows.Forms.Button();
            this.nudTablePorogObnar = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.tpPlayer = new System.Windows.Forms.TabPage();
            this.bStop = new System.Windows.Forms.Button();
            this.bPause = new System.Windows.Forms.Button();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.bWavPlayer = new System.Windows.Forms.Button();
            this.listBox2 = new System.Windows.Forms.ListBox();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.pictureBox18 = new System.Windows.Forms.PictureBox();
            this.bAOR = new System.Windows.Forms.Button();
            this.label23 = new System.Windows.Forms.Label();
            this.bENT = new System.Windows.Forms.Button();
            this.bDot = new System.Windows.Forms.Button();
            this.tm500 = new System.Windows.Forms.Timer(this.components);
            this.tmFlashFRQ = new System.Windows.Forms.Timer(this.components);
            this.tmBaraban = new System.Windows.Forms.Timer(this.components);
            this.bFRSonRS = new System.Windows.Forms.Button();
            this.pictureBox13 = new System.Windows.Forms.PictureBox();
            this.pictureBox14 = new System.Windows.Forms.PictureBox();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbAudioGain)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbSQL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarManualGain)).BeginInit();
            this.pScreen.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel22.SuspendLayout();
            this.panel23.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.waveformGraph1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbWaterflowMin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbWaterflowMax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intensityGraph1)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tpIRI_KRPU.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudTableFRQ)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPause)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudTablePorogObnar)).BeginInit();
            this.tpPlayer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.tbAudioGain);
            resources.ApplyResources(this.groupBox4, "groupBox4");
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.TabStop = false;
            // 
            // tbAudioGain
            // 
            resources.ApplyResources(this.tbAudioGain, "tbAudioGain");
            this.tbAudioGain.Maximum = 100;
            this.tbAudioGain.Name = "tbAudioGain";
            this.tbAudioGain.TickStyle = System.Windows.Forms.TickStyle.None;
            this.tbAudioGain.Value = 25;
            this.tbAudioGain.Scroll += new System.EventHandler(this.tbAudioGain_Scroll);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.cbSQL);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.tbSQL);
            resources.ApplyResources(this.groupBox2, "groupBox2");
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.TabStop = false;
            // 
            // cbSQL
            // 
            this.cbSQL.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbSQL.FormattingEnabled = true;
            this.cbSQL.Items.AddRange(new object[] {
            resources.GetString("cbSQL.Items"),
            resources.GetString("cbSQL.Items1")});
            resources.ApplyResources(this.cbSQL, "cbSQL");
            this.cbSQL.Name = "cbSQL";
            this.cbSQL.SelectedIndexChanged += new System.EventHandler(this.cbSQL_SelectedIndexChanged);
            // 
            // label5
            // 
            resources.ApplyResources(this.label5, "label5");
            this.label5.Name = "label5";
            // 
            // tbSQL
            // 
            resources.ApplyResources(this.tbSQL, "tbSQL");
            this.tbSQL.Maximum = 255;
            this.tbSQL.Name = "tbSQL";
            this.tbSQL.TickStyle = System.Windows.Forms.TickStyle.None;
            this.tbSQL.Value = 25;
            this.tbSQL.Scroll += new System.EventHandler(this.tbSQL_Scroll);
            // 
            // numericUpDown1
            // 
            resources.ApplyResources(this.numericUpDown1, "numericUpDown1");
            this.numericUpDown1.Maximum = new decimal(new int[] {
            21600,
            0,
            0,
            0});
            this.numericUpDown1.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numericUpDown1.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // label22
            // 
            resources.ApplyResources(this.label22, "label22");
            this.label22.Name = "label22";
            // 
            // cbFrqStep
            // 
            this.cbFrqStep.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbFrqStep.FormattingEnabled = true;
            this.cbFrqStep.Items.AddRange(new object[] {
            resources.GetString("cbFrqStep.Items"),
            resources.GetString("cbFrqStep.Items1"),
            resources.GetString("cbFrqStep.Items2"),
            resources.GetString("cbFrqStep.Items3"),
            resources.GetString("cbFrqStep.Items4"),
            resources.GetString("cbFrqStep.Items5"),
            resources.GetString("cbFrqStep.Items6"),
            resources.GetString("cbFrqStep.Items7"),
            resources.GetString("cbFrqStep.Items8"),
            resources.GetString("cbFrqStep.Items9"),
            resources.GetString("cbFrqStep.Items10"),
            resources.GetString("cbFrqStep.Items11")});
            resources.ApplyResources(this.cbFrqStep, "cbFrqStep");
            this.cbFrqStep.Name = "cbFrqStep";
            this.cbFrqStep.SelectedIndexChanged += new System.EventHandler(this.cbFrqStep_SelectedIndexChanged);
            // 
            // bFrqPlus
            // 
            resources.ApplyResources(this.bFrqPlus, "bFrqPlus");
            this.bFrqPlus.Name = "bFrqPlus";
            this.bFrqPlus.UseVisualStyleBackColor = true;
            this.bFrqPlus.Click += new System.EventHandler(this.bFrqPlus_Click);
            // 
            // bFrqMinus
            // 
            resources.ApplyResources(this.bFrqMinus, "bFrqMinus");
            this.bFrqMinus.Name = "bFrqMinus";
            this.bFrqMinus.UseVisualStyleBackColor = true;
            this.bFrqMinus.Click += new System.EventHandler(this.bFrqMinus_Click);
            // 
            // lPel
            // 
            resources.ApplyResources(this.lPel, "lPel");
            this.lPel.Name = "lPel";
            // 
            // bIspPel
            // 
            resources.ApplyResources(this.bIspPel, "bIspPel");
            this.bIspPel.Name = "bIspPel";
            this.bIspPel.UseVisualStyleBackColor = true;
            this.bIspPel.Click += new System.EventHandler(this.bIspPel_Click);
            // 
            // comboBoxMode
            // 
            this.comboBoxMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            resources.ApplyResources(this.comboBoxMode, "comboBoxMode");
            this.comboBoxMode.FormattingEnabled = true;
            this.comboBoxMode.Items.AddRange(new object[] {
            resources.GetString("comboBoxMode.Items"),
            resources.GetString("comboBoxMode.Items1"),
            resources.GetString("comboBoxMode.Items2"),
            resources.GetString("comboBoxMode.Items3"),
            resources.GetString("comboBoxMode.Items4"),
            resources.GetString("comboBoxMode.Items5"),
            resources.GetString("comboBoxMode.Items6"),
            resources.GetString("comboBoxMode.Items7"),
            resources.GetString("comboBoxMode.Items8"),
            resources.GetString("comboBoxMode.Items9"),
            resources.GetString("comboBoxMode.Items10"),
            resources.GetString("comboBoxMode.Items11"),
            resources.GetString("comboBoxMode.Items12"),
            resources.GetString("comboBoxMode.Items13"),
            resources.GetString("comboBoxMode.Items14"),
            resources.GetString("comboBoxMode.Items15"),
            resources.GetString("comboBoxMode.Items16"),
            resources.GetString("comboBoxMode.Items17"),
            resources.GetString("comboBoxMode.Items18"),
            resources.GetString("comboBoxMode.Items19"),
            resources.GetString("comboBoxMode.Items20"),
            resources.GetString("comboBoxMode.Items21"),
            resources.GetString("comboBoxMode.Items22"),
            resources.GetString("comboBoxMode.Items23")});
            this.comboBoxMode.Name = "comboBoxMode";
            this.comboBoxMode.SelectedIndexChanged += new System.EventHandler(this.comboBoxMode_SelectedIndexChanged);
            // 
            // comboBoxHPF
            // 
            this.comboBoxHPF.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            resources.ApplyResources(this.comboBoxHPF, "comboBoxHPF");
            this.comboBoxHPF.FormattingEnabled = true;
            this.comboBoxHPF.Items.AddRange(new object[] {
            resources.GetString("comboBoxHPF.Items"),
            resources.GetString("comboBoxHPF.Items1"),
            resources.GetString("comboBoxHPF.Items2")});
            this.comboBoxHPF.Name = "comboBoxHPF";
            this.comboBoxHPF.SelectedIndexChanged += new System.EventHandler(this.comboBoxHPF_SelectedIndexChanged);
            // 
            // comboBoxLPF
            // 
            this.comboBoxLPF.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            resources.ApplyResources(this.comboBoxLPF, "comboBoxLPF");
            this.comboBoxLPF.FormattingEnabled = true;
            this.comboBoxLPF.Items.AddRange(new object[] {
            resources.GetString("comboBoxLPF.Items"),
            resources.GetString("comboBoxLPF.Items1"),
            resources.GetString("comboBoxLPF.Items2")});
            this.comboBoxLPF.Name = "comboBoxLPF";
            this.comboBoxLPF.SelectedIndexChanged += new System.EventHandler(this.comboBoxLPF_SelectedIndexChanged);
            // 
            // label9
            // 
            resources.ApplyResources(this.label9, "label9");
            this.label9.Name = "label9";
            // 
            // label10
            // 
            resources.ApplyResources(this.label10, "label10");
            this.label10.Name = "label10";
            // 
            // comboBoxBW
            // 
            this.comboBoxBW.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            resources.ApplyResources(this.comboBoxBW, "comboBoxBW");
            this.comboBoxBW.FormattingEnabled = true;
            this.comboBoxBW.Items.AddRange(new object[] {
            resources.GetString("comboBoxBW.Items"),
            resources.GetString("comboBoxBW.Items1"),
            resources.GetString("comboBoxBW.Items2"),
            resources.GetString("comboBoxBW.Items3"),
            resources.GetString("comboBoxBW.Items4"),
            resources.GetString("comboBoxBW.Items5"),
            resources.GetString("comboBoxBW.Items6"),
            resources.GetString("comboBoxBW.Items7"),
            resources.GetString("comboBoxBW.Items8"),
            resources.GetString("comboBoxBW.Items9")});
            this.comboBoxBW.Name = "comboBoxBW";
            this.comboBoxBW.SelectedIndexChanged += new System.EventHandler(this.comboBoxBW_SelectedIndexChanged);
            // 
            // b0
            // 
            resources.ApplyResources(this.b0, "b0");
            this.b0.Name = "b0";
            this.b0.Tag = "OFF";
            this.b0.UseVisualStyleBackColor = true;
            this.b0.Click += new System.EventHandler(this.b0_Click);
            // 
            // comboBoxAtt
            // 
            this.comboBoxAtt.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            resources.ApplyResources(this.comboBoxAtt, "comboBoxAtt");
            this.comboBoxAtt.FormattingEnabled = true;
            this.comboBoxAtt.Items.AddRange(new object[] {
            resources.GetString("comboBoxAtt.Items"),
            resources.GetString("comboBoxAtt.Items1"),
            resources.GetString("comboBoxAtt.Items2"),
            resources.GetString("comboBoxAtt.Items3"),
            resources.GetString("comboBoxAtt.Items4")});
            this.comboBoxAtt.Name = "comboBoxAtt";
            this.comboBoxAtt.SelectedIndexChanged += new System.EventHandler(this.comboBoxAtt_SelectedIndexChanged);
            // 
            // b4
            // 
            resources.ApplyResources(this.b4, "b4");
            this.b4.Name = "b4";
            this.b4.Tag = "OFF";
            this.b4.UseVisualStyleBackColor = true;
            this.b4.Click += new System.EventHandler(this.b4_Click);
            // 
            // label11
            // 
            resources.ApplyResources(this.label11, "label11");
            this.label11.Name = "label11";
            // 
            // b5
            // 
            resources.ApplyResources(this.b5, "b5");
            this.b5.Name = "b5";
            this.b5.Tag = "OFF";
            this.b5.UseVisualStyleBackColor = true;
            this.b5.Click += new System.EventHandler(this.b5_Click);
            // 
            // b2
            // 
            resources.ApplyResources(this.b2, "b2");
            this.b2.Name = "b2";
            this.b2.Tag = "OFF";
            this.b2.UseVisualStyleBackColor = true;
            this.b2.Click += new System.EventHandler(this.b2_Click);
            // 
            // label16
            // 
            resources.ApplyResources(this.label16, "label16");
            this.label16.Name = "label16";
            // 
            // b3
            // 
            resources.ApplyResources(this.b3, "b3");
            this.b3.Name = "b3";
            this.b3.Tag = "OFF";
            this.b3.UseVisualStyleBackColor = true;
            this.b3.Click += new System.EventHandler(this.b3_Click);
            // 
            // bESC
            // 
            resources.ApplyResources(this.bESC, "bESC");
            this.bESC.Name = "bESC";
            this.bESC.Tag = "OFF";
            this.bESC.UseVisualStyleBackColor = true;
            this.bESC.Click += new System.EventHandler(this.bESC_Click);
            // 
            // b7
            // 
            resources.ApplyResources(this.b7, "b7");
            this.b7.Name = "b7";
            this.b7.Tag = "OFF";
            this.b7.UseVisualStyleBackColor = true;
            this.b7.Click += new System.EventHandler(this.b7_Click);
            // 
            // b6
            // 
            resources.ApplyResources(this.b6, "b6");
            this.b6.Name = "b6";
            this.b6.Tag = "OFF";
            this.b6.UseVisualStyleBackColor = true;
            this.b6.Click += new System.EventHandler(this.b6_Click);
            // 
            // label17
            // 
            resources.ApplyResources(this.label17, "label17");
            this.label17.Name = "label17";
            // 
            // b1
            // 
            resources.ApplyResources(this.b1, "b1");
            this.b1.Name = "b1";
            this.b1.Tag = "OFF";
            this.b1.UseVisualStyleBackColor = true;
            this.b1.Click += new System.EventHandler(this.b1_Click);
            // 
            // b9
            // 
            resources.ApplyResources(this.b9, "b9");
            this.b9.Name = "b9";
            this.b9.Tag = "OFF";
            this.b9.UseVisualStyleBackColor = true;
            this.b9.Click += new System.EventHandler(this.b9_Click);
            // 
            // b8
            // 
            resources.ApplyResources(this.b8, "b8");
            this.b8.Name = "b8";
            this.b8.Tag = "OFF";
            this.b8.UseVisualStyleBackColor = true;
            this.b8.Click += new System.EventHandler(this.b8_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.cbAGC);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this.trackBarManualGain);
            resources.ApplyResources(this.groupBox3, "groupBox3");
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.TabStop = false;
            // 
            // cbAGC
            // 
            this.cbAGC.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbAGC.FormattingEnabled = true;
            this.cbAGC.Items.AddRange(new object[] {
            resources.GetString("cbAGC.Items"),
            resources.GetString("cbAGC.Items1"),
            resources.GetString("cbAGC.Items2"),
            resources.GetString("cbAGC.Items3")});
            resources.ApplyResources(this.cbAGC, "cbAGC");
            this.cbAGC.Name = "cbAGC";
            this.cbAGC.SelectedIndexChanged += new System.EventHandler(this.cbAGC_SelectedIndexChanged);
            // 
            // label13
            // 
            resources.ApplyResources(this.label13, "label13");
            this.label13.Name = "label13";
            // 
            // trackBarManualGain
            // 
            resources.ApplyResources(this.trackBarManualGain, "trackBarManualGain");
            this.trackBarManualGain.Maximum = 110;
            this.trackBarManualGain.Name = "trackBarManualGain";
            this.trackBarManualGain.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trackBarManualGain.Value = 25;
            this.trackBarManualGain.Scroll += new System.EventHandler(this.trackBarManualGain_Scroll);
            // 
            // pScreen
            // 
            this.pScreen.BackColor = System.Drawing.Color.Black;
            this.pScreen.Controls.Add(this.lFRQFromAOR);
            this.pScreen.Controls.Add(this.lFrq);
            this.pScreen.Controls.Add(this.lBw);
            this.pScreen.Controls.Add(this.lat);
            this.pScreen.Controls.Add(this.lMode);
            this.pScreen.Controls.Add(this.lLevel);
            this.pScreen.Controls.Add(this.lAmplifier);
            resources.ApplyResources(this.pScreen, "pScreen");
            this.pScreen.Name = "pScreen";
            // 
            // lFRQFromAOR
            // 
            resources.ApplyResources(this.lFRQFromAOR, "lFRQFromAOR");
            this.lFRQFromAOR.BackColor = System.Drawing.Color.Black;
            this.lFRQFromAOR.ForeColor = System.Drawing.Color.Green;
            this.lFRQFromAOR.Name = "lFRQFromAOR";
            // 
            // lFrq
            // 
            resources.ApplyResources(this.lFrq, "lFrq");
            this.lFrq.ForeColor = System.Drawing.Color.Green;
            this.lFrq.Name = "lFrq";
            // 
            // lBw
            // 
            resources.ApplyResources(this.lBw, "lBw");
            this.lBw.ForeColor = System.Drawing.Color.Green;
            this.lBw.Name = "lBw";
            // 
            // lat
            // 
            resources.ApplyResources(this.lat, "lat");
            this.lat.ForeColor = System.Drawing.Color.Green;
            this.lat.Name = "lat";
            // 
            // lMode
            // 
            resources.ApplyResources(this.lMode, "lMode");
            this.lMode.ForeColor = System.Drawing.Color.Green;
            this.lMode.Name = "lMode";
            // 
            // lLevel
            // 
            resources.ApplyResources(this.lLevel, "lLevel");
            this.lLevel.ForeColor = System.Drawing.Color.Green;
            this.lLevel.Name = "lLevel";
            // 
            // lAmplifier
            // 
            resources.ApplyResources(this.lAmplifier, "lAmplifier");
            this.lAmplifier.ForeColor = System.Drawing.Color.Green;
            this.lAmplifier.Name = "lAmplifier";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.radioButton1);
            this.groupBox1.Controls.Add(this.radioButton2);
            resources.ApplyResources(this.groupBox1, "groupBox1");
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.TabStop = false;
            // 
            // radioButton1
            // 
            resources.ApplyResources(this.radioButton1, "radioButton1");
            this.radioButton1.Checked = true;
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.TabStop = true;
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // radioButton2
            // 
            resources.ApplyResources(this.radioButton2, "radioButton2");
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.UseVisualStyleBackColor = true;
            this.radioButton2.CheckedChanged += new System.EventHandler(this.radioButton2_CheckedChanged);
            // 
            // panel22
            // 
            resources.ApplyResources(this.panel22, "panel22");
            this.panel22.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel22.Controls.Add(this.panel23);
            this.panel22.Controls.Add(this.intensityGraph1);
            this.panel22.Controls.Add(this.cbIntensityPaint);
            this.panel22.Name = "panel22";
            // 
            // panel23
            // 
            this.panel23.Controls.Add(this.lREC);
            this.panel23.Controls.Add(this.cbARec);
            this.panel23.Controls.Add(this.cbRec);
            this.panel23.Controls.Add(this.cbACP);
            this.panel23.Controls.Add(this.cbAverage);
            this.panel23.Controls.Add(this.label21);
            this.panel23.Controls.Add(this.label20);
            this.panel23.Controls.Add(this.waveformGraph1);
            this.panel23.Controls.Add(this.tbWaterflowMin);
            this.panel23.Controls.Add(this.tbWaterflowMax);
            resources.ApplyResources(this.panel23, "panel23");
            this.panel23.Name = "panel23";
            // 
            // lREC
            // 
            resources.ApplyResources(this.lREC, "lREC");
            this.lREC.BackColor = System.Drawing.Color.Black;
            this.lREC.ForeColor = System.Drawing.Color.Red;
            this.lREC.Name = "lREC";
            // 
            // cbARec
            // 
            resources.ApplyResources(this.cbARec, "cbARec");
            this.cbARec.Image = global::ControlAR6000.Properties.Resources.a_rec_grey;
            this.cbARec.Name = "cbARec";
            this.cbARec.UseVisualStyleBackColor = true;
            this.cbARec.CheckedChanged += new System.EventHandler(this.cbARec_CheckedChanged);
            // 
            // cbRec
            // 
            resources.ApplyResources(this.cbRec, "cbRec");
            this.cbRec.Image = global::ControlAR6000.Properties.Resources.rec_grey;
            this.cbRec.Name = "cbRec";
            this.cbRec.UseVisualStyleBackColor = true;
            this.cbRec.CheckedChanged += new System.EventHandler(this.cbRec_CheckedChanged);
            // 
            // cbACP
            // 
            resources.ApplyResources(this.cbACP, "cbACP");
            this.cbACP.Checked = true;
            this.cbACP.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbACP.Image = global::ControlAR6000.Properties.Resources.stop;
            this.cbACP.Name = "cbACP";
            this.cbACP.UseVisualStyleBackColor = true;
            this.cbACP.CheckedChanged += new System.EventHandler(this.cbACP_CheckedChanged);
            // 
            // cbAverage
            // 
            resources.ApplyResources(this.cbAverage, "cbAverage");
            this.cbAverage.Image = global::ControlAR6000.Properties.Resources.average_gray;
            this.cbAverage.Name = "cbAverage";
            this.cbAverage.UseVisualStyleBackColor = true;
            this.cbAverage.CheckedChanged += new System.EventHandler(this.cbAverage_CheckedChanged);
            // 
            // label21
            // 
            resources.ApplyResources(this.label21, "label21");
            this.label21.Name = "label21";
            // 
            // label20
            // 
            resources.ApplyResources(this.label20, "label20");
            this.label20.Name = "label20";
            // 
            // waveformGraph1
            // 
            resources.ApplyResources(this.waveformGraph1, "waveformGraph1");
            this.waveformGraph1.Border = NationalInstruments.UI.Border.None;
            this.waveformGraph1.Name = "waveformGraph1";
            this.waveformGraph1.Plots.AddRange(new NationalInstruments.UI.WaveformPlot[] {
            this.PlotSignal,
            this.PlotAverage,
            this.MinPorogPlot,
            this.MaxPorogPlot,
            this.PlotPorogObn});
            this.waveformGraph1.UseColorGenerator = true;
            this.waveformGraph1.XAxes.AddRange(new NationalInstruments.UI.XAxis[] {
            this.xAxis1});
            this.waveformGraph1.YAxes.AddRange(new NationalInstruments.UI.YAxis[] {
            this.yAxis1});
            // 
            // PlotSignal
            // 
            this.PlotSignal.XAxis = this.xAxis1;
            this.PlotSignal.YAxis = this.yAxis1;
            // 
            // xAxis1
            // 
            this.xAxis1.BaseLineVisible = true;
            this.xAxis1.Caption = "Частота, Гц";
            this.xAxis1.CaptionFont = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.xAxis1.EndLabelsAlwaysVisible = false;
            this.xAxis1.Mode = NationalInstruments.UI.AxisMode.Fixed;
            this.xAxis1.Range = new NationalInstruments.UI.Range(0D, 22050D);
            // 
            // yAxis1
            // 
            this.yAxis1.Caption = "Уровень, дБм";
            this.yAxis1.CaptionFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.yAxis1.CaptionPosition = NationalInstruments.UI.YAxisPosition.Right;
            this.yAxis1.MajorDivisions.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.yAxis1.MajorDivisions.GridLineStyle = NationalInstruments.UI.LineStyle.Dot;
            this.yAxis1.MajorDivisions.GridVisible = true;
            this.yAxis1.MajorDivisions.TickVisible = false;
            this.yAxis1.Mode = NationalInstruments.UI.AxisMode.Fixed;
            this.yAxis1.Position = NationalInstruments.UI.YAxisPosition.Right;
            this.yAxis1.Range = new NationalInstruments.UI.Range(-120D, 0D);
            // 
            // PlotAverage
            // 
            this.PlotAverage.LineColor = System.Drawing.Color.White;
            this.PlotAverage.LineColorPrecedence = NationalInstruments.UI.ColorPrecedence.UserDefinedColor;
            this.PlotAverage.XAxis = this.xAxis1;
            this.PlotAverage.YAxis = this.yAxis1;
            // 
            // MinPorogPlot
            // 
            this.MinPorogPlot.LineColor = System.Drawing.Color.Gray;
            this.MinPorogPlot.LineColorPrecedence = NationalInstruments.UI.ColorPrecedence.UserDefinedColor;
            this.MinPorogPlot.LineStyle = NationalInstruments.UI.LineStyle.Dash;
            this.MinPorogPlot.XAxis = this.xAxis1;
            this.MinPorogPlot.YAxis = this.yAxis1;
            // 
            // MaxPorogPlot
            // 
            this.MaxPorogPlot.LineColor = System.Drawing.Color.Gray;
            this.MaxPorogPlot.LineColorPrecedence = NationalInstruments.UI.ColorPrecedence.UserDefinedColor;
            this.MaxPorogPlot.LineStyle = NationalInstruments.UI.LineStyle.Dash;
            this.MaxPorogPlot.XAxis = this.xAxis1;
            this.MaxPorogPlot.YAxis = this.yAxis1;
            // 
            // PlotPorogObn
            // 
            this.PlotPorogObn.BasePlot = this.PlotSignal;
            this.PlotPorogObn.LineColor = System.Drawing.Color.DarkGray;
            this.PlotPorogObn.LineColorPrecedence = NationalInstruments.UI.ColorPrecedence.UserDefinedColor;
            this.PlotPorogObn.XAxis = this.xAxis1;
            this.PlotPorogObn.YAxis = this.yAxis1;
            // 
            // tbWaterflowMin
            // 
            resources.ApplyResources(this.tbWaterflowMin, "tbWaterflowMin");
            this.tbWaterflowMin.Maximum = 0;
            this.tbWaterflowMin.Minimum = -120;
            this.tbWaterflowMin.Name = "tbWaterflowMin";
            this.tbWaterflowMin.TickStyle = System.Windows.Forms.TickStyle.None;
            this.tbWaterflowMin.Value = -110;
            this.tbWaterflowMin.Scroll += new System.EventHandler(this.tbWaterflowMin_Scroll);
            // 
            // tbWaterflowMax
            // 
            resources.ApplyResources(this.tbWaterflowMax, "tbWaterflowMax");
            this.tbWaterflowMax.Maximum = 0;
            this.tbWaterflowMax.Minimum = -120;
            this.tbWaterflowMax.Name = "tbWaterflowMax";
            this.tbWaterflowMax.TickStyle = System.Windows.Forms.TickStyle.None;
            this.tbWaterflowMax.Value = -40;
            this.tbWaterflowMax.Scroll += new System.EventHandler(this.tbWaterflowMax_Scroll);
            // 
            // intensityGraph1
            // 
            resources.ApplyResources(this.intensityGraph1, "intensityGraph1");
            this.intensityGraph1.Border = NationalInstruments.UI.Border.None;
            this.intensityGraph1.CaptionVisible = false;
            this.intensityGraph1.ColorScales.AddRange(new NationalInstruments.UI.ColorScale[] {
            this.colorScale1});
            this.intensityGraph1.Name = "intensityGraph1";
            this.intensityGraph1.Plots.AddRange(new NationalInstruments.UI.IntensityPlot[] {
            this.intensityPlot1});
            this.intensityGraph1.XAxes.AddRange(new NationalInstruments.UI.IntensityXAxis[] {
            this.intensityXAxis1});
            this.intensityGraph1.YAxes.AddRange(new NationalInstruments.UI.IntensityYAxis[] {
            this.intensityYAxis1});
            // 
            // colorScale1
            // 
            this.colorScale1.ColorMap.AddRange(new NationalInstruments.UI.ColorMapEntry[] {
            new NationalInstruments.UI.ColorMapEntry(-80D, System.Drawing.Color.Blue),
            new NationalInstruments.UI.ColorMapEntry(-40D, System.Drawing.Color.Lime)});
            this.colorScale1.HighColor = System.Drawing.Color.Red;
            this.colorScale1.Range = new NationalInstruments.UI.Range(-120D, 0D);
            // 
            // intensityPlot1
            // 
            this.intensityPlot1.ColorScale = this.colorScale1;
            this.intensityPlot1.PixelInterpolation = true;
            this.intensityPlot1.XAxis = this.intensityXAxis1;
            this.intensityPlot1.YAxis = this.intensityYAxis1;
            // 
            // intensityXAxis1
            // 
            this.intensityXAxis1.Mode = NationalInstruments.UI.IntensityAxisMode.StripChart;
            this.intensityXAxis1.Range = new NationalInstruments.UI.Range(0D, 2205D);
            this.intensityXAxis1.Visible = false;
            // 
            // intensityYAxis1
            // 
            this.intensityYAxis1.Range = new NationalInstruments.UI.Range(0D, 100D);
            this.intensityYAxis1.Visible = false;
            // 
            // cbIntensityPaint
            // 
            resources.ApplyResources(this.cbIntensityPaint, "cbIntensityPaint");
            this.cbIntensityPaint.Image = global::ControlAR6000.Properties.Resources.play;
            this.cbIntensityPaint.Name = "cbIntensityPaint";
            this.cbIntensityPaint.UseVisualStyleBackColor = true;
            this.cbIntensityPaint.CheckedChanged += new System.EventHandler(this.cbIntensityPaint_CheckedChanged);
            // 
            // tabControl1
            // 
            resources.ApplyResources(this.tabControl1, "tabControl1");
            this.tabControl1.Controls.Add(this.tpIRI_KRPU);
            this.tabControl1.Controls.Add(this.tpPlayer);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Selecting += new System.Windows.Forms.TabControlCancelEventHandler(this.tabControl1_Selecting);
            // 
            // tpIRI_KRPU
            // 
            this.tpIRI_KRPU.Controls.Add(this.bAddToTableFromAOR);
            this.tpIRI_KRPU.Controls.Add(this.nudTableFRQ);
            this.tpIRI_KRPU.Controls.Add(this.dgv);
            this.tpIRI_KRPU.Controls.Add(this.tbNote);
            this.tpIRI_KRPU.Controls.Add(this.bTableChange);
            this.tpIRI_KRPU.Controls.Add(this.nudPause);
            this.tpIRI_KRPU.Controls.Add(this.bTableAdd);
            this.tpIRI_KRPU.Controls.Add(this.label18);
            this.tpIRI_KRPU.Controls.Add(this.bTableDel);
            this.tpIRI_KRPU.Controls.Add(this.cbTablePriority);
            this.tpIRI_KRPU.Controls.Add(this.bTableClear);
            this.tpIRI_KRPU.Controls.Add(this.nudTablePorogObnar);
            this.tpIRI_KRPU.Controls.Add(this.label8);
            this.tpIRI_KRPU.Controls.Add(this.label12);
            this.tpIRI_KRPU.Controls.Add(this.label19);
            this.tpIRI_KRPU.Controls.Add(this.label14);
            resources.ApplyResources(this.tpIRI_KRPU, "tpIRI_KRPU");
            this.tpIRI_KRPU.Name = "tpIRI_KRPU";
            this.tpIRI_KRPU.UseVisualStyleBackColor = true;
            // 
            // bAddToTableFromAOR
            // 
            this.bAddToTableFromAOR.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            resources.ApplyResources(this.bAddToTableFromAOR, "bAddToTableFromAOR");
            this.bAddToTableFromAOR.Name = "bAddToTableFromAOR";
            this.bAddToTableFromAOR.UseVisualStyleBackColor = true;
            this.bAddToTableFromAOR.Click += new System.EventHandler(this.bAddToTableFromAOR_Click);
            // 
            // nudTableFRQ
            // 
            this.nudTableFRQ.DecimalPlaces = 2;
            this.nudTableFRQ.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            resources.ApplyResources(this.nudTableFRQ, "nudTableFRQ");
            this.nudTableFRQ.Maximum = new decimal(new int[] {
            3300000,
            0,
            0,
            0});
            this.nudTableFRQ.Minimum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.nudTableFRQ.Name = "nudTableFRQ";
            this.nudTableFRQ.Value = new decimal(new int[] {
            99500,
            0,
            0,
            0});
            this.nudTableFRQ.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.nudTableFRQ_KeyPress);
            this.nudTableFRQ.KeyUp += new System.Windows.Forms.KeyEventHandler(this.nudTableFRQ_KeyUp);
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            resources.ApplyResources(this.dgv, "dgv");
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Number,
            this.OnOff,
            this.Fequency,
            this.Pause,
            this.Treshold,
            this.TimeFirst,
            this.Att,
            this.Mode,
            this.BW,
            this.HPF,
            this.LPF,
            this.Priority,
            this.Note});
            this.dgv.MultiSelect = false;
            this.dgv.Name = "dgv";
            this.dgv.RowHeadersVisible = false;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_CellClick);
            this.dgv.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_CellContentClick);
            this.dgv.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_CellDoubleClick);
            this.dgv.Resize += new System.EventHandler(this.dgv_Resize);
            // 
            // Number
            // 
            resources.ApplyResources(this.Number, "Number");
            this.Number.Name = "Number";
            this.Number.ReadOnly = true;
            this.Number.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // OnOff
            // 
            resources.ApplyResources(this.OnOff, "OnOff");
            this.OnOff.Name = "OnOff";
            this.OnOff.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // Fequency
            // 
            resources.ApplyResources(this.Fequency, "Fequency");
            this.Fequency.Name = "Fequency";
            this.Fequency.ReadOnly = true;
            this.Fequency.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Pause
            // 
            resources.ApplyResources(this.Pause, "Pause");
            this.Pause.Name = "Pause";
            this.Pause.ReadOnly = true;
            this.Pause.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Treshold
            // 
            resources.ApplyResources(this.Treshold, "Treshold");
            this.Treshold.Name = "Treshold";
            this.Treshold.ReadOnly = true;
            this.Treshold.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // TimeFirst
            // 
            resources.ApplyResources(this.TimeFirst, "TimeFirst");
            this.TimeFirst.Name = "TimeFirst";
            this.TimeFirst.ReadOnly = true;
            this.TimeFirst.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Att
            // 
            resources.ApplyResources(this.Att, "Att");
            this.Att.Name = "Att";
            this.Att.ReadOnly = true;
            this.Att.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Att.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Mode
            // 
            resources.ApplyResources(this.Mode, "Mode");
            this.Mode.Name = "Mode";
            this.Mode.ReadOnly = true;
            this.Mode.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Mode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // BW
            // 
            resources.ApplyResources(this.BW, "BW");
            this.BW.Name = "BW";
            this.BW.ReadOnly = true;
            this.BW.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.BW.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // HPF
            // 
            resources.ApplyResources(this.HPF, "HPF");
            this.HPF.Name = "HPF";
            this.HPF.ReadOnly = true;
            this.HPF.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.HPF.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // LPF
            // 
            resources.ApplyResources(this.LPF, "LPF");
            this.LPF.Name = "LPF";
            this.LPF.ReadOnly = true;
            this.LPF.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.LPF.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Priority
            // 
            resources.ApplyResources(this.Priority, "Priority");
            this.Priority.Name = "Priority";
            this.Priority.ReadOnly = true;
            this.Priority.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Priority.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Note
            // 
            resources.ApplyResources(this.Note, "Note");
            this.Note.Name = "Note";
            this.Note.ReadOnly = true;
            this.Note.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // tbNote
            // 
            resources.ApplyResources(this.tbNote, "tbNote");
            this.tbNote.Name = "tbNote";
            this.tbNote.Validating += new System.ComponentModel.CancelEventHandler(this.tbNote_Validating);
            // 
            // bTableChange
            // 
            this.bTableChange.ForeColor = System.Drawing.Color.Green;
            resources.ApplyResources(this.bTableChange, "bTableChange");
            this.bTableChange.Name = "bTableChange";
            this.bTableChange.Tag = "OFF";
            this.bTableChange.UseVisualStyleBackColor = true;
            this.bTableChange.Click += new System.EventHandler(this.bTableChange_Click);
            // 
            // nudPause
            // 
            this.nudPause.Increment = new decimal(new int[] {
            50,
            0,
            0,
            0});
            resources.ApplyResources(this.nudPause, "nudPause");
            this.nudPause.Maximum = new decimal(new int[] {
            60000,
            0,
            0,
            0});
            this.nudPause.Minimum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.nudPause.Name = "nudPause";
            this.nudPause.Value = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.nudPause.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.nudPause_KeyPress);
            this.nudPause.Validated += new System.EventHandler(this.nudPause_Validated);
            // 
            // bTableAdd
            // 
            this.bTableAdd.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            resources.ApplyResources(this.bTableAdd, "bTableAdd");
            this.bTableAdd.Name = "bTableAdd";
            this.bTableAdd.Tag = "OFF";
            this.bTableAdd.UseVisualStyleBackColor = true;
            this.bTableAdd.Click += new System.EventHandler(this.bTableAdd_Click);
            // 
            // label18
            // 
            resources.ApplyResources(this.label18, "label18");
            this.label18.Name = "label18";
            // 
            // bTableDel
            // 
            this.bTableDel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            resources.ApplyResources(this.bTableDel, "bTableDel");
            this.bTableDel.Name = "bTableDel";
            this.bTableDel.Tag = "OFF";
            this.bTableDel.UseVisualStyleBackColor = true;
            this.bTableDel.Click += new System.EventHandler(this.bTableDel_Click);
            // 
            // cbTablePriority
            // 
            this.cbTablePriority.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbTablePriority.FormattingEnabled = true;
            this.cbTablePriority.Items.AddRange(new object[] {
            resources.GetString("cbTablePriority.Items"),
            resources.GetString("cbTablePriority.Items1")});
            resources.ApplyResources(this.cbTablePriority, "cbTablePriority");
            this.cbTablePriority.Name = "cbTablePriority";
            // 
            // bTableClear
            // 
            this.bTableClear.ForeColor = System.Drawing.Color.Red;
            resources.ApplyResources(this.bTableClear, "bTableClear");
            this.bTableClear.Name = "bTableClear";
            this.bTableClear.Tag = "OFF";
            this.bTableClear.UseVisualStyleBackColor = true;
            this.bTableClear.Click += new System.EventHandler(this.bTableClear_Click);
            // 
            // nudTablePorogObnar
            // 
            resources.ApplyResources(this.nudTablePorogObnar, "nudTablePorogObnar");
            this.nudTablePorogObnar.Maximum = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.nudTablePorogObnar.Minimum = new decimal(new int[] {
            120,
            0,
            0,
            -2147483648});
            this.nudTablePorogObnar.Name = "nudTablePorogObnar";
            this.nudTablePorogObnar.Value = new decimal(new int[] {
            110,
            0,
            0,
            -2147483648});
            this.nudTablePorogObnar.ValueChanged += new System.EventHandler(this.nudTablePorogObnar_ValueChanged);
            // 
            // label8
            // 
            resources.ApplyResources(this.label8, "label8");
            this.label8.Name = "label8";
            // 
            // label12
            // 
            resources.ApplyResources(this.label12, "label12");
            this.label12.Name = "label12";
            // 
            // label19
            // 
            resources.ApplyResources(this.label19, "label19");
            this.label19.Name = "label19";
            // 
            // label14
            // 
            resources.ApplyResources(this.label14, "label14");
            this.label14.Name = "label14";
            // 
            // tpPlayer
            // 
            this.tpPlayer.Controls.Add(this.bStop);
            this.tpPlayer.Controls.Add(this.bPause);
            this.tpPlayer.Controls.Add(this.checkBox1);
            this.tpPlayer.Controls.Add(this.bWavPlayer);
            this.tpPlayer.Controls.Add(this.listBox2);
            this.tpPlayer.Controls.Add(this.listBox1);
            this.tpPlayer.Controls.Add(this.pictureBox18);
            resources.ApplyResources(this.tpPlayer, "tpPlayer");
            this.tpPlayer.Name = "tpPlayer";
            this.tpPlayer.UseVisualStyleBackColor = true;
            // 
            // bStop
            // 
            this.bStop.Image = global::ControlAR6000.Properties.Resources.Stop1;
            resources.ApplyResources(this.bStop, "bStop");
            this.bStop.Name = "bStop";
            this.bStop.UseVisualStyleBackColor = true;
            this.bStop.Click += new System.EventHandler(this.bStop_Click);
            // 
            // bPause
            // 
            this.bPause.Image = global::ControlAR6000.Properties.Resources.Pause;
            resources.ApplyResources(this.bPause, "bPause");
            this.bPause.Name = "bPause";
            this.bPause.UseVisualStyleBackColor = true;
            this.bPause.Click += new System.EventHandler(this.bPause_Click);
            // 
            // checkBox1
            // 
            resources.ApplyResources(this.checkBox1, "checkBox1");
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // bWavPlayer
            // 
            resources.ApplyResources(this.bWavPlayer, "bWavPlayer");
            this.bWavPlayer.Name = "bWavPlayer";
            this.bWavPlayer.UseVisualStyleBackColor = true;
            this.bWavPlayer.Click += new System.EventHandler(this.bWavPlayer_Click);
            // 
            // listBox2
            // 
            this.listBox2.FormattingEnabled = true;
            resources.ApplyResources(this.listBox2, "listBox2");
            this.listBox2.Name = "listBox2";
            this.listBox2.DoubleClick += new System.EventHandler(this.listBox2_DoubleClick);
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            resources.ApplyResources(this.listBox1, "listBox1");
            this.listBox1.Name = "listBox1";
            this.listBox1.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
            // 
            // pictureBox18
            // 
            resources.ApplyResources(this.pictureBox18, "pictureBox18");
            this.pictureBox18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox18.Image = global::ControlAR6000.Properties.Resources.wavPlayerGray;
            this.pictureBox18.Name = "pictureBox18";
            this.pictureBox18.TabStop = false;
            // 
            // bAOR
            // 
            this.bAOR.BackColor = System.Drawing.Color.Red;
            resources.ApplyResources(this.bAOR, "bAOR");
            this.bAOR.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.bAOR.FlatAppearance.BorderSize = 0;
            this.bAOR.Name = "bAOR";
            this.bAOR.UseVisualStyleBackColor = false;
            this.bAOR.Click += new System.EventHandler(this.bAOR_Click);
            // 
            // label23
            // 
            resources.ApplyResources(this.label23, "label23");
            this.label23.Name = "label23";
            // 
            // bENT
            // 
            resources.ApplyResources(this.bENT, "bENT");
            this.bENT.Name = "bENT";
            this.bENT.Tag = "OFF";
            this.bENT.UseVisualStyleBackColor = true;
            this.bENT.Click += new System.EventHandler(this.bENT_Click);
            // 
            // bDot
            // 
            resources.ApplyResources(this.bDot, "bDot");
            this.bDot.Name = "bDot";
            this.bDot.Tag = "OFF";
            this.bDot.UseVisualStyleBackColor = true;
            this.bDot.Click += new System.EventHandler(this.bDot_Click);
            // 
            // tm500
            // 
            this.tm500.Enabled = true;
            this.tm500.Tick += new System.EventHandler(this.tm500_Tick);
            // 
            // tmFlashFRQ
            // 
            this.tmFlashFRQ.Interval = 400;
            this.tmFlashFRQ.Tick += new System.EventHandler(this.tmFlashFRQ_Tick);
            // 
            // tmBaraban
            // 
            this.tmBaraban.Tick += new System.EventHandler(this.tmBaraban_Tick);
            // 
            // bFRSonRS
            // 
            resources.ApplyResources(this.bFRSonRS, "bFRSonRS");
            this.bFRSonRS.Name = "bFRSonRS";
            this.bFRSonRS.UseVisualStyleBackColor = true;
            this.bFRSonRS.Click += new System.EventHandler(this.bFRSonRS_Click);
            // 
            // pictureBox13
            // 
            this.pictureBox13.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox13.Image = global::ControlAR6000.Properties.Resources.gray;
            resources.ApplyResources(this.pictureBox13, "pictureBox13");
            this.pictureBox13.Name = "pictureBox13";
            this.pictureBox13.TabStop = false;
            // 
            // pictureBox14
            // 
            this.pictureBox14.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox14.Image = global::ControlAR6000.Properties.Resources.gray;
            resources.ApplyResources(this.pictureBox14, "pictureBox14");
            this.pictureBox14.Name = "pictureBox14";
            this.pictureBox14.TabStop = false;
            // 
            // controlAR6000
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.bFRSonRS);
            this.Controls.Add(this.numericUpDown1);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.bENT);
            this.Controls.Add(this.bDot);
            this.Controls.Add(this.pictureBox13);
            this.Controls.Add(this.pictureBox14);
            this.Controls.Add(this.bAOR);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.panel22);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.cbFrqStep);
            this.Controls.Add(this.bFrqPlus);
            this.Controls.Add(this.bFrqMinus);
            this.Controls.Add(this.lPel);
            this.Controls.Add(this.bIspPel);
            this.Controls.Add(this.comboBoxMode);
            this.Controls.Add(this.comboBoxHPF);
            this.Controls.Add(this.comboBoxLPF);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.comboBoxBW);
            this.Controls.Add(this.b0);
            this.Controls.Add(this.comboBoxAtt);
            this.Controls.Add(this.b4);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.b5);
            this.Controls.Add(this.b2);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.b3);
            this.Controls.Add(this.bESC);
            this.Controls.Add(this.b7);
            this.Controls.Add(this.b6);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.b1);
            this.Controls.Add(this.b9);
            this.Controls.Add(this.b8);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.pScreen);
            this.Controls.Add(this.groupBox1);
            this.Name = "controlAR6000";
            this.groupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tbAudioGain)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbSQL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarManualGain)).EndInit();
            this.pScreen.ResumeLayout(false);
            this.pScreen.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel22.ResumeLayout(false);
            this.panel23.ResumeLayout(false);
            this.panel23.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.waveformGraph1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbWaterflowMin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbWaterflowMax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intensityGraph1)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tpIRI_KRPU.ResumeLayout(false);
            this.tpIRI_KRPU.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudTableFRQ)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPause)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudTablePorogObnar)).EndInit();
            this.tpPlayer.ResumeLayout(false);
            this.tpPlayer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox cbSQL;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TrackBar tbSQL;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.ComboBox cbFrqStep;
        private System.Windows.Forms.Button bFrqPlus;
        private System.Windows.Forms.Button bFrqMinus;
        private System.Windows.Forms.Label lPel;
        private System.Windows.Forms.Button bIspPel;
        public System.Windows.Forms.ComboBox comboBoxMode;
        private System.Windows.Forms.ComboBox comboBoxHPF;
        private System.Windows.Forms.ComboBox comboBoxLPF;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        public System.Windows.Forms.ComboBox comboBoxBW;
        private System.Windows.Forms.Button b0;
        private System.Windows.Forms.ComboBox comboBoxAtt;
        private System.Windows.Forms.Button b4;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button b5;
        private System.Windows.Forms.Button b2;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Button b3;
        private System.Windows.Forms.Button bESC;
        private System.Windows.Forms.Button b7;
        private System.Windows.Forms.Button b6;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Button b1;
        private System.Windows.Forms.Button b9;
        private System.Windows.Forms.Button b8;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ComboBox cbAGC;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TrackBar trackBarManualGain;
        private System.Windows.Forms.Panel pScreen;
        private System.Windows.Forms.Label lFRQFromAOR;
        private System.Windows.Forms.Label lFrq;
        private System.Windows.Forms.Label lBw;
        private System.Windows.Forms.Label lat;
        private System.Windows.Forms.Label lMode;
        private System.Windows.Forms.Label lLevel;
        private System.Windows.Forms.Label lAmplifier;
        private System.Windows.Forms.GroupBox groupBox1;
        public System.Windows.Forms.RadioButton radioButton1;
        public System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.Panel panel22;
        private System.Windows.Forms.Panel panel23;
        private System.Windows.Forms.CheckBox cbARec;
        private System.Windows.Forms.CheckBox cbRec;
        private System.Windows.Forms.CheckBox cbACP;
        private System.Windows.Forms.CheckBox cbAverage;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label lREC;
        private NationalInstruments.UI.WindowsForms.WaveformGraph waveformGraph1;
        private NationalInstruments.UI.WaveformPlot PlotSignal;
        private NationalInstruments.UI.XAxis xAxis1;
        private NationalInstruments.UI.YAxis yAxis1;
        private NationalInstruments.UI.WaveformPlot PlotAverage;
        private NationalInstruments.UI.WaveformPlot MinPorogPlot;
        private NationalInstruments.UI.WaveformPlot MaxPorogPlot;
        private NationalInstruments.UI.WaveformPlot PlotPorogObn;
        private System.Windows.Forms.TrackBar tbWaterflowMin;
        private System.Windows.Forms.TrackBar tbWaterflowMax;
        private NationalInstruments.UI.WindowsForms.IntensityGraph intensityGraph1;
        private NationalInstruments.UI.ColorScale colorScale1;
        private NationalInstruments.UI.IntensityPlot intensityPlot1;
        private NationalInstruments.UI.IntensityXAxis intensityXAxis1;
        private NationalInstruments.UI.IntensityYAxis intensityYAxis1;
        private System.Windows.Forms.CheckBox cbIntensityPaint;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tpIRI_KRPU;
        private System.Windows.Forms.NumericUpDown nudTableFRQ;
        private System.Windows.Forms.DataGridView dgv;
        private System.Windows.Forms.DataGridViewTextBoxColumn Number;
        private System.Windows.Forms.DataGridViewCheckBoxColumn OnOff;
        private System.Windows.Forms.DataGridViewTextBoxColumn Fequency;
        private System.Windows.Forms.DataGridViewTextBoxColumn Pause;
        private System.Windows.Forms.DataGridViewTextBoxColumn Treshold;
        private System.Windows.Forms.DataGridViewTextBoxColumn TimeFirst;
        private System.Windows.Forms.DataGridViewTextBoxColumn Att;
        private System.Windows.Forms.DataGridViewTextBoxColumn Mode;
        private System.Windows.Forms.DataGridViewTextBoxColumn BW;
        private System.Windows.Forms.DataGridViewTextBoxColumn HPF;
        private System.Windows.Forms.DataGridViewTextBoxColumn LPF;
        private System.Windows.Forms.DataGridViewTextBoxColumn Priority;
        private System.Windows.Forms.DataGridViewTextBoxColumn Note;
        private System.Windows.Forms.TextBox tbNote;
        private System.Windows.Forms.Button bTableChange;
        private System.Windows.Forms.NumericUpDown nudPause;
        private System.Windows.Forms.Button bTableAdd;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Button bTableDel;
        private System.Windows.Forms.ComboBox cbTablePriority;
        private System.Windows.Forms.Button bTableClear;
        private System.Windows.Forms.NumericUpDown nudTablePorogObnar;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TabPage tpPlayer;
        public  System.Windows.Forms.PictureBox pictureBox13;
        public  System.Windows.Forms.PictureBox pictureBox14;
        public  System.Windows.Forms.Button bAOR;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Button bENT;
        private System.Windows.Forms.Button bDot;
        private System.Windows.Forms.Timer tm500;
        private System.Windows.Forms.Timer tmFlashFRQ;
        private System.Windows.Forms.Timer tmBaraban;
        private System.Windows.Forms.Button bWavPlayer;
        private System.Windows.Forms.ListBox listBox2;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.PictureBox pictureBox18;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Button bAddToTableFromAOR;
        private System.Windows.Forms.Button bFRSonRS;
        private System.Windows.Forms.Button bPause;
        private System.Windows.Forms.Button bStop;
        public System.Windows.Forms.TrackBar tbAudioGain;
    }
}
